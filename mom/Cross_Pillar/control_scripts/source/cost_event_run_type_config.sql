
--------------------------------------------------------
-- Copyright (c) 2012, Oracle Inc.  All rights reserved.
-- :   cost_event_run_type_config.sql  $
-- :   1.0  $
-- :   Wed Apr 11 15:07:22 CDT 2012  $
--------------------------------------------------------
REM -------------------------------------------------
REM
REM  Refreshes cost_event_run_type_config table.
REM
REM -------------------------------------------------
REM -------------------------------------------------
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
set define "^";
REM ****************************************************************
PROMPT updating/inserting into cost_event_run_type_config
REM ****************************************************************
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'CC' EVENT_TYPE, 'Cost Change' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'CZ' EVENT_TYPE, 'Cost Zone Location Move' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'D' EVENT_TYPE, 'Deal' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'DP' EVENT_TYPE, 'Deal Pass Through' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'ELC' EVENT_TYPE, 'Estimated Landed Cost Component' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'ICZ' EVENT_TYPE, 'Item Cost Zone Changes' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'MH' EVENT_TYPE, 'Merchandise Hierarchy' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'NIL' EVENT_TYPE, 'New Item Location' EVENT_DESC, 'SYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'OH' EVENT_TYPE, 'Organization Hierarchy' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'PP' EVENT_TYPE, 'Primary Pack Cost' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'R' EVENT_TYPE, 'Reclassification' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'RTC' EVENT_TYPE, 'Retail Change' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'SC' EVENT_TYPE, 'Item Supplier Country Location Relationship Changes' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'SH' EVENT_TYPE, 'Supplier Hierarchy' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'T' EVENT_TYPE, 'WF Cost Template' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'TR' EVENT_TYPE, 'WF Cost Template Relationship' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG ERR USING ( SELECT
'CL' EVENT_TYPE, 'Change in Costing Location of Franchise Store' EVENT_DESC, 'ASYNC' EVENT_RUN_TYPE, '1000' MAX_TRAN_SIZE
FROM DUAL) USE_THIS
ON (ERR.EVENT_TYPE = USE_THIS.EVENT_TYPE)
WHEN MATCHED THEN UPDATE
SET ERR.EVENT_DESC = USE_THIS.EVENT_DESC, ERR.EVENT_RUN_TYPE = USE_THIS.EVENT_RUN_TYPE, ERR.MAX_TRAN_SIZE = USE_THIS.MAX_TRAN_SIZE
WHEN NOT MATCHED THEN INSERT (EVENT_TYPE,EVENT_DESC,EVENT_RUN_TYPE,MAX_TRAN_SIZE)
VALUES (USE_THIS.EVENT_TYPE,USE_THIS.EVENT_DESC,USE_THIS.EVENT_RUN_TYPE,USE_THIS.MAX_TRAN_SIZE);
------------------------------------------------------------------------
set define "&";
commit;