SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
----------------------------------------------------------------------------------
-- Copyright (c) 2014, Oracle Inc.  All rights reserved.
-- :   coresvc_item_config.sql  $
----------------------------------------------------------------------------------
REM ------------------------------------------------------------------------------
REM
REM  Insert into coresvc_item_config table if entry does not exist.
REM
REM ------------------------------------------------------------------------------

DECLARE
   L_item_config_exist   VARCHAR2(1);

   cursor C_ITEM_CONFIG_EXIST is
      select 'x'
        from coresvc_item_config;

BEGIN
   open C_ITEM_CONFIG_EXIST;
   fetch C_ITEM_CONFIG_EXIST into L_item_config_exist;
   close C_ITEM_CONFIG_EXIST;

   if L_item_config_exist is NULL then
      insert into coresvc_item_config(max_chunk_size,
                                      proc_err_retention_days,
                                      cascade_uda_details,
                                      cascade_iud_item_supp_country,
                                      cascade_iud_isc_dimensions,
                                      cascade_iud_ismc,
                                      isc_update_all_locs,
                                      isc_update_all_child_locs,
                                      cascade_vat_item,
                                      cascade_iim_details,
                                      cascade_iud_item_supplier,
                                      max_threads,
                                      wait_btwn_threads,
                                      max_item_resv_qty,
                                      max_item_expiry_days,
                                      default_all_locs_iscl)
                               values(5,
                                      5,
                                      'N',
                                      'N',
                                      'N',
                                      'N',
                                      'N',
                                      'N',
                                      'N',
                                      'Y',
                                      'Y',
                                      1,
                                      1000,
                                      1,
                                      1,
                                      'Y');
   else
      update coresvc_item_config
         set max_chunk_size = NVL(max_chunk_size, 5),
             proc_err_retention_days = NVL(proc_err_retention_days, 5),
             cascade_uda_details = NVL(cascade_uda_details, 'N'),
             cascade_iud_item_supp_country = NVL(cascade_iud_item_supp_country, 'N'),
             cascade_iud_isc_dimensions = NVL(cascade_iud_isc_dimensions, 'N'),
             cascade_iud_ismc = NVL(cascade_iud_ismc, 'N'),
             isc_update_all_locs = NVL(isc_update_all_locs, 'N'),
             isc_update_all_child_locs = NVL(isc_update_all_child_locs, 'N'),
             cascade_vat_item = NVL(cascade_vat_item, 'N'),
             cascade_iim_details = NVL(cascade_iim_details, 'Y'),
             cascade_iud_item_supplier = NVL(cascade_iud_item_supplier, 'Y'),
             max_threads = NVL(max_threads, 1),
             wait_btwn_threads = NVL(wait_btwn_threads, 1000),
             max_item_resv_qty = NVL(max_item_resv_qty, 1),
             max_item_expiry_days = NVL(max_item_expiry_days, 1),
             default_all_locs_iscl = NVL(default_all_locs_iscl,'Y');
   end if;
END;
/
REM ****** (commiting...) ********************************************
COMMIT;