SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
----------------------------------------------------------------------------------
-- Copyright (c) 2014, Oracle Inc.  All rights reserved.
-- :   deal_comp_type.sql  $
----------------------------------------------------------------------------------
REM ------------------------------------------------------------------------------
REM
REM  Refreshes deal_comp_type  and deal_comp_type_tl tables.
REM
REM ------------------------------------------------------------------------------
REM ******************************************************************************
PROMPT Updating/inserting into DEAL_COMP_TYPE and DEAL_COMP_TYPE_TL
REM ******************************************************************************
-----------------------------------------------------------------------------------
MERGE INTO DEAL_COMP_TYPE DEALTYPE USING ( SELECT
'VFP' DEAL_COMP_TYPE
FROM DUAL) USE_THIS
ON (DEALTYPE.DEAL_COMP_TYPE = USE_THIS.DEAL_COMP_TYPE)
WHEN NOT MATCHED THEN INSERT (DEAL_COMP_TYPE)
VALUES (USE_THIS.DEAL_COMP_TYPE);

MERGE INTO DEAL_COMP_TYPE_TL DEALTYPETL USING ( SELECT
'VFP' DEAL_COMP_TYPE, '1' LANG, 'Vender Funded Promotion' DEAL_COMP_TYPE_DESC, 'Y' ORIG_LANG_IND, 'N' REVIEWED_IND, GET_USER CREATE_ID, SYSDATE CREATE_DATETIME, GET_USER LAST_UPDATE_ID, SYSDATE LAST_UPDATE_DATETIME
FROM DUAL) USE_THIS
ON (DEALTYPETL.DEAL_COMP_TYPE = USE_THIS.DEAL_COMP_TYPE AND DEALTYPETL.LANG = USE_THIS.LANG)
WHEN MATCHED THEN UPDATE
SET DEALTYPETL.DEAL_COMP_TYPE_DESC = USE_THIS.DEAL_COMP_TYPE_DESC, DEALTYPETL.LAST_UPDATE_ID = GET_USER, DEALTYPETL.LAST_UPDATE_DATETIME = USE_THIS.LAST_UPDATE_DATETIME
WHEN NOT MATCHED THEN INSERT (DEAL_COMP_TYPE, LANG, DEAL_COMP_TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (USE_THIS.DEAL_COMP_TYPE, USE_THIS.LANG, USE_THIS.DEAL_COMP_TYPE_DESC, USE_THIS.ORIG_LANG_IND, USE_THIS.REVIEWED_IND, USE_THIS.CREATE_ID, USE_THIS.CREATE_DATETIME, USE_THIS.LAST_UPDATE_ID, USE_THIS.LAST_UPDATE_DATETIME);
-----------------------------------------------------------------------------------
COMMIT;