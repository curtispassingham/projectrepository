/******************************************************************************/
/* CREATE DATE - April 2018                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Grants                                                       */
/******************************************************************************/
set serveroutput on size unlimited
set escape on

declare
  grant_schema      varchar2(30);
  owning_schema       varchar2(30);
  run_schema          varchar2(30);
  missing_object      varchar2(130);
  prefix1             varchar2(128);
  prefix2             varchar2(128);
  
  cursor C_get_missing_object (ownerschema in varchar2,grantschema in varchar2) is
    (select object_name
       from dba_objects 
      where owner = ownerschema
        and object_type IN ('TABLE', 'VIEW', 'CLUSTER', 'FUNCTION', 'PACKAGE', 'PROCEDURE', 'SEQUENCE', 'TYPE')
		and (object_type not in ('SYNONYM') 
		and  object_name not like ('XXADEO_%'))
     union
     select synonym_name 
       from dba_synonyms
      where owner = ownerschema)
     minus 
     select object_name 
       from dba_objects 
      where owner = upper(grantschema)
	    and object_type IN ('TABLE', 'VIEW', 'CLUSTER', 'FUNCTION', 'PACKAGE', 'PROCEDURE', 'SEQUENCE', 'TYPE')
     order by 1 desc;
  --
begin
  --
  grant_schema   := sys.dbms_assert.schema_name(upper('&xxadeo_schema_owner'));
  owning_schema  := sys.dbms_assert.schema_name(upper('&rms_schema_owner'));
  run_schema     := sys.dbms_assert.schema_name('&_USER');
  
  IF grant_schema <> run_schema THEN
    prefix1:=sys.dbms_assert.enquote_name(grant_schema,FALSE);
  ELSE
    prefix1:='';
  END IF;
  
  IF owning_schema <> run_schema THEN
    prefix2:=sys.dbms_assert.enquote_name(owning_schema,FALSE)||'.';
  ELSE
    prefix2:='';
  END IF;

  open c_get_missing_object(owning_schema,grant_schema);
  LOOP
    fetch c_get_missing_object into missing_object;
    --When at end of objects, exit
    if c_get_missing_object%NOTFOUND then
      exit;
    end if;

    missing_object:=sys.dbms_assert.enquote_name(missing_object,FALSE);
    
    BEGIN
      execute immediate 'GRANT ALL ON '||prefix2||missing_object||' TO '||prefix1;
      dbms_output.put_line('Created grant '||prefix2|| missing_object||' pointing to '|| prefix1);
    EXCEPTION
    WHEN OTHERS THEN
      dbms_output.put_line('Create grant FAILED '||missing_object||' '||SQLCODE||' - '||SQLERRM);
    END;
  END LOOP;
  close c_get_missing_object;
EXCEPTION
  WHEN OTHERS THEN
    raise;
end;
/