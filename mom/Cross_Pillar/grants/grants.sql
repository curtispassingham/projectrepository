/******************************************************************************/
/* CREATE DATE - March 2017                                                   */
/* CREATE USER - Elsa Barros                                                  */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Grants                                                       */
/******************************************************************************/
set serveroutput on size unlimited
set escape on

declare
  rms_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&rms_schema_owner'));
  adeo_owner varchar2(30) := sys.dbms_assert.schema_name(upper('&adeo_schema_owner'));
begin
  execute immediate 'GRANT EXECUTE ON '||rms_owner||'.CUSTOM_OBJ_REC TO '||adeo_owner;
  execute immediate 'GRANT EXECUTE ON '||rms_owner||'.SQL_LIB TO  '||adeo_owner;
  execute immediate 'GRANT SELECT,INSERT,UPDATE,DELETE ON '||rms_owner||'.LOGGER_LOGS TO '||adeo_owner;
  execute immediate 'GRANT SELECT ON '||rms_owner||'.CUSTOM_PKG_CONFIG TO '||adeo_owner;
end;
/
