LOAD DATA
APPEND
INTO TABLE S9T_TMPL_COLS_DEF
WHEN  
    ( TEMPLATE_KEY != BLANKS )
AND ( WKSHT_KEY != BLANKS )
AND ( COLUMN_KEY != BLANKS )
FIELDS TERMINATED BY "|" TRAILING NULLCOLS
( TEMPLATE_KEY,
  WKSHT_KEY,
  COLUMN_KEY,
  COLUMN_NAME,
  MANDATORY,
  DEFAULT_VALUE,
  REQUIRED_VISUAL_IND "NVL(:REQUIRED_VISUAL_IND, 'N')")