-- SQL types assisting the procedures that invoke webserviecs
-- Create SQL type for com.oracle.retail.integration.base.bo.referenceid.v1.Pair
CREATE OR REPLACE TYPE OBJ_PAIR AS OBJECT (
name_ VARCHAR2(4000), 
value_ VARCHAR2(4000)); 
/
show errors
-- Create collection type for com.oracle.retail.integration.base.bo.referenceid.v1.Pair[]
CREATE OR REPLACE TYPE TBL_OBJ_PAIR AS TABLE OF OBJ_PAIR;
/
show errors
-- Create SQL type for com.oracle.retail.integration.base.bo.referenceid.v1.ReferenceId
CREATE OR REPLACE TYPE OBJ_REFERENCEID AS OBJECT (
pair_ TBL_OBJ_PAIR, 
id_ VARCHAR2(4000)); 
/
show errors
-- Create SQL type for com.oracle.retail.integration.base.bo.invocationsuccess.v1.InvocationSuccess
CREATE OR REPLACE TYPE OBJ_INVOCATIONSUCCESS AS OBJECT (
success_message_ VARCHAR2(4000)); 
/
show errors

-- PL/SQL procedures that invoke webserviecs
CREATE OR REPLACE PACKAGE DrillBackForwardUrlServiceCons AS
   PROCEDURE setHttpProxy(host VARCHAR2, port NUMBER);
   PROCEDURE setSystemProperty(key VARCHAR2, value VARCHAR2);
   PROCEDURE setProperty (key VARCHAR2, value VARCHAR2);
   FUNCTION getPassword RETURN VARCHAR2;
   FUNCTION getEndpoint RETURN VARCHAR2;
   PROCEDURE setPassword(password_  VARCHAR2);
   FUNCTION ping(arg0_  VARCHAR2) RETURN VARCHAR2;
   FUNCTION getDrillBackForwardUrl(referenceId_  OBJ_REFERENCEID) RETURN OBJ_INVOCATIONSUCCESS;
   PROCEDURE setEndpoint(endpoint_  VARCHAR2);
   FUNCTION getMaintainSession RETURN NUMBER;
   PROCEDURE setMaintainSession(maintainSession_  NUMBER);
   FUNCTION getUsername RETURN VARCHAR2;
   PROCEDURE setUsername(username_  VARCHAR2);

END DrillBackForwardUrlServiceCons;
/
SHOW ERRORS

CREATE OR REPLACE PACKAGE BODY DrillBackForwardUrlServiceCons IS
   PROCEDURE setHttpProxy(host VARCHAR2, port NUMBER)
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.setHttpProxy(java.lang.String, java.lang.Integer)';
   PROCEDURE setSystemProperty(key VARCHAR2, value VARCHAR2)
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.setSystemProperty(java.lang.String, java.lang.String)';
   PROCEDURE setProperty (key VARCHAR2, value VARCHAR2)
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.setProperty(java.lang.String, java.lang.String)';
   FUNCTION getPassword RETURN VARCHAR2
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.getPassword() return java.lang.String';
   FUNCTION getEndpoint RETURN VARCHAR2
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.getEndpoint() return java.lang.String';
   PROCEDURE setPassword(password_  VARCHAR2)
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.setPassword(java.lang.String)';
   FUNCTION ping(arg0_  VARCHAR2) RETURN VARCHAR2
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.ping(java.lang.String) return java.lang.String';
   FUNCTION getDrillBackForwardUrl(referenceId_  OBJ_REFERENCEID) RETURN OBJ_INVOCATIONSUCCESS
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.getDrillBackForwardUrl(oracle.sql.STRUCT) return oracle.sql.STRUCT';
   PROCEDURE setEndpoint(endpoint_  VARCHAR2)
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.setEndpoint(java.lang.String)';
   FUNCTION getMaintainSession RETURN NUMBER
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.getMaintainSession() return boolean';
   PROCEDURE setMaintainSession(maintainSession_  NUMBER)
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.setMaintainSession(boolean)';
   FUNCTION getUsername RETURN VARCHAR2
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.getUsername() return java.lang.String';
   PROCEDURE setUsername(username_  VARCHAR2)
   as language java
   name 'oracle.retail.integration.services.consumer.DrillBackForwardUrlService.DrillBackForwardUrlPortClientJPub.setUsername(java.lang.String)';

END DrillBackForwardUrlServiceCons;
/
SHOW ERRORS

EXIT;

