-- SQL types assisting the procedures that invoke webserviecs
-- Create SQL type for com.oracle.retail.integration.base.bo.glacctref.v1.GLAcctRef
CREATE OR REPLACE TYPE OBJ_GLACCTREF AS OBJECT (
requesting_system_ VARCHAR2(4000), 
set_of_books_id_ NUMBER, 
ccid_ NUMBER, 
segment1_ VARCHAR2(4000), 
segment2_ VARCHAR2(4000), 
segment3_ VARCHAR2(4000), 
segment4_ VARCHAR2(4000), 
segment5_ VARCHAR2(4000), 
segment6_ VARCHAR2(4000), 
segment7_ VARCHAR2(4000), 
segment8_ VARCHAR2(4000), 
segment9_ VARCHAR2(4000), 
segment10_ VARCHAR2(4000), 
segment11_ VARCHAR2(4000), 
segment12_ VARCHAR2(4000), 
segment13_ VARCHAR2(4000), 
segment14_ VARCHAR2(4000), 
segment15_ VARCHAR2(4000), 
segment16_ VARCHAR2(4000), 
segment17_ VARCHAR2(4000), 
segment18_ VARCHAR2(4000), 
segment19_ VARCHAR2(4000), 
segment20_ VARCHAR2(4000), 
account_status_ VARCHAR2(4000)); 
/
show errors
-- Create SQL type for com.oracle.retail.integration.base.bo.glacctdesc.v1.GLAcctDesc
CREATE OR REPLACE TYPE OBJ_GLACCTDESC AS OBJECT (
requesting_system_ VARCHAR2(4000), 
set_of_books_id_ NUMBER, 
ccid_ NUMBER, 
segment1_ VARCHAR2(4000), 
segment2_ VARCHAR2(4000), 
segment3_ VARCHAR2(4000), 
segment4_ VARCHAR2(4000), 
segment5_ VARCHAR2(4000), 
segment6_ VARCHAR2(4000), 
segment7_ VARCHAR2(4000), 
segment8_ VARCHAR2(4000), 
segment9_ VARCHAR2(4000), 
segment10_ VARCHAR2(4000), 
segment11_ VARCHAR2(4000), 
segment12_ VARCHAR2(4000), 
segment13_ VARCHAR2(4000), 
segment14_ VARCHAR2(4000), 
segment15_ VARCHAR2(4000), 
segment16_ VARCHAR2(4000), 
segment17_ VARCHAR2(4000), 
segment18_ VARCHAR2(4000), 
segment19_ VARCHAR2(4000), 
segment20_ VARCHAR2(4000)); 
/
show errors
-- Create collection type for com.oracle.retail.integration.base.bo.glacctref.v1.GLAcctRef[]
CREATE OR REPLACE TYPE TBL_OBJ_GLACCTREF AS TABLE OF OBJ_GLACCTREF;
/
show errors
-- Create SQL type for com.oracle.retail.integration.base.bo.glacctcolref.v1.GLAcctColRef
CREATE OR REPLACE TYPE OBJ_GLACCTCOLREF AS OBJECT (
collection_size_ NUMBER, 
gLAcctRef_ TBL_OBJ_GLACCTREF); 
/
show errors
-- Create collection type for com.oracle.retail.integration.base.bo.glacctdesc.v1.GLAcctDesc[]
CREATE OR REPLACE TYPE TBL_OBJ_GLACCTDESC AS TABLE OF OBJ_GLACCTDESC;
/
show errors
-- Create SQL type for com.oracle.retail.integration.base.bo.glacctcoldesc.v1.GLAcctColDesc
CREATE OR REPLACE TYPE OBJ_GLACCTCOLDESC AS OBJECT (
collection_size_ NUMBER, 
gLAcctDesc_ TBL_OBJ_GLACCTDESC); 
/
show errors

-- PL/SQL procedures that invoke webserviecs
CREATE OR REPLACE PACKAGE GlAccountValidationServiceCons AS
   PROCEDURE setHttpProxy(host VARCHAR2, port NUMBER);
   PROCEDURE setSystemProperty(key VARCHAR2, value VARCHAR2);
   PROCEDURE setProperty (key VARCHAR2, value VARCHAR2);
   FUNCTION getPassword RETURN VARCHAR2;
   PROCEDURE setPassword(password_  VARCHAR2);
   FUNCTION getEndpoint RETURN VARCHAR2;
   FUNCTION validateGlAccount(GLAcctColDesc_  OBJ_GLACCTCOLDESC) RETURN OBJ_GLACCTCOLREF;
   PROCEDURE setEndpoint(endpoint_  VARCHAR2);
   FUNCTION getMaintainSession RETURN NUMBER;
   PROCEDURE setMaintainSession(maintainSession_  NUMBER);
   FUNCTION getUsername RETURN VARCHAR2;
   PROCEDURE setUsername(username_  VARCHAR2);
   FUNCTION ping(arg0_  VARCHAR2) RETURN VARCHAR2;

END GlAccountValidationServiceCons;
/
SHOW ERRORS

CREATE OR REPLACE PACKAGE BODY GlAccountValidationServiceCons IS
   PROCEDURE setHttpProxy(host VARCHAR2, port NUMBER)
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.setHttpProxy(java.lang.String, java.lang.Integer)';
   PROCEDURE setSystemProperty(key VARCHAR2, value VARCHAR2)
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.setSystemProperty(java.lang.String, java.lang.String)';
   PROCEDURE setProperty (key VARCHAR2, value VARCHAR2)
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.setProperty(java.lang.String, java.lang.String)';
   FUNCTION getPassword RETURN VARCHAR2
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.getPassword() return java.lang.String';
   PROCEDURE setPassword(password_  VARCHAR2)
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.setPassword(java.lang.String)';
   FUNCTION getEndpoint RETURN VARCHAR2
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.getEndpoint() return java.lang.String';
   FUNCTION validateGlAccount(GLAcctColDesc_  OBJ_GLACCTCOLDESC) RETURN OBJ_GLACCTCOLREF
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.validateGlAccount(oracle.sql.STRUCT) return oracle.sql.STRUCT';
   PROCEDURE setEndpoint(endpoint_  VARCHAR2)
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.setEndpoint(java.lang.String)';
   FUNCTION getMaintainSession RETURN NUMBER
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.getMaintainSession() return boolean';
   PROCEDURE setMaintainSession(maintainSession_  NUMBER)
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.setMaintainSession(boolean)';
   FUNCTION getUsername RETURN VARCHAR2
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.getUsername() return java.lang.String';
   PROCEDURE setUsername(username_  VARCHAR2)
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.setUsername(java.lang.String)';
   FUNCTION ping(arg0_  VARCHAR2) RETURN VARCHAR2
   as language java
   name 'oracle.retail.integration.services.consumer.GlAccountValidationService.GlAccountValidationPortClientJPub.ping(java.lang.String) return java.lang.String';

END GlAccountValidationServiceCons;
/
SHOW ERRORS

EXIT;

