-- Drop the PL/SQL procedures that invoke webserviecs
DROP TYPE OBJ_GLACCTREF FORCE;
DROP TYPE OBJ_GLACCTDESC FORCE;
DROP TYPE TBL_OBJ_GLACCTREF FORCE;
DROP TYPE OBJ_GLACCTCOLREF FORCE;
DROP TYPE TBL_OBJ_GLACCTDESC FORCE;
DROP TYPE OBJ_GLACCTCOLDESC FORCE;

DROP PACKAGE GlAccountValidationServiceCons;
EXIT;

