-- Run this script as SYSDBA to grant permissions for <USER> to run the PL/SQL wrapper procedures
CONN / AS SYSDBA

BEGIN
dbms_java.grant_permission('<USER>', 'SYS:java.lang.RuntimePermission', 'accessClassInPackage.sun.util.calendar', '' );
dbms_java.grant_permission('<USER>', 'SYS:java.lang.RuntimePermission', 'getClassLoader', '' );
dbms_java.grant_permission('<USER>', 'SYS:java.lang.RuntimePermission', 'shutdownHooks', '' );
dbms_java.grant_permission('<USER>', 'SYS:java.lang.RuntimePermission', 'setFactory', '' );
dbms_java.grant_permission('<USER>', 'SYS:java.lang.RuntimePermission', 'createClassLoader', '' );
dbms_java.grant_permission('<USER>', 'SYS:java.net.SocketPermission', '*', 'connect,resolve' );
dbms_java.grant_permission('<USER>', 'SYS:java.util.PropertyPermission', '*', 'read,write' );
dbms_java.grant_permission('<USER>', 'SYS:java.util.logging.LoggingPermission', 'control', '' );
END;
/
EXIT;

