-- Run this script as SYSDBA to revoke the permissions granted to <USER>
BEGIN
dbms_java.revoke_permission('<USER>', 'SYS:java.lang.RuntimePermission', 'accessClassInPackage.sun.util.calendar', '' );
dbms_java.revoke_permission('<USER>', 'SYS:java.lang.RuntimePermission', 'getClassLoader', '' );
dbms_java.revoke_permission('<USER>', 'SYS:java.lang.RuntimePermission', 'shutdownHooks', '' );
dbms_java.revoke_permission('<USER>', 'SYS:java.lang.RuntimePermission', 'setFactory', '' );
dbms_java.revoke_permission('<USER>', 'SYS:java.net.SocketPermission', '*', 'connect,resolve' );
dbms_java.revoke_permission('<USER>', 'SYS:java.util.logging.LoggingPermission', 'control', '' );
END;
/
EXIT;

