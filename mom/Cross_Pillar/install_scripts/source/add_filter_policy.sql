
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
/* Organizational Hierarchy Filtering Policies */
execute dbms_rls.add_policy(user, 'v_chain',    'v_chain_s',    user, 'filter_policy_sql.v_chain_s',    'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_area',     'v_area_s',     user, 'filter_policy_sql.v_area_s',     'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_region',   'v_region_s',   user, 'filter_policy_sql.v_region_s',   'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_district', 'v_district_s', user, 'filter_policy_sql.v_district_s', 'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_store',    'v_store_s',    user, 'filter_policy_sql.v_store_s',    'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_wh',       'v_wh_s',       user, 'filter_policy_sql.v_wh_s',       'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_external_finisher', 'v_external_finisher_s', user, 'filter_policy_sql.v_external_finisher_s', 'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_internal_finisher', 'v_internal_finisher_s', user, 'filter_policy_sql.v_internal_finisher_s', 'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_tsf_entity',        'v_tsf_entity_s',        user, 'filter_policy_sql.v_tsf_entity_s',        'select', FALSE, TRUE);

/* Merchandise Hierarchy Filtering Policies */
execute dbms_rls.add_policy(user, 'v_division',    'v_division_s',    user, 'filter_policy_sql.v_division_s',    'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_groups',      'v_groups_s',      user, 'filter_policy_sql.v_groups_s',      'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_deps',        'v_deps_s',        user, 'filter_policy_sql.v_deps_s',        'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_class',       'v_class_s',       user, 'filter_policy_sql.v_class_s',       'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_subclass',    'v_subclass_s',    user, 'filter_policy_sql.v_subclass_s',    'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_item_master', 'v_item_master_s', user, 'filter_policy_sql.v_item_master_s', 'select', FALSE, TRUE);

/* Data Element Filtering Policies */
execute dbms_rls.add_policy(user, 'v_diff_group_head',  'v_diff_group_head_s',  user, 'filter_policy_sql.v_diff_group_head_s',  'select', FALSE, TRUE, FALSE, 5, TRUE,NULL,NULL);
execute dbms_rls.add_policy(user, 'v_loc_list_head',    'v_loc_list_head_s',    user, 'filter_policy_sql.v_loc_list_head_s',    'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_loc_traits',       'v_loc_traits_s',       user, 'filter_policy_sql.v_loc_traits_s',       'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_seasons',          'v_seasons_s',          user, 'filter_policy_sql.v_seasons_s',          'select', FALSE, TRUE, FALSE, 5, TRUE,NULL,NULL);
execute dbms_rls.add_policy(user, 'v_skulist_head',     'v_skulist_head_s',     user, 'filter_policy_sql.v_skulist_head_s',     'select', FALSE, TRUE,FALSE,5,TRUE,NULL,NULL);
execute dbms_rls.add_policy(user, 'v_ticket_type_head', 'v_ticket_type_head_s', user, 'filter_policy_sql.v_ticket_type_head_s', 'select', FALSE, TRUE,FALSE,5,TRUE,NULL,NULL);
execute dbms_rls.add_policy(user, 'v_uda',              'v_uda_s',              user, 'filter_policy_sql.v_uda_s',              'select', FALSE, TRUE, FALSE, 5, TRUE,NULL,NULL);
execute dbms_rls.add_policy(user, 'v_sups',             'v_sups_s',             user, 'filter_policy_sql.v_sups_s',             'select', FALSE, TRUE, FALSE, 5, TRUE,NULL,NULL);

/* Product Location Security Policies */
execute dbms_rls.add_policy(user, 'v_transfer_from_store', 'v_transfer_from_store_s', user, 'filter_policy_sql.v_transfer_from_store_s', 'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_transfer_from_wh',    'v_transfer_from_wh_s',    user, 'filter_policy_sql.v_transfer_from_wh_s',    'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_transfer_to_store',   'v_transfer_to_store_s',   user, 'filter_policy_sql.v_transfer_to_store_s',   'select', FALSE, TRUE);
execute dbms_rls.add_policy(user, 'v_transfer_to_wh',      'v_transfer_to_wh_s',      user, 'filter_policy_sql.v_transfer_to_wh_s',      'select', FALSE, TRUE);


