
--------------------------------------------------------
-- Copyright (c) 2012, Oracle Inc.  All rights reserved.
-- :   allUsersSuper.sql  $
-- :   1.0  $
-- :   Wed Apr 11 18:46:21 CDT 2012  $
--------------------------------------------------------
REM -------------------------------------------------
REM
REM  Run after superGroup.sql
REM  Associates all users on the USER_ATTRIB table
REM   with the super user group
REM
REM -------------------------------------------------
REM -------------------------------------------------
set define "^";
REM ****************************************************************
PROMPT updating/inserting into sec_user_group
REM ****************************************************************
------------------------------------------------------------------------
MERGE INTO SEC_USER_GROUP ERR USING (SELECT
SG.GROUP_ID GROUP_ID, UA.USER_SEQ USER_SEQ
FROM SEC_GROUP SG, SEC_USER UA
WHERE SG.GROUP_NAME LIKE UPPER('%super%')) USE_THIS
ON (ERR.GROUP_ID = USE_THIS.GROUP_ID AND ERR.USER_SEQ = USE_THIS.USER_SEQ)
WHEN NOT MATCHED THEN INSERT (GROUP_ID,USER_SEQ)
VALUES (USE_THIS.GROUP_ID,USE_THIS.USER_SEQ);
------------------------------------------------------------------------
set define "&";
commit;
