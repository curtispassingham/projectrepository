
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
DECLARE
   L_policy_count   NUMBER := 0;

   cursor C_DBA_POLICIES is
      select count(1)
        from dba_policies
       where object_owner = USER
         and package = 'FILTER_POLICY_SQL';

   cursor C_GET_DBA_POLICY is
      select UPPER(policy_name) policy_name
        from dba_policies
       where object_owner = USER
         and package = 'FILTER_POLICY_SQL';
BEGIN
   open C_DBA_POLICIES;
   fetch C_DBA_POLICIES into L_policy_count;
   close C_DBA_POLICIES;

   if L_policy_count > 0 then
      FOR rec in C_GET_DBA_POLICY LOOP
         /* Organizational Hierarchy Filtering Policies */
         if rec.policy_name = 'V_CHAIN_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_CHAIN', 'V_CHAIN_S');
         elsif rec.policy_name = 'V_AREA_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_AREA', 'V_AREA_S');
         elsif rec.policy_name = 'V_REGION_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_REGION', 'V_REGION_S');
         elsif rec.policy_name = 'V_DISTRICT_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_DISTRICT', 'V_DISTRICT_S');
         elsif rec.policy_name = 'V_STORE_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_STORE', 'V_STORE_S');
         elsif rec.policy_name = 'V_WH_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_WH', 'V_WH_S');
         end if;

         if rec.policy_name = 'V_EXTERNAL_FINISHER_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_EXTERNAL_FINISHER', 'V_EXTERNAL_FINISHER_S');
         elsif rec.policy_name = 'V_INTERNAL_FINISHER_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_INTERNAL_FINISHER', 'V_INTERNAL_FINISHER_S');
         elsif rec.policy_name = 'V_TSF_ENTITY_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_TSF_ENTITY', 'V_TSF_ENTITY_S');
         end if;

         /* Merchandise Hierarchy Filtering Policies */
         if rec.policy_name = 'V_DIVISION_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_DIVISION', 'V_DIVISION_S');
         elsif rec.policy_name = 'V_GROUPS_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_GROUPS', 'V_GROUPS_S');
         elsif rec.policy_name = 'V_DEPS_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_DEPS', 'V_DEPS_S');
         elsif rec.policy_name = 'V_CLASS_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_CLASS', 'V_CLASS_S');
         elsif rec.policy_name = 'V_SUBCLASS_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_SUBCLASS', 'V_SUBCLASS_S');
         elsif rec.policy_name = 'V_ITEM_MASTER_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_ITEM_MASTER', 'V_ITEM_MASTER_S');
         end if;

         /* Data Element Filtering Policies */
         if rec.policy_name = 'V_DIFF_GROUP_HEAD_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_DIFF_GROUP_HEAD', 'V_DIFF_GROUP_HEAD_S');
         elsif rec.policy_name = 'V_LOC_LIST_HEAD_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_LOC_LIST_HEAD', 'V_LOC_LIST_HEAD_S');
         elsif rec.policy_name = 'V_LOC_TRAITS_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_LOC_TRAITS', 'V_LOC_TRAITS_S');
         elsif rec.policy_name = 'V_SEASONS_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_SEASONS', 'V_SEASONS_S');
         elsif rec.policy_name = 'V_SKULIST_HEAD_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_SKULIST_HEAD', 'V_SKULIST_HEAD_S');
         elsif rec.policy_name = 'V_TICKET_TYPE_HEAD_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_TICKET_TYPE_HEAD', 'V_TICKET_TYPE_HEAD_S');
         elsif rec.policy_name = 'V_UDA_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_UDA', 'V_UDA_S');
         elsif rec.policy_name = 'V_SUPS_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_SUPS', 'V_SUPS_S');
         end if;

         /* Location Product Security Filtering Policies */
         if rec.policy_name = 'V_TRANSFER_FROM_STORE_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_TRANSFER_FROM_STORE', 'V_TRANSFER_FROM_STORE_S');
         elsif rec.policy_name = 'V_TRANSFER_FROM_WH_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_TRANSFER_FROM_WH', 'V_TRANSFER_FROM_WH_S');
         elsif rec.policy_name = 'V_TRANSFER_TO_STORE_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_TRANSFER_TO_STORE', 'V_TRANSFER_TO_STORE_S');
         elsif rec.policy_name = 'V_TRANSFER_TO_WH_S' then
            DBMS_RLS.DROP_POLICY(USER, 'V_TRANSFER_TO_WH', 'V_TRANSFER_TO_WH_S');
         end if;
      END LOOP;
   end if;
END;
/
