--------------------------------------
--       Dropping TYPE
--------------------------------------
PROMPT Dropping TYPE 'TAX_EVENT_MSG'
DECLARE
  L_type_exists number := 0;
BEGIN
  SELECT count(1) INTO L_type_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'TAX_EVENT_MSG'
     AND OBJECT_TYPE = 'TYPE';

  if (L_type_exists != 0) then
      execute immediate 'DROP TYPE TAX_EVENT_MSG FORCE';
  end if;
end;
/
--------------------------------------
--       Creating TYPE
--------------------------------------
PROMPT Creating TYPE 'TAX_EVENT_MSG'
create or replace TYPE tax_event_msg as object (tax_event_id NUMBER(25))
/



