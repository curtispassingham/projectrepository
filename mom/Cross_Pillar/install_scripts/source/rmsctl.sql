
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
spool rmsctl.log;

--- Startall ---
prompt 'Starting File: staticin.sql';
@@staticin.sql;
PROMPT 'Starting File: add_filter_policy.sql
@@add_filter_policy.sql;
PROMPT 'Starting File: navigate.sql
@@navigate.sql;
PROMPT 'Starting File: codes.sql
@@codes.sql;
PROMPT 'Starting File: restart.sql
@@restart.sql;
PROMPT 'Starting File: rtk_errors.sql
@@rtk_errors.sql;
PROMPT 'Starting File: context.sql
@@context.sql;
PROMPT 'Starting File: populate_form_links.sql
@@populate_form_links.sql;
PROMPT 'Starting File: populate_form_links_role.sql
@@populate_form_links_role.sql;
PROMPT 'Starting File: ari_interface_test_data.sql
@@ari_interface_test_data.sql;
prompt 'Starting File: var_upc_ean_load.sql';
@@var_upc_ean_load.sql;
prompt 'Starting File: multiview_data.sql';
@@multiview_data.sql;

spool off;

exit;
