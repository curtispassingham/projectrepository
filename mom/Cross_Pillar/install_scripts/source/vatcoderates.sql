SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

set define "^";

REM ****This Script inserts seed data into vat_code_rates table****
DELETE FROM vat_code_rates;

DECLARE
L_date   PERIOD.VDATE%TYPE := GET_VDATE;

BEGIN
   INSERT INTO VAT_CODE_RATES VALUES('S',
                                     to_date(L_date, 'DD_MM_YYYY'),
                                     '10.00',
                                     to_date(SYSDATE, 'DD_MM_YYYY'),
                                     USER);
                                     
END;
/
set define "&";

COMMIT;