
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------------
-- Copyright (c) 2007, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.1 $
-- $Modtime$
-------------------------------------------------------------------------------------
--	ATTENTION: This script DOES preserve data. 
--
--	The customer DBA is responsible to review this script to ensure data 
--	is preserved as desired.
--
-------------------------------------------------------------------------------------
--	ROLE CREATED: 	CC_ACCESS
-------------------------------------------------------------------------------------
PROMPT Creating role 'CC_ACCESS'

CREATE ROLE CC_ACCESS;
