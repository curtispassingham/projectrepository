
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------------
-- Copyright (c) 2007, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.1 $
-- $Modtime$
-------------------------------------------------------------------------------------
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
-------------------------------------------------------------------------------------
PROMPT Dropping CC Security FGA policy to table 'SA_TRAN_TENDER', 'SA_TRAN_TENDER_REV'
BEGIN
   dbms_fga.drop_policy(object_schema=>user,
                        object_name=>'SA_TRAN_TENDER',
                        policy_name=>'SA_TRAN_TENDER_CC_SEC_FGA');
   dbms_fga.drop_policy(object_schema=>user,
                        object_name=>'SA_TRAN_TENDER_REV',
                        policy_name=>'SA_TRAN_TENDER_REV_CC_SEC_FGA');
END;
/
