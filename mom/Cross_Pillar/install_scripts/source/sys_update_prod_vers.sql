
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
set serveroutput on;
set trimspool on

spool off
spool sys_update_prod_vers.log replace

------------------------------------------------------------------------------------
-- Copyright (c) 2017, Oracle Retail.  All rights reserved
-- $Workfile:  $
-- $Revision: 1.0 $
-- $Modtime:   $
------------------------------------------------------------------------------------

REM --------------------------------------------------------------------------------
REM
REM  This script updates PRODUCT_VERS_CONFIG_OPTIONS table.
REM  This script is used for system installation, and  for every patch update to
REM  MOM product installed in the system. It accepts 8 valid values as user input:
REM  First input as  Allocation Version, Second input as RWMS version, Third input
REM  as REIM version, Fourth input as SIM Version, Fifth input as AIP version,
REM  Sixth input as RPM version, Seventh input as RMS version, Eigth input as ReSA version.
REM
REM --------------------------------------------------------------------------------

prompt 'Starting File: sys_update_prod_vers.sql';
prompt 'Enter the verion no of the MOM product prompted, if installed. Else press Enter.'

DECLARE

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   update product_vers_config_options
      set allocation_vers = '&ALLOCATION';

   update product_vers_config_options
      set rwms_vers = '&RWMS';

   update product_vers_config_options
      set reim_vers = '&REIM';

   update product_vers_config_options
      set sim_vers = '&SIM';

   update product_vers_config_options
      set aip_vers = '&AIP';

   update product_vers_config_options
      set rpm_vers = '&RPM';

   update product_vers_config_options
      set rms_vers = '&RMS';   
      
   update product_vers_config_options
      set resa_vers = '&RESA';
      
   commit;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'sys_update_prod_vers.sql',
                                            to_char(SQLCODE));
      dbms_output.put_line(L_error_message);
      return;
END;
/

set serveroutput off

spool off