SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
DECLARE
-- Define a record type to store all of the values will be inserted

TYPE address_type_rectype IS RECORD(address_type      VARCHAR2(2),
                                    address_type_desc VARCHAR2(40),
                                    external_addr_ind VARCHAR2(1));

-- Define a table type based upon the record type defined above.

TYPE address_type_tabletype IS TABLE OF address_type_rectype
   INDEX BY BINARY_INTEGER;

address_type_list   address_type_tabletype;

BEGIN

/* Fill the table.  If you need to add a new address type, add a new record to the table below*/

   address_type_list(1).address_type := '01';
   address_type_list(1).address_type_desc := 'Business';
   address_type_list(1).external_addr_ind := 'N';

   address_type_list(2).address_type := '02';
   address_type_list(2).address_type_desc := 'Postal';
   address_type_list(2).external_addr_ind := 'N';

   address_type_list(3).address_type := '03';
   address_type_list(3).address_type_desc := 'Returns';
   address_type_list(3).external_addr_ind := 'N';

   address_type_list(4).address_type := '04';
   address_type_list(4).address_type_desc := 'Order';
   address_type_list(4).external_addr_ind := 'N';

   address_type_list(5).address_type := '05';
   address_type_list(5).address_type_desc := 'Invoice';
   address_type_list(5).external_addr_ind := 'N';

   address_type_list(6).address_type := '06';
   address_type_list(6).address_type_desc := 'Remittance';
   address_type_list(6).external_addr_ind := 'N';

   address_type_list(7).address_type := '07';
   address_type_list(7).address_type_desc := 'Franchise';
   address_type_list(7).external_addr_ind := 'N';

   FOR i in 1..address_type_list.COUNT
   LOOP
         merge into add_type addt
         using (select address_type_list(i).address_type address_type,
                       address_type_list(i).external_addr_ind external_addr_ind
                  from dual) use_this
         on (addt.address_type = use_this.address_type)
         when matched then
            update set addt.external_addr_ind = use_this.external_addr_ind
         when not matched then
            insert (address_type,
                    external_addr_ind)
            values (use_this.address_type,
                    use_this.external_addr_ind);
         
         merge into add_type_tl addttl
         using (select address_type_list(i).address_type address_type,
                       '1' lang,
                       address_type_list(i).address_type_desc type_desc,
                       'Y' orig_lang_ind,
                       'N' reviewed_ind,
                       user create_id, 
                       sysdate create_datetime,
                       user last_update_id,
                       sysdate last_update_datetime                       
                  from dual) use_this
         on (    addttl.address_type = use_this.address_type 
             and addttl.lang         = use_this.lang)
         when matched then
            update set addttl.type_desc            = use_this.type_desc,
                       addttl.last_update_id       = use_this.last_update_id,
                       addttl.last_update_datetime = use_this.last_update_datetime
         when not matched then
            insert (address_type,
                    lang,
                    type_desc,
                    orig_lang_ind,
                    reviewed_ind,
                    create_id,
                    create_datetime,
                    last_update_id,
                    last_update_datetime)
            values (use_this.address_type,
                    use_this.lang,
                    use_this.type_desc,
                    use_this.orig_lang_ind,
                    use_this.reviewed_ind,
                    use_this.create_id,
                    use_this.create_datetime,
                    use_this.last_update_id,
                    use_this.last_update_datetime);

   END LOOP;
---

END;
/
REM ****** (commiting...) ********************************************
commit;

