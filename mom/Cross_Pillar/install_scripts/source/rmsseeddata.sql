
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
set serveroutput on;
set trimspool on

REM  -----------------------------------------------------
REM  -----------------------------------------------------
REM  This script populates generic basic information.  Typically, this is used for
REM  seed data insertion at the time of new istallation. 
REM
REM  This script prompts the user whether RTM should be turned on.s
REM  This script gives users an option of changing the primary language which is 
REM  set to English by default.
REM
REM  The user also has the option to change the Primary currency and Base country.
REM  -----------------------------------------------------
REM  The language files carries the release number so need to be replaced with the
REM  language wrapper scripts for each release.

spool off
spool rmsseeddata.log replace

VARIABLE rmsrtm_cmd VARCHAR2(1000);
VARIABLE primlang_cmd VARCHAR2(1000);

define base_country = &1;
define prim_currency = &2;
define table_owner = &3;
define default_tax_type = &4;
define vat_class_ind = &5;
define supplier_sites_ind = &6;
define G_cal_type = &7;
define G_week_option = &8;
define G_vdate = &9;

define role_to_add = &10;

--installerize
--accept RTM_ind char   prompt 'Would you like to turn RTM on? Y or N - '
define RTM_ind = &11;
define hts_tracking_level = &12;

--installerize
--accept Primary_lang_ind char
--prompt 'The primary language is set to English, to change the primary language:';
--prompt 'Enter ''de'' for German';
--prompt 'Enter ''el'' for Greek';
--prompt 'Enter ''es'' for Spanish';
--prompt 'Enter ''fr'' for French';
--prompt 'Enter ''hr'' for Croatian';
--prompt 'Enter ''hu'' for Hungarian';
--prompt 'Enter ''it'' for Italian';
--prompt 'Enter ''ja'' for Japanese';
--prompt 'Enter ''ko'' for Korean';
--prompt 'Enter ''nl'' for Dutch';
--prompt 'Enter ''pl'' for Polish';
--prompt 'Enter ''ptb'' for Brazilian Portuguese';
--prompt 'Enter ''ru'' for Russian';
--prompt 'Enter ''sv'' for Swedish';
--prompt 'Enter ''tr'' for Turkish';
--prompt 'Enter ''zhs'' for Simplified Chinese';
--prompt 'Enter ''zht'' for Traditional Chinese';
define Primary_lang_ind = &13;
define Data_lvl_sec_ind = &14;
define RPM_ind = &15;

prompt 'Starting File: sys_uop_install.sql';
@@sys_uop_install.sql &table_owner &vat_class_ind &supplier_sites_ind &base_country &prim_currency &hts_tracking_level &default_tax_type &G_cal_type &Data_lvl_sec_ind &RPM_ind;
prompt 'Starting File: lang.sql';
@@lang.sql;
prompt 'Starting File: rmsuom.sql';
@@rmsuom.sql;
prompt 'Starting File: rmscountries.sql';
@@rmscountries.sql;
prompt 'Starting File: rmscurrencies.sql';
@@rmscurrencies.sql;
prompt 'Starting File:vatcodes.sql';
@@vatcodes.sql;
prompt 'Starting File: staticin.sql';
@@staticin.sql &base_country &prim_currency;
prompt 'Starting File: drop_filter_policy.sql';
@@drop_filter_policy.sql;
prompt 'Starting File: add_filter_policy.sql';
@@add_filter_policy.sql;
prompt 'Starting File: codes.sql';
@@codes.sql;
prompt 'Starting File: restart.sql';
@@restart.sql;
prompt 'Starting File: rtk_errors.sql';
@@rtk_errors.sql;
prompt 'Starting File: context.sql';
@@context.sql;
prompt 'Starting File: populate_form_links.sql';
@@populate_form_links.sql;
prompt 'Starting File: var_upc_ean_load.sql';
@@var_upc_ean_load.sql;
prompt 'Starting File: uom_x_conversion.sql';
@@uom_x_conversion.sql;
prompt 'Starting File: rmsuomconv.sql';
@@rmsuomconv.sql;
prompt 'Starting File: calendar.sql';
@@calendar.sql &G_cal_type &G_week_option &G_vdate;
prompt 'Starting File: vatcoderates.sql';
@@vatcoderates.sql;
prompt 'Starting File: sa_system_required.sql';
@@sa_system_required.sql;
prompt 'Starting File: cfapreenableentitydata.sql';
@@cfapreenableentitydata.sql;
prompt 'Starting File: obj_cost_event_msg.sql';
@@obj_cost_event_msg.sql;
prompt 'Starting File: obj_cost_event_thread_msg.sql';
@@obj_cost_event_thread_msg.sql;
prompt 'Starting File: obj_tax_event_msg.sql';
@@obj_tax_event_msg.sql;
prompt 'Starting File: create_queue_subscriber.sql';
@@create_queue_subscriber.sql;
prompt 'Starting File: create_tax_queue_subscriber.sql';
@@create_tax_queue_subscriber.sql;

prompt 'Starting File: inv_adj_reason.sql';
@@inv_adj_reason.sql;

DECLARE
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_rtm_ind         VARCHAR2(1) := '&RTM_ind';
   L_prim_lang       VARCHAR2(3) := '&Primary_lang_ind';
   PROGRAM_ERROR     EXCEPTION;
BEGIN
   if UPPER(L_rtm_ind) = 'Y' then
      :rmsrtm_cmd := '@@rmsrtm.sql';
   end if;
   ---
   if UPPER(L_prim_lang) = 'EN' then
      NULL;
   elsif UPPER(L_prim_lang) = 'DE' then
      :primlang_cmd := '@@rms_set_primary_lang_de.sql';
   elsif UPPER(L_prim_lang) = 'EL' then
      :primlang_cmd := '@@rms_set_primary_lang_el.sql';
   elsif UPPER(L_prim_lang) = 'ES' then
      :primlang_cmd := '@@rms_set_primary_lang_es.sql';
   elsif UPPER(L_prim_lang) = 'FR' then
      :primlang_cmd := '@@rms_set_primary_lang_fr.sql';
   elsif UPPER(L_prim_lang) = 'HR' then
      :primlang_cmd := '@@rms_set_primary_lang_hr.sql';
   elsif UPPER(L_prim_lang) = 'HU' then
      :primlang_cmd := '@@rms_set_primary_lang_hu.sql';
   elsif UPPER(L_prim_lang) = 'IT' then
      :primlang_cmd := '@@rms_set_primary_lang_it.sql';
   elsif UPPER(L_prim_lang) = 'JA' then
      :primlang_cmd := '@@rms_set_primary_lang_ja.sql';
   elsif UPPER(L_prim_lang) = 'KO' then
      :primlang_cmd := '@@rms_set_primary_lang_ko.sql';
   elsif UPPER(L_prim_lang) = 'NL' then
      :primlang_cmd := '@@rms_set_primary_lang_nl.sql';
   elsif UPPER(L_prim_lang) = 'PL' then
      :primlang_cmd := '@@rms_set_primary_lang_pl.sql';
   elsif UPPER(L_prim_lang) = 'PTB' then
      :primlang_cmd := '@@rms_set_primary_lang_ptb.sql';
   elsif UPPER(L_prim_lang) = 'RU' then
      :primlang_cmd := '@@rms_set_primary_lang_ru.sql';
   elsif UPPER(L_prim_lang) = 'SV' then
      :primlang_cmd := '@@rms_set_primary_lang_sv.sql';
   elsif UPPER(L_prim_lang) = 'TR' then
      :primlang_cmd := '@@rms_set_primary_lang_tr.sql';
   elsif UPPER(L_prim_lang) = 'ZHS' then
      :primlang_cmd := '@@rms_set_primary_lang_zhs.sql';
   elsif UPPER(L_prim_lang) = 'ZHT' then
      :primlang_cmd := '@@rms_set_primary_lang_zht.sql';
   end if;

EXCEPTION
   when OTHERS then
      dbms_output.put_line(L_error_message);
      return;
END;
/

spool off

set heading off
set echo off
set feedback off
spool rmsrtm.log;
PRINT :rmsrtm_cmd;
spool off;
set heading on
set feedback on
spool rmsseeddata.log append
@rmsrtm.log;
spool off

prompt 'Starting File: rms_install_language_pack.sql to load language pack for all languages.';
--Commenting the installation of language pack in the seedscript as this will be done after the seeddata load to allow
--versioning of the language scripts to allow reruns of latest version of scripts only. 
--@@rms_install_language_pack.sql;

set heading off
set feedback off
spool prim_lang.log;
PRINT :primlang_cmd;
spool off;
set heading on
set feedback on
spool RmsSeedData_lang.log append
@prim_lang.log;
spool off;

spool rmsseeddata.log append

undefine base_country;
undefine prim_currency;
undefine G_cal_type;
undefine G_week_option;
undefine G_vdate;
undefine role_to_add;
undefine table_owner;
undefine vat_class_ind;
undefine supplier_sites_ind;
undefine hts_tracking_level;
undefine default_tax_type;
undefine RTM_ind;
undefine Primary_lang_ind;
undefine Data_lvl_sec_ind;

commit;

prompt 'Execution of Seed Data script has completed.';
prompt 'Please check RmsSeedData_lang.log for any errors.';

spool off
