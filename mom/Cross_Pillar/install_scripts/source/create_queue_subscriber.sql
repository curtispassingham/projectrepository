DECLARE
   
   L_ce_queue_table          VARCHAR2(100) := USER||'.cost_event_qtab';
   L_ce_queue_payload_type   VARCHAR2(100) := USER||'.cost_event_msg';
   L_cet_queue_table         VARCHAR2(100) := USER||'.cost_event_thread_qtab';
   L_cet_queue_payload_type  VARCHAR2(100) := USER||'.cost_event_thread_msg';
   L_ce_queue_count          NUMBER        := 0;
   L_ce_queue_table_count    NUMBER        := 0;
   L_cet_queue_count         NUMBER        := 0;
   L_cet_queue_table_count   NUMBER        := 0;
   L_ce_subscriber_count     NUMBER        := 0;
   L_cet_subscriber_count    NUMBER        := 0;
   L_ce_queue_name           VARCHAR2(50)  := USER||'.cost_event_queue';
   L_ce_recepient            VARCHAR2(50)  := USER||'.cost_event_queue:ce_recipient';
   L_ce_event                VARCHAR2(100) := 'plsql://'||USER||'.notify_cost_event';
   L_cet_queue_name          VARCHAR2(50)  := USER||'.cost_event_thread_queue';
   L_cet_recepient           VARCHAR2(100) := USER||'.cost_event_thread_queue:cet_recipient';
   L_cet_event               VARCHAR2(100) := 'plsql://'||USER||'.notify_cost_event_thread';
   
   cursor C_CE_QUEUE_EXISTS is
      select count(1) 
        from dba_queues
       where owner = user
         and name = 'COST_EVENT_QUEUE';
         
   cursor C_CE_QUEUE_TABLE_EXISTS is
      select count(1)
        from dba_queue_tables
       where owner = user
         and queue_table = 'COST_EVENT_QTAB';

   cursor C_CET_QUEUE_EXISTS is
         select count(1) 
           from dba_queues
          where owner = user
            and name = 'COST_EVENT_THREAD_QUEUE';
            
   cursor C_CET_QUEUE_TABLE_EXISTS is
      select count(1)
        from dba_queue_tables
       where owner = user
         and queue_table = 'COST_EVENT_THREAD_QTAB';

   cursor C_CE_SUBSCRIBER_EXISTS is
      select count(1)
        from dba_queue_subscribers  
       where owner = user
         and queue_name = 'COST_EVENT_QUEUE';

   cursor C_CET_SUBSCRIBER_EXISTS is
      select count(1)
        from dba_queue_subscribers  
       where owner = user
         and queue_name = 'COST_EVENT_THREAD_QUEUE';

BEGIN

   open C_CE_SUBSCRIBER_EXISTS;
   fetch C_CE_SUBSCRIBER_EXISTS into L_ce_subscriber_count;
   close C_CE_SUBSCRIBER_EXISTS;
      
   if L_ce_subscriber_count > 0 then
      DBMS_AQADM.REMOVE_SUBSCRIBER
         (QUEUE_NAME => L_ce_queue_name,
          SUBSCRIBER => sys.aq$_agent('ce_recipient',null,null));
   end if;

   open C_CE_QUEUE_EXISTS;
   fetch C_CE_QUEUE_EXISTS into L_ce_queue_count;
   close C_CE_QUEUE_EXISTS;
         
   if L_ce_queue_count > 0 then
      DBMS_AQADM.STOP_QUEUE
         (QUEUE_NAME => 'COST_EVENT_QUEUE');
         
      DBMS_AQADM.DROP_QUEUE
         (QUEUE_NAME => 'COST_EVENT_QUEUE');
   end if;
   
   open C_CE_QUEUE_TABLE_EXISTS;
   fetch C_CE_QUEUE_TABLE_EXISTS into L_ce_queue_table_count;
   close C_CE_QUEUE_TABLE_EXISTS;
      
   if L_ce_queue_table_count > 0 then
      DBMS_AQADM.DROP_QUEUE_TABLE
         (QUEUE_TABLE => 'COST_EVENT_QTAB');
   end if;
   
   DBMS_AQADM.CREATE_QUEUE_TABLE
      (QUEUE_TABLE => L_ce_queue_table,
       QUEUE_PAYLOAD_TYPE =>  L_ce_queue_payload_type,
       MULTIPLE_CONSUMERS => TRUE);
      
   DBMS_AQADM.CREATE_QUEUE
      (QUEUE_NAME => 'cost_event_queue',
       QUEUE_TABLE => L_ce_queue_table);
     
   DBMS_AQADM.START_QUEUE
      (QUEUE_NAME => 'cost_event_queue');
      
   open C_CET_SUBSCRIBER_EXISTS;
   fetch C_CET_SUBSCRIBER_EXISTS into L_cet_subscriber_count;
   close C_CET_SUBSCRIBER_EXISTS;
         
   if L_cet_subscriber_count > 0 then
      DBMS_AQADM.REMOVE_SUBSCRIBER
         (QUEUE_NAME => L_cet_queue_name,
          SUBSCRIBER => sys.aq$_agent('cet_recipient',null,null));
   end if;
      
   open C_CET_QUEUE_EXISTS;
   fetch C_CET_QUEUE_EXISTS into L_cet_queue_count;
   close C_CET_QUEUE_EXISTS;
            
   if L_cet_queue_count > 0 then
      DBMS_AQADM.STOP_QUEUE
         (QUEUE_NAME => 'COST_EVENT_THREAD_QUEUE');
            
      DBMS_AQADM.DROP_QUEUE
         (QUEUE_NAME => 'COST_EVENT_THREAD_QUEUE');
   end if;
      
   open C_CET_QUEUE_TABLE_EXISTS;
   fetch C_CET_QUEUE_TABLE_EXISTS into L_cet_queue_table_count;
   close C_CET_QUEUE_TABLE_EXISTS;
         
   if L_cet_queue_table_count > 0 then
      DBMS_AQADM.DROP_QUEUE_TABLE
         (QUEUE_TABLE => 'COST_EVENT_THREAD_QTAB');
   end if;
   
   DBMS_AQADM.CREATE_QUEUE_TABLE
      (QUEUE_TABLE => L_cet_queue_table,
       QUEUE_PAYLOAD_TYPE => L_cet_queue_payload_type,
       MULTIPLE_CONSUMERS => TRUE );
   
   DBMS_AQADM.CREATE_QUEUE
      (QUEUE_NAME => 'cost_event_thread_queue',
       QUEUE_TABLE => L_cet_queue_table);
   
   DBMS_AQADM.START_QUEUE
      (queue_name => 'cost_event_thread_queue');

   DBMS_AQADM.ADD_SUBSCRIBER
      (QUEUE_NAME => L_ce_queue_name,
       SUBSCRIBER => sys.aq$_agent('ce_recipient',null,null));
        
   DBMS_AQ.REGISTER
      (sys.aq$_reg_info_list(
         sys.aq$_reg_info(L_ce_recepient,
                          DBMS_AQ.NAMESPACE_AQ,
                          L_ce_event,
                          HEXTORAW('FF'))),
                          --null)),
       1);
       
  DBMS_AQADM.ADD_SUBSCRIBER
     (QUEUE_NAME => L_cet_queue_name,
      SUBSCRIBER => sys.aq$_agent( 'cet_recipient',null,null ) );
        
   DBMS_AQ.REGISTER
         (sys.aq$_reg_info_list(
             sys.aq$_reg_info(L_cet_recepient,
                              DBMS_AQ.NAMESPACE_AQ,
                              L_cet_event,
                              HEXTORAW('FF')) ) ,
                              --null) ) ,
          1);
        
end;
/

