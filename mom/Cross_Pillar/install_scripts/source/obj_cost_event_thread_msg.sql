--------------------------------------
--       Dropping TYPE
--------------------------------------
PROMPT Dropping TYPE 'COST_EVENT_THREAD_MSG'
DECLARE
  L_type_exists number := 0;
BEGIN
  SELECT count(1) INTO L_type_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'COST_EVENT_THREAD_MSG'
     AND OBJECT_TYPE = 'TYPE';

  if (L_type_exists != 0) then
      execute immediate 'DROP TYPE COST_EVENT_THREAD_MSG FORCE';
  end if;
end;
/

--------------------------------------
--       Creating TYPE
--------------------------------------
PROMPT Creating TYPE 'COST_EVENT_THREAD_MSG'
create or replace TYPE COST_EVENT_THREAD_MSG as object (cost_event_process_id NUMBER(15),
                                                        thread_id NUMBER(10))
/
