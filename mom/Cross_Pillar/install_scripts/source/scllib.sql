
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
HOST echo DEFINE LIBSUPCSTRR=$MMHOME/oracle/lib/bin/libsupcstrr.`sed -n 's/^SHARED_SUFFIX *= *//p' $MMHOME/oracle/lib/src/platform.mk` >libsupcstrr.sql
START libsupcstrr
CREATE OR REPLACE LIBRARY scale_library AS '&libsupcstrr'
/
