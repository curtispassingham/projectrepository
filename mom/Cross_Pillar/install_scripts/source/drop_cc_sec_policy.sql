
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------------
-- Copyright (c) 2007, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.1 $
-- $Modtime$
-------------------------------------------------------------------------------------
--  ATTENTION: This script DOES preserve data.
--
--  The customer DBA is responsible to review this script to ensure data
--  is preserved as desired.
--
-------------------------------------------------------------------------------------
--  POLICIES DROPPED:   SA_TRAN_TENDER_CC_SEC
--                      SA_TRAN_TENDER_REV_CC_SEC
-------------------------------------------------------------------------------------
-- This script will drop the VPD credit card security policies from the listed tables.
-------------------------------------------------------------------------------------
PROMPT Dropping CC Security policy to table 'SA_TRAN_TENDER', 'SA_TRAN_TENDER_REV'
BEGIN
   dbms_rls.drop_policy(object_schema=>user,
                        object_name=>'SA_TRAN_TENDER',
                        policy_name=>'SA_TRAN_TENDER_CC_SEC');

   dbms_rls.drop_policy(object_schema=>user,
                        object_name=>'SA_TRAN_TENDER_REV',
                        policy_name=>'SA_TRAN_TENDER_REV_CC_SEC');

END;
/
