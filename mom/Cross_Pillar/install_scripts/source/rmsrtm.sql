
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--- Startall ---
prompt 'Starting File: entry_type.sql';
@@entry_type.sql;
prompt 'Starting File: entry_status.sql';
@@entry_status.sql;
prompt 'Starting File: oga.sql';
@@oga.sql;
prompt 'Starting File: tariff_treatment.sql';
@@tariff_treatment.sql;
prompt 'Starting File: quota_category.sql';
@@quota_category.sql;
prompt 'Starting File: country_tariff_treatment.sql';
@@country_tariff_treatment.sql;
prompt 'Starting File: hts_headings.sql';
@@hts_headings.sql;
