
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
spool rmsctlb.log

prompt 'Starting File: elc_comp_pre_htsupld.sql;'
@@elc_comp_pre_htsupld.sql;
prompt 'Starting File: sa_system_required.sql;'
@@sa_system_required.sql;

spool off;

exit;
