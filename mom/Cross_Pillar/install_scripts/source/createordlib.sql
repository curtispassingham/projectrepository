
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
HOST echo DEFINE CREATEORD=$MMHOME/oracle/lib/bin/libcreateord.`sed -n 's/^SHARED_SUFFIX *= *//p' $MMHOME/oracle/lib/src/platform.mk` >createord.sql
START createord
CREATE OR REPLACE LIBRARY order_build_split_lib AS '&createord'
/
