
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-- Run after superGroup.sql

-- Inserts record to the SEC_USER table
insert into sec_user (USER_SEQ,
                      DATABASE_USER_ID,
                      CREATE_ID,
                      CREATE_DATETIME,
					  RMS_USER_IND,
					  RESA_USER_IND,
					  REIM_USER_IND,
					  ALLOCATION_USER_IND)
                     (select sec_user_sequence.NEXTVAL,
                             user_id,
                             USER,
                             SYSDATE,
							 'Y',
							 'N',
							 'N',
							 'N'
                        from user_attrib
                       where user_id = USER)
/

-- Associates the user running this script with the RMS security super group
insert into sec_user_group
(GROUP_ID,
 USER_SEQ,
 CREATE_ID,
 CREATE_DATETIME)
values
((select group_id from sec_group where group_name like UPPER('%super%')),
 (select user_seq from sec_user where database_user_id = USER),
 USER,
 SYSDATE)
/

commit;
