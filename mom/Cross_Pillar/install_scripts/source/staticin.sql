
REM ****************************************************************************************************
REM This script contains ONLY DATA REQUIRED by RMS for the system to run.  This is data that all
REM development, test, demo and client production environments will need.
REM
REM Anything needed only for specific testing or environments should not be added to this script.
REM
REM This script does not include any data specific to certain environments.
REM
REM All optional data that depends on the configuration of the environment
REM has been moved to mmdemo or a script specific to the data.
REM
REM Tables loaded in this script include:
REM
REM cost_chg_reason        cost_chg_reason_tl           dummy                        inv_status_codes             inv_status_codes_tl
REM doc_link               inv_status_types             inv_status_types_tl
REM deal_comp_type         deal_comp_type_tl            order_types                  order_types_tl               vehicle_round
REM lang                   mc_rejection_reason          
REM add_type_module        add_type
REM safety_stock_lookup    tsf_type
REM tran_data_codes        tran_data_codes_ref
REM cvb_head               cvb_detail                   elc_comp
REM cost_zone_group        outloc                       non_merch_code_head          non_merch_code_head_tl
REM rms_batch_status
REM sa_system_options
REM
REM The last line of this script issues a commit.
REM ****************************************************************************
REM Clearing Data from tables before inserting fresh data.
REM ****************************************************************************
delete from sa_system_options;
delete from cost_chg_reason_tl;
delete from cost_chg_reason;
delete from dummy;
delete from deal_comp_type_tl;
delete from deal_comp_type;
delete from doc_link;
delete from inv_status_codes_tl;
delete from inv_status_codes;
delete from inv_status_types_tl;
delete from inv_status_types;
delete from mc_rejection_reasons_tl;
delete from mc_rejection_reasons;
delete from cfa_ext_entity_key_labels;
delete from rtk_errors_tl;
delete from rtk_errors;
delete from sec_user_group;
delete from code_detail_tl;
delete from code_detail;
delete from code_head;
delete from fif_line_type_xref;
delete from order_types_tl;
delete from order_types;
delete from safety_stock_lookup;
delete from tran_data_codes_tl;
delete from tran_data_codes;
delete from tran_data_codes_ref_tl;
delete from tran_data_codes_ref;
delete from tsf_type;
delete from vehicle_round;
delete from add_type_module;
delete from add_type_tl;
delete from add_type;
delete from cost_zone_group_tl;
delete from cost_zone_group;
delete from cvb_detail;
delete from elc_comp_tl;
delete from elc_comp;
delete from cvb_head_tl;
delete from cvb_head;
delete from outloc_tl;
delete from outloc;
delete from non_merch_code_head_tl;
delete from non_merch_code_head;
delete from rms_batch_status;
delete from tax_event_run_type;
delete from inv_adj_reason_tl;
delete from uom_class_tl;
delete from lang_tl;
---
REM *********** Populates MC_REJECTION_REASONS **********
prompt 'Starting File: mc_rejection_reasons.sql';
@@mc_rejection_reasons.sql;
REM ****** (order types Seed Data) ********************************************
Insert into order_types(ORDER_TYPE, PO_IND) values ('PO', 'Y');
Insert into order_types(ORDER_TYPE, PO_IND) values ('PREDIST', 'Y');
Insert into order_types(ORDER_TYPE, PO_IND) values ('MANUAL', 'N');
Insert into order_types(ORDER_TYPE, PO_IND) values ('AUTOMATIC', 'N');
Insert into order_types(ORDER_TYPE, PO_IND) values ('WAVE', 'N');
REM ****** (order_types_tl Seed Data Translation) ************************************
INSERT INTO order_types_tl (ORDER_TYPE, LANG, ORDER_TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ('PO', 1, 'After receipt of PO', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO order_types_tl (ORDER_TYPE, LANG, ORDER_TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ('PREDIST', 1, 'Before receipt of PO', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO order_types_tl (ORDER_TYPE, LANG, ORDER_TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ('MANUAL', 1, 'Manual', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO order_types_tl (ORDER_TYPE, LANG, ORDER_TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ('AUTOMATIC', 1, 'Automatic', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO order_types_tl (ORDER_TYPE, LANG, ORDER_TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ('WAVE', 1, 'Wave', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
---
REM ****** (sa_system_options seed data) ********************************************
   INSERT INTO SA_SYSTEM_OPTIONS
   (DAYS_BEFORE_PURGE,
    DAY_POST_SALE,
    BALANCE_LEVEL_IND,
    MAX_DAYS_COMPARE_DUPS,
    COMP_BASE_DATE,
    COMP_NO_DAYS,
    CHECK_DUP_MISS_TRAN,
    UNIT_OF_WORK,
    AUDIT_AFTER_IMP_IND,
    FUEL_DEPT,
    DEFAULT_CHAIN,
    CLOSE_IN_ORDER,
    ESCHEAT_IND,
    PARTNER_TYPE,
    PARTNER_ID,
    AUTO_VALIDATE_TRAN_EMPLOYEE_ID,
    VIEW_SYS_CALC_TOTAL,
    WKSTATION_TRAN_APPEND_IND,
    INV_RESV_FROM_STORE_IND,
    INV_RESV_LAYAWAY_IND,
    CC_NO_MASK_CHAR,
    TRAN_NAVIGATION_IND
    )
   VALUES (1,                    --DAYS_BEFORE_PURGE
           999,                  --DAY_POST_SALE
             'R',                    --BALANCE_LEVEL_IND
             30,                   --MAX_DAYS_COMPARE_DUPS
             'R',                    --COMP_BASE_DATE
             999,                  --COMP_NO_DAYS
             'Y',                    --CHECK_DUP_MISS_TRAN
             'T',                    --UNIT_OF_WORK
             'Y',                    --AUDIT_AFTER_IMP_IND
             null,                 --FUEL_DEPT
             null,                 --DEFAULT_CHAIN
             'N',                    --CLOSE_IN_ORDER
             null,                 --ESCHEAT_IND
             null,                 --PARTNER_TYPE
             null,                 --PARTNER_ID
             'N',                    --AUTO_VALIDATE_TRAN_EMPLOYEE_ID
             'A',                    --VIEW_SYS_CALC_TOTAL
             'N',                    --WKSTATION_TRAN_APPEND_IND
             'N',                    --INV_RESV_FROM_STORE_IND
             'Y',                    --INV_RESV_LAYAWAY_IND
             '*',                    --CC_NO_MASK_CHAR
             'R');

REM ****** (outloc seed data) ********************************************
INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL )
VALUES ('BT', '1000', 'Default Bill to Location', '&prim_currency', NULL, NULL, NULL, NULL, '&base_country', NULL, '1000', NULL, NULL, NULL, NULL, NULL);
REM ****** (calendar Seed Data) ********************************************
REM ****************************************************************************
REM *** Reason codes 1, 2, 3 are required for Bracket Costing and can not be ***
REM *** changed or deleted.  RMR 3/22/2001                                   ***
REM ****************************************************************************
INSERT INTO cost_chg_reason (REASON) VALUES (1);
INSERT INTO cost_chg_reason (REASON) VALUES (2);
INSERT INTO cost_chg_reason (REASON) VALUES (3);
INSERT INTO cost_chg_reason (REASON) VALUES (4);
INSERT INTO cost_chg_reason (REASON) VALUES (5);
INSERT INTO cost_chg_reason (REASON) VALUES (6);
INSERT INTO cost_chg_reason (REASON) VALUES (7);
INSERT INTO cost_chg_reason (REASON) VALUES (8);
INSERT INTO cost_chg_reason (REASON) VALUES (9);
INSERT INTO cost_chg_reason (REASON) VALUES (10);
REM ****************************************************************************************************************
INSERT INTO cost_chg_reason_tl (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (1, 1, 'New bracket', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO cost_chg_reason_tl (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (2, 1, 'New bracket structure', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO cost_chg_reason_tl (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (3, 1, 'Default bracket change', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO cost_chg_reason_tl (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (4, 1, 'Primary Supplier Changed', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO cost_chg_reason_tl (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (5, 1, 'Primary Country Changed', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO cost_chg_reason_tl (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (6, 1, 'Primary Loc Changed', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO cost_chg_reason_tl (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (7, 1, 'ItemLoc Supplier/Country Chg', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO cost_chg_reason_tl (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (8, 1, 'RCA cost change', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO cost_chg_reason_tl (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (9, 1, 'Tax Law Changed', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO cost_chg_reason_tl (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (10,1, 'External Cost Change', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
REM *******************************************************************************************************************
REM ****** (dummy Seed Data) ***********************************************
INSERT INTO dummy VALUES ('Y');
REM ****** (deal comp type for RPM created deals) ***********************************************
INSERT INTO deal_comp_type (DEAL_COMP_TYPE, CREATE_ID, CREATE_DATETIME ) VALUES ('VFP',USER,SYSDATE);
REM ****************************************************************************************************************
INSERT INTO deal_comp_type_tl (DEAL_COMP_TYPE, LANG, DEAL_COMP_TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ('VFP', 1, 'Vender Funded Promotion', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
REM ******************************************************************************************************************
REM ****** (doc_link Seed Data) ********************************************
INSERT INTO doc_link VALUES ('CE','REQ');
INSERT INTO doc_link VALUES ('CE','SI');
INSERT INTO doc_link VALUES ('CTRY','REQ');
INSERT INTO doc_link VALUES ('CTRY','SI');
INSERT INTO doc_link VALUES ('HTSC','REQ');
INSERT INTO doc_link VALUES ('HTSC','SI');
INSERT INTO doc_link VALUES ('IT','AI');
INSERT INTO doc_link VALUES ('IT','BI');
INSERT INTO doc_link VALUES ('IT','CS');
INSERT INTO doc_link VALUES ('IT','REQ');
INSERT INTO doc_link VALUES ('IT','SI');
INSERT INTO doc_link VALUES ('LC','AI');
INSERT INTO doc_link VALUES ('LC','BI');
INSERT INTO doc_link VALUES ('LC','CS');
INSERT INTO doc_link VALUES ('LC','REQ');
INSERT INTO doc_link VALUES ('LC','SI');
INSERT INTO doc_link VALUES ('LCA','AI');
INSERT INTO doc_link VALUES ('LCA','BI');
INSERT INTO doc_link VALUES ('LCA','CS');
INSERT INTO doc_link VALUES ('LCA','REQ');
INSERT INTO doc_link VALUES ('LCA','SI');
INSERT INTO doc_link VALUES ('PO','AI');
INSERT INTO doc_link VALUES ('PO','BI');
INSERT INTO doc_link VALUES ('PO','CS');
INSERT INTO doc_link VALUES ('PO','REQ');
INSERT INTO doc_link VALUES ('PO','SI');
INSERT INTO doc_link VALUES ('POIT','AI');
INSERT INTO doc_link VALUES ('POIT','BI');
INSERT INTO doc_link VALUES ('POIT','CS');
INSERT INTO doc_link VALUES ('POIT','REQ');
INSERT INTO doc_link VALUES ('POIT','SI');
INSERT INTO doc_link VALUES ('PTNR','REQ');
INSERT INTO doc_link VALUES ('PTNR','SI');
INSERT INTO doc_link VALUES ('SUPP','REQ');
INSERT INTO doc_link VALUES ('SUPP','SI');

REM ****** (inv_status_types Seed Data) ********************************************
INSERT INTO inv_status_types (INV_STATUS) VALUES (1);
INSERT INTO inv_status_types (INV_STATUS) VALUES (2);
REM ****** (inv_status_types_tl Seed Data Translation) ************************************
INSERT INTO inv_status_types_tl (INV_STATUS, LANG, INV_STATUS_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ( 1, 1, 'Trouble Merchandise', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO inv_status_types_tl (INV_STATUS, LANG, INV_STATUS_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ( 2, 1, 'Customer Order', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
REM ****** (inv_status_code Seed Data) ********************************************
INSERT INTO inv_status_codes (INV_STATUS_CODE, INV_STATUS ) VALUES ( 'ATS', NULL);
INSERT INTO inv_status_codes (INV_STATUS_CODE, INV_STATUS ) VALUES ( 'UTS', NULL);
INSERT INTO inv_status_codes (INV_STATUS_CODE, INV_STATUS ) VALUES ( 'WIP', NULL);
INSERT INTO inv_status_codes (INV_STATUS_CODE, INV_STATUS ) VALUES ( 'DIST', NULL);
INSERT INTO inv_status_codes (INV_STATUS_CODE, INV_STATUS ) VALUES ( 'TRBL', 1);
INSERT INTO inv_status_codes (INV_STATUS_CODE, INV_STATUS ) VALUES ( 'RIP', NULL);
INSERT INTO inv_status_codes (INV_STATUS_CODE, INV_STATUS ) VALUES ( 'COR', 2);
REM ****** (inv_status_codes_tl Seed Data Translation) **********************************
INSERT INTO inv_status_codes_tl (INV_STATUS_CODE, LANG, INV_STATUS_CODE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ( 'ATS', 1, 'Available To Sell', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO inv_status_codes_tl (INV_STATUS_CODE, LANG, INV_STATUS_CODE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ( 'UTS', 1, 'Unavailable To Sell', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO inv_status_codes_tl (INV_STATUS_CODE, LANG, INV_STATUS_CODE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ( 'WIP', 1, 'Work In Progress', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO inv_status_codes_tl (INV_STATUS_CODE, LANG, INV_STATUS_CODE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ( 'DIST', 1, 'Distributed', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO inv_status_codes_tl (INV_STATUS_CODE, LANG, INV_STATUS_CODE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ( 'TRBL', 1, 'Trouble', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO inv_status_codes_tl (INV_STATUS_CODE, LANG, INV_STATUS_CODE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ( 'RIP', 1, 'Receipt In Process', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
INSERT INTO inv_status_codes_tl (INV_STATUS_CODE, LANG, INV_STATUS_CODE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES ( 'COR', 1, 'Customer Order', 'Y', 'N', USER, SYSDATE, USER, SYSDATE);
REM ****** (safety stock lookup Seed Data) ********************************************
INSERT INTO safety_stock_lookup VALUES(-10,10.0000);
INSERT INTO safety_stock_lookup VALUES(-4.5,4.5000);
INSERT INTO safety_stock_lookup VALUES(-4.4,4.4000);
INSERT INTO safety_stock_lookup VALUES(-4.3,4.3000);
INSERT INTO safety_stock_lookup VALUES(-4.2,4.2000);
INSERT INTO safety_stock_lookup VALUES(-4.1,4.1000);
INSERT INTO safety_stock_lookup VALUES(-4,4.0000);
INSERT INTO safety_stock_lookup VALUES(-3.9,3.9000);
INSERT INTO safety_stock_lookup VALUES(-3.8,3.8000);
INSERT INTO safety_stock_lookup VALUES(-3.7,3.7000);
INSERT INTO safety_stock_lookup VALUES(-3.6,3.6000);
INSERT INTO safety_stock_lookup VALUES(-3.5,3.5000);
INSERT INTO safety_stock_lookup VALUES(-3.4,3.4000);
INSERT INTO safety_stock_lookup VALUES(-3.3,3.3000);
INSERT INTO safety_stock_lookup VALUES(-3.2,3.2000);
INSERT INTO safety_stock_lookup VALUES(-3.1,3.1000);
INSERT INTO safety_stock_lookup VALUES(-3,3.0000);
INSERT INTO safety_stock_lookup VALUES(-2.9,2.9010);
INSERT INTO safety_stock_lookup VALUES(-2.8,2.8010);
INSERT INTO safety_stock_lookup VALUES(-2.7,2.7010);
INSERT INTO safety_stock_lookup VALUES(-2.6,2.6010);
INSERT INTO safety_stock_lookup VALUES(-2.5,2.5020);
INSERT INTO safety_stock_lookup VALUES(-2.4,2.4030);
INSERT INTO safety_stock_lookup VALUES(-2.3,2.3030);
INSERT INTO safety_stock_lookup VALUES(-2.2,2.2050);
INSERT INTO safety_stock_lookup VALUES(-2.1,2.1060);
INSERT INTO safety_stock_lookup VALUES(-2,2.0080);
INSERT INTO safety_stock_lookup VALUES(-1.9,1.9110);
INSERT INTO safety_stock_lookup VALUES(-1.8,1.8140);
INSERT INTO safety_stock_lookup VALUES(-1.7,1.7180);
INSERT INTO safety_stock_lookup VALUES(-1.6,1.6230);
INSERT INTO safety_stock_lookup VALUES(-1.5,1.5290);
INSERT INTO safety_stock_lookup VALUES(-1.4,1.4370);
INSERT INTO safety_stock_lookup VALUES(-1.3,1.3460);
INSERT INTO safety_stock_lookup VALUES(-1.2,1.2560);
INSERT INTO safety_stock_lookup VALUES(-1.1,1.1690);
INSERT INTO safety_stock_lookup VALUES(-1,1.0830);
INSERT INTO safety_stock_lookup VALUES(-0.9,1.0000);
INSERT INTO safety_stock_lookup VALUES(-0.8,0.9200);
INSERT INTO safety_stock_lookup VALUES(-0.7,0.8430);
INSERT INTO safety_stock_lookup VALUES(-0.6,0.7690);
INSERT INTO safety_stock_lookup VALUES(-0.5,0.6980);
INSERT INTO safety_stock_lookup VALUES(-0.4,0.6300);
INSERT INTO safety_stock_lookup VALUES(-0.3,0.5670);
INSERT INTO safety_stock_lookup VALUES(-0.2,0.5070);
INSERT INTO safety_stock_lookup VALUES(-0.1,0.4510);
INSERT INTO safety_stock_lookup VALUES(0,0.3990);
INSERT INTO safety_stock_lookup VALUES(0.1,0.3510);
INSERT INTO safety_stock_lookup VALUES(0.2,0.3070);
INSERT INTO safety_stock_lookup VALUES(0.3,0.2670);
INSERT INTO safety_stock_lookup VALUES(0.4,0.2300);
INSERT INTO safety_stock_lookup VALUES(0.5,0.1980);
INSERT INTO safety_stock_lookup VALUES(0.6,0.1690);
INSERT INTO safety_stock_lookup VALUES(0.7,0.1430);
INSERT INTO safety_stock_lookup VALUES(0.8,0.1200);
INSERT INTO safety_stock_lookup VALUES(0.9,0.1000);
INSERT INTO safety_stock_lookup VALUES(1,0.0830);
INSERT INTO safety_stock_lookup VALUES(1.1,0.0690);
INSERT INTO safety_stock_lookup VALUES(1.2,0.0560);
INSERT INTO safety_stock_lookup VALUES(1.3,0.0460);
INSERT INTO safety_stock_lookup VALUES(1.4,0.0370);
INSERT INTO safety_stock_lookup VALUES(1.5,0.0290);
INSERT INTO safety_stock_lookup VALUES(1.6,0.0230);
INSERT INTO safety_stock_lookup VALUES(1.7,0.0180);
INSERT INTO safety_stock_lookup VALUES(1.8,0.0140);
INSERT INTO safety_stock_lookup VALUES(1.9,0.0110);
INSERT INTO safety_stock_lookup VALUES(2,0.0080);
INSERT INTO safety_stock_lookup VALUES(2.1,0.0060);
INSERT INTO safety_stock_lookup VALUES(2.2,0.0050);
INSERT INTO safety_stock_lookup VALUES(2.3,0.0040);
INSERT INTO safety_stock_lookup VALUES(2.4,0.0030);
INSERT INTO safety_stock_lookup VALUES(2.5,0.0020);
INSERT INTO safety_stock_lookup VALUES(2.8,0.0010);
INSERT INTO safety_stock_lookup VALUES(4.5,0.0000);
REM ****** (tran_data_codes Seed Data) ********************************************

INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 1, 'Net Sales');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 2, 'Net Sales VAT Exclusive');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 3, 'Non-inventory Items Sales/Returns');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 4, 'Returns');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 5, 'Non-inventory VAT Exclusive Sales');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 6, 'Deals income (Sales)');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 7, 'Deals income (Purchases)');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 8, 'Fixed Income Accrual');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 10,'Weight Variance');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 11, 'Markup');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 12, 'Markup Cancel');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 13, 'Permanent Markdown');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 14, 'Markdown Cancel');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 15, 'Promotional Markdown');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 16, 'Clearance Markdown');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 17, 'Intercompany Markup');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 18, 'Intercompany Markdown');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 20, 'Purchases');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 21, 'Invoice-used for Oracle GL intrfc only');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 22, 'Stock Adjustment');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 23, 'Stock Adjustment - COGS');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 24, 'Return to Vendor');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 25, 'Unavailable Inventory Transfer');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 26, 'Freight');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 27, 'QC RTV');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 28, 'Profit Up Charge - Receiving Location');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 29, 'Expense Up Charge - Receiving Location');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 30, 'Transfers In');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 31, 'Book Transfers In');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 32, 'Transfers Out');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 33, 'Book Transfers Out');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 34, 'Reclassifications In');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 36, 'Reclassifications Out');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 37, 'Intercompany In');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 38, 'Intercompany Out');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 39, 'Intercompany Margin');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 41, 'Data Warehouse Stock Ledger Adjustment');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 44, 'Data Warehouse Inbound Transfer Receipt');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 50, 'Open Stock');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) values ( 51, 'Shrinkage');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 52, 'Close Stock');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 53, 'Gross Margin');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 54, 'HTD GAFS');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 55, 'Inter Stocktake Sales Amt');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 56, 'Inter Stocktake Shrink Amt');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 57, 'Stocktake Mtd Sales Amt');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 58, 'Stocktake Mtd Shrink Amt');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 59, 'Stocktake Bookstk Cost');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 60, 'Employee Discount');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 61, 'Stocktake Actstk Cost/Retail');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 62, 'Freight Claim');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 63, 'WO Activity - Update Inventory');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 64, 'WO Activity - Post to Financials');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 65, 'Restocking Fee');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 70, 'Cost Variance');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 71, 'Cost Variance - Retail Accounting');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 72, 'Cost Variance - Cost Accounting');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 73, 'Cost Variance - Rec. Cost Adj.  FiFO');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 74, 'Recoverable Tax for Destination Location');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 75, 'Recoverable Tax for Source Location');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 80, 'Workroom/Other Cost of Sales');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 81, 'Cash Discount');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 82, 'Franchise Sales');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 83, 'Franchise Returns');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 84, 'Franchise Markups');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 85, 'Franchise Markdowns');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 86, 'Franchise Restocking fee');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 87, 'Vat In Cost');
INSERT INTO TRAN_DATA_CODES ( CODE, DECODE ) VALUES ( 88, 'Vat Out Retail');

REM ****** (tran_data_codes_ref Seed Data) ********************************************

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (1, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (2, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (3, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (4, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (5, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (6, 'DEALINC.PC', 'Deal ID', 'Stock Ledger Indicator', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (7, 'DEALINC.PC', 'Deal ID', 'Stock Ledger Indicator', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (8, 'DEALFINC.PC', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (10, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (11, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (11, 'TRANSFER_OUT_SQL.EXECUTE', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (12, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (13, 'TRANSFER_OUT_SQL.EXECUTE', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (13, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (14, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (15, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (16, 'RMSSUB_PRICECHANGE_UPDATE.PERSIST', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (17, 'STKLEDGR_SQL.WRITE_FINANCIALS', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (18, 'STKLEDGR_SQL.WRITE_FINANCIALS', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (20, 'RECEIVE_SQL.ITEM', 'Order No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (20, 'salesprocess', 'Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (20, 'RECCTADJ.FMB', 'Order No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (20, 'ALC_SQL.UPDATE_STKLEDGR', 'Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (20,'BOL_SQL.PROCESS_TSF','Distro Number','WF Order No',NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'INV_SQL.ADJ_TRAN_DATA', 'Reason Code', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'STKVAR.PC', 'Stock Count No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'INVADJ_SQL.ADJ_STOCK', 'Reason Code', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'INVADJSK', 'Reason Code', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'prodtsfm.fmb', 'Reason Code', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'invaupld.pc', 'Reason Code', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'INVADJST', 'Reason Code', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'wasteadj.pc', 'Reason Code', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'TRANSFER_IN_SQL.EXECUTE', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'BOL_SQL.PROCESS_TSF', 'RMA No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (22, 'INVADJ_SQL.BUILD_PROCESS_INVADJ', NULL, NULL, 'Reason Code');

INSERT INTO TRAN_DATA_CODES_REF (TRAN_CODE,PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC ) 
VALUES (22, 'TRANSFER_SQL.TRAN_DATA_WRITES', 'Transfer No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF (TRAN_CODE,PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC ) 
VALUES (22, 'ORDER_RCV_SQL.STOCKLEDGER_INFO', 'Order No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF (TRAN_CODE,PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC ) 
VALUES (22, 'RTV_SQL.INVENTORY', 'RTV No', NULL, 'Reason Code');

INSERT INTO TRAN_DATA_CODES_REF (TRAN_CODE,PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC ) 
VALUES (22, 'STOCK_ORDER_RCV_SQL.TRANDATA_OVERAGE', 'Distro No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF (TRAN_CODE,PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC ) 
VALUES (22, 'STOCK_ORDER_RCV_SQL.PROC_STK_CNT_TD_WRITE', 'Distro No', 'Stock Count No', 'Reason Code');

INSERT INTO TRAN_DATA_CODES_REF (TRAN_CODE,PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC ) 
VALUES (22, 'SOERBOL.FMB', 'Distro No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF (TRAN_CODE,PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC ) 
VALUES (22, 'STOCK_ORDER_RECONCILE_SQL.TO_LOC_SHORTAGE_NL_BL', 'Distro No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF (TRAN_CODE,PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC ) 
VALUES (22, 'UPDATE_SNAPSHOT_SQL.PROC_STK_CNT_TD_WRITE', 'Stock Count No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (23, 'wasteadj.pc', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (23, 'BOL_SQL.PROCESS_TSF', 'RMA No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (23, 'INVADJSK', NULL, NULL, 'Reason Code');

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (23, 'INVADJST', NULL, NULL, 'Reason Code');

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (23, 'INVADJ_SQL.ADJ_STOCK', NULL, NULL, 'Reason Code');

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (23, 'INVADJ_SQL.BUILD_PROCESS_INVADJ', NULL, NULL, 'Reason Code');

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (24, 'RTV_SQL.INVENTORY', 'RTV Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (24,'BOL_SQL.PROCESS_TSF','Distro Number','RMA No',NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (25, 'invaupld.pc', 'Inventory Status', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (25, 'INVADJSK', 'Inventory Status', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (25, 'TRANSFER_OUT_SQL.EXECUTE', 'Inventory Status', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (25, 'INV_SQL.ADJ_TRAN_DATA', 'Inventory Status', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (25, 'INVADJ_SQL.CHANGE_STATUS', 'Inventory Status', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (25, 'TRANSFER_IN_SQL.EXECUTE', 'Inventory Status', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (25, 'RTV_SQL.INVENTORY', 'Inventory Status', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (25, 'INVADJST', 'Inventory Status', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (27, 'rtvupld.pc', 'RTV Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (27, 'RTV.FMB', 'RTV Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (28, 'TRANSFER_OUT_SQL.EXECUTE', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (29, 'TRANSFER_OUT_SQL.EXECUTE', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (30, 'TRANSFER_OUT_SQL.EXECUTE', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (31, 'TRANSFER_OUT_SQL.EXECUTE', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (32, 'TRANSFER_OUT_SQL.EXECUTE', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (33, 'TRANSFER_OUT_SQL.EXECUTE', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (34, 'RECLASS_SQL.ITEM_PROCESS', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (36, 'RECLASS_SQL.ITEM_PROCESS', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (37, 'STKLEDGR_SQL.WRITE_FINANCIALS', 'Transfer No', 'Shipment No', 'From Location');

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (38, 'STKLEDGR_SQL.WRITE_FINANCIALS', 'Transfer No', 'Shipment No', 'To Location');

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (41, 'stkvar', NULL, 'Cycle (stock) count', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (44, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (50, 'Freight Claim', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (60, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (63, 'STKLEDGR_SQL.WRITE_FINANCIALS', 'Transfer No', 'Shipment No', 'Activity ID');

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (64, 'STKLEDGR_SQL.WRITE_FINANCIALS', 'Transfer No', 'Shipment No', 'Activity ID');

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (65, 'RTV_SQL.INVENTORY', 'RTV Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (65, 'RTV_SQL.WRITE_RESTOCKING_FEE', 'RTV Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (65, 'STKLEDGR_SQL.WRITE_FINANCIALS', 'Transfer No', 'Shipment No', 'To Location');

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (65,'BOL_SQL.PROCESS_TSF','Distro Number','RMA No',NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (70, 'TRANSFER_OUT_SQL.EXECUTE', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (70, 'RECEIVE_SQL.ITEM', 'Order No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (70, 'UPDATE_BASE_COST', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (70, 'RECCTADJ.FMB', 'Order No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (70, 'avcstadj.fmb', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (71, 'INVC_SQL.WRITE_INVC_TOL_TRAN_DATA', 'Invoice No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (71, 'STKLEDGR_SQL.POST_COST_VARIANCE', 'RTV Order No / Transfer No / Alloc No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (71,'BOL_SQL.PROCESS_TSF','Distro Number', 'RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (72, 'STKLEDGR_SQL.POST_COST_VARIANCE', 'RTV Order No / Transfer No / Alloc No', 'Shipment No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (72,'BOL_SQL.PROCESS_TSF','Distro Number', 'RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (73, 'recctadj.fmb', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (82, 'BOL_SQL.PROCESS_TSF', 'Distro Number', 'WF Order No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (83, 'BOL_SQL.PROCESS_TSF', 'Distro Number', 'RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (84, 'BOL_SQL.PROCESS_TSF', 'Distro Number', 'WF Order No/RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (85, 'BOL_SQL.PROCESS_TSF', 'Distro Number', 'WF Order No/RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (86, 'BOL_SQL.PROCESS_TSF', 'Distro Number', 'RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (87, 'ORDER_RCV_SQL.STOCKLEDGER_INFO', 'Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (87, 'ORDER_RCV_SQL.BACK_OUT_ALC', 'Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (87, 'RTV_SQL.WRITE_TRAN_DATA', 'RTV Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (87, 'REC_COST_ADJ_SQL.ITEM', 'Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (87, 'REC_UNIT_ADJ_SQL.CHECK_RECORDS', 'Order No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (87, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (87, 'BOL_SQL.PROCESS_TSF', 'Distro Number', 'WF Order No/RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (88, 'salesprocess', NULL, NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (88, 'WF_BOL_SQL.SEND_TSF', 'Transfer No', NULL, NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (88, 'WF_RETURN_SQL.APPROVE', 'Distro Number', 'RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (88, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING', 'Distro Number', 'WF Order No/RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC,GL_REF_NO_DESC )
VALUES (88,'BOL_SQL.PROCESS_TSF', 'Distro Number', 'WF Order No/RMA No',NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (82, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING', 'Distro Number', 'WF Order No', NULL); 

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (82, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE', 'Distro Number', 'WF Order No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (20, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING', 'Distro Number', 'WF Order No', NULL); 

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (20, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE', 'Distro Number', 'WF Order No/RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (84, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING', 'Distro Number', 'WF Order No/RMA No', NULL); 

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (84, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE', 'Distro Number', 'WF Order No/RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (85, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING', 'Distro Number', 'WF Order No/RMA No', NULL); 

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (85, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE', 'Distro Number', 'WF Order No/RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (87, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING', 'Distro Number', 'WF Order No/RMA No', NULL); 

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (87, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE', 'Distro Number', 'WF Order No/RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (83, 'WF_RETURN_SQL.APPROVE', 'Distro Number', 'RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (83, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING', 'Distro Number', 'RMA No', NULL); 

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (83, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE', 'Distro Number', 'RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (84, 'WF_RETURN_SQL.APPROVE', 'Distro Number', 'RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (85, 'WF_RETURN_SQL.APPROVE', 'Distro Number', 'RMA No', NULL); 

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (86, 'WF_RETURN_SQL.APPROVE', 'Distro Number', 'RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (88, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE', 'Distro Number', 'WF Order No/RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (24, 'WF_RETURN_SQL.APPROVE', 'Distro Number', 'RMA No', NULL);

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (24, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING', 'Distro Number', 'RMA No', NULL); 

INSERT INTO TRAN_DATA_CODES_REF ( TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC )
VALUES (24, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE', 'Distro Number', 'RMA No', NULL);

REM ****** (tsf_types Seed Data) ********************************************
insert into tsf_type(tsf_type, default_chrgs_ind) values ('CO','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('RV','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('CF','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('NS','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('AD','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('MR','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('BT','N');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('SR','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('PO','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('CT','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('PL','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('NB','N');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('EG','N');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('IC','Y');
insert into tsf_type(tsf_type, default_chrgs_ind) values ('SG','Y');
REM ****** Populate the vehicle_round table.  Used by supplier constraint scaling. **********
insert into vehicle_round(low_value, high_value, vehicle_qty) values (0.01,1.19,1.00);
insert into vehicle_round(low_value, high_value, vehicle_qty) values (1.20,2.29,2.00);
insert into vehicle_round(low_value, high_value, vehicle_qty) values (2.30,3.39,3.00);
insert into vehicle_round(low_value, high_value, vehicle_qty) values (3.40,4.39,4.00);
insert into vehicle_round(low_value, high_value, vehicle_qty) values (4.40,5.49,5.00);
insert into vehicle_round(low_value, high_value, vehicle_qty) values (5.50,6.49,6.00);
insert into vehicle_round(low_value, high_value, vehicle_qty) values (6.50,7.49,7.00);
insert into vehicle_round(low_value, high_value, vehicle_qty) values (7.50,8.49,8.00);
insert into vehicle_round(low_value, high_value, vehicle_qty) values (8.50,9.49,9.00);
insert into vehicle_round(low_value, high_value, vehicle_qty) values (9.50,10.00,10.00);
REM ****** Populate cost_zone_group **********
INSERT INTO COST_ZONE_GROUP(ZONE_GROUP_ID, COST_LEVEL, DESCRIPTION ) VALUES (1000, 'L', 'Location Zone Group');
INSERT INTO COST_ZONE_GROUP(ZONE_GROUP_ID, COST_LEVEL, DESCRIPTION ) VALUES (2000, 'Z', 'Geographic Cost Zones');

REM ****** Populate tax_event_run_type **********
insert into tax_event_run_type values ('PO','Order Tax Breakup','SYNC');
REM *********** Populates CVB_HEAD and ELC_COMP with hts cost components**********
prompt 'Starting File: elc_comp_pre_htsupld.sql';
@@elc_comp_pre_htsupld.sql &prim_currency;

REM *********** Populates COST_EVENT_RUN_TYPE_CONFIG **********
prompt 'Starting File: cost_event_run_type_config.sql';
@@cost_event_run_type_config.sql;

REM *********** Insert the Miscellaneous non-merch code into the NON_MERCH_CODE_HEAD and NON_MERCH_CODE_HEAD_TL tables **********
insert into non_merch_code_head(non_merch_code, service_ind)
                        values ('M', 'N');
insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                            values ('M', '1', 'Miscellaneous', 'Y', 'N', USER, SYSDATE, USER, SYSDATE); 

REM *********** Insert N seed record into RMS_BATCH_STATUS table **********
insert into rms_batch_status (BATCH_RUNNING_IND) values ('N');

REM ************** Populates ELC_COMP , CVB_HEAD AND CVB_DETAIL******************
prompt 'Starting File: elccomp.sql';
@@elccomp.sql;

REM ************** Populates ADD_TYPE ******************
prompt 'Starting File: addtype.sql';
@@addtype.sql;
REM ************** ADD_TYPE_MODULE ******************
insert into add_type_module values ('01', 'SUPP', 'Y', 'Y');
insert into add_type_module values ('02', 'SUPP', 'N', 'N');
insert into add_type_module values ('03', 'SUPP', 'N', 'Y');
insert into add_type_module values ('04', 'SUPP', 'N', 'Y');
insert into add_type_module values ('05', 'SUPP', 'N', 'Y');
insert into add_type_module values ('06', 'SUPP', 'N', 'Y');
--partners
insert into add_type_module values ('01', 'PTNR', 'Y', 'Y');
insert into add_type_module values ('02', 'PTNR', 'N', 'Y');
insert into add_type_module values ('03', 'PTNR', 'N', 'N');
insert into add_type_module values ('04', 'PTNR', 'N', 'N');
insert into add_type_module values ('05', 'PTNR', 'N', 'Y');
insert into add_type_module values ('06', 'PTNR', 'N', 'N');
--locations
insert into add_type_module values ('01', 'WH', 'Y', 'Y');
insert into add_type_module values ('02', 'WH', 'N', 'Y');
insert into add_type_module (select '04','WH','N','N' 
                               from dual 
                              where not exists (select 'x' 
                                                  from add_type_module 
                                                 where address_type=04 
                                                   and module='WH'));
insert into add_type_module values ('01', 'ST', 'Y', 'Y');
insert into add_type_module values ('02', 'ST', 'N', 'Y');
insert into add_type_module values ('03', 'ST', 'N', 'N');
insert into add_type_module values ('04', 'ST', 'N', 'N');

---
insert into add_type_module values ('01', 'WFST', 'N', 'Y');
insert into add_type_module values ('02', 'WFST', 'N', 'N');
insert into add_type_module values ('05', 'WFST', 'N', 'Y');
insert into add_type_module values ('07', 'WFST', 'Y', 'Y');

REM ************** FIF_LINE_TYPE_XREF ******************   
INSERT INTO FIF_LINE_TYPE_XREF ( RMS_LINE_TYPE, RMS_LINE_TYPE_DESC,
FIF_LINE_TYPE ) VALUES (
'ITEM', 'Item', 'ITEM');
INSERT INTO FIF_LINE_TYPE_XREF ( RMS_LINE_TYPE, RMS_LINE_TYPE_DESC,
FIF_LINE_TYPE ) VALUES (
'FRGHT', 'Freight', 'FREIGHT');
INSERT INTO FIF_LINE_TYPE_XREF ( RMS_LINE_TYPE, RMS_LINE_TYPE_DESC,
FIF_LINE_TYPE ) VALUES (
'TAX', 'Tax', 'TAX');
INSERT INTO FIF_LINE_TYPE_XREF ( RMS_LINE_TYPE, RMS_LINE_TYPE_DESC,
FIF_LINE_TYPE ) VALUES ( 'MISC', 'Miscellaneous', 'MISCELLANEOUS');

REM ****** (commiting...) ********************************************
commit;
