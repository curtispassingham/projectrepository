
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-- calendar.sql
-- This script sets 'vdate' in the system and also inserts past dates 
-- using either 454 calendar or standard calendar.
set serveroutput on
set verify off
set feedback off
exec dbms_output.enable(200000)

--installerize
--accept G_cal_type    char prompt 'Enter Calendar type to be used. (4)454 calendar. (C)Standard calendar. (4/C): ' 
--accept G_week_option char prompt 'Enter week start - week end option. (1)Sat-Fri. (2)Sun-Sat. (3)Mon-Sun. (1/2/3): '
--prompt *** Note: vdate should be entered as any date greater than or equal to 'calendar start date + 1 month'. 
--prompt *** Calendar start date is hard-coded in the script.
--prompt *** For example, if calendar start date is 04-FEB-2001, then vdate should be any date greater than or equal to 04-MAR-2001.
--rem    *** If that is not the case then error 'CALENDAR table out of range, no data found see your DBA...ORA-01403: no data found from CAL_TO_454' is received.
define G_cal_type = &1;
define G_week_option = &2;

--installerize
--accept G_vdate       date prompt 'Enter date to be set as transaction date (vdate as DD-MON-YYYY): '
define G_vdate = &3;

DECLARE
   LP_start_date           DATE;     
   LP_working_date         CALENDAR.FIRST_DAY%TYPE;     
   LP_first_day            DATE;     
   LP_month_end            DATE;     
   LP_week_cnt             NUMBER(2) := 0;
   LP_year                 NUMBER(2) := 0;
   LP_num_years            NUMBER(2) := 30;
   LP_dummy                VARCHAR2(12);
   LP_year_454             CALENDAR.YEAR_454%TYPE;
   LP_month_454            CALENDAR.MONTH_454%TYPE;
   LP_454                  VARCHAR2(12) := '445445445445';
   LP_now                  NUMBER(2) := 0;
   LP_ret_code             VARCHAR2(5);
   LP_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   LP_last_eom_start_month SYSTEM_VARIABLES.LAST_EOM_START_MONTH%TYPE;
   LP_last_dd_454          VARCHAR2(4) := NULL;
   LP_last_wk_454          VARCHAR2(4) := NULL;
   LP_last_mm_454          VARCHAR2(4) := NULL;
   LP_last_yr_454          VARCHAR2(4) := NULL;
   LP_adj_days             NUMBER(2)   := 0;
   LP_start_of_half_month  SYSTEM_OPTIONS.START_OF_HALF_MONTH%TYPE;
   LP_year_454_ref         CALENDAR.YEAR_454%TYPE;


   --- Var. for Half processing.
   LP_cal_yr               VARCHAR2(4) := NULL;
   LP_cal_month            VARCHAR2(4) := NULL;
   LP_cal_day              VARCHAR2(4) := NULL;
   LP_dd_454               PERIOD.CURR_454_DAY%TYPE  := NULL; 
   LP_wk_454               PERIOD.CURR_454_WEEK%TYPE  := NULL;
   LP_mm_454               PERIOD.CURR_454_MONTH%TYPE := NULL;
   LP_yr_454               PERIOD.CURR_454_YEAR%TYPE  := NULL;
   LP_half_no              PERIOD.HALF_NO%TYPE;
   LP_half_no_G            PERIOD.HALF_NO%TYPE;
   LP_month_in_half        PERIOD.CURR_454_MONTH_IN_HALF%TYPE;
   LP_month_in_half_G      PERIOD.CURR_454_MONTH_IN_HALF%TYPE;
   LP_next_half            PERIOD.NEXT_HALF_NO%TYPE;
   LP_next_half_G          PERIOD.NEXT_HALF_NO%TYPE;
   LP_work_dd              NUMBER;
   LP_work_mm              NUMBER;
   LP_work_yr              NUMBER;
   LP_fdoh                 PERIOD.START_454_HALF%TYPE;
   LP_ldoh                 PERIOD.END_454_HALF%TYPE;
   LP_fdoh_G               PERIOD.START_454_HALF%TYPE;
   LP_ldoh_G               PERIOD.END_454_HALF%TYPE;

   --- Var. for Month processing.
   LP_fdom                 PERIOD.START_454_MONTH%TYPE;
   LP_ldom                 PERIOD.END_454_MONTH%TYPE;
   LP_midm                 PERIOD.MID_454_MONTH%TYPE;
   LP_midm_G               SYSTEM_VARIABLES.LAST_EOM_MID_MONTH%TYPE;

   --- Var. for Week processing.
   LP_week_no              PERIOD.CURR_454_WEEK_IN_HALF%TYPE;

   LP_fdow                 SYSTEM_VARIABLES.LAST_EOW_DATE%TYPE;
   LP_ldow                 SYSTEM_VARIABLES.NEXT_EOW_DATE_UNIT%TYPE;
   LP_ldom_454             PERIOD.END_454_MONTH%TYPE; 
   LP_fdom_454             PERIOD.START_454_MONTH%TYPE; 
   ---
   DATE_CALC_ERROR        EXCEPTION;
   INVALID_INPUT          EXCEPTION;
   PROGRAM_ERROR          EXCEPTION;
   ---
   ------------------------------------------------------------------------------------------
   FUNCTION IS_LEAP_YR (I_year IN NUMBER)
   RETURN BOOLEAN IS

   BEGIN
      if ( MOD(I_year,4) = 0 AND ((MOD(I_year,100) != 0) OR (MOD(I_year,400) = 0)) ) then
         --dbms_output.put_line (' ~~~ Leap Yr ~~~ '||I_year);
         return TRUE;
      else
         return FALSE;
      end if;
   
   EXCEPTION
      when OTHERS then 
         dbms_output.put_line ('*** In IS_LEAP_YR. Error *** '||SQLERRM);
         return FALSE;

   END IS_LEAP_YR;
   ------------------------------------------------------------------------------------------
   FUNCTION CALC_HALF 
   RETURN BOOLEAN IS

   BEGIN
      CAL_TO_454 (LP_cal_day,
                  LP_cal_month,
                  LP_cal_yr,
                  LP_dd_454,
                  LP_wk_454,
                  LP_mm_454,
                  LP_yr_454,
                  LP_ret_code,
                  LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message||' In CAL_TO_454';
         raise DATE_CALC_ERROR;
      end if;

      CAL_TO_454_HALF (LP_cal_day,
                       LP_cal_month,
                       LP_cal_yr,
                       LP_half_no,
                       LP_month_in_half,
                       LP_ret_code,
                       LP_error_message);
      
      if LP_ret_code = 'FALSE' then
         LP_error_message :=  LP_error_message||' In CAL_TO_454_HALF';
         raise DATE_CALC_ERROR;
      end if;

      HALF_TO_454_FDOH (LP_half_no,
                        LP_work_dd,
                        LP_work_mm,
                        LP_work_yr,
                        LP_ret_code,
                        LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message||' In HALF_TO_454_FDOH ';
         raise DATE_CALC_ERROR;
      end if;

      LP_fdoh := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');

      HALF_TO_454_LDOH (LP_half_no,
                        LP_work_dd,
                        LP_work_mm,
                        LP_work_yr,
                        LP_ret_code,
                        LP_error_message);

      if LP_ret_code = 'FALSE' then
          LP_error_message := LP_error_message||' In HALF_TO_454_LDOH';
         raise DATE_CALC_ERROR;
      end if;

      LP_ldoh := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');

      if (MOD(LP_half_no,10) - 1) = 0 then
         LP_next_half := LP_half_no + 1;
      else
         LP_next_half := LP_half_no + 9;
      end if;

      return TRUE;

   EXCEPTION
      when DATE_CALC_ERROR then
         dbms_output.put_line ('*** In CALC_HALF. Error *** '||LP_error_message||'-'||SQLERRM);
         return FALSE;

      when OTHERS then 
         dbms_output.put_line ('*** In CALC_HALF. Error *** '||SQLERRM);
         return FALSE;

   END CALC_HALF;
   ------------------------------------------------------------------------------------------
   FUNCTION CALC_MONTH
   RETURN BOOLEAN IS

   BEGIN

      CAL_TO_454_FDOM (LP_cal_day,
                       LP_cal_month,
                       LP_cal_yr,
                       LP_work_dd,
                       LP_work_mm,
                       LP_work_yr,
                       LP_ret_code,
                       LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message||' In CAL_TO_454_FDOM';
         raise DATE_CALC_ERROR;
      end if;

      LP_fdom := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');

      LP_midm := TO_DATE(LP_yr_454||LPAD(LP_mm_454,2,'0')||15,'YYYYMMDD');

      CAL_TO_454_LDOM (LP_cal_day,
                       LP_cal_month,
                       LP_cal_yr,
                       LP_work_dd,
                       LP_work_mm,
                       LP_work_yr,
                       LP_ret_code,
                       LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message||' In CAL_TO_454_LDOM';
         raise DATE_CALC_ERROR;
      end if;

      LP_ldom := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');

      return TRUE;

   EXCEPTION
      when DATE_CALC_ERROR then
         dbms_output.put_line ('*** In CALC_MONTH. Error *** '||LP_error_message||':'||SQLERRM);
         return FALSE;

      when OTHERS then 
         dbms_output.put_line ('*** In CALC_MONTH. Error *** '||SQLERRM);
         return FALSE;

   END CALC_MONTH;
   ------------------------------------------------------------------------------------------
   FUNCTION CALC_WEEK
   RETURN BOOLEAN IS

   BEGIN

      CAL_TO_454_WEEKNO (LP_cal_day,
                         LP_cal_month,
                         LP_cal_yr,
                         LP_week_no,
                         LP_ret_code,
                         LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message||' In CAL_TO_454_WEEKNO';
         raise DATE_CALC_ERROR;
      end if;

      C454_TO_CAL (1,
                   LP_wk_454,
                   LP_mm_454,
                   LP_yr_454,
                   LP_work_dd,
                   LP_work_mm,
                   LP_work_yr,
                   LP_ret_code,
                   LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message||' In C454_TO_CAL';
         raise DATE_CALC_ERROR;
      end if;

      LP_fdow := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');

      C454_TO_CAL (7,
                   LP_wk_454,
                   LP_mm_454,
                   LP_yr_454,
                   LP_work_dd,
                   LP_work_mm,
                   LP_work_yr,
                   LP_ret_code,
                   LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message||' In C454_TO_CAL';
         raise DATE_CALC_ERROR;
      end if;

      LP_ldow := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');

      return TRUE;

   EXCEPTION
      when DATE_CALC_ERROR then
         dbms_output.put_line ('*** In CALC_WEEK. Error *** '||LP_error_message||':'||SQLERRM);
         return FALSE;

      when OTHERS then 
         dbms_output.put_line ('*** In CALC_WEEK. Error *** '||SQLERRM);
         return FALSE;

   END CALC_WEEK;
   ------------------------------------------------------------------------------------------
   FUNCTION CALC_MONTH_GREG 
   RETURN BOOLEAN IS

   BEGIN

      CAL_TO_454_FDOM (LP_cal_day,
                       LP_cal_month,
                       LP_cal_yr,
                       LP_work_dd,
                       LP_work_mm,
                       LP_work_yr,
                       LP_ret_code,
                       LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message || ' In CAL_TO_454_FDOM';
         raise DATE_CALC_ERROR;
      end if;

      LP_fdom_454 := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');
      LP_midm := TO_DATE(LP_yr_454||LPAD(LP_mm_454,2,'0')||15,'YYYYMMDD');

      CAL_TO_454_LDOM (LP_cal_day,
                       LP_cal_month,
                       LP_cal_yr,
                       LP_work_dd,
                       LP_work_mm,
                       LP_work_yr,
                       LP_ret_code,
                       LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message || ' In CAL_TO_454_LDOM';
         raise DATE_CALC_ERROR;
      end if;

      LP_ldom_454 := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');

      CAL_TO_CAL_FDOM (LP_cal_day,
                       LP_cal_month,
                       LP_cal_yr,
                       LP_work_dd,
                       LP_work_mm,
                       LP_work_yr,
                       LP_ret_code,
                       LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message || ' In CAL_TO_454_FDOM';
         raise DATE_CALC_ERROR;
      end if;

      LP_fdom := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');
      LP_midm_G := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||15,'YYYYMMDD');

      CAL_TO_CAL_LDOM (LP_cal_day,
                       LP_cal_month,
                       LP_cal_yr,
                       LP_work_dd,
                       LP_work_mm,
                       LP_work_yr,
                       LP_ret_code,
                       LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message || ' In CAL_TO_CAL_LDOM';
         raise DATE_CALC_ERROR;
      end if;

      LP_ldom := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');

      return TRUE;

   EXCEPTION
      when DATE_CALC_ERROR then
         dbms_output.put_line ('*** In CALC_MONTH_GREG. Error *** '||LP_error_message||':'||SQLERRM);
         return FALSE;

      when OTHERS then 
         dbms_output.put_line ('*** In CALC_MONTH_GREG. Error *** '||LP_error_message);
         return FALSE;

   END CALC_MONTH_GREG;
   ------------------------------------------------------------------------------------------
   FUNCTION CALC_HALF_GREG 
   RETURN BOOLEAN IS

   BEGIN

      if not CALC_HALF then
         raise DATE_CALC_ERROR;
      end if;
      
      CAL_TO_CAL_HALF (LP_cal_day,
                       LP_cal_month,
                       LP_cal_yr,
                       LP_half_no_G,
                       LP_month_in_half_G,
                       LP_ret_code,
                       LP_error_message);
      
      if LP_ret_code = 'FALSE' then
         LP_error_message :=  LP_error_message||' In CAL_TO_CAL_HALF';
         raise DATE_CALC_ERROR;
      end if;

      HALF_TO_CAL_FDOH (LP_half_no_G,
                        LP_work_dd,
                        LP_work_mm,
                        LP_work_yr,
                        LP_ret_code,
                        LP_error_message);

      if LP_ret_code = 'FALSE' then
         LP_error_message := LP_error_message||' In HALF_TO_CAL_FDOH ';
         raise DATE_CALC_ERROR;
      end if;

      LP_fdoh_G := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');

      HALF_TO_CAL_LDOH (LP_half_no_G,
                        LP_work_dd,
                        LP_work_mm,
                        LP_work_yr,
                        LP_ret_code,
                        LP_error_message);

      if LP_ret_code = 'FALSE' then
          LP_error_message := LP_error_message||' In HALF_TO_CAL_LDOH';
         raise DATE_CALC_ERROR;
      end if;

      LP_ldoh_G := TO_DATE(LP_work_yr||LPAD(LP_work_mm,2,'0')||LPAD(LP_work_dd,2,'0'),'YYYYMMDD');

      if (MOD(LP_half_no_G,10) - 1) = 0 then
         LP_next_half_G := LP_half_no_G + 1;
      else
         LP_next_half_G := LP_half_no_G + 9;
      end if;

      return TRUE;

   EXCEPTION
      when DATE_CALC_ERROR then
         dbms_output.put_line ('*** In CALC_HALF_GREG. Error *** '||LP_error_message||'-'||SQLERRM);
         return FALSE;

      when OTHERS then 
         dbms_output.put_line ('*** In CALC_HALF_GREG. Error *** '||SQLERRM);
         return FALSE;

   END CALC_HALF_GREG;
   ------------------------------------------------------------------------------------------
   FUNCTION PROCESS_VDATE (I_cal_454_ind   IN   SYSTEM_OPTIONS.CALENDAR_454_IND%TYPE,
                           I_vdate         IN   PERIOD.VDATE%TYPE)
   RETURN BOOLEAN IS
      L_vdate                VARCHAR2(12) := NULL;
      L_temp_date            VARCHAR2(12) := NULL;
      L_month_of_fdoh        NUMBER(2);
      L_month_of_vdate       NUMBER(2);
      L_last_eom_month_no   NUMBER(2);
   BEGIN

      L_vdate := TO_CHAR(TO_DATE(I_vdate,'DD-MON-RR'),'YYYYMMDD');
       
      LP_cal_yr    := substr(L_vdate,1,4);
      LP_cal_month := substr(L_vdate,5,2);
      LP_cal_day   := substr(L_vdate,7,2);

      if I_cal_454_ind = '4' then -- Its 4-5-4 calendar
         -- Calculate Half.
         if not CALC_HALF then
            raise DATE_CALC_ERROR;
         end if;

         -- Calculate Month.
         if not CALC_MONTH then
            raise DATE_CALC_ERROR;
         end if;

         -- Calculate Week.
         if not CALC_WEEK then
            raise DATE_CALC_ERROR;
         end if;

         BEGIN 

            update period
               set vdate                  = TO_DATE(L_vdate,'YYYYMMDD'),
                   start_454_half         = LP_fdoh,
                   end_454_half           = LP_ldoh,
                   start_454_month        = LP_fdom,
                   mid_454_month          = LP_midm,
                   end_454_month          = LP_ldom,
                   half_no                = LP_half_no,
                   next_half_no           = LP_next_half,
                   curr_454_day           = LP_dd_454,
                   curr_454_week          = LP_wk_454,
                   curr_454_month         = LP_mm_454,
                   curr_454_year          = LP_yr_454,
                   curr_454_month_in_half = LP_month_in_half,
                   curr_454_week_in_half  = LP_week_no;
         EXCEPTION
            when OTHERS then
               LP_error_message := 'Update Period Error';
               raise PROGRAM_ERROR;
         END;

         if (MOD(LP_half_no, 10) - 1) = 0 then
            LP_next_half := LP_half_no + 1;
         else
            LP_next_half := LP_half_no + 9;
         end if;

         if LP_month_in_half = 1 then
            L_last_eom_month_no := 6;
         else
            L_last_eom_month_no :=  LP_month_in_half - 1;
         end if;

         select max(first_day)
           into LP_last_eom_start_month
           from calendar
          where first_day < (LP_fdom - 1);

         select 7, no_of_weeks, month_454, year_454
           into LP_last_dd_454,LP_last_wk_454,LP_last_mm_454, LP_last_yr_454
           from calendar
          where first_day = LP_last_eom_start_month;

         select trunc((LP_fdom-LP_fdoh)/7)
           into LP_week_no
           from dual;

         BEGIN
            update system_variables
               set last_eom_half_no      = LP_half_no,
                   last_eom_month_no     = L_last_eom_month_no,
                   last_eom_date         = LP_fdom - 1,
                   next_eom_date         = LP_ldom,
                   last_eom_start_half   = LP_fdoh,
                   last_eom_end_half     = LP_ldoh,
                   last_eom_start_month  = LP_last_eom_start_month,
                   last_eom_mid_month    = LP_midm,
                   last_eom_next_half_no = LP_next_half,
                   last_eom_day          = LP_last_dd_454,
                   last_eom_week         = LP_last_wk_454,
                   last_eom_month        = LP_last_mm_454,
                   last_eom_year         = LP_last_yr_454,
                   last_eom_week_in_half = LP_week_no ,
                   last_eom_date_unit    = LP_fdom - 1,
                   next_eom_date_unit    = LP_ldom,
                   last_eow_date         = LP_fdow - 1,
                   last_eow_date_unit    = LP_fdow - 1,
                   next_eow_date_unit    = LP_ldow,
                   last_cont_order_date  = NULL;
        EXCEPTION
           when OTHERS then
             LP_error_message := 'Update System_variables Error';
             raise PROGRAM_ERROR;
        END;
      ---

      else -- Gregorian cal.
         -- Calculate Half
         if not CALC_HALF_GREG then
            raise DATE_CALC_ERROR;
         end if;

         -- Calculate Grgorian Month.
         if not CALC_MONTH_GREG then
            raise DATE_CALC_ERROR;
         end if;

         -- Calculate Week.
         if not CALC_WEEK then
            raise DATE_CALC_ERROR;
         end if;

         BEGIN
            update period
               set vdate                  = TO_DATE(L_vdate,'YYYYMMDD'),
                   start_454_half         = LP_fdoh,
                   end_454_half           = LP_ldoh,
                   start_454_month        = LP_fdom_454,
                   mid_454_month          = LP_midm,
                   end_454_month          = LP_ldom_454,
                   half_no                = LP_half_no,
                   next_half_no           = LP_next_half,
                   curr_454_day           = LP_dd_454,
                   curr_454_week          = LP_wk_454,
                   curr_454_month         = LP_mm_454,
                   curr_454_year          = LP_yr_454,
                   curr_454_month_in_half = LP_month_in_half,
                   curr_454_week_in_half  = LP_week_no;
         EXCEPTION
            when OTHERS then
               LP_error_message := 'Update Period Error';
               raise PROGRAM_ERROR;
         END;

         if (MOD(LP_half_no_G, 10) - 1) = 0 then
            LP_next_half_G := LP_half_no_G + 1;
         else
            LP_next_half_G := LP_half_no_G + 9;
         end if;

         if LP_month_in_half_G = 1 then
            L_last_eom_month_no := 6;
         else
            L_last_eom_month_no :=  LP_month_in_half_G - 1;
         end if;

         select max(first_day)
           into LP_last_eom_start_month
           from calendar
          where first_day < (LP_fdom_454 - 1);

         select 7, no_of_weeks, month_454, year_454
           into LP_last_dd_454,LP_last_wk_454,LP_last_mm_454, LP_last_yr_454
           from calendar
          where first_day = LP_last_eom_start_month;

         select trunc((LP_fdom-LP_fdoh)/7)
           into LP_week_no
           from dual;

         BEGIN
            update system_variables
               set last_eom_half_no      = LP_half_no_G,
                   last_eom_month_no     = L_last_eom_month_no,
                   last_eom_date         = LP_fdom - 1,
                   next_eom_date         = LP_ldom,
                   last_eom_start_half   = LP_fdoh_G,
                   last_eom_end_half     = LP_ldoh_G,
                   last_eom_start_month  = add_months(LP_fdom, -1),
                   last_eom_mid_month    = add_months(LP_midm_G, -1),
                   last_eom_next_half_no = LP_next_half_G,
                   last_eom_day          = LP_last_dd_454,
                   last_eom_week         = LP_last_wk_454,
                   last_eom_month        = LP_last_mm_454,
                   last_eom_year         = LP_last_yr_454,
                   last_eom_week_in_half = LP_week_no,
                   last_eom_date_unit    = LP_fdom - 1,
                   next_eom_date_unit    = LP_ldom,
                   last_eow_date         = LP_fdow - 1,
                   last_eow_date_unit    = LP_fdow - 1,
                   next_eow_date_unit    = LP_ldow,
                   last_cont_order_date  = NULL;
        EXCEPTION
           when OTHERS then
              LP_error_message := 'Update System_Variables Error';
              raise PROGRAM_ERROR;
        END;
      ---
      end if; -- Standard/Gregorain.

      return TRUE;

   EXCEPTION
      when DATE_CALC_ERROR then
         dbms_output.put_line ('*** In PROCESS_VDATE. Error in date calculation *** '||LP_error_message);
         return FALSE;

      when OTHERS then
         dbms_output.put_line ('*** In PROCESS_VDATE. Error *** '||LP_error_message||'-'||SQLERRM);
         return FALSE;

   END PROCESS_VDATE;
   ------------------------------------------------------------------------------------------
   FUNCTION INS_HALF (I_year   IN NUMBER)
   RETURN BOOLEAN IS

      L_half_no      VARCHAR2(5);
      L_half_name    HALF.HALF_NAME%TYPE;
      L_half_date    HALF.HALF_DATE%TYPE;
      L_cnt          NUMBER(2);

   BEGIN
      FOR L_cnt in 1..2
      LOOP
         L_half_no := I_year||L_cnt;
         
         select TO_CHAR(ADD_MONTHS(
                TO_DATE('01-JAN-'||DECODE(abs(LP_start_of_half_month),LP_start_of_half_month,I_year,I_year-1),'DD-MON-YYYY'),
                abs(LP_start_of_half_month)-1+((L_cnt-1)*6)),'MON YYYY') || ' to ' ||
                TO_CHAR(ADD_MONTHS(
                TO_DATE('01-JAN-'||DECODE(abs(LP_start_of_half_month),LP_start_of_half_month,I_year,I_year-1),'DD-MON-YYYY'),
                abs(LP_start_of_half_month)+4+((L_cnt-1)*6)),'MON YYYY') 
           into L_half_date 
           from dual;

         select DECODE(L_cnt,1,'Summer ','Winter ')||I_year
           into L_half_name
           from dual; 

         insert into half (half_no,
                           half_name,
                           half_date)
                   values (TO_NUMBER(L_half_no),
                           L_half_name,
                           L_half_date);
      END LOOP;

      return TRUE;

   EXCEPTION
      when OTHERS then
         dbms_output.put_line ('Error in INS_HALF !!! '||SQLERRM);
         return FALSE;

   END INS_HALF;
   ------------------------------------------------------------------------------------------
BEGIN
   -- Main routine body starts here.

   -- Check User input.
   if upper('&G_cal_type') != '4' and upper('&G_cal_type') != 'C' then
      LP_error_message := 'Enter (C) or (4) for Calendar type.';
      raise INVALID_INPUT;
   end if;

   if '&G_week_option' != '1' and '&G_week_option' != '2' and '&G_week_option' != '3' then
      LP_error_message := 'Enter 1, 2 or 3 for week type.';
      raise INVALID_INPUT;
   end if;

   -- Check if User has entered the date in correct format.
   BEGIN
      select TO_DATE('&G_vdate','DD-MON-YYYY')
        into LP_dummy
        from dual;

   EXCEPTION
      when OTHERS then
         dbms_output.put_line ('*** Invalid Date: '||SQLERRM);
         return;
   END;

   -- Delete existing data, if any.
   delete calendar;
   delete period;
   delete half;
   delete system_variables;
   --
   select start_of_half_month
     into LP_start_of_half_month
     from system_options;


   -- Check calendar type. Iterate the loop to insert data in the table.
   if upper('&G_cal_type') = '4' then -- its a 4-5-4 calendar.

      -- Initializing based on NRF calendar.  Jan 2007 is a 5 week month.
      -- RMS calendar table starts in January regardless of the fiscal year.
      LP_adj_days := 7;
      LP_start_date := TO_DATE('31-12-2006','DD-MM-YYYY');
      LP_working_date := LP_start_date ; 
      LP_year_454 := TO_NUMBER(TO_CHAR(TO_DATE(LP_working_date + 15), 'YYYY'));
      LP_month_454 := TO_NUMBER(TO_CHAR(TO_DATE(LP_working_date + 15), 'MM'));

      LOOP -- num_years
         if not INS_HALF (LP_year_454) then
            raise PROGRAM_ERROR;
         end if;

         FOR i IN 1..12 LOOP
            IF (i = 1) THEN
               --- 4-5-4 calendar counts to 52 weeks that equals 364 days a year.
               --- But calendar has 365 days,
               --- and 366 days in a leap year. So, that difference needs to be accumulated.
               --- As per reference provided on NRF site, extra week is adjusted at year-end.
               --- This is hardcoded to January based on NRF calendar, would have to be modified
               --- to support a different calendar.
               if LP_adj_days >= 7 then
                  LP_now := 5;
                  LP_adj_days := LP_adj_days - 7;
               else
                  LP_now := 4;
               end if;
            ELSE
               LP_now := TO_NUMBER(SUBSTR(LP_454, i, 1));
            END IF;

            insert into calendar (first_day,
                                  year_454,
                                  month_454,
                                  no_of_weeks)
                          values (LP_working_date,
                                  LP_year_454,
                                  LP_month_454,
                                  LP_now);

            LP_working_date := TO_DATE(LP_working_date + LP_now*7);
            LP_year_454 := TO_NUMBER(TO_CHAR(TO_DATE(LP_working_date + 15), 'YYYY'));
            LP_month_454 := TO_NUMBER(TO_CHAR(TO_DATE(LP_working_date + 15), 'MM'));
         END LOOP;

         LP_year := LP_year + 1;

         if LP_year >=  LP_num_years then
            EXIT;
         end if;
         --- Check whether the prior year was a leap year and adjust accordingly
         if IS_LEAP_YR (LP_year_454 - 1) = TRUE then
            LP_adj_days := LP_adj_days + 2;
         else
            LP_adj_days := LP_adj_days + 1;
         end if;         
         --- 
      END LOOP; -- num years
   else -- populate gregorian (standard) Calendar.

      LP_start_date := TO_DATE('01-01-07','DD-MM-RR');

      if '&G_week_option' = '1' then
         LP_working_date := NEXT_DAY((to_date(LP_start_date,'DD-MM-RR')) - 1, 'SAT');
      elsif '&G_week_option' = '2' then
         LP_working_date := NEXT_DAY((to_date(LP_start_date,'DD-MM-RR')) - 1, 'SUN');
      elsif '&G_week_option' = '3' then
         LP_working_date := NEXT_DAY((to_date(LP_start_date,'DD-MM-RR')) - 1, 'MON');
      end if;

      LP_month_end := ADD_MONTHS(LP_start_date,1) - 1;

      -- Loop for no. of years.
      LOOP

         -- Iterate for 12 months in the year
         FOR LP_mm_cnt in 1..12 
         LOOP
            LP_first_day := LP_working_date;

            -- Iterate to get no. of weeks in a month.
            LOOP
               LP_week_cnt := LP_week_cnt + 1;
               LP_working_date := LP_working_date + 7;

               if LP_working_date > LP_month_end then
                  insert into calendar (first_day,
                                        year_454,
                                        month_454,
                                        no_of_weeks) 
                                values (LP_first_day,
                                        TO_CHAR(TO_DATE(LP_first_day,'DD-MM-RR'),'YYYY'),
                                        LP_mm_cnt,
                                        LP_week_cnt);
                  LP_week_cnt := 0;
                  LP_month_end := ADD_MONTHS(LP_month_end,1);
                  EXIT;
               end if;

            END LOOP; -- no. of weeks.
            --
         END LOOP; -- months

         if not INS_HALF (TO_NUMBER(TO_CHAR(TO_DATE(LP_first_day,'DD-MM-RR'),'YYYY'))) then
            raise PROGRAM_ERROR;
         end if;

         LP_year := LP_year + 1;
         --
         if LP_year >= LP_num_years then
            EXIT;
         end if;
         --
      END LOOP; -- years

   end if; -- calendar type

   --
   -- Setup initial values in the 'period' table. 
   -- These values get updated later during vdate processing (PROCESS_VDATE). 
   --
   insert into period (SYSAVAIL,
                       VDATE,
                       START_454_HALF,
                       END_454_HALF,
                       START_454_MONTH,
                       MID_454_MONTH,
                       END_454_MONTH,
                       HALF_NO,
                       NEXT_HALF_NO,
                       CURR_454_DAY,
                       CURR_454_WEEK,
                       CURR_454_MONTH,
                       CURR_454_YEAR,
                       CURR_454_MONTH_IN_HALF,
                       CURR_454_WEEK_IN_HALF)
               values (1,
                       TO_DATE('09-03-2001', 'DD-MM-YYYY'),
                       TO_DATE('22-01-2001', 'DD-MM-YYYY'),
                       TO_DATE('29-07-2001', 'DD-MM-YYYY'),
                       TO_DATE('19-02-2001', 'DD-MM-YYYY'),
                       TO_DATE('15-02-2001', 'DD-MM-YYYY'),
                       TO_DATE('25-03-2001', 'DD-MM-YYYY'),
                       20011,
                       20012,
                       5,
                       3,
                       3,
                       2001,
                       2,
                       7);

   -- 
   -- Setup initial values in the 'system_variables' table. 
   -- These values get updated later during vdate processing (PROCESS_VDATE).
   --

   insert into system_variables ( LAST_EOM_HALF_NO,
                                  LAST_EOM_MONTH_NO,
                                  LAST_EOM_DATE,
                                  NEXT_EOM_DATE,
                                  LAST_EOM_START_HALF,
                                  LAST_EOM_END_HALF,
                                  LAST_EOM_START_MONTH,
                                  LAST_EOM_MID_MONTH,
                                  LAST_EOM_NEXT_HALF_NO,
                                  LAST_EOM_DAY,
                                  LAST_EOM_WEEK,
                                  LAST_EOM_MONTH,
                                  LAST_EOM_YEAR,
                                  LAST_EOM_WEEK_IN_HALF,
                                  LAST_EOM_DATE_UNIT,
                                  NEXT_EOM_DATE_UNIT,
                                  LAST_EOW_DATE,
                                  LAST_EOW_DATE_UNIT,
                                  NEXT_EOW_DATE_UNIT,
                                  LAST_CONT_ORDER_DATE )
                         VALUES ( 20042, 5,  TO_Date( '10/31/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '11/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '06/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '12/25/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '11/15/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  20051, 1, 2, 11, 2004, 19,
                                  TO_Date( '01/22/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '02/26/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '10/03/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '01/22/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '01/29/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  NULL );

   -- vdate processing goes here. Calculate appropriate values using data from 
   -- 'calendar' table and update those in 'period' & 'system_variables' tables. 
   -- PROCESS_VDATE function calls various sub-routines to derive different
   -- column values and updates above said two tables.
   -- This function mimics the 'process' fn. from 'dtesys' batch program.

   if not PROCESS_VDATE('&G_cal_type', 
                        TO_DATE('&G_vdate','DD-MON-YYYY')) then
      raise PROGRAM_ERROR;
   end if;

   commit;

EXCEPTION
   when INVALID_INPUT then
      dbms_output.put_line ('*** Error *** Invalid value entered.'||LP_error_message); 
      return;

   when PROGRAM_ERROR then
      dbms_output.put_line ('*** Error encountered in PROCESS_VDATE ***');
      return;

   when OTHERS then
      dbms_output.put_line ('*** SQL Error *** '||SQLERRM);
      return;

END;
/

set verify on
--set feedback on

