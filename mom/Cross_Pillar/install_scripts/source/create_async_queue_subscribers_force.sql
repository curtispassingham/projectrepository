clear buffer;
set serveroutput on size 999999

----------------------------------------------------------------------------------
-- Copyright (c) 2012, Oracle Inc.  All rights reserved.
----------------------------------------------------------------------------------
--  Run this script in Owning Schema.
----------------------------------------------------------------------------------
DECLARE

   L_register_event_ind   VARCHAR2(1)  := 'Y';
   L_job_type             RMS_ASYNC_JOB.JOB_TYPE%TYPE;
   L_queue_table          RMS_ASYNC_JOB.QUEUE_TABLE%TYPE;
   L_queue_name           RMS_ASYNC_JOB.QUEUE_NAME%TYPE;
   L_recipient            RMS_ASYNC_JOB.RECIPIENT_NAME%TYPE;
   L_queue_payload_type   RMS_ASYNC_JOB.QUEUE_PAYLOAD_TYPE%TYPE;
   L_event                RMS_ASYNC_JOB.DEQUEUE_EVENT%TYPE;
   L_user                 RMS_ASYNC_STATUS.CREATE_ID%TYPE  := USER;

   L_queue_count          NUMBER        := 0;
   L_queue_table_count    NUMBER        := 0;
   L_subscriber_count     NUMBER        := 0;

   cursor C_JOB_QUEUE is
      select queue_table,
             queue_name,
             recipient_name,
             queue_payload_type,
             dequeue_event,
             job_type
        from rms_async_job,
             system_config_options;

   cursor C_SUBSCRIBER_EXISTS is
      select count(1)
        from dba_queue_subscribers
       where owner      = L_user
         and queue_name = L_queue_name;

   cursor C_QUEUE_EXISTS is
      select count(1)
        from dba_queues
       where owner = L_user
         and name  = L_queue_name;

   cursor C_QUEUE_TABLE_EXISTS is
      select count(1)
        from dba_queue_tables
       where owner       = L_user
         and queue_table = L_queue_table;

BEGIN

   DBMS_OUTPUT.PUT_LINE('Start of script...');

   open C_JOB_QUEUE;

   Loop

      fetch C_JOB_QUEUE into L_queue_table,
                             L_queue_name,
                             L_recipient,
                             L_queue_payload_type,
                             L_event,
                             L_job_type;

      if C_JOB_QUEUE%NOTFOUND then
         EXIT;
      end if;

      DBMS_OUTPUT.PUT_LINE('--------Creating Queue: '||L_queue_name||'---------------');

      ----------------------------------------------------------------------------------
      -- DROP EXISTING QUEUE SUBSCRIBERS
      ----------------------------------------------------------------------------------
      open C_SUBSCRIBER_EXISTS;
      fetch C_SUBSCRIBER_EXISTS into L_subscriber_count;
      close C_SUBSCRIBER_EXISTS;

      if L_subscriber_count > 0 then
         DBMS_AQADM.REMOVE_SUBSCRIBER(QUEUE_NAME => L_user||'.'||L_queue_name,
                                      SUBSCRIBER => sys.aq$_agent(L_recipient,NULL,NULL));
      end if;

      ----------------------------------------------------------------------------------
      -- STOP AND DROP EXISTING QUEUES
      ----------------------------------------------------------------------------------
      open C_QUEUE_EXISTS;
      fetch C_QUEUE_EXISTS into L_queue_count;
      close C_QUEUE_EXISTS;

      if L_queue_count > 0 then
         DBMS_AQADM.STOP_QUEUE(QUEUE_NAME => L_user||'.'||L_queue_name);

         DBMS_AQADM.DROP_QUEUE(QUEUE_NAME => L_user||'.'||L_queue_name);
      end if;

      ----------------------------------------------------------------------------------
      -- DROP EXISTING QUEUE TABLE
      ----------------------------------------------------------------------------------
      open C_QUEUE_TABLE_EXISTS;
      fetch C_QUEUE_TABLE_EXISTS into L_queue_table_count;
      close C_QUEUE_TABLE_EXISTS;

      if L_queue_table_count > 0 then
         DBMS_AQADM.DROP_QUEUE_TABLE(QUEUE_TABLE => L_user||'.'||L_queue_table);
      end if;

      DBMS_OUTPUT.PUT_LINE('Creating queue table: '||L_queue_table);

      ----------------------------------------------------------------------------------
      -- CREATE QUEUE TABLE
      ----------------------------------------------------------------------------------
      DBMS_AQADM.CREATE_QUEUE_TABLE(QUEUE_TABLE        => L_user||'.'||L_queue_table,
                                    QUEUE_PAYLOAD_TYPE => L_user||'.'||L_queue_payload_type,
                                    MULTIPLE_CONSUMERS => TRUE);

      ----------------------------------------------------------------------------------
      -- CREATE AND START QUEUES
      ----------------------------------------------------------------------------------
      DBMS_AQADM.CREATE_QUEUE(QUEUE_NAME  => L_user||'.'||L_queue_name,
                              QUEUE_TABLE => L_user||'.'||L_queue_table);

      DBMS_AQADM.START_QUEUE(QUEUE_NAME => L_user||'.'||L_queue_name);

      ----------------------------------------------------------------------------------
      -- CREATE AND REGISTER SUBSCRIBERS TO PL/SQL NOTIFY PACKAGES
      ----------------------------------------------------------------------------------
      DBMS_AQADM.ADD_SUBSCRIBER
         (QUEUE_NAME => L_user||'.'||L_queue_name,
          SUBSCRIBER => sys.aq$_agent(L_recipient,NULL,NULL));

      --Only register events that will have a specific PL/SQL callback procedure.
      --Otherwise, do NOT register it. This is to handle things like notification queue,
      --which will be registered with rtkstrt on form runtime using the Events object property.
      if L_register_event_ind = 'Y' then
         DBMS_AQ.REGISTER
            (sys.aq$_reg_info_list(
               sys.aq$_reg_info(L_user||'.'||L_queue_name||':'||L_recipient,
                                DBMS_AQ.NAMESPACE_AQ,
                                'plsql://'||L_user||'.'||L_event,
                                HEXTORAW('FF'))),
             1);
      end if;

      DBMS_OUTPUT.PUT_LINE('Successfully created: '||L_queue_name);
      DBMS_OUTPUT.PUT_LINE('   ');

   End Loop;

   close C_JOB_QUEUE;

   DBMS_OUTPUT.PUT_LINE('End of Script...');

EXCEPTION
   when OTHERS then
      DBMS_OUTPUT.PUT_LINE('exception: '||SQLCODE||', '||SQLERRM);
END;
/

