SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

set define "^";

REM ****This Script inserts seed data into vat_region,vat_codes table****
DELETE FROM outloc_tl;
DELETE FROM outloc;
DELETE FROM vat_region_tl;
DELETE FROM vat_region;
DELETE FROM vat_code_rates;
DELETE FROM vat_codes_tl;
DELETE FROM vat_codes;

REM******* (vat_region Seed Data)************************************
INSERT INTO VAT_REGION VALUES (1000, 'Vat Region 1000', 'E', 'N', NULL, 'S');

REM  ******(vat_codes Seed Data)*******
INSERT INTO VAT_CODES VALUES ('S', 'Standard','N');
INSERT INTO VAT_CODES VALUES ('E', 'Exempt','N');
INSERT INTO VAT_CODES VALUES ('C', 'Composite','N');
INSERT INTO VAT_CODES VALUES ('Z', 'Zero','N');

set define "&";

COMMIT;