
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------------
-- Copyright (c) 2007, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.1 $
-- $Modtime$
-------------------------------------------------------------------------------------
--  ATTENTION: This script DOES preserve data.
--
--  The customer DBA is responsible to review this script to ensure data
--  is preserved as desired.
--
-------------------------------------------------------------------------------------
--  POLICIES ADDED:     SA_TRAN_TENDER_CC_SEC
--                      SA_TRAN_TENDER_REV_CC_SEC
-------------------------------------------------------------------------------------
-- This script will add the VPD credit card security policy to the listed tables.
-------------------------------------------------------------------------------------
PROMPT Applying CC Security policy to table 'SA_TRAN_TENDER', 'SA_TRAN_TENDER_REV'
BEGIN
   dbms_rls.add_policy(object_schema=>user,
                       object_name=>'SA_TRAN_TENDER',
                       policy_name=>'SA_TRAN_TENDER_CC_SEC',
                       function_schema=>user,
                       policy_function=>'SA_CC_SECURITY_PREDICATE',
                       sec_relevant_cols=>'CC_NO,CC_EXP_DATE',
                       sec_relevant_cols_opt=>dbms_rls.ALL_ROWS);

   dbms_rls.add_policy(object_schema=>user,
                       object_name=>'SA_TRAN_TENDER_REV',
                       policy_name=>'SA_TRAN_TENDER_REV_CC_SEC',
                       function_schema=>user,
                       policy_function=>'SA_CC_SECURITY_PREDICATE',
                       sec_relevant_cols=>'CC_NO,CC_EXP_DATE',
                       sec_relevant_cols_opt=>dbms_rls.ALL_ROWS);

END;
/
