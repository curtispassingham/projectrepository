
--------------------------------------------------------
-- Copyright (c) 2012, Oracle Inc.  All rights reserved.
-- :   elc_comp_pre_htsupl.sql  $
-- :   1.0  $
-- :   Thu Apr 12 16:24:42 CDT 2012  $
--------------------------------------------------------
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
REM ----------------------------------------------------
REM
REM  This is the elc_comp script that is required for RMS.
REM  It is run in the basic install scripts one time
REM  only...at install. See the most recent
REM  install_scriptsXX.sql for the order it should be run.
REM
REM ----------------------------------------------------
REM -------------------------------------------------
REM ****************************************************************
PROMPT updating/inserting into cvb_head
REM ****************************************************************
------------------------------------------------------------------------
MERGE INTO CVB_HEAD TARGET USING (SELECT
'TEXP' CVB_CODE, 'Total Expense' CVB_DESC, 'N' NOM_FLAG_1, 'N' NOM_FLAG_2,
'N' NOM_FLAG_3, 'Y' NOM_FLAG_4, 'N' NOM_FLAG_5
FROM DUAL) USE_THIS
ON (TARGET.CVB_CODE = USE_THIS.CVB_CODE)
WHEN MATCHED THEN UPDATE
SET TARGET.CVB_DESC = USE_THIS.CVB_DESC,TARGET.NOM_FLAG_1 = USE_THIS.NOM_FLAG_1, TARGET.NOM_FLAG_2 = USE_THIS.NOM_FLAG_2,
TARGET.NOM_FLAG_3 = USE_THIS.NOM_FLAG_3,TARGET.NOM_FLAG_4 = USE_THIS.NOM_FLAG_4, TARGET.NOM_FLAG_5 = USE_THIS.NOM_FLAG_5
WHEN NOT MATCHED THEN INSERT (CVB_CODE,CVB_DESC,NOM_FLAG_1,NOM_FLAG_2,NOM_FLAG_3,NOM_FLAG_4,NOM_FLAG_5)
VALUES (USE_THIS.CVB_CODE,USE_THIS.CVB_DESC,USE_THIS.NOM_FLAG_1,USE_THIS.NOM_FLAG_2,USE_THIS.NOM_FLAG_3,USE_THIS.NOM_FLAG_4,USE_THIS.NOM_FLAG_5);
------------------------------------------------------------------------
MERGE INTO CVB_HEAD TARGET USING (SELECT
'TEXPC' CVB_CODE, 'Total Expense Country' CVB_DESC, 'N' NOM_FLAG_1, 'N' NOM_FLAG_2,
'N' NOM_FLAG_3, 'Y' NOM_FLAG_4, 'N' NOM_FLAG_5
FROM DUAL) USE_THIS
ON (TARGET.CVB_CODE = USE_THIS.CVB_CODE)
WHEN MATCHED THEN UPDATE
SET TARGET.CVB_DESC = USE_THIS.CVB_DESC,TARGET.NOM_FLAG_1 = USE_THIS.NOM_FLAG_1, TARGET.NOM_FLAG_2 = USE_THIS.NOM_FLAG_2,
TARGET.NOM_FLAG_3 = USE_THIS.NOM_FLAG_3,TARGET.NOM_FLAG_4 = USE_THIS.NOM_FLAG_4, TARGET.NOM_FLAG_5 = USE_THIS.NOM_FLAG_5
WHEN NOT MATCHED THEN INSERT (CVB_CODE,CVB_DESC,NOM_FLAG_1,NOM_FLAG_2,NOM_FLAG_3,NOM_FLAG_4,NOM_FLAG_5)
VALUES (USE_THIS.CVB_CODE,USE_THIS.CVB_DESC,USE_THIS.NOM_FLAG_1,USE_THIS.NOM_FLAG_2,USE_THIS.NOM_FLAG_3,USE_THIS.NOM_FLAG_4,USE_THIS.NOM_FLAG_5);
------------------------------------------------------------------------
MERGE INTO CVB_HEAD TARGET USING (SELECT
'TEXPZ' CVB_CODE, 'Total Expense Zone' CVB_DESC, 'N' NOM_FLAG_1, 'N' NOM_FLAG_2,
'N' NOM_FLAG_3, 'Y' NOM_FLAG_4, 'N' NOM_FLAG_5
FROM DUAL) USE_THIS
ON (TARGET.CVB_CODE = USE_THIS.CVB_CODE)
WHEN MATCHED THEN UPDATE
SET TARGET.CVB_DESC = USE_THIS.CVB_DESC,TARGET.NOM_FLAG_1 = USE_THIS.NOM_FLAG_1, TARGET.NOM_FLAG_2 = USE_THIS.NOM_FLAG_2,
TARGET.NOM_FLAG_3 = USE_THIS.NOM_FLAG_3,TARGET.NOM_FLAG_4 = USE_THIS.NOM_FLAG_4, TARGET.NOM_FLAG_5 = USE_THIS.NOM_FLAG_5
WHEN NOT MATCHED THEN INSERT (CVB_CODE,CVB_DESC,NOM_FLAG_1,NOM_FLAG_2,NOM_FLAG_3,NOM_FLAG_4,NOM_FLAG_5)
VALUES (USE_THIS.CVB_CODE,USE_THIS.CVB_DESC,USE_THIS.NOM_FLAG_1,USE_THIS.NOM_FLAG_2,USE_THIS.NOM_FLAG_3,USE_THIS.NOM_FLAG_4,USE_THIS.NOM_FLAG_5);
------------------------------------------------------------------------
REM ****************************************************************
--PROMPT updating/inserting into elc_comp
REM ****************************************************************
REM ****************************************************************
--PROMPT When prompted, enter the value for currency code
--PROMPT in single quotes: 'XXX'
REM ****************************************************************
------------------------------------------------------------------------
MERGE INTO ELC_COMP TARGET USING (SELECT
'ORDCST' COMP_ID, 'Order Cost' COMP_DESC, 'E' COMP_TYPE, '' ASSESS_TYPE,
'' IMPORT_COUNTRY_ID, 'C' EXPENSE_TYPE, '' UP_CHRG_TYPE, '' UP_CHRG_GROUP,
'' CVB_CODE, 'V' CALC_BASIS, 'O' COST_BASIS, 'M' EXP_CATEGORY,
'100' COMP_RATE, '1' COMP_LEVEL, '1' DISPLAY_ORDER, 'Y' ALWAYS_DEFAULT_IND,
''||'&&prim_currency'||'' COMP_CURRENCY, '' PER_COUNT, '' PER_COUNT_UOM, 'N' NOM_FLAG_1,
'N' NOM_FLAG_2, 'N' NOM_FLAG_3, 'N' NOM_FLAG_4, 'N' NOM_FLAG_5, 'N' SYS_GENERATED_IND
FROM DUAL) USE_THIS
ON (TARGET.COMP_ID = USE_THIS.COMP_ID)
WHEN MATCHED THEN UPDATE
SET TARGET.COMP_DESC = USE_THIS.COMP_DESC,TARGET.COMP_TYPE = USE_THIS.COMP_TYPE, TARGET.ASSESS_TYPE = USE_THIS.ASSESS_TYPE,
TARGET.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID,TARGET.EXPENSE_TYPE = USE_THIS.EXPENSE_TYPE, TARGET.UP_CHRG_TYPE = USE_THIS.UP_CHRG_TYPE,
TARGET.UP_CHRG_GROUP = USE_THIS.UP_CHRG_GROUP,TARGET.CVB_CODE = USE_THIS.CVB_CODE, TARGET.CALC_BASIS = USE_THIS.CALC_BASIS,
TARGET.COST_BASIS = USE_THIS.COST_BASIS,TARGET.EXP_CATEGORY = USE_THIS.EXP_CATEGORY, TARGET.COMP_RATE = USE_THIS.COMP_RATE,
TARGET.COMP_LEVEL = USE_THIS.COMP_LEVEL,TARGET.DISPLAY_ORDER = USE_THIS.DISPLAY_ORDER, TARGET.ALWAYS_DEFAULT_IND = USE_THIS.ALWAYS_DEFAULT_IND,
TARGET.COMP_CURRENCY = USE_THIS.COMP_CURRENCY,TARGET.PER_COUNT = USE_THIS.PER_COUNT, TARGET.PER_COUNT_UOM = USE_THIS.PER_COUNT_UOM,
TARGET.NOM_FLAG_1 = USE_THIS.NOM_FLAG_1,TARGET.NOM_FLAG_2 = USE_THIS.NOM_FLAG_2, TARGET.NOM_FLAG_3 = USE_THIS.NOM_FLAG_3, TARGET.NOM_FLAG_4 = USE_THIS.NOM_FLAG_4,
TARGET.NOM_FLAG_5 = USE_THIS.NOM_FLAG_5, TARGET.SYS_GENERATED_IND = USE_THIS.SYS_GENERATED_IND
WHEN NOT MATCHED THEN INSERT (COMP_ID,COMP_DESC,COMP_TYPE,ASSESS_TYPE,IMPORT_COUNTRY_ID,EXPENSE_TYPE,UP_CHRG_TYPE,UP_CHRG_GROUP,CVB_CODE,CALC_BASIS,COST_BASIS,EXP_CATEGORY,COMP_RATE,
COMP_LEVEL,DISPLAY_ORDER,ALWAYS_DEFAULT_IND,COMP_CURRENCY,PER_COUNT,PER_COUNT_UOM,NOM_FLAG_1,NOM_FLAG_2,NOM_FLAG_3,NOM_FLAG_4,NOM_FLAG_5,SYS_GENERATED_IND)
VALUES (USE_THIS.COMP_ID,USE_THIS.COMP_DESC,USE_THIS.COMP_TYPE,USE_THIS.ASSESS_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.EXPENSE_TYPE,USE_THIS.UP_CHRG_TYPE,USE_THIS.UP_CHRG_GROUP,
USE_THIS.CVB_CODE,USE_THIS.CALC_BASIS,USE_THIS.COST_BASIS,USE_THIS.EXP_CATEGORY,USE_THIS.COMP_RATE,USE_THIS.COMP_LEVEL,USE_THIS.DISPLAY_ORDER,USE_THIS.ALWAYS_DEFAULT_IND,
USE_THIS.COMP_CURRENCY,USE_THIS.PER_COUNT,USE_THIS.PER_COUNT_UOM,USE_THIS.NOM_FLAG_1,USE_THIS.NOM_FLAG_2,USE_THIS.NOM_FLAG_3,USE_THIS.NOM_FLAG_4,USE_THIS.NOM_FLAG_5,USE_THIS.SYS_GENERATED_IND);
------------------------------------------------------------------------
MERGE INTO ELC_COMP TARGET USING (SELECT
'TEXP' COMP_ID, 'Total Expense' COMP_DESC, 'E' COMP_TYPE, '' ASSESS_TYPE,
'' IMPORT_COUNTRY_ID, 'Z' EXPENSE_TYPE, '' UP_CHRG_TYPE, '' UP_CHRG_GROUP,
'TEXP' CVB_CODE, 'V' CALC_BASIS, '' COST_BASIS, 'A' EXP_CATEGORY,
'100' COMP_RATE, '99' COMP_LEVEL, '99' DISPLAY_ORDER, 'N' ALWAYS_DEFAULT_IND,
''||'&&prim_currency'||'' COMP_CURRENCY, '' PER_COUNT, '' PER_COUNT_UOM, 'N' NOM_FLAG_1,
'N' NOM_FLAG_2, 'N' NOM_FLAG_3, 'N' NOM_FLAG_4, 'N' NOM_FLAG_5, 'N' SYS_GENERATED_IND
FROM DUAL) USE_THIS
ON (TARGET.COMP_ID = USE_THIS.COMP_ID)
WHEN MATCHED THEN UPDATE
SET TARGET.COMP_DESC = USE_THIS.COMP_DESC,TARGET.COMP_TYPE = USE_THIS.COMP_TYPE, TARGET.ASSESS_TYPE = USE_THIS.ASSESS_TYPE,
TARGET.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID,TARGET.EXPENSE_TYPE = USE_THIS.EXPENSE_TYPE, TARGET.UP_CHRG_TYPE = USE_THIS.UP_CHRG_TYPE,
TARGET.UP_CHRG_GROUP = USE_THIS.UP_CHRG_GROUP,TARGET.CVB_CODE = USE_THIS.CVB_CODE, TARGET.CALC_BASIS = USE_THIS.CALC_BASIS,
TARGET.COST_BASIS = USE_THIS.COST_BASIS,TARGET.EXP_CATEGORY = USE_THIS.EXP_CATEGORY, TARGET.COMP_RATE = USE_THIS.COMP_RATE,
TARGET.COMP_LEVEL = USE_THIS.COMP_LEVEL,TARGET.DISPLAY_ORDER = USE_THIS.DISPLAY_ORDER, TARGET.ALWAYS_DEFAULT_IND = USE_THIS.ALWAYS_DEFAULT_IND,
TARGET.COMP_CURRENCY = USE_THIS.COMP_CURRENCY,TARGET.PER_COUNT = USE_THIS.PER_COUNT, TARGET.PER_COUNT_UOM = USE_THIS.PER_COUNT_UOM,
TARGET.NOM_FLAG_1 = USE_THIS.NOM_FLAG_1,TARGET.NOM_FLAG_2 = USE_THIS.NOM_FLAG_2, TARGET.NOM_FLAG_3 = USE_THIS.NOM_FLAG_3, TARGET.NOM_FLAG_4 = USE_THIS.NOM_FLAG_4,
TARGET.NOM_FLAG_5 = USE_THIS.NOM_FLAG_5, TARGET.SYS_GENERATED_IND = USE_THIS.SYS_GENERATED_IND
WHEN NOT MATCHED THEN INSERT (COMP_ID,COMP_DESC,COMP_TYPE,ASSESS_TYPE,IMPORT_COUNTRY_ID,EXPENSE_TYPE,UP_CHRG_TYPE,UP_CHRG_GROUP,CVB_CODE,CALC_BASIS,COST_BASIS,EXP_CATEGORY,COMP_RATE,
COMP_LEVEL,DISPLAY_ORDER,ALWAYS_DEFAULT_IND,COMP_CURRENCY,PER_COUNT,PER_COUNT_UOM,NOM_FLAG_1,NOM_FLAG_2,NOM_FLAG_3,NOM_FLAG_4,NOM_FLAG_5,SYS_GENERATED_IND)
VALUES (USE_THIS.COMP_ID,USE_THIS.COMP_DESC,USE_THIS.COMP_TYPE,USE_THIS.ASSESS_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.EXPENSE_TYPE,USE_THIS.UP_CHRG_TYPE,USE_THIS.UP_CHRG_GROUP,
USE_THIS.CVB_CODE,USE_THIS.CALC_BASIS,USE_THIS.COST_BASIS,USE_THIS.EXP_CATEGORY,USE_THIS.COMP_RATE,USE_THIS.COMP_LEVEL,USE_THIS.DISPLAY_ORDER,USE_THIS.ALWAYS_DEFAULT_IND,
USE_THIS.COMP_CURRENCY,USE_THIS.PER_COUNT,USE_THIS.PER_COUNT_UOM,USE_THIS.NOM_FLAG_1,USE_THIS.NOM_FLAG_2,USE_THIS.NOM_FLAG_3,USE_THIS.NOM_FLAG_4,USE_THIS.NOM_FLAG_5,USE_THIS.SYS_GENERATED_IND);
------------------------------------------------------------------------
MERGE INTO ELC_COMP TARGET USING (SELECT
'TEXPC' COMP_ID, 'Total Expense County' COMP_DESC, 'E' COMP_TYPE, '' ASSESS_TYPE,
'' IMPORT_COUNTRY_ID, 'C' EXPENSE_TYPE, '' UP_CHRG_TYPE, '' UP_CHRG_GROUP,
'TEXPC' CVB_CODE, 'V' CALC_BASIS, '' COST_BASIS, 'M' EXP_CATEGORY,
'100' COMP_RATE, '99' COMP_LEVEL, '99' DISPLAY_ORDER, 'Y' ALWAYS_DEFAULT_IND,
''||'&&prim_currency'||'' COMP_CURRENCY, '' PER_COUNT, '' PER_COUNT_UOM, 'N' NOM_FLAG_1,
'N' NOM_FLAG_2, 'N' NOM_FLAG_3, 'N' NOM_FLAG_4, 'N' NOM_FLAG_5, 'N' SYS_GENERATED_IND
FROM DUAL) USE_THIS
ON (TARGET.COMP_ID = USE_THIS.COMP_ID)
WHEN MATCHED THEN UPDATE
SET TARGET.COMP_DESC = USE_THIS.COMP_DESC,TARGET.COMP_TYPE = USE_THIS.COMP_TYPE, TARGET.ASSESS_TYPE = USE_THIS.ASSESS_TYPE,
TARGET.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID,TARGET.EXPENSE_TYPE = USE_THIS.EXPENSE_TYPE, TARGET.UP_CHRG_TYPE = USE_THIS.UP_CHRG_TYPE,
TARGET.UP_CHRG_GROUP = USE_THIS.UP_CHRG_GROUP,TARGET.CVB_CODE = USE_THIS.CVB_CODE, TARGET.CALC_BASIS = USE_THIS.CALC_BASIS,
TARGET.COST_BASIS = USE_THIS.COST_BASIS,TARGET.EXP_CATEGORY = USE_THIS.EXP_CATEGORY, TARGET.COMP_RATE = USE_THIS.COMP_RATE,
TARGET.COMP_LEVEL = USE_THIS.COMP_LEVEL,TARGET.DISPLAY_ORDER = USE_THIS.DISPLAY_ORDER, TARGET.ALWAYS_DEFAULT_IND = USE_THIS.ALWAYS_DEFAULT_IND,
TARGET.COMP_CURRENCY = USE_THIS.COMP_CURRENCY,TARGET.PER_COUNT = USE_THIS.PER_COUNT, TARGET.PER_COUNT_UOM = USE_THIS.PER_COUNT_UOM,
TARGET.NOM_FLAG_1 = USE_THIS.NOM_FLAG_1,TARGET.NOM_FLAG_2 = USE_THIS.NOM_FLAG_2, TARGET.NOM_FLAG_3 = USE_THIS.NOM_FLAG_3, TARGET.NOM_FLAG_4 = USE_THIS.NOM_FLAG_4,
TARGET.NOM_FLAG_5 = USE_THIS.NOM_FLAG_5, TARGET.SYS_GENERATED_IND = USE_THIS.SYS_GENERATED_IND
WHEN NOT MATCHED THEN INSERT (COMP_ID,COMP_DESC,COMP_TYPE,ASSESS_TYPE,IMPORT_COUNTRY_ID,EXPENSE_TYPE,UP_CHRG_TYPE,UP_CHRG_GROUP,CVB_CODE,CALC_BASIS,COST_BASIS,EXP_CATEGORY,COMP_RATE,
COMP_LEVEL,DISPLAY_ORDER,ALWAYS_DEFAULT_IND,COMP_CURRENCY,PER_COUNT,PER_COUNT_UOM,NOM_FLAG_1,NOM_FLAG_2,NOM_FLAG_3,NOM_FLAG_4,NOM_FLAG_5,SYS_GENERATED_IND)
VALUES (USE_THIS.COMP_ID,USE_THIS.COMP_DESC,USE_THIS.COMP_TYPE,USE_THIS.ASSESS_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.EXPENSE_TYPE,USE_THIS.UP_CHRG_TYPE,USE_THIS.UP_CHRG_GROUP,
USE_THIS.CVB_CODE,USE_THIS.CALC_BASIS,USE_THIS.COST_BASIS,USE_THIS.EXP_CATEGORY,USE_THIS.COMP_RATE,USE_THIS.COMP_LEVEL,USE_THIS.DISPLAY_ORDER,USE_THIS.ALWAYS_DEFAULT_IND,
USE_THIS.COMP_CURRENCY,USE_THIS.PER_COUNT,USE_THIS.PER_COUNT_UOM,USE_THIS.NOM_FLAG_1,USE_THIS.NOM_FLAG_2,USE_THIS.NOM_FLAG_3,USE_THIS.NOM_FLAG_4,USE_THIS.NOM_FLAG_5,USE_THIS.SYS_GENERATED_IND);
------------------------------------------------------------------------
MERGE INTO ELC_COMP TARGET USING (SELECT
'TEXPZ' COMP_ID, 'Total Expense Zone' COMP_DESC, 'E' COMP_TYPE, '' ASSESS_TYPE,
'' IMPORT_COUNTRY_ID, 'Z' EXPENSE_TYPE, '' UP_CHRG_TYPE, '' UP_CHRG_GROUP,
'TEXPZ' CVB_CODE, 'V' CALC_BASIS, '' COST_BASIS, 'M' EXP_CATEGORY,
'100' COMP_RATE, '99' COMP_LEVEL, '99' DISPLAY_ORDER, 'Y' ALWAYS_DEFAULT_IND,
''||'&&prim_currency'||'' COMP_CURRENCY, '' PER_COUNT, '' PER_COUNT_UOM, 'N' NOM_FLAG_1,
'N' NOM_FLAG_2, 'N' NOM_FLAG_3, 'N' NOM_FLAG_4, 'N' NOM_FLAG_5, 'N' SYS_GENERATED_IND
FROM DUAL) USE_THIS
ON (TARGET.COMP_ID = USE_THIS.COMP_ID)
WHEN MATCHED THEN UPDATE
SET TARGET.COMP_DESC = USE_THIS.COMP_DESC,TARGET.COMP_TYPE = USE_THIS.COMP_TYPE, TARGET.ASSESS_TYPE = USE_THIS.ASSESS_TYPE,
TARGET.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID,TARGET.EXPENSE_TYPE = USE_THIS.EXPENSE_TYPE, TARGET.UP_CHRG_TYPE = USE_THIS.UP_CHRG_TYPE,
TARGET.UP_CHRG_GROUP = USE_THIS.UP_CHRG_GROUP,TARGET.CVB_CODE = USE_THIS.CVB_CODE, TARGET.CALC_BASIS = USE_THIS.CALC_BASIS,
TARGET.COST_BASIS = USE_THIS.COST_BASIS,TARGET.EXP_CATEGORY = USE_THIS.EXP_CATEGORY, TARGET.COMP_RATE = USE_THIS.COMP_RATE,
TARGET.COMP_LEVEL = USE_THIS.COMP_LEVEL,TARGET.DISPLAY_ORDER = USE_THIS.DISPLAY_ORDER, TARGET.ALWAYS_DEFAULT_IND = USE_THIS.ALWAYS_DEFAULT_IND,
TARGET.COMP_CURRENCY = USE_THIS.COMP_CURRENCY,TARGET.PER_COUNT = USE_THIS.PER_COUNT, TARGET.PER_COUNT_UOM = USE_THIS.PER_COUNT_UOM,
TARGET.NOM_FLAG_1 = USE_THIS.NOM_FLAG_1,TARGET.NOM_FLAG_2 = USE_THIS.NOM_FLAG_2, TARGET.NOM_FLAG_3 = USE_THIS.NOM_FLAG_3, TARGET.NOM_FLAG_4 = USE_THIS.NOM_FLAG_4,
TARGET.NOM_FLAG_5 = USE_THIS.NOM_FLAG_5, TARGET.SYS_GENERATED_IND = USE_THIS.SYS_GENERATED_IND
WHEN NOT MATCHED THEN INSERT (COMP_ID,COMP_DESC,COMP_TYPE,ASSESS_TYPE,IMPORT_COUNTRY_ID,EXPENSE_TYPE,UP_CHRG_TYPE,UP_CHRG_GROUP,CVB_CODE,CALC_BASIS,COST_BASIS,EXP_CATEGORY,COMP_RATE,
COMP_LEVEL,DISPLAY_ORDER,ALWAYS_DEFAULT_IND,COMP_CURRENCY,PER_COUNT,PER_COUNT_UOM,NOM_FLAG_1,NOM_FLAG_2,NOM_FLAG_3,NOM_FLAG_4,NOM_FLAG_5,SYS_GENERATED_IND)
VALUES (USE_THIS.COMP_ID,USE_THIS.COMP_DESC,USE_THIS.COMP_TYPE,USE_THIS.ASSESS_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.EXPENSE_TYPE,USE_THIS.UP_CHRG_TYPE,USE_THIS.UP_CHRG_GROUP,
USE_THIS.CVB_CODE,USE_THIS.CALC_BASIS,USE_THIS.COST_BASIS,USE_THIS.EXP_CATEGORY,USE_THIS.COMP_RATE,USE_THIS.COMP_LEVEL,USE_THIS.DISPLAY_ORDER,USE_THIS.ALWAYS_DEFAULT_IND,
USE_THIS.COMP_CURRENCY,USE_THIS.PER_COUNT,USE_THIS.PER_COUNT_UOM,USE_THIS.NOM_FLAG_1,USE_THIS.NOM_FLAG_2,USE_THIS.NOM_FLAG_3,USE_THIS.NOM_FLAG_4,USE_THIS.NOM_FLAG_5,USE_THIS.SYS_GENERATED_IND);
------------------------------------------------------------------------
MERGE INTO ELC_COMP TARGET USING (SELECT
'UNCST' COMP_ID, 'Unit Cost' COMP_DESC, 'E' COMP_TYPE, '' ASSESS_TYPE,
'' IMPORT_COUNTRY_ID, 'Z' EXPENSE_TYPE, '' UP_CHRG_TYPE, '' UP_CHRG_GROUP,
'' CVB_CODE, 'V' CALC_BASIS, 'S' COST_BASIS, 'M' EXP_CATEGORY,
'100' COMP_RATE, '1' COMP_LEVEL, '1' DISPLAY_ORDER, 'N' ALWAYS_DEFAULT_IND,
''||'&&prim_currency'||'' COMP_CURRENCY, '' PER_COUNT, '' PER_COUNT_UOM, 'N' NOM_FLAG_1,
'N' NOM_FLAG_2, 'N' NOM_FLAG_3, 'N' NOM_FLAG_4, 'N' NOM_FLAG_5, 'N' SYS_GENERATED_IND
FROM DUAL) USE_THIS
ON (TARGET.COMP_ID = USE_THIS.COMP_ID)
WHEN MATCHED THEN UPDATE
SET TARGET.COMP_DESC = USE_THIS.COMP_DESC,TARGET.COMP_TYPE = USE_THIS.COMP_TYPE, TARGET.ASSESS_TYPE = USE_THIS.ASSESS_TYPE,
TARGET.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID,TARGET.EXPENSE_TYPE = USE_THIS.EXPENSE_TYPE, TARGET.UP_CHRG_TYPE = USE_THIS.UP_CHRG_TYPE,
TARGET.UP_CHRG_GROUP = USE_THIS.UP_CHRG_GROUP,TARGET.CVB_CODE = USE_THIS.CVB_CODE, TARGET.CALC_BASIS = USE_THIS.CALC_BASIS,
TARGET.COST_BASIS = USE_THIS.COST_BASIS,TARGET.EXP_CATEGORY = USE_THIS.EXP_CATEGORY, TARGET.COMP_RATE = USE_THIS.COMP_RATE,
TARGET.COMP_LEVEL = USE_THIS.COMP_LEVEL,TARGET.DISPLAY_ORDER = USE_THIS.DISPLAY_ORDER, TARGET.ALWAYS_DEFAULT_IND = USE_THIS.ALWAYS_DEFAULT_IND,
TARGET.COMP_CURRENCY = USE_THIS.COMP_CURRENCY,TARGET.PER_COUNT = USE_THIS.PER_COUNT, TARGET.PER_COUNT_UOM = USE_THIS.PER_COUNT_UOM,
TARGET.NOM_FLAG_1 = USE_THIS.NOM_FLAG_1,TARGET.NOM_FLAG_2 = USE_THIS.NOM_FLAG_2, TARGET.NOM_FLAG_3 = USE_THIS.NOM_FLAG_3, TARGET.NOM_FLAG_4 = USE_THIS.NOM_FLAG_4,
TARGET.NOM_FLAG_5 = USE_THIS.NOM_FLAG_5, TARGET.SYS_GENERATED_IND = USE_THIS.SYS_GENERATED_IND
WHEN NOT MATCHED THEN INSERT (COMP_ID,COMP_DESC,COMP_TYPE,ASSESS_TYPE,IMPORT_COUNTRY_ID,EXPENSE_TYPE,UP_CHRG_TYPE,UP_CHRG_GROUP,CVB_CODE,CALC_BASIS,COST_BASIS,EXP_CATEGORY,COMP_RATE,
COMP_LEVEL,DISPLAY_ORDER,ALWAYS_DEFAULT_IND,COMP_CURRENCY,PER_COUNT,PER_COUNT_UOM,NOM_FLAG_1,NOM_FLAG_2,NOM_FLAG_3,NOM_FLAG_4,NOM_FLAG_5,SYS_GENERATED_IND)
VALUES (USE_THIS.COMP_ID,USE_THIS.COMP_DESC,USE_THIS.COMP_TYPE,USE_THIS.ASSESS_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.EXPENSE_TYPE,USE_THIS.UP_CHRG_TYPE,USE_THIS.UP_CHRG_GROUP,
USE_THIS.CVB_CODE,USE_THIS.CALC_BASIS,USE_THIS.COST_BASIS,USE_THIS.EXP_CATEGORY,USE_THIS.COMP_RATE,USE_THIS.COMP_LEVEL,USE_THIS.DISPLAY_ORDER,USE_THIS.ALWAYS_DEFAULT_IND,
USE_THIS.COMP_CURRENCY,USE_THIS.PER_COUNT,USE_THIS.PER_COUNT_UOM,USE_THIS.NOM_FLAG_1,USE_THIS.NOM_FLAG_2,USE_THIS.NOM_FLAG_3,USE_THIS.NOM_FLAG_4,USE_THIS.NOM_FLAG_5,USE_THIS.SYS_GENERATED_IND);
------------------------------------------------------------------------
commit;
