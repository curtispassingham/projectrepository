
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
set serveroutput on

spool off
spool sys_update_oms_ind.log replace

------------------------------------------------------------------------------------
-- Copyright (c) 2006, Oracle Retail.  All rights reserved
-- $Workfile:  $
-- $Revision: 1.0 $
-- $Modtime:   $
------------------------------------------------------------------------------------

REM --------------------------------------------------------------------------------
REM
REM  This script updates PRODUCT_CONFIG_OPTIONS table.
REM  This script is used for system installation, and  for every patch update to
REM  MOM product installed in the system. It accepts 1 valid values as user input:
REM  OMS ind.  Entering invalid
REM  values will terminate the script.
REM
REM --------------------------------------------------------------------------------

prompt 'Starting File: sys_update_oms_ind.sql';
prompt 'Enter if any Order Management System is installed in the system (Y/N):'
define L_oms_ind = &1_oms_ind;

DECLARE

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_oms_ind         VARCHAR2(1)             := '&L_oms_ind';

BEGIN

   if L_oms_ind IS NOT NULL then
      update product_config_options 
         set oms_ind = L_oms_ind;
   end if;

   commit;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'sys_update_oms_ind.sql',
                                            to_char(SQLCODE));
      dbms_output.put_line(L_error_message);
      return;
END;
/

undefine L_oms_ind;

set serveroutput off

spool off

