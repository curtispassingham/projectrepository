DECLARE
   
   L_te_queue_table          VARCHAR2(100) := USER||'.tax_event_qtab';
   L_te_queue_payload_type   VARCHAR2(100) := USER||'.tax_event_msg';
   L_te_queue_count          NUMBER        := 0;
   L_te_queue_table_count    NUMBER        := 0;
   L_te_subscriber_count     NUMBER        := 0;
   L_te_queue_name           VARCHAR2(50)  := USER||'.tax_event_queue';
   L_te_recepient            VARCHAR2(50)  := USER||'.tax_event_queue:ce_recipient';
   L_te_event                VARCHAR2(100) := 'plsql://'||USER||'.notify_tax_event';
   
   cursor C_CE_QUEUE_EXISTS is
      select count(1) 
        from dba_queues
       where owner = user
         and name = 'TAX_EVENT_QUEUE';
         
   cursor C_CE_QUEUE_TABLE_EXISTS is
      select count(1)
        from dba_queue_tables
       where owner = user
         and queue_table = 'TAX_EVENT_QTAB';

   cursor C_CE_SUBSCRIBER_EXISTS is
      select count(1)
        from dba_queue_subscribers  
       where owner = user
         and queue_name = 'TAX_EVENT_QUEUE';

BEGIN

   open C_CE_SUBSCRIBER_EXISTS;
   fetch C_CE_SUBSCRIBER_EXISTS into L_te_subscriber_count;
   close C_CE_SUBSCRIBER_EXISTS;
      
   if L_te_subscriber_count > 0 then
      DBMS_AQADM.REMOVE_SUBSCRIBER
         (QUEUE_NAME => L_te_queue_name,
          SUBSCRIBER => sys.aq$_agent('ce_recipient',null,null));
   end if;

   open C_CE_QUEUE_EXISTS;
   fetch C_CE_QUEUE_EXISTS into L_te_queue_count;
   close C_CE_QUEUE_EXISTS;
         
   if L_te_queue_count > 0 then
      DBMS_AQADM.STOP_QUEUE
         (QUEUE_NAME => 'TAX_EVENT_QUEUE');
         
      DBMS_AQADM.DROP_QUEUE
         (QUEUE_NAME => 'TAX_EVENT_QUEUE');
   end if;
   
   open C_CE_QUEUE_TABLE_EXISTS;
   fetch C_CE_QUEUE_TABLE_EXISTS into L_te_queue_table_count;
   close C_CE_QUEUE_TABLE_EXISTS;
      
   if L_te_queue_table_count > 0 then
      DBMS_AQADM.DROP_QUEUE_TABLE
         (QUEUE_TABLE => 'TAX_EVENT_QTAB', FORCE => TRUE);
   end if;
   
   DBMS_AQADM.CREATE_QUEUE_TABLE
      (QUEUE_TABLE => L_te_queue_table,
       QUEUE_PAYLOAD_TYPE =>  L_te_queue_payload_type,
       MULTIPLE_CONSUMERS => TRUE);
      
   DBMS_AQADM.CREATE_QUEUE
      (QUEUE_NAME => 'tax_event_queue',
       QUEUE_TABLE => L_te_queue_table);
     
   DBMS_AQADM.START_QUEUE
      (QUEUE_NAME => 'tax_event_queue');
      
   DBMS_AQADM.ADD_SUBSCRIBER
      (QUEUE_NAME => L_te_queue_name,
       SUBSCRIBER => sys.aq$_agent('ce_recipient',null,null));
        
   DBMS_AQ.REGISTER
      (sys.aq$_reg_info_list(
         sys.aq$_reg_info(L_te_recepient,
                          DBMS_AQ.NAMESPACE_AQ,
                          L_te_event,
                          HEXTORAW('FF'))),
                          --null)),
       1);
       
end;
/
