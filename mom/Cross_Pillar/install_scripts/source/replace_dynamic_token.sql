SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
set serveroutput on;

REM -- This script replaces the dynamic hierarchy token in code_detail, code_detail_tl, rtk_errors and rtk_errors_tl table. 
REM -- This script should be called during the installation and patch after the control scripts have been executed. This script
REM -- requries the above tables to be populated first with the tokens so that the tokens can be replaced with the token to 
REM -- string mapping maintained in dynamic_hier_token_map and dynamic_hier_token_map_tl table. 

DECLARE
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   if DYNAMIC_HIER_TOKEN_SQL.REPLACE_TOKENS(L_error_message) = false then
      rollback;
      raise_application_error(-20001, L_error_message);
   end if;
   commit;
END;
/
