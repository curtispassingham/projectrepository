
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------------
-- Copyright (c) 2007, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.1 $
-- $Modtime$
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--      ATTENTION: This script DOES preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
-------------------------------------------------------------------------------------
--      TABLE UPDATED: SA_TRAN_TENDER, SA_TRAN_TENDER_REV
-------------------------------------------------------------------------------------
PROMPT Applying CC Security FGA policy to table SA_TRAN_TENDER, SA_TRAN_TENDER_REV
BEGIN
   dbms_fga.add_policy (object_schema=>user,
                        object_name=>'SA_TRAN_TENDER',
                        audit_column=>'CC_NO,CC_EXP_DATE',
                        statement_types=>'INSERT,UPDATE,SELECT',
                        policy_name=>'SA_TRAN_TENDER_CC_SEC_FGA');

   dbms_fga.add_policy (object_schema=>user,
                        object_name=>'SA_TRAN_TENDER_REV',
                        audit_column=>'CC_NO,CC_EXP_DATE',
                        statement_types=>'INSERT,UPDATE,SELECT',
                        policy_name=>'SA_TRAN_TENDER_REV_CC_SEC_FGA');

END;
/
