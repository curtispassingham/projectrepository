/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Miguel Duarte                                                */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Base constraint change RD448                           	  */
/******************************************************************************/

PROMPT DISABLE CONSTRAINT 'CHK_SUPS_DBT_MEMO_CODE';
alter table SUPS
  disable constraint CHK_SUPS_DBT_MEMO_CODE
/

PROMPT CREATE CONSTRAINT 'CHK_SUPS_DBT_MEMO_CODE';
alter table SUPS
  add constraint XXADEO_CHK_SUPS_DBT_MEMO_CODE
  check (DBT_MEMO_CODE in ('Y','L','N','I'))
/