/******************************************************************************/
/* CREATE DATE - October 2018                                                 */
/* CREATE USER - Miguel Duarte                                                */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - Column type change from default(byte) to char            	  */
/******************************************************************************/

PROMPT ALTER COLUMN 'ITEM_MASTER_TL.ITEM_DESC_SECONDARY';
alter table ITEM_MASTER_TL
  modify (ITEM_DESC_SECONDARY VARCHAR2(250 CHAR));
/