--CUSTOM_RIB_OBJ_REC

DROP TYPE CUSTOM_RIB_OBJ_REC FORCE
/

CREATE OR REPLACE TYPE CUSTOM_RIB_OBJ_REC UNDER CUSTOM_OBJ_REC
(
 RIB_MSG             RIB_OBJECT,
 RIB_MSG_TYPE        VARCHAR2(255),
 RIB_OTBDESC_REC     RIB_OBJECT,
 STATUS_CODE         VARCHAR2(255),
 CONSTRUCTOR FUNCTION CUSTOM_RIB_OBJ_REC
 (
  FUNCTION_KEY                  VARCHAR2    DEFAULT NULL,
  CALL_SEQ_NO                   VARCHAR2    DEFAULT NULL,
  DOC_TYPE                      VARCHAR2    DEFAULT NULL,
  DOC_ID                        NUMBER      DEFAULT NULL,
  SOURCE_ENTITY                 VARCHAR2    DEFAULT NULL,
  SOURCE_TYPE                   VARCHAR2    DEFAULT NULL,
  SOURCE_ID                     VARCHAR2    DEFAULT NULL,
  DEST_ENTITY                   VARCHAR2    DEFAULT NULL,
  DEST_TYPE                     VARCHAR2    DEFAULT NULL,
  DEST_ID                       VARCHAR2    DEFAULT NULL,
  DEPT                          NUMBER      DEFAULT NULL,
  CLASS                         NUMBER      DEFAULT NULL,
  SUBCLASS                      NUMBER      DEFAULT NULL,
  ITEM                          VARCHAR2    DEFAULT NULL,
  SUPPLIER                      NUMBER      DEFAULT NULL,
  ORIGIN_COUNTRY_ID             VARCHAR2    DEFAULT NULL,
  LOC                           NUMBER      DEFAULT NULL,
  LOC_TYPE                      VARCHAR2    DEFAULT NULL,
  PARTNER                       NUMBER      DEFAULT NULL,
  PARTNER_TYPE                  VARCHAR2    DEFAULT NULL,
  ORDER_NO                      NUMBER      DEFAULT NULL,
  RIB_MSG                       RIB_OBJECT  DEFAULT NULL,
  RIB_MSG_TYPE                  VARCHAR2    DEFAULT NULL,
  RIB_OTBDESC_REC               RIB_OBJECT  DEFAULT NULL,
  STATUS_CODE                   VARCHAR2    DEFAULT NULL
 ) RETURN SELF AS RESULT
) NOT FINAL
/
CREATE OR REPLACE TYPE BODY CUSTOM_RIB_OBJ_REC AS
 CONSTRUCTOR FUNCTION CUSTOM_RIB_OBJ_REC
 (
      FUNCTION_KEY              VARCHAR2        DEFAULT NULL,
      CALL_SEQ_NO               VARCHAR2        DEFAULT NULL,
      DOC_TYPE                  VARCHAR2        DEFAULT NULL,
      DOC_ID                    NUMBER          DEFAULT NULL,
      SOURCE_ENTITY             VARCHAR2        DEFAULT NULL,
      SOURCE_TYPE               VARCHAR2        DEFAULT NULL,
      SOURCE_ID                 VARCHAR2        DEFAULT NULL,
      DEST_ENTITY               VARCHAR2        DEFAULT NULL,
      DEST_TYPE                 VARCHAR2        DEFAULT NULL,
      DEST_ID                   VARCHAR2        DEFAULT NULL,
      DEPT                      NUMBER          DEFAULT NULL,
      CLASS                     NUMBER          DEFAULT NULL,
      SUBCLASS                  NUMBER          DEFAULT NULL,
      ITEM                      VARCHAR2        DEFAULT NULL,
      SUPPLIER                  NUMBER          DEFAULT NULL,
      ORIGIN_COUNTRY_ID         VARCHAR2        DEFAULT NULL,
      LOC                       NUMBER          DEFAULT NULL,
      LOC_TYPE                  VARCHAR2        DEFAULT NULL,
      PARTNER                   NUMBER          DEFAULT NULL,
      PARTNER_TYPE              VARCHAR2        DEFAULT NULL,
      ORDER_NO                  NUMBER          DEFAULT NULL,
      RIB_MSG                   RIB_OBJECT      DEFAULT NULL,
      RIB_MSG_TYPE              VARCHAR2        DEFAULT NULL,
      RIB_OTBDESC_REC           RIB_OBJECT      DEFAULT NULL,
      STATUS_CODE               VARCHAR2        DEFAULT NULL
 ) RETURN SELF AS RESULT IS
    BEGIN
        SELF.FUNCTION_KEY := FUNCTION_KEY;
        SELF.CALL_SEQ_NO := CALL_SEQ_NO;
        SELF.DOC_TYPE := DOC_TYPE;
        SELF.DOC_ID := DOC_ID;
        SELF.SOURCE_ENTITY := SOURCE_ENTITY;
        SELF.SOURCE_TYPE := SOURCE_TYPE;
        SELF.SOURCE_ID := SOURCE_ID;
        SELF.DEST_ENTITY := DEST_ENTITY;
        SELF.DEST_TYPE := DEST_TYPE;
        SELF.DEST_ID := DEST_ID;
        SELF.DEPT := DEPT;
        SELF.CLASS := CLASS;
        SELF.SUBCLASS := SUBCLASS;
        SELF.ITEM := ITEM;
        SELF.SUPPLIER := SUPPLIER;
        SELF.ORIGIN_COUNTRY_ID := ORIGIN_COUNTRY_ID;
        SELF.LOC := LOC;
        SELF.LOC_TYPE := LOC_TYPE;
        SELF.PARTNER := PARTNER;
        SELF.PARTNER_TYPE := PARTNER_TYPE;
        SELF.ORDER_NO := ORDER_NO;
        SELF.RIB_MSG := RIB_MSG;
        SELF.RIB_MSG_TYPE := RIB_MSG_TYPE;
        SELF.RIB_OTBDESC_REC := RIB_OTBDESC_REC;
        SELF.STATUS_CODE := STATUS_CODE;
        RETURN;
    END;
END;
/