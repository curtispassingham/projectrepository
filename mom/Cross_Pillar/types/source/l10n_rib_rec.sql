
--L10N_RIB_REC

DROP type L10N_RIB_REC FORCE
/

CREATE OR REPLACE TYPE L10N_RIB_REC UNDER L10N_OBJ
(
 RIB_MSG             RIB_OBJECT,
 RIB_MSG_TYPE        VARCHAR2(255),
 RIB_OTBDESC_REC     RIB_OBJECT,
 RIB_ERR_TBL         RIB_ERROR_TBL,
 STATUS_CODE         VARCHAR2(255),
 CONSTRUCTOR FUNCTION L10N_RIB_REC 
 (
    PROCEDURE_KEY                 VARCHAR2        DEFAULT NULL,
    COUNTRY_ID                    VARCHAR2        DEFAULT NULL,
    DOC_TYPE                      VARCHAR2        DEFAULT NULL,
    DOC_ID                        NUMBER          DEFAULT NULL,
    SOURCE_ENTITY                 VARCHAR2        DEFAULT NULL,
    SOURCE_TYPE                   VARCHAR2        DEFAULT NULL,
    SOURCE_ID                     VARCHAR2        DEFAULT NULL,
    DEST_ENTITY                   VARCHAR2        DEFAULT NULL,
    DEST_TYPE                     VARCHAR2        DEFAULT NULL,
    DEST_ID                       VARCHAR2        DEFAULT NULL,
    DEPT                          NUMBER          DEFAULT NULL,
    CLASS                         NUMBER          DEFAULT NULL,
    SUBCLASS                      NUMBER          DEFAULT NULL,
    ITEM                          VARCHAR2        DEFAULT NULL,
    AV_COST                       NUMBER          DEFAULT NULL,
    UNIT_COST                     NUMBER          DEFAULT NULL,
    UNIT_RETAIL                   NUMBER          DEFAULT NULL,
    STOCK_ON_HAND                 NUMBER          DEFAULT NULL,
    RIB_MSG                       RIB_OBJECT      DEFAULT NULL,
    RIB_MSG_TYPE                  VARCHAR2        DEFAULT NULL,
    RIB_OTBDESC_REC               RIB_OBJECT      DEFAULT NULL,
    RIB_ERR_TBL                   RIB_ERROR_TBL   DEFAULT NULL,
    STATUS_CODE                   VARCHAR2        DEFAULT NULL
 ) RETURN SELF AS RESULT
) NOT FINAL
/
CREATE OR REPLACE TYPE BODY L10N_RIB_REC AS
 CONSTRUCTOR FUNCTION L10N_RIB_REC 
 (
      PROCEDURE_KEY             VARCHAR2        DEFAULT NULL,
      COUNTRY_ID                VARCHAR2        DEFAULT NULL,
      DOC_TYPE                  VARCHAR2        DEFAULT NULL,
      DOC_ID                    NUMBER          DEFAULT NULL,
      SOURCE_ENTITY             VARCHAR2        DEFAULT NULL,
      SOURCE_TYPE               VARCHAR2        DEFAULT NULL,
      SOURCE_ID                 VARCHAR2        DEFAULT NULL,
      DEST_ENTITY               VARCHAR2        DEFAULT NULL,
      DEST_TYPE                 VARCHAR2        DEFAULT NULL,
      DEST_ID                   VARCHAR2        DEFAULT NULL,
      DEPT                      NUMBER          DEFAULT NULL,
      CLASS                     NUMBER          DEFAULT NULL,
      SUBCLASS                  NUMBER          DEFAULT NULL,
      ITEM                      VARCHAR2        DEFAULT NULL,
      AV_COST                   NUMBER          DEFAULT NULL,
      UNIT_COST                 NUMBER          DEFAULT NULL,
      UNIT_RETAIL               NUMBER          DEFAULT NULL,
      STOCK_ON_HAND             NUMBER          DEFAULT NULL,
      RIB_MSG                   RIB_OBJECT      DEFAULT NULL,
      RIB_MSG_TYPE              VARCHAR2        DEFAULT NULL,
      RIB_OTBDESC_REC           RIB_OBJECT      DEFAULT NULL,
      RIB_ERR_TBL               RIB_ERROR_TBL   DEFAULT NULL,
      STATUS_CODE               VARCHAR2        DEFAULT NULL
 ) RETURN SELF AS RESULT IS
    BEGIN
        SELF.PROCEDURE_KEY := PROCEDURE_KEY;
        SELF.COUNTRY_ID := COUNTRY_ID;
        SELF.DOC_TYPE := DOC_TYPE;
        SELF.DOC_ID := DOC_ID;
        SELF.SOURCE_ENTITY := SOURCE_ENTITY;
        SELF.SOURCE_TYPE := SOURCE_TYPE;
        SELF.SOURCE_ID := SOURCE_ID;
        SELF.DEST_ENTITY := DEST_ENTITY;
        SELF.DEST_TYPE := DEST_TYPE;
        SELF.DEST_ID := DEST_ID;
        SELF.DEPT := DEPT;
        SELF.CLASS := CLASS;
        SELF.SUBCLASS := SUBCLASS;
        SELF.ITEM := ITEM;
        SELF.AV_COST := AV_COST;
        SELF.UNIT_COST := UNIT_COST;
        SELF.UNIT_RETAIL := UNIT_RETAIL;
        SELF.STOCK_ON_HAND := STOCK_ON_HAND;
        SELF.RIB_MSG := RIB_MSG;
        SELF.RIB_MSG_TYPE := RIB_MSG_TYPE;
        SELF.RIB_OTBDESC_REC := RIB_OTBDESC_REC;
        SELF.RIB_ERR_TBL := RIB_ERR_TBL;
        SELF.STATUS_CODE := STATUS_CODE;
        RETURN;
    END;  
END;
/
