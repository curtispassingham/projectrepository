DROP TYPE s9t_names_tab FORCE
/
DROP TYPE s9t_name FORCE
/
CREATE OR REPLACE type s9t_name AUTHID CURRENT_USER
AS
  object
  (
    t_name VARCHAR2(255),
    t_pos  NUMBER(5) )
/
CREATE OR REPLACE type s9t_names_tab
AS
  TABLE OF s9t_name
/