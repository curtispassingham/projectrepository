
--L10N_OBJ

DROP type L10N_OBJ FORCE
/
DROP type TABLE_TBL FORCE
/
DROP type TABLE_REC FORCE
/

CREATE TYPE TABLE_REC AS OBJECT
(
   TABLE_NAME           VARCHAR2(30)
)
/
CREATE TYPE TABLE_TBL AS TABLE OF TABLE_REC
/

CREATE TYPE L10N_OBJ AS OBJECT
(PROCEDURE_KEY                 VARCHAR2 (30),
 COUNTRY_ID                    VARCHAR2 (3),
 DOC_TYPE                      VARCHAR2 (6),
 DOC_ID                        NUMBER (12),
 SOURCE_ENTITY                 VARCHAR2 (6),
 SOURCE_TYPE                   VARCHAR2 (6),
 SOURCE_ID                     VARCHAR2 (10),
 DEST_ENTITY                   VARCHAR2 (6),
 DEST_TYPE                     VARCHAR2 (6),
 DEST_ID                       VARCHAR2 (10),
 DEPT                          NUMBER (4),
 CLASS                         NUMBER (4),
 SUBCLASS                      NUMBER (4),
 ITEM                          VARCHAR2 (25),
 AV_COST                       NUMBER (20, 4),
 UNIT_COST                     NUMBER (20, 4),
 UNIT_RETAIL                   NUMBER (20, 4),
 STOCK_ON_HAND                 NUMBER (12, 4),
 PROCESS_ID                    NUMBER (15),
 CHUNK_ID                      NUMBER (15),
 REFERENCE_ID                  NUMBER (15),
 TABLE_NAMES                   TABLE_TBL,
 CONSTRUCTOR FUNCTION L10N_OBJ 
 (
  PROCEDURE_KEY                 VARCHAR2 DEFAULT NULL,
  COUNTRY_ID                    VARCHAR2 DEFAULT NULL,
  DOC_TYPE                      VARCHAR2 DEFAULT NULL,
  DOC_ID                        NUMBER DEFAULT NULL,
  SOURCE_ENTITY                 VARCHAR2 DEFAULT NULL,
  SOURCE_TYPE                   VARCHAR2 DEFAULT NULL,
  SOURCE_ID                     VARCHAR2 DEFAULT NULL,
  DEST_ENTITY                   VARCHAR2 DEFAULT NULL,
  DEST_TYPE                     VARCHAR2 DEFAULT NULL,
  DEST_ID                       VARCHAR2 DEFAULT NULL,
  DEPT                          NUMBER DEFAULT NULL,
  CLASS                         NUMBER DEFAULT NULL,
  SUBCLASS                      NUMBER DEFAULT NULL,
  ITEM                          VARCHAR2 DEFAULT NULL,
  AV_COST                       NUMBER DEFAULT NULL,
  UNIT_COST                     NUMBER DEFAULT NULL,
  UNIT_RETAIL                   NUMBER DEFAULT NULL,
  STOCK_ON_HAND                 NUMBER DEFAULT NULL,
  PROCESS_ID                    NUMBER DEFAULT NULL,
  CHUNK_ID                      NUMBER DEFAULT NULL,
  REFERENCE_ID                  NUMBER DEFAULT NULL,
  TABLE_NAMES                   TABLE_TBL DEFAULT NULL
   ) RETURN SELF AS RESULT
) NOT FINAL
/
CREATE TYPE BODY L10N_OBJ AS
 CONSTRUCTOR FUNCTION L10N_OBJ 
 (
  PROCEDURE_KEY                 VARCHAR2 DEFAULT NULL,
  COUNTRY_ID                    VARCHAR2 DEFAULT NULL,
  DOC_TYPE                      VARCHAR2 DEFAULT NULL,
  DOC_ID                        NUMBER   DEFAULT NULL,
  SOURCE_ENTITY                 VARCHAR2 DEFAULT NULL,
  SOURCE_TYPE                   VARCHAR2 DEFAULT NULL,
  SOURCE_ID                     VARCHAR2 DEFAULT NULL,
  DEST_ENTITY                   VARCHAR2 DEFAULT NULL,
  DEST_TYPE                     VARCHAR2 DEFAULT NULL,
  DEST_ID                       VARCHAR2 DEFAULT NULL,
  DEPT                          NUMBER DEFAULT NULL,
  CLASS                         NUMBER DEFAULT NULL,
  SUBCLASS                      NUMBER DEFAULT NULL,
  ITEM                          VARCHAR2 DEFAULT NULL,
  AV_COST                       NUMBER DEFAULT NULL,
  UNIT_COST                     NUMBER DEFAULT NULL,
  UNIT_RETAIL                   NUMBER DEFAULT NULL,
  STOCK_ON_HAND                 NUMBER DEFAULT NULL,
  PROCESS_ID                    NUMBER DEFAULT NULL,
  CHUNK_ID                      NUMBER DEFAULT NULL,
  REFERENCE_ID                  NUMBER DEFAULT NULL,
  TABLE_NAMES                   TABLE_TBL DEFAULT NULL
  ) RETURN SELF AS RESULT IS
    BEGIN
        SELF.PROCEDURE_KEY := PROCEDURE_KEY;
        SELF.COUNTRY_ID := COUNTRY_ID;
        SELF.DOC_TYPE := DOC_TYPE;
        SELF.DOC_ID := DOC_ID;
        SELF.SOURCE_ENTITY := SOURCE_ENTITY;
        SELF.SOURCE_TYPE := SOURCE_TYPE;
        SELF.SOURCE_ID := SOURCE_ID;
        SELF.DEST_ENTITY := DEST_ENTITY;
        SELF.DEST_TYPE := DEST_TYPE;
        SELF.DEST_ID := DEST_ID;
        SELF.DEPT := DEPT;
        SELF.CLASS := CLASS;
        SELF.SUBCLASS := SUBCLASS;
        SELF.ITEM := ITEM;
        SELF.AV_COST := AV_COST;
        SELF.UNIT_COST := UNIT_COST;
        SELF.UNIT_RETAIL := UNIT_RETAIL;
        SELF.STOCK_ON_HAND := STOCK_ON_HAND;
        SELF.PROCESS_ID := PROCESS_ID;
        SELF.CHUNK_ID := CHUNK_ID;
        SELF.REFERENCE_ID := REFERENCE_ID;
        SELF.TABLE_NAMES := TABLE_NAMES;
        RETURN;
    END;  
END;
/