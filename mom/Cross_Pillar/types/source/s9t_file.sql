SET DEFINE OFF
CREATE OR REPLACE type body s9t_file
IS
  constructor
FUNCTION s9t_file
  RETURN Self
AS
  result
IS
BEGIN
  self.file_name   := NULL;
  self.sheet_names := s9t_cells();
  self.sheets      := s9t_sheet_tab();
  RETURN;
END;
constructor
FUNCTION s9t_file
  (
    I_file_ID IN NUMBER,
    I_load    IN BOOLEAN DEFAULT FALSE
  )
  RETURN Self
AS
  result
IS
  l_clob CLOB;
  myParser DBMS_XMLPARSER.parser;
  indomdoc DBMS_XMLDOM.DOMDocument;
  worksheets DBMS_XMLDOM.DOMNODELIST;
  worksheet_count NUMBER;
  worksheet_atts DBMS_XMLDOM.DOMNAMEDNODEMAP;
  worksheet_name VARCHAR2(255);
  sheet DBMS_XMLDOM.DOMNODE;
  data_tag_list DBMS_XMLDOM.DOMNODELIST;
  --wtable DBMS_XMLDOM.DOMNODE;
  drows DBMS_XMLDOM.DOMNODELIST;
  drow DBMS_XMLDOM.DOMNODE;
  cells DBMS_XMLDOM.DOMNODELIST;
  cell DBMS_XMLDOM.DOMNODE;
  data_tag DBMS_XMLDOM.DOMNODE;
  data_val VARCHAR2(4000);
  --l_sheet workbook_sheet;
  l_row s9t_cells;
  l_nodelist dbms_xmldom.DOMNodeList;
  buf               VARCHAR2(32000);
  l_count           NUMBER := 0;
  cell_repeat_count NUMBER;
  row_repeat_count  NUMBER;
  l_blob BLOB;
  l_column_count NUMBER;
BEGIN
  IF I_load = FALSE THEN
    SELECT s9t_file_obj
    INTO self
    FROM s9t_folder
    WHERE file_ID = I_file_ID;
    RETURN;
  END IF;
  SELECT ods_blob,
    file_name,
    template_key,
    user_lang
  INTO l_blob,
    self.file_name,
    self.template_key,
    self.user_lang
  FROM s9t_folder
  WHERE file_id = I_file_id;
  dbms_lob.createtemporary(l_clob,false);
  zip.unzip(l_blob,l_clob);
  self.file_id     := I_file_id;
  self.sheet_names := s9t_cells();
  self.sheets      := s9t_sheet_tab();
  /*  SELECT file_clob
  INTO l_clob
  FROM s9t_folder
  WHERE file_name = I_file_name
  And create_id = USER;*/
  myParser := DBMS_XMLPARSER.newParser;
  DBMS_XMLPARSER.parseClob(myParser, l_clob);
  indomdoc        := DBMS_XMLPARSER.getDocument(myParser);
  worksheets      := dbms_xmldom.getelementsbytagname(indomdoc,'table');
  worksheet_count := dbms_xmldom.getlength(worksheets);
  FOR i IN 1..worksheet_count
  LOOP
    sheet          := dbms_xmldom.item(Worksheets,i-1);
    worksheet_atts := dbms_xmldom.getattributes(sheet);
    worksheet_name := DBMS_XMLDOM.GETATTRIBUTE( DBMS_XMLDOM.MAKEELEMENT(sheet),'name' );
    add_sheet(worksheet_name);
    drows          := dbms_xmldom.getchildrenbytagname(DBMS_XMLDOM.MAKEELEMENT(sheet),'table-row');
    l_column_count := 0;
    FOR j IN 1..dbms_xmldom.getlength(drows)
    LOOP
      l_row               := NULL;
      l_row               :=NEW s9t_cells();
      drow                := dbms_xmldom.item(drows,j-1);
      cells               := dbms_xmldom.getchildrenbytagname(DBMS_XMLDOM.MAKEELEMENT(drow),'table-cell');
      row_repeat_count    := DBMS_XMLDOM.GETATTRIBUTE( DBMS_XMLDOM.MAKEELEMENT(drow),'number-rows-repeated' );
      IF row_repeat_count IS NOT NULL THEN
        EXIT;
      END IF;
      FOR k IN 1..dbms_xmldom.getlength(cells)
      LOOP
        cell              := dbms_xmldom.item(cells,k-1);
        cell_repeat_count := NULL;
        cell_repeat_count := DBMS_XMLDOM.GETATTRIBUTE( DBMS_XMLDOM.MAKEELEMENT(cell),'number-columns-repeated' );
        data_tag_list     := dbms_xmldom.getchildrenbytagname(DBMS_XMLDOM.MAKEELEMENT(cell),'p');
        data_tag          := dbms_xmldom.getfirstchild(dbms_xmldom.item(data_tag_list,0));
        data_val          := dbms_xmldom.getnodevalue(data_tag);
        -- For first row, exit at the first null cell.
        -- for non-header rows, exit if end of columns are reached.
        IF ((j=1 AND data_val IS NULL) OR (j>1 AND l_row.count() > l_column_count)) THEN
          EXIT;
        END IF;
        FOR rc IN 1..greatest(1,NVL(cell_repeat_count,0))
        LOOP
          -- for non-header rows, exit if end of columns are reached.
          IF (j>1 AND l_row.count() > l_column_count) THEN
            EXIT;
          END IF;
          l_row.EXTEND;
          l_row(l_row.count()) := data_val;
          l_count              := l_count + 1;
        END LOOP;
      END LOOP;
      IF l_row.count()                                            > 0 THEN
        IF j                                                      = 1 THEN
          sheets(get_sheet_index(worksheet_name)).column_headers := l_row;
          l_column_count                                         := l_row.count();
        ELSE
          sheets(get_sheet_index(worksheet_name)).add_row(l_row);
        END IF;
      END IF;
    END LOOP;
  END LOOP;
  DBMS_XMLDOM.freeDocument(indomdoc);
  DBMS_XMLPARSER.freeParser(myParser);
  RETURN;
END;
constructor
FUNCTION s9t_file(
    I_file_name IN VARCHAR2,
    I_load      IN BOOLEAN DEFAULT FALSE )
  RETURN Self
AS
  result
IS
  l_file_id NUMBER;
BEGIN
  SELECT file_id
  INTO l_file_id
  FROM s9t_folder
  WHERE file_name = I_file_name
  AND create_id   = USER;
  self           := s9t_file(l_file_id,I_load);
  RETURN;
END;
member FUNCTION get_sheet(
    I_sheet_name IN VARCHAR2)
  RETURN s9t_sheet
IS
BEGIN
  FOR i IN sheets.FIRST .. sheets.LAST
  LOOP
    IF sheets(i).sheet_name = I_sheet_name THEN
      RETURN sheets(i);
    END IF;
  END LOOP;
  RETURN NULL;
END;
member FUNCTION get_file
  RETURN CLOB
IS
  l_wbk1 VARCHAR2(4000):=
  '<?xml version="1.0" encoding="UTF-8" ?>
<office:document-content xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:ooo="http://openoffice.org/2004/office" xmlns:ooow="http://openoffice.org/2004/writer" xmlns:oooc="http://openoffice.org/2004/calc" xmlns:dom="http://www.w3.org/2001/xml-events" xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:rpt="http://openoffice.org/2005/report" xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:grddl="http://www.w3.org/2003/g/data-view#" xmlns:field="urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0" office:version="1.2" grddl:transformation="http://docs.oasis-open.org/office/1.2/xslt/odf2rdf.xsl">
<office:scripts />
<office:font-face-decls>
<style:font-face style:name="Arial" svg:font-family="Arial" style:font-family-generic="swiss" style:font-pitch="variable" />
<style:font-face style:name="Arial Unicode MS" svg:font-family="Arial Unicode MS" style:font-family-generic="system" style:font-pitch="variable" />
<style:font-face style:name="FZSongTi" svg:font-family="FZSongTi" style:font-family-generic="system" style:font-pitch="variable" />
<style:font-face style:name="Mangal" svg:font-family="Mangal" style:font-family-generic="system" style:font-pitch="variable" />
<style:font-face style:name="Tahoma" svg:font-family="Tahoma" style:font-family-generic="system" style:font-pitch="variable" />
</office:font-face-decls>
<office:automatic-styles>
<style:style style:name="co1" style:family="table-column">
<style:table-column-properties fo:break-before="auto" style:column-width="0.8925in" />
</style:style>
<style:style style:name="ro1" style:family="table-row">
<style:table-row-properties style:row-height="0.1783in" fo:break-before="auto" style:use-optimal-row-height="true" />
</style:style>
<style:style style:name="ta1" style:family="table" style:master-page-name="Default">
<style:table-properties table:display="true" style:writing-mode="lr-tb" />
</style:style>
<style:style style:name="ce1" style:family="table-cell" style:parent-style-name="Default">
<style:table-cell-properties fo:background-color="#e6e6e6" fo:border="0.0008in solid #000000" />
<style:text-properties fo:font-weight="bold" style:font-weight-asian="bold" style:font-weight-complex="bold" />
</style:style>
<style:style style:name="ce2" style:family="table-cell" style:parent-style-name="Default">
<style:table-cell-properties fo:border="0.0008in solid #000000" />
</style:style>
<style:style style:name="ta_extref" style:family="table">
<style:table-properties table:display="false" />
</style:style>
</office:automatic-styles>
<office:body>
<office:spreadsheet> '
  ;
  l_sheet1     VARCHAR2(4000) := '
<table:table table:name="Sheet1" table:style-name="ta1" table:print="false">
<office:forms form:automatic-focus="false" form:apply-design-mode="false" />
<table:table-column table:style-name="co1" table:number-columns-repeated="COLUMN_COUNT" table:default-cell-style-name="ce2" /> ';
  l_row1       VARCHAR2(4000) := '
<table:table-row table:style-name="ro1">
';
  l_head_cell1 VARCHAR2(4000) :='
<table:table-cell table:style-name="ce1" office:value-type="string">
<text:p>';
  l_head_cell2 VARCHAR2(4000) :='</text:p></table:table-cell>';
  l_row2       VARCHAR2(4000) :='</table:table-row>';
  l_cell1      VARCHAR2(4000) :='
<table:table-cell office:value-type="string"><text:p>';
  l_cell2      VARCHAR2(4000) :='</text:p></table:table-cell>';
  l_cell_lov1a VARCHAR2(4000) :='
<table:table-cell table:content-validation-name="';
  l_cell_lov1b VARCHAR2(4000) := '" office:value-type="string"><text:p>';
  l_sheet2     VARCHAR2(4000) := '
</table:table>';
  l_wbk2       VARCHAR2(4000) := '
</office:spreadsheet>
</office:body>
</office:document-content>';
  l_clob CLOB;
  l_str           VARCHAR2(32000);
  is_content_vals BOOLEAN;
  l_lov           VARCHAR2(4000);
type list_val_arr_typ
IS
  TABLE OF VARCHAR2(255) INDEX BY binary_integer;
  l_list_val_arr list_val_arr_typ;
BEGIN
  l_clob :=to_clob(l_wbk1);
  l_list_val_arr.delete;
  is_content_vals := false;
  -- Write content validations tag
  FOR sh IN 1..sheets.count()
  LOOP
    FOR lv IN 1..sheets(sh).s9t_list_vals.count()
    LOOP
      IF NOT is_content_vals THEN
        l_str := '<table:content-validations>';
        dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
        is_content_vals := true;
      END IF;
      l_str := '<table:content-validation table:name="s'||sh||'val'||lv||'" table:condition="of:cell-content-is-in-list(';
      SELECT listagg('&quot;'
        ||column_value
        ||'&quot;',';') within GROUP (
      ORDER BY 1)
      INTO l_lov
      FROM TABLE(sheets(sh).s9t_list_vals(lv).list_of_values);
      l_str := l_str ||l_lov ||')" table:allow-empty-cell="true" table:display-list="unsorted" table:base-cell-address="&apos;' ||sheets(sh).sheet_name ||'&apos;.' ||s9t_pkg.calc_col_head(sheets(sh).get_col_index(sheets(sh).s9t_list_vals(lv).column_name))||'1048576"><table:error-message table:message-type="stop" table:display="true" />  </table:content-validation>';
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    END LOOP;
  END LOOP;
  IF is_content_vals THEN
    l_str:='</table:content-validations>';
    dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
  END IF;
  --finished writing content validations tag
  --Write data for each sheet
  FOR s IN 1..sheets.count()
  LOOP
    -- Initialize list validation array for thi sheet
    l_list_val_arr.delete;
    FOR lv IN 1..sheets(s).s9t_list_vals.count()
    LOOP
      l_list_val_arr(sheets(s).get_col_index(sheets(s).s9t_list_vals(lv).column_name)):= 's'||s||'val'||lv;
    END LOOP;
    l_str := REPLACE(l_sheet1,'Sheet1',sheets(s).sheet_name);
    l_str := REPLACE(l_str,'COLUMN_COUNT',sheets(s).column_headers.count());
    dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    l_str := l_row1;
    dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    FOR h IN 1..sheets(s).column_headers.count()
    LOOP
      l_str := l_head_cell1||sheets(s).column_headers(h)||l_head_cell2;
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    END LOOP;
    l_str := l_row2;
    dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    FOR r IN 1..sheets(s).s9t_rows.count()
    LOOP
      l_str := l_row1;
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      FOR d IN 1..sheets(s).s9t_rows(r).cells.count()
      LOOP
        IF l_list_val_arr.exists(d) THEN
          l_str:=l_cell_lov1a||l_list_val_arr(d)||l_cell_lov1b||DBMS_XMLGEN.CONVERT(sheets(s).s9t_rows(r).cells(d),0)||l_cell2;
        ELSE
          l_str:=l_cell1||DBMS_XMLGEN.CONVERT(sheets(s).s9t_rows(r).cells(d),0)||l_cell2;
        END IF;
        dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      END LOOP;
      l_str := l_row2;
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    END LOOP;
    IF l_list_val_arr.count() > 0 THEN
      l_str                  :='<table:table-row table:style-name="ro1" table:number-rows-repeated="'||(1048574-sheets(s).s9t_rows.count())||'">';
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      FOR col IN 1..sheets(s).column_headers.count()
      LOOP
        l_str := '<table:table-cell ';
        IF l_list_val_arr.exists(col) THEN
          l_str := l_str || 'table:content-validation-name="'||l_list_val_arr(col)||'" /> ';
        ELSE
          l_str := l_str || '/> ';
        END IF;
        dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      END LOOP;
      l_str :='</table:table-row>';
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      l_str :='<table:table-row table:style-name="ro1" >';
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      FOR col IN 1..sheets(s).column_headers.count()
      LOOP
        l_str := '<table:table-cell ';
        IF l_list_val_arr.exists(col) THEN
          l_str := l_str || 'table:content-validation-name="'||l_list_val_arr(col)||'" /> ';
        ELSE
          l_str := l_str || '/> ';
        END IF;
        dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      END LOOP;
      l_str :='</table:table-row>';
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    END IF;
    l_str := l_sheet2;
    dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
  END LOOP;
  l_str := l_wbk2;
  dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
  RETURN l_clob;

END;
member PROCEDURE add_sheet(
    I_sheet_name IN VARCHAR2)
IS
BEGIN
  sheet_names.extend();
  sheet_names(sheet_names.count()):= I_sheet_name;
  sheets.extend();
  sheets(sheets.count()):=s9t_sheet(I_sheet_name,s9t_cells(NULL),s9t_row_tab(),s9t_list_val_tab());
END;
member FUNCTION get_sheet_index(
    I_sheet_name IN VARCHAR2)
  RETURN NUMBER
IS
BEGIN
  FOR s IN sheets.first .. sheets.last
  LOOP
    IF sheets(s).sheet_name = I_sheet_name THEN
      RETURN s;
    END IF;
  END LOOP;
  RETURN NULL;
END;
member PROCEDURE add_sheet_for_sql(
    I_sheet_name IN VARCHAR2,
    I_query      IN VARCHAR2)
IS
  l_sheet s9t_sheet := s9t_sheet(I_sheet_name,s9t_cells(),s9t_row_tab(),s9t_list_val_tab());
  l_cells s9t_cells;
  c1      INTEGER;
  col_cnt INTEGER;
  rec_tab DBMS_SQL.DESC_TAB;
  col_num       NUMBER;
  d             NUMBER;
  col_val       VARCHAR2(4000);
  col_varchar   VARCHAR(4000);
  col_varchar2  VARCHAR2(4000);
  col_date      DATE;--
  col_timestamp TIMESTAMP;
  col_char      CHAR;
  col_number    NUMBER;
BEGIN
  c1 := dbms_sql.open_cursor;
  dbms_sql.parse(c1,I_query,dbms_sql.native);
  d := DBMS_SQL.EXECUTE(c1);
  DBMS_SQL.DESCRIBE_COLUMNS(c1, col_cnt, rec_tab);
  FOR j IN 1..col_cnt
  LOOP
    --dbms_output.put_line(' col name: '||rec_tab(j).col_name);
    l_sheet.add_column(rec_tab(j).col_name);
    IF rec_tab(j).col_type = dbms_types.typecode_number THEN
      dbms_sql.define_column(c1, j, col_number);
    END IF;
    IF rec_tab(j).col_type = dbms_types.typecode_char THEN
      dbms_sql.define_column(c1, j, col_char,1);
    END IF;
    IF rec_tab(j).col_type = dbms_types.typecode_varchar2 THEN
      dbms_sql.define_column(c1, j, col_varchar2,4000);
    END IF;
    IF rec_tab(j).col_type = dbms_types.typecode_varchar THEN
      dbms_sql.define_column(c1, j, col_varchar,4000);
    END IF;
    IF rec_tab(j).col_type = dbms_types.typecode_date THEN
      dbms_sql.define_column(c1, j, col_date);
    END IF;
    IF rec_tab(j).col_type = dbms_types.typecode_timestamp THEN
      dbms_sql.define_column(c1, j, col_timestamp);
    END IF;
  END LOOP;
  LOOP
    IF dbms_sql.fetch_rows(c1) > 0 THEN
      l_cells                 := s9t_cells();
      FOR j IN 1..col_cnt
      LOOP
        IF rec_tab(j).col_type = dbms_types.typecode_number THEN
          dbms_sql.column_value(c1, j, col_number);
          col_val := TO_CHAR(col_number);
        END IF;
        IF rec_tab(j).col_type = dbms_types.typecode_char THEN
          dbms_sql.column_value(c1, j, col_char);
          col_val := TO_CHAR(col_char);
        END IF;
        IF rec_tab(j).col_type = dbms_types.typecode_varchar2 THEN
          dbms_sql.column_value(c1, j, col_varchar2);
          col_val := TO_CHAR(col_varchar2);
        END IF;
        IF rec_tab(j).col_type = dbms_types.typecode_varchar THEN
          dbms_sql.column_value(c1, j, col_varchar);
          col_val := TO_CHAR(col_varchar);
        END IF;
        IF rec_tab(j).col_type = dbms_types.typecode_date THEN
          dbms_sql.column_value(c1, j, col_date);
          col_val := TO_CHAR(col_date);
        END IF;
        IF rec_tab(j).col_type = dbms_types.typecode_timestamp THEN
          dbms_sql.column_value(c1, j, col_timestamp);
          col_val := TO_CHAR(col_timestamp);
        END IF;
        l_cells.extend();
        l_cells(l_cells.count()) := col_val;
      END LOOP;
      l_sheet.add_row(l_cells);
    ELSE
      EXIT;
    END IF;
  END LOOP;
  dbms_sql.close_cursor(C1);
  add_sheet(I_sheet_name);
  sheets(get_sheet_index(I_sheet_name)):=l_sheet;
END;
member PROCEDURE SAVE
IS
BEGIN
  IF self.file_id IS NULL THEN
    self.file_id  := s9t_folder_seq.nextval();
  END IF;
  Merge INTO s9t_folder sf USING
  ( SELECT self.file_id AS file_id FROM dual
  ) sq ON (sq.file_id = sf.file_id)
WHEN Matched THEN
  UPDATE SET s9t_file_obj = self WHEN NOT matched THEN
  INSERT
    (
      file_id,
      file_name,
      template_key,
      user_lang,
      s9t_file_obj
    )
    VALUES
    (
      self.file_id,
      self.file_name,
      self.template_key,
      self.user_lang,
      self
    );
  self.UPDATE_ODS;
END;
member PROCEDURE UPDATE_ODS
IS
  l_clob CLOB;
  l_blob BLOB;
  l_lblob BLOB;
  O_blob BLOB;
  l_tmpl_blob BLOB;
BEGIN
  dbms_lob.createtemporary(l_clob,false);
  l_clob := self.get_file();
  dbms_lob.createtemporary(l_lblob,false);
  dbms_lob.createtemporary(O_blob,false);
  SELECT ODS_BLOB
  INTO l_tmpl_blob
  FROM s9t_folder
  WHERE file_name = 'ODS_SYSTEM_TEMPLATE_FOR_OUTPUT_FILES.ods';
  zip.zip(l_tmpl_blob,l_clob,l_lblob,O_blob);
  UPDATE s9t_folder
  SET ods_blob  =O_blob
  WHERE file_id = self.file_id;
END;
member PROCEDURE translate_to_user_lang
IS
Type t_names
IS
  TABLE OF VARCHAR2(255) INDEX BY VARCHAR2(255);
  l_wksht t_names;
  l_cols t_names;
BEGIN
  FOR rec IN
  (SELECT template_key,
    wksht_key,
    wksht_name
  FROM s9t_tmpl_wksht_def_tl
  WHERE template_key = self.template_key
  AND lang           = self.user_lang
  )
  LOOP
    l_wksht(rec.wksht_key):= rec.wksht_name;
  END LOOP;
  FOR i IN 1..self.sheet_names.count()
  LOOP
    BEGIN
      self.sheet_names(i) := l_wksht(self.sheet_names(i));
    EXCEPTION
    WHEN no_data_found THEN
      NULL;
    WHEN OTHERS THEN
      raise;
    END;
  END LOOP;
  FOR i IN 1..self.sheets.count()
  LOOP
    FOR rec IN
    (SELECT column_key,
      column_name
    FROM s9t_tmpl_cols_def_tl
    WHERE template_key = self.template_key
    AND wksht_key      = self.sheets(i).sheet_name
    AND lang           = self.user_lang
    )
    LOOP
      l_cols(rec.column_key):= rec.column_name;
    END LOOP;
    FOR j IN 1..self.sheets(i).column_headers.count()
    LOOP
      BEGIN
        FOR k IN 1..self.sheets(i).s9t_list_vals.count()
        LOOP
          IF self.sheets(i).column_headers(j)            = self.sheets(i).s9t_list_vals(k).column_name THEN
            self.sheets(i).s9t_list_vals(k).column_name := l_cols(self.sheets(i).column_headers(j));
          END IF;
        END LOOP;
        self.sheets(i).column_headers(j) := l_cols(self.sheets(i).column_headers(j));
      EXCEPTION
      WHEN no_data_found THEN
        NULL;
      WHEN OTHERS THEN
        raise;
      END;
    END LOOP;
    BEGIN
      self.sheets(i).sheet_name := l_wksht(self.sheets(i).sheet_name);
    EXCEPTION
    WHEN no_data_found THEN
      NULL;
    WHEN OTHERS THEN
      raise;
    END;
  END LOOP;
END translate_to_user_lang;
member FUNCTION validate_template
  RETURN BOOLEAN
IS
BEGIN
  INSERT
  INTO s9t_errors
    (
      file_id,
      error_seq_no,
      template_key,
      wksht_key,
      column_key,
      error_key
    )
  SELECT file_id,
    s9t_errors_seq.nextval AS error_seq_no,
    template_key,
    wksht_key,
    column_key,
    error_key
  FROM
    (SELECT sf.file_id,
      sf.s9t_file_obj.template_key AS template_key,
      wdm.wksht_key,
      NULL                AS column_key,
      'S9T_MISSING_SHEET' AS error_key
    FROM s9t_folder sf,
      s9t_tmpl_wksht_def_tl wd,
      s9t_tmpl_wksht_def wdm
    WHERE sf.file_name  = 'codes_data.ods'
    AND wd.template_key = sf.s9t_file_obj.template_key
    AND wd.lang         = sf.s9t_file_obj.user_lang
    AND wd.template_key = wdm.template_key
    AND wd.wksht_key    = wdm.wksht_key
    AND wdm.mandatory   = 'Y'
    AND NOT EXISTS
      (SELECT 1
      FROM TABLE(sf.s9t_file_obj.sheet_names)
      WHERE column_value = wd.wksht_name
      )
    UNION ALL
    SELECT sf.file_id,
      sf.s9t_file_obj.template_key AS template_key,
      wdm.wksht_key,
      cd.column_key,
      'S9T_MISSING_COLUMN' AS error_key
    FROM s9t_folder sf,
      TABLE(sf.s9t_file_obj.sheets) ss,
      s9t_tmpl_wksht_def_tl wd,
      s9t_tmpl_wksht_def wdm,
      s9t_tmpl_cols_def cd,
      s9t_tmpl_cols_def_tl cdm
    WHERE sf.file_id     = self.file_id
    AND wd.template_key  = sf.s9t_file_obj.template_key
    AND wd.lang          = sf.s9t_file_obj.user_lang
    AND wd.template_key  = wdm.template_key
    AND wd.wksht_key     = wdm.wksht_key
    AND wd.wksht_name    = ss.sheet_name
    AND cd.template_key  = wdm.template_key
    AND cd.wksht_key     = wdm.wksht_key
    AND cdm.template_key = cd.template_key
    AND cdm.wksht_key    = cd.wksht_key
    AND cdm.column_key   = cd.column_key
    AND cdm.lang         = wd.lang
    AND cd.mandatory     = 'Y'
    AND NOT EXISTS
      (SELECT 1
      FROM TABLE(ss.column_headers) ch
      WHERE cdm.column_name = ch.column_value
      )
    );
  IF sql%rowcount > 0 THEN
    RETURN false;
  ELSE
    RETURN true;
  END IF;
END;
END;
/

