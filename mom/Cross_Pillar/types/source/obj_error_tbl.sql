----------------------------------------------------------------------------
-- OBJECT CREATED :                    OBJ_ERROR_TBL
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object OBJ_ERROR_TBL

DROP TYPE OBJ_ERROR_TBL FORCE
/
--------------------------------------
--       Creating Object
--------------------------------------
PROMPT Creating Object OBJ_ERROR_TBL

CREATE OR REPLACE TYPE OBJ_ERROR_TBL AS TABLE OF VARCHAR2(2000);
/
