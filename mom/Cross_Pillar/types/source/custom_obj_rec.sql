--CUSTOM_OBJ_REC


DROP TYPE CUSTOM_OBJ_REC FORCE
/

CREATE OR REPLACE TYPE CUSTOM_OBJ_REC AS OBJECT
(FUNCTION_KEY                  VARCHAR2 (70),
 CALL_SEQ_NO                   VARCHAR2 (3),
 DOC_TYPE                      VARCHAR2 (6),
 DOC_ID                        NUMBER (10),
 SOURCE_ENTITY                 VARCHAR2 (6),
 SOURCE_TYPE                   VARCHAR2 (6),
 SOURCE_ID                     VARCHAR2 (10),
 DEST_ENTITY                   VARCHAR2 (6),
 DEST_TYPE                     VARCHAR2 (6),
 DEST_ID                       VARCHAR2 (10),
 DEPT                          NUMBER (4),
 CLASS                         NUMBER (4),
 SUBCLASS                      NUMBER (4),
 ITEM                          VARCHAR2 (25),
 SUPPLIER                      NUMBER(10),
 ORIGIN_COUNTRY_ID             VARCHAR2(3),
 LOC                           NUMBER(10),
 LOC_TYPE                      VARCHAR2(1),
 PARTNER                       VARCHAR2(10),
 PARTNER_TYPE                  VARCHAR2(6),
 ORDER_NO                      NUMBER(12),
 MRT_NO                        NUMBER(10),
 TSF_NO                        NUMBER(12),
 RTV_ORDER_NO                  NUMBER(10),
 COST_CHANGE                   NUMBER(8),
 CC_EFFECTIVE_DATE             DATE,
 VESSEL_ID                     VARCHAR2(20),
 VOYAGE_FLT_ID                 VARCHAR2(10),
 ESTIMATED_DEPART_DATE         DATE,
 CONSTRUCTOR FUNCTION CUSTOM_OBJ_REC
 (
  FUNCTION_KEY                  VARCHAR2 DEFAULT NULL,
  CALL_SEQ_NO                    VARCHAR2 DEFAULT NULL,
  DOC_TYPE                      VARCHAR2 DEFAULT NULL,
  DOC_ID                        NUMBER DEFAULT NULL,
  SOURCE_ENTITY                 VARCHAR2 DEFAULT NULL,
  SOURCE_TYPE                   VARCHAR2 DEFAULT NULL,
  SOURCE_ID                     VARCHAR2 DEFAULT NULL,
  DEST_ENTITY                   VARCHAR2 DEFAULT NULL,
  DEST_TYPE                     VARCHAR2 DEFAULT NULL,
  DEST_ID                       VARCHAR2 DEFAULT NULL,
  DEPT                          NUMBER DEFAULT NULL,
  CLASS                         NUMBER DEFAULT NULL,
  SUBCLASS                      NUMBER DEFAULT NULL,
  ITEM                          VARCHAR2 DEFAULT NULL,
  SUPPLIER                      NUMBER DEFAULT NULL,
  ORIGIN_COUNTRY_ID             VARCHAR2 DEFAULT NULL,
  LOC                           NUMBER DEFAULT NULL,
  LOC_TYPE                      VARCHAR2 DEFAULT NULL,
  PARTNER                       VARCHAR2 DEFAULT NULL,
  PARTNER_TYPE                  VARCHAR2 DEFAULT NULL,
  ORDER_NO                      NUMBER DEFAULT NULL,
  MRT_NO                        NUMBER DEFAULT NULL,
  TSF_NO                        NUMBER DEFAULT NULL,
  RTV_ORDER_NO                  NUMBER DEFAULT NULL,
  COST_CHANGE                   NUMBER DEFAULT NULL,
  CC_EFFECTIVE_DATE             DATE DEFAULT NULL,
  VESSEL_ID                     VARCHAR2 DEFAULT NULL,
  VOYAGE_FLT_ID                 VARCHAR2 DEFAULT NULL,
  ESTIMATED_DEPART_DATE         DATE DEFAULT NULL
   ) RETURN SELF AS RESULT
) NOT FINAL
/
create or replace TYPE BODY CUSTOM_OBJ_REC AS
 CONSTRUCTOR FUNCTION CUSTOM_OBJ_REC
 (
  FUNCTION_KEY                  VARCHAR2 DEFAULT NULL,
  CALL_SEQ_NO                  VARCHAR2 DEFAULT NULL,
  DOC_TYPE                      VARCHAR2 DEFAULT NULL,
  DOC_ID                        NUMBER DEFAULT NULL,
  SOURCE_ENTITY                 VARCHAR2 DEFAULT NULL,
  SOURCE_TYPE                   VARCHAR2 DEFAULT NULL,
  SOURCE_ID                     VARCHAR2 DEFAULT NULL,
  DEST_ENTITY                   VARCHAR2 DEFAULT NULL,
  DEST_TYPE                     VARCHAR2 DEFAULT NULL,
  DEST_ID                       VARCHAR2 DEFAULT NULL,
  DEPT                          NUMBER DEFAULT NULL,
  CLASS                         NUMBER DEFAULT NULL,
  SUBCLASS                      NUMBER DEFAULT NULL,
  ITEM                          VARCHAR2 DEFAULT NULL,
  SUPPLIER                      NUMBER DEFAULT NULL,
  ORIGIN_COUNTRY_ID             VARCHAR2 DEFAULT NULL,
  LOC                           NUMBER DEFAULT NULL,
  LOC_TYPE                      VARCHAR2 DEFAULT NULL,
  PARTNER                       VARCHAR2 DEFAULT NULL,
  PARTNER_TYPE                  VARCHAR2 DEFAULT NULL,
  ORDER_NO                      NUMBER DEFAULT NULL,
  MRT_NO                        NUMBER DEFAULT NULL,
  TSF_NO                        NUMBER DEFAULT NULL,
  RTV_ORDER_NO                  NUMBER DEFAULT NULL,
  COST_CHANGE                   NUMBER DEFAULT NULL,
  CC_EFFECTIVE_DATE             DATE DEFAULT NULL,
  VESSEL_ID                     VARCHAR2 DEFAULT NULL,
  VOYAGE_FLT_ID                 VARCHAR2 DEFAULT NULL,
  ESTIMATED_DEPART_DATE         DATE DEFAULT NULL
   
  ) RETURN SELF AS RESULT IS
    BEGIN
        SELF.FUNCTION_KEY := FUNCTION_KEY;
        SELF.CALL_SEQ_NO := CALL_SEQ_NO;
        SELF.DOC_TYPE := DOC_TYPE;
        SELF.DOC_ID := DOC_ID;
        SELF.SOURCE_ENTITY := SOURCE_ENTITY;
        SELF.SOURCE_TYPE := SOURCE_TYPE;
        SELF.SOURCE_ID := SOURCE_ID;
        SELF.DEST_ENTITY := DEST_ENTITY;
        SELF.DEST_TYPE := DEST_TYPE;
        SELF.DEST_ID := DEST_ID;
        SELF.DEPT := DEPT;
        SELF.CLASS := CLASS;
        SELF.SUBCLASS := SUBCLASS;
        SELF.ITEM := ITEM;
        SELF.SUPPLIER := SUPPLIER;
        SELF.ORIGIN_COUNTRY_ID := ORIGIN_COUNTRY_ID;
        SELF.LOC := LOC;
        SELF.LOC_TYPE := LOC_TYPE;
        SELF.PARTNER := PARTNER;
        SELF.PARTNER_TYPE := PARTNER_TYPE;
        SELF.ORDER_NO := ORDER_NO;
        SELF.MRT_NO := MRT_NO;
        SELF.TSF_NO := TSF_NO;
        SELF.RTV_ORDER_NO := RTV_ORDER_NO;
        SELF.COST_CHANGE := COST_CHANGE;
        SELF.CC_EFFECTIVE_DATE := CC_EFFECTIVE_DATE;
        SELF.VESSEL_ID  := CC_EFFECTIVE_DATE;
        SELF.VOYAGE_FLT_ID := CC_EFFECTIVE_DATE;
        SELF.ESTIMATED_DEPART_DATE := CC_EFFECTIVE_DATE;
        RETURN;
    END;
END;
/