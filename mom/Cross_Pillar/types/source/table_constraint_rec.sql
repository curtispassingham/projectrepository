DROP TYPE TABLE_CONSTRAINT_TBL FORCE
/
DROP TYPE TABLE_CONSTRAINT_REC FORCE
/

CREATE TYPE TABLE_CONSTRAINT_REC IS OBJECT
(
CONSTRAINT_NAME           VARCHAR2(100),          --This parameter will be the constraint name for add, modify or drop of target table name.
CONSTRAINT_TYPE           VARCHAR2(30),          --This hold all types of constraint type (UNIQUE, FOREIGN KEY, CHECK, PRIMARY KEY).
COLUMN_NAME               VARCHAR2(1000),        --This will be associated to target column of the table for constraint type <> CHECK, else NULL is used.
INDEX_IND                 VARCHAR2(30),          --This field will hold values for drop conditions (eg. DROP INDEX).
INDEX_CONDITION           VARCHAR2(10000),       --This will be populated by the index conditions for data types like UNIQUE and PRIMARY KEY (eg. USING INDEX INITRANS 12 STORAGE (FREELISTS 6) TABLESPACE RETEK_INDEX), else NULL is used.
REF_IDX                   VARCHAR2(30),          --This holds the value of 'REFERENCE' for FOREIGN KEY data type, else NULL is used.
REF_TABLE                 VARCHAR2(30),          --This is referenced table name when FOREIGN KEY data type is used, else NULL value is used.
REF_COLUMN                VARCHAR2(1000),         --Column/s of the referenced table for FOREIGN KEY data types, else NULL is used.
SEARCH_CONDITION          VARCHAR2(10000)       --This will be populated for constraint type as CHECK, else NULL is used.
)
/

CREATE TYPE TABLE_CONSTRAINT_TBL AS TABLE OF TABLE_CONSTRAINT_REC
/