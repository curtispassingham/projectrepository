DROP TYPE "OBJ_L10N_ATTRIB_TBL" FORCE
/
DROP TYPE "OBJ_L10N_ATTRIB_REC" FORCE
/
CREATE TYPE "OBJ_L10N_ATTRIB_REC" AS OBJECT
(
   key_1                VARCHAR2(60),
   key_2                VARCHAR2(60),
   key_3                VARCHAR2(60),
   key_4                VARCHAR2(60),
   key_5                VARCHAR2(60),
   key_6                VARCHAR2(60),
   key_7                VARCHAR2(60),
   key_8                VARCHAR2(60),
   key_9                VARCHAR2(60),
   key_10               VARCHAR2(60),
   key_11               VARCHAR2(60),
   key_12               VARCHAR2(60),
   key_13               VARCHAR2(60),
   key_14               VARCHAR2(60),
   key_15               VARCHAR2(60),
   key_16               VARCHAR2(60),
   key_17               VARCHAR2(60),
   key_18               VARCHAR2(60),
   key_19               VARCHAR2(60),
   key_20               VARCHAR2(60),
   ---
   group_id             NUMBER(10),
   attrib_col           VARCHAR2(11),
   attrib_value         VARCHAR2(250),
   attrib_desc          VARCHAR2(255)
)
/

CREATE TYPE "OBJ_L10N_ATTRIB_TBL" as table of OBJ_L10N_ATTRIB_REC
/
