DROP TYPE KEY_VAL_TBL FORCE
/
DROP TYPE KEY_VAL_RECORD FORCE
/

CREATE OR REPLACE
TYPE KEY_VAL_RECORD AS OBJECT
   (
    l10n_ext_table VARCHAR2(22),
    base_rms_table VARCHAR2(30),
    key_col        VARCHAR2(30),
    key_number     NUMBER(2),
    data_type      VARCHAR2(10),
    key_desc       VARCHAR2(255),
    key_val        VARCHAR2(60)
    )
/

CREATE OR REPLACE TYPE KEY_VAL_TBL AS TABLE OF KEY_VAL_RECORD
/
