create or replace type stragg_type as object
(
  val_table  OBJ_VARCHAR_ID_TABLE,

  static function ODCIAggregateInitialize
    ( sctx in out stragg_type )
    return number ,

  member function ODCIAggregateIterate
    ( self  in out stragg_type ,
      value in     varchar2
    ) return number ,

  member function ODCIAggregateTerminate
    ( self        in  stragg_type,
      returnvalue out varchar2,
      flags in number
    ) return number ,

  member function ODCIAggregateMerge
    ( self in out stragg_type,
      ctx2 in     stragg_type
    ) return number
);
/

create or replace type body stragg_type
is

  static function ODCIAggregateInitialize
  ( sctx in out stragg_type )
  return number
  is
  begin

    sctx := stragg_type( OBJ_VARCHAR_ID_TABLE() );

    return ODCIConst.Success ;

  end;

  member function ODCIAggregateIterate
  ( self  in out stragg_type ,
    value in     varchar2
  ) return number
  is
  begin

    self.val_table.extend;
    self.val_table(self.val_table.count) := value;

    return ODCIConst.Success;
  end;

  member function ODCIAggregateTerminate
  ( self        in  stragg_type ,
    returnvalue out varchar2 ,
    flags       in  number
  ) return number
  is

    v_data varchar2(4000);

  begin

    for x in
    (
      select column_value
      from   table(val_table)
      order by 1
    )
    loop
      v_data := v_data || ',' || x.column_value;
    end loop;

    returnValue := ltrim( v_data, ',' );        -- added

    return ODCIConst.Success;

  end;

  member function ODCIAggregateMerge
  ( self in out stragg_type ,
    ctx2 in     stragg_type
  ) return number
  is
  begin

    self.val_table := self.val_table multiset union ( ctx2.val_table ) ;

    return ODCIConst.Success;

  end;

end;
/
