DROP TYPE TABLE_COLUMN_TBL FORCE
/
DROP TYPE TABLE_COLUMN_REC FORCE
/
CREATE TYPE TABLE_COLUMN_REC IS OBJECT
(
   COLUMN_NAME           VARCHAR2(30),          --The column/field name of the table to be added, modified or dropped.
   DATA_TYPE             VARCHAR2(106),         --The data type associated to the column name.
   DATA_LENGTH           NUMBER,                --The length definition of the data type for the column name.
   DATA_PRECISION        NUMBER,                --This field will hold the precision for NUMBER data type, else NULL value is used.
   DATA_DEFAULT          VARCHAR2(30),          --This field will be populated if the Key Default is set to 'DEFAULT', else NUL is used.
   NULL_CONSTRAINT       VARCHAR2(30)           --This field will indicate if the column name is NULLable or NOT NULLable field in the table.
)
/



CREATE TYPE TABLE_COLUMN_TBL AS TABLE OF TABLE_COLUMN_REC
/