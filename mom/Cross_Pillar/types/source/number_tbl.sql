--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TYPE ADDED  :				NUMBER_TBL
----------------------------------------------------------------------------

-- whenever sqlerror exit

--------------------------------------
--       Altering Table
--------------------------------------
PROMPT Creating Type 'NUMBER_TBL'
DROP TYPE NUMBER_TBL
/
CREATE OR REPLACE TYPE NUMBER_TBL AS TABLE OF NUMBER
/
