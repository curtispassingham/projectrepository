DROP TYPE TABLE_INDEX_TBL FORCE
/
DROP TYPE TABLE_INDEX_REC FORCE
/

CREATE TYPE TABLE_INDEX_REC IS OBJECT
(
INDEX_NAME           VARCHAR2(30),          --The index name to be add, modify or drop from the target table.
TABLE_NAME           VARCHAR2(30),          --The table name where the index name will be applied.
COLUMN_NAME          VARCHAR2(1000),        --The column name of the table in which affected by the index name.
INDEX_CONDITION      VARCHAR2(10000)        --The condition text for the index name being added, modified or dropped.
)
/

CREATE TYPE TABLE_INDEX_TBL AS TABLE OF TABLE_INDEX_REC
/