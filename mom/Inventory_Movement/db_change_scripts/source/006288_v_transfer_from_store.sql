--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_TRANSFER_FROM_STORE
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_TRANSFER_FROM_STORE'

CREATE OR REPLACE FORCE VIEW V_TRANSFER_FROM_STORE ( STORE, STORE_NAME, STORE_NAME_SECONDARY, STOCKHOLDING_IND, TSF_ENTITY_ID, ORG_UNIT_ID ) AS
SELECT STR.STORE STORE
      ,V.STORE_NAME STORE_NAME
      ,V.STORE_NAME_SECONDARY STORE_NAME_SECONDARY
      ,STR.STOCKHOLDING_IND STOCKHOLDING_IND
      ,STR.TSF_ENTITY_ID TSF_ENTITY_ID
      ,STR.ORG_UNIT_ID ORG_UNIT_ID 
 FROM STORE STR,
      V_STORE_TL V
WHERE STR.STORE = V.STORE
/

COMMENT ON TABLE V_TRANSFER_FROM_STORE IS 'This view will be used to display the Transfer From Store  LOVs using a security policy to filter User access.'
/
