--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View Modified:						 v_recadj_detail
---------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       MODIFYING VIEW
--------------------------------------
PROMPT CREATING VIEW 'V_RECADJ_DETAIL';
CREATE OR REPLACE FORCE  VIEW V_RECADJ_DETAIL 
("SHIPMENT", 
 "PARENT_SHIPMENT",
 "TO_LOC",
 "TO_LOC_TYPE", 
 "ITEM", 
 "CARTON", 
 "QTY_EXPECTED", 
 "QTY_RECEIVED", 
 "QTY_MATCHED",
 "QTY_ORDERED") 
AS
   select distinct s.shipment shipment,
          s.parent_shipment parent_shipment,
          s.to_loc,
          s.to_loc_type,
          ss.item item,
          ss.carton carton,
          ss.qty_expected qty_expected,
          ss.qty_received qty_received,
          ss.qty_matched qty_matched,
          --aggregated order qty by physical location
          sum(ol.qty_ordered) over (partition by ss.seq_no, s.shipment, ss.item, ss.carton, s.to_loc) qty_ordered
     from shipment s,
          shipsku ss,
          ordloc ol
    where s.shipment = ss.shipment
      and ol.order_no  = s.order_no
      and ol.item      = ss.item
      and (ol.location = s.to_loc
       or exists (select 'x'
                    from wh
                   where physical_wh = s.to_loc
                     and ol.location = wh
                     and rownum = 1));

COMMENT ON TABLE V_RECADJ_DETAIL IS 'This view extracts the order receipt adjustment details for the receipt adjustment detail section of the shipment screen. It rolls up the order quantity to the physical warehouse on the shipment table.'
/

COMMENT ON COLUMN V_RECADJ_DETAIL.SHIPMENT IS 'Contains the shipment number of a standalone shipment or the child shipment number if adjustments have been made after matching.'
/

COMMENT ON COLUMN V_RECADJ_DETAIL.PARENT_SHIPMENT IS 'Contains the parent shipment number the child shipment is associated with.'
/

COMMENT ON COLUMN V_RECADJ_DETAIL.TO_LOC IS 'Contains the ship to location. It will be a physical warehouse if the ship to location type is a warehouse.'
/

COMMENT ON COLUMN V_RECADJ_DETAIL.TO_LOC_TYPE IS 'Contains the ship to location type.'
/

COMMENT ON COLUMN V_RECADJ_DETAIL.ITEM IS 'Contains the item number on the shipsku.'
/

COMMENT ON COLUMN V_RECADJ_DETAIL.CARTON IS 'Contains the carton number on the shipsku.'
/

COMMENT ON COLUMN V_RECADJ_DETAIL.QTY_EXPECTED IS 'Contains the total expected quantity for the item.'
/

COMMENT ON COLUMN V_RECADJ_DETAIL.QTY_RECEIVED IS 'Contains the total received quantity for the item.'
/

COMMENT ON COLUMN V_RECADJ_DETAIL.QTY_MATCHED IS 'Contains the total invoice matched quantity for the item.'
/

COMMENT ON COLUMN V_RECADJ_DETAIL.QTY_ORDERED IS 'Contains the total ordered quantity for the item at the physical to-location.'
/
