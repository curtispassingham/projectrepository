--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE MODIFIED:				RTV_ORDER_NO_SEQ
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       MODIFYING SEQUENCE
--------------------------------------
PROMPT Modifying Sequence 'RTV_ORDER_NO_SEQ'

DECLARE
  L_sequence_exists number := 0;
BEGIN
  SELECT count(*) INTO L_sequence_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'RTV_ORDER_NO_SEQ'
     AND OBJECT_TYPE = 'SEQUENCE';

  if (L_sequence_exists != 0) then
      execute immediate 'DROP SEQUENCE RTV_ORDER_NO_SEQ';
  end if;
end;
/

CREATE SEQUENCE RTV_ORDER_NO_SEQ
    START WITH 100000000
    INCREMENT BY 1 
    MAXVALUE 10000000000 
    MINVALUE 100000000 
    CYCLE 
    CACHE 100 
/

