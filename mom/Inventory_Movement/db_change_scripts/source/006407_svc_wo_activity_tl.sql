--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

---------------------------------------------------------
--       Modifying Table        SVC_WO_ACTIVITY_TL       
---------------------------------------------------------

PROMPT Modifying Table 'SVC_WO_ACTIVITY_TL'
ALTER TABLE SVC_WO_ACTIVITY_TL ADD ACTIVITY_CODE VARCHAR2 (10 ) NULL
/

COMMENT ON COLUMN SVC_WO_ACTIVITY_TL.ACTIVITY_CODE is 'This field holds a unique, user-defined identifier for a particular activity to be carried out through a work order at the finisher location.'
/

PROMPT Dropping Constraint 'SVC_WO_ACTIVITY_TL_UK'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'SVC_WO_ACTIVITY_TL_UK'
     AND CONSTRAINT_TYPE = 'U';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE SVC_WO_ACTIVITY_TL DROP CONSTRAINT SVC_WO_ACTIVITY_TL_UK';
  end if;
end;
/

PROMPT Adding Constraint 'SVC_WO_ACTIVITY_TL_UK'
ALTER TABLE SVC_WO_ACTIVITY_TL ADD CONSTRAINT SVC_WO_ACTIVITY_TL_UK UNIQUE 
 (LANG, 
  ACTIVITY_CODE ) 
  USING INDEX 
  INITRANS 12 
  TABLESPACE RETAIL_INDEX
  /
