--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_TSF_ENTITY_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

CREATE TABLE SVC_TSF_ENTITY_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, TSF_ENTITY_ID NUMBER(10)
, TSF_ENTITY_DESC VARCHAR2(120)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_TSF_ENTITY_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in TSF_ENTITY_TL.'
/

COMMENT ON COLUMN SVC_TSF_ENTITY_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_TSF_ENTITY_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_TSF_ENTITY_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_TSF_ENTITY_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_TSF_ENTITY_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_TSF_ENTITY_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_TSF_ENTITY_TL.TSF_ENTITY_ID is 'This field contains the number which uniquely identifies the transfer entity.'
/

COMMENT ON COLUMN SVC_TSF_ENTITY_TL.TSF_ENTITY_DESC is 'This field contains the name of the transfer entity.'
/

ALTER TABLE SVC_TSF_ENTITY_TL
ADD CONSTRAINT SVC_TSF_ENTITY_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SVC_TSF_ENTITY_TL
ADD CONSTRAINT SVC_TSF_ENTITY_TL_UK UNIQUE
(LANG, TSF_ENTITY_ID)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

