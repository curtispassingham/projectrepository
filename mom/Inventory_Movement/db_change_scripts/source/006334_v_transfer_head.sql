--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View Created:						 v_transfer_head
---------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT CREATING VIEW 'V_TRANSFER_HEAD';
CREATE OR REPLACE FORCE VIEW V_TRANSFER_HEAD 
("TSF_NO", 
 "CHILD_TSF_NO",
 "TSF_TYPE",
 "OVERALL_STATUS",
 "DEPT",
 "INVENTORY_TYPE",
 "MRT_NO",
 "CONTEXT_TYPE",
 "CONTEXT_VALUE",
 "EXT_REF_NO",
 "EXP_DC_DATE",
 "NOT_AFTER_DATE",
 "DELIVERY_DATE",
 "DELIVERY_SLOT_ID",
 "COMMENT_DESC",
 "CREATE_ID",
 "CREATE_DATE",
 "APPROVAL_ID",
 "APPROVAL_DATE",
 "LEG_1_STATUS",
 "LEG_1_PROGRESS_IND",
 "LEG_1_FREIGHT_CODE",
 "LEG_1_ROUTING_CODE",
 "LEG_2_STATUS",
 "LEG_2_PROGRESS_IND",
 "LEG_2_FREIGHT_CODE",
 "LEG_2_ROUTING_CODE",
 "LEG_1_FROM_LOC",
 "LEG_1_FROM_LOC_TYPE",
 "LEG_1_FROM_LOC_ENTITY",
 "LEG_1_FROM_LOC_SOB_ID",
 "LEG_1_TO_LOC",
 "LEG_1_TO_LOC_TYPE",
 "LEG_1_TO_LOC_ENTITY",
 "LEG_1_TO_LOC_SOB_ID",
 "LEG_2_FROM_LOC",
 "LEG_2_FROM_LOC_TYPE",
 "LEG_2_FROM_LOC_ENTITY",
 "LEG_2_FROM_LOC_SOB_ID",
 "LEG_2_TO_LOC",
 "LEG_2_TO_LOC_TYPE",
 "LEG_2_TO_LOC_ENTITY",
 "LEG_2_TO_LOC_SOB_ID") 
AS 
   SELECT /*+ use_nl(t, t2) */
          -- COLUMN NOTES --------------------------------------------------------------------------------
          --
          -- OVERALL_STATUS: Used for two-legged transfers.
          --    If tsf has 2 legs, values will include:
          --       1. In Progress ('N') if LEG_1_STATUS is 'P'icked, se'L'ected or 'S'hipped.
          --       2. 'C'losed          if LEG_1_STATUS and LEG_2_STATUS are 'C'losed.
          --       3. In Progress ('N') if LEG_1_STATUS is 'C'losed and LEG_2_STATUS != 'C'
          --    In all other cases, or if tsf has only one leg, OVERALL_STATUS will equal LEG_1_STATUS.
          --
          -- LEG_1_PROGRESS_IND: value is 'Y' if LEG_1_STATUS is 'P'icked, se'L'ected or 'S'hipped
          --
          -- LEG_2_PROGRESS_IND: value is 'Y' if tsf has two legs and LEG_2_STATUS is 'P'icked, se'L'ected or 'S'hipped
          -- -----------------------------------------------------------------------------------------------
          t.tsf_no TSF_NO,
          t2.tsf_no CHILD_TSF_NO,
          t.tsf_type TSF_TYPE,
          DECODE(t.status, 'P', 'N', 'L','N','S', 'N', 'C', DECODE(t2.status, NULL, t.status, 'C', 'C', 'N'), t.status) OVERALL_STATUS,
          t.dept DEPT,
          t.inventory_type INVENTORY_TYPE,
          t.mrt_no MRT_NO,
          t.context_type CONTEXT_TYPE,
          t.context_value CONTEXT_VALUE,
          t.ext_ref_no EXT_REF_NO,
          t.exp_dc_date EXP_DC_DATE,
          t.not_after_date NOT_AFTER_DATE,
          t.delivery_date DELIVERY_DATE,
          t.delivery_slot_id DELIVERY_SLOT_ID,
          t.comment_desc COMMENT_DESC,
          t.create_id CREATE_ID,
          t.create_date CREATE_DATE,
          t.approval_id APPROVAL_ID,
          t.approval_date APPROVAL_DATE,
          t.status LEG_1_STATUS,
          DECODE(t.status, 'P', 'Y', 'L', 'Y', 'S', 'Y', 'N') LEG_1_PROGRESS_IND,
          t.freight_code LEG_1_FREIGHT_CODE,
          t.routing_code LEG_1_ROUTING_CODE,
          t2.status LEG_2_STATUS,
          DECODE(t2.tsf_parent_no, NULL, NULL, DECODE(t2.status, 'P', 'Y','L','Y', 'S', 'Y', 'N')) LEG_2_PROGRESS_IND,
          t2.freight_code LEG_2_FREIGHT_CODE,
          t2.routing_code LEG_2_ROUTING_CODE,
          -- 1st leg from-locs:
          t.from_loc LEG_1_FROM_LOC,
          t.from_loc_type LEG_1_FROM_LOC_TYPE,
          DECODE(t.from_loc_type, 'W',(SELECT tsf_entity_id FROM wh WHERE wh = t.from_loc),
                                  'S',(SELECT tsf_entity_id FROM store WHERE store = t.from_loc)) LEG_1_FROM_LOC_ENTITY,
          mls1.set_of_books_id LEG_1_FROM_LOC_SOB_ID,
          -- 1st leg to-locs:
          t.to_loc LEG_1_TO_LOC,
          DECODE(t2.tsf_parent_no, NULL, t.to_loc_type, DECODE(t.to_loc_type, 'W', 'I', t.to_loc_type)) LEG_1_TO_LOC_TYPE,   --'I' for internal finisher
          DECODE(t.to_loc_type, 'W',(SELECT tsf_entity_id FROM wh WHERE wh = t.to_loc),
                                'S',(SELECT tsf_entity_id FROM store WHERE store = t.to_loc),
                                'E',(SELECT tsf_entity_id FROM partner WHERE partner_type = 'E' AND partner_id = t.to_loc)) LEG_1_TO_LOC_ENTITY,
          mls2.set_of_books_id LEG_1_TO_LOC_SOB_ID,
          -- 2nd leg from-locs:
          t2.from_loc LEG_2_FROM_LOC,
          DECODE(t2.from_loc_type, 'W', 'I', t2.from_loc_type) LEG_2_FROM_LOC_TYPE,  --'I' for internal finisher
          DECODE(t2.from_loc_type, 'W',(SELECT tsf_entity_id FROM wh WHERE wh = t2.from_loc), 
                                   'E',(SELECT tsf_entity_id FROM partner WHERE partner_type = 'E' AND partner_id = t2.from_loc)) LEG_2_FROM_LOC_ENTITY,
          DECODE(t2.tsf_parent_no, NULL, to_number(NULL), mls2.set_of_books_id) LEG_2_FROM_LOC_SOB_ID,
          -- 2nd leg to-locs:
          t2.to_loc LEG_2_TO_LOC,
          t2.to_loc_type LEG_2_TO_LOC_TYPE,
          DECODE(t2.to_loc_type, 'W',(SELECT tsf_entity_id FROM wh WHERE wh = t2.to_loc),
                                     (SELECT tsf_entity_id FROM store WHERE store = t2.to_loc)) LEG_2_TO_LOC_ENTITY,
          mls3.set_of_books_id LEG_2_TO_LOC_SOB_ID
     FROM tsfhead t,
          tsfhead t2,
          mv_loc_sob mls1,
          mv_loc_sob mls2,
          mv_loc_sob mls3
    WHERE t.tsf_parent_no IS NULL
      AND t.tsf_no = t2.tsf_parent_no (+)
      -- security: user must have visibility to at least 1 item on the transfer,
      -- if tsfdetail records exist for the tsf.
      AND (t.tsf_no IN (SELECT tsf_no FROM v_tsfdetail WHERE tsf_no = t.tsf_no AND rownum = 1)
        OR NOT EXISTS (SELECT tdt.tsf_no FROM tsfdetail tdt WHERE tdt.tsf_no = t.tsf_no AND rownum = 1))
      -- security: users must have visibility to both the from and to loc
      AND (t.from_loc IN (SELECT location_id FROM v_transfer_from_loc WHERE location_id = t.from_loc AND rownum = 1)
        OR t.from_loc in (SELECT wh FROM v_physical_wh WHERE wh = t.from_loc AND rownum=1))
      AND (t.to_loc IS NULL 
        OR t.to_loc IN (SELECT location_id FROM v_transfer_to_loc WHERE location_id = t.to_loc AND rownum = 1)
        OR t.to_loc in (SELECT wh FROM v_physical_wh WHERE wh = t.to_loc AND rownum = 1))
      AND (t2.to_loc IS NULL
        OR t2.to_loc IN (SELECT location_id FROM v_transfer_to_loc WHERE location_id = t2.to_loc AND rownum = 1)
        OR t2.to_loc in (SELECT wh FROM v_physical_wh WHERE wh = t2.to_loc and rownum = 1))
      AND mls1.location          = t.from_loc
      AND mls1.location_type     = t.from_loc_type
      AND mls2.location(+)       = t.to_loc
      AND mls2.location_type(+)  = t.to_loc_type
      AND mls3.location (+)      = t2.to_loc
      AND mls3.location_type (+) = t2.to_loc_type
/

COMMENT ON TABLE V_TRANSFER_HEAD IS 'This view is used to display the transfer header level information for the transfer screen. For a 2-legged transfer, the view includes attributes for both parent and child transfers. It only returns the data if the user has privilege to access the data.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.TSF_NO IS 'Contains the transfer no.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.CHILD_TSF_NO IS 'Contains the child transfer no in a 2-legged transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.TSF_TYPE IS 'Contains the transfer type.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.OVERALL_STATUS IS 'Contains the overall status of the transfer. It is In Progress (''N'') if the first leg is ''P''icked, se''L''ected, or ''S''hipped, or if the first legged is ''C''losed but the second leg is not. It is ''C''losed if both transfer legs are closed. Otherwise, or in case of a single leg transfer, it is the status of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.DEPT IS 'Contains the department all items in the transfer belong to.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.INVENTORY_TYPE IS 'Indicates if the transfer is from available or unavailable inventory.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.MRT_NO IS 'Contains the mass return transfer no the transfer is linked to.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.CONTEXT_TYPE IS 'Contains the reason code related to which a transfer is made. Valid values are: Promotion, Customer Transfer, Store Requisition and Repairing. Two-legged transfer cannot be created for context types Promotion, Customer Transfer and Store Requisition. Context type Repairing is only applicable to external finishers.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.CONTEXT_VALUE IS 'Contains the value relating to the context type, for example Promotion Number.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.EXT_REF_NO IS 'Contains the external reference number of the transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.EXP_DC_DATE IS 'Contains the date on which the transfer is expected to be shipped to a warehouse.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.NOT_AFTER_DATE IS 'Contains the last day upon which a store can ship the requested merchandise to the warehouse.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.DELIVERY_DATE IS 'Contains the earliest date that the transfer can be delivered to the store.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.DELIVERY_SLOT_ID IS 'Contains the delivery slot that will be used for the transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.COMMENT_DESC IS 'Contains the comment.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.CREATE_ID IS 'Contains the user id that created the transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.CREATE_DATE IS 'Contains the transfer create date.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.APPROVAL_ID IS 'Contains the user id that approved the transfer.'
/
      
COMMENT ON COLUMN V_TRANSFER_HEAD.APPROVAL_DATE IS 'Contains the transfer approval date.'
/
      
COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_STATUS IS 'Status of the transfer. In case of a 2-legged transfer, it is the status of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_PROGRESS_IND IS 'Indicates if the transfer is in progress. value is Y if LEG_1_STATUS is ''P''icked, se''L''ected or ''S''hipped. Otherwise N.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_FREIGHT_CODE IS 'Freight code of the transfer. It is used to determine the priority for the transfer. In case of a 2-legged transfer, it is the freight code of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_ROUTING_CODE IS 'Indicates the type of freight to use on the transfer. In case of a 2-legged transfer, it is the routing code of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_STATUS IS 'Status of the second leg transfer for a 2-legged transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_PROGRESS_IND IS 'Indicates if the second leg transfer is in progress. value is Y if the transfer has two legs and LEG_2_STATUS is ''P''icked, se''L''ected or ''S''hipped. Otherwise N.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_FREIGHT_CODE IS 'Freight code of the second leg transfer for a 2-legged transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_ROUTING_CODE IS 'Routing code of the second leg transfer for a 2-legged transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_FROM_LOC IS 'From location of the transfer. In case of a 2-legged transfer, it is the from location of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_FROM_LOC_TYPE IS 'From location type of the transfer. In case of a 2-legged transfer, it is based on the from location of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_FROM_LOC_ENTITY IS 'Transfer entity of the transfer from-loc. In case of a 2-legged transfer, it is based on the from location of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_FROM_LOC_SOB_ID IS 'Set of books ID of the transfer from-loc. In case of a 2-legged transfer, it is based on the from location of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_TO_LOC IS 'To location of the transfer. In case of a 2-legged transfer, it is the to location of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_TO_LOC_TYPE IS 'To location type of the transfer. In case of a 2-legged transfer, it is based on the to location of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_TO_LOC_ENTITY IS 'Transfer entity of the transfer to-loc. In case of a 2-legged transfer, it is based on the to location of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_1_TO_LOC_SOB_ID IS 'Set of books ID of the transfer to-loc. In case of a 2-legged transfer, it is based on the to location of the first leg.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_FROM_LOC IS 'From location of the second leg transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_FROM_LOC_TYPE IS 'From location type of the second leg transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_FROM_LOC_ENTITY IS 'Transfer entity of the from-loc of the second leg transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_FROM_LOC_SOB_ID IS 'Set of books ID of the from-loc of the second leg transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_TO_LOC IS 'To location of the second leg transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_TO_LOC_TYPE IS 'To location type of the second leg transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_TO_LOC_ENTITY IS 'Transfer entity of the to-loc of the second leg transfer.'
/

COMMENT ON COLUMN V_TRANSFER_HEAD.LEG_2_TO_LOC_SOB_ID IS 'Set of books ID of the to-loc of the second leg transfer.'
/
