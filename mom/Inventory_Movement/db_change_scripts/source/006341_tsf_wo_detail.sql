--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

ALTER TABLE TSF_WO_DETAIL DROP CONSTRAINT UK_TSF_WO_DETAIL DROP INDEX
/


PROMPT Modifying Table 'TSF_WO_DETAIL'
ALTER TABLE TSF_WO_DETAIL ADD INV_STATUS NUMBER  NULL
/

COMMENT ON COLUMN TSF_WO_DETAIL.INV_STATUS is 'This field contains the inventory status, if one exists, for the transfer detail record.  The inventory status field corresponds to the inventory status type found on the INV_STATUS_TYPES table.'
/


ALTER TABLE TSF_WO_DETAIL ADD CONSTRAINT UK_TSF_WO_DETAIL UNIQUE (
TSF_WO_ID,
ITEM,
ACTIVITY_ID,
INV_STATUS
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/


ALTER TABLE TSF_WO_DETAIL
 ADD CONSTRAINT TWD_IST_FK
 FOREIGN KEY (INV_STATUS)
 REFERENCES INV_STATUS_TYPES (INV_STATUS)
/

