--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Trigger dropped:						rms_view_bws_dr
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING TRIGGER
--------------------------------------
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM ALL_TRIGGERS
   WHERE TRIGGER_NAME='RMS_VIEW_BWS_DR';

  if (L_table_exists != 0) then
      execute immediate 'DROP TRIGGER RMS_VIEW_BWS_DR';
  end if;
end;
/
