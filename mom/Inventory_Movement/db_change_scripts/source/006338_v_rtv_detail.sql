--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW MODIFIED:          V_RTV_DETAIL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT CREATING VIEW 'V_RTV_DETAIL';
CREATE OR REPLACE FORCE  VIEW V_RTV_DETAIL 
("RTV_ORDER_NO", 
 "SEQ_NO", 
 "ITEM", 
 "SHIPMENT", 
 "INV_STATUS", 
 "QTY_RETURNED", 
 "UNIT_COST", 
 "REASON", 
 "QTY_REQUESTED", 
 "QTY_CANCELLED", 
 "PUBLISH_IND", 
 "RESTOCK_PCT", 
 "ORIGINAL_UNIT_COST", 
 "UPDATED_BY_RMS_IND") 
 AS SELECT RDT.RTV_ORDER_NO RTV_ORDER_NO
          ,RDT.SEQ_NO SEQ_NO
          ,RDT.ITEM ITEM
          ,RDT.SHIPMENT SHIPMENT
          ,RDT.INV_STATUS INV_STATUS
          ,RDT.QTY_RETURNED QTY_RETURNED
          ,RDT.UNIT_COST UNIT_COST
          ,RDT.REASON REASON
          ,RDT.QTY_REQUESTED QTY_REQUESTED
          ,RDT.QTY_CANCELLED QTY_CANCELLED
          ,RDT.PUBLISH_IND PUBLISH_IND
          ,RDT.RESTOCK_PCT RESTOCK_PCT
          ,RDT.ORIGINAL_UNIT_COST
          ,RDT.UPDATED_BY_RMS_IND
     FROM RTV_DETAIL RDT
    WHERE EXISTS (select 1 from v_item_master vim where vim.item = RDT.item)
/   

COMMENT ON TABLE V_RTV_DETAIL IS 'This view extracts the Return To Vendor detail information.'
/

