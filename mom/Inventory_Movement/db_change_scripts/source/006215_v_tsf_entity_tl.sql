CREATE OR REPLACE FORCE VIEW V_TSF_ENTITY_TL (TSF_ENTITY_ID, TSF_ENTITY_DESC, LANG ) AS
SELECT  b.tsf_entity_id,
        case when tl.lang is not null then tl.tsf_entity_desc else b.tsf_entity_desc end tsf_entity_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  TSF_ENTITY b,
        TSF_ENTITY_TL tl
 WHERE  b.tsf_entity_id = tl.tsf_entity_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_TSF_ENTITY_TL is 'This is the translation view for base table TSF_ENTITY. This view fetches data in user langauge either from translation table TSF_ENTITY_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_TSF_ENTITY_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_TSF_ENTITY_TL.TSF_ENTITY_ID is 'This field contains the number which uniquely identifies the transfer entity.'
/

COMMENT ON COLUMN V_TSF_ENTITY_TL.TSF_ENTITY_DESC is 'This field contains the name of the transfer entity.'
/

