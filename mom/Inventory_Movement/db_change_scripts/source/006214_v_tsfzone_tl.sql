CREATE OR REPLACE FORCE VIEW V_TSFZONE_TL (TRANSFER_ZONE, DESCRIPTION, LANG ) AS
SELECT  b.transfer_zone,
        case when tl.lang is not null then tl.description else b.description end description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  TSFZONE b,
        TSFZONE_TL tl
 WHERE  b.transfer_zone = tl.transfer_zone (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_TSFZONE_TL is 'This is the translation view for base table TSFZONE. This view fetches data in user langauge either from translation table TSFZONE_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_TSFZONE_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_TSFZONE_TL.TRANSFER_ZONE is 'This field contains the number which uniquely identifies the transfer zone.'
/

COMMENT ON COLUMN V_TSFZONE_TL.DESCRIPTION is 'This field contains the name of the transfer zone.'
/

