--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Type dropped:						WRP_BUYER_WKSHT_TBL
--										WRP_BUYER_WKSHT_REC
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING TYPE
--------------------------------------
PROMPT DROPPING TYPE WRP_BUYER_WKSHT_TBL
DECLARE
  L_type_exists number := 0;
BEGIN
  SELECT count(*) INTO L_type_exists
    FROM ALL_TYPES
   WHERE TYPE_NAME='WRP_BUYER_WKSHT_TBL';

  if (L_type_exists != 0) then
      execute immediate 'DROP TYPE WRP_BUYER_WKSHT_TBL';
  end if;
end;
/

PROMPT DROPPING TYPE WRP_BUYER_WKSHT_REC
DECLARE
  L_type_exists number := 0;
BEGIN
  SELECT count(*) INTO L_type_exists
    FROM ALL_TYPES
   WHERE TYPE_NAME='WRP_BUYER_WKSHT_REC';

  if (L_type_exists != 0) then
      execute immediate 'DROP TYPE WRP_BUYER_WKSHT_REC';
  end if;
end;
/