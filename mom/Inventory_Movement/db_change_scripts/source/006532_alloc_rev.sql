--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table
--------------------------------------

PROMPT Modifying Table 'ALLOC_REV'
ALTER TABLE ALLOC_REV ADD REV_DATE DATE  DEFAULT sysdate NOT NULL
/

COMMENT ON COLUMN ALLOC_REV.REV_DATE is 'This field contains the date the revision was processed by Oracle Retail.'
/

