--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RTV_HEAD_CFA_EXT'
ALTER TABLE RTV_HEAD_CFA_EXT MODIFY RTV_ORDER_NO NUMBER (10)
/

COMMENT ON COLUMN RTV_HEAD_CFA_EXT.RTV_ORDER_NO is 'This column holds the RTV Order Number this extended data is associated with.'
/

