--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ORDCUST'

PROMPT Creating Index 'ORDCUST_I2'
CREATE INDEX ORDCUST_I2 on ORDCUST
  (ORDER_NO
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Index 'ORDCUST_I3'
CREATE INDEX ORDCUST_I3 on ORDCUST
  (TSF_NO
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

