--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 BUYER_WKSHT_PO_LIST_GTT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'BUYER_WKSHT_PO_LIST_GTT'
CREATE GLOBAL TEMPORARY TABLE BUYER_WKSHT_PO_LIST_GTT
 (ORDER_NO NUMBER(12),
  DEPT NUMBER(4),
  WRITTEN_DATE DATE,
  SUPPLIER NUMBER(10),
  SUP_NAME VARCHAR2(240 ),
  POOL_SUPPLIER NUMBER(10),
  LOC_TYPE VARCHAR2(1 ),
  LOCATION NUMBER(10),
  LOC_NAME VARCHAR2(150 ),
  FIRST_ORDER_TOTAL NUMBER(20,4),
  SECOND_ORDER_TOTAL NUMBER(20,4)
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE BUYER_WKSHT_PO_LIST_GTT is ''
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.ORDER_NO is 'Contains the purchase order number.'
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.DEPT is 'Contains the department number.'
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.WRITTEN_DATE is 'Contains the order written date.'
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.SUPPLIER is 'Contains the order supplier.'
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.SUP_NAME is 'Contains the translated name of the supplier in user language.'
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.POOL_SUPPLIER is 'Contains the master supplier id.'
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.LOC_TYPE is 'Location type - ''S'' store, ''W'' arehouse.'
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.LOCATION is 'Contains a store or warehouse id.'
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.LOC_NAME is 'Translated store name or warehouse name.'
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.FIRST_ORDER_TOTAL is 'Total order quantity based on the first scaling constraint.'
/

COMMENT ON COLUMN BUYER_WKSHT_PO_LIST_GTT.SECOND_ORDER_TOTAL is 'Total order quantity based on the second scaling constraint.'
/

