--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SHIPMENT'
ALTER TABLE SHIPMENT MODIFY NO_BOXES NUMBER (6,0)
/

COMMENT ON COLUMN SHIPMENT.NO_BOXES is 'Contains the number of boxes associated with the shipment.'
/

