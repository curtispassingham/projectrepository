--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


---------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
---------------------------------------------------------------------------
--	View Modified:						 V_MRT_ITEM_LOC
---------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT CREATING VIEW 'V_MRT_ITEM_LOC';
CREATE OR REPLACE FORCE VIEW V_MRT_ITEM_LOC
(mrt_no, item, loc_type, location, location_name, unit_retail, supplier_unit_retail, primary_unit_retail, unit_cost, supplier_unit_cost, primary_unit_cost, return_avail_qty, tsf_qty, restock_pct, tsf_cost, supplier_tsf_cost, primary_tsf_cost, tsf_price, supplier_tsf_price, primary_tsf_price, final_tsf_cost, supplier_final_tsf_cost, primary_final_tsf_cost, final_tsf_price, supplier_final_tsf_price, primary_final_tsf_price, total_cost, supplier_total_cost, primary_total_cost, total_retail, supplier_total_retail, primary_total_retail, received_qty,total_tsf_price,supplier_total_tsf_price,primary_total_tsf_price)
AS
SELECT mrl.mrt_no mrt_no
      ,mrl.item item
      ,mrl.loc_type loc_type
      ,mrl.location location
      ,vln.location_name location_name
      ,NVL(mrl.unit_retail,0)
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  su.currency_code,
                                  mr.currency_code,
                                  NVL(mrl.unit_retail,0)
                                  ) supplier_unit_retail
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  so.currency_code,
                                  mr.currency_code,
                                  NVL(mrl.unit_retail,0)
                                  ) primary_unit_retail
      ,NVL(mrl.unit_cost,0)
      ,CURRENCY_SQL.CONVERT_VALUE('C',
                                  su.currency_code,
                                  mr.currency_code,
                                  NVL(mrl.unit_cost,0)
                                  ) supplier_unit_cost
      ,CURRENCY_SQL.CONVERT_VALUE('C',
                                  so.currency_code,
                                  mr.currency_code,
                                  NVL(mrl.unit_cost,0)
                                  ) primary_unit_cost
      ,NVL(mrl.return_avail_qty,0) return_avail_qty
      ,NVL(mrl.tsf_qty,0) tsf_qty
      ,NVL(mri.restock_pct,0) restock_pct
      ,NVL(mrl.tsf_cost,0) tsf_cost
      ,CURRENCY_SQL.CONVERT_VALUE('C',
                                  su.currency_code,
                                  mr.currency_code,
                                  NVL(mrl.tsf_cost,0)
                                  ) supplier_tsf_cost
      ,CURRENCY_SQL.CONVERT_VALUE('C',
                                  so.currency_code,
                                  mr.currency_code,
                                  NVL(mrl.tsf_cost,0)
                                  ) primary_tsf_cost
      ,NVL(mrl.tsf_price,0) tsf_price
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  su.currency_code,
                                  mr.currency_code,
                                  NVL(mrl.tsf_price,0)
                                  ) supplier_tsf_price
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  so.currency_code,
                                  mr.currency_code,
                                  NVL(mrl.tsf_price,0)
                                  ) primary_tsf_price
      ,(NVL(mrl.tsf_cost,0) * (1 - (NVL(mri.restock_pct,0)/100))) final_tsf_cost
      ,CURRENCY_SQL.CONVERT_VALUE('C',
                                  su.currency_code,
                                  mr.currency_code,
                                  (NVL(mrl.tsf_cost,0) * (1 - (NVL(mri.restock_pct,0)/100)))
                                  ) supplier_final_tsf_cost
      ,CURRENCY_SQL.CONVERT_VALUE('C',
                                  so.currency_code,
                                  mr.currency_code,
                                  (NVL(mrl.tsf_cost,0) * (1 - (NVL(mri.restock_pct,0)/100)))
                                  ) primary_final_tsf_cost
      ,(NVL(mrl.tsf_price,0) * (1 - (NVL(mri.restock_pct,0)/100))) final_tsf_price
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  su.currency_code,
                                  mr.currency_code,
                                  (NVL(mrl.tsf_price,0) * (1 - (NVL(mri.restock_pct,0)/100)))
                                  ) supplier_final_tsf_price
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  so.currency_code,
                                  mr.currency_code,
                                  (NVL(mrl.tsf_price,0) * (1 - (NVL(mri.restock_pct,0)/100)))
                                  ) primary_final_tsf_price
      ,((NVL(mrl.tsf_cost,0) * (1-(NVL(mri.restock_pct,0)/100))) * NVL(mrl.tsf_qty,0)) total_cost
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  su.currency_code,
                                  mr.currency_code,
                                  ((NVL(mrl.tsf_cost,0) * (1-(NVL(mri.restock_pct,0)/100))) * NVL(mrl.tsf_qty,0))
                                  ) supplier_total_cost
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  so.currency_code,
                                  mr.currency_code,
                                  ((NVL(mrl.tsf_cost,0) * (1-(NVL(mri.restock_pct,0)/100))) * NVL(mrl.tsf_qty,0))
                                  ) primary_total_cost
      ,( NVL(mrl.unit_retail,0)  *  NVL(mrl.tsf_qty,0)) total_retail
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  su.currency_code,
                                  mr.currency_code,
                                  ( NVL(mrl.unit_retail,0) * NVL(mrl.tsf_qty,0))
                                  ) supplier_total_retail
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  so.currency_code,
                                  mr.currency_code,
                                  ( NVL(mrl.unit_retail,0) * NVL(mrl.tsf_qty,0))
                                  ) primary_total_retail
      ,NVL(mrl.received_qty,0) received_qty
      ,((NVL(mrl.tsf_price,0) * (1-(NVL(mri.restock_pct,0)/100))) * NVL(mrl.tsf_qty,0)) total_tsf_price
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  su.currency_code,
                                  mr.currency_code,
                                  ((NVL(mrl.tsf_price,0) * (1-(NVL(mri.restock_pct,0)/100))) * NVL(mrl.tsf_qty,0))
                                  )  supplier_total_tsf_price
      ,CURRENCY_SQL.CONVERT_VALUE('R',
                                  so.currency_code,
                                  mr.currency_code,
                                  ((NVL(mrl.tsf_price,0) * (1-(NVL(mri.restock_pct,0)/100))) * NVL(mrl.tsf_qty,0))
                                  ) primary_total_tsf_price
 from system_options so
     ,store st
     ,sups su
     ,wh wah
     ,v_location vln
     ,mrt mr
     ,mrt_item mri
     ,mrt_item_loc mrl
  WHERE mri.mrt_no       = mr.mrt_no
  and mrl.mrt_no       = mri.mrt_no
  and mrl.item        = mri.item
  and vln.location_id    = mrl.location
  and st.store(+)     = mrl.location
  and wah.wh(+)       = mrl.location
  and su.supplier(+)  = mr.supplier
  /
