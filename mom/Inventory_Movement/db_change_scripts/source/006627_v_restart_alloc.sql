--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	VIEW ADDED:				V_RESTART_ALLOC
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING VIEW
--------------------------------------
PROMPT Creating View 'V_RESTART_ALLOC'
CREATE OR REPLACE FORCE VIEW V_RESTART_ALLOC
 AS SELECT DISTINCT RC.DRIVER_NAME DRIVER_NAME,
           RC.NUM_THREADS NUM_THREADS,
           ALLOC.ALLOC_NO DRIVER_VALUE,
           RESTART_THREAD_RETURN(ALLOC.ALLOC_NO, RC.NUM_THREADS)
           THREAD_VAL
      FROM RESTART_CONTROL RC, 
           ALLOC_HEADER ALLOC
     WHERE RC.DRIVER_NAME = 'TRANSFER'
       AND ALLOC.ALLOC_PARENT IS NULL       
       AND ALLOC.ORDER_NO IS NULL
/


COMMENT ON TABLE V_RESTART_ALLOC IS 'This view extracts the restart control information for alloc.'
/

