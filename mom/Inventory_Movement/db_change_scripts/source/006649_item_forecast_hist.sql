--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 ITEM_FORECAST_HIST
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'ITEM_FORECAST_HIST'
CREATE TABLE ITEM_FORECAST_HIST
 (ITEM VARCHAR2(25 BYTE) NOT NULL,
  LOC NUMBER(10,0) NOT NULL,
  EOW_DATE DATE NOT NULL,
  FORECAST_SALES NUMBER(12,4) NOT NULL,
  FORECAST_STD_DEV NUMBER(12,4) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE ITEM_FORECAST_HIST is 'Holds 4 weeks of history of the item level forecasted sales information from the RDF extractions. It is used to support the Inventory Variance to Forecast dashboard report. This table should be partitioned according to the end of week date.'
/

COMMENT ON COLUMN ITEM_FORECAST_HIST.ITEM is 'Contains the item id. Only forecasted items will be on this table.'
/

COMMENT ON COLUMN ITEM_FORECAST_HIST.LOC is 'Contains the location corresponding to the forecasted sales information for the Item.'
/

COMMENT ON COLUMN ITEM_FORECAST_HIST.EOW_DATE is 'Contains the end of the week date from which the forecasted sales totals are gathered. Since this history holds 4 weeks of history, it will be the previous eow_date with regard to the vdate and three previous to that.'
/

COMMENT ON COLUMN ITEM_FORECAST_HIST.FORECAST_SALES is 'Contains forecasted sales units for the given item/store/date combination.  This field will only contain a value for sales_type = R (regular sales).'
/

COMMENT ON COLUMN ITEM_FORECAST_HIST.FORECAST_STD_DEV is 'Contains the standard deviation value for the given item/store/date combination.  This value represents the confidence level in the sales forecast.  This field is used in the safety stock calculations for the dynamic replenishment method. This field will only contain a value for sales_type = R (regular).'
/


PROMPT Creating Primary Key on 'ITEM_FORECAST_HIST'
ALTER TABLE ITEM_FORECAST_HIST
 ADD CONSTRAINT PK_ITEM_FORECAST_HIST PRIMARY KEY
  (ITEM,
   LOC,
   EOW_DATE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


