CREATE OR REPLACE FORCE VIEW V_WO_TMPL_HEAD_TL (WO_TMPL_ID, WO_TMPL_DESC, LANG ) AS
SELECT  b.wo_tmpl_id,
        case when tl.lang is not null then tl.wo_tmpl_desc else b.wo_tmpl_desc end wo_tmpl_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  WO_TMPL_HEAD b,
        WO_TMPL_HEAD_TL tl
 WHERE  b.wo_tmpl_id = tl.wo_tmpl_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_WO_TMPL_HEAD_TL is 'This is the translation view for base table WO_TMPL_HEAD. This view fetches data in user langauge either from translation table WO_TMPL_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_WO_TMPL_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_WO_TMPL_HEAD_TL.WO_TMPL_ID is 'This field holds a System-generated ID for the work order template.'
/

COMMENT ON COLUMN V_WO_TMPL_HEAD_TL.WO_TMPL_DESC is 'This field contains the description of a work order template.'
/

