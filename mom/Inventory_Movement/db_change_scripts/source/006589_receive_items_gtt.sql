--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RECEIVE_ITEMS_GTT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RECEIVE_ITEMS_GTT'
CREATE GLOBAL TEMPORARY TABLE RECEIVE_ITEMS_GTT
 (ITEM VARCHAR2(25 ) NOT NULL,
  SEQ_NO NUMBER(10),
  ITEM_DESC VARCHAR2(255 ),
  CARTON VARCHAR2(20 ),
  DISTRO_NO NUMBER(12),
  DISTRO_TYPE VARCHAR2(1 ),
  QTY_RECEIVED NUMBER(12,4),
  QTY_EXPECTED NUMBER(12,4),
  STANDARD_UOM VARCHAR2(4 ),
  WEIGHT_EXPECTED NUMBER(12,4),
  WEIGHT_EXPECTED_UOM VARCHAR2(4 ),
  WEIGHT_RECEIVED NUMBER(12,4),
  WEIGHT_RECEIVED_UOM VARCHAR2(4 ),
  ERROR_CODE VARCHAR2(4 ),
  RETURN_CODE VARCHAR2(4 ),
  INV_STATUS VARCHAR2(4 ),
  QTY_EXPECTED_UOM VARCHAR2(4 ),
  QTY_RECIEVED_UOM VARCHAR2(4 ),
  QTY_RECIEVED_UOM_FOR_DISPLAY VARCHAR2(4 )
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE RECEIVE_ITEMS_GTT is 'This global temporary table is used to support the Receive By Item screen. It holds the item receipts for a shipment.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.ITEM is 'Unique identifier for the item.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.SEQ_NO is 'Due to the fact that there can be multiple Transfers[Distros], Inventory Statuses, or Cartons for the same Shipment/Item, this field contains the sequence number used along with the Shipment number and Item to make the Shipment Item records unique.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.ITEM_DESC is 'Item Description'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.CARTON is 'Identifies the UCC-128 carton number for shipments originating from the Advance Shipment Notification process as carton shipments.  This field will be zero for all shipments that are not at a carton level.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.DISTRO_NO is 'This column will hold a transfer or allocation number associated with the shipment/item/carton.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.DISTRO_TYPE is 'Contains distro type that indicates the distro_no is for allocation  number or transfer number.       If A then allocation       If T then transfer'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.QTY_RECEIVED is 'Contains the number of items already received for the SKU / Shipment combination.  This value is used to compare the quantity expected for a shipment against the quantity actually received.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.QTY_EXPECTED is 'Contains the number of items expected to be received based on the associated order number or on the suppliers advance shipment notification for this SKU / Shipment combination.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.STANDARD_UOM is 'Standard unit of measurement for the item.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.WEIGHT_EXPECTED is 'This column contains the expected weight of the item on the shipment.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.WEIGHT_EXPECTED_UOM is 'This column contains the unit of measure of the expected weight.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.WEIGHT_RECEIVED is 'This column contains the actual weight of the item on received on the shipment.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.WEIGHT_RECEIVED_UOM is 'This column contains the unit of measure of the received weight.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.ERROR_CODE is 'Holds the error code if there is failure in receiving the shipment.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.RETURN_CODE is 'Holds the returns code if there is failure in receiving the shipment.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.INV_STATUS is 'Holds the inventory status of the items in shipment.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.QTY_EXPECTED_UOM is 'UOM for the expected quantity.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.QTY_RECIEVED_UOM is 'UOM for the received quantity.'
/

COMMENT ON COLUMN RECEIVE_ITEMS_GTT.QTY_RECIEVED_UOM_FOR_DISPLAY is 'UOM for the received quantity.'
/

