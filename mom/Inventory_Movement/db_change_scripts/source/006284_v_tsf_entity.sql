--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_TSF_ENTITY
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_TSF_ENTITY'

CREATE OR REPLACE FORCE VIEW V_TSF_ENTITY  AS
SELECT te.tsf_entity_id,
      v.tsf_entity_desc,
      te.secondary_desc
 FROM tsf_entity te,
      v_tsf_entity_tl v
WHERE v.tsf_entity_id = te.tsf_entity_id
/

COMMENT ON TABLE V_TSF_ENTITY IS 'This view will be used to display the transfer entity LOVs using a security policy to filter User access.'
/

