--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       TSFZONE_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE TSFZONE_TL(
LANG NUMBER(6) NOT NULL,
TRANSFER_ZONE NUMBER(4) NOT NULL,
DESCRIPTION VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TSFZONE_TL is 'This is the translation table for TSFZONE table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN TSFZONE_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN TSFZONE_TL.TRANSFER_ZONE is 'This field contains the number which uniquely identifies the transfer zone.'
/

COMMENT ON COLUMN TSFZONE_TL.DESCRIPTION is 'This field contains the name of the transfer zone.'
/

COMMENT ON COLUMN TSFZONE_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN TSFZONE_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN TSFZONE_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN TSFZONE_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE TSFZONE_TL ADD CONSTRAINT PK_TSFZONE_TL UNIQUE (
LANG,
TRANSFER_ZONE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE TSFZONE_TL
 ADD CONSTRAINT TZNT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE TSFZONE_TL ADD CONSTRAINT TZNT_TZN_FK FOREIGN KEY (
TRANSFER_ZONE
) REFERENCES TSFZONE (
TRANSFER_ZONE
)
/

