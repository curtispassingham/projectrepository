--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW MODIFIED:          V_TSFDETAIL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_TSFDETAIL'
CREATE OR REPLACE FORCE VIEW V_TSFDETAIL
 (TSF_NO
 ,TSF_SEQ_NO
 ,ITEM
 ,INV_STATUS
 ,TSF_PRICE
 ,TSF_QTY
 ,FILL_QTY
 ,SHIP_QTY
 ,RECEIVED_QTY
 ,RECONCILED_QTY
 ,DISTRO_QTY
 ,SELECTED_QTY
 ,CANCELLED_QTY
 ,SUPP_PACK_SIZE
 ,TSF_PO_LINK_NO
 ,DEFAULT_CHRGS_2_LEG_IND
 ,MBR_PROCESSED_IND
 ,PUBLISH_IND
 ,TSF_COST
 ,RESTOCK_PCT
 ,UPDATED_BY_RMS_IND)
 AS SELECT t.tsf_no,
       t.tsf_seq_no,
       t.item,
       t.inv_status,
       t.tsf_price,
       t.tsf_qty,
       t.fill_qty,
       t.ship_qty,
       t.received_qty,
       t.reconciled_qty,
       t.distro_qty,
       t.selected_qty,
       t.cancelled_qty,
       t.supp_pack_size,
       t.tsf_po_link_no,
       t.default_chrgs_2_leg_ind,
       t.mbr_processed_ind,
       t.publish_ind,
       t.tsf_cost,
       t.restock_pct,
       t.updated_by_rms_ind
  from tsfdetail t
 where exists (select 1 from v_item_master vim where vim.item = t.item)
/

COMMENT ON TABLE V_TSFDETAIL IS 'This view returns the transfer details using a security policy to filter User access'
/

