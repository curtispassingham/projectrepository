--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Modifying Table :				ALC_HEAD
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Re-Creating CHECK CONSTRAINT
--------------------------------------
PROMPT DROPPING CHECK CONSTRAINT ON 'ALC_HEAD';
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ALC_HEAD_STATUS'
     AND constraint_TYPE = 'C';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE ALC_HEAD DROP CONSTRAINT CHK_ALC_HEAD_STATUS';
  end if;
end;
/

UPDATE ALC_HEAD SET STATUS = 'PW' WHERE STATUS = 'PR'
/

PROMPT CREATING CHECK CONSTRAINT ON 'ALC_HEAD';
ALTER TABLE ALC_HEAD ADD CONSTRAINT CHK_ALC_HEAD_STATUS CHECK ((OBLIGATION_KEY IS NULL AND CE_ID IS NULL AND STATUS IN ('E', 'N')) OR (OBLIGATION_KEY IS NOT NULL AND STATUS IN ('P','PW')) OR (CE_ID IS NOT NULL AND STATUS IN ('P','PW')))
/ 
