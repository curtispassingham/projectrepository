--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View Modified:						 v_recutadj
---------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       MODIFYING VIEW
--------------------------------------
PROMPT CREATING VIEW 'V_RECUTADJ';
CREATE OR REPLACE FORCE  VIEW V_RECUTADJ 
("SEQ_NO", 
 "SHIPMENT", 
 "ITEM", 
 "ITEM_DESC", 
 "DEPOSIT_CONTAINER_IND",
 "CARTON", 
 "REF_ITEM", 
 "QTY_RECEIVED", 
 "QTY_EXPECTED", 
 "QTY_MATCHED",
 "QTY_ORDERED") 
AS 
  select min(v.seq_no) seq_no,
         nvl(v.parent_shipment, v.shipment) shipment,
         v.item item,
         max(v.item_desc) item_desc,
         max(v.deposit_container_ind) deposit_container_ind,
         v.carton carton,
         max(v.ref_item) ref_item,
         sum(v.qty_received) qty_received,
         sum(v.qty_expected) qty_expected,
         sum(v.qty_matched) qty_matched,
         avg(v.qty_ordered) qty_ordered
    from (select distinct ss.seq_no,
                 s.shipment, 
                 s.parent_shipment,
                 ss.item,
                 vim.item_desc,
                 DECODE(vim.deposit_item_type, 'A', 'Y', 'N') deposit_container_ind,
                 ss.carton,
                 ss.ref_item,
                 ss.qty_received,
                 ss.qty_expected,
                 ss.qty_matched,
                 --aggregated order qty by physical location
                 sum(ol.qty_ordered) over (partition by ss.seq_no, s.shipment, ss.item, ss.carton, s.to_loc) qty_ordered,
                 s.to_loc, 
                 s.to_loc_type
            from shipment s, 
                 shipsku ss, 
                 v_item_master vim,
                 ordloc ol
           where s.shipment = ss.shipment
             and ss.item = vim.item
             and ol.order_no = s.order_no
             and ol.item = ss.item
             and (ol.location = s.to_loc
              or exists (select 'x' 
                           from wh
                          where physical_wh = s.to_loc
                            and ol.location = wh
                            and rownum = 1))) v
        group by nvl(v.parent_shipment, v.shipment),
                 v.item,
                 v.carton;

COMMENT ON TABLE V_RECUTADJ IS 'This view extracts the shipment details for the receiver unit adjustment screen. Receipt quantity for a child shipment is summed up together with its parent shipment.'
/

COMMENT ON COLUMN V_RECUTADJ.SEQ_NO IS 'Contains the sequence no on the shipsku.'
/

COMMENT ON COLUMN V_RECUTADJ.SHIPMENT IS 'Contains the parent shipment number or the shipment number of a standalone shipment.'
/

COMMENT ON COLUMN V_RECUTADJ.ITEM IS 'Contains the item on the shipment to be receipt adjusted.'
/

COMMENT ON COLUMN V_RECUTADJ.ITEM_DESC IS 'Contains the translated description of the item on the shipment.'
/

COMMENT ON COLUMN V_RECUTADJ.DEPOSIT_CONTAINER_IND IS 'Indicates if the item on the shipment is a deposit container item.'
/

COMMENT ON COLUMN V_RECUTADJ.CARTON IS 'Contains the carton number on the shipsku.'
/

COMMENT ON COLUMN V_RECUTADJ.REF_ITEM IS 'Contains the reference item on the shipsku.'
/

COMMENT ON COLUMN V_RECUTADJ.QTY_RECEIVED IS 'Contains the total received quantity for the item.'
/

COMMENT ON COLUMN V_RECUTADJ.QTY_EXPECTED IS 'Contains the total expected quantity for the item.'
/

COMMENT ON COLUMN V_RECUTADJ.QTY_MATCHED IS 'Contains the total invoice matched quantity for the item.'
/

COMMENT ON COLUMN V_RECUTADJ.QTY_ORDERED IS 'Contains the total ordered quantity for the item at the physical to-location.'
/
