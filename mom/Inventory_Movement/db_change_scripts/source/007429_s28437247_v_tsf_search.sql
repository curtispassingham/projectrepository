--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:   V_TSF_SEARCH
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
-- CREATING VIEW V_TSF_SEARCH
--------------------------------------
PROMPT Creating View 'V_TSF_SEARCH'
CREATE OR REPLACE FORCE VIEW "V_TSF_SEARCH" 
("TSF_NO",
"TSF_TYPE",
"OVERALL_STATUS",
"FROM_LOC",
"TO_LOC",
"FROM_PHYSICAL_LOC",
"TO_PHYSICAL_LOC",
"ITEM",
"ITEM_DESC",
"FROM_LOC_TYPE",
"TO_LOC_TYPE",
"MRT",
"WF_ORDER_NO",
"RMA_NO",
"SHIPMENT",
"CREATE_DATE",
"EXP_DC_DATE",
"TSF_PO_LINK_ID",
"DIVISION",
"GROUP1",
"DEPT",
"CLASS",
"SUBCLASS",
"CONTEXT_TYPE",
"CONTEXT_VALUE",
"FINISHER_TYPE",
"FINISHER",
"LEG_1_STATUS",
"LEG_2_STATUS",
"FROM_LOC_NAME",
"TO_LOC_NAME",
"DELIVERY_DATE",
"FINISHER_NAME",
"DEPT_DESC",
"ITEM_TRANSFORMATION_IND",
"FINISHING_IND",
"MRT_DESC",
"CONTEXT_TYPE_DESC",
"EXT_REF_NO")
AS
SELECT TSF_NO,
  TSF_TYPE,
  OVERALL_STATUS,
  FROM_LOC,
  TO_LOC,
  TO_NUMBER(DECODE(FROM_LOC_TYPE,'W', (SELECT PHYSICAL_WH FROM WH WHERE WH=FROM_LOC), FROM_LOC)) FROM_PHYSICAL_LOC,
  TO_NUMBER(DECODE(TO_LOC_TYPE,'W', (SELECT PHYSICAL_WH FROM WH WHERE WH=TO_LOC), TO_LOC)) TO_PHYSICAL_LOC,
  NULL ITEM,
  NULL ITEM_DESC,
  FROM_LOC_TYPE,
  TO_LOC_TYPE,
  MRT_NO AS MRT,
  WF_ORDER_NO,
  RMA_NO,
  NULL SHIPMENT,
  CREATE_DATE,
  EXP_DC_DATE,
  NULL TSF_PO_LINK_ID,
  NULL DIVISION,
  NULL GROUP1 ,
  DEPT AS DEPT,
  NULL CLASS,
  NULL SUBCLASS,
  CONTEXT_TYPE,
  CONTEXT_VALUE,
  FINISHER_TYPE,
  FINISHER,
  LEG_1_STATUS,
  LEG_2_STATUS,
  DECODE(FROM_LOC_TYPE,'S',
  (SELECT STORE_NAME FROM V_STORE_TL WHERE STORE=FROM_LOC
  ),
  (SELECT WH_NAME FROM V_WH_TL WHERE WH=FROM_LOC
  )) FROM_LOC_NAME,
  DECODE(TO_LOC_TYPE,'S',
  (SELECT STORE_NAME FROM V_STORE_TL WHERE STORE=TO_LOC
  ),
  (SELECT WH_NAME FROM V_WH_TL WHERE WH=TO_LOC
  )) TO_LOC_NAME,
  DELIVERY_DATE,
  DECODE(FINISHER_TYPE, 'I',
  (SELECT WH_NAME FROM V_WH_TL WHERE wh = FINISHER
  ), 'E',
  (SELECT partner_desc
  FROM v_partner_tl
  WHERE partner_id = FINISHER
  AND partner_type = FINISHER_TYPE
  ),NULL) FINISHER_NAME,
  nvl2(TH.DEPT,
  (SELECT dept_name FROM v_deps_tl de WHERE de.dept =TH.DEPT
  ),NULL) DEPT_DESC,
  DECODE (
  (SELECT COUNT(*) FROM tsf_xform WHERE tsf_no = TH.TSF_NO
  ),0,'N','Y') item_transformation_ind,
  NVL2(FINISHER,'Y','N') finishing_ind,
  nvl2(MRT_NO,
  (SELECT mrt_desc
  FROM MRT
  WHERE mrt_no = TH.MRT_NO
  ),NULL) MRT_DESC,
  NVL2(context_value, DECODE(context_type,'PROM',
  (SELECT pm.name name FROM gtt_promo_temp pm WHERE promo_id = context_value
  )), NULL) CONTEXT_TYPE_DESC,
  EXT_REF_NO EXT_REF_NO
FROM V_TSFHEAD TH
/

COMMENT ON TABLE V_TSF_SEARCH IS 'This view is used for the transfer search screen in RMS Alloy. It includes all fields that can be used as transfer search criteria.'
/

COMMENT ON COLUMN V_TSF_SEARCH."TSF_NO"  IS 'This field contains a number that uniquely identifies the transfer within the system'
/

COMMENT ON COLUMN V_TSF_SEARCH."TSF_TYPE"  IS 'This field identifies the type or reason for the transfer. Valid values are:
SR - Store Requisition, CO - Customer Order, RV - RTV, CF -
Confirmation, AD - Administrative, MR - Manual Requisition, PL -
PO-Linked Transfer, BT - Book Transfer, EG - Externally Generated, IC -
Intercompany, RAC - Reallocation, AIP - AIP Generated, SIM - SIM
Generated, FO - Franchise Order, FR - Franchise Return, SG - System
Generated'
/

COMMENT ON COLUMN V_TSF_SEARCH."OVERALL_STATUS"  IS 'This field contains the status of the transfer. Valid values are: I - Input,
B - Submitted, A - Approved, S - Shipped, C - Closed, D - Deleted (will be
deleted during batch), X - Transfer is being externally closed, P - Picked,
L - Selected. Note: Statuses of B and A are reserved stock. X is a dummy
status that is only used in integration processing and should never
actually get saved to the table.'
/

COMMENT ON COLUMN V_TSF_SEARCH."FROM_LOC"  IS 'This field contains the location number of the transfer from location.
This field will contain a store, internal finisher, external finisher or
warehouse number based upon the FROM_LOC_TYPE field. An internal
finisher is a warehouse and an external finisher is a partner.'
/

COMMENT ON COLUMN V_TSF_SEARCH."TO_LOC"  IS 'This field contains the location number of the transfer to location. This
field will contain a store, internal finisher, external finisher or warehouse
number based upon the TO_LOC_TYPE field. An internal finisher is a
warehouse and an external finisher is a partner.'
/

COMMENT ON COLUMN V_TSF_SEARCH."FROM_PHYSICAL_LOC"  IS 'This field contains the physical warehouse of the from location if the from location type is a warehouse.
Otherwise, it contains the from location on the transfer, which can be a store or an external finisher.'
/

COMMENT ON COLUMN V_TSF_SEARCH."TO_PHYSICAL_LOC"  IS 'This field contains the physical warehouse of the to location if the to location type is a warehouse.
Otherwise, it contains the to location on the transfer, which can be a store or an external finisher.'
/

COMMENT ON COLUMN V_TSF_SEARCH."ITEM"  IS 'Unique alphanumeric value that identifies the item.'
/

COMMENT ON COLUMN V_TSF_SEARCH."ITEM_DESC"  IS 'Long description of the item. This description is used through out the
system to help online users identify the item. For items that have
parents, this description will default to the parents description plus any
differentiators. For items without parents, this description will default to
null.'
/

COMMENT ON COLUMN V_TSF_SEARCH."FROM_LOC_TYPE"  IS 'This field contains the location type of the from location of the transfer.
S - Store W - Warehouse (for warehouse and internal finishers) E -
External Finisher (on PARTNER table)'
/

COMMENT ON COLUMN V_TSF_SEARCH."TO_LOC_TYPE"  IS 'This field contains the location type of the to location of the transfer.
Valid values are: S - Store W - Warehouse (for warehouse and internal
finishers) E - External Finisher (on PARTNER table)'
/

COMMENT ON COLUMN V_TSF_SEARCH."MRT"  IS 'This field holds the Mass Return Transfer Number this transfer
is associated with. This is the primary key for the table MRT.'
/

COMMENT ON COLUMN V_TSF_SEARCH."WF_ORDER_NO"  IS 'This field holds the Franchise Order Number this transfer
is associated with.'
/

COMMENT ON COLUMN V_TSF_SEARCH."RMA_NO"  IS 'This field holds the Franchise Return Number this transfer
is associated with.'
/

COMMENT ON COLUMN V_TSF_SEARCH."SHIPMENT"  IS 'Contains the unique number identifying a specific shipment of goods
within the system. This number will either be system generated or
entered by the user.'
/

COMMENT ON COLUMN V_TSF_SEARCH."CREATE_DATE"  IS 'This field contains the date the transfer was created.'
/

COMMENT ON COLUMN V_TSF_SEARCH."EXP_DC_DATE"  IS 'This field is communicated to a WMS. It is the date that the transfer is
expected to be shipped a the warehouse.'
/

COMMENT ON COLUMN V_TSF_SEARCH."TSF_PO_LINK_ID"  IS 'This field contains a reference number to link the item on the transfer to
any purchase orders that have been created to allow the from location
(i.e. warehouse) on the transfer to fulfill the transfer quantity to the to
location (i.e store) on the transfer.'
/

COMMENT ON COLUMN V_TSF_SEARCH."DIVISION"  IS 'Contains the number of the division of which the group is a member.'
/

COMMENT ON COLUMN V_TSF_SEARCH."GROUP1"  IS 'Contains the number which uniquely identifies the group.'
/ 
       
COMMENT ON COLUMN V_TSF_SEARCH."DEPT"  IS 'Number identifying the department to which the item is attached. The
items department will be the same as that of its parent (and, by
transitivity, to that of its grandparent). Valid values for this field are
located on the deps table.'
/

COMMENT ON COLUMN V_TSF_SEARCH."CLASS"  IS 'Number identifying the class to which the item is attached. The items
class will be the same as that of its parent (and, by transitivity, to that of
its grandparent). Valid values for this field are located on the class table.'
/

COMMENT ON COLUMN V_TSF_SEARCH."SUBCLASS"  IS 'Number identifying the subclass to which the item is attached. The
items subclass will be the same as that of its parent (and, by transitivity,
to that of its grandparent). Valid values for this field are located on the
subclass table.'
/

COMMENT ON COLUMN V_TSF_SEARCH."CONTEXT_TYPE"  IS 'This field holds the reason code related to which a transfer is made. Valid
values are: Promotion, Customer Transfer, Store Requisition and
Repairing. Two-legged transfer can not be created for context types
Promotion, Customer Transfer and Store Requisition. For the context
type Repairing only external finisher can be selected.' 
/

COMMENT ON COLUMN V_TSF_SEARCH."CONTEXT_VALUE"  IS 'This field holds value relating to the context type, for example Promotion
Number.'
/

COMMENT ON COLUMN V_TSF_SEARCH."FINISHER_TYPE"  IS 'This field contains the location type of the to location of the transfer.'
/

COMMENT ON COLUMN V_TSF_SEARCH."FINISHER"  IS 'This field contains the location number of the transfer to location.'
/

COMMENT ON COLUMN V_TSF_SEARCH."LEG_1_STATUS"  IS 'This field contains the status of the transfer.'
/

COMMENT ON COLUMN V_TSF_SEARCH."LEG_2_STATUS"  IS 'This field contains the status of the transfer.'
/

COMMENT ON COLUMN V_TSF_SEARCH."FROM_LOC_NAME"  IS 'This field contains the location name of the transfer from location.'
/

COMMENT ON COLUMN V_TSF_SEARCH."TO_LOC_NAME"  IS 'This field contains the location name of the transfer to location.'
/

COMMENT ON COLUMN V_TSF_SEARCH."DELIVERY_DATE"  IS 'Indicates the earliest date that the transfer can be delivered to the store.'
/

COMMENT ON COLUMN V_TSF_SEARCH."FINISHER_NAME"  IS 'This field contains the location name of the transfer to location.'
/

COMMENT ON COLUMN V_TSF_SEARCH."DEPT_DESC"  IS 'Contains the name of the department. Valid values for this field are
located on the deps table.'
/

COMMENT ON COLUMN V_TSF_SEARCH."ITEM_TRANSFORMATION_IND"  IS 'This field indicates item trnasformation exists for this transfer'
/

COMMENT ON COLUMN V_TSF_SEARCH."FINISHING_IND"  IS 'This field indicates the location number of the transfer to location exist.'
/

COMMENT ON COLUMN V_TSF_SEARCH."MRT_DESC"  IS 'A descriptive name for the MRT to make identification of an MRT easier.'
/

COMMENT ON COLUMN V_TSF_SEARCH."CONTEXT_TYPE_DESC"  IS 'Contains the name of the promotion.Valid values for this field are
located on the rpm_promo table.'
/

COMMENT ON COLUMN V_TSF_SEARCH."EXT_REF_NO" IS 'This field contains the External transfer Ref no.'
/
