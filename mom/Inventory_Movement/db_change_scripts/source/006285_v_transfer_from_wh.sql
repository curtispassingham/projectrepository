--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_TRANSFER_FROM_WH
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_TRANSFER_FROM_WH'

CREATE OR REPLACE FORCE VIEW V_TRANSFER_FROM_WH ( WH, WH_NAME, WH_NAME_SECONDARY, STOCKHOLDING_IND, TSF_ENTITY_ID, ORG_UNIT_ID ) AS
SELECT WAH.WH WH
      ,V.WH_NAME WH_NAME
      ,V.WH_NAME_SECONDARY WH_NAME_SECONDARY
      ,WAH.STOCKHOLDING_IND STOCKHOLDING_IND
      ,WAH.TSF_ENTITY_ID TSF_ENTITY_ID
      ,WAH.ORG_UNIT_ID ORG_UNIT_ID 
 FROM WH WAH,
      V_WH_TL V
WHERE wah.finisher_ind = 'N' 
  AND V.WH = WAH.WH
/  

COMMENT ON TABLE V_TRANSFER_FROM_WH IS 'This view will be used to display the Transfer From Warehouse  LOVs using a security policy to filter User access.'
/
