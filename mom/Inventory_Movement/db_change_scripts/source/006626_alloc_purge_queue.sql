--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 ALLOC_PURGE_QUEUE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'ALLOC_PURGE_QUEUE'
CREATE TABLE ALLOC_PURGE_QUEUE
 (ALLOC_NO NUMBER(12,0),
  CHILD_ALLOC_NO NUMBER(12,0),
  FROM_LOC NUMBER(10,0),
  FROM_LOC_TYPE VARCHAR2(1 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE ALLOC_PURGE_QUEUE is 'This table contains the details of the allocation based on inventory existing in a warehouse to be purged from the system.'
/

COMMENT ON COLUMN ALLOC_PURGE_QUEUE.ALLOC_NO is 'This field contains the number that uniquely identifies the Allocation within the system.'
/

COMMENT ON COLUMN ALLOC_PURGE_QUEUE.CHILD_ALLOC_NO is 'This field contains the number that uniquely identifies the second leg of a two legged Allocation within the system.'
/

COMMENT ON COLUMN ALLOC_PURGE_QUEUE.FROM_LOC is 'This field contains the location number of the transfer from location. This field will contain a store, internal finisher, external finisher or warehouse number based upon the FROM_LOC_TYPE field. An internal finisher is a warehouse and an external finisher is a partner.'
/

COMMENT ON COLUMN ALLOC_PURGE_QUEUE.FROM_LOC_TYPE is 'This field contains the location type of the from location of the transfer. S - Store, W - Warehouse (for warehouse and internal finishers) E - External Finisher (on PARTNER table)'
/


PROMPT Creating Index on 'ALLOC_PURGE_QUEUE'
 CREATE INDEX ALLOC_PURGE_QUEUE_I1 on ALLOC_PURGE_QUEUE
 (ALLOC_NO
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

