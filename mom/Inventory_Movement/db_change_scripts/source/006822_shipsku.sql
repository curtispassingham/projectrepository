--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Modifying INDEX:  SHIPSKU_I4
----------------------------------------------------------------------------

whenever sqlerror exit  

--------------------------------------
--        Table               
--------------------------------------
PROMPT Modifying INDEX "SHIPSKU_I4"
DECLARE
L_table_part_exists1 number := 0;
L_table_part_exists2 number := 0;

BEGIN
SELECT count(*) INTO L_table_part_exists1
FROM USER_PART_INDEXES
WHERE TABLE_NAME='SHIPSKU' AND INDEX_NAME='SHIPSKU_I4';

SELECT count(*) INTO L_table_part_exists2
FROM USER_INDEXES
WHERE TABLE_NAME='SHIPSKU' AND INDEX_NAME='SHIPSKU_I4';

if (L_table_part_exists1 != 0 and L_table_part_exists2 = 0) then
execute immediate 'DROP INDEX SHIPSKU_I4';
execute immediate 'CREATE INDEX SHIPSKU_I4 ON SHIPSKU (SHIPMENT, ITEM, CARTON, DISTRO_NO, INV_STATUS, DISTRO_TYPE) TABLESPACE RETAIL_INDEX INITRANS 12 NOLOGGING LOCAL';
execute immediate 'ALTER INDEX SHIPSKU_I4 LOGGING';

else if (L_table_part_exists1 = 0 and L_table_part_exists2 != 0) then
execute immediate 'DROP INDEX SHIPSKU_I4';
execute immediate 'CREATE INDEX SHIPSKU_I4 ON SHIPSKU (SHIPMENT, ITEM, CARTON, DISTRO_NO, INV_STATUS, DISTRO_TYPE) TABLESPACE RETAIL_INDEX INITRANS 12 NOLOGGING';
execute immediate 'ALTER INDEX SHIPSKU_I4 LOGGING';
end if;
end if;
end;
/
