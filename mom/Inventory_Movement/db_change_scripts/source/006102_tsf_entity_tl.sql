--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       TSF_ENTITY_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE TSF_ENTITY_TL(
LANG NUMBER(6) NOT NULL,
TSF_ENTITY_ID NUMBER(10) NOT NULL,
TSF_ENTITY_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TSF_ENTITY_TL is 'This is the translation table for TSF_ENTITY table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN TSF_ENTITY_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN TSF_ENTITY_TL.TSF_ENTITY_ID is 'This field contains the number which uniquely identifies the transfer entity.'
/

COMMENT ON COLUMN TSF_ENTITY_TL.TSF_ENTITY_DESC is 'This field contains the name of the transfer entity.'
/

COMMENT ON COLUMN TSF_ENTITY_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN TSF_ENTITY_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN TSF_ENTITY_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN TSF_ENTITY_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE TSF_ENTITY_TL ADD CONSTRAINT PK_TSF_ENTITY_TL PRIMARY KEY (
LANG,
TSF_ENTITY_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE TSF_ENTITY_TL
 ADD CONSTRAINT TET_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE TSF_ENTITY_TL ADD CONSTRAINT TET_TE_FK FOREIGN KEY (
TSF_ENTITY_ID
) REFERENCES TSF_ENTITY (
TSF_ENTITY_ID
)
/

