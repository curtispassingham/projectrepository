----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View Modified:						 v_recutadj
---------------------------------------------------------------------------
PROMPT CREATING VIEW 'V_RECUTADJ';
CREATE OR REPLACE FORCE  VIEW V_RECUTADJ 
("SEQ_NO", 
 "SHIPMENT", 
 "ITEM", 
 "ITEM_DESC", 
 "STANDARD_UOM", 
 "CARTON", 
 "REF_ITEM", 
 "QTY_RECEIVED", 
 "QTY_EXPECTED", 
 "QTY_MATCHED",
 "QTY_ORDERED") 
AS 
  SELECT max(ss.seq_no) seq_no,
         nvl(s.parent_shipment, s.shipment) shipment,
         ss.item item,
         max(vim.item_desc) item_desc,
         max(vim.standard_uom) standard_uom,
         ss.carton carton,
         max(ss.ref_item) ref_item,
         sum(ss.qty_received) qty_received,
         sum(ss.qty_expected) qty_expected,
         sum(ss.qty_matched) qty_matched,
         sum(ol.qty_ordered) qty_ordered
    FROM shipment s, 
         shipsku ss, 
         v_item_master vim,
         ordloc ol
   WHERE s.shipment = ss.shipment
     AND ss.item = vim.item
     AND ol.order_no = s.order_no
     AND ol.item = ss.item
     AND (ol.location = s.to_loc
      or exists (select 'x' 
                   from wh
                  where physical_wh = s.to_loc
                    and ol.location = wh
                    and rownum = 1))
   GROUP BY NVL(s.parent_shipment, s.shipment),
            ss.item,
            ss.carton;

COMMENT ON TABLE V_RECUTADJ IS 'This view extracts the shipment details for the receiver unit adjustment screen. Receipt quantity for a child shipment is summed up together with its parent shipment.'
/

COMMENT ON COLUMN V_RECUTADJ.SEQ_NO IS 'Contains the sequence no on the shipsku.'
/

COMMENT ON COLUMN V_RECUTADJ.SHIPMENT IS 'Contains the parent shipment number or the shipment number of a standalone shipment.'
/

COMMENT ON COLUMN V_RECUTADJ.ITEM IS 'Contains the item on the shipment to be receipt adjusted.'
/

COMMENT ON COLUMN V_RECUTADJ.ITEM_DESC IS 'Contains the translated description of the item on the shipment.'
/

COMMENT ON COLUMN V_RECUTADJ.STANDARD_UOM IS 'Contains the standard unit of measure of the item on the shipment.'
/

COMMENT ON COLUMN V_RECUTADJ.CARTON IS 'Contains the carton number on the shipsku.'
/

COMMENT ON COLUMN V_RECUTADJ.REF_ITEM IS 'Contains the reference item on the shipsku.'
/

COMMENT ON COLUMN V_RECUTADJ.QTY_RECEIVED IS 'Contains the total received quantity for the item.'
/

COMMENT ON COLUMN V_RECUTADJ.QTY_EXPECTED IS 'Contains the total expected quantity for the item.'
/

COMMENT ON COLUMN V_RECUTADJ.QTY_MATCHED IS 'Contains the total invoice matched quantity for the item.'
/

COMMENT ON COLUMN V_RECUTADJ.QTY_ORDERED IS 'Contains the total ordered quantity for the item.'
/
