

/*ibexpl.pc*/
#include <retek_2.h>

EXEC SQL INCLUDE SQLCA.H;
long SQLCODE;

init_parameter parameter[] =
{
   "thread_val",             "string",   ""
};

#define NUM_INIT_PARAMETERS     (sizeof(parameter)/sizeof(init_parameter))
#define NUM_COMMIT_PARAMETERS   0

char ps_restart_thread_val[NULL_THREAD];

/* prototypes */
int init(void);
int process(void);
int unusable_indexes(char *is_table_name,
                     char *is_mode);
int truncate_table(char *is_table_name);
int pop_wh_dept_expl(void);
int pop_sim_expl(void);
int final(void);

int main(int argc, char* argv[])
{
   char* function = "main";
   int   li_init_results;   
   char  ls_log_message[NULL_ERROR_MESSAGE];
   
   if (argc < 2)
   {
      fprintf(stderr,"Usage: %s userid/passwd\n",argv[0]);
      return(FAILED);
   }

   if (LOGON(argc, argv) < 0)
      return(FAILED);

   if ((li_init_results = init()) < 0)
      gi_error_flag = 2;
   
   if (li_init_results != NO_THREAD_AVAILABLE)
   {
      if (li_init_results == OK)
      {
         if (process() < 0)
            gi_error_flag = 1;
      }

      if (final() < 0)
      {
         if (gi_error_flag == 0)
            gi_error_flag = 3;
      }
   }

   if (gi_error_flag == 2)
   {
      LOG_MESSAGE("Aborted in init");
      return(FAILED);
   }
   else if (gi_error_flag == 1)
   {
      sprintf(ls_log_message,"Thread %s - Aborted in process",ps_restart_thread_val);
      LOG_MESSAGE(ls_log_message);
      return (FAILED);
   }
   else if (gi_error_flag == 3)
   {
      sprintf(ls_log_message,"Thread %s - Aborted in final",ps_restart_thread_val);
      LOG_MESSAGE(ls_log_message);
      return (FAILED);
   }
   else if (li_init_results == NO_THREAD_AVAILABLE)
   {
      LOG_MESSAGE("Terminated - No threads available");
      return(NO_THREADS);
   }
   else
   {
      sprintf(ls_log_message,"Thread %s - Terminated Successfully",ps_restart_thread_val);
      LOG_MESSAGE(ls_log_message);
   }

   return (SUCCEEDED);
}  /* End of main() */

int init()
{
   char *function = "init";
   int li_init_return;

   li_init_return = retek_init(NUM_INIT_PARAMETERS,
                               parameter,
                               ps_restart_thread_val);

   if (li_init_return != 0)
      return(li_init_return);

   return 0;

} /* end of init() */

int process(void)
{
   char *function = "process";

   /* turn off indexes */
   if (unusable_indexes("wh_dept_expl",
                        "disable") < 0) return -1;
   if (unusable_indexes("sim_expl",
                        "disable") < 0) return -1;

   /* clear out the explosion tables */
   if (truncate_table("wh_dept_expl") < 0) return -1;
   if (truncate_table("sim_expl") < 0) return -1;

   /* repopulate the explosion tables */
   if (pop_wh_dept_expl() < 0) return -1;
   if (pop_sim_expl() < 0) return -1;

   /* rebuild indexes */
   if (unusable_indexes("wh_dept_expl",
                        "rebuild") < 0) return -1;
   if (unusable_indexes("sim_expl",
                        "rebuild") < 0) return -1;

   return 0;

} /* end of process() */

int unusable_indexes(char *is_table_name,
                     char *is_mode)
{
   char *function = "unusable_indexes";

   char ls_index_name[NULL_TABLE_NAME]="";
   char ls_dynamic_statement[255] = "";
   char ls_table_owner[NULL_TABLE_OWNER] = "";
   char ls_table_name[NULL_TABLE_NAME];
   char ls_index_owner[NULL_TABLE_OWNER] = "";

   EXEC SQL DECLARE c_indexes CURSOR FOR
      SELECT index_name,
             owner
        FROM dba_indexes
       WHERE table_owner = UPPER(:ls_table_owner)
         AND table_name  = UPPER(:ls_table_name);

   if (synonym_trace(is_table_name,
                     ls_table_owner,
                     ls_table_name) < 0)
      return -1;

   EXEC SQL OPEN c_indexes;
   if SQL_ERROR_FOUND
   {
      sprintf(err_data,"OPEN: cursor=c_indexes");
      strcpy(table,"dba_indexes");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   do  /* while !NO_DATA_FOUND */
   {

      EXEC SQL FETCH c_indexes INTO :ls_index_name,
                                    :ls_index_owner;
      if SQL_ERROR_FOUND
      {
         sprintf(err_data,"FETCH: cursor=c_indexes");
         strcpy(table,"dba_indexes");
         WRITE_ERROR(SQLCODE,function,table,err_data);
         return(-1);
      }
      else if (!NO_DATA_FOUND)
      {
         if (strcmp(is_mode, "disable") == 0)
         {
            sprintf(ls_dynamic_statement,"ALTER INDEX %s.%s UNUSABLE", 
                    ls_index_owner, ls_index_name);
            EXEC SQL EXECUTE IMMEDIATE :ls_dynamic_statement;
            if SQL_ERROR_FOUND
            {
               sprintf(err_data,"EXECUTE DYNAMIC STATEMENT %s", ls_dynamic_statement);
               WRITE_ERROR(SQLCODE,function,"",err_data);
               return(-1); 
            }
         }
         else if (!strcmp(is_mode, "rebuild"))
         {
            /* ************************************************************************* */
            /* The parallel hint is customizable for each run-time                       */
            /* environment.  The x in the following command should be used to specify    */
            /* the degree of parallism (number of query servers) used in the             */
            /* alter index command                                                       */
            /*                                                                           */
            /*  sprintf(os_dynamic_statement,"ALTER INDEX %s                             */
            /*      REBUILD PARALLEL (DEGREE x) UNRECOVERABLE",                          */
            /* ************************************************************************* */
             sprintf(ls_dynamic_statement,"ALTER INDEX %s.%s REBUILD PARALLEL (DEGREE 4) UNRECOVERABLE",
                    ls_index_owner, ls_index_name);
            EXEC SQL EXECUTE IMMEDIATE :ls_dynamic_statement;
            if SQL_ERROR_FOUND
            {
               sprintf(err_data,"EXECUTE DYNAMIC STATEMENT %s", ls_dynamic_statement);
               WRITE_ERROR(SQLCODE,function,"",err_data);
               return(-1);
            }
   
            sprintf(ls_dynamic_statement,"ALTER INDEX %s.%s NOPARALLEL",
                    ls_index_owner, ls_index_name);
            EXEC SQL EXECUTE IMMEDIATE :ls_dynamic_statement;
            if SQL_ERROR_FOUND
            {
               sprintf(err_data,"EXECUTE DYNAMIC STATEMENT %s", ls_dynamic_statement);
               WRITE_ERROR(SQLCODE,function,"",err_data);
               return(-1);
            }
         }
         else
         {
            snprintf(err_data, sizeof(err_data), "APPLICATION ERROR: Expected 'disabled' or 'rebuild', got '%s'",
                              is_mode);
            WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
            return(-1);
         }
      }
   }
   while (!NO_DATA_FOUND);

   EXEC SQL CLOSE c_indexes;
   if SQL_ERROR_FOUND
   {
      sprintf(err_data,"CLOSE: cursor=c_indexes");
      strcpy(table,"dba_indexes");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   return 0;
}

int truncate_table(char *is_table_name)
{
   char *function = "truncate_table";
   char ls_table_owner[NULL_TABLE_OWNER] = "";
   char ls_table_name[NULL_TABLE_NAME];
   char ls_dynamic_statement[255] = "TRUNCATE TABLE";
  

   if (synonym_trace(is_table_name,
                     ls_table_owner,
                     ls_table_name) < 0)
      return -1;

   sprintf(ls_dynamic_statement, "%s %s.%s", ls_dynamic_statement, ls_table_owner, ls_table_name);
   EXEC SQL EXECUTE IMMEDIATE :ls_dynamic_statement;
   if SQL_ERROR_FOUND
   {
      sprintf(err_data,"EXECUTE DYNAMIC STATEMENT %s", ls_dynamic_statement);
      strcpy(table, is_table_name);
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   return 0;
} /* end truncate_table */

int pop_wh_dept_expl(void)
{
   char *function = "pop_wh_dept_expl";

   /***************************************************************************
   * 
   *  Populate a temp table with all the IB elegible warehouses and their
   *  IB parameters exploded to all departments in the system.
   *   
   *  IB parameters can be held at three levels -- WH/DEPT, WH, SYSTEM.  The
   *  lowest level always takes precedence.
   *   
   *  -------------------------------------------------------------------------
   *   
   *  The first union handles instances where IB parameters are set up at the
   *  WH/DEPT level.
   *   
   *  The second union handles instances where IB parameters are set up at the
   *  WH but not at the WH/DEPT level.
   *   
   *  The third union handles instances where IB parameters are set up at the
   *  SYSTEM but not WH/DEPT or WH level.
   *   
   *   
   ***************************************************************************/

   EXEC SQL INSERT INTO wh_dept_expl(wh,
                                     physical_wh,
                                     repl_wh_link,
                                     dept,
                                     buyer,
                                     cost_wh_storage_meas,
                                     cost_wh_storage,
                                     cost_wh_storage_uom,
                                     cost_out_storage_meas,
                                     cost_out_storage,
                                     cost_out_storage_uom,
                                     storage_type,
                                     storage_currency,
                                     max_weeks_supply,
                                     target_roi,
                                     cost_money,
                                     RECORD_SRC)
      /* records at the wh/dept level */
      SELECT wd.wh,
             wh.physical_wh,
             wh.repl_wh_link,
             wd.dept,
             d.buyer,
             wd.cost_wh_storage_meas,
             wd.cost_wh_storage,
             wd.cost_wh_storage_uom,
             wd.cost_out_storage_meas,
             wd.cost_out_storage,
             wd.cost_out_storage_uom,
             wd.storage_type,
             wh.currency_code,
             wd.max_weeks_supply,
             wd.target_roi,
             wd.cost_money,
             'WH/DEPT'
        FROM wh wh,
             wh_dept wd,
             deps d
       WHERE wh.ib_ind = 'Y'
         AND wh.wh     = wd.wh
         AND wd.dept   = d.dept
    UNION ALL
      /* records at the wh level */
      SELECT wd.wh,
             wh.physical_wh,
             wh.repl_wh_link,
             d.dept,
             d.buyer,
             wd.cost_wh_storage_meas,
             wd.cost_wh_storage,
             wd.cost_wh_storage_uom,
             wd.cost_out_storage_meas,
             wd.cost_out_storage,
             wd.cost_out_storage_uom,
             wd.storage_type,
             wh.currency_code,
             wd.max_weeks_supply,
             wd.target_roi,
             wd.cost_money,
             'WH'
        FROM wh wh,
             wh_dept wd,
             deps d
       WHERE wh.ib_ind = 'Y'
         AND wh.wh     = wd.wh
         AND wd.dept   IS NULL
         AND NOT EXISTS (SELECT 'x'
                           FROM wh_dept wd2
                          WHERE wd2.wh   = wh.wh
                            AND Wd2.dept = d.dept)
    UNION ALL
      /* records at the system level */
      SELECT wh.wh,
             wh.physical_wh,
             wh.repl_wh_link,
             d.dept,
             d.buyer,
             so.cost_wh_storage_meas,
             so.cost_wh_storage,
             so.cost_wh_storage_uom,
             so.cost_out_storage_meas,
             so.cost_out_storage,
             so.cost_out_storage_uom,
             so.storage_type,
             so.currency_code,
             /*default to 10 years */
             NVL(so.max_weeks_supply, 520),
             so.target_roi,
             so.cost_money,
             'SYSTEM'
        FROM wh wh,
             system_options so,
             deps d
       WHERE wh.ib_ind = 'Y'
         AND so.target_roi IS NOT NULL
         AND NOT EXISTS (SELECT 'x'
                           FROM wh_dept wd
                          WHERE wd.wh   = wh.wh
                            AND wd.dept = d.dept)
         AND NOT EXISTS (SELECT 'x'
                           FROM wh_dept wd
                          WHERE wd.wh   = wh.wh
                            AND wd.dept IS NULL);
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data,"INSERT");
      strcpy(table, "wh_dept_expl");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return -1;
   }

   return 0;

} /* end pop_wh_dept_expl */

int pop_sim_expl(void)
{
   char *function = "pop_sim_expl";


   /*****************************************************************************************
   *
   * Populate a temp table with all the IB elegible WH/DEPT records from WH_DEPT_EXPL
   * exploded to all IB elgible SUP/DEPT/LOCS in the system.
   *
   * To do this we need to get all investment buy eligible suppliers from the SUP_INV_MGMT
   * table.  The problem is that this table can store information at different levels
   * depending on the supplier's inv_mgmt_lvl -- SUPS.INV_MGMT_LVL (in parenthesis).
   * 
   *    - sup (S)
   *    - sup/dept (D)
   *    - sup/loc (L)
   *    - sup/dept/loc (A)
   * 
   * If the record is not found at the level suggested by the SUPS.INV_MGMT_LVL, it
   * needs to look up the hierarchy as shown below - up to the highest level (sup).  If no
   * record exists at the sup level, its not IB elgible.
   * 
   *    - sup
   *    - sup/dept -> sup
   *    - sup/loc -> sup
   *    - sup/dept/loc -> sup/dept -> sup
   * 
   * ---------------------------------------------------------------------------------------
   * 
   * The first part gets records for supplier/dept/locs that do inventory management at
   * the supplier level.  It also gets supplier/dept/locs that do inventory management
   * at lower levels but have not actually set up inventory management parameters at
   * those lower levels.
   * 
   * The second part gets records for supplier/dept/locs that do inventory management at
   * the supplier/dept level.  It also gets supplier/dept/locs that do inventory management
   * at lower levels but have not actually set up inventory management parameters at those
   * lower levels.
   * 
   * The third part gets records for supplier/dept/locs that do inventory management at the
   * supplier/loc level and have records set up at supplier/loc.
   * 
   * The fourth part gets records for supplier/dept/locs that do inventory management at the
   * supplier/dept/loc level and have records setup up at supplier/dept/loc.
   * 
   * The results are placed in the SIM_EXPL table.
   *
   *****************************************************************************************/

   EXEC SQL INSERT INTO sim_expl (
                         supplier,
                         pool_supplier,
                         ib_order_ctrl,
                         terms,
                         duedays,
                         wh,
                         physical_wh,
                         repl_wh_link,
                         dept,
                         buyer,
                         cost_wh_storage_meas,
                         cost_wh_storage,
                         cost_wh_storage_uom,
                         cost_out_storage_meas,
                         cost_out_storage,
                         cost_out_storage_uom,
                         storage_type,
                         storage_currency,
                         supplier_currency,
                         max_weeks_supply,
                         target_roi,
                         cost_money,
                         record_src)
      /* SIM supplier level records */
      SELECT sim.supplier,
             sim.pool_supplier,
             sim.ib_order_ctrl,
             t.terms,
             t.duedays,
             wde.wh,
             wde.physical_wh,
             wde.repl_wh_link,
             wde.dept,
             wde.buyer,
             wde.cost_wh_storage_meas,
             wde.cost_wh_storage,
             wde.cost_wh_storage_uom,
             wde.cost_out_storage_meas,
             wde.cost_out_storage,
             wde.cost_out_storage_uom,
             wde.storage_type,
             wde.storage_currency,
             s.currency_code,
             wde.max_weeks_supply,
             wde.target_roi,
             wde.cost_money,
             'SUPP_LVL'
        FROM sup_inv_mgmt sim,
             sups s,
             terms t,
             wh_dept_expl wde
       WHERE sim.ib_ind                = 'Y'
         AND sim.dept                  IS NULL
         AND sim.location              IS NULL
         AND s.supplier                = sim.supplier
         AND s.terms                   = t.terms
         /* not at dept/loc lvl */
         AND NOT EXISTS (SELECT 'x'
                           FROM sup_inv_mgmt sim1
                          WHERE sim1.supplier      = sim.supplier
                            AND sim1.dept          = wde.dept
                            AND sim1.location      = wde.physical_wh)
         /* not at dept lvl */
         AND NOT EXISTS (SELECT 'x'
                           FROM sup_inv_mgmt sim2
                          WHERE sim2.supplier      = sim.supplier
                            AND sim2.dept          = wde.dept
                            AND sim2.location      IS NULL)
         /* not at loc lvl */
         AND NOT EXISTS (SELECT 'x'
                           FROM sup_inv_mgmt sim3
                          WHERE sim3.supplier      = sim.supplier
                            AND sim3.location      = wde.physical_wh
                            AND sim3.dept          IS NULL)
    UNION ALL
      /* SIM dept level records */
      SELECT sim.supplier,
             sim.pool_supplier,
             sim.ib_order_ctrl,
             t.terms,
             t.duedays,
             wde.wh,
             wde.physical_wh,
             wde.repl_wh_link,
             wde.dept,
             wde.buyer,
             wde.cost_wh_storage_meas,
             wde.cost_wh_storage,
             wde.cost_wh_storage_uom,
             wde.cost_out_storage_meas,
             wde.cost_out_storage,
             wde.cost_out_storage_uom,
             wde.storage_type,
             wde.storage_currency,
             s.currency_code,
             wde.max_weeks_supply,
             wde.target_roi,
             wde.cost_money,
             'DEPT_LVL'
        FROM sup_inv_mgmt sim,
             sups s,
             terms t,
             wh_dept_expl wde
       WHERE sim.ib_ind                = 'Y'
         AND sim.dept                  = wde.dept
         AND sim.location              IS NULL
         AND s.supplier                = sim.supplier
         AND s.inv_mgmt_lvl            IN ('D', 'A')
         AND s.terms                   = t.terms
         /* not at dept/loc lvl */
         AND NOT EXISTS (SELECT 'x'
                           FROM sup_inv_mgmt sim2
                          WHERE sim2.supplier      = sim.supplier
                            AND sim2.dept          = wde.dept
                            AND sim2.location      = wde.physical_wh)
    UNION ALL
      /* SIM loc level records */
      SELECT sim.supplier,
             sim.pool_supplier,
             sim.ib_order_ctrl,
             t.terms,
             t.duedays,
             wde.wh,
             wde.physical_wh,
             wde.repl_wh_link,
             wde.dept,
             wde.buyer,
             wde.cost_wh_storage_meas,
             wde.cost_wh_storage,
             wde.cost_wh_storage_uom,
             wde.cost_out_storage_meas,
             wde.cost_out_storage,
             wde.cost_out_storage_uom,
             wde.storage_type,
             wde.storage_currency,
             s.currency_code,
             wde.max_weeks_supply,
             wde.target_roi,
             wde.cost_money,
             'LOC_LVL'
        FROM sup_inv_mgmt sim,
             sups s,
             terms t,
             wh_dept_expl wde
       WHERE sim.ib_ind                = 'Y'
         AND sim.dept                  IS NULL
         AND sim.location              = wde.physical_wh
         AND s.supplier                = sim.supplier
         AND s.inv_mgmt_lvl            = 'L'
         AND s.terms                   = t.terms
    UNION ALL
      /* SIM all level records */
      SELECT sim.supplier,
             sim.pool_supplier,
             sim.ib_order_ctrl,
             t.terms,
             t.duedays,
             wde.wh,
             wde.physical_wh,
             wde.repl_wh_link,
             wde.dept,
             wde.buyer,
             wde.cost_wh_storage_meas,
             wde.cost_wh_storage,
             wde.cost_wh_storage_uom,
             wde.cost_out_storage_meas,
             wde.cost_out_storage,
             wde.cost_out_storage_uom,
             wde.storage_type,
             wde.storage_currency,
             s.currency_code,
             wde.max_weeks_supply,
             wde.target_roi,
             wde.cost_money,
             'ALL_LVL'
        FROM sup_inv_mgmt sim,
             sups s,
             terms t,
             wh_dept_expl wde
       WHERE sim.ib_ind                = 'Y'
         AND sim.dept                  = wde.dept
         AND sim.location              = wde.physical_wh
         AND s.supplier                = sim.supplier
         AND s.inv_mgmt_lvl            = 'A'
         AND s.terms                   = t.terms;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data,"INSERT");
      strcpy(table, "sim_expl");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return -1;
   }

   return 0;

} /* end pop_sim_expl */

int final(void)
{
   char *function = "final";

   return(retek_close());

} /* end final() */
