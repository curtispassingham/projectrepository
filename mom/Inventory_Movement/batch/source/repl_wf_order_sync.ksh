#! /bin/ksh
#-------------------------------------------------------------------------
#  Desc:  UNIX shell script calls a plsql function that will
#         create the accompanying Franchise orders for allocations created in replenishment.
#-------------------------------------------------------------------------
pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate

OK=0
FATAL=255

USAGE="Usage: `basename $0` [-d <# date MMDDYYYY>] <connect> "

#-------------------------------------------------------------------------
# Function Name: LOG_ERROR
# Purpose      : Log the error messages to the error file.
#-------------------------------------------------------------------------
function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: LOG_MESSAGE
# Purpose      : Log the  messages to the log file.
#-------------------------------------------------------------------------
function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code   := 0;
      EXEC :GV_script_error  := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: REPL_SYNC_F_ORDER
# Purpose      : calls the package WF_ALLOC_SQL.REPL_SYNC_F_ORDER
#-------------------------------------------------------------------------

function REPL_SYNC_F_ORDER
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_vdate           PERIOD.VDATE%TYPE;
      BEGIN

         select DECODE('$PDATE', '99999999', vdate, to_date('$PDATE', 'MMDDYYYY'))
           into L_vdate
           from period;

         if NOT WF_ALLOC_SQL.REPL_SYNC_F_ORDER(:GV_script_error,
                                                L_vdate,
                                                NULL) then
            raise FUNCTION_ERROR;
         end if;

         commit;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "WF_ALLOC_SQL.REPL_SYNC_F_ORDER Failed" >>${ERRORFILE}
      return ${FATAL}
   else
      LOG_MESSAGE "Successfully Completed"
      return ${OK}
   fi
}

#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

#default variables
PDATE=99999999

# Parse the command line
while getopts d: CMD
   do
      case $CMD in
         d)  PDATE=$OPTARG;;
         *)  echo $0: Unknown option $OPTARG
             echo -e $USAGE
             exit 1;;
      esac
   done

shift $OPTIND-1

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   echo $USAGE
   exit 1
elif [[ ! -z $PDATE && ${#PDATE} -ne 8 ]];
then
   echo $USAGE
   exit 1
fi

CONNECT=$1
USER=${CONNECT%/*}

LOG_MESSAGE "Started by ${USER}"

nparam=`wc -l $ERRORFILE | awk '{print $1}'`

$ORACLE_HOME/bin/sqlplus -s $CONNECT<<EOF >>$ERRORFILE
EOF

if [ `cat $ERRORFILE | wc -l` -gt nparam ]
then
   LOG_MESSAGE "Exiting due to ORA/LOGIN Error. Check error file."
   exit 1;
fi

REPL_SYNC_F_ORDER

exit 0
