#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  rplext.ksh
#
#  Desc:  This shell script will be used to extract the replenishment order.
#         The extracted order are stored in ORD_TMP and REPL_RESULTS table.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='rplext.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
lPid=${pgmPID}
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
traceLog="${MMHOME}/log/${pgmName}_${pgmPID}_tracefiles.log"

OK=0
FATAL=255

VALID_Y=Y
VALID_N=N

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <last_run_of_day> <restart_ind> <sql_trace> &

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <last_run_of_day> Indicate last run of this shell script for the day. Valid values are Y or N.

   <restart_ind> Valid values are Y - continue processing where it left off after an error
                               or N - process a new sets of data.

   <sql trace> optional parameter. (valid values are:
               SQL_TRACE1,  -- no binds and waits
               SQL_TRACE4,  -- trace with binds
               SQL_TRACE8,  -- trace with waits
            or SQL_TRACE12).-- trace witn binds and waits
"
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_result         NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_result := 0;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_MESSAGE "Aborted in: " "EXEC_SQL" ${OK} ${LOGFILE} ${pgmName} ${lPid}
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: GET_THREADS_WH
# Purpose      : Retrieve the number of threads for WH initial processing setup.
#-------------------------------------------------------------------------
function GET_THREADS_WH
{
set -A restart_arr_wh `$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
set pause off
set pagesize 0
set feedback off
set verify off
set heading off
set echo off
select DISTINCT thread_id
  from svc_repl_roq
 where i_locn_type = 'W'
   and i_stock_cat = 'W'
   and i_repl_method in ('D','T')
 order by thread_id;
exit;
EOF`
}

#-------------------------------------------------------------------------
# Function Name: GET_THREADS
# Purpose      : Retrieve the number of threads.
#-------------------------------------------------------------------------
function GET_THREADS
{
set -A restart_arr `$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
set pause off
set pagesize 0
set feedback off
set verify off
set heading off
set echo off
select DISTINCT thread_id
  from svc_repl_roq
 order by thread_id;
exit;
EOF`
}

#-------------------------------------------------------------------------
# Function Name: TRUNCATE_TABLE
# Purpose      : 
#-------------------------------------------------------------------------
function TRUNCATE_TABLE
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_tbl_owner       USER_SYNONYMS.TABLE_OWNER%TYPE             := NULL;

         cursor C_GET_TBL_OWNER is
            select table_owner
              from user_synonyms
             where table_name = 'SVC_REPL_ROQ';
      BEGIN

         open C_GET_TBL_OWNER;
         fetch C_GET_TBL_OWNER into L_tbl_owner;
         close C_GET_TBL_OWNER;

         if  L_tbl_owner is NULL then
            execute immediate 'truncate table svc_repl_roq';
         else
            execute immediate 'truncate table '||L_tbl_owner||'.svc_repl_roq';
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
        LOG_ERROR "TRUNCATE_TABLE function Failed." "TRUNCATE_TABLE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
        return ${FATAL}
   else
        LOG_MESSAGE "Successfully Completed" "TRUNCATE_TABLE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        return ${OK}
   fi
}

#-------------------------------------------------------------------------
# Function Name: SETUP_DATA
# Purpose      : calls the package CORESVC_REPL_EXT_SQL.SETUP_DATA
#-------------------------------------------------------------------------
function SETUP_DATA
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_chunk_size      RMS_PLSQL_BATCH_CONFIG.MAX_CHUNK_SIZE%TYPE := $1;
         L_last_run        VARCHAR2(1)                                := '$2';

      BEGIN

         if NOT CORESVC_REPL_EXT_SQL.SETUP_DATA( :GV_script_error,
                                                 L_chunk_size,
                                                 L_last_run ) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL_T "$sqlTxt" ${CONNECT} ${SQL_TRACE} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID} "SETUP_DATA"

   if [[ $? -ne ${OK} ]]; then
        LOG_ERROR "CORESVC_REPL_EXT_SQL.SETUP_DATA Chunk Size: $1 Failed." "SETUP_DATA" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
        return ${FATAL}
   else
        LOG_MESSAGE "Chunk Size $1 - Successfully Completed" "SETUP_DATA" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        return ${OK}
   fi
}

#-------------------------------------------------------------------------
# Function Name: CALL_REPLROQ
# Purpose      : calls the package CORESVC_REPL_EXT_SQL.CALL_REPLROQ to
#              : setup data for WH loc/stock_cat.
#-------------------------------------------------------------------------
function CALL_REPLROQ
{
   sqlTxt="
      DECLARE
         L_last_run      VARCHAR2(1)  := '$2';
         FUNCTION_ERROR  EXCEPTION;

      BEGIN

         if NOT CORESVC_REPL_EXT_SQL.CALL_REPLROQ (:GV_script_error,
                                                   $1,
                                                   L_last_run)then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL_T "$sqlTxt" ${CONNECT} ${SQL_TRACE} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID} "CALL_REPLROQ_$1"

   if [[ $? -ne ${OK} ]]; then
        LOG_MESSAGE "Aborted in: " "CALL_REPLROQ" ${OK} ${LOGFILE} ${pgmName} ${lPid}
        LOG_ERROR "CORESVC_REPL_EXT_SQL.CALL_REPLROQ Thread: $1 Failed." "CALL_REPLROQ" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
        return ${FATAL}
   else
        LOG_MESSAGE "Thread ID $1 - Successfully Completed" "CALL_REPLROQ" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        return ${OK}
   fi
}


#-------------------------------------------------------------------------
# Function Name: PROCESS_ROQ
# Purpose      : calls the package CORESVC_REPL_EXT_SQL.CONSUME.
#-------------------------------------------------------------------------
function PROCESS_ROQ
{
   sqlTxt="
      DECLARE
         L_last_run      VARCHAR2(1)  := '$2';
         FUNCTION_ERROR  EXCEPTION;

      BEGIN

         if NOT CORESVC_REPL_EXT_SQL.CONSUME (:GV_script_error,
                                              $1,
                                              L_last_run)then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL_T "$sqlTxt" ${CONNECT} ${SQL_TRACE} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID} "PROCESS_ROQ_$1"

   if [[ $? -ne ${OK} ]]; then
        LOG_MESSAGE "Aborted in: " "PROCESS_ROQ" ${OK} ${LOGFILE} ${pgmName} ${lPid}
        LOG_ERROR "CORESVC_REPL_EXT_SQL.CONSUME Thread: $1 Failed." "PROCESS_ROQ" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
        return ${FATAL}
   else
        LOG_MESSAGE "Thread ID $1 - Successfully Completed" "PROCESS_ROQ" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        return ${OK}
   fi
}

#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 3 ]
then
   USAGE
   exit 1
elif [ $# -lt 4 ]
then
   SQL_TRACE="SQL_TRACE0"
elif [ $# -eq 4 ]
then
   case "$4" in
      "SQL_TRACE1") SQL_TRACE=$4
      ;;
      "SQL_TRACE4") SQL_TRACE=$4
      ;;
      "SQL_TRACE8") SQL_TRACE=$4
      ;;
      "SQL_TRACE12") SQL_TRACE=$4
      ;;
      *) USAGE
         exit 1
      ;;
   esac
else
   USAGE
   exit 1
fi

CONNECT=$1
if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "rplext.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

last_run_of_day=$2
#Validate if last run of day is a valid value and/or passed as NULL
if [[ $last_run_of_day != $VALID_Y ]]; then
   if [[ $last_run_of_day != $VALID_N ]]; then
   LOG_ERROR "Invalid Last Run of Day - passed value is ${last_run_of_day} - valid values either Y or N" "INIT" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
   fi
fi

restart_ind=$3
#Validate if restart indicator is a valid value and/or passed as NULL
if [[ $restart_ind != $VALID_Y ]]; then
   if [[ $restart_ind != $VALID_N ]]; then
   LOG_ERROR "Invalid Restart Indicator - passed value is ${restart_ind} - valid values either Y or N" "INIT" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
   fi
fi

#------------------------------------------------------------
# Initialize chunksize.
# Retrieve from the rms_plsql_batch_config table
#------------------------------------------------------------
chunksize=""
chunksize=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select max_chunk_size
  from rms_plsql_batch_config
 where program_name = 'CORESVC_REPL_EXT_SQL';
exit;
EOF`
# Check the maximum chunk size
if [ -z $chunksize ]; then
   LOG_ERROR "Unable to retrieve the maximum chunk size from the rms_plsql_batch_config table." "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
else
   LOG_MESSAGE "The maximum of chunk size for processing last_run_of_day ${last_run_of_day} is ${chunksize}." "" ${OK} ${LOGFILE}  ${pgmName} ${pgmPID}
fi

#------------------------------------------------------------
# Initialize number of parallel threads.
# Retrieve from the rms_plsql_batch_config table
#------------------------------------------------------------
parallelThreads=""
parallelThreads=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select max_concurrent_threads
  from rms_plsql_batch_config
 where program_name = 'CORESVC_REPL_EXT_SQL';
exit;
EOF`
# Check the number of threads
if [ -z $parallelThreads ]; then
   LOG_ERROR "Unable to retrieve the number of threads from the rms_plsql_batch_config table." "MAIN" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
else
   LOG_MESSAGE "The number of concurrent threads for processing last_run_of_day ${last_run_of_day} is ${parallelThreads}." "" ${OK} ${LOGFILE}  ${pgmName} ${pgmPID}
fi

#------------------------------------------------------------
# SETUP DATA
#------------------------------------------------------------
if [[ $restart_ind !=  $VALID_Y ]]; then

   TRUNCATE_TABLE

   SETUP_DATA $chunksize $last_run_of_day

   GET_THREADS_WH

   #------------------------------------------------------------------------------
   # Start to invoke call_replroq based on Thread ID
   #------------------------------------------------------------------------------
   NUM_THREADS_WH=${#restart_arr_wh[*]}
   
   if [ NUM_THREADS_WH -lt 1 ]; then
     LOG_MESSAGE "No WH loc/stock_cat threads to be processed for this last run of the day: $last_run_of_day" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
     if [ -e $ERRORFILE ]
     then
        if [ `wc -l $ERRORFILE | awk '{print $1}'` -eq 0 ]
        then
           rm -f $ERRORFILE
        fi
     fi
   else
      arr_index=0
      
      while [ $arr_index -lt  NUM_THREADS_WH ]
      do
       if [ `jobs | wc -l` -lt ${parallelThreads} ]
         then
            (
              CALL_REPLROQ  ${restart_arr_wh[arr_index]} $last_run_of_day
            ) &
         else
            # Loop until a thread becomes available
            while [ `jobs | wc -l` -ge ${parallelThreads} ]
            do
               sleep 1
            done
            (
              CALL_REPLROQ ${restart_arr_wh[arr_index]} $last_run_of_day
            ) &
         fi
        let arr_index=$arr_index+1;
      done
      # Wait for all threads to complete
      wait
      # Check for any Oracle errors from the SQLPLUS process
      if [ `grep "${lPid}: Aborted" $LOGFILE | wc -l` -gt 0 ]
      then
         LOG_MESSAGE "Errors encountered. See error file." "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
         exit 1
      fi
   fi
   #------------------------------------------------------------------------------
fi

#------------------------------------------------------------
# Get NUM_THREADS
#------------------------------------------------------------
GET_THREADS

#------------------------------------------------------------------------------
# Start to invoke process roq based on Thread ID
#------------------------------------------------------------------------------
NUM_THREADS=${#restart_arr[*]}

if [ NUM_THREADS -lt 1 ]; then
  LOG_MESSAGE "No threads to be processed for this last run of the day: $last_run_of_day" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
  if [ -e $ERRORFILE ]
  then
     if [ `wc -l $ERRORFILE | awk '{print $1}'` -eq 0 ]
     then
        rm -f $ERRORFILE
     fi
  fi

  if [ -e $traceLog ]
  then
     if [ `wc -l $traceLog | awk '{print $1}'` -gt 0 ]
     then
        LOG_MESSAGE "SQL TRACE Level ${SQL_TRACE##*E} files created." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        LOG_MESSAGE "Please see ${pgmName}_${pgmPID}_tracefiles.log in the LOG directory for the listed trace files." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
     fi
  fi
  exit 0
fi

arr_index=0

while [ $arr_index -lt  NUM_THREADS ]
do
 if [ `jobs | wc -l` -lt ${parallelThreads} ]
   then
      (
        PROCESS_ROQ  ${restart_arr[arr_index]} $last_run_of_day
      ) &
   else
      # Loop until a thread becomes available
      while [ `jobs | wc -l` -ge ${parallelThreads} ]
      do
         sleep 1
      done
      (
        PROCESS_ROQ ${restart_arr[arr_index]} $last_run_of_day
      ) &
   fi
  let arr_index=$arr_index+1;
done

# Wait for all threads to complete
wait
#------------------------------------------------------------------------------

# Check for any Oracle errors from the SQLPLUS process
if [ `grep "${lPid}: Aborted" $LOGFILE | wc -l` -gt 0 ]
then
   LOG_MESSAGE "Errors encountered. See error file." "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   if [ -e $traceLog ]
   then
      if [ `wc -l $traceLog | awk '{print $1}'` -gt 0 ]
      then
         LOG_MESSAGE "SQL TRACE Level ${SQL_TRACE##*E} files created." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
         LOG_MESSAGE "Please see ${pgmName}_${pgmPID}_tracefiles.log in the LOG directory for the listed trace files." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      fi
   fi
   LOG_MESSAGE "Program rplext.ksh terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi
#-----------End of Processing -------------------------------------------------

exit 0
