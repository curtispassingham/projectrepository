#!/bin/ksh
#-------------------------------------------------------------------------
#  File:  batch_reqext.ksh
#
#  Desc:  UNIX shell script to run the reqext program multi-threaded.
#-------------------------------------------------------------------------

# Initialize number of parallel threads
SLOTS=0

. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='batch_reqext.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

USAGE="Usage: `basename $0` [-p <# parallel threads>] <connect> <last run ind> [<reqext options>] \n
The valid values of last run of the day indicator is Y/N.\n
<# parallel threads> is the number of costcompupd threads to run in parallel.\n
The number of reqext threads should be equal to the\n
number of partitions on the rpl_net_inventory_tmp table.\n
The default is the value on RESTART_CONTROL.NUM_THREADS.\n"

while getopts :p: CMD
   do
      case $CMD in
         p)  SLOTS=$OPTARG;;
         *)  echo $0: Unknown option $OPTARG
             echo $USAGE
             exit 1;;
      esac
   done

shift $OPTIND-1

# Test for the number of input arguments
if [ $# -lt 2 ]
then
   echo $USAGE
   exit 1
fi

CONNECT=$1
LAST_RUN=$2

if [ $LAST_RUN != "Y" -a $LAST_RUN != "N" ]
then
   echo The valid values of last run of the day indicator is Y/N
   exit 1
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "batch_reqext.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

if [ $SLOTS -eq 0 -o $SLOTS -lt 1 ]
then

   # Get the number of threads
   SLOTS=`$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF 
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select num_threads
     from restart_control
    where program_name = 'reqext';
EOF`
fi

# Adjust for any remaining arguments to be passed to reqext
shift 2
ADDL_OPTIONS=${1:+$@}

# Set filename to contain this runs partition list
PARTITIONS=reqext_partitions.$$

# Set filename to flag any failed executions
failed=reqext.$$
[ -f $failed ] && rm $failed

# If this script is killed, cleanup
trap "kill -15 0; rm -f $failed ; exit 15" 1 2 3 15

# Get the list of partitions to be processed
[ -f $PARTITIONS ] && rm $PARTITIONS
$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$PARTITIONS
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select partition_position
  from all_tab_partitions
 where table_name like 'RPL_NET_INVENTORY_TMP'
   and table_owner = nvl((select table_owner
                        from user_synonyms
                       where synonym_name like 'RPL_NET_INVENTORY_TMP'),USER)
 order by partition_position;
EOF

#Check for any Oracle errors from the SQLPLUS process
[ `grep "^ORA-" $PARTITIONS | wc -l` -gt 0 ] && exit 1

# Run the reqext program in parallel each processing a partition
cat $PARTITIONS | while read partition_position
do
   if [ `jobs | wc -l` -lt $SLOTS ]
   then
      (
       reqext $CONNECT $partition_position $LAST_RUN $ADDL_OPTIONS || touch $failed;
      ) &
   else
      # Loop until a thread becomes available
      while [ `jobs | wc -l` -ge $SLOTS ]
      do
         sleep 1

      done
      (
       reqext $CONNECT $partition_position $LAST_RUN $ADDL_OPTIONS || touch $failed;
      ) &
   fi
done

# Wait for all of the threads to complete
wait


# Remove the list of partitions file
rm $PARTITIONS

# Check for the existence of a failed file from any of the threads
# and determine exit status
if [ -f $failed ]
then
   rm $failed
   exit 1
else
   exit 0
fi

