#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <std_len.h>
#include <oci.h>
#include <pthread.h>

#define DELIMITER "~"
#define LEN_ERR_MSG 512
#define NULL_ERR_MSG 513

static int gi_sql_trace_flag = 0;

void CheckError();
void ReportError();
int WriteError();
void OciTime();
void ParseUid();
sword SetEnv();

/* Prototypes of internal use functions */
int get_option(int   argc,
               char* argv[],
               char* option_name,
               int*  oi_flag);

void CheckError(OCIError *errhp, int status)
{
   char ls_error_message[NULL_ERR_MSG];

   switch (status)
   {
   case OCI_SUCCESS: break;
   case OCI_SUCCESS_WITH_INFO:
     ReportError(errhp, ls_error_message);
     break;
   case OCI_NEED_DATA:
     break;
   case OCI_NO_DATA:
     break;
   case OCI_ERROR:
     ReportError(errhp, ls_error_message);
     break;
   case OCI_INVALID_HANDLE:
     break;
   case OCI_STILL_EXECUTING:
     break;
   case OCI_CONTINUE:
     break;
   default:
     break;
  }
}

void ReportError(OCIError *errhp, char *os_err_message)
{
  char  msgbuf[512];
  sb4   errcode = 0;

  memset((void *) msgbuf, (int)'\0', (size_t)512);

  OCIErrorGet((dvoid *) errhp, (ub4) 1, (dvoid *) NULL, &errcode, (dvoid*) msgbuf, (ub4) sizeof(msgbuf), (ub4) OCI_HTYPE_ERROR);

  if (errcode) strcpy(os_err_message, msgbuf);
}

void ParseUid(char *logon, char *os_username, char *os_password, char *os_dbname)
{
   char *username = "";
   char *password = "";
   char *dbname   = "";
   char *dummy    = "";

   if(strchr(logon,'@'))
   {
      
      /* is using OS Authentication don't look for the username and password */
      if(logon[0] == '/')
      {
         dummy    = (char*) strtok(logon,"@");
         dbname   = (char*) strtok(NULL,"@");
      }
      else
      {
         username = (char*) strtok(logon,"/");
         password = (char*) strtok(NULL,"@");
         dbname   = (char*) strtok(NULL,"@");
      }

      strcpy(os_dbname, dbname);
   }
   else
   {
      username = (char*) strtok(logon,"/");
      password = (char*) strtok(NULL,"/");

      strcpy(os_dbname, "");
   }

   strcpy(os_username, username);
   strcpy(os_password, password);
}

static int log_open;
static FILE *log_file;
static char program_name[20] = "";

int LogMessage(char *message_text)
{
   char clock[26];
   char *log_name;
   /*Dynamic memory allocation is required for log_name as MMHOME can be greater than 100*/
   log_name= (char *)malloc (strlen((char *)getenv("MMHOME")) + 57);
   /* Extra size for 56 is for clock and /log/ =5+25+26  and 1 for NULL*/
   if (!log_open)
   {
      strcpy(log_name, (char *)getenv("MMHOME"));
      strcat(log_name, "/log/");

      OciTime("%h_%d.log", 25, clock);
      strcat(log_name, clock);

      log_file = fopen(log_name, "a");
      if (log_file == NULL) fprintf(stderr, "WARNING - cannot open log file\n");
      chmod(log_name, 00666);
      log_open = 1;
   }

   OciTime("%a %b %e %T", 25, clock);
   strcat(log_name, clock);

   fprintf(log_file, "%s Program: %s: %s\n", clock, program_name, message_text);
   fflush(log_file);
   free(log_name);
   
   return(0);
}

int OciConnect(OCIEnv     **envhp,
               OCIError   **errhp,
               OCIServer  **svrhp,
               OCISvcCtx  **svchp,
               OCISession **seshp,
               char *username,
               char *password,
               char *dbname)
{
   char *function = "OciConnect";
   char ls_error_message[NULL_ERROR_MESSAGE];

   memset((void *) ls_error_message, (int)'\0', (size_t) NULL_ERROR_MESSAGE);

   if(OCIEnvCreate((OCIEnv **)envhp, OCI_DEFAULT, (dvoid *)0, (dvoid * (*)(dvoid *, size_t))0,
                   (dvoid * (*)(dvoid *, dvoid *, size_t))0,
                   (void (*)(dvoid *, dvoid *))0,(size_t)0, (dvoid **)0))
   {
      WriteError(function, "OCIEnvInit()", "Failed to Initialize Environment");
      return OCI_ERROR;
   }
  
   /* Allocate the Error Handle for this Environment */
   if(OCIHandleAlloc((OCIEnv **) *envhp, (dvoid **) errhp, OCI_HTYPE_ERROR, (size_t) 0, (dvoid **) 0))
   {
      strcpy(ls_error_message, "Unable to allocate Error Handle");
      WriteError(function, "OCIHandleAlloc()~OCI_HTYPE_ERROR", ls_error_message);
      return OCI_ERROR;
   }

   /* Allocate the Server Handle for this Environment */
   if(OCIHandleAlloc((OCIEnv **) *envhp, (dvoid **) svrhp, OCI_HTYPE_SERVER, (size_t) 0, (dvoid **) 0))
   {
      strcpy(ls_error_message, "Unable to allocate Server Handle");
      WriteError(function, "OCIHandleAlloc()~OCI_HTYPE_SERVER", ls_error_message);
      return OCI_ERROR;
   }

   /* Allocate the Service Handle for this Environment */
   if(OCIHandleAlloc((OCIEnv **) *envhp, (dvoid **) svchp, OCI_HTYPE_SVCCTX, (size_t) 0, (dvoid **) 0))
   {
      strcpy(ls_error_message, "Unable to allocate Service Handle");
      WriteError(function, "OCIHandleAlloc()~OCI_HTYPE_SVCCTX", ls_error_message);
      return OCI_ERROR;
   }

   /* Allocate the Session Handle for this Environment */
   if(OCIHandleAlloc((OCIEnv **) *envhp, (dvoid **) seshp, OCI_HTYPE_SESSION, (size_t) 0, (dvoid **) 0))
   {
      strcpy(ls_error_message, "Unable to allocate Session Handle");
      WriteError(function, "OCIHandleAlloc()~OCI_HTYPE_SESSION", ls_error_message);
      return OCI_ERROR;
   }

   /* Attach to Server */
   if(OCIServerAttach((OCIServer *) *svrhp, (OCIError *) *errhp, 
                      (dvoid *) dbname, (sb4) strlen((char *)dbname), (ub4) OCI_DEFAULT)) 
   {
      ReportError((OCIError *) *errhp, ls_error_message);
      WriteError(function, "OCIServerAttach()", ls_error_message);
      return OCI_ERROR;
   }

   /* Set server attribute to Service Context */
   if(OCIAttrSet((dvoid *) *svchp, (ub4) OCI_HTYPE_SVCCTX, 
                 (dvoid *) *svrhp, (ub4) 0, (ub4) OCI_ATTR_SERVER, 
                 (OCIError *) *errhp)) 
   {
      ReportError((OCIError *) *errhp, ls_error_message);
      WriteError(function, "OCIAttrSet()~OCI_HTYPE_SVCCT", ls_error_message);
      return OCI_ERROR;
   }

   if(!strcmp(username,""))
   {
      /* If using OS Authentication no username or password is required */
      if(OCISessionBegin((OCISvcCtx *) *svchp, (OCIError *) *errhp, 
                         (OCISession *) *seshp, (ub4) OCI_CRED_EXT, 
                         (ub4) OCI_DEFAULT)) 
      {
         ReportError((OCIError *) *errhp, ls_error_message);
         WriteError(function, "OCISessionBegin()~OCI_CRED_EXT", ls_error_message);
         return OCI_ERROR;
      } 
   }
   else
   {
      /* If using Oracle Authenication set the username or password as required */
      if(OCIAttrSet((dvoid *) *seshp, (ub4) OCI_HTYPE_SESSION, 
                    (dvoid *) username, (ub4) strlen((char *)username), 
                    (ub4) OCI_ATTR_USERNAME, (OCIError *) *errhp)) 
      { 
         ReportError((OCIError *) *errhp, ls_error_message);
         WriteError(function, "OCIAttrSet()~OCI_ATTR_USERNAME", ls_error_message);
         return OCI_ERROR;
      }

      /* Set password attribute to Session */
      if(OCIAttrSet((dvoid *) *seshp, (ub4) OCI_HTYPE_SESSION, 
                    (dvoid *) password, (ub4) strlen((char *)password), 
                    (ub4) OCI_ATTR_PASSWORD, (OCIError *) *errhp)) 
      { 
         ReportError((OCIError *) *errhp, ls_error_message);
         WriteError(function, "OCIAttrSet()~OCI_ATTR_PASSWORD", ls_error_message);
         return OCI_ERROR;
      }

      /* Begin a Session  */
      if(OCISessionBegin((OCISvcCtx *) *svchp, (OCIError *) *errhp, 
                         (OCISession *) *seshp, (ub4) OCI_CRED_RDBMS, 
                         (ub4) OCI_DEFAULT)) 
      {
         ReportError((OCIError *) *errhp, ls_error_message);
         WriteError(function, "OCISessionBegin()~OCI_CRED_RDBMS", ls_error_message);
         return OCI_ERROR;
      } 
   }

   /* Set session attribute to Service Context */
   if(OCIAttrSet((dvoid *) *svchp, (ub4) OCI_HTYPE_SVCCTX, 
                 (dvoid *) *seshp, (ub4) 0, (ub4) OCI_ATTR_SESSION, 
                 (OCIError *) *errhp)) 
   {
      ReportError((OCIError *) *errhp, ls_error_message);
      WriteError(function, "OCIAttrSet()~OCI_ATTR_SESSION", ls_error_message);
      return OCI_ERROR;
   }

   if(SetEnv(*envhp, *errhp, *svchp, "alter session set nls_date_format = 'YYYYMMDD'")) return OCI_ERROR;

   if(gi_sql_trace_flag)
   {
      if(SetEnv(*envhp, *errhp, *svchp, "alter session set sql_trace = true")) return OCI_ERROR;
   }

   return OCI_SUCCESS;
}

int OciDisconnect(OCIEnv     **envhp,
                  OCIError   **errhp,
                  OCIServer  **svrhp,
                  OCISvcCtx  **svchp,
                  OCISession **seshp)
{
   char *function = "OciDisconnect";
   char ls_error_message[NULL_ERROR_MESSAGE];

   memset((void *) ls_error_message, (int)'\0', (size_t) NULL_ERROR_MESSAGE);

   /* End the Session */
   if(OCISessionEnd((OCISvcCtx *) *svchp, (OCIError *) *errhp, (OCISession *) *seshp, (ub4) OCI_DEFAULT)) 
   {
      ReportError((OCIError *) *errhp, ls_error_message);
      WriteError(function, "OCISessionEnd()", ls_error_message);
      return OCI_ERROR;
   }

   /* Free the Session Handle */
   if(OCIHandleFree((dvoid *) *seshp, OCI_HTYPE_SESSION))
   {
      strcpy(ls_error_message, "Unable to free Session Handle");
      WriteError(function, "OCIHandleFree()~OCI_HTYPE_SESSION", ls_error_message);
      return OCI_ERROR;
   }

   /* Free the Service Handle */
   if(OCIHandleFree((dvoid *) *svchp, OCI_HTYPE_SVCCTX))
   {
      strcpy(ls_error_message, "Unable to free Service Handle");
      WriteError(function, "OCIHandleFree()~OCI_HTYPE_SVCCTX", ls_error_message);
      return OCI_ERROR;
   }

   /* Detach from the Server */
   if(OCIServerDetach((OCIServer *) *svrhp, (OCIError *) *errhp, (ub4) OCI_DEFAULT)) 
   {
      ReportError((OCIError *) *errhp, ls_error_message);
      WriteError(function, "OCIServerDetach()", ls_error_message);
      return OCI_ERROR;
   }

   /* Free the Server Handle */
   if(OCIHandleFree((dvoid *) *svrhp, OCI_HTYPE_SERVER))
   {
      strcpy(ls_error_message, "Unable to free Server Handle");
      WriteError(function, "OCIHandleFree()~OCI_HTYPE_SERVER", ls_error_message);
      return OCI_ERROR;
   }

   /* Free the Error Handle */
   if(OCIHandleFree((dvoid *) *errhp, OCI_HTYPE_ERROR))
   {
      strcpy(ls_error_message, "Unable to free Error Handle");
      WriteError(function, "OCIHandleFree()~OCI_HTYPE_ERROR", ls_error_message);
      return OCI_ERROR;
   }

   /* Free the Environment Handle */
   if(OCIHandleFree((dvoid *) *envhp, OCI_HTYPE_ENV))
   {
      strcpy(ls_error_message, "Unable to free Environment Handle");
      WriteError(function, "OCIHandleFree()~OCI_HTYPE_ENV", ls_error_message);
      return OCI_ERROR;
   }

   return OCI_SUCCESS;
}

int OciInitLogon(int argc,
                 char *argv[],
                 OCIEnv     **envhp,
                 OCIError   **errhp,
                 OCIServer  **svrhp,
                 OCISvcCtx  **svchp,
                 OCISession **seshp,
                 char *username,
                 char *password,
                 char *dbname)
{
   char *progname = argv[0];
   char message_txt[100];

   if (strrchr(progname, '/'))
      strcpy(program_name, strrchr(progname, '/') + 1);
   else
      strcpy(program_name, progname);

   if(argc > 1) ParseUid(argv[1], username, password, dbname);

   /* parse the parameters looking for SQL_TRACE flag */
   get_option(argc, argv, "SQL_TRACE", &gi_sql_trace_flag);

   if(OciConnect(envhp, errhp, svrhp, svchp, seshp, username, password, dbname) != OCI_SUCCESS) return OCI_ERROR;

   snprintf(message_txt, sizeof(message_txt), "Started by %s", username);
   LogMessage(message_txt);

   return OCI_SUCCESS;
}

void OciTime(char *fmt, size_t max, char *os_str)
{
   time_t lr_t;
   struct tm *lr_ts;

   lr_t = time(NULL);
   lr_ts = (struct tm *) localtime(&lr_t);

   strftime(os_str, max, fmt, lr_ts);
}

static FILE *fperror_file;

int WriteError(char *function, char *table_name, char *err_data)
{
   char errdata[100];
   char clock[26];
   char *err_name;
   char format_string[100];

   char err_date[15];

   /*Dynamic memory allocation is required for err_name as MMHOME can be greater than 100*/
   err_name= (char *)malloc (strlen((char *)getenv("MMHOME")) + 57);
   /* Extra size of 56 is for /error/err. and program_name (19) and clock =11+19+26 and 1 for NULL*/
   strcpy(err_name, (char *)getenv("MMHOME"));
   strcat(err_name, "/error/err.");
   strcat(err_name, program_name);

   OciTime(".%h_%d", 25, clock);
   strcat(err_name, clock);

   fperror_file = fopen(err_name, "a");
   if (fperror_file == NULL)
   {
      sprintf(errdata,"WARNING - CANNOT OPEN ERROR FILE\n");
      return(-1);
   }

   /* set up the format string */
   strcpy(format_string, "\0");
   strcat(format_string, "%s");     /* program name */
   strcat(format_string, DELIMITER);
   strcat(format_string, "%s");     /* error date */
   strcat(format_string, DELIMITER);
   strcat(format_string, "%s");     /* function */
   strcat(format_string, DELIMITER);
   strcat(format_string, "%s");     /* table name */
   strcat(format_string, DELIMITER);
   strcat(format_string, "%s");     /* error data */
   strcat(format_string, "\n");

   OciTime("%Y%m%d%H%M%S", 25, err_date);

   /* printf error data to error file so it can be loaded into the */
   /* batch error table */
   fprintf(fperror_file, format_string, program_name,
                                        err_date,
                                        function,
                                        table_name,
                                        err_data);
   fclose(fperror_file);
   free(err_name);
   return(0);
} 

sword SetEnv(OCIEnv **envhp, OCIError **errhp, OCISvcCtx *svchp, char *SqlStmt)
{
   char *function = "SetEnv";
   char ls_error_message[NULL_ERROR_MESSAGE];

   OCIStmt  *stmthp;

   if(OCIHandleAlloc((dvoid **) envhp, (dvoid **) &stmthp, OCI_HTYPE_STMT, (size_t) 0, (dvoid **)0))
   {
      strcpy(ls_error_message, "Unable to allocate Statement Handle");
      WriteError(function, "OCIHandleAlloc()", ls_error_message);
      return OCI_ERROR;
   }

   if(OCIStmtPrepare((OCIStmt *) stmthp, (OCIError *) errhp, (dvoid *) SqlStmt, strlen(SqlStmt), OCI_NTV_SYNTAX, OCI_DEFAULT))
   {
      ReportError((OCIError *) errhp, ls_error_message);
      WriteError(function, "OCIStmtPrepare()", ls_error_message);
      return OCI_ERROR;
   }

   if(OCIStmtExecute((OCISvcCtx *) svchp, (OCIStmt *) stmthp, (OCIError *) errhp,
                     (ub4) 1, (ub4) 0, (CONST OCISnapshot *) NULL, (OCISnapshot *) NULL,
                     (ub4) OCI_DEFAULT))
   {
      ReportError((OCIError *) errhp, ls_error_message);
      WriteError(function, "OCIStmtPrepare()", ls_error_message);
      return OCI_ERROR;
   }

   if(OCIHandleFree((dvoid *) stmthp, OCI_HTYPE_STMT))
   {
      strcpy(ls_error_message, "Unable to free Statement Handle");
      WriteError(function, "OCIHandleAlloc()~OCI_HTYPE_ENV", ls_error_message);
      return OCI_ERROR;
   }

   return OCI_SUCCESS;
}

/* Internal use: parse argv[] for value of a specified option */
int get_option(int   argc,
               char* argv[],
               char* option_name,
               int*  oi_flag)
{
   int arg_index;
   char* option_value = "";
   char* ptr;

   /* search argv after userid/password@connect_string for specified option */
   for (arg_index = 2; arg_index < argc; arg_index++)
   {
      ptr = strchr(argv[arg_index], '=');
      if (ptr && !strncmp(argv[arg_index], option_name, strlen(option_name)))
      {
         option_value  = ptr + 1;
         break;
      }
   }

   if (!strcmp(option_value, "TRUE"))
      *oi_flag = 1;
   else if (!strcmp(option_value, "FALSE"))
      *oi_flag = -1;
   else
      *oi_flag = 0;

   return 0;
}

void RaiseError(pthread_mutex_t *mutex, pthread_cond_t *cv, int *status)
{
   pthread_mutex_lock(mutex); 
   *status = OCI_ERROR;
   pthread_cond_broadcast (cv);
   pthread_mutex_unlock(mutex);
}