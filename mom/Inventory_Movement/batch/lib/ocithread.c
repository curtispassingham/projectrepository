#include <stdio.h>
#include <math.h>

int GetChunkSize (char *ls_error_message, long max_array, int num_threads, int thread, long *ol_start_rec, long *ol_num_recs)
{
   long li_start_rec = 0, li_stop_rec = 0, li_chunk = 0;

   if(thread >= num_threads) 
   {
      sprintf(ls_error_message,"Error : Invalid thread number");
      return(-1);
   }

   li_chunk = ceil((double)max_array / (double)num_threads);

   li_start_rec = (thread == 0) ? 0 : li_chunk * thread;
   li_stop_rec  = li_start_rec + li_chunk;

   *ol_start_rec = (li_start_rec > max_array) ? max_array : li_start_rec;
   *ol_num_recs  = (li_stop_rec  > max_array) ? max_array - li_start_rec : li_stop_rec - li_start_rec;

   return(0);
}

int GetThreadCount (long max_array, int max_threads, int min_chunk, int *oi_num_threads)
{
   int li_num_threads = 0;

   li_num_threads = ceil((double)max_array / (double)(min_chunk * 2));

   *oi_num_threads = (li_num_threads > max_threads) ? max_threads : li_num_threads;

   return(0);
}
