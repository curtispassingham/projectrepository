CREATE OR REPLACE PACKAGE BODY MBL_TSF_SVC AS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION ITEM_QUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result          IN OUT MBL_TSF_ITEM_SEARCH_RESULT_TBL,
                    IO_pagination     IN OUT MBL_PAGINATION_REC,
                    I_search_type     IN     VARCHAR2,
                    I_search_string   IN     VARCHAR2,
                    I_items           IN     OBJ_VARCHAR_ID_TABLE,
                    I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                    I_from_loc        IN     TSFHEAD.FROM_LOC%TYPE) --optional

RETURN NUMBER;
--
FUNCTION LOC_QUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result          IN OUT MBL_TSF_LOC_SEARCH_RESULT_TBL,
                   IO_pagination     IN OUT MBL_PAGINATION_REC,
                   I_search_string   IN     VARCHAR2,
                   I_search_loc_type IN     VARCHAR2,
                   I_from_loc        IN     ITEM_LOC.LOC%TYPE,
                   I_to_loc          IN     ITEM_LOC.LOC%TYPE,
                   I_tsf_type        IN     TSFHEAD.TSF_TYPE%TYPE,
                   I_items           IN     OBJ_VARCHAR_ID_TABLE,  --optional
                   I_selected_loc    IN     ITEM_LOC.LOC%TYPE,     --optional
                   I_search_loc      IN     VARCHAR2)              --optional
RETURN NUMBER;
--
 --------------------------------------------------------------------------------
 --------------------------------------------------------------------------------
FUNCTION GET_NEXT_TSF_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_tsf_no           OUT TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_TSF_SVC.GET_NEXT_TSF_NO';
   L_return_code        VARCHAR2(10) := 'FALSE';

BEGIN

   NEXT_TRANSFER_NUMBER(O_tsf_no,
                        L_return_code,
                        O_error_message);
   if L_return_code = 'FALSE' then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);   
      RETURN MBL_CONSTANTS.FAILURE;
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END GET_NEXT_TSF_NO;
--------------------------------------------------------------------------------
FUNCTION ITEM_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_result          IN OUT MBL_TSF_ITEM_SEARCH_RESULT_TBL,
                     IO_pagination     IN OUT MBL_PAGINATION_REC,
                     I_search_type     IN     VARCHAR2,
                     I_search_string   IN     VARCHAR2,
                     I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                     I_from_loc        IN     TSFHEAD.FROM_LOC%TYPE) --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_TSF_SVC.ITEM_SEARCH';
   L_return_code        VARCHAR2(10) := 'FALSE';

BEGIN

   if ITEM_QUERY(O_error_message,
                 O_result,
                 IO_pagination,
                 I_search_type,
                 I_search_string,
                 NULL,
                 I_dept,
                 I_from_loc) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END ITEM_SEARCH;
--------------------------------------------------------------------------------
FUNCTION ITEM_LOAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result          IN OUT MBL_TSF_ITEM_SEARCH_RESULT_TBL,
                   I_items           IN     OBJ_VARCHAR_ID_TABLE,
                   I_from_loc        IN     TSFHEAD.FROM_LOC%TYPE) --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_TSF_SVC.ITEM_LOAD';
   L_pagination         MBL_PAGINATION_REC;
BEGIN

   if ITEM_QUERY(O_error_message,
                 O_result,
                 L_pagination,
                 NULL,  --I_search_type,
                 NULL,  --I_search_string,
                 I_items,
                 NULL,  --I_dept,
                 I_from_loc) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END ITEM_LOAD;
--------------------------------------------------------------------------
FUNCTION ITEM_QUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result          IN OUT MBL_TSF_ITEM_SEARCH_RESULT_TBL,
                    IO_pagination     IN OUT MBL_PAGINATION_REC,
                    I_search_type     IN     VARCHAR2,
                    I_search_string   IN     VARCHAR2,
                    I_items           IN     OBJ_VARCHAR_ID_TABLE,
                    I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                    I_from_loc        IN     TSFHEAD.FROM_LOC%TYPE) --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61)   := 'MBL_TSF_SVC.ITEM_QUERY';
   L_return_code        VARCHAR2(10)   := 'FALSE';
   L_row_start          NUMBER(10)     := 0;
   L_row_end            NUMBER(10)     := 0;
   L_num_rec_found      NUMBER(10)     := 0;
   L_system_options_rec SYSTEM_OPTIONS%ROWTYPE;
   L_counter_tabel      OBJ_NUMERIC_ID_TABLE;

   cursor c_item is
      select MBL_TSF_ITEM_SEARCH_RESULT_REC(inner2.item,
                                            inner2.item_desc,
                                            inner2.dept,
                                            NULL,  --AVAIL_QTY
                                            NULL,  --AV_COST
                                            NULL,  --UNIT_RETAIL
                                            NULL,  --CURRENCY_CODE
                                            inner2.standard_uom,
                                            inner2.supp_pack_size,
                                            inner2.inner_pack_size,
                                            inner2.item_image_url),
             inner2.total_rows
        from (select inner.item,
                     inner.item_desc,
                     inner.dept,
                     inner.standard_uom,
                     inner.supp_pack_size,
                     inner.inner_pack_size,
                     inner.item_image_url,
                     row_number() over (order by inner.item_desc, inner.item) counter,
                     count(*) over () total_rows
                from (select distinct im.item,
                                      im.item_desc,
                                      im.dept,
                                      im.standard_uom,
                                      isc.supp_pack_size,
                                      isc.inner_pack_size,
                                      img.item_image_url
                        from v_item_master im,
                             item_supplier isup,
                             item_supp_country isc,
                             (select item,
                                     image_addr||image_name item_image_url,
                                     row_number() over (partition by item 
                                                            order by create_datetime) image_row_number
                                from item_image) img
                       where (((I_search_type = MBL_CONSTANTS.ITEM_SEARCH_TYPE_ITEM and
                                (lower(im.item)      like(lower('%'||NVL(I_search_string,'-90909')||'%')) or
                                 lower(im.item_desc) like(lower('%'||NVL(I_search_string,'-90909')||'%'))))
                               or
                               (I_search_type = MBL_CONSTANTS.ITEM_SEARCH_TYPE_VPN and
                                (lower(isup.vpn) like(lower('%'||NVL(I_search_string,'-90909')||'%'))))
                              )
                              or
                              (I_search_type IS NULL and
                               im.item in (select value(input) from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input)
                              )
                             )
                         and im.item_level              = im.tran_level
                         and im.status                  = 'A'
                         and im.inventory_ind           = 'Y'
                         and im.item                    = isup.item(+)
                         --
                         and im.item                    = img.item(+)
                         and img.image_row_number(+)    = 1
                         --
                         and im.item                    = isc.item(+)
                         and isc.primary_supp_ind(+)    = 'Y'
                         and isc.primary_country_ind(+) = 'Y'
                         and im.item                    = isc.item(+)
                         --
                         and im.dept                    = DECODE(L_system_options_rec.DEPT_LEVEL_TRANSFERS, 
                                                                 'Y',
                                                                 NVL(I_dept,im.dept),
                                                                 im.dept)
                         --
                         and not exists (select 'X'
                                           from daily_purge dp
                                          where dp.key_value = im.item
                                            and dp.table_name = 'ITEM_MASTER')) inner) inner2
      where inner2.counter    >= L_row_start
        and inner2.counter     < L_row_end
      order by inner2.item_desc;

   cursor c_item_loc is
      select MBL_TSF_ITEM_SEARCH_RESULT_REC(inner2.item,
                                            inner2.item_desc,
                                            inner2.dept,
                                            inner2.avail_qty,
                                            inner2.av_cost,
                                            inner2.selling_unit_retail,
                                            inner2.currency_code,
                                            inner2.standard_uom,
                                            inner2.supp_pack_size,
                                            inner2.inner_pack_size,
                                            inner2.item_image_url),
             inner2.total_rows
        from (select inner.item,
                     inner.item_desc,
                     inner.dept,
                     inner.avail_qty,
                     inner.av_cost,
                     inner.selling_unit_retail,
                     inner.currency_code,
                     inner.standard_uom,
                     inner.supp_pack_size,
                     inner.inner_pack_size,
                     inner.item_image_url,
                     row_number() over (order by inner.item_desc, inner.item) counter,
                     count(*) over () total_rows
                from (select distinct im.item,
                                      im.item_desc,
                                      im.dept,
                                      GREATEST(ilsoh.stock_on_hand,0) -
                                         (ilsoh.tsf_reserved_qty + ilsoh.rtv_qty +
                                          GREATEST(ilsoh.non_sellable_qty,0) +
                                          GREATEST(NVL(dist.qty_distro,0),0) +
                                          ilsoh.customer_resv) avail_qty,
                                      ilsoh.av_cost,
                                      il.selling_unit_retail,
                                      loc.currency_code,
                                      im.standard_uom,
                                      isc.supplier,
                                      isc.supp_pack_size,
                                      isc.inner_pack_size,
                                      img.item_image_url
                        from v_item_master im,
                             item_supplier isup,
                             item_supp_country isc,
                             item_loc il,
                             item_loc_soh ilsoh,
                             (select s.store loc, s.currency_code from store s
                              union all
                              select w.wh loc, w.currency_code from wh w) loc,
                             (select item,
                                     image_addr||image_name item_image_url,
                                     row_number() over (partition by item 
                                                            order by create_datetime) image_row_number
                                from item_image) img,
                             (select h.item,
                                     h.wh loc,
                                     NVL(sum(d.qty_distro),0) qty_distro
                               from alloc_detail d,
                                    alloc_header h
                              where d.alloc_no = h.alloc_no
                                and h.status   in ('A','R')
                                and (exists (select 'x'
                                               from ordhead o
                                              where o.order_no = h.order_no) or
                                     exists (select 'x'
                                               from alloc_header h1
                                              where h1.alloc_no = h.order_no))
                           group by h.item, h.wh) dist
                       where (((I_search_type = MBL_CONSTANTS.ITEM_SEARCH_TYPE_ITEM and
                                (lower(im.item)      like(lower('%'||NVL(I_search_string,'-90909')||'%')) or
                                 lower(im.item_desc) like(lower('%'||NVL(I_search_string,'-90909')||'%'))))
                               or
                               (I_search_type = MBL_CONSTANTS.ITEM_SEARCH_TYPE_VPN and
                                (lower(isup.vpn) like(lower('%'||NVL(I_search_string,'-90909')||'%'))))
                              )
                              or
                              (I_search_type IS NULL and
                               im.item in (select value(input) from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input)
                              )
                             )
                         and im.item_level              = im.tran_level
                         and im.status                  = 'A'
                         and im.inventory_ind           = 'Y'
                         and im.item                    = isup.item(+)
                         --
                         and im.item                    = isc.item(+)
                         and isc.primary_supp_ind(+)    = 'Y'
                         and isc.primary_country_ind(+) = 'Y'
                         and im.item                    = isc.item(+)
                         --
                         and im.item                    = img.item(+)
                         and img.image_row_number(+)    = 1
                         --
                         and im.item                    = dist.item(+)
                         and dist.loc(+)                = I_from_loc
                         --
                         and im.dept                    = DECODE(L_system_options_rec.DEPT_LEVEL_TRANSFERS, 
                                                                 'Y',
                                                                 NVL(I_dept,im.dept),
                                                                 im.dept)
                         --
                         and not exists (select 'X'
                                           from daily_purge dp
                                          where dp.key_value = im.item
                                            and dp.table_name = 'ITEM_MASTER')
                         --
                         and im.item                    = il.item
                         and il.loc                     = I_from_loc
                         and im.item                    = ilsoh.item
                         and ilsoh.loc                  = I_from_loc
                         and loc.loc                    = I_from_loc
                         --
                         and ((im.pack_ind                = 'Y' and
                              il.loc_type                 = 'W' and
                              NVL(il.receive_as_type,'P') = 'P')
                             or
                             (im.pack_ind                 = 'N')
                            )
                     ) inner
       where inner.avail_qty  > 0) inner2
      where inner2.counter    >= L_row_start
        and inner2.counter     < L_row_end
      order by inner2.item_desc;
        
BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;
   --
   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   ---
   if I_from_loc is NULL then
      open c_item;
      fetch c_item bulk collect into O_result, L_counter_tabel;
      close c_item;
   else
      open c_item_loc;
      fetch c_item_loc bulk collect into O_result, L_counter_tabel;
      close c_item_loc;
   end if;
   --
   if L_counter_tabel       is NULL or 
      L_counter_tabel.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_tabel(L_counter_tabel.first);
   end if;
   --
   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   --
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if c_item%ISOPEN then
         close c_item;
      end if;
      if c_item_loc%ISOPEN then
         close c_item_loc;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);   
      RETURN MBL_CONSTANTS.FAILURE;
   when OTHERS then
      if c_item%ISOPEN then
         close c_item;
      end if;
      if c_item_loc%ISOPEN then
         close c_item_loc;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END ITEM_QUERY;
--------------------------------------------------------------------------------
FUNCTION FROM_LOC_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result          IN OUT MBL_TSF_LOC_SEARCH_RESULT_TBL,
                         IO_pagination     IN OUT MBL_PAGINATION_REC,
                         I_search_string   IN     VARCHAR2,
                         I_search_loc_type IN     VARCHAR2,
                         I_tsf_type        IN     TSFHEAD.TSF_TYPE%TYPE,
                         I_items           IN     OBJ_VARCHAR_ID_TABLE, --optional
                         I_to_loc          IN     ITEM_LOC.LOC%TYPE)    --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_TSF_SVC.FROM_LOC_SEARCH';

BEGIN

   if LOC_QUERY(O_error_message   => O_error_message,
                O_result          => O_result,
                IO_pagination     => IO_pagination,
                I_search_string   => I_search_string,  
                I_search_loc_type => I_search_loc_type,    
                I_from_loc        => NULL,
                I_to_loc          => NULL,
                I_tsf_type        => I_tsf_type,
                I_items           => I_items,
                I_selected_loc    => I_to_loc,
                I_search_loc      => MBL_CONSTANTS.SEARCH_FROM) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END FROM_LOC_SEARCH;
--------------------------------------------------------------------------------
FUNCTION TO_LOC_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_result          IN OUT MBL_TSF_LOC_SEARCH_RESULT_TBL,
                       IO_pagination     IN OUT MBL_PAGINATION_REC,
                       I_search_string   IN     VARCHAR2,
                       I_search_loc_type IN     VARCHAR2,
                       I_tsf_type        IN     TSFHEAD.TSF_TYPE%TYPE,
                       I_from_loc        IN     ITEM_LOC.LOC%TYPE)    --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_TSF_SVC.TO_LOC_SEARCH';

BEGIN

   if LOC_QUERY(O_error_message   => O_error_message,
                O_result          => O_result,
                IO_pagination     => IO_pagination,
                I_search_string   => I_search_string,
                I_search_loc_type => I_search_loc_type,
                I_from_loc        => NULL,
                I_to_loc          => NULL,
                I_tsf_type        => I_tsf_type,
                I_items           => NULL,
                I_selected_loc    => I_from_loc,
                I_search_loc      => MBL_CONSTANTS.SEARCH_TO) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END TO_LOC_SEARCH;
--------------------------------------------------------------------------------
FUNCTION LOC_LOAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_result          IN OUT MBL_TSF_LOC_SEARCH_RESULT_TBL,
                  I_from_loc        IN     ITEM_LOC.LOC%TYPE,
                  I_to_loc          IN     ITEM_LOC.LOC%TYPE)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_TSF_SVC.LOC_LOAD';
   L_pagination         MBL_PAGINATION_REC;

BEGIN

   if LOC_QUERY(O_error_message   => O_error_message,
                O_result          => O_result,
                IO_pagination     => L_pagination,
                I_search_string   => NULL,
                I_search_loc_type => NULL,
                I_from_loc        => I_from_loc,
                I_to_loc          => I_to_loc,
                I_tsf_type        => NULL,
                I_items           => NULL,
                I_selected_loc    => NULL,
                I_search_loc      => NULL) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END LOC_LOAD;
--------------------------------------------------------------------------------
FUNCTION LOC_QUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result          IN OUT MBL_TSF_LOC_SEARCH_RESULT_TBL,
                   IO_pagination     IN OUT MBL_PAGINATION_REC,
                   I_search_string   IN     VARCHAR2,
                   I_search_loc_type IN     VARCHAR2,
                   I_from_loc        IN     ITEM_LOC.LOC%TYPE,
                   I_to_loc          IN     ITEM_LOC.LOC%TYPE,
                   I_tsf_type        IN     TSFHEAD.TSF_TYPE%TYPE,
                   I_items           IN     OBJ_VARCHAR_ID_TABLE,  --optional
                   I_selected_loc    IN     ITEM_LOC.LOC%TYPE,     --optional
                   I_search_loc      IN     VARCHAR2)              --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_TSF_SVC.LOC_QUERY';
   L_system_options     system_options%ROWTYPE;
   L_row_start          NUMBER(10)                    := 0;
   L_row_end            NUMBER(10)                    := 0;
   L_num_rec_found      NUMBER(10)                    := 0;
   L_num_item           NUMBER(10)                    := 0;
   L_physical_wh        wh.wh%TYPE                    := NULL;
   L_transfer_zone      store.transfer_zone%TYPE      := NULL;
   L_entity             org_unit.set_of_books_id%TYPE := NULL;
   L_counter_tabel      OBJ_NUMERIC_ID_TABLE;

   cursor c_selected_entity is
      select decode(L_system_options.intercompany_transfer_basis,
                    'B', ou.set_of_books_id,
                    'T', l.tsf_entity_id,
                    MBL_CONSTANTS.DUMMY_NUMBER),
                    l.physical_wh,
                    l.transfer_zone
        from org_unit ou,
             (select s.store location,
                     s.tsf_entity_id,
                     s.org_unit_id,
                     null physical_wh,
                     s.transfer_zone
                from store s
               where s.store = I_selected_loc
              union all
              select w.wh location,
                     w.tsf_entity_id,
                     w.org_unit_id,
                     w.physical_wh,
                     null transfer_zone
                from wh w
               where w.wh = I_selected_loc) l
       where l.org_unit_id = ou.org_unit_id(+);

   cursor c_loc is
      select MBL_TSF_LOC_SEARCH_RESULT_REC(inner.location,
                                           inner.loc_type,
                                           inner.loc_name, 
                                           inner.location_currency,
                                           decode(L_system_options.intercompany_transfer_basis,
                                                  'B', inner.set_of_books_id,
                                                  'T', inner.tsf_entity_id,
                                                  NULL),
                                           CASE WHEN L_system_options.intercompany_transfer_basis = 'B' THEN
                                                     inner.set_of_books_desc --set_of_books_desc
                                                WHEN L_system_options.intercompany_transfer_basis = 'T' THEN
                                                      inner.tsf_entity_desc   --tsf_entity_desc
                                                ELSE NULL
                                           END, -- entity_desc
                                           CAST(MULTISET(select MBL_TSF_LOCITEM_SEARCH_RES_REC(
                                                                il.item,
                                                                --
                                                                GREATEST(ilsoh.stock_on_hand,0) -
                                                                (ilsoh.tsf_reserved_qty +
                                                                 ilsoh.rtv_qty +
                                                                 GREATEST(ilsoh.non_sellable_qty,0) +
                                                                 GREATEST(NVL(dist.qty_distro,0),0) +
                                                                 ilsoh.customer_resv
                                                                ), --avail_qty
                                                                --
                                                                ilsoh.av_cost,
                                                                il.selling_unit_retail,
                                                                inner.location_currency)
                                                           from item_master im,
                                                                item_loc il,
                                                                item_loc_soh ilsoh,
                                                                table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input,
                                                                (select h.item,
                                                                        h.wh loc,
                                                                        NVL(sum(d.qty_distro),0) qty_distro
                                                                   from alloc_detail d,
                                                                        alloc_header h,
                                                                        table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) it
                                                                  where h.item     = value(it)
                                                                    and h.wh       = inner.location
                                                                    and d.alloc_no = h.alloc_no
                                                                    and h.status   in ('A','R')
                                                                    and (exists (select 'x'
                                                                                   from ordhead o
                                                                                  where o.order_no = h.order_no) or
                                                                         exists (select 'x'
                                                                                   from alloc_header h1
                                                                                  where h1.alloc_no = h.order_no))
                                                                  group by h.item, h.wh
                                                                ) dist
                                                          where value(input)  = im.item
                                                            and im.item       = il.item
                                                            and il.item       = ilsoh.item
                                                            and il.loc        = inner.location
                                                            and il.loc        = ilsoh.loc
                                                            --
                                                            and il.item       = dist.item(+)
                                                            and il.loc        = dist.loc(+)
                                                            --
                                                            and ((im.pack_ind                 = 'Y' and
                                                                  il.loc_type                 = 'W' and
                                                                  NVL(il.receive_as_type,'P') = 'P')
                                                                 or
                                                                 (im.pack_ind                 = 'N')
                                                                )
                                                            )
                                                as MBL_TSF_LOCITEM_SEARCH_RES_TBL)),
             inner.total_rows
        from (select i.location,
                     i.loc_type,
                     i.loc_name,
                     i.location_currency,
                     i.tsf_entity_id,
                     te.tsf_entity_desc,
                     mvls.set_of_books_id,  
                     mvls.set_of_books_desc,
                     row_number() over (order by i.location) counter,
                     count(*) over () total_rows
                from mv_loc_sob mvls,
                     v_tsf_entity_tl te,
                     (with transfer_loc as
                             (select location_id
                                from v_transfer_from_loc
                               where I_search_loc = 'FROM'
                                  or location_id  = I_from_loc
                               union all
                              select location_id
                                from v_transfer_to_loc
                               where I_search_loc = 'TO'
                                  or location_id  = I_to_loc
                             )
                      select distinct s.store location,
                             'S' loc_type,
                             s.store_name loc_name, --v_store translate store name to user lang
                             s.currency_code location_currency,
                             s.tsf_entity_id,
                             s.org_unit_id
                        from v_store s,
                             transfer_loc tl
                       where ((s.store             like('%'||NVL(I_search_string,'-90909')||'%') or
                               lower(s.store_name) like('%'||lower(NVL(I_search_string,'-90909'))||'%'))
                              or
                              (s.store             in (I_from_loc,I_to_loc))
                             )
                         and NVL(I_search_loc_type, 'S') = 'S'
                         and s.stockholding_ind          = 'Y'
                         and s.store_type                = 'C'
                         and s.store                    != NVL(I_selected_loc, MBL_CONSTANTS.DUMMY_NUMBER)
                         and NVL(s.transfer_zone,
                             MBL_CONSTANTS.DUMMY_NUMBER) = NVL(L_transfer_zone,NVL(s.transfer_zone, MBL_CONSTANTS.DUMMY_NUMBER))
                         and s.store                     = tl.location_id
                      union all
                      select distinct w.wh location,
                             'W' loc_type,
                             w.wh_name loc_name,
                             w.currency_code location_currency,
                             w.tsf_entity_id,
                             w.org_unit_id
                        from v_wh w,
                             transfer_loc tl
                       where ((w.wh             like('%'||NVL(I_search_string,'-90909')||'%') or
                               lower(w.wh_name) like('%'||lower(NVL(I_search_string,'-90909'))||'%'))
                              or
                              (w.wh             in (I_from_loc,I_to_loc))
                             )
                         and NVL(I_search_loc_type, 'W') = 'W'
                         and w.stockholding_ind          = 'Y'
                         and w.wh                       != NVL(I_selected_loc,MBL_CONSTANTS.DUMMY_NUMBER)
                         and w.physical_wh              != NVL(L_physical_wh, MBL_CONSTANTS.DUMMY_NUMBER)
                         and w.wh                        = tl.location_id
                     ) i
               where i.location                   = mvls.location
                 and i.loc_type                   = mvls.location_type
                 and NVL(i.tsf_entity_id,
                     MBL_CONSTANTS.DUMMY_NUMBER)  = te.tsf_entity_id(+)
                 and (I_selected_loc is NULL 
                      or 
                      (I_tsf_type = 'MR' and
                       decode(L_system_options.intercompany_transfer_basis,
                              'B', mvls.set_of_books_id,
                              'T', i.tsf_entity_id,
                                   MBL_CONSTANTS.DUMMY_NUMBER) = L_entity)
                      or 
                      (I_tsf_type = 'IC' and
                       decode(L_system_options.intercompany_transfer_basis,
                              'B', mvls.set_of_books_id,
                              'T', i.tsf_entity_id,
                                   MBL_CONSTANTS.DUMMY_NUMBER) != L_entity)
                     )
                 and (   L_num_item = 0 
                      or (i.location in (select loc 
                                           from (select ilsoh.loc,
                                                        rank() over (partition by ilsoh.loc
                                                                         order by ilsoh.loc, ilsoh.item) res_counter
                                                   from item_loc_soh ilsoh,
                                                        table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input
                                                  where value(input)  = ilsoh.item
                                                    and ilsoh.loc     = i.location)
                                          where res_counter = L_num_item)))) inner
       where inner.counter    >= L_row_start
         and inner.counter     < L_row_end;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;
   --
   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   ---
   if I_selected_loc is not NULL then
      open c_selected_entity;
      fetch c_selected_entity into L_entity,
                                   L_physical_wh,
                                   L_transfer_zone;
      close c_selected_entity;
   end if;
   --
   if I_items is not NULL and 
      I_items.count > 1 then
      L_num_item := I_items.count;
   elsif I_items is not NULL and 
         I_items.count = 1   and
         I_items(1) is not NULL then
      --Java code create the I_items object with one record that is null to avoid
      --null records make sure the first record has value.
      L_num_item := I_items.count;
   end if;
   
   --
   open c_loc;
   fetch c_loc bulk collect into O_result, L_counter_tabel;
   close c_loc;
   ---
   if L_counter_tabel       is NULL or 
      L_counter_tabel.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_tabel(L_counter_tabel.first);
   end if;
   --
   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   --
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if c_selected_entity%ISOPEN then
         close c_selected_entity;
      end if;
      if c_loc%ISOPEN then
         close c_loc;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);   
      RETURN MBL_CONSTANTS.FAILURE;
   when OTHERS then
      if c_selected_entity%ISOPEN then
         close c_selected_entity;
      end if;
      if c_loc%ISOPEN then
         close c_loc;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END LOC_QUERY;
--------------------------------------------------------------------------------
FUNCTION CREATE_TSF(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_tsf_object      IN     MBL_TSF_XTSFDESC_REC)
RETURN NUMBER IS
   L_program      VARCHAR2(61) := 'MBL_TSF_SVC.CREATE_TSF';
   L_status_code  VARCHAR2(10) := NULL;
   L_tsf_object   "RIB_XTsfDesc_REC";
   
   cursor C_CONV_TO_RIB is
      select "RIB_XTsfDesc_REC"( 0  --rib_oid
                               , I_tsf_object.tsf_no
                               , I_tsf_object.from_loc_type
                               , I_tsf_object.from_loc
                               , I_tsf_object.to_loc_type
                               , I_tsf_object.to_loc
                               , I_tsf_object.delivery_date
                               , I_tsf_object.dept
                               , I_tsf_object.routing_code
                               , I_tsf_object.freight_code
                               , I_tsf_object.tsf_type
                               , CAST(MULTISET(select "RIB_XTsfDtl_REC"( 0 --rib_oid
                                                                       , input_dtl.item
                                                                       , input_dtl.tsf_qty
                                                                       , input_dtl.supp_pack_size
                                                                       , input_dtl.inv_status
                                                                       , input_dtl.unit_cost)
                                                 from table(cast(I_tsf_object.XTsfDtl_TBL as MBL_TSF_XTSFDTL_TBL)) input_dtl)as "RIB_XTsfDtl_TBL")
                               , I_tsf_object.status
                               , I_tsf_object.user_id
                               , I_tsf_object.comment_desc
                               , I_tsf_object.context_type
                               , I_tsf_object.context_value)        
        from dual;
BEGIN
   
   open C_CONV_TO_RIB;
   fetch C_CONV_TO_RIB into L_tsf_object;
   close C_CONV_TO_RIB;
   --
   RMSSUB_XTSF.CONSUME(L_status_code,
                       O_error_message,
                       L_tsf_object,
                       RMSSUB_XTSF.LP_cre_type);
   --
   if L_status_code != API_CODES.SUCCESS then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   ---
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_CONV_TO_RIB%ISOPEN then
         close C_CONV_TO_RIB;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END CREATE_TSF;
--------------------------------------------------------------------------------
END MBL_TSF_SVC;
/
