CREATE OR REPLACE PACKAGE MBL_RTSF_SVC AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
--Function Name : LOC_SEARCH
--Purpose       : This function will return list of locations based on the input
--                search string, location type, page number and page size.
-------------------------------------------------------------------------------------
FUNCTION LOC_SEARCH(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result            IN OUT   MBL_RTSF_LOC_SEARCH_RESULT_TBL,
                    IO_pagination       IN OUT   MBL_PAGINATION_REC,
                    I_search_string     IN       VARCHAR2,
                    I_search_loc_type   IN       VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------------
--Function Name : TSF_SEARCH
--Purpose       : This function will return transfer(s) based on the sent in
--                search criteria, page number and page size.
-------------------------------------------------------------------------------------
FUNCTION TSF_SEARCH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result          IN OUT   MBL_RTSF_SEARCH_RES_TBL,
                    IO_pagination     IN OUT   MBL_PAGINATION_REC,
                    I_search_record   IN       MBL_RTSF_SEARCH_REC)
RETURN NUMBER;
-------------------------------------------------------------------------------------
--Function Name : GET_TSF_DETAIL
--Purpose       : This function will return transfer detail information for 
--                input transfer number, page number and page size.
-------------------------------------------------------------------------------------
FUNCTION GET_TSF_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result          IN OUT   MBL_RTSF_TSF_DTL_REC,
                        IO_pagination     IN OUT   MBL_PAGINATION_REC,
                        I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------
--Function Name : GET_TSF_TYPE_LIST
--Purpose       : This function will return transfer types and their description.
-------------------------------------------------------------------------------------
FUNCTION GET_TSF_TYPE_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_result          IN OUT   MBL_CODE_DETAIL_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------------
--Function Name : GET_TSF_STATUS_LIST
--Purpose       : This function will return a list of Transfer status code and 
--                their description.
-------------------------------------------------------------------------------------
FUNCTION GET_TSF_STATUS_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_result          IN OUT   MBL_CODE_DETAIL_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------------
--Function Name : GET_TSF_USERS
--Purpose       : This function will return list of trasfer created user ids  
--                based on the input search string, page number and page size.
-------------------------------------------------------------------------------------
FUNCTION GET_TSF_USERS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_result          IN OUT   OBJ_VARCHAR_ID_TABLE,
                       IO_pagination     IN OUT   MBL_PAGINATION_REC,
                       I_search_string   IN       VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------------
--Function Name : UPDATE_TSF_STATUS
--Purpose       : This function is to approve a list of Transfers or set them
--                back to input status.
-------------------------------------------------------------------------------------
FUNCTION UPDATE_TSF_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_result          IN OUT   MBL_RTSF_STATUS_REC,
                           I_new_status      IN       TSFHEAD.STATUS%TYPE,
                           I_tsfs            IN       OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------
--Function Name : LOC_LOAD
--Purpose       : This function will return location details based on the input
--                list of locations.
-------------------------------------------------------------------------------------
FUNCTION LOC_LOAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_result             OUT   MBL_RTSF_LOC_SEARCH_RESULT_TBL,
                  I_locs            IN       OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------
END MBL_RTSF_SVC;
/
