CREATE OR REPLACE PACKAGE BODY MBL_RTSF_SVC AS
--------------------------------------------------------------------------------
FUNCTION LOC_SEARCH(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result            IN OUT   MBL_RTSF_LOC_SEARCH_RESULT_TBL,
                    IO_pagination       IN OUT   MBL_PAGINATION_REC,
                    I_search_string     IN       VARCHAR2,
                    I_search_loc_type   IN       VARCHAR2)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'MBL_RTSF_SVC.LOC_SEARCH';
   L_row_start       NUMBER(10)   := 0;
   L_row_end         NUMBER(10)   := 0;
   L_num_rec_found   NUMBER(10)   := 0;
   L_counter_table   OBJ_NUMERIC_ID_TABLE;

   cursor C_LOCATION is
      select MBL_RTSF_LOC_SEARCH_RESULT_REC(inner.location,
                                            inner.loc_type,
                                            inner.loc_name,
                                            inner.location_currency),
             inner.total_rows
        from (select i.location,
                     i.loc_type,
                     i.loc_name,
                     i.location_currency,
                     RANK() OVER (ORDER BY i.loc_name,
                                           i.location) counter,
                     COUNT(*) OVER () total_rows
                from (select s.store location,
                             MBL_CONSTANTS.LOC_TYPE_S loc_type,
                             s.store_name loc_name,
                             s.currency_code location_currency
                        from v_store s
                       where (s.store             like('%'||NVL(I_search_string,MBL_CONSTANTS.DUMMY_STRING)||'%') or
                              LOWER(s.store_name) like('%'||LOWER(NVL(I_search_string,MBL_CONSTANTS.DUMMY_STRING))||'%'))
                         and NVL(I_search_loc_type, MBL_CONSTANTS.LOC_TYPE_S) = MBL_CONSTANTS.LOC_TYPE_S
                         and s.stockholding_ind          = MBL_CONSTANTS.YES_IND
                         and s.store_type                = MBL_CONSTANTS.STORE_TYPE_C
                      union all
                      select w.wh location,
                             MBL_CONSTANTS.LOC_TYPE_W loc_type,
                             w.wh_name loc_name,
                             w.currency_code location_currency
                        from v_wh w
                       where (w.wh             like('%'||NVL(I_search_string,MBL_CONSTANTS.DUMMY_STRING)||'%') or
                              LOWER(w.wh_name) like('%'||LOWER(NVL(I_search_string,MBL_CONSTANTS.DUMMY_STRING))||'%'))
                         and NVL(I_search_loc_type, MBL_CONSTANTS.LOC_TYPE_W) = MBL_CONSTANTS.LOC_TYPE_W
                     ) i
             ) inner
       where inner.counter    >= L_row_start
         and inner.counter     < L_row_end
       order by inner.loc_name;

BEGIN

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   open C_LOCATION;
   fetch C_LOCATION BULK COLLECT into O_result,
                                      L_counter_table;
   close C_LOCATION;

   if L_counter_table       is NULL or
      L_counter_table.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_table(L_counter_table.FIRST);
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_LOCATION%ISOPEN then
         close C_LOCATION;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
END LOC_SEARCH;
--------------------------------------------------------------------------------
FUNCTION TSF_SEARCH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result          IN OUT   MBL_RTSF_SEARCH_RES_TBL,
                    IO_pagination     IN OUT   MBL_PAGINATION_REC,
                    I_search_record   IN       MBL_RTSF_SEARCH_REC)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'MBL_RTSF_SVC.TSF_SEARCH';
   L_row_start       NUMBER(10)   := 0;
   L_row_end         NUMBER(10)   := 0;
   L_num_rec_found   NUMBER(10)   := 0;
   L_counter_table   OBJ_NUMERIC_ID_TABLE;
   L_statuses        OBJ_VARCHAR_ID_TABLE;
   L_tsf_types       OBJ_VARCHAR_ID_TABLE;
   L_create_id       OBJ_VARCHAR_ID_TABLE;

   L_all_departments VARCHAR2(1) := NULL;
   L_all_locations   VARCHAR2(1) := NULL;
   L_std_av_ind      SYSTEM_OPTIONS.STD_AV_IND%TYPE;

   cursor C_TSF_SEARCH is
      select MBL_RTSF_SEARCH_RES_REC(i2.tsf_no,
                                     i2.tsf_type,
                                     i2.from_loc,
                                     i2.from_loc_type,
                                     i2.from_loc_name,
                                     i2.to_loc,
                                     i2.to_loc_type,
                                     i2.to_loc_name,
                                     i2.status,
                                     i2.total_cost,
                                     i2.currency_code,
                                     i2.delivery_date),
             i2.total_rows
        from (select i.tsf_no,
                     i.tsf_type,
                     i.from_loc,
                     i.from_loc_type,
                     i.from_loc_name,
                     i.to_loc,
                     i.to_loc_type,
                     i.to_loc_name,
                     i.status,
                     i.total_cost,
                     i.currency_code,
                     i.create_date,
                     i.delivery_date,
                     ROW_NUMBER() OVER (ORDER BY i.create_date desc,
                                                 i.tsf_no) counter,
                     COUNT(*) OVER () total_rows
               from (select distinct
                            th.tsf_no,
                            th.tsf_type,
                            --
                            th.from_loc,
                            th.from_loc_type,
                            from_loc.loc_name from_loc_name,
                            --
                            th.to_loc,
                            th.to_loc_type,
                            to_loc.location_name to_loc_name,
                            --
                            th.overall_status status,
                            SUM(NVL(td.tsf_cost,decode(L_std_av_ind,
                                                       'A',NVL(ils.av_cost,ils.unit_cost),
                                                       ils.unit_cost)) * td.tsf_qty) OVER (PARTITION BY th.tsf_no) total_cost,
                            from_loc.currency_code,
                            th.create_date,
                            th.delivery_date
                       from table(cast(L_tsf_types as OBJ_VARCHAR_ID_TABLE)) input_types,
                            table(cast(L_statuses as OBJ_VARCHAR_ID_TABLE)) input_statuses,
                            table(cast(L_create_id as OBJ_VARCHAR_ID_TABLE)) input_create_id,
                            ---
                            v_tsfhead th,
                            tsfdetail td,
                            item_loc_soh ils,
                            tsfitem_inv_flow tif,
                            --
                            (select s.store loc,
                                    s.store_name loc_name,
                                    s.currency_code
                               from v_store s
                             union all
                             select w.wh loc,
                                    w.wh_name loc_name,
                                    w.currency_code
                               from v_wh w) from_loc,
                            --
                            (select store location_id,
                                    store_name location_name
                               from v_store
                             union all
                             select wh location_id,
                                    wh_name location_name
                               from v_wh) to_loc
                      where th.overall_status    = value(input_statuses)
                        and th.tsf_type          = value(input_types)
                        and th.tsf_type NOT IN (MBL_CONSTANTS.TSF_TYPE_CO, MBL_CONSTANTS.TSF_TYPE_BT)
                        and(I_search_record.tsf_no is NULL or th.tsf_no like('%'||NVL(I_search_record.tsf_no,th.tsf_no)||'%'))
                        --
                        and th.create_date      >= NVL(I_search_record.start_create_date, th.create_date)
                        and th.create_date      <= NVL(I_search_record.end_create_date, th.create_date)
                        and NVL(th.delivery_date,DATE '0000-01-01') >= NVL(I_search_record.start_delivery_date, NVL(th.delivery_date,DATE '0000-01-01'))
                        and NVL(th.delivery_date,DATE '9999-01-01') <= NVL(I_search_record.end_delivery_date, NVL(th.delivery_date,DATE '9999-01-01'))
                        and th.create_id         = value(input_create_id)
                        --
                        and (L_all_locations = MBL_CONSTANTS.YES_IND or EXISTS (select 1
                                                                                  from table(cast(I_search_record.locations as OBJ_NUMERIC_ID_TABLE)) input_locations
                                                                                 where th.to_loc = value(input_locations) or th.from_loc = value(input_locations)))
                        --
                        and th.tsf_no            = td.tsf_no
                        and (th.from_loc         = ils.loc or
                             tif.from_loc = ils.loc)
                        and th.tsf_no = tif.tsf_no(+)
                        and td.tsf_no = tif.tsf_no(+)
                        and td.item              = ils.item
                        --
                        and th.to_loc            = to_loc.location_id
                        and th.from_loc          = from_loc.loc
                        --
                        and EXISTS (select 1 
                                      from v_tsfdetail td1
                                     where td1.tsf_no = th.tsf_no)
                        --
                        and (L_all_departments = MBL_CONSTANTS.YES_IND
                             or EXISTS (select 1
                                              from v_tsfdetail td2,
                                                   item_master im2
                                             where td2.tsf_no = th.tsf_no
                                               and td2.item=im2.item  
                                               and im2.dept IN (select value(input_merch_hier)
                                                                     from table(cast(I_search_record.merch_hier as OBJ_NUMERIC_ID_TABLE)) input_merch_hier)))
                    ) i
            ) i2
      where i2.counter    >= L_row_start
        and i2.counter    <  L_row_end
      order by i2.create_date desc, i2.tsf_no;

BEGIN

   if SYSTEM_OPTIONS_SQL.STD_AV_IND(O_error_message,
                                    L_std_av_ind) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;

   if I_search_record.locations is not NULL and I_search_record.locations.count > 0 then
      L_all_locations := MBL_CONSTANTS.NO_IND;
   else
      L_all_locations := MBL_CONSTANTS.YES_IND;
   end if;

   if I_search_record.merch_hier is not NULL and I_search_record.merch_hier.count > 0 then
      L_all_departments := MBL_CONSTANTS.NO_IND;
   else
      L_all_departments := MBL_CONSTANTS.YES_IND;
   end if;

   if I_search_record.statuses is NOT NULL and I_search_record.statuses.COUNT > 0 then
      L_statuses := I_search_record.statuses;
   else
      L_statuses := OBJ_VARCHAR_ID_TABLE('I', 'B', 'A', 'N');
   end if;

   if I_search_record.tsf_types is NOT NULL and I_search_record.tsf_types.COUNT > 0 then
      L_tsf_types := I_search_record.tsf_types;
   else
      select code BULK COLLECT into L_tsf_types
        from code_detail
       where code_type = MBL_CONSTANTS.CODE_TYPE_TR4E;
   end if;
  
   if I_search_record.create_id is NOT NULL and I_search_record.create_id.COUNT > 0 then
      L_create_id := I_search_record.create_id;
   else
      select distinct create_id BULK COLLECT INTO L_create_id
        from v_tsfhead;
   end if;

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   open C_TSF_SEARCH;
   fetch C_TSF_SEARCH BULK COLLECT into O_result,
                                        L_counter_table;
   close C_TSF_SEARCH;

   if L_counter_table is NULL or L_counter_table.COUNT <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_table(L_counter_table.FIRST);
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if C_TSF_SEARCH%ISOPEN then
         close C_TSF_SEARCH;
      end if;   
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;   
   when OTHERS then
      if C_TSF_SEARCH%ISOPEN then
         close C_TSF_SEARCH;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
END TSF_SEARCH;
----------------------------------------------------------------------------
FUNCTION GET_TSF_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result          IN OUT   MBL_RTSF_TSF_DTL_REC,
                        IO_pagination     IN OUT   MBL_PAGINATION_REC,
                        I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'MBL_RTSF_SVC.GET_TSF_DETAIL';
   L_row_start       NUMBER(10)   := 0;
   L_row_end         NUMBER(10)   := 0;
   L_num_rec_found   NUMBER(10)   := 0;
   L_counter_table   OBJ_NUMERIC_ID_TABLE;
   L_std_av_ind      SYSTEM_OPTIONS.STD_AV_IND%TYPE;

   cursor C_TSF is 
      select MBL_RTSF_TSF_DTL_REC(i.tsf_no,
                                  i.status,
                                  i.from_loc,
                                  i.from_loc_name,
                                  i.finisher,
                                  i.finisher_name,
                                  i.to_loc,
                                  i.to_loc_name,
                                  i.tsf_type,
                                  i.total_cost,
                                  i.total_retail,
                                  i.currency_code,
                                  i.delivery_date,
                                  i.create_id,
                                  i.create_date,
                                  NULL)
        from (select distinct th.tsf_no,
                     th.overall_status status,
                     th.from_loc,
                     from_loc.loc_name from_loc_name,
                     th.finisher,
                     case 
                        when th.finisher_type = MBL_CONSTANTS.FINISHER_TYPE_E then
                           (select finisher_desc
                              from v_external_finisher
                             where finisher_id = th.finisher)
                        when th.finisher_type = MBL_CONSTANTS.FINISHER_TYPE_I then
                           (select finisher_desc
                              from v_internal_finisher
                             where finisher_id = th.finisher)
                        else 
                           NULL
                     end finisher_name,
                     th.to_loc,
                     (select location_name
                        from v_location
                       where location_id = th.to_loc) to_loc_name,
                     th.tsf_type,
                     SUM(NVL(td.tsf_cost,decode(L_std_av_ind,
                                                'A',NVL(ils.av_cost,ils.unit_cost),
                                                ils.unit_cost)) * td.tsf_qty) OVER (PARTITION BY th.tsf_no) total_cost,
                     SUM(NVL(td.tsf_price, NVL(il.unit_retail,0)) * td.tsf_qty) OVER (PARTITION BY th.tsf_no) total_retail,
                     from_loc.currency_code,
                     th.delivery_date,
                     th.create_id,
                     th.create_date
                from v_tsfhead th,
                     v_tsfdetail td,
                     tsfitem_inv_flow tif,
                     (select s.store loc,
                             s.store_name loc_name,
                             s.currency_code 
                        from v_store s
                      union all
                      select w.wh loc,
                             w.wh_name loc_name,
                             w.currency_code
                        from v_wh w) from_loc,
                     item_loc_soh ils,
                     item_loc il
               where th.tsf_no = I_tsf_no
                 and th.from_loc = from_loc.loc
                 and th.tsf_no = td.tsf_no
                 and th.tsf_no = tif.tsf_no(+)
                 and td.tsf_no = tif.tsf_no(+)
                 and td.item = ils.item
                 and (ils.loc = th.from_loc or
                      ils.loc = tif.from_loc)
                 and ils.item = il.item
                 and ils.loc  = il.loc) i;

   cursor C_TSF_NO_DTL is 
      select MBL_RTSF_TSF_DTL_REC(i.tsf_no,
                                  i.status,
                                  i.from_loc,
                                  i.from_loc_name,
                                  i.finisher,
                                  i.finisher_name,
                                  i.to_loc,
                                  i.to_loc_name,
                                  i.tsf_type,
                                  i.total_cost,
                                  i.total_retail,
                                  i.currency_code,
                                  i.delivery_date,
                                  i.create_id,
                                  i.create_date,
                                  NULL)
        from (select distinct th.tsf_no,
                     th.overall_status status,
                     th.from_loc,
                     from_loc.loc_name from_loc_name,
                     th.finisher,
                     case 
                        when th.finisher_type = MBL_CONSTANTS.FINISHER_TYPE_E then
                           (select finisher_desc
                              from v_external_finisher
                             where finisher_id = th.finisher)
                        when th.finisher_type = MBL_CONSTANTS.FINISHER_TYPE_I then
                           (select finisher_desc
                              from v_internal_finisher
                             where finisher_id = th.finisher)
                        else 
                           NULL
                     end finisher_name,
                     th.to_loc,
                     (select location_name
                        from v_location
                       where location_id = th.to_loc) to_loc_name,
                     th.tsf_type,
                     0 total_cost,
                     0 total_retail,
                     from_loc.currency_code,
                     th.delivery_date,
                     th.create_id,
                     th.create_date
                from v_tsfhead th,
                     tsfitem_inv_flow tif,
                     (select s.store loc,
                             s.store_name loc_name,
                             s.currency_code 
                        from v_store s
                      union all
                      select w.wh loc,
                             w.wh_name loc_name,
                             w.currency_code
                        from v_wh w) from_loc,
                     item_loc_soh ils
               where th.tsf_no = I_tsf_no
                 and th.from_loc = from_loc.loc
                 and th.tsf_no = tif.tsf_no(+)) i;

   cursor C_TSF_ITEM is 
      select MBL_RTSF_TSF_DTL_ITEM_REC(i.item,
                                       i.item_desc,
                                       i.tsf_qty),
             i.total_rows
        from (select td.item,
                     im.item_desc,
                     td.tsf_qty,
                     ROW_NUMBER() OVER (ORDER BY td.item desc) counter,
                     COUNT(*) OVER () total_rows
                from v_tsfdetail td,
                     v_item_master im
               where td.tsf_no = I_tsf_no
                 and td.item = im.item
             )i
       where i.counter  >= L_row_start
         and i.counter  <  L_row_end
       order by i.item desc;

BEGIN

   if SYSTEM_OPTIONS_SQL.STD_AV_IND(O_error_message,
                                    L_std_av_ind) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   open C_TSF;
   fetch C_TSF into O_result;

   if C_TSF%NOTFOUND then
      close C_TSF;

      open C_TSF_NO_DTL;
      fetch C_TSF_NO_DTL into O_result;

      if C_TSF_NO_DTL%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TSF',NULL,NULL,NULL);
         close C_TSF_NO_DTL;
         raise MBL_CONSTANTS.PROGRAM_ERROR;
      else
         close C_TSF_NO_DTL;

         open C_TSF_ITEM;
         fetch C_TSF_ITEM BULK COLLECT into O_result.tsf_items,
                                            L_counter_table;
         close C_TSF_ITEM;

         if L_counter_table is NULL or L_counter_table.COUNT <= 0 then
            L_num_rec_found := 0;
         else
            L_num_rec_found := L_counter_table(L_counter_table.FIRST);
         end if;

         if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                            IO_pagination,
                                            L_num_rec_found) = MBL_CONSTANTS.FAILURE then
            return MBL_CONSTANTS.FAILURE;
         end if;

      end if;

   else
      close C_TSF;

      open C_TSF_ITEM;
      fetch C_TSF_ITEM BULK COLLECT into O_result.tsf_items,
                                         L_counter_table;
      close C_TSF_ITEM;

      if L_counter_table is NULL or L_counter_table.COUNT <= 0 then
         L_num_rec_found := 0;
      else
         L_num_rec_found := L_counter_table(L_counter_table.FIRST);
      end if;

      if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                         IO_pagination,
                                         L_num_rec_found) = MBL_CONSTANTS.FAILURE then
         return MBL_CONSTANTS.FAILURE;
      end if;

   end if;


   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if C_TSF%ISOPEN then
         close C_TSF;
      end if;
      if C_TSF_NO_DTL%ISOPEN then
         close C_TSF;
      end if;
      if C_TSF_ITEM%ISOPEN then
         close C_TSF_ITEM;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
   when OTHERS then
      if C_TSF%ISOPEN then
         close C_TSF;
      end if;
      if C_TSF_NO_DTL%ISOPEN then
         close C_TSF;
      end if;
      if C_TSF_ITEM%ISOPEN then
         close C_TSF_ITEM;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
END GET_TSF_DETAIL;
--------------------------------------------------------------------------------
FUNCTION GET_TSF_TYPE_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_result          IN OUT   MBL_CODE_DETAIL_TBL)
RETURN NUMBER IS

   L_program     VARCHAR2(61) := 'MBL_RTSF_SVC.GET_TSF_TYPE_LIST';
   L_valid       BOOLEAN      := FALSE;

BEGIN

   if MBL_COMMON_SVC.GET_CODE_DETAIL(O_error_message,
                                     MBL_CONSTANTS.CODE_TYPE_TR4E,
                                     O_result)= MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;
   
   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
END GET_TSF_TYPE_LIST;
--------------------------------------------------------------------------------
FUNCTION GET_TSF_STATUS_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_result          IN OUT   MBL_CODE_DETAIL_TBL)
RETURN NUMBER IS

   L_program   VARCHAR2(61) := 'MBL_RTSF_SVC.GET_TSF_STATUS_LIST';

BEGIN

   if MBL_COMMON_SVC.GET_CODE_DETAIL(O_error_message,
                                     MBL_CONSTANTS.CODE_TYPE_TRS1,
                                     O_result)= MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
END GET_TSF_STATUS_LIST;
--------------------------------------------------------------------------------
FUNCTION GET_TSF_USERS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_result          IN OUT   OBJ_VARCHAR_ID_TABLE,
                       IO_pagination     IN OUT   MBL_PAGINATION_REC,
                       I_search_string   IN       VARCHAR2)
RETURN NUMBER IS

   L_program   VARCHAR2(61) := 'MBL_RTSF_SVC.GET_TSF_USERS';
   L_row_start       NUMBER(10)   := 0;
   L_row_end         NUMBER(10)   := 0;
   L_num_rec_found   NUMBER(10)   := 0;
   L_counter_table   OBJ_NUMERIC_ID_TABLE;
   
    cursor C_GET_TSF_USERS is
       select i2.create_id,
              i2.total_rows
         from (select i.create_id,
                      ROW_NUMBER() OVER (ORDER BY i.create_id) counter,
                      COUNT(*) OVER () total_rows
                 from (select distinct vt.create_id
                         from v_tsfhead vt
                         where UPPER(vt.create_id) like('%'||UPPER(nvl(I_search_string,vt.create_id))||'%')
                       ) i
              )i2
        where i2.counter    >= L_row_start
          and i2.counter    <  L_row_end
        order by i2.create_id;

BEGIN

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   open C_GET_TSF_USERS;
   fetch C_GET_TSF_USERS BULK COLLECT into O_result,
                                           L_counter_table;
   close C_GET_TSF_USERS;

   if L_counter_table is NULL or L_counter_table.COUNT <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_table(L_counter_table.FIRST);
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
END GET_TSF_USERS;
--------------------------------------------------------------------------------
FUNCTION UPDATE_TSF_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_result          IN OUT   MBL_RTSF_STATUS_REC,
                           I_new_status      IN       TSFHEAD.STATUS%TYPE,
                           I_tsfs            IN       OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program    VARCHAR2(61) := 'MBL_RTSF_SVC.UPDATE_TSF_STATUS';

   L_fail_rec          MBL_RTSF_FAIL_REC;
   L_function_return   NUMBER(1);

BEGIN

   O_result := MBL_RTSF_STATUS_REC(0,NULL,0,NULL);
   O_result.success_transfers_tbl := OBJ_NUMERIC_ID_TABLE();
   O_result.fail_transfers_tbl    := MBL_RTSF_FAIL_TBL();

   FOR i IN 1 .. I_tsfs.COUNT LOOP

      SAVEPOINT mobile_rtsf_savepoint;

      L_function_return := NULL;

      if I_new_status = MBL_CONSTANTS.TSF_STATUS_A then
         L_function_return := TSF_STATUS_SQL.APPROVE_TRANSFER(O_error_message,
                                                              I_tsfs(i));
      elsif I_new_status = MBL_CONSTANTS.TSF_STATUS_I then
         L_function_return := TSF_STATUS_SQL.UNAPPROVE_TRANSFER(O_error_message,
                                                                I_tsfs(i));
      else
         L_function_return := TSF_CONSTANTS.FAILURE;
         O_error_message   := SQL_LIB.CREATE_MSG('INV_STATUS_ACTION',I_new_status,NULL,NULL);
      end if;

      if L_function_return = TSF_CONSTANTS.FAILURE then
         ROLLBACK TO mobile_rtsf_savepoint;
         O_result.fail_transfers_count := O_result.fail_transfers_count + 1;
         MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
         L_fail_rec := MBL_RTSF_FAIL_REC(I_tsfs(i), O_error_message);
         O_result.fail_transfers_tbl.extend;
         O_result.fail_transfers_tbl(O_result.fail_transfers_tbl.count) := L_fail_rec;
      else
         O_result.success_transfers_count := O_result.success_transfers_count + 1;
         O_result.success_transfers_tbl.extend;
         O_result.success_transfers_tbl(O_result.success_transfers_tbl.count) := I_tsfs(i);
      end if;

   END LOOP;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
END UPDATE_TSF_STATUS;
--------------------------------------------------------------------------------
FUNCTION LOC_LOAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_result             OUT   MBL_RTSF_LOC_SEARCH_RESULT_TBL,
                  I_locs            IN       OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'MBL_RTSF_SVC.LOC_LOAD';

   cursor C_LOC is
      select MBL_RTSF_LOC_SEARCH_RESULT_REC(inner.location,
                                            inner.loc_type,
                                            inner.loc_name,
                                            inner.location_currency)
        from (select s.store location,
                     MBL_CONSTANTS.LOC_TYPE_S loc_type,
                     s.store_name loc_name,
                     s.currency_code location_currency
                from v_store s,
                     table(cast(I_locs as OBJ_NUMERIC_ID_TABLE)) input_locs
               where s.store = value(input_locs)
                 and s.stockholding_ind = MBL_CONSTANTS.YES_IND
                 and s.store_type       = MBL_CONSTANTS.STORE_TYPE_C
              union all
              select w.wh location,
                     MBL_CONSTANTS.LOC_TYPE_W loc_type,
                     w.wh_name loc_name,
                     w.currency_code location_currency
                from v_wh w,
                     table(cast(I_locs as OBJ_NUMERIC_ID_TABLE)) input_locs
               where w.wh = value(input_locs)
             ) inner
       order by inner.loc_name;

BEGIN

   open C_LOC;
   fetch C_LOC bulk collect into O_result;
   close C_LOC;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_LOC%ISOPEN then
         close C_LOC;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END LOC_LOAD;
--------------------------------------------------------------------------------
END MBL_RTSF_SVC;
/
