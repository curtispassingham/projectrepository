CREATE OR REPLACE PACKAGE MBL_TSF_SVC AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
FUNCTION GET_NEXT_TSF_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_tsf_no           OUT TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION ITEM_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_result          IN OUT MBL_TSF_ITEM_SEARCH_RESULT_TBL,
                     IO_pagination     IN OUT MBL_PAGINATION_REC,
                     I_search_type     IN     VARCHAR2,
                     I_search_string   IN     VARCHAR2,
                     I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                     I_from_loc        IN     TSFHEAD.FROM_LOC%TYPE) --optional
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION ITEM_LOAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result          IN OUT MBL_TSF_ITEM_SEARCH_RESULT_TBL,
                   I_items           IN     OBJ_VARCHAR_ID_TABLE,
                   I_from_loc        IN     TSFHEAD.FROM_LOC%TYPE) --optional
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION FROM_LOC_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result          IN OUT MBL_TSF_LOC_SEARCH_RESULT_TBL,
                         IO_pagination     IN OUT MBL_PAGINATION_REC,
                         I_search_string   IN     VARCHAR2,
                         I_search_loc_type IN     VARCHAR2,
                         I_tsf_type        IN     TSFHEAD.TSF_TYPE%TYPE,
                         I_items           IN     OBJ_VARCHAR_ID_TABLE, --optional
                         I_to_loc          IN     ITEM_LOC.LOC%TYPE)    --optional
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION TO_LOC_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_result          IN OUT MBL_TSF_LOC_SEARCH_RESULT_TBL,
                       IO_pagination     IN OUT MBL_PAGINATION_REC,
                       I_search_string   IN     VARCHAR2,
                       I_search_loc_type IN     VARCHAR2,
                       I_tsf_type        IN     TSFHEAD.TSF_TYPE%TYPE,
                       I_from_loc        IN     ITEM_LOC.LOC%TYPE)    --optional
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION LOC_LOAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_result          IN OUT MBL_TSF_LOC_SEARCH_RESULT_TBL,
                  I_from_loc        IN     ITEM_LOC.LOC%TYPE,
                  I_to_loc          IN     ITEM_LOC.LOC%TYPE) 
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION CREATE_TSF(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_tsf_object      IN     MBL_TSF_XTSFDESC_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END MBL_TSF_SVC;
/
