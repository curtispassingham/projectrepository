
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSMFM_WOIN AS

TYPE rowid_TBL      is table of ROWID INDEX BY BINARY_INTEGER;

LP_max_details_to_publish     NUMBER := NULL;
LP_num_threads                NUMBER := NULL;
LP_minutes_time_lag           NUMBER := NULL;

-- depending upon the message type, there will come a point in the publication process
-- when error retry will no longer be possible.  this variable will track whether or
-- not that point has been reached.
LP_error_status varchar2(1):=NULL;

PROGRAM_ERROR EXCEPTION;

            /*** Private Functions/Procedures ***/

--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD(O_error_message         OUT        VARCHAR2,
                              O_message           IN  OUT nocopy RIB_OBJECT,
                              O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id        IN  OUT nocopy RIB_BUSOBJID_TBL,
                              O_message_type      IN  OUT        VARCHAR2,
                              I_wo_id             IN             WOIN_MFQUEUE.WO_ID%TYPE,
                              I_order_no          IN             WOIN_MFQUEUE.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION LOCK_THE_BLOCK(O_error_msg         OUT VARCHAR2,
                        O_queue_locked      OUT BOOLEAN,
                        I_wo_id          IN     woin_mfqueue.wo_id%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_OBJECTS( O_error_msg      IN OUT VARCHAR2,
                               O_message        IN OUT nocopy "RIB_WOInDtl_TBL",
                               O_routing_info   IN OUT nocopy RIB_ROUTINGINFO_TBL,
                               I_message_type   IN     WOIN_MFQUEUE.MESSAGE_TYPE%TYPE,
                               I_wo_id          IN     WOIN_MFQUEUE.WO_ID%TYPE,
                               I_max_details    IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_DELETE_OBJECTS( O_error_msg      IN OUT VARCHAR2,
                                      O_message        IN OUT nocopy "RIB_WOInDtlRef_TBL",
                                      O_routing_info   IN OUT nocopy RIB_ROUTINGINFO_TBL,
                                      I_wo_id          IN     WOIN_MFQUEUE.WO_ID%TYPE,
                                      I_max_details    IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION ROUTING_INFO_ADD(O_error_message         OUT VARCHAR2,
                          O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                          I_location          IN      WOIN_MFQUEUE.LOCATION%TYPE,
                          I_loc_type          IN      WOIN_MFQUEUE.LOC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION HOSPITALIZED(O_status         OUT VARCHAR2,
                      O_text           OUT VARCHAR2,
                      I_wo_id       IN     WOIN_MFQUEUE.WO_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code       IN OUT         VARCHAR2,
                        O_error_message     IN OUT         VARCHAR2,
                        O_message           IN OUT  nocopy RIB_OBJECT,
                        O_bus_obj_id        IN OUT  nocopy RIB_BUSOBJID_TBL,
                        O_routing_info      IN OUT  nocopy RIB_ROUTINGINFO_TBL,
                        I_seq_no            IN             WOIN_MFQUEUE.SEQ_NO%TYPE,
                        I_wo_id             IN             WOIN_MFQUEUE.WO_ID%TYPE,
                        I_order_no          IN             WOIN_MFQUEUE.ORDER_NO%TYPE,
                        I_wh                IN             WOIN_MFQUEUE.WH%TYPE,
                        I_wo_seq_no         IN             WOIN_MFQUEUE.WO_SEQ_NO%TYPE,
                        I_item              IN             WOIN_MFQUEUE.ITEM%TYPE,
                        I_location          IN             WOIN_MFQUEUE.LOCATION%TYPE,
                        I_loc_type          IN             WOIN_MFQUEUE.LOC_TYPE%TYPE,
                        I_wip_code          IN             WOIN_MFQUEUE.WIP_CODE%TYPE);
--------------------------------------------------------------------------------

           /*** Public Program Bodies***/

FUNCTION ADDTOQ(O_error_msg      OUT VARCHAR2,
                I_queue_rec      IN  WOIN_MFQUEUE%ROWTYPE,                
                I_publish_ind    IN  WO_DETAIL.PUBLISH_IND%TYPE)
RETURN BOOLEAN IS

   L_module             VARCHAR2(64) := 'RMSMFM_WOIN.ADDTOQ';
   L_status_code        VARCHAR2(1)  := NULL;

BEGIN

   -- If the message is a DELETE message, all previous records on the queue
   -- relating to the record can be deleted.
   if I_queue_rec.message_type = WO_DEL then
   
      delete from woin_mfqueue
       where wo_id = I_queue_rec.wo_id
         and wh = I_queue_rec.wh
         and item = I_queue_rec.item
         and location = I_queue_rec.location
         and loc_type = I_queue_rec.loc_type
         and wo_seq_no = I_queue_rec.wo_seq_no;

   -- If the message is an UPDATE message, all previous UPDATE records on the queue
   -- relating to the record can be deleted.
   elsif I_queue_rec.message_type = WO_UPD then

      delete from woin_mfqueue
       where wo_id = I_queue_rec.wo_id
         and wh = I_queue_rec.wh
         and item = I_queue_rec.item
         and location = I_queue_rec.location
         and loc_type = I_queue_rec.loc_type
         and wo_seq_no = I_queue_rec.wo_seq_no
         and message_type = WO_UPD;
   
   end if;

   ---
   -- I_publish_ind will be set for WO_DEL messages.  A WO_DEL message
   -- should not be inserted into the queue if the WO_ADD message
   -- for the same record was never published.
   ---
   if I_publish_ind = 'Y' then
      if LP_num_threads is NULL then
         API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                      O_error_msg,
                                      LP_max_details_to_publish,
                                      LP_num_threads,
                                      LP_minutes_time_lag,
                                      FAMILY);
         if L_status_code = API_CODES.UNHANDLED_ERROR then
            return FALSE;
         end if;
      end if;
         
      insert into woin_mfqueue(seq_no,
                               pub_status,
                               message_type,
                               wo_id,
			       wh,
			       wo_seq_no,
			       item,
			       location,
			       order_no,
			       loc_type,
                               wip_code,
                               thread_no,
                               family,
                               custom_message_type,
                               transaction_number,
                               transaction_time_stamp)
                        values(workorder_mfsequence.NEXTVAL,
                               'U',
                               I_queue_rec.message_type,
                               I_queue_rec.wo_id,
                               I_queue_rec.wh,
                               I_queue_rec.wo_seq_no,
                               I_queue_rec.item,
                               I_queue_rec.location,
                               I_queue_rec.order_no,
                               I_queue_rec.loc_type,
                               I_queue_rec.wip_code,
                               mod(I_queue_rec.wo_id,LP_num_threads) + 1,
                               FAMILY,
                               'N',
                               NULL,
                               NULL);
                            
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1)
IS

   L_module          VARCHAR2(64) := 'RMSMFM_WOIN.GETNXT';

   L_wo_id           WOIN_MFQUEUE.WO_ID%TYPE:=NULL;
   L_order_no        WOIN_MFQUEUE.ORDER_NO%TYPE:=NULL;
   L_wh              WOIN_MFQUEUE.WH%TYPE:=NULL;
   L_wo_seq_no       WOIN_MFQUEUE.WO_SEQ_NO%TYPE:=NULL;
   L_item            WOIN_MFQUEUE.ITEM%TYPE:=NULL;
   L_location        WOIN_MFQUEUE.LOCATION%TYPE:=NULL;
   L_loc_type        WOIN_MFQUEUE.LOC_TYPE%TYPE:=NULL;
   L_wip_code        WOIN_MFQUEUE.WIP_CODE%TYPE:=NULL;
   L_seq_no          WOIN_MFQUEUE.SEQ_NO%TYPE:=NULL;

   L_hospital_found     VARCHAR2(1) := 'N';
   
   L_queue_locked       BOOLEAN := FALSE;
   L_seq_limit          WOIN_MFQUEUE.SEQ_NO%TYPE := 0;   
   ---
   -- DRIVING CURSOR - selects ONE message from the queue
   ---
   cursor C_QUEUE is
     select wo_id,
            order_no,
            wh,
            wo_seq_no,
            item,
            location,
            loc_type,
            wip_code,
            message_type,
            seq_no
       from (select q.wo_id,
                    q.order_no,
                    q.wh,
                    q.wo_seq_no,
                    q.item,
                    q.location,
                    q.loc_type,
                    q.wip_code,
                    q.message_type,
                    q.seq_no,
	            row_number() over(order by seq_no asc) row_num
               from woin_mfqueue q
	      where q.thread_no = I_thread_val
                and q.pub_status = 'U'
                and q.seq_no > L_seq_limit
                and ( q.order_no is NULL or exists (select 'x'
                                                      from order_pub_info opi
                                                     where opi.order_no = q.order_no
                                                       and opi.published = 'Y')))
      where row_num = 1;


   cursor C_GET_MESSAGE is
      select *
        from WOIN_MFQUEUE wmq
       where wmq.thread_no = I_thread_val
         and wmq.pub_status = 'U'
         and (wmq.order_no is NULL or exists (select 'x'
                                                from order_pub_info opi
                                               where opi.order_no = wmq.order_no
                                                 and opi.published = 'Y'))
    order by seq_no;

   cursor C_CHECK_FOR_HOSPITAL_MSGS is
      select 'Y'
        from woin_mfqueue
       where wo_id = L_wo_id
         and pub_status = 'H';

BEGIN

   -- status of 'H'ospital
   LP_error_status := API_CODES.HOSPITAL;

   LOOP

      L_wo_id         := NULL;
      O_message       := NULL;
      ---

      open C_QUEUE;
      fetch C_QUEUE into L_wo_id,
                         L_order_no,
                         L_wh,
                         L_wo_seq_no,
                         L_item,
                         L_location,
                         L_loc_type,
                         L_wip_code,
                         O_message_type,
                         L_seq_no; 
      close C_QUEUE;

      if L_wo_id is NULL then 
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;

      if LOCK_THE_BLOCK(O_error_msg,
                        L_queue_locked,
                        L_wo_id) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked then
         L_seq_limit := L_seq_no;
         O_error_msg := NULL;
      else

         open  C_CHECK_FOR_HOSPITAL_MSGS;
         fetch C_CHECK_FOR_HOSPITAL_MSGS into L_hospital_found;
         close C_CHECK_FOR_HOSPITAL_MSGS;

         if L_hospital_found = 'Y' then
            O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                           NULL,
                                           NULL,
                                           NULL);
            raise PROGRAM_ERROR;
         end if;

         if PROCESS_QUEUE_RECORD(O_error_msg,
                                 O_message,
                                 O_routing_info,
                                 O_bus_obj_id,
                                 O_message_type,
                                 L_wo_id,
                                 L_order_no) = FALSE then
            raise PROGRAM_ERROR;
         end if;

         EXIT;

      end if; -- if L_queue_locked

   END LOOP;

   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_wo_id);
   end if;

EXCEPTION
   when OTHERS then
      if O_error_msg is NULL then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      end if;
      
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_seq_no,
                    L_wo_id,
                    L_order_no,
                    L_wh,
                    L_wo_seq_no,
                    L_item,
                    L_location,
                    L_loc_type,
                    L_wip_code);
END GETNXT;
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT)
IS

   L_module          VARCHAR2(64) := 'RMSMFM_WOIN.PUB_RETRY';
   
   L_seq_no          WOIN_MFQUEUE.SEQ_NO%TYPE:=NULL;

   L_wo_id           WOIN_MFQUEUE.WO_ID%TYPE:=NULL;
   L_order_no        WOIN_MFQUEUE.ORDER_NO%TYPE:=NULL;
   L_wh              WOIN_MFQUEUE.WH%TYPE:=NULL;
   L_wo_seq_no       WOIN_MFQUEUE.WO_SEQ_NO%TYPE:=NULL;
   L_item            WOIN_MFQUEUE.ITEM%TYPE:=NULL;
   L_location        WOIN_MFQUEUE.LOCATION%TYPE:=NULL;
   L_loc_type        WOIN_MFQUEUE.LOC_TYPE%TYPE:=NULL;
   L_wip_code        WOIN_MFQUEUE.WIP_CODE%TYPE:=NULL;
   
   L_rowid            ROWID:=NULL;

   L_queue_locked     BOOLEAN := FALSE;

   cursor C_RETRY_QUEUE is
      select q.wo_id,
             q.order_no,
             q.wh,
             q.wo_seq_no,
             q.item,
             q.location,
             q.loc_type,
             q.wip_code,
             q.message_type,
             q.rowid
        from woin_mfqueue q
       where q.seq_no = L_seq_no;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;


   --get info from routing info
   ---assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;

   O_message := null;

   --get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_wo_id,
                            L_order_no,
                            L_wh,
                            L_wo_seq_no,
                            L_item,
                            L_location,
                            L_loc_type,
                            L_wip_code,
                            O_message_type,
                            L_rowid;
   close C_RETRY_QUEUE;

   if L_wo_id IS NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   if LOCK_THE_BLOCK( O_error_msg,
                      L_queue_locked,
                      L_wo_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then

      if PROCESS_QUEUE_RECORD( O_error_msg,
                               O_message,
                               O_routing_info,
                               O_bus_obj_id,
                               O_message_type,
                               L_wo_id,
                               L_order_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;
  
      if O_message IS NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         O_bus_obj_id := RIB_BUSOBJID_TBL(L_wo_id);
      end if;
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION
   when OTHERS then
      if O_error_msg is NULL then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      end if;
      
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_seq_no,
                    L_wo_id,
                    L_order_no,
                    L_wh,
                    L_wo_seq_no,
                    L_item,
                    L_location,
                    L_loc_type,
                    L_wip_code);
END PUB_RETRY;
--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD(O_error_message         OUT        VARCHAR2,
                              O_message           IN  OUT nocopy RIB_OBJECT,
                              O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id        IN  OUT nocopy RIB_BUSOBJID_TBL,
                              O_message_type      IN  OUT        VARCHAR2,
                              I_wo_id             IN             WOIN_MFQUEUE.WO_ID%TYPE,
                              I_order_no          IN             WOIN_MFQUEUE.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_module        VARCHAR2(64) := 'RMSMFM_WOIN.PROCESS_QUEUE_RECORD';
   L_status_code   VARCHAR2(1) := NULL;
   
   L_rib_woindesc_rec  "RIB_WOInDesc_REC";
   L_rib_woinref_rec   "RIB_WOInRef_REC";
   
   L_max_details        rib_settings.max_details_to_publish%TYPE:=NULL;
   L_num_threads        rib_settings.num_threads%TYPE:=NULL;
   L_minutes_time_lag   rib_settings.minutes_time_lag%TYPE:=NULL;

BEGIN

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details,
                                L_num_threads,
                                L_minutes_time_lag,
                                FAMILY);   --- I_family                              
   if L_status_code = API_CODES.UNHANDLED_ERROR then
      return FALSE;
   end if;

   if O_message_type in (WO_ADD, WO_UPD) then       -- WoInDesc msg will be created
      L_rib_woindesc_rec := "RIB_WOInDesc_REC"(0,
                                             I_wo_id,
                                             I_order_no,
                                             NULL);
      if BUILD_DETAIL_OBJECTS(O_error_message,
                              L_rib_woindesc_rec.woindtl_tbl,
                              O_routing_info,
                              O_message_type,
                              I_wo_id,
                              L_max_details) = FALSE then
         return FALSE;
      end if;

      O_message := L_rib_woindesc_rec;
      
   elsif O_message_type = WO_DEL then       -- Ref msg will be created
      L_rib_woinref_rec := "RIB_WOInRef_REC"(0,
                                           I_wo_id,
                                           I_order_no,
                                           NULL);

      if BUILD_DETAIL_DELETE_OBJECTS(O_error_message,
                                     L_rib_woinref_rec.woindtlref_tbl,
                                     O_routing_info,
                                     I_wo_id,
                                     L_max_details) = FALSE then
         return FALSE;
      end if;

      O_message := L_rib_woinref_rec;

   end if; -- O_message_type = WO_ADD, WO_UPD

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_RECORD;
------------------------------------------------------------------------------------------
FUNCTION LOCK_THE_BLOCK(O_error_msg         OUT VARCHAR2,
                        O_queue_locked      OUT BOOLEAN,
                        I_wo_id          IN     woin_mfqueue.wo_id%TYPE)
RETURN BOOLEAN IS

   L_module            VARCHAR2(64)  := 'RMSMFM_WOIN.LOCK_THE_BLOCK';
   L_table             VARCHAR2(30)  := 'WOIN_MFQUEUE';
   L_key1              VARCHAR2(100) := I_wo_id;
   L_key2              VARCHAR2(100) := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from woin_mfqueue wmq
       where wmq.wo_id = I_wo_id
         for update nowait;

BEGIN

   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      O_queue_locked := TRUE;
      return TRUE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_THE_BLOCK;
------------------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_OBJECTS( O_error_msg      IN OUT VARCHAR2,
                               O_message        IN OUT nocopy "RIB_WOInDtl_TBL",
                               O_routing_info   IN OUT nocopy RIB_ROUTINGINFO_TBL,
                               I_message_type   IN     WOIN_MFQUEUE.MESSAGE_TYPE%TYPE,
                               I_wo_id          IN     WOIN_MFQUEUE.WO_ID%TYPE,
                               I_max_details    IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN IS

   L_module         VARCHAR2(64) := 'RMSMFM_WOIN.BUILD_DETAIL_OBJECTS';
   
   L_rib_woindtl_rec    "RIB_WOInDtl_REC" := NULL;

   L_mfqueue_rowid     rowid_TBL;
   L_mfqueue_size      BINARY_INTEGER := 0;

   L_wo_update_rowid       rowid_TBL;
   L_wo_update_size        BINARY_INTEGER := 0;
   
   L_previous_location     WOIN_MFQUEUE.LOCATION%TYPE := -1;
   
   L_records_found    BOOLEAN:=FALSE;

   cursor C_WO_DETAIL is
      select q.wh,
             q.item,
             q.loc_type,
             q.location,
             q.wo_seq_no,
             q.wip_code,
             q.rowid      queue_rowid,
             wo.rowid     wo_rowid
        from woin_mfqueue q,
             wo_detail wo
       where q.wo_id           = I_wo_id
         and q.message_type    = I_message_type
         and wo.wo_id          = q.wo_id
         and wo.wh             = q.wh
         and wo.item           = q.item
         and wo.location       = q.location
         and wo.loc_type       = q.loc_type
         and wo.seq_no         = q.wo_seq_no
         and rownum           <= I_max_details
         order by q.location
         for update of wo.publish_ind nowait;

BEGIN

   L_rib_woindtl_rec := "RIB_WOInDtl_REC"(0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
   O_message := "RIB_WOInDtl_TBL"();

   FOR rec IN C_WO_DETAIL LOOP

      L_records_found := TRUE;
            
      ----- add work order info to work order node.
      L_rib_woindtl_rec.wh             := rec.wh;
      L_rib_woindtl_rec.item           := rec.item;
      L_rib_woindtl_rec.loc_type       := rec.loc_type;
      L_rib_woindtl_rec.location       := rec.location;
      L_rib_woindtl_rec.seq_no         := rec.wo_seq_no;
      L_rib_woindtl_rec.wip_code       := rec.wip_code;
            
      ----- add work order node to work order table
      O_message.EXTEND;
      O_message(O_message.COUNT)  := L_rib_woindtl_rec;

      if rec.location != L_previous_location then

         ------ add location to routing info
         if ROUTING_INFO_ADD(O_error_msg,
                             O_routing_info,
                             rec.location,
                             rec.loc_type) = FALSE then 
            return FALSE;
         end if;

         L_previous_location := rec.location;
         
      end if;
      
      L_mfqueue_size := L_mfqueue_size + 1;   
      L_mfqueue_rowid(L_mfqueue_size) := rec.queue_rowid;
            
      L_wo_update_size := L_wo_update_size + 1;                                                        
      L_wo_update_rowid(L_wo_update_size) := rec.wo_rowid;
      
   END LOOP;
   
   -- if no data found in cursor, raise error
   if not L_records_found then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_WOIN_DETAIL_PUB',   /* IS: */
                                        I_wo_id, NULL, NULL);
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_mfqueue_size > 0 then
      FORALL i IN 1..L_mfqueue_size
         delete from woin_mfqueue where rowid = L_mfqueue_rowid(i);
   end if;
   
   if L_wo_update_size > 0 then
      FORALL i IN 1..L_wo_update_size
         update wo_detail
            set publish_ind = 'Y' 
          where rowid = L_wo_update_rowid(i);
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_OBJECTS;
------------------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_DELETE_OBJECTS( O_error_msg      IN OUT VARCHAR2,
                                      O_message        IN OUT nocopy "RIB_WOInDtlRef_TBL",
                                      O_routing_info   IN OUT nocopy RIB_ROUTINGINFO_TBL,
                                      I_wo_id          IN     WOIN_MFQUEUE.WO_ID%TYPE,
                                      I_max_details    IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN IS

   L_module         VARCHAR2(64) := 'RMSMFM_WOIN.BUILD_DETAIL_DELETE_OBJECTS';

   L_rib_woindtlref_rec    "RIB_WOInDtlRef_REC" := NULL;
   
   L_mfqueue_rowid     rowid_TBL;
   L_mfqueue_size      BINARY_INTEGER := 0;
   
   L_previous_location     WOIN_MFQUEUE.LOCATION%TYPE := -1;
   
   L_records_found    BOOLEAN:=FALSE;

   cursor C_WO_DETAIL is
      select q.wh,
             q.item,
             q.loc_type,
             q.location,
             q.wo_seq_no,
             q.wip_code,
             q.rowid      queue_rowid
        from woin_mfqueue q
       where q.wo_id             = I_wo_id
         and q.message_type      = WO_DEL
         and rownum             <= I_max_details
         order by q.location;

BEGIN

   L_rib_woindtlref_rec := "RIB_WOInDtlRef_REC"(0, NULL, NULL, NULL, NULL, NULL, NULL);
   O_message := "RIB_WOInDtlRef_TBL"();

   FOR rec IN C_WO_DETAIL LOOP
      
      L_records_found := TRUE;
      
      ----- add work order info to work order node.
      L_rib_woindtlref_rec.wh             := rec.wh;
      L_rib_woindtlref_rec.item           := rec.item;
      L_rib_woindtlref_rec.loc_type       := rec.loc_type;
      L_rib_woindtlref_rec.location       := rec.location;
      L_rib_woindtlref_rec.seq_no         := rec.wo_seq_no;
      L_rib_woindtlref_rec.wip_code       := rec.wip_code;
            
      ----- add work order node to work order table
      O_message.EXTEND;
      O_message(O_message.COUNT)  := L_rib_woindtlref_rec;

      if rec.location != L_previous_location then

         ------ add location to routing info
         if ROUTING_INFO_ADD(O_error_msg,
                             O_routing_info,
                             rec.location,
                             rec.loc_type) = FALSE then 
            return FALSE;
         end if;

         L_previous_location := rec.location;
         
      end if;
      
      L_mfqueue_size := L_mfqueue_size + 1;   
      L_mfqueue_rowid(L_mfqueue_size) := rec.queue_rowid;
      
   END LOOP;
   
   -- if no data found in cursor, raise error
   if not L_records_found then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_WOIN_DETAIL_PUB',   /* IS: */
                                        I_wo_id, NULL, NULL);
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_mfqueue_size > 0 then
      FORALL i IN 1..L_mfqueue_size
         delete from woin_mfqueue where rowid = L_mfqueue_rowid(i);
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_DELETE_OBJECTS;
--------------------------------------------------------------------------------
FUNCTION ROUTING_INFO_ADD(O_error_message         OUT VARCHAR2,
                          O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                          I_location          IN      WOIN_MFQUEUE.LOCATION%TYPE,
                          I_loc_type          IN      WOIN_MFQUEUE.LOC_TYPE%TYPE)
RETURN BOOLEAN IS
   
   L_module   VARCHAR2(64) := 'RMSMFM_WOIN.ROUTING_INFO_ADD';
   
   L_rib_routing_rec    RIB_ROUTINGINFO_REC := NULL;

BEGIN

   if O_routing_info is NULL then
      O_routing_info := RIB_ROUTINGINFO_TBL();
   end if;
   
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', I_location, 
                                            'to_phys_loc_type', I_loc_type, 
                                            NULL, NULL);
   
   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ROUTING_INFO_ADD;
------------------------------------------------------------------------------------------
FUNCTION HOSPITALIZED(O_status         OUT VARCHAR2,
                      O_text           OUT VARCHAR2,
                      I_wo_id       IN     WOIN_MFQUEUE.WO_ID%TYPE)
RETURN BOOLEAN IS

   L_module   VARCHAR2(64) := 'RMSMFM_WOIN.HOSPITALIZED';
   
   L_dummy         NUMBER;
   L_hospitalized  BOOLEAN := FALSE;

   cursor C_GET_HOSPITALIZED_MSG is
      select 1
        from woin_mfqueue wmq
         where wmq.wo_id = I_wo_id
           and wmq.pub_status = API_CODES.HOSPITAL;
           
BEGIN

   open C_GET_HOSPITALIZED_MSG;
   fetch C_GET_HOSPITALIZED_MSG into L_dummy;
   ---
   L_hospitalized := C_GET_HOSPITALIZED_MSG%FOUND;
   close C_GET_HOSPITALIZED_MSG;
   ---
   return L_hospitalized;
   
EXCEPTION
   when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   L_module,
                                   TO_CHAR(SQLCODE));
      return FALSE;
END HOSPITALIZED;
---------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code       IN OUT         VARCHAR2,
                        O_error_message     IN OUT         VARCHAR2,
                        O_message           IN OUT  nocopy RIB_OBJECT,
                        O_bus_obj_id        IN OUT  nocopy RIB_BUSOBJID_TBL,
                        O_routing_info      IN OUT  nocopy RIB_ROUTINGINFO_TBL,
                        I_seq_no            IN             WOIN_MFQUEUE.SEQ_NO%TYPE,
                        I_wo_id             IN             WOIN_MFQUEUE.WO_ID%TYPE,
                        I_order_no          IN             WOIN_MFQUEUE.ORDER_NO%TYPE,
                        I_wh                IN             WOIN_MFQUEUE.WH%TYPE,
                        I_wo_seq_no         IN             WOIN_MFQUEUE.WO_SEQ_NO%TYPE,
                        I_item              IN             WOIN_MFQUEUE.ITEM%TYPE,
                        I_location          IN             WOIN_MFQUEUE.LOCATION%TYPE,
                        I_loc_type          IN             WOIN_MFQUEUE.LOC_TYPE%TYPE,
                        I_wip_code          IN             WOIN_MFQUEUE.WIP_CODE%TYPE)
IS

   L_module   VARCHAR2(64) := 'RMSMFM_WOIN.HANDLE_ERRORS';
   
   L_rib_woinref_rec    "RIB_WOInRef_REC" :=NULL;
   
   L_rib_woindtlref_rec     "RIB_WOInDtlRef_REC" :=NULL;
   L_rib_woindtlref_tbl     "RIB_WOInDtlRef_TBL" :=NULL;

   L_error_type    VARCHAR2(5)   := NULL;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then

      O_bus_obj_id    := RIB_BUSOBJID_TBL(I_wo_id);
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no', I_seq_no,null,null,null,null));

      L_rib_woindtlref_rec := "RIB_WOInDtlRef_REC"(0,
                                                 I_wh,
                                                 I_item,
                                                 I_loc_type,
                                                 I_location,
                                                 I_wo_seq_no,
                                                 I_wip_code);
      L_rib_woindtlref_tbl := "RIB_WOInDtlRef_TBL"(L_rib_woindtlref_rec);
      ---
      L_rib_woinref_rec := "RIB_WOInRef_REC"(0,
                                           I_wo_id,
                                           I_order_no,
                                           L_rib_woindtlref_tbl);
      ---

      O_message := L_rib_woinref_rec;

      update woin_mfqueue
         set pub_status = LP_error_status
       where seq_no    = I_seq_no;

   end if;

   /* Pass out parsed error message */
   SQL_LIB.API_MSG(L_error_type,
                   O_error_message);

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_module);
END HANDLE_ERRORS;
------------------------------------------------------------------------------------------
END RMSMFM_WOIN;
/
