create or replace
PACKAGE BODY RMSSUB_XTSF_SQL AS

LP_vdate  PERIOD.VDATE%TYPE   := NULL;

LP_ils_upd_rec                RMSSUB_XTSF.ILS_UPD_REC;

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
   -------------------------------------------------------------------------------------------------------
      -- Function Name: CREATE_HEADER
      -- Purpose      : This function will create a tsf HEADER record.
   -------------------------------------------------------------------------------------------------------
   FUNCTION CREATE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
      RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------
      -- Function Name: MODIFY_HEADER
      -- Purpose      : This function will update a HEADER record.
   -------------------------------------------------------------------------------------------------------
   FUNCTION MODIFY_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
      RETURN BOOLEAN;
$end
--------------------------------------------------------------------------------------------------------
   -- Function Name: CREATE_SECOND_LEG
   -- Purpose      : This function will create the second leg of the transfer if the
   --                context type is repair.
-------------------------------------------------------------------------------------------------------
   FUNCTION CREATE_SECOND_LEG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
      RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   -- Function Name: MODIFY_SECOND_LEG
   -- Purpose      : This function will modify the second leg of the transfer if it exists
-------------------------------------------------------------------------------------------------------
   FUNCTION MODIFY_SECOND_LEG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC,
                              I_message_type    IN       VARCHAR2)
      RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   -- Function Name: DELETE_SECOND_LEG
   -- Purpose      : This function will create the second leg of the transfer if it exists
-------------------------------------------------------------------------------------------------------
   FUNCTION DELETE_SECOND_LEG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC,
                              I_message_type    IN       VARCHAR2)
      RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   -- Function Name: DELETE_HEADER
   -- Purpose      : This function will delete transfer charges and set tsfhead to 'D' status.
   --                It also updates inventory on item_loc_soh.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CREATE_DETAIL
   -- Purpose      : This function will create transfer details and update inventory on item_loc_soh.
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tsf_status      IN OUT   TSFHEAD.STATUS%TYPE,
                       O_wf_order_no     IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                       O_rma_no          IN OUT   WF_RETURN_HEAD.RMA_NO%TYPE,
                       I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC,
                       I_action          IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: MODIFY_DETAIL
   -- Purpose      : This function will update transfer details and inventory on item_loc_soh.
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: DELETE_DETAIL
   -- Purpose      : This function will delete transfer details and associated transfer charges.
   --                It also updates inventory on item_loc_soh.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPD_ITEM_RESV_EXP
   -- Purpose      : This function will update the item_loc_soh expected and resv fields
-------------------------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_RESV_EXP(O_error_message  IN OUT  VARCHAR2,
                           I_ils_upd_rec    IN      RMSSUB_XTSF.ILS_UPD_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CREATE_ITEMLOC
   -- Purpose      : This function will call TSF_SQL.CREATE_ITEMLOC to create a item location
   --                record if the item being transferred does not exist within RMS at the to_location
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEMLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_INVENTORY
   -- Purpose      : This function will call UPD_ITEM_RESV_EXP to update the item_loc_soh expected
   --                and reserve fields. It also calls UPD_PHYSICAL_WHS for physical to virtual wh
   --                distribution.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_INVENTORY(O_error_message   IN OUT               RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_tsf_rec        IN OUT   NOCOPY      RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPD_PHYSICAL_WHS
   -- Purpose      : This function will update transfer from/to loc inventory at the virtual
   --                location level for an 'EG' type of transfer.
-------------------------------------------------------------------------------------------------------
FUNCTION UPD_PHYSICAL_WHS(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_rec         IN        RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: BACKOUT_TSF_STOCK
   -- Purpose      : This function will is called from UPD_PHYSICAL_WHS to handle from/loc inventory
   --                delete for at the virtual location level for an 'EG' type of transfer for 1 tsf detail.
-------------------------------------------------------------------------------------------------------
FUNCTION BACKOUT_TSF_STOCK(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_rec         IN        RMSSUB_XTSF.TSF_REC,
                           I_dtl_index       IN        INTEGER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CREATE_TSF_STOCK
   -- Purpose      : This function will is called from UPD_PHYSICAL_WHS to handle from/loc inventory
   --                create at the virtual location level for an 'EG' type of transfer for 1 tsf detail.
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_TSF_STOCK(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_rec         IN        RMSSUB_XTSF.TSF_REC,
                          I_dtl_index       IN        INTEGER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: PUT_TSF_INV_MAP
   -- Purpose      : This function will is called from BACKOUT_TSF_STOCK and CREATE_TSF_STOCK to add
   --                inventory distribution to global variable LP_ils_upd_rec for item_loc_soh update.
-------------------------------------------------------------------------------------------------------
FUNCTION PUT_TSF_INV_MAP(O_error_message   IN OUT rtk_errors.rtk_text%TYPE,
                         I_tsf_no          IN     tsfhead.tsf_no%TYPE,
                         I_seq_no          IN     tsfdetail.tsf_seq_no%TYPE,
                         I_item            IN     item_loc.item%TYPE,
                         I_pack_ind        IN     item_master.pack_ind%TYPE,
                         I_from_loc        IN     item_loc.loc%TYPE,
                         I_from_loc_type   IN     item_loc.loc_type%TYPE,
                         I_to_loc          IN     item_loc.loc%TYPE,
                         I_to_loc_type     IN     item_loc.loc_type%TYPE,
                         I_existing_qty    IN     tsfdetail.tsf_qty%TYPE,
                         I_tsf_qty         IN     tsfdetail.tsf_qty%TYPE,
                         I_inv_status      IN     tsfdetail.inv_status%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------

FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XTSF_SQL.PERSIST';

   L_tsf_status       TSFHEAD.STATUS%TYPE;
   L_wf_order_no      WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_rma_no           WF_RETURN_HEAD.RMA_NO%TYPE;
   L_action_type      VARCHAR2(10);
BEGIN
   LP_VDATE := get_vdate;
   if LOWER(I_message_type) = RMSSUB_XTSF.LP_cre_type then

      if not CREATE_HEADER(O_error_message,
                           I_tsf_rec) then
         return FALSE;
      end if;

      if I_tsf_rec.header.to_loc_type = 'E' then
         if not RMSSUB_XTSF_SQL.CREATE_SECOND_LEG(O_error_message,
                                                  I_tsf_rec) then
            return FALSE;
         end if;
      end if;

   elsif LOWER(I_message_type) = RMSSUB_XTSF.LP_mod_type then
      if not MODIFY_HEADER(O_error_message,
                           I_tsf_rec) then
         return FALSE;
      end if;
      ---
      if I_tsf_rec.header.to_loc_type = 'E' then
         if not RMSSUB_XTSF_SQL.MODIFY_SECOND_LEG(O_error_message,
                                                  I_tsf_rec,
                                                  I_message_type) then
            return FALSE;
         end if;
      end if;

   elsif LOWER(I_message_type) = RMSSUB_XTSF.LP_del_type then
      if not DELETE_HEADER(O_error_message,
                           I_tsf_rec) then
         return FALSE;
      end if;
      ---
      if I_tsf_rec.header.to_loc_type = 'E' then
         if not RMSSUB_XTSF_SQL.DELETE_SECOND_LEG(O_error_message,
                                                  I_tsf_rec,
                                                  I_message_type) then
            return FALSE;
         end if;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XTSF.LP_dtl_cre_type then
      L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;
      if NOT CREATE_DETAIL(O_error_message,
                           L_tsf_status,
                           L_wf_order_no,
                           L_rma_no,
                           I_tsf_rec,
                           L_action_type) then
         return FALSE;
      end if;
      ---
      if I_tsf_rec.header.to_loc_type = 'E' then
         if not RMSSUB_XTSF_SQL.MODIFY_SECOND_LEG(O_error_message,
                                                  I_tsf_rec,
                                                  I_message_type) then
            return FALSE;
         end if;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XTSF.LP_dtl_mod_type then
      if not MODIFY_DETAIL(O_error_message,
                           I_tsf_rec) then
         return FALSE;
      end if;
      ---
      if I_tsf_rec.header.to_loc_type = 'E' then
         if not RMSSUB_XTSF_SQL.MODIFY_SECOND_LEG(O_error_message,
                                                  I_tsf_rec,
                                                  I_message_type) then
            return FALSE;
         end if;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XTSF.LP_dtl_del_type then
      if not DELETE_DETAIL(O_error_message,
                           I_tsf_rec) then
         return FALSE;
      end if;
      ---
      if I_tsf_rec.header.to_loc_type = 'E' then
         if not RMSSUB_XTSF_SQL.DELETE_SECOND_LEG(O_error_message,
                                                  I_tsf_rec,
                                                  I_message_type) then
            return FALSE;
         end if;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST;
-----------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_tsf_tbl         IN       RMSSUB_XTSF.TSF_TBL,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XTSF_SQL.PERSIST';

BEGIN
   if I_tsf_tbl is NOT NULL and I_tsf_tbl.COUNT > 0 then
      FOR i in I_tsf_tbl.FIRST..I_tsf_tbl.LAST LOOP
         if PERSIST(O_error_message,
                    I_tsf_tbl(i),
                    I_message_type) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST;
-----------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-----------------------------------------------------------------------------------------------------
FUNCTION CREATE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50) := 'RMSSUB_XTSF_SQL.CREATE_HEADER';
   L_l10n_obj          L10n_obj     := L10n_obj();
   L_exp_dc_eow_date   TSFHEAD.EXP_DC_EOW_DATE%TYPE := NULL;

   L_tsf_status       TSFHEAD.STATUS%TYPE := NULL;
   L_wf_order_no      WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_rma_no           WF_RETURN_HEAD.RMA_NO%TYPE;
   L_action_type      VARCHAR2(10);


BEGIN

   -- When creating transfers in 'A'pproved status we are going to insert the
   -- status as 'I'nput.  Immediately after inserting this record we will update
   -- tsfhead and set the status to 'A'pproved.
   -- EXPLANATION:
   -- Transfers with tsf_type = 'EG' (externally generated) are not published
   -- since the subscribing products are already aware of them.
   -- Transfers with tsf_type in ('AIP','SIM','CO') need to be published similar to
   -- transfers created within RMS.
   -- When inserting transfers with tsf_type!='EG' into tsfhead, a record is
   -- created on transfers_pub_info. When updating tsfhead (normally when status
   -- changes from input/submitted to approved), a record is created on
   -- tsf_mfqueue.  For the transfer to be published, records must exist on both
   -- of these tables.  Since AIP/SIM/CO transfers in 'A'pproved status may never
   -- be updated, we need to create records on both tables (transfers_pub_info
   -- and tsf_mfqueue) to ensure that the transfer is published.  Doing the
   -- insert/update here handles this case rather than adding business logic to
   -- the publishing code.

   if I_tsf_rec.header.delivery_date is not NULL then
      if DATES_SQL.GET_EOW_DATE(O_error_message,
                                L_exp_dc_eow_date,
                                I_tsf_rec.header.delivery_date) = FALSE then
         return FALSE;
      end if;
   end if;

   insert into TSFHEAD (tsf_no,
                        tsf_parent_no,
                        from_loc_type,
                        from_loc,
                        to_loc_type,
                        to_loc,
                        exp_dc_date,
                        dept,
                        tsf_type,
                        status,
                        freight_code,
                        routing_code,
                        create_date,
                        create_id,
                        approval_date,
                        approval_id,
                        delivery_date,
                        ext_ref_no,
                        repl_tsf_approve_ind,
                        inventory_type,
                        comment_desc,
                        exp_dc_eow_date,
                        not_after_date,
                        context_type,
                        context_value)
                values (I_tsf_rec.header.tsf_no,
                        I_tsf_rec.header.tsf_parent_no,
                        I_tsf_rec.header.from_loc_type,
                        I_tsf_rec.header.from_loc,
                        I_tsf_rec.header.to_loc_type,
                        I_tsf_rec.header.to_loc,
                        I_tsf_rec.header.delivery_date,
                        I_tsf_rec.header.dept,
                        I_tsf_rec.header.tsf_type,
                        DECODE(I_tsf_rec.header.status, 'A', 'I',I_tsf_rec.header.status),
                        I_tsf_rec.header.freight_code,
                        I_tsf_rec.header.routing_code,
                        LP_vdate,
                        I_tsf_rec.header.create_id,
                        DECODE(I_tsf_rec.header.status, 'A', LP_vdate, NULL),
                        DECODE(I_tsf_rec.header.status, 'A', I_tsf_rec.header.create_id, NULL),
                        I_tsf_rec.header.delivery_date,
                        I_tsf_rec.header.ext_ref_no,
                        'N', -- repl_tsf_approve_ind
                        'A', -- inventory_type
                        I_tsf_rec.header.comment_desc,
                        L_exp_dc_eow_date,
                        I_tsf_rec.header.delivery_date,
                        I_tsf_rec.header.context_type,
                        I_tsf_rec.header.context_value);

   L_l10n_obj.procedure_key := 'CREATE_TRAN_UTIL_CODE';
   L_l10n_obj.doc_type      := 'TSF';
   L_l10n_obj.doc_id        := I_tsf_rec.header.tsf_no;
   L_l10n_obj.source_entity := 'LOC';
   L_l10n_obj.source_id     := I_tsf_rec.header.from_loc;

   -- get the default utilization code for transfer created.
   if L10N_SQL.EXEC_FUNCTION (O_error_message,
                              L_l10n_obj) = FALSE then
         return FALSE;
   end if;

   L_action_type := RMS_CONSTANTS.WF_ACTION_CREATE;
   L_tsf_status := I_tsf_rec.header.status;
   if NOT CREATE_DETAIL(O_error_message,
                        L_tsf_status,
                        L_wf_order_no,
                        L_rma_no,
                        I_tsf_rec,
                        L_action_type) then
      return FALSE;
   end if;

    -- See comment above (insert into tsfhead)
   update tsfhead
      set status           = L_tsf_status,
          wf_order_no      = NVL(L_wf_order_no, wf_order_no),
          rma_no           = NVL(L_rma_no, rma_no),
          approval_date    = DECODE(L_tsf_status, 'A', approval_date, NULL),
          approval_id      = DECODE(L_tsf_status, 'A', approval_id, NULL)
    where tsf_no      = I_tsf_rec.header.tsf_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_HEADER;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'RMSSUB_TSF_SQL.MODIFY_HEADER';

   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   L_table             VARCHAR2(25) := 'TSFHEAD';
   L_exp_dc_eow_date   TSFHEAD.EXP_DC_EOW_DATE%TYPE := NULL;
   L_tsf_rec           RMSSUB_XTSF.TSF_REC;
   L_exist_status      TSFHEAD.STATUS%TYPE;

   L_tsf_status       TSFHEAD.STATUS%TYPE;
   L_wf_order_no      WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_rma_no           WF_RETURN_HEAD.RMA_NO%TYPE;
   L_action_type      VARCHAR2(10);
   L_f_action_type    VARCHAR2(1);

   cursor C_LOCK_TSF_HEAD is
      select 'x'
        from tsfhead
       where tsf_no = I_tsf_rec.header.tsf_no
         for update nowait;

   cursor C_GET_HEADER_STATUS is
      select status
        from tsfhead
       where tsf_no = I_tsf_rec.header.tsf_no;

BEGIN
   L_tsf_rec := I_tsf_rec;

   open C_GET_HEADER_STATUS;
   fetch C_GET_HEADER_STATUS into L_exist_status;
   close C_GET_HEADER_STATUS;

   open C_LOCK_TSF_HEAD;
   close C_LOCK_TSF_HEAD;

   if I_tsf_rec.header.delivery_date is not NULL then
      if DATES_SQL.GET_EOW_DATE(O_error_message,
                                L_exp_dc_eow_date,
                                I_tsf_rec.header.delivery_date) = FALSE then
         return FALSE;
      end if;
   end if;
   if I_tsf_rec.header.status = 'A' and L_exist_status = 'I' then
      if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                   L_f_action_type,
                                                   I_tsf_rec.header.from_loc,
                                                   I_tsf_rec.header.from_loc_type,
                                                   I_tsf_rec.header.to_loc,
                                                   I_tsf_rec.header.to_loc_type) = FALSE then
         return FALSE;
      end if;

      L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;

      if L_f_action_type = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then
         if WF_TRANSFER_SQL.SYNC_F_ORDER(O_error_message,
                                         L_wf_order_no,
                                         L_tsf_status,
                                         L_action_type,
                                         I_tsf_rec.header.tsf_no,
                                         I_tsf_rec.header.status,
                                         I_tsf_rec.header.tsf_type) = FALSE then
            return FALSE;
         end if;
      elsif L_f_action_type = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
         if WF_TRANSFER_SQL.SYNC_F_RETURN(O_error_message,
                                          L_rma_no,
                                          L_action_type,
                                          I_tsf_rec.header.tsf_no,
                                          I_tsf_rec.header.status,
                                          I_tsf_rec.header.tsf_type) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   if L_tsf_status is NULL then
      L_tsf_status := I_tsf_rec.header.status;
   end if;

   update tsfhead
      set routing_code    = NVL(I_tsf_rec.header.routing_code, routing_code),
          delivery_date   = NVL(I_tsf_rec.header.delivery_date, delivery_date),
          not_after_date  = NVL(I_tsf_rec.header.delivery_date, not_after_date),
          exp_dc_date     = NVL(I_tsf_rec.header.delivery_date, exp_dc_date),
          exp_dc_eow_date = NVL(L_exp_dc_eow_date, exp_dc_eow_date),
          status          = NVL(L_tsf_status, status),
          comment_desc    = NVL(I_tsf_rec.header.comment_desc, comment_desc),
          approval_date   = DECODE(approval_date,
                                   NULL, DECODE(I_tsf_rec.header.status, 'A', LP_vdate, approval_date),
                                   approval_date),
          approval_id     = DECODE(approval_id,
                                   NULL, DECODE(I_tsf_rec.header.status, 'A', I_tsf_rec.header.create_id, approval_id),
                                   approval_id),
          context_type    = NVL(I_tsf_rec.header.context_type, context_type),
          context_value   = NVL(I_tsf_rec.header.context_value, context_value)
    where tsf_no = I_tsf_rec.header.tsf_no;

   if L_tsf_status = 'A' and L_exist_status = 'I' then
      if not UPDATE_INVENTORY(O_error_message,
                              L_tsf_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
       O_error_message := sql_lib.create_msg('RECORD_LOCKED',
                                             L_table,
                                             I_tsf_rec.header.tsf_no,
                                             NULL);
       return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MODIFY_HEADER;
--------------------------------------------------------------------------------------------------------
FUNCTION DELETE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XTSF_SQL.DELETE_HEADER';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(25) := 'TSFHEAD';

   L_status        TSFHEAD.STATUS%TYPE;
   L_tsf_rec       RMSSUB_XTSF.TSF_REC;
   L_wf_order_no   WF_ORDER_HEAD.WF_ORDER_NO%TYPE := NULL;
   L_rma_no        WF_RETURN_HEAD.RMA_NO%TYPE     := NULL;

   cursor C_LOCK_TSF_HEAD is
      select status
        from tsfhead
       where tsf_no = I_tsf_rec.header.tsf_no
         for update nowait;

   cursor C_GET_F_ORD_RET is
      select wf_order_no,
             rma_no
        from tsfhead
       where tsf_no = I_tsf_rec.header.tsf_no;

BEGIN
   L_tsf_rec := I_tsf_rec;

   if not TRANSFER_CHARGE_SQL.DELETE_CHRGS(O_error_message,
                                           I_tsf_rec.header.tsf_no,
                                           null) then
      return FALSE;
   end if;

   open C_LOCK_TSF_HEAD;
   fetch C_LOCK_TSF_HEAD into L_status;
   close C_LOCK_TSF_HEAD;

   if L_status in ('A', 'B') then
      if not UPDATE_INVENTORY(O_error_message,
                              L_tsf_rec) then
         return FALSE;
      end if;
   end if;
   ---
   update tsfhead
      set status = 'D'
    where tsf_no = I_tsf_rec.header.tsf_no;
   ---
   open C_GET_F_ORD_RET;
   fetch C_GET_F_ORD_RET into L_wf_order_no,
                              L_rma_no;
   close C_GET_F_ORD_RET;
   ---
   if L_wf_order_no is NOT NULL then
      if WF_TRANSFER_SQL.CANCEL_F_ORDER(O_error_message,
                                        I_tsf_rec.header.tsf_no) = FALSE then
         return FALSE;
      end if;
   elsif L_rma_no is NOT NULL then
      if WF_TRANSFER_SQL.CANCEL_F_RETURN(O_error_message,
                                         I_tsf_rec.header.tsf_no) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
       O_error_message := sql_lib.create_msg('RECORD_LOCKED',
                                             L_table,
                                             'tsf_no: '||I_tsf_rec.header.tsf_no,
                                             NULL);
       return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_HEADER;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tsf_status      IN OUT   TSFHEAD.STATUS%TYPE,
                       O_wf_order_no     IN OUT   WF_ORDER_HEAD.WF_ORDER_NO%TYPE,
                       O_rma_no          IN OUT   WF_RETURN_HEAD.RMA_NO%TYPE,
                       I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC,
                       I_action          IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'RMSSUB_XTSF_SQL.CREATE_DETAIL';

   L_tsf_rec                RMSSUB_XTSF.TSF_REC;
   L_l10n_obj               L10N_OBJ     := L10N_OBJ();
   L_system_options_row     SYSTEM_OPTIONS%ROWTYPE;

   L_f_action               VARCHAR2(10) := I_action;
   L_f_action_type          VARCHAR2(1);
   L_pwh_frm_loc            VARCHAR2(1);
   L_pwh_to_loc             VARCHAR2(1);

   cursor C_CHK_PWH(L_wh   IN   WH.WH%TYPE) is
      select 'Y'
        from wh
       where wh = L_wh
         and wh = physical_wh;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   L_tsf_rec := I_tsf_rec;

   if I_tsf_rec.tsf_detail.items is NOT NULL and
      I_tsf_rec.tsf_detail.items.COUNT > 0 then
      ---

      if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                   L_f_action_type,
                                                   I_tsf_rec.header.from_loc,
                                                   I_tsf_rec.header.from_loc_type,
                                                   I_tsf_rec.header.to_loc,
                                                   I_tsf_rec.header.to_loc_type) = FALSE then
         return FALSE;
      end if;

      FORALL i in I_tsf_rec.tsf_detail.items.FIRST .. I_tsf_rec.tsf_detail.items.LAST
         -- Set updated_by_rms_ind='Y' for both 'AIP' and 'SIM' type of transfers.
         -- This will ensure that the 'AIP' transfers and 'SIM' transfers (via
         -- RSL call) will get published.
         insert into tsfdetail (tsf_no,
                                tsf_seq_no,
                                item,
                                tsf_qty,
                                supp_pack_size,
                                inv_status,
                                tsf_po_link_no,
                                mbr_processed_ind,
                                publish_ind,
                                fill_qty,
                                ship_qty,
                                received_qty,
                                distro_qty,
                                selected_qty,
                                cancelled_qty,
                                tsf_price,
                                restock_pct,
                                updated_by_rms_ind)
                        values (I_tsf_rec.header.tsf_no,
                                I_tsf_rec.tsf_detail.seq_nos(i),
                                I_tsf_rec.tsf_detail.items(i),
                                I_tsf_rec.tsf_detail.qtys(i),
                                I_tsf_rec.tsf_detail.supp_pack_sizes(i),
                                I_tsf_rec.tsf_detail.inv_statuses(i),
                                NULL,  -- tsf_po_link_no
                                NULL,  -- mbr_processed_ind
                                'N',   -- publish_ind
                                NULL,  -- fill_qty
                                NULL,  -- ship_qty
                                NULL,  -- received_qty
                                NULL,  -- distro_qty
                                NULL,  -- selected_qty
                                NULL,  -- cancelled_qty
                                I_tsf_rec.tsf_detail.tsf_price(i),
                                DECODE(I_tsf_rec.header.tsf_type,'RV', 0, NULL),  --default restock_pct to 0 for RV transfers
                                DECODE(I_tsf_rec.header.tsf_type,'EG','N','Y'));
      -- Updating inventory_type in tsfhead if status = 'U'.

      if I_tsf_rec.tsf_detail.items is NOT NULL and I_tsf_rec.tsf_detail.items.COUNT > 0 then
         if NVL(I_tsf_rec.tsf_detail.inv_statuses(1),-1) != -1 then
            update tsfhead
               set inventory_type = 'U'
             where tsf_no = I_tsf_rec.header.tsf_no;
         end if;
      end if;

      -- Include the condition for checking the physical wh for CO transfer.
      if I_tsf_rec.header.tsf_type = 'CO' then
         L_pwh_frm_loc := 'N';
         L_pwh_to_loc  := 'N';
         if I_tsf_rec.header.from_loc_type = 'W' then
            open C_CHK_PWH(I_tsf_rec.header.from_loc);
            fetch C_CHK_PWH into L_pwh_frm_loc;
            close C_CHK_PWH;
         end if;
         if I_tsf_rec.header.to_loc_type = 'W' then
            open C_CHK_PWH(I_tsf_rec.header.to_loc);
            fetch C_CHK_PWH into L_pwh_to_loc;
            close C_CHK_PWH;
         end if;
      end if;

      -- At tranfser create time, transfer charges should only be written for transfers that do NOT involve a physical warehouse.
      -- 'EG' transfers and 'CO' transfers with OMS_IND = 'Y' will have physical whs if either from-loc or to-loc is a warehouse.
      -- For transfers with a physical warehouse, transfer charges will be written at shipment time (bol_sql) with
      -- shipment and ship_seq_no, and physical whs will be broken down to virtual whs when written to tsfdetail_chrg.
      if (I_tsf_rec.header.tsf_type NOT in ('EG','CO')) or                              --non-EG/CO transfer will NOT have physical wh
         (L_pwh_frm_loc = 'N' and L_pwh_to_loc = 'N') or   --CO transfers in OMS_IND = 'Y' configuration will NOT have physical wh
         (I_tsf_rec.header.from_loc_type != 'W' and I_tsf_rec.header.to_loc_type != 'W') then   --transfers involoving stores only
         FOR i in I_tsf_rec.tsf_detail.items.FIRST .. I_tsf_rec.tsf_detail.items.LAST LOOP
            if not TRANSFER_CHARGE_SQL.DEFAULT_CHRGS(O_error_message,
                                                     I_tsf_rec.header.tsf_no,
                                                     I_tsf_rec.header.tsf_type,
                                                     i,    -- tsf_seq_no
                                                     NULL, -- shipment
                                                     NULL, -- ship_seq_no
                                                     I_tsf_rec.header.from_loc,
                                                     I_tsf_rec.header.from_loc_type,
                                                     I_tsf_rec.header.to_loc,
                                                     I_tsf_rec.header.to_loc_type,
                                                     I_tsf_rec.tsf_detail.items(i)) then
               return FALSE;
            end if;
         END LOOP;
      end if;

      if not CREATE_ITEMLOC(O_error_message,
                            I_tsf_rec) then
         return FALSE;
      end if;

      -- Create F Order/ F Return if required
      if L_f_action_type = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then
         if WF_TRANSFER_SQL.SYNC_F_ORDER(O_error_message,
                                         O_wf_order_no,
                                         O_tsf_status,
                                         L_f_action,
                                         I_tsf_rec.header.tsf_no,
                                         I_tsf_rec.header.status,
                                         I_tsf_rec.header.tsf_type) = FALSE then
            return FALSE;
         end if;
      elsif L_f_action_type = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
         if WF_TRANSFER_SQL.SYNC_F_RETURN(O_error_message,
                                          O_rma_no,
                                          L_f_action,
                                          I_tsf_rec.header.tsf_no,
                                          I_tsf_rec.header.status,
                                          I_tsf_rec.header.tsf_type) = FALSE then
            return FALSE;
         end if;
      end if;

      if O_tsf_status is NULL then
         O_tsf_status := I_tsf_rec.header.status;
      end if;
      --
      update tsfhead 
         set status = O_tsf_status
       where tsf_no = I_tsf_rec.header.tsf_no
         and status in ('C','D');
      --
      if O_tsf_status != 'I' then
         if not UPDATE_INVENTORY(O_error_message,
                                 L_tsf_rec) then
            return FALSE;
         end if;
      end if;
      --
   end if;

   -- For SIM generated Store to Warehouse (Return to Warehouse) transfers with 'REPAIR' as the context type,
   -- a regular transfer is created in RMS and not a 2-legged one so the utilization code should be updated to
   -- 83 (for outbound transfer) and not 85 (for outbound repairing transfer)

   if I_tsf_rec.header.tsf_type = 'EG' and
      I_tsf_rec.header.from_loc_type = 'S' and
      I_tsf_rec.header.to_loc_type = 'W' and
      I_tsf_rec.header.context_type = 'REPAIR' then

      L_l10n_obj.procedure_key := 'UPDATE_TRAN_UTIL_CODE';
      L_l10n_obj.doc_type      := 'TSF';
      L_l10n_obj.doc_id        := I_tsf_rec.header.tsf_no;
      L_l10n_obj.source_entity := 'LOC';
      L_l10n_obj.source_id     := I_tsf_rec.header.from_loc;

      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                 L_l10n_obj) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;


EXCEPTION
   when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_DETAIL;
--------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'RMSSUB_XTSF_SQL.MODIFY_DETAIL';

   L_item                 TSFDETAIL.ITEM%TYPE;
   L_inv_status           TSFDETAIL.INV_STATUS%TYPE;
   L_tsf_rec              RMSSUB_XTSF.TSF_REC;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_updated_by_rms_ind   VARCHAR2(1);
   L_fo_status            WF_ORDER_HEAD.STATUS%TYPE;
   L_fr_status            WF_RETURN_HEAD.STATUS%TYPE;
   L_tsf_status           TSFHEAD.STATUS%TYPE;
   L_tsf_type             TSFHEAD.TSF_TYPE%TYPE;
   L_action_type          VARCHAR2(10);
   L_wf_order_no          TSFHEAD.WF_ORDER_NO%TYPE := NULL;
   L_rma_no               TSFHEAD.RMA_NO%TYPE := NULL;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(25) := 'TSFDETAIL';

   cursor C_LOCK_TSF_DETAIL is
      select 'x'
        from tsfdetail
       where tsf_no = I_tsf_rec.header.tsf_no
         and item = L_item
         and (inv_status = L_inv_status or
              (inv_status is NULL and
               L_inv_status is NULL))
         for update nowait;

   cursor C_GET_TSF_F_STATUS is
      select t.status,
             t.tsf_type,
             o.status,
             r.status
        from tsfhead t,
             wf_order_head o,
             wf_return_head r
       where t.tsf_no      = I_tsf_rec.header.tsf_no
         and t.wf_order_no = o.wf_order_no (+)
         and t.rma_no      = r.rma_no (+);

BEGIN
   L_tsf_rec := I_tsf_rec;
   FOR i in I_tsf_rec.tsf_detail.qtys.FIRST .. I_tsf_rec.tsf_detail.qtys.LAST LOOP

      L_item := I_tsf_rec.tsf_detail.items(i);
      L_inv_status := I_tsf_rec.tsf_detail.inv_statuses(i);

      open C_LOCK_TSF_DETAIL;
      close C_LOCK_TSF_DETAIL;
   END LOOP;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   --Set updated_by_rms_ind to 'Y' will allow RMS to publish the transfer update as a mod message to SIM/WMS.
   --Set updated_by_rms_ind to 'N' will prevent RMS from publishing the transfer update to SIM/WMS.
   --1) tsf_type = 'SIM': modified through SIM RSL call, should be published
   --2) tsf_type = 'AIP': modified through XTSF call, should be published
   --3) tsf_type = 'EG': modified through SOStatus call, should NOT be published since SIM/WMS is the source of change
   --4) tsf_type = 'CO' in an OMS='N' configuration: modified through SOStatus call, should NOT be published since SIM/WMS is the source of change
   --5) tsf_type = 'CO' in an OMS='Y' configuration, sourced from store: modified through cross channel Customer Order API,
   --should NOT publish since SIM is the source of the change.
   --5) tsf_type = 'CO' in an OMS='Y' configuration, sourced from non-store: modified through cross channel Customer Order API,
   --should publish change to SIM/WMS.

   if I_tsf_rec.header.tsf_type = 'CO' and L_system_options_row.oms_ind = 'Y' and I_tsf_rec.header.from_loc_type != 'S' or
      I_tsf_rec.header.tsf_type in ('SIM','AIP','IC','MR') then      L_updated_by_rms_ind := 'Y';
   else
      L_updated_by_rms_ind := 'N';
   end if;

   -- first leg transfer updates
   FORALL i in I_tsf_rec.tsf_detail.qtys.FIRST .. I_tsf_rec.tsf_detail.qtys.LAST
      update tsfdetail
         set tsf_qty        = GREATEST(I_tsf_rec.tsf_detail.qtys(i), 0),
             supp_pack_size = NVL(I_tsf_rec.tsf_detail.supp_pack_sizes(i), supp_pack_size),
             cancelled_qty  = GREATEST(NVL(I_tsf_rec.tsf_detail.cancelled_qtys(i), 0), 0),
             selected_qty   = GREATEST(NVL(I_tsf_rec.tsf_detail.selected_qtys(i), 0), 0),
             updated_by_rms_ind = L_updated_by_rms_ind
       where tsf_no      = I_tsf_rec.header.tsf_no
         and item        = I_tsf_rec.tsf_detail.items(i)
         and (inv_status = I_tsf_rec.tsf_detail.inv_statuses(i) or
              (inv_status is NULL and
               I_tsf_rec.tsf_detail.inv_statuses(i) is NULL));


 if I_tsf_rec.tsf_detail.tsf_price is NOT NULL then
    FORALL i in I_tsf_rec.tsf_detail.tsf_price.FIRST..I_tsf_rec.tsf_detail.tsf_price.LAST
     update tsfdetail
        set tsf_price   = I_tsf_rec.tsf_detail.tsf_price(i)
      where tsf_no      = I_tsf_rec.header.tsf_no
        and item        = I_tsf_rec.tsf_detail.items(i);
   end if;

   -- Update F Order/ F Return
   open C_GET_TSF_F_STATUS;
   fetch C_GET_TSF_F_STATUS into L_tsf_status,
                                 L_tsf_type,
                                 L_fo_status,
                                 L_fr_status;
   close C_GET_TSF_F_STATUS;

   if L_fo_status is NOT NULL then
      L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;

      if WF_TRANSFER_SQL.SYNC_F_ORDER(O_error_message,
                                      L_wf_order_no,
                                      L_tsf_status,
                                      L_action_type,
                                      I_tsf_rec.header.tsf_no,
                                      L_tsf_status,
                                      L_tsf_type) = FALSE then
         return FALSE;
      end if;

   elsif L_fr_status is NOT NULL then
      L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;
      if WF_TRANSFER_SQL.SYNC_F_RETURN(O_error_message,
                                       L_rma_no,
                                       L_action_type,
                                       I_tsf_rec.header.tsf_no,
                                       L_tsf_status,
                                       L_tsf_type) = FALSE then
         return FALSE;
      end if;

   end if;

   if L_tsf_status != 'I' then
      if not UPDATE_INVENTORY(O_error_message,
                              L_tsf_rec) then
         return FALSE;

      end if;
   end if;

   return TRUE;
EXCEPTION
    when RECORD_LOCKED then
       O_error_message := sql_lib.create_msg('RECORD_LOCKED',
                                             L_table,
                                             'tsf_no: '||I_tsf_rec.header.tsf_no,
                                             'item: '||L_item);
       return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MODIFY_DETAIL;
--------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'RMSSUB_XTSF_SQL.DELETE_DETAIL';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(25);

   L_dummy         VARCHAR2(1);
   L_item          TSFDETAIL.ITEM%TYPE;
   L_status        TSFHEAD.STATUS%TYPE;
   L_tsf_rec       RMSSUB_XTSF.TSF_REC;
   L_wf_order_no   TSFHEAD.WF_ORDER_NO%TYPE;
   L_rma_no        TSFHEAD.RMA_NO%TYPE;

   L_fo_status     WF_ORDER_HEAD.STATUS%TYPE;
   L_fr_status     WF_RETURN_HEAD.STATUS%TYPE;
   L_tsf_status    TSFHEAD.STATUS%TYPE;
   L_action_type   VARCHAR2(10);

   cursor C_LOCK_TSF_DETAIL is
      select 'x'
        from tsfdetail
       where tsf_no = I_tsf_rec.header.tsf_no
         and item   = L_item
         for update nowait;

   cursor C_LOCK_TSF_CHRG is
      select 'x'
        from tsfdetail_chrg
       where tsf_no = I_tsf_rec.header.tsf_no
         and (item         = L_item or
              pack_item    = L_item)
         for update nowait;

   cursor C_LOCK_TSF_HEAD is
      select status
        from tsfhead
       where tsf_no = I_tsf_rec.header.tsf_no
         for update nowait;

   cursor C_TSF_DETAILS_EXIST is
      select 'x'
        from tsfdetail
       where tsf_no = I_tsf_rec.header.tsf_no
         and rownum = 1;

   cursor C_GET_TSF_F_STATUS is
      select t.status,
             o.status,
             r.status
        from tsfhead t,
             wf_order_head o,
             wf_return_head r
       where t.tsf_no      = I_tsf_rec.header.tsf_no
         and t.wf_order_no = o.wf_order_no (+)
         and t.rma_no      = r.rma_no (+);

   cursor C_GET_F_ORD_RET is
      select wf_order_no,
             rma_no
        from tsfhead
       where tsf_no = I_tsf_rec.header.tsf_no;

BEGIN
   L_tsf_rec := I_tsf_rec;
   FOR i in I_tsf_rec.tsf_detail.items.FIRST .. I_tsf_rec.tsf_detail.items.LAST LOOP
      L_item := I_tsf_rec.tsf_detail.items(i);
      L_table := 'TSFDETAIL_CHRG';

      open C_LOCK_TSF_CHRG;
      close C_LOCK_TSF_CHRG;
   END LOOP;

   FORALL i in I_tsf_rec.tsf_detail.items.FIRST..I_tsf_rec.tsf_detail.items.LAST
      delete from tsfdetail_chrg
       where tsf_no = I_tsf_rec.header.tsf_no
         and (item         = I_tsf_rec.tsf_detail.items(i) or
              pack_item    = I_tsf_rec.tsf_detail.items(i));

   FOR i in I_tsf_rec.tsf_detail.items.FIRST .. I_tsf_rec.tsf_detail.items.LAST LOOP
      L_item := I_tsf_rec.tsf_detail.items(i);
      L_table := 'TSFDETAIL';

      open C_LOCK_TSF_DETAIL;
      close C_LOCK_TSF_DETAIL;
   END LOOP;

   FORALL i in I_tsf_rec.tsf_detail.items.FIRST..I_tsf_rec.tsf_detail.items.LAST
      delete from tsfdetail
       where tsf_no = I_tsf_rec.header.tsf_no
         and item   = I_tsf_rec.tsf_detail.items(i);

   L_table := 'TSFHEAD';

   open C_LOCK_TSF_HEAD;
   fetch C_LOCK_TSF_HEAD into L_status;
   close C_LOCK_TSF_HEAD;

   if L_status in ('A', 'B', 'S') then
      if not UPDATE_INVENTORY(O_error_message,
                              L_tsf_rec) then
         return FALSE;
      end if;
   end if;

   -- check if any details on transfer
   -- if none, remove header record.
   open C_TSF_DETAILS_EXIST;
   fetch C_TSF_DETAILS_EXIST into L_dummy;

   if C_TSF_DETAILS_EXIST%NOTFOUND then
      update tsfhead
         set status = 'D'
       where tsf_no = I_tsf_rec.header.tsf_no;

      open C_GET_F_ORD_RET;
      fetch C_GET_F_ORD_RET into L_wf_order_no,
                                 L_rma_no;
      close C_GET_F_ORD_RET;

      if L_wf_order_no is NOT NULL then
         if WF_TRANSFER_SQL.CANCEL_F_ORDER(O_error_message,
                                           I_tsf_rec.header.tsf_no) = FALSE then
            return FALSE;
         end if;
      elsif L_rma_no is NOT NULL then
         if WF_TRANSFER_SQL.CANCEL_F_RETURN(O_error_message,
                                            I_tsf_rec.header.tsf_no) = FALSE then
            return FALSE;
         end if;
      end if;
   close C_TSF_DETAILS_EXIST;
   else
   -- Update F Order/ F Return
      open C_GET_TSF_F_STATUS;
      fetch C_GET_TSF_F_STATUS into L_tsf_status,
                                    L_fo_status,
                                    L_fr_status;
      close C_GET_TSF_F_STATUS;

      if L_fo_status is NOT NULL then
         L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;
         if WF_TRANSFER_SQL.SYNC_F_ORDER(O_error_message,
                                         L_wf_order_no,
                                         L_tsf_status,
                                         L_action_type,
                                         I_tsf_rec.header.tsf_no,
                                         L_tsf_status,
                                         NULL) = FALSE then
            return FALSE;
         end if;
      elsif L_fr_status is NOT NULL then
         L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;
         if WF_TRANSFER_SQL.SYNC_F_RETURN(O_error_message,
                                          L_rma_no,
                                          L_action_type,
                                          I_tsf_rec.header.tsf_no,
                                          I_tsf_rec.header.status,
                                          I_tsf_rec.header.tsf_type) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
       O_error_message := sql_lib.create_msg('RECORD_LOCKED',
                                             L_table,
                                             'tsf_no: '||I_tsf_rec.header.tsf_no,
                                             'item: '||L_item);
       return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_DETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEMLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XTSF_SQL.CREATE_ITEMLOC';

   L_primary_supp     SUPS.SUPPLIER%TYPE;
   L_primary_cntry    COUNTRY.COUNTRY_ID%TYPE;
   L_receive_as_type  ITEM_LOC.RECEIVE_AS_TYPE%TYPE := NULL;
   L_ranged_ind       ITEM_LOC.RANGED_IND%TYPE;
   L_event_run_type   COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE := NULL;

   cursor C_GET_RUN_TYPE is 
        select event_run_type 
           from cost_event_run_type_config 
         where event_type='NIL';  

BEGIN
   
   FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE:=NULL;
   
   if I_tsf_rec.new_itemloc.items.COUNT = 0 then
      return TRUE;
   end if;

   open C_GET_RUN_TYPE;
   fetch C_GET_RUN_TYPE into L_event_run_type;
   close C_GET_RUN_TYPE;

   if L_event_run_type = FUTURE_COST_EVENT_SQL.SYNC_COST_EVENT_RUN_TYPE then
      FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE := FUTURE_COST_EVENT_SQL.ASYNC_COST_EVENT_RUN_TYPE;
   end if;
   
   for i in I_tsf_rec.new_itemloc.items.FIRST..I_tsf_rec.new_itemloc.items.LAST LOOP
      if not ITEM_SUPP_COUNTRY_SQL.GET_PRIM_SUPP_CNTRY(O_error_message,
                                                       L_primary_supp,
                                                       L_primary_cntry,
                                                       I_tsf_rec.new_itemloc.items(i)) then
         return FALSE;
      end if;

      if I_tsf_rec.new_itemloc.pack_inds(i) = 'Y' and I_tsf_rec.new_itemloc.pack_types(i) = 'V' then
         L_receive_as_type := 'P';  -- vendor packs are always received as pack
      end if;

      --Transfers generated from AIP will have item/loc created with ranged_ind of 'Y' (intentionally ranged).
      --All other transfers generated from external systems (including 'SIM', 'EG', 'CO' transfers) will have
      --item/loc created with ranged_ind of 'N' (incidentally ranged).
      if I_tsf_rec.header.tsf_type in ('AIP') then
         L_ranged_ind := 'Y';
      else
         L_ranged_ind := 'N';
      end if;

      if NEW_ITEM_LOC(O_error_message,
                      I_tsf_rec.new_itemloc.items(i),
                      I_tsf_rec.new_itemloc.locs(i),
                      NULL,
                      NULL,
                      I_tsf_rec.new_itemloc.loc_types(i),
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      L_primary_supp,
                      L_primary_cntry,
                      NULL,
                      NULL,
                      NULL,
                      L_receive_as_type,  -- receive as type
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      L_ranged_ind) = FALSE then
         return FALSE;
      end if;
   end LOOP;

   FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE:=NULL;   
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_ITEMLOC;
--------------------------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_RESV_EXP(O_error_message  IN OUT  VARCHAR2,
                           I_ils_upd_rec    IN      RMSSUB_XTSF.ILS_UPD_REC)
RETURN BOOLEAN IS

   L_program               VARCHAR2(50)            := 'RMSSUB_XTSF_SQL.UPD_ITEM_RESV_EXP';

   L_item                       TSFDETAIL.ITEM%TYPE;
   L_pack_no                    ITEM_LOC_SOH.ITEM%TYPE;
   L_from_loc                   ITEM_LOC.LOC%TYPE;
   L_from_loc_type              ITEM_LOC.LOC_TYPE%TYPE;
   L_to_loc                     ITEM_LOC.LOC%TYPE;
   L_to_loc_type                ITEM_LOC.LOC_TYPE%TYPE;
   L_user                       VARCHAR2(30) := GET_USER;
   L_vdate                      PERIOD.VDATE%TYPE                   := LP_vdate;
   L_tran_code                  TRAN_DATA.TRAN_CODE%TYPE            := 25;
   L_neg_transferred_qty        TSFDETAIL.TSF_QTY%TYPE              := NULL;
   L_found                      BOOLEAN;
   L_frm_non_stkhlding_f_store  VARCHAR2(1)                         := 'N';
   L_to_non_stkhlding_f_store   VARCHAR2(1)                         := 'N';
   L_store_rec                  STORE%ROWTYPE;
   L_exists                     BOOLEAN;
   L_tab_cnt                    NUMBER(10)                          := 0;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(25) := 'ITEM_LOC_SOH';
   L_receive_as_type       ITEM_LOC.RECEIVE_AS_TYPE%TYPE;

   cursor C_LOCK_ILS is
     select 'x'
       from item_loc_soh
      where (item = L_item
        and  loc = L_from_loc
        and  loc_type = L_from_loc_type
        and  L_frm_non_stkhlding_f_store = 'N')
         or (item = L_item
        and  loc = L_to_loc
        and  loc_type = L_to_loc_type
        and  L_to_non_stkhlding_f_store = 'N')
        for update nowait;

   cursor C_LOCK_ILS_COMPS is
      select 'X'
        from item_loc_soh ils
       where ils.item in (select vpq.item
                            from v_packsku_qty vpq,
                                 item_master im
                           where vpq.pack_no = L_pack_no
                             and vpq.item = im.item
                             and im.inventory_ind ='Y')
         and (( ils.loc = L_from_loc
                and ils.loc_type = L_from_loc_type
                and L_frm_non_stkhlding_f_store = 'N')
          or (ils.loc = L_to_loc
               and ils.loc_type = L_to_loc_type
               and L_to_non_stkhlding_f_store = 'N'))
         for update of ils.tsf_reserved_qty,
                       ils.tsf_expected_qty nowait;

   cursor C_PACK_COMP_QTYS is
      select vpq.item item,
             vpq.qty qty
        from v_packsku_qty vpq,
             item_master im
       where vpq.pack_no = L_pack_no
         and vpq.item = im.item
         and im.inventory_ind = 'Y';

   TYPE L_receive_as_type_table IS TABLE OF ITEM_LOC.RECEIVE_AS_TYPE%TYPE INDEX BY BINARY_INTEGER;
   L_receive_as_type_tab L_RECEIVE_AS_TYPE_TABLE;

BEGIN

   if I_ils_upd_rec.items is NULL or I_ils_upd_rec.items.COUNT <= 0 then
      return TRUE;
   end if;

   if I_ils_upd_rec.from_loc_types(1) = 'S' then
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exists,
                                  L_store_rec,
                                  I_ils_upd_rec.from_locs(1)) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE');
         return FALSE;
      end if;
      ---
      if L_store_rec.stockholding_ind = 'N' and L_store_rec.store_type = 'F' then
         L_frm_non_stkhlding_f_store := 'Y';
      end if;
   else
      L_frm_non_stkhlding_f_store := 'N';
   end if;

   if I_ils_upd_rec.to_loc_types(1) = 'S' then
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exists,
                                  L_store_rec,
                                  I_ils_upd_rec.to_locs(1)) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE');
         return FALSE;
      end if;
      ---
      if L_store_rec.stockholding_ind = 'N' and L_store_rec.store_type = 'F' then
         L_to_non_stkhlding_f_store := 'Y';
      end if;
   else
      L_to_non_stkhlding_f_store := 'N';
   end if;

   L_tab_cnt := 0;
   for i in I_ils_upd_rec.items.FIRST..I_ils_upd_rec.items.LAST loop
      L_item          := I_ils_upd_rec.items(i);
      L_from_loc      := I_ils_upd_rec.from_locs(i);
      L_from_loc_type := I_ils_upd_rec.from_loc_types(i);
      L_to_loc        := I_ils_upd_rec.to_locs(i);
      L_to_loc_type   := I_ils_upd_rec.to_loc_types(i);

      if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                L_receive_as_type,
                                                I_ils_upd_rec.items(i),
                                                I_ils_upd_rec.to_locs(i)) = FALSE then
         return FALSE;
      end if;

      L_tab_cnt := L_tab_cnt + 1;
      L_receive_as_type_tab(L_tab_cnt) := L_receive_as_type;

      open C_LOCK_ILS;
      close C_LOCK_ILS;
   end loop;

   -- update the resv qty for the from_loc for all items
   -- item_loc_soh should never be updated for pack item/stores.
   FORALL i in I_ils_upd_rec.items.FIRST..I_ils_upd_rec.items.LAST
      update item_loc_soh
         set tsf_reserved_qty = tsf_reserved_qty - I_ils_upd_rec.existing_qtys(i)
                                                 + I_ils_upd_rec.qtys(i),
             last_update_datetime = sysdate,
             last_update_id = NVL(I_ils_upd_rec.create_id, L_user)
       where item     = I_ils_upd_rec.items(i)
         and loc      = I_ils_upd_rec.from_locs(i)
         and loc_type = I_ils_upd_rec.from_loc_types(i)
         and (I_ils_upd_rec.pack_inds(i) = 'N'
          or  I_ils_upd_rec.from_loc_types(i) != 'S')
         and L_frm_non_stkhlding_f_store = 'N';

   -- update the expected qty for the to_loc for all items
   -- item_loc_soh should never be updated for pack item/stores.
   FORALL i in I_ils_upd_rec.items.FIRST..I_ils_upd_rec.items.LAST
      update item_loc_soh
         set tsf_expected_qty = tsf_expected_qty - I_ils_upd_rec.existing_qtys(i)
                                                 + I_ils_upd_rec.qtys(i),
             last_update_datetime = sysdate,
             last_update_id = NVL(I_ils_upd_rec.create_id, L_user)
       where item     = I_ils_upd_rec.items(i)
         and loc      = I_ils_upd_rec.to_locs(i)
         and loc_type = I_ils_upd_rec.to_loc_types(i)
         and (I_ils_upd_rec.pack_inds(i) = 'N'
          or (I_ils_upd_rec.to_loc_types(i) = 'W' and L_receive_as_type_tab(i) ='P'))
         and L_to_non_stkhlding_f_store = 'N';

   -- update the non-sellable qty for the from_loc for all items
   -- non sellable qty should not be updated for item_loc_soh should never be updated for pack item at stores.

   FOR i in I_ils_upd_rec.items.FIRST..I_ils_upd_rec.items.LAST LOOP
      if NVL(I_ils_upd_rec.inv_statuses(i),-1) != -1 and L_frm_non_stkhlding_f_store = 'N' then

         if ((I_ils_upd_rec.from_loc_types(i) != 'S') or (I_ils_upd_rec.from_loc_types(i) = 'S' and  I_ils_upd_rec.pack_inds(i) = 'N')) then

            L_neg_transferred_qty := ((I_ils_upd_rec.qtys(i)-I_ils_upd_rec.existing_qtys(i)) * -1);  --decrementing non_sellable_qty/inv_status_qty

            if INVADJ_SQL.BUILD_ADJ_UNAVAILABLE(O_error_message,
                                                L_found,
                                                I_ils_upd_rec.items(i),
                                                I_ils_upd_rec.inv_statuses(i),
                                                I_ils_upd_rec.from_loc_types(i),
                                                I_ils_upd_rec.from_locs(i),
                                                L_neg_transferred_qty) = FALSE then
               return FALSE;
            end if;

            -- insert a tran_data record (code 25)
            if INVADJ_SQL.BUILD_ADJ_TRAN_DATA(O_error_message,
                                              L_found,
                                              I_ils_upd_rec.items(i),
                                              I_ils_upd_rec.from_loc_types(i),
                                              I_ils_upd_rec.from_locs(i),
                                              L_neg_transferred_qty,
                                              NULL,             -- total_cost
                                              NULL,             -- total_retail
                                              NULL,             -- order_no
                                              L_program,
                                              L_vdate,
                                              L_tran_code,
                                              NULL,              -- reason
                                              I_ils_upd_rec.inv_statuses(i),
                                              NULL,              -- wac
                                              NULL,              -- unit_retail
                                              NULL) = FALSE then -- pack_ind
               return FALSE;
            end if;

            if INVADJ_SQL.FLUSH_ALL(O_error_message) = FALSE then
               return FALSE;
            end if;

         elsif (I_ils_upd_rec.from_loc_types(i) = 'S' and  I_ils_upd_rec.pack_inds(i) = 'Y') then
            L_pack_no       := I_ils_upd_rec.items(i);

            FOR rec in C_PACK_COMP_QTYS LOOP

               L_neg_transferred_qty := (((I_ils_upd_rec.qtys(i)-I_ils_upd_rec.existing_qtys(i)) * -1) * rec.qty);  --decrementing non_sellable_qty/inv_status_qty

               if INVADJ_SQL.BUILD_ADJ_UNAVAILABLE(O_error_message,
                                                   L_found,
                                                   rec.item,
                                                   I_ils_upd_rec.inv_statuses(i),
                                                   I_ils_upd_rec.from_loc_types(i),
                                                   I_ils_upd_rec.from_locs(i),
                                                   L_neg_transferred_qty) = FALSE then
                  return FALSE;
               end if;

               -- insert a tran_data record (code 25)
               if INVADJ_SQL.BUILD_ADJ_TRAN_DATA(O_error_message,
                                                 L_found,
                                                 rec.item,
                                                 I_ils_upd_rec.from_loc_types(i),
                                                 I_ils_upd_rec.from_locs(i),
                                                 L_neg_transferred_qty,
                                                 NULL,             -- total_cost
                                                 NULL,             -- total_retail
                                                 NULL,             -- order_no
                                                 L_program,
                                                 L_vdate,
                                                 L_tran_code,
                                                 NULL,              -- reason
                                                 I_ils_upd_rec.inv_statuses(i),
                                                 NULL,              -- wac
                                                 NULL,              -- unit_retail
                                                 NULL) = FALSE then -- pack_ind
                  return FALSE;
               end if;

               if INVADJ_SQL.FLUSH_ALL(O_error_message) = FALSE then
                  return FALSE;
               end if;
            END LOOP;

         end if;
      end if;
   END LOOP;

   -- find all pack items and update comp exp and resv qtys accordingly
   -- For stores, tsf_expected_qty and tsf_reserved_qty (NOT pack_comp_exp and pack_comp_resv) should be updated for the component item/stores.
   -- For whs, pack_comp_exp and pack_comp_resv should be updated for the component item/whs.
   for i in I_ils_upd_rec.items.FIRST..I_ils_upd_rec.items.LAST loop
      if I_ils_upd_rec.pack_inds(i) = 'Y' then
         L_pack_no       := I_ils_upd_rec.items(i);
         L_from_loc      := I_ils_upd_rec.from_locs(i);
         L_from_loc_type := I_ils_upd_rec.from_loc_types(i);
         L_to_loc        := I_ils_upd_rec.to_locs(i);
         L_to_loc_type   := I_ils_upd_rec.to_loc_types(i);

         open C_LOCK_ILS_COMPS;
         close C_LOCK_ILS_COMPS;

         for rec in C_PACK_COMP_QTYS loop
            update item_loc_soh
               set pack_comp_resv = decode (I_ils_upd_rec.from_loc_types(i), 'S',
                                            pack_comp_resv,
                                            pack_comp_resv - (I_ils_upd_rec.existing_qtys(i) * rec.qty)
                                                   + (I_ils_upd_rec.qtys(i) * rec.qty)),
                   tsf_reserved_qty = decode (I_ils_upd_rec.from_loc_types(i), 'S',
                                              tsf_reserved_qty - (I_ils_upd_rec.existing_qtys(i) * rec.qty)
                                                   + (I_ils_upd_rec.qtys(i) * rec.qty),
                                              tsf_reserved_qty),
                   last_update_datetime = sysdate,
                   last_update_id = NVL(I_ils_upd_rec.create_id, L_user)
             where item = rec.item
               and loc = I_ils_upd_rec.from_locs(i)
               and loc_type = I_ils_upd_rec.from_loc_types(i)
               and L_frm_non_stkhlding_f_store = 'N';

            update item_loc_soh
               set pack_comp_exp = decode (I_ils_upd_rec.to_loc_types(i), 'S',
                                           pack_comp_exp,
                                           decode(L_receive_as_type_tab(i), 'P',
                                                  (pack_comp_exp - (I_ils_upd_rec.existing_qtys(i) * rec.qty)+(I_ils_upd_rec.qtys(i) * rec.qty)),
                                                  pack_comp_exp )),
                   tsf_expected_qty = decode (I_ils_upd_rec.to_loc_types(i), 'S',
                                              (tsf_expected_qty - (I_ils_upd_rec.existing_qtys(i) * rec.qty)+ (I_ils_upd_rec.qtys(i) * rec.qty)),
                                              decode(L_receive_as_type_tab(i), 'P',
                                                     tsf_expected_qty,
                                                     (tsf_expected_qty - (I_ils_upd_rec.existing_qtys(i) * rec.qty)+ (I_ils_upd_rec.qtys(i) * rec.qty)))),
                   last_update_datetime = sysdate,
                   last_update_id = NVL(I_ils_upd_rec.create_id, L_user)
             where item = rec.item
               and loc = I_ils_upd_rec.to_locs(i)
               and loc_type = I_ils_upd_rec.to_loc_types(i)
               and L_to_non_stkhlding_f_store = 'N';
         end loop;
      end if;
   end loop;

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
       O_error_message := sql_lib.create_msg('RECORD_LOCKED',
                                             L_table,
                                             'tsf_no: '||I_ils_upd_rec.tsf_no,
                                             'item: '||L_item);
       return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_ITEM_RESV_EXP;
--------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_INVENTORY(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_tsf_rec        IN OUT   NOCOPY RMSSUB_XTSF.TSF_REC)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50)   := 'RMSSUB_XTSF_SQL.UPDATE_INVENTORY';

   L_index_ils_upd_rec   BINARY_INTEGER := 0;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_phwh_ind            VARCHAR2(1)    := 'N';

   cursor C_GET_TSFHEAD is
      select *
        from tsfhead
       where tsf_no = IO_tsf_rec.header.tsf_no;

   cursor C_CHK_PWH(L_wh   IN   WH.WH%TYPE) is
      select 'Y'
        from wh
       where wh = L_wh
         and wh = physical_wh;

BEGIN
   --Inventory is updated at external finishers for the 1st leg transfer.
   --Inventory should not be updated for the 2nd leg transfer from an external finisher.
   if IO_tsf_rec.header.from_loc_type = 'E' then
      return TRUE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   -- Initialize LP_ils_upd_rec
   LP_ils_upd_rec.items := ITEM_TBL();
   LP_ils_upd_rec.pack_inds := INDICATOR_TBL();
   LP_ils_upd_rec.from_locs := LOC_TBL();
   LP_ils_upd_rec.from_loc_types := INDICATOR_TBL();
   LP_ils_upd_rec.to_locs := LOC_TBL();
   LP_ils_upd_rec.to_loc_types := INDICATOR_TBL();
   LP_ils_upd_rec.qtys := QTY_TBL();
   LP_ils_upd_rec.existing_qtys := QTY_TBL();
   LP_ils_upd_rec.inv_statuses := RMSSUB_XTSF.INV_TBL();
   -- initialize header
   LP_ils_upd_rec.tsf_no      := IO_tsf_rec.header.tsf_no;
   LP_ils_upd_rec.create_id   := IO_tsf_rec.header.create_id;

   if IO_tsf_rec.header.tsf_type is NULL then
      open C_GET_TSFHEAD;
      fetch C_GET_TSFHEAD into IO_tsf_rec.header;
      close C_GET_TSFHEAD;
   end if;

   -- Include the condition for checking physical wh in case of CO transfer.
   if ((IO_tsf_rec.header.from_loc_type = 'W' or IO_tsf_rec.header.to_loc_type = 'W') and IO_tsf_rec.header.tsf_type = 'CO') then
      open C_CHK_PWH(IO_tsf_rec.header.from_loc);
      fetch C_CHK_PWH into L_phwh_ind;
      close C_CHK_PWH;
      if L_phwh_ind = 'N' then
         open C_CHK_PWH(IO_tsf_rec.header.to_loc);
         fetch C_CHK_PWH into L_phwh_ind;
         close C_CHK_PWH;
      end if;
   end if;

   -- call UPD_PHYSICAL_WHS for transfers involving a physical warehouse
   -- In CO transfer if physical wh is involved then loop through UPD_PHYSICAL_WHS
   -- In CO transfer if virtual wh is invloved loop through else part.
   if (IO_tsf_rec.header.tsf_type = 'EG' or L_phwh_ind = 'Y')  and
      (IO_tsf_rec.header.from_loc_type = 'W' or IO_tsf_rec.header.to_loc_type = 'W') then
      if not UPD_PHYSICAL_WHS(O_error_message,
                              IO_tsf_rec) then
         return FALSE;
      end if;
   else
      for i in IO_tsf_rec.tsf_detail.items.FIRST..IO_tsf_rec.tsf_detail.items.LAST loop
         LP_ils_upd_rec.items.EXTEND;
         LP_ils_upd_rec.pack_inds.EXTEND;
         LP_ils_upd_rec.from_locs.EXTEND;
         LP_ils_upd_rec.from_loc_types.EXTEND;
         LP_ils_upd_rec.to_locs.EXTEND;
         LP_ils_upd_rec.to_loc_types.EXTEND;
         LP_ils_upd_rec.qtys.EXTEND;
         LP_ils_upd_rec.existing_qtys.EXTEND;
         LP_ils_upd_rec.inv_statuses.EXTEND;

         L_index_ils_upd_rec := LP_ils_upd_rec.items.COUNT;

         LP_ils_upd_rec.items(L_index_ils_upd_rec)          := IO_tsf_rec.tsf_detail.items(i);
         LP_ils_upd_rec.pack_inds(L_index_ils_upd_rec)      := IO_tsf_rec.tsf_detail.pack_inds(i);
         LP_ils_upd_rec.from_locs(L_index_ils_upd_rec)      := IO_tsf_rec.header.from_loc;
         LP_ils_upd_rec.from_loc_types(L_index_ils_upd_rec) := IO_tsf_rec.header.from_loc_type;
         LP_ils_upd_rec.to_locs(L_index_ils_upd_rec)        := IO_tsf_rec.header.to_loc;
         LP_ils_upd_rec.to_loc_types(L_index_ils_upd_rec)   := IO_tsf_rec.header.to_loc_type;
         LP_ils_upd_rec.qtys(L_index_ils_upd_rec)           := IO_tsf_rec.tsf_detail.qtys(i);
         LP_ils_upd_rec.existing_qtys(L_index_ils_upd_rec)  := IO_tsf_rec.tsf_detail.existing_qtys(i);
         LP_ils_upd_rec.inv_statuses(L_index_ils_upd_rec)   := IO_tsf_rec.tsf_detail.inv_statuses(i);
      end loop;
   end if;

   if LP_ils_upd_rec.items is NOT NULL and LP_ils_upd_rec.items.COUNT > 0 and IO_tsf_rec.header.status not in ('I') then
      if UPD_ITEM_RESV_EXP(O_error_message,
                           LP_ils_upd_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_INVENTORY;
--------------------------------------------------------------------------------------------------------
FUNCTION UPD_PHYSICAL_WHS(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_rec         IN        RMSSUB_XTSF.TSF_REC)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'RMSSUB_XTSF_SQL.UPD_PHYSICAL_WHS';

BEGIN

   for i in I_tsf_rec.tsf_detail.items.FIRST..I_tsf_rec.tsf_detail.items.LAST loop
      if I_tsf_rec.tsf_detail.qtys(i) > 0 and I_tsf_rec.tsf_detail.existing_qtys(i) = 0 then
         -- process detail create

         if CREATE_TSF_STOCK(O_error_message,
                              I_tsf_rec,
                              i) = FALSE then
            return FALSE;
         end if;
      elsif I_tsf_rec.tsf_detail.qtys(i) > 0 and I_tsf_rec.tsf_detail.existing_qtys(i) > 0 then
         -- process detail modification
         ---
         -- Detail modification can be handled by first backing out the existing qty
         -- based on distribution on tsfitem_inv_flow table. Then call distribution logic
         -- based on the new tsf_qty for create.
         ---
         if BACKOUT_TSF_STOCK(O_error_message,
                              I_tsf_rec,
                              i) = FALSE then
            return FALSE;
         end if;

         if CREATE_TSF_STOCK(O_error_message,
                              I_tsf_rec,
                              i) = FALSE then
            return FALSE;
         end if;
      elsif I_tsf_rec.tsf_detail.qtys(i) = 0 and I_tsf_rec.tsf_detail.existing_qtys(i) > 0 then
         -- process detail delete
         if BACKOUT_TSF_STOCK(O_error_message,
                              I_tsf_rec,
                              i) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop; -- loop through tranfer details

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_PHYSICAL_WHS;
-------------------------------------------------------------------------------
FUNCTION CREATE_TSF_STOCK(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_rec         IN        RMSSUB_XTSF.TSF_REC,
                          I_dtl_index       IN        INTEGER)
RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'RMSSUB_XTSF_SQL.CREATE_TSF_STOCK';

   L_dist_array           DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   L_from_locs            LOC_TBL;
   L_to_locs              LOC_TBL;
   L_dist_qtys            QTY_TBL;
   L_cust_order_loc_ind   WH.CUSTOMER_ORDER_LOC_IND%TYPE;

BEGIN
   -- The call to DISTRIBUTE will fetch the from-to locations mapping with the
   -- distributed quantity for a transfer create scenario
   if I_tsf_rec.header.tsf_type = 'CO' then
      L_cust_order_loc_ind := 'Y';
   else
      L_cust_order_loc_ind := 'N';
   end if;

   if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                  L_dist_array,
                                  I_tsf_rec.tsf_detail.items(I_dtl_index),
                                  I_tsf_rec.header.from_loc,
                                  I_tsf_rec.tsf_detail.qtys(I_dtl_index),
                                  'TRANSFER',
                                  I_tsf_rec.tsf_detail.inv_statuses(I_dtl_index),
                                  I_tsf_rec.header.to_loc_type,
                                  I_tsf_rec.header.to_loc,
                                  NULL,                              --order_no
                                  NULL,                              --shipment
                                  I_tsf_rec.tsf_detail.seq_nos(I_dtl_index),  --seq_no
                                  NULL,                              --cycle_count
                                  NULL,                              --rtv_order_no
                                  NULL,                              --rtv_seq_no
                                  I_tsf_rec.header.tsf_no,
                                  'Y',                               --indicate tsf create
                                  L_cust_order_loc_ind) = FALSE then
      return FALSE;
   end if;

   if L_dist_array is NULL or L_dist_array.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_TSF_DIST_VWH', NULL, NULL, NULL);
      return FALSE;
   end if;

   -- Write distributed result to LP_ils_upd_rec for item_loc_soh update at virtual loc level.
   -- Also write to collections of from_loc/to_loc/dist_qty for bulk insert into tsfitem_inv_flow.
   L_from_locs := LOC_TBL();
   L_to_locs := LOC_TBL();
   L_dist_qtys := QTY_TBL();

   for i in L_dist_array.FIRST..L_dist_array.LAST loop
      if PUT_TSF_INV_MAP(O_error_message,
                         I_tsf_rec.header.tsf_no,
                         I_tsf_rec.tsf_detail.seq_nos(I_dtl_index),
                         I_tsf_rec.tsf_detail.items(I_dtl_index),
                         I_tsf_rec.tsf_detail.pack_inds(I_dtl_index),
                         L_dist_array(i).from_loc,
                         I_tsf_rec.header.from_loc_type,
                         L_dist_array(i).to_loc,
                         I_tsf_rec.header.to_loc_type,
                         0,                                        -- for create, 0 is the existing qty to back out
                         L_dist_array(i).dist_qty,
                         I_tsf_rec.tsf_detail.inv_statuses(I_dtl_index)) = FALSE then    -- for create, dist_qty is the new tsf qty
         return FALSE;
      end if;

      L_from_locs.EXTEND;
      L_to_locs.EXTEND;
      L_dist_qtys.EXTEND;
      L_from_locs(i) := L_dist_array(i).from_loc;
      L_to_locs(i) := L_dist_array(i).to_loc;
      L_dist_qtys(i) := L_dist_array(i).dist_qty;
   end loop;

   -- Merge distribution results to TSFITEM_INV_FLOW
   forall i in L_from_locs.FIRST..L_from_locs.LAST
      merge into tsfitem_inv_flow t
         using (select I_tsf_rec.header.tsf_no tsf_no,
                       I_tsf_rec.tsf_detail.seq_nos(I_dtl_index) seq_no,
                       I_tsf_rec.tsf_detail.items(I_dtl_index) item,
                       L_from_locs(i) from_loc,
                       L_to_locs(i) to_loc
                  from dual) use_this
            on (t.tsf_no         = use_this.tsf_no
                and t.tsf_seq_no = use_this.seq_no
                and t.item       = use_this.item
                and t.from_loc   = use_this.from_loc
                and t.to_loc     = use_this.to_loc)
            when matched then
            update set t.tsf_qty  = L_dist_qtys(i),
                       t.dist_pct = L_dist_qtys(i)/I_tsf_rec.tsf_detail.qtys(I_dtl_index)
            when not matched then
            insert (tsf_no,
                    tsf_seq_no,
                    item,
                    from_loc,
                    to_loc,
                    from_loc_type,
                    to_loc_type,
                    tsf_qty,
                    shipped_qty,
                    dist_pct)
            values (I_tsf_rec.header.tsf_no,
                    I_tsf_rec.tsf_detail.seq_nos(I_dtl_index),
                    I_tsf_rec.tsf_detail.items(I_dtl_index),
                    L_from_locs(i),
                    L_to_locs(i),
                    I_tsf_rec.header.from_loc_type,
                    I_tsf_rec.header.to_loc_type,
                    L_dist_qtys(i),
                    0,
                    L_dist_qtys(i)/I_tsf_rec.tsf_detail.qtys(I_dtl_index));
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_TSF_STOCK;
-------------------------------------------------------------------------------
FUNCTION BACKOUT_TSF_STOCK(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_rec         IN        RMSSUB_XTSF.TSF_REC,
                           I_dtl_index       IN        INTEGER)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'RMSSUB_XTSF_SQL.BACKOUT_TSF_STOCK';

   TYPE tsfitem_inv_flow_tbl is TABLE OF TSFITEM_INV_FLOW%ROWTYPE;
   L_tsfitem_inv_flow_tbl   tsfitem_inv_flow_tbl;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(25) := 'TSFITEM_INV_FLOW';

   cursor C_GET_TSFITEM_INV_FLOW is
      select *
        from tsfitem_inv_flow
       where tsf_no = I_tsf_rec.header.tsf_no
         and tsf_seq_no = I_tsf_rec.tsf_detail.seq_nos(I_dtl_index)
         and item = I_tsf_rec.tsf_detail.items(I_dtl_index)
         for update nowait;

BEGIN
   -- Get the previously distributed qty for 1 tsfdetail from tsfitem_inv_flow table to back out on ITEM_LOC_SOH
   open C_GET_TSFITEM_INV_FLOW;
   fetch C_GET_TSFITEM_INV_FLOW BULK COLLECT INTO L_tsfitem_inv_flow_tbl;
   close C_GET_TSFITEM_INV_FLOW;

   if L_tsfitem_inv_flow_tbl is NULL or L_tsfitem_inv_flow_tbl.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_TSF_DIST_VWH',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- write distibuted results to LP_ils_upd_rec
   for i in L_tsfitem_inv_flow_tbl.FIRST..L_tsfitem_inv_flow_tbl.LAST loop
      if PUT_TSF_INV_MAP(O_error_message,
                         I_tsf_rec.header.tsf_no,
                         I_tsf_rec.tsf_detail.seq_nos(I_dtl_index),
                         I_tsf_rec.tsf_detail.items(I_dtl_index),
                         I_tsf_rec.tsf_detail.pack_inds(I_dtl_index),
                         L_tsfitem_inv_flow_tbl(i).from_loc,
                         I_tsf_rec.header.from_loc_type,
                         L_tsfitem_inv_flow_tbl(i).to_loc,
                         I_tsf_rec.header.to_loc_type,
                         L_tsfitem_inv_flow_tbl(i).tsf_qty, -- for delete, tsf_qty is the existing_qty to back out
                         0,
                         I_tsf_rec.tsf_detail.inv_statuses(I_dtl_index)) = FALSE then                    -- for delete, 0 is the new tsf qty
         return FALSE;
       end if;
   end loop;

   -- delete tsfitem_inv_flow for the tsf detail
   delete from tsfitem_inv_flow
    where tsf_no = I_tsf_rec.header.tsf_no
      and tsf_seq_no = I_tsf_rec.tsf_detail.seq_nos(I_dtl_index)
      and item   = I_tsf_rec.tsf_detail.items(I_dtl_index);

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
       O_error_message := sql_lib.create_msg('RECORD_LOCKED',
                                             L_table,
                                             'tsf_no: '||I_tsf_rec.header.tsf_no,
                                             'item: '||I_tsf_rec.tsf_detail.items(I_dtl_index));
       return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BACKOUT_TSF_STOCK;
-------------------------------------------------------------------------------
FUNCTION PUT_TSF_INV_MAP(O_error_message   IN OUT rtk_errors.rtk_text%TYPE,
                         I_tsf_no          IN     tsfhead.tsf_no%TYPE,
                         I_seq_no          IN     tsfdetail.tsf_seq_no%TYPE,
                         I_item            IN     item_loc.item%TYPE,
                         I_pack_ind        IN     item_master.pack_ind%TYPE,
                         I_from_loc        IN     item_loc.loc%TYPE,
                         I_from_loc_type   IN     item_loc.loc_type%TYPE,
                         I_to_loc          IN     item_loc.loc%TYPE,
                         I_to_loc_type     IN     item_loc.loc_type%TYPE,
                         I_existing_qty    IN     tsfdetail.tsf_qty%TYPE,
                         I_tsf_qty         IN     tsfdetail.tsf_qty%TYPE,
                         I_inv_status      IN     tsfdetail.inv_status%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'RMSSUB_XTSF_SQL.PUT_TSF_INV_MAP';

   L_index_ils_upd_rec  BINARY_INTEGER := 0;

BEGIN
   --If already in map, update qtys
   if LP_ils_upd_rec.items is NOT NULL and LP_ils_upd_rec.items.COUNT > 0 then
      FOR i IN LP_ils_upd_rec.items.FIRST..LP_ils_upd_rec.items.LAST LOOP

         -- LP_ils_upd_rec may already exist if the same item is on the same
         -- transfer multiple times or for a detail modify, the existing tsf qty
         -- is first backed out and then created again.
         -- Accumulate the new tsf qty and the existing qty.
         if LP_ils_upd_rec.from_locs(i) = I_from_loc and
            LP_ils_upd_rec.to_locs(i) = I_to_loc and
            LP_ils_upd_rec.items(i) = I_item then
            ---
            LP_ils_upd_rec.qtys(i) := LP_ils_upd_rec.qtys(i) + I_tsf_qty;
            LP_ils_upd_rec.existing_qtys(i)  :=  LP_ils_upd_rec.existing_qtys(i) + I_existing_qty;
            ---
            return TRUE;
         end if;
      END LOOP;
   end if;

   -- Not exists, add new mapping
   LP_ils_upd_rec.items.EXTEND;
   LP_ils_upd_rec.pack_inds.EXTEND;
   LP_ils_upd_rec.from_locs.EXTEND;
   LP_ils_upd_rec.from_loc_types.EXTEND;
   LP_ils_upd_rec.to_locs.EXTEND;
   LP_ils_upd_rec.to_loc_types.EXTEND;
   LP_ils_upd_rec.qtys.EXTEND;
   LP_ils_upd_rec.existing_qtys.EXTEND;
   LP_ils_upd_rec.inv_statuses.EXTEND;

   L_index_ils_upd_rec := LP_ils_upd_rec.items.COUNT;

   LP_ils_upd_rec.items(L_index_ils_upd_rec)          := I_item;
   LP_ils_upd_rec.pack_inds(L_index_ils_upd_rec)      := I_pack_ind;
   LP_ils_upd_rec.from_locs(L_index_ils_upd_rec)      := I_from_loc;
   LP_ils_upd_rec.from_loc_types(L_index_ils_upd_rec) := I_from_loc_type;
   LP_ils_upd_rec.to_locs(L_index_ils_upd_rec)        := I_to_loc;
   LP_ils_upd_rec.to_loc_types(L_index_ils_upd_rec)   := I_to_loc_type;
   LP_ils_upd_rec.qtys(L_index_ils_upd_rec)           := I_tsf_qty;
   LP_ils_upd_rec.existing_qtys(L_index_ils_upd_rec)  := I_existing_qty;
   LP_ils_upd_rec.inv_statuses(L_index_ils_upd_rec)   := I_inv_status;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PUT_TSF_INV_MAP;
-------------------------------------------------------------------------------
FUNCTION CREATE_SECOND_LEG(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_rec         IN      RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'RMSSUB_XTSF_SQL.CREATE_SECOND_LEG';
   L_return_code    VARCHAR2(5)          := 'TRUE';
   L_tsf_no         TSFHEAD.TSF_NO%TYPE;
   L_tsf_rec        RMSSUB_XTSF.TSF_REC;

BEGIN

   L_tsf_rec := I_tsf_rec;

   NEXT_TRANSFER_NUMBER(L_tsf_no,
                        L_return_code,
                        O_error_message);

   if (L_return_code <> 'TRUE') then
       return FALSE;
   end if;

   --overwrite record with values for the second leg of the transfer
   L_tsf_rec.header.tsf_no        := L_tsf_no;
   L_tsf_rec.header.tsf_parent_no := I_tsf_rec.header.tsf_no;
   L_tsf_rec.header.from_loc      := I_tsf_rec.header.to_loc;
   L_tsf_rec.header.from_loc_type := I_tsf_rec.header.to_loc_type;
   L_tsf_rec.header.to_loc        := I_tsf_rec.header.from_loc;
   L_tsf_rec.header.to_loc_type   := I_tsf_rec.header.from_loc_type;

   if not CREATE_HEADER(O_error_message,
                        L_tsf_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_SECOND_LEG;
------------------------------------------------------------------------------------------
FUNCTION MODIFY_SECOND_LEG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC,
                           I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'RMSSUB_XTSF_SQL.MODIFY_SECOND_LEG';
   L_tsf_rec                RMSSUB_XTSF.TSF_REC;
   L_wf_order_no            WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_rma_no                 WF_RETURN_HEAD.RMA_NO%TYPE;
   L_action_type            VARCHAR2(10);
   L_tsf_status             TSFHEAD.STATUS%TYPE;

   cursor C_GET_2ND_LEG_TSF is
      select tsf_no,
             tsf_parent_no,
             from_loc,
             from_loc_type,
             to_loc,
             to_loc_type
        from tsfhead
       where tsf_parent_no = I_tsf_rec.header.tsf_no;

BEGIN
   L_tsf_rec  := I_tsf_rec;
   ---
   -- Get Child transfer details.
   OPEN C_GET_2ND_LEG_TSF;
   FETCH C_GET_2ND_LEG_TSF into L_tsf_rec.header.tsf_no,
                                L_tsf_rec.header.tsf_parent_no,
                                L_tsf_rec.header.from_loc,
                                L_tsf_rec.header.from_loc_type,
                                L_tsf_rec.header.to_loc,
                                L_tsf_rec.header.to_loc_type;
   if C_GET_2ND_LEG_TSF%NOTFOUND then

      O_error_message := SQL_LIB.CREATE_MSG('CHILD_TSF_NO_EXIST',
                                            I_tsf_rec.header.tsf_no,
                                            NULL,
                                            NULL);
      CLOSE C_GET_2ND_LEG_TSF;
      return FALSE;
   end if;
   CLOSE C_GET_2ND_LEG_TSF;
   ---
   if LOWER(I_message_type) = RMSSUB_XTSF.LP_mod_type then
      -- Modify the header of child transfer in sync with parent transfer.
      ---
      if NOT MODIFY_HEADER(O_error_message,
                           L_tsf_rec) then
         return FALSE;
      end if;
      ---
   elsif LOWER(I_message_type) = RMSSUB_XTSF.LP_dtl_cre_type then
      --- Dummy variable. Multi-Legged Transfer are not supported for Franchise Transaction
      L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;
      -- Add the detail to child transfer in sync with parent transfer.
      ---
      if NOT CREATE_DETAIL(O_error_message,
                           L_tsf_status,
                           L_wf_order_no,
                           L_rma_no,
                           L_tsf_rec,
                           L_action_type) then
         return FALSE;
      end if;
      ---
   elsif LOWER(I_message_type) = RMSSUB_XTSF.LP_dtl_mod_type then
      -- Modify existing detail record of child transfer in sync with parent transfer.
      ---
      if not MODIFY_DETAIL(O_error_message,
                           L_tsf_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MODIFY_SECOND_LEG;
------------------------------------------------------------------------------------------
FUNCTION DELETE_SECOND_LEG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC,
                           I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'RMSSUB_XTSF_SQL.DELETE_SECOND_LEG';
   L_tsf_rec                RMSSUB_XTSF.TSF_REC;

   cursor C_GET_2ND_LEG_TSF is
      select tsf_no,
             tsf_parent_no,
             from_loc,
             from_loc_type,
             to_loc,
             to_loc_type
        from tsfhead
       where tsf_parent_no = I_tsf_rec.header.tsf_no;

BEGIN
   L_tsf_rec := I_tsf_rec;
   ---
   -- Get Child transfer details.
   OPEN C_GET_2ND_LEG_TSF;
   FETCH C_GET_2ND_LEG_TSF into L_tsf_rec.header.tsf_no,
                                L_tsf_rec.header.tsf_parent_no,
                                L_tsf_rec.header.from_loc,
                                L_tsf_rec.header.from_loc_type,
                                L_tsf_rec.header.to_loc,
                                L_tsf_rec.header.to_loc_type;
   if C_GET_2ND_LEG_TSF%NOTFOUND then

      O_error_message := SQL_LIB.CREATE_MSG('CHILD_TSF_NO_EXIST',
                                            I_tsf_rec.header.tsf_no,
                                            NULL,
                                            NULL);
      CLOSE C_GET_2ND_LEG_TSF;
      return FALSE;
   end if;
   ---
   CLOSE C_GET_2ND_LEG_TSF;
   ---
   if LOWER(I_message_type) = RMSSUB_XTSF.LP_del_type then
      -- Delete Child transfer with Parent Transfer.
      if not DELETE_HEADER(O_error_message,
                           L_tsf_rec) then
         return FALSE;
      end if;
    elsif LOWER(I_message_type) = RMSSUB_XTSF.LP_dtl_del_type then
      -- Delete Child transfer detail record in sync with Parent Transfer.
      if not DELETE_DETAIL(O_error_message,
                           L_tsf_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_SECOND_LEG;
------------------------------------------------------------------------------------------
END RMSSUB_XTSF_SQL;
/