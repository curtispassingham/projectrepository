-- File Name : CORESVC_WO_ACTIVITY_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_WORK_ORDER AUTHID CURRENT_USER AS
------------------------------------------------------------------------
----Changes
   template_key       CONSTANT VARCHAR2(255)          := 'WORK_ORDER_DATA';
   template_category  CONSTANT VARCHAR2(255)          := 'RMSINV';
---Changes
   action_new                   VARCHAR2(25)          := 'NEW';
   action_mod                   VARCHAR2(25)          := 'MOD';
   action_del                   VARCHAR2(25)          := 'DEL';
   WO_ACTIVITY_sheet            VARCHAR2(255)         := 'WO_ACTIVITY';
   WO_ACTIVITY$Action           NUMBER                :=1;
   WO_ACTIVITY$COST_TYPE        NUMBER                :=6;
   WO_ACTIVITY$UNIT_COST        NUMBER                :=5;
   WO_ACTIVITY$ACTIVITY_DESC    NUMBER                :=4;
   WO_ACTIVITY$ACTIVITY_CODE    NUMBER                :=3;

   WO_ACTIVITY_TL_sheet            VARCHAR2(255)         := 'WO_ACTIVITY_TL';
   WO_ACTIVITY_TL$Action           NUMBER                :=1;
   WO_ACTIVITY_TL$LANG             NUMBER                :=2;
   WO_ACTIVITY_TL$ACTIVITY_DESC    NUMBER                :=3;
   WO_ACTIVITY_TL$ACTIVITY_CODE    NUMBER                :=4;
   
   WO_TMPL_HEAD_sheet                     VARCHAR2(255)         := 'WO_TMPL_HEAD';
   WO_TMPL_HEAD$Action                    NUMBER                :=1;
   WO_TMPL_HEAD$WO_TMPL_DESC              NUMBER                :=3;
   WO_TMPL_HEAD$WO_TMPL_ID                NUMBER                :=2;

   WO_TMPL_HEAD_TL_sheet                  VARCHAR2(255)         := 'WO_TMPL_HEAD_TL';
   WO_TMPL_HEAD_TL$Action                 NUMBER                :=1;
   WO_TMPL_HEAD_TL$LANG                   NUMBER                :=2;
   WO_TMPL_HEAD_TL$WO_TMPL_ID             NUMBER                :=3;
   WO_TMPL_HEAD_TL$WO_TMPL_DESC           NUMBER                :=4;
   
   WO_TMPL_DTL_sheet                      VARCHAR2(255)         := 'WO_TMPL_DETAIL';
   WO_TMPL_DTL$Action                     NUMBER                :=1;
   WO_TMPL_DTL$COMMENTS                   NUMBER                :=6;
   WO_TMPL_DTL$UNIT_COST                  NUMBER                :=5;
   WO_TMPL_DTL$WO_TMPL_ID                 NUMBER                :=3;
   WO_TMPL_DTL$WO_TMPL_DTL_ID             NUMBER                :=2;
   WO_TMPL_DTL$ACTIVITY_CODE              NUMBER                :=4;
   
-----------------------------------------------------------------
   TYPE WO_TMPL_HEAD_rec_tab IS TABLE OF WO_TMPL_HEAD%ROWTYPE;   
----------------------------------------------------------
   TYPE WO_ACTIVITY_rec_tab IS TABLE OF WO_ACTIVITY%ROWTYPE;
   
   TYPE WO_TMPL_DETAIL_rec_tab IS TABLE OF WO_TMPL_DETAIL%ROWTYPE;
   
---Changes
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';
---Changes
---------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN CHAR DEFAULT 'N')
    RETURN BOOLEAN;
----------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count    IN OUT NUMBER,
                        I_file_id        IN     S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id     IN     NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)   --Changes
   RETURN VARCHAR2;
-------------------------------------------------------------------
END CORESVC_WORK_ORDER;
/
