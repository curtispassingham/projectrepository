CREATE OR REPLACE PACKAGE BODY TSF_STATUS_SQL AS
--------------------------------------------------------------------------------
--Function Name : VALIDATE_TSF_TYPE
--Purpose       : Validates that the transfer can be updated based on tsf_type.
--                A transfer involving a franchise store is considered a franchise
--                transaction even if the tsf_type is not FO/FR, therefore cannot
--                be updated.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_TYPE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE,
                           I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                           I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                           I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                           I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--Function Name : VALIDATE_TSF_STATUS
--Purpose       : Validates that the transfer can be approved, unapproved, deleted
--                based on the current status.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_current_status  IN       TSFHEAD.STATUS%TYPE,
                             I_action          IN       VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION APPROVE_TRANSFER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER IS

   L_program                       VARCHAR2(61) := 'TSF_STATUS_SQL.APPROVE_TRANSFER';
   L_cust_ind                      VARCHAR2(1);
   L_exists                        BOOLEAN;
   L_exists_ind                    VARCHAR2(1);
   L_open_close_ind                VARCHAR2(1);
   L_procedure_key                 VARCHAR2(30) := 'CHECK_UTIL_CODE_EXISTS';
   L_doc_type                      VARCHAR2(10) := 'TSF';
   L_add_1                         ADDR.ADD_1%TYPE;
   L_add_2                         ADDR.ADD_2%TYPE;
   L_add_3                         ADDR.ADD_3%TYPE;
   L_city                          ADDR.CITY%TYPE;
   L_state                         ADDR.STATE%TYPE;
   L_country_id                    COUNTRY_ATTRIB.COUNTRY_ID%TYPE;
   L_post                          ADDR.POST%TYPE;
   L_module                        ADDR.MODULE%TYPE;
   L_instr_exist                   BOOLEAN;
   L_rejected_packs_table          TSF_VALIDATE_SQL.REJECTED_PACK_TABLE;  -- this table will contain all the transformed
                                                                          -- packs that do not include repacking instructions

   L_aip_ind                       SYSTEM_OPTIONS.AIP_IND%TYPE;
   L_tsf_price_exc_wac_ind         SYSTEM_OPTIONS.TSF_PRICE_EXCEED_WAC_IND%TYPE;
   L_rac_rtv_tsf_ind               SYSTEM_OPTIONS.RAC_RTV_TSF_IND%TYPE;
   L_intercompany_transfer_basis   SYSTEM_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE;
   L_wac_exceeded                  BOOLEAN;
   L_tsf_entity_type               VARCHAR2(1);
   L_approve_flag                  VARCHAR2(1);
   L_available                     NUMBER(10);
   L_item                          TSFDETAIL.ITEM%TYPE;
   L_inv_status                    INV_STATUS_QTY.INV_STATUS%TYPE;
   L_from_pwh                      WH.WH%TYPE := NULL;
   L_to_pwh                        WH.WH%TYPE := NULL;
   L_chrg_exists                   BOOLEAN;
   L_chrg_items                    TRANSFER_CHARGE_SQL.CHRG_ITEM_TBL;
   L_finisher_loc_type             ITEM_LOC.LOC_TYPE%TYPE;
   L_default_chrgs_ind             TSF_TYPE.DEFAULT_CHRGS_IND%TYPE;
   L_default_chrgs_2_leg_ind       TSFDETAIL.DEFAULT_CHRGS_2_LEG_IND%TYPE;
   L_mrt_bt                        BOOLEAN := FALSE;
   L_details_exist                 BOOLEAN;
   L_appr_second_leg_only          BOOLEAN:=FALSE;
   L_leg_1_status                  VARCHAR2(1);
   L_new_leg_1_status              VARCHAR2(1);
   L_mrt_rec                       MRT%ROWTYPE;
   L_deft_date                     CODE_DETAIL.CODE_DESC%TYPE;
   L_mrt_desc                      MRT.MRT_DESC%TYPE;
   L_vdate                         PERIOD.VDATE%TYPE := GET_VDATE;

   cursor C_GET_SYSTEM_OPTIONS is
      select aip_ind, 
             tsf_price_exceed_wac_ind,
             rac_rtv_tsf_ind,
             intercompany_transfer_basis
        from system_options;

   cursor C_GET_TSF_REC is
      select tsf_no,
             tsf_type,
             overall_status,
             leg_1_status,
             child_tsf_no,
             from_loc_type,
             from_loc,
             from_loc_entity,
             from_loc_sob_id,
             finisher,
             finisher_type,
             to_loc_type,
             to_loc,
             to_loc_entity,
             to_loc_sob_id,
             mrt_no,
             inventory_type,
             not_after_date,
             approval_date,
             exp_dc_date,
             comment_desc,
             ext_ref_no,
             leg_1_freight_code,
             leg_1_routing_code,
             leg_2_freight_code,
             leg_2_routing_code,
             delivery_date,
             context_type,
             context_value,
             dept,
             create_date
        from v_tsfhead
       where tsf_no = I_tsf_no;
       
   L_tsf_rec C_GET_TSF_REC%ROWTYPE;

   cursor C_GET_MRT_TYPE is
      select mrt_type
        from mrt
       where mrt_no = L_tsf_rec.mrt_no;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;
   
   open C_GET_TSF_REC;
   fetch C_GET_TSF_REC into L_tsf_rec;
   close C_GET_TSF_REC;

   if L_tsf_rec.tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER',
                                            NULL,
                                            NULL,
                                            NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;
   
   --If the transfer is already approved, no further processing is needed
   if L_tsf_rec.overall_status = TSF_CONSTANTS.TSF_STATUS_A then
      return TSF_CONSTANTS.SUCCESS;
   end if;

   if VALIDATE_TSF_TYPE(O_error_message, 
                        L_tsf_rec.tsf_type,
                        L_tsf_rec.from_loc,
                        L_tsf_rec.from_loc_type,
                        L_tsf_rec.to_loc,
                        L_tsf_rec.to_loc_type) = TSF_CONSTANTS.FAILURE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   if VALIDATE_TSF_STATUS(O_error_message,
                          L_tsf_rec.overall_status,
                          'APPROVE') = TSF_CONSTANTS.FAILURE then
      return TSF_CONSTANTS.FAILURE;
   end if;
                    
   open C_GET_SYSTEM_OPTIONS;
   fetch C_GET_SYSTEM_OPTIONS into L_aip_ind, 
                                   L_tsf_price_exc_wac_ind,
                                   L_rac_rtv_tsf_ind,
                                   L_intercompany_transfer_basis;
   close C_GET_SYSTEM_OPTIONS;

   --Delivery date is required for non-RAC/EG/SIM transfers when AIP_IND is Y
   if (L_tsf_rec.delivery_date is null and L_aip_ind = 'Y' and
       L_tsf_rec.tsf_type not in ('SIM', 'EG', 'RAC')) then
      O_error_message := SQL_LIB.CREATE_MSG('ENTER_DELIVERY_DATE',
                                            NULL,
                                            NULL,
                                            NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   --Delivery date cannot be before vdate
   if L_tsf_rec.delivery_date is NOT NULL and L_tsf_rec.delivery_date < L_vdate then
      O_error_message := SQL_LIB.CREATE_MSG('DELIVER_DATE_AFTER',
                                            L_vdate,
                                            NULL,
                                            NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   --Checking the utilization code
   if L_tsf_rec.from_loc_type = TSF_CONSTANTS.LOC_TYPE_S then
         L_module := 'ST';
      else
         L_module := 'WH';
   end if;

   if ADDRESS_SQL.GET_PRIM_ADDR(O_error_message,
                                L_add_1,
                                L_add_2,
                                L_add_3,
                                L_city, 
                                L_state,
                                L_country_id,
                                L_post,
                                L_module,
                                L_tsf_rec.from_loc,
                                NULL) = FALSE then 
      return TSF_CONSTANTS.FAILURE;
   end if; 

   if L10N_SQL.CALL_EXEC_FUNC_DOC(O_error_message, 
                                  L_exists_ind, 
                                  L_procedure_key,
                                  L_country_id,
                                  L_doc_type,
                                  L_tsf_rec.tsf_no) = FALSE then
      return TSF_CONSTANTS.FAILURE;
   end if; 

   if L_exists_ind != TSF_CONSTANTS.YES_IND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_UTILIZATION_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   --Checking detail exists
   if TRANSFER_SQL.DETAILS_EXIST(O_error_message,
                                 L_details_exist,
                                 L_tsf_rec.tsf_no) = FALSE then
      return TSF_CONSTANTS.FAILURE;
   end if;
   ---
   if L_details_exist = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('NO_APPROVE_TSF_DETAILS',
                                            NULL,
                                            NULL,
                                            NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   --check to-loc store is open
   if L_tsf_rec.to_loc_type = TSF_CONSTANTS.LOC_TYPE_S then
      if STORE_ATTRIB_SQL.ACTIVE(L_tsf_rec.to_loc, --I_STORE,
                                 L_tsf_rec.create_date, --I_DATE,
                                 L_open_close_ind, --O_OPEN_CLOSE_IND,
                                 O_error_message) = FALSE then --O_ERROR_MESSAGE
         return TSF_CONSTANTS.FAILURE;
      end if;   
  
      if L_open_close_ind = 'C' then
         O_error_message := SQL_LIB.CREATE_MSG('STORE_CLOSED',
                                               L_tsf_rec.to_loc,
                                               NULL,
                                               NULL);
         return TSF_CONSTANTS.FAILURE;
      end if;
   end if;

   --Check available qty
   --Do NOT need to check available for PL transfers
   if L_tsf_rec.tsf_type != TSF_CONSTANTS.TSF_TYPE_PL then

      --Do NOT need to check available at the original sending location on a multi-legged tsf
      --when the first leg is already closed. This scenario occurs when the user 'unapproves' 
      --the 2nd leg of a tsf on which the first leg is already closed and then re-submits the tsf.
      --Since qty is reserved when submitting a transfer, do NOT check available qty either
      --when the transfer is already in submitted status.

      if (L_tsf_rec.leg_1_status != TSF_CONSTANTS.TSF_STATUS_C and
          L_tsf_rec.overall_status != TSF_CONSTANTS.TSF_STATUS_B) then
         if TRANSFER_SQL.RESERVE(O_error_message,
                                 L_item,
                                 L_inv_status,
                                 L_approve_flag,
                                 L_available,
                                 L_tsf_rec.tsf_no,
                                 L_tsf_rec.from_loc_type,
                                 L_tsf_rec.from_loc) = FALSE then
            return TSF_CONSTANTS.FAILURE;
         end if;
   
         if L_available < 0 then
            L_available := 0;
         end if;

         if L_approve_flag = TSF_CONSTANTS.NO_IND then
            O_error_message := SQL_LIB.CREATE_MSG('QTY_AVAIL_ST_TSF',
                                                  L_item,
                                                  L_tsf_rec.from_loc,
                                                  TO_CHAR(L_available));
            return TSF_CONSTANTS.FAILURE;
         elsif L_approve_flag = 'U' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_STATUS_QTY_TSF',
                                                  L_item,
                                                  TO_CHAR(L_inv_status),
                                                  TO_CHAR(L_available));
            return TSF_CONSTANTS.FAILURE;
         end if;
      end if;
   end if;

   -- ensure if any packs have been unpacked and transformed that
   -- the repacking instructions exist
   if L_tsf_rec.finisher is NOT NULL then
      if TSF_VALIDATE_SQL.PACK_REPACK_INSTR_EXISTS(O_error_message,
                                                   L_instr_exist,
                                                   L_rejected_packs_table,
                                                   L_tsf_rec.from_loc) = FALSE then
         return TSF_CONSTANTS.FAILURE;
      end if;

      if L_instr_exist = FALSE then 
         if TSF_VALIDATE_SQL.EXPLODE_TSF_PACKS(O_error_message,
                                               L_rejected_packs_table,
                                               L_tsf_rec.from_loc) = FALSE then
            return TSF_CONSTANTS.FAILURE;
        end if;
      end if;
   end if;

   -- Ensure the tsf price does not exceed the tsf WAC
   if L_tsf_price_exc_wac_ind = TSF_CONSTANTS.NO_IND then
      if TSF_VALIDATE_SQL.TSF_PRICE_EXCEED_WAC(O_error_message,
                                               L_wac_exceeded,
                                               L_tsf_rec.tsf_no,
                                               L_tsf_rec.from_loc,
                                               L_tsf_rec.from_loc_type) = FALSE then
         return TSF_CONSTANTS.FAILURE;
      end if;

      if L_wac_exceeded = TRUE then
         if TSF_VALIDATE_SQL.OVERWRITE_TSF_PRICE(O_error_message,
                                                 L_tsf_rec.tsf_no,
                                                 L_tsf_rec.child_tsf_no,
                                                 L_tsf_rec.from_loc,
                                                 L_tsf_rec.from_loc_type) = FALSE then
            return TSF_CONSTANTS.FAILURE;
         end if;
      end if;
   end if;   

   ---Get physical WH
   if L_tsf_rec.from_loc_type = TSF_CONSTANTS.LOC_TYPE_W and L_tsf_rec.to_loc_type = TSF_CONSTANTS.LOC_TYPE_W then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                        L_from_pwh,
                                        L_tsf_rec.from_loc) = FALSE then
         return TSF_CONSTANTS.FAILURE;
      end if;

      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_to_pwh,
                                       L_tsf_rec.to_loc) = FALSE then
         return TSF_CONSTANTS.FAILURE;
      end if;
   end if;

   if L_tsf_rec.leg_1_status = TSF_CONSTANTS.TSF_STATUS_I then
      if L_tsf_rec.tsf_type != TSF_CONSTANTS.TSF_TYPE_NS then
         L_appr_second_leg_only := FALSE;
          -- overwrite the child transfer tsfdetail records with the new repacked/transformed items.
          if L_tsf_rec.finisher is NOT NULL then
             if TRANSFER_SQL.UPDATE_TSFDETAIL(O_error_message,
                                              L_tsf_rec.tsf_no) = FALSE then
                return TSF_CONSTANTS.FAILURE;
             end if;
          end if;
          
          if L_tsf_rec.finisher is NULL and 
             L_tsf_rec.tsf_type = TSF_CONSTANTS.TSF_TYPE_IC and 
             L_tsf_rec.from_loc_type = TSF_CONSTANTS.LOC_TYPE_W and 
             L_tsf_rec.to_loc_type = TSF_CONSTANTS.LOC_TYPE_W then
             
             if nvl(L_from_pwh, -999) != nvl(L_to_pwh, -998) then
                 if TRANSFER_SQL.UPD_TSF_RESV_EXP(O_error_message,
                                                  L_tsf_rec.tsf_no,
                                                  TSF_CONSTANTS.ADD_IND,
                                                  L_appr_second_leg_only) = FALSE then
                    return TSF_CONSTANTS.FAILURE;
                 end if;
             end if;
          else
             if TRANSFER_SQL.UPD_TSF_RESV_EXP(O_error_message,
                                                L_tsf_rec.tsf_no,
                                                TSF_CONSTANTS.ADD_IND,
                                                L_appr_second_leg_only) = FALSE then
                return TSF_CONSTANTS.FAILURE;
             end if;
          end if;
       end if;
   end if; --if L_tsf_rec.leg_1_status = TSF_CONSTANTS.TSF_STATUS_I then

   --checking ordcust data
   if L_tsf_rec.tsf_type = TSF_CONSTANTS.TSF_TYPE_CO then
      if TSF_VALIDATE_SQL.CHECK_CUST_RECS(O_error_message,
                                          L_exists_ind,  --O_CUST_EXIST_IND,
                                          I_tsf_no) = FALSE then
         return TSF_CONSTANTS.FAILURE;
      end if;

      if L_exists_ind = TSF_CONSTANTS.NO_IND then
         O_error_message := SQL_LIB.CREATE_MSG('MUST_ENTER_CUST_TSF',
                                               NULL,
                                               NULL,
                                               NULL);
         return TSF_CONSTANTS.FAILURE;
      else
         --copy ordcust_detail based on tsfdetail
         if TRANSFER_SQL.POP_ORDCUST_DETAIL(O_error_message,
                                            I_tsf_no) = FALSE then
            return TSF_CONSTANTS.FAILURE;
         end if;
      end if;
   end if;
         
   --Checking mrt_bt
   if L_tsf_rec.tsf_type != 'EG' then  --'EG' transfers cannot be Approved from RMS, only Closed.
      if L_tsf_rec.mrt_no is not NULL then
         if nvl(L_from_pwh, -999) = nvl(L_to_pwh, -998) then
            L_mrt_bt := TRUE;
         end if;
      end if;
   end if;
   --
   if L_tsf_rec.finisher is NULL and L_tsf_rec.tsf_type NOT IN (TSF_CONSTANTS.TSF_TYPE_BT,TSF_CONSTANTS.TSF_TYPE_IC) then
      if nvl(L_from_pwh, -999) = nvl(L_to_pwh, -998) then
         if L_tsf_rec.mrt_no is NOT NULL then
            L_mrt_bt := TRUE;
         else
            O_error_message := SQL_LIB.CREATE_MSG('SAME_PWH_TSF_NO_ALLOW',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return TSF_CONSTANTS.FAILURE;
         end if;
      else
         if L_tsf_rec.mrt_no is NOT NULL then
            L_mrt_bt := FALSE;
         end if;
      end if;
   end if;

   if L_tsf_rec.tsf_type != TSF_CONSTANTS.TSF_TYPE_BT and L_mrt_bt = FALSE then
      --Checking transfer_entity_type
      if L_tsf_rec.tsf_type IN (TSF_CONSTANTS.TSF_TYPE_RAC, TSF_CONSTANTS.TSF_TYPE_RV) and L_tsf_rec.mrt_no is NOT NULL then
         open C_GET_MRT_TYPE;
         fetch C_GET_MRT_TYPE into L_tsf_entity_type;
         close C_GET_MRT_TYPE;
      elsif L_tsf_rec.tsf_type IN (TSF_CONSTANTS.TSF_TYPE_RAC, TSF_CONSTANTS.TSF_TYPE_RV) and L_tsf_rec.mrt_no is NULL and L_rac_rtv_tsf_ind = 'A' then
         L_tsf_entity_type := TSF_CONSTANTS.TSF_ENTITY_TYPE_A;
      elsif L_tsf_rec.tsf_type IN (TSF_CONSTANTS.TSF_TYPE_RAC, TSF_CONSTANTS.TSF_TYPE_RV) and L_tsf_rec.mrt_no is NULL and L_rac_rtv_tsf_ind = 'E' then
         if (L_tsf_rec.from_loc_entity = L_tsf_rec.to_loc_entity and L_intercompany_transfer_basis = 'T')
             or (L_tsf_rec.from_loc_sob_id = L_tsf_rec.to_loc_sob_id and L_intercompany_transfer_basis = 'B') then
            L_tsf_entity_type := TSF_CONSTANTS.TSF_ENTITY_TYPE_A;
         else
            L_tsf_entity_type := TSF_CONSTANTS.TSF_ENTITY_TYPE_E;
         end if;
      end if;

      L_appr_second_leg_only := FALSE;

      if L_tsf_rec.tsf_type in (TSF_CONSTANTS.TSF_TYPE_RAC, TSF_CONSTANTS.TSF_TYPE_RV) and L_tsf_entity_type = TSF_CONSTANTS.TSF_ENTITY_TYPE_E then
         if TRANSFER_SQL.APPROVE(O_error_message,
                                 L_new_leg_1_status,
                                 L_tsf_rec.tsf_no,
                                 L_tsf_rec.child_tsf_no,
                                 TSF_CONSTANTS.TSF_TYPE_IC,
                                 FALSE,
                                 L_tsf_rec.inventory_type,
                                 L_tsf_rec.from_loc_type,
                                 L_tsf_rec.from_loc,
                                 L_tsf_rec.finisher_type,
                                 L_tsf_rec.finisher,
                                 L_tsf_rec.to_loc_type,
                                 L_tsf_rec.to_loc) = FALSE then
             return TSF_CONSTANTS.FAILURE;
          end if;
      else
         -- L_tsf_rec.tsf_type NOT in ('RAC', 'RV') or L_tsf_entity_type != 'E'
         if TRANSFER_SQL.APPROVE(O_error_message,
                                 L_new_leg_1_status,
                                 L_tsf_rec.tsf_no,
                                 L_tsf_rec.child_tsf_no,
                                 L_tsf_rec.tsf_type,
                                 L_appr_second_leg_only,
                                 L_tsf_rec.inventory_type,
                                 L_tsf_rec.from_loc_type,
                                 L_tsf_rec.from_loc,
                                 L_tsf_rec.finisher_type,
                                 L_tsf_rec.finisher,
                                 L_tsf_rec.to_loc_type,
                                 L_tsf_rec.to_loc) = FALSE then
             return TSF_CONSTANTS.FAILURE;
         end if;
      end if; --if L_tsf_rec.tsf_type in ('RAC', 'RV') and L_tsf_entity_type = 'E' then

      if TRANSFER_CHARGE_SQL.GET_DEFAULT_CHRGS_IND(O_error_message,
                                                   L_default_chrgs_ind,
                                                   L_tsf_rec.tsf_type) = FALSE then
         return TSF_CONSTANTS.FAILURE;
      end if;

      if L_default_chrgs_ind = TSF_CONSTANTS.YES_IND then
         if L_tsf_rec.finisher is NOT NULL then
            if L_tsf_rec.finisher_type = TSF_CONSTANTS.FINISHER_TYPE_I then
               L_finisher_loc_type := TSF_CONSTANTS.LOC_TYPE_W;
            else
               L_finisher_loc_type := TSF_CONSTANTS.PARTNER_TYPE_E;
            end if;
            --
            if TRANSFER_CHARGE_SQL.CHARGES_EXIST_2ND_LEG(O_error_message,
                                                         L_chrg_exists,
                                                         L_chrg_items,
                                                         L_tsf_rec.tsf_no,
                                                         L_tsf_rec.child_tsf_no,
                                                         L_tsf_rec.finisher,
                                                         L_finisher_loc_type,
                                                         L_tsf_rec.to_loc,
                                                         L_tsf_rec.to_loc_type) = FALSE then
               return TSF_CONSTANTS.FAILURE;
            end if;
            --
            if L_chrg_exists = TRUE then
               L_default_chrgs_2_leg_ind := TSF_CONSTANTS.YES_IND;     
            end if;
            --
            if TRANSFER_CHARGE_SQL.DEFAULT_CHRGS_2ND_LEG(O_error_message,
                                                         L_tsf_rec.tsf_no,
                                                         L_tsf_rec.tsf_type,
                                                         L_tsf_rec.child_tsf_no,
                                                         L_tsf_rec.finisher,
                                                         L_finisher_loc_type,
                                                         L_tsf_rec.to_loc,
                                                         L_tsf_rec.to_loc_type,
                                                         L_default_chrgs_2_leg_ind,
                                                         L_chrg_items) = FALSE then
               return TSF_CONSTANTS.FAILURE;
            end if;
         end if; --if L_finisher is not null then
      end if; --if L_default_chrgs_ind = 'Y' then
   else
      --It's a BT tsf type, or a MRT book transfer
      if TRANSFER_SQL.MANUAL_BT_EXECUTE (O_error_message,
                                         L_tsf_rec.tsf_no,
                                         L_tsf_rec.from_loc,
                                         L_tsf_rec.to_loc,
                                         L_tsf_rec.tsf_type) = FALSE then
         return TSF_CONSTANTS.FAILURE;
      end if;
      --
      if L_tsf_rec.mrt_no is NOT NULL then
         -- MRT_ITEM_LOC.RECEIVED_QTY must be updated for an MRT book transfer
         if MRT_SQL.UPDATE_RECEIVED_QTY(O_error_message,
                                        L_tsf_rec.mrt_no,
                                        L_tsf_rec.tsf_no) = FALSE then
            return TSF_CONSTANTS.FAILURE;
         end if;
      end if;
   end if;

   if nvl(L_new_leg_1_status, TSF_CONSTANTS.TSF_STATUS_A) != TSF_CONSTANTS.TSF_STATUS_C then
      L_new_leg_1_status := TSF_CONSTANTS.TSF_STATUS_A;
   end if;
   if L_tsf_rec.tsf_type = TSF_CONSTANTS.TSF_TYPE_BT then
      L_new_leg_1_status := TSF_CONSTANTS.TSF_STATUS_C;
   end if;

   L_tsf_rec.approval_date := L_vdate;
   
   if L_tsf_rec.approval_date > L_tsf_rec.not_after_date then
      O_error_message := SQL_LIB.CREATE_MSG('ENTER_NOT_AFTER_DATE',
                                            NULL,
                                            NULL,
                                            NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   -- Populate the NOT_AFTER_DATE
   if L_tsf_rec.mrt_no is NOT NULL then
      if MRT_ATTRIB_SQL.GET_MRT_INFO(O_error_message,
                                     L_mrt_rec,
                                     L_exists,
                                     L_tsf_rec.mrt_no) = FALSE then
         return TSF_CONSTANTS.FAILURE;
      end if;

      if L_exists then
         L_tsf_rec.not_after_date := L_mrt_rec.tsf_not_after_date;
         L_mrt_desc               := L_mrt_rec.mrt_desc;
      end if;
   else
      if L_tsf_rec.not_after_date is NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'DEFT',
                                       'DATE',
                                       L_deft_date) = FALSE then
            return TSF_CONSTANTS.FAILURE;
         end if;
         L_tsf_rec.not_after_date := L_tsf_rec.approval_date + L_deft_date;
      end if;
   end if;

   if L_tsf_rec.not_after_date < L_vdate then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DATE_EARLY',
                                            'Not After Date.',
                                            NULL,
                                            NULL);
      return TSF_CONSTANTS.FAILURE;
   end if; 

   if L_tsf_rec.tsf_type in (TSF_CONSTANTS.TSF_TYPE_RAC, TSF_CONSTANTS.TSF_TYPE_RV, TSF_CONSTANTS.TSF_TYPE_IC) and L_tsf_rec.mrt_no is NOT NULL then
      if MRT_SQL.UPDATE_TRANSFER_QTY(O_error_message,
                                     L_tsf_rec.mrt_no,
                                     L_tsf_rec.from_loc) = FALSE then
         return TSF_CONSTANTS.FAILURE;
      end if;
   end if;

   if TRANSFER_SQL.UPDATE_TSFHEAD(O_error_message,
                                  L_tsf_rec.tsf_no,
                                  L_tsf_rec.inventory_type,
                                  L_tsf_rec.tsf_type,
                                  L_new_leg_1_status,
                                  TSF_CONSTANTS.TSF_STATUS_A, --leg_2_status
                                  L_tsf_rec.exp_dc_date,
                                  L_tsf_rec.comment_desc,
                                  L_tsf_rec.ext_ref_no,
                                  L_tsf_rec.from_loc,
                                  L_tsf_rec.from_loc_type,
                                  L_tsf_rec.leg_1_freight_code,
                                  L_tsf_rec.leg_1_routing_code,
                                  L_tsf_rec.finisher,
                                  L_tsf_rec.finisher_type,
                                  L_tsf_rec.leg_2_freight_code,
                                  L_tsf_rec.leg_2_routing_code,
                                  TSF_CONSTANTS.NO_IND,  --I_IN_PROCESS_LEG_1
                                  L_tsf_rec.to_loc,
                                  L_tsf_rec.to_loc_type,
                                  TSF_CONSTANTS.NO_IND,  --I_IN_PROCESS_LEG_2
                                  L_tsf_rec.delivery_date,
                                  L_tsf_rec.mrt_no,
                                  L_tsf_rec.not_after_date,
                                  L_tsf_rec.context_type,
                                  L_tsf_rec.context_value,
                                  L_tsf_rec.dept) = FALSE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   return TSF_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return TSF_CONSTANTS.FAILURE;
END APPROVE_TRANSFER;
--------------------------------------------------------------------------------
FUNCTION UNAPPROVE_TRANSFER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER IS

   L_program   VARCHAR2(61) := 'TSF_STATUS_SQL.UNAPPROVE_TRANSFER';

   L_tsfhead_row   V_TSFHEAD%ROWTYPE;

   cursor C_LOCK_TSFHEAD(CP_tsf_no TSFHEAD.TSF_NO%TYPE) is
      select 'x'
        from tsfhead
       where tsf_no = CP_tsf_no or tsf_parent_no = CP_tsf_no
         for update nowait;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   if TRANSFER_SQL.GET_TSFHEAD_INFO(O_error_message,
                                    L_tsfhead_row,
                                    I_tsf_no) = FALSE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   if VALIDATE_TSF_TYPE(O_error_message, 
                        L_tsfhead_row.tsf_type,
                        L_tsfhead_row.from_loc,
                        L_tsfhead_row.from_loc_type,
                        L_tsfhead_row.to_loc,
                        L_tsfhead_row.to_loc_type) = TSF_CONSTANTS.FAILURE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   if VALIDATE_TSF_STATUS(O_error_message,
                          L_tsfhead_row.overall_status,
                          'UNAPPROVE') = TSF_CONSTANTS.FAILURE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   if TRANSFER_SQL.UNAPPROVE(O_error_message,
                             L_tsfhead_row.tsf_no,
                             L_tsfhead_row.leg_1_status,
                             L_tsfhead_row.inventory_type,
                             L_tsfhead_row.from_loc,
                             L_tsfhead_row.from_loc_type,
                             L_tsfhead_row.finisher,
                             L_tsfhead_row.finisher_type,
                             L_tsfhead_row.to_loc,
                             L_tsfhead_row.to_loc_type,
                             L_tsfhead_row.tsf_type)  = FALSE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   open C_LOCK_TSFHEAD(L_tsfhead_row.tsf_no);
   close C_LOCK_TSFHEAD;

   update tsfhead
      set status = TSF_CONSTANTS.TSF_STATUS_I
    where tsf_no = L_tsfhead_row.tsf_no 
       or tsf_parent_no = L_tsfhead_row.tsf_no;
   ---
   if L_tsfhead_row.overall_status = 'B' then
      if REJECT_TSF_NOTIFICATION(O_error_message,
                                 I_tsf_no)= TSF_CONSTANTS.FAILURE then
         return TSF_CONSTANTS.FAILURE;
      end if;
   end if;

   return TSF_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return TSF_CONSTANTS.FAILURE;
END UNAPPROVE_TRANSFER;
--------------------------------------------------------------------------------
FUNCTION UPDATE_TO_DELETE_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'TSF_STATUS_SQL.UPDATE_TO_DELETE_STATUS';
   L_exists          BOOLEAN;

   cursor C_GET_TSF_REC is
      select tsf_no,
             tsf_type,
             overall_status,
             leg_1_status,
             leg_2_status,
             child_tsf_no,
             from_loc_type,
             from_loc,
             from_loc_entity,
             from_loc_sob_id,
             finisher,
             finisher_type,
             to_loc_type,
             to_loc,
             to_loc_entity,
             to_loc_sob_id,
             mrt_no,
             inventory_type,
             not_after_date,
             approval_date,
             exp_dc_date,
             comment_desc,
             ext_ref_no,
             leg_1_freight_code,
             leg_1_routing_code,
             leg_2_freight_code,
             leg_2_routing_code,
             delivery_date,
             context_type,
             context_value,
             dept
        from v_tsfhead
       where tsf_no = I_tsf_no;
       
   L_tsf_rec C_GET_TSF_REC%ROWTYPE;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   open C_GET_TSF_REC;
   fetch C_GET_TSF_REC into L_tsf_rec;
   close C_GET_TSF_REC;

   if L_tsf_rec.tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER',
                                            NULL,
                                            NULL,
                                            NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   --If the transfer is already deleted, no further processing is needed
   if L_tsf_rec.overall_status = TSF_CONSTANTS.TSF_STATUS_D then
      return TSF_CONSTANTS.SUCCESS;
   end if;

   if VALIDATE_TSF_TYPE(O_error_message, 
                        L_tsf_rec.tsf_type,
                        L_tsf_rec.from_loc,
                        L_tsf_rec.from_loc_type,
                        L_tsf_rec.to_loc,
                        L_tsf_rec.to_loc_type) = TSF_CONSTANTS.FAILURE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   if VALIDATE_TSF_STATUS(O_error_message,
                          L_tsf_rec.overall_status,
                          'DELETE') = TSF_CONSTANTS.FAILURE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   --If leg 1 is in closed or shipped statuses, stock has already been moved, therefore should not delete the current tsf.
   if L_tsf_rec.leg_1_status in (TSF_CONSTANTS.TSF_STATUS_C, TSF_CONSTANTS.TSF_STATUS_S) then
      O_error_message := SQL_LIB.CREATE_MSG('DEL_PROCESSED_TSF',
                                            NULL,
                                            NULL,
                                            NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   --close open allocations linked to the transfer
   if TRANSFER_SQL.CHECK_OPEN_ALLOC(O_error_message, 
                                    L_exists,
                                    I_tsf_no) = FALSE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   if L_exists = TRUE then
      if TRANSFER_SQL.CLOSE_ALLOC(O_error_message, 
                                  I_tsf_no) = FALSE then
         return TSF_CONSTANTS.FAILURE;
      end if;
   end if;

   --reverse the quantity reservation for submitted and approved transfers
   if L_tsf_rec.overall_status in (TSF_CONSTANTS.TSF_STATUS_A, TSF_CONSTANTS.TSF_STATUS_B) then
      if TRANSFER_SQL.UPD_TSF_RESV_EXP(O_error_message,
                                       I_tsf_no,
                                       'D',  --I_ADD_DELETE_IND,
                                       false) = FALSE then --I_APPR_SECOND_LEG
         return TSF_CONSTANTS.FAILURE;
      end if;

      if TRANSFER_SQL.UPD_NON_SELLABLE_QTYS_WHEN_DEL(O_error_message, 
                                                     I_tsf_no) = FALSE then --BigDecimal I_TSF_NO
         return TSF_CONSTANTS.FAILURE;
      end if;
   end if;

   if L_tsf_rec.leg_2_status is NOT NULL then
      L_tsf_rec.leg_2_status := TSF_CONSTANTS.TSF_STATUS_D;
   end if;

   --update tsfhead to Deleted status
   if TRANSFER_SQL.UPDATE_TSFHEAD(O_error_message,
                                  L_tsf_rec.tsf_no,
                                  L_tsf_rec.inventory_type,
                                  L_tsf_rec.tsf_type,
                                  TSF_CONSTANTS.TSF_STATUS_D, --leg_1_status
                                  L_tsf_rec.leg_2_status,     --leg_2_status
                                  L_tsf_rec.exp_dc_date,
                                  L_tsf_rec.comment_desc,
                                  L_tsf_rec.ext_ref_no,
                                  L_tsf_rec.from_loc,
                                  L_tsf_rec.from_loc_type,
                                  L_tsf_rec.leg_1_freight_code,
                                  L_tsf_rec.leg_1_routing_code,
                                  L_tsf_rec.finisher,
                                  L_tsf_rec.finisher_type,
                                  L_tsf_rec.leg_2_freight_code,
                                  L_tsf_rec.leg_2_routing_code,
                                  TSF_CONSTANTS.NO_IND,  --I_IN_PROCESS_LEG_1
                                  L_tsf_rec.to_loc,
                                  L_tsf_rec.to_loc_type,
                                  TSF_CONSTANTS.NO_IND,  --I_IN_PROCESS_LEG_2
                                  L_tsf_rec.delivery_date,
                                  L_tsf_rec.mrt_no,
                                  L_tsf_rec.not_after_date,
                                  L_tsf_rec.context_type,
                                  L_tsf_rec.context_value,
                                  L_tsf_rec.dept) = FALSE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   return TSF_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return TSF_CONSTANTS.FAILURE;
END UPDATE_TO_DELETE_STATUS;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_TYPE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE,
                           I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                           I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                           I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                           I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'TSF_STATUS_SQL.VALIDATE_TSF_TYPE';

   L_oms_ind         VARCHAR2(1);
   L_f_action_type   VARCHAR2(1); 

   cursor C_GET_SYSTEM_OPTIONS is
      select oms_ind
        from product_config_options;

BEGIN

   --Validate only transfer types that can be edited in the RMS transfer screen can be updated here
   if I_tsf_type in (TSF_CONSTANTS.TSF_TYPE_EG, 
                     TSF_CONSTANTS.TSF_TYPE_AIP, 
                     TSF_CONSTANTS.TSF_TYPE_SIM, 
                     TSF_CONSTANTS.TSF_TYPE_SG, 
                     TSF_CONSTANTS.TSF_TYPE_FO,
                     TSF_CONSTANTS.TSF_TYPE_FR) then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_UPD_TSF_TYPE',
                                             I_tsf_type,
                                             NULL,
                                             NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   --Validate CO transfers in a OMS_IND = 'Y' configuration cannot be updated here
   open C_GET_SYSTEM_OPTIONS;
   fetch C_GET_SYSTEM_OPTIONS into L_oms_ind;
   close C_GET_SYSTEM_OPTIONS;

   if L_oms_ind = 'Y' and I_tsf_type = TSF_CONSTANTS.TSF_TYPE_CO then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_UPD_TSF_TYPE',
                                             I_tsf_type,
                                             NULL,
                                             NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   --Validate transfers involving a franchise location cannot be updated here
   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                L_f_action_type,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_to_loc,
                                                I_to_loc_type) = FALSE then
      return TSF_CONSTANTS.FAILURE;
   end if;

   if L_f_action_type in (WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER, WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN) then
      O_error_message := SQL_LIB.CREATE_MSG('NO_CHG_FORDRET',
                                            NULL,
                                            NULL,
                                            NULL);
      return TSF_CONSTANTS.FAILURE;
   end if;

   return TSF_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return TSF_CONSTANTS.FAILURE;
END VALIDATE_TSF_TYPE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_current_status  IN       TSFHEAD.STATUS%TYPE,
                             I_action          IN       VARCHAR2)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'TSF_STATUS_SQL.VALIDATE_TSF_STATUS';

BEGIN

   if I_action = 'APPROVE' then
      -- can only approve a transfer in input or submitted status
      if I_current_status not in ('I', 'B') then   --input or submitted
         O_error_message := SQL_LIB.CREATE_MSG('TSF_MUST_BE_INPUT_SUBMIT',
                                                NULL,
                                                NULL,
                                                NULL);
         return TSF_CONSTANTS.FAILURE;
      end if;

   elsif I_action = 'UNAPPROVE' then
      -- can only unapprove a transfer in approved status
      if I_current_status not in ('A', 'B') then
         O_error_message := SQL_LIB.CREATE_MSG('UNAPPROVE_TSF',
                                                NULL,
                                                NULL,
                                                NULL);
         return TSF_CONSTANTS.FAILURE;
      end if;

   elsif I_action = 'DELETE' then
      -- can only delete a transfer that hasn't been processed yet (i.e. in input, submitted, approved status)
      if I_current_status not in ('I', 'B', 'A') then
         O_error_message := SQL_LIB.CREATE_MSG('DEL_PROCESSED_TSF',
                                                NULL,
                                                NULL,
                                                NULL);
         return TSF_CONSTANTS.FAILURE;
      end if;
   end if;

   return TSF_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return TSF_CONSTANTS.FAILURE;
END VALIDATE_TSF_STATUS;
--------------------------------------------------------------------------------
FUNCTION REJECT_TSF_NOTIFICATION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER IS

   L_program              VARCHAR2(61)   := 'TSF_STATUS_SQL.REJECT_TSF_NOTIFICATION';
   L_task_flow_url        VARCHAR2(4000) := '/WEB-INF/oracle/retail/apps/rms/inventory/view/transfer/setup/header/flow/MaintainTransferHeaderFlow.xml#MaintainTransferHeaderFlow'||'|pmTsfNo='||I_tsf_no||'|pmMode=EDIT';
   
   L_notification_id      NUMBER       := RAF_NOTIFICATION_SEQ.NEXTVAL;
   L_notify_context       VARCHAR2(4000);
   L_notification_type    NUMBER       := 5348;
   L_severity             NUMBER       := 1;
   L_application_code     VARCHAR2(50) := 'Rms';
   L_notify_desc          VARCHAR2(255);
   L_notify_title         VARCHAR2(200);
   L_error_type           VARCHAR2(5)  := NULL;
   L_notification_active  VARCHAR2(1)  := 'U';
   L_notif_comnd_cls_name VARCHAR2(10) := null;
   L_create_date          TIMESTAMP    := to_timestamp(to_char(sysdate,'MM/DD/RRRR HH12:MI:SS'), 'MM/DD/RRRR HH12:MI:SS');
   L_recipent_id          VARCHAR2(64);
   L_created_by           VARCHAR2(64) := GET_USER; 
   L_launchable           VARCHAR2(1)  := 'Y';
   L_obj_ver_num          NUMBER       := 1;
   
   cursor C_GET_DESC_TL is
      select description
        from raf_notification_type_tl
       where notification_type = L_notification_type
         and language = userenv('LANG');
         
   cursor C_GET_RECIPENT_ID is
      select create_id
        from tsfhead
       where tsf_no = I_tsf_no;
       
BEGIN
   open C_GET_RECIPENT_ID;
   fetch C_GET_RECIPENT_ID into L_recipent_id;
   close C_GET_RECIPENT_ID;
   --
   if upper(L_recipent_id) != upper(L_created_by) then
   
      open C_GET_DESC_TL;
      fetch C_GET_DESC_TL into L_notify_title;
      close C_GET_DESC_TL;
      --
      L_notify_desc := SQL_LIB.CREATE_MSG('NOTIFICATION_REJECT_TSF',I_tsf_no);
      SQL_LIB.API_MSG(L_error_type, L_notify_desc);
      --
      L_notify_context := 'title=' || L_notify_title || '|url=' || L_task_flow_url;
      if RAF_NOTIFICATION_TASK_PKG.CREATE_NOTIFY_TASK_AUTOTRAN(O_error_message,
                                                               L_notification_id,
                                                               L_notify_context,
                                                               L_notification_type,
                                                               L_severity,
                                                               L_application_code,
                                                               L_notify_desc,
                                                               L_notification_active,
                                                               L_notif_comnd_cls_name,
                                                               L_create_date,
                                                               L_recipent_id,
                                                               L_created_by,
                                                               L_launchable,
                                                               L_obj_ver_num) = TSF_CONSTANTS.FAILURE then
         return TSF_CONSTANTS.FAILURE;
      end if;
   end if;

   return TSF_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_GET_DESC_TL%ISOPEN then close C_GET_DESC_TL; end if;
      if C_GET_RECIPENT_ID%ISOPEN then close C_GET_RECIPENT_ID; end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return TSF_CONSTANTS.FAILURE;
END REJECT_TSF_NOTIFICATION;
--------------------------------------------------------------------------------
END TSF_STATUS_SQL;
/
