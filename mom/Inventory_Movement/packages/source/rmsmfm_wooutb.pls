
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSMFM_WOOUT AS

TYPE rowid_TBL      is table of ROWID INDEX BY BINARY_INTEGER;

-- depending upon the message type, there will come a point in the publication process
-- when error retry will no longer be possible.  This variable will track whether or
-- not that point has been reached.
LP_error_status        VARCHAR2(1):=NULL;

   --Finisher is the 'from' loc for the second leg
   cursor C_HEADER_INFO(CV_tsf_wo_id TSF_WO_HEAD.TSF_WO_ID%TYPE) is
      select th.tsf_no,
             th.from_loc,
             th.from_loc_type,
             th.inventory_type,
             th.tsf_parent_no,
             NVL(w.physical_wh, th.from_loc)
        from tsfhead th,
             tsf_wo_head twh,
             wh w 
       where twh.tsf_wo_id = CV_tsf_wo_id
         and twh.tsf_no    = th.tsf_parent_no
         and th.from_loc = w.wh(+);


--------------------------------------------------------
       /*Private Program Declarations*/
--------------------------------------------------------

---
PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT         VARCHAR2,
                        O_error_msg       IN  OUT         VARCHAR2,
                        O_message             OUT         RIB_OBJECT,
                        O_bus_obj_id      IN  OUT  nocopy RIB_BUSOBJID_TBL,
                        O_routing_info    IN  OUT  nocopy RIB_ROUTINGINFO_TBL,
                        I_tsf_wo_id       IN              woout_mfqueue.tsf_wo_id%TYPE,
                        I_seq_no          IN              woout_mfqueue.seq_no%TYPE);
---
FUNCTION PROCESS_QUEUE_RECORD( O_error_msg           OUT        VARCHAR2,
                               O_break_loop          OUT        BOOLEAN,
                               O_message         IN  OUT nocopy RIB_OBJECT,
                               O_routing_info    IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                               O_bus_obj_id      IN  OUT nocopy RIB_BUSOBJID_TBL,
                               O_message_type    IN  OUT        VARCHAR2,
                               I_tsf_wo_id       IN             woout_mfqueue.tsf_wo_id%TYPE,
                               I_hdr_published   IN             woout_pub_info.published%TYPE,
                               I_pub_status      IN             woout_mfqueue.pub_status%TYPE,
                               I_seq_no          IN             woout_mfqueue.seq_no%TYPE,
                               I_rowid           IN             ROWID,
                               O_keep_queue      IN OUT         BOOLEAN)
RETURN BOOLEAN;
---
FUNCTION MAKE_CREATE( O_error_msg       IN OUT        VARCHAR2,
                      O_message         IN OUT nocopy RIB_OBJECT,
                      O_routing_info    IN OUT nocopy RIB_ROUTINGINFO_TBL,
                      I_tsf_wo_id       IN            tsf_wo_head.tsf_wo_id%TYPE,
                      I_seq_no          IN            woout_mfqueue.seq_no%TYPE,
                      I_max_details     IN            rib_settings.max_details_to_publish%TYPE,
                      I_rowid           IN            ROWID,
                      O_keep_queue      IN OUT        BOOLEAN)

RETURN BOOLEAN;
---
FUNCTION BUILD_HEADER_OBJECT( O_error_msg        IN OUT        VARCHAR2,
                              O_message          IN OUT nocopy RIB_OBJECT,
                              O_rib_wooutref_rec IN OUT nocopy "RIB_WOOutRef_REC",
                              O_routing_info     IN OUT nocopy RIB_ROUTINGINFO_TBL,
                              I_tsf_wo_id        IN            tsf_wo_head.tsf_wo_id%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_OBJECTS( O_error_msg                IN OUT VARCHAR2,
                               O_detail_message           IN OUT nocopy "RIB_WOOutDtl_TBL",
                               O_xform_message            IN OUT nocopy "RIB_WOOutXForm_TBL",
                               O_packing_message          IN OUT nocopy "RIB_WOOutPacking_TBL",
                               O_tsf_wo_detail_rowid      IN OUT nocopy rowid_TBL,
                               O_tsf_wo_detail_size       IN OUT        BINARY_INTEGER,
                               O_tsf_xform_detail_rowid   IN OUT nocopy rowid_TBL,
                               O_tsf_xform_detail_size    IN OUT        BINARY_INTEGER,
                               O_tsf_packing_rowid        IN OUT nocopy rowid_TBL,
                               O_tsf_packing_size         IN OUT        BINARY_INTEGER,
                               O_delete_rowid_ind         IN OUT        VARCHAR2,
                               I_message_type             IN            woout_mfqueue.message_type%TYPE,
                               I_tsf_wo_id                IN            tsf_wo_head.tsf_wo_id%TYPE,
                               I_tsf_parent_no            IN            tsf_wo_head.tsf_no%TYPE,
                               I_to_loc                   IN            item_loc.loc%TYPE,
                               I_max_details              IN            rib_settings.max_details_to_publish%TYPE)

RETURN BOOLEAN;
---
FUNCTION BUILD_WODTL_OBJECT( O_error_msg            IN OUT        VARCHAR2,
                             O_detail_message       IN OUT nocopy "RIB_WOOutDtl_TBL",
                             O_tsf_wo_detail_rowid  IN OUT nocopy rowid_TBL,
                             O_tsf_wo_detail_size   IN OUT        BINARY_INTEGER,
                             IO_rib_wooutdtl_rec    IN OUT nocopy "RIB_WOOutDtl_REC",
                             I_tsf_wo_id            IN            tsf_wo_head.tsf_wo_id%TYPE,
                             I_tsf_parent_no        IN            tsfhead.tsf_parent_no%TYPE,
                             I_item                 IN            tsf_wo_detail.item%TYPE,
                             I_to_loc               IN            item_loc.loc%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_PACKING_OBJECT ( O_error_msg             IN OUT VARCHAR2,
                                O_packing_message       IN OUT nocopy "RIB_WOOutPacking_TBL",
                                IO_rib_wooutpacking_rec IN OUT nocopy "RIB_WOOutPacking_REC",
                                I_tsf_packing_id        IN            tsf_packing.tsf_packing_id%TYPE)
RETURN BOOLEAN;
---
FUNCTION DELETE_QUEUE_REC( O_error_msg   OUT VARCHAR2,
                           I_seq_no   IN     woout_mfqueue.seq_no%TYPE)
RETURN BOOLEAN;
---
FUNCTION LOCK_THE_BLOCK( O_error_msg        OUT VARCHAR2,
                         O_queue_locked     OUT BOOLEAN,
                         I_tsf_wo_id     IN     woout_mfqueue.tsf_wo_id%TYPE)
RETURN BOOLEAN;
---
FUNCTION UPDATE_DETAILS(O_error_msg        OUT VARCHAR2,
                        I_tsf_wo_id     IN      woout_mfqueue.tsf_wo_id%TYPE,
                        I_tsf_parent_no IN      tsfhead.tsf_no%TYPE)
RETURN BOOLEAN;
---

--------------------------------------------------------------------------------
           /*** Public Program Bodies***/
--------------------------------------------------------------------------------

FUNCTION ADDTOQ(O_error_msg            OUT VARCHAR2,
                I_message_type      IN     VARCHAR2,
                I_tsf_wo_id         IN     tsf_wo_head.tsf_wo_id%TYPE)
RETURN BOOLEAN IS

   
   L_thread_no          rib_settings.num_threads%TYPE:=NULL;
   L_published          VARCHAR2(1):='N';
   L_max_details        rib_settings.max_details_to_publish%TYPE:=NULL;
   L_num_threads        rib_settings.num_threads%TYPE:=NULL;
   L_min_time_lag       rib_settings.minutes_time_lag%TYPE:=NULL;

   L_status_code        VARCHAR2(1):=NULL;
   L_hdr_notfound       BOOLEAN := FALSE;
   
   cursor C_HEAD is
      select who.thread_no,
             published
        from woout_pub_info who
       where who.tsf_wo_id = I_tsf_wo_id;

BEGIN	  
   -- A HDR_ADD is sent when the transfer is approved. 
   -- A header record will exist for HDR_UNAPRV and HDR_ADD when the associated transfer
   -- has previously been unapproved.
   open C_HEAD;
   fetch C_HEAD into L_thread_no,
                     L_published;
                     
   if C_HEAD%NOTFOUND then
      L_hdr_notfound := TRUE;
   end if;
   close C_HEAD;

   --Since WO pub info header recs aren't inserted until initial approval there
   --may or may not be a WO header record for HDR_DEL.  We are certain to have a
   --header rec if the transfer is being un-approved.
   if L_hdr_notfound and I_message_type = HDR_UNAPRV then
      O_error_msg := SQL_LIB.CREATE_MSG('PUB_INFO_NOT_FOUND',
                                        'woout_pub_info',
                                        I_tsf_wo_id,
                                        NULL);
      return FALSE;
   end if;
   
   --If there is no WO header rec and the message type is HDR_DEL assume that
   --the transfer was never approved so the WO was never published.
   if L_hdr_notfound and I_message_type = HDR_DEL then
      return TRUE;
   end if;
   
   
   -- If the header record was not found then the transfer has never before been approved.  
   -- Insert a header record before publishing it.   
   if I_message_type = HDR_ADD and L_hdr_notfound then    
      API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                   O_error_msg,
                                   L_max_details,
                                   L_num_threads,
                                   L_min_time_lag,
                                   RMSMFM_WOOUT.FAMILY);
      if L_status_code in (API_CODES.UNHANDLED_ERROR) then
         return FALSE;
      end if;
    
      L_thread_no := MOD(I_tsf_wo_id, L_num_threads)+1;
      
      insert into woout_pub_info( tsf_wo_id, 
                                  thread_no,
                                  published)
                         values ( I_tsf_wo_id,
                                  L_thread_no,
                                  'N' );
   end if;
   
   if I_message_type = HDR_DEL then
      -- cleanup pub info and queue   
      if L_published = 'N' then

         delete from woout_mfqueue
          where tsf_wo_id = I_tsf_wo_id;
      
        
         delete from woout_pub_info
          where tsf_wo_id = I_tsf_wo_id;
           
         return TRUE;
      end if;
      
   elsif I_message_type = HDR_UNAPRV then
      -- cleanup queue 
      if L_published = 'N' then
         
         delete from woout_mfqueue
          where tsf_wo_id = I_tsf_wo_id;
          
         return TRUE;
      end if;
   end if;
   
   insert into woout_mfqueue ( SEQ_NO,
                               TSF_WO_ID,
                               MESSAGE_TYPE,
                               THREAD_NO,
                               FAMILY,
                               CUSTOM_MESSAGE_TYPE,
                               PUB_STATUS,
                               TRANSACTION_NUMBER,
                               TRANSACTION_TIME_STAMP)         
                       values( woout_mfsequence.nextval,
                               I_tsf_wo_id,
                               I_message_type,
                               L_thread_no,
                               RMSMFM_WOOUT.FAMILY,
                               NULL,
                               'U',
                               I_tsf_wo_id,
                               SYSDATE);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WOOUT.ADDTOQ',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;
--------------------------------------------------------------------------------

PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1)
IS


   L_break_loop         BOOLEAN := FALSE;
   L_message_type       woout_mfqueue.message_type%TYPE:=NULL;

   L_tsf_wo_id          tsf_wo_head.tsf_wo_id%TYPE:=NULL;
   L_seq_no             woout_mfqueue.seq_no%TYPE:=NULL;
   L_seq_limit          woout_mfqueue.seq_no%TYPE:=0;

   L_pub_status         woout_mfqueue.pub_status%TYPE:=NULL;
   L_rowid              ROWID  :=NULL;

   L_hdr_published      woout_pub_info.published%TYPE:=NULL;

   L_hosp               VARCHAR2(1) := 'N';
   L_keep_queue         BOOLEAN := FALSE;
   L_queue_locked       BOOLEAN := FALSE;

   cursor C_QUEUE is
      select q.tsf_wo_id,
             q.message_type,
             q.pub_status,
             q.seq_no,
             q.rowid
        from woout_mfqueue q
       where q.seq_no = (select min(q2.seq_no)
                           from woout_mfqueue q2
                          where q2.thread_no  = I_thread_val
                            and q2.pub_status = 'U'
                            and q2.seq_no     > L_seq_limit)
         and q.thread_no = I_thread_val;
  
   cursor C_HOSP is
      select 'Y'
        from woout_mfqueue
       where tsf_wo_id = L_tsf_wo_id
         and pub_status = API_CODES.HOSPITAL;

   cursor C_WHO is
      select who.published
        from woout_pub_info who
       where who.tsf_wo_id = L_tsf_wo_id;

BEGIN
  
   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   LOOP

      O_message := null;

      open C_QUEUE;
      fetch C_QUEUE into L_tsf_wo_id,
                         L_message_type,
                         L_pub_status,
                         L_seq_no,
                         L_rowid;
      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
      close C_QUEUE;

      if LOCK_THE_BLOCK( O_error_msg,
                         L_queue_locked,
                         L_tsf_wo_id) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked = FALSE then

         open  C_HOSP;
         fetch C_HOSP into L_hosp;
         close C_HOSP;
      
         if L_hosp = 'Y' then
            O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                              NULL, NULL, NULL);
            raise PROGRAM_ERROR;
         end if;
   
         
         --Query this seperatly rather than joining with C_QUEUE so that we don't
         --return status code NO_MSG (non-fatal) when the header object row is not found.
         open  C_WHO;
         fetch C_WHO into L_hdr_published;
         close C_WHO;
   
         if PROCESS_QUEUE_RECORD( O_error_msg,
                                  L_break_loop,
                                  O_message,
                                  O_routing_info,
                                  O_bus_obj_id,
                                  L_message_type,
                                  L_tsf_wo_id,
                                  L_hdr_published,
                                  L_pub_status,
                                  L_seq_no,
                                  L_rowid,
                                  L_keep_queue) = FALSE then        
            raise PROGRAM_ERROR;
         end if;
     
         if L_break_loop = TRUE then
            O_message_type   := L_message_type;
            EXIT;
         end if;

      else 
         L_seq_limit := L_seq_no;
      end if;

   END LOOP;

   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_tsf_wo_id);
   end if;
   
EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_tsf_wo_id,
                    L_seq_no);
END GETNXT;
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT)
IS
   L_seq_no           woout_mfqueue.seq_no%TYPE:=NULL;

   L_break_loop       BOOLEAN := FALSE;

   L_tsf_wo_id        woout_mfqueue.tsf_wo_id%TYPE:=NULL;
   L_hdr_published    woout_pub_info.published%TYPE:=NULL;
   L_message_type     woout_mfqueue.message_type%TYPE:=NULL;
   L_pub_status       woout_mfqueue.pub_status%TYPE:=NULL;
   L_rowid            ROWID:=NULL;

   L_keep_queue       BOOLEAN := FALSE;
   L_queue_locked     BOOLEAN := FALSE;

   cursor C_RETRY_QUEUE is
      select q.tsf_wo_id,
             who.published hdr_published,
             q.message_type,
             q.pub_status,
             q.rowid
        from woout_mfqueue q,
             woout_pub_info who
       where q.seq_no = L_seq_no
         and q.tsf_wo_id = who.tsf_wo_id;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;


   --get info from routing info
   ---assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;

   O_message := null;

   --get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_tsf_wo_id,
                            L_hdr_published,
                            O_message_type,
                            L_pub_status,
                            L_rowid;
   close C_RETRY_QUEUE;

   if L_tsf_wo_id IS NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;


   if LOCK_THE_BLOCK( O_error_msg,
                      L_queue_locked,
                      L_tsf_wo_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then

      if PROCESS_QUEUE_RECORD( O_error_msg,
                               L_break_loop,
                               O_message,
                               O_routing_info,
                               O_bus_obj_id,
                               O_message_type,
                               L_tsf_wo_id,
                               L_hdr_published,
                               L_pub_status,
                               L_seq_no,
                               L_rowid,
                               L_keep_queue) = FALSE then
         raise PROGRAM_ERROR;
      end if;
  
      if O_message IS NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         if L_keep_queue then
            --If the keep queue indicator is true, the maximum number of details has been exceeded for the message
            O_status_code := API_CODES.INCOMPLETE_MSG;
         else
            O_status_code := API_CODES.NEW_MSG;
         end if;
         O_bus_obj_id := RIB_BUSOBJID_TBL(L_tsf_wo_id);
      end if;
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_tsf_wo_id,
                    L_seq_no);

END PUB_RETRY;

--------------------------------------------------------------------------------
           /*** Private Program Bodies***/
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT         VARCHAR2,
                        O_error_msg       IN  OUT         VARCHAR2,
                        O_message             OUT         RIB_OBJECT,
                        O_bus_obj_id      IN  OUT  nocopy RIB_BUSOBJID_TBL,
                        O_routing_info    IN  OUT  nocopy RIB_ROUTINGINFO_TBL,
                        I_tsf_wo_id       IN              woout_mfqueue.tsf_wo_id%TYPE,
                        I_seq_no          IN              woout_mfqueue.seq_no%TYPE)
IS

   L_rib_wooutref_rec              "RIB_WOOutRef_REC":=NULL;
   L_finisher                      ITEM_LOC.LOC%TYPE:=NULL;
   L_finisher_loc_type             ITEM_LOC.LOC_TYPE%TYPE:=NULL;
   L_physical_loc                  ITEM_LOC.LOC%TYPE:=NULL;
   L_inventory_type                TSFHEAD.INVENTORY_TYPE%TYPE:=NULL;
   L_tsf_no                        TSFHEAD.TSF_NO%TYPE:=NULL;
   L_tsf_parent_no                 TSFHEAD.TSF_NO%TYPE:=NULL;
   L_error_type                    VARCHAR2(5) := NULL;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then

      O_bus_obj_id    := RIB_BUSOBJID_TBL(I_tsf_wo_id);
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no', I_seq_no,null,null,null,null));

      open C_HEADER_INFO(I_tsf_wo_id);
      fetch C_HEADER_INFO into L_tsf_no,
                               L_finisher,
                               L_finisher_loc_type,
                               L_inventory_type,
                               L_tsf_parent_no,
                               L_physical_loc;
      close C_HEADER_INFO;
           
      L_rib_wooutref_rec := "RIB_WOOutRef_REC"(0,                     --RIB_OID             NUMBER
                                             I_tsf_wo_id,           --WO_ID               NUMBER(12) 
                                             L_finisher,            --DC_DEST_ID          VARCHAR2(10)
                                             to_char(L_tsf_no),     --DISTRO_NBR          VARCHAR2(10),
                                             NULL,                  --WOOUTDTLREF_TBL     "RIB_WOOutDtlRef_TBL"
                                             to_char(L_tsf_parent_no));  --DISTRO_PARENT_NBR    VARCHAR2(10)

      O_message := L_rib_wooutref_rec;

      update woout_mfqueue
         set pub_status = LP_error_status
       where seq_no    = I_seq_no;

   end if;
   
   /* Pass out parsed error message */ 
   SQL_LIB.API_MSG(L_error_type, 
                   O_error_msg); 


EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code, 
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR, 
                                'RMSMFM_WOOUT'); 
END HANDLE_ERRORS;

--------------------------------------------------------------------------------

FUNCTION PROCESS_QUEUE_RECORD( O_error_msg           OUT        VARCHAR2,
                               O_break_loop          OUT        BOOLEAN,
                               O_message         IN  OUT nocopy RIB_OBJECT,
                               O_routing_info    IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                               O_bus_obj_id      IN  OUT nocopy RIB_BUSOBJID_TBL,
                               O_message_type    IN  OUT        VARCHAR2,
                               I_tsf_wo_id       IN             woout_mfqueue.tsf_wo_id%TYPE,
                               I_hdr_published   IN             woout_pub_info.published%TYPE,
                               I_pub_status      IN             woout_mfqueue.pub_status%TYPE,
                               I_seq_no          IN             woout_mfqueue.seq_no%TYPE,
                               I_rowid           IN             ROWID,
                               O_keep_queue      IN OUT         BOOLEAN)
RETURN BOOLEAN IS
   
   L_status_code        VARCHAR2(1) := NULL;
   L_max_details        rib_settings.max_details_to_publish%TYPE:=NULL;
   L_num_threads        rib_settings.num_threads%TYPE:=NULL;
   L_min_time_lag       rib_settings.minutes_time_lag%TYPE:=NULL;
   
   L_tsf_no             tsfhead.tsf_no%TYPE:=NULL;
   L_tsf_parent_no      tsfhead.tsf_parent_no%TYPE:=NULL;
   L_finisher           item_loc.loc%TYPE:=NULL;
   L_finisher_loc_type  item_loc.loc_type%TYPE:=NULL;
   L_physical_loc       item_loc.loc%TYPE:=NULL;
   L_inventory_type     tsfhead.inventory_type%TYPE:=NULL;
   
   L_rib_routing_rec    RIB_ROUTINGINFO_REC := NULL;

BEGIN 
   O_break_loop := TRUE;
   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_WOOUT.FAMILY);
   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   if O_message_type = HDR_DEL then
      
      open C_HEADER_INFO(I_tsf_wo_id);
      fetch C_HEADER_INFO into L_tsf_no,
                               L_finisher,
                               L_finisher_loc_type,
                               L_inventory_type,
                               L_tsf_parent_no,
                               L_physical_loc;
      close C_HEADER_INFO;

      O_message := "RIB_WOOutRef_REC"(0,                     --RIB_OID             NUMBER
                                    I_tsf_wo_id,           --WO_ID               NUMBER(12) 
                                    L_finisher,            --DC_DEST_ID          VARCHAR2(10)
                                    to_char(L_tsf_no),     --DISTRO_NBR          VARCHAR2(10),
                                    NULL,                  --WOOUTDTLREF_TBL     "RIB_WOOutDtlRef_TBL"
                                    to_char(L_tsf_parent_no));      --DISTRO_PARENT_NBR   VARCHAR2(10)

      -- create routing info --
      O_routing_info := RIB_ROUTINGINFO_TBL();
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', L_physical_loc, 'to_phys_loc_type', L_finisher_loc_type, null,null);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      delete from woout_pub_info
       where tsf_wo_id = I_tsf_wo_id;

      if DELETE_QUEUE_REC( O_error_msg,
                           I_seq_no) = FALSE then
         return FALSE;
      end if;

   elsif O_message_type = HDR_UNAPRV then
      --the Rib should treat it just like a hdr_del. However, internal table processes are slightly different
      O_message_type := HDR_DEL;
      
      open C_HEADER_INFO(I_tsf_wo_id);
      fetch C_HEADER_INFO into L_tsf_no,
                               L_finisher,
                               L_finisher_loc_type,
                               L_inventory_type,
                               L_tsf_parent_no,
                               L_physical_loc;
      close C_HEADER_INFO;

      O_message := "RIB_WOOutRef_REC"(0,                     --RIB_OID             NUMBER
                                    I_tsf_wo_id,           --WO_ID               NUMBER(12) 
                                    L_finisher,            --DC_DEST_ID          VARCHAR2(10)
                                    to_char(L_tsf_no),     --DISTRO_NBR          VARCHAR2(10),
                                    NULL,                  --WOOUTDTLREF_TBL     "RIB_WOOutDtlRef_TBL"
                                    to_char(L_tsf_parent_no));      --DISTRO_PARENT_NBR   VARCHAR2(10)

      -- create routing info --
      O_routing_info := RIB_ROUTINGINFO_TBL();
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', L_physical_loc, 'to_phys_loc_type', L_finisher_loc_type, null,null);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

      LP_error_status := API_CODES.UNHANDLED_ERROR;
    
      if UPDATE_DETAILS( O_error_msg,
                         I_tsf_wo_id,
                         L_tsf_parent_no) = FALSE then
         return FALSE;
      end if;
      
      update woout_pub_info
         set published = 'N'
       where tsf_wo_id = I_tsf_wo_id;

      if DELETE_QUEUE_REC( O_error_msg,
                           I_seq_no) = FALSE then
         return FALSE;
      end if;
   elsif I_hdr_published = 'N' or I_hdr_published = 'I' then
   
      if I_hdr_published = 'N' then
         O_message_type := HDR_ADD;
      else
         O_message_type := DTL_ADD;
      end if;
      
      -- publish the entire Work Order 
      if MAKE_CREATE( O_error_msg,
                      O_message,
                      O_routing_info,
                      I_tsf_wo_id,
                      I_seq_no,
                      L_max_details,
                      I_rowid,
                      O_keep_queue) = FALSE then
         return FALSE;
      end if;
        
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_WOOOUT.PROCESS_QUEUE_RECORD',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_QUEUE_RECORD;
--------------------------------------------------------------------------------
FUNCTION MAKE_CREATE( O_error_msg       IN OUT        VARCHAR2,
                      O_message         IN OUT nocopy RIB_OBJECT,
                      O_routing_info    IN OUT nocopy RIB_ROUTINGINFO_TBL,
                      I_tsf_wo_id       IN            tsf_wo_head.tsf_wo_id%TYPE,
                      I_seq_no          IN            woout_mfqueue.seq_no%TYPE,
                      I_max_details     IN            rib_settings.max_details_to_publish%TYPE,
                      I_rowid           IN            ROWID,
                      O_keep_queue      IN OUT        BOOLEAN)

RETURN BOOLEAN IS
   
   L_rib_routing_rec          RIB_ROUTINGINFO_REC := NULL;
   L_rib_wooutref_rec         "RIB_WOOutRef_REC"  := NULL;
   L_rib_wooutdesc_rec        "RIB_WOOutDesc_REC" := NULL;

   L_rib_wooutdtl_tbl         "RIB_WOOutDtl_TBL" := NULL;
   L_rib_wooutxform_tbl       "RIB_WOOutXForm_TBL" := NULL;
   L_rib_wooutpacking_tbl     "RIB_WOOutPacking_TBL" := NULL;
   L_tsf_wo_detail_rowid      rowid_TBL;
   L_tsf_wo_detail_size       BINARY_INTEGER := 0;
   L_tsf_xform_detail_rowid   rowid_TBL;
   L_tsf_xform_detail_size    BINARY_INTEGER := 0;
   L_tsf_packing_rowid        rowid_TBL;
   L_tsf_packing_size         BINARY_INTEGER := 0;
   L_delete_rowid_ind         VARCHAR2(1) := 'Y';

   PROGRAM_ERROR              EXCEPTION;

BEGIN

   if BUILD_HEADER_OBJECT( O_error_msg,
                           L_rib_wooutdesc_rec,
                           L_rib_wooutref_rec,
                           O_routing_info,
                           I_tsf_wo_id) = FALSE then
      return FALSE;
   end if;
   

   if BUILD_DETAIL_OBJECTS( O_error_msg,
                            L_rib_wooutdtl_tbl,
                            L_rib_wooutxform_tbl,
                            L_rib_wooutpacking_tbl,
                            L_tsf_wo_detail_rowid,
                            L_tsf_wo_detail_size,
                            L_tsf_xform_detail_rowid,
                            L_tsf_xform_detail_size,
                            L_tsf_packing_rowid,
                            L_tsf_packing_size,
                            L_delete_rowid_ind,
                            null,  --message_type
                            I_tsf_wo_id,
                            to_number(L_rib_wooutdesc_rec.distro_parent_nbr),
                            L_rib_wooutdesc_rec.dc_dest_id,
                            I_max_details) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;
   
   
   update woout_pub_info
      set published   = DECODE(L_delete_rowid_ind, 'Y', 'Y', 'I')
    where tsf_wo_id = I_tsf_wo_id;

   --We don't have detail updates/deletes so there will 
   --only be the header queue rec to delete   
   if L_delete_rowid_ind = 'Y' then
      if DELETE_QUEUE_REC( O_error_msg,
                           I_seq_no) = FALSE then
         return FALSE;
      end if;
   end if;
   
   if L_tsf_wo_detail_size > 0 then
      FORALL i IN 1..L_tsf_wo_detail_size
         update tsf_wo_detail set publish_ind = 'Y' 
          where rowid = L_tsf_wo_detail_rowid(i);
   end if;
   
   if L_tsf_xform_detail_size > 0 then
      FORALL i IN 1..L_tsf_xform_detail_size
         update tsf_xform_detail set publish_ind = 'Y'
          where rowid = L_tsf_xform_detail_rowid(i);
   end if;
   
   --publish/update an entire set
   if L_tsf_packing_size > 0 then
      FORALL i IN 1..L_tsf_packing_size
         update tsf_packing set publish_ind = 'Y'
          where rowid = L_tsf_packing_rowid(i);
   end if;
   
   -- add the details to the header
   L_rib_wooutdesc_rec.wooutdtl_tbl := L_rib_wooutdtl_tbl;
   L_rib_wooutdesc_rec.wooutxform_tbl := L_rib_wooutxform_tbl;
   L_rib_wooutdesc_rec.wooutpacking_tbl := L_rib_wooutpacking_tbl;
   
   O_message := L_rib_wooutdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WOOUT.MAKE_CREATE',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END MAKE_CREATE;
--------------------------------------------------------------------------------
FUNCTION BUILD_HEADER_OBJECT( O_error_msg        IN OUT        VARCHAR2,
                              O_message          IN OUT nocopy RIB_OBJECT,
                              O_rib_wooutref_rec IN OUT nocopy "RIB_WOOutRef_REC",
                              O_routing_info     IN OUT nocopy RIB_ROUTINGINFO_TBL,
                              I_tsf_wo_id        IN            tsf_wo_head.tsf_wo_id%TYPE)
RETURN BOOLEAN IS

   L_tsf_no             tsfhead.tsf_no%TYPE:=NULL;
   L_tsf_parent_no      tsfhead.tsf_parent_no%TYPE:=NULL;
   L_finisher           item_loc.loc%TYPE:=NULL;
   L_finisher_loc_type  item_loc.loc_type%TYPE:=NULL;
   L_inventory_type     tsfhead.inventory_type%TYPE:=NULL;
   L_physical_loc       item_loc.loc%TYPE:=NULL;
   
   L_rib_wooutdesc_rec  "RIB_WOOutDesc_REC" := NULL;
   L_rib_routing_rec    RIB_ROUTINGINFO_REC := NULL;
   	
BEGIN

   open C_HEADER_INFO(I_tsf_wo_id);
   fetch C_HEADER_INFO into L_tsf_no,
                            L_finisher,
                            L_finisher_loc_type,
                            L_inventory_type,
                            L_tsf_parent_no,
                            L_physical_loc;
   close C_HEADER_INFO;
   
   L_rib_wooutdesc_rec := "RIB_WOOutDesc_REC"(
             0,                --RIB_OID            NUMBER
             I_tsf_wo_id,      --wo_id              NUMBER(12)
	       L_finisher,       --dc_dest_id         VARCHAR2(10)
	       to_char(L_tsf_no),--distro_nbr	    VARCHAR2(10)
	       NULL,             --WOOutDtl_TBL       "RIB_WOOutDtl_TBL"
	       to_char(L_tsf_parent_no),  --distro_parent_nbr  VARCHAR2(10)
	       NULL,             --WOOutXForm_TBL     "RIB_WOOutXForm_TBL"
	       NULL,             --WOOutPacking_TBL   "RIB_WOOutPacking_TBL"
	       L_inventory_type);--inv_type           VARCHAR2(6)
	     
	     
   O_routing_info := RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', L_physical_loc, 'to_phys_loc_type', L_finisher_loc_type, null,null);
   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   O_message := L_rib_wooutdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WOOUT.BUILD_HEADER_OBJECT',
                                        TO_CHAR(SQLCODE));
      return FALSE;

END BUILD_HEADER_OBJECT;
--------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_OBJECTS( O_error_msg                IN OUT VARCHAR2,
                               O_detail_message           IN OUT nocopy "RIB_WOOutDtl_TBL",
                               O_xform_message            IN OUT nocopy "RIB_WOOutXForm_TBL",
                               O_packing_message          IN OUT nocopy "RIB_WOOutPacking_TBL",
                               O_tsf_wo_detail_rowid      IN OUT nocopy rowid_TBL,
                               O_tsf_wo_detail_size       IN OUT        BINARY_INTEGER,
                               O_tsf_xform_detail_rowid   IN OUT nocopy rowid_TBL,
                               O_tsf_xform_detail_size    IN OUT        BINARY_INTEGER,
                               O_tsf_packing_rowid        IN OUT nocopy rowid_TBL,
                               O_tsf_packing_size         IN OUT        BINARY_INTEGER,
                               O_delete_rowid_ind         IN OUT        VARCHAR2,
                               I_message_type             IN            woout_mfqueue.message_type%TYPE,
                               I_tsf_wo_id                IN            tsf_wo_head.tsf_wo_id%TYPE,
                               I_tsf_parent_no            IN            tsf_wo_head.tsf_no%TYPE,
                               I_to_loc                   IN            item_loc.loc%TYPE,
                               I_max_details              IN            rib_settings.max_details_to_publish%TYPE)

RETURN BOOLEAN IS

   L_records_found         BOOLEAN:=FALSE;

   L_details_processed     rib_settings.max_details_to_publish%TYPE := 0;

   L_rib_wooutdtl_rec      "RIB_WOOutDtl_REC" := NULL;
   L_rib_wooutdtl_tbl      "RIB_WOOutDtl_TBL" := NULL;
   L_rib_wooutxform_rec    "RIB_WOOutXForm_REC" := NULL;
   L_rib_wooutxform_tbl    "RIB_WOOutXForm_TBL" := NULL;
   L_rib_wooutpacking_rec  "RIB_WOOutPacking_REC" := NULL;
   L_rib_wooutpacking_tbl  "RIB_WOOutPacking_TBL" := NULL;

   L_table                 VARCHAR2(30)  := 'TSF_XFORM_DETAIL';
   L_key1                  VARCHAR2(100) := I_tsf_parent_no;
   L_key2                  VARCHAR2(100) := NULL;
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   cursor C_TSF_WO_DETAIL is
      select distinct item
        from tsf_wo_detail 
       where tsf_wo_id = I_tsf_wo_id
         and publish_ind = 'N';
   
   cursor C_TSF_XFORM_DETAIL is
      select xd.from_item,
             xd.to_item,
             xd.rowid xform_detail_rowid
        from tsf_xform xh,
             tsf_xform_detail xd
       where xh.tsf_no = I_tsf_parent_no
         and xh.tsf_xform_id = xd.tsf_xform_id
         and xd.publish_ind = 'N'
         for update of xd.publish_ind nowait;
         
   cursor C_TSF_PACKING is
      select p.tsf_packing_id,
             p.rowid packing_rowid
        from tsf_packing p
       where p.tsf_no = I_tsf_parent_no
         and p.publish_ind = 'N'
         for update of p.publish_ind nowait;
	
BEGIN
   
   --Initialize a new object once
   L_rib_wooutdtl_rec := "RIB_WOOutDtl_REC"(
           0, NULL, NULL, NULL, NULL,NULL,
           NULL, NULL, NULL, NULL, NULL);
   L_rib_wooutdtl_tbl := "RIB_WOOutDtl_TBL"();
   
   FOR rec in C_TSF_WO_DETAIL LOOP
      
      L_records_found := TRUE;         
      if L_details_processed >= I_max_details then
         O_delete_rowid_ind := 'N';
         EXIT;
      end if;  
        
                      
      --populate detail rec and put in dtl tbl
      if BUILD_WODTL_OBJECT ( O_error_msg,
                              L_rib_wooutdtl_tbl,
                              O_tsf_wo_detail_rowid,
                              O_tsf_wo_detail_size,
                              L_rib_wooutdtl_rec,
                              I_tsf_wo_id,
                              I_tsf_parent_no,
                              rec.item,
                              I_to_loc)= FALSE then
         return FALSE;
      end if;        
      L_details_processed := L_details_processed +1;  
           
   END LOOP;
   
   --WO Details are required, packing and xformation are not.
   if not L_records_found then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_TSF_WO_DETAIL_PUB',
                                        I_tsf_parent_no, NULL, NULL);
      return FALSE;
   end if;

   --Initialize a new object once
   L_rib_wooutxform_rec := "RIB_WOOutXForm_REC"(0, NULL, NULL);
   L_rib_wooutxform_tbl := "RIB_WOOutXForm_TBL"();
     
   L_details_processed := 0;
   
   FOR rec in C_TSF_XFORM_DETAIL LOOP
   
      if L_details_processed >= I_max_details then
         O_delete_rowid_ind := 'N';
         EXIT;
      end if;
      
      --populate xform rec
      L_rib_wooutxform_rec.from_item := rec.from_item;
      L_rib_wooutxform_rec.to_item   := rec.to_item;
      
      --put xform rec in table
      L_rib_wooutxform_tbl.EXTEND;
      L_rib_wooutxform_tbl(L_rib_wooutxform_tbl.COUNT) := L_rib_wooutxform_rec;
      
      --save rowid to be updated to published
      O_tsf_xform_detail_size := O_tsf_xform_detail_size +1;
      O_tsf_xform_detail_rowid(O_tsf_xform_detail_size) := rec.xform_detail_rowid;
      
      L_details_processed := L_details_processed +1;  
   
   END LOOP;
   
   L_table := 'TSF_PACKING';
   
   L_rib_wooutpacking_rec := "RIB_WOOutPacking_REC"(0, NULL, NULL);
   L_rib_wooutpacking_tbl := "RIB_WOOutPacking_TBL"();
   
   L_details_processed := 0;

   FOR rec in C_TSF_PACKING LOOP
      
      if L_details_processed >= I_max_details then
      	 O_delete_rowid_ind := 'N';
      	 EXIT;
      end if;
   
      if BUILD_PACKING_OBJECT( O_error_msg,
                               L_rib_wooutpacking_tbl,
                               L_rib_wooutpacking_rec,
                               rec.tsf_packing_id) = FALSE then
         return FALSE;
      end if;
   
      O_tsf_packing_size := O_tsf_packing_size +1;
      O_tsf_packing_rowid(O_tsf_packing_size) := rec.packing_rowid;
      
      L_details_processed := L_details_processed +1;
   
   end LOOP;
     
   --set all local tables to output variables if size > 0 
   --otherwise return null rather than an empty table;
   if L_rib_wooutdtl_tbl.COUNT > 0 then
      O_detail_message := L_rib_wooutdtl_tbl;
   else
      O_detail_message := NULL;
   end if;
   
   if L_rib_wooutxform_tbl.COUNT > 0 then
      O_xform_message := L_rib_wooutxform_tbl;
   else
      O_xform_message := NULL;
   end if;
   
   if L_rib_wooutpacking_tbl.COUNT > 0 then
      O_packing_message := L_rib_wooutpacking_tbl;
   else
      O_packing_message := NULL;
   end if;
   

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      return FALSE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_WOOUT.BUILD_DETAIL_OBJECTS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_OBJECTS;
--------------------------------------------------------------------------------
FUNCTION BUILD_WODTL_OBJECT( O_error_msg            IN OUT        VARCHAR2,
                             O_detail_message       IN OUT nocopy "RIB_WOOutDtl_TBL",
                             O_tsf_wo_detail_rowid  IN OUT nocopy rowid_TBL,
                             O_tsf_wo_detail_size   IN OUT        BINARY_INTEGER,
                             IO_rib_wooutdtl_rec    IN OUT nocopy "RIB_WOOutDtl_REC",
                             I_tsf_wo_id            IN            tsf_wo_head.tsf_wo_id%TYPE,
                             I_tsf_parent_no        IN            tsfhead.tsf_parent_no%TYPE,
                             I_item                 IN            tsf_wo_detail.item%TYPE,
                             I_to_loc               IN            item_loc.loc%TYPE)
RETURN BOOLEAN IS

   L_rib_wooutactivity_rec   "RIB_WOOutActivity_REC":= NULL;
   L_rib_wooutactivity_tbl   "RIB_WOOutActivity_TBL":= NULL;
   
   L_table                 VARCHAR2(30)  := 'TSF_WO_DETAIL';
   L_key1                  VARCHAR2(100) := I_tsf_wo_id;
   L_key2                  VARCHAR2(100) := I_item;
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);
   
   cursor C_INV_STATUS is 
      select inv_status
        from tsfdetail
       where tsf_no = I_tsf_parent_no
         and item = I_item;
   
   
   cursor C_DETAIL_ACTIVITIES is
      select activity_id,
             unit_cost,
             comments,
             rowid wodetail_rowid
        from tsf_wo_detail
       where tsf_wo_id = I_tsf_wo_id
         and item = I_item
         and publish_ind = 'N'
         for update of publish_ind nowait;
BEGIN

   L_rib_wooutactivity_rec := "RIB_WOOutActivity_REC"( 0, NULL, NULL, NULL);
   L_rib_wooutactivity_tbl := "RIB_WOOutActivity_TBL"();

   IO_rib_wooutdtl_rec.item_id := I_item;
   IO_rib_wooutdtl_rec.dest_id := I_to_loc;
   IO_rib_wooutdtl_rec.auto_complete := 'N';
   open C_INV_STATUS;
   fetch C_INV_STATUS into IO_rib_wooutdtl_rec.inv_status;
   close C_INV_STATUS;
   
   --Populate activity table
   FOR rec in C_DETAIL_ACTIVITIES LOOP
   
      L_rib_wooutactivity_rec.activity_id := rec.activity_id;
      L_rib_wooutactivity_rec.activity_cost := rec.unit_cost;
      L_rib_wooutactivity_rec.comments := rec.comments;
   
      --put activity rec into activity table
      L_rib_wooutactivity_tbl.EXTEND;
      L_rib_wooutactivity_tbl(L_rib_wooutactivity_tbl.COUNT) := L_rib_wooutactivity_rec;
      
      --save detail rowids to update published ind
      O_tsf_wo_detail_size := O_tsf_wo_detail_size +1;
      O_tsf_wo_detail_rowid(O_tsf_wo_detail_size) :=  rec.wodetail_rowid;

   end LOOP;
   
   --Put activity table into detail rec
   
   --We know that there is at least one unpublished activity rec for this item
   --so there is no need to check for table size 0.
   IO_rib_wooutdtl_rec.wooutactivity_tbl := L_rib_wooutactivity_tbl;
   

   --Put detail rec into detail table.
   O_detail_message.EXTEND;
   O_detail_message(O_detail_message.COUNT) := IO_rib_wooutdtl_rec;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      return FALSE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         'RMSMFM_WOOUT.BUILD_WODTL_OBJECT',
                                         TO_CHAR(SQLCODE));
      return FALSE;


END BUILD_WODTL_OBJECT;
--------------------------------------------------------------------------------
FUNCTION BUILD_PACKING_OBJECT ( O_error_msg             IN OUT        VARCHAR2,
                                O_packing_message       IN OUT nocopy "RIB_WOOutPacking_TBL",
                                IO_rib_wooutpacking_rec IN OUT nocopy "RIB_WOOutPacking_REC",
                                I_tsf_packing_id        IN            tsf_packing.tsf_packing_id%TYPE)
RETURN BOOLEAN IS

   L_rib_wooutpackfrom_rec   "RIB_WOOutPackFrom_REC":= NULL;
   L_rib_wooutpackfrom_tbl   "RIB_WOOutPackFrom_TBL":= NULL;
   L_rib_wooutpackto_rec     "RIB_WOOutPackTo_REC":= NULL;
   L_rib_wooutpackto_tbl     "RIB_WOOutPackTo_TBL":= NULL;
   
   cursor C_FROM_ITEM is 
      select item
        from tsf_packing_detail
       where tsf_packing_id = I_tsf_packing_id
         and record_type = 'F';
   
   cursor C_TO_ITEM is
      select item
        from tsf_packing_detail
       where tsf_packing_id = I_tsf_packing_id
         and record_type = 'R';
      

BEGIN
   
   --Initialize object once
   L_rib_wooutpackfrom_rec := "RIB_WOOutPackFrom_REC"(0, null);
   L_rib_wooutpackfrom_tbl := "RIB_WOOutPackFrom_TBL"();
   
   --populate the 'from' item table
   FOR rec in C_FROM_ITEM LOOP
      
      L_rib_wooutpackfrom_rec.from_item := rec.item;
      L_rib_wooutpackfrom_tbl.EXTEND;
      L_rib_wooutpackfrom_tbl(L_rib_wooutpackfrom_tbl.COUNT) := L_rib_wooutpackfrom_rec;
      
   end LOOP;
   
   --put the 'from' item table in packing rec
   IO_rib_wooutpacking_rec.wooutpackfrom_tbl := L_rib_wooutpackfrom_tbl;
   
   
   L_rib_wooutpackto_rec := "RIB_WOOutPackTo_REC"(0, null);
   L_rib_wooutpackto_tbl := "RIB_WOOutPackTo_TBL"();
   
   --populate the 'to'item table
   FOR rec in C_TO_ITEM LOOP
      
      L_rib_wooutpackto_rec.to_item := rec.item;
      L_rib_wooutpackto_tbl.EXTEND;
      L_rib_wooutpackto_tbl(L_rib_wooutpackto_tbl.COUNT) := L_rib_wooutpackto_rec;
      
   end LOOP;
   
   --put the 'to' item table in packing rec
   IO_rib_wooutpacking_rec.wooutpackto_tbl := L_rib_wooutpackto_tbl;

   --put the packing rec in the packing table
   O_packing_message.EXTEND;
   O_packing_message(O_packing_message.COUNT) := IO_rib_wooutpacking_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         'RMSMFM_WOOUT.BUILD_PACKING_OBJECT',
                                         TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_PACKING_OBJECT;
--------------------------------------------------------------------------------

FUNCTION DELETE_QUEUE_REC( O_error_msg   OUT VARCHAR2,
                           I_seq_no   IN     woout_mfqueue.seq_no%TYPE)
RETURN BOOLEAN IS

BEGIN

   delete from woout_mfqueue
    where seq_no = I_seq_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         'RMSMFM_WOOUT.DELETE_QUEUE_REC',
                                         TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_QUEUE_REC;
--------------------------------------------------------------------------------
FUNCTION LOCK_THE_BLOCK( O_error_msg        OUT VARCHAR2,
                         O_queue_locked     OUT BOOLEAN,
                         I_tsf_wo_id     IN     woout_mfqueue.tsf_wo_id%TYPE)
RETURN BOOLEAN IS

   L_table             VARCHAR2(30)  := 'WOOUT_MFQUEUE';
   L_key1              VARCHAR2(100) := I_tsf_wo_id;
   L_key2              VARCHAR2(100) := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from woout_mfqueue wq
       where wq.tsf_wo_id = I_tsf_wo_id
        for update nowait;

BEGIN

   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      O_queue_locked := TRUE;
      return TRUE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WOOUT.LOCK_THE_BLOCK',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_THE_BLOCK; 

--------------------------------------------------------------------------------
FUNCTION UPDATE_DETAILS(O_error_msg        OUT VARCHAR2,
                        I_tsf_wo_id     IN      woout_mfqueue.tsf_wo_id%TYPE,
                        I_tsf_parent_no IN      tsfhead.tsf_no%TYPE)
RETURN BOOLEAN IS

   L_table             VARCHAR2(30)  := 'TSF_WO_DETAIL';
   L_key1              VARCHAR2(100) := I_tsf_parent_no;
   L_key2              VARCHAR2(100) := I_tsf_wo_id;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   
   cursor C_LOCK_TSF_WO_DETAIL is
      select rowid tsf_wo_detail_rowid
        from tsf_wo_detail
       where tsf_wo_id = I_tsf_wo_id
         for update of publish_ind nowait;
   
   cursor C_LOCK_XFORM_DETAIL is
      select xd.rowid xform_detail_rowid
        from tsf_xform_detail xd,
             tsf_xform xh
       where xh.tsf_no = I_tsf_parent_no
         and xh.tsf_xform_id = xd.tsf_xform_id
         for update of publish_ind nowait;
         
   cursor C_LOCK_PACKING is
      select tp.rowid packing_rowid
        from tsf_packing tp
       where tp.tsf_no = I_tsf_parent_no
         for update of publish_ind nowait; 

BEGIN	


   FOR rec IN C_LOCK_TSF_WO_DETAIL LOOP      
      update tsf_wo_detail
         set publish_ind = 'N'
       where rowid = rec.tsf_wo_detail_rowid;    

   end LOOP;
   
   L_table := 'TSF_XFORM_DETAIL';
   L_key2 := NULL;
   
   FOR rec IN C_LOCK_XFORM_DETAIL LOOP 
      update tsf_xform_detail
         set publish_ind = 'N'
       where rowid = rec.xform_detail_rowid;  
   end LOOP;
   
   
   L_table := 'TSF_PACKING';
   
   FOR rec IN C_LOCK_PACKING LOOP
      update tsf_packing
         set publish_ind = 'N'
       where rowid = rec.packing_rowid;
   end LOOP;
  
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      return FALSE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WOOUT.UPDATE_DETAILS',
                                        TO_CHAR(SQLCODE));
      return FALSE;	
END UPDATE_DETAILS;
--------------------------------------------------------------------------------
END RMSMFM_WOOUT;
/
