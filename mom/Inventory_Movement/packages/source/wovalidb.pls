
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY WO_VALIDATE_SQL AS
------------------------------------------------------------------------
FUNCTION EXIST(O_error_message  IN OUT  VARCHAR2,
               O_exist	        IN OUT  VARCHAR2,
               I_wo_id	        IN	    WO_HEAD.WO_ID%TYPE,
               I_order_no       IN      WO_HEAD.ORDER_NO%TYPE,
               I_tsfalloc_no    IN      WO_HEAD.TSFALLOC_NO%TYPE)
RETURN BOOLEAN is
---
L_program  VARCHAR2(64)  := 'WO_VALIDATE_SQL.EXIST';
L_exists   VARCHAR2(1)   := NULL;
---
cursor C_WO_HEAD is
   select 'x'
     from wo_head
    where wo_id = NVL(I_wo_id, wo_id)
      and ((order_no = I_order_no and order_no is not NULL)
       or (order_no is NULL and I_order_no is NULL))
      and ((tsfalloc_no = I_tsfalloc_no and tsfalloc_no is not NULL)
       or (tsfalloc_no is NULL and I_tsfalloc_no is NULL));
   ---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_WO_HEAD', 'WO_HEAD', NULL);
   open C_WO_HEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_WO_HEAD', 'WO_HEAD', NULL);
   fetch C_WO_HEAD into L_exists;
   SQL_LIB.SET_MARK('CLOSE', 'C_WO_HEAD', 'WO_HEAD', NULL);
   close C_WO_HEAD;
   ---
   if L_exists is NULL then
      O_exist := 'FALSE';
   else
      O_exist := 'TRUE';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;

END EXIST;
------------------------------------------------------------------------
END WO_VALIDATE_SQL;
/
