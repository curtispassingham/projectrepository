CREATE OR REPLACE PACKAGE ASN_SQL AUTHID CURRENT_USER AS

--used by the ASN subscribe packages and their dependent packages
TYPE shipment_shipsku_TBL is table of shipsku.shipment%TYPE INDEX BY BINARY_INTEGER;
TYPE shipment_seq_no_TBL is table of shipsku.seq_no%TYPE INDEX BY BINARY_INTEGER;
TYPE item_shipsku_TBL is table of shipsku.item%TYPE INDEX BY BINARY_INTEGER;
TYPE distro_no_shipsku_TBL is table of shipsku.distro_no%TYPE INDEX BY BINARY_INTEGER;
TYPE ref_item_shipsku_TBL is table of shipsku.ref_item%TYPE INDEX BY BINARY_INTEGER;
TYPE carton_shipsku_TBL is table of shipsku.carton%TYPE INDEX BY BINARY_INTEGER;
TYPE inv_status_shipsku_TBL is table of shipsku.inv_status%TYPE INDEX BY BINARY_INTEGER;
TYPE status_code_shipsku_TBL is table of shipsku.status_code%TYPE INDEX BY BINARY_INTEGER;
TYPE qty_received_shipsku_TBL is table of shipsku.qty_received%TYPE INDEX BY BINARY_INTEGER;
TYPE unit_cost_shipsku_TBL is table of shipsku.unit_cost%TYPE INDEX BY BINARY_INTEGER;
TYPE unit_retail_shipsku_TBL is table of shipsku.unit_retail%TYPE INDEX BY BINARY_INTEGER;
TYPE qty_expected_shipsku_TBL is table of shipsku.qty_expected%TYPE INDEX BY BINARY_INTEGER;
TYPE ind_TBL is table of VARCHAR2(1) INDEX BY BINARY_INTEGER;

TYPE shipsku_loc_shipment_TBL is table of shipsku_loc.shipment%TYPE INDEX BY BINARY_INTEGER;
TYPE shipsku_loc_seq_no_TBL is table of shipsku_loc.seq_no%TYPE INDEX BY BINARY_INTEGER;
TYPE shipsku_loc_item_TBL is table of shipsku_loc.item%TYPE INDEX BY BINARY_INTEGER;
TYPE shipsku_loc_to_loc_TBL is table of shipsku_loc.to_loc%TYPE INDEX BY BINARY_INTEGER;
TYPE shipsku_loc_qty_expected_TBL is table of shipsku_loc.qty_expected%TYPE INDEX BY BINARY_INTEGER;
TYPE shipsku_loc_qty_received_TBL is table of shipsku_loc.qty_received%TYPE INDEX BY BINARY_INTEGER;

P_shipments shipment_shipsku_TBL;
P_seq_nos shipment_seq_no_TBL;
P_items item_shipsku_TBL;
P_distro_nos distro_no_shipsku_TBL;
P_ref_items ref_item_shipsku_TBL;
P_cartons carton_shipsku_TBL;
P_inv_statuses inv_status_shipsku_TBL;
P_status_codes status_code_shipsku_TBL;
P_qty_receiveds qty_received_shipsku_TBL;
P_unit_costs unit_cost_shipsku_TBL;
P_unit_retails unit_retail_shipsku_TBL;
P_qty_expecteds qty_expected_shipsku_TBL;

P_shipskus_size NUMBER := 0;

P_shipsku_loc_size         NUMBER := 0;
P_shipsku_loc_shipment     shipsku_loc_shipment_TBL;
P_shipsku_loc_seq_no       shipsku_loc_seq_no_TBL;
P_shipsku_loc_item         shipsku_loc_item_TBL;
P_shipsku_loc_to_loc       shipsku_loc_to_loc_TBL;
P_shipsku_loc_qty_expected shipsku_loc_qty_expected_TBL;
P_shipsku_loc_qty_received shipsku_loc_qty_received_TBL;

TYPE item_asn_record IS RECORD 
 ( 
    input_item           item_master.item%TYPE,                 --parameter: may be ref or tran level 
    carton               shipsku.carton%TYPE,                   --parameter 
    item                 item_master.item%TYPE,                 --lookup: item_master -- tran_level item 
    item_parent          item_master.item_parent%TYPE,          --lookup: item_master 
    item_grandparent     item_master.item_grandparent%TYPE,     --lookup: item_master 
    item_level           item_master.item_level%TYPE,           --lookup: item_master 
    tran_level           item_master.tran_level%TYPE,           --lookup: item_master 
    diff_1               item_master.diff_1%TYPE,               --lookup: item_master 
    diff_2               item_master.diff_2%TYPE,               --lookup: item_master 
    diff_3               item_master.diff_3%TYPE,               --lookup: item_master 
    diff_4               item_master.diff_4%TYPE,               --lookup: item_master 
    dept                 item_master.dept%TYPE,                 --lookup: item_master 
    class                item_master.class%TYPE,                --lookup: item_master 
    subclass             item_master.subclass%TYPE,             --lookup: item_master 
    pack_ind             item_master.pack_ind%TYPE,             --lookup: item_master 
    pack_type            item_master.pack_type%TYPE,            --lookup: item_master 
    sellable_ind         item_master.sellable_ind%TYPE,         --lookup: item_master 
    orderable_ind        item_master.orderable_ind%TYPE,        --lookup: item_master 
    inventory_ind        item_master.inventory_ind%TYPE,        --lookup: item_master 
    suom                 item_master.standard_uom%TYPE,         --lookup: item_master 
    simple_pack_ind      item_master.simple_pack_ind%TYPE,      --lookup: item_master 
    catch_weight_ind     item_master.catch_weight_ind%TYPE,     --lookup: item_master 
    deposit_item_type    item_master.deposit_item_type%TYPE,    --lookup: item_master 
    container_item       item_master.container_item%TYPE,       --lookup: item_master 
    tran_date            DATE,                                  --parameter 
    loc                  item_loc.loc%TYPE,                     --lookup/parameter 
    loc_type             item_loc.loc_type%TYPE,                --lookup: LOCATION_ATTRIB_SQL.GET_TYPE 
    phy_loc              item_loc.loc%TYPE,                     --parameter 
    distro_type          VARCHAR2(1),                           --parameter 
    distro_number        alloc_header.alloc_no%TYPE,            --parameter 
    destination          alloc_detail.to_loc%TYPE,              --parameter: location allocated to 
    disp                 inv_status_codes.inv_status_code%TYPE, --parameter: inventory bucket received into 
    inv_status           shipsku.inv_status%TYPE,               --lookup: INVADJ_SQL.GET_INV_STATUS 
    order_no             ordhead.order_no%TYPE,                 --parameter 
    order_status         ordhead.status%TYPE,                   --lookup: ordhead 
    order_orig_ind       ordhead.orig_ind%TYPE,                 --lookup: ordhead 
    earliest_ship_date   DATE,                                  --lookup: ordhead 
    latest_ship_date     DATE,                                  --lookup: ordhead 
    import_country_id    ordhead.import_country_id%TYPE,        --lookup: ordhead 
    origin_country_id    ordsku.origin_country_id%TYPE,         --lookup: ordhead , primary if new ordsku 
    asn                  shipment.asn%TYPE,                     --parameter 
    appt                 appt_head.appt%TYPE,                   --parameter 
    receipt_no           SHIPMENT.EXT_REF_NO_IN%TYPE,           --parameter 
    ship_no              shipment.shipment%TYPE,                --lookup: shipment based on ord/asn/item or generated 
    ship_origin          shipment.ship_origin%TYPE,             --lookup: shipment based on ord/asn/item or generated 
    new_shipsku_ind      VARCHAR2(1),                           --calculated: 'Y',if shipsku record created 
                                                                --            'N',if shipsku record existed 
    new_ordloc_ind       VARCHAR2(1),                           --calculated: 'Y',if ordloc record created 
                                                                --            'N',if ordloc record existed 
    supplier             sups.supplier%TYPE,                    --lookup: ordhead 
    edi_asn              sups.edi_asn%TYPE,                     --lookup: sups 
    settlement_code      sups.settlement_code%TYPE,             --lookup: sups 
    supp_pack_size       ordsku.supp_pack_size%TYPE,            --lookup: ordloc or iscl 
    tran_type            VARCHAR2(1),                           --parameter: R = Receipt 
                                                                --           A = Adjustment 
    online_ind           VARCHAR2(1),                            --parameter: Y = Yes, online 
                                                                --           N = No, not online 
                                                                --           A = Auto PO 
    alc_finalize_ind     VARCHAR2(1),                           --ALC Finalize indicator 
    ship_seq_no          shipsku.seq_no%TYPE                    --Shipment seq no 
 ); -- end item_asn_record 
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_loc_type         IN OUT SHIPMENT.TO_LOC_TYPE%TYPE,
                           I_location         IN     SHIPMENT.TO_LOC%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then
FUNCTION VALIDATE_ORDER(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_order_no         IN OUT ORDHEAD.ORDER_NO%TYPE,
                        O_pre_mark_ind     IN OUT ORDHEAD.PRE_MARK_IND%TYPE,
                        O_import_id        IN OUT ORDHEAD.IMPORT_ID%TYPE,
                        O_import_type      IN OUT ORDHEAD.IMPORT_TYPE%TYPE,
                        I_order_no         IN     ORDHEAD.VENDOR_ORDER_NO%TYPE,
                        I_payment_method   IN     ORDHEAD.SHIP_PAY_METHOD%TYPE,
                        I_not_after_date   IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                        I_supplier         IN     ORDHEAD.SUPPLIER%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------
FUNCTION MATCH_SHIPMENT(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_shipment         IN OUT SHIPMENT.SHIPMENT%TYPE,
                        O_ship_match       IN OUT BOOLEAN,
                        I_asn              IN     SHIPMENT.ASN%TYPE,
                        I_order_no         IN     SHIPMENT.ORDER_NO%TYPE,
                        I_to_loc           IN     SHIPMENT.TO_LOC%TYPE,
                        I_ship_date        IN     SHIPMENT.SHIP_DATE%TYPE,
                        I_est_arr_date     IN     SHIPMENT.EST_ARR_DATE%TYPE,
                        I_message_type     IN     VARCHAR2)
return BOOLEAN;
-----------------------------------------------------------------------
FUNCTION NEW_SHIPMENT(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_shipment           IN OUT SHIPMENT.SHIPMENT%TYPE,
                      I_asn                IN     SHIPMENT.ASN%TYPE,
                      I_order_no           IN     SHIPMENT.ORDER_NO%TYPE,
                      I_location           IN     SHIPMENT.TO_LOC%TYPE,
                      I_loc_type           IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                      I_shipdate           IN     SHIPMENT.SHIP_DATE%TYPE,
                      I_est_arr_date       IN     SHIPMENT.EST_ARR_DATE%TYPE,
                      I_carton_ind         IN     VARCHAR2,
                      I_inbound_bol        IN     SHIPMENT.EXT_REF_NO_IN%TYPE,
                      I_courier            IN     SHIPMENT.COURIER%TYPE,
                      I_bill_to_loc        IN     SHIPMENT.BILL_TO_LOC%TYPE,
                      I_bill_to_loc_type   IN     SHIPMENT.BILL_TO_LOC_TYPE%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------
FUNCTION MATCH_ITEM(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                    I_asn              IN     SHIPMENT.ASN%TYPE,
                    I_order_no         IN     SHIPMENT.ORDER_NO%TYPE,
                    I_location         IN     SHIPMENT.TO_LOC%TYPE,
                    I_item             IN     SHIPSKU.ITEM%TYPE,
                    I_ref_item         IN     SHIPSKU.REF_ITEM%TYPE,
                    I_carton           IN     SHIPSKU.CARTON%TYPE,
                    I_qty              IN     SHIPSKU.QTY_EXPECTED%TYPE,
                    I_status_code      IN     SHIPSKU.STATUS_CODE%TYPE,
                    I_unit_cost        IN     ORDLOC.UNIT_COST%TYPE,
                    I_unit_retail      IN     ORDLOC.UNIT_RETAIL%TYPE,
                    I_loc_type         IN     SHIPMENT.TO_LOC_TYPE%TYPE)
return BOOLEAN;
$end
-----------------------------------------------------------------------------------
FUNCTION PROCESS_ORDER(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_order_no         IN OUT ORDHEAD.ORDER_NO%TYPE,
                       O_pre_mark_ind     IN OUT ORDHEAD.PRE_MARK_IND%TYPE,
                       O_shipment         IN OUT SHIPMENT.SHIPMENT%TYPE,
                       O_ship_match       IN OUT BOOLEAN,
                       I_asn              IN     SHIPMENT.ASN%TYPE,
                       I_order_no         IN     SHIPMENT.ORDER_NO%TYPE,
                       I_to_loc           IN     SHIPMENT.TO_LOC%TYPE,
                       I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                       I_ship_pay_method  IN     ORDHEAD.SHIP_PAY_METHOD%TYPE,
                       I_not_after_date   IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                       I_ship_date        IN     SHIPMENT.SHIP_DATE%TYPE,
                       I_est_arr_date     IN     SHIPMENT.EST_ARR_DATE%TYPE,
                       I_courier          IN     SHIPMENT.COURIER%TYPE,
                       I_inbound_bol      IN     SHIPMENT.EXT_REF_NO_IN%TYPE,
                       I_supplier         IN     ORDHEAD.SUPPLIER%TYPE,
                       I_carton_ind       IN     VARCHAR2,
                       I_message_type     IN     VARCHAR2)
return BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_CARTON(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_carton           IN     CARTON.CARTON%TYPE,
                         I_alloc_loc        IN     CARTON.LOCATION%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION CHECK_ITEM(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                    I_supplier         IN     ORDHEAD.SUPPLIER%TYPE,
                    I_asn              IN     SHIPMENT.ASN%TYPE,
                    I_order_no         IN     SHIPMENT.ORDER_NO%TYPE,
                    I_location         IN     SHIPMENT.TO_LOC%TYPE,
                    I_alloc_loc        IN     CARTON.LOCATION%TYPE,
                    I_item             IN     SHIPSKU.ITEM%TYPE,
                    I_ref_item         IN     SHIPSKU.REF_ITEM%TYPE,
                    I_vpn              IN     ITEM_SUPPLIER.VPN%TYPE,
                    I_carton           IN     SHIPSKU.CARTON%TYPE,
                    I_premark_ind      IN     ORDHEAD.PRE_MARK_IND%TYPE,
                    I_qty              IN     SHIPSKU.QTY_EXPECTED%TYPE,
                    I_ship_match       IN     BOOLEAN,
                    I_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION CREATE_INVOICE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                        I_supplier         IN     ORDHEAD.SUPPLIER%TYPE,
                        I_ship_match       IN     BOOLEAN)
return BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION DELETE_ASN(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_asn              IN     SHIPMENT.ASN%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION DO_SHIPSKU_INSERTS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION RESET_GLOBALS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION PROCESS_ORDER_ONLINE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_order_no         IN OUT ORDHEAD.ORDER_NO%TYPE,
                              O_pre_mark_ind     IN OUT ORDHEAD.PRE_MARK_IND%TYPE,
                              O_shipment         IN OUT SHIPMENT.SHIPMENT%TYPE,
                              I_asn              IN     SHIPMENT.ASN%TYPE,
                              I_order_no         IN     SHIPMENT.ORDER_NO%TYPE,
                              I_to_loc           IN     SHIPMENT.TO_LOC%TYPE,
                              I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                              I_ship_pay_method  IN     ORDHEAD.SHIP_PAY_METHOD%TYPE,
                              I_not_after_date   IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                              I_ship_date        IN     SHIPMENT.SHIP_DATE%TYPE,
                              I_est_arr_date     IN     SHIPMENT.EST_ARR_DATE%TYPE,
                              I_courier          IN     SHIPMENT.COURIER%TYPE,
                              I_no_boxes         IN     SHIPMENT.NO_BOXES%TYPE,
                              I_comments         IN     SHIPMENT.COMMENTS%TYPE,
                              I_inbound_bol      IN     SHIPMENT.EXT_REF_NO_IN%TYPE,
                              I_supplier         IN     ORDHEAD.SUPPLIER%TYPE,
                              I_carton_ind       IN     VARCHAR2)
return BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION NEW_SHIPMENT_ONLINE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_shipment           IN OUT SHIPMENT.SHIPMENT%TYPE,
                             I_asn                IN     SHIPMENT.ASN%TYPE,
                             I_order_no           IN     SHIPMENT.ORDER_NO%TYPE,
                             I_location           IN     SHIPMENT.TO_LOC%TYPE,
                             I_loc_type           IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                             I_shipdate           IN     SHIPMENT.SHIP_DATE%TYPE,
                             I_est_arr_date       IN     SHIPMENT.EST_ARR_DATE%TYPE,
                             I_carton_ind         IN     VARCHAR2,
                             I_inbound_bol        IN     SHIPMENT.EXT_REF_NO_IN%TYPE,
                             I_courier            IN     SHIPMENT.COURIER%TYPE,
                             I_no_boxes           IN     SHIPMENT.NO_BOXES%TYPE,
                             I_comments           IN     SHIPMENT.COMMENTS%TYPE,
                             I_bill_to_loc        IN     SHIPMENT.BILL_TO_LOC%TYPE,
                             I_bill_to_loc_type   IN     SHIPMENT.BILL_TO_LOC_TYPE%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION MATCH_MANUAL_SHIPMENT(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_asn                     IN     SHIPMENT.ASN%TYPE,
                               I_order_no                IN     SHIPMENT.ORDER_NO%TYPE,
                               I_to_loc                  IN     SHIPMENT.TO_LOC%TYPE,
                               O_asn_dup_indicator       IN OUT    BOOLEAN)
return BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION SHIPSKU_LOC_INSERT(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_shipment         IN     SHIPSKU_LOC.SHIPMENT%TYPE,
                            I_order_no         IN     SHIPMENT.ORDER_NO%TYPE,
                            I_seq_no           IN     SHIPSKU.SEQ_NO%TYPE,
                            I_item             IN     SHIPSKU_LOC.ITEM%TYPE,
                            I_loc              IN     SHIPMENT.TO_LOC%TYPE,
                            I_loc_type         IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                            I_qty              IN     SHIPSKU_LOC.QTY_EXPECTED%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------------------
END ASN_SQL;
/
