
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_WOOUT AUTHID CURRENT_USER AS
/*--- message type parameters ---*/
--HEADER_CREATE_TYPE     VARCHAR2(64) := 'outbdwocre';
--HEADER_CREATE_TYPE     VARCHAR2(64) := 'outbdwomod';
--HEADER_DELETE_TYPE     VARCHAR2(64) := 'outbdwodel';
--DETAIL_CREATE_TYPE     VARCHAR2(64) := 'outbdwodtlcre';


FAMILY       VARCHAR2(64) := 'woout';

HDR_ADD      VARCHAR2(64) := 'outbdwocre';
HDR_UNAPRV   VARCHAR2(64) := 'outbdwounaprv';
HDR_DEL      VARCHAR2(64) := 'outbdwodel';
DTL_ADD      VARCHAR2(64) := 'outbdwodtlcre';

---------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_msg            OUT VARCHAR2,
                I_message_type      IN     VARCHAR2,
                I_tsf_wo_id         IN     tsf_wo_head.tsf_wo_id%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1);
---------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT);
---------------------------------------------------------------------------
END RMSMFM_WOOUT;
/
