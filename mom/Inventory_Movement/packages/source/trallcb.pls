CREATE OR REPLACE PACKAGE BODY TRAN_ALLOC_SQL AS
   LP_vdate   DATE   := GET_VDATE;
------------------------------------------------------------------------------------
FUNCTION INSERT_ALC_COMP_LOCS(O_error_message     IN OUT VARCHAR2,
                              I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                              I_item              IN     ITEM_MASTER.ITEM%TYPE,
                              I_pack_item         IN     ITEM_MASTER.ITEM%TYPE,
                              I_obligation_key    IN     OBLIGATION.OBLIGATION_KEY%TYPE,
                              I_vessel_id         IN     TRANSPORTATION.VESSEL_ID%TYPE,
                              I_voyage_flt_id     IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                              I_etd               IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                              I_comp_id           IN     ELC_COMP.COMP_ID%TYPE,
                              I_location          IN     ORDLOC.LOCATION%TYPE,
                              I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                              I_act_value         IN     ALC_COMP_LOC.ACT_VALUE%TYPE,
                              I_qty               IN     ALC_COMP_LOC.QTY%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'TRAN_ALLOC_SQL.INSERT_ALC_COMP_LOCS';

BEGIN
   -- Expense comps will be reallocated at the end of the process
   if ALC_ALLOC_SQL.ADD_PO_TO_QUEUE(O_error_message,
                                    I_order_no) = FALSE then
      return FALSE;
   end if;
   insert into alc_comp_loc (order_no,
                             seq_no,
                             comp_id,
                             location,
                             loc_type,
                             act_value,
                             qty,
                             last_calc_date)
                      select I_order_no,
                             seq_no,
                             I_comp_id,
                             I_location,
                             I_loc_type,
                             I_act_value,
                             I_qty,
                             LP_vdate
                        from alc_head
                       where order_no           = I_order_no
                         and item               = I_item
                         and ((pack_item       is NULL
                               and I_pack_item is NULL)
                           or (pack_item        = I_pack_item
                               and pack_item   is not NULL
                               and I_pack_item is not NULL))
                         and obligation_key        = I_obligation_key
                         and vessel_id             = I_vessel_id
                         and voyage_flt_id         = I_voyage_flt_id
                         and estimated_depart_date = I_etd;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ALC_COMP_LOCS;
------------------------------------------------------------------------------------
--- Removed function INSERT_ELC_EXPS.  The function of inserting ALC records
--- for each expense component is now done by alc_alloc_sql.insert_expense_comps
-------------------------------------------------------------------------------
FUNCTION INSERT_ALC_HEAD(O_error_message     IN OUT VARCHAR2,
                         I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item         IN     ITEM_MASTER.ITEM%TYPE,
                         I_obligation_key    IN     OBLIGATION.OBLIGATION_KEY%TYPE,
                         I_ce_id             IN     CE_HEAD.CE_ID%TYPE,
                         I_vessel_id         IN     TRANSPORTATION.VESSEL_ID%TYPE,
                         I_voyage_flt_id     IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                         I_etd               IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                         I_item_qty          IN     ORDLOC.QTY_RECEIVED%TYPE,
                         I_error_ind         IN     ALC_HEAD.ERROR_IND%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64)             := 'TRAN_ALLOC_SQL.INSERT_ALC_HEAD';
   L_seq_no          ALC_HEAD.SEQ_NO%TYPE     := 0;
   L_shipment        ALC_HEAD.SHIPMENT%TYPE;
   L_alc_qty         ALC_HEAD.ALC_QTY%TYPE;
   L_ce_exists       VARCHAR2(1)              := 'N';
   L_cleared_qty     CE_ORD_ITEM.CLEARED_QTY%TYPE;
   L_status          ALC_HEAD.STATUS%TYPE;
   L_trnsprt_method  RTM_UNIT_OPTIONS.RTM_TRNSPRT_OBL_ALLOC_METHOD%TYPE;

   cursor C_GET_MAX_SEQ is
      select NVL(MAX(seq_no), 0) + 1
        from alc_head
       where order_no = I_order_no;

   cursor C_GET_SHIPMENT is
      select tsh.shipment,
             NVL(decode(sh.status_code, 'I', sk.qty_expected, sk.qty_received),0) qty
        from transportation_shipment tsh,
             shipsku sk,
             shipment sh
       where tsh.shipment              = sk.shipment
         and sh.shipment               = sk.shipment
         and sk.item                   = NVL(I_pack_item,I_item)
         and tsh.order_no              = I_order_no
         and tsh.vessel_id             = I_vessel_id
         and tsh.voyage_flt_id         = I_voyage_flt_id
         and tsh.estimated_depart_date = I_etd;

   cursor C_TRNSPRT_METHOD is
      select rtm_trnsprt_obl_alloc_method
        from rtm_unit_options;
  
   cursor C_CE_EXISTS is
    select 'Y'
      from ce_ord_item coi,
           ce_shipment cs
     where coi.vessel_id = I_vessel_id
       and coi.voyage_flt_id = I_voyage_flt_id
       and coi.estimated_depart_date = I_etd
       and coi.order_no = I_order_no
       and coi.item = NVL(I_pack_item,I_item)
       and coi.ce_id = cs.ce_id
       and coi.vessel_id = cs.vessel_id
       and coi.voyage_flt_id = cs.voyage_flt_id
       and coi.estimated_depart_date = cs.estimated_depart_date;

BEGIN

   ---
   open C_GET_SHIPMENT;
   fetch C_GET_SHIPMENT into L_shipment,L_alc_qty;
   close C_GET_SHIPMENT;
   ---

   ---
   open  C_TRNSPRT_METHOD;
   fetch C_TRNSPRT_METHOD into L_trnsprt_method;
   close C_TRNSPRT_METHOD;
   ---

   -- Get the maximum sequence number plus one.
   SQL_LIB.SET_MARK('OPEN','C_GET_MAX_SEQ','ALC_HEAD',NULL);
   open  C_GET_MAX_SEQ;
   SQL_LIB.SET_MARK('FETCH','C_GET_MAX_SEQ','ALC_HEAD',NULL);
   fetch C_GET_MAX_SEQ into L_seq_no;
   SQL_LIB.SET_MARK('CLOSE','C_GET_MAX_SEQ','ALC_HEAD',NULL);
   close C_GET_MAX_SEQ;

   if I_obligation_key is null and I_ce_id is NULL then
      L_status := 'E';  -- estimated expenses
   else
      L_status := 'P';  -- pending obligation
   end if;

   if L_trnsprt_method = 'ASN' then
      if L_shipment is NULL then
         -- Shipment record(s) must exist before charges can be allocated
         O_error_message := SQL_LIB.CREATE_MSG('ALC_ALLOC_NO_SHIPMENT', NULL, NULL, NULL);
         return FALSE;
      end if;
      
      -- If the Transportation Shipment record is associated with a Customs Entry record, then
      -- the cleared quantity should be inserted to ALC_HEAD instead of the shipment quantity.
      SQL_LIB.SET_MARK('OPEN','C_CE_EXISTS','CE_ORD_ITEM,CE_SHIPMENT',NULL);
      open  C_CE_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_CE_EXISTS','CE_ORD_ITEM,CE_SHIPMENT',NULL);
      fetch C_CE_EXISTS into L_ce_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CE_EXISTS','CE_ORD_ITEM,CE_SHIPMENT',NULL);
      close C_CE_EXISTS;
      
      -- I_item_qty contains this calculated cleared quantity if a CE record exists.
      if L_ce_exists = 'Y' and I_item_qty is NOT NULL then
         L_cleared_qty := I_item_qty;
      end if;
      
      FOR ship_rec in C_GET_SHIPMENT LOOP
         -- insert into ALC_HEAD
         insert into alc_head (order_no,
                               item,
                               pack_item,
                               seq_no,
                               obligation_key,
                               ce_id,
                               vessel_id,
                               voyage_flt_id,
                               estimated_depart_date,
                               alc_qty,
                               status,
                               error_ind,
                               shipment)
                       values (I_order_no,
                               I_item,
                               I_pack_item,
                               L_seq_no,
                               I_obligation_key,
                               I_ce_id,
                               I_vessel_id,
                               I_voyage_flt_id,
                               I_etd,
                               NVL(L_cleared_qty, ship_rec.qty),
                               L_status,       -- status = 'Pending'
                               I_error_ind,
                               ship_rec.shipment);   -- error flag

         L_seq_no := L_seq_no + 1;
       --
      end LOOP;
   else
      -- insert into ALC_HEAD
      insert into alc_head (order_no,
                            item,
                            pack_item,
                            seq_no,
                            obligation_key,
                            ce_id,
                            vessel_id,
                            voyage_flt_id,
                            estimated_depart_date,
                            alc_qty,
                            status,
                            error_ind)
                    values (I_order_no,
                            I_item,
                            I_pack_item,
                            L_seq_no,
                            I_obligation_key,
                            I_ce_id,
                            I_vessel_id,
                            I_voyage_flt_id,
                            I_etd,
                            I_item_qty,
                            L_status,       -- status = 'Pending'
                            I_error_ind);   -- error flag
      ---
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ALC_HEAD;
-------------------------------------------------------------------------------
FUNCTION ALLOC_PO_ITEM(O_error_message         IN OUT VARCHAR2,
                       I_obligation_key        IN     OBLIGATION.OBLIGATION_KEY%TYPE,
                       I_obligation_level      IN     OBLIGATION.OBLIGATION_LEVEL%TYPE,
                       I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                       I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                       I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                       I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                       I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                       I_comp_id               IN     ELC_COMP.COMP_ID%TYPE,
                       I_alloc_basis_uom       IN     UOM_CLASS.UOM%TYPE,
                       I_qty                   IN     OBLIGATION_COMP.QTY%TYPE,
                       I_amt_prim              IN     OBLIGATION_COMP.AMT%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(64)                := 'TRAN_ALLOC_SQL.ALLOC_PO_ITEM';
   L_obl_locs_exist       VARCHAR2(1)                 := 'N';
   L_alc_head_exists      VARCHAR2(1)                 := 'N';
   L_error_ind            ALC_HEAD.ERROR_IND%TYPE     := 'N';
   L_item_rec_qty         ORDLOC.QTY_ORDERED%TYPE     := 0;
   L_item_ord_qty         ORDLOC.QTY_ORDERED%TYPE     := 0;
   L_comp_rec_qty         ORDLOC.QTY_ORDERED%TYPE     := 0;
   L_comp_qty             ORDLOC.QTY_ORDERED%TYPE     := 0;
   L_total_comp_qty       ORDLOC.QTY_ORDERED%TYPE     := 0;
   L_temp_qty             ORDLOC.QTY_ORDERED%TYPE     := 0;
   L_act_value            ALC_COMP_LOC.ACT_VALUE%TYPE := 0;
   L_amt_prim             ALC_COMP_LOC.ACT_VALUE%TYPE := 0;
   L_loc_amt              ALC_COMP_LOC.ACT_VALUE%TYPE := 0;
   L_unit_cost            ORDLOC.UNIT_COST%TYPE       := 0;
   L_total_pack_cost      ORDLOC.UNIT_COST%TYPE       := 0;
   L_packitem_cost        ORDLOC.UNIT_COST%TYPE       := 0;
   L_qty                  ALC_COMP_LOC.QTY%TYPE       := 0;
   L_qty_rec              ORDLOC.QTY_RECEIVED%TYPE    := 0;
   L_qty_shp              ORDLOC.QTY_RECEIVED%TYPE    := 0;
   L_qty_ord              ORDLOC.QTY_ORDERED%TYPE     := 0;
   L_loc_qty              ORDLOC.QTY_ORDERED%TYPE     := 0;
   L_qty_shipped          SHIPSKU.QTY_EXPECTED%TYPE   := 0;
   L_comp_item            ITEM_MASTER.ITEM%TYPE;
   L_temp_comp_item        ITEM_MASTER.ITEM%TYPE;
   L_seq_no               ORDLOC_EXP.SEQ_NO%TYPE;
   L_supplier             SUPS.SUPPLIER%TYPE;
   L_origin_country_id    COUNTRY.COUNTRY_ID%TYPE;
   L_location             ORDLOC.LOCATION%TYPE;
   L_unit_of_work         IF_ERRORS.UNIT_OF_WORK%TYPE;
   L_uom                  UOM_CLASS.UOM%TYPE;
   L_standard_uom         UOM_CLASS.UOM%TYPE;
   L_temp_standard_uom    UOM_CLASS.UOM%TYPE;
   L_standard_class       UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor          UOM_CONVERSION.FACTOR%TYPE;
   ---
   L_carton_qty           TRANSPORTATION.CARTON_QTY%TYPE;
   L_carton_qty_uom       TRANSPORTATION.CARTON_UOM%TYPE;
   L_tran_item_qty        TRANSPORTATION.ITEM_QTY%TYPE;
   L_tran_item_qty_uom    TRANSPORTATION.ITEM_QTY_UOM%TYPE;
   L_gross_wt             TRANSPORTATION.GROSS_WT%TYPE;
   L_gross_wt_uom         TRANSPORTATION.GROSS_WT_UOM%TYPE;
   L_net_wt               TRANSPORTATION.NET_WT%TYPE;
   L_net_wt_uom           TRANSPORTATION.NET_WT_UOM%TYPE;
   L_cubic                TRANSPORTATION.CUBIC%TYPE;
   L_cubic_uom            TRANSPORTATION.CUBIC_UOM%TYPE;
   L_invoice_amt          TRANSPORTATION.INVOICE_AMT%TYPE;
   ---
   L_pack_ind             ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind         ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind        ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type            ITEM_MASTER.PACK_TYPE%TYPE;
   ---
   L_table                VARCHAR2(30);
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
   L_physical_loc         SHIPMENT.TO_LOC%TYPE;
   ---
   cursor C_GET_ITEM_QTY is
      select NVL(SUM(qty_ordered), 0) qty_ordered,
             NVL(SUM(qty_received), 0) qty_received
        from ordloc
       where order_no = I_order_no
         and item     = I_item;

   cursor C_GET_ITEM_SHP_QTY is
      select NVL(SUM(k.qty_expected), 0) qty_expected
        from shipment s,
             shipsku k
       where s.order_no = I_order_no
         and k.item     = I_item
         and s.shipment = k.shipment;

   cursor C_ALC_HEAD_EXISTS is
      select 'Y'
        from alc_head
       where order_no           = I_order_no
         and ((item             = I_item
               and pack_item   is NULL
               and nvl(L_pack_type,'N') != 'B')
           or (item             = L_comp_item
               and pack_item    = I_item
               and nvl(L_pack_type,'N') = 'B'))
         and obligation_key        = I_obligation_key
         and vessel_id             = I_vessel_id
         and voyage_flt_id         = I_voyage_flt_id
         and estimated_depart_date = I_estimated_depart_date;

   cursor C_GET_ORD_SUP_CTRY is
      select o.supplier,
             s.origin_country_id
        from ordhead o,
             ordsku s
       where o.order_no = I_order_no
         and o.order_no = s.order_no
         and s.item     = I_item;

   cursor C_GET_PACKITEMS is
      select item,
             qty
        from v_packsku_qty
       where pack_no = I_item;

   cursor C_GET_PACKITEM_COST is
        select (i.unit_cost * v.qty)
          from v_packsku_qty v,
               item_supp_country i
         where v.pack_no           = I_item
           and v.item              = L_comp_item
           and v.item              = i.item
           and i.supplier          = L_supplier
           and i.origin_country_id = L_origin_country_id;

   cursor C_SUM_PACKITEM_COST is
        select SUM(i.unit_cost * v.qty)
          from v_packsku_qty v,
               item_supp_country i
         where v.pack_no           = I_item
           and v.item              = i.item
           and i.supplier          = L_supplier
           and i.origin_country_id = L_origin_country_id;

   cursor C_GET_ORD_LOCS is
      select location,
             loc_type,
             NVL(qty_received, 0) qty_received,
             NVL(qty_ordered, 0) qty_ordered
        from ordloc
       where order_no = I_order_no
         and item     = I_item;

   cursor C_GET_QTY_SHIPPED is
      select NVL(SUM(k.qty_expected), 0) qty_expected
        from shipment s,
             shipsku k
       where s.order_no = I_order_no
         and k.item     = I_item
         and s.to_loc   = L_physical_loc
         and s.shipment = k.shipment;

   cursor C_GET_QTY_UOM is
      select per_count_uom
        from obligation_comp
       where obligation_key = I_obligation_key
         and comp_id        = I_comp_id;

   cursor C_LOCK_ALC_HEAD is
      select 'x'
        from alc_head
       where order_no         = I_order_no
         and ((item           = I_item
               and pack_item is NULL)
           or (item           = L_comp_item
               and pack_item  = I_item))
         and obligation_key        = I_obligation_key
         and vessel_id             = I_vessel_id
         and voyage_flt_id         = I_voyage_flt_id
         and estimated_depart_date = I_estimated_depart_date
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_ORD_SUP_CTRY','ORDHEAD,ORDSKU','Order: '||to_char(I_order_no));
   open C_GET_ORD_SUP_CTRY;
   SQL_LIB.SET_MARK('FETCH','C_GET_ORD_SUP_CTRY','ORDHEAD,ORDSKU','Order: '||to_char(I_order_no));
   fetch C_GET_ORD_SUP_CTRY into L_supplier,
                                 L_origin_country_id;
   SQL_LIB.SET_MARK('CLOSE','C_GET_ORD_SUP_CTRY','ORDHEAD,ORDSKU','Order: '||to_char(I_order_no));
   close C_GET_ORD_SUP_CTRY;
   ---
   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack_ind,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_pack_type,
                                    I_item) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_QTY_UOM','OBLIGATION_COMP','Obligation: '||to_char(I_obligation_key)||
                                                             ' Component: '||I_comp_id);
   open C_GET_QTY_UOM;
   SQL_LIB.SET_MARK('FETCH','C_GET_QTY_UOM','OBLIGATION_COMP','Obligation: '||to_char(I_obligation_key)||
                                                              ' Component: '||I_comp_id);
   fetch C_GET_QTY_UOM into L_uom;
   SQL_LIB.SET_MARK('CLOSE','C_GET_QTY_UOM','OBLIGATION_COMP','Obligation: '||to_char(I_obligation_key)||
                                                              ' Component: '||I_comp_id);
   close C_GET_QTY_UOM;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_ITEM_QTY','ORDLOC','order no: '||to_char(I_order_no)||
                                                     ' item: '||I_item);
   open C_GET_ITEM_QTY;
   SQL_LIB.SET_MARK('FETCH','C_GET_ITEM_QTY','ORDLOC','order no: '||to_char(I_order_no)||
                                                      ' item: '||I_item);
   fetch C_GET_ITEM_QTY into L_qty_ord,
                             L_qty_rec;
   SQL_LIB.SET_MARK('CLOSE','C_GET_ITEM_QTY','ORDLOC','order no: '||to_char(I_order_no)||
                                                      ' item: '||I_item);
   close C_GET_ITEM_QTY;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_ITEM_SHP_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||
                                                                   ' item: '||I_item);
   open C_GET_ITEM_SHP_QTY;
   SQL_LIB.SET_MARK('FETCH','C_GET_ITEM_SHP_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||
                                                                    ' item: '||I_item);
   fetch C_GET_ITEM_SHP_QTY into L_qty_shp;
   SQL_LIB.SET_MARK('CLOSE','C_GET_ITEM_SHP_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||
                                                                    ' item: '||I_item);
   close C_GET_ITEM_SHP_QTY;
   ---
   if nvl(L_pack_type,'N') = 'B' then
      SQL_LIB.SET_MARK('OPEN','C_SUM_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
      open C_SUM_PACKITEM_COST;
      SQL_LIB.SET_MARK('FETCH','C_SUM_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
      fetch C_SUM_PACKITEM_COST into L_total_pack_cost;
      SQL_LIB.SET_MARK('CLOSE','C_SUM_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
      close C_SUM_PACKITEM_COST;
      ---
      -- Get the Total Qty of the Pack
      ---
      FOR P_rec in C_GET_PACKITEMS LOOP
         L_temp_comp_item := P_rec.item;
         ---
         if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                             L_temp_standard_uom,
                                             L_standard_class,
                                             L_conv_factor,
                                             L_temp_comp_item,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if UOM_SQL.CONVERT(O_error_message,
                            L_comp_qty,
                            NVL(I_alloc_basis_uom, L_uom),
                            P_rec.qty,
                            L_temp_standard_uom,
                            L_temp_comp_item,
                            L_supplier,
                            L_origin_country_id) = FALSE then
            return FALSE;
         end if;
         ---
         L_total_comp_qty := L_total_comp_qty + L_comp_qty;
      END LOOP;
      ---
      FOR C_rec in C_GET_PACKITEMS LOOP
         L_comp_item       := C_rec.item;
         L_error_ind      := 'N';
         L_qty            := 0;
         ---
         if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                             L_standard_uom,
                                             L_standard_class,
                                             L_conv_factor,
                                             L_comp_item,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if I_qty <> 0 then
            if UOM_SQL.CONVERT(O_error_message,
                               L_comp_qty,
                               NVL(I_alloc_basis_uom, L_uom),
                               C_rec.qty,
                               L_standard_uom,
                               C_rec.item,
                               L_supplier,
                               L_origin_country_id) = FALSE then
               return FALSE;
            end if;
            ---
            if L_comp_qty = 0 then
               L_unit_of_work := 'Order No. '||to_char(I_order_no)||', Item '||L_comp_item||
                                 ', Pack '||I_item||
                                 ', Obligation '||to_char(I_obligation_key)||
                                 ', Vessel '||I_vessel_id||
                                 ', Voyage/Flight '||I_voyage_flt_id||
                                 ', ETD '||to_char(I_estimated_depart_date)||
                                 ', Component '||I_comp_id;
               ---
               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                       SQL_LIB.GET_MESSAGE_TEXT('ALC_UOM_ERROR',
                                                                                L_comp_item,
                                                                                NULL,
                                                                                NULL),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
               ---
               L_error_ind := 'Y';
            end if;
            ---
            if L_total_comp_qty = 0 then
               L_total_comp_qty := 1;
            end if;
            ---
            L_temp_qty := I_qty * (L_comp_qty / L_total_comp_qty);
            ---
            -- Convert the temp qty from the alloc basis uom to the comp item's stndrd uom.
            ---
            if L_temp_qty <> 0 then
               if UOM_SQL.CONVERT(O_error_message,
                                  L_qty,
                                  L_standard_uom,
                                  I_qty * L_comp_qty,
                                  NVL(I_alloc_basis_uom, L_uom),
                                  L_comp_item,
                                  L_supplier,
                                  L_origin_country_id) = FALSE then
                  return FALSE;
               end if;
               ---
               if L_qty = 0 then
                  L_unit_of_work := 'Order No. '||to_char(I_order_no)||', Item '||L_comp_item||
                                    ', Pack '||I_item||
                                    ', Obligation '||to_char(I_obligation_key)||
                                    ', Vessel '||I_vessel_id||
                                    ', Voyage/Flight '||I_voyage_flt_id||
                                    ', ETD '||to_char(I_estimated_depart_date)||
                                    ', Component '||I_comp_id;
                  ---
                  if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                          SQL_LIB.GET_MESSAGE_TEXT('ALC_UOM_ERROR',
                                                                                   L_comp_item,
                                                                                   NULL,
                                                                                   NULL),
                                                          L_program,
                                                          L_unit_of_work) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  L_error_ind := 'Y';
               end if;
            end if;
            ---
            if I_qty <> 0 then
               L_amt_prim := I_amt_prim * (L_temp_qty / I_qty);
            else
               L_amt_prim := 0;
            end if;
         else
            L_unit_of_work := 'Order No. '||to_char(I_order_no)||', Item '||L_comp_item||
                              ', Pack '||I_item||
                              ', Obligation '||to_char(I_obligation_key)||
                              ', Vessel '||I_vessel_id||
                              ', Voyage/Flight '||I_voyage_flt_id||
                              ', ETD '||to_char(I_estimated_depart_date)||
                              ', Component '||I_comp_id;
            ---
            if I_obligation_level = 'CUST' then
               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                       SQL_LIB.GET_MESSAGE_TEXT('CE_QTY_ERROR',
                                                                                NULL,
                                                                                NULL,
                                                                                NULL),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
            else
               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                       SQL_LIB.GET_MESSAGE_TEXT('TRAN_QTY_ERROR',
                                                                                NULL,
                                                                                NULL,
                                                                                NULL),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
            L_error_ind := 'Y';
            ---
            L_qty      := 1;
            L_amt_prim := 0;
         end if;
         ---
         if I_alloc_basis_uom is NULL then
            SQL_LIB.SET_MARK('OPEN','C_GET_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
            open C_GET_PACKITEM_COST;
            SQL_LIB.SET_MARK('FETCH','C_GET_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
            fetch C_GET_PACKITEM_COST into L_packitem_cost;
            SQL_LIB.SET_MARK('CLOSE','C_GET_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
            close C_GET_PACKITEM_COST;
            ---
            if L_total_pack_cost <> 0 then
               L_amt_prim := I_amt_prim * (L_packitem_cost / L_total_pack_cost);
            else
               L_amt_prim := 0;
            end if;
         end if;
         ---
         SQL_LIB.SET_MARK('OPEN','C_ALC_HEAD_EXISTS','ALC_HEAD',NULL);
         open C_ALC_HEAD_EXISTS;
         SQL_LIB.SET_MARK('FETCH','C_ALC_HEAD_EXISTS','ALC_HEAD',NULL);
         fetch C_ALC_HEAD_EXISTS into L_alc_head_exists;
         SQL_LIB.SET_MARK('CLOSE','C_ALC_HEAD_EXISTS','ALC_HEAD',NULL);
         close C_ALC_HEAD_EXISTS;
         ---
         if L_alc_head_exists = 'N' then
            if INSERT_ALC_HEAD(O_error_message,
                               I_order_no,
                               L_comp_item,
                               I_item,
                               I_obligation_key,
                               NULL,
                               I_vessel_id,
                               I_voyage_flt_id,
                               I_estimated_depart_date,
                               L_qty,
                               L_error_ind) = FALSE then
               return FALSE;
            end if;
         else
            if L_error_ind = 'Y' then
               ---
               -- Lock the ALC records.
               ---
               L_table := 'ALC_HEAD';
               SQL_LIB.SET_MARK('OPEN','C_LOCK_ALC_HEAD','ALC_HEAD',NULL);
               open C_LOCK_ALC_HEAD;
               SQL_LIB.SET_MARK('CLOSE','C_LOCK_ALC_HEAD','ALC_HEAD',NULL);
               close C_LOCK_ALC_HEAD;
               ---
               SQL_LIB.SET_MARK('UPDATE',NULL,'ALC_HEAD',NULL);
               ---
               update alc_head
                  set error_ind = 'Y'
                where order_no         = I_order_no
                  and ((item           = I_item
                        and pack_item is NULL)
                    or (item           = L_comp_item
                        and pack_item  = I_item))
                 and obligation_key        = I_obligation_key
                 and vessel_id             = I_vessel_id
                 and voyage_flt_id         = I_voyage_flt_id
                 and estimated_depart_date = I_estimated_depart_date;
            end if;
         end if;
         ---
         L_alc_head_exists := 'N';
         ---
         FOR L_rec in C_GET_ORD_LOCS LOOP
            L_location := L_rec.location;
            ---
            if L_rec.loc_type = 'W' then
               ---
               if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                                L_physical_loc,
                                                L_location)  = FALSE then
                  return FALSE;
               end if;
               ---
            else
               L_physical_loc := L_location;
            end if;

            SQL_LIB.SET_MARK('OPEN','C_GET_QTY_SHIPPED','SHIPMENT,SHIPSKU',NULL);
            open C_GET_QTY_SHIPPED;
            SQL_LIB.SET_MARK('FETCH','C_GET_QTY_SHIPPED','SHIPMENT,SHIPSKU',NULL);
            fetch C_GET_QTY_SHIPPED into L_qty_shipped;
            SQL_LIB.SET_MARK('CLOSE','C_GET_QTY_SHIPPED','SHIPMENT,SHIPSKU',NULL);
            close C_GET_QTY_SHIPPED;
            ---
            if L_qty_rec <> 0 then
               L_loc_qty := (NVL(L_rec.qty_received, 0) / L_qty_rec) * L_qty;
            else
               L_loc_qty := 0;
            end if;
            ---
            if L_loc_qty = 0 then
               L_amt_prim := 0;
               L_qty      := 1;
            end if;
            ---
            if INSERT_ALC_COMP_LOCS(O_error_message,
                                    I_order_no,
                                    L_comp_item,
                                    I_item,
                                    I_obligation_key,
                                    I_vessel_id,
                                    I_voyage_flt_id,
                                    I_estimated_depart_date,
                                    I_comp_id,
                                    L_rec.location,
                                    L_rec.loc_type,
                                    L_amt_prim / L_qty,
                                    L_loc_qty) = FALSE then
               return FALSE;
            end if;
            ---
         -- Removed call to insert_elc_exps.  Expenses are now
         -- logged at order approval time in alc_alloc_sql.insert_expense_comps
         ---
         END LOOP;
      END LOOP;  -- C_GET_PACKITEMS LOOP
   else  -- I_item is not a buyer pack
      ---
      -- convert obligation qty from alloc_basis_uom to standard uom
      ---
      if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                          L_standard_uom,
                                          L_standard_class,
                                          L_conv_factor,
                                          I_item,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if I_qty <> 0 then
         if UOM_SQL.CONVERT(O_error_message,
                            L_qty,
                            L_standard_uom,
                            I_qty,
                            NVL(I_alloc_basis_uom, L_uom),
                            I_item,
                            L_supplier,
                            L_origin_country_id) = FALSE then
            return FALSE;
         end if;
         ---
         if L_qty = 0 then
            L_unit_of_work := 'Order No. '||to_char(I_order_no)||', Item '||I_item||
                              ', Obligation '||to_char(I_obligation_key)||
                              ', Vessel '||I_vessel_id||
                              ', Voyage/Flight '||I_voyage_flt_id||
                              ', ETD '||to_char(I_estimated_depart_date)||
                              ', Component '||I_comp_id;
            L_error_ind    := 'Y';
            ---
            if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                    SQL_LIB.GET_MESSAGE_TEXT('ALC_UOM_ERROR',
                                                                             I_item,
                                                                             NULL,
                                                                             NULL),
                                                    L_program,
                                                    L_unit_of_work) = FALSE then
               return FALSE;
            end if;
         end if;
      else
         L_unit_of_work := 'Order No. '||to_char(I_order_no)||', Item '||I_item||
                           ', Obligation '||to_char(I_obligation_key)||
                           ', Vessel '||I_vessel_id||
                           ', Voyage/Flight '||I_voyage_flt_id||
                           ', ETD '||to_char(I_estimated_depart_date)||
                           ', Component '||I_comp_id;
         L_error_ind    := 'Y';
         ---
         if I_obligation_level = 'CUST' then
            if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                    SQL_LIB.GET_MESSAGE_TEXT('CE_QTY_ERROR',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL),
                                                    L_program,
                                                    L_unit_of_work) = FALSE then
               return FALSE;
            end if;
         else
            if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                    SQL_LIB.GET_MESSAGE_TEXT('TRAN_QTY_ERROR',
                                                                             NULL,
                                                                             NULL,
                                                                             NULL),
                                                    L_program,
                                                    L_unit_of_work) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         L_qty := I_qty;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ALC_HEAD_EXISTS','ALC_HEAD',NULL);
      open C_ALC_HEAD_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_ALC_HEAD_EXISTS','ALC_HEAD',NULL);
      fetch C_ALC_HEAD_EXISTS into L_alc_head_exists;
      SQL_LIB.SET_MARK('CLOSE','C_ALC_HEAD_EXISTS','ALC_HEAD',NULL);
      close C_ALC_HEAD_EXISTS;
      ---
      if L_alc_head_exists = 'N' then
         if INSERT_ALC_HEAD(O_error_message,
                            I_order_no,
                            I_item,
                            NULL,
                            I_obligation_key,
                            NULL,
                            I_vessel_id,
                            I_voyage_flt_id,
                            I_estimated_depart_date,
                            L_qty,
                            L_error_ind) = FALSE then
            return FALSE;
         end if;
      else
         if L_error_ind = 'Y' then
            ---
            -- Lock the ALC records.
            ---
            L_table := 'ALC_HEAD';
            SQL_LIB.SET_MARK('OPEN','C_LOCK_ALC_HEAD','ALC_HEAD',NULL);
            open C_LOCK_ALC_HEAD;
            SQL_LIB.SET_MARK('CLOSE','C_LOCK_ALC_HEAD','ALC_HEAD',NULL);
            close C_LOCK_ALC_HEAD;
            ---
            SQL_LIB.SET_MARK('UPDATE',NULL,'ALC_HEAD',NULL);
            ---
            update alc_head
               set error_ind = 'Y'
             where order_no         = I_order_no
               and ((item           = I_item
                     and pack_item is NULL)
                 or (item           = L_comp_item
                     and pack_item  = I_item))
               and obligation_key        = I_obligation_key
               and vessel_id             = I_vessel_id
               and voyage_flt_id         = I_voyage_flt_id
               and estimated_depart_date = I_estimated_depart_date;
         end if;
      end if;
      ---
      FOR C_rec in C_GET_ORD_LOCS LOOP
         L_location := C_rec.location;
         ---
         if c_rec.loc_type = 'W' then
            ---
            if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                             L_physical_loc,
                                             L_location)  = FALSE then
               return FALSE;
            end if;
            ---
         else
            L_physical_loc := L_location;
         end if;

         SQL_LIB.SET_MARK('OPEN','C_GET_QTY_SHIPPED','SHIPMENT,SHIPSKU',NULL);
         open C_GET_QTY_SHIPPED;
         SQL_LIB.SET_MARK('FETCH','C_GET_QTY_SHIPPED','SHIPMENT,SHIPSKU',NULL);
         fetch C_GET_QTY_SHIPPED into L_qty_shipped;
         SQL_LIB.SET_MARK('CLOSE','C_GET_QTY_SHIPPED','SHIPMENT,SHIPSKU',NULL);
         close C_GET_QTY_SHIPPED;
         ---
         if L_qty_rec <> 0 then
            L_loc_qty := (NVL(C_rec.qty_received, 0) / L_qty_rec) * L_qty;
         else
            L_loc_qty := 0;
         end if;
         ---
         if L_qty = 0 then
            L_amt_prim := 0;
            L_temp_qty := 1;
         else
            L_temp_qty := L_qty;
            L_amt_prim := I_amt_prim;
         end if;
         ---
         if INSERT_ALC_COMP_LOCS(O_error_message,
                                 I_order_no,
                                 I_item,
                                 NULL,
                                 I_obligation_key,
                                 I_vessel_id,
                                 I_voyage_flt_id,
                                 I_estimated_depart_date,
                                 I_comp_id,
                                 C_rec.location,
                                 C_rec.loc_type,
                                 L_amt_prim / L_temp_qty,
                                 L_loc_qty) = FALSE then
            return FALSE;
         end if;
         ---
         -- Removed call to insert_elc_exps.  Expenses are now
         -- logged at order approval time in alc_alloc_sql.insert_expense_comps
         ---
      END LOOP;
   end if;  -- nvl(L_pack_type,'N') = 'B'
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ALLOC_PO_ITEM;
------------------------------------------------------------------------------------
END TRAN_ALLOC_SQL;
/
