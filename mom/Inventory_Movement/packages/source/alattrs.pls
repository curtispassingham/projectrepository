CREATE OR REPLACE PACKAGE ALLOC_ATTRIB_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------
-- FUNCTION: ALLOC_DESC
-- Purpose:  Returns the allocation description.
------------------------------------------------------------------
FUNCTION ALLOC_DESC(O_error_message   IN OUT   VARCHAR2,
                    I_alloc           IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                    O_alloc_desc      IN OUT   ALLOC_HEADER.ALLOC_DESC%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------
-- FUNCTION: GET_HEADER_INFO
-- Purpose:  This function returns the header information that
--           is used by the allcfind form.
------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                         O_order_no        IN OUT   ALLOC_HEADER.ORDER_NO%TYPE,
                         O_wh              IN OUT   WH.WH%TYPE,
                         O_item            IN OUT   ITEM_MASTER.ITEM%TYPE,
                         O_status          IN OUT   ALLOC_HEADER.STATUS%TYPE,
                         O_alloc_desc      IN OUT   ALLOC_HEADER.ALLOC_DESC%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------
-- FUNCTION: DECODE_STATUS
-- Purpose:  This function takes a status indicator and
--           translates it into a full status description.
------------------------------------------------------------------
FUNCTION DECODE_STATUS(O_error_message   IN OUT   VARCHAR2,
                       I_status_ind      IN       VARCHAR2,
                       O_status_decode   IN OUT   VARCHAR2)
   RETURN BOOLEAN;
------------------------------------------------------------------
-- FUNCTION: GET_TOTAL_QTY_ALLOC
-- Purpose:  This function takes an alloc_no and gets the total
--           qty allocated in that allocation.
------------------------------------------------------------------
FUNCTION GET_TOTAL_QTY_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                             O_total_qty       IN OUT   NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------
-- FUNCTION: GET_TOTAL_QTY_TSF
-- Purpose:  This function takes an alloc_no and gets the total
--           qty transferred in that allocation.
------------------------------------------------------------------
FUNCTION GET_TOTAL_QTY_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                           O_total_qty       IN OUT   NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------
-- FUNCTION: GET_OTHER_ALLOC_QTY
-- Purpose:  This function takes an alloc number as well as a
--           order, wh, sku, combination and gets the total
--           qty allocated for that order-loc-sku on other
--           allocations.  If other allocations exist for that
--           order-loc-sku then O_found will be set to TRUE and
--           O_qty_alloc will contain the amount allocated on
--           those allocations.  If no other allocations exist
--           then O_found is set to false and O_qty_alloc is
--           set to 0.
------------------------------------------------------------------
FUNCTION GET_OTHER_ALLOC_QTY(O_error_message   IN OUT   VARCHAR2,
                             I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                             I_wh              IN       WH.WH%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             O_qty_alloc       IN OUT   ALLOC_DETAIL.QTY_ALLOCATED%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION: GET_TOTAL_QTY_ALLOC
-- Purpose:  This function takes the order_no, warehouse and item and returns the
--           qty allocated in that allocation.
------------------------------------------------------------------
FUNCTION GET_TOTAL_ALLOC_QTY(O_error_message IN OUT VARCHAR2,
                             O_qty_alloc     IN OUT ALLOC_DETAIL.QTY_ALLOCATED%TYPE,
                             I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                             I_wh            IN     WH.WH%TYPE,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------
-- Function:  CHECK_IN_PROGRESS
-- Purpose:   This function queries the ALLOC_DETAIL table to see if any
--            of the items on a particular allocation are currently being
--            worked on (e.g. have values > 0 in either of the selected_qty
--            or dist_qty columns).  If either of these columns have
--            a value for any item on the allocation, the output O_in_progress
--            should be set to 'Y'es.
-----------------------------------------------------------------------
FUNCTION CHECK_IN_PROGRESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_in_progress     IN OUT   VARCHAR2,
                           I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
--FUNCTION: UPD_QTYS_WHEN_CLOSE
--Purpose:  This function is called from APPT_DOC_CLOSE_SQL.CLOSE_ALL_ALLOCS.
--          This will release TSF_EXPECTED_QTY and TSF_RESERVED_QTY for non 
--          PREDIST allocations. This function will not do any reconcilation.
--          During docclose the reconcilation will be done by
--          STOCK_ORDER_RECONCILE_SQL.APPLY_ADJUSTMENTS_DOC_CLOSE.
-------------------------------------------------------------------------------
FUNCTION UPD_QTYS_WHEN_CLOSE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--FUNCTION: UPD_ALLOC_RESV_EXP
--Purpose:  This function will be used to update reserved qty and
--          expected qty's for an entire alloc_no by calling
--          UPD_ITEM_RESV_EXP for each alloc_detail on the alloc_no.
--------------------------------------------------------------------------------
FUNCTION UPD_ALLOC_RESV_EXP(O_error_message    IN OUT   VARCHAR2,
                            I_alloc_no         IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                            I_add_delete_ind   IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--FUNCTION: UPD_ITEM_RESV_EXP
--Purpose:  This function will be used to update reserved qty and
--          expected qty's when alloc_detail.allocated_qty is inserted, updated or
--          deleted for one location.
--------------------------------------------------------------------------------
FUNCTION UPD_ITEM_RESV_EXP(O_error_message   IN OUT   VARCHAR2,
                           I_item            IN       ALLOC_HEADER.ITEM%TYPE,
                           I_allocated_qty   IN       ALLOC_DETAIL.QTY_ALLOCATED%TYPE,
                           I_from_loc        IN       ALLOC_HEADER.WH%TYPE,
                           I_from_loc_type   IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                           I_to_loc          IN       ITEM_LOC_SOH.LOC%TYPE,
                           I_to_loc_type     IN       ITEM_LOC_SOH.LOC_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       DELETE_ALLOC_HEADER
-- Purpose:    Deletes alloc header records if all alloc detail records have
--             been deleted.
-------------------------------------------------------------------------------
FUNCTION DELETE_ALLOC_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    CLOSE_ALLOC
-- Purpose: This function is called from ordhead and ordloc form.
--          This will release the open alloc qty and close the allocation
--          This function will not do any reconcilation.
-------------------------------------------------------------------------------
FUNCTION CLOSE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function:  CHECK_IN_PROGRESS
-- Purpose:   This function queries the ALLOC_DETAIL table to see if any
--            of the items on a particular allocation are currently being
--            worked on (e.g. have values > 0 in either of the selected_qty
--            dist_qty, received qty, po rcvd qty or cancelled qty columns).
--       If either of these columns have a value for any item on the
--       allocation, the output O_in_progress should be set to 'Y'es.
-----------------------------------------------------------------------
FUNCTION CHECK_IN_PROGRESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_in_progress        OUT   VARCHAR2,
                           I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                           I_to_loc          IN       ALLOC_DETAIL.TO_LOC%TYPE,
                           I_to_loc_type     IN       ALLOC_DETAIL.TO_LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Name:    CHECK_RELEASE_DATE
-- Purpose: Verifies that the Not Before Date on ORDHEAD is NOT after the
--          Release Date on ALLOC_HEADER
-------------------------------------------------------------------------------
FUNCTION CHECK_RELEASE_DATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                            I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE,
                            I_nbd_date        IN       ORDHEAD.NOT_BEFORE_DATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    UPDATE_RELEASE_DATE
-- Purpose: Updates the Release Date on ALLOC_HEADER
-------------------------------------------------------------------------------
FUNCTION UPDATE_RELEASE_DATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                             I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE,
                             I_release_date    IN       ALLOC_HEADER.RELEASE_DATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    UPDATE_ASN_RELEASE_DATE
-- Purpose: Updates the Release Date of an ASN based allocation on ALLOC_HEADER
-------------------------------------------------------------------------------
FUNCTION UPDATE_ASN_RELEASE_DATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE,
                                 I_nb_date         IN       ALLOC_HEADER.RELEASE_DATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    MANUAL_ALLOC_EXISTS
-- Purpose: Check if any manually created allocation(s) are attached to the purchase order
-------------------------------------------------------------------------------
FUNCTION MANUAL_ALLOC_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   BOOLEAN,
                             I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    CLOSE_MANUAL_ALLOC
-- Purpose: Closes any manually created allocation(s) attached to the purchase order
--          by calling ALLOC_ATTRIB_SQL.CLOSE_ALLOC
-------------------------------------------------------------------------------
FUNCTION CLOSE_MANUAL_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    REINSTATE_MANUAL_ALLOC
-- Purpose: Reinstates any manually created allocation(s) attached to the purchase order
--          to 'A'pproved status
-------------------------------------------------------------------------------
FUNCTION REINSTATE_MANUAL_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    REINSTATE_REPL_ALLOC
-- Purpose: Reinstates any replenishment-generated allocation(s) attached to the purchase order
--          to 'A'pproved status
-------------------------------------------------------------------------------
FUNCTION REINSTATE_REPL_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE,
                              I_status          IN       ALLOC_HEADER.STATUS%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    OPN_MANL_ALC_EXIST
-- Purpose: Check if any manually created allocation(s) are attached to the purchase order, 
-- which are not in 'C'losed status.
-------------------------------------------------------------------------------
FUNCTION OPN_MANL_ALC_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
END ALLOC_ATTRIB_SQL;
/
