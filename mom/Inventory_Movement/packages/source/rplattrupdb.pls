CREATE OR REPLACE PACKAGE BODY REPL_ATTRIBUTE_UPDATE_SQL AS
-----------------------------------------------------------------------------------------------
FUNCTION CREATE_NEW_MRA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN       REPL_ITEM_LOC.ITEM%TYPE,
                        I_location        IN       REPL_ITEM_LOC.LOCATION%TYPE,
                        I_loc_type        IN       REPL_ITEM_LOC.LOC_TYPE%TYPE
                        )
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'REPL_ATTRIBUTE_UPDATE_SQL.CREATE_NEW_MRA';

   -- local variables to determine weekday
   L_offset        NUMBER(1)   := NULL;
   L_sunday        NUMBER(2)   := NULL;
   L_monday        NUMBER(2)   := NULL;
   L_tuesday       NUMBER(2)   := NULL;
   L_wednesday     NUMBER(2)   := NULL;
   L_thursday      NUMBER(2)   := NULL;
   L_friday        NUMBER(2)   := NULL;
   L_saturday      NUMBER(2)   := NULL;

   TYPE item_rec is RECORD(ITEM   ITEM_MASTER.ITEM%TYPE);
   TYPE item_table_type is TABLE of item_rec;
   items   ITEM_TBL;

   cursor C_ITEM is
      select item
        from item_master
       where (item = I_item
              or item_parent = I_item
              or item_grandparent = I_item);

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ITEM;
   fetch C_ITEM BULK COLLECT into items;
   close C_ITEM;

   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset) = FALSE then
      return FALSE;
   end if;

   L_sunday := L_offset + 1;

   L_monday := L_offset + 2;
   if L_monday > 7 then
      L_monday := L_monday - 7;
   end if;

   L_tuesday := L_offset + 3;
   if L_tuesday > 7 then
      L_tuesday := L_tuesday - 7;
   end if;

   L_wednesday := L_offset + 4;
   if L_wednesday > 7 then
      L_wednesday := L_wednesday - 7;
   end if;

   L_thursday := L_offset + 5;
   if L_thursday > 7 then
      L_thursday := L_thursday - 7;
   end if;

   L_friday := L_offset + 6;
   if L_friday > 7 then
      L_friday := L_friday - 7;
   end if;

   L_saturday := L_offset + 7;
   if L_saturday > 7 then
      L_saturday := L_saturday - 7;
   end if;

   delete from item_repl_day;

   insert into item_repl_day(item,
                             sunday_ind,
                             monday_ind,
                             tuesday_ind,
                             wednesday_ind,
                             thursday_ind,
                             friday_ind,
                             saturday_ind,
                             day_ind)
                      select item,
                             MAX(DECODE(weekday, L_sunday, 'Y', 'N')),
                             MAX(DECODE(weekday, L_monday, 'Y', 'N')),
                             MAX(DECODE(weekday, L_tuesday, 'Y', 'N')),
                             MAX(DECODE(weekday, L_wednesday, 'Y', 'N')),
                             MAX(DECODE(weekday, L_thursday, 'Y', 'N')),
                             MAX(DECODE(weekday, L_friday, 'Y', 'N')),
                             MAX(DECODE(weekday, L_saturday, 'Y', 'N')),
                             MAX(DECODE(weekday, -1, 'N', 'Y'))
                        from (select /*+ cardinality(item_tab,100) */item,
                                     weekday
                                from repl_day rd,
                                     TABLE(CAST(items as ITEM_TBL)) item_tab
                               where item = value(item_tab)
                                 and location = I_location
                               union all
                              select /*+ cardinality(item_tab,100) */ value(item_tab) item,
                                     -1 weekday
                                from TABLE(CAST(items as ITEM_TBL)) item_tab
                               where NOT EXISTS (select 1
                                                   from repl_day rd
                                                  where rd.item = value(item_tab)
                                                    and rd.location = I_location))
                       group by item;

   merge into master_repl_attr tgt
      using (select ril.item,
                    ril.location,
                    ril.loc_type,
                    ril.item_parent,
                    ril.item_grandparent,
                    ril.primary_repl_supplier,
                    ril.origin_country_id,
                    ril.review_cycle,
                    ril.stock_cat,
                    ril.repl_order_ctrl,
                    ril.source_wh,
                    ril.pres_stock,
                    ril.demo_stock,
                    ril.repl_method,
                    ril.min_stock,
                    ril.max_stock,
                    ril.incr_pct,
                    ril.min_supply_days,
                    ril.max_supply_days,
                    ril.time_supply_horizon,
                    ril.inv_selling_days,
                    ril.service_level,
                    ril.lost_sales_factor,
                    ril.reject_store_ord_ind,
                    ril.non_scaling_ind,
                    ril.max_scale_value,
                    ril.pickup_lead_time,
                    ril.wh_lead_time,
                    ril.terminal_stock_qty,
                    ril.season_id,
                    ril.phase_id,
                    ril.last_review_date,
                    ril.next_review_date,
                    ril.primary_pack_no,
                    ril.primary_pack_qty,
                    ril.unit_tolerance,
                    ril.pct_tolerance,
                    ril.item_season_seq_no,
                    ril.use_tolerance_ind,
                    ril.last_delivery_date,
                    ril.next_delivery_date,
                    ril.mbr_order_qty,
                    ril.adj_pickup_lead_time,
                    ril.adj_supp_lead_time,
                    ril.tsf_po_link_no,
                    ril.last_roq,
                    ril.status,
                    ril.dept,
                    ril.class,
                    ril.subclass,
                    ril.store_ord_mult,
                    ril.unit_cost,
                    isc.lead_time,
                    ril.inner_pack_size,
                    ril.supp_pack_size,
                    ril.ti,
                    ril.hi,
                    ril.round_lvl,
                    ril.round_to_inner_pct,
                    ril.round_to_case_pct,
                    ril.round_to_layer_pct,
                    ril.round_to_pallet_pct,
                    ril.service_level_type,
                    ird.day_ind update_days_ind,
                    ird.monday_ind,
                    ird.tuesday_ind,
                    ird.wednesday_ind,
                    ird.thursday_ind,
                    ird.friday_ind,
                    ird.saturday_ind,
                    ird.sunday_ind,
                    sysdate create_datetime,
                    sysdate last_update_datetime,
                    get_user last_update_id,
                    ril.mult_runs_per_day_ind,
                    ril.tsf_zero_soh_ind
               from repl_item_loc ril,
                    item_supp_country isc,
                    item_repl_day ird
              where ril.item = ird.item
                and ril.location = I_location
                and ril.loc_type = I_loc_type
                and ril.item = isc.item(+)
                and ril.origin_country_id = isc.origin_country_id(+)
                and ril.primary_repl_supplier = isc.supplier(+)) src
      on (tgt.item = src.item
          and tgt.location = src.location)
      when matched then
         update set tgt.last_update_datetime = src.last_update_datetime,
                    tgt.last_update_id = src.last_update_id
      when not matched then
         insert(tgt.item,
                tgt.location,
                tgt.loc_type,
                tgt.item_parent,
                tgt.item_grandparent,
                tgt.primary_repl_supplier,
                tgt.origin_country_id,
                tgt.review_cycle,
                tgt.stock_cat,
                tgt.repl_order_ctrl,
                tgt.source_wh,
                tgt.pres_stock,
                tgt.demo_stock,
                tgt.repl_method,
                tgt.min_stock,
                tgt.max_stock,
                tgt.incr_pct,
                tgt.min_supply_days,
                tgt.max_supply_days,
                tgt.time_supply_horizon,
                tgt.inv_selling_days,
                tgt.service_level,
                tgt.lost_sales_factor,
                tgt.reject_store_ord_ind,
                tgt.non_scaling_ind,
                tgt.max_scale_value,
                tgt.pickup_lead_time,
                tgt.wh_lead_time,
                tgt.terminal_stock_qty,
                tgt.season_id,
                tgt.phase_id,
                tgt.last_review_date,
                tgt.next_review_date,
                tgt.primary_pack_no,
                tgt.primary_pack_qty,
                tgt.unit_tolerance,
                tgt.pct_tolerance,
                tgt.item_season_seq_no,
                tgt.use_tolerance_ind,
                tgt.last_delivery_date,
                tgt.next_delivery_date,
                tgt.mbr_order_qty,
                tgt.adj_pickup_lead_time,
                tgt.adj_supp_lead_time,
                tgt.tsf_po_link_no,
                tgt.last_roq,
                tgt.status,
                tgt.dept,
                tgt.class,
                tgt.subclass,
                tgt.store_ord_mult,
                tgt.unit_cost,
                tgt.supp_lead_time,
                tgt.inner_pack_size,
                tgt.supp_pack_size,
                tgt.ti,
                tgt.hi,
                tgt.round_lvl,
                tgt.round_to_inner_pct,
                tgt.round_to_case_pct,
                tgt.round_to_layer_pct,
                tgt.round_to_pallet_pct,
                tgt.service_level_type,
                tgt.update_days_ind,
                tgt.monday_ind,
                tgt.tuesday_ind,
                tgt.wednesday_ind,
                tgt.thursday_ind,
                tgt.friday_ind,
                tgt.saturday_ind,
                tgt.sunday_ind,
                tgt.create_datetime,
                tgt.last_update_datetime,
                tgt.last_update_id,
                tgt.mult_runs_per_day_ind,
                tgt.tsf_zero_soh_ind)
         values(src.item,
                src.location,
                src.loc_type,
                src.item_parent,
                src.item_grandparent,
                src.primary_repl_supplier,
                src.origin_country_id,
                src.review_cycle,
                src.stock_cat,
                src.repl_order_ctrl,
                src.source_wh,
                src.pres_stock,
                src.demo_stock,
                src.repl_method,
                src.min_stock,
                src.max_stock,
                src.incr_pct,
                src.min_supply_days,
                src.max_supply_days,
                src.time_supply_horizon,
                src.inv_selling_days,
                src.service_level,
                src.lost_sales_factor,
                src.reject_store_ord_ind,
                src.non_scaling_ind,
                src.max_scale_value,
                src.pickup_lead_time,
                src.wh_lead_time,
                src.terminal_stock_qty,
                src.season_id,
                src.phase_id,
                src.last_review_date,
                src.next_review_date,
                src.primary_pack_no,
                src.primary_pack_qty,
                src.unit_tolerance,
                src.pct_tolerance,
                src.item_season_seq_no,
                src.use_tolerance_ind,
                src.last_delivery_date,
                src.next_delivery_date,
                src.mbr_order_qty,
                src.adj_pickup_lead_time,
                src.adj_supp_lead_time,
                src.tsf_po_link_no,
                src.last_roq,
                src.status,
                src.dept,
                src.class,
                src.subclass,
                src.store_ord_mult,
                src.unit_cost,
                src.lead_time,
                src.inner_pack_size,
                src.supp_pack_size,
                src.ti,
                src.hi,
                src.round_lvl,
                src.round_to_inner_pct,
                src.round_to_case_pct,
                src.round_to_layer_pct,
                src.round_to_pallet_pct,
                src.service_level_type,
                src.update_days_ind,
                src.monday_ind,
                src.tuesday_ind,
                src.wednesday_ind,
                src.thursday_ind,
                src.friday_ind,
                src.saturday_ind,
                src.sunday_ind,
                src.create_datetime,
                src.last_update_datetime,
                src.last_update_id,
                src.mult_runs_per_day_ind,
                src.tsf_zero_soh_ind);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_NEW_MRA;
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_MRA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item            IN       REPL_ITEM_LOC.ITEM%TYPE,
                    I_location        IN       REPL_ITEM_LOC.LOCATION%TYPE,
                    I_loc_type        IN       REPL_ITEM_LOC.LOC_TYPE%TYPE
                    )
   RETURN BOOLEAN IS

   L_program       VARCHAR2(65)                                 := 'REPL_ATTRIBUTE_UPDATE_SQL.UPDATE_MRA';
   L_table         VARCHAR2(20)                                 := 'MASTER_REPL_ATTR';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_user          MASTER_REPL_ATTR.LAST_UPDATE_ID%TYPE         := GET_USER;
   L_sysdate       MASTER_REPL_ATTR.LAST_UPDATE_DATETIME%TYPE   := SYSDATE;

   -- local variables to determine weekday
   L_offset        NUMBER(1)   := NULL;
   L_sunday        NUMBER(2)   := NULL;
   L_monday        NUMBER(2)   := NULL;
   L_tuesday       NUMBER(2)   := NULL;
   L_wednesday     NUMBER(2)   := NULL;
   L_thursday      NUMBER(2)   := NULL;
   L_friday        NUMBER(2)   := NULL;
   L_saturday      NUMBER(2)   := NULL;

   cursor C_UPDATE_MRA is
      select r.*,
             NVL(sun.sun_ind, 'N') sun_ind,
             NVL(mon.mon_ind, 'N') mon_ind,
             NVL(tue.tue_ind, 'N') tue_ind,
             NVL(wed.wed_ind, 'N') wed_ind,
             NVL(thu.thu_ind, 'N') thu_ind,
             NVL(fri.fri_ind, 'N') fri_ind,
             NVL(sat.sat_ind, 'N') sat_ind,
             NVL(update_days.update_days_ind, 'N') update_days_ind
        from repl_item_loc r,
             item_master i,
             item_supp_country isc,
             (select item,
                     location,
                     'Y' sun_ind
                from repl_day
               where weekday = L_sunday) sun,
             (select item,
                     location,
                     'Y' mon_ind
                from repl_day
               where weekday = L_monday) mon,
             (select item,
                     location,
                     'Y' tue_ind
                from repl_day
               where weekday = L_tuesday) tue,
             (select item,
                     location,
                     'Y' wed_ind
                from repl_day
               where weekday = L_wednesday) wed,
             (select item,
                     location,
                     'Y' thu_ind
                from repl_day
               where weekday = L_thursday) thu,
             (select item,
                     location,
                     'Y' fri_ind
                from repl_day
               where weekday = L_friday) fri,
             (select item,
                     location,
                     'Y' sat_ind
                from repl_day
               where weekday = L_saturday) sat,
             (select distinct 'Y' update_days_ind,
                     item,
                     location
                from repl_day) update_days
       where (i.item = I_item or
              i.item_parent = I_item or
              i.item_grandparent = I_item)
         and r.item = i.item
         and r.item = mon.item(+)
         and r.item = tue.item(+)
         and r.item = wed.item(+)
         and r.item = thu.item(+)
         and r.item = fri.item(+)
         and r.item = sat.item(+)
         and r.item = sun.item(+)
         and r.item = update_days.item(+)
         and r.location = I_location
         and r.loc_type = I_loc_type
         and r.item = isc.item(+)
         and r.origin_country_id = isc.origin_country_id(+)
         and r.primary_repl_supplier = isc.supplier(+)
         and r.location = mon.location(+)
         and r.location = tue.location(+)
         and r.location = wed.location(+)
         and r.location = thu.location(+)
         and r.location = fri.location(+)
         and r.location = sat.location(+)
         and r.location = sun.location(+)
         and r.location = update_days.location(+);

   cursor C_LOCK_MRA is
      select mra.*
        from master_repl_attr mra,
             item_master i
       where (i.item             = I_item or
              i.item_parent      = I_item or
              i.item_grandparent = I_item)
         and mra.item     = i.item
         and mra.location = I_location
         for update of mra.item nowait;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_MRA',
                    L_table,
                    'ITEM: '||TO_CHAR(I_item)||'/ LOCATION: '||TO_CHAR(I_location));
   open C_LOCK_MRA;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_MRA',
                    L_table,
                    'ITEM: '||TO_CHAR(I_item)||'/ LOCATION: '||TO_CHAR(I_location));
   close C_LOCK_MRA;

   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset) = FALSE then
      return FALSE;
   end if;

   ---
   L_sunday := L_offset + 1;

   L_monday := L_offset + 2;
   if L_monday > 7 then
      L_monday := L_monday - 7;
   end if;

   L_tuesday := L_offset + 3;
   if L_tuesday > 7 then
      L_tuesday := L_tuesday - 7;
   end if;

   L_wednesday := L_offset + 4;
   if L_wednesday > 7 then
      L_wednesday := L_wednesday - 7;
   end if;

   L_thursday := L_offset + 5;
   if L_thursday > 7 then
      L_thursday := L_thursday - 7;
   end if;

   L_friday := L_offset + 6;
   if L_friday > 7 then
      L_friday := L_friday - 7;
   end if;

   L_saturday := L_offset + 7;
   if L_saturday > 7 then
      L_saturday := L_saturday - 7;
   end if;
   ---
   for rec in C_UPDATE_MRA LOOP
      update master_repl_attr
         set primary_repl_supplier = rec.primary_repl_supplier,
             origin_country_id     = rec.origin_country_id,
             review_cycle          = rec.review_cycle,
             stock_cat             = rec.stock_cat,
             repl_order_ctrl       = rec.repl_order_ctrl,
             source_wh             = rec.source_wh,
             pres_stock            = rec.pres_stock,
             demo_stock            = rec.demo_stock,
             repl_method           = rec.repl_method,
             min_stock             = rec.min_stock,
             max_stock             = rec.max_stock,
             incr_pct              = rec.incr_pct,
             min_supply_days       = rec.min_supply_days,
             max_supply_days       = rec.max_supply_days,
             time_supply_horizon   = rec.time_supply_horizon,
             inv_selling_days      = rec.inv_selling_days,
             service_level         = rec.service_level,
             lost_sales_factor     = rec.lost_sales_factor,
             reject_store_ord_ind  = rec.reject_store_ord_ind,
             non_scaling_ind       = rec.non_scaling_ind,
             max_scale_value       = rec.max_scale_value,
             pickup_lead_time      = rec.pickup_lead_time,
             wh_lead_time          = rec.wh_lead_time,
             terminal_stock_qty    = rec.terminal_stock_qty,
             season_id             = rec.season_id,
             phase_id              = rec.phase_id,
             primary_pack_no       = rec.primary_pack_no,
             unit_tolerance        = rec.unit_tolerance,
             pct_tolerance         = rec.pct_tolerance,
             use_tolerance_ind     = rec.use_tolerance_ind,
             service_level_type    = rec.service_level_type,
             update_days_ind       = rec.update_days_ind,
             monday_ind            = rec.mon_ind,
             tuesday_ind           = rec.tue_ind,
             wednesday_ind         = rec.wed_ind,
             thursday_ind          = rec.thu_ind,
             friday_ind            = rec.fri_ind,
             saturday_ind          = rec.sat_ind,
             sunday_ind            = rec.sun_ind,
             last_review_date      = rec.last_review_date,
             next_review_date      = rec.next_review_date,
             primary_pack_qty      = rec.primary_pack_qty,
             item_season_seq_no    = rec.item_season_seq_no,
             last_delivery_date    = rec.last_delivery_date,
             next_delivery_date    = rec.next_delivery_date,
             mbr_order_qty         = rec.mbr_order_qty,
             adj_pickup_lead_time  = rec.adj_pickup_lead_time,
             adj_supp_lead_time    = rec.adj_supp_lead_time,
             tsf_po_link_no        = rec.tsf_po_link_no,
             last_roq              = rec.last_roq,
             status                = rec.status,
             dept                  = rec.dept,
             class                 = rec.class,
             subclass              = rec.subclass,
             store_ord_mult        = rec.store_ord_mult,
             unit_cost             = rec.unit_cost,
             supp_lead_time        = rec.supp_lead_time,
             inner_pack_size       = rec.inner_pack_size,
             supp_pack_size        = rec.supp_pack_size,
             ti                    = rec.ti,
             hi                    = rec.hi,
             round_lvl             = rec.round_lvl,
             round_to_inner_pct    = rec.round_to_inner_pct,
             round_to_case_pct     = rec.round_to_case_pct,
             round_to_layer_pct    = rec.round_to_layer_pct,
             round_to_pallet_pct   = rec.round_to_pallet_pct,
             last_update_datetime  = L_sysdate,
             last_update_id        = L_user,
             mult_runs_per_day_ind = rec.mult_runs_per_day_ind,
             tsf_zero_soh_ind      = rec.tsf_zero_soh_ind
       where item     = rec.item
         and location = rec.location;
   END LOOP;

   return TRUE;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'I_item     = '||I_item,
                                            'I_location = '||I_location);
      return FALSE;
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_MRA;
--------------------------------------------------------------------------------------------
END REPL_ATTRIBUTE_UPDATE_SQL;
/
