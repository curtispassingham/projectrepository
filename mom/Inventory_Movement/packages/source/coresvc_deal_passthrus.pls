-- File Name : CORESVC_DEAL_PASSTHRU_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_DEAL_PASSTHRU AUTHID CURRENT_USER AS
   template_key               CONSTANT  VARCHAR2(255) :='DEAL_PASSTHRU_DATA';
   action_new                           VARCHAR2(25)  :='NEW';
   action_mod                           VARCHAR2(25)  :='MOD';
   action_del                           VARCHAR2(25)  :='DEL';
   DEAL_PASSTHRU_sheet                  VARCHAR2(255) :='DEAL_PASSTHRU';
   DEAL_PASSTHRU$Action                 NUMBER        := 1;
   DEAL_PASSTHRU$DEPT                   NUMBER        := 2;
   DEAL_PASSTHRU$SUPPLIER               NUMBER        := 3;
   DEAL_PASSTHRU$COSTING_LOC            NUMBER        := 4;
   DEAL_PASSTHRU$LOCATION               NUMBER        := 5;
   DEAL_PASSTHRU$LOC_TYPE               NUMBER        := 6;
   DEAL_PASSTHRU$PASSTHRU_PCT           NUMBER        := 7;
   sheet_name_trans                     S9T_PKG.trans_map_typ;
   action_column                        VARCHAR2(255)         :='ACTION';
   template_category                    CODE_DETAIL.CODE%TYPE :='RMSFRN';

   TYPE DEAL_PASSTHRU_rec_tab IS TABLE OF DEAL_PASSTHRU%ROWTYPE;
--------------------------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT  NUMBER,
                        I_file_id         IN      s9t_folder.file_id%TYPE,
                        I_process_id      IN      NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------------------------------
END CORESVC_DEAL_PASSTHRU;
/
