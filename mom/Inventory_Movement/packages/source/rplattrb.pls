CREATE OR REPLACE PACKAGE BODY REPL_ATTRIBUTE_MAINTENANCE_SQL AS

-- declare variables that will be used globally in the package.

   LP_report_required                 BOOLEAN                                  := FALSE;
   LP_action                          VARCHAR2(1)                              := Null;
   LP_repl_method_update              VARCHAR2(1)                              := Null;
   LP_item_type                       ITEM_MASTER.PACK_IND%TYPE                := Null;
   LP_item_level                      ITEM_MASTER.ITEM_LEVEL%TYPE              := Null;
   LP_tran_level                      ITEM_MASTER.TRAN_LEVEL%TYPE              := Null;
   LP_item_grandparent                ITEM_MASTER.ITEM_GRANDPARENT%TYPE        := Null;
   LP_item_parent                     ITEM_MASTER.ITEM_PARENT%TYPE             := Null;
   LP_item                            ITEM_MASTER.ITEM%TYPE                    := Null;
   LP_group_type                      VARCHAR2(3)                              := Null;
   LP_value                           VARCHAR2(10)                             := Null;
   LP_repl_order_ctrl                 REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE       := Null;
   LP_stock_cat                       REPL_ITEM_LOC.STOCK_CAT%TYPE             := Null;
   LP_activate_date                   REPL_ITEM_LOC.ACTIVATE_DATE%TYPE         := Null;
   LP_deactivate_date                 REPL_ITEM_LOC.DEACTIVATE_DATE%TYPE       := Null;
   LP_source_wh                       REPL_ITEM_LOC.SOURCE_WH%TYPE             := Null;
   LP_pres_stock                      REPL_ITEM_LOC.PRES_STOCK%TYPE            := Null;
   LP_demo_stock                      REPL_ITEM_LOC.DEMO_STOCK%TYPE            := Null;
   LP_repl_method                     REPL_ITEM_LOC.REPL_METHOD%TYPE           := Null;
   LP_min_stock                       REPL_ITEM_LOC.MIN_STOCK%TYPE             := Null;
   LP_max_stock                       REPL_ITEM_LOC.MAX_STOCK%TYPE             := Null;
   LP_incr_pct                        REPL_ITEM_LOC.INCR_PCT%TYPE              := Null;
   LP_min_supply_days                 REPL_ITEM_LOC.MIN_SUPPLY_DAYS%TYPE       := Null;
   LP_max_supply_days                 REPL_ITEM_LOC.MAX_SUPPLY_DAYS%TYPE       := Null;
   LP_time_supply_horizon             REPL_ITEM_LOC.TIME_SUPPLY_HORIZON%TYPE   := Null;
   LP_inv_selling_days                REPL_ITEM_LOC.INV_SELLING_DAYS%TYPE      := Null;
   LP_service_level                   REPL_ITEM_LOC.SERVICE_LEVEL%TYPE         := Null;
   LP_lost_sales_factor               REPL_ITEM_LOC.LOST_SALES_FACTOR%TYPE     := Null;
   LP_non_scaling_ind                 REPL_ITEM_LOC.NON_SCALING_IND%TYPE       := Null;
   LP_max_scale_value                 REPL_ITEM_LOC.MAX_SCALE_VALUE%TYPE       := Null;
   LP_pickup_lead_time                REPL_ITEM_LOC.PICKUP_LEAD_TIME%TYPE      := Null;
   LP_wh_lead_time                    REPL_ITEM_LOC.WH_LEAD_TIME%TYPE          := Null;
   LP_user_id                         VARCHAR2(30)                             := Null;
   LP_forecast_ind                    ITEM_MASTER.FORECAST_IND%TYPE            := Null;
   LP_terminal_stock_qty              REPL_ITEM_LOC.TERMINAL_STOCK_QTY%TYPE    := Null;
   LP_season_id                       REPL_ITEM_LOC.SEASON_ID%TYPE             := Null;
   LP_phase_id                        REPL_ITEM_LOC.PHASE_ID%TYPE              := Null;
   LP_supplier                        REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE := Null;
   LP_origin_country_id               REPL_ITEM_LOC.ORIGIN_COUNTRY_ID%TYPE     := Null;
   LP_review_cycle                    REPL_ITEM_LOC.REVIEW_CYCLE%TYPE          := Null;
   LP_update_days                     VARCHAR2(1)                              := Null;
   LP_monday                          VARCHAR2(1)                              := Null;
   LP_tuesday                         VARCHAR2(1)                              := Null;
   LP_wednesday                       VARCHAR2(1)                              := Null;
   LP_thursday                        VARCHAR2(1)                              := Null;
   LP_friday                          VARCHAR2(1)                              := Null;
   LP_saturday                        VARCHAR2(1)                              := Null;
   LP_sunday                          VARCHAR2(1)                              := Null;
   LP_system_options_rec              SYSTEM_OPTIONS%ROWTYPE;
   LP_department                      ITEM_MASTER.DEPT%TYPE                    := Null;
   LP_class                           ITEM_MASTER.CLASS%TYPE                   := Null;
   LP_subclass                        ITEM_MASTER.SUBCLASS%TYPE                := Null;
   LP_store_ord_mult                  ITEM_MASTER.STORE_ORD_MULT%TYPE          := Null;
   LP_simple_pack_ind                 ITEM_MASTER.PACK_IND%TYPE                := Null;
   LP_primary_pack_no                 REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE       := Null;
   LP_primary_pack_qty                REPL_ITEM_LOC.PRIMARY_PACK_QTY%TYPE      := Null;
   LP_unit_tolerance                  REPL_ITEM_LOC.UNIT_TOLERANCE%TYPE        := Null;
   LP_pct_tolerance                   REPL_ITEM_LOC.PCT_TOLERANCE%TYPE         := Null;
   LP_use_tolerance_ind               REPL_ITEM_LOC.USE_TOLERANCE_IND%TYPE     := Null;
   LP_item_season_seq_no              REPL_ITEM_LOC.ITEM_SEASON_SEQ_NO%TYPE    := Null;
   LP_last_delivery_date              REPL_ITEM_LOC.LAST_DELIVERY_DATE%TYPE    := Null;
   LP_create_datetime                 REPL_ITEM_LOC.CREATE_DATETIME%TYPE       := Null;
   LP_last_update_datetime            REPL_ITEM_LOC.LAST_UPDATE_DATETIME%TYPE  := Null;
   LP_last_update_id                  REPL_ITEM_LOC.LAST_UPDATE_ID%TYPE        := Null;
   LP_rej_st_ord_ind                  REPL_ITEM_LOC.REJECT_STORE_ORD_IND%TYPE  := Null;
   LP_default_pack_ind                VARCHAR2(1)                              := Null;
   LP_reject_pack                     VARCHAR2(1)                              := 'N';
   LP_remove_pack_ind                 VARCHAR2(1)                              := Null;

   LP_repl_attr_id                    REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE          := NULL;
   LP_sch_active_date                 REPL_ATTR_UPDATE_HEAD.SCHEDULED_ACTIVE_DATE%TYPE := NULL;
   LP_mra_update                      REPL_ATTR_UPDATE_HEAD.MRA_UPDATE%TYPE            := NULL;
   LP_mra_restore                     REPL_ATTR_UPDATE_HEAD.MRA_RESTORE%TYPE           := NULL;
   LP_service_level_type              REPL_ATTR_UPDATE_HEAD.SERVICE_LEVEL_TYPE%TYPE    := NULL;
   LP_create_date                     REPL_ATTR_UPDATE_HEAD.CREATE_DATE%TYPE           := NULL;
   LP_create_id                       REPL_ATTR_UPDATE_HEAD.CREATE_ID%TYPE             := NULL;
   LP_sch_repl_desc                   REPL_ATTR_UPDATE_HEAD.SCH_RPL_DESC%TYPE          := NULL;
   LP_size_profile_ind                VARCHAR2(1)                                      := NULL;
   LP_diff_1                          REPL_ATTR_UPDATE_ITEM.DIFF_1%TYPE                := NULL;
   LP_diff_2                          REPL_ATTR_UPDATE_ITEM.DIFF_2%TYPE                := NULL;
   LP_diff_3                          REPL_ATTR_UPDATE_ITEM.DIFF_3%TYPE                := NULL;
   LP_diff_4                          REPL_ATTR_UPDATE_ITEM.DIFF_4%TYPE                := NULL;
   LP_add_lead_time_ind               REPL_ITEM_LOC.ADD_LEAD_TIME_IND%TYPE             := NULL;
   LP_apply_pres_ind                  SYSTEM_OPTIONS.APPLY_PROF_PRES_STOCK%TYPE        := NULL;
   LP_repl_hist_ind                   VARCHAR2(1)                                      := NULL;
   LP_repl_hist_ret_weeks             SYSTEM_OPTIONS.REPL_ATTR_HIST_RETENTION_WEEKS%TYPE  := NULL;
   LP_orig_min_stock                  REPL_ITEM_LOC.MIN_STOCK%TYPE                     := NULL;
   LP_orig_max_stock                  REPL_ITEM_LOC.MAX_STOCK%TYPE                     := NULL;
   LP_orig_terminal_stock             REPL_ITEM_LOC.TERMINAL_STOCK_QTY%TYPE            := NULL;
   LP_orig_pres_stock                 REPL_ITEM_LOC.PRES_STOCK%TYPE                    := NULL;
   LP_supp_lead_time                  REPL_ITEM_LOC.SUPP_LEAD_TIME%TYPE                := NULL;
   LP_multiple_runs_per_day_ind       REPL_ITEM_LOC.MULT_RUNS_PER_DAY_IND%TYPE         := NULL;
   LP_tsf_zero_soh_ind                REPL_ITEM_LOC.TSF_ZERO_SOH_IND%TYPE              := NULL;


--------------------------------------------------------------------------------------------------------
FUNCTION GET_REPL_ATTR_UPDATE_EXCLUDE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists          IN OUT   BOOLEAN,
                                      O_row             IN OUT   REPL_ATTR_UPDATE_EXCLUDE%ROWTYPE,
                                      I_repl_id         IN       REPL_ATTR_UPDATE_EXCLUDE.REPL_ATTR_ID%TYPE,
                                      I_item            IN       REPL_ATTR_UPDATE_EXCLUDE.ITEM%TYPE,
                                      I_location        IN       REPL_ATTR_UPDATE_EXCLUDE.LOCATION%TYPE
                                      )
RETURN BOOLEAN IS
   cursor c_raue is
   select raue.*
    from  repl_attr_update_exclude raue
   where  raue.repl_attr_id  = I_repl_id
     and  raue.item          = I_item
     and  raue.location      = I_location;
   L_program         VARCHAR2(60) := 'REPL_ATTRIBUTE_MAINTENANCE_SQL.GET_REPL_ATTR_UPDATE_EXCLUDE';
BEGIN
   open  c_raue;
   fetch c_raue into O_row;
   close c_raue;
   if O_row.repl_attr_id is null then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_REPL_ATTR_UPDATE_EXCLUDE;
------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEM_LIST( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                            O_exist                 IN OUT   VARCHAR2,
                            O_sched_updates_exist   IN OUT   BOOLEAN
                          )
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'REPL_ATTRIBUTE_MAINTENANCE_SQL.EXPLODE_ITEM_LIST';

   L_repl_attr_id    REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE;

   L_dup_rec_exist   VARCHAR2(1)  := NULL;
   L_locn_exists     VARCHAR2(1)  := 'N';

   cursor C_CHECK_DUP_RECS is
      select 'x'
        from repl_attr_update_loc  l,
             repl_attr_update_item i,
             repl_attr_update_head h
       where h.repl_attr_id          = L_repl_attr_id
         and h.scheduled_active_date = LP_sch_active_date
         and i.item is not null
         and i.repl_attr_id          = h.repl_attr_id
         and l.loc_type is not null
         and l.repl_attr_id          = i.repl_attr_id
         and exists ( select 'x'
                        from repl_attr_update_loc  l2,
                             repl_attr_update_item i2,
                             repl_attr_update_head h2
                       where h2.repl_attr_id          != h.repl_attr_id
                         and h2.scheduled_active_date  = h.scheduled_active_date
                         and i2.item                   = i.item
                         and i2.repl_attr_id           = h2.repl_attr_id
                         and l2.loc_type is not null
                         and l2.loc                    = l.loc
                         and l2.repl_attr_id           = i2.repl_attr_id
                         and rownum                    = 1 )
         and rownum = 1;

BEGIN
   if GET_NEXT_REPL_ID( O_error_message ,
                        L_repl_attr_id ) = FALSE then
      return FALSE;
   end if;

   insert into repl_attr_update_head (repl_attr_id,
                                      scheduled_active_date,
                                      action,
                                      mra_update,
                                      mra_restore,
                                      repl_method_ind,
                                      stock_cat,
                                      repl_order_ctrl,
                                      sourcing_wh,
                                      activate_date,
                                      deactivate_date,
                                      pres_stock,
                                      demo_stock,
                                      repl_method,
                                      min_stock,
                                      max_stock,
                                      incr_pct,
                                      min_supply_days,
                                      max_supply_days,
                                      time_supply_horizon,
                                      inv_selling_days,
                                      service_level,
                                      lost_sales_factor,
                                      reject_store_ord_ind,
                                      non_scaling_ind,
                                      max_scale_value,
                                      pickup_lead_time,
                                      wh_lead_time,
                                      terminal_stock_qty,
                                      season_id,
                                      phase_id,
                                      supplier,
                                      origin_country_id,
                                      review_cycle,
                                      update_days_ind,
                                      monday_ind,
                                      tuesday_ind,
                                      wednesday_ind,
                                      thursday_ind,
                                      friday_ind,
                                      saturday_ind,
                                      sunday_ind,
                                      unit_tolerance,
                                      pct_tolerance,
                                      default_pack_ind,
                                      remove_pack_ind,
                                      use_tolerance_ind,
                                      service_level_type,
                                      size_profile_ind,
                                      create_date,
                                      create_id,
                                      sch_rpl_desc,
                                      mult_runs_per_day_ind,
                                      tsf_zero_soh_ind,
                                      add_lead_time_ind)
                              values (L_repl_attr_id,
                                      LP_sch_active_date,
                                      LP_action,
                                      LP_mra_update,
                                      LP_mra_restore,
                                      LP_repl_method_update,
                                      LP_stock_cat,
                                      LP_repl_order_ctrl,
                                      LP_source_wh,
                                      LP_activate_date,
                                      LP_deactivate_date,
                                      LP_pres_stock,
                                      LP_demo_stock,
                                      LP_repl_method,
                                      LP_min_stock,
                                      LP_max_stock,
                                      LP_incr_pct,
                                      LP_min_supply_days,
                                      LP_max_supply_days,
                                      LP_time_supply_horizon,
                                      LP_inv_selling_days,
                                      LP_service_level,
                                      LP_lost_sales_factor,
                                      LP_rej_st_ord_ind,
                                      LP_non_scaling_ind,
                                      LP_max_scale_value,
                                      LP_pickup_lead_time,
                                      LP_wh_lead_time,
                                      LP_terminal_stock_qty,
                                      LP_season_id,
                                      LP_phase_id,
                                      LP_supplier,
                                      LP_origin_country_id,
                                      LP_review_cycle,
                                      LP_update_days,
                                      NVL(LP_monday, 'N'),
                                      NVL(LP_tuesday, 'N'),
                                      NVL(LP_wednesday, 'N'),
                                      NVL(LP_thursday, 'N'),
                                      NVL(LP_friday, 'N'),
                                      NVL(LP_saturday, 'N'),
                                      NVL(LP_sunday, 'N'),
                                      LP_unit_tolerance,
                                      LP_pct_tolerance,
                                      LP_default_pack_ind,
                                      LP_remove_pack_ind,
                                      LP_use_tolerance_ind,
                                      LP_service_level_type,
                                      LP_size_profile_ind,
                                      get_vdate,
                                      GET_USER,
                                      LP_sch_repl_desc,
                                      NVL(LP_multiple_runs_per_day_ind, 'N'),
                                      NVL(LP_tsf_zero_soh_ind, 'N'),
                                      LP_add_lead_time_ind);

   -- Insert the replenishment attributes for each item in the item list into the
   -- repl_attr_update_item table.

   insert into repl_attr_update_item
      (
        repl_attr_id ,
        item
      ) select
               L_repl_attr_id ,
               sd.item
          from skulist_detail sd,
               item_supplier its,
               item_master im
         where TO_CHAR( sd.skulist ) = LP_item
           and its.item              = sd.item
           and sd.item               = im.item
           and (    (     its.supplier = LP_supplier
                      and LP_supplier is not NULL )
                 or (     LP_supplier is NULL
                      and its.primary_supp_ind = 'Y' ) )
           and im.pack_ind           = 'N'
           and its.consignment_rate is NULL
           and (    im.item_parent  is NULL
                 or not exists ( select 'x'
                                   from skulist_detail sd2
                                  where sd2.skulist = sd.skulist
                                    and sd2.item    = im.item_parent ) )
           and (    im.item_grandparent is NULL
                 or not exists ( select 'x'
                                   from skulist_detail sd2
                                  where sd2.skulist = sd.skulist
                                    and sd2.item    = im.item_grandparent ) )
           and not exists ( select 'x'
                              from item_master im2
                             where im2.item          = im.item
                               and im2.inventory_ind = 'N' );

   if SQL%FOUND then

      if LP_group_type = 'S' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     LP_value
                from store
               where store            = LP_value
                 and stockholding_ind = 'Y';

         L_locn_exists := 'Y';

      elsif LP_group_type = 'C' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store s
               where s.store_class      = LP_value
                 and s.stockholding_ind = 'Y';

         if SQL%FOUND then
            L_locn_exists:='Y';
         end if;

      elsif LP_group_type = 'D' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store_hierarchy sh ,
                     store s
               where sh.district        = LP_value
                 and sh.store           = s.store
                 and s.stockholding_ind = 'Y';

         if SQL%FOUND then
            L_locn_exists:='Y';
         end if;

      elsif LP_group_type = 'R' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store_hierarchy sh ,
                     store s
               where sh.region          = LP_value
                 and sh.store           = s.store
                 and s.stockholding_ind = 'Y';

         if SQL%FOUND then
            L_locn_exists:='Y';
         end if;


      elsif LP_group_type = 'T' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store s
               where s.transfer_zone    = LP_value
                 and s.stockholding_ind = 'Y';

         if SQL%FOUND then
            L_locn_exists:='Y';
         end if;

      elsif LP_group_type = 'L' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     ltm.store
                from loc_traits_matrix ltm ,
                     store s
               where ltm.loc_trait      = LP_value
                 and ltm.store          = s.store
                 and s.stockholding_ind = 'Y';

         if SQL%FOUND then
            L_locn_exists:='Y';
         end if;

      elsif LP_group_type = 'DW' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store s
               where s.default_wh       = LP_value
                 and s.stockholding_ind = 'Y';

         if SQL%FOUND then
            L_locn_exists:='Y';
         end if;

      elsif LP_group_type = 'AS' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store s
               where s.stockholding_ind = 'Y';

         if SQL%FOUND then
            L_locn_exists:='Y';
         end if;

      elsif LP_group_type =  'W' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'W'            ,
                     w.wh
                from wh w
               where w.wh               = LP_value
                 and w.stockholding_ind = 'Y'
                 and w.repl_ind         = 'Y';

         if SQL%FOUND then
            L_locn_exists:='Y';
         end if;

      elsif LP_group_type = 'AW' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'W'            ,
                     wh.wh
                from wh
               where stockholding_ind = 'Y'
                 and repl_ind         = 'Y'
                 and finisher_ind     = 'N';

         if SQL%FOUND then
            L_locn_exists:='Y';
         end if;

      elsif LP_group_type = 'LLS' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     l.loc_type     ,
                     l.location
                from loc_list_detail l ,
                     store s
               where l.loc_list         = LP_value
                 and l.location         = s.store
                 and s.stockholding_ind = 'Y'
                 and l.loc_type         = 'S';

         if SQL%FOUND then
            L_locn_exists := 'Y';
         end if;

      elsif LP_group_type = 'LLW' then

         insert into repl_attr_update_loc
            ( loc          ,
              repl_attr_id ,
              loc_type
            ) select distinct w.wh  ,
                     L_repl_attr_id ,
                     l.loc_type
                from loc_list_detail l ,
                     wh w
               where (     l.loc_list = LP_value
                       and l.location = w.wh
                       and w.stockholding_ind = 'Y'
                       and w.repl_ind = 'Y' )
                  or (     l.loc_list = LP_value
                       and w.physical_wh = l.location
                       and w.stockholding_ind = 'Y'
                       and w.repl_ind = 'Y' )
                 and finisher_ind = 'N';
         ---
         if SQL%FOUND then
            L_locn_exists := 'Y';
         end if;
         ---
      elsif LP_group_type = 'A' then

         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store_hierarchy sh ,
                     store s
               where sh.area            = LP_value
                 and sh.store           = s.store
                 and s.stockholding_ind = 'Y';
         ---
         if SQL%FOUND then
            L_locn_exists:='Y';
         end if;
      end if;

      -- If no locations where written then delete the items from repl_attr_update_item

      if L_locn_exists = 'N' then
         delete from repl_attr_update_item
          where repl_attr_id = L_repl_attr_id;
      end if;

      O_exist := L_locn_exists;

      SQL_LIB.SET_MARK( 'OPEN' ,
                        'C_CHECK_DUP_RECS' ,
                        NULL ,
                        NULL );
      open C_CHECK_DUP_RECS;

      SQL_LIB.SET_MARK( 'FETCH' ,
                        'C_CHECK_DUP_RECS' ,
                        NULL ,
                        NULL );
      fetch C_CHECK_DUP_RECS into L_dup_rec_exist;

      SQL_LIB.SET_MARK( 'CLOSE',
                        'C_CHECK_DUP_RECS' ,
                        NULL ,
                        NULL );
      close C_CHECK_DUP_RECS;

      if L_dup_rec_exist = 'x' then
         O_sched_updates_exist := TRUE;
      else
         O_sched_updates_exist := FALSE;
      end if;

   else
      O_exist := 'N';
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_ITEM_LIST;
------------------------------------------------------------------------------------------------------------
FUNCTION ITEM_LOCNS_DEACTIVATE (O_error_message   IN OUT VARCHAR2,
                                I_item            IN     repl_item_loc.item%TYPE)
   RETURN BOOLEAN IS

   L_table          VARCHAR2(100)             := Null;
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_RILU_STORE is
      select 'x'
        from repl_item_loc_updates
       where item = I_item
         and location = LP_value
         for update nowait;

   cursor C_LOCK_REPL_DAY_STORE is
      select 'x'
        from repl_day
       where item = I_item
         and location = LP_value
         for update nowait;

   cursor C_LOCK_STORE_ORDERS_STORE is
      select 'x'
        from store_orders
       where item = I_item
         and store = LP_value
         for update nowait;

   cursor C_LOCK_REPL_ITEM_STORE is
      select 'x'
        from repl_item_loc
       where item = I_item
         and location = LP_value
         for update nowait;

   cursor C_LOCK_RILU_STORE_CLASS is
      select 'x'
        from repl_item_loc_updates ru
       where ru.item = I_item
         and exists
             (select 'x'
                from store s
               where s.store_class = LP_value
                 and ru.location = s.store)
         for update nowait;

   cursor C_LOCK_REPL_DAY_STORE_CLASS is
      select 'x'
        from repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store s
               where s.store_class = LP_value
                 and rd.location = s.store)
         for update nowait;

   cursor C_LOCK_SO_STORE_CLASS is
      select 'x'
        from store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store s
               where s.store_class = LP_value
                 and so.store = s.store)
         for update nowait;

   cursor C_LOCK_REPL_ITEM_STORE_CLASS is
      select 'x'
        from repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store s
               where s.store_class = LP_value
                 and ril.location = s.store)
         for update nowait;

   cursor C_LOCK_RILU_DISTRICT is
      select 'x'
        from repl_item_loc_updates ru
       where ru.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.district = LP_value
                 and ru.location = sh.store)
         for update nowait;

   cursor C_LOCK_REPL_DAY_DISTRICT is
      select 'x'
        from repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.district = LP_value
                 and sh.store = rd.location)
         for update nowait;

   cursor C_LOCK_STORE_ORDERS_DISTRICT is
      select 'x'
        from store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.district = LP_value
                 and sh.store = so.store)
         for update nowait;

   cursor C_LOCK_REPL_ITEM_DISTRICT is
      select 'x'
        from repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.district = LP_value
                 and sh.store = ril.location)
         for update nowait;

   cursor C_LOCK_RILU_REGION is
      select 'x'
        from repl_item_loc_updates ru
       where ru.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.region = LP_value
                 and ru.location = sh.store)
         for update nowait;

   cursor C_LOCK_REPL_DAY_REGION is
      select 'x'
        from repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.region = LP_value
                 and rd.location = sh.store)
         for update nowait;

   cursor C_LOCK_STORE_ORDERS_REGION is
      select 'x'
        from store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.region = LP_value
                 and so.store = sh.store)
         for update nowait;

   cursor C_LOCK_REPL_ITEM_REGION is
      select 'x'
        from repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.region = LP_value
                 and ril.location = sh.store)
         for update nowait;


   cursor C_LOCK_RILU_TRANSFER_ZONE is
      select 'x'
        from repl_item_loc_updates ru
       where ru.item = I_item
         and exists
             (select 'x'
                from store s
               where s.transfer_zone = LP_value
                 and s.store = ru.location)
         for update nowait;

   cursor C_LOCK_REPL_DAY_TRANSFER_ZONE is
      select 'x'
        from repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store s
               where s.transfer_zone = LP_value
                 and s.store = rd.location)
         for update nowait;

   cursor C_LOCK_SO_TRANSFER_ZONE is
      select 'x'
        from store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store s
               where s.transfer_zone = LP_value
                 and s.store = so.store)
         for update nowait;

   cursor C_LOCK_REPL_ITEM_TRANSFER_ZONE is
      select 'x'
        from repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store s
               where s.transfer_zone = LP_value
                 and s.store = ril.location)
         for update nowait;

   cursor C_LOCK_RILU_LOC_TRAIT is
      select 'x'
        from repl_item_loc_updates ru
       where ru.item = I_item
         and exists
             (select 'x'
                from loc_traits_matrix l
               where l.loc_trait = LP_value
                 and l.store = ru.location)
         for update nowait;

   cursor C_LOCK_REPL_DAY_LOC_TRAIT is
      select 'x'
        from repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from loc_traits_matrix l
               where l.loc_trait = LP_value
                 and l.store = rd.location)
         for update nowait;

   cursor C_LOCK_STORE_ORDERS_LOC_TRAIT is
      select 'x'
        from store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from loc_traits_matrix l
               where l.loc_trait = LP_value
                 and l.store = so.store)
         for update nowait;

   cursor C_LOCK_REPL_ITEM_LOC_TRAIT is
      select 'x'
        from repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from loc_traits_matrix l
               where l.loc_trait = LP_value
                 and l.store = ril.location)
         for update nowait;

   cursor C_LOCK_RILU_DEFAULT_WH is
      select 'x'
        from repl_item_loc_updates ru
       where ru.item = I_item
         and exists
             (select 'x'
                from store s
               where s.default_wh = LP_value
                 and s.store = ru.location)
         for update nowait;

   cursor C_LOCK_REPL_DAY_DEFAULT_WH is
      select 'x'
        from repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store s
               where s.default_wh = LP_value
                 and s.store = rd.location)
         for update nowait;

   cursor C_LOCK_STORE_ORDERS_DEFAULT_WH is
      select 'x'
        from store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store s
               where s.default_wh = LP_value
                 and s.store = so.store)
         for update nowait;

   cursor C_LOCK_REPL_ITEM_DEFAULT_WH is
      select 'x'
        from repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store s
               where s.default_wh = LP_value
                 and s.store = ril.location)
         for update nowait;

   cursor C_LOCK_RILU_ALL_STORES is
      select 'x'
        from repl_item_loc_updates
       where item = I_item
         and loc_type = 'S'
         for update nowait;

   cursor C_LOCK_REPL_DAY_ALL_STORES is
      select 'x'
        from repl_day
       where item = I_item
         and loc_type = 'S'
         for update nowait;

   cursor C_LOCK_SO_ALL_STORES is
      select 'x'
        from store_orders
       where item     = I_item
         for update nowait;

   cursor C_LOCK_REPL_ITEM_ALL_STORES is
      select 'x'
        from repl_item_loc
       where item     = I_item
         and loc_type = 'S'
         for update nowait;

   cursor C_LOCK_RILU_WAREHOUSE is
      select 'x'
        from repl_item_loc_updates
       where item = I_item
         and location = LP_value
         for update nowait;

   cursor C_LOCK_REPL_DAY_WAREHOUSE is
      select 'x'
        from repl_day
       where item = I_item
         and location = LP_value
         for update nowait;

   cursor C_LOCK_REPL_ITEM_WAREHOUSE is
      select 'x'
        from repl_item_loc
       where item = I_item
         and location = LP_value
         for update nowait;

   cursor C_LOCK_RILU_ALL_WAREHOUSE is
      select 'x'
        from repl_item_loc_updates
       where item = I_item
         and loc_type = 'W'
         for update nowait;

   cursor C_LOCK_REPL_DAY_ALL_WAREHOUSE is
      select 'x'
        from repl_day
       where item = I_item
         and loc_type = 'W'
         for update nowait;

   cursor C_LOCK_REPL_ITEM_ALL_WAREHOUSE is
      select 'x'
        from repl_item_loc
       where item = I_item
         and loc_type = 'W'
         for update nowait;

   cursor C_LOCK_RILU_LOCLIST_ST is
      select 'x'
        from repl_item_loc_updates ru
       where ru.item = I_item
         and exists (select 'x'
                       from loc_list_detail lld
                      where ru.location = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'S')
         for update nowait;

   cursor C_LOCK_REPL_DAY_LOCLIST_ST is
      select 'x'
        from repl_day rd
       where rd.item = I_item
         and exists (select 'x'
                       from loc_list_detail lld
                      where rd.location = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'S')
         for update nowait;

   cursor C_LOCK_STORE_ORDERS_LOCLIST_ST is
      select 'x'
        from store_orders so
       where so.item = I_item
         and exists (select 'x'
                       from loc_list_detail lld
                      where so.store = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'S')
         for update nowait;

   cursor C_LOCK_REPL_ITEM_LOCLIST_ST is
      select 'x'
        from repl_item_loc ril
       where ril.item = I_item
         and exists (select 'x'
                       from loc_list_detail lld
                      where ril.location = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'S')
         for update nowait;

   cursor C_LOCK_RILU_LOCLIST_WH is
      select 'x'
        from repl_item_loc_updates ru
       where ru.item = I_item
         and exists (select 'x'
                       from loc_list_detail lld
                      where ru.location = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'W')
         for update nowait;

   cursor C_LOCK_REPL_DAY_LOCLIST_WH is
      select 'x'
        from repl_day rd
       where rd.item = I_item
         and exists (select 'x'
                       from loc_list_detail lld
                      where rd.location = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'W')
         for update nowait;

   cursor C_LOCK_REPL_ITEM_LOCLIST_WH is
      select 'x'
        from repl_item_loc ril
       where ril.item = I_item
         and exists (select 'x'
                       from loc_list_detail lld
                      where ril.location = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'W')
         for update nowait;

   cursor C_LOCK_RILU_AREA is
      select 'x'
        from repl_item_loc_updates ru
       where ru.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.area = LP_value
                 and ru.location = sh.store)
         for update nowait;

   cursor C_LOCK_REPL_DAY_AREA is
      select 'x'
        from repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.area = LP_value
                 and rd.location = sh.store)
         for update nowait;

   cursor C_LOCK_STORE_ORDERS_AREA is
      select 'x'
        from store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.area = LP_value
                 and so.store = sh.store)
         for update nowait;

   cursor C_LOCK_REPL_ITEM_AREA is
      select 'x'
        from repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.area = LP_value
                 and ril.location = sh.store)
         for update nowait;

BEGIN
   -- DEACTIVATE REPLENISHMENT
   -- If the item location was on replenishment, remove the records from the
   -- replenishment item/location table.

   if LP_group_type = 'S' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_STORE;
      close C_LOCK_RILU_STORE;
      ---
      delete repl_item_loc_updates
       where item = I_item
         and location = LP_value;
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         values(I_item,
                Null,
                Null,
                LP_value,
                'S',
                'RILD');
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_STORE;
      close C_LOCK_REPL_DAY_STORE;
      ---
      delete repl_day
       where item = I_item
         and location = LP_value;
      ---
      L_table := 'STORE_ORDERS';
      open  C_LOCK_STORE_ORDERS_STORE;
      close C_LOCK_STORE_ORDERS_STORE;
      ---
      delete store_orders
       where item = I_item
         and store = LP_value;
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_STORE;
      close C_LOCK_REPL_ITEM_STORE;
      ---
      delete repl_item_loc
       where item = I_item
         and location = LP_value;
      ---
      delete master_repl_attr mra
       where mra.item = I_item
         and mra.location = LP_value;

   elsif LP_group_type = 'C' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_STORE_CLASS;
      close C_LOCK_RILU_STORE_CLASS;
      ---
      delete repl_item_loc_updates ru
       where ru.item = I_item
         and ru.change_type != 'RILD'
         and exists
             (select 'x'
                from store s
               where s.store_class = LP_value
                 and s.store = ru.location);
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                ril.location,
                'S',
                'RILD'
           from store s,
                repl_item_loc ril
          where s.store_class = LP_value
            and s.store = ril.location
            and ril.item = I_item;
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_STORE_CLASS;
      close C_LOCK_REPL_DAY_STORE_CLASS;
      ---
      delete repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store s
               where s.store_class = LP_value
                 and s.store = rd.location);
      ---
      L_table := 'STORE_ORDERS';
      open  C_LOCK_SO_STORE_CLASS;
      close C_LOCK_SO_STORE_CLASS;
      ---
      delete store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store s
               where s.store_class = LP_value
                 and s.store = so.store);
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_STORE_CLASS;
      close C_LOCK_REPL_ITEM_STORE_CLASS;
      ---
      delete repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store s
               where s.store_class = LP_value
                 and s.store = ril.location);
      ---
      delete master_repl_attr mra
       where mra.item = I_item
         and exists
             (select 'x'
                from store s
               where s.store_class = LP_value
                 and s.store = mra.location);

   elsif LP_group_type = 'D' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_DISTRICT;
      close C_LOCK_RILU_DISTRICT;
      ---
      delete repl_item_loc_updates ru
       where ru.item = I_item
         and ru.change_type != 'RILD'
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.district = LP_value
                 and ru.location = sh.store);
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                ril.location,
                'S',
                'RILD'
           from store_hierarchy sh,
                repl_item_loc ril
          where sh.district = LP_value
            and sh.store = ril.location
            and ril.item = I_item;
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_DISTRICT;
      close C_LOCK_REPL_DAY_DISTRICT;
      ---
      delete repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.district = LP_value
                 and rd.location = sh.store);
      ---
      L_table := 'STORE_ORDERS';
      open  C_LOCK_STORE_ORDERS_DISTRICT;
      close C_LOCK_STORE_ORDERS_DISTRICT;
      ---
      delete store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.district = LP_value
                 and so.store = sh.store);
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_DISTRICT;
      close C_LOCK_REPL_ITEM_DISTRICT;
      ---
      delete repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.district = LP_value
                 and ril.location = sh.store);
      ---
      delete master_repl_attr mra
       where mra.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.district = LP_value
                 and mra.location = sh.store);
      ---
   elsif LP_group_type = 'R' then
      ---      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_REGION;
      close C_LOCK_RILU_REGION;
      ---
      delete repl_item_loc_updates ru
       where ru.item = I_item
         and ru.change_type != 'RILD'
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.region = LP_value
                 and ru.location = sh.store);
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                ril.location,
                'S',
                'RILD'
           from store_hierarchy sh,
                repl_item_loc ril
          where sh.region = LP_value
            and sh.store = ril.location
            and ril.item = I_item;
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_REGION;
      close C_LOCK_REPL_DAY_REGION;
      ---
      delete repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.region = LP_value
                 and rd.location = sh.store);
      ---
      L_table := 'STORE_ORDERS';
      open  C_LOCK_STORE_ORDERS_REGION;
      close C_LOCK_STORE_ORDERS_REGION;
      ---
      delete store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.region = LP_value
                 and so.store = sh.store);
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_REGION;
      close C_LOCK_REPL_ITEM_REGION;
      ---
      delete repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.region = LP_value
                 and ril.location = sh.store);
      ---
      delete master_repl_attr mra
      where mra.item = I_item
        and exists
            (select 'x'
             from store_hierarchy sh
             where sh.region = LP_value
               and mra.location = sh.store);
      ---

   elsif LP_group_type = 'T' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_TRANSFER_ZONE;
      close C_LOCK_RILU_TRANSFER_ZONE;
      ---
      delete repl_item_loc_updates ru
       where ru.item = I_item
         and ru.change_type != 'RILD'
         and exists
             (select 'x'
                from store s
               where s.transfer_zone = LP_value
                 and s.store = ru.location);
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                ril.location,
                'S',
                'RILD'
           from store s,
                repl_item_loc ril
          where s.transfer_zone = LP_value
            and s.store = ril.location
            and ril.item = I_item;
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_TRANSFER_ZONE;
      close C_LOCK_REPL_DAY_TRANSFER_ZONE;
      ---
      delete repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store s
               where s.transfer_zone = LP_value
                 and s.store = rd.location);
      ---
      L_table := 'STORE_ORDERS';
      open  C_LOCK_SO_TRANSFER_ZONE;
      close C_LOCK_SO_TRANSFER_ZONE;
      ---
      delete store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store s
               where s.transfer_zone = LP_value
                 and s.store = so.store);
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_TRANSFER_ZONE;
      close C_LOCK_REPL_ITEM_TRANSFER_ZONE;
      ---
      delete repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store s
               where s.transfer_zone = LP_value
                 and s.store = ril.location);
      ---
      delete master_repl_attr mra
       where mra.item = I_item
         and exists
             (select 'x'
                from store s
               where s.transfer_zone = LP_value
                 and s.store = mra.location);
      ---
   elsif LP_group_type = 'L' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_LOC_TRAIT;
      close C_LOCK_RILU_LOC_TRAIT;
      ---
      delete repl_item_loc_updates ru
       where ru.item = I_item
         and ru.change_type != 'RILD'
         and exists
             (select 'x'
                from loc_traits_matrix l
               where l.loc_trait = LP_value
                 and l.store = ru.location);
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                ril.location,
                'S',
                'RILD'
           from loc_traits_matrix l,
                repl_item_loc ril
          where l.loc_trait = LP_value
            and l.store = ril.location
            and ril.item = I_item;
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_LOC_TRAIT;
      close C_LOCK_REPL_DAY_LOC_TRAIT;
      ---
      delete repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from loc_traits_matrix l
               where l.loc_trait = LP_value
                 and l.store = rd.location);
      ---
      L_table := 'STORE_ORDERS';
      open  C_LOCK_STORE_ORDERS_LOC_TRAIT;
      close C_LOCK_STORE_ORDERS_LOC_TRAIT;
      ---
      delete store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from loc_traits_matrix l
               where l.loc_trait = LP_value
                 and l.store = so.store);
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_LOC_TRAIT;
      close C_LOCK_REPL_ITEM_LOC_TRAIT;
      ---
      delete repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from loc_traits_matrix l
               where l.loc_trait = LP_value
                 and l.store = ril.location);
      ---
      delete master_repl_attr mra
       where mra.item = I_item
         and exists
             (select 'x'
                from loc_traits_matrix l
               where l.loc_trait = LP_value
                 and l.store = mra.location);
      ---
   elsif LP_group_type = 'DW' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_DEFAULT_WH;
      close C_LOCK_RILU_DEFAULT_WH;
      ---
      delete repl_item_loc_updates ru
       where ru.item = I_item
         and ru.change_type != 'RILD'
         and exists
             (select 'x'
                from store s
               where s.default_wh = LP_value
                 and s.store = ru.location);
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                ril.location,
                'S',
                'RILD'
           from store s,
                repl_item_loc ril
          where s.default_wh = LP_value
            and s.store = ril.location
            and ril.item = I_item;
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_DEFAULT_WH;
      close C_LOCK_REPL_DAY_DEFAULT_WH;
      ---
      delete repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store s
               where s.default_wh = LP_value
                 and s.store = rd.location);
      ---
      L_table := 'STORE_ORDERS';
      open  C_LOCK_STORE_ORDERS_DEFAULT_WH;
      close C_LOCK_STORE_ORDERS_DEFAULT_WH;
      ---
      delete store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store s
               where s.default_wh = LP_value
                 and s.store = so.store);
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_DEFAULT_WH;
      close C_LOCK_REPL_ITEM_DEFAULT_WH;
      ---
      delete repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store s
               where s.default_wh = LP_value
                 and s.store = ril.location);
      ---
      delete master_repl_attr mra
      where mra.item = I_item
        and exists
            (select 'x'
               from store s
              where s.default_wh = LP_value
                and s.store = mra.location);
      ---
   elsif LP_group_type = 'AS' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_ALL_STORES;
      close C_LOCK_RILU_ALL_STORES;
      ---
      delete repl_item_loc_updates
       where item = I_item
         and change_type != 'RILD'
         and loc_type = 'S';
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                location,
                'S',
                'RILD'
           from repl_item_loc
          where item = I_item
            and loc_type = 'S';
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_ALL_STORES;
      close C_LOCK_REPL_DAY_ALL_STORES;
      ---
      delete repl_day
       where item = I_item
         and loc_type = 'S';
      ---
      L_table := 'STORE_ORDERS';
      open  C_LOCK_SO_ALL_STORES;
      close C_LOCK_SO_ALL_STORES;
      ---
      delete store_orders
       where item  = I_item;
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_ALL_STORES;
      close C_LOCK_REPL_ITEM_ALL_STORES;
      ---
      delete repl_item_loc
       where item  = I_item
         and loc_type = 'S';
      ---
      delete master_repl_attr mra
       where mra.item  = I_item
         and mra.loc_type = 'S';
      ---
   elsif LP_group_type =  'W' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_WAREHOUSE;
      close C_LOCK_RILU_WAREHOUSE;
      ---
      delete repl_item_loc_updates
       where item = I_item
         and location = LP_value;
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         values(I_item,
                Null,
                Null,
                LP_value,
                'W',
                'RILD');
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_WAREHOUSE;
      close C_LOCK_REPL_DAY_WAREHOUSE;
      ---
      delete repl_day
       where item     = I_item
         and location = LP_value;
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_WAREHOUSE;
      close C_LOCK_REPL_ITEM_WAREHOUSE;
      ---
      delete repl_item_loc
       where item     = I_item
         and loc_type = 'W'
         and location = LP_value;
      ---
      delete master_repl_attr
       where item     = I_item
         and loc_type = 'W'
         and location = LP_value;
      ---
   elsif LP_group_type = 'AW' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_ALL_WAREHOUSE;
      close C_LOCK_RILU_ALL_WAREHOUSE;
      ---
      delete repl_item_loc_updates
       where item = I_item
         and change_type != 'RILD'
         and loc_type = 'W';
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                location,
                'W',
                'RILD'
           from repl_item_loc
          where item = I_item
            and loc_type = 'W';
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_ALL_WAREHOUSE;
      close C_LOCK_REPL_DAY_ALL_WAREHOUSE;
      ---
      delete repl_day
       where item     = I_item
         and loc_type = 'W';
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_ALL_WAREHOUSE;
      close C_LOCK_REPL_ITEM_ALL_WAREHOUSE;
      ---
      delete repl_item_loc
       where item     = I_item
         and loc_type = 'W';
      ---
      delete master_repl_attr
       where item     = I_item
         and loc_type = 'W';
      ---
   elsif LP_group_type = 'LLS' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_LOCLIST_ST;
      close C_LOCK_RILU_LOCLIST_ST;
      ---
      delete repl_item_loc_updates ru
       where ru.item = I_item
         and ru.change_type != 'RILD'
         and exists (select lld.location
                       from loc_list_detail lld
                      where ru.location  = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'S');
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                ril.location,
                'S',
                'RILD'
           from repl_item_loc ril,
                loc_list_detail lld
          where ril.item = I_item
            and lld.loc_list = LP_value
            and lld.location = ril.location
            and lld.loc_type = 'S';
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_LOCLIST_ST;
      close C_LOCK_REPL_DAY_LOCLIST_ST;
      ---
      delete repl_day rd
       where rd.item = I_item
         and exists (select lld.location
                       from loc_list_detail lld
                      where rd.location  = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'S');
      ---
      L_table := 'STORE_ORDERS';
      open  C_LOCK_STORE_ORDERS_LOCLIST_ST;
      close C_LOCK_STORE_ORDERS_LOCLIST_ST;
      ---
      delete store_orders so
       where so.item = I_item
         and exists (select lld.location
                       from loc_list_detail lld
                      where so.store = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'S');
      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_LOCLIST_ST;
      close C_LOCK_REPL_ITEM_LOCLIST_ST;
      ---
      delete repl_item_loc ril
       where ril.item = I_item
         and exists (select lld.location
                       from loc_list_detail lld
                      where ril.location = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'S');
      ---
      delete master_repl_attr mra
       where mra.item = I_item
         and exists (select lld.location
                       from loc_list_detail lld
                      where mra.location = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'S');
      ---
   elsif LP_group_type = 'LLW' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';
      open  C_LOCK_RILU_LOCLIST_WH;
      close C_LOCK_RILU_LOCLIST_WH;
      ---
      delete repl_item_loc_updates ru
       where ru.item = I_item
         and ru.change_type != 'RILD'
         and exists (select lld.location
                       from loc_list_detail lld
                      where ru.location  = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'W');
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                ril.location,
                'W',
                'RILD'
           from repl_item_loc ril,
                loc_list_detail lld
          where ril.item = I_item
            and lld.loc_list = LP_value
            and lld.location = ril.location
            and lld.loc_type = 'W';
      ---
      L_table := 'REPL_DAY';
      open  C_LOCK_REPL_DAY_LOCLIST_WH;
      close C_LOCK_REPL_DAY_LOCLIST_WH;
      ---
      delete repl_day rd
       where rd.item = I_item
         and exists (select lld.location
                       from loc_list_detail lld
                      where rd.location  = lld.location
                        and lld.loc_type = 'W'
                        and lld.loc_list = LP_Value);

      ---
      L_table := 'REPL_ITEM_LOC';
      open  C_LOCK_REPL_ITEM_LOCLIST_WH;
      close C_LOCK_REPL_ITEM_LOCLIST_WH;
      ---
      delete repl_item_loc ril
       where ril.item = I_item
         and exists (select lld.location
                       from loc_list_detail lld
                      where ril.location = lld.location
                        and lld.loc_type = 'W'
                        and lld.loc_list = LP_Value);
      ---
      delete master_repl_attr mra
       where mra.item = I_item
         and exists (select lld.location
                       from loc_list_detail lld
                      where mra.location = lld.location
                        and lld.loc_list = LP_value
                        and lld.loc_type = 'W');
      ---
   elsif LP_group_type = 'A' then
      ---
      L_table := 'REPL_ITEM_LOC_UPDATES';

      SQL_LIB.SET_MARK ('OPEN',
                        'C_LOCK_RILU_AREA',
                        'REPL_ITEM_LOC_UPDATES',
                        LP_value);
      open  C_LOCK_RILU_AREA;

      SQL_LIB.SET_MARK ('CLOSE',
                        'C_LOCK_RILU_AREA',
                        'REPL_ITEM_LOC_UPDATES',
                        LP_value);
      close C_LOCK_RILU_AREA;
      ---
      SQL_LIB.SET_MARK ('DELETE',
                        'C_LOCK_RILU_AREA',
                        'REPL_ITEM_LOC_UPDATES',
                        LP_value);
      delete repl_item_loc_updates ru
       where ru.item = I_item
         and ru.change_type != 'RILD'
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.area = LP_value
                 and ru.location = sh.store);
      ---
      insert into repl_item_loc_updates
               (item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                change_type)
         select I_item,
                Null,
                Null,
                ril.location,
                'S',
                'RILD'
           from store_hierarchy sh,
                repl_item_loc ril
          where sh.area = LP_value
            and sh.store = ril.location
            and ril.item = I_item;
      ---
      L_table := 'REPL_DAY';

      SQL_LIB.SET_MARK ('OPEN',
                        'C_LOCK_REPL_DAY_AREA',
                        'REPL_DAY',
                        LP_value);
      open  C_LOCK_REPL_DAY_AREA;

      SQL_LIB.SET_MARK ('CLOSE',
                        'C_LOCK_REPL_DAY_AREA',
                        'REPL_DAY',
                        LP_value);
      close C_LOCK_REPL_DAY_AREA;
      ---
      SQL_LIB.SET_MARK ('DELETE',
                        'C_LOCK_REPL_DAY_AREA',
                        'REPL_DAY',
                        LP_value);
      delete repl_day rd
       where rd.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.area = LP_value
                 and rd.location = sh.store);
      ---
      L_table := 'STORE_ORDERS';

      SQL_LIB.SET_MARK ('OPEN',
                        'C_LOCK_STORE_ORDERS_AREA',
                        'STORE_ORDERS',
                        LP_value);
      open  C_LOCK_STORE_ORDERS_AREA;

      SQL_LIB.SET_MARK ('CLOSE',
                        'C_LOCK_STORE_ORDERS_AREA',
                        'STORE_ORDERS',
                        LP_value);
      close C_LOCK_STORE_ORDERS_AREA;
      ---
      SQL_LIB.SET_MARK ('DELETE',
                        'C_LOCK_STORE_ORDERS_AREA',
                        'STORE_ORDERS',
                        LP_value);
      delete store_orders so
       where so.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.area = LP_value
                 and so.store = sh.store);
      ---
      L_table := 'REPL_ITEM_LOC';

      SQL_LIB.SET_MARK ('OPEN',
                        'C_LOCK_REPL_ITEM_AREA',
                        'REPL_ITEM_LOC',
                        LP_value);
      open  C_LOCK_REPL_ITEM_AREA;

      SQL_LIB.SET_MARK ('CLOSE',
                        'C_LOCK_REPL_ITEM_AREA',
                        'REPL_ITEM_LOC',
                        LP_value);
      close C_LOCK_REPL_ITEM_AREA;
      ---
      SQL_LIB.SET_MARK ('DELETE',
                        'C_LOCK_REPL_ITEM_AREA',
                        'REPL_ITEM_LOC',
                        LP_value);
      delete repl_item_loc ril
       where ril.item = I_item
         and exists
             (select 'x'
                from store_hierarchy sh
               where sh.area = LP_value
                 and ril.location = sh.store);
      ---
      SQL_LIB.SET_MARK ('DELETE',
                        'NULL',
                        'MASTER_REPL_ATTR',
                        LP_value);
      delete master_repl_attr mra
      where mra.item = I_item
        and exists
            (select 'x'
             from store_hierarchy sh
             where sh.area = LP_value
               and mra.location = sh.store);
      ---

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            Null);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'REPL_ATTRIBUTE_MAINTENANCE_SQL.ITEM_LOCNS_DEACTIVATE',
                                             to_char(SQLCODE));
      return FALSE;
END ITEM_LOCNS_DEACTIVATE;
-------------------------------------------------------------------------------------------------------------
FUNCTION ITEM_LOCNS_UPDATE(O_error_message  IN OUT VARCHAR2,
                           I_item           IN     repl_item_loc.item%type)
   RETURN BOOLEAN IS

   L_exist                  BOOLEAN;
   L_item                   REPL_ITEM_LOC.ITEM%TYPE    := I_item;
   L_loc_exists             VARCHAR2(1)                := Null;
   L_table                  VARCHAR2(30)               := Null;
   EXIT_REJECT              EXCEPTION;
   RECORD_LOCKED            EXCEPTION;
   L_exists                 BOOLEAN;
   PRAGMA                   EXCEPTION_INIT(Record_Locked, -54);

   cursor C_STORE is
      select location,
             primary_pack_no,
             repl_order_ctrl,
             stock_cat,
             activate_date,
             deactivate_date,
             min_stock,
             max_stock,
             min_supply_days,
             max_supply_days,
             repl_method,
             primary_repl_supplier,
             origin_country_id,
             source_wh,
             rowid
        from repl_item_loc
       where item = L_item
         and location = LP_value
         for update nowait;

   cursor C_STORE_CLASS is
      select ril.location,
             ril.primary_pack_no,
             ril.repl_order_ctrl,
             ril.stock_cat,
             ril.activate_date,
             ril.deactivate_date,
             ril.min_stock,
             ril.max_stock,
             ril.min_supply_days,
             ril.max_supply_days,
             ril.repl_method,
             ril.primary_repl_supplier,
             ril.origin_country_id,
             ril.source_wh,
             ril.rowid
        from repl_item_loc ril,
             v_store s
       where ril.location = s.store
         and ril.item = L_item
         and s.store_class = LP_value
         for update nowait;

   cursor C_DISTRICT is
      select ril.location,
             ril.primary_pack_no,
             ril.repl_order_ctrl,
             ril.stock_cat,
             ril.activate_date,
             ril.deactivate_date,
             ril.min_stock,
             ril.max_stock,
             ril.min_supply_days,
             ril.max_supply_days,
             ril.repl_method,
             ril.primary_repl_supplier,
             ril.origin_country_id,
             ril.source_wh,
             ril.rowid
        from repl_item_loc ril,
             store_hierarchy sh
       where ril.location = sh.store
         and sh.district  = LP_value
         and ril.item     = L_item
         for update nowait;

   cursor C_REGION is
      select ril.location,
             ril.primary_pack_no,
             ril.repl_order_ctrl,
             ril.stock_cat,
             ril.activate_date,
             ril.deactivate_date,
             ril.min_stock,
             ril.max_stock,
             ril.min_supply_days,
             ril.max_supply_days,
             ril.repl_method,
             ril.primary_repl_supplier,
             ril.origin_country_id,
             ril.source_wh,
             ril.rowid
        from repl_item_loc ril,
             store_hierarchy sh
       where ril.location = sh.store
         and sh.region    = LP_value
         and ril.item     = L_item
         for update nowait;


   cursor C_TRANSFER_ZONE is
      select ril.location,
             ril.primary_pack_no,
             ril.repl_order_ctrl,
             ril.stock_cat,
             ril.activate_date,
             ril.deactivate_date,
             ril.min_stock,
             ril.max_stock,
             ril.min_supply_days,
             ril.max_supply_days,
             ril.repl_method,
             ril.primary_repl_supplier,
             ril.origin_country_id,
             ril.source_wh,
             ril.rowid
        from repl_item_loc ril,
             v_store s
       where ril.location    = s.store
         and s.transfer_zone = LP_value
         and ril.item        = L_item
         for update nowait;

   cursor C_LOC_TRAIT is
      select ril.location,
             ril.primary_pack_no,
             ril.repl_order_ctrl,
             ril.stock_cat,
             ril.activate_date,
             ril.deactivate_date,
             ril.min_stock,
             ril.max_stock,
             ril.min_supply_days,
             ril.max_supply_days,
             ril.repl_method,
             ril.primary_repl_supplier,
             ril.origin_country_id,
             ril.source_wh,
             ril.rowid
        from repl_item_loc ril,
             loc_traits_matrix ltm
       where ril.location  = ltm.store
         and ltm.loc_trait = LP_value
         and ril.item      = L_item
         for update nowait;

   cursor C_DEFAULT_WH is
      select ril.location,
             ril.primary_pack_no,
             ril.repl_order_ctrl,
             ril.stock_cat,
             ril.activate_date,
             ril.deactivate_date,
             ril.min_stock,
             ril.max_stock,
             ril.min_supply_days,
             ril.max_supply_days,
             ril.repl_method,
             ril.primary_repl_supplier,
             ril.origin_country_id,
             ril.source_wh,
             ril.rowid
        from repl_item_loc ril,
             v_store s
       where ril.location = s.store
         and ril.item = L_item
         and s.default_wh = LP_value
         for update nowait;

   cursor C_ALL_STORES is
      select ril.location,
             ril.primary_pack_no,
             ril.repl_order_ctrl,
             ril.stock_cat,
             ril.activate_date,
             ril.deactivate_date,
             ril.min_stock,
             ril.max_stock,
             ril.min_supply_days,
             ril.max_supply_days,
             ril.repl_method,
             ril.primary_repl_supplier,
             ril.origin_country_id,
             ril.source_wh,
             ril.rowid
        from repl_item_loc ril,
             store s
       where ril.loc_type = 'S'
         and ril.location = s.store
         and s.stockholding_ind = 'Y'
         and ril.item = L_item
         for update nowait;

   cursor C_WAREHOUSE is
      select location,
             primary_pack_no,
             repl_order_ctrl,
             stock_cat,
             activate_date,
             deactivate_date,
             min_stock,
             max_stock,
             min_supply_days,
             max_supply_days,
             repl_method,
             primary_repl_supplier,
             origin_country_id,
             source_wh,
             rowid
        from repl_item_loc
       where item = L_item
         and location = LP_value
         for update nowait;

   cursor C_ALL_WAREHOUSES is
      select location,
             primary_pack_no,
             repl_order_ctrl,
             stock_cat,
             activate_date,
             deactivate_date,
             min_stock,
             max_stock,
             min_supply_days,
             max_supply_days,
             repl_method,
             primary_repl_supplier,
             origin_country_id,
             source_wh,
             rowid
        from repl_item_loc
       where loc_type = 'W'
         and item = L_item
       for update nowait;

   cursor C_LOC_LIST_ST is
      select lld.location,
             ril.primary_pack_no,
             lld.loc_type,
             ril.repl_order_ctrl,
             ril.stock_cat,
             ril.activate_date,
             ril.deactivate_date,
             ril.min_stock,
             ril.max_stock,
             ril.min_supply_days,
             ril.max_supply_days,
             ril.repl_method,
             ril.primary_repl_supplier,
             ril.origin_country_id,
             ril.source_wh,
             ril.rowid
        from repl_item_loc ril,
             loc_list_detail lld
       where ril.location = lld.location
         and lld.loc_list = LP_value
         and lld.loc_type = 'S'
         and ril.item = L_item
         for update nowait;

   cursor C_LOC_LIST_WH is
      select lld.location,
             ril.primary_pack_no,
             lld.loc_type,
             ril.repl_order_ctrl,
             ril.stock_cat,
             ril.activate_date,
             ril.deactivate_date,
             ril.min_stock,
             ril.max_stock,
             ril.min_supply_days,
             ril.max_supply_days,
             ril.repl_method,
             ril.primary_repl_supplier,
             ril.origin_country_id,
             ril.source_wh,
             ril.rowid
        from repl_item_loc ril,
             loc_list_detail lld
       where ril.location = lld.location
         and lld.loc_list = LP_value
         and lld.loc_type = 'W'
         and ril.item = L_item
         for update nowait;

   cursor C_AREA is
      select ril.location,
             ril.primary_pack_no,
             ril.repl_order_ctrl,
             ril.stock_cat,
             ril.activate_date,
             ril.deactivate_date,
             ril.min_stock,
             ril.max_stock,
             ril.min_supply_days,
             ril.max_supply_days,
             ril.repl_method,
             ril.primary_repl_supplier,
             ril.origin_country_id,
             ril.source_wh,
             ril.rowid
        from repl_item_loc ril,
             store_hierarchy sh
       where ril.location = sh.store
         and sh.area      = LP_value
         and ril.item     = L_item
         for update nowait;

--------------------------------------------------------------------------------------------------------------
   FUNCTION UPDATE_ITEM (O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item                 IN     repl_item_loc.item%TYPE,
                         I_locn_type            IN     repl_item_loc.loc_type%TYPE,
                         I_locn                 IN     repl_item_loc.location%TYPE,
                         I_repl_order_ctrl      IN     repl_item_loc.repl_order_ctrl%TYPE,
                         I_stock_cat            IN     repl_item_loc.stock_cat%TYPE,
                         I_activate_date        IN     repl_item_loc.activate_date%TYPE,
                         I_deactivate_date      IN     repl_item_loc.deactivate_date%TYPE,
                         I_min_stock            IN     repl_item_loc.min_stock%TYPE,
                         I_max_stock            IN     repl_item_loc.max_stock%TYPE,
                         I_min_supply_days      IN     repl_item_loc.min_supply_days%TYPE,
                         I_max_supply_days      IN     repl_item_loc.max_supply_days%TYPE,
                         I_repl_method          IN     repl_item_loc.repl_method%TYPE,
                         I_supplier             IN     REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE,
                         I_source_wh            IN     REPL_ITEM_LOC.SOURCE_WH%TYPE,
                         I_rowid                IN     ROWID)
      RETURN BOOLEAN IS

      L_table                     VARCHAR2(30) := Null;
      EXIT_REJECT                 EXCEPTION;
      RECORD_LOCKED               EXCEPTION;
      PRAGMA                      EXCEPTION_INIT(Record_Locked, -54);
      L_stock_desc                CODE_DETAIL.CODE_DESC%TYPE;
      L_wh_exists                 VARCHAR2(1);
      L_sup_loc_same_ou           BOOLEAN;
      L_ratio                     NUMBER(12,4);
      L_loc_type                  ITEM_LOC.LOC_TYPE%TYPE;
      L_location                  ITEM_LOC.LOC%TYPE;
      L_multiple_runs_per_day_ind REPL_ITEM_LOC.MULT_RUNS_PER_DAY_IND%TYPE := LP_multiple_runs_per_day_ind;
      L_tsf_zero_soh_ind          REPL_ITEM_LOC.TSF_ZERO_SOH_IND%TYPE      := LP_tsf_zero_soh_ind;
      L_store_type                STORE.STORE_TYPE%TYPE;
      L_exists                    BOOLEAN;
      L_import_exists             BOOLEAN := FALSE;
      L_origin_country_id         ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
      L_loc_country               COUNTRY.COUNTRY_ID%TYPE;
      L_item_loc_status           ITEM_LOC.STATUS%TYPE;
      L_clear_ind                 ITEM_LOC.CLEAR_IND%TYPE;
      L_value                     NUMBER;
      cursor C_LOCK_REPL_DAY is
         select 'x'
           from repl_day
          where location = I_locn
            and item     = I_item
          for update nowait;

      cursor C_PARENT_WH_EXISTS is
         select 'x'
           from item_loc
          where loc  = LP_source_wh
            and item = I_item
            and loc_type = 'W'
            and status = DECODE(LP_stock_cat,'C','A','L','A',status);

      cursor C_SIZE_PROFILE is
         select nvl(vsp.ratio,0)
           from mv_size_profile  vsp
          where vsp.item = I_item
            and vsp.store = I_locn
            and rownum =1
          order by vsp.seq;

      cursor C_STORE_TYPE is
         select store_type
           from store
          where store= I_locn;

      cursor C_ITEM_LOC_STATUS is 
         select status,clear_ind
           from item_loc
          where item = I_item
            and loc = I_locn;
   BEGIN

      L_table := 'REPL_ITEM_LOC';

      -- If the activate date is being updated, verify that the new
      -- activate date is before the current deactivate date.

      if LP_activate_date is not Null then
         if LP_activate_date > LP_deactivate_date then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       I_locn_type,
                                                       I_locn,
                                                       'R',
                                                       'INVALID_ACTIVATE_DATE',
                                                       LP_user_id,
                                                       LP_activate_date,
                                                       I_deactivate_date,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;

      -- If the deactivate date is being updated, verify that the new
      -- deactivate date is greater than the current activate date.

      if LP_deactivate_date is not Null and LP_activate_date is Null then
         if LP_deactivate_date < I_activate_date then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       I_locn_type,
                                                       I_locn,
                                                       'R',
                                                       'INVALID_DEACTIVATE_DATE',
                                                       LP_user_id,
                                                       LP_deactivate_date,
                                                       I_activate_date,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;

      -- If the order control is being updated to 'B'uyer Worksheet and the stock category is not being updated then
      -- if the current stock category is 'W'arehouse Stocked and the location type is 'S'tore or if the current stock
      -- category is 'C'ross-docked, the order control can not be updated to 'B'.

      if LP_repl_order_ctrl = 'B' and LP_stock_cat is Null then
         if (I_stock_cat = 'W' and I_locn_type = 'S') or (I_stock_cat = 'C') then
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'SCST',
                                          I_stock_cat,
                                          L_stock_desc) = FALSE then
               return FALSE;
            end if;
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       I_locn_type,
                                                       I_locn,
                                                       'R',
                                                       'NO_BUYER_WKSHT',
                                                       LP_user_id,
                                                       L_stock_desc,
                                                       Null,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;

      -- If the existing order control is 'B'uyer Worksheet and the stock category is being updated to 'C'ross Docked,
      -- or if the stock category is being updated to 'W'arehouse Stocked when the location type is 'S'tore, the stock
      -- category will not be updated.

      if I_repl_order_ctrl = 'B' and LP_repl_order_ctrl is Null then
         if (LP_stock_cat = 'W' and I_locn_type = 'S') or (LP_stock_cat = 'C') then
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'SCST',
                                          LP_stock_cat,
                                          L_stock_desc) = FALSE then
               return FALSE;
            end if;
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       I_locn_type,
                                                       I_locn,
                                                       'R',
                                                       'INVALID_REPL_ORDER_CTRL',
                                                       LP_user_id,
                                                       L_stock_desc,
                                                       Null,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;

      -- If the min stock is being updated, verify that the new
      -- min stock is not greater than the current max stock for the repl method.

      if LP_min_stock is not Null and LP_max_stock is Null then
         if LP_min_stock > I_max_stock and LP_repl_method = I_repl_method then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       I_locn_type,
                                                       I_locn,
                                                       'R',
                                                       'INVALID_MIN_STOCK',
                                                       LP_user_id,
                                                       LP_min_stock,
                                                       I_max_stock,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;

      -- If the max stock is being updated, verify that the new
      -- max stock is not less than the current min stock for the repl method.

      if LP_max_stock is not Null and LP_min_stock is Null then
         if LP_max_stock < I_min_stock and LP_repl_method = I_repl_method then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       I_locn_type,
                                                       I_locn,
                                                       'R',
                                                       'INVALID_MAX_STOCK',
                                                       LP_user_id,
                                                       LP_max_stock,
                                                       I_min_stock,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;

      -- If the min time supply days is being updated, verify that the new
      -- min supply days is not greater than the current max time supply days.

      if LP_min_supply_days is not Null and LP_max_supply_days is Null then
         if LP_min_supply_days > I_max_supply_days then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       I_locn_type,
                                                       I_locn,
                                                       'R',
                                                       'INVALID_MIN_SUPPLY_DAYS',
                                                       LP_user_id,
                                                       LP_min_supply_days,
                                                       I_max_supply_days,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;

      -- If the max time supply days is being updated, verify that the new
      -- max time supply days is not less than the current min time supply days.

      if LP_max_supply_days is not Null and LP_min_supply_days is Null then
         if LP_max_supply_days < I_min_supply_days then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       I_locn_type,
                                                       I_locn,
                                                       'R',
                                                       'INVALID_MAX_SUPPLY_DAYS',
                                                       LP_user_id,
                                                       LP_max_supply_days,
                                                       I_min_supply_days,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;

      --- if a store is being placed on replenishment, validate that the source warehouse exists
      --- for the transaction level item.
      if LP_group_type not in ('W','AW','LLW') and LP_tran_level > LP_item_level and
         LP_source_wh is not Null then
         open C_PARENT_WH_EXISTS;
         fetch C_PARENT_WH_EXISTS into L_wh_exists;
         if C_PARENT_WH_EXISTS%NOTFOUND then
            close C_PARENT_WH_EXISTS;
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       Null,
                                                       Null,
                                                       'R',
                                                       'SOURCE_WH_NO_EXIST',
                                                       LP_user_id,
                                                       LP_source_wh,
                                                       Null,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
        close C_PARENT_WH_EXISTS;
      end if;

      -- Apply size profiles for stores if enabled
      if I_locn_type = 'S' and
         LP_size_profile_ind = 'Y' then

         open C_SIZE_PROFILE;
         fetch C_SIZE_PROFILE into L_ratio;

         if C_SIZE_PROFILE%NOTFOUND then
           L_ratio := 0;
         end if;

         close C_SIZE_PROFILE;

         if L_ratio is NULL then
            L_ratio := 0;
         end if;

         if LP_apply_pres_ind = 'Y' then
            LP_pres_stock := round(LP_orig_pres_stock * L_ratio);
         end if;
      end if;

      -- This check is added to check the Supplier and location belong to same org unit for 'D'irect to Store stock catagory.

      if LP_system_options_rec.org_unit_ind = 'Y'
      and LP_supplier is not NULL and LP_stock_cat ='D' then

         L_location := I_locn;
         L_loc_type := I_locn_type;

               ---
         if SET_OF_BOOKS_SQL.CHECK_SUPP_SINGLE_LOC(O_error_message,
                                                   L_sup_loc_same_ou,
                                                   LP_supplier,
                                                   L_location,
                                                   L_loc_type) = FALSE then
            return FALSE;
         end if;
               ---

         if L_sup_loc_same_ou = FALSE then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                      I_item,
                                                      I_locn_type,
                                                      I_locn,
                                                      'R',
                                                      'SUP_LOC_NOT_SAME_ORG_UNIT',
                                                       LP_user_id,
                                                       LP_supplier,
                                                       L_location,
                                                       NULL) = FALSE then
               RETURN FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;

      ---
      --- Check if the Supplier and location belong to same org unit
      if (LP_system_options_rec.org_unit_ind = 'Y') and
         (LP_stock_cat in ('C','L','W') or I_stock_cat in ('C','L','W')) and
         (LP_supplier is not NULL or I_supplier is not NULL) and
         (LP_source_wh is not NULL or I_source_wh is not NULL) and
         ((LP_source_wh is not NULL or LP_supplier is not NULL) or
          (LP_stock_cat <> I_stock_cat)) then
         ---
         if LOCATION_ATTRIB_SQL.GET_LOC_COUNTRY(O_error_message,
                                                L_loc_country,
                                                NVL(LP_source_wh,I_source_wh)) = FALSE then
             return FALSE;
         end if;

         if LP_origin_country_id is not NULL then
            L_origin_country_id := LP_origin_country_id;
         else
            if ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                         L_exists,
                                                         L_origin_country_id,
                                                         I_item,
                                                         NVL(LP_supplier,I_supplier)) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_loc_country != L_origin_country_id then
            if SUPP_ATTRIB_SQL.CHECK_SUP_IMPORT_EXIST(O_error_message,
                                                      L_import_exists,
                                                      LP_supplier,
                                                      NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
		 
      if not ((LP_stock_cat = 'W' and LP_group_type not in ('W','DW','LLW','AW')) or LP_stock_cat = 'D') then  
         if L_import_exists = FALSE then
            if SET_OF_BOOKS_SQL.CHECK_SUPP_SINGLE_LOC(O_error_message,
                                                      L_sup_loc_same_ou,
                                                      NVL(LP_supplier,I_supplier),
                                                      NVL(LP_source_wh,I_source_wh),
                                                      'W') = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_sup_loc_same_ou = FALSE then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                      I_item,
                                                      I_locn_type,
                                                      I_locn,
                                                      'R',
                                                      'SUP_LOC_NOT_SAME_ORG_UNIT',
                                                      LP_user_id,
                                                      NVL(LP_supplier,I_supplier),
                                                      NVL(LP_source_wh,I_source_wh),
                                                      NULL) = FALSE then
               RETURN FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;
	 end if; 
      if I_locn_type='S' and (LP_multiple_runs_per_day_ind = 'Y' or LP_tsf_zero_soh_ind ='Y') then
         open C_STORE_TYPE;
         fetch C_STORE_TYPE into L_store_type ;

         if C_STORE_TYPE%FOUND then
            if L_store_type in ('W','F') then
               L_multiple_runs_per_day_ind := 'N';
               L_tsf_zero_soh_ind          := 'N';
            end if;
         end if;
         close C_STORE_TYPE;
      end if;
      ---
      -- If all checks pass then record(s) will be written to repl_item_loc_updates
      -- depending on the values being updated.

      if REPL_ITEM_LOC_UPDATES(O_error_message,
                               I_item,
                               I_locn_type,
                               I_locn) = FALSE then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_STATUS','ITEM_LOC', NULL);
      open C_ITEM_LOC_STATUS;
      SQL_LIB.SET_MARK('FETCH', 'C_ITEM_LOC_STATUS', 'ITEM_LOC',NULL);
      fetch C_ITEM_LOC_STATUS into L_item_loc_status,L_clear_ind;
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_LOC_STATUS', 'ITEM_LOC',NULL);
      close C_ITEM_LOC_STATUS;
      
      if L_item_loc_status = 'A' and L_clear_ind = 'N' then
         if LP_group_type not in('S','W','DW') and NVL(LP_activate_date, I_activate_date) < I_deactivate_date then
            L_value:=0;
         else
            L_value:=1;
         end if;
      else 
         L_value:=2;
      end if;
      -- If all checks pass, then update all replenishment attributes
      -- that are not replenishment method specific.

      update repl_item_loc
         set repl_order_ctrl       = NVL(LP_repl_order_ctrl,   repl_order_ctrl),
             stock_cat             = NVL(LP_stock_cat,         stock_cat),
             activate_date         = NVL(LP_activate_date,     activate_date),
             deactivate_date       = DECODE(L_value,0,NVL(LP_deactivate_date,deactivate_date),1,LP_deactivate_date,2,deactivate_date),
             source_wh             = NVL(LP_source_wh,         source_wh),
             pres_stock            = NVL(LP_pres_stock,        pres_stock),
             demo_stock            = NVL(LP_demo_stock,        demo_stock),
             pickup_lead_time      = NVL(LP_pickup_lead_time,  pickup_lead_time),
             wh_lead_time          = NVL(LP_wh_lead_time,      wh_lead_time),
             primary_repl_supplier = NVL(LP_supplier,          primary_repl_supplier),
             origin_country_id     = NVL(LP_origin_country_id, origin_country_id),
             review_cycle          = NVL(LP_review_cycle,      review_cycle),
             primary_pack_no       = DECODE(LP_reject_pack,'Y',primary_pack_no,LP_primary_pack_no),
             use_tolerance_ind     = NVL(LP_use_tolerance_ind, use_tolerance_ind),
             pct_tolerance         = DECODE(LP_use_tolerance_ind,
                                            'Y', LP_pct_tolerance,
                                            NVL(LP_pct_tolerance,pct_tolerance)),
             unit_tolerance        = DECODE(LP_use_tolerance_ind,
                                           'Y', LP_unit_tolerance,
                                           NVL(LP_unit_tolerance,unit_tolerance)),
             service_level_type    = LP_service_level_type,
             mult_runs_per_day_ind = DECODE(L_multiple_runs_per_day_ind,
                                            'Y',
                                            DECODE(LP_repl_method_update,
                                            'Y',
                                            mult_runs_per_day_ind,
                                            L_multiple_runs_per_day_ind),
                                            L_multiple_runs_per_day_ind),

             tsf_zero_soh_ind      = DECODE(L_tsf_zero_soh_ind,
                                            'Y',
                                            DECODE(LP_repl_method_update,
                                            'Y',
                                            tsf_zero_soh_ind,
                                            L_tsf_zero_soh_ind),
                                            L_tsf_zero_soh_ind),
             last_update_datetime  = sysdate,
             last_update_id        = get_user,
             supp_lead_time        = NVL(LP_supp_lead_time, supp_lead_time)
       where rowid = I_rowid;

      -- If the user wanted to update the replenishment method,
      -- then update the replenishment method and the replenishment method
      -- specific attributes.

      if LP_repl_method is not Null then
         if I_locn_type = 'S' and
            LP_size_profile_ind = 'Y' then

            if LP_repl_method = 'M' then
              LP_min_stock := round(LP_orig_min_stock * L_ratio);
              LP_max_stock := round(LP_orig_max_stock * L_ratio);
            elsif LP_repl_method = 'C' then
              LP_max_stock := round(LP_orig_max_stock * L_ratio);
            elsif LP_repl_method in ('D','T') and LP_orig_terminal_stock is not null then
               LP_terminal_stock_qty := round(LP_orig_terminal_stock * L_ratio);
            end if;
         end if;
         ---
         if LP_repl_method_update = 'Y' then

            -- Update the repl method and method specific attributes for
            -- the item location.

            update repl_item_loc
               set repl_method           = LP_repl_method,
                   min_stock             = LP_min_stock,
                   max_stock             = LP_max_stock,
                   incr_pct              = LP_incr_pct,
                   min_supply_days       = LP_min_supply_days,
                   max_supply_days       = LP_max_supply_days,
                   time_supply_horizon   = LP_time_supply_horizon,
                   inv_selling_days      = LP_inv_selling_days,
                   service_level         = LP_service_level,
                   lost_sales_factor     = LP_lost_sales_factor,
                   terminal_stock_qty    = LP_terminal_stock_qty,
                   season_id             = LP_season_id,
                   phase_id              = LP_phase_id,
                   non_scaling_ind       = LP_non_scaling_ind,
                   max_scale_value       = LP_max_scale_value,
                   reject_store_ord_ind  = LP_rej_st_ord_ind,
                   last_update_datetime  = sysdate,
                   last_update_id        = get_user,
                   mult_runs_per_day_ind = NVL(L_multiple_runs_per_day_ind,'N'),
                   tsf_zero_soh_ind      = NVL(L_tsf_zero_soh_ind,'N'),
                   add_lead_time_ind     = LP_add_lead_time_ind
             where rowid = I_rowid;

         -- If the user wanted only to update the replenishment method
         -- specific attributes but not the replenishment method itself.

         elsif LP_repl_method_update = 'N' then

            -- Update only the replenishment specific attributes for the item location
            -- and the specified replenishment method.

            update repl_item_loc
               set min_stock             = NVL(LP_min_stock,           min_stock),
                   max_stock             = NVL(LP_max_stock,           max_stock),
                   incr_pct              = NVL(LP_incr_pct,            incr_pct),
                   min_supply_days       = NVL(LP_min_supply_days,     min_supply_days),
                   max_supply_days       = NVL(LP_max_supply_days,     max_supply_days),
                   time_supply_horizon   = NVL(LP_time_supply_horizon, time_supply_horizon),
                   inv_selling_days      = NVL(LP_inv_selling_days,    inv_selling_days),
                   service_level         = NVL(LP_service_level,       service_level),
                   lost_sales_factor     = NVL(LP_lost_sales_factor,   lost_sales_factor),
                   terminal_stock_qty    = NVL(LP_terminal_stock_qty,  terminal_stock_qty),
                   season_id             = NVL(LP_season_id,           season_id),
                   phase_id              = NVL(LP_phase_id,            phase_id),
                   review_cycle          = NVL(LP_review_cycle,        review_cycle),
                   non_scaling_ind       = NVL(LP_non_scaling_ind,     non_scaling_ind),
                   max_scale_value       = NVL(LP_max_scale_value,     max_scale_value),
                   reject_store_ord_ind  = NVL(LP_rej_st_ord_ind,      reject_store_ord_ind),
                   service_level_type    = NVL(LP_service_level_type,  service_level_type),
                   last_update_datetime  = SYSDATE,
                   last_update_id        = GET_USER,
                   add_lead_time_ind     = NVL(LP_add_lead_time_ind,   add_lead_time_ind)
             where rowid = I_rowid
               and repl_method = LP_repl_method;
         end if;

      end if;

      -- Clear out any unwanted information due to certain attributes being updated.

      if I_locn_type = 'S' then
         if LP_stock_cat is not Null or LP_source_wh is not Null or
            LP_pickup_lead_time is not Null or LP_wh_lead_time is not Null or
            LP_supplier is not Null or LP_use_tolerance_ind is not Null or LP_pct_tolerance is not Null or
            LP_unit_tolerance is not Null or LP_non_scaling_ind is not Null or LP_max_scale_value is not Null then

            update repl_item_loc
               set source_wh             = DECODE(stock_cat,
                                                  'D', Null,
                                                  source_wh),
                   pickup_lead_time      = DECODE(stock_cat,
                                                  'W', Null,
                                                  pickup_lead_time),
                   wh_lead_time          = DECODE(stock_cat,
                                                  'D', Null,
                                                  wh_lead_time),
                   primary_repl_supplier = DECODE(stock_cat,
                                                  'W', Null,
                                                  primary_repl_supplier),
                   origin_country_id     = DECODE(stock_cat,
                                                  'W',Null,
                                                  origin_country_id),
                   use_tolerance_ind     = DECODE(stock_cat,
                                                  'W', 'N',
                                                  use_tolerance_ind),
                   pct_tolerance         = DECODE(stock_cat,
                                                  'W', Null,
                                                  DECODE(use_tolerance_ind,
                                                         'N', Null,
                                                         pct_tolerance)),
                   unit_tolerance       = DECODE(stock_cat,
                                                  'W', Null,
                                                  DECODE(use_tolerance_ind,
                                                         'N', Null,
                                                         unit_tolerance)),
                   max_scale_value      = DECODE(non_scaling_ind,
                                                 'Y', Null,
                                                  max_scale_value),
                   last_update_datetime = sysdate,
                   last_update_id       = get_user
             where rowid = I_rowid;
         end if;

      elsif I_locn_type = 'W' then
         if LP_non_scaling_ind is not Null or LP_max_scale_value is not Null then

            update repl_item_loc
               set max_scale_value      = DECODE(non_scaling_ind,'Y',Null,max_scale_value),
                   last_update_datetime = sysdate,
                   last_update_id       = get_user
               where rowid = I_rowid;
         end if;
      end if;

      -- Record days of the week on which the item/location will be reviewed.

      if LP_update_days = 'Y' then
         L_table := 'REPL_DAY';
         open  C_LOCK_REPL_DAY;
         close C_LOCK_REPL_DAY;
         ---
         delete repl_day
          where item     = I_item
            and location = I_locn;

         if not REPLENISHMENT_DAYS_SQL.INSERT_DAY_ITEM(O_error_message,
                                                       I_item,
                                                       I_locn,
                                                       I_locn_type,
                                                       LP_sunday,
                                                       LP_monday,
                                                       LP_tuesday,
                                                       LP_wednesday,
                                                       LP_thursday,
                                                       LP_friday,
                                                       LP_saturday) then
            return FALSE;
         end if;
      end if;

     -- Update associated Master Replenishment Record
      if LP_mra_update = 'Y' then
         if not REPL_ATTRIBUTE_UPDATE_SQL.UPDATE_MRA(O_error_message,
                                                     I_item,
                                                     I_locn,
                                                     I_locn_type) then
            return FALSE;
         end if;
      end if;


      return TRUE;

   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', L_table,
                                                               I_item,
                                                               to_char(I_locn));
         return FALSE;
      when EXIT_REJECT then

         -- Set an indicator to show that an item has been rejected, therefore
         -- a rejection report can be run when all update processing is completed.

         LP_report_required := TRUE;
         return TRUE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                'REPL_ATTRIBUTE_MAINTENANCE_SQL.UPDATE_ITEM',
                                                to_char(SQLCODE));
         return FALSE;
   END UPDATE_ITEM;
---------------------------------------------------------------------------------------------------------
   FUNCTION UPDATE_PACK(O_error_message        IN OUT     VARCHAR2,
                        I_item                 IN         ITEM_MASTER.ITEM%TYPE,
                        I_primary_pack_no      IN         REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE,
                        I_loc_type             IN         ITEM_LOC.LOC_TYPE%TYPE,
                        I_loc                  IN         ITEM_LOC.LOC%TYPE,
                        I_repl_order_ctrl      IN         REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE,
                        I_stock_cat            IN         REPL_ITEM_LOC.STOCK_CAT%TYPE,
                        I_activate_date        IN         REPL_ITEM_LOC.ACTIVATE_DATE%TYPE,
                        I_deactivate_date      IN         REPL_ITEM_LOC.DEACTIVATE_DATE%TYPE,
                        I_min_stock            IN         REPL_ITEM_LOC.MIN_STOCK%TYPE,
                        I_max_stock            IN         REPL_ITEM_LOC.MAX_STOCK%TYPE,
                        I_min_supply_days      IN         REPL_ITEM_LOC.MIN_SUPPLY_DAYS%TYPE,
                        I_max_supply_days      IN         REPL_ITEM_LOC.MAX_SUPPLY_DAYS%TYPE,
                        I_repl_method          IN         REPL_ITEM_LOC.REPL_METHOD%TYPE,
                        I_supplier             IN         REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE,
                        I_origin_country_id    IN         REPL_ITEM_LOC.ORIGIN_COUNTRY_ID%TYPE,
                        I_source_wh            IN         REPL_ITEM_LOC.SOURCE_WH%TYPE,
                        I_rowid                IN         ROWID)

      RETURN BOOLEAN is

      L_exists                       BOOLEAN;
      L_status                       ITEM_LOC.STATUS%TYPE;
      L_location                     ITEM_LOC.LOC%TYPE;
      L_desc                         ITEM_MASTER.ITEM_DESC%TYPE;
      L_item_status                  ITEM_MASTER.STATUS%TYPE;
      L_pack_status                  ITEM_MASTER.STATUS%TYPE;
      L_supplier                     REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE;
      L_origin_country_id            REPL_ITEM_LOC.ORIGIN_COUNTRY_ID%TYPE;

      cursor C_PRIMARY_COST_PACK is
         select primary_cost_pack
           from item_loc
          where item = I_item
            and loc = I_loc;

      cursor C_STATUS is
         select status
           from item_loc
          where loc = L_location
            and item = LP_primary_pack_no;

   BEGIN

      if LP_default_pack_ind = 'Y' then
         ---
         if I_primary_pack_no is Null then
            ---
            open C_PRIMARY_COST_PACK;
            fetch C_PRIMARY_COST_PACK into LP_primary_pack_no;
            close C_PRIMARY_COST_PACK;
            ---
         elsif I_primary_pack_no is not null then
            LP_primary_pack_no := I_primary_pack_no;
         end if;
      end if;
      ---
      if LP_remove_pack_ind = 'Y' then
         LP_primary_pack_no := Null;
      elsif LP_remove_pack_ind = 'N' and LP_default_pack_ind = 'N' and LP_primary_pack_no is Null
         and not (LP_item_level = LP_tran_level and LP_group_type in ('S','W')) then
         LP_reject_pack := 'Y';
      end if;
      ---
      if LP_primary_pack_no is not Null then
         ---
         L_location := I_loc;
         ---
         open C_STATUS;
         fetch C_STATUS into L_status;
         close C_STATUS;

         if L_status is Null or L_status != 'A' then
            ---
            LP_reject_pack := 'Y';
            ---
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                      I_item,
                                                      I_loc_type,
                                                      I_loc,
                                                      'R',
                                                      'PACK_LOC_NO_EXIST',
                                                      LP_user_id,
                                                      LP_primary_pack_no,
                                                      I_loc,
                                                      Null) = FALSE then
               return FALSE;
            end if;
            LP_report_required := TRUE;
         end if;
         if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                     L_desc,
                                     L_pack_status,
                                     LP_primary_pack_no) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                     L_desc,
                                     L_item_status,
                                     I_item) = FALSE then
            return FALSE;
         end if;
         ---
         if L_pack_status != L_item_status then
            ---
            LP_reject_pack := 'Y';
            ---
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                      I_item,
                                                      I_loc_type,
                                                      I_loc,
                                                      'R',
                                                      'PACK_STATUS',
                                                      LP_user_id,
                                                      LP_primary_pack_no,
                                                      I_item,
                                                      Null) = FALSE then
               return FALSE;
            end if;
            LP_report_required := TRUE;
         end if;
         ---
         L_status  := Null;
         ---
         if LP_source_wh is not Null and LP_default_pack_ind = 'Y' then
            ---
            L_location := LP_source_wh;
            ---
            open C_STATUS;
            fetch C_STATUS into L_status;
            close C_STATUS;
            ---
            if L_status is Null or (L_status != 'A' and LP_stock_cat != 'W') then
               ---
               LP_reject_pack := 'Y';
               ---
               if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                         I_item,
                                                         'W',
                                                         LP_source_wh,
                                                         'R',
                                                         'PACK_LOC_NO_EXIST',
                                                         LP_user_id,
                                                         LP_primary_pack_no,
                                                         LP_source_wh,
                                                         Null) = FALSE then
                  return FALSE;
               end if;
               LP_report_required := TRUE;
            end if;
         end if;
         ---
         if (LP_supplier is not Null or I_supplier is not Null) and LP_default_pack_ind = 'Y' then
            if LP_supplier is not Null then
               L_supplier := LP_supplier;
               L_origin_country_id := LP_origin_country_id;
            else
               L_supplier := I_supplier;
               L_origin_country_id := I_origin_country_id;
            end if;
            ---
            if SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS(O_error_message,
                                                      L_exist,
                                                      LP_primary_pack_no,
                                                      L_supplier,
                                                      L_origin_country_id) = FALSE then
               return FALSE;
            end if;
            ---
            if L_exist = FALSE then
               ---
               LP_reject_pack := 'Y';
               ---
               if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                         I_item,
                                                         I_loc_type,
                                                         I_loc,
                                                         'R',
                                                         'SUPP_NO_EXIST_PACK',
                                                         LP_user_id,
                                                         LP_primary_pack_no,
                                                         L_supplier,
                                                         L_origin_country_id) = FALSE then
                  return FALSE;
               end if;
               LP_report_required := TRUE;
            end if;
         end if;
      end if;
      ---

      if UPDATE_ITEM(O_error_message,
                     I_item,
                     I_loc_type,
                     I_loc,
                     I_repl_order_ctrl,
                     I_stock_cat,
                     I_activate_date,
                     I_deactivate_date,
                     I_min_stock,
                     I_max_stock,
                     I_min_supply_days,
                     I_max_supply_days,
                     I_repl_method,
                     I_supplier,
                     I_source_wh,
                     I_rowid) = FALSE then
         return FALSE;
      end if;
      ---
      LP_reject_pack := 'N';
      ---
      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               'REPL_ATTRIBUTE_MAINTENANCE_SQL.UPDATE_PACK',
                                                to_char(SQLCODE));
         return FALSE;
   END UPDATE_PACK;
-------------------------------------------------------------------------------------------------------------
BEGIN
   -- If the item is using seasonal repl, check if the item exists
   -- on the item seasons table.  If it doesn't, insert a record.

   if LP_repl_method is not Null and LP_phase_id is NOT Null then
      if NOT SEASON_SQL.CREATE_PHASE_ITEM_RELATION(O_error_message,
                                                   LP_item_season_seq_no,
                                                   LP_season_id,
                                                   LP_phase_id,
                                                   I_item) then
         return FALSE;
      end if;
   end if;

   if LP_group_type = 'S' then
      FOR C_rec in C_STORE LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'S',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type = 'C' then
      FOR C_rec in C_STORE_CLASS LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'S',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type = 'D' then
      FOR C_rec in C_DISTRICT LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'S',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type = 'R' then
      FOR C_rec in C_REGION LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'S',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;


   elsif LP_group_type = 'T' then
      FOR C_rec in C_TRANSFER_ZONE LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'S',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type = 'L' then
      FOR C_rec in C_LOC_TRAIT LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'S',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type = 'DW' then
      FOR C_rec in C_DEFAULT_WH LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'S',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type = 'AS' then
      FOR C_rec in C_ALL_STORES LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'S',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type =  'W' then
      FOR C_rec in C_WAREHOUSE LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'W',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type = 'AW' then
      FOR C_rec in C_ALL_WAREHOUSES LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'W',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type = 'LLS' then
      FOR C_rec in C_LOC_LIST_ST LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             C_rec.loc_type,
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type = 'LLW' then
      FOR C_rec in C_LOC_LIST_WH LOOP
         if not UPDATE_PACK(O_error_message,
                            L_item,
                            C_rec.primary_pack_no,
                            C_rec.loc_type,
                            C_rec.location,
                            C_rec.repl_order_ctrl,
                            C_rec.stock_cat,
                            C_rec.activate_date,
                            C_rec.deactivate_date,
                            C_rec.min_stock,
                            C_rec.max_stock,
                            C_rec.min_supply_days,
                            C_rec.max_supply_days,
                            C_rec.repl_method,
                            C_rec.primary_repl_supplier,
                            C_rec.origin_country_id,
                            C_rec.source_wh,
                            C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;

   elsif LP_group_type = 'A' then
      FOR C_rec in C_AREA LOOP
         if not UPDATE_PACK (O_error_message,
                             L_item,
                             C_rec.primary_pack_no,
                             'S',
                             C_rec.location,
                             C_rec.repl_order_ctrl,
                             C_rec.stock_cat,
                             C_rec.activate_date,
                             C_rec.deactivate_date,
                             C_rec.min_stock,
                             C_rec.max_stock,
                             C_rec.min_supply_days,
                             C_rec.max_supply_days,
                             C_rec.repl_method,
                             C_rec.primary_repl_supplier,
                             C_rec.origin_country_id,
                             C_rec.source_wh,
                             C_rec.rowid) then
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED','REPL_ITEM_LOC',
                                             L_item,
                                             Null);
      return false;
   when EXIT_REJECT then
      LP_report_required := TRUE;
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'REPL_ATTRIBUTE_MAINTENANCE_SQL.ITEM_LOCNS_UPDATE',
                                             to_char(SQLCODE));
      return FALSE;
END ITEM_LOCNS_UPDATE;
---------------------------------------------------------------------------------------------------------
FUNCTION ITEM_LOCNS_ACTIVATE( O_error_message   IN OUT   VARCHAR2,
                              I_item            IN       REPL_ITEM_LOC.ITEM%TYPE )
RETURN BOOLEAN IS

   L_exist                 BOOLEAN;
   L_ind                   VARCHAR2(1)    := 'N';
   EXIT_REJECT             EXCEPTION;

----------------------------------------------------------------------------------------------------------
   FUNCTION ACTIVATE(O_error_message    IN OUT VARCHAR2,
                     I_item             IN     repl_item_loc.item%TYPE,
                     I_locn_type        IN     repl_item_loc.loc_type%TYPE,
                     I_locn             IN     repl_item_loc.location%TYPE,
                     I_clear_ind        IN     item_loc.clear_ind%TYPE)
      RETURN BOOLEAN IS

      EXIT_REJECT                         EXCEPTION;
      L_wh_exists                         VARCHAR2(1);
      L_sup_loc_same_ou                   BOOLEAN;
      L_location                          ITEM_LOC.LOC%TYPE;
      L_loc_type                          ITEM_LOC.LOC_TYPE%TYPE;
      L_ratio                             NUMBER(12,4);
      L_multiple_runs_per_day_ind         REPL_ITEM_LOC.MULT_RUNS_PER_DAY_IND%TYPE := LP_multiple_runs_per_day_ind;
      L_tsf_zero_soh_ind                  REPL_ITEM_LOC.TSF_ZERO_SOH_IND%TYPE      := LP_tsf_zero_soh_ind;
      L_store_type                        STORE.STORE_TYPE%TYPE;
      L_exists                            BOOLEAN;
      L_import_exists                     BOOLEAN := FALSE;
      L_origin_country_id                 ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
      L_loc_country                       COUNTRY.COUNTRY_ID%TYPE;
      L_stockholding_ind                  STORE.STOCKHOLDING_IND%TYPE;

      cursor C_SUB_ITEM_CHECK is
         select 'x'
           from sub_items_detail
          where sub_item = I_item
            and loc_type = I_locn_type
            and location = I_locn;

      cursor C_PARENT_WH_EXISTS is
         select 'x'
           from item_loc
          where loc  = LP_source_wh
            and item = I_item
            and loc_type = 'W'
            and status = DECODE(LP_stock_cat,'C','A','L','A',status);

      -- when size_prof_ind is 'Y'
      cursor C_SIZE_PROFILE is
         select nvl(vsp.ratio,0)
      from mv_size_profile  vsp
     where vsp.item = I_item
       and vsp.store = I_locn
       and rownum =1
          order by vsp.seq;


      cursor C_STORE_TYPE is
         select store_type,
                stockholding_ind
           from store
          where store= I_locn;

   BEGIN
      -- ACTIVATE REPLENISHMENT
      ---
      -- Check if the Supplier and location belong to same org unit
      if LP_system_options_rec.org_unit_ind = 'Y'
      and LP_supplier is not NULL then

         if LP_stock_cat in ('C','L') then
            L_location := LP_source_wh;
            L_loc_type := 'W';
         elsif LP_stock_cat = 'W' then
            if I_locn_type = 'W' then
               L_loc_type := 'W';
            elsif I_locn_type = 'S' then
               L_loc_type := 'S';
            elsif I_locn_type = 'E' then
               L_loc_type := 'E';
            end if;
            L_location := I_locn;
         else
            L_location := I_locn;
            L_loc_type := I_locn_type;
         end if;
         ---
         if LOCATION_ATTRIB_SQL.GET_LOC_COUNTRY(O_error_message,
                                                L_loc_country,
                                                L_location) = FALSE then
             return FALSE;
         end if;

         if LP_origin_country_id is not NULL then
            L_origin_country_id := LP_origin_country_id;
         else
            if ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                         L_exists,
                                                         L_origin_country_id,
                                                         I_item,
                                                         LP_supplier) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_loc_country != L_origin_country_id then
            if SUPP_ATTRIB_SQL.CHECK_SUP_IMPORT_EXIST(O_error_message,
                                                      L_import_exists,
                                                      LP_supplier,
                                                      NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_import_exists = FALSE or LP_stock_cat ='D' then
            if SET_OF_BOOKS_SQL.CHECK_SUPP_SINGLE_LOC(O_error_message,
                                                      L_sup_loc_same_ou,
                                                      LP_supplier,
                                                      L_location,
                                                      L_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_sup_loc_same_ou = FALSE then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                      I_item,
                                                      I_locn_type,
                                                      I_locn,
                                                      'R',
                                                      'SUP_LOC_NOT_SAME_ORG_UNIT',
                                                      LP_user_id,
                                                      LP_supplier,
                                                      L_location,
                                                      NULL) = FALSE then
               RETURN FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
      end if;
      ---
      -- Write a rejection message and reject the item location if it is on clearance
      -- and contract is disabled
      
         if I_clear_ind = 'Y' and LP_system_options_rec.contract_ind = 'N' then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       I_locn_type,
                                                       I_locn,
                                                       'R',
                                                       'ON_CLEAR_NO_ACTIVATE',
                                                       LP_user_id,
                                                       Null,
                                                       Null,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
         ---
      if I_locn_type = 'S' then
         -- Apply size curves if turned on
         if LP_size_profile_ind = 'Y' then

            open C_SIZE_PROFILE;
            fetch C_SIZE_PROFILE into L_ratio;

            if C_SIZE_PROFILE%NOTFOUND then
              L_ratio := 0;
            end if;

            close C_SIZE_PROFILE;

            if L_ratio is NULL then
               L_ratio := 0;
            end if;

            if LP_repl_method = 'M' then
              LP_min_stock    := round(LP_orig_min_stock * L_ratio);
              LP_max_stock    := round(LP_orig_max_stock * L_ratio);
            elsif LP_repl_method = 'C' then
              LP_max_stock    := round(LP_orig_max_stock * L_ratio);
            elsif LP_repl_method in ('D','T') and LP_orig_terminal_stock is not null then
               LP_terminal_stock_qty := round(LP_orig_terminal_stock * L_ratio);
            end if;

            if LP_apply_pres_ind = 'Y' then
               LP_pres_stock := round(LP_orig_pres_stock * L_ratio);
            end if;
         end if;
      end if;
      ---
      open C_SUB_ITEM_CHECK;
      fetch C_SUB_ITEM_CHECK into L_ind;
      if C_SUB_ITEM_CHECK%FOUND then
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_item,
                                                   I_locn_type,
                                                   I_locn,
                                                   'R',
                                                   'SUB_ITEM_NO_ACTIVATE',
                                                   LP_user_id,
                                                   I_locn,
                                                   Null,
                                                   Null) = FALSE then
            close C_SUB_ITEM_CHECK;
            return FALSE;
         end if;
         close C_SUB_ITEM_CHECK;
         raise EXIT_REJECT;
      end if;
      close C_SUB_ITEM_CHECK;

      --- if a store is being placed on replenishment, validate that the source warehouse exists
      --- for the transaction level item.
      if LP_group_type not in ('W','AW','LLW') and LP_tran_level > LP_item_level and
         LP_source_wh is not Null then
         open C_PARENT_WH_EXISTS;
         fetch C_PARENT_WH_EXISTS into L_wh_exists;
         if C_PARENT_WH_EXISTS%NOTFOUND then
            close C_PARENT_WH_EXISTS;
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS (O_error_message,
                                                       I_item,
                                                       Null,
                                                       Null,
                                                       'R',
                                                       'SOURCE_WH_NO_EXIST',
                                                       LP_user_id,
                                                       LP_source_wh,
                                                       Null,
                                                       Null) = FALSE then
               return FALSE;
            end if;
            raise EXIT_REJECT;
         end if;
        close C_PARENT_WH_EXISTS;
      end if;
      ---
      if I_locn_type='S' and (LP_multiple_runs_per_day_ind = 'Y' or LP_tsf_zero_soh_ind ='Y') then
         open C_STORE_TYPE;
         fetch C_STORE_TYPE into L_store_type,L_stockholding_ind ;

         if C_STORE_TYPE%FOUND then
            if L_store_type = 'F' and L_stockholding_ind = 'N'  then
               L_multiple_runs_per_day_ind := 'N';
               L_tsf_zero_soh_ind          := 'N';
            end if;
         end if;
         close C_STORE_TYPE;
      end if;
      ---
      -- If the item location passed checks, activate the item/location
      -- on replenishment.

      insert into repl_item_loc (item,
                                 location,
                                 loc_type,
                                 item_parent,
                                 item_grandparent,
                                 primary_repl_supplier,
                                 origin_country_id,
                                 review_cycle,
                                 stock_cat,
                                 repl_order_ctrl,
                                 source_wh,
                                 activate_date,
                                 deactivate_date,
                                 pres_stock,
                                 demo_stock,
                                 repl_method,
                                 min_stock,
                                 max_stock,
                                 incr_pct,
                                 min_supply_days,
                                 max_supply_days,
                                 time_supply_horizon,
                                 inv_selling_days,
                                 service_level,
                                 lost_sales_factor,
                                 reject_store_ord_ind,
                                 non_scaling_ind,
                                 max_scale_value,
                                 pickup_lead_time,
                                 wh_lead_time,
                                 terminal_stock_qty,
                                 season_id,
                                 phase_id,
                                 primary_pack_no,
                                 unit_tolerance,
                                 pct_tolerance,
                                 item_season_seq_no,
                                 use_tolerance_ind,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id,
                                 service_level_type,
                                 mult_runs_per_day_ind,
                                 tsf_zero_soh_ind,
                                 add_lead_time_ind)
                         values (I_item,
                                 I_locn,
                                 I_locn_type,
                                 LP_item_parent,
                                 LP_item_grandparent,
                                 LP_supplier,
                                 LP_origin_country_id,
                                 LP_review_cycle,
                                 LP_stock_cat,
                                 LP_repl_order_ctrl,
                                 LP_source_wh,
                                 LP_activate_date,
                                 LP_deactivate_date,
                                 LP_pres_stock,
                                 LP_demo_stock,
                                 LP_repl_method,
                                 LP_min_stock,
                                 LP_max_stock,
                                 LP_incr_pct,
                                 LP_min_supply_days,
                                 LP_max_supply_days,
                                 LP_time_supply_horizon,
                                 LP_inv_selling_days,
                                 LP_service_level,
                                 LP_lost_sales_factor,
                                 LP_rej_st_ord_ind,
                                 LP_non_scaling_ind,
                                 LP_max_scale_value,
                                 LP_pickup_lead_time,
                                 LP_wh_lead_time,
                                 LP_terminal_stock_qty,
                                 LP_season_id,
                                 LP_phase_id,
                                 DECODE(LP_reject_pack,'Y',Null,'N',LP_primary_pack_no),
                                 LP_unit_tolerance,
                                 LP_pct_tolerance,
                                 LP_item_season_seq_no,
                                 LP_use_tolerance_ind,
                                 sysdate,
                                 sysdate,
                                 get_user,
                                 LP_service_level_type,
                                 NVL(L_multiple_runs_per_day_ind,'N'),
                                 NVL(L_tsf_zero_soh_ind, 'N'),
                                 LP_add_lead_time_ind);

         -- Record days of the week on which the item/location will be reviewed.

         if not REPLENISHMENT_DAYS_SQL.INSERT_DAY_ITEM(O_error_message,
                                                    I_item,
                                                    I_locn,
                                                    I_locn_type,
                                                    LP_sunday,
                                                    LP_monday,
                                                    LP_tuesday,
                                                    LP_wednesday,
                                                    LP_thursday,
                                                    LP_friday,
                                                    LP_saturday) then
            return FALSE;
         end if;

         if REPL_ATTRIBUTE_UPDATE_SQL.CREATE_NEW_MRA( O_error_message ,
                                                      I_item ,
                                                      I_locn,
                                                      I_locn_type
                                                      ) = FALSE then
            return FALSE;
         end if;

         -- if the supplier, origin country, or primary pack is being updated, then the repl_item_loc_update
         -- table will be inserted to to ensure that supplier, country and pack attributes are maintained.

         if REPL_ITEM_LOC_UPDATES(O_error_message,
                                  I_item,
                                  I_locn_type,
                                  I_locn) = FALSE then
            return FALSE;
         end if;

      return TRUE;

   EXCEPTION
      when EXIT_REJECT then
         -- Set an indicator to show that an item has been rejected, therefore
         -- a rejection report can be run when all update processing is completed.
         LP_report_required := TRUE;
         return TRUE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                'REPL_ATTRIBUTE_MAINTENANCE_SQL.ACTIVATE',
                                                to_char(SQLCODE));
         return FALSE;
   END ACTIVATE;
--------------------------------------------------------------------------------------------------------------
   FUNCTION ACTIVATE_PACK(O_error_message     IN OUT     VARCHAR2,
                          I_item              IN         ITEM_MASTER.ITEM%TYPE,
                          I_primary_cost_pack IN         ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                          I_loc_type          IN         ITEM_LOC.LOC_TYPE%TYPE,
                          I_loc               IN         ITEM_LOC.LOC%TYPE,
                          I_clear_ind         IN         ITEM_LOC.CLEAR_IND%TYPE)
      RETURN BOOLEAN is

      L_status          ITEM_LOC.STATUS%TYPE;
      L_location        ITEM_LOC.LOC%TYPE;
      L_desc            ITEM_MASTER.ITEM_DESC%TYPE;
      L_pack_status     ITEM_MASTER.STATUS%TYPE;
      L_item_status     ITEM_MASTER.STATUS%TYPE;
      L_exist           BOOLEAN;

      cursor C_STATUS is
         select status
           from item_loc
          where loc = L_location
            and item = LP_primary_pack_no;

   BEGIN

      if LP_default_pack_ind = 'Y' then
         LP_primary_pack_no := I_primary_cost_pack;
      end if;

      if LP_primary_pack_no is not Null then
         ---
         L_location := I_loc;
         ---
         open C_STATUS;
         fetch C_STATUS into L_status;
         close C_STATUS;

         if L_status is Null or L_status != 'A' then
            ---
            LP_reject_pack := 'Y';
            ---
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                      I_item,
                                                      I_loc_type,
                                                      I_loc,
                                                      'R',
                                                      'PACK_LOC_NO_EXIST',
                                                      LP_user_id,
                                                      LP_primary_pack_no,
                                                      I_loc,
                                                      Null) = FALSE then
               return FALSE;
            end if;
            LP_report_required := TRUE;
         end if;
         ---
         if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                     L_desc,
                                     L_pack_status,
                                     LP_primary_pack_no) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                     L_desc,
                                     L_item_status,
                                     I_item) = FALSE then
            return FALSE;
         end if;
         ---
         if L_pack_status != L_item_status then
            ---
            LP_reject_pack := 'Y';
            ---
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                      I_item,
                                                      I_loc_type,
                                                      I_loc,
                                                      'R',
                                                      'PACK_STATUS',
                                                      LP_user_id,
                                                      LP_primary_pack_no,
                                                      I_item,
                                                      Null) = FALSE then
               return FALSE;
            end if;
            LP_report_required := TRUE;
         end if;
         ---
         L_status  := Null;
         ---
         if LP_source_wh is not Null and LP_default_pack_ind = 'Y' then
            ---
            L_location := LP_source_wh;
            ---
            open C_STATUS;
            fetch C_STATUS into L_status;
            close C_STATUS;
            ---
            if L_status is Null or (L_status != 'A' and LP_stock_cat != 'W') then
               ---
               LP_reject_pack := 'Y';
               ---
               if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                         I_item,
                                                         'W',
                                                         LP_source_wh,
                                                         'R',
                                                         'PACK_LOC_NO_EXIST',
                                                         LP_user_id,
                                                         LP_primary_pack_no,
                                                         LP_source_wh,
                                                         Null) = FALSE then
                  return FALSE;
               end if;
               LP_report_required := TRUE;
            end if;
         end if;
         ---
         if LP_supplier is not Null and LP_default_pack_ind = 'Y' then
            ---
            if SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS(O_error_message,
                                                      L_exist,
                                                      LP_primary_pack_no,
                                                      LP_supplier,
                                                      LP_origin_country_id) = FALSE then
               return FALSE;
            end if;
            ---
            if L_exist = FALSE then
               ---
               LP_reject_pack := 'Y';
               ---
               if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                         I_item,
                                                         I_loc_type,
                                                         I_loc,
                                                         'R',
                                                         'SUPP_NO_EXIST_PACK',
                                                         LP_user_id,
                                                         LP_primary_pack_no,
                                                         LP_supplier,
                                                         LP_origin_country_id) = FALSE then
                  return FALSE;
               end if;
               LP_report_required := TRUE;
            end if;
         end if;
      end if;
      ---
      if not ACTIVATE(O_error_message,
                      I_item,
                      I_loc_type,
                      I_loc,
                      I_clear_ind) then
         return FALSE;
      end if;
      ---
      LP_reject_pack := 'N';
      ---
      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                               'REPL_ATTRIBUTE_MAINTENANCE_SQL.ACTIVATE_PACK',
                                                to_char(SQLCODE));
         return FALSE;
   END ACTIVATE_PACK;
-----------------------------------------------------------------------------------------------------------------
 FUNCTION ACTIVATE_ITEM(O_error_message  IN OUT VARCHAR2,
                        I_item           IN     repl_item_loc.item%type)
   RETURN BOOLEAN IS

   L_primary_cost_pack     ITEM_LOC.PRIMARY_COST_PACK%TYPE;
   L_location              ITEM_LOC.LOC%TYPE;
   L_exists                BOOLEAN;

       cursor C_STORE is
         select il.loc,
                il.clear_ind,
                il.primary_cost_pack
           from item_loc il,
                v_store s
          where il.item = I_item
            and il.loc = LP_value
            and s.store  = il.loc
            and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y')
            and il.status = 'A'
            and il.loc_type = 'S'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = il.loc);

      cursor C_STORE_CLASS is
         select s.store,
                il.clear_ind,
                il.primary_cost_pack
           from v_store s,
                item_loc il
          where il.loc = s.store
            and il.item = I_item
            and s.store_class = LP_value
            and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y')
            and il.status = 'A'
            and il.loc_type = 'S'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = s.store);

      cursor C_DISTRICT is
         select sh.store,
                il.clear_ind,
                il.primary_cost_pack
           from store_hierarchy sh,
                v_store s,
                item_loc il
          where sh.district = LP_value
            and il.loc = sh.store
            and s.store = sh.store
            and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y')
            and il.item = I_item
            and il.status = 'A'
            and il.loc_type = 'S'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = s.store);

      cursor C_REGION is
         select sh.store,
                il.clear_ind,
                il.primary_cost_pack
           from store_hierarchy sh,
                v_store s,
                item_loc il
          where sh.region = LP_value
            and sh.store = s.store
            and il.loc = s.store
            and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y')
            and il.item = I_item
            and il.status = 'A'
            and il.loc_type = 'S'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = s.store);


      cursor C_TRANSFER_ZONE is
         select s.store,
                il.clear_ind,
                il.primary_cost_pack
           from v_store s,
                item_loc il
          where il.loc = s.store
            and s.transfer_zone = LP_value
            and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y')
            and il.item = I_item
            and il.status = 'A'
            and il.loc_type = 'S'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = s.store);

      cursor C_LOC_TRAIT is
         select il.loc,
                il.clear_ind,
                il.primary_cost_pack
           from loc_traits_matrix ltm,
                item_loc il,
                v_store s
          where il.loc = ltm.store
            and il.loc = s.store
            and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y')
            and ltm.loc_trait = LP_value
            and il.item = I_item
            and il.status = 'A'
            and il.loc_type = 'S'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = il.loc);

      cursor C_DEFAULT_WH is
         select s.store,
                il.clear_ind,
                il.primary_cost_pack
           from v_store s,
                item_loc il
          where il.loc = s.store
            and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y')
            and s.default_wh = LP_value
            and il.item = I_item
            and il.status = 'A'
            and il.loc_type = 'S'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = s.store);

      cursor C_ALL_STORES is
         select il.loc,
                il.clear_ind,
                il.primary_cost_pack
           from item_loc il,
                v_store s
          where il.item = I_item
            and il.loc = s.store
            and s.stockholding_ind = 'Y'
            and il.status = 'A'
            and il.loc_type = 'S'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = il.loc);

      cursor C_WAREHOUSE is
         select il.loc,
                il.clear_ind,
                il.primary_cost_pack
           from item_loc il,
                wh w,
                v_wh vwh
          where il.item = I_item
            and il.loc = LP_value
            and il.loc = w.wh
            and il.loc = vwh.wh
            and w.stockholding_ind = 'Y'
            and w.finisher_ind = 'N'
            and w.repl_ind = 'Y'
            and il.status = 'A'
            and il.loc_type = 'W'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = il.loc);

     cursor C_ALL_WAREHOUSES is
         select il.loc,
                il.clear_ind,
                il.primary_cost_pack
           from item_loc il,
                wh w,
                v_wh vwh
          where il.item = I_item
            and il.loc = w.wh
            and il.loc = vwh.wh
            and w.stockholding_ind = 'Y'
            and w.finisher_ind = 'N'
            and w.repl_ind = 'Y'
            and il.status = 'A'
            and il.loc_type = 'W'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = il.loc);

      cursor C_LOC_LIST_ST is
         select il.loc,
                il.clear_ind,
                il.primary_cost_pack
           from item_loc il,
                loc_list_detail lld,
                loc_list_head llh,
                v_store s
          where il.loc = lld.location
            and llh.loc_list = lld.loc_list
            and il.loc = s.store
            and ((s.store_type in ('F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y')
            and lld.loc_list = LP_value
            and lld.loc_type = 'S'
            and il.item = I_item
            and il.status = 'A'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = il.loc);

      cursor C_LOC_LIST_WH is
         select distinct il.loc,
                il.clear_ind,
                il.primary_cost_pack
           from item_loc il,
                loc_list_detail lld,
                v_wh w
          where lld.loc_list = LP_value
            and lld.loc_type = 'W'
            and il.item = I_item
            and il.status = 'A'
            and il.loc = w.wh
            and w.stockholding_ind = 'Y'
            and w.repl_ind = 'Y'
            and (il.loc = lld.location
             or (lld.location = w.physical_wh))
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = il.loc);

      cursor C_AREA is
         select sh.store,
                il.clear_ind,
                il.primary_cost_pack
           from store_hierarchy sh,
                v_store s,
                item_loc il
          where sh.area = LP_value
            and sh.store = s.store
            and il.loc = s.store
            and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y')
            and il.item = I_item
            and il.status = 'A'
            and il.loc_type = 'S'
            and not exists (select 'x'
                              from repl_item_loc ril
                             where ril.item = I_item
                               and ril.location = s.store);




BEGIN
      -- Retrieve all locations in the group type and call ACTIVATE
      -- to update the replenishment attributes for each location.

      if LP_group_type = 'S' then
         FOR C_rec in C_STORE LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'S',
                             C_rec.loc,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type = 'C' then
         FOR C_rec in C_STORE_CLASS LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'S',
                             C_rec.store,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type = 'D' then
         FOR C_rec in C_DISTRICT LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'S',
                             C_rec.store,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type = 'R' then
         FOR C_rec in C_REGION LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'S',
                             C_rec.store,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;


      elsif LP_group_type = 'T' then
         FOR C_rec in C_TRANSFER_ZONE LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'S',
                             C_rec.store,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type = 'L' then
         FOR C_rec in C_LOC_TRAIT LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'S',
                             C_rec.loc,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type = 'DW' then
         FOR C_rec in C_DEFAULT_WH LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'S',
                             C_rec.store,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type = 'AS' then
         FOR C_rec in C_ALL_STORES LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'S',
                             C_rec.loc,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type =  'W' then
         FOR C_rec in C_WAREHOUSE LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'W',
                             C_rec.loc,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type = 'AW' then
         FOR C_rec in C_ALL_WAREHOUSES LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'W',
                             C_rec.loc,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type = 'LLS' then
         FOR C_rec in C_LOC_LIST_ST LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'S',
                             C_rec.loc,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type = 'LLW' then
         FOR C_rec in C_LOC_LIST_WH LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'W',
                             C_rec.loc,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;

      elsif LP_group_type = 'A' then
         FOR C_rec in C_AREA LOOP
            if ACTIVATE_PACK(O_error_message,
                             I_item,
                             C_rec.primary_cost_pack,
                             'S',
                             C_rec.store,
                             C_rec.clear_ind) = FALSE then
               return FALSE;
            end if;
         END LOOP;
      end if;

      return TRUE;

   EXCEPTION
      when EXIT_REJECT then
           LP_report_required := TRUE;
           return TRUE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                'REPL_ATTRIBUTE_MAINTENANCE_SQL.ACTIVATE_ITEM',
                                                to_char(SQLCODE));
        return FALSE;
   END ACTIVATE_ITEM;
------------------------------------------------------------------------------------------------------------
BEGIN
   if LP_phase_id is NOT Null then
      if NOT SEASON_SQL.CREATE_PHASE_ITEM_RELATION(O_error_message,
                                                   LP_item_season_seq_no,
                                                   LP_season_id,
                                                   LP_phase_id,
                                                   I_item) then
         return FALSE;
      end if;
   end if;

   if not ACTIVATE_ITEM(O_error_message,
                        I_item) then
      RETURN FALSE;
   end if;

   return TRUE;

EXCEPTION
   when EXIT_REJECT then
        LP_report_required := TRUE;
        return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'REPL_ATTRIBUTE_MAINTENANCE_SQL.ITEM_LOCNS_ACTIVATE',
                                             to_char(SQLCODE));
      return FALSE;
END ITEM_LOCNS_ACTIVATE;
------------------------------------------------------------------------------------------------------------
FUNCTION ITEM_LOCNS (O_error_message    IN OUT VARCHAR2,
                     I_item             IN repl_item_loc.item%type)
   RETURN BOOLEAN IS

BEGIN

   if LP_action = 'D' then
      if not ITEM_LOCNS_DEACTIVATE(O_error_message,
                                   I_item) then
         return FALSE;
      end if;
   elsif LP_action = 'U' then
      if not ITEM_LOCNS_UPDATE(O_error_message,
                               I_item) then
         return FALSE;
      end if;
   elsif LP_action = 'A' then
      if not ITEM_LOCNS_ACTIVATE(O_error_message,
                                 I_item ) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'REPL_ATTRIBUTE_MAINTENANCE_SQL.ITEM_LOCNS',
                                             to_char(SQLCODE));
      return FALSE;
END ITEM_LOCNS;
------------------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_PARENT_ITEM (O_error_message IN OUT VARCHAR2) RETURN BOOLEAN IS

 L_item ITEM_MASTER.ITEM%TYPE;
 L_ratio NUMBER(12,4);

   -- To explode all child items for a parent  item
   cursor C_ITEM is
       select ic.item,
              ic.item_parent,
              ic.item_grandparent
         from item_master ic,
              item_master ip
        where ic.tran_level = ic.item_level
          and ic.inventory_ind = 'Y'
          and ic.pack_ind = 'N'
          and nvl(ic.deposit_item_type,'-1') not in ('Z','A','P')
          and (ic.item = LP_item
           or ic.item_parent = LP_item
           or ic.item_grandparent = LP_item)
          and (ip.item = ic.item_parent
           or  ip.item = ic.item_grandparent)
          and ((decode(ip.diff_1_aggregate_ind, 'Y', ic.diff_1, '-999') = NVL(LP_diff_1, '-999')
          and decode(ip.diff_2_aggregate_ind, 'Y', ic.diff_2, '-999') = NVL(LP_diff_2, '-999')
          and decode(ip.diff_3_aggregate_ind, 'Y', ic.diff_3, '-999') = NVL(LP_diff_3, '-999')
          and decode(ip.diff_4_aggregate_ind, 'Y', ic.diff_4, '-999') = NVL(LP_diff_4, '-999'))
          or LP_diff_1||LP_diff_2||LP_diff_3||LP_diff_4 is NULL);

BEGIN

   FOR C_rec in C_ITEM LOOP
      LP_item_parent := C_rec.item_parent;
      LP_item_grandparent := C_rec.item_grandparent;
      L_item := C_rec.item;

      if not ITEM_LOCNS(O_error_message,
                        C_rec.item) then
         return FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'REPL_ATTRIBUTE_MAINTENANCE_SQL.EXPLODE_PARENT_ITEM',
                                             to_char(SQLCODE));
      return FALSE;
END EXPLODE_PARENT_ITEM;
------------------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEM_HIER (O_error_message IN OUT VARCHAR2) RETURN BOOLEAN IS

   L_item      ITEM_MASTER.ITEM%TYPE;
   L_ratio     NUMBER(12,4);

   L_program   VARCHAR2(64) := 'REPL_ATTRIBUTE_MAINTENANCE_SQL.EXPLODE_ITEM_HIER';

   -- To explode all items within a specific hierarchy
   cursor C_HIER is
      select im.item,
             im.item_parent,
             im.item_grandparent
        from item_master    im,
             item_supplier  isup,
             item_supp_country isc
       where im.tran_level = im.item_level
         and im.pack_ind = 'N'
         and im.inventory_ind = 'Y'
         and nvl(im.deposit_item_type,'-1') not in ('Z','A','P')
         and im.dept = LP_department
         and im.class = NVL(LP_class,im.class)
         and im.subclass = NVL(LP_subclass,im.subclass)
         and isup.item = im.item
         and isup.primary_supp_ind = 'Y'
         and isup.item = isc.item
         and isup.supplier = isc.supplier
         and im.forecast_ind = decode(LP_repl_method,'T','Y','D','Y','TI','Y','DI','Y',im.forecast_ind)
         and exists (select 'x'
                       from item_loc il
                      where il.item = im.item
                        and il.loc = decode(LP_source_wh,NULL,il.loc,LP_source_wh)
                        and il.loc_type = decode(LP_source_wh,NULL,il.loc_type,'W')
                        and il.status = decode(LP_source_wh,NULL,il.status,'A')
                        and rownum = 1);

BEGIN

   FOR C_rec in C_HIER LOOP
      LP_item_parent := C_rec.item_parent;
      LP_item_grandparent := C_rec.item_grandparent;
      L_item := C_rec.item;

      if NOT ITEM_LOCNS(O_error_message,
                        C_rec.item) then
        return FALSE;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXPLODE_ITEM_HIER;
------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_TYPE( O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_report_required            IN OUT   BOOLEAN,
                        O_exist                      IN OUT   VARCHAR2,
                        O_found                      IN OUT   BOOLEAN,
                        O_repl_attr_id               IN OUT   REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE,
                        I_action                     IN       VARCHAR2,
                        I_repl_method_update         IN       VARCHAR2,
                        I_item_level                 IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                        I_tran_level                 IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                        I_item                       IN       REPL_ITEM_LOC.ITEM%TYPE,
                        I_sch_active_date            IN       REPL_ATTR_UPDATE_HEAD.SCHEDULED_ACTIVE_DATE%TYPE,
                        I_mra_update                 IN       REPL_ATTR_UPDATE_HEAD.MRA_UPDATE%TYPE,
                        I_mra_restore                IN       REPL_ATTR_UPDATE_HEAD.MRA_RESTORE%TYPE,
                        I_group_type                 IN       VARCHAR2,
                        I_value                      IN       VARCHAR2,
                        I_repl_order_ctrl            IN       REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE,
                        I_stock_cat                  IN       REPL_ITEM_LOC.STOCK_CAT%TYPE,
                        I_activate_date              IN       REPL_ITEM_LOC.ACTIVATE_DATE%TYPE,
                        I_deactivate_date            IN       REPL_ITEM_LOC.DEACTIVATE_DATE%TYPE,
                        I_source_wh                  IN       REPL_ITEM_LOC.SOURCE_WH%TYPE,
                        I_pres_stock                 IN       REPL_ITEM_LOC.PRES_STOCK%TYPE,
                        I_demo_stock                 IN       REPL_ITEM_LOC.DEMO_STOCK%TYPE,
                        I_repl_method                IN       REPL_ITEM_LOC.REPL_METHOD%TYPE,
                        I_min_stock                  IN       REPL_ITEM_LOC.MIN_STOCK%TYPE,
                        I_max_stock                  IN       REPL_ITEM_LOC.MAX_STOCK%TYPE,
                        I_incr_pct                   IN       REPL_ITEM_LOC.INCR_PCT%TYPE,
                        I_min_supply_days            IN       REPL_ITEM_LOC.MIN_SUPPLY_DAYS%TYPE,
                        I_max_supply_days            IN       REPL_ITEM_LOC.MAX_SUPPLY_DAYS%TYPE,
                        I_time_supply_horizon        IN       REPL_ITEM_LOC.TIME_SUPPLY_HORIZON%TYPE,
                        I_inv_selling_days           IN       REPL_ITEM_LOC.INV_SELLING_DAYS%TYPE,
                        I_service_level              IN       REPL_ITEM_LOC.SERVICE_LEVEL%TYPE,
                        I_lost_sales_factor          IN       REPL_ITEM_LOC.LOST_SALES_FACTOR%TYPE,
                        I_non_scale_ind              IN       REPL_ITEM_LOC.NON_SCALING_IND%TYPE,
                        I_max_scale_value            IN       REPL_ITEM_LOC.MAX_SCALE_VALUE%TYPE,
                        I_pickup_lead_time           IN       REPL_ITEM_LOC.PICKUP_LEAD_TIME%TYPE,
                        I_wh_lead_time               IN       REPL_ITEM_LOC.WH_LEAD_TIME%TYPE,
                        I_terminal_stock_qty         IN       REPL_ITEM_LOC.TERMINAL_STOCK_QTY%TYPE,
                        I_season_id                  IN       REPL_ITEM_LOC.SEASON_ID%TYPE,
                        I_phase_id                   IN       REPL_ITEM_LOC.PHASE_ID%TYPE,
                        I_supplier                   IN       REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE,
                        I_origin_country_id          IN       REPL_ITEM_LOC.ORIGIN_COUNTRY_ID%TYPE,
                        I_review_cycle               IN       REPL_ITEM_LOC.REVIEW_CYCLE%TYPE,
                        I_service_level_type         IN       REPL_ATTR_UPDATE_HEAD.SERVICE_LEVEL_TYPE%TYPE,
                        I_update_days                IN       VARCHAR2,
                        I_monday                     IN       VARCHAR2,
                        I_tuesday                    IN       VARCHAR2,
                        I_wednesday                  IN       VARCHAR2,
                        I_thursday                   IN       VARCHAR2,
                        I_friday                     IN       VARCHAR2,
                        I_saturday                   IN       VARCHAR2,
                        I_sunday                     IN       VARCHAR2,
                        I_unit_tolerance             IN       REPL_ITEM_LOC.UNIT_TOLERANCE%TYPE,
                        I_pct_tolerance              IN       REPL_ITEM_LOC.PCT_TOLERANCE%TYPE,
                        I_use_tolerance_ind          IN       REPL_ITEM_LOC.USE_TOLERANCE_IND%TYPE,
                        I_user_id                    IN       USER_USERS.USERNAME%TYPE,
                        I_primary_pack_no            IN       REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE,
                        I_rej_st_ord_ind             IN       REPL_ITEM_LOC.REJECT_STORE_ORD_IND%TYPE,
                        I_default_pack_ind           IN       VARCHAR2,
                        I_remove_pack_ind            IN       VARCHAR2,
                        I_create_date                IN       REPL_ATTR_UPDATE_HEAD.CREATE_DATE%TYPE,
                        I_create_id                  IN       REPL_ATTR_UPDATE_HEAD.CREATE_ID%TYPE,
                        I_sch_repl_desc              IN       REPL_ATTR_UPDATE_HEAD.SCH_RPL_DESC%TYPE,
                        I_size_profile_ind           IN       VARCHAR2,
                        I_add_lead_time_ind          IN       REPL_ITEM_LOC.ADD_LEAD_TIME_IND%TYPE,
                        I_dept                       IN       ITEM_MASTER.DEPT%TYPE,
                        I_class                      IN       ITEM_MASTER.CLASS%TYPE,
                        I_subclass                   IN       ITEM_MASTER.SUBCLASS%TYPE,
                        I_diff_1                     IN       REPL_ATTR_UPDATE_ITEM.DIFF_1%TYPE,
                        I_diff_2                     IN       REPL_ATTR_UPDATE_ITEM.DIFF_2%TYPE,
                        I_diff_3                     IN       REPL_ATTR_UPDATE_ITEM.DIFF_3%TYPE,
                        I_diff_4                     IN       REPL_ATTR_UPDATE_ITEM.DIFF_4%TYPE,
                        I_supp_lead_time             IN       REPL_ITEM_LOC.SUPP_LEAD_TIME%TYPE,
                        I_multiple_runs_per_day_ind  IN       REPL_ITEM_LOC.MULT_RUNS_PER_DAY_IND%TYPE,
                        I_transfer_zero_soh_ind      IN       REPL_ITEM_LOC.TSF_ZERO_SOH_IND%TYPE)

   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'REPL_ATTRIBUTE_MAINTENANCE_SQL.GET_ITEM_TYPE';
   L_num1               NUMBER;
   L_num2               NUMBER;
   L_num_diff_comp      NUMBER;


   cursor C_PARENT is
      select item_parent,
             item_grandparent
        from item_master
       where item = I_item;

BEGIN

   -- Set variables that will be used by internal functions.

   LP_report_required              := FALSE;
   LP_action                       := I_action;
   LP_repl_method_update           := I_repl_method_update;
   LP_item_level                   := I_item_level;
   LP_tran_level                   := I_tran_level;
   LP_item                         := I_item;
   LP_group_type                   := I_group_type;
   LP_value                        := I_value;
   LP_repl_order_ctrl              := I_repl_order_ctrl;
   LP_stock_cat                    := I_stock_cat;
   LP_activate_date                := I_activate_date;
   LP_deactivate_date              := I_deactivate_date;
   LP_source_wh                    := I_source_wh;
   LP_pres_stock                   := I_pres_stock;
   LP_demo_stock                   := I_demo_stock;
   LP_repl_method                  := I_repl_method;
   LP_min_stock                    := I_min_stock;
   LP_max_stock                    := I_max_stock;
   LP_incr_pct                     := I_incr_pct;
   LP_min_supply_days              := I_min_supply_days;
   LP_max_supply_days              := I_max_supply_days;
   LP_time_supply_horizon          := I_time_supply_horizon;
   LP_inv_selling_days             := I_inv_selling_days;
   LP_service_level                := I_service_level;
   LP_lost_sales_factor            := I_lost_sales_factor;
   LP_non_scaling_ind              := I_non_scale_ind;
   LP_max_scale_value              := I_max_scale_value;
   LP_pickup_lead_time             := I_pickup_lead_time;
   LP_wh_lead_time                 := I_wh_lead_time;
   LP_terminal_stock_qty           := I_terminal_stock_qty;
   LP_season_id                    := I_season_id;
   LP_phase_id                     := I_phase_id;
   LP_supplier                     := I_supplier;
   LP_origin_country_id            := I_origin_country_id;
   LP_review_cycle                 := I_review_cycle;
   LP_update_days                  := I_update_days;
   LP_monday                       := I_monday;
   LP_tuesday                      := I_tuesday;
   LP_wednesday                    := I_wednesday;
   LP_thursday                     := I_thursday;
   LP_friday                       := I_friday;
   LP_saturday                     := I_saturday;
   LP_sunday                       := I_sunday;
   LP_unit_tolerance               := I_unit_tolerance;
   LP_pct_tolerance                := I_pct_tolerance;
   LP_use_tolerance_ind            := I_use_tolerance_ind;
   LP_user_id                      := I_user_id;
   LP_primary_pack_no              := I_primary_pack_no;
   LP_rej_st_ord_ind               := I_rej_st_ord_ind;
   LP_default_pack_ind             := I_default_pack_ind;
   LP_remove_pack_ind              := I_remove_pack_ind;

   LP_repl_attr_id                 := O_repl_attr_id;
   LP_sch_active_date              := I_sch_active_date;
   LP_mra_update                   := I_mra_update;
   LP_mra_restore                  := I_mra_restore;
   LP_service_level_type           := I_service_level_type;
   LP_create_date                  := I_create_date;
   LP_create_id                    := I_create_id;
   LP_sch_repl_desc                := I_sch_repl_desc;
   LP_size_profile_ind             := I_size_profile_ind;
   LP_add_lead_time_ind            := I_add_lead_time_ind;
   LP_department                   := I_dept;
   LP_class                        := I_class;
   LP_subclass                     := I_subclass;
   LP_diff_1                       := I_diff_1;
   LP_diff_2                       := I_diff_2;
   LP_diff_3                       := I_diff_3;
   LP_diff_4                       := I_diff_4;
   LP_multiple_runs_per_day_ind    := I_multiple_runs_per_day_ind;
   LP_tsf_zero_soh_ind             := I_transfer_zero_soh_ind;
   ---
   LP_orig_min_stock       := LP_min_stock;
   LP_orig_max_stock       := LP_max_stock;
   LP_orig_terminal_stock  := LP_terminal_stock_qty;
   LP_orig_pres_stock      := LP_pres_stock;
   LP_supp_lead_time       := I_supp_lead_time;


   if I_group_type != 'C' then
      if SQL_LIB.CHECK_NUMERIC(O_error_message,
                               I_value) = FALSE then
         return FALSE;
      end if;
   end if;

   -- Get system_options
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_rec) = FALSE then
      return FALSE;
   end if;

   LP_apply_pres_ind      := LP_system_options_rec.apply_prof_pres_stock;
   LP_repl_hist_ret_weeks := LP_system_options_rec.repl_attr_hist_retention_weeks;

   if I_sch_active_date is not NULL and
      (I_item_level != 0 or I_item_level is null) then

      if CREATE_SCHEDULED_UPDATE( O_error_message ,
                                  O_repl_attr_id,
                                  O_found ) = FALSE then
         return FALSE;
      end if;
   elsif I_item_level = 0 then

      if EXPLODE_ITEM_LIST( O_error_message ,
                            O_exist ,
                            O_found
                          ) = FALSE then
         return FALSE;
      end if;

   elsif I_item_level = I_tran_level then
      ---
      if I_item_level != 1 and LP_action = 'A' then
         open C_PARENT;
         fetch C_PARENT into LP_item_parent,
                             LP_item_grandparent;
         close C_PARENT;
      end if;
      ---
      if not ITEM_LOCNS(O_error_message,
                        I_item) then
         return FALSE;
      end if;

   elsif I_item_level != I_tran_level then
      if not EXPLODE_PARENT_ITEM(O_error_message) then
         return FALSE;
      end if;

  -- Get the items within a specific hierarchy dept/class/subclass
   elsif I_item is NULL then
      if not EXPLODE_ITEM_HIER(O_error_message) then
         return FALSE;
      end if;

   end if;

   if LP_report_required = TRUE then
      O_report_required := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'REPL_ATTRIBUTE_MAINTENANCE_SQL.GET_ITEM_TYPE',
                                             to_char(SQLCODE));
      return FALSE;
END GET_ITEM_TYPE;
---------------------------------------------------------------------------------------------------------
FUNCTION REPL_ITEM_LOC_UPDATES(O_error_message     IN OUT     VARCHAR2,
                               I_item              IN         ITEM_MASTER.ITEM%TYPE,
                               I_loc_type          IN         ITEM_LOC.LOC_TYPE%TYPE,
                               I_location          IN         ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN is

   L_supplier              REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE;
   L_origin_country_id     REPL_ITEM_LOC.ORIGIN_COUNTRY_ID%TYPE;
   L_stock_cat             REPL_ITEM_LOC.STOCK_CAT%TYPE;
   L_primary_pack_no       REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE;
   L_review_cycle          REPL_ITEM_LOC.REVIEW_CYCLE%TYPE;
   L_source_wh             REPL_ITEM_LOC.SOURCE_WH%TYPE;

   CURSOR C_GET_EXISTING_VALUES is
      select primary_repl_supplier,
             origin_country_id,
             stock_cat,
             primary_pack_no,
             review_cycle,
             source_wh
        from repl_item_loc
       where item = I_item
         and location = I_location;

BEGIN
   --- Insert to repl_item_loc_updates when activating a new item location on replenishment.
   if LP_action = 'A' then
      insert into repl_item_loc_updates (item,
                                supplier,
                                origin_country_id,
                                location,
                                loc_type,
                                change_type)
                           values(I_item,
                                         Null,
                                 Null,
                                  I_location,
                                    I_loc_type,
                                         'RIL');
   end if;

   if LP_action = 'U' then
      open C_GET_EXISTING_VALUES;
      fetch C_GET_EXISTING_VALUES into L_supplier,
                                       L_origin_country_id,
                                       L_stock_cat,
                                       L_primary_pack_no,
                                       L_review_cycle,
                                       L_source_wh;
      close C_GET_EXISTING_VALUES;

      --- Insert to repl_item_loc_updates when updating the supplier or origin country.
      if (LP_supplier is not Null and ((LP_supplier != L_supplier) or L_supplier is Null)) or
         (LP_origin_country_id is not Null and (LP_origin_country_id != L_origin_country_id)) then
         insert into repl_item_loc_updates (item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 loc_type,
                                 change_type)
                            values(I_item,
                                   Null,
                                 Null,
                                 I_location,
                                  I_loc_type,
                                  'RILSC');
      end if;

      --- Insert to repl_item_loc_updates when updating the primary replenishment pack.
      --- Only insert to repl_item_loc_updates if the pack was added
      if LP_reject_pack = 'N' then
         if (LP_primary_pack_no is not Null and ((LP_primary_pack_no != L_primary_pack_no) or L_primary_pack_no is Null)) or
            (L_primary_pack_no is not Null and LP_primary_pack_no is Null and LP_tran_level = LP_item_level and
            LP_group_type in ('S','W')) or (L_primary_pack_no is not Null and LP_remove_pack_ind = 'Y') then
            insert into repl_item_loc_updates (item,
                                    supplier,
                                    origin_country_id,
                                    location,
                                    loc_type,
                                    change_type)
                                values(I_item,
                                    Null,
                                    Null,
                                    I_location,
                                    I_loc_type,
                                    'RILP');
         end if;
      end if;

      --- Insert to repl_item_loc_updates when updating the review cycle or review days.
      if (LP_review_cycle is not Null and LP_review_cycle != L_review_cycle) or LP_update_days = 'Y' then
         insert into repl_item_loc_updates (item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 loc_type,
                                 change_type)
                            values(I_item,
                                 Null,
                                 Null,
                                 I_location,
                                 I_loc_type,
                                  'RILRC');
     end if;

      --- Insert to repl_item_loc_updates when updating the source warehouse.
      if (LP_source_wh is not Null and ((LP_source_wh != L_source_wh) or L_source_wh is Null)) then
         insert into repl_item_loc_updates (item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 loc_type,
                                 change_type)
                            values(I_item,
                                 Null,
                                 Null,
                                 I_location,
                                 I_loc_type,
                                 'RILSW');

      end if;

      --- Insert to repl_item_loc_updates when updating the stock category.
      if LP_stock_cat is not Null and LP_stock_cat != L_stock_cat then
         insert into repl_item_loc_updates (item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 loc_type,
                                 change_type)
                             values(I_item,
                                  Null,
                                  Null,
                                  I_location,
                                  I_loc_type,
                                 'RILST');

      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'REPL_ATTRIBUTE_MAINTENANCE_SQL.REPL_ITEM_LOC_UPDATES',
                                             to_char(SQLCODE));
      return FALSE;
END REPL_ITEM_LOC_UPDATES;
-------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SCH_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_repl_attr_id    IN       REPL_ATTR_UPDATE_ITEM.REPL_ATTR_ID%TYPE,
                           I_item            IN       REPL_ATTR_UPDATE_ITEM.ITEM%TYPE,
                           I_loc             IN       REPL_ATTR_UPDATE_LOC.LOC%TYPE,
                           I_loc_type        IN       REPL_ATTR_UPDATE_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN is

   L_program   VARCHAR2(61) := 'REPL_ATTRIBUTE_MAINTENANCE_SQL.DELETE_SCH_UPDATE';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_CHECK_UPD_EXISTS is
      select 'x'
        from repl_attr_update_exclude
       where repl_attr_id = I_repl_attr_id
         and item = I_item
         and location = I_loc
         and loc_type = I_loc_type;

BEGIN
   if I_repl_attr_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_repl_attr_id',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;
   if I_loc is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_loc',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;
   if I_loc_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_loc_type',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_item is NOT NULL then

      open C_CHECK_UPD_EXISTS;
      fetch C_CHECK_UPD_EXISTS into L_exists;
      close C_CHECK_UPD_EXISTS;

      if L_exists is NULL then
         -- writes the deleted item/location record to table repl_attr_update_exclude
         insert into repl_attr_update_exclude(repl_attr_id,
                                              item,
                                              location,
                                              loc_type)
                                       values(I_repl_attr_id,
                                              I_item,
                                              I_loc,
                                              I_loc_type);
      end if;
   else
       -- deletes the records from the update tables for merch level replenishment
       -- to avoid performance issue due to large number of items involved
     delete from repl_attr_update_loc
           where repl_attr_id = I_repl_attr_id;

     delete from repl_attr_update_item
           where repl_attr_id = I_repl_attr_id;

     delete from repl_attr_update_head
           where repl_attr_id = I_repl_attr_id;


   end if;
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_SCH_UPDATE;
-------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_REPL_ID( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                           O_repl_attr_id    IN OUT   REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE )
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'REPL_ATTRIBUTE_MAINTENANCE_SQL.GET_NEXT_REPL_ID';

   L_wrap_seq_no    REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE;
   L_first_time     VARCHAR2(3) := 'Yes';
   L_exists         VARCHAR2(3) := 'No';

   cursor C_ID_EXISTS is
      select 'Yes'
        from repl_attr_update_head
       where repl_attr_id = O_repl_attr_id
         and rownum       = 1;

   cursor C_NEXT_VAL is
      select replenishment_id_seq.nextval
        from dual;

BEGIN
   LOOP
      SQL_LIB.SET_MARK( 'OPEN' ,
                        'C_NEXT_VAL' ,
                        'DUAL' ,
                        NULL );
      open C_NEXT_VAL;

      SQL_LIB.SET_MARK( 'FETCH' ,
                        'C_NEXT_VAL' ,
                        'DUAL' ,
                        NULL );
      fetch C_NEXT_VAL into O_repl_attr_id;

      SQL_LIB.SET_MARK( 'CLOSE',
                        'C_NEXT_VAL' ,
                        'DUAL' ,
                        NULL );
      close C_NEXT_VAL;

      if L_first_time = 'Yes' then
         L_wrap_seq_no := O_repl_attr_id;
         L_first_time  := 'No';
      elsif O_repl_attr_id = L_wrap_seq_no then
         O_error_message := SQL_LIB.CREATE_MSG( 'NO_SEQ_NO_AVAIL', NULL, NULL, NULL );
         return FALSE;
      end if;

      SQL_LIB.SET_MARK( 'OPEN' ,
                        'C_ID_EXISTS' ,
                        'REPL_ATTR_UPDATE_HEAD' ,
                        'REPL_ATTR_ID: ' || TO_CHAR( O_repl_attr_id ) );
      open C_ID_EXISTS;

      SQL_LIB.SET_MARK( 'FETCH' ,
                        'C_ID_EXISTS' ,
                        'REPL_ATTR_UPDATE_HEAD' ,
                        'REPL_ATTR_ID: ' || TO_CHAR( O_repl_attr_id ) );
      fetch C_ID_EXISTS into L_exists;

      SQL_LIB.SET_MARK( 'CLOSE' ,
                        'C_ID_EXISTS' ,
                        'REPL_ATTR_UPDATE_HEAD' ,
                        'REPL_ATTR_ID: ' || TO_CHAR( O_repl_attr_id ) );
      close C_ID_EXISTS;

      if L_exists = 'No' then
         return TRUE;
      else
         O_error_message := SQL_LIB.CREATE_MSG( 'WIP_SEQ_EXIST', NULL, L_program, NULL );
    return FALSE;
      end if;

   END LOOP;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR' ,
                                             SQLERRM         ,
                                             L_program       ,
                                             to_char( SQLCODE ) );
      return FALSE;

END GET_NEXT_REPL_ID;
------------------------------------------------------------------------------------------------------

FUNCTION CREATE_SCHEDULED_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_repl_attr_id    IN OUT   REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE,
                                 O_found           IN OUT   BOOLEAN)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'REPL_ATTRIBUTE_MAINTENANCE_SQL.CREATE_SCHEDULED_UPDATE';
   L_repl_attr_id   REPL_ATTR_UPDATE_ITEM.REPL_ATTR_ID%TYPE;
   L_repl_id        REPL_ATTR_UPDATE_ITEM.REPL_ATTR_ID%TYPE;
   L_loc_type       REPL_ATTR_UPDATE_EXCLUDE.LOC_TYPE%TYPE;

   cursor C_REPL_ATTR_ID_EXISTS is
      select h.repl_attr_id
        from repl_attr_update_loc  l,
             repl_attr_update_item i,
             repl_attr_update_head h
       where h.repl_attr_id                     = L_repl_attr_id
         and i.dept                             = LP_department
         and NVL(i.class,-1)                    = NVL(LP_class,-1)
         and NVL(i.subclass,-1)                 = NVL(LP_subclass,-1)
         and NVL(i.item,-1)                     = NVL(LP_item,-1)
         and i.repl_attr_id                     = h.repl_attr_id
         and l.loc_type is not null
         and l.repl_attr_id                     = i.repl_attr_id
         and l.loc in ( select l2.loc
                        from repl_attr_update_loc  l2,
                             repl_attr_update_item i2,
                             repl_attr_update_head h2
                       where h2.repl_attr_id          != h.repl_attr_id
                         and i2.dept                   = i.dept
                         and NVL(i2.class,-1)          = NVL(i.class,-1)
                         and NVL(i2.subclass,-1)       = NVL(i.subclass,-1)
                         and NVL(i2.item,-1)           = NVL(i.item,-1)
                         and i2.repl_attr_id           = h2.repl_attr_id
                         and l2.loc_type is not null
                         and l2.loc                    = l.loc
                         and l2.repl_attr_id           = i2.repl_attr_id
                         and rownum                    = 1 )
         and rownum = 1;

    cursor C_REPL_LOC_SCH_EXISTS is
        select rauh.repl_attr_id,
               rau.item,
               ral.loc,
               ral.loc_type
               from repl_attr_update_head rauh,
                    repl_attr_update_item rau,
                    repl_attr_update_loc ral
              where rauh.repl_attr_id = LP_repl_attr_id
                and rau.dept                              = LP_department
                and NVL(rau.class,-1)                     = NVL(LP_class,-1)
                and NVL(rau.subclass,-1)                  = NVL(LP_subclass,-1)
                and NVL(rau.item,-1)                      = NVL(LP_item,-1)
                and ral.loc                               = to_number(LP_value)
                and rau.repl_attr_id                      = rauh.repl_attr_id
                and ral.repl_attr_id                      = rauh.repl_attr_id
                and ral.repl_attr_id not in (select  repl_attr_id
                                               from  repl_attr_update_exclude
                                              where  item = LP_item
                                                and  loc = to_number(LP_value))
                and rownum                     = 1;

L_repl_loc_sch_exists_rec C_REPL_LOC_SCH_EXISTS%ROWTYPE;

BEGIN
   if LP_group_type in ('S', 'W') then
      SQL_LIB.SET_MARK('OPEN' ,
                       'C_REPL_LOC_SCH_EXISTS' ,
                       'REPL_ATTR_UPDATE_HEAD, REPL_ATTR_UPDATE_ITEM, REPL_ATTR_UPDATE_LOC' ,
                        NULL );
      open C_REPL_LOC_SCH_EXISTS;
      SQL_LIB.SET_MARK('FETCH' ,
                       'C_REPL_LOC_SCH_EXISTS' ,
                       'REPL_ATTR_UPDATE_HEAD, REPL_ATTR_UPDATE_ITEM, REPL_ATTR_UPDATE_LOC' ,
                        NULL );
      fetch C_REPL_LOC_SCH_EXISTS into L_repl_loc_sch_exists_rec;
      SQL_LIB.SET_MARK('CLOSE' ,
                       'C_REPL_LOC_SCH_EXISTS' ,
                       'REPL_ATTR_UPDATE_HEAD, REPL_ATTR_UPDATE_ITEM, REPL_ATTR_UPDATE_LOC' ,
                       NULL );
      close C_REPL_LOC_SCH_EXISTS;

      if L_repl_loc_sch_exists_rec.repl_attr_id is not null then
         if DELETE_SCH_UPDATE(O_error_message,
                              L_repl_loc_sch_exists_rec.repl_attr_id,
                              L_repl_loc_sch_exists_rec.item,
                              L_repl_loc_sch_exists_rec.loc,
                              L_repl_loc_sch_exists_rec.loc_type) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   -- get the sequence number
   if GET_NEXT_REPL_ID( O_error_message,
                        L_repl_attr_id ) = FALSE THEN
      return FALSE;
   end if;

   if LP_item is not null then
      -- insert replenishment records based on global variables
      insert into repl_attr_update_head(repl_attr_id,
                                     scheduled_active_date,
                                     action,
                                     mra_update,
                                     mra_restore,
                                     repl_method_ind,
                                     stock_cat,
                                     repl_order_ctrl,
                                     sourcing_wh,
                                     activate_date,
                                     deactivate_date,
                                     pres_stock,
                                     demo_stock,
                                     repl_method,
                                     min_stock,
                                     max_stock,
                                     incr_pct,
                                     min_supply_days,
                                     max_supply_days,
                                     time_supply_horizon,
                                     inv_selling_days,
                                     service_level,
                                     lost_sales_factor,
                                     reject_store_ord_ind,
                                     non_scaling_ind,
                                     max_scale_value,
                                     pickup_lead_time,
                                     wh_lead_time,
                                     terminal_stock_qty,
                                     season_id,
                                     phase_id,
                                     supplier,
                                     origin_country_id,
                                     review_cycle,
                                     update_days_ind,
                                     monday_ind,
                                     tuesday_ind,
                                     wednesday_ind,
                                     thursday_ind,
                                     friday_ind,
                                     saturday_ind,
                                     sunday_ind,
                                     unit_tolerance,
                                     pct_tolerance,
                                     use_tolerance_ind,
                                     primary_pack_no,
                                     default_pack_ind,
                                     remove_pack_ind,
                                     service_level_type,
                                     create_date,
                                     create_id,
                                     sch_rpl_desc,
                                     size_profile_ind,
                                     mult_runs_per_day_ind,
                                     tsf_zero_soh_ind,
                                     add_lead_time_ind)
                              select L_repl_attr_id,
                                     LP_sch_active_date,
                                     LP_action,
                                     LP_mra_update,
                                     LP_mra_restore,
                                     LP_repl_method_update,
                                     LP_stock_cat,
                                     LP_repl_order_ctrl,
                                     LP_source_wh,
                                     LP_activate_date,
                                     LP_deactivate_date,
                                     LP_pres_stock,
                                     LP_demo_stock,
                                     LP_repl_method,
                                     LP_min_stock,
                                     LP_max_stock,
                                     LP_incr_pct,
                                     LP_min_supply_days,
                                     LP_max_supply_days,
                                     LP_time_supply_horizon,
                                     LP_inv_selling_days,
                                     LP_service_level,
                                     LP_lost_sales_factor,
                                     LP_rej_st_ord_ind,
                                     LP_non_scaling_ind,
                                     LP_max_scale_value,
                                     LP_pickup_lead_time,
                                     LP_wh_lead_time,
                                     LP_terminal_stock_qty,
                                     LP_season_id,
                                     LP_phase_id,
                                     LP_supplier,
                                     LP_origin_country_id,
                                     LP_review_cycle,
                                     LP_update_days,
                                     nvl(LP_monday,'N'),
                                     nvl(LP_tuesday,'N'),
                                     nvl(LP_wednesday,'N'),
                                     nvl(LP_thursday,'N'),
                                     nvl(LP_friday,'N'),
                                     nvl(LP_saturday,'N'),
                                     nvl(LP_sunday,'N'),
                                     LP_unit_tolerance,
                                     LP_pct_tolerance,
                                     LP_use_tolerance_ind,
                                     LP_primary_pack_no,
                                     LP_default_pack_ind,
                                     LP_remove_pack_ind,
                                     LP_service_level_type,
                                     GET_VDATE,
                                     GET_USER,
                                     LP_sch_repl_desc,
                                     LP_size_profile_ind,
                                     LP_multiple_runs_per_day_ind,
                                     LP_tsf_zero_soh_ind,
                                     LP_add_lead_time_ind
                                from item_supplier its,
                                     item_master im
                               where its.item = LP_item
                                 and its.item = im.item
                                 and ((its.supplier = LP_supplier
                                        and LP_supplier is NOT NULL)
                                     or (LP_supplier is NULL
                                        and its.primary_supp_ind = 'Y'))
                                 and im.pack_ind = 'N'
                                 and its.consignment_rate is NULL;
   else
      insert into repl_attr_update_head(repl_attr_id,
                                     scheduled_active_date,
                                     action,
                                     mra_update,
                                     mra_restore,
                                     repl_method_ind,
                                     stock_cat,
                                     repl_order_ctrl,
                                     sourcing_wh,
                                     activate_date,
                                     deactivate_date,
                                     pres_stock,
                                     demo_stock,
                                     repl_method,
                                     min_stock,
                                     max_stock,
                                     incr_pct,
                                     min_supply_days,
                                     max_supply_days,
                                     time_supply_horizon,
                                     inv_selling_days,
                                     service_level,
                                     lost_sales_factor,
                                     reject_store_ord_ind,
                                     non_scaling_ind,
                                     max_scale_value,
                                     pickup_lead_time,
                                     wh_lead_time,
                                     terminal_stock_qty,
                                     season_id,
                                     phase_id,
                                     supplier,
                                     origin_country_id,
                                     review_cycle,
                                     update_days_ind,
                                     monday_ind,
                                     tuesday_ind,
                                     wednesday_ind,
                                     thursday_ind,
                                     friday_ind,
                                     saturday_ind,
                                     sunday_ind,
                                     unit_tolerance,
                                     pct_tolerance,
                                     use_tolerance_ind,
                                     primary_pack_no,
                                     default_pack_ind,
                                     remove_pack_ind,
                                     service_level_type,
                                     create_date,
                                     create_id,
                                     sch_rpl_desc,
                                     size_profile_ind,
                                     mult_runs_per_day_ind,
                                     tsf_zero_soh_ind,
                                     add_lead_time_ind)
                             values (L_repl_attr_id,
                                     LP_sch_active_date,
                                     LP_action,
                                     LP_mra_update,
                                     LP_mra_restore,
                                     LP_repl_method_update,
                                     LP_stock_cat,
                                     LP_repl_order_ctrl,
                                     LP_source_wh,
                                     LP_activate_date,
                                     LP_deactivate_date,
                                     LP_pres_stock,
                                     LP_demo_stock,
                                     LP_repl_method,
                                     LP_min_stock,
                                     LP_max_stock,
                                     LP_incr_pct,
                                     LP_min_supply_days,
                                     LP_max_supply_days,
                                     LP_time_supply_horizon,
                                     LP_inv_selling_days,
                                     LP_service_level,
                                     LP_lost_sales_factor,
                                     LP_rej_st_ord_ind,
                                     LP_non_scaling_ind,
                                     LP_max_scale_value,
                                     LP_pickup_lead_time,
                                     LP_wh_lead_time,
                                     LP_terminal_stock_qty,
                                     LP_season_id,
                                     LP_phase_id,
                                     LP_supplier,
                                     LP_origin_country_id,
                                     LP_review_cycle,
                                     LP_update_days,
                                     nvl(LP_monday,'N'),
                                     nvl(LP_tuesday,'N'),
                                     nvl(LP_wednesday,'N'),
                                     nvl(LP_thursday,'N'),
                                     nvl(LP_friday,'N'),
                                     nvl(LP_saturday,'N'),
                                     nvl(LP_sunday,'N'),
                                     LP_unit_tolerance,
                                     LP_pct_tolerance,
                                     LP_use_tolerance_ind,
                                     LP_primary_pack_no,
                                     LP_default_pack_ind,
                                     LP_remove_pack_ind,
                                     LP_service_level_type,
                                     GET_VDATE,
                                     GET_USER,
                                     LP_sch_repl_desc,
                                     LP_size_profile_ind,
                                     NVL(LP_multiple_runs_per_day_ind, 'N'),
                                     NVL(LP_tsf_zero_soh_ind, 'N'),
                                     LP_add_lead_time_ind);
   end if;

   insert into repl_attr_update_item(repl_attr_id,
                                     item,
                                     dept,
                                     class,
                                     subclass,
                                     diff_1,
                                     diff_2,
                                     diff_3,
                                     diff_4)
                              values(L_repl_attr_id,
                                     LP_item,
                                     LP_department,
                                     LP_class,
                                     LP_subclass,
                                     LP_diff_1,
                                     LP_diff_2,
                                     LP_diff_3,
                                     LP_diff_4);
   if LP_group_type = 'S' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     LP_value
                from store
               where store            = LP_value
                 and ((store_type in ('W','F') and stockholding_ind = 'N') or stockholding_ind = 'Y');
      elsif LP_group_type = 'C' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store s
               where s.store_class      = LP_value
                 and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y');
      elsif LP_group_type = 'D' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store_hierarchy sh ,
                     store s
               where sh.district        = LP_value
                 and sh.store           = s.store
                 and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y');
      elsif LP_group_type = 'R' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store_hierarchy sh ,
                     store s
               where sh.region          = LP_value
                 and sh.store           = s.store
                 and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y');
      elsif LP_group_type = 'T' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store s
               where s.transfer_zone    = LP_value
                 and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y');
      elsif LP_group_type = 'L' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     ltm.store
                from loc_traits_matrix ltm ,
                     store s
               where ltm.loc_trait      = LP_value
                 and ltm.store          = s.store
                 and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y');
      elsif LP_group_type = 'DW' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store s
               where s.default_wh       = LP_value
                 and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y');
      elsif LP_group_type = 'AS' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store s
               where s.stockholding_ind = 'Y';
      elsif LP_group_type =  'W' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'W'            ,
                     w.wh
                from wh w
               where w.wh               = LP_value
                 and w.stockholding_ind = 'Y'
                 and w.repl_ind         = 'Y';
      elsif LP_group_type = 'AW' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'W'            ,
                     wh.wh
                from wh
               where stockholding_ind = 'Y'
                 and repl_ind         = 'Y'
                 and finisher_ind     = 'N';
      elsif LP_group_type = 'LLS' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     ld.loc_type     ,
                     ld.location
                from loc_list_detail ld ,
                     loc_list_head lh ,
                     store s
               where ld.loc_list         = lh.loc_list
                 and ld.loc_list         = LP_value
                 and ld.location         = s.store
                 and ((s.store_type in ('F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y')
                 and ld.loc_type         = 'S';
      elsif LP_group_type = 'LLW' then
         insert into repl_attr_update_loc
            ( loc          ,
              repl_attr_id ,
              loc_type
            ) select distinct w.wh  ,
                     L_repl_attr_id ,
                     l.loc_type
                from loc_list_detail l ,
                     wh w
               where (     l.loc_list = LP_value
                       and l.location = w.wh
                       and w.stockholding_ind = 'Y'
                       and w.repl_ind = 'Y' )
                  or (     l.loc_list = LP_value
                       and w.physical_wh = l.location
                       and w.stockholding_ind = 'Y'
                       and w.repl_ind = 'Y' )
                 and finisher_ind = 'N';
      elsif LP_group_type = 'A' then
         insert into repl_attr_update_loc
            (
              repl_attr_id ,
              loc_type     ,
              loc
            ) select
                     L_repl_attr_id ,
                     'S'            ,
                     s.store
                from store_hierarchy sh ,
                     store s
               where sh.area            = LP_value
                 and sh.store           = s.store
                 and ((s.store_type in ('W','F') and s.stockholding_ind = 'N') or s.stockholding_ind = 'Y');
      end if;

      SQL_LIB.SET_MARK('OPEN' ,
                       'C_REPL_ATTR_ID_EXISTS' ,
                       'REPL_ATTR_UPDATE_HEAD, REPL_ATTR_UPDATE_ITEM, REPL_ATTR_UPDATE_LOC' ,
                       NULL );
      open C_REPL_ATTR_ID_EXISTS;
      SQL_LIB.SET_MARK('FETCH' ,
                       'C_REPL_ATTR_ID_EXISTS' ,
                       'REPL_ATTR_UPDATE_HEAD, REPL_ATTR_UPDATE_ITEM, REPL_ATTR_UPDATE_LOC' ,
                       NULL );
      fetch C_REPL_ATTR_ID_EXISTS into L_repl_id;
      SQL_LIB.SET_MARK('CLOSE' ,
                       'C_REPL_ATTR_ID_EXISTS' ,
                       'REPL_ATTR_UPDATE_HEAD, REPL_ATTR_UPDATE_ITEM, REPL_ATTR_UPDATE_LOC' ,
                       NULL );
      close C_REPL_ATTR_ID_EXISTS;

      if L_repl_id is NOT NULL then
         O_found := TRUE;
      else
         O_found := FALSE;
      end if;

      O_repl_attr_id := L_repl_attr_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char( SQLCODE));
      return FALSE;

END CREATE_SCHEDULED_UPDATE;
------------------------------------------------------------------------------------------------------
FUNCTION INSERT_REPL_HIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_repl_hist_row IN     REPL_ATTR_UPD_HIST%ROWTYPE)
RETURN BOOLEAN IS

        L_repl_attr_hist_row      REPL_ATTR_UPD_HIST%ROWTYPE;
        L_program                 VARCHAR2(100);
        cursor C_NEXT_REPL_HIST_ID is
            select REPL_ATTR_UPD_ID_SEQ.NEXTVAL,
                   get_user,
                   sysdate,
                   to_char(sysdate,'YYYY')||to_char(sysdate,'WW')
              from dual;
BEGIN

   L_program := 'REPL_ATTRIBUTE_MAINTENANCE_SQL.INSERT_REPL_HIST';
   -- Assign values from input pamameter to a local variable
   L_repl_attr_hist_row := I_repl_hist_row;

   open C_NEXT_REPL_HIST_ID;
   fetch C_NEXT_REPL_HIST_ID into L_repl_attr_hist_row.repl_attr_upd_id,
                                  L_repl_attr_hist_row.last_update_id,
                                  L_repl_attr_hist_row.last_update_datetime,
                                  L_repl_attr_hist_row.week_num;
   close C_NEXT_REPL_HIST_ID;

   -- Insert changes in the replenishment attributes
   insert into repl_attr_upd_hist
      values L_repl_attr_hist_row;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char( SQLCODE));
      return FALSE;
END INSERT_REPL_HIST;
------------------------------------------------------------------------------------------------------
FUNCTION GET_MASTER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_master_item     IN OUT   ITEM_MASTER.ITEM%TYPE,
                         I_item            IN       ITEM_MASTER.ITEM%TYPE,
                         I_location_type   IN       REPL_RESULTS.LOC_TYPE%TYPE,
                         I_location        IN       REPL_RESULTS.LOCATION%TYPE,
                         I_alloc_no        IN       REPL_RESULTS.ALLOC_NO%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                         I_repl_date       IN       REPL_RESULTS.REPL_DATE%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'REPL_ATTRIBUTE_MAINTENANCE_SQL.GET_MASTER_ITEM';

   cursor C_GET_MASTER is
      select master_item
        from repl_results
       where item       = I_item
         and loc_type   = I_location_type
         and location   = I_location
         and (I_alloc_no is null or
              alloc_no  = I_alloc_no)
         and (I_order_no is null or
              order_no  = I_order_no)
         and (I_repl_date is null or
              repl_date = I_repl_date);

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_location_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_location_type',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_location',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   open C_GET_MASTER;
   fetch C_GET_MASTER into O_master_item;
   close C_GET_MASTER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_MASTER_ITEM;
------------------------------------------------------------------------------------------------------
END REPL_ATTRIBUTE_MAINTENANCE_SQL;
/

