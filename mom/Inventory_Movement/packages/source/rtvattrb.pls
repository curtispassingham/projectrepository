CREATE OR REPLACE PACKAGE BODY RTV_ATTRIB_SQL AS
--------------------------------------------------------------------
FUNCTION DECODE_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_status_ind      IN       NUMBER,
                       O_status_decode   IN OUT   VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'RTV_ATTRIB_SQL.DECODE_STATUS';
BEGIN
   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'RVST',
                                 to_char(I_status_ind),
                                 O_status_decode) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
   O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
   return FALSE;

END DECODE_STATUS;
--------------------------------------------------------------------
FUNCTION GET_SUPP_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_supplier        IN       NUMBER,
                       O_sup_name        IN OUT   VARCHAR2,
                       O_contact_name    IN OUT   VARCHAR2,
                       O_contact_phone   IN OUT   VARCHAR2,
                       O_contact_fax     IN OUT   VARCHAR2,
                       O_address_1       IN OUT   VARCHAR2,
                       O_address_2       IN OUT   VARCHAR2,
                       O_address_3       IN OUT   VARCHAR2,
                       O_city            IN OUT   VARCHAR2,
                       O_state           IN OUT   VARCHAR2,
                       O_post_code       IN OUT   VARCHAR2,
                       O_country         IN OUT   VARCHAR2,
                       O_jurisdiction    IN OUT   ADDR.JURISDICTION_CODE%TYPE,
                       O_min_dol_amt     IN OUT   NUMBER,
                       O_ret_courier     IN OUT   VARCHAR2,
                       O_handling_pct    IN OUT   NUMBER)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'RTV_ATTRIB_SQL.GET_SUPP_INFO';

   cursor C_ADDRESS is
      select contact_name,
             contact_phone,
             contact_fax,
             add_1,
             add_2,
             add_3,
             city,
             state,
             post,
             country_id,
             jurisdiction_code
        from addr
       where module = 'SUPP'
         and key_value_1 = to_char(I_supplier)
         and addr_type = '03'
         and primary_addr_ind = 'Y';

   cursor C_SUPS is
      select vsp.sup_name,
             sp.ret_min_dol_amt,
             sp.ret_courier,
             sp.handling_pct
        from sups sp,
             v_sups_tl vsp
       where sp.supplier = I_supplier 
         and sp.supplier = vsp.supplier;

BEGIN

   open C_SUPS;
   fetch C_SUPS into O_sup_name,
                     O_min_dol_amt,
                     O_ret_courier,
                     O_handling_pct;
   if C_SUPS%NOTFOUND then
      close C_SUPS;
      O_error_message := sql_lib.create_msg('INV_SUP_NUM',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   close C_SUPS;

   open C_ADDRESS;
   fetch C_ADDRESS into O_contact_name,
                        O_contact_phone,
                        O_contact_fax,
                        O_address_1,
                        O_address_2,
                        O_address_3,
                        O_city,
                        O_state,
                        O_post_code,
                        O_country,
                        O_jurisdiction;
   if C_ADDRESS%NOTFOUND then
      close C_ADDRESS;
      O_error_message := sql_lib.create_msg('NO_RET_ADDR',
                                            I_supplier,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   close C_ADDRESS;


   return TRUE;

EXCEPTION
   when OTHERS then
   O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
   return FALSE;

END GET_SUPP_INFO;
--------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rtv_order_no    IN       NUMBER,
                         O_supplier        IN OUT   NUMBER,
                         O_store           IN OUT   NUMBER,
                         O_wh              IN OUT   NUMBER,
                         O_status          IN OUT   NUMBER,
                         O_create_date     IN OUT   DATE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'RTV_ATTRIB_SQL.GET_HEADER_INFO';

   cursor C_HEADER_INFO is
      select supplier,
             store,
             wh,
             status_ind,
             created_date
        from rtv_head
       where rtv_order_no = I_rtv_order_no;

BEGIN

   open C_HEADER_INFO;
   fetch C_HEADER_INFO into O_supplier,
                            O_store,
                            O_wh,
                            O_status,
                            O_create_date;
   if C_HEADER_INFO%NOTFOUND then
      close C_HEADER_INFO;
      O_error_message := sql_lib.create_msg('INV_RTV',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   close C_HEADER_INFO;

   return TRUE;

EXCEPTION
   when OTHERS then
   O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
   return FALSE;

END GET_HEADER_INFO;
--------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_supplier        IN OUT   RTV_HEAD.SUPPLIER%TYPE,
                         O_ret_auth_num    IN OUT   RTV_HEAD.RET_AUTH_NUM%TYPE,
                         O_created_date    IN OUT   RTV_HEAD.CREATED_DATE%TYPE,
                         O_courier         IN OUT   RTV_HEAD.COURIER%TYPE,
                         O_restock_pct     IN OUT   RTV_HEAD.RESTOCK_PCT%TYPE,
                         I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'RTV_ATTRIB_SQL.GET_HEADER_INFO';
   cursor C_GET_INFO is
    select supplier,
           ret_auth_num,
           created_date,
           courier,
           restock_pct
      from rtv_head
     where rtv_order_no = I_rtv_order_no;

BEGIN
   if I_rtv_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_rtv_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('open','C_GET_INFO','rtv_head',
                    'RTV NO.:'||to_char(I_rtv_order_no));
   open C_GET_INFO;
   SQL_LIB.SET_MARK('fetch','C_GET_INFO','rtv_head',
                    'RTV NO.:'||to_char(I_rtv_order_no));
   fetch C_GET_INFO into O_supplier,
                         O_ret_auth_num,
                         O_created_date,
                         O_courier,
                         O_restock_pct;
   if C_GET_INFO%NOTFOUND then
      SQL_LIB.SET_MARK('close','C_GET_INFO','rtv_head',
                       'RTV NO.:'||to_char(I_rtv_order_no));
      close C_GET_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('INV_RTV',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('close','C_GET_INFO','rtv_head',
                    'RTV NO.:'||to_char(I_rtv_order_no));
   close C_GET_INFO;
   --- 
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;

END GET_HEADER_INFO;
--------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_RTV_DETAIL
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION RTV_FILTER_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diff            IN OUT   VARCHAR2,
                         I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   L_program   VARCHAR2(64)   := 'ORDER_VALIDATE_SQL.ORDSKU_FILTER_LIST';

   cursor C_COUNT_ORDER IS
      SELECT count(rtv_order_no)
      FROM   rtv_detail
      where  rtv_order_no = I_rtv_order_no;

   cursor C_COUNT_VORDER IS
      SELECT count(rtv_order_no)
      FROM   v_rtv_detail
      WHERE  rtv_order_no = I_rtv_order_no;

   L_order  NUMBER(6) := -765547;
   L_vorder NUMBER(6) := -987436;

BEGIN
   open C_COUNT_ORDER;
   fetch C_COUNT_ORDER INTO L_order;
   close C_COUNT_ORDER;

   open C_COUNT_VORDER;
   fetch C_COUNT_VORDER INTO L_vorder;
   close C_COUNT_VORDER;

   if L_vorder != L_order then
     O_diff := 'Y';
   else
     O_diff := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END RTV_FILTER_LIST;
---------------------------------------------------------------------
FUNCTION GET_HANDLING_PCT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_handling_pct    IN OUT   SUPS.HANDLING_PCT%TYPE,
                          I_item            IN       ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'RTV_ATTRIB_SQL.GET_HANDLING_PCT';
   
   cursor C_HANDLING_PCT is
      select s.handling_pct
        from sups s,
             item_supplier its
       where s.supplier = its.supplier
         and its.item = I_item
         and its.primary_supp_ind = 'Y';

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_item',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_HANDLING_PCT',
                    'ITEM_SUPPLIER',
                    'ITEM:'||I_item);
   open C_HANDLING_PCT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_HANDLING_PCT',
                    'ITEM_SUPPLIER',
                    'ITEM:'||I_item);
   fetch C_HANDLING_PCT into O_handling_pct;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_HANDLING_PCT',
                    'ITEM_SUPPLIER',
                    'ITEM:'||I_item);
   close C_HANDLING_PCT;   

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_HANDLING_PCT;
---------------------------------------------------------------------
END RTV_ATTRIB_SQL;
/
