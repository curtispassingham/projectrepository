
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TRANSIT_TIMES_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------
-- Function Name: NEXT_TRANSIT_TIMES_ID
-- Purpose:       This function will return the next transit times id
--                to be used for the insert to table TRANSIT_TIMES.
-------------------------------------------------------------------------------------------------
FUNCTION NEXT_TRANSIT_TIMES_ID(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_tran_times_id      IN OUT   TRANSIT_TIMES.TRANSIT_TIMES_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: TRANTIME_DUP_CHECK
-- Purpose:       This function checks for duplicate records on the TRANSIT_TIMES table.
-------------------------------------------------------------------------------------------------
FUNCTION TRANTIME_DUP_CHECK(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist              IN OUT   BOOLEAN,
                            I_dept               IN       TRANSIT_TIMES.DEPT%TYPE,
                            I_class              IN       TRANSIT_TIMES.CLASS%TYPE,
                            I_subclass           IN       TRANSIT_TIMES.SUBCLASS%TYPE,
                            I_origin_type        IN       TRANSIT_TIMES.ORIGIN_TYPE%TYPE,
                            I_origin             IN       TRANSIT_TIMES.ORIGIN%TYPE,
                            I_destination_type   IN       TRANSIT_TIMES.DESTINATION_TYPE%TYPE,
                            I_destination        IN       TRANSIT_TIMES.DESTINATION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: TRANTIME_APPLY
-- Purpose:       This function insert/updates the TRANSIT_TIMES table.
-------------------------------------------------------------------------------------------------
FUNCTION TRANTIME_APPLY(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_overwrite          IN       BOOLEAN,
                        I_transit_times_id   IN       TRANSIT_TIMES.TRANSIT_TIMES_ID%TYPE,
                        I_dept               IN       TRANSIT_TIMES.DEPT%TYPE,
                        I_class              IN       TRANSIT_TIMES.CLASS%TYPE,
                        I_subclass           IN       TRANSIT_TIMES.SUBCLASS%TYPE,
                        I_origin_type        IN       TRANSIT_TIMES.ORIGIN_TYPE%TYPE,
                        I_origin             IN       TRANSIT_TIMES.ORIGIN%TYPE,
                        I_destination_type   IN       TRANSIT_TIMES.DESTINATION_TYPE%TYPE,
                        I_destination        IN       TRANSIT_TIMES.DESTINATION%TYPE,
                        I_transit_time       IN       TRANSIT_TIMES.TRANSIT_TIME%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: TRANTIME_DELETE
-- Purpose:       This function deletes from the TRANSIT_TIMES table.
-------------------------------------------------------------------------------------------------
FUNCTION TRANTIME_DELETE(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_transit_times_id    IN      TRANSIT_TIMES.TRANSIT_TIMES_ID%TYPE,
                         I_origin              IN      TRANSIT_TIMES.ORIGIN%TYPE,
                         I_destination         IN      TRANSIT_TIMES.DESTINATION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: TRANTIME_FILTER_LIST
-- Purpose:       This function compares the count between the transit_time view and base table.
-------------------------------------------------------------------------------------------------
FUNCTION TRANTIME_FILTER_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_diff            IN OUT   VARCHAR2,
                              I_dept            IN       TRANSIT_TIMES.DEPT%TYPE,
                              I_class           IN       TRANSIT_TIMES.CLASS%TYPE,
                              I_subclass        IN       TRANSIT_TIMES.SUBCLASS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------

FUNCTION GET_ROW(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists              IN OUT   BOOLEAN,
                 O_transit_times_row   IN OUT   TRANSIT_TIMES%ROWTYPE,
                 I_supplier            IN       TRANSIT_TIMES.ORIGIN%TYPE,
                 I_location            IN       TRANSIT_TIMES.DESTINATION%TYPE,
                 I_item                IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: EXPLODE_LOC_LIST
-- Purpose:       This function will be responsible in exploding the location list down to the 
--                store/warehouse level before it is inserted into the TRANSIT_TIMES table.
-------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_LOC_LIST(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_other_loc_exists   IN OUT   BOOLEAN,
                          I_dept               IN       TRANSIT_TIMES.DEPT%TYPE,
                          I_class              IN       TRANSIT_TIMES.CLASS%TYPE,
                          I_subclass           IN       TRANSIT_TIMES.SUBCLASS%TYPE,
                          I_origin_type        IN       TRANSIT_TIMES.ORIGIN_TYPE%TYPE,
                          I_origin             IN       TRANSIT_TIMES.ORIGIN%TYPE,
                          I_destination_type   IN       TRANSIT_TIMES.DESTINATION_TYPE%TYPE,
                          I_destination        IN       TRANSIT_TIMES.DESTINATION%TYPE,
                          I_transit_time       IN       TRANSIT_TIMES.TRANSIT_TIME%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END TRANSIT_TIMES_SQL;
/

