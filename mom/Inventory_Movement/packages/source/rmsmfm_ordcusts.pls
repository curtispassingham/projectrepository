CREATE OR REPLACE PACKAGE RMSMFM_ORDCUST AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- PUBLIC VARIABLES
--------------------------------------------------------------------------------
FAMILY        RIB_SETTINGS.FAMILY%TYPE := 'fulfilordcfm';

LP_cre_type   RIB_TYPE_SETTINGS.TYPE%TYPE := 'fulfilordcfmcre';
--------------------------------------------------------------------------------
-- This public function adds the ordcust_no to the ORDCUST_PUB_INFO table 
-- in 'U'npublished status. It stages the ordcust for publishing to the RIB.
--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_message_type    IN       ORDCUST_PUB_INFO.MESSAGE_TYPE%TYPE,
                I_ordcust_no      IN       ORDCUST.ORDCUST_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- This public procedure is called from the RIB to get the next unpublished 
-- ordcust_no on ORDCUST_PUB_INFO table for publishing.
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code     IN OUT   VARCHAR2,
                 O_error_message   IN OUT   VARCHAR2,
                 O_message_type    IN OUT   VARCHAR2,
                 O_message         IN OUT   RIB_OBJECT,
                 O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                 O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                 I_num_threads     IN       NUMBER DEFAULT 1,
                 I_thread_val      IN       NUMBER DEFAULT 1);
--------------------------------------------------------------------------------
-- This public procedure re-processes the records in the ORDCUST_PUB_INFO table 
-- with pub_status equal to 'H'ospital.
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code     IN OUT   VARCHAR2,
                    O_error_message   IN OUT   VARCHAR2,
                    O_message_type    IN OUT   VARCHAR2,
                    O_message         IN OUT   RIB_OBJECT,
                    O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                    O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                    I_ref_object      IN       RIB_OBJECT);
--------------------------------------------------------------------------------
END RMSMFM_ORDCUST;
/
