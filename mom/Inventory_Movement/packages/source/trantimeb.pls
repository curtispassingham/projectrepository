
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY TRANSIT_TIMES_SQL AS
-------------------------------------------------------------------------------------------------
FUNCTION NEXT_TRANSIT_TIMES_ID(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_tran_times_id      IN OUT   TRANSIT_TIMES.TRANSIT_TIMES_ID%TYPE)

   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'TRANSIT_TIMES_SQL.NEXT_TRANSIT_TIMES_ID';
   L_first_time             VARCHAR2(3)  := 'Yes';
   L_dummy                  VARCHAR2(1);
   L_tran_times_sequence    TRANSIT_TIMES.TRANSIT_TIMES_ID%TYPE;
   L_wrap_sequence_number   TRANSIT_TIMES.TRANSIT_TIMES_ID%TYPE;

   cursor C_TRAN_ID is
      select 'x'
        from transit_times
       where transit_times_id = L_tran_times_sequence
         and rownum           = 1;

BEGIN

   LOOP

      select tran_times_seq.NEXTVAL
        into L_tran_times_sequence
        from sys.dual;

      if (L_first_time = 'Yes') then
         L_wrap_sequence_number := L_tran_times_sequence;
         L_first_time := 'No';
      elsif (L_tran_times_sequence = L_wrap_sequence_number) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_TRAN_ID',
                       'transit_times',
                       NULL);
      open C_TRAN_ID;

      SQL_LIB.SET_MARK('FETCH',
                       'C_TRAN_ID',
                       'transit_times',
                       NULL);
      fetch C_TRAN_ID into L_dummy;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_TRAN_ID',
                       'transit_times',
                       NULL);
      close C_TRAN_ID;

      ---
      if L_dummy is NULL then
         O_tran_times_id := L_tran_times_sequence;
         exit;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END NEXT_TRANSIT_TIMES_ID;
-------------------------------------------------------------------------------------------------
FUNCTION TRANTIME_DUP_CHECK(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist              IN OUT   BOOLEAN,
                            I_dept               IN       TRANSIT_TIMES.DEPT%TYPE,
                            I_class              IN       TRANSIT_TIMES.CLASS%TYPE,
                            I_subclass           IN       TRANSIT_TIMES.SUBCLASS%TYPE,
                            I_origin_type        IN       TRANSIT_TIMES.ORIGIN_TYPE%TYPE,
                            I_origin             IN       TRANSIT_TIMES.ORIGIN%TYPE,
                            I_destination_type   IN       TRANSIT_TIMES.DESTINATION_TYPE%TYPE,
                            I_destination        IN       TRANSIT_TIMES.DESTINATION%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(61)  := 'TRANSIT_TIMES_SQL.TRANTIME_DUP_CHECK';
   L_table     VARCHAR2(20)  := 'TRANSIT_TIME';
   L_exist     VARCHAR2(1);

   cursor C_TRANTIME_EXISTS is
      select 'x'
        from transit_times tt
       where tt.dept = I_dept
         and (tt.class = I_class 
             or (tt.class IS NULL
                and I_class IS NULL))
         and (tt.subclass = I_subclass
             or (tt.subclass IS NULL 
                and I_subclass IS NULL))
         and tt.origin_type = I_origin_type
         and tt.origin = I_origin
         and tt.destination_type = I_destination_type
         and tt.destination = I_destination;

BEGIN

   --- Check required input parameters
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_class is NULL then 
      if I_subclass is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_class',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
   end if;
  
   if I_origin_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_origin is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_destination_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_destination_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_destination is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_destination',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   ---
   open C_TRANTIME_EXISTS;
   fetch C_TRANTIME_EXISTS into L_exist;
   close C_TRANTIME_EXISTS;
   ---

   O_exist := (L_exist is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END TRANTIME_DUP_CHECK;
-------------------------------------------------------------------------------------------------
FUNCTION TRANTIME_APPLY(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_overwrite           IN      BOOLEAN,
                        I_transit_times_id    IN      TRANSIT_TIMES.TRANSIT_TIMES_ID%TYPE,
                        I_dept                IN      TRANSIT_TIMES.DEPT%TYPE,
                        I_class               IN      TRANSIT_TIMES.CLASS%TYPE,
                        I_subclass            IN      TRANSIT_TIMES.SUBCLASS%TYPE,
                        I_origin_type         IN      TRANSIT_TIMES.ORIGIN_TYPE%TYPE,
                        I_origin              IN      TRANSIT_TIMES.ORIGIN%TYPE,
                        I_destination_type    IN      TRANSIT_TIMES.DESTINATION_TYPE%TYPE,
                        I_destination         IN      TRANSIT_TIMES.DESTINATION%TYPE,
                        I_transit_time        IN      TRANSIT_TIMES.TRANSIT_TIME%TYPE)

   RETURN BOOLEAN IS

   L_program            VARCHAR2(61)  := 'TRANSIT_TIMES_SQL.TRANTIME_APPLY';
   L_table              VARCHAR2(20)  := 'TRANSIT_TIME';
   L_rowid              ROWID;
   L_transit_times_id   TRANSIT_TIMES.TRANSIT_TIMES_ID%TYPE;

   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_TRANTIME is
      select rowid
        from transit_times tt
       where tt.origin_type = I_origin_type
         and tt.origin = I_origin
         and tt.destination_type = I_destination_type
         and tt.destination = I_destination
         and tt.dept = I_dept
         and (tt.class = I_class 
             or (tt.class IS NULL
                and I_class IS NULL))
         and (tt.subclass = I_subclass
             or (tt.subclass IS NULL 
                and I_subclass IS NULL))
         for update nowait;
         
BEGIN

   --- Check required input parameters
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
      
   if I_class is NULL then
      if I_subclass is NOT NULL then
         o_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_class',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
   end if;
  
   if I_origin_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_origin is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_destination_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_destination_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_destination is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_destination',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_transit_time is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_transit_time',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   if I_overwrite is NULL and I_transit_times_id is NULL then
      if TRANSIT_TIMES_SQL.NEXT_TRANSIT_TIMES_ID(O_error_message,
                                                 L_transit_times_id) = FALSE then
         return FALSE;
      end if;
      ---
      insert into transit_times(transit_times_id,
                                dept,
                                class,
                                subclass,
                                origin,
                                destination,
                                origin_type,
                                destination_type,
                                transit_time)
                         values(L_transit_times_id,
                                I_dept,
                                I_class,
                                I_subclass,
                                I_origin,
                                I_destination,
                                I_origin_type,
                                I_destination_type,
                                I_transit_time);
   else
      ---
      open C_LOCK_TRANTIME;
      fetch C_LOCK_TRANTIME into L_rowid;
      close C_LOCK_TRANTIME;
      ---
      update transit_times
         set transit_time = I_transit_time
       where rowid = L_rowid;
      ---
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_origin,
                                            I_destination);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END TRANTIME_APPLY;
-------------------------------------------------------------------------------------------------
FUNCTION TRANTIME_DELETE(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_transit_times_id    IN      TRANSIT_TIMES.TRANSIT_TIMES_ID%TYPE,
                         I_origin              IN      TRANSIT_TIMES.ORIGIN%TYPE,
                         I_destination         IN      TRANSIT_TIMES.DESTINATION%TYPE)

RETURN BOOLEAN IS

   L_program            VARCHAR2(61)  := 'TRANSIT_TIMES_SQL.TRANTIME_DELETE';
   L_table              VARCHAR2(20)  := 'TRANSIT_TIME';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_TRANTIME is
      select 'x'
        from transit_times tt
       where tt.transit_times_id = I_transit_times_id
         for update nowait;

BEGIN
   --- Check required input parameters
   if I_transit_times_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_transit_times_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_LOCK_TRANTIME;
   close C_LOCK_TRANTIME;
   ---
   delete from transit_times
    where transit_times_id = I_transit_times_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_origin,
                                            I_destination);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END TRANTIME_DELETE;
-------------------------------------------------------------------------------------------------
FUNCTION TRANTIME_FILTER_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_diff            IN OUT   VARCHAR2,
                              I_dept            IN       TRANSIT_TIMES.DEPT%TYPE,
                              I_class           IN       TRANSIT_TIMES.CLASS%TYPE,
                              I_subclass        IN       TRANSIT_TIMES.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   CURSOR C_COUNT_TRANTIME IS
      select count(transit_times_id)
        from transit_times
       where dept = I_dept
         and (class = I_class 
             or (class IS NULL
                and I_class IS NULL))
         and (subclass = I_subclass
             or (subclass IS NULL 
                and I_subclass IS NULL));

   CURSOR C_COUNT_VTRANTIME IS
      select count(transit_times_id)
        from v_transit_times
       where dept = I_dept
         and (class = I_class 
             or (class IS NULL
                and I_class IS NULL))
         and (subclass = I_subclass
             or (subclass IS NULL 
                and I_subclass IS NULL));
   
   L_program     VARCHAR2(40) := 'TRANSIT_TIMES_SQL.TRANTIME_FILTER_LIST';
   L_trantime    NUMBER(6)    := -765547;
   L_vtrantime   NUMBER(6)    := -987436;

BEGIN
   OPEN c_count_trantime;
   FETCH c_count_trantime INTO L_trantime;
   CLOSE c_count_trantime;

   OPEN c_count_vtrantime;
   FETCH c_count_vtrantime INTO L_vtrantime;
   CLOSE c_count_vtrantime;

   if L_vtrantime != L_trantime then
     O_diff := 'Y';
   else
     O_diff := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END TRANTIME_FILTER_LIST;
-------------------------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists              IN OUT   BOOLEAN,
                 O_transit_times_row   IN OUT   TRANSIT_TIMES%ROWTYPE,
                 I_supplier            IN       TRANSIT_TIMES.ORIGIN%TYPE,
                 I_location            IN       TRANSIT_TIMES.DESTINATION%TYPE,
                 I_item                IN       ITEM_MASTER.ITEM%TYPE)
                 
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)                := 'TRANSIT_TIMES_SQL.GET_ROW';
   L_subclass   TRANSIT_TIMES.SUBCLASS%TYPE := NULL;
   L_class      TRANSIT_TIMES.CLASS%TYPE    := NULL;
   L_dept       TRANSIT_TIMES.DEPT%TYPE     := NULL;
   
   cursor C_GET_DEPT_CLASS is
      select dept,
             class,
             subclass
        from item_master 
       where item = I_item;      

   cursor C_GET_ROW is
      select tt.transit_times_id,
             tt.dept,
             tt.class,
             tt.subclass,
             tt.origin,
             tt.destination,
             tt.origin_type,
             tt.destination_type,
             tt.transit_time
        from transit_times tt,
             ( select store vloc, store ploc from store
               union
               select wh vloc, physical_wh ploc from wh ) loc
       where loc.vloc       = I_location
         and tt.origin      = I_supplier
         and tt.destination = loc.ploc
         and tt.dept        = L_dept
         and (tt.class      = L_class
             or (tt.class is NULL and tt.subclass is NULL))
         and (tt.subclass   = L_subclass
             or tt.subclass is NULL)
         and tt.origin_type = 'SU'
       order by subclass, class, dept desc;

BEGIN
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
 
   O_transit_times_row := NULL;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_DEPT_CLASS',
                    'item_master',
                    NULL);
   open C_GET_DEPT_CLASS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DEPT_CLASS',
                    'item_master',
                    NULL);
   fetch C_GET_DEPT_CLASS into L_dept,
                               L_class,
                               L_subclass;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ROW',
                    'item_master',
                    NULL);
   close C_GET_DEPT_CLASS;

   if L_dept is not NULL and
      L_class is not NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_ROW',
                       'transit_times',
                       NULL);
      open C_GET_ROW;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_ROW',
                       'transit_times',
                       NULL);
      fetch C_GET_ROW into O_transit_times_row;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_ROW',
                       'transit_times',
                       NULL);
      close C_GET_ROW;

      if O_transit_times_row.transit_times_id is NULL then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ROW;
-------------------------------------------------------------------------------------------------

FUNCTION EXPLODE_LOC_LIST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_other_loc_exists    IN OUT   BOOLEAN,
                          I_dept                IN       TRANSIT_TIMES.DEPT%TYPE,
                          I_class               IN       TRANSIT_TIMES.CLASS%TYPE,
                          I_subclass            IN       TRANSIT_TIMES.SUBCLASS%TYPE,
                          I_origin_type         IN       TRANSIT_TIMES.ORIGIN_TYPE%TYPE,
                          I_origin              IN       TRANSIT_TIMES.ORIGIN%TYPE,
                          I_destination_type    IN       TRANSIT_TIMES.DESTINATION_TYPE%TYPE,
                          I_destination         IN       TRANSIT_TIMES.DESTINATION%TYPE,
                          I_transit_time        IN       TRANSIT_TIMES.TRANSIT_TIME%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)                        := 'TRANSIT_TIMES_SQL.EXPLODE_LOC_LIST';
   L_origin_type        TRANSIT_TIMES.ORIGIN_TYPE%TYPE      := NULL;
   L_origin             TRANSIT_TIMES.ORIGIN%TYPE           := NULL;
   L_destination_type   TRANSIT_TIMES.DESTINATION_TYPE%TYPE := NULL;
   L_destination        TRANSIT_TIMES.DESTINATION%TYPE      := NULL;
   L_trantime_id        TRANSIT_TIMES.TRANSIT_TIMES_ID%TYPE := NULL;
   L_overwrite          BOOLEAN                             := TRUE;
   L_loc_list           LOC_LIST_HEAD.LOC_LIST%TYPE         := NULL;
   L_other_loc_exists   VARCHAR2(2)                         := NULL;
   L_loc_type           LOC_LIST_DETAIL.LOC_TYPE%TYPE       := NULL;

   cursor C_ORIGIN is
      select distinct wh.physical_wh location
        from loc_list_detail lld,
             v_physical_wh vwh,
             wh
       where lld.loc_list = I_origin
         and (lld.action_type is NULL or
              lld.action_type != 'D')
         and lld.loc_type = L_loc_type
         and lld.location = wh.wh
         and wh.physical_wh = vwh.wh
       union all
      select lld.location
        from loc_list_detail lld,
             v_store vs
       where lld.loc_list = I_origin
         and (lld.action_type is NULL or
              lld.action_type != 'D')
         and lld.loc_type = L_loc_type
         and lld.location = vs.store;
         
   cursor C_DESTINATION is
      select distinct wh.physical_wh location
        from loc_list_detail lld,
             v_physical_wh vwh,
             wh
       where lld.loc_list = I_destination
         and (lld.action_type is NULL or
              lld.action_type != 'D')
         and lld.loc_type = L_loc_type
         and lld.location = wh.wh
         and wh.physical_wh = vwh.wh
       union all
      select lld.location
        from loc_list_detail lld,
             v_store vs
       where lld.loc_list = I_destination
         and (lld.action_type is NULL or
              lld.action_type != 'D')
         and lld.loc_type = L_loc_type
         and lld.location = vs.store;

   cursor C_GET_TRANTIME_ID is
      select transit_times_id
        from transit_times
       where dept = I_dept
         and ((I_class is NULL and class is NULL)  or
              class = I_class)
         and ((I_subclass is NULL and subclass is NULL) or
              subclass = I_subclass)
         and origin = L_origin
         and origin_type = L_origin_type
         and destination = L_destination
         and destination_type = L_destination_type;
         
   cursor C_GET_OTHER_LOC_TYPE is
      select 'x'
        from loc_list_detail
       where loc_list  = L_loc_list
         and loc_type != L_loc_type
         and rownum = 1;
BEGIN
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_origin_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_origin is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_destination_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_destination_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_destination is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_destination',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_transit_time is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_transit_times',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_other_loc_exists := FALSE;

   if I_origin_type in ('LLS','LLW') then
      if I_origin_type = 'LLS' then
         L_origin_type := 'ST';
         L_loc_type    := 'S';
      else
         L_origin_type := 'WH';
         L_loc_type    := 'W';
      end if;

      L_loc_list := I_origin;
      L_other_loc_exists := NULL;

      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_OTHER_LOC_TYPE',
                       'loc_list_detail',
                       NULL);
      open C_GET_OTHER_LOC_TYPE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_OTHER_LOC_TYPE',
                       'loc_list_detail',
                       NULL);
      fetch C_GET_OTHER_LOC_TYPE into L_other_loc_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_OTHER_LOC_TYPE',
                       'loc_list_detail',
                       NULL);
      close C_GET_OTHER_LOC_TYPE;

      if L_other_loc_exists is NOT NULL then
         O_other_loc_exists := TRUE;
      end if;

      for rec_org in C_ORIGIN LOOP
         L_origin := rec_org.location;

         if I_destination_type in ('LLS','LLW') then
            if I_destination_type = 'LLS' then
               L_destination_type := 'ST';
               L_loc_type         := 'S';
            else
               L_destination_type := 'WH';
               L_loc_type         := 'W';
            end if;

            L_loc_list := I_destination;
            L_other_loc_exists := NULL;
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_OTHER_LOC_TYPE',
                             'loc_list_detail',
                             NULL);
            open C_GET_OTHER_LOC_TYPE;
            
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_OTHER_LOC_TYPE',
                             'loc_list_detail',
                             NULL);
            fetch C_GET_OTHER_LOC_TYPE into L_other_loc_exists;
            
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_OTHER_LOC_TYPE',
                             'loc_list_detail',
                             NULL);
            close C_GET_OTHER_LOC_TYPE;

            if L_other_loc_exists is NOT NULL then
               O_other_loc_exists := TRUE;
            end if;

            for rec_des in C_DESTINATION LOOP
               L_destination := rec_des.location;
               L_overwrite   := TRUE;
               L_trantime_id := NULL;
               
               SQL_LIB.SET_MARK('OPEN',
                                'C_GET_TRANTIME_ID',
                                'transit_times',
                                NULL);
               open C_GET_TRANTIME_ID;

               SQL_LIB.SET_MARK('FETCH',
                                'C_GET_TRANTIME_ID',
                                'transit_times',
                                NULL);
               fetch C_GET_TRANTIME_ID into L_trantime_id;

               SQL_LIB.SET_MARK('CLOSE',
                                'C_GET_TRANTIME_ID',
                                'transit_times',
                                NULL);
               close C_GET_TRANTIME_ID;
               
               if L_trantime_id is NULL then
                  L_overwrite := NULL;
               end if;
               
               if (L_origin_type = L_destination_type and L_destination != L_origin) or
                  (L_origin_type != L_destination_type) then
                  if TRANSIT_TIMES_SQL.TRANTIME_APPLY(O_error_message,
                                                      L_overwrite,
                                                      L_trantime_id,
                                                      I_dept,
                                                      I_class,
                                                      I_subclass,
                                                      L_origin_type,
                                                      L_origin,
                                                      L_destination_type,
                                                      L_destination,
                                                      I_transit_time) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end LOOP;
         else
            L_destination_type := I_destination_type;
            L_destination      := I_destination;
            L_overwrite        := TRUE;
            L_trantime_id      := NULL;
            
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_TRANTIME_ID',
                             'transit_times',
                             NULL);
            open C_GET_TRANTIME_ID;
            
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_TRANTIME_ID',
                             'transit_times',
                             NULL);
            fetch C_GET_TRANTIME_ID into L_trantime_id;
            
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_TRANTIME_ID',
                             'transit_times',
                             NULL);
            close C_GET_TRANTIME_ID;
            
            if L_trantime_id is NULL then
               L_overwrite := NULL;
            end if;
            
            if (L_origin_type = L_destination_type and L_destination != L_origin) or
               (L_origin_type != L_destination_type) then
               if TRANSIT_TIMES_SQL.TRANTIME_APPLY(O_error_message,
                                                   L_overwrite,
                                                   L_trantime_id,
                                                   I_dept,
                                                   I_class,
                                                   I_subclass,
                                                   L_origin_type,
                                                   L_origin,
                                                   L_destination_type,
                                                   L_destination,
                                                   I_transit_time) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
      end LOOP;
   else
      if I_destination_type in ('LLS','LLW') then
         if I_destination_type = 'LLS' then
            L_destination_type := 'ST';
            L_loc_type         := 'S';
         else
            L_destination_type := 'WH';
            L_loc_type         := 'W';
         end if;

         L_loc_list := I_destination;
         L_other_loc_exists := NULL;

         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_OTHER_LOC_TYPE',
                          'loc_list_detail',
                          NULL);
         open C_GET_OTHER_LOC_TYPE;
         
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_OTHER_LOC_TYPE',
                          'loc_list_detail',
                          NULL);
         fetch C_GET_OTHER_LOC_TYPE into L_other_loc_exists;
         
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_OTHER_LOC_TYPE',
                          'loc_list_detail',
                          NULL);
         close C_GET_OTHER_LOC_TYPE;

         if L_other_loc_exists is NOT NULL then
            O_other_loc_exists := TRUE;
         end if;

         for rec_des in C_DESTINATION LOOP
            L_destination := rec_des.location;
            L_origin_type := I_origin_type;
            L_origin      := I_origin;
            L_overwrite   := TRUE;
            L_trantime_id := NULL;
            
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_TRANTIME_ID',
                             'transit_times',
                             NULL);
            open C_GET_TRANTIME_ID;
         
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_TRANTIME_ID',
                             'transit_times',
                             NULL);
            fetch C_GET_TRANTIME_ID into L_trantime_id;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_TRANTIME_ID',
                             'transit_times',
                             NULL);
            close C_GET_TRANTIME_ID;

            if L_trantime_id is NULL then
               L_overwrite := NULL;
            end if;
            
            if (L_origin_type = L_destination_type and L_destination != L_origin) or
               (L_origin_type != L_destination_type) then
               if TRANSIT_TIMES_SQL.TRANTIME_APPLY(O_error_message,
                                                   L_overwrite,
                                                   L_trantime_id,
                                                   I_dept,
                                                   I_class,
                                                   I_subclass,
                                                   L_origin_type,
                                                   L_origin,
                                                   L_destination_type,
                                                   L_destination,
                                                   I_transit_time) = FALSE then
                  return FALSE;
               end if;
           end if;
         end LOOP;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_LOC_LIST;
-------------------------------------------------------------------------------------------------
END TRANSIT_TIMES_SQL;
/
