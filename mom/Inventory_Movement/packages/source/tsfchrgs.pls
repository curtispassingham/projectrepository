CREATE OR REPLACE PACKAGE TRANSFER_CHARGE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
TYPE  REC_CHRG_ITEMS IS RECORD(
   item        TSFDETAIL.ITEM%TYPE,
   tsf_seq_no  TSFDETAIL.TSF_SEQ_NO%TYPE);

TYPE  CHRG_ITEM_TBL IS TABLE OF  REC_CHRG_ITEMS
   INDEX BY BINARY_INTEGER;

-------------------------------------------------------------------------------
--Function Name:  DELETE_CHRGS
--Purpose      :  Deletes all the Transfer Detail Charges records for
--                the passed in Transfer or Transfer/Item combination.
-------------------------------------------------------------------------------
FUNCTION DELETE_CHRGS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                      I_item          IN     ITEM_MASTER.ITEM%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  GET_DEFAULT_CHRGS_IND
--Purpose      :  Retrieves the up charge indicator for the passed in Transfer
--                Type.  This indicator indicates whether or not Up Charges 
--                should be associated with transfers of the passed in type.
--------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_CHRGS_IND(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_default_chrgs_ind IN OUT TSF_TYPE.DEFAULT_CHRGS_IND%TYPE,
                               I_tsf_type          IN     TSF_TYPE.TSF_TYPE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_CHRGS
-- Purpose      : Attaches the appropriate Up Charges to an Transfer/From Loc/To Loc/
--                Item combination or Transfer/Shipment/Item combination when the Item
--                is added to the Transfer or the shipment is created.
------------------------------------------------------------------------------------------
FUNCTION DEFAULT_CHRGS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                       I_tsf_type      IN     TSFHEAD.TSF_TYPE%TYPE,
                       I_tsf_seq_no    IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                       I_shipment      IN     SHIPMENT.SHIPMENT%TYPE,
                       I_ship_seq_no   IN     SHIPSKU.SEQ_NO%TYPE,
                       I_from_loc      IN     STORE.STORE%TYPE,
                       I_from_loc_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_to_loc        IN     STORE.STORE%TYPE,
                       I_to_loc_type   IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: CHARGES_EXIST
-- Purpose      : This function will check if up charges exist for the given 
--                transfer or transfer/item combination (if I_item is null). 
------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists        IN OUT BOOLEAN,
                       I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: SET_DEFAULT_CHRGS_2_LEG_IND
--       Purpose: Set the DEFAULT_CHRGS_2_LEG_IND on tsfdetail table for 2nd_leg. 

FUNCTION SET_DEFAULT_CHRGS_2_LEG_IND(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_tsf_no            IN     TSFHEAD.TSF_NO%TYPE,
                                     I_item              IN     ITEM_MASTER.ITEM%TYPE,
                                     I_chrgs_2_leg_ind   IN     TSFDETAIL.DEFAULT_CHRGS_2_LEG_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------- 
-- Function Name: CHARGES_EXIST_2ND_LEG
--       Purpose: Verify whether there exists up-charges among items on the 2nd-leg which isn't connected
--                to the items on 1st-leg whose default_chrgs_2_leg_ind is set to 'Y'.
--                If there are, return these item to the parameter O_chrg_items, and set O_chrg_exists to 
--                TRUE, otherwise set O_chrg_exists to FALSE.

FUNCTION CHARGES_EXIST_2ND_LEG(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_chrg_exists              IN OUT BOOLEAN,
                               O_chrg_items               IN OUT CHRG_ITEM_TBL,
                               I_tsf_no                   IN     TSFHEAD.TSF_NO%TYPE,
                               I_child_tsf_no             IN     TSFHEAD.TSF_NO%TYPE, 
                               I_finisher                 IN     STORE.STORE%TYPE,
                               I_finisher_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_to_loc                   IN     STORE.STORE%TYPE,
                               I_to_loc_type              IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------- 
-- Function Name: CHARGES_EXIST_2ND_LEG
--       Purpose: Overloaded function used by the transfer screen in ADF. It does NOT return a list
--                of 2nd-leg items that need to have upcharges defaulted. 
-------------------------------------------------------------------------------------------------------- 
FUNCTION CHARGES_EXIST_2ND_LEG(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_chrg_exists         IN OUT   BOOLEAN,
                               I_tsf_no              IN       TSFHEAD.TSF_NO%TYPE,
                               I_child_tsf_no        IN       TSFHEAD.TSF_NO%TYPE, 
                               I_finisher            IN       STORE.STORE%TYPE,
                               I_finisher_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                               I_to_loc              IN       STORE.STORE%TYPE,
                               I_to_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_CHRGS_2ND_LEG
--       Purpose: Applies item up charges to the 2nd-leg when approve transfer based on:
--                1. the indicator default_chrgs_2_leg_ind on the 1st-leg if the connected items on the
--                   2nd-leg at from_loc/to_loc have item up charges
--                2. the input parameters I_default_chrgs_2_leg_ind and I_chrg_items

FUNCTION DEFAULT_CHRGS_2ND_LEG(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no                   IN     TSFHEAD.TSF_NO%TYPE,
                               I_tsf_type                 IN     TSFHEAD.TSF_TYPE%TYPE,
                               I_child_tsf_no             IN     TSFHEAD.TSF_NO%TYPE, 
                               I_finisher                 IN     STORE.STORE%TYPE,
                               I_finisher_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_to_loc                   IN     STORE.STORE%TYPE,
                               I_to_loc_type              IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_default_chrgs_2_leg_ind  IN     TSFDETAIL.DEFAULT_CHRGS_2_LEG_IND%TYPE,
                               I_chrg_items               IN     CHRG_ITEM_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_CHRGS_2ND_LEG
--       Purpose: Overloaded function used by the transfer screen in ADF. It does NOT take a list
--                of 2nd-leg items that need to have upcharges defaulted as input parameter. 
--------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_CHRGS_2ND_LEG(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no                    IN       TSFHEAD.TSF_NO%TYPE,
                               I_tsf_type                  IN       TSFHEAD.TSF_TYPE%TYPE,
                               I_child_tsf_no              IN       TSFHEAD.TSF_NO%TYPE, 
                               I_finisher                  IN       STORE.STORE%TYPE,
                               I_finisher_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                               I_to_loc                    IN       STORE.STORE%TYPE,
                               I_to_loc_type               IN       ITEM_LOC.LOC_TYPE%TYPE,
                               I_default_chrgs_2_leg_ind   IN       TSFDETAIL.DEFAULT_CHRGS_2_LEG_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name: ITEM_EXISTS_ON_LEG_2
--       Purpose: Predict whether the item will exist on the 2nd-leg transfer.

FUNCTION ITEM_EXISTS_ON_LEG_2(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists                   IN OUT BOOLEAN,
                              I_tsf_no                   IN     TSFHEAD.TSF_NO%TYPE,
                              I_item                     IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END TRANSFER_CHARGE_SQL;
/
