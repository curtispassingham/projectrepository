CREATE OR REPLACE PACKAGE RTV_SQL AUTHID CURRENT_USER AS

-- Define a rtv record type.

TYPE INV_STATUS_CODES_TBL  is TABLE OF INV_STATUS_CODES.INV_STATUS_CODE%TYPE;
TYPE REASON_TBL            is TABLE OF RTV_DETAIL.REASON%TYPE;
TYPE UOM_CLASS_TBL         is TABLE OF UOM_CLASS.UOM%TYPE;
TYPE WEIGHT_TBL            is TABLE OF ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
TYPE COST_UOM_TBL          is TABLE OF ITEM_SUPP_COUNTRY.COST_UOM%TYPE;
TYPE RESTOCK_PCT_TBL       is TABLE OF RTV_DETAIL.RESTOCK_PCT%TYPE;
TYPE RTV_DETAIL_TBL        is TABLE OF RTV_DETAIL%ROWTYPE;
TYPE SEQ_NO_TBL            is TABLE OF RTV_DETAIL.SEQ_NO%TYPE;

TYPE item_master_TBL       is TABLE of ITEM_MASTER%ROWTYPE;

TYPE rtv_detail_rec is RECORD (seq_nos            SEQ_NO_TBL,
                               items              ITEM_TBL,
                               returned_qtys      QTY_TBL,
                               from_disps         INV_STATUS_CODES_TBL,
                               unit_cost_exts     UNIT_COST_TBL,  -- from message, in location currency, hold the gross cost
                               extended_base_cost UNIT_COST_TBL,  -- hold the EBC which will be used for posting into TRAN_DATA
                               unit_cost_supps    UNIT_COST_TBL,  -- in supplier currency
                               unit_cost_locs     UNIT_COST_TBL,  -- in location currency
                               reasons            REASON_TBL,
                               restock_pcts       RESTOCK_PCT_TBL,
                               inv_statuses       INV_STATUS_TBL,
                               mc_returned_qtys   QTY_TBL,
                               weights            WEIGHT_TBL,
                               weight_uoms        UOM_CLASS_TBL,
                               weight_cuoms       WEIGHT_TBL,
                               mc_weight_cuoms    WEIGHT_TBL,
                               cuoms              COST_UOM_TBL);

TYPE rtv_record IS RECORD (rtv_order_no        rtv_head.rtv_order_no%TYPE,
                           loc                 item_loc.loc%TYPE,
                           store               rtv_head.store%TYPE,
                           wh                  rtv_head.wh%TYPE,
                           physical_wh         rtv_head.wh%TYPE,
                           loc_type            item_loc.loc_type%TYPE,
                           ext_ref_no          rtv_head.ext_ref_no%TYPE,
                           ret_auth_num        rtv_head.ret_auth_num%TYPE,
                           supplier            rtv_head.supplier%TYPE,
                           ship_addr1          rtv_head.ship_to_add_1%TYPE,
                           ship_addr2          rtv_head.ship_to_add_2%TYPE,
                           ship_addr3          rtv_head.ship_to_add_3%TYPE,
                           state               rtv_head.state%TYPE,
                           city                rtv_head.ship_to_city%TYPE,
                           pcode               rtv_head.ship_to_pcode%TYPE,
                           country             rtv_head.ship_to_country_id%TYPE,
                           jurisdiction_code   rtv_head.ship_to_jurisdiction_code%TYPE,
                           tran_date           tran_data.tran_date%TYPE,
                           comments            rtv_head.comment_desc%TYPE,
     -- unit based total order amount is used for writing to RTV_HEAD.TOTAL_ORDER_AMT
     -- weight based total order amount is used for writing stock ledgers
                           total_order_amt_unit_based  rtv_head.total_order_amt%TYPE,  -- in supplier currency
                           total_order_amt_wgt_based   rtv_head.total_order_amt%TYPE,  -- in supplier currency
                           ret_courier      rtv_head.courier%TYPE,
                           restock_pct      rtv_detail.restock_pct%TYPE,
                           restock_cost     rtv_head.restock_cost%TYPE,
                           detail_tbl       RTV_DETAIL_REC,
                           status_ind       rtv_head.status_ind%TYPE,
                           im_row_tbl       item_master_TBL);

---------------------------------------------------------------------
-- Function    : Inventory
-- Purpose     : Called for each ITEM being returned to the vendor
--               that is returned from inventory.
---------------------------------------------------------------------
FUNCTION INVENTORY (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_upd_rtv_qty_ind   IN       VARCHAR2,
                    I_item_xform_ind    IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                    I_sellable_ind      IN       ITEM_MASTER.SELLABLE_IND%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function    :  UPD_ITEM_RTV_QTY
-- Purpose     :  Adds I_qty to the RTV_QTY for the item/location combination
--                passed in.   Called by UPD_RTV_QTY and RSS 2.0
---------------------------------------------------------------------
FUNCTION UPD_ITEM_RTV_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item            IN       ITEM_MASTER.ITEM %TYPE,
                          I_location        IN       INV_ADJ.LOCATION%TYPE,
                          I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                          I_qty             IN       RTV_DETAIL.QTY_RETURNED%TYPE,
                          I_inv_status      IN       RTV_DETAIL.INV_STATUS%TYPE,
                          I_action_type     IN       VARCHAR2,
                          I_rtv_order_no    IN       RTV_DETAIL.RTV_ORDER_NO%TYPE
)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function    :  UPD_RTV_QTY
-- Purpose     :  To increment or decrement rtv_qty when an RTV is
--                approved, unapproved, or cancelled for items returned
--                from the inventory.
---------------------------------------------------------------------
FUNCTION UPD_RTV_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_rtv_order_no    IN       RTV_DETAIL.RTV_ORDER_NO%TYPE,
                     I_item            IN       ITEM_MASTER.ITEM %TYPE,
                     I_action_type     IN       VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function   : PROCESS_DEPOSIT_ITEM
-- Purpose    : This function serves the purpose of adding/updating the container
--              item for a contents deposit item in RTV_DETAIL table for online processing
--              as well as RTV subscription processing. Online processing creates and modifies
--              RTVs in approved status, whereas RTV subscription processing creates and modifies
--              RTVs in approved or shipped status. For this reason 2 sets of update statements
--              are used, 1 for updating approved RTVs and 1 for updating shipped RTVs.
--              For approved RTVs, only the qty_requested column is updated. The I_qty parameter
--              should be the full amount returned to the physical warehouse if multichannel
--              is turned on.
---------------------------------------------------------------------------------------
FUNCTION PROCESS_DEPOSIT_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_seq_no          IN       RTV_DETAIL.SEQ_NO%TYPE,
                              I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE,
                              I_supplier        IN       RTV_HEAD.SUPPLIER%TYPE,
                              I_container_item  IN       RTV_DETAIL.ITEM%TYPE,
                              I_contents_item   IN       RTV_DETAIL.ITEM%TYPE,
                              I_loc             IN       ITEM_LOC.LOC%TYPE,
                              I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                              I_qty             IN       RTV_DETAIL.QTY_REQUESTED%TYPE,
                              I_reason          IN       RTV_DETAIL.REASON%TYPE,
                              I_inv_status      IN       RTV_DETAIL.INV_STATUS%TYPE,
                              I_online_ind      IN       VARCHAR2,
                              I_approve_ind     IN       VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION APPLY_PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_rtv_record      IN       RTV_RECORD)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- FUNCTION : REMOVE_RTV_DETAIL
-- Purpose  : This is called by the rtv form, when the user attempts to delete a detail item
--            from the form. For cases when there is only one contents item for its respective
--            container both the contents item and its container should be removed.
--            If the container item has other rtv contents items associated then function
--            returns to the form, with the O_container_ind set to Y, but no records deleted.
--            If the rtv item does not have a container item then the function returns to
--            the form with the O_container_ind set to 'N'
--
--------------------------------------------------------------------------------------------
FUNCTION REMOVE_RTV_DETAIL (  O_error_message OUT rtk_errors.rtk_text%TYPE
                             ,O_container_ind OUT VARCHAR2
                             ,I_rtv_order_no  IN  rtv_detail.rtv_order_no%TYPE
                             ,I_item          IN  item_master.item%TYPE ) RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- FUNCTION : UPDATE_CONTAINER_ITEM
-- Purpose  : Update the container item with the sum of qty requested for its respective
--            contents items for the rtv.
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_CONTAINER_ITEM (  O_error_message OUT rtk_errors.rtk_text%TYPE
                                 ,I_rtv_order_no  IN  rtv_detail.rtv_order_no%TYPE
                                 ,I_item          IN  item_master.item%TYPE ) RETURN BOOLEAN ;
--------------------------------------------------------------------------------------------
-- FUNCTION : DETERMINE_RTV_COST
-- Purpose  : This function returns the RTV unit cost in local currency and in supplier currency.
--            Based on the RTV_UNIT_COST_IND present in UNIT_OPTIONS table, RTV unit cost can be
--            the item/loc's last receipt cost, or the standard unit cost or average cost.
--------------------------------------------------------------------------------------------
FUNCTION DETERMINE_RTV_COST (O_error_message  IN OUT	RTK_ERRORS.RTK_TEXT%TYPE,
                             O_unit_cost_loc  IN OUT	SHIPSKU.UNIT_COST%TYPE,
                             O_unit_cost_supp IN OUT	SHIPSKU.UNIT_COST%TYPE,
                             I_item	          IN	ITEM_LOC.ITEM%TYPE,
                             I_location       IN	ITEM_LOC.LOC%TYPE,
                             I_loc_type       IN	ITEM_LOC.LOC_TYPE%TYPE,
                             I_supplier       IN	SUPS.SUPPLIER%TYPE ) RETURN BOOLEAN ;
--------------------------------------------------------------------------------------------
-- FUNCTION : SHIP_RTV
-- Purpose  : This function is called from rtv form, when T_SHIP trigger is fired. It
--            collects data from RTV_HEAD and RTV_DETAIL for the passed in order no and
--            builds a RTV_SQL.RTV_RECORD. It then calls RTV_SQL.APPLY_PROCESS to ship
--            the RTV.
--------------------------------------------------------------------------------------------
FUNCTION SHIP_RTV (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_rtv_order_no   IN       RTV_HEAD.RTV_ORDER_NO%TYPE )
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- FUNCTION : APPROVE_RTV
-- Purpose  : This function is called from RMSSUB_RTV API for approved RTVs.
--            It creates/modifies a RTV in RMS in aprroved status.
--------------------------------------------------------------------------------------------
FUNCTION APPROVE_RTV (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_rtv_record     IN       RTV_SQL.RTV_RECORD)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: CUSTOM_VAL
-- Purpose : This function will be used for validationg the custom function.
--------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_rtv_order_no      IN OUT   RTV_HEAD.RTV_ORDER_NO%TYPE,
                    I_function_key      IN OUT   VARCHAR2,
                    I_seq_no            IN OUT   NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
$if $$UTPLSQL=TRUE $then
--------------------------------------------------------------------------------------------
FUNCTION INSERT_RTVITEM_INV_FLOW (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_rtv_order_no   IN     RTV_DETAIL.RTV_ORDER_NO%TYPE,
                                  I_seq_no         IN     RTV_DETAIL.SEQ_NO%TYPE,
                                  I_item           IN     RTV_DETAIL.ITEM%TYPE,
                                  I_dist_tab       IN     DISTRIBUTION_SQL.DIST_TABLE_TYPE)
   RETURN BOOLEAN;
$end
--------------------------------------------------------------------------------------------
END;
/
