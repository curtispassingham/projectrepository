CREATE OR REPLACE PACKAGE BODY RMSMFM_ALLOC AS

-----------------------------------------------------------------------

-----------------------------------------------------------------------
--Globals
TYPE rowid_TBL                          is table of ROWID
   INDEX BY BINARY_INTEGER;
TYPE alloc_details_pub_TBL              is table of ALLOC_DETAILS_PUBLISHED.ALLOC_NO%TYPE
   INDEX BY BINARY_INTEGER;
TYPE alloc_details_pub_to_loc_TBL       is table of ALLOC_DETAILS_PUBLISHED.TO_LOC_VIR%TYPE
   INDEX BY BINARY_INTEGER;


LP_num_threads NUMBER(4):=NULL;
LP_max_count NUMBER(4):=NULL;

LP_nbf_days NUMBER(38):=NULL;

-- depending upon the message type, there will come a point in the publication process
-- when error retry will no longer be possible.  this variable will track whether or
-- not that point has been reached.
LP_error_status varchar2(1):=NULL;

-----------------------------------------------------------------------
--Local Specs

---
PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT      VARCHAR2,
                        O_error_msg       IN  OUT      VARCHAR2,
                        O_message             OUT      RIB_OBJECT,
                        O_message_type    IN  OUT      VARCHAR2,
                        O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                        O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                        I_alloc_no        IN           alloc_mfqueue.alloc_no%TYPE,
                        I_seq_no          IN           alloc_mfqueue.seq_no%TYPE,
                        I_to_loc          IN           alloc_mfqueue.to_loc%TYPE);
---
FUNCTION PROCESS_QUEUE_RECORD( O_error_msg           OUT        VARCHAR2,
                               O_break_loop          OUT        BOOLEAN,
                               O_message         IN  OUT nocopy RIB_OBJECT,
                               O_routing_info    IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                               O_bus_obj_id      IN  OUT nocopy RIB_BUSOBJID_TBL,
                               O_message_type    IN  OUT        VARCHAR2,
                               I_alloc_no        IN             alloc_mfqueue.alloc_no%TYPE,
                               I_hdr_published   IN             alloc_pub_info.published%TYPE,
                               I_to_loc          IN             alloc_mfqueue.to_loc%TYPE,
                               I_pub_status      IN             alloc_mfqueue.pub_status%TYPE,
                               I_seq_no          IN             alloc_mfqueue.seq_no%TYPE,
                               I_rowid           IN             ROWID,
                               O_keep_queue      IN OUT         BOOLEAN)
RETURN BOOLEAN;
---
FUNCTION MAKE_CREATE( O_error_msg       IN OUT VARCHAR2,
                      O_message         IN OUT nocopy RIB_OBJECT,
                      O_routing_info    IN OUT nocopy RIB_ROUTINGINFO_TBL,
                      I_alloc_no        IN     alloc_header.alloc_no%TYPE,
                      I_seq_no          IN     alloc_mfqueue.seq_no%TYPE,
                      I_to_loc          IN     item_loc.loc%TYPE,
                      I_max_details     IN     rib_settings.max_details_to_publish%TYPE,
                      I_rowid           IN     ROWID,
                      O_keep_queue      IN OUT BOOLEAN)
RETURN BOOLEAN;
---
FUNCTION DELETE_QUEUE_REC( O_error_msg   OUT VARCHAR2,
                           I_seq_no      IN alloc_mfqueue.seq_no%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_HEADER_OBJECT( O_error_msg        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_message          IN OUT nocopy RIB_OBJECT,
                              O_rib_allocref_rec IN OUT nocopy "RIB_AllocRef_REC",
                              O_pack_ind         IN OUT item_master.pack_ind%TYPE,
                              O_sellable_ind     IN OUT item_master.sellable_ind%TYPE,
                              I_alloc_no         IN     alloc_header.alloc_no%TYPE)
RETURN BOOLEAN;
---

------ If to_loc is passed as NULL all detail records will be returned.  If
------ to_loc is passed only that detail will be returned.
FUNCTION BUILD_DETAIL_OBJECTS( O_error_msg                IN OUT VARCHAR2,
                               O_message                  IN OUT nocopy "RIB_AllocDtl_TBL",
                               O_alloc_mfqueue_rowid      IN OUT nocopy rowid_TBL,
                               O_alloc_mfqueue_size       IN OUT        BINARY_INTEGER,
                               O_adp_ins_to_loc           IN OUT nocopy alloc_details_pub_to_loc_TBL,
                               O_adp_ins_alloc            IN OUT nocopy alloc_details_pub_TBL,
                               O_adp_ins_size             IN OUT        BINARY_INTEGER,
                               O_adp_upd_rowid            IN OUT nocopy rowid_TBL,
                               O_adp_upd_size             IN OUT        BINARY_INTEGER,
                               O_routing_info             IN OUT nocopy RIB_ROUTINGINFO_TBL,
                               O_delete_rowid_ind         IN OUT        VARCHAR2,
                               I_message_type             IN     alloc_mfqueue.message_type%TYPE,
                               I_pack_ind                 IN     item_master.pack_ind%TYPE,
                               I_sellable_ind             IN     item_master.sellable_ind%TYPE,
                               I_alloc_no                 IN     alloc_header.alloc_no%TYPE,
                               I_item                     IN     item_master.item%TYPE,
                               I_max_details              IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_SINGLE_DETAIL ( O_error_msg            IN OUT VARCHAR2,
                               O_message              IN OUT nocopy "RIB_AllocDtl_TBL",
                               IO_rib_allocdtl_rec    IN OUT nocopy "RIB_AllocDtl_REC",
                               I_to_loc               IN            alloc_detail.to_loc%TYPE,
                               I_physical_to_loc      IN            alloc_detail.to_loc%TYPE,
                               I_loc_type             IN            alloc_detail.to_loc_type%TYPE,
                               I_store_type           IN            store.store_type%TYPE,
                               I_stockholding_ind     IN            store.stockholding_ind%TYPE,
                               I_qty_allocated        IN            alloc_detail.qty_allocated%TYPE,
                               I_store_ord_mult       IN            item_loc.store_ord_mult%TYPE,
                               I_rush_flag            IN            alloc_detail.rush_flag%TYPE,
                               I_in_store_date        IN            alloc_detail.in_store_date%TYPE,
                               I_message_type         IN            alloc_mfqueue.message_type%TYPE,
                               I_pack_ind             IN            item_master.pack_ind%TYPE,
                               I_sellable_ind         IN            item_master.sellable_ind%TYPE,
                               I_alloc_no             IN            alloc_header.alloc_no%TYPE,
                               I_item                 IN            item_master.item%TYPE,
                               I_max_details          IN            rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_CHANGE_OBJECTS( O_error_msg    IN OUT VARCHAR2,
                                      O_message      IN OUT nocopy RIB_OBJECT,
                                      O_routing_info IN OUT nocopy RIB_ROUTINGINFO_TBL,
                                      I_pack_ind     IN OUT item_master.pack_ind%TYPE,
                                      I_sellable_ind IN OUT item_master.sellable_ind%TYPE,
                                      I_message_type IN     alloc_mfqueue.message_type%TYPE,
                                      I_alloc_no     IN     alloc_header.alloc_no%TYPE,
                                      I_max_details  IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_DELETE_OBJECTS( O_error_msg    IN OUT VARCHAR2,
                                      O_message      IN OUT nocopy RIB_OBJECT,
                                      O_routing_info IN OUT nocopy RIB_ROUTINGINFO_TBL,
                                      I_alloc_no     IN     alloc_header.alloc_no%TYPE,
                                      I_max_details  IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
---
FUNCTION GET_ROUTING_TO_LOCS( O_error_msg    IN OUT VARCHAR2,
                              I_alloc_no     IN     alloc_header.alloc_no%TYPE,
                              O_routing_info IN OUT nocopy RIB_ROUTINGINFO_TBL)
RETURN BOOLEAN;
---
FUNCTION GET_NOT_BEFORE_DAYS( O_error_msg    IN OUT VARCHAR2,
                              O_days            OUT NUMBER)
RETURN BOOLEAN;
---
FUNCTION GET_RETAIL( O_error_msg    IN OUT VARCHAR2,
                     I_item         IN     item_loc.item%TYPE,
                     I_loc          IN     item_loc.loc%TYPE,
                     I_loc_type     IN     item_loc.loc_type%TYPE,
                     O_price        IN OUT item_loc.unit_retail%TYPE,
                     O_selling_uom  IN OUT item_loc.selling_uom%TYPE)
RETURN BOOLEAN;
---
PROCEDURE CHECK_STATUS(I_status_code IN VARCHAR2);
---
FUNCTION LOCK_THE_BLOCK( O_error_msg        OUT VARCHAR2,
                         O_queue_locked     OUT BOOLEAN,
                         I_alloc_no      IN     alloc_mfqueue.alloc_no%TYPE)
RETURN BOOLEAN;
---
FUNCTION ROLLUP_DETAILS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message         IN OUT   NOCOPY "RIB_AllocDesc_REC")
RETURN BOOLEAN;
---
FUNCTION ROLLUP_DETAILS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message         IN OUT   NOCOPY "RIB_AllocRef_REC")
RETURN BOOLEAN;
---
FUNCTION SET_ROUTING_TO_INFO(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_routing_info           IN OUT   nocopy RIB_ROUTINGINFO_TBL,
                             I_physical_to_loc        IN       ITEM_LOC.LOC%TYPE,
                             I_physical_to_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE)

RETURN BOOLEAN;
---
-----------------------------------------------------------------------

--------------------------------------------------------------------------------

FUNCTION ADDTOQ(O_error_msg                 OUT VARCHAR2,
                I_message_type          IN      ALLOC_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_alloc_no              IN      ALLOC_HEADER.ALLOC_NO%TYPE,
                I_alloc_header_status   IN      ALLOC_HEADER.STATUS%TYPE,
                I_to_loc                IN      ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_max_details        RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE:=NULL;
   L_thread_no          RIB_SETTINGS.NUM_THREADS%TYPE:=NULL;

   L_num_threads        RIB_SETTINGS.NUM_THREADS%TYPE:=NULL;
   L_min_time_lag       RIB_SETTINGS.MINUTES_TIME_LAG%TYPE:=NULL;

   L_initial_approval   VARCHAR2(1):='N';
   L_status_code        VARCHAR2(1):='N';

   L_to_pwh             WH.PHYSICAL_WH%TYPE := NULL;
   L_alloc_pwh          WH.PHYSICAL_WH%TYPE := NULL;
   L_insert_mfqueue     VARCHAR2(1) := 'N';

   cursor C_HEAD is
      select api.thread_no,
             api.initial_approval_ind
        from alloc_pub_info api
       where api.alloc_no = I_alloc_no;

   cursor C_TO_PWH is
      select physical_wh
        from wh
       where wh = I_to_loc;

   cursor C_ALLOC_PWH is
      select w.physical_wh
        from wh w,
             alloc_header ah
       where ah.alloc_no = I_alloc_no
         and w.wh = ah.wh;

BEGIN

   if I_message_type != HDR_ADD then
      open C_HEAD;
      fetch C_HEAD into L_thread_no,
                        L_initial_approval;
      if C_HEAD%NOTFOUND then
         close C_HEAD;
         O_error_msg := SQL_LIB.CREATE_MSG('PUB_INFO_NOT_FOUND',
                                           'alloc_pub_info',
                                           I_alloc_no,
                                           NULL);
         return FALSE;
      end if;
      close C_HEAD;
   end if;

   if I_message_type in(DTL_ADD,DTL_UPD,DTL_DEL,HDR_DEL,HDR_UPD) then
      if I_message_type = HDR_UPD and
         I_alloc_header_status = 'A' and
         L_initial_approval = 'N' then
            update alloc_pub_info
               set initial_approval_ind = 'Y'
             where alloc_no = I_alloc_no;
         L_initial_approval := 'Y';
      end if;
      if L_initial_approval = 'N' then
         return TRUE;
      end if;
   end if;

   -- If the message is a detail message, all previous records on the queue
   -- relating to the detail record can be deleted.
   if I_message_type = DTL_DEL then

      delete from alloc_mfqueue
       where alloc_no = I_alloc_no
         and to_loc = I_to_loc;

   elsif I_message_type = DTL_UPD then

      delete from alloc_mfqueue
       where alloc_no = I_alloc_no
         and to_loc = I_to_loc
         and message_type = DTL_UPD;

   -- If the message is a header delete, all previous records on the queue
   -- relating to the allocation can be deleted.

   elsif I_message_type = HDR_DEL then

      delete from alloc_mfqueue
       where alloc_no = I_alloc_no;

   end if;


   if I_message_type = HDR_ADD then

      API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                   O_error_msg,
                                   L_max_details,
                                   L_num_threads,
                                   L_min_time_lag,
                                   RMSMFM_ALLOC.FAMILY);
      if L_status_code in (API_CODES.UNHANDLED_ERROR) then
         return FALSE;
      end if;

      insert into alloc_pub_info( alloc_no,
                                  initial_approval_ind,
                                  thread_no,
                                  wh,
                                  physical_wh,
                                  item,
                                  pack_ind,
                                  sellable_ind,
                                  published )
                         values ( I_alloc_no,
                                  DECODE(I_alloc_header_status, 'A', 'Y', 'N'),
                                  MOD(I_alloc_no, L_num_threads)+1,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  'N' );
   else

      -- Retrieve the Physical WH for I_to_loc
      -- If I_to_loc is a Store, no Physcal WH will be retrieved.
      open C_TO_PWH;
      fetch C_TO_PWH into L_to_pwh;
      close C_TO_PWH;

      -- If L_to_pwh has a value, retrieve the Physical WH of alloc_header.wh
      -- Compare the two physical warehouses. If they are the same, do NOT insert
      -- to alloc_mfqueue. If I_to_loc is a store (L_to_pwh is NULL), insert data
      -- in the table as well.  This is to ensure that allocation to a wh in the
      -- same physical warehouse will NOT be published.
      if L_to_pwh is NOT NULL then
         open C_ALLOC_PWH;
         fetch C_ALLOC_PWH into L_alloc_pwh;
         close C_ALLOC_PWH;

         if L_to_pwh = L_alloc_pwh then
            L_insert_mfqueue := 'N';
         else
            L_insert_mfqueue := 'Y';
         end if;
      else
         L_insert_mfqueue := 'Y';
      end if; 

      if L_insert_mfqueue = 'Y' then
         insert into alloc_mfqueue ( seq_no,
                                      alloc_no,
                                      to_loc,
                                      message_type,
                                      thread_no,
                                      family,
                                      custom_message_type,
                                      pub_status,
                                      transaction_number,
                                      transaction_time_stamp )
                             values ( alloc_mfsequence.NEXTVAL,
                                      I_alloc_no,
                                      I_to_loc,
                                      I_message_type,
                                      L_thread_no,
                                      RMSMFM_ALLOC.FAMILY,
                                      'N',
                                      'U',
                                      I_alloc_no,
                                      SYSDATE);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_ALLOC.ADDTOQ',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1)
IS

   L_break_loop         BOOLEAN := FALSE;
   L_message_type       alloc_mfqueue.message_type%TYPE:=NULL;

   L_alloc_no           alloc_header.alloc_no%TYPE:=NULL;
   L_to_loc             alloc_detail.to_loc%TYPE:=NULL;
   L_seq_no             alloc_mfqueue.seq_no%TYPE:=NULL;
   L_seq_limit          tsf_mfqueue.seq_no%TYPE:=0;


   L_pub_status         alloc_mfqueue.pub_status%TYPE:=NULL;
   L_rowid              ROWID  :=NULL;

   L_hdr_published      alloc_pub_info.published%TYPE:=NULL;

   L_hosp               VARCHAR2(1) := 'N';
   L_keep_queue         BOOLEAN := FALSE;
   L_queue_locked       BOOLEAN := FALSE;

   cursor C_QUEUE is
      select q.alloc_no,
             q.to_loc,
             q.message_type,
             q.pub_status,
             q.seq_no,
             q.rowid
        from alloc_mfqueue q
       where q.seq_no = (select min(q2.seq_no)
                           from alloc_mfqueue q2
                          where q2.thread_no = I_thread_val
                            and q2.pub_status = 'U'
                            and q2.seq_no     > L_seq_limit)
         and q.thread_no = I_thread_val;

   cursor C_HOSP is
      select 'Y'
        from alloc_mfqueue
       where alloc_no = L_alloc_no
         and pub_status = API_CODES.HOSPITAL;

   cursor C_HDR_PUB is
      select api.published
        from alloc_pub_info api
       where api.alloc_no = L_alloc_no;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   LOOP

      O_message := null;

      open C_QUEUE;
      fetch C_QUEUE into L_alloc_no,
                         L_to_loc,
                         L_message_type,
                         L_pub_status,
                         L_seq_no,
                         L_rowid;
      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
      close C_QUEUE;

      if LOCK_THE_BLOCK( O_error_msg,
                         L_queue_locked,
                         L_alloc_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked = FALSE then

         open  C_HOSP;
         fetch C_HOSP into L_hosp;
         close C_HOSP;

         if L_hosp = 'Y' then
            O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                              NULL, NULL, NULL);
            raise PROGRAM_ERROR;
         end if;

         open  C_HDR_PUB;
         fetch C_HDR_PUB into L_hdr_published;
         close C_HDR_PUB;

         if PROCESS_QUEUE_RECORD( O_error_msg,
                              L_break_loop,
                              O_message,
                              O_routing_info,
                              O_bus_obj_id,
                              L_message_type,
                              L_alloc_no,
                              L_hdr_published,
                              L_to_loc,
                              L_pub_status,
                              L_seq_no,
                              L_rowid,
                              L_keep_queue) = FALSE then
            raise PROGRAM_ERROR;
         end if;


         if L_break_loop = TRUE then
            O_message_type   := L_message_type;
            EXIT;
         end if;

      else
         L_seq_limit := L_seq_no;
      end if;

   END LOOP;

   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;

   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_alloc_no);
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_alloc_no,
                    L_seq_no,
                    L_to_loc);
END GETNXT;

--------------------------------------------------------------------------------

PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT)
IS

   L_seq_no alloc_mfqueue.seq_no%TYPE:=NULL;

   L_break_loop         BOOLEAN := FALSE;

   L_alloc_no                    alloc_mfqueue.alloc_no%TYPE:=NULL;
   L_hdr_published               alloc_pub_info.published%TYPE:=NULL;
   L_to_loc                      alloc_mfqueue.to_loc%TYPE:=NULL;
   L_message_type                alloc_mfqueue.message_type%TYPE:=NULL;
   L_pub_status                  alloc_mfqueue.pub_status%TYPE:=NULL;
   L_rowid                       ROWID:=NULL;

   L_keep_queue                  BOOLEAN := FALSE;
   L_queue_locked     BOOLEAN := FALSE;

   cursor C_RETRY_QUEUE is
      select q.alloc_no,
             api.published hdr_published,
             q.to_loc,
             q.message_type,
             q.pub_status,
             q.rowid
        from alloc_mfqueue q,
             alloc_pub_info api
       where q.seq_no = L_seq_no
         and q.alloc_no = api.alloc_no;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   --get info from routing info
   ---assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;

   O_message := null;

   --get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_alloc_no,
                            L_hdr_published,
                            L_to_loc,
                            L_message_type,
                            L_pub_status,
                            L_rowid;
   close C_RETRY_QUEUE;

   if LOCK_THE_BLOCK( O_error_msg,
                      L_queue_locked,
                      L_alloc_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then

      if PROCESS_QUEUE_RECORD( O_error_msg,
                           L_break_loop,
                           O_message,
                           O_routing_info,
                           O_bus_obj_id,
                           L_message_type,
                           L_alloc_no,
                           L_hdr_published,
                           L_to_loc,
                           L_pub_status,
                           L_seq_no,
                           L_rowid,
                           L_keep_queue) = FALSE then
            raise PROGRAM_ERROR;
         end if;

      O_message_type   := L_message_type;

      if O_message IS NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         if L_keep_queue then
            O_status_code := API_CODES.INCOMPLETE_MSG;
         else
            O_status_code := API_CODES.NEW_MSG;
         end if;
         O_bus_obj_id := RIB_BUSOBJID_TBL(L_alloc_no);
      end if;

   else
      O_status_code := API_CODES.HOSPITAL;
   end if;


EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_alloc_no,
                    L_seq_no,
                    L_to_loc);
END PUB_RETRY;

--------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT      VARCHAR2,
                        O_error_msg       IN  OUT      VARCHAR2,
                        O_message             OUT      RIB_OBJECT,
                        O_message_type    IN  OUT      VARCHAR2,
                        O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                        O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                        I_alloc_no        IN           alloc_mfqueue.alloc_no%TYPE,
                        I_seq_no          IN           alloc_mfqueue.seq_no%TYPE,
                        I_to_loc          IN           alloc_mfqueue.to_loc%TYPE)
IS

   L_rib_allocdtlref_rec "RIB_AllocDtlRef_REC";
   L_rib_allocdtlref_tbl "RIB_AllocDtlRef_TBL";
   L_rib_allocref_rec    "RIB_AllocRef_REC";

   L_error_type                  VARCHAR2(5) := NULL;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then

      O_bus_obj_id    := RIB_BUSOBJID_TBL(I_alloc_no);
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no', I_seq_no,null,null,null,null));

      L_rib_allocdtlref_rec := "RIB_AllocDtlRef_REC"(0,I_to_loc,NULL,NULL,NULL);
      L_rib_allocdtlref_tbl := "RIB_AllocDtlRef_TBL"(L_rib_allocdtlref_rec);
      L_rib_allocref_rec    := "RIB_AllocRef_REC"(0,
                                                I_alloc_no,
                                                'A',
                                                -1,
                                                -1,
                                                '-1',
                                                L_rib_allocdtlref_tbl);
      O_message := L_rib_allocref_rec;

      update alloc_mfqueue
         set pub_status = LP_error_status
       where seq_no    = I_seq_no;

   end if;

   /* Pass out parsed error message */
   SQL_LIB.API_MSG(L_error_type,
                   O_error_msg);

EXCEPTION
   when OTHERS then
      O_status_code   := API_CODES.UNHANDLED_ERROR;
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_ALLOC.HANDLE_ERRORS',
                                            TO_CHAR(SQLCODE));
END HANDLE_ERRORS;

--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD(O_error_msg           OUT        VARCHAR2,
                              O_break_loop          OUT        BOOLEAN,
                              O_message         IN  OUT NOCOPY RIB_OBJECT,
                              O_routing_info    IN  OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id      IN  OUT NOCOPY RIB_BUSOBJID_TBL,
                              O_message_type    IN  OUT        VARCHAR2,
                              I_alloc_no        IN             ALLOC_MFQUEUE.ALLOC_NO%TYPE,
                              I_hdr_published   IN             ALLOC_PUB_INFO.PUBLISHED%TYPE,
                              I_to_loc          IN             ALLOC_MFQUEUE.TO_LOC%TYPE,
                              I_pub_status      IN             ALLOC_MFQUEUE.PUB_STATUS%TYPE,
                              I_seq_no          IN             ALLOC_MFQUEUE.SEQ_NO%TYPE,
                              I_rowid           IN             ROWID,
                              O_keep_queue      IN OUT         BOOLEAN)
RETURN BOOLEAN IS

   L_first_write_ind     ITEM_MASTER.PACK_IND%TYPE                := NULL;
   L_wh                  ITEM_LOC.LOC%TYPE                        := NULL;
   L_physical_wh         ITEM_LOC.LOC%TYPE                        := NULL;
   L_item                ITEM_MASTER.ITEM%TYPE                    := NULL;
   L_pack_ind            ITEM_MASTER.PACK_IND%TYPE                := NULL;
   L_sellable_ind        ITEM_MASTER.SELLABLE_IND%TYPE            := NULL;
   L_max_details         RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 12;
   L_num_threads         RIB_SETTINGS.NUM_THREADS%TYPE            := NULL;
   L_min_time_lag        RIB_SETTINGS.MINUTES_TIME_LAG%TYPE       := NULL;

   L_rib_routing_rec     RIB_ROUTINGINFO_REC := NULL;
   L_rib_allocdesc_rec   "RIB_AllocDesc_REC" := NULL;
   L_rib_allocref_rec    "RIB_AllocRef_REC"  := NULL;

   L_message             "RIB_AllocDesc_REC" := NULL;
   L_ref_message         "RIB_AllocRef_REC" := NULL;
 
   L_status_code         VARCHAR2(1):= NULL;

   cursor C_HDR_INFO is
      select api.wh,
             api.physical_wh,
             item
        from alloc_pub_info api
       where api.alloc_no = I_alloc_no;

BEGIN

   O_break_loop := TRUE;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_ALLOC.FAMILY);
   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   if O_message_type = HDR_DEL and I_hdr_published = 'N' then

      O_break_loop := FALSE;

      if DELETE_QUEUE_REC(O_error_msg,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;

   elsif O_message_type = HDR_DEL then       --AllocRef

      open C_HDR_INFO;
      fetch C_HDR_INFO into L_wh,
                            L_physical_wh,
                            L_item;
      close C_HDR_INFO;

      L_rib_allocref_rec := "RIB_AllocRef_REC"(0,
                                               I_alloc_no,
                                               'A',
                                               L_physical_wh,
                                               L_wh,
                                               L_item,
                                               NULL);

      --create routing info from table--
      O_routing_info := RIB_ROUTINGINFO_TBL();
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('from_phys_loc',
                                               L_physical_wh,
                                               'from_phys_loc_type',
                                               'W',
                                               NULL,
                                               NULL);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

      if GET_ROUTING_TO_LOCS(O_error_msg,
                             I_alloc_no,
                             O_routing_info) = FALSE then
         return FALSE;
      end if;

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      delete from alloc_pub_info
       where alloc_no     = I_alloc_no;

      delete from alloc_details_published
       where alloc_no     = I_alloc_no;

      if DELETE_QUEUE_REC(O_error_msg,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;

      O_message:= L_rib_allocref_rec;

   elsif I_hdr_published = 'N' or I_hdr_published = 'I' then

      if I_hdr_published = 'N' then
         O_message_type := HDR_ADD;
      else
         O_message_type := DTL_ADD;
      end if;

      -- publish the entire alloc             --AllocDesc (all details)
      if MAKE_CREATE(O_error_msg,
                     O_message,
                     O_routing_info,
                     I_alloc_no,
                     I_seq_no,
                     I_to_loc,
                     L_max_details,
                     I_rowid,
                     O_keep_queue) = FALSE then
         return FALSE;
      end if;

      -- write the current record (anything but HDR_ADD)
   elsif O_message_type = HDR_UPD then          --AllocDesc (no details)

      if BUILD_HEADER_OBJECT(O_error_msg,
                             O_message,
                             L_rib_allocref_rec,
                             L_pack_ind,
                             L_sellable_ind,
                             I_alloc_no) = FALSE then
         return FALSE;
      end if;

      L_rib_allocdesc_rec := TREAT(O_message AS "RIB_AllocDesc_REC");

      update alloc_pub_info
         set wh            = L_rib_allocref_rec.wh,
             physical_wh   = L_rib_allocref_rec.physical_wh,
             item          = L_rib_allocref_rec.item,
             pack_ind      = L_pack_ind,
             sellable_ind  = L_sellable_ind,
             published = 'Y'
       where alloc_no = I_alloc_no;

      --create routing info from table--
      O_routing_info := RIB_ROUTINGINFO_TBL();
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('from_phys_loc',
                                               L_rib_allocdesc_rec.physical_wh,
                                               'from_phys_loc_type',
                                               'W',
                                               NULL,
                                               NULL);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

      if GET_ROUTING_TO_LOCS(O_error_msg,
                             I_alloc_no,
                             O_routing_info) = FALSE then
         return FALSE;
      end if;

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      if DELETE_QUEUE_REC(O_error_msg,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;

   elsif O_message_type in (DTL_ADD, DTL_UPD) then       --AllocDesc (one detail)

      if BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg,
                                     O_message,
                                     O_routing_info,
                                     L_pack_ind,
                                     L_sellable_ind,
                                     O_message_type,
                                     I_alloc_no,
                                     L_max_details) = FALSE then
         return FALSE;
      end if;

   elsif O_message_type = DTL_DEL then       --AllocDtlRef

      if BUILD_DETAIL_DELETE_OBJECTS(O_error_msg,
                                     O_message,
                                     O_routing_info,
                                     I_alloc_no,
                                     L_max_details) = FALSE then
         return FALSE;
      end if;

   end if;

   if O_message_type in (HDR_ADD, DTL_ADD, DTL_UPD) then
      L_message := treat(O_message AS "RIB_AllocDesc_REC");

      if ROLLUP_DETAILS(O_error_msg,
                        L_message) = FALSE then
         return FALSE;
      end if;

      O_message := L_message;

   elsif O_message_type = DTL_DEL then
      L_ref_message := treat(O_message AS "RIB_AllocRef_REC");

      if ROLLUP_DETAILS(O_error_msg,
                        L_ref_message) = FALSE then
         return FALSE;
      end if;

      O_message := L_ref_message;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_ALLOC.PROCESS_QUEUE_RECORD',
                                        to_char(SQLCODE));
      return FALSE;
END PROCESS_QUEUE_RECORD;
--------------------------------------------------------------------------------

FUNCTION MAKE_CREATE( O_error_msg       IN OUT VARCHAR2,
                      O_message         IN OUT nocopy RIB_OBJECT,
                      O_routing_info    IN OUT nocopy RIB_ROUTINGINFO_TBL,
                      I_alloc_no        IN     alloc_header.alloc_no%TYPE,
                      I_seq_no          IN     alloc_mfqueue.seq_no%TYPE,
                      I_to_loc          IN     item_loc.loc%TYPE,
                      I_max_details     IN     rib_settings.max_details_to_publish%TYPE,
                      I_rowid           IN     ROWID,
                      O_keep_queue      IN OUT BOOLEAN)

RETURN BOOLEAN IS

   L_sellable_ind           item_master.sellable_ind%TYPE := NULL;
   L_pack_ind               item_master.pack_ind%TYPE := NULL;

   L_rib_allocdesc_rec      "RIB_AllocDesc_REC" := NULL;
   L_rib_allocdtl_tbl       "RIB_AllocDtl_TBL" := NULL;

   L_alloc_mfqueue_rowid    rowid_TBL;
   L_alloc_mfqueue_size     BINARY_INTEGER := 0;

   L_adp_ins_to_loc         alloc_details_pub_to_loc_TBL;
   L_adp_ins_alloc          alloc_details_pub_TBL;
   L_adp_ins_size           BINARY_INTEGER := 0;

   L_adp_upd_rowid          rowid_TBL;
   L_adp_upd_size           BINARY_INTEGER := 0;

   L_rib_routing_rec RIB_ROUTINGINFO_REC := NULL;
   L_delete_rowid_ind       VARCHAR2(1) := 'Y';

   L_rib_allocref_rec   "RIB_AllocRef_REC" := NULL;

   PROGRAM_ERROR         EXCEPTION;

BEGIN

   if BUILD_HEADER_OBJECT( O_error_msg,
                        L_rib_allocdesc_rec,
                        L_rib_allocref_rec,
                        L_pack_ind,
                        L_sellable_ind,
                        I_alloc_no) = FALSE then
      return FALSE;
   end if;


   O_routing_info := RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('from_phys_loc',L_rib_allocdesc_rec.physical_wh,
                                            'from_phys_loc_type','W',null,null);
   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   if BUILD_DETAIL_OBJECTS( O_error_msg,
                         L_rib_allocdtl_tbl,
                         L_alloc_mfqueue_rowid,
                         L_alloc_mfqueue_size,
                         L_adp_ins_to_loc,
                         L_adp_ins_alloc,
                         L_adp_ins_size,
                         L_adp_upd_rowid,
                         L_adp_upd_size,
                         O_routing_info,
                         L_delete_rowid_ind,
                         null,
                         L_pack_ind,
                         L_sellable_ind,
                         I_alloc_no,
                         L_rib_allocdesc_rec.item,
                         I_max_details) = FALSE then
      return FALSE;
   end if;


   LP_error_status := API_CODES.UNHANDLED_ERROR;

   --- if flag from BUILD_DETAIL_OBJECTS is TRUE, add rowid to L_alloc_mfqueue_rowid table;
   if L_delete_rowid_ind = 'Y' then
      L_alloc_mfqueue_size := L_alloc_mfqueue_size + 1;
      L_alloc_mfqueue_rowid(L_alloc_mfqueue_size) := I_rowid;
   else
      O_keep_queue := TRUE;
   end if;

   update alloc_pub_info
      set wh            = L_rib_allocref_rec.wh,
          physical_wh   = L_rib_allocref_rec.physical_wh,
          item          = L_rib_allocref_rec.item,
          pack_ind      = L_pack_ind,
          sellable_ind  = L_sellable_ind,
          published = DECODE(L_delete_rowid_ind, 'Y', 'Y', 'I')
    where alloc_no = I_alloc_no;

   -- add the detail to the header
   L_rib_allocdesc_rec.allocdtl_tbl := L_rib_allocdtl_tbl;

   if L_alloc_mfqueue_size > 0 then
      FORALL i IN 1..L_alloc_mfqueue_size
         delete from alloc_mfqueue where rowid = L_alloc_mfqueue_rowid(i);
   end if;

  if L_adp_ins_size > 0 then
      FORALL i IN 1..L_adp_ins_size
         insert into alloc_details_published (alloc_no,
                                              to_loc_vir,
                                              detail_exists_ind)
                                      values (L_adp_ins_alloc(i),
                                              L_adp_ins_to_loc(i),
                                              'Y');
   end if;

   if L_adp_upd_size > 0 then
      FORALL i IN 1..L_adp_upd_size
         update alloc_details_published
            set detail_exists_ind = 'Y'
          where rowid = L_adp_upd_rowid(i);
   end if;

   O_message := L_rib_allocdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_ALLOC.MAKE_CREATE',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END MAKE_CREATE;

------------------------------------------------------------------------------------------

FUNCTION DELETE_QUEUE_REC( O_error_msg   OUT VARCHAR2,
                           I_seq_no      IN alloc_mfqueue.seq_no%TYPE)
RETURN BOOLEAN IS

BEGIN

   delete from alloc_mfqueue
    where seq_no = I_seq_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_ALLOC.DELETE_QUEUE_REC',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_QUEUE_REC;

------------------------------------------------------------------------------------------

FUNCTION BUILD_HEADER_OBJECT( O_error_msg         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message          IN OUT nocopy RIB_OBJECT,
                               O_rib_allocref_rec IN OUT nocopy "RIB_AllocRef_REC",
                               O_pack_ind         IN OUT item_master.pack_ind%TYPE,
                               O_sellable_ind     IN OUT item_master.sellable_ind%TYPE,
                               I_alloc_no         IN     alloc_header.alloc_no%TYPE)
RETURN BOOLEAN IS

   --C_ALLOC_HEAD
   L_item          alloc_header.item%TYPE := NULL;
   L_order_no      alloc_header.order_no%TYPE := NULL;
   L_order_type    alloc_header.order_type%TYPE := NULL;
   L_not_before_date  DATE := NULL;
   L_not_after_date   DATE := NULL;
   L_wh            alloc_header.wh%TYPE := NULL;
   L_physical_wh   alloc_header.wh%TYPE := NULL;
   L_alloc_status  alloc_header.status%TYPE := NULL;
   L_seq_no        alloc_mfqueue.seq_no%TYPE := NULL;
   L_context_type  alloc_header.context_type%TYPE := NULL;
   L_context_value alloc_header.context_value%TYPE := NULL;

   --C_ORDER_LOOKUP
   L_promotion     ordhead.promotion%TYPE := NULL;
   L_event         RPM_PROMO_EVENT.PROMO_EVENT_ID%TYPE := NULL;
   L_event_desc    RPM_PROMO_EVENT.DESCRIPTION%TYPE := NULL;
   L_pm_promo_rec  PM_PROMO_SQL.PROMO_REC := NULL;

   L_ticket_type_id   item_ticket.ticket_type_id%TYPE := NULL;
   L_rib_allocdesc_rec      "RIB_AllocDesc_REC" := NULL;

   L_days NUMBER(38):=NULL;

   PROGRAM_ERROR         EXCEPTION;

   cursor C_ALLOC_HEAD is
      select ah.item,
             ah.order_no,
             ah.order_type,
             nvl(ah.release_date, oh.not_before_date),
             nvl(ah.release_date, oh.not_before_date) + L_days,
             ah.wh,
             w.physical_wh,
             ah.status,
             im.pack_ind,
             im.sellable_ind,
             it.ticket_type_id,
             ah.context_type,
             ah.context_value
        from alloc_header ah,
             item_master im,
             item_ticket it,
             wh w,
             ordhead oh
       where ah.alloc_no = I_alloc_no
         and ah.wh       = w.wh
         and ah.item     = im.item
         and ah.item     = it.item(+)
         and it.po_print_type(+) = 'R'    -- PO Receipt time
         --
         and oh.order_no (+) = ah.order_no;

   cursor C_ORDER_LOOKUP is
      select oh.promotion
        from ordhead oh
       where oh.order_no  = L_order_no;

BEGIN

   if GET_NOT_BEFORE_DAYS( O_error_msg,
                           L_days) = FALSE then
      return FALSE;
   end if;


   open C_ALLOC_HEAD;
   fetch C_ALLOC_HEAD into L_item,
                           L_order_no,
                           L_order_type,
                           L_not_before_date,
                           L_not_after_date,
                           L_wh,
                           L_physical_wh,
                           L_alloc_status,
                           O_pack_ind,
                           O_sellable_ind,
                           L_ticket_type_id,
                           L_context_type,
                           L_context_value;
   if C_ALLOC_HEAD%NOTFOUND then
      close C_ALLOC_HEAD;
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ALLOC_HEADER_PUB',
                                        I_alloc_no, NULL, NULL);
      return FALSE;
   end if;
   close C_ALLOC_HEAD;

   if L_order_no IS NOT NULL then
      open C_ORDER_LOOKUP;
      fetch C_ORDER_LOOKUP into L_promotion;
      close C_ORDER_LOOKUP;
      --
      if L_promotion is not null then
         if PM_PROMO_SQL.GET_PROMO(O_error_msg,
                                   L_promotion,
                                   L_pm_promo_rec) = FALSE then
            return FALSE;
         end if;
      end if;
      --
      L_event := L_pm_promo_rec.promo_event_id;
      L_event_desc := L_pm_promo_rec.promo_event_desc;
   end if;
   
   if L_alloc_status = 'C' then
      L_not_after_date := L_not_before_date - 1;
   end if;

   L_rib_allocdesc_rec := "RIB_AllocDesc_REC"(
        0,                  --rib_oid              NUMBER
        I_alloc_no,         --alloc_no             NUMBER(10),
        'A',                --doc_type             VARCHAR2(1),
        L_physical_wh,      --physical_wh          NUMBER(10,0),
        L_wh,               --wh                   NUMBER(10),
        L_item,             --item                 VARCHAR2(25),
        L_not_before_date,  --pick_not_before_date DATE,
        L_not_after_date,   --pick_not_after_date  DATE,
        L_order_type,       --order_type           VARCHAR2(3),
        L_order_no,         --order_no             NUMBER(12),
        'P',                --order_doc_type       VARCHAR2(1),
        L_event,            --event                VARCHAR2(6),
        L_event_desc,       --event_description    VARCHAR2(1000),
        1,                  --priority             NUMBER(4),
        L_ticket_type_id,   --ticket_type_id       VARCHAR2(4),
        null,               --AllocDtl_TBL         "RIB_AllocDtl_TBL",
        L_context_type,     --context_type         VARCHAR2(6),
        L_context_value,    --context_value        VARCHAR2(25),
        L_alloc_status);    --alloc_status         VARCHAR2(1)      

   O_rib_allocref_rec := "RIB_AllocRef_REC"(
        0,
        I_alloc_no,
        'A',
        L_physical_wh,
        L_wh,
        L_item,
        null);

   O_message := L_rib_allocdesc_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_ALLOC.BUILD_HEADER_OBJECT',
                                        TO_CHAR(SQLCODE));
      return FALSE;

END BUILD_HEADER_OBJECT;

------------------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_OBJECTS( O_error_msg      IN OUT VARCHAR2,
                               O_message        IN OUT nocopy "RIB_AllocDtl_TBL",
                               O_alloc_mfqueue_rowid  IN OUT nocopy rowid_TBL,
                               O_alloc_mfqueue_size   IN OUT        BINARY_INTEGER,
                               O_adp_ins_to_loc       IN OUT nocopy alloc_details_pub_to_loc_TBL,
                               O_adp_ins_alloc        IN OUT nocopy alloc_details_pub_TBL,
                               O_adp_ins_size         IN OUT        BINARY_INTEGER,
                               O_adp_upd_rowid        IN OUT nocopy rowid_TBL,
                               O_adp_upd_size         IN OUT        BINARY_INTEGER,
                               O_routing_info         IN OUT nocopy RIB_ROUTINGINFO_TBL,
                               O_delete_rowid_ind     IN OUT        VARCHAR2,
                               I_message_type   IN     alloc_mfqueue.message_type%TYPE,
                               I_pack_ind       IN     item_master.pack_ind%TYPE,
                               I_sellable_ind   IN     item_master.sellable_ind%TYPE,
                               I_alloc_no       IN     alloc_header.alloc_no%TYPE,
                               I_item           IN     item_master.item%TYPE,
                               I_max_details    IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN IS

   --C_ALLOC_DETAIL
   L_to_loc            item_loc.loc%TYPE := NULL;
   L_physical_to_loc   item_loc.loc%TYPE := NULL;
   L_loc_type          item_loc.loc_type%TYPE := NULL;
   L_qty_allocated     alloc_detail.qty_allocated%TYPE := NULL;
   L_store_order_mult  item_loc.store_ord_mult%TYPE := NULL;

   L_rib_allocdtl_rec  "RIB_AllocDtl_REC" := NULL;
   L_rib_allocdtl_tbl  "RIB_AllocDtl_TBL" := NULL;

   L_rib_routing_rec   RIB_ROUTINGINFO_REC :=NULL;

   L_prev_to_loc       alloc_detail.to_loc%TYPE := -1;
   L_records_found     BOOLEAN := FALSE;

   L_details_processed   RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 0;

   cursor C_ALLOC_DETAIL is
      select ad.to_loc,
             decode(w.wh, null, ad.to_loc, w.physical_wh) physical_to_loc,
             il.loc_type,
             case
                when ad.to_loc_type = 'S' then
                   (select store_type
                      from store
                     where store = ad.to_loc)
                else
                   NULL
             end store_type,
             case
                when ad.to_loc_type = 'S' then
                   (select stockholding_ind
                      from store
                     where store = ad.to_loc)
                else
                   NULL
             end stockholding_ind,
             ad.qty_allocated,
             il.store_ord_mult,
             ad.rush_flag,
             ad.in_store_date,
             aq.rowid aq_rowid,
             adp.rowid adp_rowid
        from alloc_detail ad,
             alloc_mfqueue aq,
             item_loc il,
             alloc_details_published adp,
             wh w
       where aq.alloc_no     = I_alloc_no
         and ad.alloc_no     = aq.alloc_no
         and il.item         = I_item
         and ad.to_loc       = il.loc
         and ad.to_loc       = aq.to_loc
         and aq.message_type = I_message_type
         and aq.alloc_no     = adp.alloc_no (+)
         and aq.to_loc       = adp.to_loc_vir (+)
         and aq.to_loc       = w.wh (+)
         and rownum         <= I_max_details
         order by 1;

   cursor C_ALLOC_DETAIL_MC is
      select ad.to_loc,
             decode(w.wh, null, ad.to_loc, w.physical_wh) physical_to_loc,
             il.loc_type,
             case
                when ad.to_loc_type = 'S' then
                   (select store_type
                      from store
                     where store = ad.to_loc)
                else
                   NULL
             end store_type,
             case
                when ad.to_loc_type = 'S' then
                   (select stockholding_ind
                      from store
                     where store = ad.to_loc)
                else
                   NULL
             end stockholding_ind,
             ad.qty_allocated,
             il.store_ord_mult,
             ad.rush_flag,
             ad.in_store_date,
             ar.rowid ar_rowid,
             NULL aq_rowid
        from alloc_detail ad,
             alloc_details_published ar,
             item_loc il,
             wh w
       where ad.alloc_no     = I_alloc_no
         and il.item         = I_item
         and ad.to_loc       = il.loc
         and ad.to_loc       = ar.to_loc_vir (+)
         and ad.alloc_no     = ar.alloc_no (+)
         and ad.to_loc       = w.wh (+)
         and (il.loc_type = 'W' and w.physical_wh NOT in (select physical_wh
                                                            from wh,
                                                                 alloc_header ah
                                                           where ah.alloc_no = I_alloc_no
                                                             and wh.wh = ah.wh )
              or il.loc_type = 'S')
   UNION ALL
      select aq.to_loc,
             decode(w.wh, null, aq.to_loc, w.physical_wh) physical_to_loc,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             ar.rowid ar_rowid,
             aq.rowid aq_rowid
        from alloc_mfqueue aq,
             alloc_details_published ar,
             wh w
       where aq.alloc_no     = I_alloc_no
         and aq.message_type in (DTL_DEL, DTL_UPD, DTL_ADD)
         and aq.to_loc       = ar.to_loc_vir (+)
         and aq.alloc_no     = ar.alloc_no (+)
         and aq.to_loc       = w.wh (+)
         order by 7 desc, 1;

BEGIN

   L_rib_allocdtl_rec := "RIB_AllocDtl_REC"(
           0, null,null,null,null,null,null,null,
              null,null,null,null,null,null);

   if O_message is NULL then
      L_rib_allocdtl_tbl := "RIB_AllocDtl_TBL"();
   else
      L_rib_allocdtl_tbl := O_message;
   end if;

   if I_message_type is NULL then

      FOR rec IN C_ALLOC_DETAIL_MC LOOP

         if rec.loc_type is NOT NULL and rec.ar_rowid is NULL then

            L_records_found := TRUE;
            
            L_prev_to_loc := rec.to_loc;

            if L_details_processed >= I_max_details then
               O_delete_rowid_ind := 'N';
               EXIT;
            end if;

            ----call procedure

            if BUILD_SINGLE_DETAIL ( O_error_msg,
                                  L_rib_allocdtl_tbl,
                                  L_rib_allocdtl_rec,
                                  rec.to_loc,
                                  rec.physical_to_loc,
                                  rec.loc_type,
                                  rec.store_type,
                                  rec.stockholding_ind,
                                  rec.qty_allocated,
                                  rec.store_ord_mult,
                                  rec.rush_flag,
                                  rec.in_store_date,
                                  I_message_type,
                                  I_pack_ind,
                                  I_sellable_ind,
                                  I_alloc_no,
                                  I_item,
                                  I_max_details) = FALSE then
               return FALSE;
            end if;

            --create routing info
            if SET_ROUTING_TO_INFO(O_error_msg,
                                   O_routing_info,
                                   rec.physical_to_loc,
                                   rec.loc_type) = FALSE then
               return FALSE;
            end if;

            O_adp_ins_size := O_adp_ins_size + 1;
            O_adp_ins_to_loc(O_adp_ins_size) := rec.to_loc;
            O_adp_ins_alloc(O_adp_ins_size) := I_alloc_no;

            L_details_processed := L_details_processed + 1;

         end if;

         if rec.aq_rowid is not null and rec.ar_rowid is null then
            O_alloc_mfqueue_size := O_alloc_mfqueue_size + 1;
            O_alloc_mfqueue_rowid(O_alloc_mfqueue_size) := rec.aq_rowid;
         end if;

      END LOOP;

   else
   
      FOR rec IN C_ALLOC_DETAIL LOOP

         L_records_found := TRUE;

         L_prev_to_loc := rec.to_loc;

         ----call procedure

         if BUILD_SINGLE_DETAIL (O_error_msg,
                                 L_rib_allocdtl_tbl,
                                 L_rib_allocdtl_rec,
                                 rec.to_loc,
                                 rec.physical_to_loc,
                                 rec.loc_type,
                                 rec.store_type,
                                 rec.stockholding_ind,
                                 rec.qty_allocated,
                                 rec.store_ord_mult,
                                 rec.rush_flag,
                                 rec.in_store_date,
                                 I_message_type,
                                 I_pack_ind,
                                 I_sellable_ind,
                                 I_alloc_no,
                                 I_item,
                                 I_max_details) = FALSE then
            return FALSE;
         end if;

         --create routing info
         if SET_ROUTING_TO_INFO(O_error_msg,
                                O_routing_info,
                                rec.physical_to_loc,
                                rec.loc_type) = FALSE then
            return FALSE;
         end if;

         if I_message_type = DTL_ADD then
            if rec.adp_rowid is NULL then
               O_adp_ins_size := O_adp_ins_size + 1;
               O_adp_ins_to_loc(O_adp_ins_size) := rec.to_loc;
               O_adp_ins_alloc(O_adp_ins_size) := I_alloc_no;
            else
               O_adp_upd_size := O_adp_upd_size + 1;
               O_adp_upd_rowid(O_adp_upd_size) := rec.adp_rowid;
            end if;
         end if;

         L_details_processed := L_details_processed + 1;

         if rec.aq_rowid is not null then
            O_alloc_mfqueue_size := O_alloc_mfqueue_size + 1;
            O_alloc_mfqueue_rowid(O_alloc_mfqueue_size) := rec.aq_rowid;
         end if;

      END LOOP;
   end if;


   -- if no data found in cursor, raise error
   if not L_records_found then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ALLOC_DETAIL_PUB',
                                        I_alloc_no, NULL, NULL);
      return FALSE;
   end if;

   if L_rib_allocdtl_tbl.COUNT > 0 then
      O_message := L_rib_allocdtl_tbl;
   else
      O_message := NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_ALLOC.BUILD_DETAIL_OBJECTS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_OBJECTS;

------------------------------------------------------------------------------------------

FUNCTION BUILD_SINGLE_DETAIL (O_error_msg            IN OUT VARCHAR2,
                              O_message              IN OUT nocopy    "RIB_AllocDtl_TBL",
                              IO_rib_allocdtl_rec    IN OUT nocopy    "RIB_AllocDtl_REC",
                              I_to_loc               IN               alloc_detail.to_loc%TYPE,
                              I_physical_to_loc      IN               alloc_detail.to_loc%TYPE,
                              I_loc_type             IN               alloc_detail.to_loc_type%TYPE,
                              I_store_type           IN               store.store_type%TYPE,
                              I_stockholding_ind     IN               store.stockholding_ind%TYPE,
                              I_qty_allocated        IN               alloc_detail.qty_allocated%TYPE,
                              I_store_ord_mult       IN               item_loc.store_ord_mult%TYPE,
                              I_rush_flag            IN               alloc_detail.rush_flag%TYPE,
                              I_in_store_date        IN               alloc_detail.in_store_date%TYPE,
                              I_message_type         IN               alloc_mfqueue.message_type%TYPE,
                              I_pack_ind             IN               item_master.pack_ind%TYPE,
                              I_sellable_ind         IN               item_master.sellable_ind%TYPE,
                              I_alloc_no             IN               alloc_header.alloc_no%TYPE,
                              I_item                 IN               item_master.item%TYPE,
                              I_max_details          IN               rib_settings.max_details_to_publish%TYPE)

RETURN BOOLEAN IS

   L_price             item_loc.unit_retail%TYPE := NULL;
   L_selling_uom       item_loc.selling_uom%TYPE := NULL;

   --C_COMP_ITEM
   L_comp_price        item_loc.unit_retail%TYPE := NULL;
   L_comp_selling_uom  item_loc.selling_uom%TYPE := NULL;

   L_rib_allocdtltckt_rec    "RIB_AllocDtlTckt_REC" := NULL;
   L_rib_allocdtltckt_tbl    "RIB_AllocDtlTckt_TBL" := NULL;

  cursor C_COMP_ITEM is
     select item
       from v_packsku_qty
      where pack_no = I_item;
BEGIN

   if I_sellable_ind = 'Y' then

      if GET_RETAIL( O_error_msg,
                 I_item,
                 I_to_loc,
                 I_loc_type,
                 L_price,
                 L_selling_uom) = FALSE then
         return FALSE;
      end if;

   end if;

   if I_loc_type = 'W' then
      IO_rib_allocdtl_rec.to_loc := I_physical_to_loc;
   else
      IO_rib_allocdtl_rec.to_loc := I_to_loc;
   end if;
   
   IO_rib_allocdtl_rec.physical_to_loc  := I_physical_to_loc;
   IO_rib_allocdtl_rec.loc_type         := I_loc_type;
   IO_rib_allocdtl_rec.store_type       := I_store_type;
   IO_rib_allocdtl_rec.stockholding_ind := I_stockholding_ind;
   IO_rib_allocdtl_rec.qty_allocated    := I_qty_allocated;
   IO_rib_allocdtl_rec.price            := L_price;
   IO_rib_allocdtl_rec.selling_uom      := L_selling_uom;
   IO_rib_allocdtl_rec.priority         := 1;
   IO_rib_allocdtl_rec.store_ord_mult   := I_store_ord_mult;
   IO_rib_allocdtl_rec.rush_flag        := I_rush_flag;
   IO_rib_allocdtl_rec.in_store_date    := I_in_store_date;

   if I_sellable_ind = 'Y' AND I_pack_ind = 'Y' AND I_loc_type = 'S' then

      L_rib_allocdtltckt_rec := "RIB_AllocDtlTckt_REC"(
              0, null,null,null);
      L_rib_allocdtltckt_tbl := "RIB_AllocDtlTckt_TBL"();

      FOR comp_rec IN C_COMP_ITEM LOOP

         if GET_RETAIL( O_error_msg,
                    comp_rec.item,
                    I_to_loc,
                    I_loc_type,
                    L_comp_price,
                    L_comp_selling_uom) = FALSE then
            return FALSE;
         end if;


         L_rib_allocdtltckt_rec.comp_item        := comp_rec.item;
         L_rib_allocdtltckt_rec.comp_price       := L_comp_price;
         L_rib_allocdtltckt_rec.comp_selling_uom := L_comp_selling_uom;

         L_rib_allocdtltckt_tbl.EXTEND;
         L_rib_allocdtltckt_tbl(L_rib_allocdtltckt_tbl.COUNT) := L_rib_allocdtltckt_rec;

      END LOOP;

      IO_rib_allocdtl_rec.allocdtltckt_tbl := L_rib_allocdtltckt_tbl;

   end if;

   O_message.EXTEND;
   O_message(O_message.COUNT) := IO_rib_allocdtl_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_ALLOC.BUILD_SINGLE_DETAIL',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_SINGLE_DETAIL;

------------------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_CHANGE_OBJECTS( O_error_msg    IN OUT VARCHAR2,
                                      O_message      IN OUT nocopy RIB_OBJECT,
                                      O_routing_info IN OUT nocopy RIB_ROUTINGINFO_TBL,
                                      I_pack_ind     IN OUT item_master.pack_ind%TYPE,
                                      I_sellable_ind IN OUT item_master.sellable_ind%TYPE,
                                      I_message_type IN     alloc_mfqueue.message_type%TYPE,
                                      I_alloc_no     IN     alloc_header.alloc_no%TYPE,
                                      I_max_details  IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN IS

   L_sellable_ind           item_master.sellable_ind%TYPE := I_sellable_ind;
   L_pack_ind               item_master.pack_ind%TYPE := I_pack_ind;

   L_rib_allocdesc_rec      "RIB_AllocDesc_REC" := NULL;
   L_rib_allocref_rec       "RIB_AllocRef_REC" := NULL;

   L_rib_routing_rec        RIB_ROUTINGINFO_REC := NULL;

   L_alloc_mfqueue_rowid    rowid_TBL;
   L_alloc_mfqueue_size     BINARY_INTEGER := 0;

   L_adp_ins_to_loc         alloc_details_pub_to_loc_TBL;
   L_adp_ins_alloc          alloc_details_pub_TBL;
   L_adp_ins_size           BINARY_INTEGER := 0;

   L_adp_upd_rowid          rowid_TBL;
   L_adp_upd_size           BINARY_INTEGER := 0;

   L_delete_rowid_ind    VARCHAR2(1);

   PROGRAM_ERROR         EXCEPTION;

BEGIN

   if O_message is NULL then
      if BUILD_HEADER_OBJECT( O_error_msg,
            L_rib_allocdesc_rec,
            L_rib_allocref_rec,
            L_pack_ind,
            L_sellable_ind,
            I_alloc_no) = FALSE then
         return FALSE;
      end if;
   else
      L_rib_allocdesc_rec := treat (O_message as "RIB_AllocDesc_REC");
   end if;

   O_routing_info := RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('from_phys_loc',L_rib_allocdesc_rec.physical_wh,
                                            'from_phys_loc_type','W',null,null);
   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   if BUILD_DETAIL_OBJECTS( O_error_msg,
                         L_rib_allocdesc_rec.allocdtl_tbl,
                         L_alloc_mfqueue_rowid,
                         L_alloc_mfqueue_size,
                         L_adp_ins_to_loc,
                         L_adp_ins_alloc,
                         L_adp_ins_size,
                         L_adp_upd_rowid,
                         L_adp_upd_size,
                         O_routing_info,
                         L_delete_rowid_ind,
                         I_message_type,
                         L_pack_ind,
                         L_sellable_ind,
                         I_alloc_no,
                         L_rib_allocdesc_rec.item,
                         I_max_details) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_adp_ins_size > 0 then
      FORALL i IN 1..L_adp_ins_size
         insert into alloc_details_published (alloc_no,
                                              to_loc_vir,
                                              detail_exists_ind)
                                      values (L_adp_ins_alloc(i),
                                              L_adp_ins_to_loc(i),
                                              'Y');
   end if;

   if L_alloc_mfqueue_size > 0 then
      FORALL i IN 1..L_alloc_mfqueue_size
         delete from alloc_mfqueue where rowid = L_alloc_mfqueue_rowid(i);
   end if;

   if L_adp_upd_size > 0 then
      FORALL i IN 1..L_adp_upd_size
         update alloc_details_published
            set detail_exists_ind = 'Y'
          where rowid = L_adp_upd_rowid(i);
   end if;

   O_message := L_rib_allocdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_ALLOC.BUILD_DETAIL_CHANGE_OBJECT',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_CHANGE_OBJECTS;

------------------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_DELETE_OBJECTS( O_error_msg    IN OUT VARCHAR2,
                                      O_message      IN OUT nocopy RIB_OBJECT,
                                      O_routing_info IN OUT nocopy RIB_ROUTINGINFO_TBL,
                                      I_alloc_no     IN     alloc_header.alloc_no%TYPE,
                                      I_max_details  IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN IS

   L_alloc_mfqueue_rowid    rowid_TBL;
   L_alloc_mfqueue_size     BINARY_INTEGER := 0;

   L_rib_allocref_rec    "RIB_AllocRef_REC" := NULL;

   L_rib_allocdtlref_tbl "RIB_AllocDtlRef_TBL" := NULL;
   L_rib_allocdtlref_rec "RIB_AllocDtlRef_REC" := NULL;

   L_alloc_details_pub_rowid     rowid_TBL;
   L_alloc_details_pub_size      BINARY_INTEGER := 0;

   L_wh               ITEM_LOC.LOC%TYPE  := NULL;
   L_physical_wh      ITEM_LOC.LOC%TYPE  := NULL;
   L_item             ITEM_LOC.ITEM%TYPE := NULL;

   L_rib_routing_rec   RIB_ROUTINGINFO_REC := NULL;

   cursor C_HEADER_REF_DELETE is
      select api.wh,
             api.physical_wh,
             item
        from alloc_pub_info api
       where api.alloc_no = I_alloc_no;

   cursor C_DETAIL_DELETE is
      select decode(w.wh, null, aq.to_loc, w.physical_wh) physical_to_loc,
             decode(w.wh, null, 'S', 'W') to_loc_type,
             case
                when w.wh is NULL then -- to_loc_type = 'S'
                   (select store_type
                      from store
                     where store = aq.to_loc)
                else
                   NULL
             end store_type,
             case
                when w.wh is NULL then -- to_loc_type = 'S'
                   (select stockholding_ind
                      from store
                     where store = aq.to_loc)
                else
                   NULL
             end stockholding_ind,
             aq.rowid aq_rowid,
             adp.rowid adp_rowid
        from alloc_mfqueue aq,
             alloc_details_published adp,
             wh w
       where aq.alloc_no     = I_alloc_no
         and aq.message_type = DTL_DEL
         and aq.alloc_no     = adp.alloc_no(+)
         and aq.to_loc       = adp.to_loc_vir(+)
         and aq.to_loc       = w.wh (+)
         and rownum         <= I_max_details;

BEGIN

   L_rib_allocdtlref_tbl := "RIB_AllocDtlRef_TBL"();
   L_rib_allocdtlref_rec := "RIB_AllocDtlRef_REC"(0,null);

   open C_HEADER_REF_DELETE;
   fetch C_HEADER_REF_DELETE into L_wh,
                                  L_physical_wh,
                                  L_item;
   close C_HEADER_REF_DELETE;

   L_rib_allocref_rec := "RIB_AllocRef_REC"(
        0,
        I_alloc_no,
        'A',
        L_physical_wh,
        L_wh,
        L_item,
        null);

   O_routing_info := RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('from_phys_loc',L_physical_wh,
                                            'from_phys_loc_type','W',null,null);
   O_routing_info.EXTEND;

   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   FOR rec IN C_DETAIL_DELETE LOOP

      if rec.adp_rowid IS NOT NULL then
         L_rib_allocdtlref_rec.to_loc := rec.physical_to_loc;
         L_rib_allocdtlref_rec.loc_type := rec.to_loc_type;
         L_rib_allocdtlref_rec.store_type := rec.store_type;
         L_rib_allocdtlref_rec.stockholding_ind := rec.stockholding_ind;
         L_rib_allocdtlref_tbl.EXTEND;
         L_rib_allocdtlref_tbl(L_rib_allocdtlref_tbl.COUNT) := L_rib_allocdtlref_rec;

         L_alloc_details_pub_size := L_alloc_details_pub_size + 1;
         L_alloc_details_pub_rowid(L_alloc_details_pub_size) := rec.adp_rowid;
      end if;

      L_alloc_mfqueue_size := L_alloc_mfqueue_size + 1;
      L_alloc_mfqueue_rowid(L_alloc_mfqueue_size) := rec.aq_rowid;

      --create routing info
      if SET_ROUTING_TO_INFO(O_error_msg,
                             O_routing_info,
                             rec.physical_to_loc,
                             rec.to_loc_type) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_alloc_details_pub_size > 0 then
      FORALL i IN 1..L_alloc_details_pub_size
         update alloc_details_published
            set detail_exists_ind = 'N'
          where rowid = L_alloc_details_pub_rowid(i);
   end if;

   if L_alloc_mfqueue_size > 0 then
      FORALL i IN 1..L_alloc_mfqueue_size
         delete from alloc_mfqueue where rowid = L_alloc_mfqueue_rowid(i);
   end if;

   if L_rib_allocdtlref_tbl.COUNT > 0 then
      L_rib_allocref_rec.allocdtlref_tbl := L_rib_allocdtlref_tbl;
      O_message := L_rib_allocref_rec;
   else
      O_message := NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_ALLOC.BUILD_DETAIL_DELETE_OBJECTS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_DELETE_OBJECTS;

------------------------------------------------------------------------------------------
FUNCTION GET_ROUTING_TO_LOCS(O_error_msg      IN OUT          VARCHAR2,
                             I_alloc_no       IN              ALLOC_HEADER.ALLOC_NO%TYPE,
                             O_routing_info   IN OUT NOCOPY   RIB_ROUTINGINFO_TBL)
RETURN BOOLEAN IS

   L_rib_routing_rec   RIB_ROUTINGINFO_REC := NULL;

   cursor C_TO_LOCS is
      select physical_to_loc,
             to_loc_type,
             MIN(to_loc_vir)
        from (select DECODE(w.wh, NULL, to_loc_vir, w.physical_wh) physical_to_loc,
                     DECODE(w.wh, NULL, 'S', 'W') to_loc_type,
                     to_loc_vir
                from alloc_details_published,
                  wh w
               where alloc_no = I_alloc_no
                 and to_loc_vir = w.wh (+)) inner
       group by physical_to_loc, to_loc_type;

BEGIN

   L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc',
                                            NULL,
                                            'to_phys_loc_type',
                                            NULL,
                                            NULL,
                                            NULL);

   FOR rec IN C_TO_LOCS LOOP

      L_rib_routing_rec.value := rec.physical_to_loc;
      L_rib_routing_rec.dtl1_value := rec.to_loc_type;
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_ALLOC.GET_ROUTING_TO_LOCS',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END GET_ROUTING_TO_LOCS;
------------------------------------------------------------------------------------------

FUNCTION GET_NOT_BEFORE_DAYS( O_error_msg    IN OUT VARCHAR2,
                              O_days            OUT NUMBER)
RETURN BOOLEAN IS

   cursor C_DATES is
      select to_number(c.code_desc)
        from code_detail c
       where c.code_type = 'DEFT'
         and c.code = 'DATE';

BEGIN

   if LP_nbf_days IS NULL then
      open C_DATES;
      fetch C_DATES into LP_nbf_days;
      close C_DATES;
   end if;

   O_days := LP_nbf_days;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_ALLOC.GET_NOT_BEFORE_DAYS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_NOT_BEFORE_DAYS;

------------------------------------------------------------------------------------------

FUNCTION GET_RETAIL( O_error_msg    IN OUT VARCHAR2,
                     I_item         IN     item_loc.item%TYPE,
                     I_loc          IN     item_loc.loc%TYPE,
                     I_loc_type     IN     item_loc.loc_type%TYPE,
                     O_price        IN OUT item_loc.unit_retail%TYPE,
                     O_selling_uom  IN OUT item_loc.selling_uom%TYPE)
RETURN BOOLEAN IS

   --fillers for pkg call
   L_dumb_unit_retail_loc         item_loc.unit_retail%TYPE := NULL;
   L_dumb_uom_loc                 item_loc.selling_uom%TYPE := NULL;
   L_dumb_multi_units_loc         item_loc.multi_units%TYPE := NULL;
   L_dumb_multi_unit_retail_loc   item_loc.unit_retail%TYPE := NULL;
   L_dumb_multi_selling_uom_loc   item_loc.selling_uom%TYPE := NULL;

   PROGRAM_ERROR         EXCEPTION;

BEGIN

   if PRICING_ATTRIB_SQL.GET_RETAIL(O_error_msg,
                                    L_dumb_unit_retail_loc,
                                    L_dumb_uom_loc,
                                    O_price,
                                    O_selling_uom,
                                    L_dumb_multi_units_loc,
                                    L_dumb_multi_unit_retail_loc,
                                    L_dumb_multi_selling_uom_loc,
                                    I_item,
                                    I_loc_type,
                                    I_loc) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_ALLOC.GET_RETAIL',
                                        TO_CHAR(SQLCODE));
      return FALSE;

END GET_RETAIL;

------------------------------------------------------------------------------------------

PROCEDURE CHECK_STATUS(I_status_code IN VARCHAR2)
IS

   -- CHECK_STATUS raises an exception if the status code is set to 'E'rror.
   -- This should be called immediately after calling a procedure
   -- that sets the status code.  Any procedure that calls CHECK_STATUS
   -- must have its own exception handling section.

   PROGRAM_ERROR   EXCEPTION;

BEGIN

   if I_status_code in (API_CODES.UNHANDLED_ERROR) then
      raise PROGRAM_ERROR;
   end if;

END CHECK_STATUS;

------------------------------------------------------------------------------------------

FUNCTION LOCK_THE_BLOCK( O_error_msg        OUT VARCHAR2,
                         O_queue_locked     OUT BOOLEAN,
                         I_alloc_no      IN     alloc_mfqueue.alloc_no%TYPE)
RETURN BOOLEAN IS

   L_table             VARCHAR2(30)  := 'TSF_MFQUEUE';
   L_key1              VARCHAR2(100) := I_alloc_no;
   L_key2              VARCHAR2(100) := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from alloc_mfqueue aq
       where aq.alloc_no = I_alloc_no
        for update nowait;

BEGIN

   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      O_queue_locked := TRUE;
      return TRUE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_ALLOC.LOCK_THE_BLOCK',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_THE_BLOCK;

------------------------------------------------------------------------------------------
FUNCTION ROLLUP_DETAILS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message         IN OUT   NOCOPY "RIB_AllocDesc_REC")
RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'RMSMFM_ALLOC.ROLLUP_DETAILS';

   L_rib_allocdtl_tbl    "RIB_AllocDtl_TBL" := NULL;

   --RWMS expects one AllocDtl for a given physical to-location.
   --Roll up alloc_detail attributes to the physical location level for publishing.
   cursor C_ALLOC_DETAILS is
      select "RIB_AllocDtl_REC"(0,
                                inner.physical_to_loc,
                                inner.to_loc,
                                inner.loc_type,
                                inner.store_type,
                                inner.stockholding_ind,
                                inner.qty_allocated,
                                ad.price,
                                ad.selling_uom,
                                inner.priority,
                                inner.store_ord_mult,
                                inner.in_store_date,
                                inner.rush_flag,
                                ad.AllocDtlTckt_TBL)
        from (with v_alloc_detail_1 as
                   (select physical_to_loc,
                           MIN(to_loc) to_loc,
                           MIN(loc_type) loc_type,
                           MIN(store_type) store_type,
                           MIN(stockholding_ind) stockholding_ind,
                           SUM(qty_allocated) qty_allocated,
                           MIN(priority) priority,
                           MIN(store_ord_mult) store_ord_mult,
                           MIN(in_store_date) in_store_date
                      from TABLE(cast(O_message.AllocDtl_TBL as "RIB_AllocDtl_TBL"))
                     group by physical_to_loc),
                    v_alloc_detail_2 as
                   (--If any alloc_detail for the same physical to-location has a rush_flag of 'Y', mark it with a rush_flag of 'Y'.
                    select physical_to_loc,
                          MIN(to_loc) to_loc,
                          first_value(rush_flag) IGNORE NULLS over (order by rush_flag desc) as rush_flag
                     from TABLE(cast(O_message.AllocDtl_TBL as "RIB_AllocDtl_TBL"))
                    group by physical_to_loc, rush_flag)
              select v1.physical_to_loc,
                     v1.to_loc,
                     v1.loc_type,
                     v1.store_type,
                     v1.stockholding_ind,
                     v1.qty_allocated,
                     v1.priority,
                     v1.store_ord_mult,
                     v1.in_store_date,
                     v2.rush_flag
                from v_alloc_detail_1 v1,
                     v_alloc_detail_2 v2
               where v1.physical_to_loc = v2.physical_to_loc
                 and v1.to_loc = v2.to_loc
              ) inner,
              TABLE(cast(O_message.AllocDtl_TBL as "RIB_AllocDtl_TBL")) ad
        where ad.physical_to_loc = inner.physical_to_loc
          and ad.to_loc = inner.to_loc;

BEGIN

   open C_ALLOC_DETAILS;
   fetch C_ALLOC_DETAILS bulk collect into L_rib_allocdtl_tbl;
   close C_ALLOC_DETAILS;

   O_message.allocdtl_tbl := L_rib_allocdtl_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ROLLUP_DETAILS;
------------------------------------------------------------------------------------------
FUNCTION ROLLUP_DETAILS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message         IN OUT   NOCOPY "RIB_AllocRef_REC")
RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'RMSMFM_ALLOC.ROLLUP_DETAILS';

   L_rib_allocrefdtl_tbl    "RIB_AllocDtlRef_TBL" := NULL;

   cursor C_ALLOC_DEL_DETAILS is
      select "RIB_AllocDtlRef_REC"(0,
                                   adrt.to_loc,
                                   adrt.loc_type,
                                   adrt.store_type,
                                   adrt.stockholding_ind)
        from (select DISTINCT(to_loc) to_loc,
                     loc_type,
                     store_type,
                     stockholding_ind
                from TABLE(cast(O_message.AllocDtlRef_TBL as "RIB_AllocDtlRef_TBL"))) adrt;

BEGIN

   open C_ALLOC_DEL_DETAILS;
   fetch C_ALLOC_DEL_DETAILS bulk collect into L_rib_allocrefdtl_tbl;
   close C_ALLOC_DEL_DETAILS;

   O_message.allocdtlref_tbl := L_rib_allocrefdtl_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ROLLUP_DETAILS;
------------------------------------------------------------------------------------------
FUNCTION SET_ROUTING_TO_INFO(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_routing_info           IN OUT   nocopy RIB_ROUTINGINFO_TBL,
                             I_physical_to_loc        IN       ITEM_LOC.LOC%TYPE,
                             I_physical_to_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'RMSMFM_ALLOC.SET_ROUTING_TO_INFO';
   L_rib_routing_rec   RIB_ROUTINGINFO_REC := NULL;
   L_found             VARCHAR2(1) := 'N';

BEGIN

   L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc',
                                            NULL,
                                            'to_phys_loc_type',
                                            NULL,
                                            NULL,
                                            NULL);

   --check if physical_to_loc is already on the routing info as a 'to_phys_loc'
   --if yes, do NOT add it again to avoid duplicates
   L_found := 'N';

   FOR i in O_routing_info.FIRST .. O_routing_info.LAST LOOP
       if O_routing_info(i).name = 'to_phys_loc' and
          O_routing_info(i).value = I_physical_to_loc and 
          O_routing_info(i).dtl1_name = 'to_phys_loc_type' and 
          O_routing_info(i).dtl1_value = I_physical_to_loc_type then
          L_found := 'Y';
          EXIT;
       end if;
   END LOOP;

   if L_found = 'N' then
      L_rib_routing_rec.name       := 'to_phys_loc';
      L_rib_routing_rec.dtl1_name  := 'to_phys_loc_type';
      L_rib_routing_rec.value := I_physical_to_loc;
      L_rib_routing_rec.dtl1_value := I_physical_to_loc_type;
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SET_ROUTING_TO_INFO;
------------------------------------------------------------------------------------------
END RMSMFM_ALLOC;
/

