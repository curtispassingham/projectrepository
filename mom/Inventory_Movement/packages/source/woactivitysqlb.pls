
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY WO_ACTIVITY_SQL AS

--------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists           IN OUT  BOOLEAN,
                 O_wo_activity_row  IN OUT  WO_ACTIVITY%ROWTYPE,
                 I_activity_id      IN      WO_ACTIVITY.ACTIVITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'WO_ACTIVITY_SQL.GET_ROW';

   cursor C_WO_ACTIVITY is
      select *
        from wo_activity
       where activity_id = I_activity_id;

BEGIN

   if I_activity_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_activity_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_wo_activity_row := NULL;

   open C_WO_ACTIVITY;
   fetch C_WO_ACTIVITY into O_wo_activity_row;
   close C_WO_ACTIVITY;

   O_exists := (O_wo_activity_row.activity_id is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ROW;

--------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_delete_ok      IN OUT  BOOLEAN,
                      I_activity_id    IN      WO_ACTIVITY.ACTIVITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'WO_ACTIVITY_SQL.CHECK_DELETE';
   L_exists   VARCHAR2(1)   := 'N';

   cursor C_CHECK_DELETE is
      select 'Y'
        from tsf_wo_detail
       where activity_id = I_activity_id
         and rownum      = 1
   union all
      select 'Y'
        from wo_tmpl_detail
       where activity_id = I_activity_id
         and rownum      = 1;

BEGIN

   if I_activity_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_activity_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CHECK_DELETE;
   fetch C_CHECK_DELETE into L_exists;
   close C_CHECK_DELETE;

   O_delete_ok := (L_exists != 'Y');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_DELETE;

--------------------------------------------------------------------------------
FUNCTION DELETE_GL_XREF(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_activity_id    IN      WO_ACTIVITY.ACTIVITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61)  := 'WO_ACTIVITY_SQL.DELETE_GL_XREF';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_RECORD_LOCK is
      select 'x'
        from fif_gl_cross_ref
       where tran_code in (51,52)
         and tran_ref_no = I_activity_id
         for update nowait;

BEGIN

   if I_activity_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_activity_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_RECORD_LOCK;
   close C_RECORD_LOCK;

   delete from fif_gl_cross_ref
    where tran_code in (51,52) 
      and tran_ref_no = I_activity_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            'FIF_GL_CROSS_REF',
                                            'ACTIVITY_ID '||I_activity_id,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_GL_XREF;

--------------------------------------------------------------------------------
FUNCTION GET_COST_TYPE(O_error_message  IN OUT   VARCHAR2,
                       O_cost_type      IN OUT   WO_ACTIVITY.COST_TYPE%TYPE,
                       I_activity_id    IN       WO_ACTIVITY.ACTIVITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'WO_ACTIVITY_SQL.GET_COST_TYPE';

   cursor C_GET_COST_TYPE is
      select cost_type
        from wo_activity
       where activity_id = I_activity_id;

BEGIN

   if I_activity_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_activity_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_COST_TYPE;
   fetch C_GET_COST_TYPE into O_cost_type;
   close C_GET_COST_TYPE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_COST_TYPE;

--------------------------------------------------------------------------------
FUNCTION NEXT_SEQ_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     O_activity_id    IN OUT  WO_ACTIVITY.ACTIVITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'WO_ACTIVITY_SQL.NEXT_SEQ_NO';

   cursor C_NEXT_SEQ_NO is
      select wo_activity_id_seq.nextval
        from dual;
        
BEGIN

   open  C_NEXT_SEQ_NO;
   fetch C_NEXT_SEQ_NO into O_activity_id;
   close C_NEXT_SEQ_NO;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END NEXT_SEQ_NO;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_ACTIVITY_CODE(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT  BOOLEAN,
                                O_wo_activity_row  IN OUT  WO_ACTIVITY%ROWTYPE,
                                I_activity_code    IN      WO_ACTIVITY.ACTIVITY_CODE%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'WO_ACTIVITY_SQL.VALIDATE_ACTIVITY_CODE';

   cursor C_VALIDATE_CODE is
      select *
        from wo_activity
       where activity_code = I_activity_code;
        
BEGIN

   if I_activity_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_activity_code',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_wo_activity_row := NULL;

   open  C_VALIDATE_CODE;
   fetch C_VALIDATE_CODE into O_wo_activity_row;
   close C_VALIDATE_CODE;

   O_exists := (O_wo_activity_row.activity_id is NOT NULL);
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_ACTIVITY_CODE;
--------------------------------------------------------------------------------
END WO_ACTIVITY_SQL;
/
