



CREATE OR REPLACE PACKAGE BODY SHIPMENT_VALIDATE_SQL AS

--------------------------------------------------------------------
FUNCTION ORDER_NO_EXISTS (O_error_message IN OUT VARCHAR2,
                          I_order_no      IN SHIPMENT.ORDER_NO%TYPE,
                          O_exist         IN OUT BOOLEAN)
RETURN BOOLEAN IS

   L_function  VARCHAR2(64) := 'SHIPMENT_VALIDATE_SQL.ORDER_NO_EXISTS';
   L_order_no  ORDHEAD.ORDER_NO%TYPE := NULL;

   cursor C_ORD_NO_EXIST is
      select order_no
        from shipment
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN' , 'C_ORD_NO_EXIST', 'SHIPMENT', 'ORDER_NO:'||to_char(I_order_no));
   open C_ORD_NO_EXIST;
   SQL_LIB.SET_MARK('FETCH' , 'C_ORD_NO_EXIST', 'SHIPMENT', 'ORDER_NO:'||to_char(I_order_no));
   fetch C_ORD_NO_EXIST into L_order_no;
   if C_ORD_NO_EXIST%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_ORD_NO_EXIST', 'SHIPMENT', 'ORDER_NO:'||to_char(I_order_no));
   close C_ORD_NO_EXIST;
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,L_function,To_Char(SQLCODE));
      RETURN FALSE;
END ORDER_NO_EXISTS;

--------------------------------------------------------------------
FUNCTION EXIST(O_error_message IN OUT VARCHAR2,
               I_shipment      IN SHIPMENT.SHIPMENT%TYPE,
               O_exist         IN OUT BOOLEAN)
RETURN BOOLEAN IS

   cursor C_SHIP_EXIST is
      select shipment
        from v_shipment
       where shipment = I_shipment;

   L_function  VARCHAR2(64) := 'SHIPMENT_VALIDATE_SQL.EXIST';
   L_shipment  SHIPMENT.SHIPMENT%TYPE := NULL;

BEGIN
   SQL_LIB.SET_MARK('OPEN' , 'C_SHIP_EXIST', 'SHIPMENT', 'SHIPMENT:'||to_char(I_shipment));
   open C_SHIP_EXIST;
   SQL_LIB.SET_MARK('FETCH' , 'C_SHIP_EXIST', 'SHIPMENT', 'SHIPMENT:'||to_char(I_shipment));
   fetch C_SHIP_EXIST into L_shipment;
   if C_SHIP_EXIST%NOTFOUND then
      O_exist := FALSE;
      O_error_message := sql_lib.create_msg('INV_SHIP',null,null,null);
   else
      O_exist := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_SHIP_EXIST', 'SHIPMENT', 'SHIPMENT:'||to_char(I_shipment));
   close C_SHIP_EXIST;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG
              ('PACKAGE_ERROR',SQLERRM,L_function,To_Char(SQLCODE));
      RETURN FALSE;
END EXIST;

--------------------------------------------------------------------
FUNCTION BOL_NO_EXISTS(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_bol_no        IN     SHIPMENT.BOL_NO%TYPE)
RETURN BOOLEAN IS

   L_function  VARCHAR2(64)         := 'SHIPMENT_VALIDATE_SQL.BOL_NO_EXISTS';
   L_bol_no    SHIPMENT.BOL_NO%TYPE := NULL;

   cursor C_BOL_NO_EXIST is
      select bol_no
        from shipment
       where UPPER(bol_no) = I_bol_no;

BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_BOL_NO_EXIST', 'SHIPMENT', 'BOL_NO:'||to_char(I_bol_no));
   open C_BOL_NO_EXIST;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_BOL_NO_EXIST', 'SHIPMENT', 'BOL_NO:'||to_char(I_bol_no));
   fetch C_BOL_NO_EXIST into L_bol_no;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_BOL_NO_EXIST', 'SHIPMENT', 'BOL_NO:'||to_char(I_bol_no));
   close C_BOL_NO_EXIST;
   ---
   if L_bol_no is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,L_function,To_Char(SQLCODE));
      RETURN FALSE;
END BOL_NO_EXISTS;
--------------------------------------------------------------------
FUNCTION ASN_EXISTS(O_error_message   IN OUT   VARCHAR2,
                    O_exists          IN OUT   BOOLEAN,
                    I_asn             IN       SHIPMENT.ASN%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(64)       := 'SHIPMENT_VALIDATE_SQL.ASN_EXISTS';
   L_asn        SHIPMENT.ASN%TYPE  := NULL;

   cursor C_ASN_EXIST is
      select asn
        from shipment
       where UPPER(asn) = UPPER(I_asn);

BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_ASN_EXIST', 'SHIPMENT', 'ASN:'||I_asn);
   open C_ASN_EXIST;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_ASN_EXIST', 'SHIPMENT', 'ASN:'||I_asn);
   fetch C_ASN_EXIST into L_asn;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_ASN_EXIST', 'SHIPMENT', 'ASN:'||I_asn);
   close C_ASN_EXIST;
   ---
   if L_asn is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,L_function,To_Char(SQLCODE));
      RETURN FALSE;
END ASN_EXISTS;
---------------------------------------------------------------------

FUNCTION CHECK_SHIPMENTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT BOOLEAN,
                         I_shipment        IN     SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60)           := 'SHIPMENT_VALIDATE_SQL.CHECK_SHIPMENTS';
   L_shipment  SHIPMENT.SHIPMENT%TYPE := NULL;
   cursor C_CHECK_SHIPMENT is
      select shipment
        from shipment
       where parent_shipment = I_shipment
         and rownum          = 1;

BEGIN

   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_shipment',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SHIPMENT', 'SHIPMENT', 'Shipment:' || to_char(I_shipment));
   open C_CHECK_SHIPMENT;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SHIPMENT', 'SHIPMENT', 'Shipment:' || to_char(I_shipment));
   fetch C_CHECK_SHIPMENT into L_shipment;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SHIPMENT', 'SHIPMENT', 'Shipment:' || to_char(I_shipment));
   close C_CHECK_SHIPMENT;
   if L_shipment is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_SHIPMENTS;
--------------------------------------------------------------------
FUNCTION PARENT_SHIPMENT_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               I_shipment        IN       SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60)  := 'SHIPMENT_VALIDATE_SQL.PARENT_SHIPMENT_EXIST';
   L_exist     VARCHAR2(1);

   cursor C_CHECK_PARENT_SHIPMENT is
      select 'x'
        from shipment
       where shipment = I_shipment
         and parent_shipment is NOT NULL
         and rownum   = 1;

BEGIN

   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_shipment',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_PARENT_SHIPMENT',
                    'SHIPMENT',
                    'Shipment:' || to_char(I_shipment));
   open C_CHECK_PARENT_SHIPMENT;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_PARENT_SHIPMENT',
                    'SHIPMENT',
                    'Shipment:' || to_char(I_shipment));
   fetch C_CHECK_PARENT_SHIPMENT into L_exist;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_PARENT_SHIPMENT',
                    'SHIPMENT',
                    'Shipment:' || to_char(I_shipment));
   close C_CHECK_PARENT_SHIPMENT;

   if L_exist = 'x' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PARENT_SHIPMENT_EXIST;
--------------------------------------------------------------------
END SHIPMENT_VALIDATE_SQL;
/


