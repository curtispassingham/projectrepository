CREATE OR REPLACE PACKAGE CORESVC_FULFILORD_TSF AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   -- PACKAGE VARIABLES
----------------------------------------------------------------------------
TYPE ORDCUST_TBL is TABLE OF ORDCUST%ROWTYPE;
TYPE ORDCUST_DETAIL_TBL is TABLE OF ORDCUST_DETAIL%ROWTYPE;
---------------------------------------------------------------------------------------------------
-- Name   : CREATE_TSF
-- Purpose: This public function contains the core logic to create a customer order transfer related
--          to a customer order fulfillment request.  It returns a collection of ordcust ids created
--          in RMS including ordcust records created in C (order fully created), P (order partially
--          created) or X (order not created) status.
---------------------------------------------------------------------------------------------------
FUNCTION CREATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_ordcust_ids     IN OUT   ID_TBL,
                    I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                    I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE) 
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : CANCEL_TSF
-- Purpose: This is a public function that contains the core logic to cancel a customer order
--          transfer related to a customer order fulfillment cancellation request.
---------------------------------------------------------------------------------------------------
FUNCTION CANCEL_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                    I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END CORESVC_FULFILORD_TSF;
/