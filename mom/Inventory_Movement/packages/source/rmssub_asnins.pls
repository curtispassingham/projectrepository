
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_ASNIN AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
PROCEDURE CONSUME(O_STATUS_CODE   IN OUT VARCHAR2,
                  O_ERROR_MESSAGE IN OUT VARCHAR2,
                  I_MESSAGE       IN     RIB_OBJECT,
                  I_MESSAGE_TYPE  IN     VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_ASNIN;
/
