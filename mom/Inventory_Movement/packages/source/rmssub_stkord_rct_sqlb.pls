
CREATE OR REPLACE PACKAGE BODY RMSSUB_STKORD_RECEIPT_SQL AS

--------------------------------------------------------------------------------
FUNCTION PERSIST_BOL(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_appt                IN      APPT_HEAD.APPT%TYPE,
                     I_doc_type            IN      APPT_DETAIL.DOC_TYPE%TYPE,
                     I_shipment            IN      SHIPMENT.SHIPMENT%TYPE,
                     I_to_loc              IN      SHIPMENT.TO_LOC%TYPE,
                     I_bol_no              IN      SHIPMENT.BOL_NO%TYPE,
                     I_receipt_date        IN      PERIOD.VDATE%TYPE,
                     I_item_table          IN      STOCK_ORDER_RCV_SQL.ITEM_TAB,
                     I_qty_expected_table  IN      STOCK_ORDER_RCV_SQL.QTY_TAB,
                     I_inv_status_table    IN      STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                     I_carton_table        IN      STOCK_ORDER_RCV_SQL.CARTON_TAB,
                     I_distro_no_table     IN      STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                     I_tampered_ind_table  IN      STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB)
return BOOLEAN IS

   L_program        VARCHAR2(61)  := 'RMSSUB_STKORD_RECEIPT_SQL.PERSIST_BOL';
   L_invalid_param  VARCHAR2(30);
   L_receipt_date   DATE          :=  NVL(I_receipt_date, GET_VDATE);
BEGIN

   -- Check for required input
   if I_doc_type is NULL then
      L_invalid_param := 'I_doc_type';
   elsif I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   elsif I_to_loc is NULL then
      L_invalid_param := 'I_to_loc';
   elsif I_bol_no is NULL then
      L_invalid_param := 'I_bol_no';
   end if;
   ---
   if I_receipt_date is NOT NULL then
      L_receipt_date := I_receipt_date;
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            L_invalid_param,
                                            'NULL');
      return FALSE;
   end if;
   -- Doc Type T is for Tranfers.  RDM also uses D and V for transfers
   if I_doc_type in ('T','D','V') then
      if STOCK_ORDER_RCV_SQL.TSF_BOL_CARTON(O_error_message,
                                            I_appt,                            -- appt_no
                                            I_shipment,                        -- shipment
                                            I_to_loc,                          -- to_loc
                                            I_bol_no,                          -- bol_no
                                            NULL,                              -- receipt_no
                                            NULL,                              -- disposition
                                            L_receipt_date,                    -- tran_date
                                            I_item_table,                      -- item_table
                                            I_qty_expected_table,              -- qty_expected_table
                                            NULL,                              -- weight      -- Catch Weight
                                            NULL,                              -- weight_uom  -- Catch Weight
                                            I_inv_status_table,                -- inv_status_table
                                            I_carton_table,                    -- carton_table
                                            I_distro_no_table,                 -- distro_no_table
                                            I_tampered_ind_table,              -- tampered_ind_table
                                            'N',                               -- wrong_store_ind
                                            NULL) = FALSE then                 -- wrong_store
         return FALSE;
      end if;

   elsif I_doc_type = 'A' then
      if STOCK_ORDER_RCV_SQL.ALLOC_BOL_CARTON(O_error_message,
                                              I_appt,                             -- appt_no
                                              I_shipment,                         -- shipment
                                              I_to_loc,                           -- to_loc
                                              I_bol_no,                           -- bol_no
                                              NULL,                               -- receipt_no
                                              NULL,                               -- disposition
                                              L_receipt_date,                     -- tran_date
                                              I_item_table,                       -- item_table
                                              I_qty_expected_table,               -- qty_expected_table
                                              NULL,                               -- weight      -- Catch Weight
                                              NULL,                               -- weight_uom  -- Catch Weight
                                              I_inv_status_table,                 -- inv_status_table
                                              I_carton_table,                     -- carton_table
                                              I_distro_no_table,                  -- distro_no_table
                                              I_tampered_ind_table,               -- tampered_ind_table
                                              'N',                                -- wrong_store_ind
                                              NULL) = FALSE then                  -- wrong_store
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_BOL;

--------------------------------------------------------------------------------
FUNCTION PERSIST_CARTON(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_appt                IN      APPT_HEAD.APPT%TYPE,
                        I_doc_type            IN      APPT_DETAIL.DOC_TYPE%TYPE,
                        I_shipment            IN      SHIPMENT.SHIPMENT%TYPE,
                        I_to_loc              IN      SHIPMENT.TO_LOC%TYPE,
                        I_bol_no              IN      SHIPMENT.BOL_NO%TYPE,
                        I_receipt_no          IN      APPT_DETAIL.RECEIPT_NO%TYPE,
                        I_disposition         IN      INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                        I_receipt_date        IN      SHIPMENT.RECEIVE_DATE%TYPE,
                        I_item_table          IN      STOCK_ORDER_RCV_SQL.ITEM_TAB,
                        I_qty_expected_table  IN      STOCK_ORDER_RCV_SQL.QTY_TAB,
                        I_weight              IN      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE, -- Catch Weight
                        I_weight_uom          IN      UOM_CLASS.UOM%TYPE,                               -- Catch Weight
                        I_inv_status_table    IN      STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                        I_carton_table        IN      STOCK_ORDER_RCV_SQL.CARTON_TAB,
                        I_distro_no_table     IN      STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                        I_tampered_ind_table  IN      STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB,
                        I_wrong_store_ind     IN      VARCHAR2,
                        I_wrong_store         IN      SHIPMENT.TO_LOC%TYPE)
return BOOLEAN IS

   L_program          VARCHAR2(61)          := 'RMSSUB_STKORD_RECEIPT_SQL.PERSIST_CARTON';
   L_invalid_param    VARCHAR2(30);
   L_receipt_date     DATE                  := NVL(I_receipt_date, GET_VDATE);
   L_actual_location  SHIPMENT.TO_LOC%TYPE  := I_to_loc;
   L_uom_class        UOM_CLASS.UOM_CLASS%TYPE;

BEGIN

   -- Check for required input
   if I_doc_type is NULL then
      L_invalid_param := 'I_doc_type';
   elsif I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   elsif I_to_loc is NULL then
      L_invalid_param := 'I_to_loc';
   elsif I_bol_no is NULL then
      L_invalid_param := 'I_bol_no';
   elsif I_wrong_store_ind is NULL then
      L_invalid_param := 'I_wrong_store_ind';
   -- Catch Weight
   elsif I_weight is NULL then
      if I_weight_uom is not NULL then
         L_invalid_param := 'I_weight';
      end if;
   elsif I_weight_uom is NULL then
      L_invalid_param := 'I_weight_uom';
   --- Catch Weight End
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            L_invalid_param,
                                            'NULL');
      return FALSE;
   end if;

   if I_weight is NOT NULL and
      I_weight_uom is NOT NULL then
      ---
      if not UOM_SQL.GET_CLASS(O_error_message,
                               L_uom_class,
                               UPPER(I_weight_uom)) then
         return FALSE;
      end if;
      ---
      if L_uom_class != 'MASS' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_WGTUOM_CLASS', I_weight_uom, L_uom_class, NULL);
         return FALSE;
      end if;
   end if;

   -- Doc Type T is for Tranfers.  RDM also uses D and V for transfers
   if I_doc_type in ('T','D','V') then
      if STOCK_ORDER_RCV_SQL.TSF_BOL_CARTON(O_error_message,
                                            I_appt,                             -- appt_no
                                            I_shipment,                         -- shipment
                                            I_to_loc,                           -- to_loc
                                            I_bol_no,                           -- bol_no
                                            I_receipt_no,                       -- receipt_no
                                            I_disposition,                      -- disposition
                                            L_receipt_date,                     -- tran_date
                                            I_item_table,                       -- item_table
                                            I_qty_expected_table,               -- qty_expected_table
                                            I_weight,                           -- weight -- Catch Weight
                                            UPPER(I_weight_uom),                -- weight_uom -- Catch Weight
                                            I_inv_status_table,                 -- inv_status_table
                                            I_carton_table,                     -- carton_table
                                            I_distro_no_table,                  -- distro_no_table
                                            I_tampered_ind_table,               -- tampered_ind_table
                                            I_wrong_store_ind,                  -- wrong_store_ind
                                            I_wrong_store) = FALSE then         -- wrong_store
         return FALSE;
      end if;
   elsif I_doc_type = 'A' then
      if STOCK_ORDER_RCV_SQL.ALLOC_BOL_CARTON(O_error_message,
                                              I_appt,                             -- appt_no
                                              I_shipment,                         -- shipment
                                              I_to_loc,                           -- to_loc
                                              I_bol_no,                           -- bol_no
                                              I_receipt_no,                       -- receipt_no
                                              I_disposition,                      -- disposition
                                              L_receipt_date,                     -- tran_date
                                              I_item_table,                       -- item_table
                                              I_qty_expected_table,               -- qty_expected_table
                                              I_weight,                           -- weight -- Catch Weight
                                              UPPER(I_weight_uom),                -- weight_uom -- Catch Weight
                                              I_inv_status_table,                 -- inv_status_table
                                              I_carton_table,                     -- carton_table
                                              I_distro_no_table,                  -- distro_no_table
                                              I_tampered_ind_table,               -- tampered_ind_table
                                              I_wrong_store_ind,                  -- wrong_store_ind
                                              I_wrong_store) = FALSE then         -- wrong_store
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_CARTON;

--------------------------------------------------------------------------------
FUNCTION PERSIST_LINE_ITEM(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_location            IN      SHIPMENT.TO_LOC%TYPE,
                           I_bol_no              IN      SHIPMENT.BOL_NO%TYPE,
                           I_distro_no           IN      SHIPSKU.DISTRO_NO%TYPE,
                           I_distro_type         IN      VARCHAR2,
                           I_appt                IN      APPT_HEAD.APPT%TYPE,
                           I_rib_receiptdtl_rec  IN      "RIB_ReceiptDtl_REC")
return BOOLEAN IS

   L_program        VARCHAR2(61)  := 'RMSSUB_STKORD_RECEIPT_SQL.PERSIST_LINE_ITEM';
   L_invalid_param  VARCHAR2(30);
   L_receipt_date   DATE          := NVL(I_rib_receiptdtl_rec.receipt_date, GET_VDATE);
   L_uom_class      UOM_CLASS.UOM_CLASS%TYPE;
   L_distro_type    VARCHAR2(1);

BEGIN

   -- Check for required input
   if I_location is NULL then
      L_invalid_param := 'I_location';
   elsif I_bol_no is NULL then
      L_invalid_param := 'I_bol_no';
   elsif I_distro_type is NULL then
      L_invalid_param := 'I_distro_type';
   elsif I_rib_receiptdtl_rec is NULL then
      L_invalid_param := 'I_rib_receiptdtl_rec';
   -- Catch Weight
   elsif I_rib_receiptdtl_rec.weight is NULL then
      if I_rib_receiptdtl_rec.weight_uom is not NULL then
         L_invalid_param := 'I_rib_receiptcdtl_rec.weight';
      end if;
   elsif I_rib_receiptdtl_rec.weight_uom is NULL then
      L_invalid_param := 'I_rib_receiptdtl_rec.weight_uom';
   end if;
   --- Catch Weight End
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            L_invalid_param,
                                            'NULL');
      return FALSE;
   end if;

   if I_rib_receiptdtl_rec.weight is NOT NULL and
      I_rib_receiptdtl_rec.weight_uom is NOT NULL then
      ---
      if not UOM_SQL.GET_CLASS(O_error_message,
                               L_uom_class,
                               UPPER(I_rib_receiptdtl_rec.weight_uom)) then
         return FALSE;
      end if;
      ---
      if L_uom_class != 'MASS' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_WGTUOM_CLASS',
                                               I_rib_receiptdtl_rec.weight_uom,
                                               L_uom_class,
                                               NULL);
         return FALSE;
      end if;
   end if;

   if I_rib_receiptdtl_rec.unit_qty = 0 then
      return TRUE;
   end if;

   if I_distro_type in ('T','D','V') then
      L_distro_type := 'T';
   else
      L_distro_type := I_distro_type;
   end if;

   if L_distro_type = 'T' then
      if STOCK_ORDER_RCV_SQL.TSF_LINE_ITEM(O_error_message,
                                           I_location,
                                           I_rib_receiptdtl_rec.item_id,
                                           I_rib_receiptdtl_rec.unit_qty,
                                           I_rib_receiptdtl_rec.weight,       -- Catch Weight
                                           UPPER(I_rib_receiptdtl_rec.weight_uom),   -- Catch Weight
                                           NVL(I_rib_receiptdtl_rec.receipt_xactn_type,'R'),
                                           L_receipt_date,
                                           I_rib_receiptdtl_rec.receipt_nbr,
                                           I_bol_no,
                                           I_appt,
                                           I_rib_receiptdtl_rec.container_id,
                                           L_distro_type,
                                           I_distro_no,
                                           NVL(I_rib_receiptdtl_rec.to_disposition,I_rib_receiptdtl_rec.from_disposition),
                                           I_rib_receiptdtl_rec.tampered_carton_ind,
                                           I_rib_receiptdtl_rec.dummy_carton_ind) = FALSE then
         return FALSE;
      end if;
   elsif L_distro_type = 'A' then
      if STOCK_ORDER_RCV_SQL.ALLOC_LINE_ITEM(O_error_message,
                                             I_location,
                                             I_rib_receiptdtl_rec.item_id,
                                             I_rib_receiptdtl_rec.unit_qty,
                                             I_rib_receiptdtl_rec.weight,      -- Catch Weight
                                             UPPER(I_rib_receiptdtl_rec.weight_uom),  -- Catch Weight
                                             NVL(I_rib_receiptdtl_rec.receipt_xactn_type,'R'),
                                             L_receipt_date,
                                             I_rib_receiptdtl_rec.receipt_nbr,
                                             I_bol_no,
                                             I_appt,
                                             I_rib_receiptdtl_rec.container_id,
                                             L_distro_type,
                                             I_distro_no,
                                             NVL(I_rib_receiptdtl_rec.to_disposition,I_rib_receiptdtl_rec.from_disposition),
                                             I_rib_receiptdtl_rec.tampered_carton_ind,
                                             I_rib_receiptdtl_rec.dummy_carton_ind) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_LINE_ITEM;

--------------------------------------------------------------------------------
FUNCTION PERSIST_INSERT_DUP_RECEIPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_bol_no          IN       RECEIVING_LOG.BOL_NO%TYPE,
                                    I_carton          IN       RECEIVING_LOG.CARTON%TYPE,
                                    I_carton_status   IN       RECEIVING_LOG.CARTON_STATUS%TYPE,
                                    I_to_loc          IN       RECEIVING_LOG.TO_LOC%TYPE,
                                    I_tran_type       IN       RECEIVING_LOG.TRAN_TYPE%TYPE,
                                    I_tran_date       IN       RECEIVING_LOG.TRAN_DATE%TYPE,
                                    I_receipt_no      IN       RECEIVING_LOG.RECEIPT_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(61) := 'RMSSUB_STKORD_RECEIPT_SQL.PERSIST_INSERT_DUP_RECEIPT';

BEGIN

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'RECEIVING_LOG',
                    NULL);

   insert into receiving_log (bol_no,
                              carton,
                              carton_status,
                              to_loc,
                              tran_type,
                              tran_date,
                              receipt_no,
                              create_datetime)
                      values (I_bol_no,
                              I_carton,
                              I_carton_status,
                              I_to_loc,
                              I_tran_type,
                              I_tran_date,
                              I_receipt_no,
                              SYSDATE);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_INSERT_DUP_RECEIPT;
--------------------------------------------------------------------------------
END RMSSUB_STKORD_RECEIPT_SQL;
/
