CREATE OR REPLACE PACKAGE MRT_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------
-- Filename: mrts/b.pls
-- Purpose : This is used to retrieve, update, add, delete data from/into
--         : MRT and MRT_ITEM tables
-- Author  : Bong Gaspar (Accenture - ERC)
----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
-- Function Name: DELETE_MRT_ITEM
-- Purpose      : Delete rows from MRT_ITEM table after calling the function
--              : MRT_SQL.DEL_MRT_ITEM_LOC to delete rows from MRT_ITEM_LOC table.
--              : This function is being called by mrt.fmb.
----------------------------------------------------------------------------------
FUNCTION DELETE_MRT_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_mrt_no          IN       MRT_ITEM.MRT_NO%TYPE,
                          I_item            IN       MRT_ITEM.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: DEL_MRT_ITEM_LOC
-- Purpose      : Delete rows from MRT_ITEM_LOC table with delete_type and
--              : filter parameters values. This function is being called by
--              : MRT_SQL.DELETE_MRT_ITEM function.
----------------------------------------------------------------------------------
FUNCTION DEL_MRT_ITEM_LOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_delete_type     IN       VARCHAR2,
                           I_mrt_no          IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                           I_loc_type        IN       MRT_ITEM_LOC.LOC_TYPE%TYPE,
                           I_location        IN       MRT_ITEM_LOC.LOCATION%TYPE,
                           I_item            IN       MRT_ITEM_LOC.ITEM%TYPE,
                           I_item_parent     IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                           I_diff1           IN       ITEM_MASTER.DIFF_1%TYPE,
                           I_diff2           IN       ITEM_MASTER.DIFF_2%TYPE,
                           I_diff3           IN       ITEM_MASTER.DIFF_3%TYPE,
                           I_diff4           IN       ITEM_MASTER.DIFF_4%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: UUPDATE_TSF_COST_PRICE_QTY
-- Purpose      : This will update the column TSF_COST or
--              : TSF_PRICE or TSF_QTY on the table MRT_ITEM_LOC
--              : for a selected Items/Locations combinations
----------------------------------------------------------------------------------
FUNCTION UPDATE_TSF_COST_PRICE_QTY      (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_apply_type            IN       VARCHAR2,
                                         I_item_loc_ind          IN       VARCHAR2,
                                         I_group_loc_type        IN       MRT_ITEM_LOC.LOC_TYPE%TYPE,
                                         I_group_loc_value       IN       MRT_ITEM_LOC.LOCATION%TYPE,
                                         I_mrt_no                IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                                         I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                         I_item_parent           IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                                         I_diff1                 IN       ITEM_MASTER.DIFF_1%TYPE,
                                         I_diff2                 IN       ITEM_MASTER.DIFF_2%TYPE,
                                         I_diff3                 IN       ITEM_MASTER.DIFF_3%TYPE,
                                         I_diff4                 IN       ITEM_MASTER.DIFF_4%TYPE,
                                         I_from_loc_unit_cost    IN       MRT_ITEM_LOC.TSF_COST%TYPE,
                                         I_from_loc_unit_price   IN       MRT_ITEM_LOC.TSF_PRICE%TYPE,
                                         I_from_loc_tsf_qty      IN       MRT_ITEM_LOC.TSF_QTY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CREATE_MRT_ITEM
-- Purpose      : Creates rows in the mrt_item table for exploded item lists and
--              : Parent/Grandparent items. This function is being called by mrt.fmb.
----------------------------------------------------------------------------------
FUNCTION CREATE_MRT_ITEM (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_mrt_no             IN       MRT_ITEM.MRT_NO%TYPE,
                          I_item_type          IN       VARCHAR2,
                          I_value              IN       ITEM_MASTER.ITEM%TYPE,
                          I_unit_cost          IN       MRT_ITEM_LOC.UNIT_COST%TYPE,
                          I_restocking_pct     IN       MRT_ITEM.RESTOCK_PCT%TYPE,
                          I_selected_ind       IN       MRT_ITEM.SELECTED_IND%TYPE,
                          I_supplier           IN       ITEM_SUPPLIER.SUPPLIER%TYPE DEFAULT NULL) ---new code
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CREATE_MRT_ITEM_LOC
-- Purpose      : Creates rows in the mrt_item_loctable for exploded item lists and
--              : Parent/Grandparent items. This function is being called by mrt.fmb.
----------------------------------------------------------------------------------
FUNCTION CREATE_MRT_ITEM_LOC (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_stockholding_exists   IN OUT   BOOLEAN,
                              O_soh_exists            IN OUT   BOOLEAN,
                              O_location_exists       IN OUT  BOOLEAN,
                              I_item_apply_type       IN       VARCHAR2,
                              I_group_loc_type        IN       MRT_ITEM_LOC.LOC_TYPE%TYPE,
                              I_group_loc_value       IN       VARCHAR2,
                              I_mrt_no                IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                              I_item                  IN       MRT_ITEM_LOC.ITEM%TYPE,
                              I_item_parent           IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                              I_diff_1                IN       ITEM_MASTER.DIFF_1%TYPE,
                              I_diff_2                IN       ITEM_MASTER.DIFF_2%TYPE,
                              I_diff_3                IN       ITEM_MASTER.DIFF_3%TYPE,
                              I_diff_4                IN       ITEM_MASTER.DIFF_4%TYPE,
                              I_inv_status            IN       VARCHAR2,
                              I_tsf_qty               IN       MRT_ITEM_LOC.TSF_QTY%TYPE,
                              I_tsf_cost              IN       MRT_ITEM_LOC.TSF_COST%TYPE,
                              I_tsf_price             IN       MRT_ITEM_LOC.TSF_PRICE%TYPE,
                              I_inter_intra_ind       IN       VARCHAR2,
                              I_tsf_entity_id         IN       V_TRANSFER_FROM_LOC.TSF_ENTITY_ID%TYPE,
                              I_set_of_books_id       IN       TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: UPDATE_TRANSFER_QTY
-- Purpose      : This will update the UPDATE_TRANSFER_QTY on the table MRT_ITEM_LOC
--                for selected Location/Item combinations and will be called by module
--                tsf.fmb when the transfer is approved.
-----------------------------------------------------------------------------------
FUNCTION UPDATE_TRANSFER_QTY (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_mrt_no          IN       MRT.MRT_NO%TYPE,
                              I_location        IN       MRT_ITEM_LOC.LOCATION%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: NEXT_MRT_NUMBER
-- Purpose : This function will generate the next unique ID sequence used for the MRT table.
-- This function will use the MRT_NUMBER_SEQ
--------------------------------------------------------------------------------------------
FUNCTION NEXT_MRT_NUMBER (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_mrt_no          IN OUT  MRT.MRT_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: LOCK_MRT_ITEM
-- Purpose : This function will lock the mrt_item table rows for the given mrt_no and item.
--------------------------------------------------------------------------------------------
FUNCTION LOCK_MRT_ITEM ( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_mrt_no        IN     MRT_ITEM.MRT_NO%TYPE,
                         I_item          IN     MRT_ITEM.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: UPDATE_MRT_ITEM
-- Purpose : This function will update the mrt_item table column of restock_pct, selected_ind
--  and rtv_cost for the given mrt_no and item combination.
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_MRT_ITEM ( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_mrt_no        IN     MRT_ITEM.MRT_NO%TYPE,
                           I_item          IN     MRT_ITEM.ITEM%TYPE,
                           I_restock_pct   IN     MRT_ITEM.RESTOCK_PCT%TYPE,
                           I_selected_ind  IN     MRT_ITEM.SELECTED_IND%TYPE,
                           I_rtv_cost      IN     MRT_ITEM.RTV_COST%TYPE DEFAULT NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function: UPDATE_RESTOCK_PCT
-- Purpose : This function will update the mrt_item table column of restock_pct to
-- the restock pct inputted.
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_RESTOCK_PCT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_mrt_no        IN     MRT_ITEM.MRT_NO%TYPE,
                            I_restock_pct   IN     MRT_ITEM.RESTOCK_PCT%TYPE,
                            I_diff          IN     ITEM_MASTER.DIFF_1%TYPE DEFAULT NULL,
                            I_vpn           IN     ITEM_SUPPLIER.VPN%TYPE DEFAULT NULL,
                            I_item_parent   IN     ITEM_MASTER.ITEM_PARENT%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function: UPDATE_MRT_QTYS
-- Purpose : This function will update the mrt quantity.
--           It is called by MRT.FMB
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_MRT_QTYS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_mrt_no          IN     MRT.MRT_NO%TYPE,
                         I_quantity_type   IN     MRT.QUANTITY_TYPE%TYPE,
                         I_inventory_type  IN     MRT.INVENTORY_TYPE%TYPE,
                         I_item            IN     MRT_ITEM.ITEM%TYPE DEFAULT NULL,
                         I_location        IN     MRT_ITEM_LOC.LOCATION%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function: LOCK_MRT_ITEM_LOC
-- Purpose : This function will lock the mrt_item_loc table rows for the given mrt_no, item.
--            and location
--------------------------------------------------------------------------------------------
FUNCTION LOCK_MRT_ITEM_LOC (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_mrt_no        IN     MRT_ITEM_LOC.MRT_NO%TYPE,
                            I_item          IN     MRT_ITEM_LOC.ITEM%TYPE,
                            I_location      IN     MRT_ITEM_LOC.lOCATION%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function: UPDATE_MRT_ITEM_LOC
-- Purpose : This function will update the mrt_item table column of tsf_qty,tsf_cost or tsf_price
--           for the given mrt_no, item and location combination.
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_MRT_ITEM_LOC (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_mrt_no        IN     MRT_ITEM_LOC.MRT_NO%TYPE,
                              I_item          IN     MRT_ITEM_LOC.ITEM%TYPE,
                              I_location      IN     MRT_ITEM_LOC.LOCATION%TYPE,
                              I_tsf_qty       IN     MRT_ITEM_LOC.TSF_QTY%TYPE,
                              I_tsf_cost      IN     MRT_ITEM_LOC.TSF_QTY%TYPE,
                              I_tsf_price     IN     MRT_ITEM_LOC.TSF_QTY%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function: UPDATE_MRT_STATUS
-- Purpose : This function will update the mrt_status
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_MRT_ITEM_LOC (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_mrt_no        IN     MRT.MRT_NO%TYPE,
                              I_status        IN     MRT.MRT_STATUS%TYPE )
RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function: VALIDATE_ITEM_LIST
-- Purpose : This function will check the items on an item list. It will set
--           O_tracking_level ind to true if the list contains non-tracking level items.
--           O_inventory_ind will be set to true if the list contans non-inventory items.
--           O_unapproved_ind will be set to true id the list contains unapproved items.
--------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_LIST (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tracking_level_ind IN OUT BOOLEAN,
                             O_inventory_ind      IN OUT BOOLEAN,
                             O_unapproved_ind     IN OUT BOOLEAN,
                             I_item_list          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function: UPDATE_RECEIVED_QTY
-- Purpose : This function will update the received_qty in mrt_item_loc based on the
--           received qty on the transfer
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_RECEIVED_QTY (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_mrt_no        IN     MRT_ITEM_LOC.MRT_NO%TYPE,
                              I_tsf_no        IN     TSFDETAIL.TSF_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: LOCK_MRT
-- Purpose : This function will lock the mrt table rows for the given mrt_no.
--------------------------------------------------------------------------------------------
FUNCTION LOCK_MRT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_mrt_no          IN       MRT.MRT_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: INSERT_RTV_INFO
-- Purpose: This function will insert the initial RTV info for the MRT-linked RTV into the
--          rtv_head and rtv_detail tables.
--------------------------------------------------------------------------------------------
FUNCTION INSERT_RTV_INFO(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rtv_order_no         IN OUT   RTV_HEAD.RTV_ORDER_NO%TYPE,
                         I_mrt_no               IN       MRT.MRT_NO%TYPE,
                         I_rtv_create_status    IN       MRT.CREATE_RTV_STATUS%TYPE,
                         I_supplier             IN       SUPS.SUPPLIER%TYPE,
                         I_ret_auth_num         IN       RTV_HEAD.RET_AUTH_NUM%TYPE,
                         I_rtv_wh               IN       RTV_HEAD.WH%TYPE,
                         I_comment_desc         IN       RTV_HEAD.COMMENT_DESC%TYPE,
                         I_rtv_not_after_date   IN       RTV_HEAD.NOT_AFTER_DATE%TYPE,
                         I_rtv_restock_pct      IN       RTV_HEAD.RESTOCK_PCT%TYPE,
                         I_rtv_reason           IN       MRT.RTV_REASON%TYPE,
                         I_include_wh_inv_ind   IN       MRT.INCLUDE_WH_INV%TYPE,
                         I_inventory_type       IN       MRT.INVENTORY_TYPE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function : VALIDATE_IP_IG_ITEMS
-- Purpose  : This function will validate the existence of unapproved child items in the
--            given Parent and Grandparent Items.
--------------------------------------------------------------------------------------------
FUNCTION VALIDATE_IP_IG_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_approved_ind    IN OUT   BOOLEAN,
                              I_item_type       IN       CODE_DETAIL.CODE%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: MRT_ITEM_EXISTS
-- Purpose : This function will check if an MRT/item relationship exists for a given item.
--------------------------------------------------------------------------------------------
FUNCTION MRT_ITEM_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_mrt_no          IN       MRT.MRT_NO%TYPE,
                         I_item            IN       MRT_ITEM.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: INSERT_MRT_INFO
-- Purpose : This function will insert the new MRT info.
--------------------------------------------------------------------------------------------
FUNCTION INSERT_MRT_INFO(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_mrt_itl_track           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_mrt_itl_inv             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_mrt_itl_app             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_mrt_no                  IN       MRT.MRT_NO%TYPE,
                         I_item                    IN       MRT_ITEM.ITEM%TYPE,
                         I_item_parent             IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                         I_item_list               IN       SKULIST_HEAD.SKULIST%TYPE,
                         I_item_type               IN       VARCHAR2,
                         I_restock_pct             IN       MRT.RESTOCK_PCT%TYPE,
                         I_rtv_cost                IN       MRT_ITEM.RTV_COST%TYPE,
                         I_selected_ind            IN       MRT_ITEM.SELECTED_IND%TYPE,
                         I_item_list_rebuild_ind   IN       VARCHAR2,
                         I_supplier                IN       MRT.SUPPLIER%TYPE,
                         I_location                IN       MRT_ITEM_LOC.LOCATION%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: CUSTOM_VAL
-- Purpose : This function will be used for validationg the custom function.
--------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_mrt_no            IN OUT   MRT.MRT_NO%TYPE,
                    I_function_key      IN OUT   VARCHAR2,
                    I_seq_no            IN OUT   NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END MRT_SQL;
/
