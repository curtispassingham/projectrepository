
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK


CREATE OR REPLACE PACKAGE RMSSUB_STKORD_RECEIPT_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PERSIST_BOL
-- Receive every line item in the BOL
--------------------------------------------------------------------------------
FUNCTION PERSIST_BOL(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_appt                IN      APPT_HEAD.APPT%TYPE,
                     I_doc_type            IN      APPT_DETAIL.DOC_TYPE%TYPE,
                     I_shipment            IN      SHIPMENT.SHIPMENT%TYPE,
                     I_to_loc              IN      SHIPMENT.TO_LOC%TYPE,
                     I_bol_no              IN      SHIPMENT.BOL_NO%TYPE,
                     I_receipt_date        IN      PERIOD.VDATE%TYPE,
                     I_item_table          IN      STOCK_ORDER_RCV_SQL.ITEM_TAB,
                     I_qty_expected_table  IN      STOCK_ORDER_RCV_SQL.QTY_TAB,
                     I_inv_status_table    IN      STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                     I_carton_table        IN      STOCK_ORDER_RCV_SQL.CARTON_TAB,
                     I_distro_no_table     IN      STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                     I_tampered_ind_table  IN      STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB)
return BOOLEAN;

--------------------------------------------------------------------------------
-- PERSIST_CARTON
-- Receive every line item in the carton
--------------------------------------------------------------------------------
FUNCTION PERSIST_CARTON(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_appt                IN      APPT_HEAD.APPT%TYPE,
                        I_doc_type            IN      APPT_DETAIL.DOC_TYPE%TYPE,
                        I_shipment            IN      SHIPMENT.SHIPMENT%TYPE,
                        I_to_loc              IN      SHIPMENT.TO_LOC%TYPE,
                        I_bol_no              IN      SHIPMENT.BOL_NO%TYPE,
                        I_receipt_no          IN      APPT_DETAIL.RECEIPT_NO%TYPE,
                        I_disposition         IN      INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                        I_receipt_date        IN      SHIPMENT.RECEIVE_DATE%TYPE,
                        I_item_table          IN      STOCK_ORDER_RCV_SQL.ITEM_TAB,
                        I_qty_expected_table  IN      STOCK_ORDER_RCV_SQL.QTY_TAB,
                        I_weight              IN      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE, -- Catch Weight
                        I_weight_uom          IN      UOM_CLASS.UOM%TYPE,				-- Catch Weight							
                        I_inv_status_table    IN      STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                        I_carton_table        IN      STOCK_ORDER_RCV_SQL.CARTON_TAB,
                        I_distro_no_table     IN      STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                        I_tampered_ind_table  IN      STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB,
                        I_wrong_store_ind     IN      VARCHAR2,
                        I_wrong_store         IN      SHIPMENT.TO_LOC%TYPE)						
return BOOLEAN;

--------------------------------------------------------------------------------
-- PERSIST_LINE_ITEM
-- Execute existing receiving functionality for each line item.
--------------------------------------------------------------------------------
FUNCTION PERSIST_LINE_ITEM(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_location            IN      SHIPMENT.TO_LOC%TYPE,
                           I_bol_no              IN      SHIPMENT.BOL_NO%TYPE,
                           I_distro_no           IN      SHIPSKU.DISTRO_NO%TYPE,
                           I_distro_type         IN      VARCHAR2,
                           I_appt                IN      APPT_HEAD.APPT%TYPE,
                           I_rib_receiptdtl_rec  IN      "RIB_ReceiptDtl_REC")
return BOOLEAN;

--------------------------------------------------------------------------------
-- PERSIST_INSERT_DUP_RECEIPT
-- Inserts duplicate bol/carton records in the RECEIVING_LOG table.
--------------------------------------------------------------------------------
FUNCTION PERSIST_INSERT_DUP_RECEIPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_bol_no          IN       RECEIVING_LOG.BOL_NO%TYPE,
                                    I_carton          IN       RECEIVING_LOG.CARTON%TYPE,
                                    I_carton_status   IN       RECEIVING_LOG.CARTON_STATUS%TYPE,
                                    I_to_loc          IN       RECEIVING_LOG.TO_LOC%TYPE,
                                    I_tran_type       IN       RECEIVING_LOG.TRAN_TYPE%TYPE,
                                    I_tran_date       IN       RECEIVING_LOG.TRAN_DATE%TYPE,
                                    I_receipt_no      IN       RECEIVING_LOG.RECEIPT_NO%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------                           
END RMSSUB_STKORD_RECEIPT_SQL;
/
