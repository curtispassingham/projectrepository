CREATE OR REPLACE PACKAGE BODY CUST_ORDER_TSF_SQL AS
-------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXT_CO_TSF(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_ext_co_tsf     IN OUT VARCHAR2,
                          I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64)   := 'CUST_ORDER_TSF_SQL.CHECK_EXT_CO_TSF';
   L_check_ind          VARCHAR2(1)    := NULL;

   cursor C_CHECK_CO_TSF is
      select 'x'
        from tsfhead t,
             store s
       where tsf_no = I_tsf_no
         and ext_ref_no is NOT NULL
         and tsf_type in ('CO')
         and ((t.from_loc = s.store and 
               t.from_loc_type = 'S'and
               s.store_type = 'C' and   --virtual store: store_type of 'C'ompany, stockholding_ind of 'N'
               s.stockholding_ind = 'N') or
              (t.to_loc = s.store and
               t.to_loc_type = 'S' and
               s.store_type = 'C' and   --virtual store: store_type of 'C'ompany, stockholding_ind of 'N'              
               s.stockholding_ind = 'N'))
         and rownum = 1;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_CO_TSF;
   fetch C_CHECK_CO_TSF into L_check_ind;
   close C_CHECK_CO_TSF;
   ---
   if L_check_ind is NOT NULL then
      O_ext_co_tsf := 'Y';
   else
      O_ext_co_tsf := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_EXT_CO_TSF;
-------------------------------------------------------------------------------------------------------------
END CUST_ORDER_TSF_SQL;
/
