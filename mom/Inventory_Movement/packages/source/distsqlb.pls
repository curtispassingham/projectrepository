CREATE OR REPLACE PACKAGE BODY DISTRIBUTION_SQL AS

--------------------------------------------------------------------------------
--                           GLOBAL VARIABLES                                 --
--------------------------------------------------------------------------------

TYPE INTERNAL_DIST_REC_TYPE IS RECORD
(
wh                              NUMBER,
protected_ind                   VARCHAR2(1),
restricted_ind                  VARCHAR2(1),
bucket_1                        NUMBER,
bucket_2                        NUMBER,
from_loc                        NUMBER,
to_loc                          NUMBER,
status                          VARCHAR2(1),
--
dist_qty                        NUMBER
);

TYPE INTERNAL_DIST_TABLE_TYPE IS TABLE OF INTERNAL_DIST_REC_TYPE
INDEX BY BINARY_INTEGER;

LP_dist_tab                     INTERNAL_DIST_TABLE_TYPE;
LP_cycle_count                  STAKE_SKU_LOC.CYCLE_COUNT%TYPE;
LP_protected_tab_index_1        INTEGER;
LP_protected_tab_index_2        INTEGER;
LP_unprotected_tab_index_1      INTEGER;
LP_unprotected_tab_index_2      INTEGER;

LP_item                         ITEM_LOC.ITEM%TYPE;
LP_physical_wh                  ITEM_LOC.LOC%TYPE;
LP_qty_to_distribute            NUMBER;
LP_negative_distribution_qty    BOOLEAN;
LP_integer_rounding             BOOLEAN;
LP_dist_rule                    SYSTEM_OPTIONS.DISTRIBUTION_RULE%TYPE;
LP_CMI                          VARCHAR2(64);
LP_sign                         INTEGER;

LP_from_array                   DISTRIBUTION_SQL.inv_flow_array;
LP_to_array                     DISTRIBUTION_SQL.inv_flow_array;
--------------------------------------------------------------------------------
--                      PRIVATE FUNCTION PROTOTYPES                           --
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_RTV(O_error_message IN OUT VARCHAR2,
                        I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE,
                        I_rtv_order_no  IN     RTV_HEAD.RTV_ORDER_NO%TYPE,
                        I_rtv_seq_no    IN     RTV_DETAIL.SEQ_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_INVADJ(O_error_message IN OUT VARCHAR2,
                        I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE)
RETURN BOOLEAN;
FUNCTION DISTRIBUTE_INVADJ_OUT(O_error_message IN OUT VARCHAR2,
                        I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE)
RETURN BOOLEAN;
FUNCTION DISTRIBUTE_INVADJ_IN(O_error_message IN OUT VARCHAR2,
                        I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_TRANSFER(O_error_message        IN OUT   VARCHAR2,
                             I_inv_status           IN       INV_STATUS_TYPES.INV_STATUS%TYPE,
                             I_to_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                             I_to_loc               IN       ITEM_LOC.LOC%TYPE,
                             I_shipment             IN       NUMBER,
                             I_seq_no               IN       NUMBER,
                             I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE,
                             I_tsf_seq_no           IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                             I_tsf_create_ind       IN       VARCHAR2,
                             I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_TRANSFER_OUT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_tsf_no        IN     TSFITEM_INV_FLOW.TSF_NO%TYPE,
                                 I_seq_no        IN     TSFITEM_INV_FLOW.TSF_SEQ_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_TRANSFER_IN(O_error_message IN OUT VARCHAR2,
                                I_shipment      IN     NUMBER,
                                I_seq_no        IN     NUMBER)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_TRANSFER_IN_ADJ(O_error_message IN OUT VARCHAR2,
                                    I_shipment      IN     NUMBER,
                                    I_seq_no        IN     NUMBER)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_STKREC(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_ORDRCV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_ORDRCV_IN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_ORDRCV_ADJ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_shipment        IN       NUMBER,
                             I_seq_no          IN       NUMBER)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_SHORT(O_error_message IN OUT VARCHAR2,
                          I_bucket_type   IN     VARCHAR2)
RETURN BOOLEAN;
--
FUNCTION DRAW_UP_SHORT(O_error_message IN OUT VARCHAR2,
                       I_bucket_type   IN     VARCHAR2)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_OVER(O_error_message  IN OUT VARCHAR2,
                         I_bucket_type    IN     VARCHAR2,
                         I_bucket_1_total IN     NUMBER)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_ALLOCATED_PO(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_PO_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION DISTRIBUTE_RTV_SHIP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE,
                             I_rtv_order_no  IN     RTV_HEAD.RTV_ORDER_NO%TYPE,
                             I_rtv_seq_no    IN     RTV_DETAIL.SEQ_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION OUTBOUND_DIST_RULE_1(O_error_message IN OUT VARCHAR2,
                              I_inv_status    IN     INV_ADJ.INV_STATUS%TYPE)
RETURN BOOLEAN;
--
FUNCTION OUTBOUND_DIST_RULE_2(O_error_message IN OUT VARCHAR2,
                              I_inv_status    IN     INV_ADJ.INV_STATUS%TYPE)
RETURN BOOLEAN;
--
FUNCTION OUTBOUND_DIST_RULE_3(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
--
FUNCTION POPULATE_BUCKET_1(O_error_message IN OUT VARCHAR2,
                           O_bucket_total  IN OUT NUMBER,
                           I_inv_status    IN     INV_ADJ.INV_STATUS%TYPE,
                           I_index_1       IN     INTEGER,
                           I_index_2       IN     INTEGER)
RETURN BOOLEAN;
--
FUNCTION PRORATE_BUCKET_1(O_error_message IN OUT VARCHAR2,
                          I_bucket_total  IN     NUMBER,
                          I_index_1       IN     INTEGER,
                          I_index_2       IN     INTEGER)
RETURN BOOLEAN;
--
FUNCTION DRAW_DOWN_BUCKET_1(O_error_message IN OUT VARCHAR2,
                            I_bucket_total  IN     NUMBER,
                            I_index_1       IN     INTEGER,
                            I_index_2       IN     INTEGER)
RETURN BOOLEAN;
--
FUNCTION POPULATE_BUCKET_2(O_error_message IN OUT VARCHAR2,
                           O_bucket_total  IN OUT NUMBER,
                           I_index_1       IN     INTEGER,
                           I_index_2       IN     INTEGER)
RETURN BOOLEAN;
--
FUNCTION PRORATE_BUCKET_2(O_error_message IN OUT VARCHAR2,
                          I_bucket_total  IN     NUMBER,
                          I_index_1       IN     INTEGER,
                          I_index_2       IN     INTEGER)
RETURN BOOLEAN;
--
FUNCTION DRAW_DOWN_BUCKET_2(O_error_message IN OUT VARCHAR2,
                            I_bucket_total  IN     NUMBER,
                            I_index_1       IN     INTEGER,
                            I_index_2       IN     INTEGER)
RETURN BOOLEAN;
--
FUNCTION DRAW_DOWN_WH(O_error_message IN OUT VARCHAR2,
                      I_wh            IN     NUMBER)
RETURN BOOLEAN;
--
FUNCTION INBOUND_DIST_RULE_1(O_error_message IN OUT VARCHAR2,
                             I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE,
                             I_order_no      IN     ORDHEAD.ORDER_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--
FUNCTION GET_INBOUND_VWH(O_error_message IN OUT VARCHAR2,
                         O_dist_tab      IN OUT INTERNAL_DIST_TABLE_TYPE,
                         I_index_1       IN     INTEGER,
                         I_index_2       IN     INTEGER)
RETURN BOOLEAN;
--
FUNCTION CREATE_ITEM_LOC_REL(O_error_message  IN OUT VARCHAR2,
                             O_wh             IN OUT WH.WH%TYPE,
                             I_il_create_rule IN     VARCHAR2,
                             I_order_no       IN     ORDHEAD.ORDER_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--
FUNCTION SET_DIST_TAB_INDEXES(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
--
FUNCTION GET_DIST_TAB_INDEXES(O_error_message IN OUT VARCHAR2,
                              O_index_1       IN OUT INTEGER,
                              O_index_2       IN OUT INTEGER,
                              I_bucket_type   IN     VARCHAR2)
RETURN BOOLEAN;
--
FUNCTION SET_ITEM_ATTRIBUTES(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
--
FUNCTION VALIDATE_INPUTS(O_error_message IN OUT VARCHAR2,
                         I_item          IN     ITEM_LOC.ITEM%TYPE,
                         I_loc           IN     ITEM_LOC.LOC%TYPE,
                         I_qty           IN     NUMBER,
                         I_CMI           IN     VARCHAR2,
                         I_shipment      IN     SHIPITEM_INV_FLOW.SHIPMENT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- This function will load the mapped virtual warehouses
--- in the location list.
-------------------------------------------------------------------------------
FUNCTION LOAD_INV_FLOW_LOC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_inv_flow_array       IN OUT   DISTRIBUTION_SQL.INV_FLOW_ARRAY,
                           I_to_from_ind          IN       VARCHAR2,
                           I_phy_loc              IN       ITEM_LOC.LOC%TYPE,
                           I_loc_type             IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_other_loc            IN       ITEM_LOC.LOC%TYPE,
                           I_other_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                           I_tsf_qty              IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_inv_status           IN       SHIPSKU.INV_STATUS%TYPE,
                           I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- This function map the from location to the to location based on the
--- business rules.
-------------------------------------------------------------------------------
FUNCTION FIND_MAP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_keepgoing       IN OUT   BOOLEAN,
                  O_to_loc          IN OUT   ITEM_LOC.LOC%TYPE,
                  I_from_loc        IN       ITEM_LOC.LOC%TYPE,
                  I_from_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                  I_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                  I_status          IN       ITEM_LOC.STATUS%TYPE,
                  I_channel_id      IN       CHANNELS.CHANNEL_ID%TYPE,
                  I_channel_type    IN       CHANNELS.CHANNEL_TYPE%TYPE,
                  I_tsf_entity_id   IN       WH.TSF_ENTITY_ID%TYPE,
                  I_org_unit_id     IN       WH.ORG_UNIT_ID%TYPE,
                  I_to_array        IN       DISTRIBUTION_SQL.INV_FLOW_ARRAY)
RETURN BOOLEAN;
--
FUNCTION NEW_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_item            IN       ITEM_MASTER.ITEM%TYPE,
                 I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                 I_dept            IN       ITEM_MASTER.DEPT%TYPE,
                 I_class           IN       ITEM_MASTER.CLASS%TYPE,
                 I_subclass        IN       ITEM_MASTER.SUBCLASS%TYPE,
                 I_loc             IN       ITEM_LOC.LOC%TYPE,
                 I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                 I_ranged_ind      IN       ITEM_LOC.RANGED_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- This function will check the net available inventory of the virtual warehouses
--- in the location list. This function sets the O_exists to false when, the
--- net available inventory is less than the quantity requested.
-------------------------------------------------------------------------------
FUNCTION CHECK_INV_FLOW_SOH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT BOOLEAN,
                            I_inv_flow_array  IN     distribution_sql.inv_flow_array,
                            I_item            IN     item_master.item%TYPE,
                            I_tsf_qty         IN     item_loc_soh.stock_on_hand%TYPE,
                            I_inv_status      IN     shipsku.inv_status%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--                            PUBLIC FUNCTIONS                                --
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE(O_error_message        IN OUT   VARCHAR2,
                    O_dist_tab             IN OUT   DIST_TABLE_TYPE,
                    I_item                 IN       ITEM_LOC.ITEM%TYPE,
                    I_loc                  IN       ITEM_LOC.LOC%TYPE,
                    I_qty                  IN       NUMBER,
                    I_CMI                  IN       VARCHAR2,
                    I_inv_status           IN       INV_STATUS_TYPES.INV_STATUS%TYPE,
                    I_to_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_to_loc               IN       ITEM_LOC.LOC%TYPE,
                    I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                    I_shipment             IN       SHIPITEM_INV_FLOW.SHIPMENT%TYPE,
                    I_seq_no               IN       SHIPITEM_INV_FLOW.SEQ_NO%TYPE,
                    I_cycle_count          IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE DEFAULT NULL,
                    I_rtv_order_no         IN       RTV_HEAD.RTV_ORDER_NO%TYPE DEFAULT NULL,
                    I_rtv_seq_no           IN       RTV_DETAIL.SEQ_NO%TYPE DEFAULT NULL,
                    I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE DEFAULT NULL,
                    I_tsf_create_ind       IN       VARCHAR2 DEFAULT 'N',
                    I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE DEFAULT 'N',
                    I_alloc_no             IN       ALLOC_HEADER.ALLOC_NO%TYPE DEFAULT NULL)

RETURN BOOLEAN IS

   j   INTEGER;

BEGIN

   --

   if VALIDATE_INPUTS(O_error_message,
                      I_item,
                      I_loc,
                      I_qty,
                      I_CMI,
                      I_shipment) = FALSE then
      return FALSE;
   end if;

   --

   LP_dist_tab.delete;
   O_dist_tab.delete;
   LP_from_array.delete;
   LP_to_array.delete;
   --

   LP_item := I_item;
   LP_physical_wh := I_loc;
   --
   if I_qty < 0 then
      LP_negative_distribution_qty := TRUE;
      LP_sign := -1;
   else
      LP_negative_distribution_qty := FALSE;
      LP_sign := +1;
   end if;
   LP_qty_to_distribute := I_qty * LP_sign;

   --

   if SET_ITEM_ATTRIBUTES(O_error_message) = FALSE then
      return FALSE;
   end if;

   --

   LP_CMI := I_CMI;

   if LP_CMI = 'RTV' then
      if DISTRIBUTE_RTV(O_error_message,
                        I_inv_status,
                        I_rtv_order_no,
                        I_rtv_seq_no) = FALSE then
         return FALSE;
      end if;
   elsif LP_CMI = 'INVADJ' then
      if DISTRIBUTE_INVADJ(O_error_message,
                           I_inv_status) = FALSE then
         return FALSE;
      end if;
   elsif LP_CMI = 'TRANSFER' then
      if DISTRIBUTE_TRANSFER(O_error_message,
                             I_inv_status,
                             I_to_loc_type,
                             I_to_loc,
                             I_shipment,
                             I_seq_no,
                             I_tsf_no,
                             I_seq_no,
                             I_tsf_create_ind,
                             I_cust_order_loc_ind) = FALSE then
         return FALSE;
      end if;
   elsif LP_CMI = 'ALLOC' then
      if DISTRIBUTE_ALLOC(O_error_message,
                          I_alloc_no) = FALSE then
         return FALSE;
      end if;   
   elsif LP_CMI = 'STKREC' then
      LP_cycle_count := I_cycle_count;
      if DISTRIBUTE_STKREC(O_error_message) = FALSE then
         return FALSE;
      end if;
   elsif LP_CMI = 'ORDRCV' then
      if DISTRIBUTE_ORDRCV(O_error_message,
                           I_order_no) = FALSE then
         return FALSE;
      end if;
   elsif LP_CMI = 'SHIPMENT' then
      if DISTRIBUTE_SHIPMENT(O_error_message,
                             I_shipment,
                             I_seq_no) = FALSE then
         return FALSE;
      end if;
   elsif LP_CMI = 'ORDRCV_DIST' then
      if DISTRIBUTE_PO_ALLOC(O_error_message,
                             I_order_no) = FALSE then
         return FALSE;
      end if;
   end if;

   --

   if LP_dist_tab.count = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DIST_WH',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   --

   j := 0;
   FOR i in LP_dist_tab.FIRST..LP_dist_tab.LAST LOOP
      if LP_dist_tab(i).dist_qty != 0 or
         I_CMI = 'ORDRCV' or
         I_CMI = 'STKREC' then
         j := j + 1;
         O_dist_tab(j).wh := LP_dist_tab(i).wh;
         O_dist_tab(j).from_loc := LP_dist_tab(i).from_loc;
         O_dist_tab(j).to_loc := LP_dist_tab(i).to_loc;
         O_dist_tab(j).dist_qty := LP_dist_tab(i).dist_qty * LP_sign;
      end if;
   END LOOP;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.DISTRIBUTE',
                                            to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--                           PRIVATE FUNCTIONS                                --
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_RTV(O_error_message IN OUT VARCHAR2,
                        I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE,
                        I_rtv_order_no  IN     RTV_HEAD.RTV_ORDER_NO%TYPE,
                        I_rtv_seq_no    IN     RTV_DETAIL.SEQ_NO%TYPE)
RETURN BOOLEAN IS

BEGIN

   --
    if DISTRIBUTE_RTV_SHIP(O_error_message,
                           I_inv_status,
                           I_rtv_order_no,
                           I_rtv_seq_no) = FALSE then
       return FALSE;
    end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.DISTRIBUTE_RTV',
                                            to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_RTV;
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_INVADJ(O_error_message IN OUT VARCHAR2,
                        I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE)
RETURN BOOLEAN IS

L_il_create_rule   VARCHAR2(10);
L_wh               WH.WH%TYPE:= null;

BEGIN

   --

   if LP_negative_distribution_qty = TRUE then
      if DISTRIBUTE_INVADJ_OUT(O_error_message,
                               I_inv_status) = FALSE then
         return FALSE;
      end if;
      L_il_create_rule := 'OUTBOUND';
   else
      if DISTRIBUTE_INVADJ_IN(O_error_message,
                              I_inv_status) = FALSE then
         return FALSE;
      end if;
      L_il_create_rule := 'INBOUND';
   end if;
   --
   if LP_dist_tab.count = 0 then
      if CREATE_ITEM_LOC_REL(O_error_message,
                             L_wh,
                             L_il_create_rule) = FALSE then
         return FALSE;
      end if;
      --
      if L_wh is not null then
         LP_dist_tab(1).wh := L_wh;
         LP_dist_tab(1).dist_qty := LP_qty_to_distribute;
      end if;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           'DISTRIBUTION_SQL.DISTRIBUTE_INVADJ',
                                           to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_INVADJ;
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_INVADJ_OUT(O_error_message IN OUT VARCHAR2,
                        I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE)
RETURN BOOLEAN IS
   i                  INTEGER;
   cursor C_WH is
      select w.wh loc,
             w.protected_ind
        from wh w,
             item_loc il
       where il.item = LP_item
         and il.loc = w.wh
         ---
         and w.stockholding_ind = 'Y'
         and w.finisher_ind = 'N'
         and w.physical_wh = LP_physical_wh
    order by protected_ind;

BEGIN

   i := 0;
   for c_rec in C_WH
   loop
      i := i + 1;
      LP_from_array(i).loc             := c_rec.loc;
      LP_from_array(i).protected_ind   := c_rec.protected_ind;
   end loop;
   --

   if I_inv_status is null then
      if OUTBOUND_DIST_RULE_1(O_error_message,
                              I_inv_status) = FALSE then
         return FALSE;
      end if;
   else
      if OUTBOUND_DIST_RULE_2(O_error_message,
                              I_inv_status) = FALSE then
         return FALSE;
      end if;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                       SQLERRM,
                                       'DISTRIBUTION_SQL.DISTRIBUTE_INVADJ_OUT',
                                       to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_INVADJ_OUT;
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_INVADJ_IN(O_error_message IN OUT VARCHAR2,
                        I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE)
RETURN BOOLEAN IS

BEGIN

   --

   if INBOUND_DIST_RULE_1(O_error_message,
                          I_inv_status) = FALSE then
      return FALSE;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'DISTRIBUTION_SQL.DISTRIBUTE_INVADJ_IN',
                                        to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_INVADJ_IN;
--------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_TRANSFER_CREATE(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_from_loc             IN       ITEM_LOC.LOC%TYPE,
                                    I_from_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                                    I_to_loc               IN       ITEM_LOC.LOC%TYPE,
                                    I_to_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                                    I_tsf_qty              IN       TSFDETAIL.TSF_QTY%TYPE,
                                    I_inv_status           IN       TSFDETAIL.INV_STATUS%TYPE,
                                    I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE,
                                    I_seq_no               IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                                    I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE)
RETURN BOOLEAN IS

L_program          VARCHAR2(60) := 'DISTRIBUTION_SQL.DISTRIBUTE_TRANSFER_CREATE';

L_to_loc           ITEM_LOC.LOC%TYPE;
L_keepgoing        BOOLEAN := TRUE;
L_found            BOOLEAN := FALSE;
L_tsf_qty          TSFDETAIL.TSF_QTY%TYPE := I_tsf_qty;
L_qty              NUMBER  := 0;

BEGIN

   -- Get the from location loading filtering with business logic
   -- When no ITEM_LOC for the from physical wh, create the item_loc for the first vwh with max level matching
   -- When no ITEM_LOC for the to physical wh, create the item_loc for the first vwh with max level matching
   -- Mapping based on the business rules.

   -- Load from loc array
   if LOAD_INV_FLOW_LOC(O_error_message,
                        LP_from_array,
                        'F',             --from_loc
                        I_from_loc,
                        I_from_loc_type,
                        I_to_loc,  --other loc
                        I_to_loc_type, --other loc type
                        LP_item,
                        L_tsf_qty,
                        I_inv_status,
                        I_cust_order_loc_ind) = FALSE then
      return FALSE;
   end if;
   --
   if I_from_loc_type in ('S','E') then
      -- For 'S'tores, dist_qty contain all the L_tsf_qty
      LP_dist_tab(1).wh := I_from_loc;
      LP_dist_tab(1).from_loc := I_from_loc;
      LP_dist_tab(1).dist_qty := L_tsf_qty;

   else
      if I_inv_status is null then
         -- this indicates that the stock on hand quantities should be distributed.
         if OUTBOUND_DIST_RULE_1(O_error_message,
                                I_inv_status) = FALSE then
            return FALSE;
         end if;
      else
         -- this indicates a distribution of unavailable quantities
         if OUTBOUND_DIST_RULE_2(O_error_message,
                                I_inv_status) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
 
   FOR dist_cnt IN LP_dist_tab.FIRST..LP_dist_tab.LAST LOOP 
      L_qty := L_qty + LP_dist_tab(dist_cnt).dist_qty; 
   END LOOP; 
           
   if L_qty != L_tsf_qty then  
      if DRAW_DOWN_WH(O_error_message, LP_from_array(1).loc) = FALSE then
         RETURN FALSE;
      end if ;
   end if; 

   /*
    * Assign dist table qtys to flow table qtys:
    *   -Walk through the array of distributed locations (L_dist_array)
    *    returned by the call to OUTBOUND_DIST_RULE_1/2.
    *
    *      -Walk through the array of possible locations to pull stock from
    *
    *         -If the distributed loc equals the possible loc
    *
    *             Assing the distribueted loc's qty to the possible loc.
    *
    */

   FOR dist_cnt IN LP_dist_tab.FIRST..LP_dist_tab.LAST LOOP
      L_found := FALSE;
      FOR flow_cnt IN LP_from_array.FIRST..LP_from_array.LAST LOOP
         if LP_dist_tab(dist_cnt).wh = LP_from_array(flow_cnt).loc then
            LP_from_array(flow_cnt).qty := LP_dist_tab(dist_cnt).dist_qty;
            L_found := TRUE;
            EXIT;
         end if;
      END LOOP;

      --Check that at least 1 loc in LP_from_array is found to match LP_dist_tab(dist_cnt).wh.
      if L_found = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DIST_WH',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   END LOOP;

   --load to loc array
   if LOAD_INV_FLOW_LOC(O_error_message,
                        LP_to_array,
                        'T',             --from_loc
                        I_to_loc,
                        I_to_loc_type,
                        I_from_loc,  --other loc
                        I_from_loc_type, --other loc type
                        LP_item,
                        L_tsf_qty,
                        I_inv_status,
                        I_cust_order_loc_ind) = FALSE then
      return FALSE;
   end if;

   /*
    * Assign from loc qtys to to locations:
    *   Walk through all the from locations, attempt to map each
    *   from location that has qty assigned to it to a to location.
    *
    *   First try to assign the qty to a to loc that has an item_loc
    *   of status A, if one cannot be found, attempt to assign the qty
    *   to a to loc that has an item_loc status of C. If none is found
    *   and cust_order_loc_ind is Y, attempt to assign the qty to a
    *   matching to-loc of any status.
    *   if one cannot be found, attempt to assign the qty to a to loc that has an item_loc status of I and if not,then for status 'D'.
    *
    *   If any qty assigned to a from loc can not be mapped to a to
    *   loc reject the transaction.
    */
   FOR dist_cnt IN LP_dist_tab.FIRST..LP_dist_tab.LAST LOOP
      L_to_loc := NULL;
      FOR from_cnt IN LP_from_array.FIRST..LP_from_array.LAST LOOP
         L_keepgoing := TRUE;

         if LP_dist_tab(dist_cnt).wh = LP_from_array(from_cnt).loc then
            --if the loc did not get any qty distributed to it, skip it

            if L_keepgoing = TRUE then
               --apply rules for locs in approved (A) status
               if FIND_MAP(O_error_message,
                           L_keepgoing,
                           L_to_loc,
                           LP_from_array(from_cnt).loc,
                           I_from_loc_type,
                           LP_from_array(from_cnt).qty,
                           'A',
                           LP_from_array(from_cnt).channel_id,
                           LP_from_array(from_cnt).channel_type,
                           LP_from_array(from_cnt).tsf_entity_id,
                           LP_from_array(from_cnt).org_unit_id,
                           LP_to_array) = FALSE then
                  return FALSE;
               end if;
            end if;
            if L_keepgoing = TRUE then
               --apply rules for locs in discontinued (C) status
               if FIND_MAP(O_error_message,
                           L_keepgoing,
                           L_to_loc,
                           LP_from_array(from_cnt).loc,
                           I_from_loc_type,
                           LP_from_array(from_cnt).qty,
                           'C',
                           LP_from_array(from_cnt).channel_id,
                           LP_from_array(from_cnt).channel_type,
                           LP_from_array(from_cnt).tsf_entity_id,
                           LP_from_array(from_cnt).org_unit_id,
                           LP_to_array) = FALSE then
                  return FALSE;
               end if;
            end if;
            
            if L_keepgoing = TRUE then 
                --apply rules for locs in Inactive (I) status 
                if FIND_MAP(O_error_message, 
                            L_keepgoing, 
                                        L_to_loc, 
                                        LP_from_array(from_cnt).loc, 
                                        I_from_loc_type, 
                                        LP_from_array(from_cnt).qty, 
                                        'I', 
                                        LP_from_array(from_cnt).channel_id, 
                                        LP_from_array(from_cnt).channel_type, 
                                        LP_from_array(from_cnt).tsf_entity_id, 
                                        LP_from_array(from_cnt).org_unit_id, 
                                        LP_to_array) = FALSE then 
                   return FALSE; 
                end if;                          
             end if; 
             
             if L_keepgoing = TRUE then 
                --apply rules for locs in Delete (D) status 
                if FIND_MAP(O_error_message, 
                            L_keepgoing, 
                                L_to_loc, 
                                LP_from_array(from_cnt).loc, 
                                I_from_loc_type, 
                                LP_from_array(from_cnt).qty, 
                                'D', 
                                LP_from_array(from_cnt).channel_id, 
                                LP_from_array(from_cnt).channel_type, 
                                LP_from_array(from_cnt).tsf_entity_id, 
                                LP_from_array(from_cnt).org_unit_id, 
                                LP_to_array) = FALSE then 
                   return FALSE; 
                end if; 
             end if; 

            if L_keepgoing = TRUE and I_cust_order_loc_ind = 'Y' then
               --apply rules for locs of any status for customer orders
               if FIND_MAP(O_error_message,
                           L_keepgoing,
                           L_to_loc,
                           LP_from_array(from_cnt).loc,
                           I_from_loc_type,
                           LP_from_array(from_cnt).qty,
                           NULL,
                           LP_from_array(from_cnt).channel_id,
                           LP_from_array(from_cnt).channel_type,
                           LP_from_array(from_cnt).tsf_entity_id,
                           LP_from_array(from_cnt).org_unit_id,
                           LP_to_array) = FALSE then
                  return FALSE;
               end if;
            end if;

            if L_keepgoing = TRUE then
               O_error_message := SQL_LIB.CREATE_MSG('NO_MAP_POSSIBLE',null,null,null);
               return FALSE;
            end if;
         end if;

      END LOOP; --From loc loop

      LP_dist_tab(dist_cnt).to_loc := L_to_loc;
   END LOOP;  -- Dist Loop

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_TRANSFER_CREATE;
--------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_TRANSFER(O_error_message        IN OUT   VARCHAR2,
                             I_inv_status           IN       INV_STATUS_TYPES.INV_STATUS%TYPE,
                             I_to_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                             I_to_loc               IN       ITEM_LOC.LOC%TYPE,
                             I_shipment             IN       NUMBER,
                             I_seq_no               IN       NUMBER,
                             I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE,
                             I_tsf_seq_no           IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                             I_tsf_create_ind       IN       VARCHAR2,
                             I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE)
RETURN BOOLEAN IS

   L_from_loc_type ITEM_LOC.LOC_TYPE%TYPE;
BEGIN

   if I_shipment is null then

      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message ,
                                      L_from_loc_type,
                                      LP_physical_wh) = FALSE then
         return FALSE;
      end if;

      if I_tsf_create_ind = 'Y' then
         if DISTRIBUTE_TRANSFER_CREATE(O_error_message,
                                       LP_physical_wh,
                                       L_from_loc_type,
                                       I_to_loc,
                                       I_to_loc_type,
                                       LP_qty_to_distribute,
                                       I_inv_status,
                                       I_tsf_no,
                                       I_tsf_seq_no,
                                       I_cust_order_loc_ind) = FALSE then
            return FALSE;
         end if;
      else
         if DISTRIBUTE_TRANSFER_OUT(O_error_message,
                                    I_tsf_no,
                                    I_tsf_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      if LP_negative_distribution_qty = FALSE then
         if DISTRIBUTE_TRANSFER_IN(O_error_message,
                                   I_shipment,
                                   I_seq_no) = FALSE then
            return FALSE;
         end if;
      else
         if DISTRIBUTE_TRANSFER_IN_ADJ(O_error_message,
                                       I_shipment,
                                       I_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         'DISTRIBUTION_SQL.DISTRIBUTE_TRANSFER',
                                         to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_TRANSFER;
--------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_TRANSFER_OUT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_tsf_no        IN     TSFITEM_INV_FLOW.TSF_NO%TYPE,
                                 I_seq_no        IN     TSFITEM_INV_FLOW.TSF_SEQ_NO%TYPE)
RETURN BOOLEAN IS
i                  INTEGER;
L_bucket_1_total   NUMBER := 0;
L_bucket_2_total   NUMBER := 0;
L_total            NUMBER := 0;
L_remainder        NUMBER := 0;
L_dist_qty         NUMBER;
L_qty_1            NUMBER := 0;
L_qty_2            NUMBER := 0;

cursor C_TSFITEM_INV_FLOW is
   select tif.from_loc,
          tif.to_loc,
          tif.tsf_qty,
          nvl(tif.shipped_qty, 0) shipped_qty,
          tif.dist_pct
     from tsfitem_inv_flow tif
    where tif.tsf_no = I_tsf_no
      and tif.tsf_seq_no = I_seq_no
      and tif.item = LP_item;

BEGIN

   /*
   *  Inbound transfer shipments will be distributed based on quantities in
   *  the tsfitem_inv_flow table.
   *
   *  bucket_1 will hold the transfer quantity
   *  bucket_2 will hold the shipped quantity capped at the transfer quantity
   *
   *  If the quantity to distribute is equal to the unshipped quantity
   *  distribution quantities are assigned to fulfill the need.
   *
   *  If the quantity to distribute is less than the unshipped quantity
   *  distribution quantities are prorated based on the transfer quantity.
   *
   *  If the quantity to distribute is greater than the unshipped quantity
   *  distribution quantities are first assigned to fulfill the need and then
   *  the orverage is prorated based on the transfer quantity.
   *
   *  The global LP_dist_tab table will contain a from-to location pairing
   *  (from the tsfitem_inv_flow table) along with the distribution quantity.
   */

   --

   i := 0;
   for c_rec in C_TSFITEM_INV_FLOW
   loop
      i := i + 1;
      LP_dist_tab(i).wh := c_rec.from_loc;
      LP_dist_tab(i).from_loc := c_rec.from_loc;
      LP_dist_tab(i).to_loc := c_rec.to_loc;
      --
      LP_dist_tab(i).bucket_1 := c_rec.tsf_qty;
      if c_rec.shipped_qty < c_rec.tsf_qty then
         LP_dist_tab(i).bucket_2 := c_rec.shipped_qty;
      else
         LP_dist_tab(i).bucket_2 := c_rec.tsf_qty;
      end if;
      LP_dist_tab(i).dist_qty := 0;
      --
      L_bucket_1_total := L_bucket_1_total + LP_dist_tab(i).bucket_1;
      L_bucket_2_total := L_bucket_2_total + LP_dist_tab(i).bucket_2;
   end loop;

   if LP_dist_tab.count = 0 then
      return TRUE;
   end if;

   --
   if (LP_qty_to_distribute = L_bucket_1_total - L_bucket_2_total) then
      -- even
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 - LP_dist_tab(i).bucket_2;
      end loop;
   elsif (LP_qty_to_distribute < L_bucket_1_total - L_bucket_2_total) then
      -- short
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
            L_total := L_total + LP_dist_tab(i).bucket_1;
         end if;
      end loop;
      --
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
            if LP_integer_rounding then
               L_dist_qty := L_remainder +
               (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_total);
               --
               LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
               L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
            else
               LP_dist_tab(i).dist_qty := LP_qty_to_distribute *
               LP_dist_tab(i).bucket_1 / L_total;
            end if;
         end if;
      end loop;
   else
      -- over
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 -
            LP_dist_tab(i).bucket_2;
            --
            LP_qty_to_distribute := LP_qty_to_distribute -
            LP_dist_tab(i).dist_qty;
         end if;
      end loop;
      --
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_integer_rounding then
            L_qty_1 := L_remainder +
            (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_bucket_1_total);
            L_qty_2 := ROUND(L_qty_1);
            L_remainder := L_qty_1 - L_qty_2;
            --
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty + L_qty_2;
         else
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty +
            (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_bucket_1_total);
         end if;
      end loop;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.DISTRIBUTE_TRANSFER_OUT',
                                            to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_TRANSFER_OUT;
--------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_TRANSFER_IN(O_error_message IN OUT VARCHAR2,
                                I_shipment      IN     NUMBER,
                                I_seq_no        IN     NUMBER)
RETURN BOOLEAN IS

i                  INTEGER;
L_bucket_1_total   NUMBER := 0;
L_bucket_2_total   NUMBER := 0;
L_total            NUMBER := 0;
L_remainder        NUMBER := 0;
L_dist_qty         NUMBER;
L_qty_1            NUMBER := 0;
L_qty_2            NUMBER := 0;

cursor C_SHIPITEM_INV_FLOW is
   select from_loc,
          to_loc,
          tsf_qty,
          nvl(received_qty,0) received_qty,
          dist_pct
    from shipitem_inv_flow
   where seq_no = I_seq_no
     and shipment = I_shipment
     and item = LP_item;

BEGIN

   /*
   *  Inbound transfer shipments will be distributed based on quantities in
   *  the shipitem_inv_flow table.
   *
   *  bucket_1 will hold the transfer quantity
   *  bucket_2 will hold the received quantity capped at the transfer quantity
   *
   *  If the quantity to distribute is equal to the unreceived quantity
   *  distribution quantities are assigned to fulfill the need.
   *
   *  If the quantity to distribute is less than the unreceived quantity
   *  distribution quantities are prorated based on the transfer quantity.
   *
   *  If the quantity to distribute is greater than the unreceived quantity
   *  distribution quantities are first assigned to fulfill the need and then
   *  the orverage is prorated based on the transfer quantity.
   *
   *  The global LP_dist_tab table will contain a from-to location pairing
   *  (from the shipitem_inv_flow table) along with the distribution quantity.
   */

   --

   i := 0;
   for c_rec in C_SHIPITEM_INV_FLOW
   loop
      i := i + 1;
      LP_dist_tab(i).from_loc := c_rec.from_loc;
      LP_dist_tab(i).to_loc := c_rec.to_loc;
      --
      LP_dist_tab(i).bucket_1 := c_rec.tsf_qty;
      if c_rec.received_qty < c_rec.tsf_qty then
         LP_dist_tab(i).bucket_2 := c_rec.received_qty;
      else
         LP_dist_tab(i).bucket_2 := c_rec.tsf_qty;
      end if;
      LP_dist_tab(i).dist_qty := 0;
      --
      L_bucket_1_total := L_bucket_1_total + LP_dist_tab(i).bucket_1;
      L_bucket_2_total := L_bucket_2_total + LP_dist_tab(i).bucket_2;
   end loop;

   if LP_dist_tab.count = 0 then
      return TRUE;
   end if;

   --

   if L_bucket_1_total = (L_bucket_2_total + LP_qty_to_distribute) then
      -- even
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 -
         LP_dist_tab(i).bucket_2;
      end loop;
   elsif L_bucket_1_total > (L_bucket_2_total + LP_qty_to_distribute) then
      -- short
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
            L_total := L_total + LP_dist_tab(i).bucket_1;
         end if;
      end loop;
      --
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
            if LP_integer_rounding then
               L_dist_qty := L_remainder +
               (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_total);
               --
               LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
               L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
            else
               LP_dist_tab(i).dist_qty := LP_qty_to_distribute *
               LP_dist_tab(i).bucket_1 / L_total;
            end if;
         end if;
      end loop;
   else
      -- over
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 -
            LP_dist_tab(i).bucket_2;
            --
            LP_qty_to_distribute := LP_qty_to_distribute -
            LP_dist_tab(i).dist_qty;
         end if;
      end loop;
      --
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_integer_rounding then
            L_qty_1 := L_remainder +
            (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_bucket_1_total);
            L_qty_2 := ROUND(L_qty_1);
            L_remainder := L_qty_1 - L_qty_2;
            --
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty + L_qty_2;
         else
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty +
            (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_bucket_1_total);
         end if;
      end loop;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                      SQLERRM,
                                      'DISTRIBUTION_SQL.DISTRIBUTE_TRANSFER_IN',
                                      to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_TRANSFER_IN;
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_TRANSFER_IN_ADJ(O_error_message IN OUT VARCHAR2,
                                    I_shipment      IN     NUMBER,
                                    I_seq_no        IN     NUMBER)
RETURN BOOLEAN IS

i                  INTEGER;
L_bucket_2_total   NUMBER := 0;
L_remainder        NUMBER := 0;
L_dist_qty         NUMBER := 0;
L_qty              NUMBER := 0;

cursor C_SHIPITEM_INV_FLOW is
   select from_loc,
          to_loc,
          nvl(received_qty,0) received_qty
    from shipitem_inv_flow
   where seq_no = I_seq_no
     and shipment = I_shipment
     and item = LP_item;

BEGIN

   /*
   *  Inbound adjustments to transfer shipments will be distributed based on
   *  received quantities in the shipitem_inv_flow table.
   *
   *  bucket_2 will hold the received quantity
   *
   *  If the quantity to distribute is equal to the received quantity
   *  distribution quantities are assigned to reverse the receipt.
   *
   *  If the quantity to distribute is less than the received quantity
   *  distribution quantities are prorated based on the received quantity.
   *
   *  If the quantity to distribute is greater than the received quantity
   *  an error is raised.
   *
   *  The global LP_dist_tab table will contain a from-to location pairing
   *  (from the shipitem_inv_flow table) along with the distribution quantity.
   */

   --

   i := 0;
   for c_rec in C_SHIPITEM_INV_FLOW
   loop
      i := i + 1;
      LP_dist_tab(i).dist_qty := 0;
      LP_dist_tab(i).from_loc := c_rec.from_loc;
      LP_dist_tab(i).to_loc := c_rec.to_loc;
      LP_dist_tab(i).bucket_2 := c_rec.received_qty;
      --
      L_bucket_2_total := L_bucket_2_total + LP_dist_tab(i).bucket_2;
   end loop;

   if LP_dist_tab.count = 0 then
      return TRUE;
   end if;

   --

   if L_bucket_2_total = LP_qty_to_distribute then
      -- even
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_2;
      end loop;
   elsif L_bucket_2_total > LP_qty_to_distribute then
      -- short
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_integer_rounding then
            L_dist_qty := L_remainder +
            (LP_qty_to_distribute * LP_dist_tab(i).bucket_2 / L_bucket_2_total);
            --
            LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
            L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
         else
            LP_dist_tab(i).dist_qty := LP_qty_to_distribute *
            LP_dist_tab(i).bucket_2 / L_bucket_2_total;
         end if;
      end loop;
   elsif L_bucket_2_total = 0 then
      L_qty := LP_qty_to_distribute / LP_dist_tab.count;
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_integer_rounding then
            L_dist_qty :=  L_remainder + L_qty;
            --
            LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
            L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
         else
            LP_dist_tab(i).dist_qty := L_qty;
         end if;
      end loop;  
   else
      -- over
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_integer_rounding then
            L_dist_qty := L_remainder +
            (LP_qty_to_distribute * LP_dist_tab(i).bucket_2 / L_bucket_2_total);
            --
            LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
            L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
         else
            LP_dist_tab(i).dist_qty := LP_qty_to_distribute *
            LP_dist_tab(i).bucket_2 / L_bucket_2_total;
         end if; 
      end loop;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                  SQLERRM,
                                  'DISTRIBUTION_SQL.DISTRIBUTE_TRANSFER_IN_ADJ',
                                  to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_TRANSFER_IN_ADJ;
--------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS

   i                  INTEGER := 0;
   L_bucket_1_total   ALLOC_DETAIL.QTY_ALLOCATED%TYPE := 0;

   cursor C_ALLOC_QTY is
      select ad.to_loc,
             ad.qty_allocated
        from alloc_detail ad,
             wh
       where ad.alloc_no = I_alloc_no
         and ad.to_loc = wh.wh
         and wh.physical_wh = LP_physical_wh;

BEGIN

   FOR rec in C_ALLOC_QTY LOOP
      i := i + 1;
      LP_dist_tab(i).to_loc := rec.to_loc;
      LP_dist_tab(i).bucket_1 := rec.qty_allocated;
      L_bucket_1_total := L_bucket_1_total + rec.qty_allocated;
   END LOOP;

   if LP_dist_tab.count = 0 then
      return TRUE;
   end if;

  if L_bucket_1_total > 0 then
   if PRORATE_BUCKET_1(O_error_message,
                       L_bucket_1_total,
                       LP_dist_tab.FIRST,
                       LP_dist_tab.LAST) = FALSE then
      return FALSE;
   end if;
  else
      LP_dist_tab(1).dist_qty := LP_qty_to_distribute;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.DISTRIBUTE_ALLOC',
                                            to_char(SQLCODE));
      return FALSE;
END DISTRIBUTE_ALLOC;
--------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_STKREC(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

L_il_create_rule   VARCHAR2(10);
L_wh               WH.WH%TYPE:= null;

BEGIN

   --

   if LP_negative_distribution_qty = TRUE then
      if OUTBOUND_DIST_RULE_3(O_error_message) = FALSE then
         return FALSE;
      end if;
      L_il_create_rule := 'OUTBOUND';
   else
      if INBOUND_DIST_RULE_1(O_error_message,
                             null) = FALSE then
         return FALSE;
      end if;
      L_il_create_rule := 'INBOUND';
   end if;
   --
   if LP_dist_tab.count = 0 then
      if CREATE_ITEM_LOC_REL(O_error_message,
                             L_wh,
                             L_il_create_rule) = FALSE then
         return FALSE;
      end if;
      --
      if L_wh is not null then
         LP_dist_tab(1).wh := L_wh;
         LP_dist_tab(1).dist_qty := LP_qty_to_distribute;
      end if;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           'DISTRIBUTION_SQL.DISTRIBUTE_STKREC',
                                           to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_STKREC;
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_ORDRCV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

BEGIN

   --

   if LP_negative_distribution_qty = FALSE then
      if DISTRIBUTE_ORDRCV_IN(O_error_message,
                              I_order_no) = FALSE then
         return FALSE;
      end if;
   else
      if DISTRIBUTE_ORDRCV_ADJ(O_error_message,
                               I_order_no) = FALSE then
         return FALSE;
      end if;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         'DISTRIBUTION_SQL.DISTRIBUTE_ORDRCV',
                                         to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_ORDRCV;
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_ORDRCV_IN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

i                              INTEGER;
L_bucket_type                  VARCHAR2(50);
L_bucket_1_unprotected_total   NUMBER := 0;
L_bucket_1_protected_total     NUMBER := 0;
L_bucket_2_unprotected_total   NUMBER := 0;
L_bucket_2_protected_total     NUMBER := 0;
L_bucket_1_total               NUMBER := 0;
L_bucket_2_total               NUMBER := 0;
L_wh                           WH.WH%TYPE:= null;

cursor C_DISTRIBUTION_RULE is
   select distribution_rule
     from system_options;

cursor C_WH is
   select w.wh,
          w.protected_ind,
          w.restricted_ind,
          ol.qty_ordered - sum(nvl(ad.qty_allocated,0)) qty_ord,
          nvl(ol.qty_received,0) - sum(nvl(ad.po_rcvd_qty,0)) qty_rcv
     from wh w,
          ordloc ol,
          alloc_header ah,
          alloc_detail ad
    where ad.alloc_no (+) = nvl(ah.alloc_no,-999)
      --
      and ah.order_no (+) = ol.order_no
      and ah.item (+) = ol.item
      and ah.wh (+) = ol.location
      --
      and ol.order_no = I_order_no
      and ol.item = LP_item
      and ol.location = w.wh
      --
      and w.physical_wh = LP_physical_wh
 group by w.wh,
          w.protected_ind,
          w.restricted_ind,
          ol.qty_ordered,
          nvl(ol.qty_received,0)
 order by w.protected_ind desc,
          qty_ord,
          w.wh;

BEGIN

   /*
   *  Order receipts are distributed based on order and receipt quantities in
   *  the ordloc table.  Allocations associated with the order are ignored.
   *
   *  bucket_1 will hold the order quantity
   *  bucket_2 will hold the received quantity capped at the order quantity
   *
   *  If the quantity to distribute is equal to the unreceived quantity,
   *  distribution quantities are assigned to fulfill the need.
   *
   *  If the quantity to distribute is less than the unreceived quantity,
   *  distribution quantities are distributed based on the distribution rule.
   *  An attempt is made first to assign the distribution quantity to the
   *  protected warehouses, and then, if quantities are still left
   *  undistributed, they will be distributed among the unprotected warehouses.
   *
   *  If the quantity to distribute is greater than the unreceived quantity
   *  distribution quantities are first assigned to fulfill the need and then
   *  the orverage is prorated based on the order quantity.
   *
   *  If no ordloc records exist for the item-location-order_no then the
   *  distribution quantities are distributed using the 'inbound distribution
   *  rule 1'.  If we are unable to perform the distribution based on this
   *  rule (all item_loc relationships are in a status of 'D'), then an
   *  ttempt is made to create an item_loc relationship for the item at a
   *  virtual warehouse (of the physical warehouse) that currently has none
   *  and assign to it the entire quantity to distribute.
   */

   --

   open C_DISTRIBUTION_RULE;
   fetch C_DISTRIBUTION_RULE into LP_dist_rule;
   close C_DISTRIBUTION_RULE;

   --

   i := 0;
   for c_rec in C_WH
   loop
      i := i + 1;
      LP_dist_tab(i).wh := c_rec.wh;
      LP_dist_tab(i).protected_ind := c_rec.protected_ind;
      LP_dist_tab(i).restricted_ind := c_rec.restricted_ind;
      --
      LP_dist_tab(i).bucket_1 := c_rec.qty_ord;
      if c_rec.qty_rcv < c_rec.qty_ord then
         LP_dist_tab(i).bucket_2 := c_rec.qty_rcv;
      else
         LP_dist_tab(i).bucket_2 := c_rec.qty_ord;
      end if;
      LP_dist_tab(i).dist_qty := 0;
      --
      if c_rec.protected_ind = 'N' then
         L_bucket_1_unprotected_total := L_bucket_1_unprotected_total +
         LP_dist_tab(i).bucket_1;
         L_bucket_2_unprotected_total := L_bucket_2_unprotected_total +
         LP_dist_tab(i).bucket_2;
      else
         L_bucket_1_protected_total := L_bucket_1_protected_total +
         LP_dist_tab(i).bucket_1;
         L_bucket_2_protected_total := L_bucket_2_protected_total +
         LP_dist_tab(i).bucket_2;
      end if;
   end loop;

   L_bucket_1_total := L_bucket_1_unprotected_total +
   L_bucket_1_protected_total;
   L_bucket_2_total := L_bucket_2_unprotected_total +
   L_bucket_2_protected_total;

   --

   -- If the PO is fully allocated an even distribution is performed.

   if LP_dist_tab.count != 0 and L_bucket_1_total = 0 then
      if DISTRIBUTE_ALLOCATED_PO(O_error_message) = FALSE then
         return FALSE;
      end if;
      --
      return TRUE;
   end if;

   --

   if LP_dist_tab.count = 0 then
      if INBOUND_DIST_RULE_1(O_error_message,
                             null,
                             I_order_no) = FALSE then
         return FALSE;
      end if;
      --
      if LP_dist_tab.count = 0 then
         if CREATE_ITEM_LOC_REL(O_error_message,
                                L_wh,
                                'INBOUND',
                                I_order_no) = FALSE then
            return FALSE;
         end if;
         --
         if L_wh is not null then
            LP_dist_tab(1).wh := L_wh;
            LP_dist_tab(1).dist_qty := LP_qty_to_distribute;
         end if;
      end if;
      --
      return TRUE;
   end if;

   --

   if L_bucket_1_total = (L_bucket_2_total + LP_qty_to_distribute) then
      -- even
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 -
         LP_dist_tab(i).bucket_2;
      end loop;
   elsif L_bucket_1_total > (L_bucket_2_total + LP_qty_to_distribute) then
      -- short
      if SET_DIST_TAB_INDEXES(O_error_message) = FALSE then
         return FALSE;
      end if;
      --
      L_bucket_type := 'PROTECTED';
      loop
         --
         if L_bucket_type = 'PROTECTED' then
            L_bucket_1_total := L_bucket_1_protected_total;
            L_bucket_2_total := L_bucket_2_protected_total;
         else
            L_bucket_1_total := L_bucket_1_unprotected_total;
            L_bucket_2_total := L_bucket_2_unprotected_total;
         end if;

         if (L_bucket_1_total - L_bucket_2_total) > LP_qty_to_distribute then
            if DISTRIBUTE_SHORT(O_error_message,
                                L_bucket_type) = FALSE then
               return FALSE;
            end if;
            return TRUE;
         else
            if DRAW_UP_SHORT(O_error_message,
                             L_bucket_type) = FALSE then
               return FALSE;
            end if;
         end if;
         --
         if L_bucket_type = 'PROTECTED' then
            L_bucket_type := 'UNPROTECTED';
         else
            exit;
         end if;
      end loop;
      --
   else
      -- over
      if DISTRIBUTE_OVER(O_error_message,
                         L_bucket_type,
                         L_bucket_1_total) = FALSE then
         return FALSE;
      end if;
   end if;


   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'DISTRIBUTION_SQL.DISTRIBUTE_ORDRCV_IN',
                                        to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_ORDRCV_IN;
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_ORDRCV_ADJ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

i                  INTEGER;
L_remainder        NUMBER := 0;
L_bucket_1_total   NUMBER := 0;
L_bucket_2_total   NUMBER := 0;
L_dist_qty         NUMBER := 0;
L_qty_1            NUMBER := 0;
L_qty_2            NUMBER := 0;

cursor C_WH is
   select w.wh,
          nvl(ol.qty_received,0) - sum(nvl(ad.po_rcvd_qty,0)) unalloc_qty_rcv,
          sum(nvl(ad.po_rcvd_qty,0)) alloc_qty_rcv
     from wh w,
          ordloc ol,
          alloc_header ah,
          alloc_detail ad
    where ad.alloc_no (+) = nvl(ah.alloc_no,-999)
      --
      and ah.order_no (+) = ol.order_no
      and ah.item (+) = ol.item
      and ah.wh (+) = ol.location
      --
      and ol.order_no = I_order_no
      and ol.item = LP_item
      and ol.location = w.wh
      --
      and w.physical_wh = LP_physical_wh
 group by w.wh,
          nvl(ol.qty_received,0);

BEGIN

   /*
   *  Receipt adjustments to order shipments will be distributed based on
   *  received quantities in the ordloc table.
   *
   *  bucket_1 will hold the unallocated received quantity
   *  bucket_2 will hold the allocated received quantity
   *
   *  If the quantity to distribute is equal to the unallocated received
   *  quantity, distribution quantities are assigned to reverse the receipt.
   *
   *  If the quantity to distribute is less than the unallocted received
   *  quantity, distribution quantities are prorated based on the received
   *  quantity.
   *
   *  If the quantity to distribute is greater than the unallocated received
   *  quantity we will attempt to pull the excess from allocated received
   *  quantities.
   *
   *  If however the quantity to distribute is greater than the total received
   *  quantity, an error is raised.
   */

   --

   i := 0;
   for c_rec in C_WH
   loop
      i := i + 1;
      LP_dist_tab(i).dist_qty := 0;
      LP_dist_tab(i).wh := c_rec.wh;
      LP_dist_tab(i).bucket_1 := c_rec.unalloc_qty_rcv;
      LP_dist_tab(i).bucket_2 := c_rec.alloc_qty_rcv;
      --
      L_bucket_1_total := L_bucket_1_total + LP_dist_tab(i).bucket_1;
      L_bucket_2_total := L_bucket_2_total + LP_dist_tab(i).bucket_2;
   end loop;

   if LP_dist_tab.count = 0 then
      return TRUE;
   end if;

   --

   if L_bucket_1_total = LP_qty_to_distribute then
      -- even
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1;
      end loop;
   elsif L_bucket_1_total > LP_qty_to_distribute then
      -- short
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         if LP_integer_rounding then
            L_dist_qty := L_remainder +
            (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_bucket_1_total);
            --
            LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
            L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
         else
            LP_dist_tab(i).dist_qty := LP_qty_to_distribute *
            LP_dist_tab(i).bucket_1 / L_bucket_1_total;
         end if;
      end loop;
   else
      -- try to pull from total received quantities (allocated and unallocated)
      if (L_bucket_1_total + L_bucket_2_total) = LP_qty_to_distribute then
         -- even
         for i in LP_dist_tab.first .. LP_dist_tab.last
         loop
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 +
            LP_dist_tab(i).bucket_2;
         end loop;
      elsif (L_bucket_1_total + L_bucket_2_total) > LP_qty_to_distribute then
         -- short
         -- first draw down bucket_1
         for i in LP_dist_tab.first .. LP_dist_tab.last
         loop
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1;
            LP_qty_to_distribute := LP_qty_to_distribute -
            LP_dist_tab(i).bucket_1;
         end loop;
         -- then pull the remainder from bucket_2
         for i in LP_dist_tab.first .. LP_dist_tab.last
         loop
            if LP_integer_rounding then
               L_qty_1 := L_remainder +
               (LP_qty_to_distribute * LP_dist_tab(i).bucket_2 /
               L_bucket_2_total);
               L_qty_2 := ROUND(L_qty_1);
               L_remainder := L_qty_1 - L_qty_2;
               --
               LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty + L_qty_2;
            else
               LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty +
               (LP_qty_to_distribute * LP_dist_tab(i).bucket_2 /
               L_bucket_2_total);
            end if;
         end loop;
      else
         -- over
         for i in LP_dist_tab.first .. LP_dist_tab.last
         loop
            if LP_integer_rounding then
               L_dist_qty := L_remainder +
               (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_bucket_1_total);
               --
               LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
               L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
            else
               LP_dist_tab(i).dist_qty := LP_qty_to_distribute *
               LP_dist_tab(i).bucket_1 / L_bucket_1_total;
            end if;
         end loop;
     end if;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                       SQLERRM,
                                       'DISTRIBUTION_SQL.DISTRIBUTE_ORDRCV_ADJ',
                                       to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_ORDRCV_ADJ;
--------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_shipment        IN       NUMBER,
                             I_seq_no          IN       NUMBER)
RETURN BOOLEAN IS

   L_program                      VARCHAR2(60)   := 'DISTRIBUTION_SQL.DISTRIBUTE_SHIPMENT';
   i                              INTEGER;
   L_bucket_type                  VARCHAR2(50);
   L_bucket_1_unprotected_total   NUMBER         := 0;
   L_bucket_1_protected_total     NUMBER         := 0;
   L_bucket_2_unprotected_total   NUMBER         := 0;
   L_bucket_2_protected_total     NUMBER         := 0;
   L_bucket_1_total               NUMBER         := 0;
   L_bucket_2_total               NUMBER         := 0;
   L_wh                           WH.WH%TYPE     := NULL;

   L_physical_wh                  WH.PHYSICAL_WH%TYPE;
   L_order_no                     ORDHEAD.ORDER_NO%TYPE;
   L_location                     ORDHEAD.LOCATION%TYPE;
   L_loc_type                     ORDHEAD.LOC_TYPE%TYPE;
   L_store_type                   STORE.STORE_TYPE%TYPE;
   
   cursor C_DISTRIBUTION_RULE is
      select distribution_rule
        from system_options;

   -- This cursor will fetch the qty_received for all the virtual warehouses for a shipment/item/seq_no
   -- combination and this will be used to prorate the unmatched qty for the shipmeht when shipment
   -- is partally matched.
   cursor C_WH is
      select w.wh,
             w.protected_ind,
             w.restricted_ind,
             skl.qty_received
        from wh w,
             shipsku_loc skl
       where skl.shipment = I_shipment
         and skl.item = LP_item
         and skl.seq_no = I_seq_no
         and w.wh = skl.to_loc
         and w.physical_wh = L_physical_wh
    order by w.protected_ind desc,
             qty_received,
             w.wh;

BEGIN
   /*
   *  When Shipments are received , receipts are distributed among the virtual
   *  warehouses based on the order quantities on the ordloc in SHIPSKU_LOC table.
   *
   *  After shipment is received and then if there is any receipt adjustment then
   *  the receipt distribution for the virtual warehouses will be based on SHIPSKU_LOC
   *
   *  bucket_1 will hold the received quantity at virtual Warehouse from SHIPSKU_LOC
   *  bucket_2 will hold 0 quantity
   *
   *
   *  This function is called when DISTRIBUTE function is called from REC_COST_ADJ_SQL.UPDATE_ORDER_COST
   *  REC_COST_ADJ_SQL.GET_PO_UNMATCHED_QTY  with I_CMI as SHIPMENT
   */
   --
   select sh.order_no
     into L_order_no
     from shipment sh
    where sh.shipment = I_shipment;

   select oh.location, oh.loc_type
     into L_location,
          L_loc_type
     from ordhead oh
    where oh.order_no = L_order_no;

   if L_loc_type = 'S' then
      select s.store_type
        into L_store_type
        from store s
       where s.store = L_location;

       if L_store_type = 'F' then
          select physical_wh
            into L_physical_wh
            from wh
           where wh = LP_physical_wh;
       else
          L_physical_wh := LP_physical_wh;
       end if;
   else
      L_physical_wh := LP_physical_wh;
   end if;

   open C_DISTRIBUTION_RULE;
   fetch C_DISTRIBUTION_RULE into LP_dist_rule;
   close C_DISTRIBUTION_RULE;
   --
   i := 0;
   FOR c_rec in C_WH LOOP
      i := i + 1;
      LP_dist_tab(i).wh := c_rec.wh;
      LP_dist_tab(i).protected_ind := c_rec.protected_ind;
      LP_dist_tab(i).restricted_ind := c_rec.restricted_ind;
      --
      LP_dist_tab(i).bucket_1 := c_rec.qty_received;
      LP_dist_tab(i).bucket_2 := 0;
      --
      LP_dist_tab(i).dist_qty := 0;
      --
      if c_rec.protected_ind = 'N' then
         L_bucket_1_unprotected_total := L_bucket_1_unprotected_total + LP_dist_tab(i).bucket_1;
         L_bucket_2_unprotected_total := L_bucket_2_unprotected_total + LP_dist_tab(i).bucket_2;
      else
         L_bucket_1_protected_total := L_bucket_1_protected_total + LP_dist_tab(i).bucket_1;
         L_bucket_2_protected_total := L_bucket_2_protected_total + LP_dist_tab(i).bucket_2;
      end if;
   END LOOP;

   L_bucket_1_total := L_bucket_1_unprotected_total + L_bucket_1_protected_total;
   L_bucket_2_total := L_bucket_2_unprotected_total + L_bucket_2_protected_total;

   if LP_dist_tab.count = 0 then
      if INBOUND_DIST_RULE_1(O_error_message,
                             null) = FALSE then
         return FALSE;
      end if;
      --
      if LP_dist_tab.count = 0 then
         if CREATE_ITEM_LOC_REL(O_error_message,
                                L_wh,
                                'INBOUND') = FALSE then
            return FALSE;
         end if;
         --
         if L_wh is not null then
            LP_dist_tab(1).wh := L_wh;
            LP_dist_tab(1).dist_qty := LP_qty_to_distribute;
         end if;
      end if;
      --
      return TRUE;
   end if;
   --
   if L_bucket_1_total = (L_bucket_2_total + LP_qty_to_distribute) then
      -- even
      FOR i in LP_dist_tab.first .. LP_dist_tab.last LOOP
         LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 - LP_dist_tab(i).bucket_2;
      END LOOP;
   elsif L_bucket_1_total > (L_bucket_2_total + LP_qty_to_distribute) then
      -- short
      if SET_DIST_TAB_INDEXES(O_error_message) = FALSE then
         return FALSE;
      end if;
      --
      L_bucket_type := 'PROTECTED';
      LOOP
         --
         if L_bucket_type = 'PROTECTED' then
            L_bucket_1_total := L_bucket_1_protected_total;
            L_bucket_2_total := L_bucket_2_protected_total;
         else
            L_bucket_1_total := L_bucket_1_unprotected_total;
            L_bucket_2_total := L_bucket_2_unprotected_total;
         end if;

         if (L_bucket_1_total - L_bucket_2_total) > LP_qty_to_distribute then
            if DISTRIBUTE_SHORT(O_error_message,
                                L_bucket_type) = FALSE then
               return FALSE;
            end if;
            return TRUE;
         else
            if DRAW_UP_SHORT(O_error_message,
                             L_bucket_type) = FALSE then
               return FALSE;
            end if;
         end if;
         --
         if L_bucket_type = 'PROTECTED' then
            L_bucket_type := 'UNPROTECTED';
         else
            exit;
         end if;
      END LOOP;
      --
   else
      -- over
      if DISTRIBUTE_OVER(O_error_message,
                         L_bucket_type,
                         L_bucket_1_total) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DISTRIBUTE_SHIPMENT;
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_SHORT(O_error_message IN OUT VARCHAR2,
                          I_bucket_type   IN     VARCHAR2)
RETURN BOOLEAN IS

i             INTEGER;
L_index_1     INTEGER;
L_index_2     INTEGER;
L_total       NUMBER := 0;
L_remainder   NUMBER := 0;
L_dist_qty    NUMBER;

BEGIN

   /*
   *  The distribution rule determines how the distribution quantity will be
   *  distributed.  The indexes control the set of global LP_dist_tab table
   *  records that are candidates for the distribution.  The indexes will
   *  represent the group of protected or unprotected warehouses.
   *
   *  Proration is based on the bucket_1 (order) quantity.
   *
   *  MN2MX and MX2MN is based on the bucket_1 quantity.  The global
   *  LP_dist_tab table must be ordered by the order quantity.  The direction
   *  of processing is adjusted based on whether the rule is MN2MX or MX2MN.
   */

   --

   if GET_DIST_TAB_INDEXES(O_error_message,
                           L_index_1,
                           L_index_2,
                           I_bucket_type) = FALSE then
      return FALSE;
   end if;

   if L_index_1 is null or L_index_2 is null then
      return TRUE;
   end if;

   if LP_dist_rule = 'PRORAT' then
      for i in L_index_1 .. L_index_2
      loop
         if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
            L_total := L_total + LP_dist_tab(i).bucket_1;
         end if;
      end loop;
      --
      for i in L_index_1 .. L_index_2
      loop
         if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
            if LP_integer_rounding then
               L_dist_qty := L_remainder +
               (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_total);
               --
               LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
               L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
            else
               LP_dist_tab(i).dist_qty := LP_qty_to_distribute *
               LP_dist_tab(i).bucket_1 / L_total;
            end if;
         end if;
      end loop;
   else
      if LP_dist_rule = 'MN2MX' then
         i := L_index_1 - 1;
      else
         i := L_index_2 + 1;
      end if;
      --
      loop
         if LP_dist_rule = 'MN2MX' then
            i := i + 1;
            --
            if i > L_index_2 then
               exit;
            end if;
         else
            i := i - 1;
            --
            if i < L_index_1 then
               exit;
            end if;
         end if;
         --
         if LP_qty_to_distribute = 0 then
            exit;
         end if;
         --
         if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
            if LP_qty_to_distribute >
            (LP_dist_tab(i).bucket_1 - LP_dist_tab(i).bucket_2) then
               LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 -
               LP_dist_tab(i).bucket_2;
               --
               LP_qty_to_distribute := LP_qty_to_distribute -
               LP_dist_tab(i).dist_qty;
            else
               LP_dist_tab(i).dist_qty := LP_qty_to_distribute;
               --
               LP_qty_to_distribute := 0;
            end if;
         end if;
      end loop;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.DISTRIBUTE_SHORT',
                                            to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_SHORT;
--------------------------------------------------------------------------------

FUNCTION DRAW_UP_SHORT(O_error_message IN OUT VARCHAR2,
                       I_bucket_type   IN     VARCHAR2)
RETURN BOOLEAN IS

L_index_1   INTEGER;
L_index_2   INTEGER;

BEGIN

   --

   -- This function will draw up (distribute) whatever stock is still
   -- unreceived.

   if GET_DIST_TAB_INDEXES(O_error_message,
                           L_index_1,
                           L_index_2,
                           I_bucket_type) = FALSE then
      return FALSE;
   end if;

   if L_index_1 is null or L_index_2 is null then
      return TRUE;
   end if;

   for i in L_index_1 .. L_index_2
   loop
      LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 -
      LP_dist_tab(i).bucket_2;
      --
      LP_qty_to_distribute := LP_qty_to_distribute - LP_dist_tab(i).dist_qty;
   end loop;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.DRAW_UP_SHORT',
                                            to_char(SQLCODE));
      return FALSE;

END DRAW_UP_SHORT;
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_OVER(O_error_message  IN OUT VARCHAR2,
                         I_bucket_type    IN     VARCHAR2,
                         I_bucket_1_total IN     NUMBER)
RETURN BOOLEAN IS

L_dist_tab   INTERNAL_DIST_TABLE_TYPE;

L_index_1       INTEGER;
L_index_2       INTEGER;
L_total         NUMBER := 0;
L_remainder     NUMBER := 0;
L_qty_1         NUMBER := 0;
L_qty_2         NUMBER := 0;

BEGIN

   /*
   *  This function will distribute receipt overages.  First the unreceived
   *  quantities are drawn up.  Then, the overage is prorated among the
   *  unrestricted warehouses.  If all the warehouses are unrestricted,
   *  the overage is prorated among all of them.
   *
   *  A local L_dist_tab table is used to hold the unrestricted warehouses
   *  while doing the proration.  Once completed, the distributed quantities
   *  are copied back to the global LP_dist_tab_table.
   */

   --

   for i in LP_dist_tab.first .. LP_dist_tab.last
   loop
      if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
         LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 -
         LP_dist_tab(i).bucket_2;
         --
         LP_qty_to_distribute := LP_qty_to_distribute - LP_dist_tab(i).dist_qty;
      end if;
   end loop;

   --

   for i in LP_dist_tab.first .. LP_dist_tab.last
   loop
      if LP_dist_tab(i).restricted_ind = 'N' then
         L_dist_tab(i) := LP_dist_tab(i);
         L_total := L_total + L_dist_tab(i).bucket_1;
      end if;
   end loop;

   if L_total = 0 then
      L_dist_tab := LP_dist_tab;
      L_total := I_bucket_1_total;
   end if;

   for i in L_dist_tab.first .. L_dist_tab.last
   loop
      if L_dist_tab.exists(i) = TRUE then
         if LP_integer_rounding then
            L_qty_1 := L_remainder +
            (LP_qty_to_distribute * L_dist_tab(i).bucket_1 / L_total);
            L_qty_2 := ROUND(L_qty_1);
            L_remainder := L_qty_1 - L_qty_2;
            --
            L_dist_tab(i).dist_qty := L_dist_tab(i).dist_qty + L_qty_2;
         else
            L_dist_tab(i).dist_qty := L_dist_tab(i).dist_qty +
            (LP_qty_to_distribute * L_dist_tab(i).bucket_1 / L_total);
         end if;
      end if;
   end loop;

   for i in L_dist_tab.first .. L_dist_tab.last
   loop
      if L_dist_tab.exists(i) = TRUE then
         LP_dist_tab(i).dist_qty := L_dist_tab(i).dist_qty;
      end if;
   end loop;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.DISTRIBUTE_OVER',
                                            to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_OVER;
--------------------------------------------------------------------------------

FUNCTION DISTRIBUTE_ALLOCATED_PO(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

L_remainder   NUMBER := 0;
L_dist_qty    NUMBER := 0;

BEGIN

   /*
   *  This function will distribute a fully allocated PO.  The quantity to
   *  distribute will be distributed evenly across all virtuals on the PO.
   */

   --

   if LP_integer_rounding then
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         L_dist_qty := L_remainder + (LP_qty_to_distribute / LP_dist_tab.count);
         --
         LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
         L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
      end loop;
   else
      for i in LP_dist_tab.first .. LP_dist_tab.last
      loop
         LP_dist_tab(i).dist_qty := LP_qty_to_distribute / LP_dist_tab.count;
      end loop;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                     SQLERRM,
                                     'DISTRIBUTION_SQL.DISTRIBUTE_ALLOCATED_PO',
                                     to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_ALLOCATED_PO;
--------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_RTV_SHIP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_inv_status      IN       INV_STATUS_TYPES.INV_STATUS%TYPE,
                             I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE,
                             I_rtv_seq_no      IN       RTV_DETAIL.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   i                  INTEGER;
   L_exists           VARCHAR2(1);
   L_bucket_1_total   NUMBER := 0;
   L_bucket_2_total   NUMBER := 0;
   L_total            NUMBER := 0;
   L_remainder        NUMBER := 0;
   L_dist_qty         NUMBER := 0;
   L_qty_1            NUMBER := 0;
   L_qty_2            NUMBER := 0;

   cursor C_rtvitem_exists is
      select 'x'
        from rtvitem_inv_flow
       where rtv_order_no = I_rtv_order_no
         and seq_no       = I_rtv_seq_no
         and item         = LP_item;

   cursor C_rtvitem_inv_flow is
      select rtv_order_no,
             seq_no,
             item,
             loc,
             loc_type,
             nvl(qty_requested,0) qty_requested,
             nvl(qty_returned,0) qty_returned
        from rtvitem_inv_flow
       where rtv_order_no = I_rtv_order_no
         and seq_no       = I_rtv_seq_no
         and item         = LP_item;

   cursor C_WH is
      select w.wh loc,
             w.protected_ind
        from wh w,
             item_loc il
       where il.item = LP_item
         and il.loc = w.wh
         ---
         and w.stockholding_ind = 'Y'
         and w.finisher_ind = 'N'
         and w.physical_wh = LP_physical_wh
         and w.org_unit_id in (select org_unit_id 
                               from partner_org_unit p,
                                    rtv_head r
                              where p.partner = r.supplier
                                and r.rtv_order_no = I_rtv_order_no)         
    order by protected_ind;

BEGIN

   /*
   *  This function receives 3 parameters I_inv_status, I_rtv_order_no, I_rtv_seq_no.
   *  RTV shipments will be distributed based on quantities in the RTVITEM_INV_FLOW table.
   *
   Bucket_1 will hold the RTV requested quantity and bucket_2 will hold the RTV returned quantity  capped
   *  at the RTV request quantity.
   *
   *  If the quantity to distribute is equal to the unreturned quantity then the distribution
   *  quantities are assigned to fullfill the need.
   *
   *  If the quantity to distribute is less than the unreturned quantity then the distribution quantities are prorated
   *  based on the RTV requested and RTV returned quantity.
   *
   *  If the quantity to distribute is greater than the unreturned quantity then the distribution quantities are first
      assigned to fullfill the need and then the balance is prorated based on RTV requested and RTV returned quantity.
   */

   open C_rtvitem_exists;
   fetch C_rtvitem_exists into L_exists;
   close C_rtvitem_exists;

   if L_exists is NOT NULL then
      i := 0;
      for c_rec in C_rtvitem_inv_flow
      loop
         i := i + 1;
         if c_rec.loc_type != 'S' then
            ---
            LP_dist_tab(i).wh := c_rec.loc;
            LP_dist_tab(i).bucket_1 := c_rec.qty_requested;
            ---
            if c_rec.qty_returned < c_rec.qty_requested then
               LP_dist_tab(i).bucket_2 := c_rec.qty_returned;
            else
               LP_dist_tab(i).bucket_2 := c_rec.qty_requested;
            end if;
            ---
            L_bucket_1_total := L_bucket_1_total + LP_dist_tab(i).bucket_1;
            L_bucket_2_total := L_bucket_2_total + LP_dist_tab(i).bucket_2;
         end if;
      end loop;
      ---
      if LP_dist_tab.count = 0 then
         return TRUE;
      end if;
      ---
      if L_bucket_1_total = (L_bucket_2_total + LP_qty_to_distribute) then
         -- even
         for i in LP_dist_tab.first .. LP_dist_tab.last
         loop
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 - LP_dist_tab(i).bucket_2;
         end loop;
      elsif L_bucket_1_total > (L_bucket_2_total + LP_qty_to_distribute) then
         -- short
         for i in LP_dist_tab.first .. LP_dist_tab.last
         loop
            if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
               L_total := L_total + LP_dist_tab(i).bucket_1;
            end if;
         end loop;
         ---
         for i in LP_dist_tab.first .. LP_dist_tab.last
         loop
            if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
               if LP_integer_rounding then
                  L_dist_qty := L_remainder + (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_total);
                  --
                  LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
                  L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
               else
                  LP_dist_tab(i).dist_qty := LP_qty_to_distribute *
                  LP_dist_tab(i).bucket_1 / L_total;
               end if;
            end if;
         end loop;
      else
         -- over
         for i in LP_dist_tab.first .. LP_dist_tab.last
         loop
            if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
               LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 - LP_dist_tab(i).bucket_2;
               ---
               LP_qty_to_distribute := LP_qty_to_distribute -
               LP_dist_tab(i).dist_qty;
            end if;
         end loop;
         ---
         for i in LP_dist_tab.first .. LP_dist_tab.last
         loop
            if LP_integer_rounding then
               L_qty_1 := L_remainder + (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_bucket_1_total);
               L_qty_2 := ROUND(L_qty_1);
               L_remainder := L_qty_1 - L_qty_2;
               --
               LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty + L_qty_2;
            else
               LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty + (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / L_bucket_1_total);
            end if;
         end loop;
      end if;
   ---
   elsif L_exists is NULL then

      i := 0;
      for c_rec in C_WH
      loop
         i := i + 1;
         LP_from_array(i).loc             := c_rec.loc;
         LP_from_array(i).protected_ind   := c_rec.protected_ind;
      end loop;

      if I_inv_status is null then
         if OUTBOUND_DIST_RULE_1(O_error_message,
                                 I_inv_status) = FALSE then
            return FALSE;
         end if;
      else
         if OUTBOUND_DIST_RULE_2(O_error_message,
                                 I_inv_status) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                     SQLERRM,
                                     'DISTRIBUTION_SQL.DISTRIBUTE_RTV_SHIP',
                                     to_char(SQLCODE));
      return FALSE;

END DISTRIBUTE_RTV_SHIP;
--------------------------------------------------------------------------------
FUNCTION OUTBOUND_DIST_RULE_1(O_error_message IN OUT VARCHAR2,
                              I_inv_status    IN     INV_ADJ.INV_STATUS%TYPE)
RETURN BOOLEAN IS

i                  INTEGER;
L_index_1          INTEGER;
L_index_2          INTEGER;
L_bucket_type      VARCHAR2(50);
L_bucket_1_total   NUMBER := 0;
L_bucket_2_total   NUMBER := 0;


BEGIN

   /*
   *  outbound distribution rule 1:
   *                                                pull      pull   pull
   *     status   protected   primary   lowest id   till      till   till
   *                ind         vwh        vwh    fulfilled   NI=0   SOH=0
   *       -         N           -          -        -          X      -
   *       -         N           -          -        -          -      X
   *       -         Y           -          -        -          X      -
   *       -         Y           -          -        -          -      X
   *      !=D        -           X          -        X          -      -
   *      !=D        -           -          X        X          -      -
   *
   *  bucket_1 holds the NI postiion
   *  bucket_2 holds the SOH postiion
   */

   if LP_from_array.count = 0 then
      return TRUE;
   end if;

   --
   --Distribute the quantity between the mapped from locations
   i := 0;
   FOR from_cnt IN LP_from_array.FIRST..LP_from_array.LAST LOOP
      i := i + 1;
      LP_dist_tab(i).wh := LP_from_array(from_cnt).loc;
      LP_dist_tab(i).from_loc := LP_from_array(from_cnt).loc;
      LP_dist_tab(i).protected_ind := LP_from_array(from_cnt).protected_ind;
      LP_dist_tab(i).dist_qty := 0;
   END LOOP;

   if SET_DIST_TAB_INDEXES(O_error_message) = FALSE then
      return FALSE;
   end if;

   --

   L_bucket_type := 'UNPROTECTED';
   loop
      if GET_DIST_TAB_INDEXES(O_error_message,
                              L_index_1,
                              L_index_2,
                              L_bucket_type) = FALSE then
         return FALSE;
      end if;

      if L_index_1 is not null and  L_index_2 is not null then
         if POPULATE_BUCKET_1(O_error_message,
                              L_bucket_1_total,
                              I_inv_status,
                              L_index_1,
                              L_index_2) = FALSE then
            return FALSE;
         end if;

         if L_bucket_1_total >= LP_qty_to_distribute then
            if PRORATE_BUCKET_1(O_error_message,
                                L_bucket_1_total,
                                L_index_1,
                                L_index_2) = FALSE then
               return FALSE;
            end if;
            return TRUE;
         else
            if DRAW_DOWN_BUCKET_1(O_error_message,
                                  L_bucket_1_total,
                                  L_index_1,
                                  L_index_2) = FALSE then
               return FALSE;
            end if;
         end if;
         --
         if POPULATE_BUCKET_2(O_error_message,
                              L_bucket_2_total,
                              L_index_1,
                              L_index_2) = FALSE then
            return FALSE;
         end if;

         if (L_bucket_2_total - L_bucket_1_total) >= LP_qty_to_distribute then
            if PRORATE_BUCKET_2(O_error_message,
                                (L_bucket_2_total - L_bucket_1_total),
                                L_index_1,
                                L_index_2) = FALSE then
               return FALSE;
            end if;
            return TRUE;
         else
            if DRAW_DOWN_BUCKET_2(O_error_message,
                                  (L_bucket_2_total - L_bucket_1_total),
                                  L_index_1,
                                  L_index_2) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      --
      if L_bucket_type = 'UNPROTECTED' then
         L_bucket_type := 'PROTECTED';
      else
         exit;
      end if;
   end loop;
   --
   if DRAW_DOWN_WH(O_error_message,
                   null) = FALSE then
      return FALSE;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'DISTRIBUTION_SQL.OUTBOUND_DIST_RULE_1',
                                        to_char(SQLCODE));
      return FALSE;

END OUTBOUND_DIST_RULE_1;
--------------------------------------------------------------------------------

FUNCTION OUTBOUND_DIST_RULE_2(O_error_message IN OUT VARCHAR2,
                              I_inv_status    IN     INV_ADJ.INV_STATUS%TYPE)
RETURN BOOLEAN IS

i                  INTEGER;
L_index_1          INTEGER;
L_index_2          INTEGER;
L_bucket_type      VARCHAR2(50);
L_bucket_1_total   NUMBER := 0;
L_wh               WH.WH%TYPE := null;

BEGIN

   /*
   *  outbound distribution rule 2:
   *                                                pull      pull
   *     status   protected   primary   lowest id   till      till
   *                ind         vwh        vwh    fulfilled   UI=0
   *       -         N           -          -        -          X
   *       -         Y           -          -        -          X
   *      !=D        -           X          -        X          -
   *      !=D        -           -          X        X          -
   *
   *  bucket_1 holds the UI postiion
   */

   if LP_from_array.count = 0 then
      return TRUE;
   end if;

   --
   -- Distribute the quantity between the mapped from locations
   i := 0;
   FOR from_cnt IN LP_from_array.FIRST..LP_from_array.LAST LOOP
      i := i + 1;
      LP_dist_tab(i).wh := LP_from_array(from_cnt).loc;
      LP_dist_tab(i).from_loc := LP_from_array(from_cnt).loc;
      LP_dist_tab(i).protected_ind := LP_from_array(from_cnt).protected_ind;
      LP_dist_tab(i).dist_qty := 0;
   END LOOP;

   if SET_DIST_TAB_INDEXES(O_error_message) = FALSE then
      return FALSE;
   end if;

   --

   L_bucket_type := 'UNPROTECTED';
   loop
      if GET_DIST_TAB_INDEXES(O_error_message,
                              L_index_1,
                              L_index_2,
                              L_bucket_type) = FALSE then
         return FALSE;
      end if;

      if L_index_1 is not null and L_index_2 is not null then
         if POPULATE_BUCKET_1(O_error_message,
                              L_bucket_1_total,
                              I_inv_status,
                              L_index_1,
                              L_index_2) = FALSE then
            return FALSE;
         end if;

         if L_bucket_1_total >= LP_qty_to_distribute then
            if PRORATE_BUCKET_1(O_error_message,
                                L_bucket_1_total,
                                L_index_1,
                                L_index_2) = FALSE then
               return FALSE;
            end if;
            return TRUE;
         else
            if DRAW_DOWN_BUCKET_1(O_error_message,
                                  L_bucket_1_total,
                                  L_index_1,
                                  L_index_2) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      --
      if L_bucket_type = 'UNPROTECTED' then
         L_bucket_type := 'PROTECTED';
      else
         exit;
      end if;
   end loop;
   --
   if DRAW_DOWN_WH(O_error_message,
                   null) = FALSE then
      return FALSE;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'DISTRIBUTION_SQL.OUTBOUND_DIST_RULE_2',
                                        to_char(SQLCODE));
      return FALSE;

END OUTBOUND_DIST_RULE_2;
--------------------------------------------------------------------------------

FUNCTION OUTBOUND_DIST_RULE_3(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

i                  INTEGER;
L_index_1          INTEGER;
L_index_2          INTEGER;
L_bucket_type      VARCHAR2(50);
L_bucket_2_total   NUMBER := 0;

cursor C_WH is
   select w.wh,
          w.protected_ind
     from wh w,
          item_loc il
    where il.item = LP_item
      and il.loc = w.wh
      --
      and w.wh != w.physical_wh
      and w.physical_wh = LP_physical_wh
 order by protected_ind;

BEGIN

   /*
   *  outbound distribution rule 3:
   *                                                pull      pull   pull
   *     status   protected   primary   lowest id   till      till   till
   *                ind         vwh        vwh    fulfilled   NI=0   SOH=0
   *       -         N           -          -        -          -      X
   *       -         Y           -          -        -          -      X
   *      !=D        -           X          -        X          -      -
   *      !=D        -           -          X        X          -      -
   *
   *  bucket_2 holds the SOH postiion
   */

   --

   i := 0;
   for c_rec in C_WH
   loop
      i := i + 1;
      LP_dist_tab(i).wh := c_rec.wh;
      LP_dist_tab(i).protected_ind := c_rec.protected_ind;
      LP_dist_tab(i).bucket_1 := 0;
      LP_dist_tab(i).dist_qty := 0;
   end loop;

   if LP_dist_tab.count = 0 then
      return TRUE;
   end if;

   if SET_DIST_TAB_INDEXES(O_error_message) = FALSE then
      return FALSE;
   end if;

   --

   L_bucket_type := 'UNPROTECTED';
   loop
      if GET_DIST_TAB_INDEXES(O_error_message,
                              L_index_1,
                              L_index_2,
                              L_bucket_type) = FALSE then
         return FALSE;
      end if;

      if L_index_1 is not null and L_index_2 is not null then
         if POPULATE_BUCKET_2(O_error_message,
                              L_bucket_2_total,
                              L_index_1,
                              L_index_2) = FALSE then
            return FALSE;
         end if;

         if L_bucket_2_total >= LP_qty_to_distribute then
            if PRORATE_BUCKET_2(O_error_message,
                                L_bucket_2_total,
                                L_index_1,
                                L_index_2) = FALSE then
               return FALSE;
            end if;
            return TRUE;
         else
            if DRAW_DOWN_BUCKET_2(O_error_message,
                                  L_bucket_2_total,
                                  L_index_1,
                                  L_index_2) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      --
      if L_bucket_type = 'UNPROTECTED' then
         L_bucket_type := 'PROTECTED';
      else
         exit;
      end if;
   end loop;
   --
   if DRAW_DOWN_WH(O_error_message,
                   null) = FALSE then
      return FALSE;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'DISTRIBUTION_SQL.OUTBOUND_DIST_RULE_3',
                                        to_char(SQLCODE));
      return FALSE;

END OUTBOUND_DIST_RULE_3;
--------------------------------------------------------------------------------

FUNCTION POPULATE_BUCKET_1(O_error_message IN OUT VARCHAR2,
                           O_bucket_total  IN OUT NUMBER,
                           I_inv_status    IN     INV_ADJ.INV_STATUS%TYPE,
                           I_index_1       IN     INTEGER,
                           I_index_2       IN     INTEGER)
RETURN BOOLEAN IS

L_total     NUMBER := 0;
L_found     BOOLEAN;

BEGIN

   --

   -- bucket_1 of the records of the global LP_dist_tab table, between the
   -- indexes, is set equal to the the NI or the UI based on the
   -- I_inv_status.  The bucket_1 total is returned.

   if I_inv_status is null then
      for i in I_index_1 .. I_index_2
      loop
         if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(O_error_message,
                                                       LP_dist_tab(i).bucket_1,
                                                       LP_item,
                                                       LP_dist_tab(i).wh,
                                                       'W') = FALSE then
            return FALSE;
         end if;
         --
         if LP_dist_tab(i).bucket_1 > 0 then
            L_total := L_total + LP_dist_tab(i).bucket_1;
         end if;
      end loop;
   else
      for i in I_index_1 .. I_index_2
      loop
         if INVADJ_SQL.GET_UNAVAILABLE(LP_item,
                                       'W',
                                       LP_dist_tab(i).wh,
                                       LP_dist_tab(i).bucket_1,
                                       O_error_message,
                                       L_found) = FALSE then
            return FALSE;
         end if;
         --
         if LP_dist_tab(i).bucket_1 > 0 then
            L_total := L_total + LP_dist_tab(i).bucket_1;
         end if;
         --
      end loop;
   end if;

   O_bucket_total := L_total;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'DISTRIBUTION_SQL.POPULATE_BUCKET_1',
                                          to_char(SQLCODE));
      return FALSE;

END POPULATE_BUCKET_1;
--------------------------------------------------------------------------------

FUNCTION PRORATE_BUCKET_1(O_error_message IN OUT VARCHAR2,
                          I_bucket_total  IN     NUMBER,
                          I_index_1       IN     INTEGER,
                          I_index_2       IN     INTEGER)
RETURN BOOLEAN IS

L_remainder   NUMBER := 0;
L_dist_qty    NUMBER := 0;

BEGIN

   --

   -- the quantity to distribute is prorated among the candidate warehouses
   -- of the global LP_dist_tab table, between the indexes passed in.  The
   -- proration is based on bucket_1 quantity.

   if LP_integer_rounding then
      for i in I_index_1 .. I_index_2
      loop
         if LP_dist_tab(i).bucket_1 > 0 then
            L_dist_qty := L_remainder +
            (LP_qty_to_distribute * LP_dist_tab(i).bucket_1 / I_bucket_total);
            --
            LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
            L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
         end if;
      end loop;
   else
      for i in I_index_1 .. I_index_2
      loop
         if LP_dist_tab(i).bucket_1 > 0 then
            LP_dist_tab(i).dist_qty := LP_qty_to_distribute *
            LP_dist_tab(i).bucket_1 / I_bucket_total;
         end if;
      end loop;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.PRORATE_BUCKET_1',
                                            to_char(SQLCODE));
      return FALSE;

END PRORATE_BUCKET_1;
--------------------------------------------------------------------------------

FUNCTION DRAW_DOWN_BUCKET_1(O_error_message IN OUT VARCHAR2,
                            I_bucket_total  IN     NUMBER,
                            I_index_1       IN     INTEGER,
                            I_index_2       IN     INTEGER)
RETURN BOOLEAN IS

BEGIN

   --

   -- This function will pull whatever stock is available in bucket_1.

   for i in I_index_1 .. I_index_2
   loop
      if LP_dist_tab(i).bucket_1 > 0 then
         LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1;
      end if;
   end loop;

   LP_qty_to_distribute := LP_qty_to_distribute - I_bucket_total;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'DISTRIBUTION_SQL.DRAW_DOWN_BUCKET_1',
                                          to_char(SQLCODE));
      return FALSE;

END DRAW_DOWN_BUCKET_1;
--------------------------------------------------------------------------------

FUNCTION POPULATE_BUCKET_2(O_error_message IN OUT VARCHAR2,
                           O_bucket_total  IN OUT NUMBER,
                           I_index_1       IN     INTEGER,
                           I_index_2       IN     INTEGER)
RETURN BOOLEAN IS

L_total     NUMBER := 0;

L_bucket_2  NUMBER;

cursor C_SNAPSHOT_ON_HAND_QTY(cv_item     STAKE_SKU_LOC.ITEM%TYPE,
                              cv_location STAKE_SKU_LOC.LOCATION%TYPE) IS
   select NVL(ssl.snapshot_on_hand_qty, 0)
     from stake_sku_loc ssl
    where ssl.item =  cv_item
      and ssl.location = cv_location
      and ssl.cycle_count = LP_cycle_count;

BEGIN

   --

   -- bucket_2 of the records of the global LP_dist_tab table, between the
   -- indexes, is set equal to the SOH position of the item at the virtual
   -- warehouse.  The bucket_2 total is returned.

   for i in I_index_1 .. I_index_2
   loop
      if LP_CMI = 'STKREC' THEN
      -- -------------------------------------------------------------------
      -- For Stock Counts use the SOH at the time the stock count was taken
      -- -------------------------------------------------------------------
         open C_SNAPSHOT_ON_HAND_QTY(LP_item, LP_dist_tab(i).wh);
         fetch C_SNAPSHOT_ON_HAND_QTY INTO L_bucket_2;
         close C_SNAPSHOT_ON_HAND_QTY;
         --
         if L_bucket_2 is NULL then
            L_bucket_2 := 0;
         end if;
         --
         LP_dist_tab(i).bucket_2 := L_bucket_2;
      else
         if ITEMLOC_QUANTITY_SQL.GET_STOCK_ON_HAND(O_error_message,
                                                   LP_dist_tab(i).bucket_2,
                                                   LP_item,
                                                   LP_dist_tab(i).wh,
                                                   'W') = FALSE then
            return FALSE;
         end if;
      end if;
      --
      if LP_dist_tab(i).bucket_2 > 0 then
         L_total := L_total + LP_dist_tab(i).bucket_2;
      end if;
   end loop;

   O_bucket_total := L_total;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'DISTRIBUTION_SQL.POPULATE_BUCKET_2',
                                          to_char(SQLCODE));
      return FALSE;

END POPULATE_BUCKET_2;
--------------------------------------------------------------------------------

FUNCTION PRORATE_BUCKET_2(O_error_message IN OUT VARCHAR2,
                          I_bucket_total  IN     NUMBER,
                          I_index_1       IN     INTEGER,
                          I_index_2       IN     INTEGER)
RETURN BOOLEAN IS

L_remainder           NUMBER := 0;
L_qty_1               NUMBER := 0;
L_qty_2               NUMBER := 0;
L_bucket_2_adjusted   NUMBER := 0;

BEGIN

   --

   -- the quantity to distribute is prorated among the candidate warehouses
   -- of the global LP_dist_tab table, between the indexes passed in.  The
   -- proration is based on the difference between the bucket_2 and bucket_1
   -- quantities.  This is done because this function will only be called
   -- after bucket_1 quantities are completely drawn down.

   if LP_integer_rounding then
      for i in I_index_1 .. I_index_2
      loop
         if LP_dist_tab(i).bucket_2 > 0 then
            if LP_dist_tab(i).bucket_1 > 0 then
               L_bucket_2_adjusted := LP_dist_tab(i).bucket_2 -
               LP_dist_tab(i).bucket_1;
            else
               L_bucket_2_adjusted := LP_dist_tab(i).bucket_2;
            end if;
            --
            if L_bucket_2_adjusted > 0 then
               L_qty_1 := L_remainder +
               (LP_qty_to_distribute * L_bucket_2_adjusted / I_bucket_total);
               L_qty_2 := ROUND(L_qty_1);
               L_remainder := L_qty_1 - L_qty_2;
               --
               LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty + L_qty_2;
            end if;
         end if;
      end loop;
   else
      for i in I_index_1 .. I_index_2
      loop
         if LP_dist_tab(i).bucket_2 > 0 then
            if LP_dist_tab(i).bucket_1 > 0 then
               L_bucket_2_adjusted := LP_dist_tab(i).bucket_2 -
               LP_dist_tab(i).bucket_1;
            else
               L_bucket_2_adjusted := LP_dist_tab(i).bucket_2;
            end if;
            --
            if L_bucket_2_adjusted > 0 then
               LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty +
               (LP_qty_to_distribute * L_bucket_2_adjusted / I_bucket_total);
            end if;
         end if;
      end loop;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.PRORATE_BUCKET_2',
                                            to_char(SQLCODE));
      return FALSE;

END PRORATE_BUCKET_2;
--------------------------------------------------------------------------------

FUNCTION DRAW_DOWN_BUCKET_2(O_error_message IN OUT VARCHAR2,
                            I_bucket_total  IN     NUMBER,
                            I_index_1       IN     INTEGER,
                            I_index_2       IN     INTEGER)
RETURN BOOLEAN IS

BEGIN

   --

   -- This function will pull whatever stock is still available to pull.
   -- It is called only after drawing down bucket_1 quantities (NI stock),
   -- and will only pull the difference between the bucket_2 and bucket_1
   -- quantities, which represents the remaining stock.

   for i in I_index_1 .. I_index_2
   loop
      if LP_dist_tab(i).bucket_2 > 0 then
         if LP_dist_tab(i).bucket_1 > 0 then
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty +
            (LP_dist_tab(i).bucket_2 - LP_dist_tab(i).bucket_1);
         else
            LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty +
            LP_dist_tab(i).bucket_2;
         end if;
      end if;
   end loop;

   LP_qty_to_distribute := LP_qty_to_distribute - I_bucket_total;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'DISTRIBUTION_SQL.DRAW_DOWN_BUCKET_2',
                                          to_char(SQLCODE));
      return FALSE;

END DRAW_DOWN_BUCKET_2;
--------------------------------------------------------------------------------

FUNCTION DRAW_DOWN_WH(O_error_message IN OUT VARCHAR2,
                      I_wh            IN     NUMBER)
RETURN BOOLEAN IS

L_wh   WH.WH%TYPE := null;

cursor C_PRIMARY_VWH is
   select w.primary_vwh
     from wh w,
          item_loc il
    where il.status != 'D'
      and il.item = LP_item
      and il.loc = w.primary_vwh
      and w.wh = LP_physical_wh;

cursor C_LOWEST_ID_VWH is
   select min(w.wh)
     from wh w,
          item_loc il
    where il.status != 'D'
      and il.item = LP_item
      and il.loc = w.wh
      and w.wh != w.physical_wh
      and w.physical_wh = LP_physical_wh;

BEGIN

   /*
   *  The remaining LP_qty_to_distribute is pulled from the I_wh passed into
   *  this function.  If however I_wh is null, then the following rule is
   *  used to choose the warehouse from which to pull the remaining stock.
   *
   *                                     pull
   *     status   primary   lowest id    till
   *                vwh       vwh      fulfilled
   *      !=D        X         -          X
   *      !=D        -         X          X
   *
   */

   --

   if I_wh is null then
      open C_PRIMARY_VWH;
      fetch C_PRIMARY_VWH into L_wh;
      close C_PRIMARY_VWH;
      --
      if L_wh is null then
         open C_LOWEST_ID_VWH;
         fetch C_LOWEST_ID_VWH into L_wh;
         close C_LOWEST_ID_VWH;
      end if;
      --
      if L_wh is null then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DIST_WH',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   else
      L_wh := I_wh;
   end if;

   for i in LP_dist_tab.first .. LP_dist_tab.last
   loop
      if LP_dist_tab(i).wh = L_wh then
         LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty +
         LP_qty_to_distribute;
         exit;
      end if;
   end loop;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           'DISTRIBUTION_SQL.DRAW_DOWN_WH',
                                           to_char(SQLCODE));
      return FALSE;

END DRAW_DOWN_WH;
--------------------------------------------------------------------------------

FUNCTION INBOUND_DIST_RULE_1(O_error_message IN OUT VARCHAR2,
                             I_inv_status    IN     INV_STATUS_TYPES.INV_STATUS%TYPE,
                             I_order_no      IN     ORDHEAD.ORDER_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   i             INTEGER;
   L_index_1     INTEGER;
   L_index_2     INTEGER;
   L_status      ITEM_LOC.STATUS%TYPE;

   L_total       NUMBER := 0;
   L_remainder   NUMBER := 0;
   L_dist_qty    NUMBER;
   L_bucket_1    NUMBER;
   L_dist_wh     WH.WH%TYPE;

   L_dist_tab    INTERNAL_DIST_TABLE_TYPE;
   L_found       BOOLEAN;

   cursor C_WH is
      select w.wh,
             il.status,
             w.restricted_ind
        from wh w,
             item_loc il
       where il.status != 'D'
         and il.item = LP_item
         and il.loc = w.wh
         and w.finisher_ind = 'N'
         and w.wh != w.physical_wh
         and w.physical_wh = LP_physical_wh
         and (LP_CMI != 'ORDRCV'
             OR (LP_CMI = 'ORDRCV'                                           -- receiving at unexpected warehouse
                  and not exists (select 'x'
                                    from ordloc ol, wh
                                   where ol.order_no = I_order_no
                                     and ol.location = wh.wh
                                     and wh.physical_wh = LP_physical_wh)))
     UNION
     select w.wh,
             il.status,
             w.restricted_ind
        from wh w,
             item_loc il,
             ordloc ol
       where il.status != 'D'
         and il.item = LP_item
         and il.loc = ol.location
         and ol.order_no = I_order_no
         and w.wh = ol.location
         and w.finisher_ind = 'N' 
         and w.wh != w.physical_wh
         and w.physical_wh = LP_physical_wh
         and LP_CMI = 'ORDRCV'
    order by status,
             restricted_ind,
             wh;
           
   cursor C_SNAPSHOT_ON_HAND_QTY(cv_item     STAKE_SKU_LOC.ITEM%TYPE,
                                 cv_location STAKE_SKU_LOC.LOCATION%TYPE) is
      select NVL(ssl.snapshot_on_hand_qty, 0)
        from stake_sku_loc ssl
       where ssl.item =  cv_item
         and ssl.location = cv_location
         and ssl.cycle_count = LP_cycle_count;


BEGIN

   /*
   *  inbound distribution rule 1:
   *     status   bucket_1   restriced_ind   logic      based on
   *       A        > 0            N         Prorate    bucket_1
   *       A        > 0            Y         Prorate    bucket_1
   *       A       <= 0            N          Even         -
   *       A       <= 0            Y          Even         -
   *       C        > 0            N         Prorate    bucket_1
   *       C        > 0            Y         Prorate    bucket_1
   *       C       <= 0            N          Even         -
   *       C       <= 0            Y          Even         -
   *       I        > 0            N         Prorate    bucket_1
   *       I        > 0            Y         Prorate    bucket_1
   *       I       <= 0            N          Even         -
   *       I       <= 0            Y          Even         -
   *
   *  bucket_1 holds the SOH postiion if the I_inv_status is null
   *  bucket_1 holds the UI postiion if the I_inv_status is not null
   */

   --

   i := 0;
   for c_rec in C_WH
   loop
      i := i + 1;
      LP_dist_tab(i).wh := c_rec.wh;
      LP_dist_tab(i).status := c_rec.status;
      LP_dist_tab(i).restricted_ind := c_rec.restricted_ind;
      --
      LP_dist_tab(i).dist_qty := 0;
   end loop;

   if LP_dist_tab.count = 0 then
      return TRUE;
   end if;

   --

   -- the bucket_1 column of the LP_dist_tab table is populated for records of
   -- a particular item_loc status.  GET_INBOUND_VWH is called for this set of
   -- records and will return a local L_dist_tab table with records that
   -- meet the inbound distribution rule 1.

   L_status := LP_dist_tab(1).status;
   L_index_1 := 1;
   for i in LP_dist_tab.first .. LP_dist_tab.last
   loop
      if LP_dist_tab(i).status != L_status then
         exit;
      else
         L_index_2 := i;
      end if;
      --
      if I_inv_status is null then
         if LP_CMI = 'STKREC' then
         -- -------------------------------------------------------------------
         -- For Stock Counts use the SOH at the time the stock count was taken
         -- -------------------------------------------------------------------
            L_bucket_1 := NULL;
            open C_SNAPSHOT_ON_HAND_QTY(LP_item, LP_dist_tab(i).wh);
            fetch C_SNAPSHOT_ON_HAND_QTY into L_bucket_1;
            if L_bucket_1 is NULL then
               L_bucket_1 := 0;
            end if;
            LP_dist_tab(i).bucket_1 := L_bucket_1;
            close C_SNAPSHOT_ON_HAND_QTY;
         else
            if ITEMLOC_QUANTITY_SQL.GET_STOCK_ON_HAND(O_error_message,
                                                      LP_dist_tab(i).bucket_1,
                                                      LP_item,
                                                      LP_dist_tab(i).wh,
                                                      'W') = FALSE then
               return FALSE;
            end if;
         end if;
      else
         if INVADJ_SQL.GET_UNAVAILABLE(LP_item,
                                       'W',
                                       LP_dist_tab(i).wh,
                                       LP_dist_tab(i).bucket_1,
                                       O_error_message,
                                       L_found) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop;

   --

   if GET_INBOUND_VWH(O_error_message,
                      L_dist_tab,
                      L_index_1,
                      L_index_2) = FALSE then
      return FALSE;
   end if;

   if L_dist_tab.count = 0 then
      return TRUE;
   end if;

   --

   -- the local L_dist_tab table is a subset of records in the global
   -- LP_dist_tab table which meet one of the rules of the 'inbound
   -- distribution rule 1'.  The distribution quantities are now calculated
   -- based on either the bucket_1 total or an even distribution. The global
   -- LP_dist_tab table is then set equal to the local L_dist_tab_table.

   for i in L_dist_tab.first .. L_dist_tab.last
   loop
      L_total := L_total + L_dist_tab(i).bucket_1;
   end loop;

   if L_total > 0 then
      for i in L_dist_tab.first .. L_dist_tab.last
      loop
         if LP_integer_rounding then
            L_dist_qty := L_remainder +
            (LP_qty_to_distribute * L_dist_tab(i).bucket_1 / L_total);
            --
            L_dist_tab(i).dist_qty := ROUND(L_dist_qty);
            L_remainder := L_dist_qty - L_dist_tab(i).dist_qty;
         else
            L_dist_tab(i).dist_qty := LP_qty_to_distribute *
            L_dist_tab(i).bucket_1 / L_total;
         end if;
      end loop;
   else
      for i in L_dist_tab.first .. L_dist_tab.last
      loop
         if LP_integer_rounding then
            L_dist_qty := L_remainder +
            (LP_qty_to_distribute / L_dist_tab.count);
            --
            L_dist_tab(i).dist_qty := ROUND(L_dist_qty);
            L_remainder := L_dist_qty - L_dist_tab(i).dist_qty;
         else
            L_dist_tab(i).dist_qty := LP_qty_to_distribute / L_dist_tab.count;
         end if;
      end loop;
   end if;

   LP_dist_tab := L_dist_tab;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         'DISTRIBUTION_SQL.INBOUND_DIST_RULE_1',
                                         to_char(SQLCODE));
      return FALSE;

END INBOUND_DIST_RULE_1;
--------------------------------------------------------------------------------

FUNCTION GET_INBOUND_VWH(O_error_message IN OUT VARCHAR2,
                         O_dist_tab      IN OUT INTERNAL_DIST_TABLE_TYPE,
                         I_index_1       IN     INTEGER,
                         I_index_2       IN     INTEGER)
RETURN BOOLEAN IS

i                  INTEGER;
j                  INTEGER;
L_restricted_ind   WH.RESTRICTED_IND%TYPE;

BEGIN

   /*
   *  records of the LP_dist_tab table between the indexes I_index_1 and
   *  I_index_2, that meet any of the following rules, are returned
   *     bucket_1 position   restricted_ind
   *           > 0                N
   *           > 0                Y
   *          <= 0                N
   *          <= 0                Y
   *
   *  the indexes indicate the first and last record of the LP_dist_tab table,
   *  with a particular item_loc status
   */

   --

   -- bucket_1 position > 0
   i := I_index_1;
   j := 0;
   L_restricted_ind := 'N';
   loop
      if LP_dist_tab(i).bucket_1 > 0 then
         if LP_dist_tab(i).restricted_ind = L_restricted_ind then
            j := j + 1;
            O_dist_tab(j) := LP_dist_tab(i);
         end if;
      end if;
      --
      if i = I_index_2 then
         if L_restricted_ind = 'N' and j = 0 then
            L_restricted_ind := 'Y';
            i := I_index_1;
         else
            exit;
         end if;
      else
         i := i + 1;
      end if;
   end loop;

   if j != 0 then
      return TRUE;
   end if;

   -- bucket_1 position <= 0
   i := I_index_1;
   j := 0;
   L_restricted_ind := 'N';
   loop
      if LP_dist_tab(i).bucket_1 <= 0 then
         if LP_dist_tab(i).restricted_ind = L_restricted_ind then
            j := j + 1;
            O_dist_tab(j) := LP_dist_tab(i);
         end if;
         --
         if i = I_index_2 then
            if L_restricted_ind = 'N' and j = 0 then
               L_restricted_ind := 'Y';
               i := I_index_1;
            else
               exit;
            end if;
         else
            i := i + 1;
         end if;
      end if;
   end loop;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.GET_INBOUND_VWH',
                                            to_char(SQLCODE));
      return FALSE;

END GET_INBOUND_VWH;
--------------------------------------------------------------------------------

FUNCTION CREATE_ITEM_LOC_REL(O_error_message  IN OUT VARCHAR2,
                             O_wh             IN OUT WH.WH%TYPE,
                             I_il_create_rule IN     VARCHAR2,
                             I_order_no       IN     ORDHEAD.ORDER_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_wh         WH.WH%TYPE := null;
   L_ind_type   VARCHAR2(1);

   cursor C_PRIMARY_VWH(I_ind_type  VARCHAR2,
                        I_ind_value VARCHAR2) is
      select w1.primary_vwh 
        from wh w1, wh w2 
       where not exists (select 'x' 
                           from item_loc il 
                          where il.loc = w1.primary_vwh 
                            and il.item = LP_item) 
         and w2.protected_ind = decode(I_ind_type,'P',I_ind_value,w2.protected_ind) 
         and w2.restricted_ind = decode(I_ind_type,'R',I_ind_value,w2.restricted_ind) 
         and w1.wh = LP_physical_wh 
         and w1.primary_vwh = w2.wh 
         and LP_CMI != 'ORDRCV'
       UNION
       select vwh.wh
       from (
       select w.wh, row_number( ) over (partition by w.wh order by w.wh asc) rk
        from wh w,
             ordloc ol
       where not exists (select 'x'
                           from item_loc il
                          where il.loc = w.wh
                            and il.item = LP_item)
         and w.physical_wh = LP_physical_wh
         and ol.order_no = I_order_no
         and w.wh = ol.location
         and LP_CMI = 'ORDRCV') vwh  where vwh.rk=1;
         
   cursor C_LOWEST_ID_VWH(I_ind_type  VARCHAR2,
                          I_ind_value VARCHAR2) is
      select min(w.wh)
        from wh w
       where not exists (select 'x'
                           from item_loc il
                          where il.loc = w.wh
                            and il.item = LP_item)
         and w.protected_ind = decode(I_ind_type,'P',I_ind_value,w.protected_ind)
         and w.restricted_ind = decode(I_ind_type,'R',I_ind_value,w.restricted_ind)
         and w.wh != w.physical_wh
         and w.physical_wh = LP_physical_wh
         and w.finisher_ind = 'N';

BEGIN

   /*
   *  inbound distribution item-loc creation rule:
   *    protected_ind    primary_vwh   lowest_id_vwh
   *         N              X              -
   *         N              -              X
   *         Y              X              -
   *         Y              -              X
   *
   *  outbound distribution item-loc creation rule:
   *    restricted_ind   primary_vwh   lowest_id_vwh
   *         N              X              -
   *         N              -              X
   *         Y              X              -
   *         Y              -              X
   */

   --

   if I_il_create_rule = 'OUTBOUND' then
      L_ind_type := 'P';   -- based on protected_ind
   else
      L_ind_type := 'R';   -- based on restricted_ind
   end if;

   open C_PRIMARY_VWH(L_ind_type,'N');
   fetch C_PRIMARY_VWH into L_wh;
   close C_PRIMARY_VWH;
   --
   if L_wh is null then
      open C_LOWEST_ID_VWH(L_ind_type,'N');
      fetch C_LOWEST_ID_VWH into L_wh;
      close C_LOWEST_ID_VWH;
      --
      if L_wh is null then
         open C_PRIMARY_VWH(L_ind_type,'Y');
         fetch C_PRIMARY_VWH into L_wh;
         close C_PRIMARY_VWH;
         --
         if L_wh is null then
            open C_LOWEST_ID_VWH(L_ind_type,'Y');
            fetch C_LOWEST_ID_VWH into L_wh;
            close C_LOWEST_ID_VWH;
            --
            if L_wh is null then
               return TRUE;
            end if;
         end if;
      end if;
   end if;

   --

   if NEW_ITEM_LOC(O_error_message,
                   LP_item,
                   L_wh,
                   NULL, -- ITEM_PARENT
                   NULL, -- ITEM_GRANDPARENT
                   'W',  -- LOC_TYPE,
                   NULL, -- SHORT_DESC
                   NULL, -- DEPT
                   NULL, -- CLASS
                   NULL, -- SUBCLASS
                   NULL, -- ITEM_LEVEL,
                   NULL, -- TRAN_LEVEL,
                   NULL, -- ITEM_STATUS
                   NULL, -- WASTE TYPE
                   NULL, -- DAILY WASTE PCT
                   NULL, -- SELLABLE_IND
                   NULL, -- ORDERABLE_IND
                   NULL, -- PACK_IND,
                   NULL, -- PACK_TYPE
                   NULL, -- UNIT_COST_LOC
                   NULL, -- UNIT_RETAIL_LOC
                   NULL, -- SELLING_RETAIL_LOC
                   NULL, -- SELLING_UOM
                   NULL, -- ITEM_LOC_STATUS
                   NULL, -- TAXABLE_IND
                   NULL, -- TI
                   NULL, -- HI
                   NULL, -- STORE_ORD_MULT
                   NULL, -- MEAS_OF_EACH
                   NULL, -- MEAS_OF_PRICE
                   NULL, -- UOM_OF_PRICE
                   NULL, -- PRIMARY_VARIANT
                   NULL, -- PRIMARY_SUPP
                   NULL, -- PRIMARY_CNTRY
                   NULL, -- LOCAL_ITEM_DESC
                   NULL, -- LOCAL_SHORT_DESC
                   NULL, -- PRIMARY_COST_PACK
                   NULL, -- RECEIVE_AS_TYPE
                   NULL, -- DATE,
                   NULL) -- DEFAULT_TO_CHILDREN
                   = FALSE then
      return FALSE;
   end if;

   O_wh := L_wh;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'DISTRIBUTION_SQL.CREATE_ITEM_LOC_REL',
                                        to_char(SQLCODE));
      return FALSE;

END CREATE_ITEM_LOC_REL;
--------------------------------------------------------------------------------

FUNCTION SET_DIST_TAB_INDEXES(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   --
   -- set the global indexes for the first and last record of the LP_dist_tab
   -- table for protected and unprotected virtual warehouses

   LP_protected_tab_index_1 := null;
   LP_protected_tab_index_2 := null;
   LP_unprotected_tab_index_1 := null;
   LP_unprotected_tab_index_2 := null;

   if LP_dist_tab(1).protected_ind = 'Y' then
      LP_protected_tab_index_1 := 1;
      --
      for i in 1 .. LP_dist_tab.last
      loop
         if LP_dist_tab(i).protected_ind = 'N' then
            LP_unprotected_tab_index_1 := i;
            exit;
         end if;
      end loop;
   else
      LP_unprotected_tab_index_1 := 1;
      --
      for i in 1 .. LP_dist_tab.last
      loop
         if LP_dist_tab(i).protected_ind = 'Y' then
            LP_protected_tab_index_1 := i;
            exit;
         end if;
      end loop;
   end if;

   if LP_protected_tab_index_1 = 1 then
      if LP_unprotected_tab_index_1 is null then
         LP_protected_tab_index_2 := LP_dist_tab.last;
      else
         LP_protected_tab_index_2 := LP_unprotected_tab_index_1 - 1;
         LP_unprotected_tab_index_2 := LP_dist_tab.last;
      end if;
   else
      if LP_protected_tab_index_1 is null then
         LP_unprotected_tab_index_2 := LP_dist_tab.last;
      else
         LP_unprotected_tab_index_2 := LP_protected_tab_index_1 - 1;
         LP_protected_tab_index_2 := LP_dist_tab.last;
      end if;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'DISTRIBUTION_SQL.SET_DIST_TAB_INDEXES',
                                        to_char(SQLCODE));
      return FALSE;

END SET_DIST_TAB_INDEXES;
--------------------------------------------------------------------------------

FUNCTION GET_DIST_TAB_INDEXES(O_error_message IN OUT VARCHAR2,
                              O_index_1       IN OUT INTEGER,
                              O_index_2       IN OUT INTEGER,
                              I_bucket_type   IN     VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   --
   -- return the indexes of the first and last record of the LP_dist_tab
   -- table for the requested bucket type

   if I_bucket_type = 'UNPROTECTED' then
      O_index_1 := LP_unprotected_tab_index_1;
      O_index_2 := LP_unprotected_tab_index_2;
   else
      O_index_1 := LP_protected_tab_index_1;
      O_index_2 := LP_protected_tab_index_2;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'DISTRIBUTION_SQL.GET_DIST_TAB_INDEXES',
                                        to_char(SQLCODE));
      return FALSE;

END GET_DIST_TAB_INDEXES;
--------------------------------------------------------------------------------

FUNCTION SET_ITEM_ATTRIBUTES(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

L_uom_class   UOM_CLASS.UOM_CLASS%TYPE;

cursor C_UOM_CLASS is
   select uc.uom_class
     from uom_class uc,
          item_master im
    where im.item = LP_item
      and im.standard_uom = uc.uom;

BEGIN

   --
   -- set the golbal item attribute LP_integer_rounding

   open C_UOM_CLASS;
   fetch C_UOM_CLASS into L_uom_class;
   close C_UOM_CLASS;

   if L_uom_class = 'PACK' or L_uom_class = 'QTY' then
      LP_integer_rounding := TRUE;
   else
      LP_integer_rounding := FALSE;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         'DISTRIBUTION_SQL.SET_ITEM_ATTRIBUTES',
                                         to_char(SQLCODE));
      return FALSE;

END SET_ITEM_ATTRIBUTES;
--------------------------------------------------------------------------------

FUNCTION VALIDATE_INPUTS(O_error_message IN OUT VARCHAR2,
                         I_item          IN     ITEM_LOC.ITEM%TYPE,
                         I_loc           IN     ITEM_LOC.LOC%TYPE,
                         I_qty           IN     NUMBER,
                         I_CMI           IN     VARCHAR2,
                         I_shipment      IN     SHIPITEM_INV_FLOW.SHIPMENT%TYPE)
RETURN BOOLEAN IS

BEGIN

   --

   if I_item is null then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_item',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;

   if I_loc is null then
      -- if not TRANSFER_IN or TRANSFER_IN_ADJ
      if not (I_CMI = 'TRANSFER' and I_shipment is not null) then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_loc',
                                               'NULL','NOT NULL');
         return FALSE;
      end if;
   end if;

   if I_qty is null then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_qty',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;

   if I_CMI not in ('RTV','INVADJ','TRANSFER','ALLOC','STKREC','ORDRCV','SHIPMENT','ORDRCV_DIST') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_CMI',
                 nvl(I_CMI,'NULL'),'RTV | INVADJ | TRANSFER | ALLOC | STKREC | ORDRCV | SHIPMENT | ORDRCV_DIST');
      return FALSE;
   end if;

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.VALIDATE_INPUTS',
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_INPUTS;
--------------------------------------------------------------------------------
FUNCTION GET_EG_TSF_DIST_VWH(O_error_message   IN OUT   VARCHAR2,
                             O_dist_vwh        IN OUT   WH.WH%TYPE,
                             I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'DISTRIBUTION_SQL.GET_EG_TSF_DIST_VWH';

   cursor C_GET_VWH is
      select sif.to_loc
        from shipitem_inv_flow sif,
             shipsku s
       where s.distro_no = I_tsf_no
         and s.shipment = sif.shipment;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_GET_VWH;
   fetch C_GET_VWH into O_dist_vwh;
   if C_GET_VWH%NOTFOUND then
      close C_GET_VWH;
      O_error_message := SQL_LIB.CREATE_MSG('NO_TSF_DIST_VWH',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   close C_GET_VWH;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_EG_TSF_DIST_VWH;
----------------------------------------------------------------------------------
FUNCTION LOAD_INV_FLOW_LOC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_inv_flow_array       IN OUT   DISTRIBUTION_SQL.INV_FLOW_ARRAY,
                           I_to_from_ind          IN       VARCHAR2,
                           I_phy_loc              IN       ITEM_LOC.LOC%TYPE,
                           I_loc_type             IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_other_loc            IN       ITEM_LOC.LOC%TYPE,
                           I_other_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                           I_tsf_qty              IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_inv_status           IN       SHIPSKU.INV_STATUS%TYPE,
                           I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'DISTRIBUTION_SQL.LOAD_INV_FLOW_LOC';
   i                 BINARY_INTEGER := 1;
   L_loc             ITEM_LOC.LOC%TYPE := NULL;
   L_pack_ind        ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind    ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind   ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type       ITEM_MASTER.PACK_TYPE%TYPE;
   L_ranged_ind      ITEM_LOC.RANGED_IND%TYPE := 'N';

   L_exists BOOLEAN := FALSE;
   /******************************************************************************
    *
    *  For EG transfers, given a physical locations, load an array of all
    *  possible virtuals.  The array of virtuals will be used to create
    *  inventory flows.  The inventory flows are where the actual movement
    *  of stock takes place.
    *
    *  Physical location is a store.
    *     Call NEW_ITEM_LOC to ensure the relationship exists.
    *     Add the store to the output array (FINISHED).
    *
    *  Physical location is a wh.
    *
    *     Load all virtual wh in the physical that stock the item being
    *     transferred into the output array. Take into account of WH's stock
    *     position for a 'from' location. Follow the following precedence rule:
    *        If the other loc is a store:
    *        -- match org_unit_id+tsf_entity_id+channel_id
    *        -- match org_unit_id+channel_id
    *        -- match tsf_entity_id_id+channel_id
    *        -- match channel_id
    *        -- match channel_type
    *        If the other loc is a warehouse:
    *        -- match org_unit_id+tsf_entity_id+channel_id
    *        -- match org_unit_id+channel_id
    *        -- match tsf_entity_id_id+channel_id
    *        -- match channel_id
    *        -- match channel_type
    *
    *     If the array contains records (FINISHED)
    *
    *     If the array does not contain records, an item loc relationship
    *     does not exist between the item and any of the virtuals in the
    *     passed in physical wh, we need to create a relationship between
    *     the item and one of the virtuals (in a single channel environment
    *     the wh itself will always be returned by these cursor):
    *
    *       if the passed in loc is a phy_wh and the other loc is a wh create
    *          the relationship for the first vwh found in this order:
    *         -- primary not protected/restricted*
    *         -- low number not protected/restricted*
    *         -- primary protected/restricted*
    *
    *       if the passed in loc is a phy_wh and the other loc is a store
    *          create the relationship for the first vwh found in this order:
    *             -- org_unit_id+tsf_entity_id+channel_id
    *             -- org_unit_id+channel_id
    *             -- tsf_entity_id+channel_id
    *             -- channel_id match
    *             -- low number channel_type match not protected/restricted*
    *             -- low number channel_type match protected/restricted*
    *             -- primary not protected/restricted*
    *             -- low number not protected/restricted*
    *             -- primary protected/restricted*
    *
    *       * use protected when pulling stock(from loc)
    *       * use restricted when pushing stock(to_loc)
    *
    *     After creating the relationship, load its information into the
    *     output array (FINISHED).
    *
    *****************************************************************************/

   -- All virtual warehouses that match the org_unit_id+tsf_entity_id+channel_id when other_loc is Store
   cursor C_GET_VWH_1_S is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c,
             store s
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and s.store                  = I_other_loc
         and NVL(s.org_unit_id,0)     = NVL(w.org_unit_id,0)
         and s.tsf_entity_id          = w.tsf_entity_id
         and w.channel_id             = c.channel_id
         and w.channel_id             = s.channel_id
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         --a non-customer order is NOT restricted by location's customer_order_loc_ind
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
    order by w.wh;

   -- All virtual warehouses that match the org_unit_id+channel_id when other_loc is Store
   cursor C_GET_VWH_2_S is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c,
             store s
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and s.store                  = I_other_loc
         and NVL(s.org_unit_id,0)     = NVL(w.org_unit_id,0)
         and w.channel_id             = c.channel_id
         and s.channel_id             = w.channel_id
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
    order by w.wh;

   -- All virtual warehouses that match the tsf_entity_id+channel_id when other_loc is Store
   cursor C_GET_VWH_3_S is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c,
             store s
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and s.store                  = I_other_loc
         and s.tsf_entity_id          = w.tsf_entity_id
         and w.channel_id             = c.channel_id
         and w.channel_id             = s.channel_id
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
    order by w.wh;

   -- All virtual warehouses that match the channel_id when other_loc is 'S'tore
   cursor C_GET_VWH_4_S is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c,
             store s
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and s.store                  = I_other_loc
         and w.channel_id             = c.channel_id(+)
         and w.channel_id             = s.channel_id
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
    order by w.wh;

   -- All virtual warehouses that match the channel_type when other_loc is 'S'tore
   cursor C_GET_VWH_5_S is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             w.channel_id channel_id,
             sc.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels sc,
             channels wc,
             store s
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and s.store                  = I_other_loc
         and s.channel_id             = sc.channel_id
         and w.channel_id             = wc.channel_id
         and sc.channel_type          = wc.channel_type
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
    order by w.wh;

     -- All virtual warehouses that match the org_unit_id+tsf_entity_id when other_loc is External Finisher
   cursor C_GET_VWH_1_E is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c,
             partner p
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and p.partner_id             = to_char(I_other_loc)
         and p.partner_type           = 'E'
         and NVL(p.org_unit_id,0)     = NVL(w.org_unit_id,0)
         and p.tsf_entity_id          = w.tsf_entity_id
         and w.channel_id             = c.channel_id(+)
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
    order by w.wh;

   -- All virtual warehouses that match the org_unit_id when other_loc is External Finisher
   cursor C_GET_VWH_2_E is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c,
             partner p
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and p.partner_id             = to_char(I_other_loc)
         and p.partner_type           = 'E'
         and NVL(p.org_unit_id,0)     = NVL(w.org_unit_id,0)
         and w.channel_id             = c.channel_id(+)
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
    order by w.wh;

   -- All virtual warehouses that match the tsf_entity_id when other_loc is External Finisher
   cursor C_GET_VWH_3_E is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c,
             partner p
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and p.partner_id             = to_char(I_other_loc)
         and p.partner_type           = 'E'
         and p.tsf_entity_id          = w.tsf_entity_id
         and w.channel_id             = c.channel_id(+)
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
    order by w.wh;

   -- All virtual warehouses that match the org_unit_id+tsf_entity_id+channel_id when other_loc is Warehouse
   cursor C_GET_VWH_1_W is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and w.channel_id             = c.channel_id
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
         and exists (select '1'
                       from wh w3
                      where w3.physical_wh            = I_other_loc
                        and NVL(w3.org_unit_id,0)     = NVL(w.org_unit_id,0)
                        and w3.tsf_entity_id          = w.tsf_entity_id
                        and w3.channel_id             = w.channel_id
                        and w3.finisher_ind           = 'N'
                        and w3.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w3.customer_order_loc_ind, I_cust_order_loc_ind))
    order by w.wh;

   -- All virtual warehouses that match the org_unit_id+channel_id when other_loc is Warehouse
   cursor C_GET_VWH_2_W is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and w.channel_id             = c.channel_id
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
         and exists (select '1'
                       from wh w3
                      where w3.physical_wh            = I_other_loc
                        and NVL(w3.org_unit_id,0)     = NVL(w.org_unit_id,0)
                        and w3.channel_id             = w.channel_id
                        and w3.finisher_ind           = 'N'
                        and w3.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w3.customer_order_loc_ind, I_cust_order_loc_ind))
    order by w.wh;

   -- All virtual warehouses that match the tsf_entity_id+channel_id when other_loc is Warehouse
   cursor C_GET_VWH_3_W is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and w.channel_id             = c.channel_id
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
         and exists (select '1'
                       from wh w3
                      where w3.physical_wh            = I_other_loc
                        and w3.tsf_entity_id          = w.tsf_entity_id
                        and w3.channel_id             = w.channel_id
                        and w3.finisher_ind           = 'N'
                        and w3.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w3.customer_order_loc_ind, I_cust_order_loc_ind))
    order by w.wh;

   -- All virtual warehouses that match the channel_id when other_loc is Warehouse
   cursor C_GET_VWH_4_W is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and w.channel_id             = c.channel_id(+)
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
         and exists (select '1'
                       from wh w3
                      where w3.physical_wh            = I_other_loc
                        and w3.channel_id             = w.channel_id
                        and w3.finisher_ind           = 'N'
                        and w3.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w3.customer_order_loc_ind, I_cust_order_loc_ind))
    order by w.wh;

   -- All virtual warehouses that match the channel_type when other_loc is Warehouse
   cursor C_GET_VWH_5_W is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             w.channel_id channel_id,
             sc.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels sc
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and w.channel_id             = sc.channel_id
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
         and exists (select '1'
                       from wh w3,
                            channels wc
                      where w3.physical_wh            = I_other_loc
                        and w3.channel_id             = wc.channel_id
                        and wc.channel_type           = sc.channel_type
                        and w3.finisher_ind           = 'N'
                        and w3.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w3.customer_order_loc_ind, I_cust_order_loc_ind))
    order by w.wh;

   -- All virtual warehouses that is present in item_loc
   cursor C_GET_VWH_4 is
      select I_phy_loc phy_loc,
             w.wh loc,
             I_loc_type loc_type,
             il.status status,
             NVL(il.receive_as_type, 'E') receive_as_type,
             c.channel_id channel_id,
             c.channel_type channel_type,
             w2.primary_vwh primary_vwh,
             w.protected_ind protected_ind,
             w.restricted_ind restricted_ind,
             w.tsf_entity_id tsf_entity_id,
             w.org_unit_id org_unit_id
        from wh w,
             wh w2,
             item_loc il,
             channels c
       where w.physical_wh            = I_phy_loc
         and w.stockholding_ind       = 'Y'
         and il.loc                   = w.wh
         and il.item                  = I_item
         and w.channel_id             = c.channel_id(+)
         and w2.wh                    = I_phy_loc
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
    order by w.wh;

   -- Select the details of store
   cursor C_GET_STORE is
      select I_phy_loc,
             s.store,
             I_loc_type,
             il.status,
             NVL(il.receive_as_type, 'E'),
             c.channel_id,
             c.channel_type,
             I_phy_loc,
             'N',
             'N',
             s.tsf_entity_id tsf_entity_id,
             s.org_unit_id org_unit_id
        from store s,
             item_loc il,
             channels c
       where s.store      = I_phy_loc
         and il.loc       = s.store
         and il.item      = I_item
         and s.channel_id = c.channel_id(+);

   --
   cursor C_PRIM_VWH is
      select vir_w.wh wh
        from wh phy_w,
             wh vir_w
       where vir_w.physical_wh            = I_phy_loc
         and vir_w.stockholding_ind       = 'Y'
         and phy_w.physical_wh(+)         = vir_w.physical_wh
         and phy_w.stockholding_ind(+)    = 'N'
         and vir_w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', vir_w.customer_order_loc_ind, I_cust_order_loc_ind)
       --order by
       -- primary not protected
       -- low number not protected
       -- primary protected
       -- low number protected
       order by vir_w.protected_ind;

   --
   cursor C_GET_EXT_FIN is
      select I_phy_loc,
             p.partner_id,
             I_loc_type,
             il.status,
             NVL(il.receive_as_type, 'E'),
             NULL,-- channel_id,
             NULL,-- channel_type,
             I_phy_loc,
             'N',
             'N',
             p.tsf_entity_id tsf_entity_id,
             p.org_unit_id org_unit_id
        from partner p,
             item_loc il,
             tsf_entity te
       where p.partner_id    = to_char(I_phy_loc)
         and il.loc          = p.partner_id
         and p.partner_type  = 'E'
         and il.item         = I_item
         and p.tsf_entity_id = te.tsf_entity_id(+);

BEGIN

   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack_ind,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_pack_type,
                                    I_item) = FALSE then
      return FALSE;
   end if;

   if I_cust_order_loc_ind = 'N' then
      L_ranged_ind := 'Y';
   else
      L_ranged_ind := 'N';
   end if;

   -- for loc type of 'S'tore, NO distribution needed. Add the store to the inv_flow array
   if I_loc_type = 'S' then

      if NEW_LOC(O_error_message,
                 I_item,
                 L_pack_ind,
                 NULL, NULL, NULL,
                 I_phy_loc,
                 'S',
                 L_ranged_ind) = FALSE then
         return FALSE;
      end if;

      i := 1;

      open C_GET_STORE;
      fetch  C_GET_STORE into O_inv_flow_array(i).phy_loc,
                              O_inv_flow_array(i).loc,
                              O_inv_flow_array(i).loc_type,
                              O_inv_flow_array(i).status,
                              O_inv_flow_array(i).receive_as_type,
                              O_inv_flow_array(i).channel_id,
                              O_inv_flow_array(i).channel_type,
                              O_inv_flow_array(i).primary_vwh,
                              O_inv_flow_array(i).protected_ind,
                              O_inv_flow_array(i).restricted_ind,
                              O_inv_flow_array(i).tsf_entity_id,
                              O_inv_flow_array(i).org_unit_id;
      if C_GET_STORE%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_ARG',
                                               I_phy_loc,
                                               NULL,
                                               NULL);
         close C_GET_STORE;
         return FALSE;
      end if;
      close C_GET_STORE;
      --Done processing for store here.
      return TRUE;
   end if;
   ---
   if I_loc_type = 'E' then
      if NEW_LOC(O_error_message,
                 I_item,
                 L_pack_ind,
                 NULL, NULL, NULL,
                 I_phy_loc,
                 'E',
                 L_ranged_ind) = FALSE then
         return FALSE;
      end if;

      i := 1;

      open C_GET_EXT_FIN;
      fetch C_GET_EXT_FIN into O_inv_flow_array(i).phy_loc,
                               O_inv_flow_array(i).loc,
                               O_inv_flow_array(i).loc_type,
                               O_inv_flow_array(i).status,
                               O_inv_flow_array(i).receive_as_type,
                               O_inv_flow_array(i).channel_id,
                               O_inv_flow_array(i).channel_type,
                               O_inv_flow_array(i).primary_vwh,
                               O_inv_flow_array(i).protected_ind,
                               O_inv_flow_array(i).restricted_ind,
                               O_inv_flow_array(i).tsf_entity_id,
                               O_inv_flow_array(i).org_unit_id;
      if C_GET_EXT_FIN%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_FINISHER',
                                               NULL,
                                               NULL,
                                               NULL);
         close C_GET_EXT_FIN;
         return FALSE;
      end if;
      close C_GET_EXT_FIN;
      --Done processing for External Finisher here.
      return TRUE;
   end if;
   ---

   if (I_loc_type = 'W') THEN
      for loop_cnt in 1 .. 5 loop
         O_inv_flow_array.DELETE;
         L_exists := FALSE;
         i := 0;
         -- Loop through the cursors to find the mapping virtual warehouses.
         -- This will loop through the mapping hierarchy till a mapping is found.
         if loop_cnt = 1 then
            if I_other_loc_type = 'S' then
               for rec in C_GET_VWH_1_S loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec.phy_loc;
                  O_inv_flow_array(i).loc             := rec.loc;
                  O_inv_flow_array(i).loc_type        := rec.loc_type;
                  O_inv_flow_array(i).status          := rec.status;
                  O_inv_flow_array(i).receive_as_type := rec.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec.channel_id;
                  O_inv_flow_array(i).channel_type    := rec.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec.org_unit_id;
               end loop;
            elsif I_other_loc_type = 'W' then
               for rec1 in C_GET_VWH_1_W loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec1.phy_loc;
                  O_inv_flow_array(i).loc             := rec1.loc;
                  O_inv_flow_array(i).loc_type        := rec1.loc_type;
                  O_inv_flow_array(i).status          := rec1.status;
                  O_inv_flow_array(i).receive_as_type := rec1.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec1.channel_id;
                  O_inv_flow_array(i).channel_type    := rec1.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec1.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec1.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec1.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec1.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec1.org_unit_id;
               end loop;
            else
               for rec2 in C_GET_VWH_1_E loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec2.phy_loc;
                  O_inv_flow_array(i).loc             := rec2.loc;
                  O_inv_flow_array(i).loc_type        := rec2.loc_type;
                  O_inv_flow_array(i).status          := rec2.status;
                  O_inv_flow_array(i).receive_as_type := rec2.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec2.channel_id;
                  O_inv_flow_array(i).channel_type    := rec2.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec2.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec2.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec2.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec2.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec2.org_unit_id;
               end loop;
            end if;
         end if;

         if loop_cnt = 2 then
            if I_other_loc_type = 'S' then
               for rec in C_GET_VWH_2_S loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec.phy_loc;
                  O_inv_flow_array(i).loc             := rec.loc;
                  O_inv_flow_array(i).loc_type        := rec.loc_type;
                  O_inv_flow_array(i).status          := rec.status;
                  O_inv_flow_array(i).receive_as_type := rec.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec.channel_id;
                  O_inv_flow_array(i).channel_type    := rec.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec.org_unit_id;
               end loop;
            elsif I_other_loc_type = 'W' then
               for rec1 in C_GET_VWH_2_W loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec1.phy_loc;
                  O_inv_flow_array(i).loc             := rec1.loc;
                  O_inv_flow_array(i).loc_type        := rec1.loc_type;
                  O_inv_flow_array(i).status          := rec1.status;
                  O_inv_flow_array(i).receive_as_type := rec1.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec1.channel_id;
                  O_inv_flow_array(i).channel_type    := rec1.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec1.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec1.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec1.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec1.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec1.org_unit_id;
               end loop;
            else
               for rec2 in C_GET_VWH_2_E loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec2.phy_loc;
                  O_inv_flow_array(i).loc             := rec2.loc;
                  O_inv_flow_array(i).loc_type        := rec2.loc_type;
                  O_inv_flow_array(i).status          := rec2.status;
                  O_inv_flow_array(i).receive_as_type := rec2.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec2.channel_id;
                  O_inv_flow_array(i).channel_type    := rec2.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec2.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec2.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec2.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec2.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec2.org_unit_id;
               end loop;
            end if;
         end if;

         if loop_cnt = 3 then
            if I_other_loc_type = 'S' then
               for rec in C_GET_VWH_3_S loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec.phy_loc;
                  O_inv_flow_array(i).loc             := rec.loc;
                  O_inv_flow_array(i).loc_type        := rec.loc_type;
                  O_inv_flow_array(i).status          := rec.status;
                  O_inv_flow_array(i).receive_as_type := rec.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec.channel_id;
                  O_inv_flow_array(i).channel_type    := rec.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec.org_unit_id;
               end loop;
            elsif I_other_loc_type = 'W' then
               for rec1 in C_GET_VWH_3_W loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec1.phy_loc;
                  O_inv_flow_array(i).loc             := rec1.loc;
                  O_inv_flow_array(i).loc_type        := rec1.loc_type;
                  O_inv_flow_array(i).status          := rec1.status;
                  O_inv_flow_array(i).receive_as_type := rec1.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec1.channel_id;
                  O_inv_flow_array(i).channel_type    := rec1.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec1.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec1.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec1.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec1.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec1.org_unit_id;
               end loop;
            else
               for rec2 in C_GET_VWH_3_E loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec2.phy_loc;
                  O_inv_flow_array(i).loc             := rec2.loc;
                  O_inv_flow_array(i).loc_type        := rec2.loc_type;
                  O_inv_flow_array(i).status          := rec2.status;
                  O_inv_flow_array(i).receive_as_type := rec2.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec2.channel_id;
                  O_inv_flow_array(i).channel_type    := rec2.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec2.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec2.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec2.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec2.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec2.org_unit_id;
               end loop;
            end if;
         end if;

         if loop_cnt = 4 then
            if I_other_loc_type = 'S' then
               for rec in C_GET_VWH_4_S loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec.phy_loc;
                  O_inv_flow_array(i).loc             := rec.loc;
                  O_inv_flow_array(i).loc_type        := rec.loc_type;
                  O_inv_flow_array(i).status          := rec.status;
                  O_inv_flow_array(i).receive_as_type := rec.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec.channel_id;
                  O_inv_flow_array(i).channel_type    := rec.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec.org_unit_id;
               end loop;
            elsif I_other_loc_type = 'W' then
               for rec in C_GET_VWH_4_W loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec.phy_loc;
                  O_inv_flow_array(i).loc             := rec.loc;
                  O_inv_flow_array(i).loc_type        := rec.loc_type;
                  O_inv_flow_array(i).status          := rec.status;
                  O_inv_flow_array(i).receive_as_type := rec.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec.channel_id;
                  O_inv_flow_array(i).channel_type    := rec.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec.org_unit_id;
               end loop;
            end if;
         end if;

         if loop_cnt = 5 then
            if I_other_loc_type = 'S' then
               for rec in C_GET_VWH_5_S loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec.phy_loc;
                  O_inv_flow_array(i).loc             := rec.loc;
                  O_inv_flow_array(i).loc_type        := rec.loc_type;
                  O_inv_flow_array(i).status          := rec.status;
                  O_inv_flow_array(i).receive_as_type := rec.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec.channel_id;
                  O_inv_flow_array(i).channel_type    := rec.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec.org_unit_id;
               end loop;
            elsif I_other_loc_type = 'W' then
               for rec in C_GET_VWH_5_W loop
                  i := i + 1;
                  O_inv_flow_array(i).phy_loc         := rec.phy_loc;
                  O_inv_flow_array(i).loc             := rec.loc;
                  O_inv_flow_array(i).loc_type        := rec.loc_type;
                  O_inv_flow_array(i).status          := rec.status;
                  O_inv_flow_array(i).receive_as_type := rec.receive_as_type;
                  O_inv_flow_array(i).channel_id      := rec.channel_id;
                  O_inv_flow_array(i).channel_type    := rec.channel_type;
                  O_inv_flow_array(i).primary_vwh     := rec.primary_vwh;
                  O_inv_flow_array(i).protected_ind   := rec.protected_ind;
                  O_inv_flow_array(i).restricted_ind  := rec.restricted_ind;
                  O_inv_flow_array(i).tsf_entity_id   := rec.tsf_entity_id;
                  O_inv_flow_array(i).org_unit_id     := rec.org_unit_id;
               end loop;
            end if;
         end if;

         -- This function will check the net available inventory of the virtual warehouses
         -- in the location list if the location is a 'F'rom location.
         if I_to_from_ind = 'F' then
            if CHECK_INV_FLOW_SOH(O_error_message,
                                  L_exists,
                                  O_inv_flow_array,
                                  I_item,
                                  I_tsf_qty,
                                  I_inv_status) = FALSE then
               return FALSE;
            end if;
         else -- I_to_from_ind = 'T'
            if i > 0 then
               L_exists := TRUE;
            end if;
         end if;

         if L_exists = TRUE then
            return TRUE;
         end if;
      end LOOP; -- END of looping through all the possible matching combinations

      if i > 0 then
         -- found at least 1 location
         -- stop processing here even if from-locs do NOT have enough available qty
         return TRUE;
      end if;

      -- When cannot match a single virtual warehouse based on the
      -- org unit/tsf entity/channel id/finisher ind, pick the first vwh found
      if (I_to_from_ind = 'F') then

         open C_PRIM_VWH;
         fetch C_PRIM_VWH into L_loc;
         close C_PRIM_VWH;

         if L_loc is NOT NULL then
            if NEW_LOC(O_error_message,
                       I_item,
                       L_pack_ind,
                       NULL, NULL, NULL,
                       L_loc,
                       'W',
                       L_ranged_ind) = FALSE then
               return FALSE;
            end if;
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_MAP_POSSIBLE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;

         for rec in C_GET_VWH_4 loop
            i := i + 1;
            O_inv_flow_array(i).phy_loc         := rec.phy_loc;
            O_inv_flow_array(i).loc             := rec.loc;
            O_inv_flow_array(i).loc_type        := rec.loc_type;
            O_inv_flow_array(i).status          := rec.status;
            O_inv_flow_array(i).receive_as_type := rec.receive_as_type;
            O_inv_flow_array(i).channel_id      := rec.channel_id;
            O_inv_flow_array(i).channel_type    := rec.channel_type;
            O_inv_flow_array(i).primary_vwh     := rec.primary_vwh;
            O_inv_flow_array(i).protected_ind   := rec.protected_ind;
            O_inv_flow_array(i).restricted_ind  := rec.restricted_ind;
            O_inv_flow_array(i).tsf_entity_id   := rec.tsf_entity_id;
            O_inv_flow_array(i).org_unit_id     := rec.org_unit_id;
         end loop;
      else  -- I_to_from_ind = 'T'o

         if FIND_MAPPING_VWH(O_error_message,
                             L_loc,
                             I_phy_loc,
                             I_to_from_ind,
                             I_other_loc,
                             I_other_loc_type,
                             I_item,
                             I_cust_order_loc_ind) = FALSE then
            return FALSE;
         end if;

         for rec in C_GET_VWH_4 loop
            i := i + 1;
            O_inv_flow_array(i).phy_loc         := rec.phy_loc;
            O_inv_flow_array(i).loc             := rec.loc;
            O_inv_flow_array(i).loc_type        := rec.loc_type;
            O_inv_flow_array(i).status          := rec.status;
            O_inv_flow_array(i).receive_as_type := rec.receive_as_type;
            O_inv_flow_array(i).channel_id      := rec.channel_id;
            O_inv_flow_array(i).channel_type    := rec.channel_type;
            O_inv_flow_array(i).primary_vwh     := rec.primary_vwh;
            O_inv_flow_array(i).protected_ind   := rec.protected_ind;
            O_inv_flow_array(i).restricted_ind  := rec.restricted_ind;
            O_inv_flow_array(i).tsf_entity_id   := rec.tsf_entity_id;
            O_inv_flow_array(i).org_unit_id     := rec.org_unit_id;
         end loop;
      end if;
   end if;

   /* ASSERT -- if this happens there is bad data */
   if i = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_MAP_POSSIBLE',null,null,null);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOAD_INV_FLOW_LOC;
-------------------------------------------------------------------------------
FUNCTION FIND_MAP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_keepgoing       IN OUT   BOOLEAN,
                  O_to_loc          IN OUT   ITEM_LOC.LOC%TYPE,
                  I_from_loc        IN       ITEM_LOC.LOC%TYPE,
                  I_from_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                  I_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                  I_status          IN       ITEM_LOC.STATUS%TYPE,
                  I_channel_id      IN       CHANNELS.CHANNEL_ID%TYPE,
                  I_channel_type    IN       CHANNELS.CHANNEL_TYPE%TYPE,
                  I_tsf_entity_id   IN       WH.TSF_ENTITY_ID%TYPE,
                  I_org_unit_id     IN       WH.ORG_UNIT_ID%TYPE,
                  I_to_array        IN       DISTRIBUTION_SQL.INV_FLOW_ARRAY)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'DISTRIBUTION_SQL.FIND_MAP';
   to_cnt      BINARY_INTEGER := 1;

BEGIN

   /*
    * This function's job is to create inventory maps between a passed in
    * from location and one of the elements in a passed in array of
    * to locations.
    *
    * It creates the inventory map using the following rules.  If the
    * lower number's rule cannot be met it moves on to the next rule.
    *  1) The from loc's org_unit_id + tsf_entity_id + channel_id equals the
    *     to loc's org_unit_id + tsf_entity_id + channel_id.
    *
    *  2) The from loc's org_unit_id + channel_id equals the to loc's
    *     org_unit_id + channel_id.
    *
    *  3) The from loc's tsf_entity_id + channel_id equals
    *     the to loc's tsf_entity_id + channel_id.
    *
    *  4) The from loc's channel_id equals the to loc's channel id.
    *
    *  5) The to loc with the smallest number that is not restricted
    *      where the from loc's channel type equals the to loc's channel_type.
    *
    *  6) The to loc with the smallest number that is restricted
    *      where the from loc's channel type equals the to loc's channel_type.
    *
    *  7) The primary to loc if it is not restricted.
    *
    *  8) The to loc with the smallest number that is not restricted.
    *
    *  9) The primary to loc if it is restricted.
    *
    */

   --org_unit_id + tsf_entity_id + channel_id
   FOR to_cnt IN I_to_array.FIRST..I_to_array.LAST LOOP

      if (I_org_unit_id = I_to_array(to_cnt).org_unit_id and
          I_tsf_entity_id = I_to_array(to_cnt).tsf_entity_id and
          I_channel_id = I_to_array(to_cnt).channel_id and
          NVL(I_status, I_to_array(to_cnt).status) = I_to_array(to_cnt).status) then

         O_to_loc := I_to_array(to_cnt).loc;
         O_keepgoing := FALSE;
         return TRUE;
      end if;
   END LOOP;

   --org_unit_id + channel_id
   FOR to_cnt IN I_to_array.FIRST..I_to_array.LAST LOOP

      if (I_org_unit_id = I_to_array(to_cnt).org_unit_id and
          I_channel_id = I_to_array(to_cnt).channel_id and
          NVL(I_status, I_to_array(to_cnt).status) = I_to_array(to_cnt).status) then

         O_to_loc := I_to_array(to_cnt).loc;
         O_keepgoing := FALSE;
         return TRUE;
      end if;
   END LOOP;

   --tsf_entity_id + channel_id
   FOR to_cnt IN I_to_array.FIRST..I_to_array.LAST LOOP

      if (I_tsf_entity_id = I_to_array(to_cnt).tsf_entity_id and
          I_channel_id = I_to_array(to_cnt).channel_id and
          NVL(I_status, I_to_array(to_cnt).status) = I_to_array(to_cnt).status) then

         O_to_loc := I_to_array(to_cnt).loc;
         O_keepgoing := FALSE;
         return TRUE;
      end if;
   END LOOP;

   --channel_id
   FOR to_cnt IN I_to_array.FIRST..I_to_array.LAST LOOP

      if (I_channel_id = I_to_array(to_cnt).channel_id and
          NVL(I_status, I_to_array(to_cnt).status) = I_to_array(to_cnt).status) then

         O_to_loc := I_to_array(to_cnt).loc;
         O_keepgoing := FALSE;
         return TRUE;
      end if;
   END LOOP;

   --lowest channel_type, not restricted
   FOR to_cnt IN I_to_array.FIRST..I_to_array.LAST LOOP

       if (I_channel_type = I_to_array(to_cnt).channel_type and
           I_to_array(to_cnt).restricted_ind = 'N' and
           NVL(I_status, I_to_array(to_cnt).status) = I_to_array(to_cnt).status) then
          O_to_loc := I_to_array(to_cnt).loc;
          O_keepgoing := FALSE;
          return TRUE;

      end if;
   END LOOP;

   --lowest channel_type, restricted
   FOR to_cnt IN I_to_array.FIRST..I_to_array.LAST LOOP

       if (I_channel_type = I_to_array(to_cnt).channel_type and
           NVL(I_status, I_to_array(to_cnt).status) = I_to_array(to_cnt).status) then
          O_to_loc := I_to_array(to_cnt).loc;
          O_keepgoing := FALSE;
          return TRUE;

      end if;
   END LOOP;

   --primary, not restricted
   FOR to_cnt IN I_to_array.FIRST..I_to_array.LAST LOOP

      if (I_to_array(to_cnt).loc = I_to_array(to_cnt).primary_vwh and
          I_to_array(to_cnt).restricted_ind = 'N' and
          NVL(I_status, I_to_array(to_cnt).status) = I_to_array(to_cnt).status) then

         O_to_loc := I_to_array(to_cnt).loc;
         O_keepgoing := FALSE;
         return TRUE;

      end if;
   END LOOP;

   --lowest, not restricted
   FOR to_cnt IN I_to_array.FIRST..I_to_array.LAST LOOP

      if (I_to_array(to_cnt).restricted_ind = 'N' and
          NVL(I_status, I_to_array(to_cnt).status) = I_to_array(to_cnt).status) then

         O_to_loc := I_to_array(to_cnt).loc;
         O_keepgoing := FALSE;
         return TRUE;

      end if;
   END LOOP;

   --primary, restricted
   FOR to_cnt IN I_to_array.FIRST..I_to_array.LAST LOOP

      if (I_to_array(to_cnt).loc = I_to_array(to_cnt).primary_vwh and
          NVL(I_status, I_to_array(to_cnt).status) = I_to_array(to_cnt).status) then

         O_to_loc := I_to_array(to_cnt).loc;
         O_keepgoing := FALSE;
         return TRUE;

      end if;
   END LOOP;

   O_keepgoing := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FIND_MAP;
--------------------------------------------------------------------------------
FUNCTION CHECK_INV_FLOW_SOH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT BOOLEAN,
                            I_inv_flow_array  IN     distribution_sql.inv_flow_array,
                            I_item            IN     item_master.item%TYPE,
                            I_tsf_qty         IN     item_loc_soh.stock_on_hand%TYPE,
                            I_inv_status      IN     shipsku.inv_status%TYPE)
RETURN BOOLEAN IS
   L_program             VARCHAR2(64) := 'BOL_SQL.CHECK_INV_FLOW_SOH';

   L_net_available_qty   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE     := 0;
   L_available_qty       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE     := 0;
   L_non_sellable_qty    ITEM_LOC_SOH.NON_SELLABLE_QTY%TYPE  := 0;
   L_dummy               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;

   from_loc_cnt BINARY_INTEGER := 1;

BEGIN

   if I_inv_flow_array.COUNT > 0 then
   FOR from_loc_cnt IN I_inv_flow_array.FIRST..I_inv_flow_array.LAST LOOP
      L_non_sellable_qty  := 0;
      L_available_qty     :=0;
      -- Check for the net available inventory at virtual whs before assigning the quantity to distribute
      if ITEMLOC_QUANTITY_SQL.GET_ITEM_LOC_QTYS(O_error_message,
                                                L_available_qty, --O_stock_on_hand,
                                                L_dummy,         --O_pack_comp_soh,
                                                L_dummy,         --O_in_transit_qty,
                                                L_dummy,         --O_pack_comp_intran,
                                                L_dummy,         --O_tsf_reserved_qty,
                                                L_dummy,         --O_pack_comp_resv,
                                                L_dummy,         --O_tsf_expected_qty,
                                                L_dummy,         --O_pack_comp_exp,
                                                L_dummy,         --O_rtv_qty,
                                                L_non_sellable_qty,
                                                L_dummy,         --O_customer_resv,
                                                L_dummy,         --O_customer_backorder,
                                                L_dummy,         --O_pack_comp_cust_resv,
                                                L_dummy,         --O_pack_comp_cust_back,
                                                I_item,
                                                I_inv_flow_array(from_loc_cnt).loc,
                                                'W') = FALSE then
         return FALSE;
      end if;
      --
      if I_inv_status is NULL then
         L_net_available_qty := L_net_available_qty + (L_available_qty - L_non_sellable_qty);
      else
         L_net_available_qty := L_net_available_qty + L_non_sellable_qty;
      end if;

   END LOOP;
   end if;

   if ( L_net_available_qty < I_tsf_qty ) Then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_INV_FLOW_SOH;
-------------------------------------------------------------------------------
FUNCTION NEW_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_item            IN       ITEM_MASTER.ITEM%TYPE,
                 I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                 I_dept            IN       ITEM_MASTER.DEPT%TYPE,
                 I_class           IN       ITEM_MASTER.CLASS%TYPE,
                 I_subclass        IN       ITEM_MASTER.SUBCLASS%TYPE,
                 I_loc             IN       ITEM_LOC.LOC%TYPE,
                 I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                 I_ranged_ind      IN       ITEM_LOC.RANGED_IND%TYPE)
RETURN BOOLEAN IS
   L_program             VARCHAR2(64) := 'DISTRIBUTION_SQL.NEW_LOC';

BEGIN

   if NEW_ITEM_LOC(O_error_message,
                   I_item,
                   I_loc,
                   NULL, NULL, I_loc_type, NULL,
                   I_dept,
                   I_class,
                   I_subclass,
                   NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL,
                   I_pack_ind,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, I_ranged_ind) = FALSE then
       return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END NEW_LOC;
----------------------------------------------------------------------------------------------
FUNCTION FIND_MAPPING_VWH(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_virtual_wh           IN OUT   WH.WH%TYPE,
                          I_phy_wh               IN       WH.WH%TYPE,
                          I_to_from_ind          IN       VARCHAR2,
                          I_other_loc            IN       ITEM_LOC.LOC%TYPE,
                          I_other_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_item                 IN       ITEM_LOC.ITEM%TYPE,
                          I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'DISTRIBUTION_SQL.FIND_MAPPING_VWH';
   L_itemloc_exists   BOOLEAN;
   L_ranged_ind       ITEM_LOC.RANGED_IND%TYPE := 'N';

BEGIN

   if FIND_MAPPING_VWH(O_error_message,
                       O_virtual_wh,
                       I_phy_wh,
                       I_to_from_ind,
                       I_other_loc,
                       I_other_loc_type,
                       I_cust_order_loc_ind) = FALSE then
      return FALSE;
   end if;

   if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                        I_item,
                                        O_virtual_wh,
                                        L_itemloc_exists) = FALSE then
      return FALSE;
   end if;

   if L_itemloc_exists = FALSE then
      if I_cust_order_loc_ind = 'N' then
         L_ranged_ind := 'Y';
      else
         L_ranged_ind := 'N';
      end if;

      if NEW_ITEM_LOC(O_error_message,
                      I_item,
                      O_virtual_wh,
                      NULL,
                      NULL,
                      'W',
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL,
                      NULL, L_ranged_ind) = FALSE then
          return FALSE;
       end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FIND_MAPPING_VWH;
---------------------------------------------------------------------------------------------
FUNCTION FIND_MAPPING_VWH(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_virtual_wh           IN OUT   WH.WH%TYPE,
                          I_phy_wh               IN       WH.WH%TYPE,
                          I_to_from_ind          IN       VARCHAR2,
                          I_other_loc            IN       ITEM_LOC.LOC%TYPE,
                          I_other_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program        VARCHAR2(64)      := 'DISTRIBUTION_SQL.FIND_MAPPING_VWH';
   L_order_helper   NUMBER;
   L_protect_ind    VARCHAR2(1);
   L_prim_ind       NUMBER;
   L_loc            ITEM_LOC.LOC%TYPE := NULL;

   cursor C_MAP_STORE is
      --org_unit_id + tsf_entity_id + channel_id match
      select 1 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             store s
       where w.physical_wh            = I_phy_wh
         and s.store                  = I_other_loc
         and w.org_unit_id            = s.org_unit_id
         and w.tsf_entity_id          = s.tsf_entity_id
         and w.channel_id             = s.channel_id
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --org_unit_id + channel_id match
      select 2 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             store s
       where w.physical_wh            = I_phy_wh
         and s.store                  = I_other_loc
         and w.org_unit_id            = s.org_unit_id
         and w.channel_id             = s.channel_id
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --tsf_entity_id + channel_id match
      select 3 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             store s
       where w.physical_wh            = I_phy_wh
         and s.store                  = I_other_loc
         and w.tsf_entity_id          = s.tsf_entity_id
         and w.channel_id             = s.channel_id
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --channel_id match
      select 4 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             store s
       where w.physical_wh            = I_phy_wh
         and s.store                  = I_other_loc
         and w.channel_id             = s.channel_id
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --channel_type match
      select 5 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             store s,
             channels sc,
             channels wc
       where w.physical_wh            = I_phy_wh
         and s.store                  = I_other_loc
         and s.channel_id             = sc.channel_id
         and w.channel_id             = wc.channel_id
         and sc.channel_type          = wc.channel_type
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --no channel connection -- get primary virtual
      select 6 order_helper,
             DECODE(I_to_from_ind,
                    'F', vir_w.protected_ind,
                    'T', vir_w.restricted_ind) pind,
             ABS(SIGN(phy_w.primary_vwh - vir_w.wh)) primind,
             vir_w.wh loc
        from wh phy_w,
             wh vir_w
       where vir_w.physical_wh            = I_phy_wh
         and vir_w.stockholding_ind       = 'Y'
         and phy_w.physical_wh(+)         = vir_w.physical_wh
         and phy_w.stockholding_ind(+)    = 'N'
         and vir_w.finisher_ind           = 'N'
         and vir_w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', vir_w.customer_order_loc_ind, I_cust_order_loc_ind)
         --order by
         -- org_unit + tsf_entity + channel_id match
         -- org_unit + channel_id match
         -- tsf_entity + channel_id match
         -- channel_id match
         -- channel_type match not protected/restricted
         -- channel_type match protected/restricted
         -- primary not protected/restricted
         -- low number not protected/restricted
         -- primary protected/restricted
         -- low number protected/restricted
    order by order_helper,
             pind,
             primind,
             loc;

   cursor C_MAP_WH is
      --org_unit_id + tsf_entity_id + channel_id match
      select 1 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,   -- all vwh in I_phy_wh
             wh w1   -- all vwh in I_other_loc
       where w.physical_wh             = I_phy_wh
         and w1.physical_wh            = I_other_loc
         and w.org_unit_id             = w1.org_unit_id
         and w.tsf_entity_id           = w1.tsf_entity_id
         and w.channel_id              = w1.channel_id
         and w.finisher_ind            = 'N'
         and w.customer_order_loc_ind  = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
         and w1.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w1.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --org_unit_id + channel_id match
      select 2 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             wh w1
       where w.physical_wh             = I_phy_wh
         and w1.physical_wh            = I_other_loc
         and w.org_unit_id             = w1.org_unit_id
         and w.channel_id              = w1.channel_id
         and w.finisher_ind            = 'N'
         and w.customer_order_loc_ind  = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
         and w1.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w1.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --tsf_entity_id + channel_id match
      select 3 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             wh w1
       where w.physical_wh             = I_phy_wh
         and w1.physical_wh            = I_other_loc
         and w.tsf_entity_id           = w1.tsf_entity_id
         and w.channel_id              = w1.channel_id
         and w.finisher_ind            = 'N'
         and w.customer_order_loc_ind  = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
         and w1.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w1.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --channel_id match
      select 4 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             wh w1
       where w.physical_wh             = I_phy_wh
         and w1.physical_wh            = I_other_loc
         and w.channel_id              = w1.channel_id
         and w.finisher_ind            = 'N'
         and w.customer_order_loc_ind  = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
         and w1.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w1.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --channel_type match
      select 5 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             wh w1,
             channels sc,
             channels wc
       where w.physical_wh             = I_phy_wh
         and w1.physical_wh            = I_other_loc
         and w1.channel_id             = sc.channel_id
         and w.channel_id              = wc.channel_id
         and sc.channel_type           = wc.channel_type
         and w.finisher_ind            = 'N'
         and w.customer_order_loc_ind  = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
         and w1.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w1.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --no channel connection -- get primary virtual
      select 6 order_helper,
             DECODE(I_to_from_ind,
                    'F', vir_w.protected_ind,
                    'T', vir_w.restricted_ind) pind,
             ABS(SIGN(phy_w.primary_vwh - vir_w.wh)) primind,
             vir_w.wh loc
        from wh phy_w,
             wh vir_w
       where vir_w.physical_wh            = I_phy_wh
         and vir_w.stockholding_ind       = 'Y'
         and phy_w.physical_wh(+)         = vir_w.physical_wh
         and phy_w.stockholding_ind(+)    = 'N'
         and vir_w.finisher_ind           = 'N'
         and vir_w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', vir_w.customer_order_loc_ind, I_cust_order_loc_ind)
         --order by
         -- org_unit + tsf_entity + channel_id match
         -- org_unit + channel_id match
         -- tsf_entity + channel_id match
         -- channel_id match
         -- channel_type match not protected/restricted
         -- channel_type match protected/restricted
         -- primary not protected/restricted
         -- low number not protected/restricted
         -- primary protected/restricted
         -- low number protected/restricted
   order by order_helper,
            pind,
            primind,
            loc;

   cursor C_MAP_EXT_FIN is
      --org_unit_id + tsf_entity_id match
      select 1 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             partner p
       where w.physical_wh            = I_phy_wh
         and p.partner_id             = to_char(I_other_loc)
         and p.partner_type           = 'E'
         and w.org_unit_id            =  p.org_unit_id
         and w.tsf_entity_id          = p.tsf_entity_id
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = NVL(I_cust_order_loc_ind,w.customer_order_loc_ind)
       UNION ALL
       --org_unit_id match
      select 2 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             partner p
       where w.physical_wh            = I_phy_wh
         and p.partner_id             = to_char(I_other_loc)
         and p.partner_type           = 'E'
         and w.org_unit_id            = p.org_unit_id
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --tsf_entity_id match
      select 3 order_helper,
             DECODE(I_to_from_ind,
                    'F', w.protected_ind,
                    'T', w.restricted_ind) pind,
             0 primind,
             w.wh loc
        from wh w,
             partner p
       where w.physical_wh            = I_phy_wh
         and p.partner_id             = to_char(I_other_loc)
         and p.partner_type           = 'E'
         and w.tsf_entity_id          = p.tsf_entity_id
         and w.finisher_ind           = 'N'
         and w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', w.customer_order_loc_ind, I_cust_order_loc_ind)
       UNION ALL
      --no channel connection -- get primary virtual
      select 4 order_helper,
             DECODE(I_to_from_ind,
                    'F', vir_w.protected_ind,
                    'T', vir_w.restricted_ind) pind,
             ABS(SIGN(phy_w.primary_vwh - vir_w.wh)) primind,
             vir_w.wh loc
        from wh phy_w,
             wh vir_w
       where vir_w.physical_wh            = I_phy_wh
         and vir_w.stockholding_ind       = 'Y'
         and phy_w.physical_wh(+)         = vir_w.physical_wh
         and phy_w.stockholding_ind(+)    = 'N'
         and vir_w.finisher_ind           = 'N'
         and vir_w.customer_order_loc_ind = decode(I_cust_order_loc_ind, 'N', vir_w.customer_order_loc_ind, I_cust_order_loc_ind)
         --order by
         -- org_unit + tsf_entity match
         -- tsf_entity match
         -- primary not protected/restricted
         -- low number not protected/restricted
         -- primary protected/restricted
         -- low number protected/restricted
    order by order_helper,
             pind,
             primind,
             loc;

BEGIN
   if I_other_loc_type = 'S' then
      open C_MAP_STORE;
      fetch C_MAP_STORE into L_order_helper,
                             L_protect_ind,
                             L_prim_ind,
                             L_loc;
      close C_MAP_STORE;
   elsif I_other_loc_type = 'W' then
      open C_MAP_WH;
      fetch C_MAP_WH into L_order_helper,
                          L_protect_ind,
                          L_prim_ind,
                          L_loc;
      close C_MAP_WH;
   else
      open C_MAP_EXT_FIN;
      fetch C_MAP_EXT_FIN into L_order_helper,
                               L_protect_ind,
                               L_prim_ind,
                               L_loc;
      close C_MAP_EXT_FIN;
   end if;

   if L_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_MAP_POSSIBLE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      O_virtual_wh := L_loc;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FIND_MAPPING_VWH;
---------------------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_PO_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'DISTRIBUTION_SQL.DISTRIBUTE_PO_ALLOC';
   i                  INTEGER;
   L_bucket_1_total   NUMBER := 0;
   L_bucket_2_total   NUMBER := 0;
   L_remainder        NUMBER := 0;
   L_dist_qty         NUMBER := 0;

   cursor C_ALLOC_WH_QTY is
      select w.wh,
             sum(NVL(ad.qty_allocated,0)) alloc_qty,
             sum(NVL(ad.po_rcvd_qty,0)) alloc_qty_rcv
        from wh w,
             ordloc ol,
             alloc_header ah,
             alloc_detail ad
       where ad.alloc_no  = ah.alloc_no
         and ah.order_no  = ol.order_no
         and ah.item  = ol.item
         and ah.wh  = ol.location
         and ol.order_no = I_order_no
         and ol.item = LP_Item
         and ol.location = w.wh
         and ah.wh = w.wh
         and w.physical_wh = LP_physical_wh
    group by w.wh
    order by w.wh;

BEGIN
   i := 0;

   FOR c_rec in C_ALLOC_WH_QTY LOOP
      i := i + 1;
      LP_dist_tab(i).dist_qty := 0;
      LP_dist_tab(i).wh := c_rec.wh;
      LP_dist_tab(i).bucket_1 := c_rec.alloc_qty;
      LP_dist_tab(i).bucket_2 := c_rec.alloc_qty_rcv;

      L_bucket_1_total := L_bucket_1_total + LP_dist_tab(i).bucket_1;
      L_bucket_2_total := L_bucket_2_total + LP_dist_tab(i).bucket_2;
   END LOOP;

   if LP_dist_tab.count = 0 then
      return TRUE;
   end if;

   if LP_negative_distribution_qty = FALSE then
      if LP_qty_to_distribute = (L_bucket_1_total - L_bucket_2_total) then
         FOR i in LP_dist_tab.FIRST..LP_dist_tab.LAST LOOP
            LP_dist_tab(i).dist_qty := (LP_dist_tab(i).bucket_1 - LP_dist_tab(i).bucket_2);
         END LOOP;
      elsif LP_qty_to_distribute < (L_bucket_1_total - L_bucket_2_total) then
         FOR i in LP_dist_tab.FIRST..LP_dist_tab.LAST LOOP
            if LP_integer_rounding then
               L_dist_qty := L_remainder + (LP_qty_to_distribute * (LP_dist_tab(i).bucket_1 - LP_dist_tab(i).bucket_2) / (L_bucket_1_total - L_bucket_2_total));
               LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
               L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
            else
               LP_dist_tab(i).dist_qty := LP_qty_to_distribute * (LP_dist_tab(i).bucket_1 - LP_dist_tab(i).bucket_2) / (L_bucket_1_total - L_bucket_2_total);
            end if;
         END LOOP;
      elsif LP_qty_to_distribute > (L_bucket_1_total - L_bucket_2_total) then
         FOR i in LP_dist_tab.FIRST..LP_dist_tab.LAST LOOP
            if LP_dist_tab(i).bucket_1 > LP_dist_tab(i).bucket_2 then
               LP_dist_tab(i).dist_qty := LP_dist_tab(i).bucket_1 - LP_dist_tab(i).bucket_2;
               LP_qty_to_distribute := LP_qty_to_distribute - LP_dist_tab(i).dist_qty;
            end if;
         END LOOP;

         if LP_qty_to_distribute > 0 then
            FOR i in LP_dist_tab.FIRST..LP_dist_tab.LAST LOOP
               if LP_integer_rounding then
                  L_dist_qty := L_remainder + LP_qty_to_distribute * (LP_dist_tab(i).bucket_1) / (L_bucket_1_total);
                  LP_dist_tab(i).dist_qty := LP_dist_tab(i).dist_qty + ROUND(L_dist_qty);
                  L_remainder := LP_dist_tab(i).dist_qty - ROUND(LP_dist_tab(i).dist_qty);
               else
                  L_dist_qty := LP_qty_to_distribute * (LP_dist_tab(i).bucket_1) / (L_bucket_1_total);
               end if;
            END LOOP;
         end if;
      end if;
   else
      if L_bucket_2_total > 0 then
         FOR i in LP_dist_tab.FIRST..LP_dist_tab.LAST LOOP
            if LP_integer_rounding then
               L_dist_qty := L_remainder + (LP_qty_to_distribute * LP_dist_tab(i).bucket_2 / L_bucket_2_total);
               LP_dist_tab(i).dist_qty := ROUND(L_dist_qty);
               L_remainder := L_dist_qty - LP_dist_tab(i).dist_qty;
            else
               LP_dist_tab(i).dist_qty := LP_qty_to_distribute * LP_dist_tab(i).bucket_2 / L_bucket_2_total;
            end if;
         END LOOP;
      else
         O_error_message := SQL_LIB.CREATE_MSG('ADJ_GT_RCV',
                                               SQLERRM,
                                               'DISTRIBUTION_SQL.DISTRIBUTE_PO_ALLOC',
                                               to_char(SQLCODE));
      end if;
   end if;

   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DISTRIBUTION_SQL.DISTRIBUTE_PO_ALLOC',
                                            to_char(SQLCODE));
      return FALSE;
END DISTRIBUTE_PO_ALLOC;
----------------------------------------------------------------------------------------------
END DISTRIBUTION_SQL;
/
