CREATE OR REPLACE PACKAGE RTV_VALIDATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------
-- Function Name: exist
-- Purpose      : to check if a given rtv number exists
--                on the rtv_head table
-- Calls        : None
-- Created      : 12-SEP-96 by Matt Sniffen
-------------------------------------------------------------------
FUNCTION EXIST(O_error_message IN OUT VARCHAR2,
               I_rtv           IN     RTV_HEAD.RTV_ORDER_NO%TYPE,
               O_exist         IN OUT BOOLEAN)
               RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: RTV_EXISTS_FOR_SUPP
-- Purpose      : check if a supplier has any RTVs in input or
--                approved status.
-- Calls        : None
-- Created      : 14-NOV-02
-------------------------------------------------------------------
FUNCTION RTV_EXISTS_FOR_SUPP(O_error_message IN OUT VARCHAR2,
                             O_rtv_exists    IN OUT BOOLEAN,
                             I_supplier      IN     RTV_HEAD.SUPPLIER%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: SHIPMENT_VALID
-- Purpose      : takes a shipment, store, and wh and checks if the
--                shipment exists at the store, and has items on
--                that failed quality control.
-- Calls        : None
-- Created      : 21-SEP-96 by Matt Sniffen
-------------------------------------------------------------------
FUNCTION SHIPMENT_VALID(O_error_message  IN OUT   VARCHAR2,
                        I_shipment       IN       SHIPMENT.SHIPMENT%TYPE,
                        I_supplier       IN       ORDHEAD.SUPPLIER%TYPE,
                        I_location       IN       SHIPMENT.TO_LOC%TYPE,
                        O_valid          IN OUT   BOOLEAN)
                        RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: INVENTORY_ITEM
-- Purpose      : takes an item, store, and wh and checks if the
--                item is valid for return to vendor from the given
--                location.
--                if the location is a store, the wh parameter should
--                be -1, or if the location is a warehouse the store
--                parameter should be -1.
-- Calls        : None
-- Created      : 21-SEP-96 by Matt Sniffen
-------------------------------------------------------------------
FUNCTION INVENTORY_ITEM(O_error_message  IN OUT  VARCHAR2,
                        I_item           IN      ITEM_MASTER.ITEM%TYPE,
                        I_supplier       IN      ITEM_SUPPLIER.SUPPLIER%TYPE,
                        I_store          IN      STORE.STORE%TYPE,
                        I_wh             IN      WH.WH%TYPE,
                        O_valid          IN OUT  BOOLEAN)
                       RETURN BOOLEAN;
-------------------------------------------------------------------
--Function:  DETAILS_EXIST
--Purpose:   This function will be used to make sure that item are
--           attached to the RTV.
---------------------------------------------------------------------
FUNCTION DETAILS_EXIST(O_error_message     IN OUT     VARCHAR2,
                       O_details_exist     IN OUT     BOOLEAN,
                       I_rtv_order_no      IN         RTV_HEAD.RTV_ORDER_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function:  VALID_RTV_QTY
--Purpose:   This function will be used to validate that the qty requested
--           to be returned to the vendor is a valid qty (doesn't exceed
--           what's actually available.
-----------------------------------------------------------------------------------------------
FUNCTION VALID_RTV_QTY(O_error_message     IN OUT     VARCHAR2,
                       I_rtv_order_no      IN         RTV_HEAD.RTV_ORDER_NO%TYPE,
                       I_loc               IN         ITEM_LOC.LOC%TYPE,
                       I_loc_type          IN         ITEM_LOC.LOC_TYPE%TYPE,
                       I_uot_size          IN         ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE)
   RETURN BOOLEAN; 
-----------------------------------------------------------------------------------------------
--Function:  GET_TOTAL_RTV_QTYS
--Purpose:   This function will be used to bring back the total RTV quantities
--           that have been created for both available stock and unavailable stock. 
-----------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_RTV_QTYS(O_error_message           IN OUT     VARCHAR2,
                            O_total_available_rtv     IN OUT     RTV_DETAIL.QTY_RETURNED%TYPE,
                            O_total_unavailable_rtv   IN OUT     RTV_DETAIL.QTY_RETURNED%TYPE,
                            I_item                    IN         RTV_DETAIL.ITEM%TYPE,
                            I_loc                     IN         ITEM_LOC.LOC%TYPE)

   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function:  VALID_RTV_ITEM_QTY
--Purpose:   This function will be used to validate that the qty requested
--           for each item/location combination. If the location is a physical warehouse,
--           the requested qty is validated against the total qty of all its virtual warehouses.
-----------------------------------------------------------------------------------------------
FUNCTION VALID_RTV_ITEM_QTY(O_error_message           IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_qty_requested          IN OUT     RTV_DETAIL.QTY_REQUESTED%TYPE,
                            I_item                    IN         RTV_DETAIL.ITEM%TYPE,
                            I_location                IN         ITEM_LOC.LOC%TYPE,
                            I_loc_type                IN         ITEM_LOC.LOC_TYPE%TYPE,
                            I_physical_wh_ind         IN         VARCHAR2,
                            I_reason                  IN         RTV_DETAIL.REASON%TYPE,
                            I_default_soh_ind         IN         VARCHAR2,
                            I_uot_size                IN         ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function:  VALID_INV_STATUS_QTY
--Purpose:   This function will be used to validate that the qty requested
--           to be returned to the vendor doesn't exceed the qty which is available
--           for the requested inventory status.
-----------------------------------------------------------------------------------------------
FUNCTION VALID_INV_STATUS_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_qty_requested   IN       RTV_DETAIL.QTY_REQUESTED%TYPE,
                              I_item            IN       RTV_DETAIL.ITEM%TYPE,
                              I_location        IN       ITEM_LOC.LOC%TYPE,
                              I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                              I_inv_status      IN       INV_STATUS_TYPES.INV_STATUS%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END RTV_VALIDATE_SQL;
/
