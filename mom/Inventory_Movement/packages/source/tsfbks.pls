
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TSF_BT_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
FUNCTION BT_EXECUTE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item            IN       ITEM_MASTER.ITEM%TYPE,
                    I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                    I_dept            IN       ITEM_MASTER.DEPT%TYPE,
                    I_class           IN       ITEM_MASTER.CLASS%TYPE,
                    I_subclass        IN       ITEM_MASTER.SUBCLASS%TYPE,
                    I_from_loc        IN       ITEM_LOC.LOC%TYPE,
                    I_to_loc          IN       ITEM_LOC.LOC%TYPE,
                    I_tsf_qty         IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                    I_inv_status      IN       TSFDETAIL.INV_STATUS%TYPE,
                    I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                    I_tran_date       IN       TSFHEAD.CREATE_DATE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: CREATE_BOOK_TSF_CO_RTN
-- Purpose      : This public function will create a book transfer for a customer
--                order returned to a warehouse. It performs inventory adjustment
--                at the warehouse if it is a 'return without inventory'.
-------------------------------------------------------------------------------
FUNCTION CREATE_BOOK_TSF_CO_RTN(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_book_tsf_co_rtn_tbl   IN OUT   OBJ_BOOK_TSF_CO_RTN_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END;
/
