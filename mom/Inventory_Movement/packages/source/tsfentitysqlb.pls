CREATE OR REPLACE PACKAGE BODY TSF_ENTITY_SQL AS
--------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists          IN OUT  BOOLEAN,
                 O_tsf_entity_row  IN OUT  TSF_ENTITY%ROWTYPE,
                 I_tsf_entity_id   IN      TSF_ENTITY.TSF_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'TSF_ENTITY_SQL.GET_ROW';
   
   cursor C_GET_ROW is
      select te.tsf_entity_id, 
             vte.tsf_entity_desc, 
             te.secondary_desc, 
             te.create_id,
             te.create_datetime
        from tsf_entity te, 
             v_tsf_entity_tl vte
       where te.tsf_entity_id = vte.tsf_entity_id
         and te.tsf_entity_id = I_tsf_entity_id;

BEGIN

   --- Check required input parameters
   if I_tsf_entity_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_entity_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --- Initialize the output variable
   O_tsf_entity_row := NULL;
  
   --- Get the row from tsf_entity
   open  C_GET_ROW;
   fetch C_GET_ROW into O_tsf_entity_row;
   close C_GET_ROW;

   O_exists := (O_tsf_entity_row.tsf_entity_id is NOT NULL);
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ROW;
--------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_delete_ok       IN OUT  BOOLEAN,
                      I_tsf_entity_id   IN      TSF_ENTITY.TSF_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   L_exists   VARCHAR2(1)   := 'N';
   L_program  VARCHAR2(61)  := 'TSF_ENTITY_SQL.CHECK_DELETE';

   cursor C_CHECK_DELETE is
      select 'Y'
        from store
       where tsf_entity_id = I_tsf_entity_id
         and rownum      = 1
   union all
      select 'Y'
        from wh
       where tsf_entity_id = I_tsf_entity_id
         and rownum      = 1
   union all
      select 'Y'
        from partner
       where tsf_entity_id = I_tsf_entity_id
         and rownum      = 1;

BEGIN

   if I_tsf_entity_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_entity_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CHECK_DELETE;
   fetch C_CHECK_DELETE into L_exists;
   close C_CHECK_DELETE;

   O_delete_ok := (L_exists = 'N');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_DELETE;
--------------------------------------------------------------------------
FUNCTION CHECK_ORG_UNIT_LOC_EXISTS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists          IN OUT  BOOLEAN,
                                   I_tsf_entity_id   IN      TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                                   I_org_unit_id     IN      ORG_UNIT.ORG_UNIT_ID%TYPE)
RETURN BOOLEAN IS

   L_loc_exists   VARCHAR2(1)   := 'N';
   L_program      VARCHAR2(64)  := 'TSF_ENTITY_SQL.CHECK_ORG_UNIT_LOC_EXISTS';

   cursor C_LOC_EXISTS is
      select 'Y'
        from store
       where tsf_entity_id = I_tsf_entity_id
         and org_unit_id = I_org_unit_id
   union all
      select 'Y'
        from wh
       where tsf_entity_id = I_tsf_entity_id
         and org_unit_id = I_org_unit_id
   union all
      select 'Y'
        from partner
       where tsf_entity_id = I_tsf_entity_id
         and org_unit_id = I_org_unit_id;

BEGIN
   O_exists := FALSE;

   if I_tsf_entity_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_entity_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_org_unit_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_org_unit_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_LOC_EXISTS;
   fetch C_LOC_EXISTS into L_loc_exists;
   close C_LOC_EXISTS;

   if L_loc_exists = 'Y' then
      O_exists := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('TSF_ENT_OU_LOC_EXISTS',
                                            I_tsf_entity_id,
                                            I_org_unit_id,
                                            NULL);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ORG_UNIT_LOC_EXISTS;
--------------------------------------------------------------------------
FUNCTION CHECK_TSF_ENT_WO_OU_EXISTS (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists          IN OUT  BOOLEAN)
RETURN BOOLEAN IS

   cursor C_CHECK_ORG_UNIT is
      select ent.tsf_entity_id
        from tsf_entity ent
       where NOT EXISTS (select 'X'
                           from tsf_entity_org_unit_sob tous
                          where ent.tsf_entity_id = tous.tsf_entity_id
                            and rownum = 1);

   L_program                     VARCHAR2(64) := 'TSF_ENTITY_SQL.CHECK_TSF_ENT_WO_OU_EXISTS';
   L_tsf_entity_id               TSF_ENTITY.TSF_ENTITY_ID%TYPE := NULL;

BEGIN
   O_exists := FALSE;

   open C_CHECK_ORG_UNIT;
   fetch C_CHECK_ORG_UNIT into L_tsf_entity_id;
   close C_CHECK_ORG_UNIT;

   if L_tsf_entity_id is not NULL then
      O_exists := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_ORG_UNIT_TSF_ENTITY',
                                            L_tsf_entity_id,
                                            NULL,
                                            NULL);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_TSF_ENT_WO_OU_EXISTS;
--------------------------------------------------------------------------
FUNCTION DELETE_TSF_ENTITY (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'TSF_ENTITY_SQL.DELETE_TSF_ENTITY';
   L_table           VARCHAR2(20) := 'TSF_ENTITY';

   cursor C_LOCK_TSF_ENTITY_TL is
      select 'x'
        from tsf_entity_tl ent
       where NOT EXISTS (select 'X'
                           from tsf_entity_org_unit_sob tous
                          where ent.tsf_entity_id = tous.tsf_entity_id
                            and rownum = 1)
         for update nowait;

   cursor C_LOCK_TSF_ENTITY is
      select 'x'
        from tsf_entity ent
       where NOT EXISTS (select 'X'
                           from tsf_entity_org_unit_sob tous
                          where ent.tsf_entity_id = tous.tsf_entity_id
                            and rownum = 1)
         for update nowait;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

BEGIN

   open C_LOCK_TSF_ENTITY_TL;
   close C_LOCK_TSF_ENTITY_TL;

   delete tsf_entity_tl ent
    where NOT EXISTS (select 'X'
                        from tsf_entity_org_unit_sob tous
                       where ent.tsf_entity_id = tous.tsf_entity_id
                         and rownum = 1);

   open C_LOCK_TSF_ENTITY;
   close C_LOCK_TSF_ENTITY;

   delete tsf_entity ent
    where NOT EXISTS (select 'X'
                        from tsf_entity_org_unit_sob tous
                       where ent.tsf_entity_id = tous.tsf_entity_id
                         and rownum = 1);
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORDS_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_TSF_ENTITY;
--------------------------------------------------------------------------
FUNCTION DEL_TSF_ENT_FISCAL_ATTRIB (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tsf_entity_id   IN       TSF_ENTITY_L10N_EXT.TSF_ENTITY_ID%TYPE)

RETURN BOOLEAN IS
   L_program       VARCHAR2(50) := 'TSF_ENTITY_SQL.DEL_TSF_ENT_FISCAL_ATTRIB';
   L_table         VARCHAR2(30) := 'TSF_ENTITY_L10N_EXT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_TSF_ENTITY_L10N_EXT is
      select 'x'
       from tsf_entity_l10n_ext
      where tsf_entity_id = I_tsf_entity_id
        for update nowait;

BEGIN

   open C_LOCK_TSF_ENTITY_L10N_EXT;
   close C_LOCK_TSF_ENTITY_L10N_EXT;

   delete from tsf_entity_l10n_ext
         where tsf_entity_id = I_tsf_entity_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DEL_TSF_ENT_FISCAL_ATTRIB;
--------------------------------------------------------------------------
END TSF_ENTITY_SQL;
/
