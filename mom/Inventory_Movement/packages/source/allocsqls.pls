CREATE OR REPLACE PACKAGE ALLOCATION_SQL AUTHID CURRENT_USER AS

TYPE ROWID_TBL IS TABLE OF ROWID;
TYPE alloc_detail_rec is RECORD(to_locs          LOC_TBL,
                                to_loc_types     INDICATOR_TBL,
                                qtys             QTY_TBL,
                                in_store_dates   DATE_TBL);

TYPE item_locs_rec IS RECORD(items               ITEM_TBL,
                             locations           LOC_TBL,
                             loc_types           LOC_TYPE_TBL,
                             units               QTY_TBL);


TYPE itemloc_rec is RECORD(header          ITEM_LOC%ROWTYPE,
                           items_to_update ITEM_TBL,
                           locs_to_update  LOC_TBL,
                           qtys_to_update  QTY_TBL,
                           rows_to_update  ROWID_TBL);

TYPE alloc_rec is RECORD (header              ALLOC_HEADER%ROWTYPE,
                          alloc_detail        ALLOC_DETAIL_REC,
                          itemloc             ITEMLOC_REC,
                          new_itemloc         ITEM_LOCS_REC,
                          pack_ind            ITEM_MASTER.PACK_IND%TYPE,
                          to_loc_type         ITEM_LOC_SOH.LOC_TYPE%TYPE,
                          total_qty_allocated ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          origin_ind          ALLOC_HEADER.ORIGIN_IND%TYPE);
                         
---------------------------------------------------------------------------
END ALLOCATION_SQL;
/