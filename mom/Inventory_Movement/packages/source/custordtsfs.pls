CREATE OR REPLACE PACKAGE CUST_ORDER_TSF_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------------------
-- Name:    CHECK_EXT_CO_TSF
-- Purpose: This function will check if the transfer is an externally generated
--          Customer Order.
-------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXT_CO_TSF(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_ext_co_tsf     IN OUT VARCHAR2,
                          I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------------
END CUST_ORDER_TSF_SQL;
/
