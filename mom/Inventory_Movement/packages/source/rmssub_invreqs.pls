
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_INVREQ AUTHID CURRENT_USER AS


/* Function and Procedure Bodies */
-------------------------------------------------------------------------
PROCEDURE CONSUME (O_status_code          IN OUT  VARCHAR2,
                   O_error_message        IN OUT  VARCHAR2,
                   I_message              IN      RIB_OBJECT,
                   I_message_type         IN      VARCHAR2,
                   O_rib_error_tbl           OUT  RIB_ERROR_TBL);
-----------------------------------------------------------------------------------------
END RMSSUB_INVREQ;
/
