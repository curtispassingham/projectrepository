CREATE OR REPLACE PACKAGE BODY DLVYSLT_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_delete_ok        IN OUT  BOOLEAN,
                       I_delivery_slot_id IN      DELIVERY_SLOT.DELIVERY_SLOT_ID%TYPE)
RETURN BOOLEAN IS

   L_exists   VARCHAR2(1) := 'N';
   L_program  VARCHAR2(64) := 'DLVYSLT_SQL.CHECK_DELETE';

   -- Cursor to select the delivery slot from different tables where it is referenced.
   cursor C_CHECK_DELIVERY_SLOT is
   select 'Y'
     from store_orders
    where delivery_slot_id = I_delivery_slot_id
    union all
   select 'Y'
     from tsfhead
    where delivery_slot_id = I_delivery_slot_id
    union all
   select 'Y'
     from repl_results
    where delivery_slot_id = I_delivery_slot_id;

BEGIN
   O_delete_ok := TRUE;
   --
   if I_delivery_slot_id is NULL then
      O_error_message := 'INV_PARAMS';
      return FALSE;
   end if;
   --
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DELIVERY_SLOT',
                    'STORE_ORDERS, TSFHEAD, REPL_RESULTS',
                    I_delivery_slot_id);
   open  C_CHECK_DELIVERY_SLOT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DELIVERY_SLOT',
                    'STORE_ORDERS, TSFHEAD, REPL_RESULTS',
                    I_delivery_slot_id);

   fetch C_CHECK_DELIVERY_SLOT into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DELIVERY_SLOT',
                    'STORE_ORDERS, TSFHEAD, REPL_RESULTS',
                    I_delivery_slot_id);

   close C_CHECK_DELIVERY_SLOT;
   --
   if L_exists = 'Y' then
      O_delete_ok := FALSE;
      O_error_message := 'CANNOT_DELETE_DEL_SLOT';
   end if;
   --
   return TRUE;
   --
EXCEPTION
   when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_DELETE;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DELIVERY_SLOT_EXISTS (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists           IN OUT  BOOLEAN,
                                     I_delivery_slot_id IN      DELIVERY_SLOT.DELIVERY_SLOT_ID%TYPE)
RETURN BOOLEAN IS

   L_exists VARCHAR2(1) := 'N';
   L_program  VARCHAR2(64) := 'DLVYSLT_SQL.CHECK_DELIVERY_SLOT_EXISTS';

   cursor C_DELIVERY_SLOT is
   select 'Y'
     from delivery_slot
    where delivery_slot_id = I_delivery_slot_id;

BEGIN
   O_exists := FALSE;
   --
   if I_delivery_slot_id is NULL then
      O_error_message := 'INV_PARAMS';
      return FALSE;
   end if;
   --
   SQL_LIB.SET_MARK('OPEN',
                    'C_DELIVERY_SLOT',
                    'DELIVERY_SLOT',
                    I_delivery_slot_id);
   open  C_DELIVERY_SLOT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_DELIVERY_SLOT',
                    'DELIVERY_SLOT',
                    I_delivery_slot_id);
   fetch C_DELIVERY_SLOT into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_DELIVERY_SLOT',
                    'DELIVERY_SLOT',
                    I_delivery_slot_id);
   close C_DELIVERY_SLOT;
   --
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;

   return TRUE;
   --
EXCEPTION
   when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      O_exists := FALSE;
      return FALSE;

END CHECK_DELIVERY_SLOT_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_SEQUENCE_UNIQUE (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT  BOOLEAN)
RETURN BOOLEAN IS

   L_exists   VARCHAR2(1) := 'N';
   L_program  VARCHAR2(64) := 'DLVYSLT_SQL.CHECK_SEQUENCE_UNIQUE';

   cursor C_DUP_SEQUENCE is
   select 'Y'
     from delivery_slot
    group by delivery_slot_sequence
   having count(1) > 1;

BEGIN
   O_exists := FALSE;
   --
   SQL_LIB.SET_MARK('OPEN',
                    'C_DUP_SEQUENCE',
                    'DELIVERY_SLOT',
                    NULL);

   open  C_DUP_SEQUENCE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_DUP_SEQUENCE',
                    'DELIVERY_SLOT',
                    NULL);
   fetch C_DUP_SEQUENCE into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_DUP_SEQUENCE',
                    'DELIVERY_SLOT',
                    NULL);
   close C_DUP_SEQUENCE;

   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;

   return TRUE;
   --
EXCEPTION
   when OTHERS then
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
      --
END CHECK_SEQUENCE_UNIQUE;
---------------------------------------------------------------------------------------------
END DLVYSLT_SQL;
/

