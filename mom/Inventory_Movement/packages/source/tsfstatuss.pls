CREATE OR REPLACE PACKAGE TSF_STATUS_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------
--Function Name : APPROVE_TRANSFER
--Purpose       : This function attempts to approve a transfer. It is used 
--                by the Mobile Service and by the BI dashboard to update a
--                transfer to approved status. As such, only tranfers that 
--                can be updated in the RMS transfer screen can be processed here. 
--------------------------------------------------------------------------------
FUNCTION APPROVE_TRANSFER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------
--Function Name : UNAPPROVE_TRANSFER
--Purpose       : This function attempts to unapprove a transfer. It is used 
--                by the Mobile Service and by the BI dashboard to update a
--                transfer to unapproved status. As such, only tranfers that 
--                can be updated in the RMS transfer screen can be processed here. 
--------------------------------------------------------------------------------
FUNCTION UNAPPROVE_TRANSFER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------
--Function Name : UPDATE_TO_DELETE_STATUS
--Purpose       : This function attempts to update a transfer to deleted status. 
--                It is used by the Mobile Service and by the BI dashboard to 
--                delete a transfer. As such, only tranfers that can be updated
--                in the RMS transfer screen can be processed here. 
--------------------------------------------------------------------------------
FUNCTION UPDATE_TO_DELETE_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------
--Function Name : REJECT_TSF_NOTIFICATION
--Purpose       : This function creates notification when a transfer is rejected 
--                by the approver and is intended to notify the user that created 
--                the transfer. 
--------------------------------------------------------------------------------
FUNCTION REJECT_TSF_NOTIFICATION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END TSF_STATUS_SQL;
/
