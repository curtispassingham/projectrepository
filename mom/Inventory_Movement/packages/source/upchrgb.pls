CREATE OR REPLACE PACKAGE BODY UP_CHARGE_SQL AS
LP_system_options SYSTEM_OPTIONS%ROWTYPE;

----------------------------------------------------------------------------------------
-- Function Name: IS_FRANCHISE_TSF_ALLOC
-- Purpose      : This function evaluates if the from/to location combination results in
--                a franchise transaction.
----------------------------------------------------------------------------------------
FUNCTION IS_FRANCHISE_TSF_ALLOC(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_franchise_tsf_alloc_ind   IN OUT   VARCHAR2,
                                I_from_loc                  IN       ITEM_LOC.LOC%TYPE,
                                I_from_loc_type             IN       ITEM_LOC.LOC_TYPE%TYPE,
                                I_to_loc                    IN       ITEM_LOC.LOC%TYPE,
                                I_to_loc_type               IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION GET_COST_BASIS(O_error_message     IN OUT VARCHAR2,
                        O_cost_basis_prim   IN OUT ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE,
                        I_item              IN     ITEM_MASTER.ITEM%TYPE,
                        I_item_level        IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                        I_tran_level        IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                        I_std_av_ind        IN     SYSTEM_OPTIONS.STD_AV_IND%TYPE,
                        I_from_loc          IN     STORE.STORE%TYPE,
                        I_from_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(62)                       := 'UP_CHARGE_SQL.GET_COST_BASIS';
   L_item            ITEM_MASTER.ITEM%TYPE;
   L_amount          ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_total_amount    ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_value           ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_per_unit_value  ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_soh_exists      VARCHAR2(1)                        := 'Y';
   L_count           NUMBER;
   L_exists          BOOLEAN;
   L_supplier        SUPS.SUPPLIER%TYPE;

   cursor C_GET_UNIT_COST is
      select unit_cost, supplier
        from item_supp_country
       where item                = L_item
         and primary_supp_ind    = 'Y'
         and primary_country_ind = 'Y';

   cursor C_GET_LOC_COST is
      select unit_cost
        from item_loc_soh
       where item = L_item
         and loc  = I_from_loc;

   cursor C_GET_AV_COST is
      select av_cost
        from item_loc_soh
       where item = L_item
         and loc  = I_from_loc;

   cursor C_GET_CHILDREN is
      select item
        from item_master
       where item_parent  = I_item
         and item_level   = tran_level
      union all
      select item
        from item_master
       where item_grandparent = I_item
         and item_level       = tran_level;

BEGIN
   if I_item_level = I_tran_level then
      L_item := I_item;
      ---
      if I_std_av_ind = 'S' then
         -- if SOH record exists use item_loc_soh.unit_cost, else use prim supp/ctry unit cost
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_LOC_COST','ITEM_LOC',NULL);
         open C_GET_LOC_COST;
         SQL_LIB.SET_MARK('FETCH','C_GET_LOC_COST','ITEM_LOC',NULL);
         fetch C_GET_LOC_COST into L_amount;
         ---
         if C_GET_LOC_COST%NOTFOUND then
            L_soh_exists := 'N';
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_GET_LOC_COST','ITEM_LOC',NULL);
         close C_GET_LOC_COST;
      else  -- I_std_av_ind = 'A'
         SQL_LIB.SET_MARK('OPEN','C_GET_AV_COST','ITEM_LOC',NULL);
         open C_GET_AV_COST;
         SQL_LIB.SET_MARK('FETCH','C_GET_AV_COST','ITEM_LOC',NULL);
         fetch C_GET_AV_COST into L_amount;
         ---
         if C_GET_AV_COST%NOTFOUND then
            L_soh_exists := 'N';
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_GET_AV_COST','ITEM_LOC',NULL);
         close C_GET_AV_COST;
      end if;
      ---
      if L_soh_exists = 'Y' then
         ---
         -- Convert L_amount from local currency to primary currency.
         ---
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_amount,
                                             L_amount,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
      else  -- SOH does not exist, Get prim supp/ctry cost
         SQL_LIB.SET_MARK('OPEN','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY',NULL);
         open C_GET_UNIT_COST;
         SQL_LIB.SET_MARK('FETCH','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY',NULL);
         fetch C_GET_UNIT_COST into L_amount, L_supplier;
         SQL_LIB.SET_MARK('CLOSE','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY',NULL);
         close C_GET_UNIT_COST;
         ---
         -- Convert L_amount from supplier currency to primary currency.
         ---
         if L_supplier is NULL then
            L_amount := 0;
         else
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                L_supplier,
                                                'V',
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_amount,
                                                L_amount,
                                                'C',
                                                NULL,
                                                NULL) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   else  -- tran_level <> item_level
      ---
      -- Need to loop through all children items and get the average of the costs
      ---
      for C_rec in C_GET_CHILDREN loop
         L_item           := C_rec.item;
         L_soh_exists := 'Y';
         ---
         if I_std_av_ind = 'S' then
            -- if SOH record exists use item_loc_soh.unit_cost, else use prim supp/ctry unit cost
            ---
            SQL_LIB.SET_MARK('OPEN','C_GET_LOC_COST','ITEM_LOC',NULL);
            open C_GET_LOC_COST;
            SQL_LIB.SET_MARK('FETCH','C_GET_LOC_COST','ITEM_LOC',NULL);
            fetch C_GET_LOC_COST into L_amount;
            ---
            if C_GET_LOC_COST%NOTFOUND then
               L_soh_exists := 'N';
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE','C_GET_LOC_COST','ITEM_LOC',NULL);
            close C_GET_LOC_COST;
         else  -- I_std_av_ind = 'A'
            SQL_LIB.SET_MARK('OPEN','C_GET_AV_COST','ITEM_LOC',NULL);
            open C_GET_AV_COST;
            SQL_LIB.SET_MARK('FETCH','C_GET_AV_COST','ITEM_LOC',NULL);
            fetch C_GET_AV_COST into L_amount;
            ---
            if C_GET_AV_COST%NOTFOUND then
               L_soh_exists := 'N';
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE','C_GET_AV_COST','ITEM_LOC',NULL);
            close C_GET_AV_COST;
         end if;
         ---
         if L_soh_exists = 'Y' then
            ---
            -- Convert L_amount from local currency to primary currency.
            ---
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                I_from_loc,
                                                I_from_loc_type,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_amount,
                                                L_amount,
                                                'C',
                                                NULL,
                                                NULL) = FALSE then
               return FALSE;
            end if;
         else  -- SOH does not exist, Get prim supp/ctry cost
            SQL_LIB.SET_MARK('OPEN','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY',NULL);
            open C_GET_UNIT_COST;
            SQL_LIB.SET_MARK('FETCH','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY',NULL);
            fetch C_GET_UNIT_COST into L_amount, L_supplier;
            SQL_LIB.SET_MARK('CLOSE','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY',NULL);
            close C_GET_UNIT_COST;
            ---
            -- Convert L_amount from supplier currency to primary currency.
            ---
            if L_supplier is NULL then
               L_amount := 0;
            else
               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   L_supplier,
                                                   'V',
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_amount,
                                                   L_amount,
                                                   'C',
                                                   NULL,
                                                   NULL) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
         ---
         L_total_amount := L_total_amount + L_amount;
         L_count        := NVL(L_count,0) + 1;
      end loop;
      ---
      L_amount := L_total_amount / NVL(L_count,1);
   end if;
   ---
   O_cost_basis_prim := L_amount;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_COST_BASIS;
----------------------------------------------------------------------------------------
FUNCTION CALC_COMP(O_error_message     IN OUT VARCHAR2,
                   O_est_value_comp    IN OUT NUMBER,
                   O_cost_basis_comp   IN OUT NUMBER,
                   I_comp_id           IN     ELC_COMP.COMP_ID%TYPE,
                   I_item              IN     ITEM_MASTER.ITEM%TYPE,
                   I_from_loc          IN     STORE.STORE%TYPE,
                   I_from_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                   I_to_loc            IN     STORE.STORE%TYPE,
                   I_to_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                   I_tsf_alloc_type    IN     VARCHAR2,
                   I_tsf_alloc_no      IN     TSFDETAIL_CHRG.TSF_NO%TYPE,
                   I_tsf_seq_no        IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                   I_shipment          IN     SHIPMENT.SHIPMENT%TYPE,
                   I_ship_seq_no       IN     SHIPSKU.SEQ_NO%TYPE,
                   I_from_loc_cost     IN     SHIPSKU.UNIT_COST%TYPE DEFAULT NULL,
                   I_wrong_st_rcv_ind  IN     VARCHAR2 DEFAULT 'N',
                   I_pack_item         IN     ITEM_MASTER.ITEM%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program                   VARCHAR2(62)                           := 'UP_CHARGE_SQL.CALC_COMP';
   L_std_av_ind                SYSTEM_OPTIONS.STD_AV_IND%TYPE;
   L_amount_prim               NUMBER                                    := 0;
   L_comp_item_amount          NUMBER                                    := 0;
   L_est_value_prim            NUMBER                                    := 0;
   L_value                     NUMBER                                    := 0;
   L_per_unit_value            NUMBER                                    := 0;
   L_supplier                  SUPS.SUPPLIER%TYPE;
   L_calc_basis                ELC_COMP.CALC_BASIS%TYPE;
   L_comp_rate                 ELC_COMP.COMP_RATE%TYPE;
   L_comp_currency             CURRENCIES.CURRENCY_CODE%TYPE;
   L_per_count                 ELC_COMP.PER_COUNT%TYPE;
   L_per_count_uom             UOM_CLASS.UOM%TYPE;
   L_origin_country_id         COUNTRY.COUNTRY_ID%TYPE;
   L_supp_pack_size            ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_ship_carton_wt            ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_weight_uom                ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_ship_carton_len           ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_ship_carton_hgt           ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_ship_carton_wid           ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_dimension_uom             ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_liquid_volume             ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_liquid_volume_uom         ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;
   L_standard_uom              UOM_CLASS.UOM%TYPE;
   L_uom                       UOM_CLASS.UOM%TYPE;
   L_standard_class            UOM_CLASS.UOM_CLASS%TYPE;
   L_uom_class                 UOM_CLASS.UOM_CLASS%TYPE;
   L_uom_conv_factor           ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_unit_of_work              IF_ERRORS.UNIT_OF_WORK%TYPE;
   L_item_level                ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level                ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_pack_ind                  ITEM_MASTER.PACK_IND%TYPE;
   L_pack_type                 ITEM_MASTER.PACK_TYPE%TYPE;
   L_tsf_type                  TSFHEAD.TSF_TYPE%TYPE;
   L_tsf_price                 TSFDETAIL.TSF_PRICE%TYPE := 0;
   L_shipsku_value             SHIPSKU.UNIT_COST%TYPE;
   L_pct_in_pack               NUMBER;
   cursor C_GET_ITEM_INFO is
      select item_level,
             tran_level,
             pack_ind,
             pack_type
        from item_master
       where item = I_item;

   cursor C_ITEM_CHRG_INFO is
      select e.calc_basis,
             i.comp_rate,
             i.comp_currency,
             i.per_count,
             i.per_count_uom
        from elc_comp e,
             item_chrg_detail i
       where i.item     = I_item
         and i.from_loc = I_from_loc
         and i.to_loc   = I_to_loc
         and i.comp_id  = I_comp_id
         and i.comp_id  = e.comp_id;

   cursor C_TSF_CHRG_INFO is
      select e.calc_basis,
             t.comp_rate,
             t.comp_currency,
             t.per_count,
             t.per_count_uom
        from elc_comp e,
             tsfdetail_chrg t
       where t.tsf_no     = I_tsf_alloc_no
         and t.tsf_seq_no = nvl(I_tsf_seq_no, t.tsf_seq_no)
         and ((t.shipment         = I_shipment
               and I_shipment    is not NULL
               and t.ship_seq_no  = I_ship_seq_no
               and I_ship_seq_no is not NULL)
             or (t.shipment     is NULL
                 and I_shipment is NULL)
             or (t.shipment     is NULL
                 and I_shipment is not NULL))
         and t.from_loc   = I_from_loc
         and t.to_loc     = I_to_loc
         and t.item       = I_item
         and t.comp_id    = I_comp_id
         and t.comp_id    = e.comp_id;

   cursor C_ALLOC_CHRG_INFO is
      select e.calc_basis,
             a.comp_rate,
             a.comp_currency,
             a.per_count,
             a.per_count_uom
        from elc_comp e,
             alloc_chrg a
       where a.alloc_no = I_tsf_alloc_no
         and a.to_loc   = I_to_loc
         and a.item     = I_item
         and a.comp_id  = I_comp_id
         and a.comp_id  = e.comp_id;

   cursor C_GET_PACK_ITEMS is
      select item,
             qty
        from v_packsku_qty
       where pack_no = I_item;

   cursor C_GET_DIMENSION is
      select i.supplier,
             i.origin_country_id,
             i.supp_pack_size,
             d.weight,
             d.weight_uom,
             d.length,
             d.height,
             d.width,
             d.lwh_uom,
             d.liquid_volume,
             d.liquid_volume_uom
        from item_supp_country i,
             item_supp_country_dim d
       where i.item                = d.item
         and i.supplier            = d.supplier
         and i.origin_country_id   = d.origin_country
         and d.dim_object          = 'CA'
         and i.item                = I_item
         and i.primary_supp_ind    = 'Y'
         and i.primary_country_ind = 'Y';

   cursor C_GET_MISC_VALUE is
      select value
        from item_supp_uom
       where item     = I_item
         and supplier = L_supplier
         and uom      = L_per_count_uom;

   cursor C_GET_TSF_INFO is
      select tsf_type,
             tsf_price
        from tsfhead th,
             tsfdetail td
       where th.tsf_no = td.tsf_no
         and th.tsf_no = I_tsf_alloc_no
         and td.item = NVL(I_pack_item,I_item);

   cursor c_get_shipsku_value is
       select shk.unit_cost
         from shipsku shk
        where seq_no = I_ship_seq_no
          and item = NVL(I_pack_item, I_item)
          and shipment = I_shipment;

BEGIN
   O_est_value_comp  := 0;
   O_cost_basis_comp := 0;
   ---
   if I_tsf_alloc_no is NULL then
      SQL_LIB.SET_MARK('OPEN','C_ITEM_CHRG_INFO','ITEM_CHRG_DETAIL, ELC_COMP',NULL);
      open C_ITEM_CHRG_INFO;
      SQL_LIB.SET_MARK('FETCH','C_ITEM_CHRG_INFO','ITEM_CHRG_DETAIL, ELC_COMP',NULL);
      fetch C_ITEM_CHRG_INFO into L_calc_basis,
                                  L_comp_rate,
                                  L_comp_currency,
                                  L_per_count,
                                  L_per_count_uom;
      if C_ITEM_CHRG_INFO%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COMP',NULL,NULL,NULL);
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_CHRG_INFO','ITEM_CHRG_DETAIL, ELC_COMP',NULL);
         close C_ITEM_CHRG_INFO;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_CHRG_INFO','ITEM_CHRG_DETAIL, ELC_COMP',NULL);
      close C_ITEM_CHRG_INFO;
   else
      if I_tsf_alloc_type = 'T' then
         SQL_LIB.SET_MARK('OPEN','C_TSF_CHRG_INFO','TSFDETAIL_CHRG, ELC_COMP',NULL);
         open C_TSF_CHRG_INFO;
         SQL_LIB.SET_MARK('FETCH','C_TSF_CHRG_INFO','TSFDETAIL_CHRG, ELC_COMP',NULL);
         fetch C_TSF_CHRG_INFO into L_calc_basis,
                                    L_comp_rate,
                                    L_comp_currency,
                                    L_per_count,
                                    L_per_count_uom;
         if C_TSF_CHRG_INFO%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_COMP',NULL,NULL,NULL);
            SQL_LIB.SET_MARK('CLOSE','C_TSF_CHRG_INFO','TSFDETAIL_CHRG, ELC_COMP',NULL);
            close C_TSF_CHRG_INFO;
            return FALSE;
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_TSF_CHRG_INFO','TSFDETAIL_CHRG, ELC_COMP',NULL);
         close C_TSF_CHRG_INFO;
      else
         SQL_LIB.SET_MARK('OPEN','C_ALLOC_CHRG_INFO','ALLOC_CHRG, ELC_COMP',NULL);
         open C_ALLOC_CHRG_INFO;
         SQL_LIB.SET_MARK('FETCH','C_ALLOC_CHRG_INFO','ALLOC_CHRG, ELC_COMP',NULL);
         fetch C_ALLOC_CHRG_INFO into L_calc_basis,
                                      L_comp_rate,
                                      L_comp_currency,
                                      L_per_count,
                                      L_per_count_uom;
         if C_ALLOC_CHRG_INFO%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_COMP',NULL,NULL,NULL);
            SQL_LIB.SET_MARK('CLOSE','C_ALLOC_CHRG_INFO','ALLOC_CHRG, ELC_COMP',NULL);
            close C_ALLOC_CHRG_INFO;
            return FALSE;
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_ALLOC_CHRG_INFO','ALLOC_CHRG, ELC_COMP',NULL);
         close C_ALLOC_CHRG_INFO;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_TSF_INFO','TSFHEAD','Tsf: '||I_tsf_alloc_no);
   open C_GET_TSF_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_TSF_INFO','TSFHEAD','Tsf: '||I_tsf_alloc_no);
   fetch C_GET_TSF_INFO into L_tsf_type,
                             L_tsf_price;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TSF_INFO','TSFHEAD','Tsf: '||I_tsf_alloc_no);
   close C_GET_TSF_INFO;
   ---
   if L_calc_basis = 'V' then    -- The component is a 'Value' based component.
      if SYSTEM_OPTIONS_SQL.STD_AV_IND(O_error_message,
                                       L_std_av_ind) = FALSE then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_ITEM_INFO','ITEM_MASTER','Item: '||I_item);
      open C_GET_ITEM_INFO;
      SQL_LIB.SET_MARK('FETCH','C_GET_ITEM_INFO','ITEM_MASTER','Item: '||I_item);
      fetch C_GET_ITEM_INFO into L_item_level,
                                 L_tran_level,
                                 L_pack_ind,
                                 L_pack_type;
      SQL_LIB.SET_MARK('CLOSE','C_GET_ITEM_INFO','ITEM_MASTER','Item: '||I_item);
      close C_GET_ITEM_INFO;
      ---

    if I_wrong_st_rcv_ind = 'Y' and (NVL(I_from_loc_cost, -1) != -1) then
         l_amount_prim := I_from_loc_cost;
    else
      if L_pack_ind = 'Y' then
         open C_GET_SHIPSKU_VALUE;
         fetch c_get_shipsku_value into l_shipsku_value;
         if C_GET_SHIPSKU_VALUE%FOUND then
            l_amount_prim := (l_shipsku_value /(1+ l_comp_rate/100));

            --convert l_amount_prim from from_loc currency to Primary currency currency
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                I_from_loc,
                                                I_from_loc_type,
                                                NULL,
                                                null,
                                                NULL,
                                                NULL,
                                                l_amount_prim,
                                                l_amount_prim,
                                                'C',
                                                NULL,
                                                NULL) = FALSE then
                  return FALSE;
            end if;
         else
           for C_rec in C_GET_PACK_ITEMS loop

             if GET_COST_BASIS(O_error_message,
                               L_comp_item_amount,
                               C_rec.item,
                               L_item_level,
                               L_tran_level,
                               L_std_av_ind,
                               I_from_loc,
                               I_from_loc_type) = FALSE then
                return FALSE;
             end if;
            ---
             l_amount_prim := NVL(l_amount_prim, 0) + (l_comp_item_amount * c_rec.qty);
         end loop;
         end if;
         close C_GET_SHIPSKU_VALUE;
      else

         open C_GET_SHIPSKU_VALUE;
         fetch C_GET_SHIPSKU_VALUE into l_shipsku_value;
         if I_pack_item is not NULL then
            if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                          L_pct_in_pack,
                                          I_pack_item,
                                          I_item,
                                          I_from_loc) = FALSE then
                return FALSE;
            end if;
            l_amount_prim := (l_shipsku_value /(1+ l_comp_rate/100)) * L_pct_in_pack;
         else
            l_amount_prim := (l_shipsku_value /(1+ l_comp_rate/100));
         end if;

            --convert l_amount_prim from from loc currency to Primary currency currency
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                I_from_loc,
                                                I_from_loc_type,
                                                NULL,
                                                null,
                                                NULL,
                                                NULL,
                                                l_amount_prim,
                                                l_amount_prim,
                                                'C',
                                                NULL,
                                                NULL) = FALSE then
                  return FALSE;
            end if;

         if C_GET_SHIPSKU_VALUE%NOTFOUND then
            if GET_COST_BASIS(O_error_message,
                              L_amount_prim,
                              I_item,
                              L_item_level,
                              L_tran_level,
                              L_std_av_ind,
                              I_from_loc,
                              I_from_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
        close C_GET_SHIPSKU_VALUE;
      end if;
    end if;
      ---
      -- Calculate the Estimate Value.  When the Calculation Basis is Value, the
      -- value is simply a percentage of an amount.
      ---
      if L_tsf_type in ('SG','IC') and L_tsf_price is not NULL then
         if I_pack_item is not NULL then
            if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                             L_pct_in_pack,
                                             I_pack_item,
                                             I_item,
                                             I_from_loc) = FALSE then
                return FALSE;
            end if;
            L_est_value_prim := (L_tsf_price * (L_comp_rate/100)) * L_pct_in_pack;
         else
            L_est_value_prim := L_tsf_price * (L_comp_rate/100);
         end if;
         -- Convert L_est_value_prim from from_loc currency to Primary currency as Tsf Price is in From Loc currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_est_value_prim,
                                             L_est_value_prim,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
      else
         L_est_value_prim := L_amount_prim * (L_comp_rate/100);
      end if;
      ---
      -- Convert O_est_value_comp from primary currency to the Component's currency.
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_est_value_prim,
                              NULL,
                              L_comp_currency,
                              O_est_value_comp,
                              'C',
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              'N') = FALSE then
         return FALSE;
      end if;
      ---
      -- Convert L_amount_prim from primary currency to the Component's currency.
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_amount_prim,
                              NULL,
                              L_comp_currency,
                              O_cost_basis_comp,
                              'C',
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              'N') = FALSE then
         return FALSE;
      end if;
   elsif L_calc_basis = 'S' then  -- The expense is a 'Specific' expense.
      ---
      -- Get all of the Dimension information.
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DIMENSION','ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_DIM',NULL);
      open C_GET_DIMENSION;
      SQL_LIB.SET_MARK('FETCH','C_GET_DIMENSION','ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_DIM',NULL);
      fetch C_GET_DIMENSION into L_supplier,
                                 L_origin_country_id,
                                 L_supp_pack_size,
                                 L_ship_carton_wt,
                                 L_weight_uom,
                                 L_ship_carton_len,
                                 L_ship_carton_hgt,
                                 L_ship_carton_wid,
                                 L_dimension_uom,
                                 L_liquid_volume,
                                 L_liquid_volume_uom;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DIMENSION','ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_DIM',NULL);
      close C_GET_DIMENSION;
      ---
      if UOM_SQL.GET_CLASS(O_error_message,
                           L_uom_class,
                           L_per_count_uom) = FALSE then
         return FALSE;
      end if;
      ---
      if L_uom_class in ('QTY') then
         if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                             L_standard_uom,
                                             L_standard_class,
                                             L_per_unit_value, -- standard UOM conversion factor
                                             I_item,
                                             'N') = FALSE then
            return FALSE;
         end if;
         ---
         if L_per_unit_value is NULL then
            if L_standard_uom <> 'EA' then
               L_per_unit_value := 0;
               ---
               if I_tsf_alloc_no is NULL then
                  L_unit_of_work   := 'Item '||I_item||
                                      ', Component '||I_comp_id;
               else
                  L_unit_of_work   := 'Transfer/Allocation '||to_char(I_tsf_alloc_no)||
                                      ', Item '||I_item||
                                      ', Component '||I_comp_id;
               end if;
               ---
               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                  SQL_LIB.GET_MESSAGE_TEXT('NO_CONV_FACTOR',
                                                           I_item,
                                                           NULL,
                                                           NULL),
                                                           L_program,
                                                           L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
            else
               L_per_unit_value := 1;
            end if;
         end if;
         ---
         L_uom  := 'EA';
      elsif L_uom_class = 'PACK' then
         L_value := 1 / L_supp_pack_size;
      elsif L_uom_class = 'MISC' then
         SQL_LIB.SET_MARK('OPEN','C_GET_MISC_VALUE','ITEM_SUPP_UOM', NULL);
         open C_GET_MISC_VALUE;
         SQL_LIB.SET_MARK('FETCH','C_GET_MISC_VALUE','ITEM_SUPP_UOM', NULL);
         fetch C_GET_MISC_VALUE into L_value;
         ---
         if C_GET_MISC_VALUE%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE','C_GET_MISC_VALUE','ITEM_SUPP_UOM', NULL);
            close C_GET_MISC_VALUE;
            L_value        := 0;
            ---
            if I_tsf_alloc_no is NULL then
               L_unit_of_work := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                 ', Component '||I_comp_id;
            else
               L_unit_of_work := 'Transfer/Allocation '||to_char(I_tsf_alloc_no)||
                                 ', Item '||I_item||', Supplier '||to_char(L_supplier)||
                                 ', Component '||I_comp_id;
            end if;
            ---
            if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                               SQL_LIB.GET_MESSAGE_TEXT('NO_MISC_CONV_INFO',
                                                        I_item,
                                                        to_char(L_supplier),
                                                        NULL),
                                                        L_program,
                                                        L_unit_of_work) = FALSE then
               return FALSE;
            end if;
         else
            SQL_LIB.SET_MARK('CLOSE','C_GET_MISC_VALUE','ITEM_SUPP_UOM', NULL);
            close C_GET_MISC_VALUE;
         end if;
         ---
      elsif L_uom_class = 'MASS' then
         if L_ship_carton_wt is NULL then
            L_per_unit_value := 0;
            ---
            if I_tsf_alloc_no is NULL then
               L_unit_of_work   := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                   ', Origin Country '||L_origin_country_id||
                                   ', Component '||I_comp_id;
            else
               L_unit_of_work   := 'Transfer/Allocation '||to_char(I_tsf_alloc_no)||
                                   ', Item '||I_item||', Supplier '||to_char(L_supplier)||
                                   ', Origin Country '||L_origin_country_id||
                                   ', Component '||I_comp_id;
            end if;
            ---
            if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                              SQL_LIB.GET_MESSAGE_TEXT('NO_WT_INFO',
                                                       I_item,
                                                       to_char(L_supplier),
                                                       L_origin_country_id),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
               return FALSE;
            end if;
        else
            L_per_unit_value := L_ship_carton_wt/L_supp_pack_size;
            L_uom := L_weight_uom;
        end if;

      elsif L_uom_class = 'LVOL' then
         if L_liquid_volume_uom is NULL then
            L_per_unit_value := 0;
            ---
            if I_tsf_alloc_no is NULL then
               L_unit_of_work   := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                   ', Origin Country '||L_origin_country_id||
                                   ', Component '||I_comp_id;
            else
               L_unit_of_work   := 'Transfer/Allocation '||to_char(I_tsf_alloc_no)||
                                   ', Item '||I_item||', Supplier '||to_char(L_supplier)||
                                   ', Origin Country '||L_origin_country_id||
                                   ', Component '||I_comp_id;
            end if;
            ---
            if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                            SQL_LIB.GET_MESSAGE_TEXT('NO_LVOL_INFO',
                                                     I_item,
                                                     to_char(L_supplier),
                                                     L_origin_country_id),
                                                     L_program,
                                                     L_unit_of_work) = FALSE then
               return FALSE;
            end if;
         else
            L_per_unit_value := L_liquid_volume/L_supp_pack_size;
            L_uom := L_liquid_volume_uom;
         end if;
      elsif L_uom_class = 'VOL' then
         if L_ship_carton_len is NULL or L_ship_carton_wid is NULL or L_ship_carton_hgt is NULL then
            L_per_unit_value := 0;
            ---
            if I_tsf_alloc_no is NULL then
               L_unit_of_work   := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                   ', Origin Country '||L_origin_country_id||
                                   ', Component '||I_comp_id;
            else
               L_unit_of_work   := 'Transfer/Allocation '||to_char(I_tsf_alloc_no)||
                                   ', Item '||I_item||', Supplier '||to_char(L_supplier)||
                                   ', Origin Country '||L_origin_country_id||
                                   ', Component '||I_comp_id;
            end if;
            ---
            if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                           SQL_LIB.GET_MESSAGE_TEXT('NO_DIMEN_INFO',
                                                    I_item,
                                                    to_char(L_supplier),
                                                    L_origin_country_id),
                                                    L_program,
                                                    L_unit_of_work) = FALSE then
               return FALSE;
            end if;
         else
            ---
            -- Get the per unit ship carton volume.
            ---
            L_per_unit_value := (L_ship_carton_len * L_ship_carton_hgt * L_ship_carton_wid)/L_supp_pack_size;
            L_uom            := L_dimension_uom||'3';
         end if;
      elsif L_uom_class = 'AREA' then
         if L_ship_carton_len is NULL or L_ship_carton_wid is NULL then
            L_per_unit_value := 0;
            ---
            if I_tsf_alloc_no is NULL then
               L_unit_of_work   := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                   ', Origin Country '||L_origin_country_id||
                                   ', Component '||I_comp_id;
            else
               L_unit_of_work   := 'Transfer/Allocation '||to_char(I_tsf_alloc_no)||
                                   ', Item '||I_item||', Supplier '||to_char(L_supplier)||
                                   ', Origin Country '||L_origin_country_id||
                                   ', Component '||I_comp_id;
            end if;
            ---
            if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                           SQL_LIB.GET_MESSAGE_TEXT('NO_DIMEN_INFO',
                                                    I_item,
                                                    to_char(L_supplier),
                                                    L_origin_country_id),
                                                    L_program,
                                                    L_unit_of_work) = FALSE then
               return FALSE;
            end if;
         else
            ---
            -- Get the per unit ship carton area.
            ---
            L_per_unit_value := (L_ship_carton_len * L_ship_carton_wid)/L_supp_pack_size;
            L_uom            := L_dimension_uom||'2';
         end if;
      elsif L_uom_class = 'DIMEN' then
         if L_ship_carton_len is NULL then
            L_per_unit_value := 0;
            ---
            if I_tsf_alloc_no is NULL then
               L_unit_of_work   := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                   ', Origin Country '||L_origin_country_id||
                                   ', Component '||I_comp_id;
            else
               L_unit_of_work   := 'Transfer/Allocation '||to_char(I_tsf_alloc_no)||
                                   ', Item '||I_item||', Supplier '||to_char(L_supplier)||
                                   ', Origin Country '||L_origin_country_id||
                                   ', Component '||I_comp_id;
            end if;
            ---
            if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                           SQL_LIB.GET_MESSAGE_TEXT('NO_DIMEN_INFO',
                                                    I_item,
                                                    to_char(L_supplier),
                                                    L_origin_country_id),
                                                    L_program,
                                                    L_unit_of_work) = FALSE then
               return FALSE;
            end if;
         else
            ---
            -- Get the per unit ship carton length.
            ---
            L_per_unit_value := L_ship_carton_len/L_supp_pack_size;
            L_uom            := L_dimension_uom;
         end if;
      end if;
      ---
      if L_uom_class in ('VOL','AREA','DIMEN','QTY','MASS','LVOL') then
         if L_per_unit_value <> 0 then
            if UOM_SQL.WITHIN_CLASS(O_error_message,
                                    L_value,
                                    L_per_count_uom,
                                    L_per_unit_value,
                                    L_uom,
                                    L_uom_class) = FALSE then
               return FALSE;
            end if;
         else
            L_value := 0;
         end if;
      end if;
      ---
      -- Calculate the Estimate Expense Value
      ---
      O_est_value_comp := L_value * (L_comp_rate/L_per_count);

   end if; -- if calc_basis = 'S' ('Specific')
   ---
   if O_est_value_comp is NULL then
      O_est_value_comp := 0;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALC_COMP;
-------------------------------------------------------------------------------
FUNCTION CALC_COMP_VALUE(O_error_message     IN OUT VARCHAR2,
                         O_comp_value        IN OUT NUMBER,
                         I_tsf_alloc_type    IN     VARCHAR2,
                         I_tsf_alloc_no      IN     TSFHEAD.TSF_NO%TYPE,
                         I_tsf_seq_no        IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                         I_shipment          IN     SHIPMENT.SHIPMENT%TYPE,
                         I_ship_seq_no       IN     SHIPSKU.SEQ_NO%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_from_loc          IN     STORE.STORE%TYPE,
                         I_from_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_to_loc            IN     STORE.STORE%TYPE,
                         I_to_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_comp_id           IN     ELC_COMP.COMP_ID%TYPE,
                         I_comp_currency     IN     CURRENCIES.CURRENCY_CODE%TYPE,
                         I_from_loc_cost     IN     SHIPSKU.UNIT_COST%TYPE DEFAULT NULL,
                         I_wrong_st_rcv_ind  IN     VARCHAR2 DEFAULT 'N',
                         I_pack_item         IN     ITEM_MASTER.ITEM%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_comp_value  NUMBER := 0;
   L_cost_basis  NUMBER := 0;

BEGIN
   if CALC_COMP(O_error_message,
                L_comp_value,
                L_cost_basis,
                I_comp_id,
                I_item,
                I_from_loc,
                I_from_loc_type,
                I_to_loc,
                I_to_loc_type,
                I_tsf_alloc_type,
                I_tsf_alloc_no,
                I_tsf_seq_no,
                I_shipment,
                I_ship_seq_no,
                I_from_loc_cost,
                I_wrong_st_rcv_ind,
                I_pack_item) = FALSE then
      return FALSE;
   end if;
   ---
   -- Convert L_comp_value from componenet currency to primary currency.
   ---
   if CURRENCY_SQL.CONVERT(O_error_message,
                           L_comp_value,
                           I_comp_currency,
                           NULL,
                           O_comp_value,
                           'C',
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                          'N') = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'UP_CHARGE_SQL.CALC_COMP_VALUE',
                                            to_char(SQLCODE));
      return FALSE;
END CALC_COMP_VALUE;
-------------------------------------------------------------------------------
FUNCTION CALC_GROUP(O_error_message    IN OUT   VARCHAR2,
                    O_value_prim       IN OUT   ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE,
                    I_tsf_alloc_type   IN       VARCHAR2,
                    I_tsf_alloc_no     IN       TSFHEAD.TSF_NO%TYPE,
                    I_tsf_type         IN       TSFHEAD.TSF_TYPE%TYPE,
                    I_tsf_seq_no       IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                    I_item             IN       ITEM_MASTER.ITEM%TYPE,
                    I_pack_item        IN       ITEM_MASTER.ITEM%TYPE,
                    I_from_loc         IN       STORE.STORE%TYPE,
                    I_from_loc_type    IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_to_loc           IN       STORE.STORE%TYPE,
                    I_to_loc_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_up_chrg_group    IN       ELC_COMP.UP_CHRG_GROUP%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(62)                       := 'UP_CHARGE_SQL.CALC_GROUP';
   L_group_value      ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_comp_value       ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_item_tsf_qty     TSFDETAIL.TSF_QTY%TYPE             := 0;
   L_loc_group_value  ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_count            BINARY_INTEGER                     := 0;
   L_shipped          BOOLEAN                            := FALSE;
   L_transferred      BOOLEAN                            := FALSE;
   L_phw_ind          VARCHAR2(1)                        := 'N';

   cursor C_GET_SHIP_LOCS is
      select shipment,
             seq_no,
             from_loc,
             to_loc,
             from_loc_type,
             to_loc_type,
             tsf_qty
        from shipitem_inv_flow
       where tsf_no     = I_tsf_alloc_no
         and tsf_seq_no = nvl(I_tsf_seq_no, tsf_seq_no)
         and item       = I_item;

   cursor C_GET_TSF_COMPS is
      select distinct comp_id,
             comp_currency
        from tsfdetail_chrg
       where tsf_no      = I_tsf_alloc_no
         and tsf_seq_no  = nvl(I_tsf_seq_no, tsf_seq_no)
         and item        = I_item
         and ((pack_item = I_pack_item
               and I_pack_item   is not NULL)
             or (pack_item       is NULL
                 and I_pack_item is NULL))
         and up_chrg_group = I_up_chrg_group;

   cursor C_GET_ALLOC_COMPS is
      select distinct comp_id,
             comp_currency
        from alloc_chrg
       where alloc_no    = I_tsf_alloc_no
         and item        = I_item
         and ((pack_item = I_pack_item
               and I_pack_item   is not NULL)
             or (pack_item       is NULL
                 and I_pack_item is NULL))
         and to_loc        = I_to_loc
         and up_chrg_group = I_up_chrg_group;

   cursor C_GET_ITEM_COMPS is
      select distinct comp_id,
             comp_currency
        from item_chrg_detail
       where item = I_item
         and from_loc = I_from_loc
         and to_loc = I_to_loc
         and up_chrg_group = I_up_chrg_group;

   cursor C_GET_TSF_DIST is
      select tif.from_loc_type,
             tif.from_loc,
             tif.tsf_qty,
             tif.to_loc_type,
             tif.to_loc
        from tsfitem_inv_flow tif
       where tif.tsf_no = I_tsf_alloc_no
         and tif.tsf_seq_no = I_tsf_seq_no
         and tif.item   = nvl(I_pack_item, I_item);

   cursor C_CHK_PWH(L_wh   IN   WH.WH%TYPE) is
      select 'Y'
        from wh
       where wh = L_wh
         and wh = physical_wh;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;

   if I_tsf_alloc_type = 'T' and I_tsf_type = 'CO' then
      if I_from_loc_type = 'W' or I_to_loc_type = 'W' then
         open C_CHK_PWH(I_from_loc);
         fetch C_CHK_PWH into L_phw_ind;
         close C_CHK_PWH;
         if L_phw_ind = 'N' then
            open C_CHK_PWH(I_to_loc);
            fetch C_CHK_PWH into L_phw_ind;
            close C_CHK_PWH;
         end if;
      end if;
   end if;

   if I_tsf_alloc_type = 'A' then
      for C_rec in C_GET_ALLOC_COMPS loop
         if CALC_COMP_VALUE(O_error_message,
                            L_comp_value,
                            I_tsf_alloc_type,
                            I_tsf_alloc_no,
                            NULL,
                            NULL,
                            NULL,
                            I_item,
                            I_from_loc,
                            I_from_loc_type,
                            I_to_loc,
                            I_to_loc_type,
                            C_rec.comp_id,
                            C_rec.comp_currency,
                            NULL,
                            NULL,
                            I_pack_item) = FALSE then
            return FALSE;
         end if;
         ---
         L_group_value := L_group_value + L_comp_value;
      end loop;
      ---
      O_value_prim := L_group_value;
      ---
   elsif I_tsf_alloc_type = 'T' and
         (I_tsf_type = 'EG' or L_phw_ind = 'Y') and
         (I_from_loc_type = 'W' or I_to_loc_type = 'W') then
      ---
      -- If the transfer involves a physical warehouse, then need to loop through all of the inventory flows
      ---
      for L_rec in C_GET_SHIP_LOCS loop
         L_group_value := 0;
         L_shipped := TRUE;
         for C_rec in C_GET_TSF_COMPS loop
            if CALC_COMP_VALUE(O_error_message,
                               L_comp_value,
                               I_tsf_alloc_type,
                               I_tsf_alloc_no,
                               I_tsf_seq_no,
                               L_rec.shipment,
                               L_rec.seq_no,
                               I_item,
                               L_rec.from_loc,
                               L_rec.from_loc_type,
                               L_rec.to_loc,
                               L_rec.to_loc_type,
                               C_rec.comp_id,
                               C_rec.comp_currency,
                               NULL,
                               NULL,
                               I_pack_item) = FALSE then
               return FALSE;
            end if;
            ---
            L_group_value := L_group_value + L_comp_value;
         end loop;
         ---
         L_loc_group_value := L_loc_group_value + (L_group_value * L_rec.tsf_qty);
         L_item_tsf_qty    := L_item_tsf_qty    + L_rec.tsf_qty;
      end loop;

      if L_shipped = TRUE then
         O_value_prim := L_loc_group_value / L_item_tsf_qty;
      elsif L_shipped = FALSE then
         for L_rec in C_GET_TSF_DIST loop
            for C_rec in C_GET_TSF_COMPS loop
               if CALC_COMP_VALUE(O_error_message,
                                  L_comp_value,
                                  I_tsf_alloc_type,
                                  I_tsf_alloc_no,
                                  I_tsf_seq_no,
                                  NULL,
                                  NULL,
                                  I_item,
                                  L_rec.from_loc,
                                  L_rec.from_loc_type,
                                  L_rec.to_loc,
                                  L_rec.to_loc_type,
                                  C_rec.comp_id,
                                  C_rec.comp_currency,
                                  NULL,
                                  NULL,
                                  I_pack_item) = FALSE then
                  return FALSE;
               end if;
               ---
               L_group_value := L_group_value + L_comp_value;
            end loop;
            ---
            L_transferred := TRUE;
            L_loc_group_value := L_loc_group_value + (L_group_value * L_rec.tsf_qty);
            L_item_tsf_qty    := L_item_tsf_qty    + L_rec.tsf_qty;
         end loop;
         if L_transferred = TRUE then
            O_value_prim := L_loc_group_value / L_item_tsf_qty;
         end if;
      end if;

      -- For store to store EG transfers there will be no distribution info
      if L_shipped = FALSE and L_transferred = FALSE then
         for C_rec in C_GET_TSF_COMPS loop
         ---
            if CALC_COMP_VALUE(O_error_message,
                               L_comp_value,
                               I_tsf_alloc_type,
                               I_tsf_alloc_no,
                               I_tsf_seq_no,
                               NULL,
                               NULL,
                               I_item,
                               I_from_loc,
                               I_from_loc_type,
                               I_to_loc,
                               I_to_loc_type,
                               C_rec.comp_id,
                               C_rec.comp_currency,
                               NULL,
                               NULL,
                               I_pack_item) = FALSE then
               return FALSE;
            end if;
            ---
            L_group_value := L_group_value + L_comp_value;
            ---
         end loop;
         ---
         O_value_prim := L_group_value;
      end if;
   else
      if I_tsf_alloc_no is not null then
         ---
         for C_rec in C_GET_TSF_COMPS loop
            ---
            if CALC_COMP_VALUE(O_error_message,
                               L_comp_value,
                               I_tsf_alloc_type,
                               I_tsf_alloc_no,
                               I_tsf_seq_no,
                               NULL,
                               NULL,
                               I_item,
                               I_from_loc,
                               I_from_loc_type,
                               I_to_loc,
                               I_to_loc_type,
                               C_rec.comp_id,
                               C_rec.comp_currency,
                               NULL,
                               NULL,
                               I_pack_item) = FALSE then
               return FALSE;
            end if;
            ---
            L_group_value := L_group_value + L_comp_value;
            ---
         end loop;
         ---
         O_value_prim := L_group_value;
         ---
      else  -- transfer or allocation no is null
         ---
         for C_rec in C_GET_ITEM_COMPS loop
            ---
            if CALC_COMP_VALUE(O_error_message,
                               L_comp_value,
                               I_tsf_alloc_type,
                               I_tsf_alloc_no,
                               I_tsf_seq_no,
                               NULL,
                               NULL,
                               I_item,
                               I_from_loc,
                               I_from_loc_type,
                               I_to_loc,
                               I_to_loc_type,
                               C_rec.comp_id,
                               C_rec.comp_currency,
                               NULL,
                               NULL,
                               I_pack_item) = FALSE then
               return FALSE;
            end if;
            ---
            L_group_value := L_group_value + L_comp_value;
         end loop;
         ---
         O_value_prim := L_group_value;
         ---
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALC_GROUP;
-------------------------------------------------------------------------------
FUNCTION CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message         IN OUT   VARCHAR2,
                                       O_total_chrgs_prim      IN OUT   ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE,
                                       O_profit_chrgs_to_loc   IN OUT   NUMBER,
                                       O_exp_chrgs_to_loc      IN OUT   NUMBER,
                                       I_tsf_alloc_type        IN       VARCHAR2,
                                       I_tsf_alloc_no          IN       TSFHEAD.TSF_NO%TYPE,
                                       I_tsf_seq_no            IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                                       I_shipment              IN       SHIPMENT.SHIPMENT%TYPE,
                                       I_ship_seq_no           IN       SHIPSKU.SEQ_NO%TYPE,
                                       I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                       I_pack_item             IN       ITEM_MASTER.ITEM%TYPE,
                                       I_from_loc              IN       STORE.STORE%TYPE,
                                       I_from_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                                       I_to_loc                IN       STORE.STORE%TYPE,
                                       I_to_loc_type           IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(62) := 'UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS';
   L_orderable_ind             ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_ind                  ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind              ITEM_MASTER.SELLABLE_IND%TYPE;
   L_pack_type                 ITEM_MASTER.PACK_TYPE%TYPE;
   L_cost_basis                NUMBER                             := 0;
   L_total_value               NUMBER                             := 0;
   L_pack_value                NUMBER                             := 0;
   L_comp_value                NUMBER                             := 0;
   L_total_profit_value        NUMBER                             := 0;
   L_total_exp_value           NUMBER                             := 0;
   L_pack_profit_value         NUMBER                             := 0;
   L_pack_exp_value            NUMBER                             := 0;
   L_item                      ITEM_MASTER.ITEM%TYPE              := I_item;
   L_pack_item                 ITEM_MASTER.ITEM%TYPE;
   L_franchise_tsf_alloc_ind   VARCHAR2(1);

   cursor C_GET_PACK_ITEMS is
      select item,
             qty
        from v_packsku_qty
       where pack_no = I_item;

   cursor C_GET_PROFIT_COMPS is
      select t.comp_id,
             t.comp_currency
        from tsfdetail_chrg t,
             elc_comp e
       where t.tsf_no      = I_tsf_alloc_no
         and t.tsf_seq_no  = I_tsf_seq_no
         and ((t.shipment         = I_shipment
               and I_shipment    is not NULL
               and t.ship_seq_no  = I_ship_seq_no
               and I_ship_seq_no is not NULL)
             or (t.shipment     is NULL
                 and I_shipment is NULL)
             or (t.shipment     is NULL
                 and I_shipment is not NULL))
         and t.from_loc    = I_from_loc
         and t.to_loc      = I_to_loc
         and t.item        = L_item
         and ((t.pack_item = L_pack_item
               and L_pack_item     is not NULL
               and t.pack_item     is not NULL)
             or (t.pack_item       is NULL
                 and L_pack_item   is NULL))
         and t.comp_id      = e.comp_id
         and e.up_chrg_type = 'P';

   cursor C_GET_ALLOC_PROFIT_COMPS is
      select a.comp_id,
             a.comp_currency
        from alloc_chrg a,
             elc_comp e
       where a.alloc_no    = I_tsf_alloc_no
         and a.to_loc      = I_to_loc
         and a.item        = L_item
         and ((a.pack_item = L_pack_item
               and L_pack_item     is not NULL
               and a.pack_item     is not NULL)
             or (a.pack_item       is NULL
                 and L_pack_item   is NULL))
         and a.comp_id      = e.comp_id
         and e.up_chrg_type = 'P';

   cursor C_GET_EXP_COMPS is
      select t.comp_id,
             t.comp_currency
        from tsfdetail_chrg t,
             elc_comp e
       where t.tsf_no      = I_tsf_alloc_no
         and t.tsf_seq_no  = I_tsf_seq_no
         and ((t.shipment         = I_shipment
               and I_shipment    is not NULL
               and t.ship_seq_no  = I_ship_seq_no
               and I_ship_seq_no is not NULL)
             or (t.shipment     is NULL
                 and I_shipment is NULL)
             or (t.shipment     is NULL
                 and I_shipment is not NULL))
         and t.from_loc    = I_from_loc
         and t.to_loc      = I_to_loc
         and t.item        = L_item
         and ((t.pack_item = L_pack_item
               and L_pack_item     is not NULL
               and t.pack_item     is not NULL)
             or (t.pack_item       is NULL
                 and L_pack_item   is NULL))
         and t.comp_id      = e.comp_id
         and e.up_chrg_type = 'E';

   cursor C_GET_ALLOC_EXP_COMPS is
      select a.comp_id,
             a.comp_currency
        from alloc_chrg a,
             elc_comp e
       where a.alloc_no    = I_tsf_alloc_no
         and a.to_loc      = I_to_loc
         and a.item        = L_item
         and ((a.pack_item = L_pack_item
               and L_pack_item     is not NULL
               and a.pack_item     is not NULL)
             or (a.pack_item       is NULL
                 and L_pack_item   is NULL))
         and a.comp_id      = e.comp_id
         and e.up_chrg_type = 'E';

BEGIN
   O_total_chrgs_prim    := 0;
   O_profit_chrgs_to_loc := 0;
   O_exp_chrgs_to_loc    := 0;
   ---
   if IS_FRANCHISE_TSF_ALLOC(O_error_message,
                             L_franchise_tsf_alloc_ind,
                             I_from_loc,
                             I_from_loc_type,
                             I_to_loc,
                             I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   if L_franchise_tsf_alloc_ind = 'Y' then
      return TRUE;
   end if;
   ---
   if I_pack_item is NULL then
     if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                      L_pack_ind,
                                      L_sellable_ind,
                                      L_orderable_ind,
                                      L_pack_type,
                                      I_item) = FALSE then
         return FALSE;
      end if;
   else
      L_pack_ind  := 'Y';
      L_pack_type := 'B';
      L_pack_item := I_pack_item;
      L_item      := I_item;
   end if;
   ---
   if L_pack_ind = 'Y' and L_pack_type = 'B' and I_pack_item is NULL then
      for L_rec in C_GET_PACK_ITEMS loop
         L_item               := L_rec.item;
         L_pack_item          := I_item;
         L_total_profit_value := 0;
         L_total_exp_value    := 0;
         ---
         if I_tsf_alloc_type = 'T' then
            for C_rec in C_GET_PROFIT_COMPS loop
               if CALC_COMP_VALUE(O_error_message,
                                  L_comp_value,
                                  I_tsf_alloc_type,
                                  I_tsf_alloc_no,
                                  I_tsf_seq_no,
                                  I_shipment,
                                  I_ship_seq_no,
                                  L_item,
                                  I_from_loc,
                                  I_from_loc_type,
                                  I_to_loc,
                                  I_to_loc_type,
                                  C_rec.comp_id,
                                  C_rec.comp_currency,
                                  NULL,
                                  NULL,
                                  L_pack_item)= FALSE then
                  return FALSE;
               end if;
               ---
               L_total_profit_value := L_total_profit_value + (L_comp_value * L_rec.qty);
            end loop;
            ---
            for C_rec in C_GET_EXP_COMPS loop
               if CALC_COMP_VALUE(O_error_message,
                                  L_comp_value,
                                  I_tsf_alloc_type,
                                  I_tsf_alloc_no,
                                  I_tsf_seq_no,
                                  I_shipment,
                                  I_ship_seq_no,
                                  L_item,
                                  I_from_loc,
                                  I_from_loc_type,
                                  I_to_loc,
                                  I_to_loc_type,
                                  C_rec.comp_id,
                                  C_rec.comp_currency,
                                  NULL,
                                  NULL,
                                  L_pack_item) = FALSE then
                  return FALSE;
               end if;
               ---
               L_total_exp_value := L_total_exp_value + (L_comp_value * L_rec.qty);
            end loop;
         else  -- I_tsf_alloc_type = 'A' - Allocation
            for C_rec in C_GET_ALLOC_PROFIT_COMPS loop
               if CALC_COMP_VALUE(O_error_message,
                                  L_comp_value,
                                  I_tsf_alloc_type,
                                  I_tsf_alloc_no,
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_item,
                                  I_from_loc,
                                  I_from_loc_type,
                                  I_to_loc,
                                  I_to_loc_type,
                                  C_rec.comp_id,
                                  C_rec.comp_currency,
                                  NULL,
                                  NULL,
                                  L_pack_item) = FALSE then
                  return FALSE;
               end if;
               ---
               L_total_profit_value := L_total_profit_value + (L_comp_value * L_rec.qty);
            end loop;
            ---
            for C_rec in C_GET_ALLOC_EXP_COMPS loop
               if CALC_COMP_VALUE(O_error_message,
                                  L_comp_value,
                                  I_tsf_alloc_type,
                                  I_tsf_alloc_no,
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_item,
                                  I_from_loc,
                                  I_from_loc_type,
                                  I_to_loc,
                                  I_to_loc_type,
                                  C_rec.comp_id,
                                  C_rec.comp_currency,
                                  NULL,
                                  NULL,
                                  L_pack_item) = FALSE then
                  return FALSE;
               end if;
               ---
               L_total_exp_value := L_total_exp_value + (L_comp_value * L_rec.qty);
            end loop;
         end if;
         ---
         L_pack_profit_value := L_pack_profit_value + L_total_profit_value;
         L_pack_exp_value    := L_pack_exp_value    + L_total_exp_value;
      end loop;
      ---
      O_total_chrgs_prim := L_pack_profit_value + L_pack_exp_value;
      ---
      -- Convert L_pack_profit_value and L_pack_exp_value from Primary
      -- to To Loc currency
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_pack_profit_value,
                                          O_profit_chrgs_to_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_pack_exp_value,
                                          O_exp_chrgs_to_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
   else
      if I_tsf_alloc_type = 'T' then
         for C_rec in C_GET_PROFIT_COMPS loop
            if CALC_COMP_VALUE(O_error_message,
                               L_comp_value,
                               I_tsf_alloc_type,
                               I_tsf_alloc_no,
                               I_tsf_seq_no,
                               I_shipment,
                               I_ship_seq_no,
                               I_item,
                               I_from_loc,
                               I_from_loc_type,
                               I_to_loc,
                               I_to_loc_type,
                               C_rec.comp_id,
                               C_rec.comp_currency,
                               NULL,
                               NULL,
                               I_pack_item) = FALSE then
               return FALSE;
            end if;
            ---
            L_total_profit_value := L_total_profit_value + L_comp_value;
         end loop;
         ---
         for C_rec in C_GET_EXP_COMPS loop
            if CALC_COMP_VALUE(O_error_message,
                               L_comp_value,
                               I_tsf_alloc_type,
                               I_tsf_alloc_no,
                               I_tsf_seq_no,
                               I_shipment,
                               I_ship_seq_no,
                               I_item,
                               I_from_loc,
                               I_from_loc_type,
                               I_to_loc,
                               I_to_loc_type,
                               C_rec.comp_id,
                               C_rec.comp_currency,
                               NULL,
                               NULL,
                               I_pack_item) = FALSE then
               return FALSE;
            end if;
            ---
            L_total_exp_value := L_total_exp_value + L_comp_value;
         end loop;
      else  -- I_tsf_alloc_type = 'A' - Allocation
         for C_rec in C_GET_ALLOC_PROFIT_COMPS loop
            if CALC_COMP_VALUE(O_error_message,
                               L_comp_value,
                               I_tsf_alloc_type,
                               I_tsf_alloc_no,
                               NULL,
                               NULL,
                               NULL,
                               I_item,
                               I_from_loc,
                               I_from_loc_type,
                               I_to_loc,
                               I_to_loc_type,
                               C_rec.comp_id,
                               C_rec.comp_currency,
                               NULL,
                               NULL,
                               I_pack_item) = FALSE then
               return FALSE;
            end if;
            ---
            L_total_profit_value := L_total_profit_value + L_comp_value;
         end loop;
         ---
         for C_rec in C_GET_ALLOC_EXP_COMPS loop
            if CALC_COMP_VALUE(O_error_message,
                               L_comp_value,
                               I_tsf_alloc_type,
                               I_tsf_alloc_no,
                               NULL,
                               NULL,
                               NULL,
                               I_item,
                               I_from_loc,
                               I_from_loc_type,
                               I_to_loc,
                               I_to_loc_type,
                               C_rec.comp_id,
                               C_rec.comp_currency,
                               NULL,
                               NULL,
                               I_pack_item) = FALSE then
               return FALSE;
            end if;
            ---
            L_total_exp_value := L_total_exp_value + L_comp_value;
         end loop;
      end if;
      ---
      O_total_chrgs_prim := L_total_profit_value + L_total_exp_value;
      ---
      -- Convert L_total_profit_value and L_total_exp_value from Primary
      -- to To Loc currency
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_total_profit_value,
                                          O_profit_chrgs_to_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_total_exp_value,
                                          O_exp_chrgs_to_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALC_TSF_ALLOC_ITEM_LOC_CHRGS;
-------------------------------------------------------------------------------
FUNCTION CALC_TSF_ALLOC_ITEM_CHRGS(O_error_message         IN OUT   VARCHAR2,
                                   O_total_chrgs_prim      IN OUT   ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE,
                                   O_profit_chrgs_to_loc   IN OUT   NUMBER,
                                   O_exp_chrgs_to_loc      IN OUT   NUMBER,
                                   I_tsf_alloc_type        IN       VARCHAR2,
                                   I_tsf_alloc_no          IN       TSFHEAD.TSF_NO%TYPE,
                                   I_tsf_type              IN       TSFHEAD.TSF_TYPE%TYPE,
                                   I_tsf_seq_no            IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                                   I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                   I_pack_item             IN       ITEM_MASTER.ITEM%TYPE,
                                   I_from_loc              IN       STORE.STORE%TYPE,
                                   I_from_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                                   I_to_loc                IN       STORE.STORE%TYPE,
                                   I_to_loc_type           IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(62) := 'UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_CHRGS';

   L_up_chrg_prim              ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_profit_chrg_loc           NUMBER                             := 0;
   L_exp_chrg_loc              NUMBER                             := 0;
   L_total_up_chrg_prim        ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE := 0;
   L_total_profit_chrg_loc     NUMBER                             := 0;
   L_total_exp_chrg_loc        NUMBER                             := 0;
   L_item_tsf_qty              TSFDETAIL.TSF_QTY%TYPE             := 0;
   L_count                     BINARY_INTEGER                     := 0;
   L_shipped                   BOOLEAN                            := FALSE;
   L_transferred               BOOLEAN                            := FALSE;
   L_franchise_tsf_alloc_ind   VARCHAR2(1);
   L_pwh_ind                   VARCHAR2(1)                        := 'N';

   cursor C_GET_INV_FLOW is
      select shipment,
             seq_no,
             from_loc,
             from_loc_type,
             to_loc,
             to_loc_type,
             tsf_qty
        from shipitem_inv_flow
       where tsf_no     = I_tsf_alloc_no
         and tsf_seq_no = I_tsf_seq_no
         and item       = nvl(I_pack_item, I_item);

   cursor C_GET_TSF_DIST is
      select tif.from_loc_type,
             tif.from_loc,
             tif.tsf_qty,
             tif.to_loc_type,
             tif.to_loc
        from tsfitem_inv_flow tif
       where tif.tsf_no = I_tsf_alloc_no
         and tif.tsf_seq_no = I_tsf_seq_no
         and tif.item   = nvl(I_pack_item, I_item);

   cursor C_CHK_PWH(L_wh   IN   WH.WH%TYPE) is
      select 'Y'
        from wh
       where wh = L_wh
         and wh = physical_wh;

BEGIN

   O_total_chrgs_prim    := 0;
   O_profit_chrgs_to_loc := 0;
   O_exp_chrgs_to_loc    := 0;
   ---
   if IS_FRANCHISE_TSF_ALLOC(O_error_message,
                             L_franchise_tsf_alloc_ind,
                             I_from_loc,
                             I_from_loc_type,
                             I_to_loc,
                             I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   if L_franchise_tsf_alloc_ind = 'Y' then
      return TRUE;
   end if;
   ---
   --- Incude the condition to check the physical wh in case of CO transfer.
   if I_tsf_alloc_type = 'T' and I_tsf_type = 'CO' then
      if I_from_loc_type = 'W' or I_to_loc_type = 'W' then
         open C_CHK_PWH(I_from_loc);
         fetch C_CHK_PWH into L_pwh_ind;
         close C_CHK_PWH;
         if L_pwh_ind = 'N' then
            open C_CHK_PWH(I_to_loc);
            fetch C_CHK_PWH into L_pwh_ind;
            close C_CHK_PWH; 
         end if;
      end if;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;

   if I_tsf_alloc_type = 'T' and
      (I_tsf_type = 'EG' or L_pwh_ind = 'Y') and
      (I_from_loc_type = 'W' or I_to_loc_type = 'W') then
      ---
      -- If the transfer involves a physical warehouse, then need to loop through
      -- all of the inventory flows to calculate cost and retail
      ---
      -- Loop through each inventory flow and calculate a weighted average up chrg value
      ---
      for C_rec in C_GET_INV_FLOW loop
         if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                        L_up_chrg_prim,
                                                        L_profit_chrg_loc,
                                                        L_exp_chrg_loc,
                                                        I_tsf_alloc_type,
                                                        I_tsf_alloc_no,
                                                        I_tsf_seq_no,
                                                        C_rec.shipment,
                                                        C_rec.seq_no,
                                                        I_item,
                                                        I_pack_item,
                                                        C_rec.from_loc,
                                                        C_rec.from_loc_type,
                                                        C_rec.to_loc,
                                                        C_rec.to_loc_type) = FALSE then
            return FALSE;
         end if;
         ---
         L_shipped := TRUE;
         L_total_up_chrg_prim    := L_total_up_chrg_prim    + (L_up_chrg_prim    * C_rec.tsf_qty);
         L_total_profit_chrg_loc := L_total_profit_chrg_loc + (L_profit_chrg_loc * C_rec.tsf_qty);
         L_total_exp_chrg_loc    := L_total_exp_chrg_loc    + (L_exp_chrg_loc    * C_rec.tsf_qty);
         L_item_tsf_qty          := L_item_tsf_qty          + C_rec.tsf_qty;
      end loop;
      ---
      -- Finish calculating the weighted average by dividing by the total item transfer qty
      ---
      if L_shipped = TRUE then
         O_total_chrgs_prim    := L_total_up_chrg_prim    / L_item_tsf_qty;
         O_profit_chrgs_to_loc := L_total_profit_chrg_loc / L_item_tsf_qty;
         O_exp_chrgs_to_loc    := L_total_exp_chrg_loc    / L_item_tsf_qty;
      end if;

      if L_shipped = FALSE then

         FOR L_rec in C_GET_TSF_DIST LOOP
            if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                           L_up_chrg_prim,
                                                           L_profit_chrg_loc,
                                                           L_exp_chrg_loc,
                                                           I_tsf_alloc_type,
                                                           I_tsf_alloc_no,
                                                           I_tsf_seq_no,
                                                           NULL,
                                                           NULL,
                                                           I_item,
                                                           I_pack_item,
                                                           L_rec.from_loc,
                                                           L_rec.from_loc_type,
                                                           L_rec.to_loc,
                                                           L_rec.to_loc_type) = FALSE then
               return FALSE;
            end if;
            ---
            L_transferred := TRUE;
            L_total_up_chrg_prim    := L_total_up_chrg_prim    + (L_up_chrg_prim    * L_rec.tsf_qty);
            L_total_profit_chrg_loc := L_total_profit_chrg_loc + (L_profit_chrg_loc * L_rec.tsf_qty);
            L_total_exp_chrg_loc    := L_total_exp_chrg_loc    + (L_exp_chrg_loc    * L_rec.tsf_qty);
            L_item_tsf_qty          := L_item_tsf_qty          + L_rec.tsf_qty;
         end LOOP;

         if L_transferred = TRUE then
            O_total_chrgs_prim    := L_total_up_chrg_prim / L_item_tsf_qty;
            O_profit_chrgs_to_loc := L_total_profit_chrg_loc / L_item_tsf_qty;
            O_exp_chrgs_to_loc    := L_total_exp_chrg_loc    / L_item_tsf_qty;
         end if;
      end if;

      -- For store to store EG transfers there will be no distribution info
      if L_shipped = FALSE and L_transferred = FALSE then
         if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                     O_total_chrgs_prim,
                                                     O_profit_chrgs_to_loc,
                                                     O_exp_chrgs_to_loc,
                                                     I_tsf_alloc_type,
                                                     I_tsf_alloc_no,
                                                     I_tsf_seq_no,
                                                     NULL,
                                                     NULL,
                                                     I_item,
                                                     I_pack_item,
                                                     I_from_loc,
                                                     I_from_loc_type,
                                                     I_to_loc,
                                                     I_to_loc_type) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                     O_total_chrgs_prim,
                                                     O_profit_chrgs_to_loc,
                                                     O_exp_chrgs_to_loc,
                                                     I_tsf_alloc_type,
                                                     I_tsf_alloc_no,
                                                     I_tsf_seq_no,
                                                     NULL,
                                                     NULL,
                                                     I_item,
                                                     I_pack_item,
                                                     I_from_loc,
                                                     I_from_loc_type,
                                                     I_to_loc,
                                                     I_to_loc_type) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALC_TSF_ALLOC_ITEM_CHRGS;
-------------------------------------------------------------------------------
FUNCTION IS_FRANCHISE_TSF_ALLOC(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_franchise_tsf_alloc_ind   IN OUT   VARCHAR2,
                                I_from_loc                  IN       ITEM_LOC.LOC%TYPE,
                                I_from_loc_type             IN       ITEM_LOC.LOC_TYPE%TYPE,
                                I_to_loc                    IN       ITEM_LOC.LOC%TYPE,
                                I_to_loc_type               IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(62) := 'UP_CHARGE_SQL.IS_FRANCHISE_TSF_ALLOC';
   L_from_store_type   STORE.STORE_TYPE%TYPE := 'X';
   L_to_store_type     STORE.STORE_TYPE%TYPE := 'X';

BEGIN
   O_franchise_tsf_alloc_ind := 'N';

   if I_from_loc_type = 'S' then
      select store_type
        into L_from_store_type
        from store
       where store = I_from_loc;
   end if;

   if I_to_loc_type = 'S' then
      select store_type
        into L_to_store_type
        from store
       where store = I_to_loc;
   end if;

   if (L_from_store_type = 'F' and L_to_store_type != 'F') or (L_from_store_type != 'F' and L_to_store_type = 'F') then
      O_franchise_tsf_alloc_ind := 'Y';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END IS_FRANCHISE_TSF_ALLOC;
-------------------------------------------------------------------------------
FUNCTION CALC_ITEM_CHRGS(O_error_message         IN OUT   VARCHAR2,
                         O_total_chrgs_prim      IN OUT   ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE,
                         O_profit_chrgs_to_loc   IN OUT   NUMBER,
                         O_exp_chrgs_to_loc      IN OUT   NUMBER,
                         I_tsf_alloc_type        IN       VARCHAR2,
                         I_tsf_alloc_no          IN       TSFHEAD.TSF_NO%TYPE,
                         I_tsf_seq_no            IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                         I_shipment              IN       SHIPMENT.SHIPMENT%TYPE,
                         I_ship_seq_no           IN       SHIPSKU.SEQ_NO%TYPE,
                         I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_item             IN       ITEM_MASTER.ITEM%TYPE,
                         I_from_loc              IN       STORE.STORE%TYPE,
                         I_from_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_to_loc                IN       STORE.STORE%TYPE,
                         I_to_loc_type           IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_from_loc_cost         IN       SHIPSKU.UNIT_COST%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(62) := 'UP_CHARGE_SQL.CALC_ITEM_CHRGS';
   L_orderable_ind             ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_ind                  ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind              ITEM_MASTER.SELLABLE_IND%TYPE;
   L_pack_type                 ITEM_MASTER.PACK_TYPE%TYPE;
   L_comp_value                NUMBER                             := 0;
   L_total_profit_value        NUMBER                             := 0;
   L_total_exp_value           NUMBER                             := 0;
   L_pack_profit_value         NUMBER                             := 0;
   L_pack_exp_value            NUMBER                             := 0;
   L_item                      ITEM_MASTER.ITEM%TYPE              := I_item;
   L_pack_item                 ITEM_MASTER.ITEM%TYPE;
   L_franchise_tsf_alloc_ind   VARCHAR2(1);
   L_dummy                     VARCHAR2(1);
   L_wrong_st_rcv_ind          VARCHAR2(1) := 'Y';
   L_from_loc_cost             SHIPSKU.UNIT_COST%TYPE;

   cursor C_CHECK_CHRGS is
      select 'X'
        from item_chrg_head ich,
             item_chrg_detail icd
       where ich.from_loc = I_from_loc
         and ich.to_loc = I_to_loc
         and ich.item = I_item
         and ich.item = icd.item
         and ich.from_loc = icd.from_loc
         and ich.to_loc = icd.to_loc
         and rownum = 1;

   cursor C_GET_PACK_ITEMS is
      select item,
             qty
        from v_packsku_qty
       where pack_no = I_item;

    cursor C_GET_PROFIT_COMPS is
      select icd.comp_id,
             icd.comp_currency
        from item_chrg_detail icd,
             elc_comp e
       where icd.from_loc    = I_from_loc
         and icd.to_loc      = I_to_loc
         and icd.item        = L_item
         and icd.comp_id      = e.comp_id
         and e.up_chrg_type = 'P';

     cursor C_GET_EXP_COMPS is
      select icd.comp_id,
             icd.comp_currency
        from item_chrg_detail icd,
             elc_comp e
        where icd.from_loc    = I_from_loc
         and icd.to_loc      = I_to_loc
         and icd.item        = L_item
         and icd.comp_id      = e.comp_id
         and e.up_chrg_type = 'E';

BEGIN
   O_total_chrgs_prim    := 0;
   O_profit_chrgs_to_loc := 0;
   O_exp_chrgs_to_loc    := 0;
   ---
   OPEN C_CHECK_CHRGS;
   FETCH C_CHECK_CHRGS into L_dummy;
   if C_CHECK_CHRGS%NOTFOUND then
      CLOSE C_CHECK_CHRGS;
      return TRUE;
   end if;
   CLOSE C_CHECK_CHRGS;

   if IS_FRANCHISE_TSF_ALLOC(O_error_message,
                             L_franchise_tsf_alloc_ind,
                             I_from_loc,
                             I_from_loc_type,
                             I_to_loc,
                             I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   if L_franchise_tsf_alloc_ind = 'Y' then
      return TRUE;
   end if;
   ---
   if I_pack_item is NULL then
     if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                      L_pack_ind,
                                      L_sellable_ind,
                                      L_orderable_ind,
                                      L_pack_type,
                                      I_item) = FALSE then
         return FALSE;
      end if;
   else
      L_pack_ind  := 'Y';
      L_pack_type := 'B';
      L_pack_item := I_pack_item;
      L_item      := I_item;
   end if;
   ---
   --Convert I_from_loc_cost from Local to Primary Currency
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          I_from_loc,
                                          I_from_loc_type,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_from_loc_cost,
                                          L_from_loc_cost,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;

   if L_pack_ind = 'Y' and L_pack_type = 'B' and I_pack_item is NULL then
      for L_rec in C_GET_PACK_ITEMS loop
         L_item               := L_rec.item;
         L_pack_item          := I_item;
         L_total_profit_value := 0;
         L_total_exp_value    := 0;
         ---
            for C_rec in C_GET_PROFIT_COMPS loop
               if CALC_COMP_VALUE(O_error_message,
                                  L_comp_value,
                                  I_tsf_alloc_type,
                                  I_tsf_alloc_no,
                                  I_tsf_seq_no,
                                  I_shipment,
                                  I_ship_seq_no,
                                  L_item,
                                  I_from_loc,
                                  I_from_loc_type,
                                  I_to_loc,
                                  I_to_loc_type,
                                  C_rec.comp_id,
                                  C_rec.comp_currency,
                                  L_from_loc_cost,
                                  L_wrong_st_rcv_ind,
                                  L_pack_item) = FALSE then
                  return FALSE;
               end if;
               ---
               L_total_profit_value := L_total_profit_value + (L_comp_value * L_rec.qty);
            end loop;
            ---
            for C_rec in C_GET_EXP_COMPS loop
               if CALC_COMP_VALUE(O_error_message,
                                  L_comp_value,
                                  I_tsf_alloc_type,
                                  I_tsf_alloc_no,
                                  I_tsf_seq_no,
                                  I_shipment,
                                  I_ship_seq_no,
                                  L_item,
                                  I_from_loc,
                                  I_from_loc_type,
                                  I_to_loc,
                                  I_to_loc_type,
                                  C_rec.comp_id,
                                  C_rec.comp_currency,
                                  L_from_loc_cost,
                                  L_wrong_st_rcv_ind,
                                  L_pack_item) = FALSE then
                  return FALSE;
               end if;
               ---
               L_total_exp_value := L_total_exp_value + (L_comp_value * L_rec.qty);
            end loop;
         ---
         L_pack_profit_value := L_pack_profit_value + L_total_profit_value;
         L_pack_exp_value    := L_pack_exp_value    + L_total_exp_value;
      end loop;
      ---
      O_total_chrgs_prim := L_pack_profit_value + L_pack_exp_value;
      ---
      -- Convert L_pack_profit_value and L_pack_exp_value from Primary
      -- to To Loc currency
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_pack_profit_value,
                                          O_profit_chrgs_to_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_pack_exp_value,
                                          O_exp_chrgs_to_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
   else
         for C_rec in C_GET_PROFIT_COMPS loop
            if CALC_COMP_VALUE(O_error_message,
                               L_comp_value,
                               I_tsf_alloc_type,
                               I_tsf_alloc_no,
                               I_tsf_seq_no,
                               I_shipment,
                               I_ship_seq_no,
                               I_item,
                               I_from_loc,
                               I_from_loc_type,
                               I_to_loc,
                               I_to_loc_type,
                               C_rec.comp_id,
                               C_rec.comp_currency,
                               L_from_loc_cost,
                               L_wrong_st_rcv_ind,
                               I_pack_item) = FALSE then
               return FALSE;
            end if;
            ---
            L_total_profit_value := L_total_profit_value + L_comp_value;
         end loop;
         ---
         for C_rec in C_GET_EXP_COMPS loop
            if CALC_COMP_VALUE(O_error_message,
                               L_comp_value,
                               I_tsf_alloc_type,
                               I_tsf_alloc_no,
                               I_tsf_seq_no,
                               I_shipment,
                               I_ship_seq_no,
                               I_item,
                               I_from_loc,
                               I_from_loc_type,
                               I_to_loc,
                               I_to_loc_type,
                               C_rec.comp_id,
                               C_rec.comp_currency,
                               L_from_loc_cost,
                               L_wrong_st_rcv_ind,
                               I_pack_item) = FALSE then
               return FALSE;
            end if;
            ---
            L_total_exp_value := L_total_exp_value + L_comp_value;
         end loop;
      ---
      O_total_chrgs_prim := L_total_profit_value + L_total_exp_value;
      ---
      -- Convert L_total_profit_value and L_total_exp_value from Primary
      -- to To Loc currency
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_total_profit_value,
                                          O_profit_chrgs_to_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_total_exp_value,
                                          O_exp_chrgs_to_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALC_ITEM_CHRGS;
-------------------------------------------------------------------------------
END UP_CHARGE_SQL;
/
