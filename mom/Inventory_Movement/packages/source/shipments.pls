CREATE OR REPLACE PACKAGE SHIPMENT_SQL AUTHID CURRENT_USER AS

-----------------------------------------------------------------------------------
-- Name   : SHIP_TSF
-- Purpose: This function will create shipment records for the new records created
--          the trasfer form using the 'Ship' option.
------------------------------------------------------------------------------------
FUNCTION SHIP_TSF(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  I_tsf_no        IN      TSFHEAD.TSF_NO%TYPE,
                  I_pub_ind       IN      VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name   : SETUP_BOL_DISTRO_ITEMS
-- Purpose: This function will populate the BOL_DISTRO_ITEMS_TEMP table with the
--          items for the specified distro when the user presses either
--          the 'Select Items' button or the 'Apply all Items' button on the
--          BOLSHIPMENT.FMB form.
------------------------------------------------------------------------------------
FUNCTION SETUP_BOL_DISTRO_ITEMS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_bol_no          IN      BOL_SHIPSKU.BOL_NO%TYPE,
                                I_distro_no       IN      BOL_DISTRO_ITEMS_TEMP.DISTRO_NO%TYPE,
                                I_distro_type     IN      BOL_DISTRO_ITEMS_TEMP.DISTRO_TYPE%TYPE,
                                I_select_all      IN      BOL_DISTRO_ITEMS_TEMP.SELECT_IND%TYPE,
                                I_from_loc        IN      TSFHEAD.FROM_LOC%TYPE,
                                I_to_loc          IN      TSFHEAD.TO_LOC%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name   : SETUP_ORDER_SHIPMENT
-- Purpose: This function retrieve the selected checkbox, item, item description,
--          carton, order quantity, and previously received quantity for the passed
--          in order number and to location from the v_ordloc and item_master tables
------------------------------------------------------------------------------------
FUNCTION SETUP_ORDER_SHIPMENT ( O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no            IN       V_ORDHEAD.ORDER_NO%TYPE,
                                I_location            IN       V_ORDLOC.LOCATION%TYPE,
                                I_loc_type            IN       V_ORDLOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name   : DELETE_SHIPSKU
-- Purpose: This function will delete records from the BOL_SHIPSKU table.
------------------------------------------------------------------------------------
FUNCTION DELETE_SHIPSKU(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_bol_no          IN       BOL_SHIPSKU.BOL_NO%TYPE,
                        I_distro_type     IN       BOL_SHIPSKU.DISTRO_TYPE%TYPE,
                        I_distro_no       IN       BOL_SHIPSKU.DISTRO_NO%TYPE,
                        I_item            IN       BOL_SHIPSKU.ITEM%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name   : DELETE_DISTRO_ITEMS_TEMP
-- Purpose: This function will delete records from the BOL_DISTRO_ITEMS_TEMP table.
------------------------------------------------------------------------------------
FUNCTION DELETE_DISTRO_ITEMS_TEMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_bol_no          IN       BOL_SHIPSKU.BOL_NO%TYPE,
                                  I_distro_type     IN       BOL_SHIPSKU.DISTRO_TYPE%TYPE,
                                  I_distro_no       IN       BOL_SHIPSKU.DISTRO_NO%TYPE,
                                  I_item            IN       BOL_SHIPSKU.ITEM%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name   : DELETE_BOL_SHIPMENT
-- Purpose: This function will delete records from the BOL_SHIPSKU and BOL_SHIPMENT
--          tables.
------------------------------------------------------------------------------------
FUNCTION DELETE_BOL_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_bol_no          IN       BOL_SHIPSKU.BOL_NO%TYPE)

   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name   : APPLY_BOL_SHIPSKU
-- Purpose: This function will insert records to the BOL_SHIPSKU table from the
--          BOL_DISTRO_ITEMS_TEMP table where select_ind is 'Y'.
-----------------------------------------------------------------------------------
FUNCTION APPLY_BOL_SHIPSKU(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_bol_no          IN       BOL_SHIPSKU.BOL_NO%TYPE,
                           I_distro_type     IN       BOL_SHIPSKU.DISTRO_TYPE%TYPE,
                           I_distro_no       IN       BOL_SHIPSKU.DISTRO_NO%TYPE,
                           I_apply_all       IN       VARCHAR2)
  RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name   : SHIP_PO
-- Purpose: This function will be used to ship PO's via
--          the ordshipment.fmb. It accepts values from the 
--          form as PL/SQL table.
-----------------------------------------------------------------------------------
FUNCTION SHIP_PO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_order_no        IN       V_ORDLOC.ORDER_NO%TYPE,
                  I_location        IN       SHIPMENT.TO_LOC%TYPE,
                  I_asn             IN       SHIPMENT.ASN%TYPE,
                  I_shipment_date   IN       SHIPMENT.SHIP_DATE%TYPE,
                  I_courier         IN       SHIPMENT.COURIER%TYPE,
                  I_no_boxes        IN       SHIPMENT.NO_BOXES%TYPE,
                  I_comments        IN       SHIPMENT.COMMENTS%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name   : SHIP_DISTROS
-- Purpose: This function will ship BOL's created in the 
--          BOLSHIPFIND.FMB form. The existing RMS subscription
--          API will be used to accomplish this.
-----------------------------------------------------------------------------------
FUNCTION SHIP_DISTROS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_bol_no          IN       BOL_SHIPMENT.BOL_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name   : LOCK_ORDER_SHIPMENT
-- Purpose: This function will lock the PO record when a user selects an order 
--          in the ORDSHIPMENT.FMB form.
-----------------------------------------------------------------------------------
FUNCTION LOCK_ORDER_SHIPMENT (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION DELETE_CARTON(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_shipment      IN     SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
END SHIPMENT_SQL;
/