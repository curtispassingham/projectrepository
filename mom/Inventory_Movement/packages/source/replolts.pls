CREATE OR REPLACE PACKAGE REPL_OLT_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name:  GET_ITEM_LOC_REVIEW_TIME
-- Purpose      :  Gets the time in days to the next review for an item/location.
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC_REVIEW_TIME(O_error_message   IN OUT   VARCHAR2,
                                  O_review_time     IN OUT   REPL_ITEM_LOC.REVIEW_CYCLE%TYPE,
                                  I_item            IN       REPL_ITEM_LOC.ITEM%TYPE,
                                  I_loc             IN       REPL_ITEM_LOC.LOCATION%TYPE,
                                  I_review_cycle    IN       REPL_ITEM_LOC.REVIEW_CYCLE%TYPE,
                                  I_today           IN       SUP_REPL_DAY.WEEKDAY%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  GET_OLTS_AND_REVIEW_TIME
-- Purpose      :  Gets the review time and the current and future order leadtimes.
--                 It will adjust the leadtimes based on user specified delivery
--                 days and the location calender.  If a store or warehouse is closed on a
--                 particular day, the leadtimes will be adjusted and passed back out.
---------------------------------------------------------------------------------------------
FUNCTION GET_OLTS_AND_REVIEW_TIME(O_error_message        IN OUT   VARCHAR2,
                                  O_colt                 IN OUT   NUMBER,
                                  O_nolt                 IN OUT   NUMBER,
                                  O_days_added_to_colt   IN OUT   NUMBER,
                                  O_days_added_to_nolt   IN OUT   NUMBER,
                                  O_review_time          IN OUT   NUMBER,
                                  O_next_delivery_date   IN OUT   REPL_ITEM_LOC.NEXT_DELIVERY_DATE%TYPE,
                                  O_next_review_date     IN OUT   REPL_ITEM_LOC.NEXT_REVIEW_DATE%TYPE,
                                  IO_supp_lead_time      IN OUT   NUMBER,
                                  IO_pickup_lead_time    IN OUT   NUMBER,
                                  IO_wh_lead_time        IN OUT   NUMBER,
                                  IO_last_dd             IN OUT   PERIOD.VDATE%TYPE,
                                  I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                                  I_location             IN       WH.WH%TYPE,
                                  I_loc_type             IN       REPL_RESULTS.LOC_TYPE%TYPE,
                                  I_repl_date            IN       REPL_RESULTS.REPL_DATE%TYPE,
                                  I_stock_cat            IN       REPL_RESULTS.STOCK_CAT%TYPE,
                                  I_repl_method          IN       REPL_RESULTS.REPL_METHOD%TYPE,
                                  I_review_cycle         IN       REPL_RESULTS.REVIEW_CYCLE%TYPE,
                                  I_supplier             IN       SUPS.SUPPLIER%TYPE,
                                  I_supp_dlvry_policy    IN       SUPS.DELIVERY_POLICY%TYPE,
                                  I_source_wh            IN       WH.WH%TYPE,
                                  I_physical_wh          IN       WH.PHYSICAL_WH%TYPE,
                                  I_wh_dlvry_policy      IN       WH.DELIVERY_POLICY%TYPE,
                                  I_loc_activity_ind     IN       SYSTEM_OPTIONS.LOC_ACTIVITY_IND%TYPE,
                                  I_loc_dlvry_ind        IN       SYSTEM_OPTIONS.LOC_DLVRY_IND%TYPE,
                                  I_thread_id            IN       NUMBER DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END;
/
