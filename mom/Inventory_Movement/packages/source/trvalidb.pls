CREATE OR REPLACE PACKAGE BODY TSF_VALIDATE_SQL AS  
--------------------------------------------------------------------------------------------
FUNCTION EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
               I_transfer        IN       NUMBER,
               O_exist           IN OUT   BOOLEAN)
   return BOOLEAN IS

   L_dummy     VARCHAR(1);

   cursor C_EXIST is
      select 'x'
        from tsfhead
       where tsf_no = I_transfer;

BEGIN
   open C_EXIST;
   fetch C_EXIST into L_dummy;
   if C_EXIST%NOTFOUND then
      O_exist := FALSE;
      O_error_message := sql_lib.create_msg('INV_TSF',null,null,null);
   else
      O_exist := TRUE;
   end if;

   close C_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'TSF_VALIDATE_SQL.EXIST', 
                                              to_char(SQLCODE));
   return FALSE;
END EXIST;
------------------------------------------------------------------------------------------------
FUNCTION VALID_TO_SHIP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid           IN OUT   BOOLEAN,
                       I_tsf_no          IN       tsfhead.tsf_no%TYPE,
                       I_from_loc        IN       tsfhead.from_loc%TYPE)
   return BOOLEAN IS

   L_dept            tsfhead.dept%TYPE;
   L_create_date     tsfhead.create_date%TYPE;
   L_status          tsfhead.status%TYPE;
   L_from_loc_type   tsfhead.from_loc_type%TYPE;
   L_from_loc        tsfhead.from_loc%TYPE;
   L_to_loc_type     tsfhead.to_loc_type%TYPE;
   L_to_loc          tsfhead.to_loc%TYPE;
   L_tsf_type        tsfhead.tsf_type%TYPE;
   L_freight_code    tsfhead.freight_code%TYPE;
   L_routing_code    tsfhead.routing_code%TYPE;
   L_comment_desc    tsfhead.comment_desc%TYPE;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_tsf_no', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_from_loc', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if TSF_ATTRIB_SQL.GET_TSFHEAD_INFO(O_error_message,
                                      I_tsf_no,
                                      L_dept,
                                      L_create_date,
                                      L_status,
                                      L_from_loc_type,
                                      L_from_loc,
                                      L_to_loc_type,
                                      L_to_loc,
                                      L_tsf_type,
                                      L_freight_code,
                                      L_routing_code,
                                      L_comment_desc) = FALSE then
      return FALSE;
   end if;

   if L_status = 'A' and L_from_loc = I_from_loc then
      O_valid := TRUE;
   else
      O_valid := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                            'TSF_VALIDATE_SQL.VALID_TO_SHIP',
                                             to_char(SQLCODE));
      return FALSE;

END VALID_TO_SHIP;
--------------------------------------------------------------------
FUNCTION VALID_TO_SHIP_TOGETHER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid           IN OUT   BOOLEAN,
                                I_prev_tsf        IN       tsfhead.tsf_no%TYPE,
                                I_curr_tsf        IN       tsfhead.tsf_no%TYPE,
                                I_from_loc        IN       tsfhead.from_loc%TYPE)
   return BOOLEAN IS

   L_dept            tsfhead.dept%TYPE;
   L_create_date     tsfhead.create_date%TYPE;
   L_status1         tsfhead.status%TYPE;
   L_status2         tsfhead.status%TYPE;
   L_from_loc_type   tsfhead.from_loc_type%TYPE;
   L_from_loc1       tsfhead.from_loc%TYPE;
   L_from_loc2       tsfhead.from_loc%TYPE;
   L_to_loc_type     tsfhead.to_loc_type%TYPE;
   L_to_loc1         tsfhead.to_loc%TYPE;
   L_to_loc2         tsfhead.to_loc%TYPE;
   L_tsf_type        tsfhead.tsf_type%TYPE;
   L_freight_code    tsfhead.freight_code%TYPE;
   L_routing_code    tsfhead.routing_code%TYPE;
   L_comment_desc    tsfhead.comment_desc%TYPE;

BEGIN
   if I_prev_tsf is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_prev_tsf', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_curr_tsf is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_curr_tsf', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if TSF_ATTRIB_SQL.GET_TSFHEAD_INFO(O_error_message,
                                      I_prev_tsf,
                                      L_dept,
                                      L_create_date,
                                      L_status1,
                                      L_from_loc_type,
                                      L_from_loc1,
                                      L_to_loc_type,
                                      L_to_loc1,
                                      L_tsf_type,
                                      L_freight_code,
                                      L_routing_code,
                                      L_comment_desc) = FALSE then
      return FALSE;
   end if;

   if TSF_ATTRIB_SQL.GET_TSFHEAD_INFO(O_error_message,
                                      I_curr_tsf,
                                      L_dept,
                                      L_create_date,
                                      L_status2,
                                      L_from_loc_type,
                                      L_from_loc2,
                                      L_to_loc_type,
                                      L_to_loc2,
                                      L_tsf_type,
                                      L_freight_code,
                                      L_routing_code,
                                      L_comment_desc) = FALSE then
      return FALSE;
   end if;

   if I_from_loc is not NULL then
      if L_from_loc1 != I_from_loc then 
         O_valid := FALSE;
         return TRUE;
      else
         O_VALID := TRUE;
      end if;
   end if;
   ---  
   if ((L_status1 in ('S', 'A') and L_status2 = 'A')
       and (L_from_loc1 = L_from_loc2)
       and (L_to_loc1 = L_to_loc2)) then
      O_valid := TRUE;
   else
      O_valid := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                            'TSF_VALIDATE_SQL.VALID_TO_SHIP_TOGETHER',
                                             to_char(SQLCODE));
      return FALSE;

END VALID_TO_SHIP_TOGETHER;
--------------------------------------------------------------------------------------------
FUNCTION TSF_PO_LINK_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist            IN OUT   BOOLEAN,
                            I_tsf_po_link_no   IN       TSFDETAIL.TSF_PO_LINK_NO%TYPE)
   return BOOLEAN IS

   L_dummy     VARCHAR(1);

   cursor C_EXIST is
      select 'x'
        from tsfdetail 
       where tsf_po_link_no = I_tsf_po_link_no;

BEGIN
   if I_tsf_po_link_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_tsf_po_link_no', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_EXIST', 'tsfdetail', 'tsf_po_link_no: '||to_char(I_tsf_po_link_no));
   open C_EXIST;
   SQL_LIB.SET_MARK('FETCH', 'C_EXIST', 'tsfdetail', 'tsf_po_link_no: '||to_char(I_tsf_po_link_no));
   fetch C_EXIST into L_dummy;
   ---
   O_exist := C_EXIST%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EXIST', 'tsfdetail', 'tsf_po_link_no: '||to_char(I_tsf_po_link_no));
   close C_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              'TSF_VALIDATE_SQL.TSF_PO_LINK_EXISTS', 
                                              to_char(SQLCODE));
   return FALSE;

END TSF_PO_LINK_EXISTS;
------------------------------------------------------------------------------------------------
FUNCTION NO_QTY_RECEIVED(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_no_qty_received   IN OUT   BOOLEAN,
                         I_tsf_no            IN       TSFDETAIL.TSF_NO%TYPE)
   return BOOLEAN is

   L_program     VARCHAR2(50)   := 'TSF_VALIDATE_SQL.NO_QTY_RECEIVED';
   L_no_qty      VARCHAR2(1)    := 'N';

   cursor C_TSFDETAIL is
      select 'Y'
        from tsfdetail
       where (NVL(received_qty,0) + NVL(reconciled_qty,0)) = 0
         and tsf_no = I_tsf_no
      union
      select 'Y'
        from tsfdetail d,
             tsfhead h
       where (NVL(d.received_qty,0) + NVL(d.reconciled_qty,0)) = 0
         and h.tsf_parent_no = I_tsf_no
         and h.tsf_no = d.tsf_no;


BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   O_no_qty_received := FALSE;

   open C_TSFDETAIL;
   fetch C_TSFDETAIL into L_no_qty;
   close C_TSFDETAIL; 

   if L_no_qty = 'Y' then
      O_no_qty_received := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              'TSF_VALIDATE_SQL.NO_QTY_RECEIVED', 
                                              to_char(SQLCODE));
   return FALSE;

END NO_QTY_RECEIVED;
---------------------------------------------------------------------------------
FUNCTION PACK_REPACK_INSTR_EXISTS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_repack_instr_exist   IN OUT   BOOLEAN,
                                  I_tsf_no               IN       TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'TSF_VALIDATE_SQL.PACK_REPACK_INSTR_EXISTS';
   L_pack_no        ITEM_MASTER.ITEM%TYPE;
   L_packing_exists BOOLEAN;
   L_xform_exists   BOOLEAN;

   cursor C_TSF_ITEMS is
      select t.item pack_no
        from tsfdetail t,
             item_master i
       where tsf_no = I_tsf_no
         and t.item = i.item
         and i.pack_ind = 'Y';

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   -- C_TSF_ITEMS selects all pack item on tsf_detail
   -- XFORM_PACKING_EXIST ensures they have a record on tsf_packing_detail
   -- as an item or as part of a new pack.

   O_repack_instr_exist := TRUE;
   for rec in C_TSF_ITEMS loop
      L_pack_no := rec.pack_no;

      if ITEM_XFORM_PACK_SQL.XFORM_PACKING_EXIST(O_error_message,
                                                 L_xform_exists,
                                                 L_packing_exists,
                                                 I_tsf_no,
                                                 L_pack_no) = FALSE then 
         return FALSE;
      end if;

      --exit loop after finding one pack on tsfdetail without packing instruction
      if L_packing_exists = FALSE and L_xform_exists = TRUE then
         O_repack_instr_exist := FALSE;
         EXIT;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program, 
                                            to_char(SQLCODE));
   return FALSE;
END PACK_REPACK_INSTR_EXISTS;
---------------------------------------------------------------------------------
FUNCTION EXPLODE_TSF_PACKS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'TSF_VALIDATE_SQL.EXPLODE_TSF_PACKS';
   L_pack_no        ITEM_MASTER.ITEM%TYPE;
   L_tsf_qty        TSFDETAIL.TSF_QTY%TYPE;
   L_packing_exists BOOLEAN;
   L_xform_exists   BOOLEAN;
   L_tsf_packing_id TSF_PACKING.TSF_PACKING_ID%TYPE;
   L_return_code    BOOLEAN;

   TYPE PACK_TSF_QTY_TBL is TABLE OF TSFDETAIL.TSF_QTY%TYPE INDEX BY VARCHAR2(25);
   L_pack_tsf_qty_tab  PACK_TSF_QTY_TBL;

   cursor C_TSF_ITEMS is
      select t.item pack_no,
             t.tsf_qty
        from tsfdetail t,
             item_master i
       where tsf_no = I_tsf_no
         and t.item = i.item
         and i.pack_ind = 'Y';

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   -- C_TSF_ITEMS selects all pack item on tsfdetail
   -- XFORM_PACKING_EXIST ensures they have a record on tsf_packing_detail
   -- as an item or as part of a new pack.

   for rec in C_TSF_ITEMS loop
      L_pack_no := rec.pack_no;
      L_tsf_qty := rec.tsf_qty;

      if ITEM_XFORM_PACK_SQL.XFORM_PACKING_EXIST(O_error_message,
                                                 L_xform_exists,
                                                 L_packing_exists,
                                                 I_tsf_no,
                                                 L_pack_no) = FALSE then 
         return FALSE;
      end if;

      if L_packing_exists = FALSE and L_xform_exists = TRUE then
         
         if L_pack_tsf_qty_tab.EXISTS(L_pack_no) = FALSE then
            --keep a list of pack_no with transfer quantity to avoid processing the same pack_no twice
            L_pack_tsf_qty_tab(L_pack_no) := L_tsf_qty;

            --write packing
            if ITEM_XFORM_PACK_SQL.NEXT_PACKING_ID(O_error_message,
                                                   L_return_code,
                                                   L_tsf_packing_id) = FALSE  or L_return_code = FALSE then
               return FALSE; 
            end if;
            ---
            insert into tsf_packing(tsf_packing_id,
                                    tsf_no,
                                    set_no)
                            (select L_tsf_packing_id,
                                    I_tsf_no,
                                    NVL(max(set_no),0) +1
                               from tsf_packing
                              where tsf_no = I_tsf_no);

            if ITEM_XFORM_PACK_SQL.EXPLODE_PACK(O_error_message,
                                                L_pack_no,
                                                L_tsf_packing_id,
                                                I_tsf_no,
                                                L_tsf_qty) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program, 
                                              to_char(SQLCODE));
   return FALSE;
END EXPLODE_TSF_PACKS;
------------------------------------------------------------------------------------------------
FUNCTION PACK_REPACK_INSTR_EXISTS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_repack_instr_exist   IN OUT   BOOLEAN,
                                  O_rejected_packs       IN OUT   REJECTED_PACK_TABLE, 
                                  I_tsf_no               IN       TSFDETAIL.TSF_NO%TYPE)
return BOOLEAN IS

   L_program        VARCHAR2(61)   := 'TSF_VALIDATE_SQL.PACK_REPACK_INSTR_EXISTS';
   L_pack_no        ITEM_MASTER.ITEM%TYPE;
   L_tsf_qty        TSFDETAIL.TSF_QTY%TYPE;
   n                INTEGER(3)  := 0;
   L_packing_exists BOOLEAN;
   L_xform_exists   BOOLEAN;

   cursor C_TSF_ITEMS is
      select t.item pack_no,
             t.tsf_qty
        from tsfdetail t,
             item_master i
       where tsf_no = I_tsf_no
         and t.item = i.item
         and i.pack_ind = 'Y';

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;


   -- C_TSF_ITEMS selects all pack item on tsf_detail
   -- XFORM_PACKING_EXIST ensures they have a record on tsf_packing_detail
   -- as an item or as part of a new pack.

   for rec in C_TSF_ITEMS loop
      L_pack_no := rec.pack_no;
      L_tsf_qty := rec.tsf_qty;

      if ITEM_XFORM_PACK_SQL.XFORM_PACKING_EXIST(O_error_message,
                                                 L_xform_exists,
                                                 L_packing_exists,
                                                 I_tsf_no,
                                                 L_pack_no) = FALSE then 
         return FALSE;
      end if;

      if L_packing_exists = FALSE and L_xform_exists = TRUE then

         if n=0 then -- if it's the first pack found (this avoids the no data found error 
                     --                               in checking the O_rejected_packs(-1) record

            O_repack_instr_exist := FALSE;
            O_rejected_packs(n).pack_no := L_pack_no;
            O_rejected_packs(n).qty := L_tsf_qty;
            n := n + 1; 
         elsif NVL(O_rejected_packs(n-1).pack_no,-999) != L_pack_no then
         -- if the current pack is not on the rejected packs table then add it
            O_rejected_packs(n).pack_no := L_pack_no;
            O_rejected_packs(n).qty := L_tsf_qty;
            n := n + 1;
         end if; 
         
      end if;

   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;
END PACK_REPACK_INSTR_EXISTS;
------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_TSF_PACKS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_rejected_packs_table   IN       REJECTED_PACK_TABLE, 
                           I_tsf_no                 IN       TSFDETAIL.TSF_NO%TYPE)
return BOOLEAN IS

   L_program        VARCHAR2(61)   := 'TSF_VALIDATE_SQL.EXPLODE_TSF_PACKS';
   L_tsf_packing_id TSF_PACKING.TSF_PACKING_ID%TYPE;
   L_return_code    BOOLEAN;
   n                INTEGER(3)  := 0;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_rejected_packs_table is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rejected_packs_table',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   for n IN I_rejected_packs_table.FIRST .. I_rejected_packs_table.LAST LOOP
      if ITEM_XFORM_PACK_SQL.NEXT_PACKING_ID(O_error_message,
                                             L_return_code,
                                             L_tsf_packing_id) = FALSE  or L_return_code = FALSE then
        return FALSE; 
      end if;
      ---
      insert into tsf_packing (tsf_packing_id,
                               tsf_no,
                               set_no)
                       (select L_tsf_packing_id,
                               I_tsf_no,
                               NVL(max(set_no),0) +1
                          from tsf_packing
                         where tsf_no = I_tsf_no);


      if ITEM_XFORM_PACK_SQL.EXPLODE_PACK(O_error_message,
                                          I_rejected_packs_table(n).pack_no,
                                          L_tsf_packing_id,
                                          I_tsf_no,
                                          I_rejected_packs_table(n).qty) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;
EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;

END EXPLODE_TSF_PACKS; 
-------------------------------------------------------------------------------------------------
FUNCTION TSF_PRICE_EXCEED_WAC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_wac_exceeded    IN OUT   BOOLEAN,
                              I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE,
                              I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                              I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)   := 'TRANSFER_SQL.TSF_PRICE_EXCEED_WAC';
   L_invalid_param  VARCHAR2(30);
   L_wac_check      VARCHAR2(1)    := 'N';

   cursor C_CHECK_WAC is
    select 'Y'
      from item_loc_soh il,
           tsfdetail td
     where td.tsf_no = I_tsf_no
       and td.item = il.item
       and il.loc = I_from_loc
       and il.loc_type = I_from_loc_type
       and td.tsf_price > il.av_cost;
BEGIN

   if I_from_loc IS NULL
      then L_invalid_param := 'I_from_loc';
   elsif I_from_loc_type IS NULL
      then L_invalid_param := 'I_from_loc_type';
   elsif I_tsf_no IS NULL
      then L_invalid_param := 'I_tsf_no';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', L_invalid_param, L_program, NULL);
      return FALSE;
   end if;

   O_wac_exceeded := FALSE;

   open C_CHECK_WAC;
   fetch C_CHECK_WAC into L_wac_check;
   close C_CHECK_WAC;

   if L_wac_check = 'Y' then
      O_wac_exceeded := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END TSF_PRICE_EXCEED_WAC;
-------------------------------------------------------------------------------------
FUNCTION OVERWRITE_TSF_PRICE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE,
                             I_child_tsf_no    IN       TSFDETAIL.TSF_NO%TYPE,
                             I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                             I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)   := 'TRANSFER_SQL.OVERWRITE_TSF_PRICE';
   L_invalid_param  VARCHAR2(30);
   L_item           TSFDETAIL.ITEM%TYPE;
   L_av_cost        ITEM_LOC_SOH.AV_COST%TYPE;
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   
   cursor C_CHECK_WAC is
    select td.item,
           av_cost 
      from item_loc_soh il,
           tsfdetail td
     where td.tsf_no = I_tsf_no
       and td.item = il.item
       and il.loc = I_from_loc
       and il.loc_type = I_from_loc_type
       and td.tsf_price > il.av_cost;

   cursor C_LOCK_TSFDETAIL is 
      select 'x'
        from tsfdetail
       where item = L_item
         and (tsf_no = I_tsf_no
             or tsf_no = I_child_tsf_no)
         and tsf_price > L_av_cost
         for update nowait;
BEGIN

   if I_from_loc IS NULL
      then L_invalid_param := 'I_from_loc';
   elsif I_from_loc_type IS NULL
      then L_invalid_param := 'I_from_loc_type';
   elsif I_tsf_no IS NULL
      then L_invalid_param := 'I_tsf_no';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', L_invalid_param, L_program, NULL);
      return FALSE;
   end if;   

   for rec in C_CHECK_WAC loop
      L_item := rec.item;
      L_av_cost := rec.av_cost;

      open C_LOCK_TSFDETAIL;
      close C_LOCK_TSFDETAIL;

      update tsfdetail
         set tsf_price = L_av_cost,
             updated_by_rms_ind = 'Y'
       where item = L_item
         and (tsf_no = I_tsf_no
             or tsf_no = I_child_tsf_no);

   end loop;
 
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'TSFDETAIL',
                                            I_tsf_no,
                                            L_item);
      return FALSE;
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END OVERWRITE_TSF_PRICE;
-------------------------------------------------------------------------------------
FUNCTION VALID_FROM_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_desc       IN OUT   ITEM_MASTER.ITEM_DESC%TYPE,
                         I_item            IN       ITEM_MASTER.ITEM%TYPE,
                         I_item_type       IN       VARCHAR2,
                         I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                         I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                         I_finisher        IN       TSFHEAD.FROM_LOC%TYPE,
                         I_finisher_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                         I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                         I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                         I_dept            IN       DEPS.DEPT%TYPE)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50)   := 'TSF_VALIDATE_SQL.VALID_FROM_ITEM';
   L_comp_item         ITEM_MASTER.ITEM%TYPE;
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   L_valid             BOOLEAN;
   L_consignment       BOOLEAN;
   L_receive_as_type   ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
   L_to_loc            ITEM_LOC.LOC%TYPE;
   L_to_loc_type       ITEM_LOC.LOC_TYPE%TYPE;
   L_item_master       V_ITEM_MASTER%ROWTYPE;
   L_valid_parent      ITEM_MASTER.ITEM%TYPE := NULL;

   cursor C_COMPONENT_ITEM is
      select item
        from v_packsku_qty
       where pack_no = I_item;
       
   cursor C_CHK_VALID_PARENT is
       select item 
         from v_item_master i 
        where (i.item_parent    = I_item
           or i.item_grandparent = I_item)
          and item_level = tran_level;

   CURSOR C_CHK_VALID_PARENT_DIFF IS 
       SELECT item 
       FROM v_item_master i
       WHERE (i.item_parent    = I_item
          or i.item_grandparent = I_item)
         AND i.diff_1  is not null
         and item_level = tran_level;

   -------------------------------------------------------------------------------
   FUNCTION CHECK_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_il_item         IN       ITEM_MASTER.ITEM%TYPE,
                           I_il_from_loc     IN       TSFHEAD.FROM_LOC%TYPE)
      RETURN BOOLEAN IS
      L_program   VARCHAR2(50)   := 'TSF_VALIDATE_SQL.VALID_FROM_ITEM.CHECK_ITEM_LOC';
      L_exists    BOOLEAN;
      L_status    ITEM_MASTER.STATUS%TYPE;

   BEGIN
      if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                           I_il_item,
                                           I_il_from_loc,
                                           L_exists) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_LOC_EXIST',
                                               I_il_item,
                                               to_char(I_il_from_loc),
                                               NULL);
         return FALSE;
      end if;
      ---
      if ITEMLOC_ATTRIB_SQL.ITEM_STATUS(O_error_message,
                                        I_il_item,
                                        I_il_from_loc,
                                        L_status) = FALSE then
         return FALSE;
      end if;
      ---
      if L_status = 'D' then
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_LOC_DELETE', 
                                               I_il_item, 
                                               to_char(I_il_from_loc),
                                               NULL);
         return FALSE;
      end if;
      ---  
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
   END CHECK_ITEM_LOC;
   ----------------------------------------------------------------------------
BEGIN
   O_item_desc := NULL;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_ITEM_MASTER(O_error_message,
                                                   L_valid,
                                                   L_item_master,
                                                   I_item)= FALSE or L_valid = FALSE then
      return FALSE;
   end if;
   ---
   if L_item_master.inventory_ind = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_SELECT_NON_INV',
                                             I_item,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   if L_item_master.status != 'A' then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_MUST_BE_APPROVED',
                                             I_item,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_dept is NOT NULL and L_item_master.dept != I_dept then
      O_error_message := SQL_LIB.CREATE_MSG('SKU_NO_STK_DEPT',
                                            I_item,
                                            I_dept,
                                            NULL);
      return FALSE;
   end if;
   ---   
   if I_item_type = 'I' then
      if L_item_master.item_level <> L_item_master.tran_level then
         O_error_message := SQL_LIB.CREATE_MSG('CANNOT_ENT_STYLES',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   elsif I_item_type = 'ST' or I_item_type = 'STC' then
      if L_item_master.item_level >= L_item_master.tran_level then
         O_error_message := SQL_LIB.CREATE_MSG('ABOVE_TRAN_LEVEL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      if I_item_type = 'ST' then
         open C_CHK_VALID_PARENT;
         fetch C_CHK_VALID_PARENT into L_valid_parent;
         close C_CHK_VALID_PARENT;

         if L_valid_parent is NULL then 
            O_error_message := SQL_LIB.CREATE_MSG('NOT_VALID_PARENT',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      END if;

      if I_item_type = 'STC' THEN 
         OPEN C_CHK_VALID_PARENT_DIFF;
         FETCH C_CHK_VALID_PARENT_DIFF INTO L_valid_parent;
         CLOSE C_CHK_VALID_PARENT_DIFF;

         if L_valid_parent is NULL then 
            O_error_message := SQL_LIB.CREATE_MSG('NOT_VALID_PARENT',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      END if;
      
   end if;
   ---
   if ITEM_ATTRIB_SQL.CONSIGNMENT_ITEM(O_error_message,
                                       I_item,
                                       L_consignment) = FALSE then
      return FALSE;
   end if;
   ---
   if L_consignment = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('SKU_CSIGN_STK_NO_TRAN',
                                            I_item,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_finisher is NULL then
      L_to_loc      := I_to_loc;
      L_to_loc_type := I_to_loc_type;
   else
      L_to_loc      := I_finisher;
      L_to_loc_type := I_finisher_type;
   end if;
   ---
   if I_item_type = 'I' then
      if L_item_master.pack_ind = 'Y' then
         if I_from_loc_type = 'W' then
            if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                      L_receive_as_type,
                                                      I_item,
                                                      I_from_loc) = FALSE then
               return FALSE;
            end if;
            ---
            if NVL(L_receive_as_type, 'P') = 'E' then
               O_error_message := SQL_LIB.CREATE_MSG('TSF_RCV_AS_TYPE_WH',
                                                     I_from_loc,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;
         end if;
         ---
         if I_from_loc_type = 'S' then
            O_error_message := SQL_LIB.CREATE_MSG('PACK_NOT_STORE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;

         if CHECK_ITEM_LOC(O_error_message,
                           I_item,
                           I_from_loc) = FALSE then
            return FALSE;
         end if;         
         ---
         if I_finisher is NULL then
            -- Validate the Pack item at the Final destination (Use I_to_loc and not L_to_loc)
            -- Packs should not be validated at the finisher since they are always received as eaches
            if TSF_VALIDATE_SQL.VALID_TO_ITEM(O_error_message,
                                              I_item,
                                              I_to_loc,
                                              I_to_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         for rec in C_COMPONENT_ITEM loop
            if CHECK_ITEM_LOC(O_error_message,
                              rec.item,
                              I_from_loc) = FALSE then
               return FALSE;
            end if;
            ---
            if TSF_VALIDATE_SQL.VALID_TO_ITEM(O_error_message,
                                              rec.item,
                                              L_to_loc,
                                              L_to_loc_type) = FALSE then
               return FALSE;
            end if;
         end loop;
      else
         if CHECK_ITEM_LOC(O_error_message,
                           I_item,
                           I_from_loc) = FALSE then
            return FALSE;
         end if;
         ---
         if TSF_VALIDATE_SQL.VALID_TO_ITEM(O_error_message,
                                           I_item,
                                           L_to_loc,
                                           L_to_loc_type) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      if I_item_type = 'ST' then 
         for rec in C_CHK_VALID_PARENT loop 
            if CHECK_ITEM_LOC(O_error_message,
                              rec.item,
                              I_from_loc) = FALSE then
               return FALSE;
            end if;
            ---
            if TSF_VALIDATE_SQL.VALID_TO_ITEM(O_error_message,
                                              rec.item,
                                              L_to_loc,
                                              L_to_loc_type) = FALSE then
               return FALSE;
            end if;
         end loop;
      end if;
      if I_item_type = 'STC' then 
         for rec in C_CHK_VALID_PARENT_DIFF loop 
            if CHECK_ITEM_LOC(O_error_message,
                              rec.item,
                              I_from_loc) = FALSE then
               return FALSE;
            end if;
            ---
            if TSF_VALIDATE_SQL.VALID_TO_ITEM(O_error_message,
                                              rec.item,
                                              L_to_loc,
                                              L_to_loc_type) = FALSE then
               return FALSE;
            end if;
         end loop;
      end if;
   end if;
   ---
   O_item_desc := L_item_master.item_desc;
   ---
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALID_FROM_ITEM;
-------------------------------------------------------------------------------------
FUNCTION VALID_TO_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_loc             IN       TSFHEAD.FROM_LOC%TYPE,
                       I_loc_type        IN       TSFHEAD.FROM_LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)   := 'TSF_VALIDATE_SQL.VALID_TO_ITEM';
   
   L_pack_ind       ITEM_MASTER.PACK_IND%TYPE := 'N';
   L_sellable_ind   ITEM_MASTER.SELLABLE_IND%TYPE := 'N';
   L_comp_item      ITEM_MASTER.ITEM%TYPE;
   L_loc_type       TSFHEAD.FROM_LOC_TYPE%TYPE;

   cursor C_GET_PACK_IND is
      select pack_ind
        from item_master
       where item = I_item;

   cursor C_COMPONENT_ITEM is
      select item
        from v_packsku_qty
       where pack_no = I_item;
   -------------------------------------------------------------------------------
   FUNCTION CHECK_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_il_item          IN       ITEM_MASTER.ITEM%TYPE,
                          I_il_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                          I_il_loc_type      IN       TSFHEAD.FROM_LOC_TYPE%TYPE)
      RETURN BOOLEAN IS
      L_program       VARCHAR2(65)   := 'TSF_VALIDATE_SQL.VALID_TO_ITEM.CHECK_ITEM_LOC';
                      
      L_exists        BOOLEAN;
      L_status        ITEM_MASTER.STATUS%TYPE;
      L_loc_type      TSFHEAD.FROM_LOC_TYPE%TYPE := I_il_loc_type;
      L_ranged_ind    ITEM_LOC.RANGED_IND%TYPE;
      ---
      L_new_loc_ind   VARCHAR2(1) := NULL;
      
      cursor C_GET_RANGED_IND is
         select ranged_ind
           from item_loc
          where item = I_il_item
            and loc = I_il_loc;
      
   BEGIN
      if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                           I_il_item,
                                           I_il_loc,
                                           L_exists) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = TRUE then
         if ITEMLOC_ATTRIB_SQL.ITEM_STATUS(O_error_message,
                                           I_il_item,
                                           I_il_loc,
                                           L_status) = FALSE then
            return FALSE;
         end if;
         ---
         if L_status in ('D','I') then
            O_error_message := SQL_LIB.CREATE_MSG('ITEM_STATUS_TSF',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;         
         ---
         open C_GET_RANGED_IND;
         fetch C_GET_RANGED_IND into L_ranged_ind;
         close C_GET_RANGED_IND;
         
         -- update ranged_ind if the item is to be included in the transaction
         if L_ranged_ind = 'N' then
            if TRANSFER_SQL.UPDATE_RANGED_IND(O_error_message,
                                              I_il_item,
                                              I_il_loc) = FALSE then
               return FALSE;  
            end if;
         end if;
         
      else 
         -- item-loc record does not exist and user chose to range the unranged item-loc combination
         if I_il_loc_type = 'I' then 
            L_loc_type := 'W';
         end if;
         ---
         L_new_loc_ind := 'Y';
         ---
         if NEW_ITEM_LOC(O_error_message, I_il_item, I_il_loc, NULL, NULL, L_loc_type,
                         NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                         NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                         NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                         NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                         NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                         NULL, NULL, 'N', NULL, NULL, L_new_loc_ind, 'Y') = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      return TRUE;
      ---
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
   END CHECK_ITEM_LOC;
   -------------------------------------------------------------------------------
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PACK_IND',
                    'item_master',
                    NULL);
   open C_GET_PACK_IND;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PACK_IND',
                    'item_master',
                    NULL);
   fetch C_GET_PACK_IND into L_pack_ind;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PACK_IND',
                    'item_master',
                    NULL);
   close C_GET_PACK_IND;
   ---
   if CHECK_ITEM_LOC(O_error_message,
                     I_item,
                     I_loc,
                     I_loc_type) = FALSE then
      return FALSE;
   end if;
   ---
   if L_pack_ind = 'Y' then   
      for rec in C_COMPONENT_ITEM loop
         if CHECK_ITEM_LOC(O_error_message,
                           rec.item,
                           I_loc,
                           I_loc_type) = FALSE then
            return FALSE;
         end if;
      end loop;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALID_TO_ITEM;
-------------------------------------------------------------------------------------
FUNCTION CHECK_IL_RANGING(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_item_not_ranged   IN OUT   BOOLEAN,
                          I_item              IN       TSFDETAIL.ITEM%TYPE,
                          I_itemlist          IN       SKULIST_DETAIL.SKULIST%TYPE,
                          I_item_parent       IN       ITEM_MASTER.ITEM%TYPE,
                          I_diff_id           IN       DIFF_IDS.DIFF_ID%TYPE,
                          I_dept              IN       DEPS.DEPT%TYPE,
                          I_tsf_no            IN       TSFHEAD.TSF_NO%TYPE,  --adding for approval checking (so pass in either item stuff above and loc. or tsfno and loc
                          I_location          IN       TSFHEAD.TO_LOC%TYPE,
                          I_loc_type          IN       TSFHEAD.TO_LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)   := 'TSF_VALIDATE_SQL.CHECK_IL_RANGING';

   TYPE item_TABLE  is table of ITEM_MASTER.ITEM%TYPE     index by binary_integer;
   LP_item_table         item_TABLE;
   L_row_system_option   SYSTEM_OPTIONS%ROWTYPE;       
   L_from_item_dept      DEPS.DEPT%TYPE;
   L_to_item_dept        DEPS.DEPT%TYPE;
   L_item_ranged         VARCHAR2(2) := 'RA';  -- RA: Initial value standard for ranged. 
                                          -- NR: queried value if any one item in the
                                          --     item/item-parent/item-parent-diff/skulist is not ranged at the given location

   -- only need to know if any 1 item in the group of items that will be on the tsf
   -- is not ranged (null item_loc) or has a item_loc.ranged_ind of N
   cursor C_GET_UNRANGED_ITEMS is
      select 'NR'
        from (select it.item, il.ranged_ind
                from (select item
                        from item_master
                       where (item = I_item
                             or ((item_parent = I_item_parent
                                  or item_grandparent = I_item_parent)
                                and (NVL(diff_1,-1) = NVL(I_diff_id, NVL(diff_1,-1))
                                    or  NVL(diff_2,-1) = NVL(I_diff_id, NVL(diff_2,-1))
                                    or  NVL(diff_3,-1) = NVL(I_diff_id, NVL(diff_3,-1))
                                    or  NVL(diff_4,-1) = NVL(I_diff_id, NVL(diff_4,-1)))
                                and item_level = tran_level))   
                       union
                       select sk.item
                         from skulist_detail sk,
                              item_master im
                        where skulist = I_itemlist
                          and im.item = sk.item
                          and (I_dept is NULL
                               or im.dept = I_dept)
                          and im.inventory_ind = 'Y'
                       union
                       select vip.item
                         from v_item_packs vip -- For the pack item, this view has a record for each component item, 
                        where pack_no = I_item --  the component item's components (if the component is a pack), 
                       union                   --  and the component item's ancestor(s) 
                       select td.item
                         from tsfdetail td
                        where td.tsf_no = I_tsf_no) it, -- it query gets all the items
                       item_loc il
                 where it.item = il.item(+)
                   and il.loc(+) = I_location
                   and il.loc_type(+) = DECODE(I_loc_type,'I','W',I_loc_type)) innerq  -- inner query joins to item loc to get the ranged_ind
        where ranged_ind is NULL or ranged_ind = 'N'
          and rownum = 1;
  ---
  cursor C_GET_DEPT is
	  select im.dept 
	    from item_master im,
		     tsfdetail td 
	   where im.item = td.item 
	     and td.tsf_no = I_tsf_no;
  ---
  cursor C_GET_TO_DEPT is
     select dept 
      from item_master
     where item = I_item;
  ---
BEGIN
   ---
   -- Check for valid parameter values
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if (I_item is NULL and
      I_itemlist is NULL and
      I_item_parent is NULL and 
      I_tsf_no is NULL) or
      I_loc_type not in ('S','W','E','I') then
      
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_row_system_option) = false then
      return false;
   end if;
   ---
   if L_row_system_option.dept_level_transfers = 'Y' then
      ---
	  open C_GET_DEPT;
	  fetch C_GET_DEPT into L_from_item_dept;
	  close C_GET_DEPT;
	  ---
      open C_GET_TO_DEPT;
      fetch C_GET_TO_DEPT into L_to_item_dept;
      close C_GET_TO_DEPT;
	  ---
	 if L_from_item_dept <> L_to_item_dept then
	     O_error_message := SQL_LIB.CREATE_MSG('TSFXFORM_INV_DEPT',
                                               L_program,
                                               NULL,
                                               NULL);
         return FALSE;
     end if;
   end if;
   ---   
   O_item_not_ranged := FALSE;

   open C_GET_UNRANGED_ITEMS;
   fetch C_GET_UNRANGED_ITEMS into L_item_ranged;
   close C_GET_UNRANGED_ITEMS;

   if L_item_ranged = 'NR' then
      O_item_not_ranged := TRUE;
   end if;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_IL_RANGING;
----------------------------------------------------------------------
FUNCTION CHECK_CUST_RECS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_cust_exist_ind   IN OUT   VARCHAR2,
                         I_tsf_no           IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_cust_exist   VARCHAR2(1)    := NULL;
   L_program      VARCHAR2(50)   := 'TSF_VALIDATE_SQL.CHECK_CUST_RECS';

   cursor C_ORDCUST is
      select 'Y'
        from ordcust
       where tsf_no = I_tsf_no;

BEGIN
   open  C_ORDCUST;
   fetch C_ORDCUST into L_cust_exist;
   close C_ORDCUST;
   ---
   if L_cust_exist is NULL then
      O_cust_exist_ind := 'N';
   else
      O_cust_exist_ind := 'Y';
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_CUST_RECS;
-------------------------------------------------------------------------------------
END TSF_VALIDATE_SQL;
/


