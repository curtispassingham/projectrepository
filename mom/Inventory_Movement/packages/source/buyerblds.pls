CREATE OR REPLACE PACKAGE BUYER_BUILD_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------------------
-- Function: BUILD_PO_WRAPPER
-- Purpose : Gets called from the function CREATE_PO_SQL. Driving function for the rest of the program,
--           contains the driving cursor.A function will be called to create RMS orders from the info held
--           in the order table types. The orders created will be validated if
--           they are going to be set to submitted or approved status.
---------------------------------------------------------------------------------------------------------------
FUNCTION BUILD_PO_WRAPPER (I_session_id       IN     VARCHAR2,
                           I_approval_ind     IN     VARCHAR2,
                           O_error_message    IN OUT VARCHAR2)
RETURN BINARY_INTEGER;

END BUYER_BUILD_SQL;
/