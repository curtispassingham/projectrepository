CREATE OR REPLACE PACKAGE BODY RMSSUB_ASNIN AS
-- Define a record to store asn records
TYPE asn_record IS RECORD(asn               SHIPMENT.ASN%TYPE,
                          destination       SHIPMENT.TO_LOC%TYPE,
                          ship_date         SHIPMENT.SHIP_DATE%TYPE,
                          est_arr_date      SHIPMENT.EST_ARR_DATE%TYPE,
                          carrier           SHIPMENT.COURIER%TYPE,
                          ship_pay_method   ORDHEAD.SHIP_PAY_METHOD%TYPE,
                          inbound_bol       SHIPMENT.EXT_REF_NO_IN%TYPE,
                          supplier          ORDHEAD.SUPPLIER%TYPE,
                          carton_ind        VARCHAR2(1));

-- Define a record to store order records
TYPE order_record IS RECORD(asn              SHIPMENT.ASN%TYPE,
                            po_num           SHIPMENT.ORDER_NO%TYPE,
                            not_after_date   ORDHEAD.NOT_AFTER_DATE%TYPE);

-- Define a record to store carton records
TYPE carton_record IS RECORD(asn            SHIPMENT.ASN%TYPE,
                             po_num         SHIPMENT.ORDER_NO%TYPE,
                             carton_num     CARTON.CARTON%TYPE,
                             location       CARTON.LOCATION%TYPE);

-- Define a record to store item records
TYPE item_record IS RECORD(asn           SHIPMENT.ASN%TYPE,
                           po_num        SHIPMENT.ORDER_NO%TYPE,
                           carton_num    CARTON.CARTON%TYPE,
                           item          SHIPSKU.ITEM%TYPE,
                           ref_item      SHIPSKU.REF_ITEM%TYPE,
                           vpn           ITEM_SUPPLIER.VPN%TYPE,
                           alloc_loc     CARTON.LOCATION%TYPE,
                           qty_shipped   VARCHAR2(20));

-- Define a table based upon the carton record defined above.
 TYPE carton_table IS TABLE OF carton_record
    INDEX BY BINARY_INTEGER;

-- Define a table based upon the item record defined above.
 TYPE item_table IS TABLE OF item_record
    INDEX BY BINARY_INTEGER;


-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_ASN (O_error_message     OUT  VARCHAR2,
                    O_asn_record        OUT  nocopy asn_record,
                    I_asn            IN      "RIB_ASNInDesc_REC")
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_ORDER(O_error_message     OUT  VARCHAR2,
                     O_order_record      OUT  nocopy order_record,
                     I_order          IN      "RIB_ASNInPO_REC",
                     I_asn_no         IN      SHIPMENT.ASN%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_CARTON (O_error_message       OUT  VARCHAR2,
                       O_carton_record    IN OUT  nocopy carton_table,
                       I_carton           IN      "RIB_ASNInCtn_REC",
                       I_asn_no           IN      SHIPMENT.ASN%TYPE,
                       I_order_no         IN      SHIPMENT.ORDER_NO%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_ITEM (O_error_message             OUT  VARCHAR2,
                     O_item_record            IN OUT  nocopy item_table,
                     I_item                   IN      "RIB_ASNInItem_REC",
                     I_asn_no                 IN      SHIPMENT.ASN%TYPE,
                     I_order_no               IN      SHIPMENT.ORDER_NO%TYPE,
                     I_carton_no              IN      CARTON.CARTON%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ASN (O_error_message    OUT     VARCHAR2,
                      I_asn              IN      asn_record,
                      I_order            IN      order_record,
                      I_cartontable      IN      carton_table,
                      I_itemtable        IN      item_table,
                      I_message_type     IN      VARCHAR2)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DELETE (O_error_message     OUT  VARCHAR2,
                         I_asn            IN      "RIB_ASNInRef_REC")
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS (O_status               IN OUT  VARCHAR2,
                         IO_error_message       IN OUT  VARCHAR2,
                         I_cause                IN      VARCHAR2,
                         I_program              IN      VARCHAR2);
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status               IN OUT  VARCHAR2,
                        IO_error_message       IN OUT  VARCHAR2,
                        I_cause                IN      VARCHAR2,
                        I_program              IN      VARCHAR2)
IS
BEGIN
   API_LIBRARY.HANDLE_ERRORS(O_status,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_ASNIN.HANDLE_ERRORS',
                                             to_char(SQLCODE));

     API_LIBRARY.HANDLE_ERRORS(O_status,
                               IO_error_message,
                               API_LIBRARY.FATAL_ERROR,
                               'RMSSUB_ASNIN.HANDLE_ERRORS');


END HANDLE_ERRORS;
-----------------------------------------------------------------------------------------
-- PUBLIC FUNCTION --
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_STATUS_CODE   IN OUT VARCHAR2,
                  O_ERROR_MESSAGE IN OUT VARCHAR2,
                  I_MESSAGE       IN     RIB_OBJECT,
                  I_MESSAGE_TYPE  IN     VARCHAR2)
IS
   PROGRAM_ERROR    EXCEPTION;

   L_ref_message           "RIB_ASNInRef_REC";
   L_message               "RIB_ASNInDesc_REC";
   L_asn_record            asn_record;
   L_order_record          order_record;
   L_carton_record         carton_table;
   L_item_record           item_table;
   L_comment               VARCHAR2(255) := 'comment';
   L_profile_id            NUMBER := 1;
BEGIN

    if API_LIBRARY.INIT(O_error_message) = FALSE then
       raise PROGRAM_ERROR;
    end if;

    O_STATUS_CODE := API_CODES.SUCCESS;

    if lower(I_message_type) != 'asnindel' then
       -- validation is currently turned off.
       -- parse the document
      L_message := treat(I_MESSAGE AS "RIB_ASNInDesc_REC");

      if PARSE_ASN(O_error_message,
                   L_asn_record,
                   L_message) = FALSE then
        raise PROGRAM_ERROR;
      else
         if I_message_type is NULL or
            (lower(I_message_type) != 'asnincre' and
             lower(I_message_type) != 'asninmod') then
            O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE_TYPE', I_message_type, NULL, NULL);
            raise PROGRAM_ERROR;
         end if;

         if L_message is NULL or
            L_message.ASNInPO_TBL is NULL or
            (L_message.ASNInPO_TBL.FIRST - L_message.ASNInPO_TBL.LAST = 0 and
             L_message.ASNInPO_TBL.FIRST != 1) then
            O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
            raise PROGRAM_ERROR;
         end if;

         -- the detail records are retrieved in a list
         -- which I can then loop through and process.
         FOR i IN L_message.ASNInPO_TBL.FIRST..L_message.ASNInPO_TBL.LAST LOOP
            if PARSE_ORDER(O_error_message,
                           L_order_record,
                           L_message.ASNInPO_TBL(i),
                           L_message.asn_nbr) = FALSE then
               raise PROGRAM_ERROR;
            end if;

            -- Delete container and item records from previous order.

            L_item_record.DELETE;
            L_carton_record.DELETE;

            if L_asn_record.carton_ind = 'C' then

               if L_message.ASNInPO_TBL(i).ASNInCtn_TBL is NULL or
                  (L_message.ASNInPO_TBL(i).ASNInCtn_TBL.FIRST - L_message.ASNInPO_TBL(i).ASNInCtn_TBL.LAST = 0 and
                   L_message.ASNInPO_TBL(i).ASNInCtn_TBL.FIRST != 1) then
                  O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
                  raise PROGRAM_ERROR;
               end if;

               FOR j IN L_message.ASNInPO_TBL(i).ASNInCtn_TBL.FIRST..L_message.ASNInPO_TBL(i).ASNInCtn_TBL.LAST LOOP

                  if PARSE_CARTON(O_error_message,
                                  L_carton_record,
                                  L_message.ASNInPO_TBL(i).ASNInCtn_TBL(j),
                                  L_message.asn_nbr,
                                  L_message.ASNInPO_TBL(i).po_nbr) = FALSE then
                     raise PROGRAM_ERROR;
                  end if;

                  if L_message.ASNInPO_TBL(i).ASNInCtn_TBL(j).ASNInItem_TBL is NULL or
                     (L_message.ASNInPO_TBL(i).ASNInCtn_TBL(j).ASNInItem_TBL.FIRST - L_message.ASNInPO_TBL(i).ASNInCtn_TBL(j).ASNInItem_TBL.LAST = 0 and
                      L_message.ASNInPO_TBL(i).ASNInCtn_TBL(j).ASNInItem_TBL.FIRST != 1) then
                     O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
                     raise PROGRAM_ERROR;
                  end if;

                  FOR k IN L_message.ASNInPO_TBL(i).ASNInCtn_TBL(j).ASNInItem_TBL.FIRST..L_message.ASNInPO_TBL(i).ASNInCtn_TBL(j).ASNInItem_TBL.LAST LOOP
                     if PARSE_ITEM(O_error_message,
                                   L_item_record,
                                   L_message.ASNInPO_TBL(i).ASNInCtn_TBL(j).ASNInItem_TBL(k),
                                   L_message.asn_nbr,
                                   L_message.ASNInPO_TBL(i).po_nbr,
                                   L_message.ASNInPO_TBL(i).ASNInCtn_TBL(j).container_id) = FALSE then
                        raise PROGRAM_ERROR;
                     end if;
                  end LOOP;
               end LOOP;
            else
               -- A separate call to parse item is required in order
               -- to retrieve those items that are not part of a container.

               if L_message.ASNInPO_TBL(i).ASNInItem_TBL is NULL or
                  (L_message.ASNInPO_TBL(i).ASNInItem_TBL.FIRST - L_message.ASNInPO_TBL(i).ASNInItem_TBL.LAST = 0 and
                   L_message.ASNInPO_TBL(i).ASNInItem_TBL.FIRST != 1) then
                  O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
                  raise PROGRAM_ERROR;
               end if;

               FOR l IN L_message.ASNInPO_TBL(i).ASNInItem_TBL.FIRST..L_message.ASNInPO_TBL(i).ASNInItem_TBL.LAST LOOP
                  if PARSE_ITEM(O_error_message,
                                L_item_record,
                                L_message.ASNInPO_TBL(i).ASNInItem_TBL(l),
                                L_message.asn_nbr,
                                L_message.ASNInPO_TBL(i).po_nbr,
                                NULL) = FALSE then
                     raise PROGRAM_ERROR;
                  end if;
               end LOOP;
            end if;

            if PROCESS_ASN(O_error_message,
                           L_asn_record,
                           L_order_record,
                           L_carton_record,
                           L_item_record,
                           I_message_type) = FALSE then
               raise PROGRAM_ERROR;
            end if;
         end LOOP;
      end if;
   else
      L_ref_message := treat(I_MESSAGE AS "RIB_ASNInRef_REC");

      if L_ref_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      if PROCESS_DELETE(O_error_message,
                        L_ref_message) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   end if;

   return;
EXCEPTION
   when PROGRAM_ERROR then
     HANDLE_ERRORS(O_status_code,
                   O_error_message,
                   API_LIBRARY.FATAL_ERROR,
                   'RMSSUB_ASNIN.CONSUME');
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     'RMSSUB_ASNIN.CONSUME');
END CONSUME;
------------------------------------------------------------------------------------------------------
FUNCTION PARSE_ASN (O_error_message     OUT  VARCHAR2,
                    O_asn_record        OUT  nocopy asn_record,
                    I_asn            IN      "RIB_ASNInDesc_REC")
return BOOLEAN IS

L_date   DATE;

BEGIN
   O_asn_record.asn              := I_asn.asn_nbr;
   O_asn_record.destination      := I_asn.to_location;
   O_asn_record.ship_date        := I_asn.shipment_date;
   O_asn_record.est_arr_date     := I_asn.est_arr_date;
   O_asn_record.carrier          := I_asn.carrier_code;
   O_asn_record.ship_pay_method  := I_asn.ship_pay_method;
   O_asn_record.inbound_bol      := I_asn.bol_nbr;
   O_asn_record.supplier         := I_asn.vendor_nbr;
   O_asn_record.carton_ind       := I_asn.asn_type;

   return TRUE;
EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_ASNIN.PARSE_ASN',
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_ASN;
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_ORDER(O_error_message     OUT  VARCHAR2,
                     O_order_record      OUT  nocopy order_record,
                     I_order          IN      "RIB_ASNInPO_REC",
                     I_asn_no         IN      SHIPMENT.ASN%TYPE)
return BOOLEAN IS
BEGIN
   O_order_record.asn            := I_asn_no;
   O_order_record.po_num         := I_order.po_nbr;
   O_order_record.not_after_date := I_order.not_after_date;

   return TRUE;
EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_ASNIN.PARSE_ORDER',
                                            to_char(SQLCODE));
      return FALSE;
END PARSE_ORDER;
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_CARTON (O_error_message       OUT         VARCHAR2,
                       O_carton_record    IN OUT  nocopy carton_table,
                       I_carton           IN             "RIB_ASNInCtn_REC",
                       I_asn_no           IN             SHIPMENT.ASN%TYPE,
                       I_order_no         IN             SHIPMENT.ORDER_NO%TYPE)
return BOOLEAN IS
   k   NUMBER;
BEGIN
   k := (O_carton_record.COUNT);
   k := k + 1;

   O_carton_record(k).asn         := to_char(I_asn_no);
   O_carton_record(k).po_num      := I_order_no;
   O_carton_record(k).carton_num  := to_char(I_carton.container_id);
   O_carton_record(k).location    := I_carton.final_location;


   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_ASNIN.PARSE_CARTON',
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_CARTON;
-----------------------------------------------------------------------------------------------
FUNCTION PARSE_ITEM (O_error_message             OUT         VARCHAR2,
                     O_item_record            IN OUT  nocopy item_table,
                     I_item                   IN             "RIB_ASNInItem_REC",
                     I_asn_no                 IN             SHIPMENT.ASN%TYPE,
                     I_order_no               IN             SHIPMENT.ORDER_NO%TYPE,
                     I_carton_no              IN             CARTON.CARTON%TYPE)
return BOOLEAN IS
   j                    NUMBER;
   L_exists             VARCHAR2(1);
   L_supp_pack_exists   BOOLEAN;
   L_container_item     ITEM_MASTER.ITEM%TYPE;
   L_sup_qty_level      SUPS.SUP_QTY_LEVEL%TYPE;
   L_unit_qty           NUMBER(12,4);
   L_supp_pack_size     ORDSKU.SUPP_PACK_SIZE%TYPE;
   L_rtk_item           ITEM_MASTER.ITEM%TYPE  := NULL;

   cursor C_REF_ITEM is
      select im.item_parent
        from item_master im
       where im.item = I_item.ref_item;

   cursor C_VPN_ITEM is
      select its.item
        from item_supplier its,
             item_master im,
             ordhead oh
       where its.supplier     = oh.supplier
         and oh.order_no      = I_order_no
         and its.vpn          = I_item.vpn
         and its.item         = im.item
         and im.tran_level    = im.item_level;

   cursor C_GET_SUP_QTY_LEVEL is
     select s.sup_qty_level
       from ordhead oh,
            sups s
      where oh.order_no = I_order_no
        and s.supplier  = oh.supplier;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SUP_QTY_LEVEL',
                    'SUPS',
                    'Order No.: '||I_order_no);
   open C_GET_SUP_QTY_LEVEL;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SUP_QTY_LEVEL',
                    'SUPS',
                    'Order No.: '||I_order_no);
   fetch C_GET_SUP_QTY_LEVEL into L_sup_qty_level;

   if L_sup_qty_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DATA_FOUND',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SUP_QTY_LEVEL',
                    'SUPS',
                    'Order No.: '||I_order_no);
   close C_GET_SUP_QTY_LEVEL;

   if L_sup_qty_level = 'CA' then
      if ORDER_ITEM_ATTRIB_SQL.GET_SUPP_PACK_SIZE (O_error_message,
                                                   L_supp_pack_exists,
                                                   L_supp_pack_size,
                                                   I_order_no,
                                                   I_item.item_id)= FALSE then
         return FALSE;
      end if;

      L_unit_qty := NVL(L_supp_pack_size, 1) * I_item.unit_qty;
   else
      L_unit_qty := I_item.unit_qty;
   end if;

   j := (O_item_record.COUNT);
   j := j + 1;

   O_item_record(j).asn              := I_asn_no;
   O_item_record(j).po_num           := I_order_no;
   O_item_record(j).carton_num       := I_carton_no;
   O_item_record(j).item             := I_item.item_id;
   O_item_record(j).ref_item         := I_item.ref_item;
   O_item_record(j).vpn              := I_item.vpn;
   O_item_record(j).alloc_loc        := I_item.final_location;
   O_item_record(j).qty_shipped      := L_unit_qty;

   if I_item.item_id is NOT NULL then
      L_rtk_item := I_item.item_id;
   elsif I_item.item_id is NULL and
         I_item.ref_item is NOT NULL then
      -- supplier has returned item as REF_ITEM
      open C_REF_ITEM;
      fetch C_REF_ITEM into L_rtk_item;
      close C_REF_ITEM;

      if L_rtk_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_REF_ITEM', NULL, NULL, NULL);
            return false;
      end if;
   elsif I_item.item_id is NULL and
         I_item.vpn is NOT NULL then
      open C_VPN_ITEM;
      fetch C_VPN_ITEM into L_rtk_item;
      close C_VPN_ITEM;

      if L_rtk_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('VPN_INV_FOR_ORDER', I_item.vpn, I_order_no, NULL);
            return false;
      end if;
   else
      -- we didn't get item, ref_item or VPN
      O_error_message := SQL_LIB.CREATE_MSG('INV_SYSTEM_IND', NULL, NULL, NULL);
         return false;
   end if;

   if ITEM_ATTRIB_SQL.CONTENTS_ITEM_EXISTS(O_error_message,
                                           L_exists,
                                           L_rtk_item) = FALSE then
      return FALSE;
   end if;

   if L_exists = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('SA_CANT_USE_DEPOSIT_CONTR',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_CONTAINER_ITEM(O_error_message,
                                         L_container_item,
                                         L_rtk_item) = FALSE then
      return FALSE;
   end if;

   if L_container_item is NOT NULL then

      j := (O_item_record.COUNT);
      j := j + 1;

      O_item_record(j).asn              := I_asn_no;
      O_item_record(j).po_num           := I_order_no;
      O_item_record(j).carton_num       := I_carton_no;
      O_item_record(j).item             := L_container_item;
      O_item_record(j).ref_item         := NULL;
      O_item_record(j).vpn              := NULL;
      O_item_record(j).alloc_loc        := I_item.final_location;
      O_item_record(j).qty_shipped      := I_item.unit_qty;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_ASNIN.PARSE_ITEM',
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_ITEM;
--------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ASN (O_error_message    OUT    VARCHAR2,
                      I_asn              IN     asn_record,
                      I_order            IN     order_record,
                      I_cartontable      IN     carton_table,
                      I_itemtable        IN     item_table,
                      I_message_type     IN     varchar2)

return BOOLEAN IS

   L_exists               BOOLEAN := TRUE;
   L_loc_type             SHIPMENT.TO_LOC_TYPE%TYPE;
   L_shipment             SHIPMENT.SHIPMENT%TYPE;
   L_order_no             ORDHEAD.ORDER_NO%TYPE;
   L_premark_ind          ORDHEAD.PRE_MARK_IND%TYPE;
   L_supplier             ORDHEAD.SUPPLIER%TYPE;
   L_store_type           STORE.STORE_TYPE%TYPE;
   L_order_status         ORDHEAD.STATUS%TYPE;
   L_wf_order_no          WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_stockholding_store   STORE.STOCKHOLDING_IND%TYPE;
   L_ship_match           BOOLEAN;

BEGIN

   if ASN_SQL.RESET_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if ASN_SQL.VALIDATE_LOCATION(O_error_message,
                                L_loc_type,
                                I_asn.destination) = FALSE then
      return FALSE;
   end if;
   ---
   if L_loc_type = 'S' then
      select s.stockholding_ind,
             s.store_type
        into L_stockholding_store,
             L_store_type
        from store s
       where s.store = I_asn.destination;
   end if;
   ---
   if ASN_SQL.PROCESS_ORDER(O_error_message,
                            L_order_no,
                            L_premark_ind,
                            L_shipment,
                            L_ship_match,
                            I_asn.asn,
                            I_order.po_num,
                            I_asn.destination,
                            L_loc_type,
                            I_asn.ship_pay_method,
                            I_order.not_after_date,
                            I_asn.ship_date,
                            I_asn.est_arr_date,
                            I_asn.carrier,
                            I_asn.inbound_bol,
                            I_asn.supplier,
                            I_asn.carton_ind,
                            I_message_type) = FALSE then
      return FALSE;
   end if;
   ---
   if I_asn.carton_ind = 'C' then
      if I_cartontable is NOT NULL then
         FOR k IN 1..I_cartontable.COUNT LOOP
            if ASN_SQL.VALIDATE_CARTON(O_error_message,
                                       I_cartontable(k).carton_num,
                                       I_cartontable(k).location) = FALSE then
               return FALSE;
            end if;
         end LOOP;
      end if;
   end if;
   ---
   FOR i IN 1..I_itemtable.COUNT LOOP
      if ASN_SQL.CHECK_ITEM(O_error_message,
                            L_shipment,
                            I_asn.supplier,
                            I_itemtable(i).asn,
                            L_order_no,
                            I_asn.destination,
                            I_itemtable(i).alloc_loc,
                            I_itemtable(i).item,
                            I_itemtable(i).ref_item,
                            I_itemtable(i).vpn,
                            I_itemtable(i).carton_num,
                            L_premark_ind,
                            I_itemtable(i).qty_shipped,
                            L_ship_match,
                            L_loc_type) = FALSE then
         return FALSE;
      end if;
   end LOOP;
   ---
   if ASN_SQL.DO_SHIPSKU_INSERTS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   -- The status of the Franchise Order should be updated once the Franchise PO is shipped
   if L_store_type is NOT NULL then
      if L_store_type = 'F' then
         if WF_PO_SQL.SYNC_F_ORDER(O_error_message,
                                   L_wf_order_no,
                                   L_order_status,
                                   RMS_CONSTANTS.WF_ACTION_SHIPPED,
                                   L_order_no,
                                   NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   -- Drop ship customer PO and Franchise PO with a non-stockholding store is automatically received
   if L_loc_type = 'S' then
      if L_stockholding_store = 'N' then

         if ORDER_RCV_SQL.RECEIVE_AUTO_PO(O_error_message,
                                          L_order_no,
                                          L_shipment,
                                          I_asn.destination,
                                          I_asn.ship_date,
                                          'Y') = FALSE then  -- I_invoice_ind
            return FALSE;
         end if;

      end if;
   end if;
   ---

   if ASN_SQL.CREATE_INVOICE(O_error_message,
                             L_shipment,
                             I_asn.supplier,
                             L_ship_match) = FALSE then
      return FALSE;
   end if;
   ---
   if NOT SHIPMENT_ATTRIB_SQL.CHECK_SHIPSKU(O_error_message,
                                            L_exists,
                                            L_shipment) then
      return FALSE;
   end if;

   if NOT L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('NO_SHIPSKU_RECORDS',
                                            L_shipment,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'RMSSUB_ASNIN.PROCESS_ASN',
                                          to_char(SQLCODE));
      return FALSE;
END PROCESS_ASN;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_DELETE (O_error_message     OUT  VARCHAR2,
                         I_asn            IN      "RIB_ASNInRef_REC")
return BOOLEAN IS
   L_asn     SHIPMENT.ASN%TYPE := NULL;
BEGIN
   L_asn  := I_asn.asn_nbr;

   if ASN_SQL.DELETE_ASN(O_error_message,
                         L_asn) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_ASNIN.PROCESS_DELETE',
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_DELETE;
-----------------------------------------------------------------------------------------
END RMSSUB_ASNIN;
/