
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE WO_SQL AUTHID CURRENT_USER AS

------------------------------------------------------------------------
-- Function Name: DELETE_WO
-- Purpose:       This function will delete work orders according to the
--                passed in criteria.
-- Created:       29-Jul-98
------------------------------------------------------------------------
FUNCTION DELETE_WO(O_error_message  IN OUT  VARCHAR2,
                   I_wo_id          IN      WO_HEAD.WO_ID%TYPE,
                   I_order_no       IN      WO_HEAD.ORDER_NO%TYPE,
                   I_tsfalloc_no    IN      WO_HEAD.TSFALLOC_NO%TYPE,
                   I_item           IN      WO_DETAIL.ITEM%TYPE,
                   I_location       IN      WO_DETAIL.LOCATION%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: INSERT_SKULOCS
-- Purpose:       This function will insert new records into wo_detail
--                for the item/location selected in the order matrix worksheet
--                form, using the super filter, or in the transfer allocation form.
-- Created:       29-Jul-98
------------------------------------------------------------------------
FUNCTION INSERT_SKULOCS(O_error_message   IN OUT VARCHAR2,
                        I_wo_id           IN     WO_HEAD.WO_ID%TYPE,
                        I_order_alloc_ind IN     VARCHAR2,
                        I_order_alloc_no  IN     WO_HEAD.ORDER_NO%TYPE,
                        I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                        I_tsf_to_loc      IN     TSFHEAD.TO_LOC%TYPE,
                        I_tsf_itemlist    IN     SKULIST_HEAD.SKULIST%TYPE,
                        I_tsf_item        IN     TSFDETAIL.ITEM%TYPE,
                        I_tsf_parent      IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                        I_tsf_diff1       IN     ITEM_MASTER.DIFF_1%TYPE,
                        I_tsf_diff2       IN     ITEM_MASTER.DIFF_2%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: EXPLD_WO_PACK
-- Purpose:       This function will explode packs on work orders out to
--                their comoponent items.  If a work order line item already
--                exists for a item/location in the pack, that item/location
--                will be skipped over in this function.
-- Created:       29-Jul-98
------------------------------------------------------------------------
FUNCTION EXPLD_WO_PACK(O_error_message  IN OUT  VARCHAR2,
                       I_order_no       IN      WO_HEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: INS_WO_DETAIL
-- Purpose:       This function will merge records from WO_SKU_LOC and
--                WO_WIP into WO_DETAIL.
-- Created:       15-JAN-02
------------------------------------------------------------------------
FUNCTION INS_WO_DETAIL(O_error_message  IN OUT  VARCHAR2)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: POP_WO_INFO
-- Purpose:       This function will populate the tables WO_SKU_LOC
--                and WO_WIP for the current work order from WO_DETAIL.
-- Created:       15-JAN-02
------------------------------------------------------------------------
FUNCTION POP_WO_INFO(O_error_message  IN OUT  VARCHAR2,
                     I_wo_id          IN      WO_HEAD.WO_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: DEL_WO_DETAIL
-- Purpose:       This function will delete records from the table WO_DETAIL
--                for the current work order.  This is used when the user
--                presses the DELETE WIP button on the work order form.
--                inserted into WO_DETAIL.
-- Created:       15-JAN-02
------------------------------------------------------------------------
FUNCTION DEL_WO_DETAIL(O_error_message  IN OUT  VARCHAR2,
                       I_wo_id          IN      WO_HEAD.WO_ID%TYPE,
                       I_seq_no         IN      WO_WIP.SEQ_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: DEL_WO_RECS
-- Purpose:       This function will delete records from the tables WO_SKU_LOC
--                and WO_WIP for the current work order after they have been
--                inserted into WO_DETAIL.
-- Created:       15-JAN-02
------------------------------------------------------------------------
FUNCTION DEL_WO_RECS(O_error_message  IN OUT  VARCHAR2,
                     I_wo_id          IN      WO_HEAD.WO_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: DEL_WO_ITEM
-- Purpose:       This function will delete records from the table WO_DETAIL
--                that are associated with the work order, item and location
--                passed into the function.
-- Created:       15-JAN-02
------------------------------------------------------------------------
FUNCTION DEL_WO_ITEM(O_error_message  IN OUT  VARCHAR2,
                     I_wo_id          IN      WO_HEAD.WO_ID%TYPE,
                     I_item           IN      WO_SKU_LOC.ITEM%TYPE,
                     I_location       IN      WO_SKU_LOC.LOCATION%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: CK_WO_CODE
-- Purpose:       This function will check to see if any wip_codes exists
--                on wo_wip more than once for the inpute wo_id.
-- Created:       15-JAN-02
------------------------------------------------------------------------
FUNCTION CK_WO_CODE(O_error_message  IN OUT  VARCHAR2,
                    I_wo_id          IN      WO_HEAD.WO_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
END WO_SQL;
/
