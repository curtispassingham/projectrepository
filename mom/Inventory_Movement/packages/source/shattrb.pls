CREATE OR REPLACE PACKAGE BODY SHIPMENT_ATTRIB_SQL AS
-------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_order_no       IN OUT  SHIPMENT.ORDER_NO%TYPE,
                  O_bol_no         IN OUT  SHIPMENT.BOL_NO%TYPE,
                  O_asn            IN OUT  SHIPMENT.ASN%TYPE,
                  O_ship_date      IN OUT  SHIPMENT.SHIP_DATE%TYPE,
                  O_receive_date   IN OUT  SHIPMENT.RECEIVE_DATE%TYPE,
                  O_ship_origin    IN OUT  SHIPMENT.SHIP_ORIGIN%TYPE,
                  O_status_code    IN OUT  SHIPMENT.STATUS_CODE%TYPE,
                  O_to_loc         IN OUT  SHIPMENT.TO_LOC%TYPE,
                  O_to_loc_type    IN OUT  SHIPMENT.TO_LOC_TYPE%TYPE,
                  O_from_loc       IN OUT  SHIPMENT.FROM_LOC%TYPE,
                  O_from_loc_type  IN OUT  SHIPMENT.FROM_LOC_TYPE%TYPE,
                  I_shipment       IN      SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN is

   L_exists  VARCHAR2(1) := 'N';

   cursor C_shipment is
      select 'Y',
             order_no,
             bol_no,
             asn,
             ship_date,
             receive_date,
             ship_origin,
             status_code,
             to_loc,
             to_loc_type,
             from_loc,
             from_loc_type
        from shipment
       where shipment = I_shipment;

BEGIN
   SQL_LIB.SET_MARK('OPEN' , 'C_shipment', 'SHIPMENT', 'SHIPMENT:'||to_char(I_shipment));
   open C_shipment;
   SQL_LIB.SET_MARK('FETCH' , 'C_shipment',  'SHIPMENT', 'SHIPMENT:'||to_char(I_shipment));
   fetch C_shipment into L_exists,
                         O_order_no,
                         O_bol_no,
                         O_asn,
                         O_ship_date,
                         O_receive_date,
                         O_ship_origin,
                         O_status_code,
                         O_to_loc,
                         O_to_loc_type,
                         O_from_loc,
                         O_from_loc_type;
   SQL_LIB.SET_MARK('CLOSE' , 'C_shipment', 'SHIPMENT', 'SHIPMENT:'||to_char(I_shipment));
   close C_shipment;

   if L_exists = 'N' then
      O_error_message := sql_lib.create_msg('INV_SHIP',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SHIPMENT_ATTRIB_SQL.GET_INFO',
                                            To_Char(SQLCODE));
      RETURN FALSE;

END GET_INFO;
-------------------------------------------------------------------------------------
FUNCTION NEXT_SHIPMENT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_shipment_number  IN OUT  SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN IS

   L_shipment_sequence      SHIPMENT.SHIPMENT%TYPE;
   L_wrap_sequence_number   SHIPMENT.SHIPMENT%TYPE;
   L_first_time             BOOLEAN := TRUE;
   L_exists                 VARCHAR2(1);

   cursor C_GET_SHIPMENT is
      select 'Y'
        from shipment
       where shipment = O_shipment_number;

BEGIN
   loop
      select shipment_sequence.NEXTVAL
        into L_shipment_sequence
        from sys.dual;

      if L_first_time then
         L_wrap_sequence_number := L_shipment_sequence;
         L_first_time := FALSE;
      elsif L_shipment_sequence = L_wrap_sequence_number then
         O_error_message := 'Fatal error - no available shipment numbers';
         return FALSE;
      end if;

      O_shipment_number := L_shipment_sequence;

      L_exists := 'N';
      open  C_GET_SHIPMENT;
      fetch C_GET_SHIPMENT into L_exists;
      close C_GET_SHIPMENT;
      if L_exists = 'N' then
         exit;
      end if;

   end loop;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SHIPMENT_ATTRIB_SQL.NEXT_SHIPMENT',
                                            to_char(SQLCODE));
      return FALSE;
END NEXT_SHIPMENT;
-------------------------------------------------------------------------------------
FUNCTION CHECK_SHIPSKU(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT  BOOLEAN,
                       I_shipment       IN      SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN IS

   L_exists  VARCHAR2(1) := 'N';

   cursor C_SHIPSKU is
      select 'Y'
        from shipsku
       where shipment = I_shipment;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_SHIPSKU','SHIPSKU','SHIPMENT:'||to_char(I_shipment));
   open C_SHIPSKU;
   SQL_LIB.SET_MARK('FETCH','C_SHIPSKU','SHIPSKU','SHIPMENT:'||to_char(I_shipment));
   fetch C_SHIPSKU into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_SHIPSKU','SHIPSKU','SHIPMENT:'||to_char(I_shipment));
   close C_SHIPSKU;

   if L_exists = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;
EXCEPTION
    WHEN OTHERS THEN
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'SHIPMENT_ATTRIB_SQL.CHECK_SHIPSKU',
                                               to_char(SQLCODE));
        return FALSE;

END CHECK_SHIPSKU;
----------------------------------------------------------------------------------------------
FUNCTION TSF_OR_ALLOC(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_tsf_or_alloc   IN OUT  VARCHAR2,
                      O_found          IN OUT  BOOLEAN,
                      I_distro_no      IN      SHIPSKU.DISTRO_NO%TYPE)
   RETURN BOOLEAN IS

   L_distro_type  VARCHAR2(1) := NULL;

   cursor C_CHECK_TSF is
      select 'T'
        from tsfhead
       where tsf_no = I_distro_no;

   cursor C_CHECK_ALLOC is
      select 'A'
        from alloc_header
       where alloc_no = I_distro_no;

BEGIN

   -- Check the transfer table
   SQL_LIB.SET_MARK('OPEN','C_CHECK_TSF','TSFHEAD', NULL);
   open C_CHECK_TSF;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_TSF','TSFHEAD', NULL);
   fetch C_CHECK_TSF into L_distro_type;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_TSF','TSFHEAD', NULL);
   close  C_CHECK_TSF;

   -- If no transfer found, check the allocation table
   if L_distro_type is NULL then
      SQL_LIB.SET_MARK('OPEN','C_CHECK_ALLOC','ALLOC_HEADER', NULL);
      open C_CHECK_ALLOC;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_ALLOC','ALLOC_HEADER', NULL);
      fetch C_CHECK_ALLOC into L_distro_type;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_ALLOC','ALLOC_HEADER', NULL);
      close  C_CHECK_ALLOC;
   end if;

   if L_distro_type is NULL then
      O_found := FALSE;
   else
      O_found := TRUE;
   end if;

   O_tsf_or_alloc := L_distro_type;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSFER_SQL.TSF_OR_ALLOC',
                                            to_char(SQLCODE));
      RETURN FALSE;
END TSF_OR_ALLOC;
----------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_SHIPMENT
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION RECD_SHIPMENT_EXISTS(O_error_message         IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exist                 IN OUT     BOOLEAN,
                              I_order_no              IN         SHIPMENT.ORDER_NO%TYPE,
                              I_shipment              IN         SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN IS

   L_program_name            VARCHAR2(50) := 'SHIPMENT_ATTRIB_SQL.RECD_SHIPMENT_EXISTS';
   L_exists                  VARCHAR2(1);

   cursor C_RECD_SHIPMENT is
      select 'x'
        from ordhead oh,
             ordloc ol,
             v_shipment sh,
             shipsku sk
       where oh.order_no = nvl(I_order_no,oh.order_no)
         and oh.order_no = sh.order_no
         and sh.shipment = sk.shipment
         and sh.shipment = nvl(I_shipment,sh.shipment)
         and oh.order_no = ol.order_no
         and ol.item     = sk.item
         and ol.loc_type = sh.to_loc_type
         and sh.status_code not in ('I','D','C')
         and ol.last_received is not null
         and sk.qty_received is not null
         and sh.receive_date is not null
         and oh.status in ('A','C')
         and ((ol.location = sh.to_loc)
          or (sh.to_loc = (select wh.physical_wh
                             from wh
                            where ol.location = wh.wh)));


BEGIN
   O_exist := TRUE;
   ---
   if I_shipment is NULL and I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMETERS',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_RECD_SHIPMENT','SHIPMENT','SHIPMENT: '||to_char(I_shipment));
   open C_RECD_SHIPMENT;
   SQL_LIB.SET_MARK('FETCH','C_RECD_SHIPMENT','SHIPMENT','SHIPMENT: '||to_char(I_shipment));
   fetch C_RECD_SHIPMENT into L_exists;
   ---
   if C_RECD_SHIPMENT%NOTFOUND then
      O_exist := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_RECD_SHIPMENT','SHIPMENT','SHIPMENT: '||to_char(I_shipment));
   close C_RECD_SHIPMENT;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END RECD_SHIPMENT_EXISTS;
----------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_SHIPSKU
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION SHIPSKU_FILTER_LIST(O_error_message         IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                             O_diff                  IN OUT     VARCHAR2,
                             I_shipment              IN         SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN IS

   L_shipsku NUMBER(6);
   L_vshipsku NUMBER(6);
   L_program_name            VARCHAR2(50) := 'SHIPMENT_ATTRIB_SQL.SHIP_FILTER_LIST';

   cursor C_SHIPSKU is
      select count(shipment)
        from shipsku
       where shipment=I_shipment;

   cursor C_VSHIPSKU is
      select count(shipment)
        from v_shipsku
       where shipment=I_shipment;


BEGIN
  open C_shipsku;
  fetch C_shipsku into L_shipsku;
  close C_shipsku;

  open C_vshipsku;
  fetch C_vshipsku into L_vshipsku;
  close C_vshipsku;

  if nvl(L_shipsku,0) != nvl(L_vshipsku,0) then
    O_diff := 'Y';
  else
    O_diff := 'N';
  end if;
  return TRUE;
EXCEPTION
  WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END SHIPSKU_FILTER_LIST;
-------------------------------------------------------------------------------------------
-- Call this function with online forms to determine if values match in table bol_shipsku
-- and view v_bol_shipsku.
--------------------------------------------------------------------------------
FUNCTION BOL_SHIPSKU_FILTER_LIST(O_error_message         IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_diff                  IN OUT     VARCHAR2,
                                 I_bol_no                IN         BOL_SHIPMENT.BOL_NO%TYPE)
   RETURN BOOLEAN IS

   L_exists         VARCHAR2(1)  := NULL;
   L_program_name   VARCHAR2(50) := 'SHIPMENT_ATTRIB_SQL.BOL_SHIPSKU_FILTER_LIST';

   cursor C_CHECK_DIFFERENCE is
      select 'x'
        from bol_shipsku b
       where bol_no = I_bol_no
         and rownum = 1
         and not exists (select 'x'
                           from v_bol_shipsku v
                          where v.bol_no = I_bol_no
                            and v.distro_type = b.distro_type
                            and v.distro_no = b.distro_no
                            and v.item = b.item);

BEGIN
   ---
   if I_bol_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('BOL_NUM_INV',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_DIFFERENCE','BOL_SHIPSKU', NULL);
   open C_CHECK_DIFFERENCE;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_DIFFERENCE','BOL_SHIPSKU', NULL);
   fetch C_CHECK_DIFFERENCE into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_DIFFERENCE','BOL_SHIPSKU', NULL);
   close C_CHECK_DIFFERENCE;
   ---
   if L_exists is NULL then
      O_diff := 'N';
   else
      O_diff := 'Y';
   end if;
   ---
   return TRUE;

EXCEPTION
  WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END BOL_SHIPSKU_FILTER_LIST;
-------------------------------------------------------------------------------------------
-- Call this function with online forms to verify if the carton and shipment information
-- are valid and exist in the database.
--------------------------------------------------------------------------------
FUNCTION CARTON_SHIPMENT_EXISTS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT BOOLEAN,
                                I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                                I_to_loc           IN     SHIPMENT.TO_LOC%TYPE,
                                I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                                I_asn              IN     SHIPMENT.ASN%TYPE,
                                I_bol_no           IN     SHIPMENT.BOL_NO%TYPE,
                                I_carton           IN     SHIPSKU.CARTON%TYPE,
                                I_distro_type      IN     SHIPSKU.DISTRO_TYPE%TYPE,
                                I_distro_no        IN     SHIPSKU.DISTRO_NO%TYPE)
   RETURN BOOLEAN IS

   L_ship_exists     VARCHAR2(1)  := NULL;
   L_carton_exists   VARCHAR2(1)  := NULL;
   L_program_name    VARCHAR2(50) := 'SHIPMENT_ATTRIB_SQL.CARTON_SHIPMENT_EXISTS';

   cursor C_SHIPMENT_EXISTS is
      select 'x'
        from shipment st
       where st.shipment = I_shipment
         and st.to_loc = NVL(I_to_loc, st.to_loc)
         and st.to_loc_type = NVL(I_to_loc_type, st.to_loc_type)
         and NVL(st.asn, '-999') = NVL(I_asn, NVL(st.asn, '-999'))
         and NVL(st.bol_no, '-999') = NVL(I_bol_no, NVL(st.bol_no, '-999'))
         and st.status_code not in ('C','R')
         and (I_distro_no is NULL
              or exists(select 'y'
                          from shipsku sk
                         where sk.shipment = st.shipment
                           and ((sk.distro_no = I_distro_no 
                                 and sk.distro_type = I_distro_type 
                                 and I_distro_type in ('T','A'))
                                or (st.order_no = substr(I_distro_no, 1,8) 
                                    and I_distro_type = 'P'))
                           and rownum = 1))
         and rownum = 1;

   cursor C_SHIPSKU_CARTON_NULL_CHECK is
      select 'x'
        from shipsku su
       where su.shipment = I_shipment
         and su.carton is NOT NULL
         and su.status_code != 'R'
         and rownum = 1;

   cursor C_SHIPSKU_CARTON_MATCH_CHECK is
      select 'x'
        from shipsku su
       where su.shipment = I_shipment
         and su.carton = I_carton
         and su.status_code != 'R'
         and rownum = 1;

BEGIN
   ---
   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMETERS',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   open C_SHIPMENT_EXISTS;
   fetch C_SHIPMENT_EXISTS into L_ship_exists;
   close C_SHIPMENT_EXISTS;
   ---
   if L_ship_exists is NOT NULL then
      ---
      if I_carton is NULL then
         ---
         open C_SHIPSKU_CARTON_NULL_CHECK;
         fetch C_SHIPSKU_CARTON_NULL_CHECK into L_carton_exists;
         close C_SHIPSKU_CARTON_NULL_CHECK;
         ---
         if L_carton_exists is NOT NULL then
            O_exists := TRUE;
         else
            O_exists := FALSE;
            O_error_message := SQL_LIB.CREATE_MSG('CARTON_NOT_EXISTS',NULL,NULL,NULL);
         end if;
         ---
      else
         ---
         open C_SHIPSKU_CARTON_MATCH_CHECK;
         fetch C_SHIPSKU_CARTON_MATCH_CHECK into L_carton_exists;
         close C_SHIPSKU_CARTON_MATCH_CHECK;
         ---
         if L_carton_exists is NOT NULL then
            O_exists := TRUE;
         else
            O_exists := FALSE;
            O_error_message := SQL_LIB.CREATE_MSG('SHIP_DTLS_NOT_EXIST',NULL,NULL,NULL);
         end if;
         ---
      end if;
   else
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('SHIP_DTLS_NOT_EXIST',NULL,NULL,NULL);
   end if;
   ---
   return TRUE;

EXCEPTION
  WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END CARTON_SHIPMENT_EXISTS;
-------------------------------------------------------------------------------------------
FUNCTION SHIP_LOC_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_loc_name      IN OUT STORE.STORE_NAME%TYPE,
                         I_loc           IN     SHIPMENT.TO_LOC%TYPE,
                         I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                         I_ctn_ind       IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program_name    VARCHAR2(50) := 'SHIPMENT_ATTRIB_SQL.SHIP_LOC_EXISTS';
   L_dummy           VARCHAR2(1)  := NULL;
   L_full_ind        VARCHAR2(1);
   L_wh_row          V_PHYSICAL_WH%ROWTYPE;

   cursor C_CHECK_LOC_SHIP is
      select 'x'
        from shipment sh,
             shipsku sk
       where sh.shipment = sk.shipment
         and sh.to_loc = I_loc
         and sh.to_loc_type = I_loc_type
         and sh.status_code NOT IN ('C','R')
         and sk.status_code != 'R'
         and rownum = 1;

   cursor C_CHECK_CTN_LOC_SHIP is
      select 'x'
        from shipment sh,
             shipsku sk
       where sh.shipment = sk.shipment
         and sh.to_loc = I_loc
         and sh.to_loc_type = I_loc_type
         and sh.status_code NOT IN ('C','R')
         and sk.status_code != 'R'
         and sk.carton is NOT NULL
         and rownum = 1;

BEGIN
   O_valid := FALSE;
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                             L_program_name,
                                             NULL);
      return FALSE;
   end if;

   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                             L_program_name,
                                             NULL);
      return FALSE;
   end if;

   if I_ctn_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ctn_ind',
                                             L_program_name,
                                             NULL);
      return FALSE;
   end if;

   if I_ctn_ind = 'N' then
      open C_CHECK_LOC_SHIP;
      fetch C_CHECK_LOC_SHIP into L_dummy;
      close C_CHECK_LOC_SHIP;
   elsif I_ctn_ind = 'Y' then
      open C_CHECK_CTN_LOC_SHIP;
      fetch C_CHECK_CTN_LOC_SHIP into L_dummy;
      close C_CHECK_CTN_LOC_SHIP;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT');
      return FALSE;
   end if;

   if L_dummy is NULL then
     O_valid := FALSE;
     O_error_message := SQL_LIB.CREATE_MSG('LOC_NOT_EXIST_SHIP');
   else
      if I_loc_type = 'S' then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(O_error_message,
                                                   O_valid,
                                                   O_loc_name,
                                                   I_loc) = FALSE then
            return FALSE;
         end if;
      elsif I_loc_type = 'W' then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_PHYSICAL_WH(O_error_message,
                                                         O_valid,
                                                         L_full_ind,
                                                         L_wh_row,
                                                         I_loc) = FALSE  then
            return FALSE;
         end if;
         ---
         O_loc_name := L_wh_row.wh_name;
      end if;
   end if;

   return TRUE;

EXCEPTION
  WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END SHIP_LOC_EXISTS;
-------------------------------------------------------------------------------------------
FUNCTION SHIP_ASN_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists        IN OUT BOOLEAN,
                         I_asn           IN     SHIPMENT.ASN%TYPE,
                         I_ctn_ind       IN     VARCHAR2,
                         I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program_name    VARCHAR2(50) := 'SHIPMENT_ATTRIB_SQL.SHIP_ASN_EXISTS';
   L_dummy           VARCHAR2(1)  := NULL;

   cursor C_SHIP_ASN_EXIST_TB is
      select 'x'
        from shipment sh,
             shipsku sk
       where sh.shipment = sk.shipment
         and sh.asn = I_asn
         and sh.status_code NOT IN ('C','R')
         and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
         and sk.status_code != 'R'
         and rownum = 1;

   cursor C_SHIP_ASN_CTN_EXIST_TB is
      select 'x'
        from shipment sh,
             shipsku sk
       where sh.shipment = sk.shipment
         and sh.asn = I_asn
         and sh.status_code NOT IN ('C','R')
         and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
         and sk.status_code != 'R'
         and sk.carton is NOT NULL
         and rownum = 1;

   cursor C_SHIP_ASN_EXIST_V is
      select 'x'
        from v_shipment vsh,
             shipsku sk
       where vsh.shipment = sk.shipment
         and vsh.asn = I_asn
         and vsh.status_code NOT IN ('C','R')
         and vsh.to_loc_type = NVL(I_loc_type, vsh.to_loc_type)
         and sk.status_code != 'R'
         and rownum = 1;

   cursor C_SHIP_ASN_CTN_EXIST_V is
      select 'x'
        from v_shipment vsh,
             shipsku sk
       where vsh.shipment = sk.shipment
         and vsh.asn = I_asn
         and vsh.status_code NOT IN ('C','R')
         and vsh.to_loc_type = NVL(I_loc_type, vsh.to_loc_type)
         and sk.status_code != 'R'
         and sk.carton is NOT NULL
         and rownum = 1;

BEGIN
   O_exists := TRUE;
   if I_ctn_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ctn_ind',
                                             L_program_name,
                                             NULL);
      return FALSE;
   end if;

   if I_asn is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_asn',
                                             L_program_name,
                                             NULL);
      return FALSE;
   end if;

   if I_ctn_ind = 'N' then
      open C_SHIP_ASN_EXIST_V;
      fetch C_SHIP_ASN_EXIST_V into L_dummy;

      if C_SHIP_ASN_EXIST_V%NOTFOUND then
         open C_SHIP_ASN_EXIST_TB;
         fetch C_SHIP_ASN_EXIST_TB into L_dummy;

         if C_SHIP_ASN_EXIST_TB%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('SHIP_NO_ASN_RECV');
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ASN_SHIP');
         end if;

         close C_SHIP_ASN_EXIST_TB;
         O_exists := FALSE;
      end if;
      close C_SHIP_ASN_EXIST_V;
   elsif I_ctn_ind = 'Y' then
      open C_SHIP_ASN_CTN_EXIST_V;
      fetch C_SHIP_ASN_CTN_EXIST_V into L_dummy;

      if C_SHIP_ASN_CTN_EXIST_V%NOTFOUND then
         open C_SHIP_ASN_CTN_EXIST_TB;
         fetch C_SHIP_ASN_CTN_EXIST_TB into L_dummy;

         if C_SHIP_ASN_CTN_EXIST_TB%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('SHIP_NO_ASN_RECV');
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ASN_SHIP');
         end if;

         close C_SHIP_ASN_CTN_EXIST_TB;
         O_exists := FALSE;
      end if;
      close C_SHIP_ASN_CTN_EXIST_V;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
  WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END SHIP_ASN_EXISTS;
-------------------------------------------------------------------------------------------
FUNCTION SHIP_BOL_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists        IN OUT BOOLEAN,
                         I_bol_no        IN     SHIPMENT.BOL_NO%TYPE,
                         I_ctn_ind       IN     VARCHAR2,
                         I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program_name    VARCHAR2(50) := 'SHIPMENT_ATTRIB_SQL.SHIP_BOL_EXISTS';
   L_dummy           VARCHAR2(1)  := NULL;

   cursor C_SHIP_BOL_EXIST_TB is
      select 'x'
        from shipment sh,
             shipsku sk
       where sh.shipment = sk.shipment
         and sh.bol_no = I_bol_no
         and sh.status_code NOT IN ('C','R')
         and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
         and sk.status_code != 'R'
         and rownum = 1;

   cursor C_SHIP_BOL_CTN_EXIST_TB is
      select 'x'
        from shipment sh,
             shipsku sk
       where sh.shipment = sk.shipment
         and sh.bol_no = I_bol_no
         and sh.status_code NOT IN ('C','R')
         and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
         and sk.status_code != 'R'
         and sk.carton is NOT NULL
         and rownum = 1;

   cursor C_SHIP_BOL_EXIST_V is
      select 'x'
        from v_shipment vsh,
             shipsku sk
       where vsh.shipment = sk.shipment
         and vsh.bol_no = I_bol_no
         and vsh.status_code NOT IN ('C','R')
         and vsh.to_loc_type = NVL(I_loc_type, vsh.to_loc_type)
         and sk.status_code != 'R'
         and rownum = 1;

   cursor C_SHIP_BOL_CTN_EXIST_V is
      select 'x'
        from v_shipment vsh,
             shipsku sk
       where vsh.shipment = sk.shipment
         and vsh.bol_no = I_bol_no
         and vsh.status_code NOT IN ('C','R')
         and vsh.to_loc_type = NVL(I_loc_type, vsh.to_loc_type)
         and sk.status_code != 'R'
         and sk.carton is NOT NULL
         and rownum = 1;

BEGIN
   O_exists := TRUE;
   if I_ctn_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ctn_ind',
                                             L_program_name,
                                             NULL);
      return FALSE;
   end if;

   if I_bol_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_bol_no',
                                             L_program_name,
                                             NULL);
      return FALSE;
   end if;

   if I_ctn_ind = 'N' then
      open C_SHIP_BOL_EXIST_V;
      fetch C_SHIP_BOL_EXIST_V into L_dummy;

      if C_SHIP_BOL_EXIST_V%NOTFOUND then
         open C_SHIP_BOL_EXIST_TB;
         fetch C_SHIP_BOL_EXIST_TB into L_dummy;

         if C_SHIP_BOL_EXIST_TB%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('SHIP_NO_BOL_RECV');
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_BOL_SHIP');
         end if;

         close C_SHIP_BOL_EXIST_TB;
         O_exists := FALSE;
      end if;
      close C_SHIP_BOL_EXIST_V;
   elsif I_ctn_ind = 'Y' then
      open C_SHIP_BOL_CTN_EXIST_V;
      fetch C_SHIP_BOL_CTN_EXIST_V into L_dummy;

      if C_SHIP_BOL_CTN_EXIST_V%NOTFOUND then
         open C_SHIP_BOL_CTN_EXIST_TB;
         fetch C_SHIP_BOL_CTN_EXIST_TB into L_dummy;

         if C_SHIP_BOL_CTN_EXIST_TB%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('SHIP_NO_BOL_RECV');
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_BOL_SHIP');
         end if;

         close C_SHIP_BOL_CTN_EXIST_TB;
         O_exists := FALSE;
      end if;
      close C_SHIP_BOL_CTN_EXIST_V;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
  WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END SHIP_BOL_EXISTS;
-------------------------------------------------------------------------------------------
FUNCTION CHECK_COMPLETE_SHIPMENT(O_error_message    IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_partial          IN OUT     BOOLEAN,
                                 I_shipment         IN         SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN IS

   L_partial                 VARCHAR2(1);
   L_program_name            VARCHAR2(64) := 'SHIPMENT_ATTRIB_SQL.CHECK_COMPLETE_SHIPMENT';

   cursor C_PARTIAL_SHIPMENT_RECEIVED is
      select 'Y'
        from shipsku
       where shipment=I_shipment
         and NVL(qty_received,(qty_expected - 1)) < qty_expected
         and rownum = 1;

BEGIN
   O_partial := FALSE;
   
   open C_PARTIAL_SHIPMENT_RECEIVED;
   fetch C_PARTIAL_SHIPMENT_RECEIVED into L_partial;
   if C_PARTIAL_SHIPMENT_RECEIVED%FOUND then
      O_partial := TRUE;
   end if;
   close C_PARTIAL_SHIPMENT_RECEIVED;

   return TRUE;
EXCEPTION
  WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_COMPLETE_SHIPMENT;
-------------------------------------------------------------------------------------------
END SHIPMENT_ATTRIB_SQL;
/