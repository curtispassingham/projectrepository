CREATE OR REPLACE PACKAGE BODY RMSMFM_TRANSFERS AS

TYPE rowid_TBL is table of ROWID INDEX BY BINARY_INTEGER;

LP_num_threads                 rib_settings.num_threads%TYPE:=NULL;
LP_max_count                   rib_settings.max_details_to_publish%TYPE:=NULL;

LP_nbf_days                    NUMBER(38):=NULL;
LP_default_order_type          SYSTEM_OPTIONS.DEFAULT_ORDER_TYPE%TYPE:=NULL;

-- depending upon the message type, there will come a point in the publication process
-- when error retry will no longer be possible.  this variable will track whether or
-- not that point has been reached.
LP_error_status        VARCHAR2(1):= NULL;

PROGRAM_ERROR  EXCEPTION;
--------------------------------------------------------
       /*Private Program Declarations*/
--------------------------------------------------------
---
PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT      VARCHAR2,
                        O_error_msg       IN  OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message         IN  OUT      RIB_OBJECT,
                        O_message_type    IN  OUT      VARCHAR2,
                        O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                        O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                        I_tsf_no          IN           tsf_mfqueue.tsf_no%TYPE,
                        I_seq_no          IN           tsf_mfqueue.seq_no%TYPE,
                        I_item            IN           tsf_mfqueue.item%TYPE);
---
FUNCTION PROCESS_QUEUE_RECORD(O_error_msg       IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                              O_break_loop      IN OUT        BOOLEAN,
                              O_message         IN OUT NOCOPY RIB_OBJECT,
                              O_routing_info    IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id      IN OUT NOCOPY RIB_BUSOBJID_TBL,
                              O_message_type    IN OUT        VARCHAR2,
                              I_tsf_no          IN            tsf_mfqueue.tsf_no%TYPE,
                              I_hdr_published   IN            transfers_pub_info.published%TYPE,
                              I_item            IN            tsf_mfqueue.item%TYPE,
                              I_pub_status      IN            tsf_mfqueue.pub_status%TYPE,
                              I_seq_no          IN            tsf_mfqueue.seq_no%TYPE,
                              I_rowid           IN            ROWID,
                              O_keep_queue      IN OUT        BOOLEAN)
RETURN BOOLEAN;
---
FUNCTION MAKE_CREATE(O_error_msg       IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                     O_message         IN OUT NOCOPY RIB_OBJECT,
                     O_routing_info    IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                     I_tsf_no          IN            tsfhead.tsf_no%TYPE,
                     I_seq_no          IN            tsf_mfqueue.seq_no%TYPE,
                     I_item            IN            item_loc.item%TYPE,
                     I_max_details     IN            rib_settings.max_details_to_publish%TYPE,
                     I_rowid           IN            ROWID,
                     O_keep_queue      IN OUT        BOOLEAN)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg        IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_rib_tsfdesc_rec  IN OUT NOCOPY "RIB_TsfDesc_REC",
                                     I_message_type     IN            tsf_mfqueue.message_type%TYPE,
                                     I_tsf_no           IN            tsfhead.tsf_no%TYPE,
                                     I_item             IN            item_loc.item%TYPE,
                                     I_freight_code     IN            tsfhead.freight_code%TYPE,
                                     I_max_details      IN            rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_DELETE_OBJECTS(O_error_msg        IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_rib_tsfref_rec   IN OUT NOCOPY "RIB_TsfRef_REC",
                                     I_tsf_no           IN            tsfhead.tsf_no%TYPE,
                                     I_max_details      IN            rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_HEADER_OBJECT(O_error_msg        IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                             O_message          IN OUT NOCOPY RIB_OBJECT,
                             O_rib_tsfref_rec   IN OUT NOCOPY "RIB_TsfRef_REC",
                             O_routing_info     IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             O_freight_code     IN OUT        tsfhead.freight_code%TYPE,
                             O_to_loc_type      IN OUT        item_loc.loc_type%TYPE,
                             O_from_loc_type    IN OUT        item_loc.loc_type%TYPE,
                             I_tsf_no           IN            tsfhead.tsf_no%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg           IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                              O_message             IN OUT NOCOPY   "RIB_TsfDtl_TBL",
                              O_tsf_mfqueue_rowid   IN OUT NOCOPY   rowid_TBL,
                              O_tsf_mfqueue_size    IN OUT          BINARY_INTEGER,
                              O_tsfdetail_rowid     IN OUT NOCOPY   rowid_TBL,
                              O_tsfdetail_size      IN OUT          BINARY_INTEGER,
                              O_delete_rowid_ind    IN OUT          VARCHAR2,
                              I_message_type        IN              tsf_mfqueue.message_type%TYPE,
                              I_tsf_no              IN              tsfhead.tsf_no%TYPE,
                              I_from_loc            IN              item_loc.loc%TYPE,
                              I_to_loc              IN              item_loc.loc%TYPE,
                              I_to_loc_type         IN              item_loc.loc_type%TYPE,
                              I_max_details         IN              rib_settings.max_details_to_publish%TYPE,
                              I_freight_code        IN              VARCHAR2)
RETURN BOOLEAN;
---
FUNCTION BUILD_SINGLE_DETAIL(O_error_msg            IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                             O_message              IN OUT NOCOPY "RIB_TsfDtl_TBL",
                             IO_rib_tsfdtl_rec      IN OUT NOCOPY "RIB_TsfDtl_REC",
                             I_tsf_no               IN            tsfhead.tsf_no%TYPE,
                             I_item                 IN            item_master.item%TYPE,
                             I_pack_ind             IN            item_master.pack_ind%TYPE,
                             I_sellable_ind         IN            item_master.sellable_ind%TYPE,
                             I_store_ord_mult       IN            item_loc.store_ord_mult%TYPE,
                             I_tsf_po_link_no       IN            tsfdetail.tsf_po_link_no%TYPE,
                             I_ticket_type_id       IN            item_ticket.ticket_type_id%TYPE,
                             I_tsf_qty              IN            tsfdetail.tsf_qty%TYPE,
                             I_freight_code         IN            tsfhead.freight_code%TYPE,
                             I_to_loc               IN            item_loc.loc%TYPE,
                             I_to_loc_type          IN            item_loc.loc_type%TYPE,
                             I_inv_status           IN            inv_status_types.inv_status%TYPE,
                             I_transaction_uom      IN            uom_class.uom%TYPE,
                             I_message_type         IN            tsf_mfqueue.message_type%TYPE)
RETURN BOOLEAN;
---
FUNCTION GET_RETAIL(O_error_msg    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item         IN     item_loc.item%TYPE,
                    I_loc          IN     item_loc.loc%TYPE,
                    I_loc_type     IN     item_loc.loc_type%TYPE,
                    O_price        IN OUT item_loc.unit_retail%TYPE,
                    O_selling_uom  IN OUT item_loc.selling_uom%TYPE)
RETURN BOOLEAN;
---
FUNCTION GET_GLOBALS(O_error_msg           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_days                IN OUT NUMBER,
                     O_default_order_type  IN OUT system_options.default_order_type%TYPE)
RETURN BOOLEAN;
---
FUNCTION DELETE_QUEUE_REC(O_error_msg   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_seq_no      IN     tsf_mfqueue.seq_no%TYPE)
RETURN BOOLEAN;
---
FUNCTION LOCK_THE_BLOCK(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_queue_locked  IN OUT BOOLEAN,
                        I_tsf_no        IN     tsf_mfqueue.tsf_no%TYPE)
RETURN BOOLEAN;
---
FUNCTION LOCK_DETAILS(O_error_msg IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tsf_no    IN     tsfhead.tsf_no%TYPE)
RETURN BOOLEAN;
---
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
           /*** Public Program Bodies***/
--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_msg         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                I_message_type      IN     VARCHAR2,
                I_tsf_no            IN     tsfhead.tsf_no%TYPE,
                I_tsf_type          IN     tsfhead.tsf_type%TYPE,
                I_tsf_head_status   IN     tsfhead.status%TYPE,
                I_item              IN     tsfdetail.item%TYPE,
                I_publish_ind       IN     tsfdetail.publish_ind%TYPE,
                I_to_loc_type       IN     tsfhead.to_loc_type%TYPE DEFAULT NULL,
                I_tsf_parent_no     IN     tsfhead.tsf_parent_no%TYPE DEFAULT NULL,
                I_from_loc_type     IN     tsfhead.from_loc_type%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_tsf_type           tsfhead.tsf_type%TYPE:=NULL;
   L_status             tsfhead.status%TYPE:= NULL;
   L_max_details        rib_settings.max_details_to_publish%TYPE:=NULL;
   L_thread_no          rib_settings.num_threads%TYPE:=NULL;
   L_initial_approval   VARCHAR2(1):='N';
   L_num_threads        rib_settings.num_threads%TYPE:=NULL;
   L_min_time_lag       rib_settings.minutes_time_lag%TYPE:=NULL;
   L_publish_ind        TRANSFERS_PUB_INFO.PUBLISHED%TYPE;
   L_from_loc_type      ITEM_LOC.LOC_TYPE%TYPE;
   L_to_loc_type        ITEM_LOC.LOC_TYPE%TYPE;
   L_tsf_parent_no      TSFHEAD.TSF_PARENT_NO%TYPE;

   L_status_code        VARCHAR2(1):=NULL;

   cursor C_TSF_TYPE is
      select tsf_type,
             status,
             from_loc_type,
             to_loc_type,
             tsf_parent_no
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_HEAD is
      select tho.tsf_type,
             tho.thread_no,
             tho.initial_approval_ind,
             tho.published
        from transfers_pub_info tho
       where tho.tsf_no = I_tsf_no;

BEGIN

   if I_tsf_type is NULL then
      open C_TSF_TYPE;
      fetch C_TSF_TYPE into L_tsf_type,L_status,L_from_loc_type,L_to_loc_type,L_tsf_parent_no;
      close C_TSF_TYPE;
   else
      L_tsf_type := I_tsf_type;
      L_status   := I_tsf_head_status;
      L_tsf_parent_no := I_tsf_parent_no;
      L_from_loc_type := I_from_loc_type;
      L_to_loc_type := I_to_loc_type;
   end if;

   -- do not publish book transfers or non sellable transfers (will never be sent
   -- from the header trigger)
   if L_tsf_type in ('BT','NB') then
      return TRUE;
   end if;

   -- do not push header adds, detail adds, detail updates if tsf
   -- is externally generated or
   -- if transfer is externally generated it is first leg
   if L_tsf_type = 'EG' and I_message_type in (DTL_ADD,DTL_UPD,DTL_DEL,HDR_ADD,HDR_UPD,HDR_DEL) and
      I_tsf_parent_no is NULL then
      return TRUE;
   end if;

   if I_message_type != HDR_ADD then
      open C_HEAD;
      fetch C_HEAD into L_tsf_type,
                        L_thread_no,
                        L_initial_approval,
                        L_publish_ind;
      if C_HEAD%NOTFOUND then
         close C_HEAD;

         -- If it is a EG transfer and 2nd leg is from ext finisher and to location is a wh  then tsf_pub_info will not have a record
         -- and no need to publishing the details.
         -- If it is a not a EG transfer and 2nd leg is from ext finisher and to location is a wh  then tsf_pub_info will not have a record
         if ((L_tsf_type != 'EG' and (L_tsf_parent_no is NULL or (L_tsf_parent_no is NOT NULL and (L_from_loc_type != 'E' or L_to_loc_type = 'S')))) or
                 (L_tsf_type = 'EG' and L_tsf_parent_no is NOT NULL and (L_from_loc_type != 'E' or L_to_loc_type = 'S'))) then
            if L_status = 'D' then
               -- the DTL_DEL message being processed references a transfer already in
               -- 'D'eleted status. Its transfers_pub_info record has already been deleted.
               -- No further processing is necessary.
               return TRUE;
            else
               O_error_msg := SQL_LIB.CREATE_MSG('PUB_INFO_NOT_FOUND',
                                                 'transfers_pub_info',
                                                 I_tsf_no,
                                                 NULL);
               return FALSE;
            end if;
         else
            return TRUE;
         end if;
      end if;

      close C_HEAD;
   end if;

   if I_message_type in(DTL_ADD,DTL_UPD,DTL_DEL,HDR_DEL,HDR_UPD,HDR_UNAPRV) then
      if I_message_type = HDR_UPD and
         I_tsf_head_status = 'A' and
         L_initial_approval = 'N' then
            update transfers_pub_info
               set initial_approval_ind = 'Y'
             where tsf_no = I_tsf_no;
         L_initial_approval := 'Y';
      end if;
      if L_publish_ind = 'N' then
         if I_message_type = HDR_DEL then
            delete from transfers_pub_info where tsf_no = I_tsf_no;
         end if;
       ---
         if L_initial_approval = 'N' then
            return TRUE;
         end if;
      end if;
      if I_message_type = HDR_UNAPRV then
         update transfers_pub_info
            set initial_approval_ind = 'N'
          where tsf_no = I_tsf_no;
      end if;
   end if;

   -- If the message is a detail message, all previous records on the queue
   -- relating to the detail record can be deleted.
   if I_message_type = DTL_DEL then

      delete from tsf_mfqueue
       where tsf_no = I_tsf_no
         and item = I_item;

   elsif I_message_type = DTL_UPD then

      delete from tsf_mfqueue
       where tsf_no = I_tsf_no
         and item = I_item
         and message_type = DTL_UPD;

   -- If the message is a header delete, all previous records on the queue
   -- relating to the transfer can be deleted.
   elsif I_message_type = HDR_DEL or I_message_type = HDR_UNAPRV then

      delete from tsf_mfqueue
       where tsf_no = I_tsf_no;

   end if;

   if I_message_type = HDR_ADD then

      API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                   O_error_msg,
                                   L_max_details,
                                   L_num_threads,
                                   L_min_time_lag,
                                   RMSMFM_TRANSFERS.FAMILY);
      if L_status_code in (API_CODES.UNHANDLED_ERROR) then
         return FALSE;
      end if;

      insert into transfers_pub_info(tsf_no,
                                  tsf_type,
                                  initial_approval_ind,
                                  thread_no,
                                  physical_from_loc,
                                  from_loc,
                                  from_loc_type,
                                  physical_to_loc,
                                  to_loc,
                                  to_loc_type,
                                  freight_code,
                                  published)
                         values (I_tsf_no,
                                  I_tsf_type,
                                  DECODE(I_tsf_head_status, 'A', 'Y', 'N'),
                                  MOD(I_tsf_no, L_num_threads)+1,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  'N' );

   elsif (I_publish_ind = 'Y' or I_message_type != DTL_DEL) then

      insert into tsf_mfqueue (SEQ_NO,
                                TSF_NO,
                                ITEM,
                                MESSAGE_TYPE,
                                THREAD_NO,
                                FAMILY,
                                CUSTOM_MESSAGE_TYPE,
                                PUB_STATUS,
                                TRANSACTION_NUMBER,
                                TRANSACTION_TIME_STAMP)
                        values(tsf_mfsequence.nextval,
                                I_tsf_no,
                                I_item,
                                I_message_type,
                                L_thread_no,
                                RMSMFM_TRANSFERS.FAMILY,
                                NULL,
                                'U',
                                I_tsf_no,
                                SYSDATE);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.ADDTOQ',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1)
IS

   L_break_loop         BOOLEAN := FALSE;
   L_message_type       tsf_mfqueue.message_type%TYPE:=NULL;

   L_tsf_no             tsfhead.tsf_no%TYPE:=NULL;
   L_item               item_master.item%TYPE:=NULL;
   L_seq_no             tsf_mfqueue.seq_no%TYPE:=NULL;
   L_seq_limit          tsf_mfqueue.seq_no%TYPE:=0;

   L_pub_status         tsf_mfqueue.pub_status%TYPE:=NULL;
   L_rowid              ROWID  :=NULL;

   L_hdr_published      transfers_pub_info.published%TYPE:=NULL;

   L_hosp               VARCHAR2(1) := 'N';
   L_keep_queue         BOOLEAN := FALSE;
   L_queue_locked       BOOLEAN := FALSE;

   cursor C_QUEUE is
      select q.tsf_no,
             q.item,
             q.message_type,
             q.pub_status,
             q.seq_no,
             q.rowid
        from tsf_mfqueue q
       where q.seq_no = (select min(q2.seq_no)
                           from tsf_mfqueue q2
                          where q2.thread_no  = I_thread_val
                            and q2.pub_status = 'U'
                            and q2.seq_no     > L_seq_limit)
         and q.thread_no = I_thread_val;

   cursor C_HOSP is
      select 'Y'
        from tsf_mfqueue
       where tsf_no = L_tsf_no
         and pub_status = API_CODES.HOSPITAL;

   cursor C_THO is
      select tho.published
        from transfers_pub_info tho
       where tho.tsf_no = L_tsf_no;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   LOOP

      O_message := null;

      open C_QUEUE;
      fetch C_QUEUE into L_tsf_no,
                         L_item,
                         L_message_type,
                         L_pub_status,
                         L_seq_no,
                         L_rowid;
      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
      close C_QUEUE;

      if LOCK_THE_BLOCK(O_error_msg,
                        L_queue_locked,
                        L_tsf_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked = FALSE then

         open  C_HOSP;
         fetch C_HOSP into L_hosp;
         close C_HOSP;

         if L_hosp = 'Y' then
            O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                              NULL, NULL, NULL);
            raise PROGRAM_ERROR;
         end if;

         open  C_THO;
         fetch C_THO into L_hdr_published;
         close C_THO;

         if PROCESS_QUEUE_RECORD(O_error_msg,
                                 L_break_loop,
                                 O_message,
                                 O_routing_info,
                                 O_bus_obj_id,
                                 L_message_type,
                                 L_tsf_no,
                                 L_hdr_published,
                                 L_item,
                                 L_pub_status,
                                 L_seq_no,
                                 L_rowid,
                                 L_keep_queue) = FALSE then
            raise PROGRAM_ERROR;
         end if;

         if L_break_loop = TRUE then
            O_message_type   := L_message_type;
            EXIT;
         end if;

      else
         L_seq_limit := L_seq_no;
      end if;

   END LOOP;

   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_tsf_no);
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_tsf_no,
                    L_seq_no,
                    L_item);
END GETNXT;
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT)
IS

   L_seq_no           tsf_mfqueue.seq_no%TYPE:=NULL;

   L_break_loop       BOOLEAN := FALSE;

   L_tsf_no           tsf_mfqueue.tsf_no%TYPE:=NULL;
   L_hdr_published    transfers_pub_info.published%TYPE:=NULL;
   L_item             tsf_mfqueue.item%TYPE:=NULL;
   L_pub_status       tsf_mfqueue.pub_status%TYPE:=NULL;
   L_rowid            ROWID:=NULL;

   L_keep_queue       BOOLEAN := FALSE;
   L_queue_locked     BOOLEAN := FALSE;

   cursor C_RETRY_QUEUE is
      select q.tsf_no,
             tho.published hdr_published,
             q.item,
             q.message_type,
             q.pub_status,
             q.rowid
        from tsf_mfqueue q,
             transfers_pub_info tho
       where q.seq_no = L_seq_no
         and q.tsf_no = tho.tsf_no;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;


   --get info from routing info
   ---assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;

   O_message := null;

   --get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_tsf_no,
                            L_hdr_published,
                            L_item,
                            O_message_type,
                            L_pub_status,
                            L_rowid;
   close C_RETRY_QUEUE;

   if L_tsf_no IS NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   if LOCK_THE_BLOCK(O_error_msg,
                     L_queue_locked,
                     L_tsf_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then

      if PROCESS_QUEUE_RECORD(O_error_msg,
                              L_break_loop,
                              O_message,
                              O_routing_info,
                              O_bus_obj_id,
                              O_message_type,
                              L_tsf_no,
                              L_hdr_published,
                              L_item,
                              L_pub_status,
                              L_seq_no,
                              L_rowid,
                              L_keep_queue) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if O_message IS NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         if L_keep_queue then
            O_status_code := API_CODES.INCOMPLETE_MSG;
         else
            O_status_code := API_CODES.NEW_MSG;
         end if;
         O_bus_obj_id := RIB_BUSOBJID_TBL(L_tsf_no);
      end if;
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_tsf_no,
                    L_seq_no,
                    L_item);
END PUB_RETRY;

--------------------------------------------------------------------------------
           /*** Private Program Bodies***/
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT      VARCHAR2,
                        O_error_msg       IN  OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message         IN  OUT      RIB_OBJECT,
                        O_message_type    IN  OUT      VARCHAR2,
                        O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                        O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                        I_tsf_no          IN           tsf_mfqueue.tsf_no%TYPE,
                        I_seq_no          IN           tsf_mfqueue.seq_no%TYPE,
                        I_item            IN           tsf_mfqueue.item%TYPE)
IS

   L_rib_tsfref_rec              "RIB_TsfRef_REC":=NULL;
   L_rib_tsfdtlref_rec           "RIB_TsfDtlRef_REC":=NULL;
   L_rib_tsfdtlref_tbl           "RIB_TsfDtlRef_TBL":=NULL;

   L_error_type                  VARCHAR2(5) := NULL;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then

      O_bus_obj_id    := RIB_BUSOBJID_TBL(I_tsf_no);
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no', I_seq_no,null,null,null,null));

      L_rib_tsfdtlref_rec := "RIB_TsfDtlRef_REC"(0,I_item);
      L_rib_tsfdtlref_tbl := "RIB_TsfDtlRef_TBL"(L_rib_tsfdtlref_rec);
      L_rib_tsfref_rec := "RIB_TsfRef_REC"(0,                     --RIB_OID               NUMBER
                                           I_tsf_no,              --TSF_NO                NUMBER(12)
                                           'T',                   --DOC_TYPE              VARCHAR2(1)
                                           NULL,                  --PHYSICAL_FROM_LOC     NUMBER(10)
                                           NULL,                  --FROM_LOC              VARCHAR2(10)
                                           NULL,                  --FROM_LOC_TYPE         VARCHAR2(1)
                                           NULL,                  --FROM_STORE_TYPE       VARCHAR2(1)
                                           NULL,                  --FROM_STOCKHOLDING_IND VARCHAR2(1)
                                           NULL,                  --PHYSICAL_TO_LOC       NUMBER(10)
                                           NULL,                  --TO_LOC                VARCHAR2(10)
                                           NULL,                  --TO_LOC_TYPE           VARCHAR2(1)
                                           NULL,                  --TO_STORE_TYPE         VARCHAR2(1)
                                           NULL,                  --TO_STOCKHOLDING_IND   VARCHAR2(1)
                                           L_rib_tsfdtlref_tbl,   --TSFDTLREF_TBL         "RIB_TsfDtlRef_TBL"
                                           NULL);                 --TSF_PARENT_NO         NUMBER(12)

      O_message := L_rib_tsfref_rec;

      update tsf_mfqueue
         set pub_status = LP_error_status
       where seq_no    = I_seq_no;

   end if;

   /* Pass out parsed error message */
   SQL_LIB.API_MSG(L_error_type,
                   O_error_msg);

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_TRANSFERS_SQL');
END HANDLE_ERRORS;
--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD(O_error_msg       IN  OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                              O_break_loop      IN  OUT        BOOLEAN,
                              O_message         IN  OUT NOCOPY RIB_OBJECT,
                              O_routing_info    IN  OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id      IN  OUT NOCOPY RIB_BUSOBJID_TBL,
                              O_message_type    IN  OUT        VARCHAR2,
                              I_tsf_no          IN             tsf_mfqueue.tsf_no%TYPE,
                              I_hdr_published   IN             transfers_pub_info.published%TYPE,
                              I_item            IN             tsf_mfqueue.item%TYPE,
                              I_pub_status      IN             tsf_mfqueue.pub_status%TYPE,
                              I_seq_no          IN             tsf_mfqueue.seq_no%TYPE,
                              I_rowid           IN             ROWID,
                              O_keep_queue      IN OUT         BOOLEAN)
RETURN BOOLEAN IS

   cursor C_HDR_INFO is
      select tho.to_loc,
             tho.from_loc,
             tho.physical_to_loc,
             tho.physical_from_loc,
             tho.to_loc_type,
             s1.store_type, -- to loc
             s1.stockholding_ind,
             tho.from_loc_type,
             s2.store_type, -- from loc
             s2.stockholding_ind,
             tho.freight_code
        from transfers_pub_info tho,
             store s1, -- to loc
             store s2  -- from loc
       where tho.tsf_no = I_tsf_no
         and tho.to_loc = s1.store(+)
         and tho.from_loc = s2.store(+);
		 
   cursor C_CHECK_TSF is 
       select 'Y'  
       from tsfhead 
       where tsf_no = I_tsf_no; 


   L_max_details         rib_settings.max_details_to_publish%TYPE:=NULL;
   L_num_threads         rib_settings.num_threads%TYPE:=NULL;
   L_min_time_lag        rib_settings.minutes_time_lag%TYPE:=NULL;

   L_rib_tsfdesc_rec     "RIB_TsfDesc_REC" := NULL;
   L_rib_tsfref_rec      "RIB_TsfRef_REC" := NULL;
   L_rib_routing_rec     RIB_ROUTINGINFO_REC := NULL;

   L_freight_code        tsfhead.freight_code%TYPE:=NULL;
   L_to_loc_type         item_loc.loc_type%TYPE:=NULL;
   L_to_loc_store_type   store.store_type%TYPE:=NULL;
   L_to_loc_sh_ind       store.stockholding_ind%TYPE:=NULL;
   L_from_loc_type       item_loc.loc_type%TYPE:=NULL;
   L_from_loc_store_type store.store_type%TYPE:=NULL;
   L_from_loc_sh_ind     store.stockholding_ind%TYPE:=NULL;

   L_to_loc              item_loc.loc%TYPE:=NULL;
   L_from_loc            item_loc.loc%TYPE:=NULL;
   L_physical_to_loc     item_loc.loc%TYPE:=NULL;
   L_physical_from_loc   item_loc.loc%TYPE:=NULL;

   L_status_code        VARCHAR2(1) := NULL;
   L_exists             VARCHAR2(1) := 'N';
   
BEGIN

   O_break_loop := TRUE;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_TRANSFERS.FAMILY);
   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   if (O_message_type = HDR_DEL or O_message_type = HDR_UNAPRV) and NVL(I_hdr_published,'N') = 'N' then

      O_break_loop := FALSE;
      
      if O_message_type = HDR_DEL then

         delete from transfers_pub_info
          where tsf_no     = I_tsf_no;
          
      end if;

      if DELETE_QUEUE_REC(O_error_msg,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;

   elsif O_message_type = HDR_DEL then
      open C_HDR_INFO;
      fetch C_HDR_INFO into L_to_loc,
                            L_from_loc,
                            L_physical_to_loc,
                            L_physical_from_loc,
                            L_to_loc_type,
                            L_to_loc_store_type,
                            L_to_loc_sh_ind,
                            L_from_loc_type,
                            L_from_loc_store_type,
                            L_from_loc_sh_ind,
                            L_freight_code;
      close C_HDR_INFO;

      O_message := "RIB_TsfRef_REC"(
              0,                     --RIB_OID               NUMBER
              I_tsf_no,              --TSF_NO                NUMBER(12)
              'T',                   --DOC_TYPE              VARCHAR2(1)
              L_physical_from_loc,   --PHYSICAL_FROM_LOC     NUMBER(10)
              L_from_loc,            --FROM_LOC              VARCHAR2(10)
              L_from_loc_type,       --FROM_LOC_TYPE         VARCHAR2(1)
              L_from_loc_store_type, --FROM_STORE_TYPE       VARCHAR2(1)
              L_from_loc_sh_ind,     --FROM_STOCKHOLDING_IND VARCHAR2(1)
              L_physical_to_loc,     --PHYSICAL_TO_LOC       NUMBER(10)
              L_to_loc,              --TO_LOC                VARCHAR2(10)
              L_to_loc_type,         --TO_LOC_TYPE           VARCHAR2(1)
              L_to_loc_store_type,   --TO_STORE_TYPE         VARCHAR2(1)
              L_to_loc_sh_ind,       --TO_STOCKHOLDING_IND   VARCHAR2(1)
              NULL,                  --TSFDTLREF_TBL         "RIB_TsfDtlRef_TBL"
              NULL);                 --TSF_PARENT_NO         NUMBER(12)

      -- create routing info --
      O_routing_info := RIB_ROUTINGINFO_TBL();
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('from_phys_loc', L_physical_from_loc, 'from_phys_loc_type', L_from_loc_type, null, null);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', L_physical_to_loc, 'to_phys_loc_type', L_to_loc_type, null,null);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

      LP_error_status := API_CODES.UNHANDLED_ERROR;
	
	 open C_CHECK_TSF; 
     fetch C_CHECK_TSF into L_exists; 
     close C_CHECK_TSF; 
        
     if L_exists = 'N' then 
         delete from transfers_pub_info 
         where tsf_no     = I_tsf_no; 
    end if; 

      if DELETE_QUEUE_REC(O_error_msg,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;

   elsif O_message_type = HDR_UNAPRV then
      --the Rib should treat it just like a hdr_del. However, internal table processes are slightly different
      O_message_type := HDR_DEL;

      open C_HDR_INFO;
      fetch C_HDR_INFO into L_to_loc,
                            L_from_loc,
                            L_physical_to_loc,
                            L_physical_from_loc,
                            L_to_loc_type,
                            L_to_loc_store_type,
                            L_to_loc_sh_ind,
                            L_from_loc_type,
                            L_from_loc_store_type,
                            L_from_loc_sh_ind,
                            L_freight_code;
      close C_HDR_INFO;

      O_message := "RIB_TsfRef_REC"(
              0,                     --RIB_OID               NUMBER
              I_tsf_no,              --TSF_NO                NUMBER(12)
              'T',                   --DOC_TYPE              VARCHAR2(1)
              L_physical_from_loc,   --PHYSICAL_FROM_LOC     NUMBER(10)
              L_from_loc,            --FROM_LOC              VARCHAR2(10)
              L_from_loc_type,       --FROM_LOC_TYPE         VARCHAR2(1)
              L_from_loc_store_type, --FROM_STORE_TYPE       VARCHAR2(1)
              L_from_loc_sh_ind,     --FROM_STOCKHOLDING_IND VARCHAR2(1)
              L_physical_to_loc,     --PHYSICAL_TO_LOC       NUMBER(10)
              L_to_loc,              --TO_LOC                VARCHAR2(10)
              L_to_loc_type,         --TO_LOC_TYPE           VARCHAR2(1)
              L_to_loc_store_type,   --TO_STORE_TYPE         VARCHAR2(1)
              L_to_loc_sh_ind,       --TO_STOCKHOLDING_IND   VARCHAR2(1)
              NULL,                  --TSFDTLREF_TBL         "RIB_TsfDtlRef_TBL"
              NULL);                 --TSF_PARENT_NO         NUMBER(12)
      -- create routing info --
      O_routing_info := RIB_ROUTINGINFO_TBL();
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('from_phys_loc', L_physical_from_loc, 'from_phys_loc_type', L_from_loc_type, null, null);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', L_physical_to_loc, 'to_phys_loc_type', L_to_loc_type, null,null);
      O_routing_info.EXTEND;
      O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      if LOCK_DETAILS(O_error_msg,
                      I_tsf_no) = FALSE then
         return FALSE;
      end if;

      --reset the published ind so the tsf is treated like a new tsf when re-approved
      update transfers_pub_info
         set published = 'N'
       where tsf_no = I_tsf_no;

      update tsfdetail
         set publish_ind = 'N'
       where tsf_no = I_tsf_no;

      if DELETE_QUEUE_REC(O_error_msg,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;

   elsif I_hdr_published = 'N' or I_hdr_published = 'I' then

      if I_hdr_published = 'N' then
         O_message_type := HDR_ADD;
      else
         O_message_type := DTL_ADD;
      end if;

      -- publish the entire tsf             --TsfDesc (all details)
      if MAKE_CREATE(O_error_msg,
                     O_message,
                     O_routing_info,
                     I_tsf_no,
                     I_seq_no,
                     I_item,
                     L_max_details,
                     I_rowid,
                     O_keep_queue) = FALSE then
         return FALSE;
      end if;

      -- write the current record (anything but HDR_ADD)
   elsif O_message_type = HDR_UPD then          --TsfDesc (no details)

      if BUILD_HEADER_OBJECT(O_error_msg,
                              L_rib_tsfdesc_rec,
                              L_rib_tsfref_rec,
                              O_routing_info,
                              L_freight_code,
                              L_to_loc_type,
                              L_from_loc_type,
                              I_tsf_no) = FALSE then
         return FALSE;
      end if;

      L_rib_tsfdesc_rec.tsfdtl_tbl := null;
      L_rib_tsfref_rec.tsfdtlref_tbl := null;

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      update transfers_pub_info
         set physical_from_loc = L_rib_tsfref_rec.physical_from_loc,
             from_loc          = L_rib_tsfref_rec.from_loc,
             from_loc_type     = L_from_loc_type,
             physical_to_loc   = L_rib_tsfref_rec.physical_to_loc,
             to_loc            = L_rib_tsfref_rec.to_loc,
             to_loc_type       = L_to_loc_type,
             freight_code      = L_freight_code,
             published         = 'Y'
       where tsf_no = I_tsf_no;

      if DELETE_QUEUE_REC(O_error_msg,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;

      O_message := L_rib_tsfdesc_rec;

   elsif O_message_type in (DTL_ADD, DTL_UPD) then       --TsfDesc (one detail)

      if BUILD_HEADER_OBJECT(O_error_msg,
                             L_rib_tsfdesc_rec,
                             L_rib_tsfref_rec,
                             O_routing_info,
                             L_freight_code,
                             L_to_loc_type,
                             L_from_loc_type,
                             I_tsf_no) = FALSE then
         return FALSE;
      end if;

      if BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg,
                                     L_rib_tsfdesc_rec,
                                     O_message_type,
                                     I_tsf_no,
                                     I_item,
                                     L_freight_code,
                                     L_max_details) = FALSE then
         return FALSE;
      end if;

      O_message := L_rib_tsfdesc_rec;

   elsif O_message_type = DTL_DEL then       --TsfDtlRef

      if BUILD_HEADER_OBJECT(O_error_msg,
                             L_rib_tsfdesc_rec,
                             L_rib_tsfref_rec,
                             O_routing_info,
                             L_freight_code,
                             L_to_loc_type,
                             L_from_loc_type,
                             I_tsf_no) = FALSE then
         return FALSE;
      end if;

      if BUILD_DETAIL_DELETE_OBJECTS(O_error_msg,
                                     L_rib_tsfref_rec,
                                     I_tsf_no,
                                     L_max_details) = FALSE then
         return FALSE;
      end if;

      O_message := L_rib_tsfref_rec;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.PROCESS_QUEUE_RECORD',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_QUEUE_RECORD;
--------------------------------------------------------------------------------
FUNCTION MAKE_CREATE(O_error_msg       IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                     O_message         IN OUT NOCOPY RIB_OBJECT,
                     O_routing_info    IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                     I_tsf_no          IN            tsfhead.tsf_no%TYPE,
                     I_seq_no          IN            tsf_mfqueue.seq_no%TYPE,
                     I_item            IN            item_loc.item%TYPE,
                     I_max_details     IN            rib_settings.max_details_to_publish%TYPE,
                     I_rowid           IN            ROWID,
                     O_keep_queue      IN OUT        BOOLEAN)
RETURN BOOLEAN IS

   L_freight_code          tsfhead.freight_code%TYPE:=NULL;
   L_to_loc_type           item_loc.loc_type%TYPE:=NULL;
   L_from_loc_type         item_loc.loc_type%TYPE:=NULL;

   L_rib_tsfdesc_rec       "RIB_TsfDesc_REC" := NULL;
   L_rib_tsfdtl_tbl        "RIB_TsfDtl_TBL" := NULL;

   L_tsf_mfqueue_rowid     rowid_TBL;
   L_tsf_mfqueue_size      BINARY_INTEGER := 0;

   L_tsfdetail_rowid       rowid_TBL;
   L_tsfdetail_size        BINARY_INTEGER := 0;

   L_rib_routing_rec       RIB_ROUTINGINFO_REC := NULL;
   L_delete_rowid_ind      VARCHAR2(1) := 'Y';

   L_rib_tsfref_rec        "RIB_TsfRef_REC";

BEGIN

   if BUILD_HEADER_OBJECT(O_error_msg,
                          L_rib_tsfdesc_rec,
                          L_rib_tsfref_rec,
                          O_routing_info,
                          L_freight_code,
                          L_to_loc_type,
                          L_from_loc_type,
                          I_tsf_no) = FALSE then
      return FALSE;
   end if;

   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           L_rib_tsfdtl_tbl,
                           L_tsf_mfqueue_rowid,
                           L_tsf_mfqueue_size,
                           L_tsfdetail_rowid,
                           L_tsfdetail_size,
                           L_delete_rowid_ind,
                           null, --message_type
                           I_tsf_no,
                           L_rib_tsfdesc_rec.from_loc,
                           L_rib_tsfdesc_rec.to_loc,
                           L_rib_tsfdesc_rec.to_loc_type,
                           I_max_details,
                           L_freight_code) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   --- if flag from BUILD_DETAIL_OBJECTS is TRUE, add rowid to L_alloc_mfqueue_rowid table;
   if L_delete_rowid_ind = 'Y' then
      L_tsf_mfqueue_size := L_tsf_mfqueue_size + 1;
      L_tsf_mfqueue_rowid(L_tsf_mfqueue_size) := I_rowid;
   else
      O_keep_queue := TRUE;
   end if;

   L_rib_tsfdesc_rec.tsfdtl_tbl := null;
   L_rib_tsfref_rec.tsfdtlref_tbl := null;

   update transfers_pub_info
      set physical_from_loc = L_rib_tsfref_rec.physical_from_loc,
          from_loc          = L_rib_tsfref_rec.from_loc,
          from_loc_type     = L_from_loc_type,
          physical_to_loc   = L_rib_tsfref_rec.physical_to_loc,
          to_loc            = L_rib_tsfref_rec.to_loc,
          to_loc_type       = L_to_loc_type,
          freight_code      = L_freight_code,
          published         = DECODE(L_delete_rowid_ind, 'Y', 'Y', 'I')
    where tsf_no = I_tsf_no;

   -- add the detail to the header
   L_rib_tsfdesc_rec.tsfdtl_tbl := L_rib_tsfdtl_tbl;

   if L_tsf_mfqueue_size > 0 then
      FORALL i IN 1..L_tsf_mfqueue_size
         delete from tsf_mfqueue where rowid = L_tsf_mfqueue_rowid(i);
   end if;

   if L_tsfdetail_size > 0 then
      FORALL i IN 1..L_tsfdetail_size
         update tsfdetail set publish_ind = 'Y'
          where rowid = L_tsfdetail_rowid(i);
   end if;

   O_message := L_rib_tsfdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.MAKE_CREATE',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END MAKE_CREATE;
--------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg        IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_rib_tsfdesc_rec  IN OUT NOCOPY "RIB_TsfDesc_REC",
                                     I_message_type     IN            tsf_mfqueue.message_type%TYPE,
                                     I_tsf_no           IN            tsfhead.tsf_no%TYPE,
                                     I_item             IN            item_loc.item%TYPE,
                                     I_freight_code     IN            tsfhead.freight_code%TYPE,
                                     I_max_details      IN            rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN IS

   L_tsf_mfqueue_rowid     rowid_TBL;
   L_tsf_mfqueue_size      BINARY_INTEGER := 0;

   L_tsfdetail_rowid       rowid_TBL;
   L_tsfdetail_size        BINARY_INTEGER := 0;

   L_delete_rowid_ind    VARCHAR2(1);

BEGIN

   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           O_rib_tsfdesc_rec.tsfdtl_tbl,
                           L_tsf_mfqueue_rowid,
                           L_tsf_mfqueue_size,
                           L_tsfdetail_rowid,
                           L_tsfdetail_size,
                           L_delete_rowid_ind,
                           I_message_type,
                           I_tsf_no,
                           O_rib_tsfdesc_rec.from_loc,
                           O_rib_tsfdesc_rec.to_loc,
                           O_rib_tsfdesc_rec.to_loc_type,
                           I_max_details,
                           I_freight_code) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_tsfdetail_size > 0 then
      FORALL i IN 1..L_tsfdetail_size
         update tsfdetail set publish_ind = 'Y'
          where rowid = L_tsfdetail_rowid(i);
   end if;

   if L_tsf_mfqueue_size > 0 then
      FORALL i IN 1..L_tsf_mfqueue_size
         delete from tsf_mfqueue where rowid = L_tsf_mfqueue_rowid(i);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.BUILD_DETAIL_CHANGE_OBJECTS',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_CHANGE_OBJECTS;
--------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_DELETE_OBJECTS(O_error_msg        IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_rib_tsfref_rec   IN OUT NOCOPY "RIB_TsfRef_REC",
                                     I_tsf_no           IN            tsfhead.tsf_no%TYPE,
                                     I_max_details      IN            rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN IS

   L_tsf_mfqueue_rowid     rowid_TBL;
   L_tsf_mfqueue_size      BINARY_INTEGER := 0;

   L_rib_tsfdtlref_tbl     "RIB_TsfDtlRef_TBL" := NULL;
   L_rib_tsfdtlref_rec     "RIB_TsfDtlRef_REC" := NULL;

   cursor C_DETAIL_DELETE is
      select tq.item,
             tq.rowid
        from tsf_mfqueue tq
       where tq.tsf_no       = I_tsf_no
         and tq.message_type = DTL_DEL
         and rownum         <= I_max_details;

BEGIN

   L_rib_tsfdtlref_tbl := "RIB_TsfDtlRef_TBL"();
   L_rib_tsfdtlref_rec := "RIB_TsfDtlRef_REC"(0,null);

   FOR rec IN C_DETAIL_DELETE LOOP

      L_rib_tsfdtlref_rec.item := rec.item;
      L_rib_tsfdtlref_tbl.EXTEND;
      L_rib_tsfdtlref_tbl(L_rib_tsfdtlref_tbl.COUNT) := L_rib_tsfdtlref_rec;

      L_tsf_mfqueue_size := L_tsf_mfqueue_size + 1;
      L_tsf_mfqueue_rowid(L_tsf_mfqueue_size) := rec.rowid;

   END LOOP;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_tsf_mfqueue_size > 0 then
      FORALL i IN 1..L_tsf_mfqueue_size
         delete from tsf_mfqueue where rowid = L_tsf_mfqueue_rowid(i);
   end if;

   O_rib_tsfref_rec.tsfdtlref_tbl := L_rib_tsfdtlref_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.BUILD_DETAIL_DELETE_OBJECTS',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_DELETE_OBJECTS;
--------------------------------------------------------------------------------
FUNCTION BUILD_HEADER_OBJECT(O_error_msg        IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                             O_message          IN OUT NOCOPY RIB_OBJECT,
                             O_rib_tsfref_rec   IN OUT NOCOPY "RIB_TsfRef_REC",
                             O_routing_info     IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             O_freight_code     IN OUT        tsfhead.freight_code%TYPE,
                             O_to_loc_type      IN OUT        item_loc.loc_type%TYPE,
                             O_from_loc_type    IN OUT        item_loc.loc_type%TYPE,
                             I_tsf_no           IN            tsfhead.tsf_no%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(61) := 'RMSMFM_TRANSFERS.BUILD_HEADER_OBJECT';

   L_days                  NUMBER(38):=NULL;
   L_default_order_type    SYSTEM_OPTIONS.DEFAULT_ORDER_TYPE%TYPE:=NULL;

   L_rib_tsfdesc_rec       "RIB_TsfDesc_REC" := NULL;
   L_rib_routing_rec       "RIB_ROUTINGINFO_REC" := NULL;

   L_vir_from_loc          ITEM_LOC.LOC%TYPE:=NULL;
   L_phy_from_loc          ITEM_LOC.LOC%TYPE:=NULL;
   L_vir_to_loc            ITEM_LOC.LOC%TYPE:=NULL;
   L_phy_to_loc            ITEM_LOC.LOC%TYPE:=NULL;

   L_pick_not_before_date  DATE:=NULL;
   L_pick_not_after_date   DATE:=NULL;
   L_wfo_not_after_date    wf_order_detail.not_after_date%TYPE := NULL;
   L_lead_time             repl_item_loc.wh_lead_time%TYPE := NULL;

   LP_vdate             DATE:=get_vdate();
   
   -- tsfhead has virtual locations --
   cursor C_TSF_HEAD is
      with locs as
      (select store loc,
              store phys_loc,
              tsf_entity_id,
              store_type,
              stockholding_ind
         from store
       union all
       select wh loc,
              physical_wh phys_loc,
              tsf_entity_id,
              NULL store_type,
              NULL stockholding_ind
         from wh
       union all
       select to_number(partner_id) loc,
              to_number(partner_id) phys_loc,
              tsf_entity_id,
              NULL store_type,
              NULL stockholding_ind
         from partner
        where partner_type = 'E')
      select th.freight_code,
             ---
             "RIB_TsfDesc_REC"(0,                          --RIB_OID NUMBER
                               I_tsf_no,                   --tsf_no number(12),
                               'T',                        --doc_type varchar2(1),
                               locs_from.phys_loc,         --physical_from_loc number(10),
                               th.from_loc_type,           --from_loc_type varchar2(1),
                               locs_from.store_type,       --from_store_type varchar2(1),
                               locs_from.stockholding_ind, --from_stockholding_ind varchar2(1),
                               th.from_loc,                --from_loc varchar2(10),
                               locs_to.phys_loc,           --physical_to_loc number(10),
                               th.to_loc_type,             --to_loc_type varchar2(1),
                               locs_to.store_type,         --to_store_type varchar2(1),
                               locs_to.stockholding_ind,   --to_stockholding_ind varchar2(1),
                               th.to_loc,                  --to_loc varchar2(10),
                               th.tsf_type,                --tsf_type varchar2(6),
                               NULL,                       --pick_not_before_date date,
                               NULL,                       --pick_not_after_date date,
                               decode(th.tsf_type, 'CO', 'MANUAL', L_default_order_type), --order_type varchar2(9),
                               NULL,                       --priority number(4),
                               decode(oc.delivery_type, 'S', 'Y', 'N'), --break_by_distro varchar2(1),
                               th.delivery_date,           --delivery_date date,
                               NULL,                       --cust_name varchar2(40),
                               oc.deliver_add1,            --deliver_add1 varchar2(240),
                               oc.deliver_add2,            --deliver_add2 varchar2(240),
                               oc.deliver_city,            --deliver_city varchar2(120),
                               oc.deliver_state,           --deliver_state varchar2(3),
                               oc.deliver_post,            --deliver_post varchar2(30),
                               oc.deliver_country_id,      --deliver_country_id varchar2(3),
                               th.comment_desc,            --message varchar2(2000),
                               NULL,                       --TsfDtl_TBL "RIB_TsfDtl_TBL",
                               th.tsf_parent_no,           --tsf_parent_no number(12),
                               th.exp_dc_date,             --exp_dc_date date,
                               th.approval_id,             --approval_id varchar2(30),
                               th.approval_date,           --approval_date date,
                               locs_from.tsf_entity_id,    --from_loc_tsf_entity number(10),
                               locs_to.tsf_entity_id,      --to_loc_tsf_entity number(10),
                               th.inventory_type,          --inv_type varchar2(6),
                               th.status,                  --tsf_status varchar2(1),
                               th.not_after_date,          --not_after_date date,
                               th.context_type,            --context_type varchar2(6),
                               th.context_value,           --context_value varchar2(25),
                               th.delivery_slot_id,        --delivery_slot_id varchar2(15),
                               ds.delivery_slot_desc,      --delivery_slot_desc varchar2(240),
                               oc.customer_order_no,       --cust_order_nbr varchar2(48),
                               oc.fulfill_order_no,        --fulfill_order_nbr varchar2(48),
                               oc.carrier_code,            --carrier_code varchar2(4),
                               oc.carrier_service_code,    --carrier_service_code varchar2(6),
                               oc.consumer_delivery_date,  --consumer_delivery_date date,
                               oc.consumer_delivery_time,  --consumer_delivery_time date,
                               oc.deliver_first_name,      --deliver_first_name varchar2(120),
                               oc.deliver_phonetic_first,  --deliver_phonetic_first varchar2(120),
                               oc.deliver_last_name,       --deliver_last_name varchar2(120),
                               oc.deliver_phonetic_last,   --deliver_phonetic_last varchar2(120),
                               oc.deliver_preferred_name,  --deliver_preferred_name varchar2(120),
                               oc.deliver_company_name,    --deliver_company_name varchar2(120),
                               oc.deliver_add3,            --deliver_add3 varchar2(240),
                               oc.deliver_county,          --deliver_county varchar2(250),
                               oc.deliver_phone,           --deliver_phone varchar2(20),
                               oc.bill_first_name,         --bill_first_name varchar2(120),
                               oc.bill_phonetic_first,     --bill_phonetic_first varchar2(120),
                               oc.bill_last_name,          --bill_last_name varchar2(120),
                               oc.bill_phonetic_last,      --bill_phonetic_last varchar2(120),
                               oc.bill_preferred_name,     --bill_preferred_name varchar2(120),
                               oc.bill_company_name,       --bill_company_name varchar2(120),
                               oc.bill_add1,               --bill_add1 varchar2(240),
                               oc.bill_add2,               --bill_add2 varchar2(240),
                               oc.bill_add3,               --bill_add3 varchar2(240),
                               oc.bill_county,             --bill_county varchar2(250),
                               oc.bill_city,               --bill_city varchar2(120),
                               oc.bill_country_id,         --bill_country varchar2(3),
                               oc.bill_post,               --bill_post varchar2(30),
                               oc.bill_state,              --bill_state varchar2(3),
                               oc.bill_phone,              --bill_phone varchar2(20),
                               oc.partial_delivery_ind,    --partial_delivery_ind varchar2(1),
                               -- consumer_direct is set to 'Y' for all CO transfers from warehouse
                               case
                                  when th.tsf_type = 'CO' then
                                     'Y'
                                  else
                                     'N'
                               end)   --consumer_direct varchar2(1)
        from tsfhead th,
             ordcust oc,
             locs locs_to,
             locs locs_from,
             delivery_slot ds
       where th.tsf_no     = I_tsf_no
         and th.tsf_no     = oc.tsf_no(+)
         and th.from_loc   = locs_from.loc
         and th.to_loc     = locs_to.loc
         and th.delivery_slot_id = ds.delivery_slot_id(+);

   cursor C_WH_CS_LEADTIME is
      select NVL(MAX(ril.wh_lead_time),0)
        from tsfhead th,
             tsfdetail td,
             repl_item_loc ril
       where th.tsf_no  = I_tsf_no
         and td.tsf_no = th.tsf_no
         and ril.item = td.item
         and ril.location = th.to_loc
         and ril.loc_type = th.to_loc_type
         and ril.source_wh = th.from_loc;

   cursor C_WH_CS_TRANSITTIME is
      select MAX(tt.transit_time)
        from tsfdetail td,
             tsfhead th,
             item_master im,
             transit_times tt
       where th.tsf_no = I_tsf_no
         and td.tsf_no = th.tsf_no
         and td.item = im.item
         and tt.dept = im.dept
         and tt.class = im.class
         and tt.subclass = im.subclass
         and tt.origin = th.from_loc
         and tt.origin_type = th.from_loc_type
         and tt.destination = th.to_loc
         and tt.destination_type = th.to_loc_type;

BEGIN

   if GET_GLOBALS(O_error_msg,
                  L_days,
                  L_default_order_type) = FALSE then
      return FALSE;
   end if;

   open C_TSF_HEAD;
   fetch C_TSF_HEAD into O_freight_code, L_rib_tsfdesc_rec;
   close C_TSF_HEAD;

   if L_rib_tsfdesc_rec is NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_TSFHEAD_PUB',
                                        I_tsf_no, NULL, NULL);
      return FALSE;
   end if;

   if L_rib_tsfdesc_rec.approval_date IS NULL then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_TSF_APPR_DATE',
                                        I_tsf_no, NULL, NULL);
      return FALSE;
   end if;

   -- set location variables
   O_from_loc_type := L_rib_tsfdesc_rec.from_loc_type;
   O_to_loc_type   := L_rib_tsfdesc_rec.to_loc_type;
   L_vir_from_loc  := L_rib_tsfdesc_rec.from_loc;
   L_phy_from_loc  := L_rib_tsfdesc_rec.physical_from_loc;
   L_vir_to_loc    := L_rib_tsfdesc_rec.to_loc;
   L_phy_to_loc    := L_rib_tsfdesc_rec.physical_to_loc;

   -- set pick_not_before_date and pick_not_after_date
   L_pick_not_before_date := L_rib_tsfdesc_rec.approval_date;
   L_pick_not_after_date := L_rib_tsfdesc_rec.approval_date + L_days;

   if L_rib_tsfdesc_rec.tsf_status = 'C' then
      L_pick_not_after_date := L_pick_not_before_date - 1;
   end if;

   L_rib_tsfdesc_rec.pick_not_before_date := L_pick_not_before_date;
   L_rib_tsfdesc_rec.pick_not_after_date := L_pick_not_after_date;

   --build Ref object
   O_rib_tsfref_rec := "RIB_TsfRef_REC"(
           0,                                       --RIB_OID               NUMBER
           I_tsf_no,                                --TSF_NO                NUMBER(12)
           'T',                                     --DOC_TYPE              VARCHAR2(1)
           L_phy_from_loc,                          --PHYSICAL_FROM_LOC     NUMBER(10)
           L_vir_from_loc,                          --FROM_LOC              VARCHAR2(10)
           L_rib_tsfdesc_rec.from_loc_type,         --FROM_LOC_TYPE         VARCHAR2(1)
           L_rib_tsfdesc_rec.from_store_type,       --FROM_STORE_TYPE       VARCHAR2(1)
           L_rib_tsfdesc_rec.from_stockholding_ind, --FROM_STOCKHOLDING_IND VARCHAR2(1)
           L_phy_to_loc,                            --PHYSICAL_TO_LOC       NUMBER(10)
           L_vir_to_loc,                            --TO_LOC                VARCHAR2(10)
           L_rib_tsfdesc_rec.to_loc_type,           --TO_LOC_TYPE           VARCHAR2(1)
           L_rib_tsfdesc_rec.to_store_type,         --TO_STORE_TYPE         VARCHAR2(1)
           L_rib_tsfdesc_rec.to_stockholding_ind,   --TO_STOCKHOLDING_IND   VARCHAR2(1)
           NULL,                                    --TSFDTLREF_TBL         "RIB_TsfDtlRef_TBL"
           L_rib_tsfdesc_rec.tsf_parent_no);        --TSF_PARENT_NO         NUMBER(12)

   -- create routing info --
   O_routing_info :=  RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec :=  RIB_ROUTINGINFO_REC('from_phys_loc', L_phy_from_loc, 'from_phys_loc_type', O_from_loc_type, null,null);
   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', L_phy_to_loc, 'to_phys_loc_type', O_to_loc_type, null,null);
   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   O_message := L_rib_tsfdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_HEADER_OBJECT;
--------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg           IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                              O_message             IN OUT NOCOPY   "RIB_TsfDtl_TBL",
                              O_tsf_mfqueue_rowid   IN OUT NOCOPY   rowid_TBL,
                              O_tsf_mfqueue_size    IN OUT          BINARY_INTEGER,
                              O_tsfdetail_rowid     IN OUT NOCOPY   rowid_TBL,
                              O_tsfdetail_size      IN OUT          BINARY_INTEGER,
                              O_delete_rowid_ind    IN OUT          VARCHAR2,
                              I_message_type        IN              tsf_mfqueue.message_type%TYPE,
                              I_tsf_no              IN              tsfhead.tsf_no%TYPE,
                              I_from_loc            IN              item_loc.loc%TYPE,
                              I_to_loc              IN              item_loc.loc%TYPE,
                              I_to_loc_type         IN              item_loc.loc_type%TYPE,
                              I_max_details         IN              rib_settings.max_details_to_publish%TYPE,
                              I_freight_code        IN              VARCHAR2)
RETURN BOOLEAN IS

   L_loc                 item_loc.loc%TYPE               := I_to_loc;
   L_finisher_ind        wh.finisher_ind%TYPE            := 'N';
   L_item                item_master.item%TYPE           := NULL;
   L_ticket_type_id      item_ticket.ticket_type_id%TYPE := NULL;
   L_publish_ind         tsfdetail.publish_ind%TYPE      := NULL;

   L_rib_tsfdtl_rec      "RIB_TsfDtl_REC" := NULL;
   L_rib_tsfdtl_tbl      "RIB_TsfDtl_TBL" := NULL;

   L_records_found       BOOLEAN:=FALSE;
   L_details_processed   rib_settings.max_details_to_publish%TYPE := 0;

   L_table               VARCHAR2(30)  := 'TSFDETAIL';
   L_key1                VARCHAR2(100) := I_tsf_no;
   L_key2                VARCHAR2(100) := I_to_loc;

   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   cursor C_TSF_DETAIL_MC is
      select /*+ index (il PK_ITEM_LOC) */ td.item,
             im.pack_ind,
             im.sellable_ind,
             td.tsf_qty,
             td.tsf_po_link_no,
             il.store_ord_mult,
             td.publish_ind,
             td.inv_status,
             ocv.transaction_uom,
             td.rowid tsfdetail_rowid,
             tq.rowid queue_rowid
        from tsfdetail      td,
             tsf_mfqueue    tq,
             item_loc       il,
             item_master    im,
             (select oc.tsf_no,
                     od.item,
                     od.transaction_uom
                from ordcust oc,
                     ordcust_detail od
               where oc.ordcust_no = od.ordcust_no) ocv
       where td.tsf_no      = I_tsf_no
         and td.publish_ind = 'N'
         and td.item        = im.item
         and il.item        = im.item
         and il.loc         = L_loc
         and td.tsf_no      = tq.tsf_no(+)
         and td.item        = tq.item(+)
         and td.tsf_no      = ocv.tsf_no(+)
         and td.item        = ocv.item(+)
         for update of td.publish_ind nowait;

   cursor C_TSF_DETAIL is
      select td.item,
             im.pack_ind,
             im.sellable_ind,
             td.tsf_qty,
             td.tsf_po_link_no,
             il.store_ord_mult,
             td.publish_ind,
             td.inv_status,
             ocv.transaction_uom,
             td.rowid tsfdetail_rowid,
             tq.rowid queue_rowid
        from tsfdetail      td,
             tsf_mfqueue    tq,
             item_loc       il,
             item_master    im,
             (select oc.tsf_no,
                     od.item,
                     od.transaction_uom
                from ordcust oc,
                     ordcust_detail od
               where oc.ordcust_no = od.ordcust_no) ocv
       where td.tsf_no       = I_tsf_no
         and td.item         = im.item
         and il.item         = im.item
         and il.loc          = L_loc
         and td.tsf_no       = tq.tsf_no
         and td.item         = tq.item
         and td.tsf_no       = ocv.tsf_no(+)
         and td.item         = ocv.item(+)
         and tq.message_type = I_message_type
         for update of td.publish_ind nowait;

   cursor C_TICKET_TYPE is
      select ticket_type_id
        from item_ticket
       where po_print_type = 'R'
         and item          = L_item;

   cursor C_FINISHER_IND is
      select finisher_ind
        from wh
       where wh = I_to_loc;

BEGIN

   L_rib_tsfdtl_rec := "RIB_TsfDtl_REC"(0,
                                      null,
                                      null,
                                      null,
                                      null,
                                      null,
                                      null,
                                      null,
                                      null,
                                      null,
                                      null,
                                      null);

   if O_message is NULL then
      L_rib_tsfdtl_tbl := "RIB_TsfDtl_TBL"();
   else
      L_rib_tsfdtl_tbl := O_message;
   end if;

   -- Packs will not exist at the finisher (to loc) so use the from_loc to retieve the 1st leg items
   -- Since STORE_ORD_MULT for a finisher is an unused column and is required, it is easiest to retieve all items from the from location
   if I_to_loc_type = 'E' then
      L_loc := I_from_loc;
   elsif I_to_loc_type = 'W' then
      open C_FINISHER_IND;
      fetch C_FINISHER_IND into L_finisher_ind;
      close C_FINISHER_IND;

      if L_finisher_ind = 'Y' then
         L_loc := I_from_loc;
      end if;
   end if;

   if I_message_type IS NULL then
      FOR rec IN C_TSF_DETAIL_MC LOOP
         L_records_found := TRUE;

         --only publish when the record exists on the tsfdetail and has not yet been published
         if rec.publish_ind = 'N' or I_message_type IS NOT NULL then

            if L_details_processed >= I_max_details then
               O_delete_rowid_ind := 'N';
               EXIT;
            end if;

            ---copy item variable to be used in ticket_type cursor
            L_item := rec.item;

            ----fetch the ticket_type_id for the item
            open  C_TICKET_TYPE;
            fetch C_TICKET_TYPE into L_ticket_type_id;
            close C_TICKET_TYPE;

            ----call procedure
            if BUILD_SINGLE_DETAIL(O_error_msg,
                                   L_rib_tsfdtl_tbl,
                                   L_rib_tsfdtl_rec,
                                   I_tsf_no,
                                   rec.item,
                                   rec.pack_ind,
                                   rec.sellable_ind,
                                   rec.store_ord_mult,
                                   rec.tsf_po_link_no,
                                   L_ticket_type_id,
                                   rec.tsf_qty,
                                   I_freight_code,
                                   I_to_loc,
                                   I_to_loc_type,
                                   rec.inv_status,
                                   rec.transaction_uom,
                                   I_message_type) = FALSE then
               return FALSE;
            end if;

            if I_message_type IS NULL or I_message_type = DTL_ADD then
               O_tsfdetail_size                    := O_tsfdetail_size + 1;
               O_tsfdetail_rowid(O_tsfdetail_size) := rec.tsfdetail_rowid;
            end if;

            L_details_processed := L_details_processed + 1;

         end if;

         if rec.queue_rowid is not null then
            O_tsf_mfqueue_size                      := O_tsf_mfqueue_size + 1;
            O_tsf_mfqueue_rowid(O_tsf_mfqueue_size) := rec.queue_rowid;
         end if;
      END LOOP;
   else
      FOR rec IN C_TSF_DETAIL LOOP
         L_records_found := TRUE;

         if L_details_processed >= I_max_details then
            O_delete_rowid_ind := 'N';
            EXIT;
         end if;

         ---copy item variable to be used in ticket_type cursor
         L_item := rec.item;

         ----fetch the ticket_type_id for the item
         open  C_TICKET_TYPE;
         fetch C_TICKET_TYPE into L_ticket_type_id;
         close C_TICKET_TYPE;

         ----call procedure
         if BUILD_SINGLE_DETAIL(O_error_msg,
                                L_rib_tsfdtl_tbl,
                                L_rib_tsfdtl_rec,
                                I_tsf_no,
                                rec.item,
                                rec.pack_ind,
                                rec.sellable_ind,
                                rec.store_ord_mult,
                                rec.tsf_po_link_no,
                                L_ticket_type_id,
                                rec.tsf_qty,
                                I_freight_code,
                                I_to_loc,
                                I_to_loc_type,
                                rec.inv_status,
                                rec.transaction_uom,
                                I_message_type) = FALSE then
            return FALSE;
         end if;

         if I_message_type = DTL_ADD then
            O_tsfdetail_size                    := O_tsfdetail_size + 1;
            O_tsfdetail_rowid(O_tsfdetail_size) := rec.tsfdetail_rowid;
         end if;

         if rec.queue_rowid is not null then
            O_tsf_mfqueue_size                      := O_tsf_mfqueue_size + 1;
            O_tsf_mfqueue_rowid(O_tsf_mfqueue_size) := rec.queue_rowid;
         end if;

         L_details_processed := L_details_processed + 1;
      END LOOP;
   end if;

   -- if no data found in cursor, raise error
   if not L_records_found then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_TSFDETAIL_PUB',
                                        I_tsf_no,
                                        NULL,
                                        NULL);
      return FALSE;
   end if;

   if L_rib_tsfdtl_tbl.COUNT > 0 then
      O_message := L_rib_tsfdtl_tbl;
   else
      O_message := NULL;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      return FALSE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.BUILD_DETAIL_OBJECTS',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_OBJECTS;
--------------------------------------------------------------------------------
FUNCTION BUILD_SINGLE_DETAIL(O_error_msg            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_message              IN OUT NOCOPY "RIB_TsfDtl_TBL",
                             IO_rib_tsfdtl_rec      IN OUT NOCOPY "RIB_TsfDtl_REC",
                             I_tsf_no               IN            tsfhead.tsf_no%TYPE,
                             I_item                 IN            item_master.item%TYPE,
                             I_pack_ind             IN            item_master.pack_ind%TYPE,
                             I_sellable_ind         IN            item_master.sellable_ind%TYPE,
                             I_store_ord_mult       IN            item_loc.store_ord_mult%TYPE,
                             I_tsf_po_link_no       IN            tsfdetail.tsf_po_link_no%TYPE,
                             I_ticket_type_id       IN            item_ticket.ticket_type_id%TYPE,
                             I_tsf_qty              IN            tsfdetail.tsf_qty%TYPE,
                             I_freight_code         IN            tsfhead.freight_code%TYPE,
                             I_to_loc               IN            item_loc.loc%TYPE,
                             I_to_loc_type          IN            item_loc.loc_type%TYPE,
                             I_inv_status           IN            inv_status_types.inv_status%TYPE,
                             I_transaction_uom      IN            uom_class.uom%TYPE,
                             I_message_type         IN            tsf_mfqueue.message_type%TYPE)
RETURN BOOLEAN IS

   L_price             item_loc.unit_retail%TYPE := NULL;
   L_selling_uom       item_loc.selling_uom%TYPE := NULL;

   --C_COMP_ITEM
   L_comp_price        item_loc.unit_retail%TYPE := NULL;
   L_comp_selling_uom  item_loc.selling_uom%TYPE := NULL;

   L_rib_tsfdtltckt_rec    "RIB_TsfDtlTckt_REC" := NULL;
   L_rib_tsfdtltckt_tbl    "RIB_TsfDtlTckt_TBL" := NULL;

  cursor C_COMP_ITEM is
     select item
       from v_packsku_qty
      where pack_no = I_item;
BEGIN

   if I_sellable_ind = 'Y' then

      if GET_RETAIL(O_error_msg,
                     I_item,
                     I_to_loc,
                     I_to_loc_type,
                     L_price,
                     L_selling_uom) = FALSE then
         return FALSE;
      end if;

   end if;

   IO_rib_tsfdtl_rec.item             := I_item;
   IO_rib_tsfdtl_rec.tsf_qty          := I_tsf_qty;
   IO_rib_tsfdtl_rec.price            := L_price;
   IO_rib_tsfdtl_rec.selling_uom      := L_selling_uom;
   IO_rib_tsfdtl_rec.priority         := NULL;
   if I_freight_code = 'E' then
      IO_rib_tsfdtl_rec.expedite_flag := 'Y';
   else
      IO_rib_tsfdtl_rec.expedite_flag := 'N';
   end if;
   IO_rib_tsfdtl_rec.store_ord_mult   := I_store_ord_mult;
   IO_rib_tsfdtl_rec.tsf_po_link_no   := I_tsf_po_link_no;
   IO_rib_tsfdtl_rec.ticket_type_id   := I_ticket_type_id;
   IO_rib_tsfdtl_rec.tsf_qty          := I_tsf_qty;
   IO_rib_tsfdtl_rec.inv_status       := I_inv_status;
   IO_rib_tsfdtl_rec.transaction_uom  := I_transaction_uom;

   if I_sellable_ind = 'Y' AND I_pack_ind = 'Y' AND I_to_loc_type = 'S' then

      L_rib_tsfdtltckt_rec := "RIB_TsfDtlTckt_REC"(
              0, null,null,null);
      L_rib_tsfdtltckt_tbl := "RIB_TsfDtlTckt_TBL"();

      FOR comp_rec IN C_COMP_ITEM LOOP

         if GET_RETAIL(O_error_msg,
                       comp_rec.item,
                       I_to_loc,
                       I_to_loc_type,
                       L_comp_price,
                       L_comp_selling_uom) = FALSE then
            return FALSE;
         end if;

         L_rib_tsfdtltckt_rec.comp_item        := comp_rec.item;
         L_rib_tsfdtltckt_rec.comp_price       := L_comp_price;
         L_rib_tsfdtltckt_rec.comp_selling_uom := L_comp_selling_uom;

         L_rib_tsfdtltckt_tbl.EXTEND;
         L_rib_tsfdtltckt_tbl(L_rib_tsfdtltckt_tbl.COUNT) := L_rib_tsfdtltckt_rec;

      END LOOP;

      IO_rib_tsfdtl_rec.tsfdtltckt_tbl := L_rib_tsfdtltckt_tbl;

   end if;

   O_message.EXTEND;
   O_message(O_message.COUNT) := IO_rib_tsfdtl_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.BUILD_SINGLE_DETAIL',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_SINGLE_DETAIL;
--------------------------------------------------------------------------------
FUNCTION GET_RETAIL(O_error_msg    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_item         IN     item_loc.item%TYPE,
                     I_loc          IN     item_loc.loc%TYPE,
                     I_loc_type     IN     item_loc.loc_type%TYPE,
                     O_price        IN OUT item_loc.unit_retail%TYPE,
                     O_selling_uom  IN OUT item_loc.selling_uom%TYPE)
RETURN BOOLEAN IS

   --fillers for pkg call
   L_dumb_unit_retail_loc         item_loc.unit_retail%TYPE := NULL;
   L_dumb_uom_loc                 item_loc.selling_uom%TYPE := NULL;
   L_dumb_multi_units_loc         item_loc.multi_units%TYPE := NULL;
   L_dumb_multi_unit_retail_loc   item_loc.unit_retail%TYPE := NULL;
   L_dumb_multi_selling_uom_loc   item_loc.selling_uom%TYPE := NULL;

BEGIN

   if PRICING_ATTRIB_SQL.GET_RETAIL(O_error_msg,
                                    L_dumb_unit_retail_loc,
                                    L_dumb_uom_loc,
                                    O_price,
                                    O_selling_uom,
                                    L_dumb_multi_units_loc,
                                    L_dumb_multi_unit_retail_loc,
                                    L_dumb_multi_selling_uom_loc,
                                    I_item,
                                    I_loc_type,
                                    I_loc) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.GET_RETAIL',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END GET_RETAIL;
--------------------------------------------------------------------------------
FUNCTION GET_GLOBALS(O_error_msg           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_days                IN OUT NUMBER,
                     O_default_order_type  IN OUT system_options.default_order_type%TYPE)
RETURN BOOLEAN IS

   cursor C_DATES is
      select to_number(c.code_desc)
        from code_detail c
       where c.code_type = 'DEFT'
         and c.code = 'DATE';

   L_system_options   SYSTEM_OPTIONS%ROWTYPE;

BEGIN

   if LP_nbf_days IS NULL then
      open C_DATES;
      fetch C_DATES into LP_nbf_days;
      close C_DATES;

      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_msg,
                                               L_system_options) = FALSE then
         return FALSE;
      end if;

      LP_default_order_type := L_system_options.default_order_type;
   end if;

   O_days := LP_nbf_days;
   O_default_order_type := LP_default_order_type;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.GET_GLOBALS',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END GET_GLOBALS;
--------------------------------------------------------------------------------
FUNCTION DELETE_QUEUE_REC(O_error_msg IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_seq_no    IN     tsf_mfqueue.seq_no%TYPE)
RETURN BOOLEAN IS

BEGIN

   delete from tsf_mfqueue
    where seq_no = I_seq_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.DELETE_QUEUE_REC',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_QUEUE_REC;
--------------------------------------------------------------------------------
FUNCTION LOCK_THE_BLOCK(O_error_msg     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_queue_locked  IN OUT BOOLEAN,
                        I_tsf_no        IN     tsf_mfqueue.tsf_no%TYPE)
RETURN BOOLEAN IS

   L_table             VARCHAR2(30)  := 'TSF_MFQUEUE';
   L_key1              VARCHAR2(100) := I_tsf_no;
   L_key2              VARCHAR2(100) := NULL;

   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from tsf_mfqueue tq
       where tq.tsf_no = I_tsf_no
        for update nowait;

BEGIN

   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      O_queue_locked := TRUE;
      return TRUE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.LOCK_THE_BLOCK',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_THE_BLOCK;
--------------------------------------------------------------------------------
FUNCTION LOCK_DETAILS(O_error_msg IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tsf_no    IN     tsfhead.tsf_no%TYPE)
RETURN BOOLEAN IS

   L_table             VARCHAR2(30)  := 'TSFDETAIL';
   L_key1              VARCHAR2(100) := I_tsf_no;
   L_key2              VARCHAR2(100) := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_DETAILS is
      select 'X'
        from tsfdetail
       where tsf_no = I_tsf_no
         for update of publish_ind nowait;

BEGIN
   open C_LOCK_DETAILS;
   close C_LOCK_DETAILS;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      return FALSE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_TRANSFERS.LOCK_DETAILS',
                                        TO_CHAR(SQLCODE));
      return FALSE;

END LOCK_DETAILS;
--------------------------------------------------------------------------------
END RMSMFM_TRANSFERS;
/

