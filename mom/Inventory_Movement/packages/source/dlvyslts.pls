CREATE OR REPLACE PACKAGE DLVYSLT_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_DELETE
-- Purpose: The function is called by the form dlvyslt.fmb, to check if the delivery
--          slot information exists in the tables where the slot id id referenced.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_delete_ok        IN OUT  BOOLEAN,
                       I_delivery_slot_id IN      DELIVERY_SLOT.DELIVERY_SLOT_ID%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_DELIVERY_SLOT_EXISTS
-- Purpose: The function is called by the form dlvyslt.fmb, to check if the delivery
--          slot information exists in the Delivery_slot table..
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DELIVERY_SLOT_EXISTS (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists           IN OUT  BOOLEAN,
                                     I_delivery_slot_id IN      DELIVERY_SLOT.DELIVERY_SLOT_ID%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_SEQUENCE_UNIQUE
-- Purpose: The function is called by the form dlvyslt.fmb, to check that there are no duplicate
--          entries of the delivery sequence..
---------------------------------------------------------------------------------------------
FUNCTION CHECK_SEQUENCE_UNIQUE (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT  BOOLEAN)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END DLVYSLT_SQL;
/
