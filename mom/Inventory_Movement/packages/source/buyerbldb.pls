CREATE OR REPLACE PACKAGE BODY BUYER_BUILD_SQL AS

TYPE L_pool_supp_tbl IS TABLE OF ORDHEAD.SUPPLIER%TYPE INDEX BY BINARY_INTEGER;
TYPE L_order_no_tbl IS TABLE OF ORDHEAD.ORDER_NO%TYPE INDEX BY BINARY_INTEGER;
TYPE L_file_id_tbl IS TABLE OF VARCHAR2(30) INDEX BY BINARY_INTEGER;

FUNCTION CREATE_LINKS(I_ordhead_tbl     IN      ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                      I_order_count     IN      NUMBER,
                      O_error_message   IN OUT  VARCHAR2)
RETURN BOOLEAN;

FUNCTION GET_FILE_ID(I_pool_supp_tbl    IN      L_pool_supp_tbl,
                     O_file_id_tbl      IN OUT  L_file_id_tbl,
                     I_pool_supp        IN      ORDHEAD.SUPPLIER%TYPE,
                     I_ord_cnt          IN      NUMBER,
                     O_error_message    IN OUT  VARCHAR2)
RETURN BOOLEAN;

FUNCTION SWITCHING_VARIABLE_CHANGE(I_cur_records   IN  ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC)
RETURN NUMBER;

FUNCTION ADD_UTIL_DETAILS_GTT(O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: BUILD_PO_WRAPPER
-- Purpose : Gets called from the function CREATE_PO_SQL. Driving function for the rest of the program,
--           contains the driving cursor.A function will be called to create RMS orders from the info held
--           in the order table types. The orders created will be validated if
--           they are going to be set to submitted or approved status.
---------------------------------------------------------------------------------------------------------------
FUNCTION BUILD_PO_WRAPPER (I_session_id       IN     VARCHAR2,
                           I_approval_ind     IN     VARCHAR2,
                           O_error_message    IN OUT VARCHAR2)
RETURN BINARY_INTEGER IS

   L_program               VARCHAR2(100):= 'BUYER_BUILD_SQL.BUILD_PO_WRAPPER';
   
   li_change               NUMBER;
   L_get_cur_rec           ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC;
   supplier_info           ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC;
   order_head_tbl          ADD_LINE_ITEM_SQL.ORDHEAD_TBL;
   orders_processed_count  NUMBER := 0;

   cursor C_DRIVER is
      select rr.source_type,
             rr.primary_repl_supplier    supplier,
             rr.dept,
             I_approval_ind  order_status,
             rr.item,
             rr.item_type    pack_ind,
             rr.location     loc,
             NVL(rr.physical_wh, rr.location)   phy_loc,
             NVL(wh.repl_wh_link, rr.location)  repl_wh_link,
             rr.loc_type,
             'Y'    due_ind,
             'N'    xdock_ind,
             '-1'   xdock_store,
             '-1'   contract,
             NULL   contract_type,
             rr.order_roq    qty,
             NVL(rr.last_rounded_qty,0)   last_rounded_qty,
             NVL(rr.last_grp_rounded_qty,0)   last_grp_rounded_qty,
             rr.origin_country_id   ctry,
             rr.unit_cost,
             rr.supp_unit_cost   unit_cost_init,
             NVL(rr.supp_lead_time, 0)   supp_lead_time,
             NVL(rr.pickup_lead_time, 0)  pickup_lead_time,
             rr.non_scaling_ind,
             NVL(rr.case_size,1)   pack_size,
             0   eso,
             0  aso,
             NVL(rr.tsf_po_link_no, -1)   tsf_po_link,
             ROWIDTOCHAR(rr.rowid)   rr_rowid,
             '-1'   ir_rowid,   -- ib_results rowid
             '-1'   bw_rowid,   -- buyer_wksht_manual rowid
             NVL(rr.pool_supplier, -1)   pool_supp,
             NULL   file_id,
             NULL   contract_terms,
             NULL   contract_approval_ind,
             NULL   contract_header_rowid,
             NULL   split_ref_ord_no,
             0      splitting_increment,
             0      round_pct,
             0      model_qty,
             0      ib_days_to_event,
             nvl(lcont.loc_country, so.base_country_id)   loc_country,
             NULL   costing_loc,
             NULL   store_type
        from repl_results rr,
             wh,
             (select key_value_1 loc, country_id loc_country
                from addr
               where module in ('ST', 'WFST', 'WH')
                 and addr_type = DECODE(module,'WFST',07,01)
                 and primary_addr_ind = 'Y') lcont,
             system_options so
       where rr.audsid   = TO_NUMBER(I_session_id)
         and rr.location = wh.wh(+)
         and to_char(rr.location) = lcont.loc(+)
         and rr.status   = 'W'
   union all
      select ir.source_type,
             ir.supplier,
             ir.dept,
             I_approval_ind                order_status,
             ir.item,
             ir.item_type                  pack_ind,
             ir.location                   loc,
             NVL(ir.physical_wh, ir.location)    phy_loc,
             NVL(ir.repl_wh_link, ir.location)      repl_wh_link,
             ir.loc_type,
             'Y'    due_ind,
             'N'    xdock_ind,
             '-1'   xdock_store,
             '-1'   contract,
             NULL   contract_type,
             NVL(ir.order_roq, 0)   qty,
             NVL(ir.last_rounded_qty,0)  last_rounded_qty,
             NVL(ir.last_grp_rounded_qty,0)   last_grp_rounded_qty,
             ir.origin_country_id   ctry,
             ir.unit_cost,
             ir.supp_unit_cost  unit_cost_init,
             NVL(ir.supp_lead_time, 0)  supp_lead_time,
             NVL(ir.pickup_lead_time, 0)  pickup_lead_time,
             'Y'   non_scale_ind,  /* non scale indicator */
             ir.case_size    pack_size,
             0      eso,
             0      aso,
             -1    tsf_po_link,   /* tsf_po_link */
             '-1'    rr_rowid, /* repl_results rowid */
             ROWIDTOCHAR(ir.rowid)   ir_rowid,
             '-1'   bw_rowid,/* buyer_wksht_manual rowid */
             NVL(ir.pool_supplier, -1)        pool_supp,
             NULL     file_id,
             NULL   contract_terms,
             NULL   contract_approval_ind,
             NULL   contract_header_rowid,
             NULL   split_ref_ord_no,
             0      splitting_increment,
             0      round_pct,
             0      model_qty,
             0      ib_days_to_event, /*days_to_event,*/
             nvl(lcont.loc_country, so.base_country_id)    loc_country,
             NULL   costing_loc,
             NULL   store_type
        from ib_results ir,
              (select key_value_1 loc, country_id loc_country
                 from addr
                where module in ('ST', 'WFST', 'WH')
                  and addr_type = DECODE(module,'WFST',07,01)
                  and primary_addr_ind = 'Y') lcont,
              system_options so
        where ir.audsid     = TO_NUMBER(I_session_id)
          and ir.status     = 'W'
          and to_char(ir.location)   = lcont.loc(+)
    union all
       select bw.source_type,
              bw.supplier,
              bw.dept,
              I_approval_ind   order_status,
              bw.item,
              bw.item_type  pack_ind,
              bw.location   loc,
              NVL(bw.physical_wh, bw.location)   phy_loc,
              NVL(bw.repl_wh_link, bw.location)   repl_wh_link,
              bw.loc_type,
              'Y'    due_ind,
              'N'    xdock_ind,
              '-1'   xdock_store,
              '-1'   contract,
              NULL   contract_type,
              bw.order_roq   qty,
              NVL(bw.last_rounded_qty,0)   last_rounded_qty,
              NVL(bw.last_grp_rounded_qty,0)  last_grp_rounded_qty,
              bw.origin_country_id  ctry,
              bw.unit_cost   unit_cost,
              bw.supp_unit_cost   unit_cost_init,
              NVL(bw.supp_lead_time, 0)   supp_lead_time,
              NVL(bw.pickup_lead_time, 0)  pickup_lead_time,
              'N'     non_scale_ind,  /* non scale indicator */
              bw.case_size   pack_size,
              0    eso,
              0    aso,
              -1   tsf_po_link,   /* tsf_po_link */
              '-1'    rr_rowid, /* repl_results rowid */
              '-1'    ir_rowid, /* ib_results rowid */
              ROWIDTOCHAR(bw.rowid)    bw_rowid,
              NVL(bw.pool_supplier, -1)      pool_supp,
              NULL    file_id,
              NULL   contract_terms,
              NULL   contract_approval_ind,
              NULL   contract_header_rowid,
              NULL   split_ref_ord_no,
              0      splitting_increment,
              0      round_pct,
              0      model_qty,
              0      ib_days_to_event,    /* l_ib_days_to_event */
              nvl(lcont.loc_country, so.base_country_id)   loc_country,
              NULL   costing_loc,
              NULL   store_type
         from buyer_wksht_manual bw,
              (select key_value_1 loc, country_id loc_country
                 from addr
                where module in ('ST', 'WFST', 'WH')
                  and addr_type = DECODE(module,'WFST',07,01)
                  and primary_addr_ind = 'Y') lcont,
              system_options so
        where bw.audsid     = TO_NUMBER(I_session_id)
          and bw.status     = 'W'
          and to_char(bw.location)   = lcont.loc(+)
        /* order by supplier, pool_supp, loc_country, source_type, days_to_event, item, loc */
        order by 2, 3, 27, 1 desc, 18, 6, 8;

BEGIN

   if ADD_LINE_ITEM_SQL.SET_GLOBALS(O_error_message) = FALSE then
      return -1;
   end if;

   ADD_LINE_ITEM_SQL.LP_batch_ind := 'N';

   open C_DRIVER;
   LOOP
      fetch C_DRIVER into L_get_cur_rec.source_type,
                          L_get_cur_rec.supplier,
                          L_get_cur_rec.dept,
                          L_get_cur_rec.order_status,
                          L_get_cur_rec.item,
                          L_get_cur_rec.pack_ind,
                          L_get_cur_rec.loc,
                          L_get_cur_rec.phy_loc,
                          L_get_cur_rec.repl_wh_link,
                          L_get_cur_rec.loc_type,
                          L_get_cur_rec.due_ind,
                          L_get_cur_rec.xdock_ind,
                          L_get_cur_rec.xdock_store,
                          L_get_cur_rec.contract,
                          L_get_cur_rec.contract_type,
                          L_get_cur_rec.qty,
                          L_get_cur_rec.last_rounded_qty,
                          L_get_cur_rec.last_grp_rounded_qty,
                          L_get_cur_rec.ctry,
                          L_get_cur_rec.unit_cost,
                          L_get_cur_rec.unit_cost_init,
                          L_get_cur_rec.supp_lead_time,
                          L_get_cur_rec.pickup_lead_time,
                          L_get_cur_rec.non_scale_ind,
                          L_get_cur_rec.pack_size,
                          L_get_cur_rec.eso,
                          L_get_cur_rec.aso,
                          L_get_cur_rec.tsf_po_link,
                          L_get_cur_rec.rr_rowid,
                          L_get_cur_rec.ir_rowid,
                          L_get_cur_rec.bw_rowid,
                          L_get_cur_rec.pool_supp,
                          L_get_cur_rec.file_id,
                          L_get_cur_rec.contract_terms,
                          L_get_cur_rec.contract_approval_ind,
                          L_get_cur_rec.contract_header_rowid,
                          L_get_cur_rec.split_ref_ord_no,
                          L_get_cur_rec.splitting_increment,
                          L_get_cur_rec.round_pct,
                          L_get_cur_rec.model_qty,
                          L_get_cur_rec.ib_days_to_event,
                          L_get_cur_rec.loc_country,
                          L_get_cur_rec.costing_loc,
                          L_get_cur_rec.store_type;
      
      EXIT WHEN c_driver%NOTFOUND;

      L_get_cur_rec.cnst_qty(1) := 0;
      L_get_cur_rec.cnst_qty(2) := 0;
      L_get_cur_rec.cnst_map(1) := 0;
      L_get_cur_rec.cnst_map(2) := 0;

      if L_get_cur_rec.rr_rowid = '-1' then
         L_get_cur_rec.rr_rowid := NULL;
      end if;
      if L_get_cur_rec.ir_rowid = '-1' then
         L_get_cur_rec.ir_rowid := NULL;
      end if;
      if L_get_cur_rec.bw_rowid = '-1' then
         L_get_cur_rec.bw_rowid := NULL;
      end if;
      if L_get_cur_rec.pool_supp = '-1' then
         L_get_cur_rec.pool_supp := NULL;
      end if;
      if L_get_cur_rec.tsf_po_link = '-1' then
         L_get_cur_rec.tsf_po_link := NULL;
      end if;

      /* load the inventory management information */
      if ADD_LINE_ITEM_SQL.GET_SUP_INFO(supplier_info,
                                        L_get_cur_rec.contract,
                                        L_get_cur_rec.supplier,
                                        L_get_cur_rec.dept,
                                        L_get_cur_rec.phy_loc,
                                        O_error_message) = FALSE then
         return -1;
      end if;

      /* reguardless of the inventory management information
            due order processing is not used from the buyer worksheet. */
      supplier_info.due_ord_process_ind := 'N';

      li_change := SWITCHING_VARIABLE_CHANGE(L_get_cur_rec);

      if li_change = 1 then

         if CREATE_PO_LIB_SQL.DO_INSERTS(order_head_tbl,
                                         orders_processed_count,
                                         O_error_message) = FALSE then
            return -1;
         end if;

         if CREATE_PO_LIB_SQL.UPDATE_GROUP_ROUNDED_QTY(order_head_tbl,
                                                       O_error_message) = FALSE then
            return -1;
         end if;

         if CREATE_LINKS(order_head_tbl,
                         orders_processed_count,
                         O_error_message)  = FALSE then
            return -1;
         end if;

         if CREATE_PO_LIB_SQL.APPROVE_SUBMIT_PO(order_head_tbl,
                                                O_error_message) = FALSE then
            return -1;
         end if;

         if CREATE_PO_LIB_SQL.ROUND_PO(order_head_tbl,
                                       O_error_message) = FALSE then
            return -1;
         end if;

         if ADD_UTIL_DETAILS_GTT(O_error_message) = FALSE then
            return -1;
         end if;

         orders_processed_count := 0;
      end if;
      /* add the item/location to an existing order in the order tables or
         create a new order for the item/location in the collection */
      if ADD_LINE_ITEM_SQL.ADD_TO_ORDER(order_head_tbl,
                                        L_get_cur_rec,
                                        supplier_info,
                                        NULL, -- order_no
                                        O_error_message) = FALSE then
         return -1;
      end if;
   END LOOP;
   close c_driver;

   if CREATE_PO_LIB_SQL.DO_INSERTS(order_head_tbl,
                                   orders_processed_count,
                                   O_error_message) = FALSE then
      return -1;
   end if;

   if CREATE_PO_LIB_SQL.UPDATE_GROUP_ROUNDED_QTY(order_head_tbl,
                                                 O_error_message) = FALSE then
      return -1;
   end if;

   if CREATE_LINKS(order_head_tbl,
                   orders_processed_count,
                   O_error_message)  = FALSE then
      return -1;
   end if;

   if CREATE_PO_LIB_SQL.APPROVE_SUBMIT_PO(order_head_tbl,
                                          O_error_message) = FALSE then
      return -1;
   end if;

   if ADD_UTIL_DETAILS_GTT(O_error_message) = FALSE then
      return -1;
   end if;

   if CREATE_PO_LIB_SQL.ROUND_PO(order_head_tbl,
                                 O_error_message) = FALSE then
      return -1;
   end if;

   return 0;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return -1;
END BUILD_PO_WRAPPER;
---------------------------------------------------------------------------------------------------------
FUNCTION CREATE_LINKS(I_ordhead_tbl      IN     ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                      I_order_count      IN     NUMBER,
                      O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN AS

   L_program        VARCHAR2(100) := 'BUYER_BUILD_SQL.CREATE_LINKS';

   ls_pool_supp     L_pool_supp_tbl;
   ls_order_no      L_order_no_tbl;
   ls_file_id       L_file_id_tbl;
   ll_ctr           NUMBER(4) := 1;

BEGIN

   if I_order_count <= 0 then
      return TRUE;
   end if;

   for i in 1..I_ordhead_tbl.count LOOP
      ls_order_no(i)  := I_ordhead_tbl(i).order_no;
      ls_pool_supp(i) := I_ordhead_tbl(i).pool_supp;
   end loop;

   for i in 1..I_order_count
   LOOP
      if ls_pool_supp(i) is NULL then
         ls_file_id(i) := NULL;
      else
         if get_file_id(ls_pool_supp,
                        ls_file_id,
                        ls_pool_supp(i),
                        I_order_count,
                        O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;

   if ls_file_id.COUNT > 0 then
      for j in 1 .. I_order_count
      LOOP
         update ord_inv_mgmt
            set file_id  = ls_file_id(j)
          where order_no = ls_order_no(j);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CREATE_LINKS; /* end create_links */
---------------------------------------------------------------------------------------------------------
FUNCTION GET_FILE_ID(I_pool_supp_tbl   IN     L_POOL_SUPP_TBL,
                     O_file_id_tbl     IN OUT L_FILE_ID_TBL,
                     I_pool_supp       IN     ORDHEAD.SUPPLIER%TYPE,
                     I_ord_cnt         IN     NUMBER,
                     O_error_message   IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program        VARCHAR2(100):= 'BUYER_BUILD_SQL.GET_FILE_ID';
   
   ll_ctr           NUMBER;
   ll_pool_count    NUMBER(4) := 0;
   ls_file_id       VARCHAR2(30);
   L_error_message  rtk_errors.rtk_text%TYPE := NULL;

BEGIN

   for ll_ctr in 1..I_ord_cnt 
   LOOP
      if I_pool_supp = I_pool_supp_tbl(ll_ctr) and O_file_id_tbl is NULL then
         O_file_id_tbl(ll_ctr) := NULL;
         ll_pool_count := ll_pool_count + 1;
      end if;
   end loop;

   if ll_pool_count < 2 then
      return TRUE;
   end if;

   if not ORDER_NUMBER_SQL.NEXT_ORDER_NUMBER(L_error_message,
                                             ls_file_id) then
      O_error_message := L_error_message;
      return FALSE;
   end if;

   for ll_ctr in 1..I_ord_cnt
   LOOP
      if I_pool_supp = I_pool_supp_tbl(ll_ctr) then
         O_file_id_tbl(ll_ctr) := ls_file_id;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_FILE_ID; /* end get_file_id */
---------------------------------------------------------------------------------------------------------
FUNCTION SWITCHING_VARIABLE_CHANGE(I_cur_records  IN ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC)
RETURN NUMBER AS

   L_program           VARCHAR2(100):= 'BUYER_BUILD_SQL.SWITCHING_VARIABLE_CHANGE';
   
   local_loc_country   VARCHAR2(10);
   first_time          NUMBER(4) := 1;
   change              NUMBER(4);
   O_error_message     VARCHAR2(2000);

BEGIN

   if first_time <> 0 then
      local_loc_country := I_cur_records.loc_country;
      first_time := 0;
   end if;

   if local_loc_country <> I_cur_records.loc_country then
      change := 1;
   else
      change := 0;
   end if;

   local_loc_country := I_cur_records.loc_country;

   return change;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return -1;
END SWITCHING_VARIABLE_CHANGE; /*end switching_variable_change*/
---------------------------------------------------------------------------------------------------------
FUNCTION ADD_UTIL_DETAILS_GTT(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN AS

   L_program        VARCHAR2(100) := 'BUYER_BUILD_SQL.ADD_UTIL_DETAILS_GTT';
   
   L_exists_ind     VARCHAR2(1):= NULL;
   L_procedure_key  VARCHAR2(30) := 'CREATE_TRAN_UTIL_CODE';
   L_error_message  VARCHAR2(2000);

BEGIN

   if not L10N_SQL.CALL_EXEC_FUNC_DOC(L_error_message,
                                      L_exists_ind,
                                      L_procedure_key) then

      O_error_message := SUBSTR(SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                   L_error_message,
                                                   'L10N_SQL.CALL_EXEC_FUNC_DOC',
                                                   NULL),1, 255);
      return FALSE;
   end if;

  return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ADD_UTIL_DETAILS_GTT;
---------------------------------------------------------------------------------------------------------
END BUYER_BUILD_SQL;
/