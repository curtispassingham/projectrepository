
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY TSF_ATTRIB_SQL AS
------------------------------------------------------------------------------
FUNCTION TSF_ZONE_DESC(O_error_message	IN OUT  VARCHAR2,
                       I_zone           IN      TSFZONE.TRANSFER_ZONE%TYPE,
                       O_desc           IN OUT  TSFZONE.DESCRIPTION%TYPE) RETURN BOOLEAN IS

   cursor C_ATTRIB is
   select description
     from v_tsfzone_tl
    where transfer_zone = I_zone;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_ATTRIB', 'V_TSFZONE_TL', 'TSFZONE: '||to_char(I_zone));
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH' , 'C_ATTRIB', 'V_TSFZONE_TL', 'TSFZONE: '||to_char(I_zone));
   fetch C_ATTRIB into O_desc;
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_TRAN_ZONE',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'V_TSFZONE_TL', 'TSFZONE: '||to_char(I_zone));
      close C_ATTRIB;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'TSFZONE', 'TSFZONE: '||to_char(I_zone));
   close C_ATTRIB;
   ---
   
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'TSF_ATTRIB_SQL.TSF_ZONE_DESC',
                                             to_char(SQLCODE));
   RETURN FALSE;
END TSF_ZONE_DESC;
--------------------------------------------------------------------
FUNCTION GET_TSFHEAD_INFO(O_error_message  IN OUT VARCHAR2,
                          I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE,
                          O_dept           IN OUT TSFHEAD.DEPT%TYPE,
                          O_create_date    IN OUT TSFHEAD.CREATE_DATE%TYPE,
                          O_status         IN OUT TSFHEAD.STATUS%TYPE,
                          O_from_loc_type  IN OUT TSFHEAD.FROM_LOC_TYPE%TYPE,
                          O_from_loc       IN OUT TSFHEAD.FROM_LOC%TYPE,
                          O_to_loc_type    IN OUT TSFHEAD.TO_LOC_TYPE%TYPE,
                          O_to_loc         IN OUT TSFHEAD.TO_LOC%TYPE,
                          O_tsf_type       IN OUT TSFHEAD.TSF_TYPE%TYPE,
                          O_freight_code   IN OUT TSFHEAD.FREIGHT_CODE%TYPE,
                          O_routing_code   IN OUT TSFHEAD.ROUTING_CODE%TYPE,
                          O_comment_desc   IN OUT TSFHEAD.COMMENT_DESC%TYPE)
RETURN BOOLEAN IS

   cursor C_TSFHEAD is
      select dept,
             create_date,
             status,
             from_loc_type,
             from_loc,
             to_loc_type,
             to_loc,
             tsf_type,
             freight_code,
             routing_code,
             comment_desc
        from tsfhead
       where tsf_no = I_tsf_no;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_TSFHEAD', 'TSFHEAD',
                    'TRANSFER: '||to_char(I_tsf_no));

   open C_TSFHEAD;

   SQL_LIB.SET_MARK('FETCH','C_TSFHEAD', 'TSFHEAD',
                    'TRANSFER: '||to_char(I_tsf_no));

   fetch C_TSFHEAD into O_dept,
                        O_create_date,
                        O_status,
                        O_from_loc_type,
                        O_from_loc,
                        O_to_loc_type,
                        O_to_loc,
                        O_tsf_type,
                        O_freight_code,
                        O_routing_code,
                        O_comment_desc;
   if C_TSFHEAD%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF',NULL,NULL,NULL);
      close C_TSFHEAD;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_TSFHEAD', 'TSFHEAD',
                    'TRANSFER: '||to_char(I_tsf_no));

   close C_TSFHEAD;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                            'TSF_ATTRIB.SQL.GET_TSFHEAD_INFO',
                                             NULL);
      return FALSE;
END GET_TSFHEAD_INFO;
-------------------------------------------------------------------------------------
FUNCTION GET_MAX_SEQ_NO(O_error_message IN OUT VARCHAR2,
                        I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                        O_seq_no        IN OUT TSFDETAIL.TSF_SEQ_NO%TYPE) RETURN BOOLEAN IS

   cursor C_SEQ_NO is
      select max(tsf_seq_no)
        from tsfdetail
       where tsf_no = I_tsf_no;

BEGIN

   SQL_LIB.SET_MARK('OPEN' ,'C_SEQ_NO','TSFDETAIL','TSF_NO: '||to_char(I_tsf_no));
   open C_SEQ_NO;
   SQL_LIB.SET_MARK('FETCH' ,'C_SEQ_NO','TSFDETAIL','TSF_NO: '||to_char(I_tsf_no));
   fetch C_SEQ_NO into O_seq_no;
   SQL_LIB.SET_MARK('CLOSE' ,'C_SEQ_NO','TSFDETAIL','TSF_NO: '||to_char(I_tsf_no));
   close C_SEQ_NO;
   ---
   if O_seq_no is NULL then
      O_seq_no := 0;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'TSF_ATTRIB_SQL.GET_MAX_SEQ_NO',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_MAX_SEQ_NO;
-------------------------------------------------------------------------
FUNCTION GET_QUANTITIES(O_error_message  IN OUT VARCHAR2,
                        I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE,
                        I_item           IN     TSFDETAIL.ITEM%TYPE,
                        O_seq_no         IN OUT TSFDETAIL.TSF_SEQ_NO%TYPE,
                        O_fill_qty       IN OUT TSFDETAIL.FILL_QTY%TYPE,
                        O_tsf_qty        IN OUT TSFDETAIL.TSF_QTY%TYPE,
                        O_ship_qty       IN OUT TSFDETAIL.SHIP_QTY%TYPE,
                        O_received_qty   IN OUT TSFDETAIL.RECEIVED_QTY%TYPE,
                        O_supp_pack_size IN OUT TSFDETAIL.SUPP_PACK_SIZE%TYPE)
     RETURN BOOLEAN IS

   cursor C_TSFDETAIL is
      select tsf_seq_no,
             nvl(fill_qty,0),
             nvl(tsf_qty,0),
             nvl(ship_qty,0),
             nvl(received_qty,0),
             supp_pack_size
        from tsfdetail
       where tsf_no = I_tsf_no
         and item   = I_item;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_TSFDETAIL','TSFDETAIL',
                    'TSF_NO: '||to_char(I_tsf_no)||'; Item: '||I_item);

   open C_TSFDETAIL;
   SQL_LIB.SET_MARK('FETCH','C_TSFDETAIL','TSFDETAIL',
                    'TSF_NO: '||to_char(I_tsf_no)||'; Item: '||I_item);
   fetch C_TSFDETAIL into O_seq_no,
                          O_fill_qty,
                          O_tsf_qty,
                          O_ship_qty,
                          O_received_qty,
                          O_supp_pack_size;
   if C_TSFDETAIL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF',NULL,NULL,NULL);
      close C_TSFDETAIL;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_TSFDETAIL','TSFDETAIL',
                    'TSF_NO: '||to_char(I_tsf_no)||'; Item: '||I_item);
   close C_TSFDETAIL;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TSF_ATTRIB_SQL.GET_QUANTITIES',
                                            to_char(SQLCODE));
      return FALSE;
END GET_QUANTITIES;
---------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_TSFHEAD
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION GET_STATUS(O_error_message       IN OUT VARCHAR2,
                    O_leg_1_status        IN OUT TSFHEAD.STATUS%TYPE,
                    O_leg_2_status        IN OUT TSFHEAD.STATUS%TYPE,
                    I_tsf_no              IN     TSFHEAD.TSF_NO%TYPE) RETURN BOOLEAN IS

   cursor C_GET_STATUS is
      select leg_1_status,
             leg_2_status
        from v_tsfhead 
       where tsf_no = I_tsf_no;

BEGIN

   SQL_LIB.SET_MARK('OPEN' ,'C_GET_STATUS','V_TSFHEAD','TRANSFER: '||to_char(I_tsf_no));
   open C_GET_STATUS;
   SQL_LIB.SET_MARK('FETCH' ,'C_GET_STATUS','V_TSFHEAD','TRANSFER: '||to_char(I_tsf_no));
   fetch C_GET_STATUS into O_leg_1_status,
                           O_leg_2_status;
   if C_GET_STATUS%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' ,'C_GET_STATUS','V_TSFHEAD','TRANSFER: '||to_char(I_tsf_no));
      close C_GET_STATUS;
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE' ,'C_GET_STATUS','V_TSFHEAD','TRANSFER: '||to_char(I_tsf_no));
   close C_GET_STATUS;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'TSF_ATTRIB_SQL.GET_STATUS',
                                             to_char(SQLCODE));
      return FALSE;
END GET_STATUS;
-------------------------------------------------------------------------------------------
FUNCTION GET_SUPP_PACK_SIZE(O_ERROR_MESSAGE  IN OUT VARCHAR2,
                            O_SUPP_PACK_SIZE IN OUT TSFDETAIL.SUPP_PACK_SIZE%TYPE,
                            I_tsf_no         IN     TSFDETAIL.TSF_NO%TYPE,
                            I_item           IN     TSFDETAIL.ITEM%TYPE) RETURN BOOLEAN IS

cursor C_SPS is
   select supp_pack_size
     from tsfdetail
    where tsf_no = I_tsf_no
      and item = I_item;

BEGIN
   if (I_tsf_no is NULL or I_item is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_SPS','TSFDETAIL',
                    'TSF_NO: '||to_char(I_tsf_no)||'; Item: '|| I_item);
   open C_SPS;
   SQL_LIB.SET_MARK('FETCH','C_SPS','TSFDETAIL',
                    'TSF_NO: '||to_char(I_tsf_no)||'; Item: '|| I_item);
   fetch C_SPS into O_supp_pack_size;
   if C_SPS%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF',NULL,NULL,NULL);
      close C_SPS;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_SPS','TSFDETAIL',
                    'TSF_NO: '||to_char(I_tsf_no)||'; Item: '|| I_item);

   close C_SPS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TSF_ATTRIB_SQL.GET_SUPP_PACK_SIZE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_SUPP_PACK_SIZE;
-------------------------------------------------------------------------------------------
FUNCTION TSF_CHILD_ITEMS_EXIST (O_error_message IN OUT VARCHAR2,
                                O_exist         IN OUT BOOLEAN,
                                I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE) RETURN BOOLEAN IS
   L_dummy	VARCHAR2(1);

   cursor C_TSF_EXIST is
      select 'x'
        from item_master,
             tsfdetail,
             packitem
       where tsfdetail.tsf_no   = I_tsf_no
         and ((item_master.item  = tsfdetail.item
               and item_master.item_level != 1)
          or (tsfdetail.item    = packitem.pack_no
              and packitem.item = item_master.item
              and item_master.item_level != 1));

BEGIN

   open C_TSF_EXIST;
   fetch C_TSF_EXIST into L_dummy;
   if C_TSF_EXIST%NOTFOUND then
      O_exist := FALSE;
      O_error_message := sql_lib.create_msg('FASH_SKU_TSF',I_tsf_no,null,null);
   else
	O_exist := TRUE;
   end if;
   close C_TSF_EXIST;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
	O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                              'TSF_ATTRIB_SQL.TSF_CHILD_ITEMS_EXIST',
                                               to_char(SQLCODE));
	return FALSE;
END TSF_CHILD_ITEMS_EXIST;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_TSFDETAIL
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------
FUNCTION TSF_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diff       IN OUT VARCHAR2,
                         I_tsf_no     IN     TSFHEAD.TSF_NO%TYPE)
            RETURN BOOLEAN IS

   CURSOR C_COUNT_TSF IS
      select count(tsf_no)
        from tsfdetail
       where tsf_no = I_tsf_no;

   CURSOR C_COUNT_VTSF IS
      select count(tsf_no)
        from v_tsfdetail
       where tsf_no = I_tsf_no;

   L_tsf  NUMBER(6) := -765547;
   L_vtsf NUMBER(6) := -987436;
BEGIN
   OPEN c_count_tsf;
   FETCH c_count_tsf INTO L_tsf;
   CLOSE c_count_tsf;

   OPEN c_count_vtsf;
   FETCH c_count_vtsf INTO L_vtsf;
   CLOSE c_count_vtsf;

   if L_vtsf != L_tsf then
     O_diff := 'Y';
   else
     O_diff := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
	O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                              'TSF_ATTRIB_SQL.TSF_FILTER_LIST',
                                               to_char(SQLCODE));
	return FALSE;
END TSF_FILTER_LIST;
--------------------------------------------------------------------------------------------
END TSF_ATTRIB_SQL;
/
