CREATE OR REPLACE PACKAGE RMSMFM_TRANSFERS AUTHID CURRENT_USER AS
/*--- message type parameters ---*/
FAMILY      VARCHAR2(64) := 'transfers';

HDR_ADD     VARCHAR2(64) := 'TransferCre';
HDR_UPD     VARCHAR2(64) := 'TransferHdrMod';
HDR_DEL     VARCHAR2(64) := 'TransferDel';
HDR_UNAPRV  VARCHAR2(64) := 'TransferUnapp';
DTL_ADD     VARCHAR2(64) := 'TransferDtlCre';
DTL_UPD     VARCHAR2(64) := 'TransferDtlMod';
DTL_DEL     VARCHAR2(64) := 'TransferDtlDel';

---------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_msg         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                I_message_type      IN     VARCHAR2,
                I_tsf_no            IN     tsfhead.tsf_no%TYPE,
                I_tsf_type          IN     tsfhead.tsf_type%TYPE,
                I_tsf_head_status   IN     tsfhead.status%TYPE,
                I_item              IN     tsfdetail.item%TYPE,
                I_publish_ind       IN     tsfdetail.publish_ind%TYPE,
                I_to_loc_type       IN     tsfhead.to_loc_type%TYPE DEFAULT NULL,
                I_tsf_parent_no     IN     tsfhead.tsf_parent_no%TYPE DEFAULT NULL,
                I_from_loc_type     IN     tsfhead.from_loc_type%TYPE DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1);
---------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT);
---------------------------------------------------------------------------
END RMSMFM_TRANSFERS;
/

