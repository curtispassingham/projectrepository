CREATE OR REPLACE PACKAGE CORESVC_TSF_ENTITY AUTHID CURRENT_USER AS
   template_key            CONSTANT VARCHAR2(255)         := 'TRANSFER_DATA';
   template_category       CONSTANT VARCHAR2(255)         := 'RMSINV';
   action_new                       VARCHAR2(25)          := 'NEW';
   action_mod                       VARCHAR2(25)          := 'MOD';
   action_del                       VARCHAR2(25)          := 'DEL';
   TSF_ENTITY_sheet                 VARCHAR2(255)         := 'TSF_ENTITY';
   TSF_ENTITY$Action                NUMBER                :=1;
   TSF_ENTITY$SECONDARY_DESC        NUMBER                :=4;
   TSF_ENTITY$TSF_ENTITY_ID         NUMBER                :=2;
   TSF_ENTITY$TSF_ENTITY_DESC       NUMBER                :=3;
   TEO_sheet                        VARCHAR2(255)         := 'TEO';
   TEO$Action                       NUMBER                :=1;
   TEO$TSF_ENTITY_ID                NUMBER                :=2;
   TEO$ORG_UNIT_ID                  NUMBER                :=3;
   TEO$SET_OF_BOOKS_ID              NUMBER                :=4;
   
   TSF_ENTITY_TL_sheet              VARCHAR2(255)         := 'TSF_ENTITY_TL';
   TSF_ENTITY_TL$Action             NUMBER                :=1;
   TSF_ENTITY_TL$LANG               NUMBER                :=2;
   TSF_ENTITY_TL$TSF_ENTITY_ID      NUMBER                :=3;
   TSF_ENTITY_TL$TSF_ENTITY_DESC    NUMBER                :=4;
  
   Type TSF_ENTITY_rec_tab IS TABLE OF TSF_ENTITY%ROWTYPE;
   Type TEO_rec_tab IS TABLE OF TSF_ENTITY_ORG_UNIT_SOB%ROWTYPE;
   sheet_name_trans   S9T_PKG.TRANS_MAP_TYP;
   action_column                    VARCHAR2(255)         := 'ACTION';
 --------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name      IN       VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------
END CORESVC_TSF_ENTITY;
/