
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SUB_ITEM_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------
-- Function Name: GET_REPL_ITEM
-- Purpose      : This function will return the Main Item (if any) associated on
--                repl_item_loc with the passed-in Primary Replenishment Pack.
--------------------------------------------------------------------------------------------
FUNCTION GET_REPL_ITEM(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_repl_item_exists IN OUT  BOOLEAN,
                       O_repl_item        IN OUT  REPL_ITEM_LOC.ITEM%TYPE,
                       I_repl_pack        IN      REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: GET_PRIM_REPL_PACK
-- Purpose      : This function will return the Primary Replenishment Pack, if any,
--                associated with the passed-in Item/Location combination.
--------------------------------------------------------------------------------------------
FUNCTION GET_PRIM_REPL_PACK(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_prim_repl_pack  IN OUT  REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE,
                            I_item            IN      REPL_ITEM_LOC.ITEM%TYPE,
                            I_location        IN      REPL_ITEM_LOC.LOCATION%TYPE,
                            I_loc_type        IN      REPL_ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: CHECK_REPL_IND
-- Purpose      : This function will check if any of the passed-in Warehouses is
--                replenishable.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_REPL_IND(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_replenishable   IN OUT  BOOLEAN,
                        I_wh_group_type   IN      CODE_DETAIL.CODE%TYPE,
                        I_wh_group_value  IN      WH.WH%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: CHECK_MAIN_CONFLICT
-- Purpose      : This function will accept an Item, a Location and an indicator which will
--                determine whether the passed-in Item is intended to be a 'M'ain (Replenishment)
--                Item or a 'S'ubstitute Item.
--                For Main Items, the function will determine whether the Item already exists
--                as a Sub. Item at the passed-in Location.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_MAIN_CONFLICT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists         IN OUT  BOOLEAN,
                             I_item           IN      SUB_ITEMS_HEAD.ITEM%TYPE,
                             I_item_ind       IN      VARCHAR2,
                             I_location       IN      SUB_ITEMS_HEAD.LOCATION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: CHECK__SUB_CONFLICT
-- Purpose      : This function will accept an Item, a Location and an indicator which will
--                determine whether the passed-in Item is intended to be a 'M'ain (Replenishment)
--                Item or a 'S'ubstitute Item.
--                For Main Items, the function will determine whether the Item already exists
--                as a Sub. Item at the passed-in Location.
--                For Sub. Items, the function will determine whether the Item already exists
--                as a Replenisbable Item, at the passed-in Location.
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_SUB_CONFLICT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists         IN OUT  BOOLEAN,
                            I_item           IN      SUB_ITEMS_HEAD.ITEM%TYPE,
                            I_item_ind       IN      VARCHAR2,
                            I_location       IN      SUB_ITEMS_HEAD.LOCATION%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_EMPTY_SUB_HEAD
-- Purpose      : This function will check for Sub. Item Head records without
--                corresponding Detail records.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_EMPTY_SUB_HEAD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists         IN OUT  BOOLEAN,
                              I_item           IN      SUB_ITEMS_HEAD.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: DELETE_EMPTY_SUB_HEAD
-- Purpose      : This function will delete any Sub. Item Head record without
--                corresponding Detail records.
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_EMPTY_SUB_HEAD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item           IN      SUB_ITEMS_HEAD.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: DELETE_MC_REJECTIONS
-- Purpose      : This function will delete all MC Rejections records for the current user
--                and the specified Change Type (wich corresponds to Code Type 'MCTP').
--------------------------------------------------------------------------------------------
FUNCTION DELETE_MC_REJECTIONS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_change_type    IN      CODE_DETAIL.CODE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: EDIT_SUB_ITEM_LOC
-- Purpose      : This function will perform all Sub. Item application functionality for the
--                SI form.  It's processing will be based on the passed-in Action Type:
--                   Apply Main('AM') - SIH record will be generated for
--                                      all specified locations that are valid for the Item;
--                                      MC_REJECTIONS records will be written for all other
--                                      specified locations;
--                   Apply Sub.('AS') - SID record will be generated for
--                                      all specified locations that are valid for the Item;
--                                      MC_REJECTIONS records will be written for all other
--                                      specified locations;
--                   Delete SI ('DS') - SID records will be deleted for the passed-in
--                                      Sub. Item/Main Item combination, for every passed-in
--                                      Location
--                   Delete MI ('DM') - SID and SIH records will be deleted for the passed-in
--                                      Sub. Item/Main Item combination, for every passed-in
--                                      Location
--
--                The O_rejected_records parameter will indicate whether any MC Rejection
--                records have been written during the application process.
--------------------------------------------------------------------------------------------
FUNCTION EDIT_SUB_ITEM_LOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_rejected_records  IN OUT  BOOLEAN,
                           I_main_item         IN      SUB_ITEMS_HEAD.ITEM%TYPE,
                           I_loc_group_type    IN      CODE_DETAIL.CODE%TYPE,
                           I_loc_group_value   IN      VARCHAR2,
                           I_fill_priority     IN      SUB_ITEMS_HEAD.FILL_PRIORITY%TYPE,
                           I_use_sales_ind     IN      SUB_ITEMS_HEAD.USE_SALES_IND%TYPE,
                           I_use_stock_ind     IN      SUB_ITEMS_HEAD.USE_STOCK_IND%TYPE,
                           I_use_forecast_ind  IN      SUB_ITEMS_HEAD.USE_FORECAST_SALES_IND%TYPE,
                           I_sub_item          IN      SUB_ITEMS_DETAIL.SUB_ITEM%TYPE,
                           I_prim_repl_pack    IN      SUB_ITEMS_DETAIL.PRIMARY_REPL_PACK%TYPE,
                           I_pick_priority     IN      SUB_ITEMS_DETAIL.PICK_PRIORITY%TYPE,
                           I_action_type       IN      VARCHAR2,
                           I_AIP_ind           IN      SYSTEM_OPTIONS.AIP_IND%TYPE,  
                           I_start_date        IN      SUB_ITEMS_DETAIL.START_DATE%TYPE,
                           I_end_date          IN      SUB_ITEMS_DETAIL.END_DATE%TYPE,
                           I_substitute_reason IN      SUB_ITEMS_DETAIL.SUBSTITUTE_REASON%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: CHK_DUP_LOC_DATE
-- Purpose      : This function will chack for overlapping of the date for the new substitute item
--                with existing substitute item present in the location.
--------------------------------------------------------------------------------------------
FUNCTION CHK_DUP_LOC_DATE(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_main_item         IN      SUB_ITEMS_HEAD.ITEM%TYPE,
                            I_location          IN      SUB_ITEMS_DETAIL.LOCATION%TYPE,
                            I_start_date        IN      SUB_ITEMS_DETAIL.START_DATE%TYPE,
                            I_end_date          IN      SUB_ITEMS_DETAIL.END_DATE%TYPE)
RETURN BOOLEAN;
END SUB_ITEM_SQL;
/

