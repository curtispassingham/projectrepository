SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE APPOINTMENT_PROCESS_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------
-- define a Table Type structure based upon the Appointment Detail record
 TYPE apptdetail_table IS TABLE OF APPT_DETAIL%ROWTYPE
    INDEX BY BINARY_INTEGER;
----------------------------------------------------------------------------------------------
-- define cache to do bulk inserts/updates into appt_detail

--BULK appt_detail insert tables
TYPE appt_TBL            is table of APPT_DETAIL.appt%TYPE           INDEX BY BINARY_INTEGER;
TYPE loc_TBL             is table of APPT_DETAIL.loc%TYPE            INDEX BY BINARY_INTEGER;
TYPE loc_type_TBL        is table of APPT_DETAIL.loc_type%TYPE       INDEX BY BINARY_INTEGER;
TYPE doc_TBL             is table of APPT_DETAIL.doc%TYPE            INDEX BY BINARY_INTEGER;
TYPE doc_type_TBL        is table of APPT_DETAIL.doc_type%TYPE       INDEX BY BINARY_INTEGER;
TYPE item_TBL            is table of APPT_DETAIL.item%TYPE           INDEX BY BINARY_INTEGER;
TYPE asn_TBL             is table of APPT_DETAIL.asn%TYPE            INDEX BY BINARY_INTEGER;
TYPE qty_appointed_TBL   is table of APPT_DETAIL.qty_appointed%TYPE  INDEX BY BINARY_INTEGER;
TYPE qty_received_TBL    is table of APPT_DETAIL.qty_received%TYPE   INDEX BY BINARY_INTEGER;
TYPE receipt_no_TBL      is table of APPT_DETAIL.receipt_no%TYPE     INDEX BY BINARY_INTEGER;

P_appt               appt_TBL;
P_loc                loc_TBL;
P_loc_type           loc_type_TBL;
P_doc                doc_TBL;
P_doc_type           doc_type_TBL;
P_item               item_TBL;
P_asn                asn_TBL;
P_qty_appointed      qty_appointed_TBL;
P_qty_received       qty_received_TBL;
P_receipt_no         receipt_no_TBL;

P_appt_detail_size       NUMBER := 0;

----------------------------------------------------------------------------------------------
-- Function Name: PROC_APPT_SCH
-- Purpose: processes Scheduled Appointments (Header and Detail)
----------------------------------------------------------------------------------------------
FUNCTION PROC_SCH(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_appt_head      IN     APPT_HEAD%ROWTYPE,
                  I_appt_detail    IN     APPTDETAIL_TABLE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: INIT_APPT_DETAIL
-- Purpose: Initializes variables used to bulk load detail records from appointment create 
--          or detail add messages
FUNCTION INIT_APPT_DETAIL(O_error_message   IN OUT  rtk_errors.rtk_text%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: FLUSH_APPT_DETAIL
-- Purpose: bulk load detail records from appointment create or detail add messages
FUNCTION FLUSH_APPT_DETAIL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: PROC_APPT_SCHDET 
-- Purpose: processes Scheduled Appointments (Detail only)
----------------------------------------------------------------------------------------------
FUNCTION PROC_SCHDET(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_appt_detail_rec  IN     APPT_DETAIL%ROWTYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: PROC_APPT_MODHED
-- Purpose: processes Modified Appointments (Header only)
----------------------------------------------------------------------------------------------
FUNCTION PROC_MODHED(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_appt_head      IN     APPT_HEAD%ROWTYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: PROC_APPT_MODDET
-- Purpose: processes Modified Appointments (Detail only)
----------------------------------------------------------------------------------------------
FUNCTION PROC_MODDET(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_appt_detail_rec  IN     APPT_DETAIL%ROWTYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: PROC_APPT_DEL
-- Purpose: processes Deleted Appointments (Header and Detail)
----------------------------------------------------------------------------------------------
FUNCTION PROC_DEL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_appt_head      IN     APPT_HEAD%ROWTYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: PROC_APPT_DELDET
-- Purpose: processes Deleted Appointments (Detail only)
----------------------------------------------------------------------------------------------
FUNCTION PROC_DELDET(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_appt_detail    IN     APPT_DETAIL%ROWTYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: PROC_APPT_CLS
-- Purpose: processes Closed Appointments
----------------------------------------------------------------------------------------------
FUNCTION PROC_CLS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_appt_head      IN     APPT_HEAD%ROWTYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
END APPOINTMENT_PROCESS_SQL;
/