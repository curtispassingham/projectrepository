CREATE OR REPLACE PACKAGE CORESVC_REPL_ROQ_SQL AUTHID CURRENT_USER AS

   cursor C_DUMMY_ROW is
      select NULL i_curr_order_lead_time,
             NULL i_next_order_lead_time,
             NULL i_days_added_to_colt,
             NULL i_days_added_to_nolt,
             NULL i_review_lead_time,
             i_next_delivery_date,
             NULL i_next_review_date,
             i_supp_lead_time,
             i_pickup_lead_time,
             i_wh_lead_time,
             i_last_delivery_date,
             i_item,
             i_locn,
             i_locn_type,
             i_date,
             i_stock_cat,
             i_repl_method,
             i_review_cycle,
             i_primary_repl_supplier,
             I_sup_delivery_policy,
             i_source_wh,
             i_source_physical_wh,
             i_wh_delivery_policy
        from svc_repl_roq
       where rownum = 1;

   LP_dummy_row   C_DUMMY_ROW%ROWTYPE;

---------------------------------------------------------------------------------------------
-- Function Name:  SETUP_DATA
-- Purpose      :  Retrieved all item-store records on replenishment for "Warehouse-Stocked"
--                 and/or "WH/Cross Linked" category and initially populate the working
--                 table "svc_repl_roq_gtt" that ready for ROQ processing by CONSUME function.
--
--                 It accept Chunk Size and Last run of Day as input parameters and a default
--                 Error Message as its output parameter.
--
--                 This function returns BOOLEAN condition.
---------------------------------------------------------------------------------------------
FUNCTION SETUP_DATA(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_chunk_size        IN       RMS_PLSQL_BATCH_CONFIG.MAX_CHUNK_SIZE%TYPE,
                    I_last_run_of_day   IN       VARCHAR2 DEFAULT 'Y')
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  CONSUME
-- Purpose      :  Calls individual and dependent packages that calculates Dates and Time
--                 and compute for ROQ and other forecast values for an Item replenished at
--                 Store location.
--
--                 It accepts Thread ID as input parameter and a default Error Message as
--                 its output parameter.
--
--                 This function returns BOOLEAN condition.
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_thread_id           IN       NUMBER,
                 I_last_run_of_day     IN       VARCHAR2 DEFAULT 'Y',
				 I_calling_program     IN       VARCHAR2 DEFAULT 'REPLROQ')
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------

END CORESVC_REPL_ROQ_SQL;
/
