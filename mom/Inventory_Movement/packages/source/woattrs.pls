
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE WO_ATTRIB_SQL AUTHID CURRENT_USER AS

------------------------------------------------------------------------
-- Function Name: NEXT_WO_ID
-- Purpose:       This function will return the next work order number in
--                the sequence workord_sequence.
-- Created:       29-Jul-98
------------------------------------------------------------------------
FUNCTION NEXT_WO_ID(O_error_message  IN OUT  VARCHAR2,
                    O_wo_id	     IN OUT  WO_HEAD.WO_ID%TYPE) 
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_MAX_SEQ_NO
-- Purpose:       This function will get the largest sequence number held
--                on the wo_wip table.
-- Created:       29-Jul-98
------------------------------------------------------------------------
FUNCTION GET_MAX_SEQ_NO(O_error_message IN OUT VARCHAR2,
                        O_max_seq_no    IN OUT WO_WIP.SEQ_NO%TYPE,
                        I_wo_id         IN     WO_WIP.WO_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_NEXT_WO
-- Purpose:       This function will get the next work order (after the 
--                passed work order number) to be displayed in the 
--                workord form.
-- Created:       29-Jul-98
------------------------------------------------------------------------
FUNCTION GET_NEXT_WO(O_error_message  IN OUT  VARCHAR2,
                     O_next_wo_id     IN OUT  WO_HEAD.WO_ID%TYPE,
                     O_last_wo_ind    IN OUT  BOOLEAN,
                     I_wo_id          IN      WO_HEAD.WO_ID%TYPE,
                     I_order_no       IN      WO_HEAD.ORDER_NO%TYPE,
                     I_tsfalloc_no    IN      WO_HEAD.TSFALLOC_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_PREV_WO
-- Purpose:       This function will get the previous work order (after  
--                the passed work order number) to be displayed in the 
--                workord form.
-- Created:       29-Jul-98
------------------------------------------------------------------------
FUNCTION GET_PREV_WO(O_error_message  IN OUT  VARCHAR2,
                     O_prev_wo_id     IN OUT  WO_HEAD.WO_ID%TYPE,
                     O_first_wo_ind   IN OUT  BOOLEAN,
                     I_wo_id          IN      WO_HEAD.WO_ID%TYPE,
                     I_order_no       IN      WO_HEAD.ORDER_NO%TYPE,
                     I_tsfalloc_no    IN      WO_HEAD.TSFALLOC_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_MAX_QTY
-- Purpose:       This function will get the maximun quantity for the work  
--                order.
-- Created:       24-Apr-01
------------------------------------------------------------------------
FUNCTION GET_MAX_QTY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_max_qty       IN OUT ORDLOC.QTY_ORDERED%TYPE,
	             I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
	             I_item          IN     ORDLOC.ITEM%TYPE,
	             I_location      IN     ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
END WO_ATTRIB_SQL;
/


