-- File Name : CORESVC_SUPP_DIST_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_SUPP_DIST AUTHID CURRENT_USER AS

   template_key         CONSTANT VARCHAR2(25)          := 'SUPP_DIST_DATA';
   template_category    CONSTANT VARCHAR2(10)          := 'RMSINV';

   action_new                    VARCHAR2(25)          := 'NEW';
   action_mod                    VARCHAR2(25)          := 'MOD';
   action_del                    VARCHAR2(25)          := 'DEL';
   repl_item_loc_supp_dist_sheet VARCHAR2(25)          := 'REPL_ITEM_LOC_SUPP_DIST';
   RILSD$ACTION                  NUMBER                := 1;
   RILSD$DIST_PCT                NUMBER                := 6;
   RILSD$ORIGIN_COUNTRY_ID       NUMBER                := 5;
   RILSD$SUPPLIER                NUMBER                := 4;
   RILSD$LOCATION                NUMBER                := 3;
   RILSD$ITEM                    NUMBER                := 2;

   sheet_name_trans S9T_PKG.TRANS_MAP_TYP;
   action_column                 VARCHAR2(25)          := 'ACTION';

   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;

   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;

   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;

   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;


END CORESVC_SUPP_DIST;
/