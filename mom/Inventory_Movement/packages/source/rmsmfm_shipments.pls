CREATE OR REPLACE PACKAGE RMSMFM_SHIPMENT AUTHID CURRENT_USER AS
---
TYPE shipment_rec IS RECORD (shipment_no                     SHIPMENT.SHIPMENT%TYPE, 
                             order_no                        SHIPMENT.ORDER_NO%TYPE,
                             bol_no                          SHIPMENT.BOL_NO%TYPE,
                             asn                             SHIPMENT.ASN%TYPE,
                             ship_date                       SHIPMENT.SHIP_DATE%TYPE,
                             receive_date                    SHIPMENT.RECEIVE_DATE%TYPE,
                             est_arr_date                    SHIPMENT.EST_ARR_DATE%TYPE,
                             ship_origin                     SHIPMENT.SHIP_ORIGIN%TYPE,
                             status_code                     SHIPMENT.STATUS_CODE%TYPE,
                             invc_match_status               SHIPMENT.INVC_MATCH_STATUS%TYPE,
                             invc_match_date                 SHIPMENT.INVC_MATCH_DATE%TYPE,
                             to_loc                          SHIPMENT.TO_LOC%TYPE,
                             to_loc_type                     SHIPMENT.TO_LOC_TYPE%TYPE,
                             to_store_type                   STORE.STORE_TYPE%TYPE,
                             to_stockholding_ind             STORE.STOCKHOLDING_IND%TYPE,
                             from_loc                        SHIPMENT.FROM_LOC%TYPE,
                             from_loc_type                   SHIPMENT.FROM_LOC_TYPE%TYPE,
                             from_store_type                 STORE.STORE_TYPE%TYPE,
                             from_stockholding_ind           STORE.STOCKHOLDING_IND%TYPE,
                             courier                         SHIPMENT.COURIER%TYPE,
                             no_boxes                        SHIPMENT.NO_BOXES%TYPE,
                             ext_ref_no_in                   SHIPMENT.EXT_REF_NO_IN%TYPE,
                             ext_ref_no_out                  SHIPMENT.EXT_REF_NO_OUT%TYPE,
                             comments                        SHIPMENT.COMMENTS%TYPE,
                             parent_shipment                 SHIPMENT.PARENT_SHIPMENT%TYPE,
                             bill_to_loc                     SHIPMENT.BILL_TO_LOC%TYPE,
                             bill_to_loc_type                SHIPMENT.BILL_TO_LOC_TYPE%TYPE,
                             ref_doc_no                      SHIPMENT.REF_DOC_NO%TYPE
                            );

---
FAMILY	    VARCHAR2(64)   := 'asnout';
---
LP_cre_type	  VARCHAR2(15) := 'asnoutcre';
-------------------------------------------------------------------------------
-- This function adds the shipment to the SHIPMENT_PUB_INFO table
-- in 'U'npublished status. It stages the shipment for publishing to the RIB.
-------------------------------------------------------------------------------
FUNCTION ADDTOQ (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
		 I_message_type    IN	    SHIPMENT_PUB_INFO.MESSAGE_TYPE%TYPE,
		 I_shipment	   IN	    SHIPMENT.SHIPMENT%TYPE,
		 I_to_loc	   IN	    SHIPMENT.TO_LOC%TYPE,
		 I_to_loc_type	   IN	    SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- This procedure is called from the RIB to get the next unpublished shipment number
-- on SHIPMENT_PUB_INFO table for publishing. Based on the shipment number on
-- SHIPMENT_PUB_INFO, it calls the PROCESS_QUEUE_RECORD procedure.
-------------------------------------------------------------------------------
PROCEDURE GETNXT (O_status_code     IN OUT   VARCHAR2,
		  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
		  O_message_type    IN OUT   VARCHAR2,
		  O_message	    IN OUT   RIB_OBJECT,
		  O_bus_obj_id	    IN OUT   RIB_BUSOBJID_TBL,
		  O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
		  I_num_threads     IN	     NUMBER DEFAULT 1,
		  I_thread_val	    IN	     NUMBER DEFAULT 1);

-------------------------------------------------------------------------------
-- This function re-processes the records in the SHIPMENT_PUB_INFO table
-- with pub_status equal to 'H'.
-------------------------------------------------------------------------------
PROCEDURE PUB_RETRY (O_status_code     IN OUT	VARCHAR2,
		     O_error_message   IN OUT	RTK_ERRORS.RTK_TEXT%TYPE,
		     O_message_type    IN OUT	VARCHAR2,
		     O_message	       IN OUT	RIB_OBJECT,
		     O_bus_obj_id      IN OUT	RIB_BUSOBJID_TBL,
		     O_routing_info    IN OUT	RIB_ROUTINGINFO_TBL,
		     I_ref_object      IN	RIB_OBJECT);

-------------------------------------------------------------------------------

END RMSMFM_SHIPMENT;
/
