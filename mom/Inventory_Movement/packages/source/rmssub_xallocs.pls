
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XALLOC AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   -- PACKAGE VARIABLES
----------------------------------------------------------------------------
   LP_cre_type       VARCHAR2(15) := 'xalloccre';
   LP_mod_type       VARCHAR2(15) := 'xallocmod';
   LP_del_type       VARCHAR2(15) := 'xallocdel';
   LP_dtl_cre_type   VARCHAR2(15) := 'xallocdtlcre';
   LP_dtl_mod_type   VARCHAR2(15) := 'xallocdtlmod';
   LP_dtl_del_type   VARCHAR2(15) := 'xallocdtldel';
----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_XALLOC;
/
