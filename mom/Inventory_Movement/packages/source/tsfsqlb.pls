create or replace PACKAGE BODY TRANSFER_SQL AS

   LP_sdate                 PERIOD.VDATE%TYPE   := SYSDATE;
   LP_vdate                 PERIOD.VDATE%TYPE   := GET_VDATE;
   LP_user                  VARCHAR2(50)        := GET_USER;
   LP_table                 VARCHAR2(30);
   LP_exception_id          NUMBER(1);
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked, -54);
   LP_tsf_force_close_ind   SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND%TYPE := NULL;

   cursor C_LOCK_TSFHEAD_CFA_EXT (CV_tsf_no TSFHEAD.TSF_NO%TYPE) is
      select 'x'
        from tsfhead_cfa_ext
       where tsf_no   = CV_tsf_no
         for update nowait;

   cursor C_LOCK_TSFHEAD (CV_tsf_no TSFHEAD.TSF_NO%TYPE) is
      select 'x'
        from tsfhead
       where tsf_no   = CV_tsf_no
         for update nowait;

   cursor C_LOCK_TSFDETAIL (CV_tsf_no TSFHEAD.TSF_NO%TYPE) is
      select 'x'
        from tsfdetail
       where tsf_no   = CV_tsf_no
         for update nowait;
------------------------------------------------------------------------------------------
FUNCTION RESERVE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_item            IN OUT   ITEM_MASTER.ITEM%TYPE,
                 O_inv_status      IN OUT   TSFDETAIL.INV_STATUS%TYPE,
                 O_approve_flag    IN OUT   VARCHAR2,
                 O_available       IN OUT   NUMBER,
                 I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                 I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                 I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE)

RETURN BOOLEAN IS

   L_program    VARCHAR2(50)   := 'TRANSFER_SQL.RESERVE';
   QUICK_EXIT   EXCEPTION;
   ABORTING     EXCEPTION;

   cursor C_TRANSFER_DETAILS is
     select item,
            NVL(tsf_qty, 0) tsf_qty,
            GREATEST(NVL(ship_qty,0),
                     NVL(distro_qty,0),
                     NVL(selected_qty,0)) shipped_qty,
            inv_status
       from tsfdetail
      where tsf_no = I_tsf_no;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   FOR REC in C_TRANSFER_DETAILS LOOP
      if TRANSFER_SQL.QUANTITY_AVAIL(O_error_message,
                                     O_available,
                                     rec.item,
                                     rec.inv_status,
                                     I_from_loc_type,
                                     I_from_loc) = FALSE then
         return FALSE;
      end if;

      if O_available < 0 then
         O_available := 0;
      end if;
      if rec.inv_status is NOT NULL then
         if O_available < (rec.tsf_qty - rec.shipped_qty) then
            O_item              := rec.item;
            O_inv_status   := rec.inv_status;
            O_approve_flag      := 'U';
            raise QUICK_EXIT;
         end if;
      else
         if O_available < (rec.tsf_qty - rec.shipped_qty) then
            O_item         := rec.item;
            O_approve_flag := 'N';
            raise QUICK_EXIT;
         end if;
      end if;
   END LOOP;

   O_approve_flag := 'Y';
   return TRUE;

EXCEPTION
   when QUICK_EXIT then
      return TRUE;
   when ABORTING then
      return FALSE;
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END RESERVE;
------------------------------------------------------------------------------------------
FUNCTION QUANTITY_AVAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_available       IN OUT   NUMBER,
                        I_item            IN       ITEM_MASTER.ITEM%TYPE,
                        I_inv_status      IN       TSFDETAIL.INV_STATUS%TYPE,
                        I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                        I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)                    := 'TRANSFER_SQL.QUANTITY_AVAIL';
   L_tsf_qty            TSFDETAIL.TSF_QTY%TYPE          :=0;
   L_unavailable        INV_STATUS_QTY.QTY%TYPE         :=0;
   L_available          INV_STATUS_QTY.QTY%TYPE         :=0;
   L_specific_unavail   INV_STATUS_QTY.QTY%TYPE         :=0;
   L_item               INV_STATUS_QTY.ITEM%TYPE;
   L_inv_status         TSFDETAIL.INV_STATUS%TYPE;

   cursor C_GET_UNAVAILABLE is
      select qty
        from inv_status_qty
       where item       = I_item
         and loc_type   = I_from_loc_type
         and location   = I_from_loc
         and inv_status = I_inv_status;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   -- The inv_status is NOT NULL then the transfer is a 'NS' transfer. It will fall
   -- into the first part of the condition and O_available will be set to the quantity
   -- of unavailable (damaged, unassembled, etc.) inventory for the item/loc/inv_status
   -- combination minus any outstanding transfers and RTVs for unavailable inventory. The
   -- reason the cursor C_UNAVAIL_RTV_QTY were written is the unavailable inventory
   -- doesn't have separate buckets that can be decremented. The unavailable bucket is
   -- decremented when a transfer is approved or RTV is shipped. If inv_status is
   -- NULL then O_available is set by ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL to
   -- the stock on hand minus outstanding RTVs, transfers and any unavailable inventory.

   if I_inv_status is NOT NULL then
      open  C_GET_UNAVAILABLE;
      fetch C_GET_UNAVAILABLE into L_unavailable;
      close C_GET_UNAVAILABLE;

      O_available := L_unavailable;

   else
      if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(O_error_message,
                                                    L_available,
                                                    I_item,
                                                    I_from_loc,
                                                    I_from_loc_type) = FALSE then
         return FALSE;
      end if;

      O_available := L_available;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;
END QUANTITY_AVAIL;
------------------------------------------------------------------------------------------
FUNCTION QUANTITY_BACKORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_backorder_qty   IN OUT   ITEM_LOC_SOH.CUSTOMER_BACKORDER%TYPE,
                            I_item            IN       ITEM_LOC_SOH.ITEM%TYPE,
                            I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE,
                            I_to_loc          IN       ITEM_LOC_SOH.LOC%TYPE,
                            I_to_loc_type     IN       ITEM_LOC_SOH.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64)   := 'TRANSFER_SQL.QUANTITY_BACKORDER';
   L_child_item   ITEM_MASTER.ITEM%TYPE;
   L_dummy        ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;

   cursor C_GET_CHILD_ITEM is
      select item
        from v_item_master
       where (item_parent = I_item or
              item_grandparent = I_item)
         and (diff_1 = I_diff_id or
              diff_2 = I_diff_id or
              diff_3 = I_diff_id or
              diff_4 = I_diff_id)
         and rownum = 1;

BEGIN

   -- Validate required inputs
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   -- Get the item number associated with this parent item/diff combo
   if I_diff_id is NOT NULL then
      open C_GET_CHILD_ITEM;
      fetch C_GET_CHILD_ITEM into L_child_item;
      close C_GET_CHILD_ITEM;

      if L_child_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_DIFF_COMBO',
                                               L_program,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   -- Get the customer backorder quantity for the item/loc combination
   if ITEMLOC_QUANTITY_SQL.GET_ITEM_LOC_QTYS(O_error_message,
                                             L_dummy,          --O_stock_on_hand
                                             L_dummy,          --O_pack_comp_soh
                                             L_dummy,          --O_in_transit_qty
                                             L_dummy,          --O_pack_comp_intran
                                             L_dummy,          --O_tsf_reserved_qty
                                             L_dummy,          --O_pack_comp_resv
                                             L_dummy,          --O_tsf_expected_qty
                                             L_dummy,          --O_pack_comp_exp
                                             L_dummy,          --O_rtv_qty
                                             L_dummy,          --O_non_sellable_qty
                                             L_dummy,          --O_customer_resv
                                             O_backorder_qty,  --O_customer_backorder
                                             L_dummy,          --O_pack_comp_cust_resv
                                             L_dummy,          --O_pack_comp_cust_back
                                             NVL(L_child_item,I_item),
                                             I_to_loc,
                                             I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END QUANTITY_BACKORDER;
------------------------------------------------------------------------------------------
FUNCTION GET_CONV_INFOR(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_conv_factor      IN OUT   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        O_supp_pack_size   IN OUT   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        I_item             IN       TSFDETAIL.ITEM%TYPE,
                        I_uot              IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.GET_CONV_INFOR';
   L_ti        ITEM_SUPP_COUNTRY.TI%TYPE;
   L_hi        ITEM_SUPP_COUNTRY.HI%TYPE;

   cursor C_SUPP is
      select supp_pack_size,
             ti,
             hi
        from item_supp_country
       where item                = I_item
         and primary_supp_ind    = 'Y'
         and primary_country_ind = 'Y';

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_SUPP',
                    'supp_pack_size',
                    'item: '||(I_item));
   open C_SUPP;

   SQL_LIB.SET_MARK('FETCH',
                    'C_SUPP',
                    'supp_pack_size',
                    'item: '||(I_item));
   fetch C_SUPP into O_supp_pack_size,
                     L_ti,
                     L_hi;
   if C_SUPP%NOTFOUND then
      O_supp_pack_size := 1;
      O_conv_factor := 1;
      close C_SUPP;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUPP',
                    'supp_pack_size',
                    'item: '||(I_item));
   close C_SUPP;
   ---
   if I_uot = 'P' then
      O_conv_factor := L_ti * L_hi * O_supp_pack_size;
   elsif I_uot = 'C' then
      O_conv_factor := O_supp_pack_size;
   else
      O_conv_factor := 1;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;

END GET_CONV_INFOR;
--------------------------------------------------------------------
FUNCTION CHECK_CLEARANCE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item            IN       TSFDETAIL.ITEM%TYPE,
                          I_itemlist        IN       SKULIST_DETAIL.SKULIST%TYPE,
                          I_item_parent     IN       ITEM_MASTER.ITEM%TYPE,
                          I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE,
                          I_location        IN       TSFHEAD.TO_LOC%TYPE,
                          O_clearance       IN OUT   ITEM_LOC.CLEAR_IND%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.CHECK_CLEARANCE';
   L_item      TSFDETAIL.ITEM%TYPE;

   cursor C_CLEAR_IND is
      select clear_ind
        from item_loc
       where item = L_item
         and loc  = I_location;

   cursor C_SKULIST_DETAIL is
      select item
        from skulist_detail
        where skulist = I_itemlist;

   cursor C_ITEMS is
      select item
        from item_master im
       where (im.item_parent = I_item_parent
              or im.item_grandparent = I_item_parent)
         and (NVL(im.diff_1,-1) = NVL(I_diff_id, NVL(im.diff_1,-1))
              or  NVL(im.diff_2,-1) = NVL(I_diff_id, NVL(im.diff_2,-1))
              or  NVL(im.diff_3,-1) = NVL(I_diff_id, NVL(im.diff_3,-1))
              or  NVL(im.diff_4,-1) = NVL(I_diff_id, NVL(im.diff_4,-1)));

BEGIN
   if I_item is NOT NULL then
      L_item := I_item;
      open C_CLEAR_IND;
      fetch C_CLEAR_IND into O_clearance;
      if C_CLEAR_IND%NOTFOUND then
         O_clearance := 'N';
      end if;
      close C_CLEAR_IND;
   elsif I_itemlist is NOT NULL then
      FOR C_rec in C_SKULIST_DETAIL LOOP
         L_item := C_rec.item;
         ---
         open C_CLEAR_IND;
         fetch C_CLEAR_IND into O_clearance;
         if C_CLEAR_IND%NOTFOUND then
            O_clearance := 'N';
         end if;
         close C_CLEAR_IND;
         ---
         if O_clearance = 'Y' then
            return TRUE;
         end if;
      END LOOP;
   elsif I_item_parent is NOT NULL then
      FOR C_rec in C_ITEMS LOOP
         L_item := C_rec.item;
         ---
         open C_CLEAR_IND;
         fetch C_CLEAR_IND into O_clearance;
         if C_CLEAR_IND%NOTFOUND then
            O_clearance := 'N';
         end if;
         close C_CLEAR_IND;
         ---
         if O_clearance = 'Y' then
            return TRUE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CLEARANCE;
-----------------------------------------------------------------------------------------
-- Internal Function called by UPD_ITEM_RESV_EXP
-----------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_RESV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                       I_from_loc        IN       TSFHEAD.TO_LOC%TYPE,
                       I_from_loc_type   IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                       I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(64)   := 'TRANSFER_SQL.UPD_ITEM_RESV';
   L_pack_ind   ITEM_MASTER.PACK_IND%TYPE;

   cursor C_LOCK_ITEM_LOC_SOH is
      select 'x'
        from item_loc_soh
       where loc      = I_from_loc
         and loc_type = I_from_loc_type
         and item     = I_item
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'ITEM_LOC_SOH',
                    'ITEM: '||(I_item)||' Location: '||to_char(I_from_loc));
   LP_table := 'item_loc_soh';
   open C_LOCK_ITEM_LOC_SOH;
   close C_LOCK_ITEM_LOC_SOH;

   if I_from_loc_type = 'S' then
      update item_loc_soh
         set tsf_reserved_qty     = tsf_reserved_qty + I_tsf_qty,
             last_update_datetime = LP_sdate,
             last_update_id       = LP_user
       where loc      = I_from_loc
         and loc_type = I_from_loc_type
         and item     = I_item;
   else
      update item_loc_soh
         set tsf_reserved_qty     = decode(I_pack_ind,'Y',
                                           tsf_reserved_qty,
                                           tsf_reserved_qty + I_tsf_qty),
             pack_comp_resv       = decode(I_pack_ind,'Y',
                                           pack_comp_resv + I_tsf_qty,
                                           pack_comp_resv),
             last_update_datetime = LP_sdate,
             last_update_id       = LP_user
       where loc      = I_from_loc
         and loc_type = I_from_loc_type
         and item     = I_item;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_from_loc),
                                            (I_item));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPD_ITEM_RESV;
-----------------------------------------------------------------------------------------
-- Internal Function called by UPD_ITEM_RESV_EXP
-----------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_EXP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_item              IN       ITEM_MASTER.ITEM%TYPE,
                      I_tsf_type          IN       TSFHEAD.TSF_TYPE%TYPE,
                      I_receive_as_type   IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                      I_to_loc            IN       TSFHEAD.TO_LOC%TYPE,
                      I_to_loc_type       IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                      I_tsf_qty           IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.UPD_ITEM_EXP';
   L_stockholding_ind   STORE.STOCKHOLDING_IND%TYPE := NULL;

   cursor C_LOCK_ITEM_LOC_SOH is
      select 'x'
        from item_loc_soh
       where loc      = I_to_loc
         and loc_type = I_to_loc_type
         and item     = I_item
         for update nowait;
   cursor C_STOCKHOLDING_IND is
        select stockholding_ind
          from store
         where store =  I_to_loc;

 BEGIN

     open C_STOCKHOLDING_IND;
    fetch C_STOCKHOLDING_IND into L_stockholding_ind;
    close C_STOCKHOLDING_IND;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'ITEM_LOC_SOH',
                    'ITEM: '||(I_item)||' Location: '||to_char(I_to_loc));
   LP_table := 'item_loc_soh';
   open C_LOCK_ITEM_LOC_SOH;
   close C_LOCK_ITEM_LOC_SOH;

   if I_to_loc_type = 'S' then
      if L_stockholding_ind <> 'N' then
         update item_loc_soh
            set tsf_expected_qty     = decode(I_tsf_type, 'PL',
                                              tsf_expected_qty,
                                              tsf_expected_qty + I_tsf_qty),
                last_update_datetime = LP_sdate,
                last_update_id       = LP_user
          where loc  = I_to_loc
            and item = I_item;
      end if;
   else
      update item_loc_soh
         set tsf_expected_qty     = decode(I_receive_as_type, 'P',
                                           tsf_expected_qty,
                                           tsf_expected_qty + I_tsf_qty),
             pack_comp_exp        = decode(I_receive_as_type, 'P',
                                           pack_comp_exp + I_tsf_qty,
                                           pack_comp_exp),
             last_update_datetime = LP_sdate,
             last_update_id       = LP_user
       where loc  = I_to_loc
         and item = I_item;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then

     if C_STOCKHOLDING_IND%ISOPEN then
         close C_STOCKHOLDING_IND;
      end if;

     O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_to_loc),
                                            (I_item));
      return FALSE;

   when OTHERS then

      if C_STOCKHOLDING_IND%ISOPEN then
         close C_STOCKHOLDING_IND;
      end if;

     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPD_ITEM_EXP;
-----------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_RESV_EXP( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item            IN       TSFDETAIL.ITEM%TYPE,
                            I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE,
                            I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE,
                            I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                            I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                            I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                            I_to_loc          IN       TSFHEAD.TO_LOC%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64)                    := 'TRANSFER_SQL.UPD_ITEM_RESV_EXP';
   L_item_qty           TSFDETAIL.TSF_QTY%TYPE;
   L_location           ITEM_LOC.LOC%TYPE;
   L_receive_as_type    ITEM_LOC.RECEIVE_AS_TYPE%TYPE   := 'E';
   L_pack_ind           ITEM_MASTER.PACK_IND%TYPE;
   L_inventory_ind      ITEM_MASTER.INVENTORY_IND%TYPE  := 'Y';
   L_simple_pack_ind    ITEM_MASTER.SIMPLE_PACK_IND%TYPE := NULL;
   L_catch_weight_ind   ITEM_MASTER.CATCH_WEIGHT_IND%TYPE := NULL;

   cursor C_ITEMS_IN_PACK is
      select v.item,
             v.qty
        from v_packsku_qty v,
             item_master im
       where v.pack_no        = I_item
         and im.item          = v.item
         and im.inventory_ind = L_inventory_ind;

   cursor C_LOCK_FROM_ITEM_LOC_SOH is
      select 'x'
        from item_loc_soh
       where loc      = I_from_loc
         and loc_type = 'W'
         and item     = I_item
         for update nowait;

   cursor C_LOCK_TO_ITEM_LOC_SOH is
      select 'x'
        from item_loc_soh
       where loc      = I_to_loc
         and loc_type = 'W'
         and item     = I_item
         for update nowait;

   cursor C_ITEM_MASTER is
      select pack_ind,
             simple_pack_ind,
             catch_weight_ind
        from item_master
       where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_MASTER',
                    'ITEM_MASTER',
                    'ITEM: '||(I_item));
   open C_ITEM_MASTER;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_MASTER',
                    'ITEM_MASTER',
                    'ITEM: '||(I_item));
   fetch C_ITEM_MASTER into L_pack_ind,
                            L_simple_pack_ind,
                            L_catch_weight_ind;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_MASTER',
                    'ITEM_MASTER',
                    'ITEM: '||(I_item));
   close C_ITEM_MASTER;
   ---
   if L_pack_ind = 'N' then
      if UPD_ITEM_RESV(O_error_message,
                       I_item,
                       L_pack_ind,
                       I_from_loc,
                       I_from_loc_type,
                       I_tsf_qty) = FALSE then
         return FALSE;
      end if;
      ---
      if UPD_ITEM_EXP(O_error_message,
                      I_item,
                      I_tsf_type,
                      L_receive_as_type,
                      I_to_loc,
                      I_to_loc_type,
                      I_tsf_qty) = FALSE then
         return FALSE;
      end if;
   elsif L_pack_ind = 'Y' then
      LP_table   := 'item_loc_soh';
      L_location := I_from_loc;
      open  C_LOCK_FROM_ITEM_LOC_SOH;
      close C_LOCK_FROM_ITEM_LOC_SOH;
      ---
      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ITEM_LOC_SOH',
                       'ITEM: '||(I_item)||' LOCATION:'||I_from_loc);
      update item_loc_soh
         set tsf_reserved_qty     = tsf_reserved_qty + I_tsf_qty,
             last_update_datetime = LP_sdate,
             last_update_id       = LP_user
       where loc = I_from_loc
         and loc_type = 'W'
         and item = I_item;
      ---
      if I_to_loc_type = 'W' then
         if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                   L_receive_as_type,
                                                   I_item,
                                                   I_to_loc) = FALSE then
            return FALSE;
         end if;

         if L_receive_as_type = 'P' then
            SQL_LIB.SET_MARK('UPDATE',
                             NULL,
                             'ITEM_LOC_SOH',
                             'ITEM: '||(I_item)||'LOCATION: '||I_to_loc);
            LP_table := 'item_loc_soh';
            L_location := I_to_loc;
            open  C_LOCK_TO_ITEM_LOC_SOH;
            close C_LOCK_TO_ITEM_LOC_SOH;
             update item_loc_soh
               set tsf_expected_qty     = tsf_expected_qty + I_tsf_qty,
                   last_update_datetime = LP_sdate,
                   last_update_id       = LP_user
             where loc      = I_to_loc
               and loc_type = 'W'
               and item     = I_item;
         end if;
      end if;
      ---
      FOR rec in C_ITEMS_in_PACK LOOP
         if L_simple_pack_ind = 'Y' and
            L_catch_weight_ind = 'Y' then
            -- calculate update qty
            if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                                     L_item_qty,
                                                     rec.item,  -- component item
                                                     rec.qty,   -- component qty
                                                     NULL,
                                                     NULL,
                                                     I_item,  -- pack item
                                                     I_from_loc,
                                                     I_from_loc_type,
                                                     I_tsf_qty) = FALSE then
               return FALSE;
            end if;
         else
            L_item_qty := I_tsf_qty * rec.qty;
         end if;

         if UPD_ITEM_RESV(O_error_message,
                          rec.item,
                          L_pack_ind,
                          I_from_loc,
                          I_from_loc_type,
                          L_item_qty) = FALSE then
            return FALSE;
         end if;
         if UPD_ITEM_EXP(O_error_message,
                         rec.item,
                         I_tsf_type,
                         L_receive_as_type,
                         I_to_loc,
                         I_to_loc_type,
                         L_item_qty) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(L_location),
                                            (I_item));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPD_ITEM_RESV_EXP;
-----------------------------------------------------------------------------------------
FUNCTION UPD_TSF_RESV_EXP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_no            IN       TSFDETAIL.TSF_NO%TYPE,
                          I_add_delete_ind    IN       VARCHAR2,
                          I_appr_second_leg   IN       BOOLEAN)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.UPD_TSF_RESV_EXP';

   TYPE tsf_no_TABLE   is table of TSFHEAD.TSF_NO%TYPE       index by binary_integer;
   TYPE type_TABLE     is table of TSFHEAD.TSF_TYPE%TYPE     index by binary_integer;
   TYPE loc_TABLE      is table of TSFHEAD.TO_LOC%TYPE       index by binary_integer;
   TYPE loc_type_TABLE is table of TSFHEAD.TO_LOC_TYPE%TYPE  index by binary_integer;
   TYPE item_TABLE     is table of ITEM_MASTER.ITEM%TYPE     index by binary_integer;
   TYPE ind_TABLE      is table of ITEM_MASTER.PACK_IND%TYPE index by binary_integer;
   TYPE qty_TABLE      is table of TSFDETAIL.TSF_QTY%TYPE    index by binary_integer;

   LP_tsf_no tsf_no_TABLE;
   LP_tsf_type type_TABLE;
   LP_tsf_status ind_TABLE;
   LP_from_loc loc_TABLE;
   LP_from_loc_type loc_type_TABLE;
   LP_to_loc loc_TABLE;
   LP_to_loc_type loc_type_TABLE;
   LP_tsf_item item_TABLE;
   LP_pack_ind ind_TABLE;
   LP_tsf_qty qty_TABLE;
   LP_ship_qty qty_TABLE;
   LP_upd_qty qty_TABLE;

   L_tsf_item        ITEM_MASTER.ITEM%TYPE            := NULL;
   L_from_item       ITEM_MASTER.ITEM%TYPE            := NULL;
   L_to_item         ITEM_MASTER.ITEM%TYPE            := NULL;
   L_from_qty        TSF_XFORM_DETAIL.FROM_QTY%TYPE   := NULL;
   L_check_qty       TSFDETAIL.TSF_QTY%TYPE           := NULL;
   L_upd_qty         TSFDETAIL.TSF_QTY%TYPE           := NULL;
   L_packing_check   VARCHAR2(1);

   L_inventory_ind   ITEM_MASTER.INVENTORY_IND%TYPE   := 'Y';

   L_finisher_loc_ind VARCHAR2(1)      := NULL;
   L_finisher_entity_ind VARCHAR2(1)   := NULL;

   cursor C_GET_FIRST_LEG_INFO is
      select th.tsf_no,
             th.tsf_type,
             th.status,
             th.from_loc,
             th.from_loc_type,
             th.to_loc,
             th.to_loc_type,
             td.item,
             im.pack_ind,
             decode(I_add_delete_ind, 'A',td.tsf_qty,'D',td.tsf_qty * -1) tsf_qty,
             td.ship_qty,
             (NVL(td.tsf_qty, 0) - GREATEST(NVL(td.ship_qty, 0),
                                            NVL(td.distro_qty, 0),
                                            NVL(td.selected_qty, 0))) * -1 upd_qty
        from tsfhead th,
             tsfdetail td,
             item_master im
       where th.tsf_no = I_tsf_no
         and th.tsf_no = td.tsf_no
         and td.item   = im.item;

   cursor C_GET_SECOND_LEG_INFO is
      select th.tsf_no,
             th.tsf_type,
             th.status,
             th.from_loc,
             th.from_loc_type,
             th.to_loc,
             th.to_loc_type,
             td.item,
             im.pack_ind,
             decode(I_add_delete_ind, 'A',td.tsf_qty,'D',td.tsf_qty * -1) tsf_qty,
             td.ship_qty
        from tsfhead th,
             tsfdetail td,
             item_master im
       where th.tsf_parent_no = I_tsf_no
         and th.tsf_no        = td.tsf_no
         and td.item          = im.item
       order by tsf_parent_no desc;

   cursor C_ITEMS_IN_PACK is
      select v.item,
             v.qty
        from v_packsku_qty v,
             item_master im
       where v.pack_no = L_tsf_item
         and v.item = im.item
         and im.inventory_ind = L_inventory_ind;

   cursor C_GET_XFORM_DETAIL (cv_item ITEM_MASTER.ITEM%TYPE) is
      select txd.from_item,
             decode(I_add_delete_ind, 'A',txd.from_qty,'D',txd.from_qty * -1) from_qty
        from tsf_xform tx,
             tsf_xform_detail txd
       where tx.tsf_no       = I_tsf_no
         and tx.tsf_xform_id = txd.tsf_xform_id
         and txd.to_item     = cv_item;

   -- the cursor checks if l_item exists as a 'F'rom item on packing_detail.
   cursor C_PACKING_CHECK (l_item ITEM_MASTER.ITEM%TYPE) is
      select 'Y'
        from tsf_packing tp,
             tsf_packing_detail tpd
       where tp.tsf_no         = I_tsf_no
         and tp.tsf_packing_id = tpd.tsf_packing_id
         and tpd.item          = l_item
         and tpd.record_type   = 'F';

BEGIN
   if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                     L_finisher_loc_ind,
                                     L_finisher_entity_ind,
                                     I_tsf_no) = FALSE then
      return FALSE;
   end if;

   if L_finisher_loc_ind = 'E' or
      L_finisher_loc_ind = 'R' or
      L_finisher_loc_ind is NULL then

      -- ----------------------------------------------------------------------
      -- For External finishers, transfers with finishing at the Receiving
      -- location and transfers with no finishing, it is not possible to approve
      -- only the second leg. The user is not able to only modify the second
      -- leg of these transfer types. Therefore, this function only needs to
      -- update the reserved and expected quantities for the entire transfer.
      -- ----------------------------------------------------------------------

      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_FIRST_LEG_INFO',
                       'TSFHEAD',
                       'TSF_NO: '||I_tsf_no);
      open C_GET_FIRST_LEG_INFO;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_FIRST_LEG_INFO',
                       'TSFHEAD',
                       'TSF_NO: '||I_tsf_no);
      fetch C_GET_FIRST_LEG_INFO BULK COLLECT INTO LP_tsf_no,
                                                   LP_tsf_type,
                                                   LP_tsf_status,
                                                   LP_from_loc,
                                                   LP_from_loc_type,
                                                   LP_to_loc,
                                                   LP_to_loc_type,
                                                   LP_tsf_item,
                                                   LP_pack_ind,
                                                   LP_tsf_qty,
                                                   LP_ship_qty,
                                                   LP_upd_qty;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_FIRST_LEG_INFO',
                       'TSFHEAD',
                       'TSF_NO: '||I_tsf_no);
      close C_GET_FIRST_LEG_INFO;

      if LP_tsf_no.COUNT = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      FOR tsf_ctr in LP_tsf_no.FIRST .. LP_tsf_no.LAST LOOP

         -- increment the from-item's reserved quantity at the from-loc --
         -- increment the from-item's expected quantity at the to-loc --

         if I_appr_second_leg = TRUE then
            -- Only update the qty that hasn't been shipped/selected/distro
            L_upd_qty := LP_upd_qty(tsf_ctr);
         else
            L_upd_qty := LP_tsf_qty(tsf_ctr);
         end if;

         if UPD_ITEM_RESV_EXP(O_error_message,
                              LP_tsf_item(tsf_ctr),
                              LP_tsf_type(tsf_ctr),
                              L_upd_qty,
                              LP_from_loc_type(tsf_ctr),
                              LP_from_loc(tsf_ctr),
                              LP_to_loc_type(tsf_ctr),
                              LP_to_loc(tsf_ctr)) = FALSE then
            return FALSE;
         end if;
      END LOOP;

   elsif ( (I_appr_second_leg = FALSE)
       and ( (L_finisher_loc_ind = 'B')
          or (L_finisher_loc_ind = 'S')) ) then

      -- ----------------------------------------------------------------------
      -- Since the Finisher is located at the sending or both locations
      -- The first leg is a book transfer and is automatically completed
      -- Update the expected and reserved quantities for the final destination
      -- ----------------------------------------------------------------------

      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_SECOND_LEG_INFO',
                       'TSFHEAD',
                       'TSF_NO: '||I_tsf_no);
      open C_GET_SECOND_LEG_INFO;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_SECOND_LEG_INFO',
                       'TSFHEAD',
                       'TSF_NO: '||I_tsf_no);
      fetch C_GET_SECOND_LEG_INFO BULK COLLECT INTO LP_tsf_no,
                                                    LP_tsf_type,
                                                    LP_tsf_status,
                                                    LP_from_loc,
                                                    LP_from_loc_type,
                                                    LP_to_loc,
                                                    LP_to_loc_type,
                                                    LP_tsf_item,
                                                    LP_pack_ind,
                                                    LP_tsf_qty,
                                                    LP_ship_qty;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SECOND_LEG_INFO',
                       'TSFHEAD',
                       'TSF_NO: '||I_tsf_no);
      close C_GET_SECOND_LEG_INFO;

      if LP_tsf_no.COUNT = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      FOR tsf_ctr in LP_tsf_no.FIRST .. LP_tsf_no.LAST LOOP
         if LP_pack_ind(tsf_ctr) = 'Y' then

            L_tsf_item := LP_tsf_item(tsf_ctr);
            FOR comp_rec in C_ITEMS_IN_PACK LOOP
               -- Update the Pack Components Expected Quantity
               if UPD_ITEM_EXP(O_error_message,
                               comp_rec.item,
                               LP_tsf_type(tsf_ctr),
                               'E', --L_receive_as_type
                               LP_to_loc(tsf_ctr),
                               LP_to_loc_type(tsf_ctr),
                               (LP_tsf_qty(tsf_ctr) * comp_rec.qty)) = FALSE then
                   return FALSE;
               end if;

               L_from_item := NULL;
               open C_GET_XFORM_DETAIL(comp_rec.item);
               fetch C_GET_XFORM_DETAIL into L_from_item,
                                             L_from_qty;
               close C_GET_XFORM_DETAIL;

               -- increment the from-item's reserved quantity at the finisher
               if L_from_item is NULL then
                  if UPD_ITEM_RESV(O_error_message,
                                   comp_rec.item,
                                   'N',
                                   LP_from_loc(tsf_ctr),
                                   LP_from_loc_type(tsf_ctr),
                                   (LP_tsf_qty(tsf_ctr) * comp_rec.qty)) = FALSE then
                     return FALSE;
                  end if;

               else
                  FOR xform in C_GET_XFORM_DETAIL(comp_rec.item) LOOP
                     if UPD_ITEM_RESV(O_error_message,
                                      xform.from_item,
                                      'N',
                                      LP_from_loc(tsf_ctr),
                                      LP_from_loc_type(tsf_ctr),
                                      (LP_tsf_qty(tsf_ctr) * comp_rec.qty)) = FALSE then -- this quantity must be pro-rated.
                        return FALSE;
                     end if;
                  END LOOP;
               end if;
            END LOOP;

         else
            -- Update expected qty and the to loc for the item
            if UPD_ITEM_EXP(O_error_message,
                            LP_tsf_item(tsf_ctr),
                            LP_tsf_type(tsf_ctr),
                            'E',
                            LP_to_loc(tsf_ctr),
                            LP_to_loc_type(tsf_ctr),
                            LP_tsf_qty(tsf_ctr)) = FALSE then
               return FALSE;
            end if;

            L_from_item := NULL;
            open C_GET_XFORM_DETAIL(LP_tsf_item(tsf_ctr));
            fetch C_GET_XFORM_DETAIL into L_from_item,
                                          L_from_qty;
            close C_GET_XFORM_DETAIL;
            ---

            if L_from_item is NULL then
               if UPD_ITEM_RESV(O_error_message,
                                LP_tsf_item(tsf_ctr),
                                'N',
                                LP_from_loc(tsf_ctr),
                                LP_from_loc_type(tsf_ctr),
                                LP_tsf_qty(tsf_ctr)) = FALSE then
                  return FALSE;
               end if;
            else
               FOR xform in C_GET_XFORM_DETAIL(LP_tsf_item(tsf_ctr)) LOOP

                  L_upd_qty := LP_tsf_qty(tsf_ctr);

                  if UPD_ITEM_RESV(O_error_message,
                                   xform.from_item,
                                   'N',
                                   LP_from_loc(tsf_ctr),
                                   LP_from_loc_type(tsf_ctr),
                                   xform.from_qty) = FALSE then
                     return FALSE;
                   end if;


                   if abs(LP_tsf_qty(tsf_ctr)) > abs(xform.from_qty) then
                      if L_check_qty is NULL then
                         L_check_qty := abs(LP_tsf_qty(tsf_ctr))-abs(xform.from_qty);
                      else
                         L_check_qty := L_check_qty - abs(xform.from_qty);
                      end if;
                   end if;
               END LOOP; -- C_GET_XFORM_DETAIL loop

               -- If the total(LP_tsf_qty(leg2 qty) is greater than the xform.from_qty) the
               -- LP_tsf_item ('to' item) also exists on the tsf as a regular item
               -- (no xform) and a call to upd resv qty for the non-xformed qty is required.
               -- If the xform functionality is enhanced to allow the user to edit the transform
               -- qty (xfrom 5items into 2 items), this code will have to be reassessed.
               if L_check_qty is NOT NULL and
                  L_check_qty > 0  then

                  if I_add_delete_ind = 'D' then
                     L_check_qty := L_check_qty * -1;
                  end if;

                  if UPD_ITEM_RESV(O_error_message,
                                   LP_tsf_item(tsf_ctr),
                                   'N',
                                   LP_from_loc(tsf_ctr),
                                   LP_from_loc_type(tsf_ctr),
                                   L_check_qty) = FALSE then
                      return FALSE;
                   end if;
               end if;
            end if; --L_check_qty
         end if; --LP_pack_ind(tsf_ctr)
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;

END UPD_TSF_RESV_EXP;
---------------------------------------------------------------------------------------
FUNCTION UPD_NON_SELLABLE_QTYS_WHEN_DEL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_tsf_no          IN     TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(255)                 := 'UPD_NON_SELLABLE_QTYS_WHEN_DEL';
   L_table                 VARCHAR2(30);
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);
   L_tran_code             tran_data.tran_code%TYPE       := 25;
   L_found                 BOOLEAN;
   LP_vdate                PERIOD.VDATE%TYPE   := GET_VDATE;
   l_upd_qty               tsfdetail.tsf_qty%TYPE;

   cursor C_GET_ITEMS is
       select th.from_loc_type,th.from_loc,th.to_loc_type,to_loc,th.inventory_type,th.status,
              td.item,td.tsf_qty,td.inv_status
         from tsfhead th,
              tsfdetail td
        where th.tsf_no = I_tsf_no
          and th.tsf_no = td.tsf_no;
BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   FOR rec_item in C_GET_ITEMS LOOP
      if ((rec_item.status='A') and (rec_item.inventory_type != 'A') ) then
         -- ----------------------------------------------------------------
         -- if the tsf inventory type is not 'A' Unavilable, then move the stock
         -- back to the unavailable bucket.
         -- ----------------------------------------------------------------
         if INVADJ_SQL.BUILD_ADJ_UNAVAILABLE(O_error_message,
                                             L_found,
                                             rec_item.item,
                                             rec_item.inv_status,
                                             rec_item.from_loc_type,
                                             rec_item.from_loc,
                                             rec_item.tsf_qty) = FALSE then
            return FALSE;
         end if;
        -- insert a tran_data record (code 25)
         if INVADJ_SQL.BUILD_ADJ_TRAN_DATA(O_error_message,
                                           L_found,
                                           rec_item.item,
                                           rec_item.from_loc_type,
                                           rec_item.from_loc,
                                           rec_item.tsf_qty,
                                           NULL,              -- total_cost
                                           NULL,              -- total_retail
                                           NULL,              -- order_no
                                           L_program,
                                           LP_vdate,
                                           L_tran_code,
                                           NULL,              -- reason
                                           rec_item.inv_status,
                                           NULL,              -- wac
                                           NULL,              -- unit_retail
                                           NULL) = FALSE then -- pack_ind
            return FALSE;
         end if;
      end if;
   END LOOP; --C_GET_ITEMS LOOP
   if BOL_SQL.FLUSH_BOL_PROCESS(O_error_message) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_tsf_no),
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPD_NON_SELLABLE_QTYS_WHEN_DEL;
-----------------------------------------------------------------------------------------
FUNCTION UPD_QTYS_WHEN_CLOSE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_no          IN     tsfdetail.tsf_no%TYPE)
RETURN BOOLEAN IS
   L_pgm_name TRAN_DATA.PGM_NAME%TYPE := 'TRANSFER_SQL.UPD_QTYS_WHEN_CLOSE';
   L_dummy   BOOLEAN;

BEGIN

   if TRANSFER_SQL.UPD_QTYS_WHEN_CLOSE(O_error_message,
                                       L_dummy,
                                       'A',
                                       I_tsf_no) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
   return FALSE;
END UPD_QTYS_WHEN_CLOSE;
-----------------------------------------------------------------------------------------
FUNCTION UPD_QTYS_WHEN_CLOSE(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_shipping_shortage_ind   IN OUT   BOOLEAN,
                             I_process_leg             IN       VARCHAR2,
                             I_tsf_no                  IN       TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program_name       VARCHAR2(50)   := 'TRANSFER_SQL.UPD_QTYS_WHEN_CLOSE';
   L_tsf_type           TSFHEAD.TSF_TYPE%TYPE := NULL;
   L_pgm_name           TRAN_DATA.PGM_NAME%TYPE := 'TRANSFER_SQL.UPD_QTYS_WHEN_CLOSE';
   ---
   TYPE tsf_no_TABLE is table of TSFHEAD.TSF_NO%TYPE index by binary_integer;
   TYPE type_TABLE is table of TSFHEAD.TSF_TYPE%TYPE index by binary_integer;
   TYPE loc_TABLE is table of TSFHEAD.TO_LOC%TYPE index by binary_integer;
   TYPE loc_type_TABLE is table of TSFHEAD.TO_LOC_TYPE%TYPE index by binary_integer;
   TYPE inv_type_TABLE  is table of TSFHEAD.INVENTORY_TYPE%TYPE index by binary_integer;
   TYPE item_TABLE is table of ITEM_MASTER.ITEM%TYPE index by binary_integer;
   TYPE inv_status_TABLE is table of SHIPSKU.INV_STATUS%TYPE index by binary_integer;
   TYPE ind_TABLE is table of ITEM_MASTER.PACK_IND%TYPE index by binary_integer;
   TYPE qty_TABLE is table of TSFDETAIL.TSF_QTY%TYPE index by binary_integer;
   TYPE rat_TABLE is table of ITEM_LOC.RECEIVE_AS_TYPE%TYPE index by binary_integer;

   LP_tsf_no tsf_no_TABLE;
   LP_tsf_parent_no tsf_no_TABLE;
   LP_from_loc loc_TABLE;
   LP_from_loc_type loc_type_TABLE;
   LP_to_loc loc_TABLE;
   LP_to_loc_type loc_type_TABLE;
   LP_tsf_item item_TABLE;
   LP_pack_ind ind_TABLE;
   LP_upd_qty qty_TABLE;
   LP_rat rat_TABLE;
   LP_inv_type      inv_type_TABLE;
   LP_inv_status    inv_status_TABLE;

   LP_system_options   SYSTEM_OPTIONS%ROWTYPE;

   L_rowid                 ROWID;
   L_tsf_item              ITEM_MASTER.ITEM%TYPE;
   L_from_item             ITEM_MASTER.ITEM%TYPE;
   L_pack_no               ITEM_MASTER.ITEM%TYPE;
   L_upd_qty               TSFDETAIL.TSF_QTY%TYPE;
   L_finisher_loc_ind      VARCHAR2(1) := NULL;
   L_finisher_entity_ind   VARCHAR2(1) := NULL;
   L_inventory_ind         ITEM_MASTER.INVENTORY_IND%TYPE := 'Y';
   L_bulk_qty              TSFDETAIL.TSF_QTY%TYPE;
   L_comp_qty              TSFDETAIL.TSF_QTY%TYPE;
   L_transferred_qty       TSFDETAIL.TSF_QTY%TYPE;
   L_leftover_qty          TSFDETAIL.TSF_QTY%TYPE;
   L_leftover_comp_qty     V_PACKSKU_QTY.QTY%TYPE;
   L_found                 BOOLEAN;
   L_vdate                 DATE := GET_VDATE;
   L_tran_code             TRAN_DATA.TRAN_CODE%TYPE := 25;

   cursor C_TSF_FOR_CANCEL_QTY is
       select th.tsf_no,
              th.tsf_parent_no,
              td.item,
              (NVL(td.tsf_qty, 0) - NVL(td.cancelled_qty, 0) - NVL(td.ship_qty, 0)) cancel_qty
         from tsfhead th,
              tsfdetail td
        where th.tsf_no = I_tsf_no
          and td.tsf_no = th.tsf_no
          and (NVL(td.tsf_qty, 0)-NVL(td.cancelled_qty,0)> NVL(td.ship_qty, 0))
       UNION
       select th.tsf_no,
              th.tsf_parent_no,
              td.item,
              (NVL(td.tsf_qty, 0) - NVL(td.cancelled_qty, 0) - NVL(td.ship_qty, 0)) cancel_qty
         from tsfhead th,
              tsfdetail td
        where th.tsf_parent_no = I_tsf_no
          and td.tsf_no = th.tsf_no
          and (NVL(td.tsf_qty, 0)-NVL(td.cancelled_qty,0)> NVL(td.ship_qty, 0));

   cursor C_GET_UNSHIPPED_TSF is
      select th.tsf_no,
             NVL(th.tsf_parent_no, -1) tsf_parent_no,
             th.inventory_type,
             th.from_loc,
             th.from_loc_type,
             th.to_loc,
             th.to_loc_type,
             td.item,
             im.pack_ind,
             (NVL(td.tsf_qty, 0) - NVL(td.ship_qty, 0)) * -1 upd_qty,
             td.inv_status,
             NVL(il.receive_as_type, 'E') receive_as_type
        from tsfhead th,
             tsfdetail td,
             item_master im,
             item_loc il
       where th.tsf_no = I_tsf_no
         and th.status != 'C'
         and td.tsf_no = th.tsf_no
         and td.item = im.item
         and im.item = il.item
         and il.loc = th.to_loc
         and NVL(td.tsf_qty, 0)> NVL(td.ship_qty, 0)
       union
      select th.tsf_no,
             NVL(th.tsf_parent_no, -1) tsf_parent_no,
             th.inventory_type,
             th.from_loc,
             th.from_loc_type,
             th.to_loc,
             th.to_loc_type,
             td.item,
             im.pack_ind,
             (NVL(td.tsf_qty, 0) - NVL(td.ship_qty, 0)) * -1 upd_qty,
             td.inv_status,
             NVL(il.receive_as_type, 'E') receive_as_type
        from tsfhead th,
             tsfdetail td,
             item_master im,
             item_loc il
       where th.tsf_parent_no = I_tsf_no
         and th.status != 'C'
         and td.tsf_no = th.tsf_no
         and td.item = im.item
         and im.item = il.item
         and il.loc = th.to_loc
         and (NVL(td.tsf_qty, 0)> NVL(td.ship_qty, 0))
       order by tsf_parent_no;

   cursor C_GET_EX_UNSHIPPED_QTY is
      select tf.tsf_no,
             NVL(th.tsf_parent_no, -1)   tsf_parent_no,
             NVL(th.inventory_type, 'A') inventory_type,
             tf.from_loc,
             tf.from_loc_type,
             tf.to_loc,
             tf.to_loc_type,
             tf.item,
             im.pack_ind,
             (NVL(tf.tsf_qty, 0) - NVL(tf.shipped_qty, 0)) * -1 upd_qty,
             td.inv_status,
             NVL(il.receive_as_type, 'E') receive_as_type
        from tsfitem_inv_flow tf,
             tsfhead th,
             item_master im,
             item_loc il,
             tsfdetail td
       where tf.tsf_no = I_tsf_no
         and tf.tsf_no = th.tsf_no
         and th.status != 'C'
         and im.item = tf.item
       and td.tsf_no = tf.tsf_no
         and td.item = tf.item
         and td.tsf_seq_no = tf.tsf_seq_no
         and il.item = tf.item
         and il.loc = tf.to_loc
         and NVL(tf.tsf_qty, 0) > NVL(tf.shipped_qty, 0)
      UNION ALL
      select th.tsf_no,
             NVL(th.tsf_parent_no, -1) tsf_parent_no,
             NVL(th.inventory_type, 'A') inventory_type,
             th.from_loc,
             th.from_loc_type,
             th.to_loc,
             th.to_loc_type,
             td.item,
             im.pack_ind,
             (NVL(td.tsf_qty, 0) - NVL(td.ship_qty, 0)) * -1 upd_qty,
             td.inv_status,
             NVL(il.receive_as_type, 'E') receive_as_type
        from tsfhead th,
             tsfdetail td,
             item_master im,
             item_loc il
       where th.tsf_no = I_tsf_no
         and th.tsf_no = td.tsf_no
         and th.from_loc_type = 'S'
         and th.to_loc_type = 'S'
         and th.status != 'C'
         and im.item = td.item
         and il.item = td.item
         and il.loc = th.to_loc
         and NVL(td.tsf_qty, 0) > NVL(td.ship_qty, 0)
       order by tsf_parent_no;

   cursor C_GET_XFORM_DETAIL (cv_item     ITEM_MASTER.ITEM%TYPE,
                              cv_tsf_no   TSFHEAD.TSF_NO%TYPE) is
      select txd.from_item
        from tsf_xform tx,
             tsf_xform_detail txd
       where tx.tsf_no = cv_tsf_no
         and tx.tsf_xform_id = txd.tsf_xform_id
         and txd.to_item = cv_item;

   cursor C_GET_LEFTOVER_QTY(cv_item ITEM_MASTER.ITEM%TYPE,
                             cv_tsf_no   TSFHEAD.TSF_NO%TYPE) is
      select AVG(td.tsf_qty/q.qty)
        from tsf_packing tp,
             tsf_packing_detail tpd,
             tsfhead th,
             tsfdetail td,
             tsf_xform tx,
             tsf_xform_detail txd,
             v_packsku_qty q
       where tp.tsf_no = cv_tsf_no
         and tpd.tsf_packing_id = tp.tsf_packing_id
         and tpd.record_type = 'R'
         and td.tsf_no = th.tsf_no
         and tpd.item = td.item
         and th.tsf_parent_no = tp.tsf_no
         and tx.tsf_no = th.tsf_parent_no
         and tx.tsf_xform_id = txd.tsf_xform_id
         and txd.to_item = td.item
         and q.item = txd.from_item
         and exists (select 'X'
                       from tsf_packing_detail pd
                      where pd.tsf_packing_id = tp.tsf_packing_id
                        and pd.record_type = 'R'
                        and pd.item = cv_item);

   cursor C_GET_BULK_QTY(cv_item ITEM_MASTER.ITEM%TYPE,
                         cv_tsf_no TSFHEAD.TSF_NO%TYPE) is
      select AVG(pd.qty/q.qty)
        from tsf_packing_detail pd,
             tsf_packing ph,
             v_packsku_qty q,
             tsf_xform tx,
             tsf_xform_detail txd
       where ph.tsf_no = cv_tsf_no
         and ph.tsf_packing_id = pd.tsf_packing_id
         and pd.item = txd.to_item
         and q.item = txd.from_item
         and pd.record_type = 'F'
         and tx.tsf_no = ph.tsf_no
         and tx.tsf_xform_id = txd.tsf_xform_id
         and exists (select 'X'
                       from tsf_packing_detail pd1
                      where pd1.tsf_packing_id = pd.tsf_packing_id
                        and pd1.item = cv_item
                        and pd1.record_type = 'R');

   cursor C_CHECK_PACKING(cv_item  ITEM_MASTER.ITEM%TYPE,
                          cv_tsf_no TSFHEAD.TSF_NO%TYPE) is
    select pd.item pack_item,
           q.qty
      from tsf_packing_detail pd,
           tsf_packing ph,
           v_packsku_qty q
     where ph.tsf_no = cv_tsf_no
       and ph.tsf_packing_id = pd.tsf_packing_id
       and pd.item = q.pack_no
       and q.item = cv_item
       and pd.record_type = 'F';

   -- this cursor selects the necessary values for
   -- updating the 2nd leg quantities
   cursor C_GET_UPD_QTY (cv_from_item   ITEM_MASTER.ITEM%TYPE,
                         cv_to_item   ITEM_MASTER.ITEM%TYPE,
                         cv_tsf_no    TSFHEAD.TSF_NO%TYPE) is
      select ( (ABS(NVL(L_leftover_comp_qty, 1) * NVL(a.received_qty, 0) + NVL(L_bulk_qty, 0) - NVL(L_leftover_comp_qty,1) *
              decode(L_leftover_comp_qty, NULL, NVL(L_leftover_qty, 0), a.tsf_qty - NVL(L_leftover_qty, 0)))
           * b.tsf_qty / ( NVL(L_leftover_comp_qty, 1) * a.tsf_qty + NVL(L_bulk_qty, 0) - NVL(L_leftover_comp_qty, 1) *
                           decode(L_leftover_comp_qty, NULL, NVL(L_leftover_qty, 0), a.tsf_qty - NVL(L_leftover_qty, 0))))
           - NVL(b.ship_qty, 0)) * -1 upd_qty
        from tsfdetail a, -- parent
             tsfdetail b  -- child
       where a.tsf_no = cv_tsf_no
         and b.tsf_no = (select tsf_no
                           from tsfhead
                          where tsf_parent_no = cv_tsf_no)
         and a.item = cv_from_item
         and b.item = cv_to_item;

   cursor C_ITEMS_IN_PACK (cv_from_loc TSFHEAD.FROM_LOC%TYPE)  is
      select v.item,
             v.qty,
             ils.av_cost comp_av_cost
        from v_packsku_qty v,
             item_loc_soh ils,
             item_master im
       where v.pack_no = L_tsf_item
         and ils.item  = v.item
         and ils.loc   = cv_from_loc
         and v.item    = im.item
         and im.inventory_ind = L_inventory_ind;

   cursor C_LOCK_TSFDETAIL_ITEM (CV_tsf_no TSFHEAD.TSF_NO%TYPE, CV_item TSFDETAIL.ITEM%TYPE) is
      select 'x'
        from tsfdetail
       where tsf_no   = CV_tsf_no
         and  item     = CV_item
         for update nowait;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;
   ---
   if TRANSFER_SQL.GET_TSF_TYPE(O_error_message,
                                L_tsf_type,
                                I_tsf_no) = FALSE then
      return FALSE;
   end if;
   ---
   if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                     L_finisher_loc_ind,
                                     L_finisher_entity_ind,
                                     I_tsf_no) = FALSE then
      return FALSE;
   end if;
   ---
   if L_tsf_type = 'EG' or (L_tsf_type = 'CO' and LP_system_options.oms_ind = 'Y') then
      open C_GET_EX_UNSHIPPED_QTY;
      fetch C_GET_EX_UNSHIPPED_QTY BULK COLLECT INTO LP_tsf_no,
                                                     LP_tsf_parent_no,
                                                     LP_inv_type,
                                                     LP_from_loc,
                                                     LP_from_loc_type,
                                                     LP_to_loc,
                                                     LP_to_loc_type,
                                                     LP_tsf_item,
                                                     LP_pack_ind,
                                                     LP_upd_qty,
                                                     LP_inv_status,
                                                     LP_rat;
      close C_GET_EX_UNSHIPPED_QTY;
   else
      open C_GET_UNSHIPPED_TSF;
      fetch C_GET_UNSHIPPED_TSF BULK COLLECT INTO LP_tsf_no,
                                                  LP_tsf_parent_no,
                                                  LP_inv_type,
                                                  LP_from_loc,
                                                  LP_from_loc_type,
                                                  LP_to_loc,
                                                  LP_to_loc_type,
                                                  LP_tsf_item,
                                                  LP_pack_ind,
                                                  LP_upd_qty,
                                                  LP_inv_status,
                                                  LP_rat;
      close C_GET_UNSHIPPED_TSF;
   end if;
   ---
   if LP_tsf_no.COUNT = 0 then
      return TRUE;
   end if;
   --

   FOR cntr in LP_tsf_no.FIRST..LP_tsf_no.last LOOP
      if NVL(LP_upd_qty(cntr), 0) < 0 then
         O_shipping_shortage_ind := TRUE;
         -- -------------------------------------------------------------------
         -- First Leg or Single Legged Transfer
         -- -------------------------------------------------------------------
         if (LP_tsf_parent_no(cntr) = -1) and (I_process_leg != '2') then
            -- ----------------------------------------------------------------
            -- Increment the 1st leg item unavailable quantity at the From
            -- location. Do this if Inventory Type` is not 'A' (available)
            -- ----------------------------------------------------------------
            if (NVL(LP_inv_type(cntr), 'A') != 'A') then
               L_transferred_qty := (LP_upd_qty(cntr) * -1);  --incrementing non_sellable_qty/inv_status_qty
               if INVADJ_SQL.ADJ_UNAVAILABLE(LP_tsf_item(cntr),
                                             LP_inv_status(cntr),
                                             LP_from_loc_type(cntr),
                                             LP_from_loc(cntr),
                                             L_transferred_qty,
                                             O_error_message,
                                             L_found) = FALSE then
                  return FALSE;
               end if;
               -- insert a tran_data record (code 25)
               if INVADJ_SQL.ADJ_TRAN_DATA(LP_tsf_item(cntr),
                                           LP_from_loc_type(cntr),
                                           LP_from_loc(cntr),
                                           L_transferred_qty,
                                           L_pgm_name,
                                           L_vdate,
                                           L_tran_code,
                                           NULL,
                                           LP_inv_status(cntr),
                                           NULL,
                                           NULL,
                                           O_error_message,
                                           L_found) = FALSE then
                   return FALSE;
               end if;
            end if;
            -- ----------------------------------------------------------------
            -- Decrement 1st leg item reserved quantity at the From Location
            -- ----------------------------------------------------------------
            if UPD_ITEM_RESV(O_error_message,
                             LP_tsf_item(cntr),
                             'N',
                             LP_from_loc(cntr),
                             LP_from_loc_type(cntr),
                             LP_upd_qty(cntr)) = FALSE then
               return FALSE;
            end if;
            -- ----------------------------------------------------------------
            --- Decrement reserved quantity for Component items.
            -- ----------------------------------------------------------------
            if LP_pack_ind(cntr) = 'Y' then
               L_tsf_item := LP_tsf_item(cntr);
               FOR comp_rec in C_ITEMS_IN_PACK(LP_from_loc(cntr)) LOOP
                  if UPD_ITEM_RESV(O_error_message,
                                   comp_rec.item,
                                   'Y',
                                   LP_from_loc(cntr),
                                   LP_from_loc_type(cntr),
                                   (LP_upd_qty(cntr) * comp_rec.qty)) = FALSE then
                     return FALSE;
                  end if;
               END LOOP;
            end if;-- Pack_ind = 'N'
            -- ----------------------------------------------------------------
            -- decrement 1st leg item expected quantity at either the To Loc,
            -- or the Internal or External Finisher.
            -- ----------------------------------------------------------------
            if (L_finisher_loc_ind = 'E' or
                L_finisher_loc_ind = 'R') and
                LP_pack_ind(cntr) = 'Y' then
               -- pack item at 'E'xternal Finisher or Internal Finishe'R'
               L_tsf_item := LP_tsf_item(cntr);

               -- update the expected quantities of the components of the pack at the Finisher.
               FOR comp_rec in C_ITEMS_IN_PACK(LP_from_loc(cntr)) LOOP
                  if UPD_ITEM_EXP(O_error_message,
                                  comp_rec.item,
                                  L_tsf_type,
                                  'E',
                                  LP_to_loc(cntr),
                                  LP_to_loc_type(cntr),
                                  (LP_upd_qty(cntr) * comp_rec.qty)) = FALSE then
                     return FALSE;
                  end if;
               END LOOP;
            else
               -- -------------------------------------------------------------
               -- no finishing, or not a pack item
               -- -------------------------------------------------------------
               -- Update 1st leg item expected quantity at the To Loc.
               -- Only Update the pack item if receive_as_type is 'P'ack.
               -- -------------------------------------------------------------
               if ( LP_pack_ind(cntr) = 'N'
                  or (LP_pack_ind(cntr) = 'Y' and LP_rat(cntr) = 'P') ) then
                  if UPD_ITEM_EXP(O_error_message,
                                  LP_tsf_item(cntr),
                                  L_tsf_type,
                                  'E',
                                  LP_to_loc(cntr),
                                  LP_to_loc_type(cntr),
                                  LP_upd_qty(cntr)) = FALSE then
                     return FALSE;
                  end if;
               end if;
               -- -------------------------------------------------------------
               -- Update the pack component items
               -- -------------------------------------------------------------
               if LP_pack_ind(cntr) = 'Y' then
                  L_tsf_item := LP_tsf_item(cntr);
                  FOR comp_rec in C_ITEMS_IN_PACK(LP_from_loc(cntr)) LOOP
                      if UPD_ITEM_EXP(O_error_message,
                                     comp_rec.item,
                                     L_tsf_type,
                                     LP_rat(cntr),
                                     LP_to_loc(cntr),
                                     LP_to_loc_type(cntr),
                                     (LP_upd_qty(cntr) * comp_rec.qty)) = FALSE then
                        return FALSE;
                     end if;
                  END LOOP;
               end if;
            end if;
         end if; --- First leg or Single-legged Transfer
         ---
         -- -------------------------------------------------------------------
         -- Second leg of a Multi-Legged Tranasfer (I_process_leg = 2)
         -- -------------------------------------------------------------------
         if (LP_tsf_parent_no(cntr) != -1) and (I_process_leg != '1') then
            if LP_pack_ind(cntr) = 'Y' then
               L_bulk_qty          := NULL;
               L_leftover_qty      := NULL;
               L_leftover_comp_qty := NULL;
               L_tsf_item := LP_tsf_item(cntr);

               open C_GET_LEFTOVER_QTY(L_tsf_item,
                                       LP_tsf_parent_no(cntr));
               fetch C_GET_LEFTOVER_QTY into L_leftover_qty;
               close C_GET_LEFTOVER_QTY;

               open C_GET_BULK_QTY(L_tsf_item,
                                   LP_tsf_parent_no(cntr));
               fetch C_GET_BULK_QTY into L_bulk_qty;
               close C_GET_BULK_QTY;

               FOR comp_rec in C_ITEMS_IN_PACK(LP_from_loc(cntr)) LOOP
                  L_from_item := NULL;
                  L_upd_qty   := NULL;
                  L_pack_no   := NULL;

                  open C_GET_XFORM_DETAIL(comp_rec.item,
                                          LP_tsf_parent_no(cntr));
                  fetch C_GET_XFORM_DETAIL into L_from_item;
                  close C_GET_XFORM_DETAIL;

                  -- ----------------------------------------------------------------
                  -- check if the item was originally part of a pack
                  -- ----------------------------------------------------------------
                  open C_CHECK_PACKING(NVL(L_from_item,LP_tsf_item(cntr)),
                                       LP_tsf_parent_no(cntr));
                  fetch C_CHECK_PACKING into L_pack_no,
                                             L_comp_qty;
                  close C_CHECK_PACKING;

                  -- -------------------------------------------------------------
                  -- the finisher resv and to loc exp qtys are based on the qty
                  -- received at the finisher and qty shipped to the final loc
                  -- as opposed to the from loc's resv qty and finisher's exp qty
                  -- which are based on tsf_qty and set at approval
                  -- -------------------------------------------------------------
                  open C_GET_UPD_QTY(NVL(L_pack_no,NVL(L_from_item,LP_tsf_item(cntr))),
                                     LP_tsf_item(cntr),
                                     LP_tsf_parent_no(cntr));
                  fetch C_GET_UPD_QTY into L_upd_qty;
                  close C_GET_UPD_QTY;
                  LP_upd_qty(cntr):= L_upd_qty;
                  -- -------------------------------------------------------------
                  -- LP_upd_qty can be greater than 0 (overship) if the second leg Shipped
                  -- more than was received. In this case Reserve and Expected
                  -- quantites don't adjusted because the full quantity has
                  -- been subtracted from the fields when shipped.

                  if LP_upd_qty(cntr) < 0 then
                     -- ----------------------------------------------------------
                     -- Update RESERVE quantities
                     -- ----------------------------------------------------------
                     if L_from_item is NULL then
                        if UPD_ITEM_RESV(O_error_message,
                                         comp_rec.item,
                                         'N',
                                         LP_from_loc(cntr),
                                         LP_from_loc_type(cntr),
                                         (LP_upd_qty(cntr) * comp_rec.qty)) = FALSE then
                           return FALSE;
                        end if;
                     else
                      -- Multiple From Items can be transformed into a single To Item
                        FOR xform in C_GET_XFORM_DETAIL(comp_rec.item,
                                                        LP_tsf_parent_no(cntr)) LOOP
                           if UPD_ITEM_RESV(O_error_message,
                                            xform.from_item,
                                           'N',
                                            LP_from_loc(cntr),
                                            LP_from_loc_type(cntr),
                                           (LP_upd_qty(cntr) * comp_rec.qty)) = FALSE then -- this quantity must be pro-rated.
                              return FALSE;
                           end if;
                        END LOOP;
                     end if;
                    -- ----------------------------------------------------------------
                     -- Update Expected Quantities
                     --    Do not update expected quantities for the pack item even where
                     --    the receive_as_type is 'P'ack. Only update the component items.
                     --    For the second leg, always treat pack components as bulk items.
                     --    i.e. receive_as_type = 'E'aches
                     --    See comments in STOCK_ORDER_RCV_SQL.UPD_ITEM_RESV_EXP
                     -- ----------------------------------------------------------------
                     if UPD_ITEM_EXP(O_error_message,
                                        comp_rec.item,
                                        L_tsf_type,
                                        'E',
                                        LP_to_loc(cntr),
                                        LP_to_loc_type(cntr),
                                       (LP_upd_qty(cntr) * comp_rec.qty)) = FALSE then
                        return FALSE;
                     end if;
                  end if;
               END LOOP; -- Component items
               ---
            else -- Pack_ind = 'N'
               L_from_item   := NULL;
               L_pack_no     := NULL;
               L_upd_qty     := NULL;

               L_bulk_qty          := NULL;
               L_leftover_qty      := NULL;
               L_leftover_comp_qty := NULL;
               ---
               open C_GET_XFORM_DETAIL(LP_tsf_item(cntr),
                                       LP_tsf_parent_no(cntr));
               fetch C_GET_XFORM_DETAIL into L_from_item;
               close C_GET_XFORM_DETAIL;
               -- ----------------------------------------------------------------
               -- check if the item was originally part of a pack
               -- ----------------------------------------------------------------
               open C_CHECK_PACKING(NVL(L_from_item,LP_tsf_item(cntr)),
                                    LP_tsf_parent_no(cntr));
               fetch C_CHECK_PACKING into L_pack_no,
                                          L_comp_qty;
               close C_CHECK_PACKING;

               if (L_from_item is NOT NULL) and (L_pack_no is NOT NULL) then
                  open C_GET_LEFTOVER_QTY(LP_tsf_item(cntr),
                                          LP_tsf_parent_no(cntr));
                  fetch C_GET_LEFTOVER_QTY into L_leftover_qty;
                  close C_GET_LEFTOVER_QTY;
                  if L_leftover_qty is NOT NULL then
                     L_leftover_comp_qty := L_comp_qty;
                  end if;
               end if;

               open C_GET_UPD_QTY(NVL(L_pack_no,NVL(L_from_item,LP_tsf_item(cntr))),
                                  LP_tsf_item(cntr),
                                  LP_tsf_parent_no(cntr));
               fetch C_GET_UPD_QTY into L_upd_qty;
               close C_GET_UPD_QTY;
               ---
               LP_upd_qty(cntr):= L_upd_qty;
               ---
               if LP_upd_qty(cntr) < 0 then
                  -- -------------------------------------------------------------
                  -- Update RESERVE Quantities
                  -- -------------------------------------------------------------
                  if L_from_item is NULL then
                     if UPD_ITEM_RESV(O_error_message,
                                      LP_tsf_item(cntr),
                                      'N',
                                      LP_from_loc(cntr),
                                      LP_from_loc_type(cntr),
                                      LP_upd_qty(cntr)) = FALSE then
                        return FALSE;
                     end if;
                  else
                     FOR xform in C_GET_XFORM_DETAIL(LP_tsf_item(cntr),
                                                     LP_tsf_parent_no(cntr)) LOOP
                        if UPD_ITEM_RESV(O_error_message,
                                         xform.from_item,
                                         'N',
                                         LP_from_loc(cntr),
                                         LP_from_loc_type(cntr),
                                         LP_upd_qty(cntr)) = FALSE then -- this quantity must be pro-rated.
                           return FALSE;
                         end if;
                     END LOOP;
                  end if;
                  -- -------------------------------------------------------------
                  -- Update EXPECTED Quantities
                  -- -------------------------------------------------------------
                  if NVL(LP_upd_qty(cntr), 0) < 0 then
                     if UPD_ITEM_EXP(O_error_message,
                                     LP_tsf_item(cntr),
                                     L_tsf_type,
                                     'E',
                                     LP_to_loc(cntr),
                                     LP_to_loc_type(cntr),
                                     LP_upd_qty(cntr)) = FALSE then
                        return FALSE;
                     end if;
                  end if;
               end if;
            end if;
         end if; -- 2nd Leg
      end if;-- NVL(LP_upd_qty, 0) >= 0
   END LOOP;

   for c_cancel_rec in C_TSF_FOR_CANCEL_QTY
    loop

       if (I_process_leg = 'A' or I_process_leg = '1') and c_cancel_rec.tsf_parent_no is null then

          open C_LOCK_TSFDETAIL_ITEM(c_cancel_rec.tsf_no, c_cancel_rec.item);
          close C_LOCK_TSFDETAIL_ITEM;

          update tsfdetail
            set cancelled_qty = NVL(cancelled_qty,0)+NVL(c_cancel_rec.cancel_qty,0),
                distro_qty = NVL2(distro_qty,0,NULL),
                selected_qty = NVL2(selected_qty,0,NULL)                 
           where tsf_no = c_cancel_rec.tsf_no
             and item = c_cancel_rec.item;

       elsif (I_process_leg = 'A' or I_process_leg = '2') and c_cancel_rec.tsf_parent_no is not null then

          open C_LOCK_TSFDETAIL_ITEM(c_cancel_rec.tsf_no, c_cancel_rec.item);
          close C_LOCK_TSFDETAIL_ITEM;

          update tsfdetail
            set cancelled_qty = NVL(cancelled_qty,0)+NVL(c_cancel_rec.cancel_qty,0),
                distro_qty = NVL2(distro_qty,0,NULL),
                selected_qty = NVL2(selected_qty,0,NULL)            
           where tsf_no = c_cancel_rec.tsf_no
             and item = c_cancel_rec.item;

       end if;

    end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;

END UPD_QTYS_WHEN_CLOSE;
-----------------------------------------------------------------------------------------
--Function: INSERT_TSFDETAIL_APPLY
--Purpose:  This function is only called by function APPLY_PARENT_ITEMLIST
--          to insert into tsfdetail
---------------------------------------------------------------------------
FUNCTION INSERT_TSFDETAIL_APPLY(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_invalid_item_ind   IN OUT   BOOLEAN,
                                O_invalid_qty        IN OUT   BOOLEAN,
                                I_item_type          IN       VARCHAR2,
                                I_tsf_no             IN       TSFDETAIL.TSF_NO%TYPE,
                                I_item               IN       ITEM_MASTER.ITEM%TYPE,
                                I_from_loc_type      IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                                I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                                I_to_loc_type        IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                                I_to_loc             IN       TSFHEAD.TO_LOC%TYPE,
                                I_qty_type           IN       VARCHAR2,
                                I_adjust_type        IN       VARCHAR2,
                                I_adjust_value       IN       NUMBER,
                                I_tsf_qty            IN       TSFDETAIL.TSF_QTY%TYPE,
                                I_inv_status         IN       TSFDETAIL.INV_STATUS%TYPE,
                                I_tsf_type           IN       TSFHEAD.TSF_TYPE%TYPE,
                                I_entity_type        IN       VARCHAR2 DEFAULT 'E',
                                I_restock_pct        IN       TSFDETAIL.RESTOCK_PCT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64)   := 'TRANSFER_SQL.INSERT_TSFDETAIL_APPLY';
   L_available_for_tsf   NUMBER(12,4);
   L_tsf_qty             TSFDETAIL.TSF_QTY%TYPE;
   L_max_seq_no          TSFDETAIL.TSF_SEQ_NO%TYPE;
   L_supp_pack_size      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_consignment         BOOLEAN := FALSE;
   L_exists              VARCHAR2(1);
   L_loc_exists          BOOLEAN;
   L_status              ITEM_LOC.STATUS%TYPE;
   L_loc                 ITEM_LOC.LOC%TYPE;
   L_tsf_status          TSFHEAD.STATUS%TYPE;
   L_from_av_cost        ITEM_LOC_SOH.AV_COST%TYPE;
   L_tsf_price           TSFDETAIL.TSF_PRICE%TYPE;
   L_dummy_item          TSF_XFORM_DETAIL.FROM_ITEM%TYPE;
   L_tsf_cost            TSFDETAIL.TSF_COST%TYPE;
   L_tsf_temp            TSFDETAIL.TSF_PRICE%TYPE;

   cursor C_MAX_SEQ_NO is
      select (NVL(max(tsf_seq_no),0) + 1)
        from tsfdetail
       where tsf_no = I_tsf_no;

   cursor C_ITEM_TSF_EXISTS is
      select 'X'
        from tsfdetail
       where item                  = I_item
         and NVL(inv_status, -999) = NVL(I_inv_status, -999)
         and tsf_no                = I_tsf_no;

   cursor C_GET_LOC_STATUS is
      select status
        from item_loc
       where item = I_item
         and loc  = L_loc;

   cursor C_GET_AV_COST is
      select av_cost
        from item_loc_soh
       where item = I_item
         and loc  = I_from_loc;

   cursor C_GET_TSF_STATUS is
      select status
        from tsfhead
       where tsf_no = I_tsf_no;

BEGIN
   open C_ITEM_TSF_EXISTS;
   fetch C_ITEM_TSF_EXISTS into L_exists;
   if C_ITEM_TSF_EXISTS%FOUND then
      close C_ITEM_TSF_EXISTS;
      O_invalid_item_ind := TRUE;
      return TRUE;
   end if;
   close C_ITEM_TSF_EXISTS;
   if I_item_type = 'IL' then
      if ITEM_ATTRIB_SQL.CONSIGNMENT_ITEM(O_error_message,
                                          I_item,
                                          L_consignment) = FALSE then
         return FALSE;
      end if;
   end if;
   -- items on consignment cannot be transferred
   if L_consignment = TRUE then
      O_invalid_item_ind := TRUE;
      return TRUE;
   end if;
   ---
   L_loc := I_from_loc;
   open C_GET_LOC_STATUS;
   fetch C_GET_LOC_STATUS into L_status;
   close C_GET_LOC_STATUS;
   -- item/loc combinations in delete status cannot be transferred
   if L_status = 'D' then
      O_invalid_item_ind := TRUE;
      return TRUE;
   end if;
   ---
   L_status := NULL;
   L_loc := I_to_loc;
   open C_GET_LOC_STATUS;
   fetch C_GET_LOC_STATUS into L_status;
   close C_GET_LOC_STATUS;
   -- the item has to be either active or discontinued at the to location,
   -- or the user must have chosen to range any unranged locations
   if L_status in ('I','D') then
      O_invalid_item_ind := TRUE;
      return TRUE;
   end if;

   if I_adjust_type is NOT NULL and I_adjust_value is NOT NULL then

      open C_GET_AV_COST;
      fetch C_GET_AV_COST into L_from_av_cost;
      close C_GET_AV_COST;

      if I_adjust_type = 'DA' then
         L_tsf_temp := L_from_av_cost - I_adjust_value;
      elsif I_adjust_type = 'DP' then
         L_tsf_temp :=
         L_from_av_cost - (L_from_av_cost * (I_adjust_value / 100));
      elsif I_adjust_type = 'IA' then
         L_tsf_temp := L_from_av_cost + I_adjust_value;
      elsif I_adjust_type = 'IP' then
         L_tsf_temp :=
         L_from_av_cost + (L_from_av_cost * (I_adjust_value / 100));
      elsif I_adjust_type = 'S' then
         L_tsf_temp := I_adjust_value;
      end if;

      if NVL(L_tsf_temp, -1) < 0 then
         O_invalid_item_ind := TRUE;
         return TRUE;
      end if;


      if I_entity_type = 'A' then
         L_tsf_cost := L_tsf_temp;
      elsif I_entity_type = 'E' then
         L_tsf_price := L_tsf_temp;
      end if;

   end if;

   if TRANSFER_SQL.QUANTITY_AVAIL(O_error_message,
                                  L_available_for_tsf,
                                  I_item,
                                  I_inv_status,
                                  I_from_loc_type,
                                  I_from_loc) = FALSE then
      return FALSE;
   end if;
   ---
   --*****************************************************************************************
   -- Qty type is either manual or total stock on hand.  If manual is chosen
   -- this program will attempt to insert the specified tsf qty into tsfdetail
   -- otherwise it will insert the available.  If there is no available quantity
   -- nothing will be inserted. If total stock on hand is chosen, then the available quantity
   -- for each item will be inserted unless there is no available quantity.
   --*****************************************************************************************
   if I_qty_type = 'M' then
      if NVL(L_available_for_tsf, 0) > 0 then
         if I_tsf_qty > L_available_for_tsf then
            L_tsf_qty := L_available_for_tsf;
         else
            L_tsf_qty := I_tsf_qty;
         end if;
      else
         L_tsf_qty := 0;
      end if;
   elsif I_qty_type = 'SOH' then
      if NVL(L_available_for_tsf, 0) > 0 then
         L_tsf_qty := L_available_for_tsf;
      else
         L_tsf_qty := 0;
      end if;
   end if;
   ---
   if L_tsf_qty = 0 then
      O_invalid_qty := TRUE;
      return TRUE;
   else
      open C_MAX_SEQ_NO;
      fetch C_MAX_SEQ_NO into L_max_seq_no;
      close C_MAX_SEQ_NO;
      ---
      if SUPP_ITEM_ATTRIB_SQL.GET_SUPP_PACK_SIZE(O_error_message,
                                                 L_supp_pack_size,
                                                 I_item,
                                                 NULL,
                                                 NULL) = FALSE then
         return FALSE;
      end if;
      ---
      insert into tsfdetail(tsf_no,
                            tsf_seq_no,
                            item,
                            inv_status,
                            tsf_price,
                            tsf_qty,
                            supp_pack_size,
                            tsf_po_link_no,
                            mbr_processed_ind,
                            tsf_cost,
                            restock_pct,
                            updated_by_rms_ind)
                     values(I_tsf_no,
                            L_max_seq_no,
                            I_item,
                            NVL(I_inv_status, NULL),
                            L_tsf_price,
                            L_tsf_qty,
                            L_supp_pack_size,
                            NULL,
                            NULL,
                            L_tsf_cost,
                            I_restock_pct,
                            'Y');
      --
      -- Insert Item Up Charges (charges will only be inserted for the appropriate tsf types).
      --
      if TRANSFER_CHARGE_SQL.DEFAULT_CHRGS(O_error_message,
                                           I_tsf_no,
                                           I_tsf_type,
                                           L_max_seq_no,
                                           NULL,    -- shipment
                                           NULL,    -- ship_seq_no
                                           I_from_loc,
                                           I_from_loc_type,
                                           I_to_loc,
                                           I_to_loc_type,
                                           I_item) = FALSE then
         return FALSE;
      end if;
      -- ----------------------------------------------------------------------------------
      -- Update Transformation qty. since this is a new item packing cannot exist
      -- ----------------------------------------------------------------------------------
      if ITEM_XFORM_PACK_SQL.UPDATE_XFORM_QTY(O_error_message,
                                              L_dummy_item,
                                              I_tsf_no,
                                              I_item,
                                              L_tsf_qty ) = FALSE then
         return FALSE;
      end if;
      --
      -- if L_status is NULL, then there is no relationship between the item and the 'To' location
      --
      if L_status is NULL then
         if NEW_ITEM_LOC(O_error_message=>O_error_message,
                         I_item=>I_item,
                         I_location=>I_to_loc,
                         I_item_parent=>NULL,
                         I_item_grandparent=>NULL,
                         I_loc_type=>I_to_loc_type,
                         I_short_desc=>NULL,
                         I_dept=>NULL,
                         I_class=>NULL,
                         I_subclass=>NULL,
                         I_item_level=>NULL,
                         I_tran_level=>NULL,
                         I_item_status=>NULL,
                         I_waste_type=>NULL,
                         I_daily_waste_pct=>NULL,
                         I_sellable_ind=>NULL,
                         I_orderable_ind=>NULL,
                         I_pack_ind=>NULL,
                         I_pack_type=>NULL,
                         I_unit_cost_loc=>NULL,
                         I_unit_retail_loc=>NULL,
                         I_selling_retail_loc=>NULL,
                         I_selling_uom=>NULL,
                         I_item_loc_status=>NULL,
                         I_taxable_ind=>NULL,
                         I_ti=>NULL,
                         I_hi=>NULL,
                         I_store_ord_mult=>NULL,  --New store order multiple input.
                         I_meas_of_each=>NULL,
                         I_meas_of_price=>NULL,
                         I_uom_of_price=>NULL,
                         I_primary_variant=>NULL,
                         I_primary_supp=>NULL,
                         I_primary_cntry=>NULL,
                         I_local_item_desc=>NULL,
                         I_local_short_desc=>NULL,
                         I_primary_cost_pack=>NULL,
                         I_receive_as_type=>NULL,  --New receive as type input.
                         I_date=>NULL,
                         I_default_to_children=>FALSE,
                         I_like_store=>NULL,
                         I_item_desc=>NULL,
                         I_diff_1=>NULL,
                         I_diff_2=>NULL,
                         I_diff_3=>NULL,
                         I_diff_4=>NULL,
                         I_lang=>NULL,
                         I_class_vat_ind=>NULL,
                         I_elc_ind=>NULL,
                         I_std_av_ind=>NULL,
                         I_vat_ind=>NULL,
                         I_rpm_ind=>NULL,
                         I_inbound_handling_days=>NULL,
                         I_group_type=>NULL,
                         I_store_price_ind=>NULL,
                         I_uin_type=>NULL,
                         I_uin_label=>NULL,
                         I_capture_time=>NULL,
                         I_ext_uin_ind=>NULL,  -- Default is 'N'
                         I_source_method=>NULL,
                         I_source_wh=>NULL,
                         I_new_loc_ind=>NULL,
                         I_ranged_ind=>'Y') = FALSE then   -- The ranging flag is set to 'Y'
            return FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_TSF_STATUS',
                       'TSFHEAD',
                       NULL);
      open C_GET_TSF_STATUS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_TSF_STATUS',
                       'TSFHEAD',
                       NULL);
      fetch C_GET_TSF_STATUS into L_tsf_status;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_TSF_STATUS',
                       'TSFHEAD',
                       NULL);
      close C_GET_TSF_STATUS;
      ---
      ---quantities are only reserved/expected once the transfer is out of 'I'nput status
      if I_tsf_type not in ('NS', 'NB') and L_tsf_status != 'I' then
         if TRANSFER_SQL.UPD_ITEM_RESV_EXP(O_error_message,
                                           I_item,
                                           I_tsf_type,
                                           L_tsf_qty,
                                           I_from_loc_type,
                                           I_from_loc,
                                           I_to_loc_type,
                                           I_to_loc) = FALSE then
             return FALSE;
          end if;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_TSFDETAIL_APPLY;
-----------------------------------------------------------------------------------------
FUNCTION APPLY_PARENT_ITEMLIST(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_invalid_item_ind   IN OUT   BOOLEAN,
                               O_invalid_qty        IN OUT   BOOLEAN,
                               I_tsf_no             IN       TSFDETAIL.TSF_NO%TYPE,
                               I_item_type          IN       VARCHAR2,
                               I_item               IN       ITEM_MASTER.ITEM%TYPE,
                               I_diff_id            IN       DIFF_IDS.DIFF_ID%TYPE,
                               I_item_list          IN       SKULIST_HEAD.SKULIST%TYPE,
                               I_tsf_type           IN       TSFHEAD.TSF_TYPE%TYPE,
                               I_inv_status         IN       TSFDETAIL.INV_STATUS%TYPE,
                               I_qty_type           IN       VARCHAR2,
                               I_tsf_qty            IN       TSFDETAIL.TSF_QTY%TYPE,
                               I_adjust_type        IN       VARCHAR2,
                               I_adjust_value       IN       NUMBER,
                               I_from_loc_type      IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                               I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                               I_to_loc_type        IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                               I_to_loc             IN       TSFHEAD.TO_LOC%TYPE,
                               I_dept               IN       TSFHEAD.DEPT%TYPE,
                               I_entity_type        IN       VARCHAR2  DEFAULT 'E',
                               I_restock_pct        IN       TSFDETAIL.RESTOCK_PCT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64)   := 'TRANSFER_SQL.APPLY_PARENT_ITEMLIST';
   L_item_1              ITEM_MASTER.ITEM%TYPE;
   L_item_2              ITEM_MASTER.ITEM%TYPE;
   L_av_cost             ITEM_LOC_SOH.AV_COST%TYPE;
   L_invalid_item_ind    BOOLEAN;
   L_invalid_qty         BOOLEAN;
   L_pack_ind            ITEM_MASTER.PACK_IND%TYPE;
   L_item_level          ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level          ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_receive_as_type     ITEM_LOC.RECEIVE_AS_TYPE%TYPE;

   cursor C_ITEMS is
      select item
        from item_master
       where (item_parent        = I_item
             or item_grandparent = I_item)
         and (I_diff_id is NULL
              or (diff_1    = I_diff_id
                  or diff_2 = I_diff_id
                  or diff_3 = I_diff_id
                  or diff_4 = I_diff_id))
         and inventory_ind = 'Y'
         and item_level = tran_level;

   cursor C_ITEMLIST is
      select item
        from item_master
       where item_parent      = L_item_1
          or item_grandparent = L_item_1
         and inventory_ind    = 'Y';

   cursor C_SKULIST is
      select sd.item,
             sd.pack_ind
        from skulist_detail sd,
             item_master im
       where sd.skulist       = I_item_list
         and sd.item          = im.item
         and (I_dept is NULL or
              im.dept = I_dept)
         and im.inventory_ind = 'Y';

BEGIN
   O_invalid_item_ind := FALSE;
   ---
   if I_item_type != 'IL' then
      FOR C_rec in C_ITEMS LOOP
         L_item_1 := C_rec.item;
         ---
         if INSERT_TSFDETAIL_APPLY(O_error_message,
                                   L_invalid_item_ind,
                                   L_invalid_qty,
                                   I_item_type,
                                   I_tsf_no,
                                   L_item_1,
                                   I_from_loc_type,
                                   I_from_loc,
                                   I_to_loc_type,
                                   I_to_loc,
                                   I_qty_type,
                                   I_adjust_type,
                                   I_adjust_value,
                                   I_tsf_qty,
                                   I_inv_status,
                                   I_tsf_type,
                                   I_entity_type,
                                   I_restock_pct) = FALSE then
            return FALSE;
         end if;
         ---
         if L_invalid_item_ind = TRUE then
            O_invalid_item_ind := TRUE;
         end if;

         if L_invalid_qty = TRUE then
            O_invalid_qty := TRUE;
         end if;

      END LOOP;
   elsif I_item_type = 'IL' then
      FOR C_rec in C_SKULIST LOOP
         L_item_1 := C_rec.item;
         ---
         -- pack items cannot be transferred from stores
         ---
         if C_rec.pack_ind = 'Y' and I_from_loc_type = 'S' then
            O_invalid_item_ind := TRUE;
         else
            if C_rec.pack_ind = 'Y' and I_from_loc_type = 'W' then
               if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                         L_receive_as_type,
                                                         L_item_1,
                                                         I_from_loc) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
            if C_rec.pack_ind = 'Y' and I_from_loc_type = 'W' and NVL(L_receive_as_type,'P') = 'E' then
               O_invalid_item_ind := TRUE;
            else
               if ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                             L_item_level,
                                             L_tran_level,
                                             L_item_1) = FALSE then
                  return FALSE;
               end if;
               ---
               if L_item_level = L_tran_level then
                  if INSERT_TSFDETAIL_APPLY(O_error_message,
                                            L_invalid_item_ind,
                                            L_invalid_qty,
                                            I_item_type,
                                            I_tsf_no,
                                            L_item_1,
                                            I_from_loc_type,
                                            I_from_loc,
                                            I_to_loc_type,
                                            I_to_loc,
                                            I_qty_type,
                                            I_adjust_type,
                                            I_adjust_value,
                                            I_tsf_qty,
                                            I_inv_status,
                                            I_tsf_type,
                                            I_entity_type,
                                            I_restock_pct) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  if L_invalid_item_ind = TRUE then
                     O_invalid_item_ind := TRUE;
                  end if;

                  if L_invalid_qty = TRUE then
                     O_invalid_qty := TRUE;
                  end if;

               elsif L_item_level < L_tran_level then
                  FOR C_rec in C_ITEMLIST LOOP
                     L_item_2 := C_rec.item;
                     ---
                     if INSERT_TSFDETAIL_APPLY(O_error_message,
                                               L_invalid_item_ind,
                                               L_invalid_qty,
                                               'IL',
                                               I_tsf_no,
                                               L_item_2,
                                               I_from_loc_type,
                                               I_from_loc,
                                               I_to_loc_type,
                                               I_to_loc,
                                               I_qty_type,
                                               I_adjust_type,
                                               I_adjust_value,
                                               I_tsf_qty,
                                               I_inv_status,
                                               I_tsf_type,
                                               I_entity_type,
                                               I_restock_pct) = FALSE then
                        return FALSE;
                     end if;
                     ---
                     if L_invalid_item_ind = TRUE then
                        O_invalid_item_ind := TRUE;
                     end if;

                     if L_invalid_qty = TRUE then
                        O_invalid_qty := TRUE;
                     end if;
                  END LOOP;
               end if;
            end if;
         end if;
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END APPLY_PARENT_ITEMLIST;
--------------------------------------------------------------------------------
FUNCTION PACK_TRANSFERS_EXIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_transfers_exist   IN OUT   BOOLEAN,
                              I_pack_no           IN       PACKITEM.PACK_NO%TYPE,
                              I_loc               IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.PACK_TRANSFERS_EXIST';
   L_exists    VARCHAR2(1)    := 'N';

   cursor C_TSF_EXISTS is
      select 'x'
        from tsfhead h,
             tsfdetail d
       where h.tsf_no = d.tsf_no
         and d.item   = I_pack_no
         and h.to_loc = I_loc
         and h.status in ('I','A','E','S');

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_TSF_EXISTS',
                    'TSFHEAD, TSFDETAIL',
                    NULL);
   open C_TSF_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_TSF_EXISTS',
                    'TSFHEAD, TSFDETAIL',
                    NULL);
   fetch C_TSF_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_TSF_EXISTS',
                    'TSFHEAD, TSFDETAIL',
                    NULL);
   close C_TSF_EXISTS;

   if L_exists = 'x' then
      O_transfers_exist := TRUE;
   else
      O_transfers_exist := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PACK_TRANSFERS_EXIST;
------------------------------------------------------------------------------------
FUNCTION DETAILS_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists          IN OUT   BOOLEAN,
                       I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60)   := 'TRANSFER_SQL.DETAILS_EXIST';
   L_exists    VARCHAR2(1)    := 'N';

   cursor C_DETAILS_EXIST is
      select 'Y'
        from tsfdetail
       where tsf_no = I_tsf_no;

BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_DETAILS_EXIST',
                    'TSFDETAIL',
                    'Transfer: '||to_char(I_tsf_no));
   open C_DETAILS_EXIST;
   SQL_LIB.SET_MARK('FETCH',
                    'C_DETAILS_EXIST',
                    'TSFDETAIL',
                    'Transfer: '||to_char(I_tsf_no));
   fetch C_DETAILS_EXIST into L_exists;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_DETAILS_EXIST',
                    'TSFDETAIL',
                    'Transfer: '||to_char(I_tsf_no));
   close C_DETAILS_EXIST;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DETAILS_EXIST;
------------------------------------------------------------------------------------
FUNCTION TSF_ITEM_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists          IN OUT   BOOLEAN,
                        I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                        I_item            IN       TSFDETAIL.ITEM%TYPE,
                        I_inv_status      IN       TSFDETAIL.INV_STATUS%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'TRANSFER_SQL.TSF_ITEM_EXIST';
   L_found   VARCHAR2(1)  := NULL;

   cursor C_TSFDETAIL is
      select 'x'
        from tsfdetail
       where tsf_no = I_tsf_no
         and item = I_item
         and NVL(inv_status, -999) = NVL(I_inv_status, -999)
         and rownum = 1;

BEGIN
   open C_TSFDETAIL;
   fetch C_TSFDETAIL into L_found;
   close C_TSFDETAIL;

   if L_found is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END;
------------------------------------------------------------------------------------
FUNCTION CREATE_FROM_EXISTING(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_new_tsf_no      IN OUT   TSFHEAD.TSF_NO%TYPE,
                              I_old_tsf_no      IN       TSFHEAD.TSF_NO%TYPE,
                              I_status          IN       TSFHEAD.STATUS%TYPE,
                              I_username        IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program            VARCHAR2(60)          := 'TRANSFER_SQL.CREATE_FROM_EXISTING';
   L_return_code        VARCHAR2(5);
   L_xform_exists       VARCHAR2(1)           := 'N';
   L_rec_qty_exists     VARCHAR2(1)           := 'N';

   L_status             TSFHEAD.STATUS%TYPE;
   L_vdate              PERIOD.VDATE%TYPE     := LP_vdate;
   L_seq_no             ORDCUST.ORDCUST_NO%TYPE;
   L_tsf_type           TSFHEAD.TSF_TYPE%TYPE;

   L_old_child_tsf_no   TSFHEAD.TSF_NO%TYPE   := NULL;
   L_new_child_tsf_no   TSFHEAD.TSF_NO%TYPE;

   L_new_tsf_wo_id      TSF_WO_HEAD.TSF_WO_ID%TYPE;
   L_old_tsf_wo_id      TSF_WO_HEAD.TSF_WO_ID%TYPE;
   L_new_tsf_xform_id   TSF_XFORM.TSF_XFORM_ID%TYPE;
   L_new_tsf_packing_id TSF_PACKING.TSF_PACKING_ID%TYPE;
   L_username           VARCHAR2(30):= NVL(I_username, GET_USER);

   cursor C_GET_TSF_CHILD is
      select h.tsf_no
        from tsfhead h
       where h.tsf_parent_no = I_old_tsf_no;

   cursor C_CHECK_XFORM_EXISTS is
      select 'Y'
        from tsf_xform
       where tsf_no = I_old_tsf_no
         and rownum = 1;

   cursor C_CHECK_RECONCILED_QTY is
      select 'Y'
        from tsfdetail
       where tsf_no = L_old_child_tsf_no
         and reconciled_qty is not NULL
         and rownum = 1;

   cursor C_GET_NEW_TSF_WO_ID is
      select tsf_wo_id_seq.NEXTVAL
        from dual;

   cursor C_GET_OLD_TSF_WO_ID is
      select twh.tsf_wo_id
        from tsf_wo_head twh,
             tsf_wo_detail twd
       where twh.tsf_no = I_old_tsf_no
         and twh.tsf_wo_id = twd.tsf_wo_id;

   cursor C_GET_NEW_TSF_XFORM_ID is
      select tsf_xform_id_sequence.NEXTVAL
        from dual;

   cursor C_GET_OLD_TSF_XFORM_ID is
      select tsf_xform_id
        from tsf_xform
       where tsf_no = I_old_tsf_no;

   cursor C_GET_NEW_TSF_PACKING_ID is
      select tsf_packing_id_sequence.NEXTVAL
        from dual;

   cursor C_GET_OLD_TSF_PACKING_ID is
      select tsf_packing_id
        from tsf_packing
       where tsf_no = I_old_tsf_no;

   cursor C_GET_DETAILS is
      select h.tsf_type,
             h.from_loc,
             h.from_loc_type,
             h.to_loc,
             h.to_loc_type,
             rownum tsf_seq_no,
             d.item
        from tsfhead h,
             tsfdetail d
       where h.tsf_no = d.tsf_no
         and h.tsf_no = I_old_tsf_no;

   cursor C_SEQ_NO is
      select ordcust_seq.nextval
        from dual;

   cursor C_GET_TSF_TYPE is
      select tsf_type
        from tsfhead
       where tsf_no = I_old_tsf_no;

BEGIN
   if I_old_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_status',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   NEXT_TRANSFER_NUMBER(O_new_tsf_no,
                        L_return_code,
                        O_error_message);

   -- this query doesn't deal with child records (see logic below for that)
   insert into tsfhead(tsf_no,
                       tsf_parent_no,
                       from_loc_type,
                       from_loc,
                       to_loc_type,
                       to_loc,
                       exp_dc_date,
                       dept,
                       tsf_type,
                       status,
                       freight_code,
                       routing_code,
                       create_date,
                       create_id,
                       approval_date,
                       approval_id,
                       delivery_date,
                       close_date,
                       ext_ref_no,
                       repl_tsf_approve_ind,
                       comment_desc,
                       inventory_type,
                       context_type,
                       context_value)
               (select O_new_tsf_no,
                       NULL,
                       from_loc_type,
                       from_loc,
                       to_loc_type,
                       to_loc,
                       greatest(p.vdate, NVL(exp_dc_date,p.vdate)) exp_dc_date,
                       dept,
                       tsf_type,
                       I_status,             -- status
                       freight_code,
                       routing_code,
                       L_vdate,              -- create_date
                       L_username,           -- create_id
                       NULL,                 -- approval_date
                       NULL,                 -- approval_id
                       NULL,                 -- delivery_date
                       NULL,                 -- close_date
                       ext_ref_no,
                       'N',                  -- repl_tsf_approve_ind
                       NULL,                 -- comment_desc
                       inventory_type,
                       context_type,
                       context_value
                  from tsfhead, (select vdate from period) p
                 where tsf_no = I_old_tsf_no);

   insert into tsfhead_l10n_ext (tsf_no,
                                 l10n_country_id,
                                 group_id,
                                 varchar2_1,
                                 varchar2_2,
                                 varchar2_3,
                                 varchar2_4,
                                 varchar2_5,
                                 varchar2_6,
                                 varchar2_7,
                                 varchar2_8,
                                 varchar2_9,
                                 varchar2_10,
                                 number_11,
                                 number_12,
                                 number_13,
                                 number_14,
                                 number_15,
                                 number_16,
                                 number_17,
                                 number_18,
                                 number_19,
                                 number_20,
                                 date_21,
                                 date_22)
                          select O_new_tsf_no,
                                 l10n_country_id,
                                 group_id,
                                 varchar2_1,
                                 varchar2_2,
                                 varchar2_3,
                                 varchar2_4,
                                 varchar2_5,
                                 varchar2_6,
                                 varchar2_7,
                                 varchar2_8,
                                 varchar2_9,
                                 varchar2_10,
                                 number_11,
                                 number_12,
                                 number_13,
                                 number_14,
                                 number_15,
                                 number_16,
                                 number_17,
                                 number_18,
                                 number_19,
                                 number_20,
                                 date_21,
                                 date_22
                            from tsfhead_l10n_ext
                           where tsf_no = I_old_tsf_no;

   -- For tsf with xform details, use ship qty for reconciled transfers so
   -- that the transformation and packing details are accurate in the case of
   -- under/over receiving.

   open  C_GET_TSF_CHILD;
   fetch C_GET_TSF_CHILD into L_old_child_tsf_no;

   if C_GET_TSF_CHILD%FOUND then
      open  C_CHECK_XFORM_EXISTS;
      fetch C_CHECK_XFORM_EXISTS into L_xform_exists;

      if C_CHECK_XFORM_EXISTS%FOUND then
         open  C_CHECK_RECONCILED_QTY;
         fetch C_CHECK_RECONCILED_QTY into L_rec_qty_exists;
         close C_CHECK_RECONCILED_QTY;
      end if;

      close C_CHECK_XFORM_EXISTS;
   end if;

   close C_GET_TSF_CHILD;

   insert into tsfdetail(tsf_no,
                         tsf_seq_no,
                         item,
                         inv_status,
                         tsf_price,
                         tsf_qty,
                         fill_qty,
                         ship_qty,
                         received_qty,
                         reconciled_qty,
                         distro_qty,
                         selected_qty,
                         cancelled_qty,
                         supp_pack_size,
                         tsf_po_link_no,
                         mbr_processed_ind,
                         publish_ind,
                         updated_by_rms_ind)
                 (select O_new_tsf_no,
                         rownum,
                         item,
                         inv_status,
                         tsf_price,
                         decode(L_rec_qty_exists, 'Y', NVL(received_qty,tsf_qty), tsf_qty),
                         NULL,               -- fill_qty
                         NULL,               -- ship_qty
                         NULL,               -- received_qty
                         NULL,               -- reconciled_qty
                         NULL,               -- distro_qty
                         NULL,               -- selected_qty
                         NULL,               -- cancelled_qty
                         supp_pack_size,
                         NULL,               -- tsf_po_link_no
                         NULL,               -- mbr_processed_ind
                         'N',                 -- publish_ind
                         'Y'
                    from tsfdetail
                   where tsf_no = I_old_tsf_no);

      -- insert into tsfdetail_chrg
      -- (upcharges are only associated with the first leg of the transfer)
      FOR C_rec in C_GET_DETAILS LOOP
         if TRANSFER_CHARGE_SQL.DEFAULT_CHRGS(O_error_message,
                                              O_new_tsf_no,
                                              C_rec.tsf_type,
                                              C_rec.tsf_seq_no,
                                              NULL,   -- shipment
                                              NULL,   -- ship_seq_no
                                              C_rec.from_loc,
                                              C_rec.from_loc_type,
                                              C_rec.to_loc,
                                              C_rec.to_loc_type,
                                              C_rec.item) = FALSE then
            return FALSE;
         end if;
      END LOOP;

   -- per design specs, a maximum of one child record is allowed

   if L_old_child_tsf_no is NOT NULL then

      NEXT_TRANSFER_NUMBER(L_new_child_tsf_no,
                           L_return_code,
                           O_error_message);
      ----
      -- multilegged transfer
      ----

      -- create records for the second leg
      -- this was broken out from the above tsfhead insert/select in
      -- order to avoid a table scan
      insert into tsfhead(tsf_no,
                          tsf_parent_no,
                          from_loc_type,
                          from_loc,
                          to_loc_type,
                          to_loc,
                          exp_dc_date,
                          dept,
                          tsf_type,
                          status,
                          freight_code,
                          routing_code,
                          create_date,
                          create_id,
                          approval_date,
                          approval_id,
                          delivery_date,
                          close_date,
                          ext_ref_no,
                          repl_tsf_approve_ind,
                          comment_desc,
                          inventory_type,
                          context_type,
                          context_value)
                  (select L_new_child_tsf_no,
                          O_new_tsf_no,
                          from_loc_type,
                          from_loc,
                          to_loc_type,
                          to_loc,
                          greatest(p.vdate, NVL(exp_dc_date,p.vdate)) exp_dc_date,
                          dept,
                          tsf_type,
                          I_status,             -- status
                          freight_code,
                          routing_code,
                          L_vdate,              -- create_date
                          L_username,           -- create_id
                          NULL,                 -- approval_date
                          NULL,                 -- approval_id
                          NULL,                 -- delivery_date
                          NULL,                 -- close_date
                          ext_ref_no,
                          'N',                  -- repl_tsf_approve_ind
                          NULL,                 -- comment_desc
                          inventory_type,
                          context_type,
                          context_value
                     from tsfhead, (select vdate from period) p
                    where tsf_parent_no = I_old_tsf_no);

      -- transfer work orders

      -- per design specs, there can only be one work order per transfer.
      -- the only reason tsf_wo_head exists is to allow the program to show
      -- the user one work order number per transfer (this is a design
      -- requirement).  also, the work order is only associated with the
      -- first leg of the transfer.

      open C_GET_OLD_TSF_WO_ID;
      fetch C_GET_OLD_TSF_WO_ID into L_old_tsf_wo_id;

      if C_GET_OLD_TSF_WO_ID%FOUND then
         open C_GET_NEW_TSF_WO_ID;
         fetch C_GET_NEW_TSF_WO_ID into L_new_tsf_wo_id;
         close C_GET_NEW_TSF_WO_ID;

         insert into tsf_wo_head(tsf_wo_id,
                                 tsf_no)
                         values (L_new_tsf_wo_id,
                                 O_new_tsf_no);

         insert into tsf_wo_detail(tsf_wo_detail_id,
                                   tsf_wo_id,
                                   item,
                                   activity_id,
                                   unit_cost,
                                   comments)
                           (select tsf_wo_detail_id_seq.NEXTVAL,
                                   L_new_tsf_wo_id,
                                   d.item,
                                   d.activity_id,
                                   d.unit_cost,
                                   d.comments
                              from tsf_wo_detail d, tsf_wo_head h
                             where h.tsf_no = I_old_tsf_no
                               and h.tsf_wo_id = d.tsf_wo_id);
      end if;
      close C_GET_OLD_TSF_WO_ID;

      -- transfer transformations

      -- the transformation is only associated with the first leg of the
      -- transfer

      FOR rec in C_GET_OLD_TSF_XFORM_ID LOOP

         open C_GET_NEW_TSF_XFORM_ID;
         fetch C_GET_NEW_TSF_XFORM_ID into L_new_tsf_xform_id;
         close C_GET_NEW_TSF_XFORM_ID;

         insert into tsf_xform (tsf_xform_id,
                                tsf_no)
                        values (L_new_tsf_xform_id,
                                O_new_tsf_no);

         insert into tsf_xform_detail (tsf_xform_detail_id,
                                       tsf_xform_id,
                                       from_item,
                                       from_qty,
                                       to_item,
                                       to_qty)
                               (select tsf_xform_detail_id_sequence.NEXTVAL,
                                       L_new_tsf_xform_id,
                                       from_item,
                                       from_qty,
                                       to_item,
                                       to_qty
                                  from tsf_xform_detail
                                 where tsf_xform_id = rec.tsf_xform_id);

      END LOOP;

      -- transfer packing

      -- packing is only associated with the first leg of the transfer

      FOR rec in C_GET_OLD_TSF_PACKING_ID LOOP

         open C_GET_NEW_TSF_PACKING_ID;
         fetch C_GET_NEW_TSF_PACKING_ID into L_new_tsf_packing_id;
         close C_GET_NEW_TSF_PACKING_ID;

         insert into tsf_packing (tsf_packing_id,
                                  tsf_no,
                                  set_no)
                          (select L_new_tsf_packing_id,
                                  O_new_tsf_no,
                                  set_no
                             from tsf_packing
                            where tsf_packing_id = rec.tsf_packing_id);

         insert into tsf_packing_detail (tsf_packing_detail_id,
                                         tsf_packing_id,
                                         record_type,
                                         item,
                                         diff_id,
                                         qty)
                                 (select tsf_packing_detail_id_sequence.NEXTVAL,
                                         L_new_tsf_packing_id,
                                         record_type,
                                         item,
                                         diff_id,
                                         qty
                                    from tsf_packing_detail
                                   where tsf_packing_id = rec.tsf_packing_id);
      END LOOP;

   else
      ----
      -- standard transfer
      ----

      open C_GET_TSF_TYPE;
      fetch C_GET_TSF_TYPE into L_tsf_type;
      close C_GET_TSF_TYPE;

      if L_tsf_type = 'CO' then

         open C_SEQ_NO;
         fetch C_SEQ_NO into L_seq_no;
         close C_SEQ_NO;

         insert into ordcust(ordcust_no,
                             status,
                             order_no,
                             tsf_no,
                             source_loc_type,
                             source_loc_id,
                             fulfill_loc_type,
                             fulfill_loc_id,
                             customer_no,
                             customer_order_no,
                             fulfill_order_no,
                             partial_delivery_ind,
                             delivery_type,
                             carrier_code,
                             carrier_service_code,
                             consumer_delivery_date,
                             consumer_delivery_time,
                             bill_first_name,
                             bill_phonetic_first,
                             bill_last_name,
                             bill_phonetic_last,
                             bill_preferred_name,
                             bill_company_name,
                             bill_add1,
                             bill_add2,
                             bill_add3,
                             bill_county,
                             bill_city,
                             bill_state,
                             bill_country_id,
                             bill_post,
                             bill_jurisdiction,
                             bill_phone,
                             deliver_first_name,
                             deliver_phonetic_first,
                             deliver_last_name,
                             deliver_phonetic_last,
                             deliver_preferred_name,
                             deliver_company_name,
                             deliver_add1,
                             deliver_add2,
                             deliver_add3,
                             deliver_county,
                             deliver_city,
                             deliver_state,
                             deliver_country_id,
                             deliver_post,
                             deliver_jurisdiction,
                             deliver_phone,
                             deliver_charge,
                             deliver_charge_curr,
                             comments,
                             create_datetime,
                             create_id,
                             last_update_datetime,
                             last_update_id)
                     (select L_seq_no,
                             'C',
                             NULL,
                             O_new_tsf_no,
                             source_loc_type,
                             source_loc_id,
                             fulfill_loc_type,
                             fulfill_loc_id,
                             customer_no,
                             O_new_tsf_no,-- Need to use this as CUSTOMER_ORDER_NO and FULFILL_ORDER_NO to avoid violating unique constraint
                             O_new_tsf_no,
                             partial_delivery_ind,
                             delivery_type,
                             carrier_code,
                             carrier_service_code,
                             consumer_delivery_date,
                             consumer_delivery_time,
                             bill_first_name,
                             bill_phonetic_first,
                             bill_last_name,
                             bill_phonetic_last,
                             bill_preferred_name,
                             bill_company_name,
                             bill_add1,
                             bill_add2,
                             bill_add3,
                             bill_county,
                             bill_city,
                             bill_state,
                             bill_country_id,
                             bill_post,
                             bill_jurisdiction,
                             bill_phone,
                             deliver_first_name,
                             deliver_phonetic_first,
                             deliver_last_name,
                             deliver_phonetic_last,
                             deliver_preferred_name,
                             deliver_company_name,
                             deliver_add1,
                             deliver_add2,
                             deliver_add3,
                             deliver_county,
                             deliver_city,
                             deliver_state,
                             deliver_country_id,
                             deliver_post,
                             deliver_jurisdiction,
                             deliver_phone,
                             deliver_charge,
                             deliver_charge_curr,
                             NULL,
                             SYSDATE,
                             GET_USER,
                             SYSDATE,
                             GET_USER
                        from ordcust
                       where tsf_no = I_old_tsf_no);

         insert into ordcust_detail(ordcust_no,
                                    item,
                                    ref_item,
                                    original_item,
                                    qty_ordered_suom,
                                    qty_cancelled_suom,
                                    standard_uom,
                                    transaction_uom,
                                    substitute_allowed_ind,
                                    unit_retail,
                                    retail_currency_code,
                                    comments,
                                    create_datetime,
                                    create_id,
                                    last_update_datetime,
                                    last_update_id)
                            (select L_seq_no,
                                    od.item,
                                    od.ref_item,
                                    NULL,
                                    od.qty_ordered_suom,
                                    NULL,
                                    od.standard_uom,
                                    od.transaction_uom,
                                    od.substitute_allowed_ind,
                                    od.unit_retail,
                                    od.retail_currency_code,
                                    NULL,
                                    SYSDATE,
                                    GET_USER,
                                    SYSDATE,
                                    GET_USER
                               from ordcust o,
                                    ordcust_detail od
                              where o.ordcust_no = od.ordcust_no
                                and o.tsf_no = I_old_tsf_no);

      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_FROM_EXISTING;
--------------------------------------------------------------------------------
FUNCTION TOTAL_COST_RETAIL(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_total_cost_from              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_cost_to                IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_cost_prim              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_chrg_from              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_chrg_to                IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_chrg_prim              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_retail_incl_vat_from   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           O_total_retail_incl_vat_to     IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           O_total_retail_incl_vat_prim   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           I_tsf_no                       IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)   := 'TRANSFER_SQL.TOTAL_COST_RETAIL';

   L_tsf_type        TSFHEAD.TSF_TYPE%TYPE;
   L_from_loc        TSFHEAD.FROM_LOC%TYPE;
   L_from_loc_type   TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_to_loc          TSFHEAD.TO_LOC%TYPE;
   L_to_loc_type     TSFHEAD.TO_LOC_TYPE%TYPE;
   L_tsfhead_info    TRANSFER_SQL.TSFHEAD_INFO;

   cursor C_TSF is
      select tsf_type
        from tsfhead
       where tsf_no = I_tsf_no;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if GET_BASIC_TSFHEAD_INFO(O_error_message,
                             L_tsfhead_info,
                             I_tsf_no) = FALSE then
      return FALSE;
   end if;

   L_from_loc        := L_tsfhead_info.from_loc;
   L_from_loc_type   := L_tsfhead_info.from_loc_type;
   L_to_loc          := L_tsfhead_info.to_loc;
   L_to_loc_type     := L_tsfhead_info.to_loc_type;

   open C_TSF;
   fetch C_TSF into L_tsf_type;
   close C_TSF;

   if TRANSFER_SQL.TOTAL_COST_RETAIL(O_error_message,
                                     O_total_cost_from,
                                     O_total_cost_to,
                                     O_total_cost_prim,
                                     O_total_chrg_from,
                                     O_total_chrg_to,
                                     O_total_chrg_prim,
                                     O_total_retail_incl_vat_from,
                                     O_total_retail_incl_vat_to,
                                     O_total_retail_incl_vat_prim,
                                     I_tsf_no,
                                     L_tsf_type,
                                     L_from_loc,
                                     L_from_loc_type,
                                     L_to_loc,
                                     L_to_loc_type) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END TOTAL_COST_RETAIL;
--------------------------------------------------------------------------------
FUNCTION TOTAL_COST_RETAIL(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_total_cost_from              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_cost_to                IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_cost_prim              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_chrg_from              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_chrg_to                IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_chrg_prim              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_retail_incl_vat_from   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           O_total_retail_incl_vat_to     IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           O_total_retail_incl_vat_prim   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           I_tsf_no                       IN       TSFHEAD.TSF_NO%TYPE,
                           I_tsf_type                     IN       TSFHEAD.TSF_TYPE%TYPE,
                           I_from_loc                     IN       TSFHEAD.FROM_LOC%TYPE,
                           I_from_loc_type                IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                           I_to_loc                       IN       TSFHEAD.TO_LOC%TYPE,
                           I_to_loc_type                  IN       TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)               := 'TRANSFER_SQL.TOTAL_COST_RETAIL';
   L_qty                  TSFDETAIL.TSF_QTY%TYPE;
   L_tsf_seq_no           TSFDETAIL.TSF_SEQ_NO%TYPE;
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_pack_ind             ITEM_MASTER.PACK_IND%TYPE;
   L_up_chrg_prim         SHIPSKU.UNIT_COST%TYPE     := 0;
   L_unit_cost_from       SHIPSKU.UNIT_COST%TYPE     := 0;
   L_unit_retail_from     SHIPSKU.UNIT_RETAIL%TYPE   := 0;
   L_unit_cost_to         SHIPSKU.UNIT_COST%TYPE     := 0;
   L_unit_retail_to       SHIPSKU.UNIT_RETAIL%TYPE   := 0;
   L_av_cost_from         SHIPSKU.UNIT_RETAIL%TYPE   := 0;
   L_av_cost_to           SHIPSKU.UNIT_RETAIL%TYPE   := 0;
   L_selling_unit_retail  SHIPSKU.UNIT_RETAIL%TYPE   := 0;
   L_profit_chrg_loc      SHIPSKU.UNIT_COST%TYPE     := 0;
   L_exp_chrg_loc         SHIPSKU.UNIT_COST%TYPE     := 0;
   L_loc_unit_cost_from   SHIPSKU.UNIT_COST%TYPE     := 0;
   L_loc_unit_retail_from SHIPSKU.UNIT_RETAIL%TYPE   := 0;
   L_selling_uom          UOM_CLASS.UOM%TYPE;
   L_elc_ind              SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_tsf_price            TSFDETAIL.TSF_PRICE%TYPE;
   L_child_tsf_no         TSFHEAD.TSF_NO%TYPE;
   L_finisher_loc_ind     VARCHAR(1);
   L_finisher_entity_ind  VARCHAR(1);

   L_dept                 DEPS.DEPT%TYPE;
   L_class                CLASS.CLASS%TYPE;
   L_subclass             SUBCLASS.SUBCLASS%TYPE;

   L_wac_cost_from        ITEM_LOC_SOH.AV_COST%TYPE;
   L_tsf_cost             TSFDETAIL.TSF_COST%TYPE;
   L_mrt_no               TSFHEAD.MRT_NO%TYPE;
   L_mrt_item_row         MRT_ITEM%ROWTYPE;
   L_exists               BOOLEAN;
   L_tsfinv_not_found     BOOLEAN;

   L_add_tax_to_retail    VARCHAR2(1);
   L_from_entity_type     VARCHAR2(6);
   L_to_entity_type       VARCHAR2(6);
   L_tax_amount           ORDLOC.UNIT_COST%TYPE;
   L_order_currency       ORDHEAD.CURRENCY_CODE%TYPE;
   L_exchange_rate        ORDHEAD.EXCHANGE_RATE%TYPE;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_oms_ind              SYSTEM_OPTIONS.OMS_IND%TYPE;
   L_vwh_ind              VARCHAR2(1)                := 'N';

   cursor C_ORDER_CURRENCY is
      select oh.currency_code,
             oh.exchange_rate
        from ordhead oh,
             tsfhead th
       where th.tsf_no = I_tsf_no
         and th.order_no = oh.order_no;

   cursor C_DETAILS is
      select td.tsf_seq_no,
             td.item,
             im.pack_ind,
             NVL(td.tsf_price,0) tsf_price,
             td.tsf_cost,
             td.tsf_qty
        from tsfdetail td,
             item_master im
       where tsf_no = I_tsf_no
         and td.item = im.item;

   cursor C_GET_INV_FLOW is
      select from_loc,
             to_loc,
             from_loc_type,
             to_loc_type,
             tsf_qty
        from shipitem_inv_flow
       where tsf_no     = I_tsf_no
         and tsf_seq_no = L_tsf_seq_no
         and item       = L_item;

   cursor C_GET_SHIPSKU_COST_RETAIL is
      select s.unit_cost,
             s.unit_retail
        from shipsku s,
             shipment m
       where s.item      = L_item
         and s.shipment  = m.shipment
         and s.distro_no = I_tsf_no;

   cursor C_GET_TSFINV_FLOW is
      select from_loc,
             to_loc,
             from_loc_type,
             to_loc_type,
             tsf_qty
        from tsfitem_inv_flow
       where tsf_no     = I_tsf_no
         and tsf_seq_no = L_tsf_seq_no
         and item       = L_item;

   cursor C_GET_UPCHRG is
      select comp_rate
        from tsfdetail_chrg tc
       where tc.tsf_no = I_tsf_no
         and tc.tsf_seq_no = L_tsf_seq_no;

   cursor C_GET_WAC is
      select av_cost
        from item_loc_soh
       where item = L_item
         and loc = I_from_loc
         and loc_type = I_from_loc_type;

   cursor C_GET_PACK_WAC is
      select sum(vpq.qty * ils.av_cost) av_cost 
        from v_packsku_qty vpq,
             item_loc_soh ils
       where vpq.pack_no = L_item
         and ils.item = vpq.item
         and ils.loc = I_from_loc;

   cursor C_CHK_VWH is
      select 'Y'
        from tsfhead th,
             wh wh
       where th.tsf_no = I_tsf_no
         and th.from_loc = I_from_loc
         and wh.wh = th.from_loc
         and wh.wh <> wh.physical_wh;

BEGIN
   O_total_cost_from              := 0;
   O_total_cost_to                := 0;
   O_total_cost_prim              := 0;
   O_total_chrg_from              := 0;
   O_total_chrg_to                := 0;
   O_total_chrg_prim              := 0;
   O_total_retail_incl_vat_from   := 0;
   O_total_retail_incl_vat_to     := 0;
   O_total_retail_incl_vat_prim   := 0;

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   L_elc_ind := L_system_options_row.elc_ind;
   L_oms_ind := L_system_options_row.oms_ind;

   if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                     L_finisher_loc_ind,
                                     L_finisher_entity_ind,
                                     I_tsf_no) = FALSE then
      return FALSE;
   end if;

   if I_from_loc_type = 'S' then
      L_from_entity_type  := 'ST';
   elsif I_from_loc_type = 'W' then
      L_from_entity_type  := 'WH';
   elsif I_from_loc_type = 'E' then
      L_from_entity_type  := 'P';
   end if;

   if I_to_loc_type = 'S' then
      L_to_entity_type  := 'ST';
   elsif I_to_loc_type = 'W' then
      L_to_entity_type  := 'WH';
   elsif I_to_loc_type = 'E' then
      L_to_entity_type  := 'P';
   end if;

   -- calculate total cost, total retail (with vat), and upcharges
   -- for each item in the transfer
   FOR C_rec in C_DETAILS LOOP

      L_item         := C_rec.item;
      L_pack_ind     := C_rec.pack_ind;
      L_tsf_seq_no   := C_rec.tsf_seq_no;
      L_tsf_price    := C_rec.tsf_price;
      L_qty          := C_rec.tsf_qty;

      L_tsf_cost     := C_rec.tsf_cost;
      L_tax_amount   := 0;

      -- get item hier for tax calculation calls
      if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                        L_item,
                                        L_dept,
                                        L_class,
                                        L_subclass) = FALSE then
         return FALSE;
      end if;

      -- get mrt item information
      if L_mrt_no is NOT NULL then
         if MRT_ATTRIB_SQL.GET_MRT_ITEM_ROW(O_error_message,
                                            L_exists,
                                            L_mrt_item_row,
                                            L_mrt_no,
                                            L_item) = FALSE then
            return FALSE;
         end if;
      else
         L_mrt_item_row.restock_pct := NULL;
      end if;

      if I_tsf_type = 'IC' then
         -- if this is an intercompany transfer
         -- calculate cost using tsfdetail.tsf_price from the 1st leg of the transfer

         O_total_cost_from := O_total_cost_from + ((1 - (NVL(L_mrt_item_row.restock_pct,0)/100)) * C_rec.tsf_price * L_qty);
         -- calculate retail
         if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                     L_item,
                                                     I_from_loc,
                                                     I_from_loc_type,
                                                     L_av_cost_from,
                                                     L_unit_cost_from,
                                                     L_unit_retail_from,
                                                     L_selling_unit_retail,
                                                     L_selling_uom) = FALSE then
            return FALSE;
         end if;

         if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                             L_unit_retail_from,
                                             L_item,
                                             L_dept,
                                             L_class,
                                             I_from_loc,
                                             I_from_loc_type,
                                             LP_vdate,
                                             L_unit_retail_from) = FALSE then
            return FALSE;
         end if;

         O_total_retail_incl_vat_from := O_total_retail_incl_vat_from + (L_unit_retail_from * L_qty);
         ---
      elsif I_tsf_type = 'SG' then
         -- if this is a system generated transfer
         -- total cost is equal to the tsf_price (at the order currency) * transfer qty.
         open C_ORDER_CURRENCY;
         fetch C_ORDER_CURRENCY into L_order_currency,
                                     L_exchange_rate;
         close C_ORDER_CURRENCY;
         ---
         O_total_cost_from := O_total_cost_from + (C_rec.tsf_price * L_qty);
         ---
         -- convert total cost from 'from' local currency to primary.
         ---
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             O_total_cost_from,
                                             O_total_cost_prim,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
         -- convert total cost from 'from' local currency to 'to' local currency
         ---
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             O_total_cost_from,
                                             O_total_cost_to,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                     L_item,
                                                     I_to_loc,
                                                     I_to_loc_type,
                                                     L_av_cost_to,
                                                     L_unit_cost_to,
                                                     L_unit_retail_to,
                                                     L_selling_unit_retail,
                                                     L_selling_uom) = FALSE then
            return FALSE;
         end if;
         ---
         if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                             L_unit_retail_to,
                                             L_item,
                                             L_dept,
                                             L_class,
                                             I_to_loc,
                                             I_to_loc_type,
                                             LP_vdate,
                                             L_unit_retail_to) = FALSE then
            return FALSE;
         end if;

         O_total_retail_incl_vat_to := O_total_retail_incl_vat_to + (L_unit_retail_to * L_qty);

         ---
         -- convert total retail with vat from 'to' local currency to primary currency
         ---
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             O_total_retail_incl_vat_to,
                                             O_total_retail_incl_vat_prim,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
         -- convert total retail with vat from 'to' local currency to 'from' local currency
         ---
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             O_total_retail_incl_vat_to,
                                             O_total_retail_incl_vat_from,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
      else
         -- else this is an intracompany tsf
         -- use cost from shipsku
         open C_GET_SHIPSKU_COST_RETAIL;
         fetch C_GET_SHIPSKU_COST_RETAIL into L_av_cost_from,
                                              L_unit_retail_from;
         ---
         if C_GET_SHIPSKU_COST_RETAIL%NOTFOUND then

            close C_GET_SHIPSKU_COST_RETAIL;
            -- 'EG' = Externally Generated
            if I_tsf_type = 'EG' or (I_tsf_type = 'CO' and L_oms_ind = 'Y' and I_from_loc_type = 'W') then
               -- loop through all of the inventory flows to
               -- calculate cost and retail
               L_tsfinv_not_found := TRUE;
               L_unit_retail_from := 0;
               FOR L_rec in C_GET_TSFINV_FLOW LOOP
                  if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                              L_item,
                                                              L_rec.from_loc,
                                                              L_rec.from_loc_type,
                                                              L_av_cost_from,
                                                              L_loc_unit_cost_from,
                                                              L_loc_unit_retail_from,
                                                              L_selling_unit_retail,
                                                              L_selling_uom) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                                      L_loc_unit_retail_from,
                                                      L_item,
                                                      L_dept,
                                                      L_class,
                                                      L_rec.from_loc,
                                                      L_rec.from_loc_type,
                                                      LP_vdate,
                                                      L_loc_unit_retail_from) = FALSE then
                     return FALSE;
                  end if;

                  L_av_cost_from       := L_av_cost_from + (L_av_cost_from * L_rec.tsf_qty);
                  L_unit_retail_from   := L_unit_retail_from + (L_loc_unit_retail_from * L_rec.tsf_qty);
                  L_tsfinv_not_found   := FALSE;
               END LOOP;
               ---
               if L_tsfinv_not_found then
                  FOR L_rec in C_GET_INV_FLOW LOOP
                     if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                                 L_item,
                                                                 L_rec.from_loc,
                                                                 L_rec.from_loc_type,
                                                                 L_av_cost_from,
                                                                 L_loc_unit_cost_from,
                                                                 L_loc_unit_retail_from,
                                                                 L_selling_unit_retail,
                                                                 L_selling_uom) = FALSE then
                        return FALSE;
                     end if;

                     if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                                         L_loc_unit_retail_from,
                                                         L_item,
                                                         L_dept,
                                                         L_class,
                                                         L_rec.from_loc,
                                                         L_rec.from_loc_type,
                                                         LP_vdate,
                                                         L_loc_unit_retail_from) = FALSE then
                        return FALSE;
                     end if;

                     L_av_cost_from       := L_av_cost_from + (L_av_cost_from * L_rec.tsf_qty);
                     L_unit_retail_from   := L_unit_retail_from + (L_loc_unit_retail_from * L_rec.tsf_qty);

                  END LOOP;
               end if;
               L_av_cost_from       := L_av_cost_from   / L_qty;
               L_unit_retail_from   := L_unit_retail_from / L_qty;
               if I_tsf_type = 'CO' then
                  open C_CHK_VWH;
                  fetch C_CHK_VWH into L_vwh_ind;
                  close C_CHK_VWH;
                  if L_vwh_ind = 'Y' then
                     if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                                 L_item,
                                                                 I_from_loc,
                                                                 I_from_loc_type,
                                                                 L_av_cost_from,
                                                                 L_loc_unit_cost_from,
                                                                 L_unit_retail_from,
                                                                 L_selling_unit_retail,
                                                                 L_selling_uom) = FALSE then
                        return FALSE;
                     end if;

                     if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                                         L_unit_retail_from,
                                                         L_item,
                                                         L_dept,
                                                         L_class,
                                                         I_from_loc,
                                                         I_from_loc_type,
                                                         LP_vdate,
                                                         L_unit_retail_from) = FALSE then
                        return FALSE;
                     end if;   
                  end if;
               end if;
            else
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           L_item,
                                                           I_from_loc,
                                                           I_from_loc_type,
                                                           L_av_cost_from,
                                                           L_unit_cost_from,
                                                           L_unit_retail_from,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
            end if;
         else
            close C_GET_SHIPSKU_COST_RETAIL;
         end if;

         if L_tsf_cost is NOT NULL then
            if L_mrt_no is NOT NULL then
               L_av_cost_from   := (1 - (L_mrt_item_row.restock_pct/100)) * L_tsf_cost;
            else
               L_av_cost_from   := L_tsf_cost;
            end if;
         else
            if nvl(L_av_cost_from,-1) = -1 then
               if L_pack_ind = 'Y' then
                  open C_GET_PACK_WAC;
                  fetch C_GET_PACK_WAC into L_av_cost_from;
                  close C_GET_PACK_WAC;
               else
                  open C_GET_WAC;
                  fetch C_GET_WAC into L_av_cost_from;
                  close C_GET_WAC;
  
                  if L_mrt_no is NOT NULL then
                     L_av_cost_from   := (1 - (L_mrt_item_row.restock_pct/100)) * L_av_cost_from;
                  end if;
               end if;               
            end if;
         end if;
         if I_tsf_type <> 'EG' or (I_tsf_type <> 'CO' and L_oms_ind <> 'Y' and I_from_loc_type <> 'W') then
            if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                                L_unit_retail_from,
                                                L_item,
                                                L_dept,
                                                L_class,
                                                I_from_loc,
                                                I_from_loc_type,
                                                LP_vdate,
                                                L_unit_retail_from) = FALSE then
               return FALSE;
            end if;
         end if;

         O_total_cost_from              := O_total_cost_from + (L_av_cost_from * L_qty);
         O_total_retail_incl_vat_from   := O_total_retail_incl_vat_from + (L_unit_retail_from * L_qty);

      end if;

      -- calculate upcharges
      if L_elc_ind = 'Y' then
         if I_tsf_type != 'SG' then
            if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_CHRGS(O_error_message,
                                                       L_up_chrg_prim,
                                                       L_profit_chrg_loc,
                                                       L_exp_chrg_loc,
                                                       'T',
                                                       I_tsf_no,
                                                       I_tsf_type,
                                                       L_tsf_seq_no,
                                                       L_item,
                                                       NULL,
                                                       I_from_loc,
                                                       I_from_loc_type,
                                                       I_to_loc,
                                                       I_to_loc_type) = FALSE then
               return FALSE;
            end if;
            O_total_chrg_prim := O_total_chrg_prim + (L_up_chrg_prim * L_qty);
         else
            -- Accumulate all the upcharges
            FOR c_getup in C_GET_UPCHRG LOOP
               O_total_chrg_prim := O_total_chrg_prim + O_total_cost_prim * (NVL(c_getup.comp_rate, 0)/100);
            END LOOP;

         end if;
      end if;
   END LOOP;  -- C_DETAILS

   if I_tsf_type != 'SG' then
      ---
      -- convert total cost from 'from' local currency to primary currency
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          I_from_loc,
                                          I_from_loc_type,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          O_total_cost_from,
                                          O_total_cost_prim,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      -- convert total cost from 'from' local currency to 'to' local currency
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          I_from_loc,
                                          I_from_loc_type,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          O_total_cost_from,
                                          O_total_cost_to,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      -- convert total retail with vat from 'from' local currency to primary currency
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          I_from_loc,
                                          I_from_loc_type,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          O_total_retail_incl_vat_from,
                                          O_total_retail_incl_vat_prim,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      -- convert total retail with vat from 'from' local currency to 'to' local currency
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          I_from_loc,
                                          I_from_loc_type,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          O_total_retail_incl_vat_from,
                                          O_total_retail_incl_vat_to,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- only convert the total up charges if system ELC_IND = Y.
   ---
   if L_elc_ind = 'Y' then
      if O_total_chrg_prim != 0 then
         ---
         -- convert total charges from primary to 'from' local currency.
         ---
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             O_total_chrg_prim,
                                             O_total_chrg_from,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
         -- convert total charges to 'to' local currency.
         ---
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             O_total_chrg_prim,
                                             O_total_chrg_to,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END TOTAL_COST_RETAIL;
--------------------------------------------------------------------------------
FUNCTION GET_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_transfer_type   IN OUT   TSFHEAD.TSF_TYPE%TYPE,
                    O_status          IN OUT   TSFHEAD.STATUS%TYPE,
                    I_tsf_no          IN OUT   TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.GET_STATUS';

   cursor C_GET_TSF_INFO is
      select tsf_type,
             status
        from tsfhead
       where tsf_no = I_tsf_no;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_tsf_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TSF_INFO',
                    'TSFHEAD',
                    'Transfer no: '||to_char(I_tsf_no));
   open C_GET_TSF_INFO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TSF_INFO',
                    'TSFHEAD',
                    'Transfer no: '||to_char(I_tsf_no));
   fetch C_GET_TSF_INFO into O_transfer_type,
                             O_status;
   ---
   if C_GET_TSF_INFO%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_TSF_INFO',
                       'TSFHEAD',
                       NULL);
      close C_GET_TSF_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TSF_INFO',
                    'TSFHEAD',
                    NULL);
   close C_GET_TSF_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_STATUS;
------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATIONS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_same_physical_wh   IN OUT   VARCHAR2,
                            I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                            I_to_loc             IN       TSFHEAD.TO_LOC%TYPE)

RETURN BOOLEAN IS

   L_program    VARCHAR2(64)   := 'TRANSFER_SQL.VALIDATE_LOCATIONS';
   L_pwh_from   WH.WH%TYPE;
   L_pwh_to     WH.WH%TYPE;

   cursor C_GET_PWH (L_location ITEM_LOC.LOC%TYPE) is
      select physical_wh
        from wh
       where wh               = L_location
         and stockholding_ind = 'Y';

BEGIN
   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_from_loc',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_to_loc',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PWH',
                    'WH',
                    NULL);
   open C_GET_PWH (I_from_loc);
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PWH',
                    'WH',
                    NULL);
   fetch C_GET_PWH into L_pwh_from;
   ---
   if C_GET_PWH%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PWH',
                       'WH',
                       NULL);
      close C_GET_PWH;
      O_error_message := SQL_LIB.CREATE_MSG('INV_WH',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PWH',
                       'WH',
                       NULL);
      close C_GET_PWH;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PWH',
                    'WH',
                    NULL);
   open C_GET_PWH (I_to_loc);
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PWH',
                    'WH',
                    NULL);
   fetch C_GET_PWH into L_pwh_to;
   ---
   if C_GET_PWH%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PWH',
                       'WH',
                       NULL);
      close C_GET_PWH;
      O_error_message := SQL_LIB.CREATE_MSG('INV_WH',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PWH',
                       'WH',
                       NULL);
      close C_GET_PWH;
   end if;
   ----
   if L_pwh_from = L_pwh_to then
      O_same_physical_wh   := 'Y';
   else
      O_same_physical_wh   := 'N';
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_LOCATIONS;
------------------------------------------------------------------------------------
FUNCTION VALIDATE_F_STORES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_sf_store            IN OUT   VARCHAR2,
                            I_from_loc            IN       TSFHEAD.FROM_LOC%TYPE,
                            I_to_loc              IN       TSFHEAD.TO_LOC%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64)   := 'TRANSFER_SQL.VALIDATE_F_STORES';
   L_from_store_type         STORE.STORE_TYPE%TYPE;
   L_from_store_sh_ind       STORE.STOCKHOLDING_IND%TYPE;
   L_to_store_type           STORE.STORE_TYPE%TYPE;
   L_to_store_sh_ind         STORE.STOCKHOLDING_IND%TYPE;

   cursor C_CHECK_SHF is
      select store,
             store_type,
             stockholding_ind
        from store
       where store in (I_from_loc, I_to_loc);

BEGIN
  /*****************************************************************************
   * This function checks whether a From/To location combination is valid for a
   * transfer that involves a stockholding franchise (SHF) store.
   * If 'N' is returned for O_sf_store, then O_error_message will be populated
   * with an error message.
   *
   * Returned value of O_sf_store based on input values:
   * 1) I_from_loc are both SHF stores --> O_sf_store = 'Y'
   * 2) I_from_loc is a SFH store, I_to_loc is NULL --> O_sf_store = 'Y'
   * 3) I_from_loc is NULL, I_to_loc is a SHF store --> O_sf_store = 'Y'
   * 4) I_from_loc is a SHF store, I_to_loc is NOT a SHF store --> O_sf_store = 'N'
   * 5) I_from_loc is NOT a SHF store, I_to_loc is a SHF store --> O_sf_store = 'N'
   * 6) I_from_loc is NOT a SHF store, I_to_loc is NOT a SHF store --> O_sf_store = NULL
   * 7) I_from_loc is NOT a SHF store, I_to_loc is NULL --> O_sf_store = NULL
   * 8) I_from_loc is NULL, I_to_loc is NOT a SHF store --> O_sf_store = NULL
   * 9) I_from_loc is NULL, I_to_loc is NULL --> O_sf_store = NULL
   *
   ****************************************************************************/

   O_sf_store := NULL; -- Will be returned as NULL if neither location is a SHF store

   FOR store_rec in C_CHECK_SHF LOOP
      if store_rec.store = I_from_loc then
         L_from_store_type := store_rec.store_type;
         L_from_store_sh_ind := store_rec.stockholding_ind;
      elsif store_rec.store = I_to_loc then
         L_to_store_type := store_rec.store_type;
         L_to_store_sh_ind := store_rec.stockholding_ind;
      end if;
   END LOOP;

   if L_from_store_type = 'F' and L_from_store_sh_ind = 'Y' then
      if (L_to_store_type = 'F' and L_to_store_sh_ind = 'Y') or
         I_to_loc is NULL then
         -- Both locations are stockholding franchise stores, or the
         -- From loc is a SF store and NULL was passed for the To loc
         O_sf_store := 'Y';
      else
         -- The To loc is not a stockholding franchise store and is not null
         O_sf_store := 'N';
         O_error_message := SQL_LIB.CREATE_MSG('TSF_BOTH_F_STORES',
                                               I_from_loc,
                                               NULL,
                                               NULL);
      end if;
   elsif L_to_store_type = 'F' and L_to_store_sh_ind = 'Y' then
      if I_from_loc is NULL then
         -- NULL was passed for the From loc and the To loc is a SF store
         O_sf_store := 'Y';
      else
         -- The From loc is not a stockholding franchise store and is not null
         O_sf_store := 'N';
         O_error_message := SQL_LIB.CREATE_MSG('TSF_BOTH_F_STORES',
                                               I_to_loc,
                                               NULL,
                                               NULL);
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_F_STORES;
------------------------------------------------------------------------------------
FUNCTION MANUAL_BT_EXECUTE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE,
                           I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                           I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                           I_transfer_type   IN       TSFHEAD.TSF_TYPE%TYPE)

RETURN BOOLEAN IS

   L_program      VARCHAR2(64)   := 'TRANSFER_SQL.MANUAL_BT_EXECUTE';
   L_inv_status   TSFDETAIL.INV_STATUS%TYPE;

   cursor C_GET_TSF_ITEM_QTY is
      select t.item,
             t.tsf_qty,
             t.inv_status,
             i.pack_ind,
             i.dept,
             i.class,
             i.subclass
        from tsfdetail t,
             item_master i
       where t.tsf_no = I_tsf_no
         and t.item   = i.item;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_tsf_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_from_loc',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_to_loc',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_transfer_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_transfer_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   FOR C_rec in C_GET_TSF_ITEM_QTY LOOP
      ---
      if I_transfer_type = 'BT' then
         L_inv_status := C_rec.inv_status;
      else
         L_inv_status := NULL;
      end if;
      ---
      if TSF_BT_SQL.BT_EXECUTE(O_error_message,
                               C_rec.item,
                               C_rec.pack_ind,
                               C_rec.dept,
                               C_rec.class,
                               C_rec.subclass,
                               I_from_loc,
                               I_to_loc,
                               C_rec.tsf_qty,
                               L_inv_status,
                               I_tsf_no) = FALSE then
         return FALSE;
      end if;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MANUAL_BT_EXECUTE;
------------------------------------------------------------------------------------
FUNCTION AUTOMATIC_BT_EXECUTE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item            IN       TSFDETAIL.ITEM%TYPE,
                              I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                              I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                              I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE)

RETURN BOOLEAN IS

   L_program    VARCHAR2(64)   := 'TRANSFER_SQL.AUTOMATIC_BT_EXECUTE';
   L_dept       ITEM_MASTER.DEPT%TYPE;
   L_class      ITEM_MASTER.CLASS%TYPE;
   L_subclass   ITEM_MASTER.SUBCLASS%TYPE;
   L_pack_ind   ITEM_MASTER.PACK_IND%TYPE;

   cursor C_GET_ITEM_INFO is
      select pack_ind,
             dept,
             class,
             subclass
        from item_master
       where item = I_item;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_from_loc',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_to_loc',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_tsf_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_tsf_qty',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ITEM_INFO',
                    'ITEM_MASTER',
                    NULL);
   open C_GET_ITEM_INFO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ITEM_INFO',
                    'ITEM_MASTER',
                    NULL);
   fetch C_GET_ITEM_INFO into L_pack_ind,
                              L_dept,
                              L_class,
                              L_subclass;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ITEM_INFO',
                    'ITEM_MASTER',
                    NULL);
   close  C_GET_ITEM_INFO;
   ---
   if TSF_BT_SQL.BT_EXECUTE(O_error_message,
                            I_item,
                            L_pack_ind,
                            L_dept,
                            L_class,
                            L_subclass,
                            I_from_loc,
                            I_to_loc,
                            I_tsf_qty,
                            NULL,
                            NULL) = FALSE then -- tsf_no
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END AUTOMATIC_BT_EXECUTE;
-----------------------------------------------------------------------------------
FUNCTION NEXT_TSF_PO_LINK_NO(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tsf_po_link_no   IN OUT   TSFDETAIL.TSF_PO_LINK_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.NEXT_TSF_PO_LINK_NO';

   cursor C_NEXT_TSF_NO is
      select tsf_po_link_no_sequence.NEXTVAL
        from dual;

BEGIN
   ---
   -- Fetch the next po_tsf_link_no into the output variable.
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_NEXT_TSF_NO',
                    'DUAL',
                    NULL);
   open C_NEXT_TSF_NO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_NEXT_TSF_NO',
                    'DUAL',
                    NULL);
   fetch C_NEXT_TSF_NO into O_tsf_po_link_no;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_NEXT_TSF_NO',
                    'DUAL',
                    NULL);
   close  C_NEXT_TSF_NO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END NEXT_TSF_PO_LINK_NO;
-------------------------------------------------------------------------------------
FUNCTION DELETE_TSF_QTYS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.DELETE_TSF_QTYS';

BEGIN
   LP_table := 'TSFDETAIL';
   open C_LOCK_TSFDETAIL(I_tsf_no);
   close C_LOCK_TSFDETAIL;
   ---
   update tsfdetail
      set tsf_qty = 0,
          updated_by_rms_ind = 'Y'
    where tsf_no  = I_tsf_no;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_tsf_no),
                                            NULL);

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_TSF_QTYS;
-----------------------------------------------------------------------------------------
FUNCTION GET_TSFDETAIL_TSF_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_tsf_qty         IN OUT   TSFDETAIL.TSF_QTY%TYPE,
                               I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                               I_tsf_seq_no      IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                               I_item            IN       TSFDETAIL.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.GET_TSFDETAIL_TSF_QTY';

   cursor C_GET_TSF_QTY is
      select tsf_qty
        from tsfdetail
       where tsf_no     = I_tsf_no
         and tsf_seq_no = NVL(I_tsf_seq_no, tsf_seq_no)
         and item       = NVL(I_item, item);

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_tsf_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TSF_QTY',
                    'TSFDETAIL',
                    NULL);
   open C_GET_TSF_QTY;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TSF_QTY',
                    'TSFDETAIL',
                    NULL);
   fetch C_GET_TSF_QTY into O_tsf_qty;
   ---
   if C_GET_TSF_QTY%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_TSF_QTY',
                       'TSFDETAIL',
                       NULL);
      close C_GET_TSF_QTY;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TSF_QTY',
                    'TSFDETAIL',
                    NULL);
   close C_GET_TSF_QTY;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_TSFDETAIL_TSF_QTY;
---------------------------------------------------------------------------------
FUNCTION CHECK_DETAILS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60)   := 'TRANSFER_SQL.CHECK_DETAILS';
   L_count     NUMBER(4)      := NULL;

   cursor C_CHECK_DETAILS is
      select count(*)
        from tsfdetail
       where tsf_no = I_tsf_no;

BEGIN
   --- check input parameter
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DETAILS',
                    'TSFDETAIL',
                    'Transfer: '||to_char(I_tsf_no));
   open C_CHECK_DETAILS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DETAILS',
                    'TSFDETAIL',
                    'Transfer: '||to_char(I_tsf_no));
   fetch C_CHECK_DETAILS into L_count;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DETAILS',
                    'TSFDETAIL',
                    'Transfer: '||to_char(I_tsf_no));
   close C_CHECK_DETAILS;
   ---
   if L_count = 1 then
      O_error_message := SQL_LIB.CREATE_MSG('TSF_NO_DEL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DETAILS;
--------------------------------------------------------------------------------------
FUNCTION DELETE_DOC_CLOSE_QUEUE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'TRANSFER_SQL.DELETE_DOC_CLOSE_QUEUE';

   cursor C_LOCK_DOC_CLOSE_Q is
      select 'x'
        from doc_close_queue
       where doc = I_tsf_no
         and doc_type = 'T'
         for update nowait;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open  C_LOCK_DOC_CLOSE_Q;
   close C_LOCK_DOC_CLOSE_Q;

   delete from doc_close_queue
    where doc      = I_tsf_no
      and doc_type = 'T';

return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;

END DELETE_DOC_CLOSE_QUEUE;
-------------------------------------------------------------------------------------
FUNCTION UOT_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists          IN OUT   BOOLEAN,
                   I_item            IN       TSFDETAIL.ITEM%TYPE,
                   I_uot             IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.UOT_EXIST';
   L_code      VARCHAR(2)     := 'Y';

   cursor C_VALIDATE_UOT is
      select 'X'
         from item_master im,
              uom_class uc
        where im.standard_uom = uc.uom
          and im.item         = I_item
          and uc.uom          = I_uot
          and rownum          = 1
       union
       select 'X'
         from code_detail
        where code_type = 'PALN'
          and code in (select isp.pallet_name
                         from item_supplier isp,
                              item_supp_country isc
                        where isp.item     = isc.item
                          and isp.item     = I_item
                          and isp.supplier = isc.supplier
                          and isc.primary_supp_ind = 'Y')
          and code   = I_uot
          and rownum = 1
       union
       select 'X'
         from code_detail
        where code_type = 'CASN'
          and code in (select isp.case_name
                         from item_supplier isp,
                              item_supp_country isc
                        where isp.item     = isc.item
                          and isp.item     = I_item
                          and isp.supplier = isc.supplier
                          and isc.primary_supp_ind = 'Y')
           and code   = I_uot
           and rownum = 1   ;

BEGIN
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_VALIDATE_UOT',
                    'CODE_DETAIL',
                    'ITEM: '||(I_item)||'uot: '|| (I_uot));
   open C_VALIDATE_UOT;
   SQL_LIB.SET_MARK('FETCH',
                    'C_VALIDATE_UOT',
                    'CODE_DETAIL',
                    'ITEM: '||(I_item)||'uot: '|| (I_uot));
   fetch C_VALIDATE_UOT into L_code;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_VALIDATE_UOT',
                    'CODE_DETAIL',
                    'Code: '||I_uot);
   close C_VALIDATE_UOT;
   ---
   O_exists := (L_code = 'X');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;

END UOT_EXIST;
---------------------------------------------------------------------------------------------
FUNCTION WORK_ORDER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_no          IN       TSF_WO_HEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'TRANSFER_SQL.WORK_ORDER_EXISTS';
   L_exists    VARCHAR2(1)    := 'N';
   L_item      TSFDETAIL.ITEM%TYPE;

   cursor C_GET_TSF_ITEMS is
      select item
        from tsfdetail
       where tsf_no = I_tsf_no;

   cursor C_WORK_ORDER_EXISTS is
      select 'X'
        from tsf_wo_head h,
             tsf_wo_detail d
       where h.tsf_no    = I_tsf_no
         and h.tsf_wo_id = d.tsf_wo_id
         and d.item      = L_item
         and d.activity_id is NOT NULL;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   FOR rec in C_GET_TSF_ITEMS LOOP
      L_item   := rec.item;
      L_exists := 'N';
      open C_WORK_ORDER_EXISTS;
      fetch C_WORK_ORDER_EXISTS into L_exists;
      if L_exists != 'X' then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_WO_ASSOC',
                                               L_item,
                                               NULL,
                                               NULL);
         close C_WORK_ORDER_EXISTS;
         return FALSE;
      end if;
      close C_WORK_ORDER_EXISTS;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WORK_ORDER_EXISTS;
-------------------------------------------------------------------------------------
FUNCTION GET_TSFHEAD_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_tsfhead         IN OUT   V_TSFHEAD%ROWTYPE,
                          I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'TRANSFER_SQL.GET_TSFHEAD_INFO';
   L_exists    VARCHAR2(1)    := 'N';

   cursor C_TSFHEAD_BASE is
      select 'Y'
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_TSFHEAD_V is
      select *
        from v_tsfhead
       where tsf_no = I_tsf_no
   UNION ALL
      select *
        from v_tsfhead
       where child_tsf_no = I_tsf_no;

BEGIN
   -- Because this function retrieves information from V_TSFHEAD users can only
   -- view it if their security allows it.

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_TSFHEAD_V;
   fetch C_TSFHEAD_V into O_tsfhead;
   close C_TSFHEAD_V;

   if O_tsfhead.tsf_no is NULL then
      -- view cursor found no records
      open C_TSFHEAD_BASE;
      fetch C_TSFHEAD_BASE into L_exists;
      close C_TSFHEAD_BASE;

      if L_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ACCESS_TSF',
                                               I_tsf_no,
                                               NULL,
                                               NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER',
                                               NULL,
                                               NULL,
                                               NULL);
      end if;

      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;

END GET_TSFHEAD_INFO;
-------------------------------------------------------------------------------------
FUNCTION CHECK_F_TSF_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_f_tsf_ind       IN OUT   BOOLEAN,
                              I_shipment_no     IN       V_SHIPSKU.SHIPMENT%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50)               := 'TRANSFER_SQL.CHECK_F_TSF_SHIPMENT';
   L_fr_tsf_exists   VARCHAR2(1);

   cursor C_SHIPSKU is
      select 'Y'
        from shipsku ss, tsfhead th
       where ss.shipment = I_shipment_no
         and ss.distro_type = 'T'
         and ss.distro_no = th.tsf_no
         and (th.wf_order_no is NOT NULL or
              th.rma_no is NOT NULL)
         and rownum = 1;

BEGIN
   if I_shipment_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_shipment_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   O_f_tsf_ind := FALSE;

   open C_SHIPSKU;
   fetch C_SHIPSKU into L_fr_tsf_exists;
   if C_SHIPSKU%FOUND = TRUE then
      O_f_tsf_ind := TRUE;
   end if;
   close C_SHIPSKU;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_SHIPSKU%ISOPEN then
         close C_SHIPSKU;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_F_TSF_SHIPMENT;
-------------------------------------------------------------------------------------
FUNCTION CREATE_TSFHEAD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tsf_no                  IN       TSFHEAD.TSF_NO%TYPE,
                        I_tsf_type                IN       TSFHEAD.TSF_TYPE%TYPE,
                        I_exp_dc_date             IN       TSFHEAD.EXP_DC_DATE%TYPE,
                        I_dept                    IN       TSFHEAD.DEPT%TYPE,
                        I_inventory_type          IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                        I_comments                IN       TSFHEAD.COMMENT_DESC%TYPE,
                        I_ext_ref_no              IN       TSFHEAD.EXT_REF_NO%TYPE,
                        I_from_loc                IN       TSFHEAD.FROM_LOC%TYPE,
                        I_from_loc_type           IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                        I_from_freight_code       IN       TSFHEAD.FREIGHT_CODE%TYPE,
                        I_from_routing_code       IN       TSFHEAD.ROUTING_CODE%TYPE,
                        I_finisher                IN       WH.WH%TYPE,
                        I_finisher_type           IN       VARCHAR2,
                        I_finisher_freight_code   IN       TSFHEAD.FREIGHT_CODE%TYPE,
                        I_finisher_routing_code   IN       TSFHEAD.ROUTING_CODE%TYPE,
                        I_to_loc                  IN       TSFHEAD.TO_LOC%TYPE,
                        I_to_loc_type             IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                        I_delivery_date           IN       TSFHEAD.DELIVERY_DATE%TYPE,
                        I_mrt_no                  IN       TSFHEAD.MRT_NO%TYPE         DEFAULT NULL,
                        I_not_after_date          IN       TSFHEAD.NOT_AFTER_DATE%TYPE DEFAULT NULL,
                        I_context_type            IN       TSFHEAD.CONTEXT_TYPE%TYPE   DEFAULT NULL,
                        I_context_value           IN       TSFHEAD.CONTEXT_VALUE%TYPE  DEFAULT NULL
                        )
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)  := 'TRANSFER_SQL.CREATE_TSFHEAD';
   L_from_loc_type      TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_same_physical_wh   VARCHAR2(1);
   L_valid              BOOLEAN;
   L_inv_parm           VARCHAR2(30)                 := NULL;
   L_exp_dc_eow_date    TSFHEAD.EXP_DC_EOW_DATE%TYPE := NULL;

BEGIN
   if I_tsf_no is NULL then
      L_inv_parm := 'I_tsf_no';
   elsif I_inventory_type is NULL then
      L_inv_parm := 'I_inventory_type';
   elsif I_from_loc_type is NULL then
      L_inv_parm := 'I_from_loc_type';
   elsif I_from_loc is NULL then
      L_inv_parm := 'I_from_loc';
   elsif I_from_freight_code is NULL then
      L_inv_parm := 'I_from_freight_code';
   end if;
   if L_inv_parm is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_exp_dc_date is NOT NULL then
      if DATES_SQL.GET_EOW_DATE(O_error_message,
                                L_exp_dc_eow_date,
                                I_exp_dc_date) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_finisher is NOT NULL or
      I_finisher_type is NOT NULL or
      I_finisher_freight_code is NOT NULL or
      I_finisher_routing_code is NOT NULL then  -- create two transfer header records
      ---
      -- Validate external partner if one was supplied
      if I_finisher_type = 'E' and I_finisher is NOT NULL then
         if PARTNER_SQL.VALID_ACTIVE(O_error_message,
                                     L_valid,
                                     I_finisher_type,
                                     I_finisher) = FALSE
                                  or L_valid = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('NO_ACTIVE_PARTNER',
                                                  I_finisher,
                                                  I_finisher_type,
                                                  NULL);
            return FALSE;
         end if;
      end if;
      -- create first tsf leg
      insert into tsfhead(tsf_no,
                          from_loc_type,
                          from_loc,
                          to_loc_type,
                          to_loc,
                          exp_dc_date,
                          exp_dc_eow_date,
                          dept,
                          inventory_type,
                          tsf_type,
                          status,
                          freight_code,
                          routing_code,
                          create_date,
                          create_id,
                          ext_ref_no,
                          repl_tsf_approve_ind,
                          comment_desc,
                          delivery_date,
                          mrt_no,
                          not_after_date,
                          context_type,
                          context_value
                         )
                    values(I_tsf_no,
                          I_from_loc_type,
                          I_from_loc,
                          decode(I_finisher_type, 'I', 'W', 'E', 'E'),
                          I_finisher,
                          I_exp_dc_date,
                          L_exp_dc_eow_date,
                          I_dept,
                          I_inventory_type,
                          I_tsf_type,
                          'I',
                          I_from_freight_code,
                          I_from_routing_code,
                          LP_vdate,
                          LP_USER,
                          I_ext_ref_no,
                          'N',
                          I_comments,
                          I_delivery_date,
                          I_mrt_no,
                          I_not_after_date,
                          I_context_type,
                          I_context_value
                         );
      ---
      -- create the second tsf leg
      select decode(I_finisher_type, 'I', 'W', 'E', 'E')
        into L_from_loc_type
        from dual;
      ---
      if CREATE_CHILD_TSFHEAD(O_error_message,
                              I_tsf_no,                  -- tsf parent no
                              I_tsf_type,
                              I_exp_dc_date,
                              L_exp_dc_eow_date,
                              I_dept,
                              I_inventory_type,
                              I_comments,
                              I_ext_ref_no,
                              I_finisher_freight_code,   -- freight code
                              I_finisher_routing_code,
                              I_finisher,                -- from loc
                              L_from_loc_type,           -- from_loc_type
                              I_to_loc,
                              I_to_loc_type,
                              I_delivery_date
                              ) = FALSE then
         return FALSE;
      end if;
      ---
   else  -- create one standard transfer header
      ---
      -- validate locations if both from/to loc_type are Warehouse except
      -- intercompany transfers can be performed in the same physical warehouse
      if I_from_loc_type = 'W' and
         I_to_loc_type = 'W' and
         I_tsf_type != 'IC' then
         ---
         if I_to_loc is NOT NULL then
            if VALIDATE_LOCATIONS(O_error_message,
                                  L_same_physical_wh,
                                  I_from_loc,
                                  I_to_loc) = FALSE then
               return FALSE;
            end if;
            ---
            if I_tsf_type in ('BT', 'NB') then
               if L_same_physical_wh = 'N' then
                  O_error_message := SQL_LIB.CREATE_MSG('SAME_PWH_FOR_BT',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;
            else
               if L_same_physical_wh = 'Y' and I_mrt_no is NULL then
                  O_error_message := SQL_LIB.CREATE_MSG('SAME_PWH_TSF_NO_ALLOW',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;
            end if;
         end if;
         ---
      end if;
      ---
      insert into tsfhead(tsf_no,
                          from_loc_type,
                          from_loc,
                          to_loc_type,
                          to_loc,
                          exp_dc_date,
                          exp_dc_eow_date,
                          dept,
                          inventory_type,
                          tsf_type,
                          status,
                          freight_code,
                          routing_code,
                          create_date,
                          create_id,
                          ext_ref_no,
                          repl_tsf_approve_ind,
                          comment_desc,
                          delivery_date,
                          mrt_no,
                          not_after_date,
                          context_type,
                          context_value
                          )
                   values(I_tsf_no,
                          I_from_loc_type,
                          I_from_loc,
                          I_to_loc_type,
                          I_to_loc,
                          I_exp_dc_date,
                          L_exp_dc_eow_date,
                          I_dept,
                          I_inventory_type,
                          I_tsf_type,
                          'I',
                          I_from_freight_code,
                          I_from_routing_code,
                          LP_vdate,
                          LP_USER,
                          I_ext_ref_no,
                          'N',
                          I_comments,
                          I_delivery_date,
                          I_mrt_no,
                          I_not_after_date,
                          I_context_type,
                          I_context_value
                          );
      ---
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;

END CREATE_TSFHEAD;
---------------------------------------------------------------------------------------
FUNCTION CREATE_CHILD_TSFHEAD(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tsf_parent_no     IN       TSFHEAD.TSF_PARENT_NO%TYPE,
                              I_tsf_type          IN       TSFHEAD.TSF_TYPE%TYPE,
                              I_exp_dc_date       IN       TSFHEAD.EXP_DC_DATE%TYPE,
                              I_exp_dc_eow_date   IN       TSFHEAD.EXP_DC_EOW_DATE%TYPE,
                              I_dept              IN       TSFHEAD.DEPT%TYPE,
                              I_inventory_type    IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                              I_comments          IN       TSFHEAD.COMMENT_DESC%TYPE,
                              I_ext_ref_no        IN       TSFHEAD.EXT_REF_NO%TYPE,
                              I_freight_code      IN       TSFHEAD.FREIGHT_CODE%TYPE,
                              I_routing_code      IN       TSFHEAD.ROUTING_CODE%TYPE,
                              I_from_loc          IN       TSFHEAD.FROM_LOC%TYPE,
                              I_from_loc_type     IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                              I_to_loc            IN       TSFHEAD.TO_LOC%TYPE,
                              I_to_loc_type       IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                              I_delivery_date     IN       TSFHEAD.DELIVERY_DATE%TYPE,
                              I_mrt_no            IN       TSFHEAD.MRT_NO%TYPE         DEFAULT NULL,
                              I_not_after_date    IN       TSFHEAD.NOT_AFTER_DATE%TYPE DEFAULT NULL,
                              I_context_type      IN       TSFHEAD.CONTEXT_TYPE%TYPE   DEFAULT NULL,
                              I_context_value     IN       TSFHEAD.CONTEXT_VALUE%TYPE  DEFAULT NULL
                              )
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)  := 'TRANSFER_SQL.CREATE_CHILD_TSFHEAD';
   L_tsf_no             TSFHEAD.TSF_NO%TYPE;
   L_return_code        VARCHAR2(5);
   L_same_physical_wh   VARCHAR2(1);
   L_valid              BOOLEAN;
   L_freight_code       TSFHEAD.FREIGHT_CODE%TYPE := I_freight_code;

BEGIN
   ---
   if I_tsf_parent_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_parent_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   -- Validate external partner if one was supplied
   if I_from_loc_type = 'E' and I_from_loc is NOT NULL then
      if PARTNER_SQL.VALID_ACTIVE(O_error_message,
                                  L_valid,
                                  I_from_loc_type,
                                  I_from_loc) = FALSE
                               or L_valid = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ACTIVE_PARTNER',
                                               I_from_loc,
                                               I_from_loc_type,
                                               NULL);
         return FALSE;
      end if;
   end if;
   ---
   -- If no freight code was entered, default 'N'ormal
   if L_freight_code is NULL then
      L_freight_code := 'N';
   end if;
   ---
   NEXT_TRANSFER_NUMBER(L_tsf_no,
                        L_return_code,
                        O_error_message);
   ---
   insert into tsfhead(tsf_no,
                       tsf_parent_no,
                       from_loc_type,
                       from_loc,
                       to_loc_type,
                       to_loc,
                       exp_dc_date,
                       exp_dc_eow_date,
                       dept,
                       inventory_type,
                       tsf_type,
                       status,
                       freight_code,
                       routing_code,
                       create_date,
                       create_id,
                       ext_ref_no,
                       repl_tsf_approve_ind,
                       comment_desc,
                       delivery_date,
                       mrt_no,
                       not_after_date,
                       context_type,
                       context_value
                       )
                values(L_tsf_no,
                       I_tsf_parent_no,
                       I_from_loc_type,
                       I_from_loc,
                       I_to_loc_type,
                       I_to_loc,
                       I_exp_dc_date,
                       I_exp_dc_eow_date,
                       I_dept,
                       I_inventory_type,
                       I_tsf_type,
                       'I',                -- status
                       L_freight_code,
                       I_routing_code,
                       LP_vdate,           -- create date
                       LP_USER,               -- create id
                       I_ext_ref_no,
                       'N',
                       I_comments,
                       I_delivery_date,
                       I_mrt_no,
                       I_not_after_date,
                       I_context_type,
                       I_context_value
                      );
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;

END CREATE_CHILD_TSFHEAD;
-- ----------------------------------------------------------------------------
-- ADJUST_EXP_QTY_FOR_NEW_LOC( )
-- When the to_loc is modified, this funciton adjusts the To Location expected
-- quantity for a transfer with finishing for items that have already be
-- received at the finisher.  This is not backed out when the transfer is
-- unapproved since it is based on the received quantity which can't be modified
-- ----------------------------------------------------------------------------

FUNCTION ADJUST_EXP_QTY_FOR_NEW_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                                    I_orig_to_loc     IN       TSFHEAD.TO_LOC%TYPE,
                                    I_new_to_loc      IN       TSFHEAD.TO_LOC%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(255)   := 'TRANSFER_SQL.ADJUST_EXP_QTY_FOR_NEW_LOC';
   L_table     VARCHAR2(30)    := 'ITEM_LOC_SOH';

   cursor C_GET_RECEIVED_ITEMS is
      select td.item to_item,
             td.received_qty update_qty
        from tsfdetail td, item_master im
       where td.tsf_no = I_tsf_no
         and td.received_qty > 0
         and im.item = td.item
         and im.pack_ind = 'N'
         and not exists (select 'X'
                           from tsf_xform tx, tsf_xform_detail txd
                          where tx.tsf_no = td.tsf_no
                            and txd.tsf_xform_id = tx.tsf_xform_id
                            and txd.from_item = td.item)
     union
      select txd.to_item,
             td.received_qty update_qty
        from tsfdetail td, item_master im, tsf_xform tx, tsf_xform_detail txd
       where td.tsf_no = I_tsf_no
         and td.received_qty > 0
         and im.item = td.item
         and im.pack_ind = 'N'
         and tx.tsf_no = td.tsf_no
         and txd.tsf_xform_id = tx.tsf_xform_id
         and txd.from_item = td.item
     union
      select txd.to_item,
             td.received_qty * vpq.qty update_qty
        from tsfdetail td, item_master im, v_packsku_qty vpq, tsf_xform tx, tsf_xform_detail txd
       where td.tsf_no = I_tsf_no
         and td.received_qty > 0
         and im.item = td.item
         and im.pack_ind = 'Y'
         and vpq.pack_no = im.item
         and tx.tsf_no(+) = td.tsf_no
         and txd.tsf_xform_id = tx.tsf_xform_id
         and txd.from_item(+) = vpq.item
     union
      select vpq.item to_item,
             td.received_qty * vpq.qty update_qty
        from tsfdetail td, item_master im, v_packsku_qty vpq
       where td.tsf_no = I_tsf_no
         and td.received_qty > 0
         and im.item = td.item
         and im.pack_ind = 'Y'
         and vpq.pack_no = im.item
         and not exists (select 'X'
                           from tsf_xform tx, tsf_xform_detail txd
                          where tx.tsf_no = td.tsf_no
                            and txd.tsf_xform_id = tx.tsf_xform_id
                            and txd.from_item = vpq.item);

   cursor C_LOCK_ITEM_LOC_SOH(cv_item  ITEM_LOC_SOH.ITEM%TYPE,
                              cv_loc   ITEM_LOC_SOH.LOC%TYPE) is
      select 'X'
        from item_loc_soh
       where item = cv_item
         and loc = cv_loc
         for update of tsf_expected_qty nowait;

BEGIN
   FOR rec_item in C_GET_RECEIVED_ITEMS LOOP
      open C_LOCK_ITEM_LOC_SOH(rec_item.to_item, I_orig_to_loc);
      close C_LOCK_ITEM_LOC_SOH;

      update item_loc_soh
         set tsf_expected_qty = tsf_expected_qty - rec_item.update_qty
       where loc = I_orig_to_loc
         and item = rec_item.to_item;

      open C_LOCK_ITEM_LOC_SOH(rec_item.to_item, I_new_to_loc);
      close C_LOCK_ITEM_LOC_SOH;

      update item_loc_soh
         set tsf_expected_qty = tsf_expected_qty + rec_item.update_qty
       where loc = I_new_to_loc
         and item = rec_item.to_item;
   END LOOP; -- C_GET_RECEIVED_ITEMS LOOP

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_tsf_no),
                                            NULL);
      return FALSE;

   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;

END ADJUST_EXP_QTY_FOR_NEW_LOC;
-------------------------------------------------------------------------------------
FUNCTION UPDATE_TSFHEAD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tsf_no                  IN       TSFHEAD.TSF_NO%TYPE,
                        I_inventory_type          IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                        I_tsf_type                IN       TSFHEAD.TSF_TYPE%TYPE,
                        I_leg_1_status            IN       TSFHEAD.STATUS%TYPE,
                        I_leg_2_status            IN       TSFHEAD.STATUS%TYPE,
                        I_exp_dc_date             IN       TSFHEAD.EXP_DC_DATE%TYPE,
                        I_comments                IN       TSFHEAD.COMMENT_DESC%TYPE,
                        I_ext_ref_no              IN       TSFHEAD.EXT_REF_NO%TYPE,
                        I_from_loc                IN       TSFHEAD.FROM_LOC%TYPE,
                        I_from_loc_type           IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                        I_from_freight_code       IN       TSFHEAD.FREIGHT_CODE%TYPE,
                        I_from_routing_code       IN       TSFHEAD.ROUTING_CODE%TYPE,
                        I_finisher                IN       WH.WH%TYPE,
                        I_finisher_type           IN       VARCHAR2,
                        I_finisher_freight_code   IN       TSFHEAD.FREIGHT_CODE%TYPE,
                        I_finisher_routing_code   IN       TSFHEAD.ROUTING_CODE%TYPE,
                        I_in_process_leg_1        IN       TSFHEAD.STATUS%TYPE,
                        I_to_loc                  IN       TSFHEAD.TO_LOC%TYPE,
                        I_to_loc_type             IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                        I_in_process_leg_2        IN       TSFHEAD.STATUS%TYPE,
                        I_delivery_date           IN       TSFHEAD.DELIVERY_DATE%TYPE,
                        I_mrt_no                  IN       TSFHEAD.MRT_NO%TYPE,
                        I_not_after_date          IN       TSFHEAD.NOT_AFTER_DATE%TYPE,
                        I_context_type            IN       TSFHEAD.CONTEXT_TYPE%TYPE,
                        I_context_value           IN       TSFHEAD.CONTEXT_VALUE%TYPE,
                        I_dept                    IN       TSFHEAD.DEPT%TYPE
                       )
RETURN BOOLEAN IS

   L_program               VARCHAR2(50)  := 'TRANSFER_SQL.UPDATE_TSFHEAD';

   L_tsf_no                TSFHEAD.TSF_NO%TYPE;
   L_from_loc_type         TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_child_tsfhead_exist   VARCHAR2(1);
   L_ext_ref_no            TSFHEAD.EXT_REF_NO%TYPE;
   L_tsfhead               V_TSFHEAD%ROWTYPE;
   L_same_physical_wh      VARCHAR2(1);
   L_valid                 BOOLEAN;
   L_exist                 BOOLEAN;
   L_approve_date          TSFHEAD.APPROVAL_DATE%TYPE;
   L_approve_id            TSFHEAD.APPROVAL_ID%TYPE;
   L_close_date            TSFHEAD.CLOSE_DATE%TYPE;
   L_exp_dc_eow_date       TSFHEAD.EXP_DC_EOW_DATE%TYPE := NULL;

   L_orig_to_loc           TSFHEAD.TO_LOC%TYPE;

   cursor C_CHILD_TSFHEAD is
      select tsf_no, to_loc
        from tsfhead
       where tsf_parent_no = I_tsf_no;

BEGIN
   ---
   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_from_freight_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_freight_code',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_inventory_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_inventory_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   else
      if TSF_VALIDATE_SQL.EXIST(O_error_message,
                                I_tsf_no,
                                L_exist) = FALSE
                             or L_exist = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   open C_CHILD_TSFHEAD;
   fetch C_CHILD_TSFHEAD into L_tsf_no, L_orig_to_loc;
   ---
   if C_CHILD_TSFHEAD%FOUND then
      L_child_tsfhead_exist := 'Y';
   else
      L_child_tsfhead_exist := 'N';
   end if;
   ---
   close C_CHILD_TSFHEAD;
   ---
   if I_exp_dc_date is NOT NULL then
      if DATES_SQL.GET_EOW_DATE(O_error_message,
                                L_exp_dc_eow_date,
                                I_exp_dc_date) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if GET_TSFHEAD_INFO(O_error_message,
                       L_tsfhead,
                       I_tsf_no) = FALSE then
      return FALSE;
   end if;
   ---
   if I_finisher is NOT NULL or
      I_finisher_type is NOT NULL or
      I_finisher_freight_code is NOT NULL or
      I_finisher_routing_code is NOT NULL then  -- Transfer becomes multi-leg tsf
      ---
      -- update leg 1 and/or force std transfer become multi-leg transfer
      ---
      -- Validate external partner if one was supplied
      if I_finisher_type = 'E' and I_finisher is NOT NULL then
         if PARTNER_SQL.VALID_ACTIVE(O_error_message,
                                     L_valid,
                                     I_finisher_type,
                                     I_finisher) = FALSE
                                  or L_valid = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('NO_ACTIVE_PARTNER',
                                                  I_finisher,
                                                  I_finisher_type,
                                                  NULL);
            return FALSE;
         end if;
      end if;
      ---
      -- leg 1 status in A,C because BT are executed immediately changing the status to 'C'ompleted.
      if I_leg_1_status in ('A','C') and L_tsfhead.overall_status in ('I', 'B') then  --To approve transfer
         L_approve_date := LP_vdate;
         L_approve_id := LP_USER;
      elsif I_leg_1_status in ('I', 'B') and L_tsfhead.overall_status = 'A' then  --To submit/input transfer from Approve status
         L_approve_date := NULL;
         L_approve_id := NULL;
      else
         L_approve_date := L_tsfhead.approval_date;
         L_approve_id := L_tsfhead.approval_id;
      end if;
      ---
      if I_leg_1_status = 'C' and L_tsfhead.overall_status != 'C' then
         L_close_date := LP_vdate;
      else
         L_close_date := L_tsfhead.close_date;
      end if;
      ---
      -- Set the exp_dc_eow_date.  This would have been set to the end of week date for
      -- exp_dc_date if one existed; since it is NULL, set it to the end of week date
      -- for the approval date if one exists
      if L_exp_dc_eow_date is NULL then
         if L_approve_date is NOT NULL then
            if DATES_SQL.GET_EOW_DATE(O_error_message,
                                      L_exp_dc_eow_date,
                                      L_approve_date) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      select decode(I_finisher_type, 'I', 'W', 'E', 'E')
        into L_from_loc_type
        from dual;
      ---
      LP_table := 'TSFHEAD';
      open C_LOCK_TSFHEAD(I_tsf_no);
      close C_LOCK_TSFHEAD;

      update tsfhead th
         set comment_desc    = I_comments,
             ext_ref_no      = I_ext_ref_no,
             exp_dc_date     = I_exp_dc_date,
             exp_dc_eow_date = L_exp_dc_eow_date,
             inventory_type  = I_inventory_type,
             status          = I_leg_1_status,
             from_loc        = I_from_loc,
             from_loc_type   = I_from_loc_type,
             freight_code    = I_from_freight_code,
             routing_code    = I_from_routing_code,
             to_loc          = I_finisher,
             to_loc_type     = L_from_loc_type,
             approval_date   = L_approve_date,
             approval_id     = L_approve_id,
             close_date      = L_close_date,
             delivery_date   = I_delivery_date,
             mrt_no          = I_mrt_no,
             not_after_date  = I_not_after_date,
             context_type    = I_context_type,
             context_value   = I_context_value,
             dept            = I_dept
       where tsf_no = I_tsf_no;
   ---
      if GET_TSFHEAD_INFO(O_error_message,
                          L_tsfhead,
                          I_tsf_no) = FALSE then
         return FALSE;
      end if;
   --- 
      if I_leg_1_status = 'C' and L_tsfhead.overall_status != 'C' then
         LP_table := 'TSFDETAIL';
         open C_LOCK_TSFDETAIL(I_tsf_no);
         close C_LOCK_TSFDETAIL;

         update tsfdetail td
            set td.cancelled_qty = NVL(td.cancelled_qty, 0) + GREATEST((NVL(td.tsf_qty, 0) - NVL(td.ship_qty, 0)), 0),
                td.distro_qty = NVL2(td.distro_qty,0,NULL),
                td.selected_qty = NVL2(td.selected_qty,0,NULL)          
          where tsf_no = I_tsf_no;
      end if;
      ---
      if L_child_tsfhead_exist = 'Y' then    -- multi-leg tsf
         ---
         -- update leg 2
         if I_leg_2_status in ('I', 'B', 'A','D') then
            ---
            if I_leg_2_status = 'I' then
               L_ext_ref_no := I_ext_ref_no;
            else
               L_ext_ref_no := NULL;
            end if;
            ---
            if UPDATE_CHILD_TSFHEAD(O_error_message,
                                    L_tsf_no,
                                    L_tsfhead,
                                    I_inventory_type,
                                    I_leg_2_status,
                                    I_exp_dc_date,
                                    L_exp_dc_eow_date,
                                    L_tsfhead.tsf_type,
                                    I_comments,
                                    L_ext_ref_no,
                                    I_finisher,
                                    L_from_loc_type,
                                    I_finisher_freight_code,
                                    I_finisher_routing_code,
                                    I_to_loc,
                                    I_to_loc_type,
                                    I_delivery_date,
                                    I_mrt_no,
                                    I_not_after_date,
                                    I_context_type,
                                    I_context_value,
                                    I_dept
                                   ) = FALSE then
               return FALSE;
            end if;
            ---
            if ( (I_leg_2_status = 'I') and (I_to_loc != L_orig_to_loc) ) then
               if ( ADJUST_EXP_QTY_FOR_NEW_LOC(O_error_message,
                                               I_tsf_no,
                                               L_orig_to_loc,
                                               I_to_loc) = FALSE ) then
                  return FALSE;
               end if;
            end if;
            ---
         else
            ---
            if I_leg_2_status = 'C' or I_in_process_leg_1 = 'Y' then
               -- update leg 2
               if UPDATE_CHILD_TSFHEAD(O_error_message,
                                       L_tsf_no,
                                       L_tsfhead,
                                       I_inventory_type,
                                       I_leg_2_status,
                                       I_exp_dc_date,
                                       L_exp_dc_eow_date,
                                       L_tsfhead.tsf_type,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       I_finisher_freight_code,
                                       I_finisher_routing_code,
                                       I_to_loc,
                                       I_to_loc_type,
                                       I_delivery_date,
                                       I_mrt_no,
                                       I_not_after_date,
                                       I_context_type,
                                       I_context_value,
                                       I_dept
                                      ) = FALSE then
                  return FALSE;
               end if;
               ---
            end if;
            ---
         end if;
         ---
      else  -- std tsf, create a child tsfhead (leg 2)
         ---
         if CREATE_CHILD_TSFHEAD(O_error_message,
                                 I_tsf_no,      -- tsf_parent_no
                                 L_tsfhead.tsf_type,
                                 I_exp_dc_date,
                                 L_exp_dc_eow_date,
                                 I_dept,
                                 I_inventory_type,
                                 I_comments,
                                 I_ext_ref_no,
                                 I_finisher_freight_code,
                                 I_finisher_routing_code,
                                 I_finisher,
                                 L_from_loc_type,
                                 I_to_loc,
                                 I_to_loc_type,
                                 I_delivery_date,
                                 I_mrt_no,
                                 I_not_after_date,
                                 I_context_type,
                                 I_context_value
                                ) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
   else  -- I_finisher is NULL, Transfer becomes std tsf
      ---
      -- validate locations if both from/to loc_type are Warehouse except
      -- intercompany transfers can be performed in the same physical warehouse
      if I_from_loc_type = 'W' and
         I_to_loc_type = 'W' and
         I_tsf_type != 'IC' then
         ---
         if I_to_loc is NOT NULL then
            if L_tsfhead.tsf_type = 'EG' then
               -- externally generated transfers cannot be wh to wh
               O_error_message := SQL_LIB.CREATE_MSG('WH_TO_WH_EG_TSF_NO_ALLOW',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            else
               if VALIDATE_LOCATIONS(O_error_message,
                                     L_same_physical_wh,
                                     I_from_loc,
                                     I_to_loc) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
            if L_tsfhead.tsf_type in ('BT', 'NB') then
               if L_same_physical_wh = 'N' then
                  O_error_message := SQL_LIB.CREATE_MSG('SAME_PWH_FOR_BT',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;
            else
               if L_same_physical_wh = 'Y' and I_mrt_no is NULL then
                  O_error_message := SQL_LIB.CREATE_MSG('SAME_PWH_TSF_NO_ALLOW',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;
            end if;
         end if;
         ---
      end if;
      ---
      if I_leg_1_status in ('A','C') and L_tsfhead.overall_status in ('I', 'B') then -- leg 1 in A,C because BTs
                                                                                     -- are executed immediately
                                                                                     -- changing the status to 'C'omplete
         L_approve_date := LP_vdate;
         L_approve_id := LP_USER;
      elsif I_leg_1_status in ('I', 'B') and L_tsfhead.overall_status = 'A' then
         L_approve_date := NULL;
         L_approve_id := NULL;
      else
         L_approve_date := L_tsfhead.approval_date;
         L_approve_id := L_tsfhead.approval_id;
      end if;
      ---
      if I_leg_1_status = 'C' and L_tsfhead.overall_status != 'C' then
         L_close_date := LP_vdate;
      else
         L_close_date := L_tsfhead.close_date;
      end if;
      ---
      -- Set the exp_dc_eow_date.  This would have been set to the end of week date for
      -- exp_dc_date if one existed; since it is NULL, set it to the end of week date
      -- for the approval date if one exists
      if L_exp_dc_eow_date is NULL then
         if L_approve_date is NOT NULL then
            if DATES_SQL.GET_EOW_DATE(O_error_message,
                                      L_exp_dc_eow_date,
                                      L_approve_date) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      LP_table := 'TSFHEAD';
      open C_LOCK_TSFHEAD(I_tsf_no);
      close C_LOCK_TSFHEAD;

      update tsfhead th
         set comment_desc    = I_comments,
             ext_ref_no      = I_ext_ref_no,
             exp_dc_date     = I_exp_dc_date,
             exp_dc_eow_date = L_exp_dc_eow_date,
             inventory_type  = I_inventory_type,
             status          = I_leg_1_status,
             from_loc        = I_from_loc,
             from_loc_type   = I_from_loc_type,
             freight_code    = I_from_freight_code,
             routing_code    = I_from_routing_code,
             to_loc          = I_to_loc,
             to_loc_type     = I_to_loc_type,
             approval_date   = L_approve_date,
             approval_id     = L_approve_id,
             close_date      = L_close_date,
             delivery_date   = I_delivery_date,
             mrt_no          = I_mrt_no,
             not_after_date  = I_not_after_date,
             context_type    = I_context_type,
             context_value   = I_context_value,
             dept            = I_dept
       where tsf_no = I_tsf_no;
      ---
      if GET_TSFHEAD_INFO(O_error_message,
                          L_tsfhead,
                          I_tsf_no) = FALSE then
         return FALSE;
      end if;       
      ---
      if I_leg_1_status = 'C' and L_tsfhead.overall_status != 'C' then
         LP_table := 'TSFDETAIL';
         open C_LOCK_TSFDETAIL(I_tsf_no);
         close C_LOCK_TSFDETAIL;

         update tsfdetail td
            set td.cancelled_qty = NVL(td.cancelled_qty, 0) + GREATEST((NVL(td.tsf_qty, 0) - NVL(td.ship_qty, 0)), 0),
                td.distro_qty = NVL2(td.distro_qty,0,NULL),
                td.selected_qty = NVL2(td.selected_qty,0,NULL)
          where tsf_no = I_tsf_no;
      end if;
      ---
      if L_child_tsfhead_exist = 'Y' then
         ---
         -- delete the existing child tsf (leg 2)
         if DELETE_CHILD_TSFHEAD(O_error_message,
                                 L_tsf_no) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_tsf_no),
                                            (I_tsf_no));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;

END UPDATE_TSFHEAD;
--------------------------------------------------------------------------------------
FUNCTION UPDATE_CHILD_TSFHEAD(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_child_tsf_no      IN       TSFHEAD.TSF_NO%TYPE,
                              I_tsfhead           IN       V_TSFHEAD%ROWTYPE,
                              I_inventory_type    IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                              I_status            IN       TSFHEAD.STATUS%TYPE,
                              I_exp_dc_date       IN       TSFHEAD.EXP_DC_DATE%TYPE,
                              I_exp_dc_eow_date   IN       TSFHEAD.EXP_DC_EOW_DATE%TYPE,
                              I_tsf_type          IN       TSFHEAD.TSF_TYPE%TYPE,
                              I_comments          IN       TSFHEAD.COMMENT_DESC%TYPE,
                              I_ext_ref_no        IN       TSFHEAD.EXT_REF_NO%TYPE,
                              I_from_loc          IN       TSFHEAD.FROM_LOC%TYPE,
                              I_from_loc_type     IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                              I_freight_code      IN       TSFHEAD.FREIGHT_CODE%TYPE,
                              I_routing_code      IN       TSFHEAD.ROUTING_CODE%TYPE,
                              I_to_loc            IN       TSFHEAD.TO_LOC%TYPE,
                              I_to_loc_type       IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                              I_delivery_date     IN       TSFHEAD.DELIVERY_DATE%TYPE,
                              I_mrt_no            IN       TSFHEAD.MRT_NO%TYPE,
                              I_not_after_date    IN       TSFHEAD.NOT_AFTER_DATE%TYPE,
                              I_context_type      IN       TSFHEAD.CONTEXT_TYPE%TYPE,
                              I_context_value     IN       TSFHEAD.CONTEXT_VALUE%TYPE,
                              I_dept              IN       TSFHEAD.DEPT%TYPE
                             )
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)  := 'TRANSFER_SQL.UPDATE_CHILD_TSFHEAD';

   L_same_physical_wh   VARCHAR2(1);
   L_valid              BOOLEAN;
   L_approve_date       TSFHEAD.APPROVAL_DATE%TYPE;
   L_approve_id         TSFHEAD.APPROVAL_ID%TYPE;
   L_close_date         TSFHEAD.CLOSE_DATE%TYPE;

BEGIN
   ---
   if I_child_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_child_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   -- Validate external partner if one was supplied
   if I_from_loc_type = 'E' and I_from_loc is NOT NULL then
      if PARTNER_SQL.VALID_ACTIVE(O_error_message,
                                  L_valid,
                                  I_from_loc_type,
                                  I_from_loc) = FALSE
                               or L_valid = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ACTIVE_PARTNER',
                                               I_from_loc,
                                               I_from_loc_type,
                                               NULL);
         return FALSE;
      end if;
   end if;
   ---
   if I_status = 'A' and I_tsfhead.overall_status in ('I', 'B') then
      L_approve_date := LP_vdate;
      L_approve_id := LP_USER;
   elsif I_status in ('I', 'B') and I_tsfhead.overall_status = 'A' then
      L_approve_date := NULL;
      L_approve_id := NULL;
   else
      L_approve_date := I_tsfhead.approval_date;
      L_approve_id := I_tsfhead.approval_id;
   end if;
   ---
   if I_status = 'C' and I_tsfhead.overall_status != 'C' then
      L_close_date := LP_vdate;
   else
      L_close_date := I_tsfhead.close_date;
   end if;
   ---
   LP_table := 'TSFHEAD';
   open C_LOCK_TSFHEAD(I_child_tsf_no);
   close C_LOCK_TSFHEAD;

   update tsfhead th
      set inventory_type  = I_inventory_type,
          comment_desc    = I_comments,
          ext_ref_no      = NVL(I_ext_ref_no, th.ext_ref_no),
          from_loc        = NVL(I_from_loc, th.from_loc),
          from_loc_type   = NVL(I_from_loc_type, th.from_loc_type),
          to_loc          = I_to_loc,
          to_loc_type     = I_to_loc_type,
          exp_dc_date     = I_exp_dc_date,
          exp_dc_eow_date = I_exp_dc_eow_date,
          status          = I_status,
          freight_code    = I_freight_code,
          routing_code    = I_routing_code,
          approval_date   = L_approve_date,
          approval_id     = L_approve_id,
          close_date      = L_close_date,
          delivery_date  =  i_delivery_date,
          mrt_no          = I_mrt_no,
          not_after_date  = I_not_after_date,
          context_type    = I_context_type,
          context_value   = I_context_value,
          dept            = I_dept
    where tsf_no = I_child_tsf_no;
   --
   -- Update Cancelled_qty in tsf_detail
   if I_status = 'C' and I_tsfhead.overall_status != 'C' then
      LP_table := 'TSFDETAIL';
      open C_LOCK_TSFDETAIL(I_child_tsf_no);
      close C_LOCK_TSFDETAIL;

      update tsfdetail td
         set td.cancelled_qty = NVL(td.cancelled_qty, 0) + GREATEST((NVL(td.tsf_qty, 0) - NVL(td.ship_qty, 0)), 0),
             td.distro_qty = NVL2(td.distro_qty,0,NULL),
             td.selected_qty = NVL2(td.selected_qty,0,NULL)            
       where tsf_no = I_child_tsf_no;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_child_tsf_no),
                                            (I_child_tsf_no));

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;

END UPDATE_CHILD_TSFHEAD;
--------------------------------------------------------------------------------------
FUNCTION DELETE_TSFHEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)   := 'TRANSFER_SQL.DELETE_TSFHEAD';
   L_child_exist        VARCHAR2(1)    := 'N';
   L_tsf_detail_exist   VARCHAR2(1)    := 'N';
   L_exist              BOOLEAN;
   L_child_tsf_no       TSFHEAD.TSF_NO%TYPE;

   cursor C_CHILD_TSF_EXIST is
      select tsf_no
        from tsfhead
       where tsf_parent_no = I_tsf_no;

   cursor C_TSF_DETAIL_EXIST is
      select 'Y'
        from tsfdetail
       where tsf_no = I_tsf_no;

BEGIN
   ---
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   else
      if TSF_VALIDATE_SQL.EXIST(O_error_message,
                                I_tsf_no,
                                L_exist) = FALSE
                             or L_exist = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   open C_TSF_DETAIL_EXIST;
   fetch C_TSF_DETAIL_EXIST into L_tsf_detail_exist;
   close C_TSF_DETAIL_EXIST;
   ---
   if L_tsf_detail_exist = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('TSF_DETAIL_EXIST',
                                            I_tsf_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_CHILD_TSF_EXIST;
   fetch C_CHILD_TSF_EXIST into L_child_tsf_no;
   if C_CHILD_TSF_EXIST%FOUND then
      L_child_exist := 'Y';
   end if;
   close C_CHILD_TSF_EXIST;
   ---
   if L_child_exist = 'Y' then
      if DELETE_CHILD_TSFHEAD(O_error_message,
                              L_child_tsf_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   LP_table := 'TSFHEAD_CFA_EXT';
   open C_LOCK_TSFHEAD_CFA_EXT (I_tsf_no);
   close C_LOCK_TSFHEAD_CFA_EXT ;

   delete from tsfhead_cfa_ext
         where tsf_no = I_tsf_no;
   ---
   LP_table := 'TSFHEAD';
   open C_LOCK_TSFHEAD(I_tsf_no);
   close C_LOCK_TSFHEAD;

   delete from tsfhead
         where tsf_no = I_tsf_no;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_tsf_no),
                                            (I_tsf_no));

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_TSFHEAD;
--------------------------------------------------------------------------------------
FUNCTION DELETE_CHILD_TSFHEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_child_tsf_no    IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(50)   := 'TRANSFER_SQL.DELETE_CHILD_TSFHEAD';
   L_child_detail_exist   VARCHAR2(1)    := 'N';

   cursor C_CHILD_DETAIL_EXIST is
      select 'Y'
        from tsfdetail
       where tsf_no = I_child_tsf_no;

BEGIN
   ---
   if I_child_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_child_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_CHILD_DETAIL_EXIST;
   fetch C_CHILD_DETAIL_EXIST into L_child_detail_exist;
   close C_CHILD_DETAIL_EXIST;
   ---
   if L_child_detail_exist = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('TSF_DETAIL_EXIST',
                                            I_child_tsf_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   LP_table := 'TSFHEAD';
   open C_LOCK_TSFHEAD(I_child_tsf_no);
   close C_LOCK_TSFHEAD;

   delete from tsfhead
         where tsf_no = I_child_tsf_no;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_child_tsf_no),
                                            (I_child_tsf_no));
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_CHILD_TSFHEAD;
-------------------------------------------------------------------------------------
FUNCTION RESET_CHILD_TSF_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(50)   := 'TRANSFER_SQL.RESET_CHILD_TSF_DETAIL';

   L_child_exist          VARCHAR2(1)    := 'N';
   L_child_detail_exist   VARCHAR2(1)    := 'N';
   L_tsf_no               TSFHEAD.TSF_NO%TYPE;
   L_tsfhead              V_TSFHEAD%ROWTYPE;
   L_chrg_exist           BOOLEAN;
   L_default_chrgs_ind    TSF_TYPE.DEFAULT_CHRGS_IND%TYPE;
   L_finisher_loc_type    ITEM_LOC.LOC_TYPE%TYPE;

   cursor C_GET_CHILD_TSF_NO is
      select tsf_no
        from tsfhead
       where tsf_parent_no = I_tsf_no;

   cursor C_CHILD_DETAIL_EXIST is
      select 'Y'
        from tsfdetail
       where tsf_no = L_tsf_no;

BEGIN
   ---
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   -- make sure it's a multi-legs transfer and the status now is 'I'
   open C_GET_CHILD_TSF_NO;
   fetch C_GET_CHILD_TSF_NO into L_tsf_no;
   ---
   if C_GET_CHILD_TSF_NO%FOUND then
      L_child_exist := 'Y';
   end if;
   ---
   close C_GET_CHILD_TSF_NO;
   ---
   if L_child_exist = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('TSF_NO_CHILD',
                                            I_tsf_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if GET_TSFHEAD_INFO(O_error_message,
                       L_tsfhead,
                       I_tsf_no) = FALSE then
      return FALSE;
   end if;
   ---
   if L_tsfhead.leg_2_status != 'I' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_RESET_DETAIL',
                                            I_tsf_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   -- Delete 2nd leg'd details if exist
   open C_CHILD_DETAIL_EXIST;
   fetch C_CHILD_DETAIL_EXIST into L_child_detail_exist;
   close C_CHILD_DETAIL_EXIST;
   ---
   if L_child_detail_exist = 'Y' then
      LP_table := 'TSFDETAIL';
      open C_LOCK_TSFDETAIL(L_tsf_no);
      close C_LOCK_TSFDETAIL;

      delete from tsfdetail
            where tsf_no = L_tsf_no;
   end if;
   ---
   -- Delete the 2nd leg's tsfdetail charges if exist
   if TRANSFER_CHARGE_SQL.CHARGES_EXIST(O_error_message,
                                        L_chrg_exist,
                                        L_tsf_no,
                                        NULL) = FALSE then
      return FALSE;
   end if;
   ---
   if L_chrg_exist = TRUE then
      if TRANSFER_CHARGE_SQL.DELETE_CHRGS(O_error_message,
                                          L_tsf_no,
                                          NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(L_tsf_no),
                                            (L_tsf_no));

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END RESET_CHILD_TSF_DETAIL;
-------------------------------------------------------------------------------------
FUNCTION GET_CHILD_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_child_tsf_no    IN OUT   TSFHEAD.TSF_NO%TYPE,
                       I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)

RETURN BOOLEAN IS

   L_program VARCHAR2(30) := 'TRANSFER_SQL.GET_CHILD_TSF';

   cursor C_GET_CHILD is
      select tsf_no
        from tsfhead
       where tsf_parent_no = I_tsf_no;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_GET_CHILD;
   fetch C_GET_CHILD into O_child_tsf_no;
   close C_GET_CHILD;

   if O_child_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('CHILD_TSF_NO_EXIST',
                                             I_tsf_no,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_CHILD_TSF;
-------------------------------------------------------------------------------------
FUNCTION GET_LOWEST_AVAIL_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_lowest_avail    IN OUT   TSFDETAIL.TSF_QTY%TYPE,
                              I_item_list       IN       SKULIST_HEAD.SKULIST%TYPE,
                              I_item_parent     IN       TSFDETAIL.ITEM%TYPE,
                              I_diff_id         IN       ITEM_MASTER.DIFF_1%TYPE,
                              I_inv_status      IN       TSFDETAIL.INV_STATUS%TYPE,
                              I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                              I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(50)              := 'TRANSFER_SQL.GET_LOWEST_AVAIL_QTY';
   L_available      INV_STATUS_QTY.QTY%TYPE   :=0;
   L_lowest_avail   INV_STATUS_QTY.QTY%TYPE   :=NULL;

   cursor C_GET_SKULIST is
      select item
        from skulist_detail
       where skulist = I_item_list;

   cursor C_GET_DIFF_ITEMS is
      select item
        from item_master
       where (item_parent = I_item_parent
             or item_grandparent = I_item_parent)
         and (I_diff_id is NULL
              or (diff_1 = I_diff_id
                  or diff_2 = I_diff_id
                  or diff_3 = I_diff_id
                  or diff_4 = I_diff_id));

BEGIN
   if I_diff_id is NOT NULL then
      if I_item_parent is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_item_parent',
                                                L_program,
                                                NULL);
         return FALSE;
      end if;
   end if;

   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_item_list is NOT NULL then
      FOR rec in C_GET_SKULIST LOOP

         if TRANSFER_SQL.QUANTITY_AVAIL(O_error_message,
                                        L_available,
                                        rec.item,
                                        I_inv_status,
                                        I_from_loc_type,
                                        I_from_loc) = FALSE then
            return FALSE;
         end if;

         if O_lowest_avail is NOT NULL then
            if (L_available < O_lowest_avail and L_available > 0) or O_lowest_avail <= 0 then
               O_lowest_avail := L_available;
            end if;
         else
            O_lowest_avail := L_available;
         end if;
      END LOOP;

   else
      FOR rec in C_GET_DIFF_ITEMS LOOP

         if TRANSFER_SQL.QUANTITY_AVAIL(O_error_message,
                                        L_available,
                                        rec.item,
                                        I_inv_status,
                                        I_from_loc_type,
                                        I_from_loc) = FALSE then
            return FALSE;
         end if;

         if O_lowest_avail is NOT NULL then
            if (L_available < O_lowest_avail and L_available > 0) or O_lowest_avail <= 0 then
               O_lowest_avail := L_available;
            end if;
         else
            O_lowest_avail := L_available;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_LOWEST_AVAIL_QTY;
-------------------------------------------------------------------------------------
-- Function: CHECK_ITEM_LOC_RELATIONS
-- Purpose : Creates item loc relationships for the from item at the to
--           location with the item hasn't been transformed.
--           From Item / Finisher relationships are created when the item is
--           validated.
--           To Item / To Location relationships are created during item
--           transformation and packing
--------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LOC_RELATIONS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_tsf_no              IN       TSFHEAD.TSF_NO%TYPE,
                                  I_to_loc              IN       TSFHEAD.TO_LOC%TYPE,
                                  I_to_loc_type         IN       TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(50)   := 'TRANSFER_SQL.CHECK_ITEM_LOC_RELATIONS';
   L_itemloc_exist        BOOLEAN        := FALSE;
   L_item                 ITEM_MASTER.ITEM%TYPE;

   -- -------------------------------------------------------------------------
   -- Select items that have not transformed
   -- -------------------------------------------------------------------------
   cursor C_NON_RANGED_ITEMS is
      select td.item
        from tsfdetail td
       where td.tsf_no = I_tsf_no
         and not exists (select 'x'
                           from tsf_xform tx, tsf_xform_detail txd
                          where tx.tsf_no = td.tsf_no
                            and txd.tsf_xform_id = tx.tsf_xform_id
                            and txd.from_item = td.item)
         and not exists (select 'x'
                           from item_loc il
                          where il.item = td.item
                            and il.loc  = I_to_loc
                            and il.ranged_ind  = 'Y')
      union
      -- Need to select the resultant item created during item transformation or packing
      -- in order to update the ranged_ind in case the location is changed after adding the items,
      -- after transformation or after packing.  The update is made in the VALID_TO_ITEM function.
      select td.item
        from tsfdetail td,
             tsfhead tsf
       where tsf.tsf_parent_no = I_tsf_no
         and tsf.tsf_no = td.tsf_no
         and not exists (select 'x'
                           from item_loc il
                          where il.item = td.item
                            and il.loc  = I_to_loc
                            and il.ranged_ind  = 'Y'
                            and rownum = 1);

BEGIN

   FOR rec in C_NON_RANGED_ITEMS LOOP
      if TSF_VALIDATE_SQL.VALID_TO_ITEM(O_error_message,
                                        rec.item,
                                        I_to_loc,
                                        I_to_loc_type) = FALSE then
         return FALSE;
      end if;
   END LOOP;
         ---
   return TRUE;
EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;

END CHECK_ITEM_LOC_RELATIONS;
--------------------------------------------------------------------------------------
FUNCTION APPROVE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_leg_1_status       IN OUT   TSFHEAD.STATUS%TYPE,
                 I_tsf_no             IN       TSFHEAD.TSF_NO%TYPE,
                 I_child_tsf_no       IN       TSFHEAD.TSF_NO%TYPE,
                 I_tsf_type           IN       TSFHEAD.TSF_TYPE%TYPE,
                 I_appr_second_leg    IN       BOOLEAN, -- this input is TRUE when only the 2nd tsfleg is being approved
                 I_inventory_type     IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                 I_from_loc_type      IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                 I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                 I_finisher_type      IN       V_TSFHEAD.FINISHER_TYPE%TYPE,
                 I_finisher           IN       TSFHEAD.TO_LOC%TYPE,
                 I_to_loc_type        IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                 I_to_loc             IN       TSFHEAD.TO_LOC%TYPE)

RETURN BOOLEAN IS

   L_program               VARCHAR2(50)               := 'TRANSFER_SQL.APPROVE';
   L_vdate                 PERIOD.VDATE%TYPE          := LP_vdate;
   L_finisher_in_loc       VARCHAR2(1);
   L_finisher_entity_ind   VARCHAR2(1);
   L_item                  ITEM_MASTER.ITEM%TYPE;
   L_itemloc_exist         BOOLEAN;
   L_xform_exists          BOOLEAN;
   L_packing_exists        BOOLEAN;
   L_xform_repack_exist    BOOLEAN                    := FALSE;

   L_from_pwh              WH.WH%TYPE                 := NULL;
   L_to_pwh                WH.WH%TYPE                 := NULL;
   L_tran_code             TRAN_DATA.TRAN_CODE%TYPE   := 25;
   L_neg_transferred_qty   TSFDETAIL.TSF_QTY%TYPE     := NULL;
   L_found                 BOOLEAN;

   cursor C_TSFDETAIL is
      select item,
             inv_status,
             tsf_qty
        from tsfdetail
       where tsf_no = I_tsf_no;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_tsf_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_appr_second_leg is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_appr_second_leg',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_from_loc is NULL then
        O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              'I_from_loc',
                                               L_program,
                                               NULL);
        return FALSE;
   end if;

   if I_to_loc is NULL then
        O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              'I_to_loc',
                                               L_program,
                                               NULL);
        return FALSE;
   end if;

   if I_finisher is NOT NULL then
      -- overwrite the child transfer tsfdetail records with the new repacked/transformed items.

      if UPDATE_TSFDETAIL(O_error_message,
                          I_tsf_no) = FALSE then
         return FALSE;
      end if;

      if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                        L_finisher_in_loc,
                                        L_finisher_entity_ind,
                                        I_tsf_no) = FALSE then
         return FALSE;
      end if;

   end if;

   if I_finisher is NOT NULL or I_tsf_type = 'IC' then
      -- if the tsf is multi-legged the function expects the 2nd leg tsf_no(child), otherwise pass the tsf_no
      if TRANSFER_COST_SQL.CALC_ACTIVITY_COSTS(O_error_message,
                                               NVL(I_child_tsf_no,I_tsf_no)) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_finisher is NOT NULL then
      if CHECK_ITEM_LOC_RELATIONS(O_error_message,
                                  I_tsf_no,
                                  I_to_loc,
                                  I_to_loc_type) = FALSE then
         return FALSE;
      end if;
   end if;

   --  if transfering 'Unavailable' inventory, need to first decrement the nonsellable qty for each item
   -- (move it back to available) before the tsf occurs, because tsf processing (tsf_reserved_qty,
   -- tsf_expected_qty etc) deals w/available stock.
   if I_inventory_type != 'A' then
      if I_appr_second_leg = FALSE then -- if only the 2nd tsf leg is being approved no need to decrement again
         if BOL_SQL.INIT_BOL_PROCESS(O_error_message) = FALSE then
            return FALSE;
         end if;

         FOR rec in C_TSFDETAIL LOOP
            L_neg_transferred_qty := (rec.tsf_qty * -1);  --decrementing non_sellable_qty/inv_status_qty

            if INVADJ_SQL.BUILD_ADJ_UNAVAILABLE(O_error_message,
                                                L_found,
                                                rec.item,
                                                rec.inv_status,
                                                I_from_loc_type,
                                                I_from_loc,
                                                L_neg_transferred_qty) = FALSE then
               return FALSE;
            end if;

            -- insert a tran_data record (code 25)
            if INVADJ_SQL.BUILD_ADJ_TRAN_DATA(O_error_message,
                                              L_found,
                                              rec.item,
                                              I_from_loc_type,
                                              I_from_loc,
                                              L_neg_transferred_qty,
                                              NULL,             -- total_cost
                                              NULL,             -- total_retail
                                              I_tsf_no,         -- ref_no_1
                                              L_program,
                                              L_vdate,
                                              L_tran_code,
                                              NULL,              -- reason
                                              rec.inv_status,
                                              NULL,              -- wac
                                              NULL,              -- unit_retail
                                              NULL) = FALSE then -- pack_ind
               return FALSE;
            end if;
         END LOOP;

         if BOL_SQL.FLUSH_BOL_PROCESS(O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   if I_finisher is NOT NULL and L_finisher_in_loc in ('S','B') then
      if I_appr_second_leg = FALSE then  -- do not call manual_bt_execute if only the 2nd tsf leg is being approved
         if TRANSFER_SQL.MANUAL_BT_EXECUTE(O_error_message,
                                           I_tsf_no,
                                           I_from_loc,
                                           I_finisher,
                                           I_tsf_type) = FALSE then
            return FALSE;
         end if;

         -- When the finisher is a VWH within the same PWH as the sending location, the first leg of the tsf
         -- becomes closed after the book tsf (manual_bt_execute) is completed
         O_leg_1_status := 'C';
      end if;
   elsif I_finisher is NULL and
         I_tsf_type = 'IC' and
         I_from_loc_type = 'W' and
         I_to_loc_type = 'W' then

      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_from_pwh,
                                       I_from_loc) = FALSE then
         return FALSE;
      end if;

      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_to_pwh,
                                       I_to_loc) = FALSE then
         return FALSE;
      end if;

      -- Approval of an inter-company transfer with no finishing, where the from loc and to loc
      -- are in the same physical warehouse requires a book transfer.
      if L_from_pwh = L_to_pwh then

         if TRANSFER_SQL.MANUAL_BT_EXECUTE(O_error_message,
                                           I_tsf_no,
                                           I_from_loc,
                                           I_to_loc,
                                           I_tsf_type) = FALSE then
            return FALSE;
         end if;

         -- When the receiving location is a VWH within the same PWH as the sending location,
         -- the tsf becomes closed after the book tsf (manual_bt_execute) is completed.  Also,
         -- the transfer reserved and expected quantities should not be updated in this case,
         -- so exit out of the APPROVE function without calling the UPD_TSF_RESV_EXP function.
         O_leg_1_status := 'C';
         return TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END APPROVE;
--------------------------------------------------------------------------------
FUNCTION GET_FINISHER_INFO(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_finisher_loc_ind      IN OUT   VARCHAR2,
                           O_finisher_entity_ind   IN OUT   VARCHAR2,
                           I_tsf_no                IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(50)   := 'TRANSFER_SQL.GET_FINISHER_INFO';

   L_from_loc          WH.PHYSICAL_WH%TYPE;
   L_from_loc_entity   WH.TSF_ENTITY_ID%TYPE;
   L_from_loc_type     ITEM_LOC.LOC_TYPE%TYPE;
   L_from_loc_pwh      WH.PHYSICAL_WH%TYPE;

   L_finisher          WH.PHYSICAL_WH%TYPE;
   L_finisher_entity   WH.TSF_ENTITY_ID%TYPE;
   L_finisher_type     V_TSFHEAD.FINISHER_TYPE%TYPE; -- 'E'=external,'I'=internal
   L_finisher_pwh      WH.PHYSICAL_WH%TYPE;

   L_to_loc            WH.PHYSICAL_WH%TYPE;
   L_to_loc_entity     WH.TSF_ENTITY_ID%TYPE;
   L_to_loc_type       ITEM_LOC.LOC_TYPE%TYPE;
   L_to_loc_pwh        WH.PHYSICAL_WH%TYPE;

   L_tsf_type          TSFHEAD.TSF_TYPE%TYPE;
   L_tsf_1st_leg       TSFHEAD.TSF_NO%TYPE;

   L_tsfhead_info      TRANSFER_SQL.TSFHEAD_INFO;

   L_system_options    SYSTEM_OPTIONS%ROWTYPE;

   cursor C_GET_TSF_TYPE is
      select tsf_type
        from tsfhead
       where tsf_no = L_tsf_1st_leg;

   cursor C_GET_TSF_ENTITY_ID_FROM_LOC is
      select w.tsf_entity_id
        from wh w,
             tsfitem_inv_flow t
       where t.from_loc = w.wh
         and t.from_loc_type = 'W'
         and t.tsf_no = L_tsf_1st_leg
         and rownum = 1;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if GET_BASIC_TSFHEAD_INFO(O_error_message,
                             L_tsfhead_info,
                             I_tsf_no) = FALSE then
      return FALSE;
   end if;

   L_from_loc          := L_tsfhead_info.from_loc;
   L_from_loc_entity   := L_tsfhead_info.from_tsf_entity;
   L_from_loc_type     := L_tsfhead_info.from_loc_type;
   L_finisher          := L_tsfhead_info.finisher;
   L_finisher_entity   := L_tsfhead_info.finisher_tsf_entity;
   L_finisher_type     := L_tsfhead_info.finisher_type;
   L_to_loc            := L_tsfhead_info.to_loc;
   L_to_loc_entity     := L_tsfhead_info.to_tsf_entity;
   L_to_loc_type       := L_tsfhead_info.to_loc_type;
   L_tsf_1st_leg       := L_tsfhead_info.tsf_no;

   --
   -- compute loc_ind
   --
   -- some assumptions:
   -- external finishers are always partners
   -- internal finishers are always virtual warehouses
   if L_finisher_type = 'I' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_finisher_pwh,
                                       L_finisher) = FALSE then
         return FALSE;
      end if;
      L_finisher := L_finisher_pwh;
   end if;

   if L_from_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_from_loc_pwh,
                                       L_from_loc) = FALSE then
         return FALSE;
      end if;
      L_from_loc := L_from_loc_pwh;
   end if;

   if L_to_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_to_loc_pwh,
                                       L_to_loc) = FALSE then
         return FALSE;
      end if;
      L_to_loc := L_to_loc_pwh;
   end if;

   if L_finisher is NULL then
      O_finisher_loc_ind := NULL;   -- no finisher
   elsif L_finisher_type = 'E' then
      O_finisher_loc_ind := 'E';    -- external finisher
   elsif L_finisher = L_to_loc   and
         L_finisher = L_from_loc then
      O_finisher_loc_ind := 'B';    -- both locations
   elsif L_finisher  = L_to_loc   and
         L_finisher != L_from_loc then
      O_finisher_loc_ind := 'R';    -- receiving location
   elsif L_finisher != L_to_loc   and
         L_finisher  = L_from_loc then
      O_finisher_loc_ind := 'S';    -- sending location
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_FINISHER',
                                            NULL,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --
   -- compute entity_ind
   --
   -- assumption: the finisher entity is the same as either the sending or
   -- the receiving wh
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;

   if L_from_loc_entity is NULL and
      L_to_loc_entity is NULL and
      L_finisher_entity is NOT NULL then
      -- get the transfer type
      open C_GET_TSF_TYPE;
      fetch C_GET_TSF_TYPE into L_tsf_type;
      close C_GET_TSF_TYPE;

      -- if Externally Generated, warehouse locations are
      -- physical warehouses in RMS transfer header records.
      -- get the virtual warehouse and transfer entity.
      if L_tsf_type = 'EG' and
         L_from_loc_type = 'W' and
         L_to_loc_type = 'W' and
         L_from_loc = L_to_loc then
         ---
            open C_GET_TSF_ENTITY_ID_FROM_LOC;
            fetch C_GET_TSF_ENTITY_ID_FROM_LOC into L_from_loc_entity;
            close C_GET_TSF_ENTITY_ID_FROM_LOC;
            ---
            -- from location and final to location are the same for
            -- repairing transfers
            L_to_loc_entity := L_from_loc_entity;
      end if;
   end if;

   ---
   if L_finisher_entity is NULL then
      O_finisher_entity_ind := NULL;
   elsif L_finisher_entity = L_from_loc_entity and
         L_finisher_entity = L_to_loc_entity   then
      O_finisher_entity_ind := 'B';  -- both locs
   elsif L_finisher_entity  = L_from_loc_entity and
         L_finisher_entity != L_to_loc_entity   then
      O_finisher_entity_ind := 'S';  -- sending loc
   elsif L_finisher_entity != L_from_loc_entity and
         L_finisher_entity  = L_to_loc_entity   then
      O_finisher_entity_ind := 'R';  -- receiving loc
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_FINISHER_ENT',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;

END GET_FINISHER_INFO;
--------------------------------------------------------------------------------
FUNCTION UPDATE_TSFDETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50)          := 'TRANSFER_SQL.UPDATE_TSFDETAIL';
   L_tsf_check           VARCHAR2(1)           := NULL;
   L_temp_child_tsf_no   TSFHEAD.TSF_NO%TYPE   := NULL;

   cursor C_CHECK_LEG_ONE is
      select 'X'
        from tsfhead
       where tsf_no = I_tsf_no;

   -- The C_GET_CHILD_TSF_NO cursor is responsible for selecting the child transfer
   -- number of the input parent transfer, along with the next-largest sequence
   -- number for the child transfer.
   cursor C_GET_CHILD_TSF_NO is
     select th.tsf_no
       from tsfhead th
      where tsf_parent_no = I_tsf_no;

   -- The C_GET_SUPP_PACK_SIZE cursor is responsible for selecting the supplier
   -- pack size for an item.
   cursor C_GET_SUPP_PACK_SIZE(cv_item  tsfdetail.item%TYPE) is
     select supp_pack_size
       from item_supp_country
      where item = cv_item
        and primary_supp_ind = 'Y'
        and primary_country_ind = 'Y';

   cursor C_ITEMS_FOR_2ND_LEG is
      select item,
             qty
        from v_tsfdetail_2_leg
       where tsf_no = I_tsf_no;

   L_tsf_seq_no          tsfdetail.tsf_seq_no%TYPE := 0;
   L_supp_pack_size      tsfdetail.supp_pack_size%TYPE;

BEGIN
   -- NULL, input transfer numbers are not allowed. --
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   -- Ensure input transfer number is a valid one. --
   open C_CHECK_LEG_ONE;
   fetch C_CHECK_LEG_ONE into L_tsf_check;

   if C_CHECK_LEG_ONE%NOTFOUND then
      close C_CHECK_LEG_ONE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRANSFER',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   close C_CHECK_LEG_ONE;
   ---
   -- prepare tables for TSFDETAIL insert --
   open C_GET_CHILD_TSF_NO;
   fetch C_GET_CHILD_TSF_NO into L_temp_child_tsf_no;

   if C_GET_CHILD_TSF_NO%NOTFOUND then
      close C_GET_CHILD_TSF_NO;
      O_error_message := SQL_LIB.CREATE_MSG('CHILD_TSF_NO_EXIST',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   close C_GET_CHILD_TSF_NO;
   ---
   -- overwrite the original 2nd leg TSFDETAIL record with the new --
   LP_table := 'TSFDETAIL';
   open C_LOCK_TSFDETAIL(L_temp_child_tsf_no);
   close C_LOCK_TSFDETAIL;

   delete from tsfdetail
     where tsf_no = L_temp_child_tsf_no;
   ---
   FOR REC_item in C_ITEMS_FOR_2ND_LEG LOOP
     ---
     open C_GET_SUPP_PACK_SIZE(REC_item.item);
     fetch C_GET_SUPP_PACK_SIZE into L_supp_pack_size;
     close C_GET_SUPP_PACK_SIZE;
     ---
     if L_supp_pack_size is NULL then
        L_supp_pack_size := 1; --For Complex Non Orderable Pack, Supp_Pack_size is Set To 1.
     end if;
     ---
     L_tsf_seq_no := L_tsf_seq_no + 1;
     ---
     insert into tsfdetail(tsf_no,
                           tsf_seq_no,
                           item,
                           tsf_qty,
                           supp_pack_size,
                           publish_ind,
                           updated_by_rms_ind)
                    values(L_temp_child_tsf_no,
                           L_tsf_seq_no,
                           REC_item.item,
                           REC_item.qty,
                           L_supp_pack_size,
                           'N',
                           'Y');
      ---
   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(L_temp_child_tsf_no),
                                            (L_temp_child_tsf_no));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_TSFDETAIL;
-------------------------------------------------------------------------------------
FUNCTION DELETE_CANCELLED_TSF(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tsf_no           IN       TSFHEAD.TSF_NO%TYPE,
                              I_child_tsf_no     IN       TSFHEAD.TSF_NO%TYPE,
                              I_overall_status   IN       VARCHAR2,
                              I_tsf_type         IN       TSFHEAD.TSF_TYPE%TYPE)

RETURN BOOLEAN IS

   L_program           VARCHAR2(50)   := 'TRANSFER_SQL.DELETE_CANCELLED_TSF';
   L_table             VARCHAR2(30);
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   L_appr_second_leg   BOOLEAN   := FALSE;
   L_child_tsf_no      TSFHEAD.TSF_NO%TYPE;

   cursor C_GET_CHILD_TSF_NO is
      select tsf_no
       from tsfhead
      where tsf_parent_no = I_tsf_no;

   cursor C_LOCK_ORDCUST is
      select 'X'
        from ordcust
       where tsf_no = I_tsf_no
         for update of ordcust_no nowait;

   cursor C_LOCK_ORDCUST_DETAIL is
      select 'X'
        from ordcust_detail
       where ordcust_no in (select ordcust_no
                              from ordcust
                             where tsf_no = I_tsf_no)
         for update of item nowait;

   cursor C_LOCK_ORDCUST_PUB_INFO is
      select 'X'
        from ordcust_pub_info
       where ordcust_no in (select ordcust_no
                              from ordcust
                             where tsf_no = I_tsf_no)
         for update of pub_status nowait;

   cursor C_LOCK_TSF_ITEM_WO_COST is
      select 'X'
        from tsf_item_wo_cost
       where tsf_item_cost_id in (select tsf_item_cost_id
                                   from tsf_item_cost
                                  where tsf_no = I_tsf_no
                                     or tsf_no = L_child_tsf_no)
         for update of tsf_item_cost_id nowait;

   cursor C_LOCK_TSF_ITEM_COST is
      select 'X'
        from tsf_item_cost
       where tsf_no = I_tsf_no
          or tsf_no = L_child_tsf_no
         for update of tsf_item_cost_id nowait;

   cursor C_LOCK_TSF_XFORM_DETAIL is
      select 'X'
        from tsf_xform_detail
       where tsf_xform_id in (select tsf_xform_id
                               from tsf_xform
                              where tsf_no = I_tsf_no)
         for update of tsf_xform_detail_id nowait;

   cursor C_LOCK_TSF_XFORM is
      select 'X'
        from tsf_xform
       where tsf_no = I_tsf_no
         for update of tsf_xform_id nowait;

   cursor C_LOCK_TSF_PACKING_DETAIL is
      select 'X'
        from tsf_packing_detail
       where tsf_packing_id in (select tsf_packing_id
                                 from tsf_packing
                                where tsf_no = I_tsf_no)
         for update of tsf_packing_detail_id nowait;

   cursor C_LOCK_TSF_PACKING is
      select 'X'
        from tsf_packing
       where tsf_no = I_tsf_no
         for update of tsf_packing_id nowait;

   cursor C_LOCK_TSF_WO_DETAIL is
      select 'X'
        from tsf_wo_detail
       where tsf_wo_id = (select tsf_wo_id
                            from tsf_wo_head
                           where tsf_no = I_tsf_no)
         for update of tsf_wo_detail_id nowait;

   cursor C_LOCK_TSF_WO_HEAD is
      select 'X'
        from tsf_wo_head
       where tsf_no = I_tsf_no
         for update of tsf_wo_id nowait;

   cursor C_LOCK_TSFHEAD_L10NEXT is
      select 'X'
        from tsfhead_l10n_ext
       where tsf_no = I_tsf_no
         for update of tsf_no nowait;

   cursor C_LOCK_TSFDETAIL is
      select 'X'
        from tsfdetail
       where tsf_no = I_tsf_no
          or tsf_no = L_child_tsf_no
         for update of tsf_no nowait;

   cursor C_LOCK_TSFHEAD_CFA_EXT is
      select 'X'
        from tsfhead_cfa_ext
       where tsf_no = I_tsf_no
         for update of tsf_no nowait;

   cursor C_LOCK_TSFHEAD is
      select 'X'
        from tsfhead
       where tsf_no = I_tsf_no
          or tsf_parent_no = I_tsf_no
         for update of tsf_no nowait;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_child_tsf_no is NULL then
      open C_GET_CHILD_TSF_NO;
      fetch C_GET_CHILD_TSF_NO into L_child_tsf_no;
      close C_GET_CHILD_TSF_NO;
   else
      L_child_tsf_no := I_child_tsf_no;
   end if;

   L_table := 'ordcust_detail';
   open C_LOCK_ORDCUST_DETAIL;
   close C_LOCK_ORDCUST_DETAIL;
   delete from ordcust_detail
    where ordcust_no in (select ordcust_no
                           from ordcust
                          where tsf_no = I_tsf_no);

   L_table := 'ordcust_pub_info';
   open C_LOCK_ORDCUST_PUB_INFO;
   close C_LOCK_ORDCUST_PUB_INFO;
   delete from ordcust_pub_info
    where ordcust_no in (select ordcust_no
                           from ordcust
                          where tsf_no = I_tsf_no);

   L_table := 'ordcust';
   open C_LOCK_ORDCUST;
   close C_LOCK_ORDCUST;
   delete from ordcust
    where tsf_no = I_tsf_no;

   -- back out resv/exp qty's when deleting transfers in approved status
   if I_overall_status in ('A', 'B') and I_tsf_type != 'NS' then
      if TRANSFER_SQL.UPD_TSF_RESV_EXP(O_error_message,
                                       I_tsf_no,
                                       'D',            -- delete (vs add)
                                       L_appr_second_leg  -- 'FALSE'
                                      ) = FALSE then
         return FALSE;
      end if;
   end if;

   if TRANSFER_CHARGE_SQL.DELETE_CHRGS(O_error_message,
                                       I_tsf_no,
                                       NULL) = FALSE then
      return FALSE;
   end if;

   L_table := 'tsf_item_wo_cost';
   open C_LOCK_TSF_ITEM_WO_COST;
   close C_LOCK_TSF_ITEM_WO_COST;
   delete from tsf_item_wo_cost
    where tsf_item_cost_id in (select tsf_item_cost_id
                                 from tsf_item_cost
                                where tsf_no = I_tsf_no
                                   or tsf_no = L_child_tsf_no);

   L_table := 'tsf_item_cost';
   open C_LOCK_TSF_ITEM_COST;
   close C_LOCK_TSF_ITEM_COST;
   delete from tsf_item_cost
    where tsf_no = I_tsf_no
       or tsf_no = L_child_tsf_no;

   L_table := 'tsf_xform_detail';
   open C_LOCK_TSF_XFORM_DETAIL;
   close C_LOCK_TSF_XFORM_DETAIL;
   delete from tsf_xform_detail
    where tsf_xform_id in (select tsf_xform_id
                             from tsf_xform
                            where tsf_no = I_tsf_no);

   L_table := 'tsf_xform';
   open C_LOCK_TSF_XFORM;
   close C_LOCK_TSF_XFORM;
   delete from tsf_xform
    where tsf_no = I_tsf_no;

   L_table := 'tsf_packing_detail';
   open C_LOCK_TSF_PACKING_DETAIL;
   close C_LOCK_TSF_PACKING_DETAIL;
   delete from tsf_packing_detail
    where tsf_packing_id in (select tsf_packing_id
                               from tsf_packing
                              where tsf_no = I_tsf_no);

   L_table := 'tsf_packing';
   open C_LOCK_TSF_PACKING;
   close C_LOCK_TSF_PACKING;
   delete from tsf_packing
    where tsf_no = I_tsf_no;

   L_table := 'tsf_wo_detail';
   open C_LOCK_TSF_WO_DETAIL;
   close C_LOCK_TSF_WO_DETAIL;
   delete from tsf_wo_detail
    where tsf_wo_id in (select tsf_wo_id
                          from tsf_wo_head
                         where tsf_no = I_tsf_no);

   L_table := 'tsf_wo_head';
   open C_LOCK_TSF_WO_HEAD;
   close C_LOCK_TSF_WO_HEAD;
   delete from tsf_wo_head
    where tsf_no = I_tsf_no;

   L_table := 'tsfhead_l10n_ext';
   open C_LOCK_TSFHEAD_L10NEXT;
   close C_LOCK_TSFHEAD_L10NEXT;
   delete from tsfhead_l10n_ext
    where tsf_no = I_tsf_no;

   L_table := 'tsfdetail';
   open C_LOCK_TSFDETAIL;
   close C_LOCK_TSFDETAIL;
   delete from tsfdetail
    where tsf_no = I_tsf_no
       or tsf_no = L_child_tsf_no;

   L_table := 'tsfhead_cfa_ext';
   open C_LOCK_TSFHEAD_CFA_EXT;
   close C_LOCK_TSFHEAD_CFA_EXT ;

   delete from tsfhead_cfa_ext
         where tsf_no = I_tsf_no;

   L_table := 'tsfhead';
   open C_LOCK_TSFHEAD;
   close C_LOCK_TSFHEAD;
   delete from tsfhead
    where tsf_no = I_tsf_no
       or tsf_parent_no = I_tsf_no;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_tsf_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_CANCELLED_TSF;
--------------------------------------------------------------------------------
FUNCTION CHECK_FOR_CHILD_TSF(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tsf_leg             IN OUT   VARCHAR2,
                             O_child_tsf_no        IN OUT   TSFHEAD.TSF_NO%TYPE,
                             O_child_to_loc        IN OUT   ITEM_LOC.LOC%TYPE,
                             O_child_to_loc_type   IN OUT   ITEM_LOC.LOC_TYPE%TYPE,
                             I_tsf_no              IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.CHECK_FOR_CHILD_TSF';

   cursor C_PARENT_CHILD is
      select 'N',
             NULL,
             NULL,
             NULL
        from tsfhead t1
       where tsf_no = I_tsf_no
         and tsf_parent_no is NULL
         and not exists (select 'X'
                           from tsfhead t2
                          where t1.tsf_no = t2.tsf_parent_no)
       union ALL
      select 'F',
             tsf_no child_tsf,
             to_loc,
             to_loc_type
        from tsfhead
       where tsf_parent_no = I_tsf_no;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_tsf_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   --determine if tsf_no is for single leg, First leg, or Second leg
   open C_PARENT_CHILD;
   fetch C_PARENT_CHILD into O_tsf_leg,
                             O_child_tsf_no,
                             O_child_to_loc,
                             O_child_to_loc_type;

   if C_PARENT_CHILD%NOTFOUND then
      --it is a child/Second leg tsf
      O_tsf_leg := 'S';
      O_child_tsf_no  := NULL;
      O_child_to_loc := NULL;
      O_child_to_loc_type := NULL;
   end if;

   close C_PARENT_CHILD;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FOR_CHILD_TSF;
-------------------------------------------------------------------------------------
FUNCTION UNAPPROVE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_tsf_no             IN       TSFHEAD.TSF_NO%TYPE,
                   I_first_leg_status   IN       TSFHEAD.STATUS%TYPE,
                   I_inv_type           IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                   I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                   I_from_loc_type      IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                   I_finisher           IN       TSFHEAD.TO_LOC%TYPE,
                   I_finisher_type      IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                   I_to_loc             IN       TSFHEAD.TO_LOC%TYPE,
                   I_to_loc_type        IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                   I_tsf_type           IN       TSFHEAD.TSF_TYPE%TYPE)

RETURN BOOLEAN IS

   L_program               VARCHAR2(255)                 := 'TRANSFER_SQL.UNAPPROVE';
   L_table                 VARCHAR2(30);
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   L_finisher_loc_ind      VARCHAR2(1)                   := NULL;
   L_finisher_entity_ind   VARCHAR2(1)                   := NULL;

   L_child_tsf_no          tsfhead.tsf_no%TYPE;
   L_pack_no               v_packsku_qty.pack_no%TYPE;

   L_tsf_no                tsfhead.tsf_no%TYPE;
   L_first_leg_ind         VARCHAR2(1)                    := 'N';
   L_second_leg_ind        VARCHAR2(1)                    := 'N';
   L_from_loc              tsfhead.from_loc%TYPE;
   L_from_loc_type         tsfhead.from_loc_type%TYPE;
   L_to_loc                tsfhead.to_loc%TYPE;
   L_to_loc_type           tsfhead.to_loc_type%TYPE;
   L_item                  tsfdetail.item%TYPE;
   L_pack_ind              VARCHAR2(1)                    := 'N';
   L_inventory_ind         item_master.inventory_ind%TYPE := 'Y';
   L_receive_as_type       ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
   L_update_quantity       TSFDETAIL.TSF_QTY%TYPE;

   L_tran_code             tran_data.tran_code%TYPE       := 25;
   L_found                 BOOLEAN;
   L_same_phy_wh           VARCHAR2(1)                    := 'N';

   cursor C_GET_CHILD_TSF_NO is
      select tsf_no
       from tsfhead
      where tsf_parent_no = I_tsf_no;

   cursor C_GET_ITEMS is
      select td.item,
             td.tsf_qty,
             NVL(td.ship_qty, 0) ship_qty,
             im.pack_ind,
             td.inv_status,
             (NVL(td.tsf_qty, 0) - GREATEST(NVL(td.ship_qty, 0),
                                            NVL(td.distro_qty, 0),
                                            NVL(td.selected_qty, 0))) upd_qty
        from tsfdetail td,
             item_master im
       where td.tsf_no = L_tsf_no
         and im.item   = td.item;

   cursor C_ITEMS_IN_PACK is
      select vpq.pack_no pack_no,
             vpq.item comp_item,
             vpq.qty qty
        from v_packsku_qty vpq,
             item_master im
       where vpq.pack_no = L_pack_no
         and vpq.item = im.item
         and im.inventory_ind = L_inventory_ind;

   cursor C_LOCK_TSFHEAD is
      select 'x'
        from tsfhead
       where tsf_no in (I_tsf_no, L_child_tsf_no)
         for update nowait;

   cursor C_LOCK_TSFDETAIL is
      select 'x'
        from tsfdetail
       where tsf_no = L_child_tsf_no
         for update nowait;

   cursor C_LOCK_TSF_ITEM_WO_COST is
      select 'x'
        from tsf_item_wo_cost
       where tsf_item_cost_id in (select tsf_item_cost_id
                                    from tsf_item_cost
                                   where tsf_no = L_child_tsf_no)
         for update nowait;

   cursor C_LOCK_TSF_ITEM_COST is
      select 'x'
        from tsf_item_cost
       where tsf_no in (I_tsf_no, L_child_tsf_no)
         for update nowait;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_first_leg_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_first_leg_status',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_tsf_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                     L_finisher_loc_ind,
                                     L_finisher_entity_ind,
                                     I_tsf_no) = FALSE then
      return FALSE;
   end if;

   if I_from_loc_type = 'W' and I_to_loc_type = 'W' and L_finisher_loc_ind is NULL then
      if TRANSFER_SQL.VALIDATE_LOCATIONS(O_error_message,
                                         L_same_phy_wh,
                                         I_from_loc,
                                         I_to_loc) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_finisher_loc_ind is NOT NULL then
      open C_GET_CHILD_TSF_NO;
      fetch C_GET_CHILD_TSF_NO into L_child_tsf_no;
      close C_GET_CHILD_TSF_NO;
   end if;

      -- ----------------------------------------------------------------------
      -- Process 1st leg
      -- ----------------------------------------------------------------------
   if L_finisher_loc_ind in('R', 'E') or L_finisher_loc_ind is NULL then
      if I_first_leg_status = 'A' then
         L_table := 'TSFHEAD';
         open C_LOCK_TSFHEAD;
         close C_LOCK_TSFHEAD;

         --update 1st leg status
         update tsfhead
            set status = 'I'
          where tsf_no = I_tsf_no;

         -- 1st leg still in approved status so
         -- items on 1st leg can change. Delete
         -- item cost records.

         --delete tsf_item_wo_cost records
         L_table := 'TSF_ITEM_WO_COST';
         open C_LOCK_TSF_ITEM_WO_COST;
         close C_LOCK_TSF_ITEM_WO_COST;

         delete from tsf_item_wo_cost
           where tsf_item_cost_id in (select tsf_item_cost_id
                                        from tsf_item_cost
                                       where tsf_no = I_tsf_no);

         --delete tsf_item_cost records
         L_table := 'TSF_ITEM_COST';
         open C_LOCK_TSF_ITEM_COST;
         close C_LOCK_TSF_ITEM_COST;

         delete from tsf_item_cost
          where tsf_no = I_tsf_no;
      end if;

          -- Set up variables to Process 1st leg/single legged transfer
      L_tsf_no        := I_tsf_no;
      L_from_loc      := I_from_loc;
      L_from_loc_type := I_from_loc_type;

      --1st leg tsf
      if L_finisher_loc_ind is NULL then
         L_to_loc := I_to_loc;
         L_to_loc_type := I_to_loc_type;
      else
         L_to_loc := I_finisher;
         if I_finisher_type = 'I' then
            L_to_loc_type := 'W';
         else
            L_to_loc_type := 'E';
         end if;
      end if;

      if ( (I_first_leg_status = 'A') and (I_inv_type != 'A') ) then
         -- initialize the bol process (necessary for the INVADJ_SQL calls)
         if BOL_SQL.INIT_BOL_PROCESS(O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;

      FOR rec_item in C_GET_ITEMS LOOP
         -- Decrement shipped/selected/distro qty from transfer qty
         -- Reserve and expected quantities where updated at shipping/selected/distro time
         L_update_quantity := rec_item.upd_qty;
         --For IC transfer within the same physical WH, transfer expected and reserved quantities are not maintained.
         if ( L_update_quantity > 0 and L_same_phy_wh = 'N') then
            -- decrement finisher or 'to' loc's expected qty
            if ( (rec_item.pack_ind = 'Y')
             and (L_to_loc_type = 'W')
             and (L_finisher_loc_ind is NULL) ) then
               if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                         L_receive_as_type,
                                                         rec_item.item,
                                                         L_to_loc) = FALSE then
                  return FALSE;
               end if;
            else
               L_receive_as_type := 'E';
            end if;

            if ((rec_item.pack_ind = 'Y' and L_receive_as_type = 'P') or
                 rec_item.pack_ind = 'N' ) then
               -- decrement pack's/Bulk Item's expected_qty
               if UPD_ITEM_EXP(O_error_message,
                               rec_item.item,
                               'IC',
                               'E',
                               L_to_loc,
                               L_to_loc_type,
                               L_update_quantity * -1) = FALSE then

                  return FALSE;
               end if;
            end if;

            -- decrement pack's resv_qty
            if UPD_ITEM_RESV(O_error_message,
                             rec_item.item,
                             'N',
                             L_from_loc,
                             L_from_loc_type,
                             L_update_quantity * -1) = FALSE then
              return FALSE;
            end if;

            if rec_item.pack_ind = 'Y' then
               L_pack_no := rec_item.item;
               FOR rec_pack in C_ITEMS_IN_PACK LOOP
                  -- decrement pack component's reserved_qty
                  if UPD_ITEM_RESV(O_error_message,
                                   rec_pack.comp_item,
                                   'Y',
                                   L_from_loc,
                                   L_from_loc_type,
                                   L_update_quantity * rec_pack.qty * -1) = FALSE then
                     return FALSE;
                  end if;

                  -- decrement pack component's expected_qty
                  if UPD_ITEM_EXP(O_error_message,
                                  rec_pack.comp_item,
                                  'IC',
                                  L_receive_as_type,
                                  L_to_loc,
                                  L_to_loc_type,
                                  L_update_quantity * rec_pack.qty * -1) = FALSE then

                     return FALSE;
                  end if;
               END LOOP;  --C_ITEMS_IN_PACK LOOP
            end if;
         end if; -- if ( L_update_quantity > 0 )

         if ( (I_first_leg_status = 'A') and (I_inv_type != 'A') ) then
            -- ----------------------------------------------------------------
            -- if the tsf inventory type is not 'A'vailable, move the stock
            -- back to the unavailable bucket. This is only necessary when
            -- unapproving the first leg of an 'U'navailable inventory type.
            -- ----------------------------------------------------------------

            if INVADJ_SQL.BUILD_ADJ_UNAVAILABLE(O_error_message,
                                                L_found,
                                                rec_item.item,
                                                rec_item.inv_status,
                                                I_from_loc_type,
                                                I_from_loc,
                                                rec_item.tsf_qty) = FALSE then
               return FALSE;
            end if;

            -- insert a tran_data record (code 25)
            if INVADJ_SQL.BUILD_ADJ_TRAN_DATA(O_error_message,
                                              L_found,
                                              rec_item.item,
                                              I_from_loc_type,
                                              I_from_loc,
                                              rec_item.tsf_qty,
                                              NULL,              -- total_cost
                                              NULL,              -- total_retail
                                              NULL,              -- order_no
                                              L_program,
                                              LP_vdate,
                                              L_tran_code,
                                              NULL,              -- reason
                                              rec_item.inv_status,
                                              NULL,              -- wac
                                              NULL,              -- unit_retail
                                              NULL) = FALSE then -- pack_ind
               return FALSE;
            end if;
         end if;
      END LOOP; --C_GET_ITEMS LOOP

      if ( (I_first_leg_status = 'A') and (I_inv_type != 'A') ) then
         -- Flush the inv_adj changes
         if BOL_SQL.FLUSH_BOL_PROCESS(O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;

   end if;
      -- ----------------------------------------------------------------------
      -- Process 2nd leg
      -- ----------------------------------------------------------------------
   if L_child_tsf_no is NOT NULL and L_finisher_loc_ind is NOT NULL then

      -- ----------------------------------------------------------------------
      -- The Reserved qty for the Finisher and Expected qty for the To Location
      -- are based on the Received quantity at the finisher. They can't be
      -- modified and should not be backed out.
      -- Only update the Status to Input
      -- ----------------------------------------------------------------------

         if I_first_leg_status = 'B' and L_finisher_loc_ind in('S', 'B') then
            if TRANSFER_SQL.UPD_TSF_RESV_EXP(O_error_message,
                                             I_tsf_no,
                                             'D',            -- delete (vs add)
                                             FALSE
                                            ) = FALSE then
               return FALSE;
            end if;
         end if;
         L_table := 'TSFHEAD';
         open C_LOCK_TSFHEAD;
         close C_LOCK_TSFHEAD;

         update tsfhead
            set status = 'I'
          where tsf_no = L_child_tsf_no;

      --Delete 2nd leg tsfdetail records
      L_table := 'TSFDETAIL';
      open C_LOCK_TSFDETAIL;
      close C_LOCK_TSFDETAIL;

      delete from tsfdetail
       where tsf_no = L_child_tsf_no;

      -- delete tsf_item_wo_cost records
      L_table := 'TSF_ITEM_WO_COST';
      open C_LOCK_TSF_ITEM_WO_COST;
      close C_LOCK_TSF_ITEM_WO_COST;

      delete from tsf_item_wo_cost
        where tsf_item_cost_id in (select tsf_item_cost_id
                                     from tsf_item_cost
                                    where tsf_no = L_child_tsf_no);

      --delete tsf_item_cost records
      L_table := 'TSF_ITEM_COST';
      open C_LOCK_TSF_ITEM_COST;
      close C_LOCK_TSF_ITEM_COST;

      delete from tsf_item_cost
       where tsf_no = L_child_tsf_no;
   else
      if I_tsf_type = 'IC' then
         L_table := 'TSF_ITEM_COST';
         open C_LOCK_TSF_ITEM_COST;
         close C_LOCK_TSF_ITEM_COST;

         delete from tsf_item_cost
          where tsf_no = I_tsf_no;
       end if;
   end if;

   --create message to inform user of the status update on the transfer legs
   if L_finisher_loc_ind in ('S', 'B') or
      (L_finisher_loc_ind in ('R', 'E') and I_first_leg_status != 'A') then
      O_error_message := SQL_LIB.CREATE_MSG('SECOND_LEG_SET_TO_INPUT',
                                              NULL,
                                              NULL,
                                              NULL);
   elsif L_finisher_loc_ind in ('R', 'E') and I_first_leg_status = 'A' then
      O_error_message := SQL_LIB.CREATE_MSG('BOTH_LEGS_SET_TO_INPUT',
                                              NULL,
                                              NULL,
                                              NULL);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_tsf_no),
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UNAPPROVE;
-------------------------------------------------------------------------------------
FUNCTION GET_BASIC_TSFHEAD_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_tsfhead_info    IN OUT   TRANSFER_SQL.TSFHEAD_INFO,
                                I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60)          := 'TRANSFER_SQL.GET_BASIC_TSFHEAD_INFO';
   L_tsf_no    TSFHEAD.TSF_NO%TYPE   := NULL;

   cursor C_GET_TSF_PARENT_NO is
      select tsf_parent_no
        from tsfhead
       where tsf_no = I_tsf_no;

   -- This cursor is based on the select statement defining v_tsfhead, except it
   -- retrieves fewer rows and doesn't include security restrictions.  The comments
   -- in the select statement are from the view.
   cursor C_GET_INFO is
      select t.tsf_no,
          t.from_loc,
          t.from_loc_type,
          decode(t.from_loc_type,
                 'W', (select tsf_entity_id
                         from wh
                        where wh = t.from_loc),
                      (select tsf_entity_id
                         from store
                        where store = t.from_loc)) FROM_LOC_ENTITY,
          (select set_of_books_id
             from mv_loc_sob
            where location = t.from_loc
              and location_type = t.from_loc_type) FROM_SET_OF_BOOKS_ID,
          -- Note on FINISHER columns below: For 2-legged transfers, the TO_LOC
          -- on the parent transfer record (leg 1) will be the finisher.
          t2.tsf_no CHILD_TSF_NO,
          decode(t2.tsf_parent_no, NULL, to_number(NULL), t.to_loc) FINISHER,
          decode(t2.tsf_parent_no, NULL, NULL,
                                   decode(t.to_loc_type, 'W', 'I', t.to_loc_type)) FINISHER_TYPE,
          decode(t2.tsf_parent_no, NULL, to_number(NULL),
                 decode(t.to_loc_type,
                        'W', (select tsf_entity_id
                                from wh
                               where wh = t.to_loc),
                        'E', (select tsf_entity_id
                                from partner
                               where partner_type = 'E'
                                 and partner_ID = t.to_loc))) FINISHER_ENTITY,
          decode(t2.tsf_parent_no, NULL, NULL,
                 (select set_of_books_id
                    from mv_loc_sob
                   where location = t.to_loc
                     and location_type = t.to_loc_type)) FINISHER_SOB_ID,
          -- Note on TO_LOC columns below: For 2-legged transfers, to TO_LOC
          -- on the child transfer record (leg 2) represents the tsf's final receiving loc.
          -- For single leg transfers, there will be no child transfer record and the
          -- TO_LOC is retrieved from the single transfer record.
          decode(t2.tsf_parent_no, NULL, t.to_loc, t2.to_loc) TO_LOC,
          decode(t2.tsf_parent_no, NULL, t.to_loc_type, t2.to_loc_type) TO_LOC_TYPE,
          decode(t2.tsf_parent_no, NULL,
                 decode(t.to_loc_type,
                        'W', (select tsf_entity_id
                                from wh
                               where wh = t.to_loc),
                        'S', (select tsf_entity_id
                                from store
                               where store = t.to_loc)),
                 decode(t2.to_loc_type,
                        'W', (select tsf_entity_id
                                from wh
                               where wh = t2.to_loc),
                             (select tsf_entity_id
                                from store
                               where store = t2.to_loc))) TO_LOC_ENTITY,
          decode(t2.tsf_parent_no, NULL,
                 (select set_of_books_id
                    from mv_loc_sob
                   where location = t.to_loc
                     and location_type = t.to_loc_type),
                 (select set_of_books_id
                    from mv_loc_sob
                   where location = t2.to_loc
                     and location_type = t2.to_loc_type)) TO_SET_OF_BOOKS_ID
     from tsfhead t,
          tsfhead t2
    where t.tsf_parent_no is NULL
      and t.tsf_no = t2.tsf_parent_no (+)
      and t.tsf_no = L_tsf_no;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_GET_TSF_PARENT_NO;
   fetch C_GET_TSF_PARENT_NO into L_tsf_no;
   close C_GET_TSF_PARENT_NO;
   ---
   if L_tsf_no is NULL then
      L_tsf_no := I_tsf_no;
   end if;
   ---
   open C_GET_INFO;
   fetch C_GET_INFO into O_tsfhead_info;

   close C_GET_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_BASIC_TSFHEAD_INFO;
-------------------------------------------------------------------------------------
FUNCTION DELETE_WO_XFORM_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE,
                              I_qty             IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(50)   := 'TRANSFER_SQL.DELETE_WO_XFORM_PACK';
   L_tsf_wo_id        TSF_WO_DETAIL.TSF_WO_ID%TYPE;
   L_tsf_packing_id   TSF_PACKING.TSF_PACKING_ID%TYPE;
   L_pack_ind         VARCHAR2(1)    := 'N';
   L_comp_item        V_PACKSKU_QTY.ITEM%TYPE;
   L_comp_qty         V_PACKSKU_QTY.QTY%TYPE;

   cursor C_WO_ID is
      select tsf_wo_id
        from tsf_wo_head
       where tsf_no = I_tsf_no;

  cursor C_LOCK_TSF_WO_HEAD is
     select tsf_wo_id
       from tsf_wo_head
      where tsf_no = I_tsf_no
        for update nowait;

   cursor C_GET_PACK_ID is
      select tp.tsf_packing_id
        from tsf_packing tp, tsf_packing_detail tpd
       where tp.tsf_no = I_tsf_no
         and tpd.tsf_packing_id = tp.tsf_packing_id
         and tpd.item = I_item
         and tpd.record_type = 'F'
      union
      select tp.tsf_packing_id
        from tsf_packing tp, tsf_packing_detail tpd, tsf_xform_detail txd, tsf_xform tx
       where tx.tsf_no = I_tsf_no
         and txd.tsf_xform_id = tx.tsf_xform_id
         and txd.from_item = I_item
         and tp.tsf_no = tx.tsf_no
         and tpd.tsf_packing_id = tp.tsf_packing_id
         and tpd.item = txd.to_item
         and record_type = 'F';

   cursor C_GET_PACK_IND is
      select pack_ind
        from item_master
       where item = I_item;

   cursor C_GET_PACK_COMP is
      select item, qty
        from v_packsku_qty
       where pack_no = I_item;

   ----------------------------------------------------------------------------------
   FUNCTION DELETE_UPDATE_XFORM_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE,
                                      I_from_item       IN       ITEM_MASTER.ITEM%TYPE,
                                      I_from_qty        IN       TSFDETAIL.TSF_QTY%TYPE)
   RETURN BOOLEAN IS

      L_program               VARCHAR2(60) := 'TRANSFER_SQL.DELETE_UPDATE_XFORM_ITEM';

      L_tsf_xform_detail_id   TSF_XFORM_DETAIL.TSF_XFORM_DETAIL_ID%TYPE;
      L_to_qty                TSF_XFORM_DETAIL.TO_QTY%TYPE;
      L_to_item               TSF_XFORM_DETAIL.TO_ITEM%TYPE;

      cursor C_GET_XFORM_INFO is
         select tsf_xform_detail_id, to_qty
           from tsf_xform tx, tsf_xform_detail txd
          where tx.tsf_no = I_tsf_no
            and txd.tsf_xform_id = tx.tsf_xform_id
            and txd.from_item = I_from_item;

      cursor C_LOCK_TSF_XFORM_DETAIL is
        select 'X'
          from tsf_xform_detail
         where tsf_xform_detail_id = L_tsf_xform_detail_id
           for update nowait;

   BEGIN
      open C_GET_XFORM_INFO;
      fetch C_GET_XFORM_INFO into L_tsf_xform_detail_id, L_to_qty;
      close C_GET_XFORM_INFO;

      if L_tsf_xform_detail_id is NULL then
         return TRUE;
      end if;

      if L_to_qty > I_from_qty then
      -- The transfer contains more than one instance of the bulk item.
      -- Update the quantities instead of deleting
         if ITEM_XFORM_PACK_SQL.UPDATE_XFORM_QTY (O_error_message,
                                                  L_to_item,
                                                  I_tsf_no,
                                                  I_from_item,
                                                  I_from_qty * -1) = FALSE then
            return FALSE;
         end if;
      else
         LP_table := 'TSF_XFORM_DETAIL';
         open C_LOCK_TSF_XFORM_DETAIL;
         close C_LOCK_TSF_XFORM_DETAIL;

         delete
           from tsf_xform_detail
          where tsf_xform_detail_id = L_tsf_xform_detail_id;
      end if;

      return TRUE;

   EXCEPTION
      when RECORD_LOCKED then
           O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                 LP_table,
                                                 to_char(L_tsf_xform_detail_id),
                                                 (L_tsf_xform_detail_id));

      when OTHERS then
           O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                 SQLERRM,
                                                 L_program,
                                                 to_char(SQLCODE));
           return FALSE;
   END DELETE_UPDATE_XFORM_ITEM;
   --------------------------------------------------------------------------

BEGIN
   -- -----------------------------------------------------------------------
   -- Delete Work Order records
   -- -----------------------------------------------------------------------
   open C_WO_ID;
   fetch C_WO_ID into L_tsf_wo_id;
   close C_WO_ID;

   if L_tsf_wo_id is NOT NULL then
      LP_table := 'TSF_WO_DETAIL';
      open C_LOCK_TSF_WO_HEAD;
      close C_LOCK_TSF_WO_HEAD;

      delete from tsf_wo_detail
       where tsf_wo_id = L_tsf_wo_id
         and item = I_item;

   end if;

   -- -----------------------------------------------------------------------
   -- Delete Packing records
   -- -----------------------------------------------------------------------
   open C_GET_PACK_ID;
   fetch C_GET_PACK_ID into L_tsf_packing_id;
   close C_GET_PACK_ID;

   if L_tsf_packing_id is NOT NULL then
      if ITEM_XFORM_PACK_SQL.DELETE_SET (O_error_message,
                                         L_tsf_packing_id) = FALSE then
         return FALSE;
      end if;
   end if;

   -- -----------------------------------------------------------------------
   -- Delete or Update Transformation records
   -- -----------------------------------------------------------------------
   open C_GET_PACK_IND;
   fetch C_GET_PACK_IND into L_pack_ind;
   close C_GET_PACK_IND;

   if L_pack_ind = 'N' then
      if DELETE_UPDATE_XFORM_ITEM (O_error_message,
                                   I_tsf_no,
                                   I_item,
                                   I_qty) = FALSE then
         return FALSE;
      end if;
   else
      FOR c_packrec in C_GET_PACK_COMP LOOP
         if DELETE_UPDATE_XFORM_ITEM (O_error_message,
                                      I_tsf_no,
                                      c_packrec.item,
                                      I_qty * c_packrec.qty) = FALSE then
            return FALSE;
         end if;

      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(L_tsf_wo_id),
                                            (L_tsf_wo_id));

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_WO_XFORM_PACK;
-------------------------------------------------------------------------------------
FUNCTION IS_INTERCOMPANY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_intercompany      IN OUT BOOLEAN,
                         I_distro_type       IN     VARCHAR2,
                         I_tsf_type          IN     TSFHEAD.TSF_TYPE%TYPE,
                         I_from_loc          IN     TSFHEAD.FROM_LOC%TYPE,
                         I_from_loc_type     IN     TSFHEAD.FROM_LOC_TYPE%TYPE,
                         I_to_loc            IN     TSFHEAD.TO_LOC%TYPE,
                         I_to_loc_type       IN     TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'TRANSFER_SQL.IS_INTERCOMPANY';

   L_system_options_row         SYSTEM_OPTIONS%ROWTYPE;
   L_to_loc_entity              tsf_entity.tsf_entity_id%TYPE := NULL;
   L_from_loc_entity            tsf_entity.tsf_entity_id%TYPE := NULL;
   L_entity_name                tsf_entity.tsf_entity_desc%TYPE := NULL;
   L_from_set_of_books_id       TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE := NULL;
   L_to_set_of_books_id         TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE := NULL;
   L_from_set_of_books_desc     FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE          := NULL;
   L_to_set_of_books_desc       FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE          := NULL;
   L_is_from_wh_physical        BOOLEAN := FALSE;
   L_is_to_wh_physical          BOOLEAN := FALSE;

BEGIN

   -- I_distro_type is 'A' for allocation, 'T' for transfer

   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_row) then
      return FALSE;
   end if;

   -- Check if either the from or to loc are physical wh.
   -- If so, then  return O_intercompany = FALSE.
   -- Because this automatically makes the transfer an intracompany transfer.
   if I_from_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_is_from_wh_physical,
                                 I_from_loc) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_to_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_is_to_wh_physical,
                                 I_to_loc) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_is_from_wh_physical = TRUE or L_is_to_wh_physical = TRUE then
      O_intercompany := FALSE;
      return TRUE;
   end if;

   -- if rac_rtv_tsf_ind = 'A', transfer types 'RAC' and 'RV',
   -- from and to loc's legal entities will always be the same, therefore, intra.
   if L_system_options_row.rac_rtv_tsf_ind = 'A' and
      I_distro_type = 'T' and
      I_tsf_type in ('RAC', 'RV') then
      O_intercompany := FALSE;
      return TRUE;
   end if;

   -- check from and to loc's legal entities and set of books id
   if L_system_options_row.intercompany_transfer_basis = 'T' then
      if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                        L_from_loc_entity,
                                        L_entity_name,
                                        I_from_loc,
                                        I_from_loc_type) = FALSE then
         return FALSE;
      end if;
      if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                        L_to_loc_entity,
                                        L_entity_name,
                                        I_to_loc,
                                        I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      O_intercompany := (L_from_loc_entity != L_to_loc_entity);
   else
      if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                      L_from_set_of_books_desc,
                                      L_from_set_of_books_id,
                                      I_from_loc,
                                      I_from_loc_type) = FALSE then
         return FALSE;
      end if;
      if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                      L_to_set_of_books_desc,
                                      L_to_set_of_books_id,
                                      I_to_loc,
                                      I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      O_intercompany := (L_from_set_of_books_id != L_to_set_of_books_id);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END IS_INTERCOMPANY;
-------------------------------------------------------------------------------------
FUNCTION CHECK_OPEN_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'TRANSFER_SQL.CHECK_OPEN_ALLOC';
   L_check     VARCHAR2(1)    := NULL;

   cursor C_CHECK_OPEN_ALLOC is
      select 'x'
        from alloc_header ah1
       where ((ah1.order_no = I_tsf_no
         and ah1.status != 'C')
          or exists (select 'x'
                       from alloc_header ah2
                      where ah2.status != 'C'
                        and ah2.alloc_parent = ah1.alloc_no
                        and ah1.order_no = I_tsf_no))
         and rownum = 1;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_OPEN_ALLOC',
                    'alloc_header',
                    to_char(I_tsf_no));
   open C_CHECK_OPEN_ALLOC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_OPEN_ALLOC',
                    'alloc_header',
                    to_char(I_tsf_no));
   fetch C_CHECK_OPEN_ALLOC into L_check;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_OPEN_ALLOC',
                    'alloc_header',
                    to_char(I_tsf_no));
   close C_CHECK_OPEN_ALLOC;

   if L_check is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_OPEN_ALLOC;
-------------------------------------------------------------------------------------
FUNCTION CLOSE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_transfer_no     IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN is

   L_closed         BOOLEAN;
   L_alloc_no       ALLOC_HEADER.ALLOC_NO%TYPE;
   L_tier_alloc_no  ALLOC_HEADER.ALLOC_NO%TYPE;
   L_program        VARCHAR2(50) := 'TRANSFER_SQL.CLOSE_ALLOC';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_CLOSE_ALH is
      select alh.alloc_no
        from alloc_header alh
       where alh.order_no = I_transfer_no
         and status != 'C'
         and alloc_parent is NULL;

   cursor C_CLOSE_ALLOC_TIER is
      select alloc_no
        from alloc_header
       where alloc_parent = L_alloc_no
         and status != 'C';

   cursor C_LOCK_ALD is
      select 'x'
        from alloc_detail
       where alloc_no = L_alloc_no
         for update nowait;

BEGIN
   if I_transfer_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_transfer_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('LOOP',
                    'C_CLOSE_ALH',
                    'ALLOC_HEADER',
                    'order no: '||to_char(I_transfer_no));
   LP_table := 'ALLOC_DETAIL';
   FOR c_close_alh_rec in C_CLOSE_ALH LOOP
      L_alloc_no := c_close_alh_rec.alloc_no;

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ALD',
                       'ALLOC_DETAIL',
                       'alloc no: '||to_char(L_alloc_no));
      open C_LOCK_ALD;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ALD',
                       'ALLOC_DETAIL',
                       'alloc no: '||to_char(L_alloc_no));
      close C_LOCK_ALD;

      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ALLOC_DETAIL',
                       'alloc no: '||to_char(L_alloc_no));
      update alloc_detail
         set qty_allocated = GREATEST(NVL(qty_transferred,0),NVL(qty_received,0)),
             qty_cancelled = qty_allocated - GREATEST(NVL(qty_transferred,0),NVL(qty_received,0))
       where alloc_no      = L_alloc_no;

      if APPT_DOC_CLOSE_SQL.CLOSE_ALLOC(O_error_message,
                                        L_closed,
                                        L_alloc_no) = FALSE then
         return FALSE;
      end if;

      FOR c_close_alloc_tier_rec in C_CLOSE_ALLOC_TIER LOOP
         L_tier_alloc_no := c_close_alloc_tier_rec.alloc_no;
         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_ALD',
                          'ALLOC_DETAIL',
                          'alloc no: '||to_char(L_tier_alloc_no));
         open C_LOCK_ALD;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_ALD',
                          'ALLOC_DETAIL',
                          'alloc no: '||to_char(L_tier_alloc_no));
         close C_LOCK_ALD;

         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'ALLOC_DETAIL',
                          'alloc no: '||to_char(L_tier_alloc_no));
         update alloc_detail
            set qty_allocated = GREATEST(NVL(qty_transferred,0),NVL(qty_received,0)),
                qty_cancelled = qty_allocated - GREATEST(NVL(qty_transferred,0),NVL(qty_received,0))
          where alloc_no      = L_tier_alloc_no;

         if APPT_DOC_CLOSE_SQL.CLOSE_ALLOC(O_error_message,
                                           L_closed,
                                           L_tier_alloc_no) = FALSE then
            return FALSE;
         end if;

      END LOOP;
   END LOOP;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('C_LOCK_ALD',
                                             to_char(I_transfer_no),
                                             NULL,
                                             NULL);

      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CLOSE_ALLOC;
-------------------------------------------------------------------------------------
FUNCTION CHECK_QTY_AVAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE,
                         I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                         I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)                    := 'TRANSFER_SQL.CHECK_QTY_AVAIL';
   L_tsf_qty            TSFDETAIL.TSF_QTY%TYPE          := 0;
   L_qty_avail          INV_STATUS_QTY.QTY%TYPE         := 0;
   L_item               INV_STATUS_QTY.ITEM%TYPE;
   L_inv_status         TSFDETAIL.INV_STATUS%TYPE;

   cursor C_GET_TSF_QTY is
      select item,
             tsf_qty,
             inv_status
        from tsfdetail
       where tsf_no     = I_tsf_no;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   O_exists := FALSE;
   FOR rec in C_GET_TSF_QTY LOOP
      L_item         := rec.item;
      L_tsf_qty      := rec.tsf_qty;
      L_inv_status   := rec.inv_status;

      if QUANTITY_AVAIL(O_error_message,
                        L_qty_avail,
                        L_item,
                        L_inv_status,
                        I_from_loc_type,
                        I_from_loc) = FALSE then
         return FALSE;
      end if;

      if L_qty_avail < L_tsf_qty then
         O_exists := TRUE;
         EXIT;
      end if;
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;
END CHECK_QTY_AVAIL;
-------------------------------------------------------------------------------------
FUNCTION GET_COST_RETAIL_VWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_loc_cost        IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                             O_loc_retail      IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                             I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS

   TYPE vwh_dist IS RECORD(
   from_loc_vwh TSFITEM_INV_FLOW.FROM_LOC%TYPE,
   dist_tsf_qty TSFITEM_INV_FLOW.TSF_QTY%TYPE);

   TYPE vwh_dist_TBL IS TABLE of vwh_dist INDEX BY BINARY_INTEGER;

   cursor C_GET_TSF_VWH is
      select tif.from_loc,
             tif.tsf_qty
        from tsfitem_inv_flow tif
       where tif.tsf_no = I_tsf_no
         and tif.item   = I_item
        order by from_loc;

   L_program      VARCHAR2(50)   := 'TRANSFER_SQL.GET_COST_RETAIL_VWH';
   L_dist_det     vwh_dist_TBL;
   L_count        BINARY_INTEGER  := 0;
   L_dummy_num    NUMBER;
   L_dummy_var    ITEM_LOC.SELLING_UOM%TYPE;
   L_total_cost   ITEM_LOC_SOH.UNIT_COST%TYPE := 0;
   L_total_retail ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_loc_cost     ITEM_LOC_SOH.UNIT_COST%TYPE := 0;
   L_loc_retail   ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_tsf_type     TSFHEAD.TSF_TYPE%TYPE;
   L_from_loc     TSFHEAD.FROM_LOC%TYPE;

   cursor C_GET_TSF_DTL is
      select th.tsf_type,
             th.from_loc
        from tsfhead th,
             wh wh
       where th.tsf_no =  I_tsf_no
         and th.from_loc = wh.wh
         and wh.wh <> wh.physical_wh;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   O_loc_cost   := 0;
   O_loc_retail := 0;

   FOR rec in C_GET_TSF_VWH LOOP

      L_count := L_count+1;

      L_dist_det(L_count).from_loc_vwh  := rec.from_loc;
      L_dist_det(L_count).dist_tsf_qty  := rec.tsf_qty;

      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  I_item,
                                                  L_dist_det(L_count).from_loc_vwh,
                                                  'W',
                                                  'Y',
                                                  L_loc_cost,
                                                  L_dummy_num,
                                                  L_loc_retail,
                                                  L_dummy_num,
                                                  L_dummy_var) = FALSE then
         return FALSE;
      end if;

      L_total_cost   := L_total_cost + L_loc_cost * L_dist_det(L_count).dist_tsf_qty;
      L_total_retail := L_total_retail + L_loc_retail * L_dist_det(L_count).dist_tsf_qty;

   end LOOP;

   O_loc_cost   := L_total_cost/I_tsf_qty;
   O_loc_retail := L_total_retail/I_tsf_qty;

   open C_GET_TSF_DTL;
   fetch C_GET_TSF_DTL into L_tsf_type,
                            L_from_loc;
   close C_GET_TSF_DTL;
   if L_tsf_type = 'CO' and L_from_loc is not null then
      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  I_item,
                                                  L_from_loc,
                                                  'W',
                                                  'Y',
                                                  O_loc_cost,
                                                  L_dummy_num,
                                                  O_loc_retail,
                                                  L_dummy_num,
                                                  L_dummy_var) = FALSE then
        return FALSE;
     end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_COST_RETAIL_VWH;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_BOL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_bol_no          IN OUT   BOL_SHIPSKU.BOL_NO%TYPE,
                     I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_BOL_NO is
      select bol_no
       from  bol_shipsku
      where  distro_no = I_tsf_no
        and  distro_type = 'T'
        and  rownum = 1;
   L_program      VARCHAR2(50)   := 'TRANSFER_SQL.GET_TSF_BOL';

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open  C_GET_BOL_NO;
   fetch C_GET_BOL_NO into O_bol_no;
   close C_GET_BOL_NO;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TSF_BOL;
---------------------------------------------------------------------------------------------
FUNCTION CREATE_TRANSFER_HEAD_RECORDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60)  := 'TRANSFER_SQL.CREATE_TRANSFER_HEAD_RECORDS';
   L_return_code        VARCHAR2(5);
   L_vdate              PERIOD.VDATE%TYPE := GET_VDATE;
   L_location           REPL_ITEM_LOC.LOCATION%TYPE;
   L_source_wh          REPL_ITEM_LOC.SOURCE_WH%TYPE;
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_tsf_no             TSFHEAD.TSF_NO%TYPE;


   cursor C_GET_REPL_RECORDS is
      select ril.source_wh,
             ril.location,
             ril.repl_order_ctrl,
             decode(so.dept_level_transfers,'Y',ril.dept,-1) dept,
             decode(ril.stock_cat, 'L', 'PL', 'SR') tsf_type,
             rnit.curr_order_lead_time curr_order_lead_time
        from repl_item_loc ril,
             rpl_net_inventory_tmp rnit,
             wh wah,
             sups sp,
             system_options so
       where ril.item = rnit.item
         and ril.location = rnit.location
         and ril.loc_type = rnit.loc_type
         and ril.source_wh = wah.wh
         and ril.primary_repl_supplier = sp.supplier(+)
         and ril.repl_method != 'SO'
         and (L_vdate!= ril.last_review_date
              or ril.last_review_date is NULL)
         and not exists (select 1
                           from tsfhead thd
                          where from_loc_type = 'W'
                            and from_loc = ril.source_wh
                            and to_loc_type = 'S'
                            and to_loc = ril.location
                            and status = 'B'
                            and inventory_type = 'A'
                            and freight_code = 'N'
                            and NVL(thd.dept,-1) = decode(so.dept_level_transfers,'Y',ril.dept,-1)
                            and thd.tsf_type = decode(ril.stock_cat, 'L', 'PL', 'SR')
                            and repl_tsf_approve_ind = decode(ril.repl_order_ctrl,'S','N','B','N','Y')
                            and NVL(thd.delivery_date,L_vdate) = NVL((L_vdate+rnit.curr_order_lead_time+1),L_vdate)
                         )
    group by ril.source_wh,
             ril.location,
             ril.repl_order_ctrl,
             decode(so.dept_level_transfers,'Y',ril.dept,-1),
             decode(ril.stock_cat, 'L', 'PL', 'SR'),
             rnit.curr_order_lead_time
    UNION
      select ril.source_wh,
             ril.location,
             ril.repl_order_ctrl,
             decode(so.dept_level_transfers,'Y',ril.dept,-1) dept,
             decode(ril.stock_cat, 'L', 'PL', 'SR') tsf_type,
             rnit.curr_order_lead_time curr_order_lead_time
        from repl_item_loc ril,
             rpl_net_inventory_tmp rnit,
             wh wah,
             sups sp,
             system_options so,
             store_orders sto
       where ril.item = rnit.item
         and ril.location = rnit.location
         and ril.loc_type = rnit.loc_type
         and ril.source_wh = wah.wh
         and rnit.location = sto.store
         and rnit.loc_type = 'S'
         and rnit.item = sto.item
         and ril.primary_repl_supplier = sp.supplier(+)
         and ril.repl_method ='SO'
         and sto.processed_date = L_vdate
         and sto.delivery_slot_id is NULL
         and sto.roq_extracted_ind = 'N'
         and (L_vdate!= ril.last_review_date
              or ril.last_revieW_date is NULL)
         and not exists (select 1
                           from tsfhead thd
                          where from_loc_type = 'W'
                            and from_loc = ril.source_wh
                            and to_loc_type = 'S'
                            and to_loc = ril.location
                            and status = 'B'
                            and inventory_type = 'A'
                            and freight_code = 'N'
                            and NVL(thd.dept,-1) = decode(so.dept_level_transfers,'Y',ril.dept,-1)
                            and thd.tsf_type = decode(ril.stock_cat, 'L', 'PL', 'SR')
                            and repl_tsf_approve_ind = decode(ril.repl_order_ctrl,'S','N','B','N','Y')
                            and NVL(thd.delivery_date,L_vdate) = NVL((L_vdate+rnit.curr_order_lead_time+1),L_vdate)
                         )
    group by ril.source_wh,
             ril.location,
             ril.repl_order_ctrl,
             decode(so.dept_level_transfers,'Y',ril.dept,-1),
             decode(ril.stock_cat, 'L', 'PL', 'SR'),
             rnit.curr_order_lead_time;

   cursor C_GET_REPL_RECORDS_DEL is
      select ril.source_wh,
             ril.location,
             ril.repl_order_ctrl,
             decode(so.dept_level_transfers,'Y',ril.dept,-1) dept,
             decode(ril.stock_cat, 'L', 'PL', 'SR') tsf_type,
             rnit.curr_order_lead_time curr_order_lead_time,
             sto.need_date,
             sto.delivery_slot_id
        from repl_item_loc ril,
             rpl_net_inventory_tmp rnit,
             wh wah,
             sups sp,
             system_options so,
             store_orders sto
       where ril.item = rnit.item
         and ril.location = rnit.location
         and ril.loc_type = rnit.loc_type
         and ril.source_wh = wah.wh
         and rnit.location = sto.store
         and rnit.loc_type = 'S'
         and rnit.item = sto.item
         and ril.primary_repl_supplier = sp.supplier(+)
         and ril.repl_method ='SO'
         and sto.processed_date = L_vdate
         and sto.delivery_slot_id is NOT NULL
         and sto.roq_extracted_ind = 'N'
         and (L_vdate!= ril.last_review_date
              or ril.last_review_date is NULL)
         and not exists (select 1
                           from tsfhead thd
                          where from_loc_type = 'W'
                            and from_loc = ril.source_wh
                            and to_loc_type = 'S'
                            and to_loc = ril.location
                            and status = 'B'
                            and inventory_type = 'A'
                            and freight_code = 'N'
                            and thd.delivery_slot_id != sto.delivery_slot_id
                            and NVL(thd.dept,-1) = decode(so.dept_level_transfers,'Y',ril.dept,-1)
                            and thd.tsf_type = decode(ril.stock_cat, 'L', 'PL', 'SR')
                            and repl_tsf_approve_ind = decode(ril.repl_order_ctrl,'S','N','B','N','Y')
                            and NVL(thd.delivery_date,L_vdate) = NVL((L_vdate+rnit.curr_order_lead_time+1),L_vdate)
                         )
    group by ril.source_wh,
             ril.location,
             ril.repl_order_ctrl,
             decode(so.dept_level_transfers,'Y',ril.dept,-1),
             decode(ril.stock_cat, 'L', 'PL', 'SR'),
          rnit.curr_order_lead_time,
             sto.need_date,
             delivery_slot_id;

BEGIN

   FOR rec in C_GET_REPL_RECORDS LOOP
      P_tsfhead_size := P_tsfhead_size + 1;

      -- Get the next transfer number from the sequence
      NEXT_TRANSFER_NUMBER(L_tsf_no,
                           L_return_code,
                           O_error_message);
      if L_return_code != 'TRUE' then
         return FALSE;
      end if;

      P_tsf_no(P_tsfhead_size) := L_tsf_no;

      -- Get the values fetched from the C_GET_REPL_RECORDS cursor into the TYPE elements.
      P_source_wh(P_tsfhead_size)             := rec.source_wh;
      P_location(P_tsfhead_size)              := rec.location;
      P_repl_order_ctrl(P_tsfhead_size)       := rec.repl_order_ctrl;
      P_dept(P_tsfhead_size)                  := rec.dept;
      P_tsf_type(P_tsfhead_size)              := rec.tsf_type;
      P_curr_order_lead_time(P_tsfhead_size)  := rec.curr_order_lead_time;
      P_need_date(P_tsfhead_size)             := NULL;
      P_delivery_slot_id(P_tsfhead_size)      := NULL;

   END LOOP;

   FOR rec in C_GET_REPL_RECORDS_DEL LOOP
      P_tsfhead_size := P_tsfhead_size + 1;

      -- Get the next transfer number from the sequence
      NEXT_TRANSFER_NUMBER(L_tsf_no,
                           L_return_code,
                           O_error_message);
      if L_return_code != 'TRUE' then
         return FALSE;
      end if;

      P_tsf_no(P_tsfhead_size) := L_tsf_no;

      -- Get the values fetched from the C_GET_REPL_RECORDS_DEL cursor into the TYPE elements.
      P_source_wh(P_tsfhead_size)             := rec.source_wh;
      P_location(P_tsfhead_size)              := rec.location;
      P_repl_order_ctrl(P_tsfhead_size)       := rec.repl_order_ctrl;
      P_dept(P_tsfhead_size)                  := rec.dept;
      P_tsf_type(P_tsfhead_size)              := rec.tsf_type;
      P_curr_order_lead_time(P_tsfhead_size)  := rec.curr_order_lead_time;
      P_need_date(P_tsfhead_size)             := rec.need_date;
      P_delivery_slot_id(P_tsfhead_size)      := rec.delivery_slot_id;

   END LOOP;

   if P_tsfhead_size > 0 then
      FORALL i in 1..P_tsfhead_size
      -- Insert the records into TSFHEAD
         insert into tsfhead(tsf_no,
                             from_loc_type,
                             from_loc,
                             to_loc_type,
                             to_loc,
                             dept,
                             tsf_type,
                             status,
                             freight_code,
                             create_date,
                             create_id,
                             approval_date,
                             approval_id,
                             delivery_date,
                             repl_tsf_approve_ind,
                             inventory_type,
                             delivery_slot_id)
                      values(P_tsf_no(i),
                            'W',
                             P_source_wh(i),
                            'S',
                             P_location(i),
                             decode(P_dept(i),'-1',NULL,P_dept(i)),
                             P_tsf_type(i),
                             'B',
                             'N',
                             L_vdate,
                             'BATCH',
                             NULL,
                             NULL,
                             decode(P_delivery_slot_id(i),NULL,TO_DATE((L_vdate +1+NVL(P_curr_order_lead_time(i),0)),'YYYYMMDD'),P_need_date(i)),
                             decode(P_repl_order_ctrl(i),'S','N','B','N','Y'),
                             'A',
                             P_delivery_slot_id(i));
   end if;

   P_tsfhead_size := 0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_TRANSFER_HEAD_RECORDS;
---------------------------------------------------------------------------------
FUNCTION LOCK_TSFHEAD (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TH is
     select 'x'
       from tsfhead
      where tsf_no = I_tsf_no
         or tsf_parent_no = I_tsf_no
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_LOCK_TH','TSFHEAD',to_char(I_tsf_no));
   open C_LOCK_TH;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_TH','TSFHEAD',to_char(I_tsf_no));
   close C_LOCK_TH;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TSFHEAD_REC_LOCK',
                                             I_tsf_no,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSFER_SQL.LOCK_TSFHEAD',
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_TSFHEAD;
-------------------------------------------------------------------------------------
FUNCTION BULK_LOCK_TSFDETAIL (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TSFDETAIL is
      select 'x'
        from tsfdetail td
       where exists (select 'x'
                       from gtt_rowid_temp rt
                      where rt.row_id = td.rowid
                        and rownum <= 1)
      for update nowait;


BEGIN

   SQL_LIB.SET_MARK('OPEN','C_LOCK_TSFDETAIL','TSFDETAIL_LOCK',NULL);
   open C_LOCK_TSFDETAIL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_TSFDETAIL','TSFDETAIL_LOCK',NULL);
   close C_LOCK_TSFDETAIL;

   SQL_LIB.SET_MARK('DELETE',NULL,'GTT_ROWID_TEMP',NULL);
   delete from gtt_rowid_temp;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'TSFDETAIL',
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSFER_SQL.BULK_LOCK_TSFDETAIL',
                                            to_char(SQLCODE));
      return FALSE;
END BULK_LOCK_TSFDETAIL;
-------------------------------------------------------------------------------------
FUNCTION UPDATE_TSF_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                         I_item            IN       TSFDETAIL.ITEM%TYPE,
                         I_tsf_cost        IN       TSFDETAIL.TSF_COST%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)   := 'TRANSFER_SQL.UPDATE_TSF_COST';

   cursor C_LOCK_TSFDETAIL is
      select 'x'
        from tsfdetail td
       where td.tsf_no = I_tsf_no
         and td.item = I_item
         and exists (select 'x'
                       from tsfhead th
                      where th.tsf_no = td.tsf_no
                        and th.tsf_type = 'SG'
                        and th.status = 'C')
         for update nowait;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_TSFDETAIL',
                    'tsfdetail',
                    'tsf_no: '||(I_tsf_no));
   open  C_LOCK_TSFDETAIL;
   ---

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_TSFDETAIL',
                    'tsfdetail',
                    'tsf_no: '||(I_tsf_no));
   close C_LOCK_TSFDETAIL;
   ---

   LP_table := 'tsfdetail';
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'TSFDETAIL',
                    'TSF_NO: '||(I_tsf_no));

   ---
   update tsfdetail td
      set td.tsf_cost = I_tsf_cost
    where td.tsf_no = I_tsf_no
      and td.item = I_item
      and exists (select 'x'
                    from tsfhead th
                   where th.tsf_no = td.tsf_no
                     and th.tsf_type = 'SG'
                     and th.status = 'C');
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            to_char(I_tsf_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_TSF_COST;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_ORDER_NO(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_order_no       OUT      TSFHEAD.ORDER_NO%TYPE,
                          I_tsf_no         IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)   := 'TRANSFER_SQL.GET_TSF_ORDER_NO';

   cursor C_GET_TSF_ORDER_NO is
      select order_no
        from tsfhead
       where tsf_no = I_tsf_no;
BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TSF_ORDER_NO',
                    'tsfhead',
                    'tsf_no: '||(I_tsf_no));
   open  C_GET_TSF_ORDER_NO;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TSF_ORDER_NO',
                    'tsfhead',
                    'tsf_no: '||(I_tsf_no));
   fetch  C_GET_TSF_ORDER_NO into O_order_no;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TSF_ORDER_NO',
                    'tsfhead',
                    'tsf_no: '||(I_tsf_no));
   close  C_GET_TSF_ORDER_NO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TSF_ORDER_NO;
---------------------------------------------------------------------------------------------
FUNCTION DEL_UTIL_TSF(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_loc            IN      ITEM_LOC.LOC%TYPE,
                      I_tsf_no         IN      TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(60):= 'TRANSFER_SQL.DEL_UTIL_TSF';
   L_table           VARCHAR2(30);
   L_country_id      COUNTRY.COUNTRY_ID%TYPE;
   L_localized_ind   VARCHAR2(1) := 'N';
   L_entity          VARCHAR2(4) := 'LOC';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_TSFHEAD_L10N_EXT is
      select 'x'
        from tsfhead_l10n_ext
       where tsf_no  = I_tsf_no
         for update nowait;

BEGIN

   if L10N_SQL.GET_CNTRY_LOCAIND(O_error_message,
                                 L_country_id,
                                 L_localized_ind,
                                 L_entity,
                                 NULL,
                                 to_char(I_loc)) = FALSE then
      return FALSE;
   end if;


   if L_localized_ind = 'N' then
      L_table := 'TSFHEAD_L10N_EXT';

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_TSFHEAD_L10N_EXT',
                       L_table,
                       'tsf_no: '||I_tsf_no);

      open  C_LOCK_TSFHEAD_L10N_EXT;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_TSFHEAD_L10N_EXT',
                       L_table,
                       'tsf_no: '||I_tsf_no);

      close C_LOCK_TSFHEAD_L10N_EXT;
      ---
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       L_table,
                      'tsf_no: '||I_tsf_no);

      delete from tsfhead_l10n_ext
            where tsf_no = I_tsf_no
              and l10n_country_id != L_country_id;
      ---
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                            'tsf_no: ' || to_char(I_tsf_no),
                                             NULL);
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END DEL_UTIL_TSF;
---------------------------------------------------------------------------------------------
FUNCTION POP_ORDCUST_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)             := 'TRANSFER_SQL.POP_ORDCUST_DETAIL';

   L_ordcust_no    ORDCUST.ORDCUST_NO%TYPE  := NULL;
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_ORDCUST_DETAIL_EXISTS is
      select distinct ordcust_no
        from ordcust_detail
       where ordcust_no in (select ordcust_no
                              from ordcust
                             where tsf_no = I_tsf_no);

   cursor C_LOCK_ORDCUST_DETAIL is
      select 'x'
        from ordcust_detail
       where ordcust_no = L_ordcust_no
         for update nowait;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ORDCUST_DETAIL_EXISTS;
   fetch C_ORDCUST_DETAIL_EXISTS into L_ordcust_no;
   close C_ORDCUST_DETAIL_EXISTS;

   if L_ordcust_no is NOT NULL then
      -- Delete all ORDCUST_DETAIL records for this transfer and then reinsert data
      L_table := 'ordcust_detail';

      open C_LOCK_ORDCUST_DETAIL;
      close C_LOCK_ORDCUST_DETAIL;

      delete from ordcust_detail
       where ordcust_no = L_ordcust_no;
   end if;

   insert into ordcust_detail(ordcust_no,
                              item,
                              ref_item,
                              original_item,
                              qty_ordered_suom,
                              qty_cancelled_suom,
                              standard_uom,
                              transaction_uom,
                              substitute_allowed_ind,
                              unit_retail,
                              retail_currency_code,
                              comments,
                              create_datetime,
                              create_id,
                              last_update_datetime,
                              last_update_id)
                       select oc.ordcust_no,
                              td.item,
                              NULL,             -- ref item
                              NULL,             -- original_item
                              td.tsf_qty,       -- qty_ordered_suom
                              NULL,             -- qty_cancelled_suom
                              im.standard_uom,  -- standard_uom
                              im.standard_uom,  -- transaction_uom            # for transfers created in RMS, transaction_uom is always in item's standard_uom
                              'N',              -- substitute_allowed_ind
                              il.unit_retail,   -- unit_retail
                              s.currency_code,  -- retail_currency_code
                              NULL,             -- comments
                              sysdate,          -- create_datetime
                              get_user,             -- create_id
                              sysdate,          -- last_update_datetime
                              get_user              -- last_update_id
                         from ordcust oc,
                              tsfdetail td,
                              item_master im,
                              item_loc il,
                              store s
                        where oc.tsf_no         = I_tsf_no
                          and oc.tsf_no         = td.tsf_no
                          and td.item           = im.item
                          and td.item           = il.item
                          and oc.fulfill_loc_id = il.loc    --fulfillment location's unit retail is used
                          and oc.fulfill_loc_id = s.store;  --fulfillment locatoin is always a virtual store when a customer order transfer is created on-line

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                            'tsf_no: ' || TO_CHAR(I_tsf_no),
                                             NULL);
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END POP_ORDCUST_DETAIL;
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_RANGED_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item            IN       TSFDETAIL.ITEM%TYPE,
                           I_loc             IN       TSFHEAD.TO_LOC%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)             := 'TRANSFER_SQL.UPDATE_RANGED_IND';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_LOC is
      select 'x'
        from item_loc
       where item = I_item
         and loc = I_loc
         and ranged_ind = 'N'
         for update of ranged_ind nowait;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   L_table := 'ITEM_LOC';

    open C_LOCK_ITEM_LOC;
    close C_LOCK_ITEM_LOC;

    update item_loc
       set ranged_ind = 'Y'
     where item = I_item
       and loc = I_loc
       and ranged_ind = 'N';

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                            'Item: ' || I_item || 'Loc: ' || TO_CHAR(I_loc),
                                             NULL);
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END UPDATE_RANGED_IND;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_ATTRIB(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_from_finisher_ind   IN OUT WH.FINISHER_IND%TYPE,
                        O_to_finisher_ind     IN OUT WH.FINISHER_IND%TYPE,
                        O_intercompany        IN OUT BOOLEAN,
                        I_tsf_alloc_type      IN     SHIPSKU.DISTRO_TYPE%TYPE,
                        I_tsf_alloc_no        IN     SHIPSKU.DISTRO_NO%TYPE,
                        I_from_loc            IN     TSFHEAD.FROM_LOC%TYPE,
                        I_from_loc_type       IN     TSFHEAD.FROM_LOC_TYPE%TYPE,
                        I_to_loc              IN     TSFHEAD.TO_LOC%TYPE,
                        I_to_loc_type         IN     TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64) := 'TRANSFER_SQL.GET_TSF_ATTRIB';

   L_tsf_type                 tsfhead.tsf_type%TYPE := NULL;
   L_finisher                 BOOLEAN;
   L_vwh_name                 WH.WH_NAME%TYPE;

   cursor C_TSF_TYPE is
      select tsf_type
        from tsfhead
       where tsf_no = I_tsf_alloc_no;

BEGIN
   ---
   if I_tsf_alloc_type = 'T' then
      open C_TSF_TYPE;
      fetch C_TSF_TYPE into L_tsf_type;
      close C_TSF_TYPE;
   end if;

   if TRANSFER_SQL.IS_INTERCOMPANY(O_error_message,
                                   O_intercompany,
                                   I_tsf_alloc_type,
                                   L_tsf_type,
                                   I_from_loc,
                                   I_from_loc_type,
                                   I_to_loc,
                                   I_to_loc_type) = FALSE then
      return FALSE;
   end if;
   ---
   if I_from_loc_type = 'W' then
      ---
      if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                      L_finisher,
                                      L_vwh_name,
                                      I_from_loc) = FALSE then
         return FALSE;
      end if;
      ---
      if L_finisher = TRUE then
         O_from_finisher_ind := 'Y';
      end if;
      ---
   elsif I_from_loc_type = 'E' then
      O_from_finisher_ind := 'Y';
   end if;
   ---
   if I_to_loc_type = 'W' then
      ---
      if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                      L_finisher,
                                      L_vwh_name,
                                      I_to_loc) = FALSE then
         return FALSE;
      end if;
      ---
      if L_finisher = TRUE then
         O_to_finisher_ind := 'Y';
      end if;
      ---
   elsif I_to_loc_type = 'E' then
      O_to_finisher_ind := 'Y';
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_TSF_ATTRIB;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_TYPE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_tsf_type        IN OUT   TSFHEAD.TSF_TYPE%TYPE,
                      I_tsf_no          IN       SHIPSKU.DISTRO_NO%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'TRANSFER_SQL.GET_TSF_TYPE';

   cursor C_TSF_TYPE is
      select tsf_type
        from tsfhead
       where tsf_no    = I_tsf_no;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_TSF_TYPE',
                    'tsfhead',
                    NULL);
   open C_TSF_TYPE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_TSF_TYPE',
                    'tsfhead',
                    NULL);
   fetch C_TSF_TYPE into O_tsf_type;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_TSF_TYPE',
                    'tsfhead',
                    NULL);
   close C_TSF_TYPE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_TSF_TYPE;
---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_VAT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_total_vat       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                       O_total_cost      IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                       I_tsf_no          IN       SHIPSKU.DISTRO_NO%TYPE,
                       I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE,
                       I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                       I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                       I_to_loc          IN       TSFHEAD.FROM_LOC%TYPE,
                       I_to_loc_type     IN       TSFHEAD.FROM_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)    := 'TRANSFER_SQL.GET_TOTAL_VAT';

   L_tax_calc_tbl            OBJ_TAX_CALC_TBL              := OBJ_TAX_CALC_TBL ();
   L_tax_calc_rec            OBJ_TAX_CALC_REC              := OBJ_TAX_CALC_REC();
   L_vat                     ITEM_LOC.UNIT_RETAIL%TYPE     :=0;
   L_entity_name             TSF_ENTITY.TSF_ENTITY_DESC%TYPE;
   L_from_loc_entity         TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_to_loc_entity           TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_item                    ITEM_MASTER.ITEM%TYPE;
   L_comp_item               ITEM_MASTER.ITEM%TYPE;
   L_pack_ind                ITEM_MASTER.PACK_IND%TYPE;
   L_to_cost                 WF_ORDER_DETAIL.CUSTOMER_COST%TYPE;
   L_from_cost               WF_ORDER_DETAIL.CUSTOMER_COST%TYPE;
   L_tsf_price               TSFDETAIL.TSF_PRICE%TYPE;
   L_tsf_qty                 TSFDETAIL.TSF_QTY%TYPE;
   L_av_cost_from            ITEM_LOC_SOH.AV_COST%TYPE;
   L_unit_cost_from          ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_unit_retail_from        ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom             ITEM_LOC.SELLING_UOM%TYPE;
   L_loc                     TSFHEAD.FROM_LOC%TYPE;
   L_loc_type                TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_from_loc                TSFHEAD.FROM_LOC%TYPE;
   L_to_loc                  TSFHEAD.FROM_LOC%TYPE;
   L_from_loc_type           VARCHAR2(2);
   L_to_loc_type             VARCHAR2(2);

   cursor C_ITEM is
      select td.item,
             im.pack_ind,
             (nvl(td.tsf_price,0)*td.tsf_qty) tsf_price,
             td.tsf_qty
        from tsfhead th,
             tsfdetail td,
             item_master im
       where td.tsf_no = th.tsf_no
         and im.item = td.item
         and th.tsf_no = I_tsf_no;

   cursor C_TSF_2ND_LEG is
      select th.from_loc from_loc_2st_leg,
             th.from_loc_type from_loc_type_2st_leg
        from tsfhead th
       where th.tsf_parent_no = I_tsf_no;

   cursor C_CUSTOMER_COST is
      select wfd.customer_cost*td.tsf_qty customer_cost
        from wf_order_detail wfd,
             wf_order_head wfh,
             tsfdetail td,
             tsfhead th
        where th.tsf_no = td.tsf_no
          and th.wf_order_no = wfh.wf_order_no
          and wfd.wf_order_no = wfh.wf_order_no
          and td.item = wfd.item
          and td.item = L_item
          and th.tsf_no = I_tsf_no;

   cursor C_RETURN_COST is
      select (wrd.return_unit_cost * td.tsf_qty) return_cost
        from wf_return_head wrh,
             wf_return_detail wrd,
             tsfdetail td,
             tsfhead th
       where th.tsf_no = td.tsf_no
         and th.rma_no = wrh.rma_no
         and wrd.rma_no = wrh.rma_no
         and td.item = wrd.item
         and td.item = L_item
         and th.tsf_no = I_tsf_no;

   cursor C_TSF_COST is
      select (nvl(td.tsf_price,ils.av_cost) * td.tsf_qty) tsf_cost
        from tsfhead th,
             tsfdetail td,
             item_loc_soh ils
       where td.tsf_no = th.tsf_no
         and ils.item = td.item
         and th.tsf_no = I_tsf_no
         and td.item = L_item
         and ils.loc = I_from_loc;

   cursor C_GET_WAC is
      select vpq.item,
             (vpq.qty * ils.av_cost) tsf_cost
        from item_loc_soh ils,
             v_packsku_qty vpq
       where vpq.item = ils.item
         and ils.loc = I_from_loc
         and vpq.pack_no = L_item;
BEGIN
   if I_tsf_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
      if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   O_total_cost := 0;
   if I_tsf_type = 'IC'
      or I_tsf_type = 'RAC'
      or I_tsf_type = 'RV' then
      open C_TSF_2ND_LEG;
      fetch C_TSF_2ND_LEG into L_loc,
                               L_loc_type;
      close C_TSF_2ND_LEG;

      if  L_loc is not null then
          if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                            L_from_loc_entity,
                                            L_entity_name,
                                            I_from_loc,
                                            I_from_loc_type) = FALSE then
               return FALSE;
           end if;
          if LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                            L_to_loc_entity,
                                            L_entity_name,
                                            L_loc,
                                            L_loc_type) = FALSE then
             return FALSE;
          end if;
          if L_from_loc_entity <> L_to_loc_entity then
             L_to_loc      := L_loc;
             L_from_loc    := I_from_loc;
             if I_from_loc_type = 'W' then
                L_from_loc_type := 'WH';
             elsif I_from_loc_type = 'S' then
                   L_from_loc_type := 'ST';
             end if;
             if L_loc_type = 'S' then
                L_to_loc_type := 'ST';
             elsif L_loc_type = 'W' then
                 L_to_loc_type := 'WH';
             else
                 L_to_loc_type := L_loc_type;
             end if;
          elsif L_from_loc_entity = L_to_loc_entity then
                L_from_loc      := L_loc;
                L_to_loc        := I_to_loc;
                if L_loc_type = 'S' then
                   L_from_loc_type := 'ST';
                elsif L_loc_type = 'W' then
                      L_from_loc_type := 'WH';
                else
                    L_from_loc_type := L_loc_type;
                end if;
                if I_to_loc_type = 'S' then
                   L_to_loc_type := 'ST';
                elsif I_to_loc_type = 'W' then
                      L_to_loc_type := 'WH';
                end if;
          end if;

      else
          L_from_loc := I_from_loc;
          L_to_loc   := I_to_loc;
         if I_from_loc_type = 'W' then
            L_from_loc_type := 'WH';
         elsif I_from_loc_type = 'S' then
               L_from_loc_type := 'ST';
         end if;

         if I_to_loc_type = 'W' then
            L_to_loc_type := 'WH';
         elsif I_to_loc_type = 'S' then
               L_to_loc_type := 'ST';
         end if;
      end if;
         for rec in C_ITEM
          loop
          O_total_cost                         := O_total_cost + rec.tsf_price;
          L_tax_calc_rec.I_item                := rec.item;
          L_tax_calc_rec.I_amount              := rec.tsf_price;
          L_tax_calc_rec.I_from_entity         := L_from_loc;
          L_tax_calc_rec.I_from_entity_type    := L_from_loc_type;
          L_tax_calc_rec.I_to_entity_type      := L_to_loc_type;
          L_tax_calc_rec.I_to_entity           := L_to_loc;
          L_tax_calc_tbl.EXTEND();
          L_tax_calc_tbl(L_tax_calc_tbl.count)  := L_tax_calc_rec;
         end loop;
         if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                    L_tax_calc_tbl) = FALSE then
          return FALSE;
         end if;

         if L_tax_calc_tbl.count > 0 and L_tax_calc_tbl.count is not null then
            for i in L_tax_calc_tbl.first..L_tax_calc_tbl.last
            loop
                if L_tax_calc_tbl(i).O_tax_exempt_ind = 'Y'then
                   L_vat := NULL;
                else
                   L_vat  := L_vat + L_tax_calc_tbl(i).O_total_tax_amount;
                end if;
            end loop;
         end if;

      O_total_vat := L_vat;
   else
      if I_from_loc_type = 'W' then
         L_from_loc_type := 'WH';
      elsif I_from_loc_type = 'S' then
            L_from_loc_type := 'ST';
      end if;
      if I_to_loc_type = 'W' then
         L_to_loc_type := 'WH';
      elsif I_to_loc_type = 'S' then
            L_to_loc_type := 'ST';
      end if;
      for c_rec in C_ITEM
      loop
         L_item      := c_rec.item;
         L_pack_ind  := c_rec.pack_ind;
         L_tsf_price := c_rec.tsf_price;
         L_tsf_qty   := c_rec.tsf_qty;
         if I_tsf_type = 'SG' then
            L_tax_calc_rec.I_item                := L_item;
            L_tax_calc_rec.I_amount              := L_tsf_price;
            L_tax_calc_rec.I_from_entity         := I_from_loc;
            L_tax_calc_rec.I_from_entity_type    := L_from_loc_type;
            L_tax_calc_rec.I_to_entity_type      := L_to_loc_type;
            L_tax_calc_rec.I_to_entity           := I_to_loc;
            L_tax_calc_tbl.EXTEND();
            L_tax_calc_tbl(L_tax_calc_tbl.count)  := L_tax_calc_rec;
         elsif I_tsf_type = 'FO' then
               open C_CUSTOMER_COST;
               fetch C_CUSTOMER_COST into L_to_cost;
               close C_CUSTOMER_COST;
           -- Convert the to loc customer cost to from loc currency
               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   I_to_loc,
                                                   I_to_loc_type,
                                                   NULL,
                                                   I_from_loc,
                                                   I_from_loc_type,
                                                   NULL,
                                                   L_to_cost,
                                                   L_from_cost,
                                                   'C',
                                                   NULL,
                                                   NULL) = FALSE then
                 return FALSE;
               end if;
               O_total_cost := O_total_cost + L_from_cost;
               L_tax_calc_rec.I_item                := L_item;
               L_tax_calc_rec.I_amount              := L_from_cost;
               L_tax_calc_rec.I_from_entity         := I_from_loc;
               L_tax_calc_rec.I_from_entity_type    := L_from_loc_type;
               L_tax_calc_rec.I_to_entity_type      := L_to_loc_type;
               L_tax_calc_rec.I_to_entity           := I_to_loc;
               L_tax_calc_tbl.EXTEND();
               L_tax_calc_tbl(L_tax_calc_tbl.count)  := L_tax_calc_rec;
         elsif I_tsf_type = 'FR' then
               open C_RETURN_COST;
               fetch C_RETURN_COST into L_from_cost;
               close C_RETURN_COST;
               O_total_cost := O_total_cost + L_from_cost;
               L_tax_calc_rec.I_item                := L_item;
               L_tax_calc_rec.I_amount              := L_from_cost;
               L_tax_calc_rec.I_from_entity         := I_from_loc;
               L_tax_calc_rec.I_from_entity_type    := L_from_loc_type;
               L_tax_calc_rec.I_to_entity_type      := L_to_loc_type;
               L_tax_calc_rec.I_to_entity           := I_to_loc;
               L_tax_calc_tbl.EXTEND();
               L_tax_calc_tbl(L_tax_calc_tbl.count)  := L_tax_calc_rec;
         elsif I_tsf_type = 'SR' then
               open C_TSF_COST;
               fetch C_TSF_COST into L_from_cost;
               close C_TSF_COST;
               O_total_cost := O_total_cost + L_from_cost;
               L_tax_calc_rec.I_item                := L_item;
               L_tax_calc_rec.I_amount              := L_from_cost;
               L_tax_calc_rec.I_from_entity         := I_from_loc;
               L_tax_calc_rec.I_from_entity_type    := L_from_loc_type;
               L_tax_calc_rec.I_to_entity_type      := L_to_loc_type;
               L_tax_calc_rec.I_to_entity           := I_to_loc;
               L_tax_calc_tbl.EXTEND();
               L_tax_calc_tbl(L_tax_calc_tbl.count)  := L_tax_calc_rec;
         elsif I_tsf_type = 'CO'
               or I_tsf_type = 'EG' then
              if L_pack_ind = 'Y' then
              -- Get the wac of the component
                 for i in C_GET_WAC
                 loop
                   O_total_cost := O_total_cost + (i.tsf_cost*L_tsf_qty);
                   L_tax_calc_rec.I_item                := i.item;
                   L_tax_calc_rec.I_amount              := i.tsf_cost*L_tsf_qty;
                   L_tax_calc_rec.I_from_entity         := I_from_loc;
                   L_tax_calc_rec.I_from_entity_type    := L_from_loc_type;
                   L_tax_calc_rec.I_to_entity_type      := L_to_loc_type;
                   L_tax_calc_rec.I_to_entity           := I_to_loc;
                   L_tax_calc_tbl.EXTEND();
                   L_tax_calc_tbl(L_tax_calc_tbl.count)  := L_tax_calc_rec;
                 end loop;
              else
                  open C_TSF_COST;
                  fetch C_TSF_COST into L_from_cost;
                  close C_TSF_COST;
                  O_total_cost := O_total_cost + L_from_cost;
                  L_tax_calc_rec.I_item                := L_item;
                  L_tax_calc_rec.I_amount              := L_from_cost;
                  L_tax_calc_rec.I_from_entity         := I_from_loc;
                  L_tax_calc_rec.I_from_entity_type    := L_from_loc_type;
                  L_tax_calc_rec.I_to_entity_type      := L_to_loc_type;
                  L_tax_calc_rec.I_to_entity           := I_to_loc;
                  L_tax_calc_tbl.EXTEND();
                  L_tax_calc_tbl(L_tax_calc_tbl.count)  := L_tax_calc_rec;
              end if;
         end if;
      end loop;
      if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                 L_tax_calc_tbl) = FALSE then
       return FALSE;
      end if;

      if L_tax_calc_tbl.count > 0 and L_tax_calc_tbl.count is not null then
         for i in L_tax_calc_tbl.first..L_tax_calc_tbl.last
         loop
             if L_tax_calc_tbl(i).O_tax_exempt_ind = 'Y'then
                L_vat := NULL;
             else
                L_vat  := L_vat + L_tax_calc_tbl(i).O_total_tax_amount;
             end if;
         end loop;
      end if;

      O_total_vat := L_vat;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_TOTAL_VAT;
-----------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_tsf_no            IN OUT   TSFHEAD.TSF_NO%TYPE,
                    I_function_key      IN OUT   VARCHAR2,
                    I_seq_no            IN OUT   NUMBER)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(75)   := 'TRANSFER_SQL.CUSTOM_VAL';
   L_custom_obj_rec      CUSTOM_OBJ_REC :=  CUSTOM_OBJ_REC();

BEGIN

   L_custom_obj_rec.function_key:= I_function_key;
   L_custom_obj_rec.call_seq_no:= I_seq_no;
   L_custom_obj_rec.tsf_no:= I_tsf_no;

   --call the custom code for client specific order approval
   if CALL_CUSTOM_SQL.EXEC_FUNCTION(O_error_message,
                                    L_custom_obj_rec) = FALSE then
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CUSTOM_VAL;
-----------------------------------------------------------------------------------
END TRANSFER_SQL;
/