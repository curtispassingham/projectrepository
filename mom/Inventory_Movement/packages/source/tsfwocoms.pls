
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TSF_WO_COMP_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: WO_ITEM_COMP
-- Purpose      : Processes data from a work order complete message.
--                Calculate quantities of items that were transformed into I_item
--                at the finisher and create inventory adjustments to process.
--                Update the appropriate tables for I_item (or for component items
--                if I_item is a pack). If I_bk_tsf_exe = TRUE then call TSF_BT_SQL.BT_EXECUTE
--                to book transfer the item from the finisher to the final transfer location
---------------------------------------------------------------------------------------------
FUNCTION WO_ITEM_COMP(O_error_message       IN OUT rtk_errors.rtk_text%TYPE,
                      I_tsf_no              IN     tsfhead.tsf_no%TYPE,
                      I_tsf_parent_no       IN     tsfhead.tsf_no%TYPE,
                      I_item                IN     item_master.item%TYPE,
                      I_finisher            IN     item_loc.loc%TYPE,
                      I_finisher_loc_type   IN     item_loc.loc_type%TYPE,
                      I_final_loc           IN     item_loc.loc%TYPE,
                      I_final_loc_type      IN     item_loc.loc_type%TYPE,
                      I_qty                 IN     tsfdetail.tsf_qty%TYPE,
                      I_complete_date       IN     DATE,
                      I_bk_tsf_exe          IN     BOOLEAN)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DOC_CLOSE_QUEUE_INSERT
-- Purpose      : Inserts the transfer number into the DOC_CLOSE_QUEUE to determine if the 
--                transfer can be closed.
---------------------------------------------------------------------------------------------
FUNCTION DOC_CLOSE_QUEUE_INSERT(O_error_message  IN OUT  rtk_errors.rtk_text%TYPE,
                                I_tsf_no         IN      tsfhead.tsf_no%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END;
/

