CREATE OR REPLACE PACKAGE BODY ALLOC_BOOK_TSF_SQL AS
--------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
--------------------------------------------------------------------------------------
FUNCTION PROCESS_WH_SOURCE_ALLOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_alloc_no_tbl    IN       ALLOC_NO_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_RCPT_SOURCED_ALLOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_alloc_no_tbl    IN       ALLOC_NO_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_BOOK_TSF (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_thread_no       IN       NUMBER,
                           I_num_threads     IN       NUMBER)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)      := 'ALLOC_BOOK_TSF_SQL.PROCESS_BOOK_TSF';
   L_vdate                    PERIOD.VDATE%TYPE := get_vdate();
   L_wh_src_alloc_no_tbl      ALLOC_NO_TBL;
   L_inb_rcpt_alloc_no_tbl    ALLOC_NO_TBL;

   -- Return all warehouse-sourced allocations that are candidates for book transfer creation.
   -- Such allocations contain at least one unprocessed alloc_detail to a wh in the same physical
   -- wh as the alloc from-wh.
   -- A wh-sourced allocation is one with NULL order_no and NULL doc on alloc_header.
   cursor C_WH_SOURCE_ALLOC is
      select ah.alloc_no
        from alloc_header ah,
             wh w1
       where mod(ah.wh, I_num_threads)+1 = I_thread_no
         and ah.release_date >= L_vdate
         and ah.wh = w1.wh
         and ah.order_no is NULL
         and ah.doc is NULL
         and exists (select 1
                       from alloc_detail ad,
                            wh w2
                      where ad.alloc_no = ah.alloc_no
                        and ad.to_loc = w2.wh
                        and ad.processed_ind = 'N'
                        and w2.physical_wh = w1.physical_wh
                        and rownum = 1);

   -- Return all inbound receipt sourced allocations that are candidates for book transfer creation.
   -- Such allocations contain at least one unprocessed alloc_detail to a wh in the same physical
   -- wh as the alloc from-wh.
   -- An inbound receipt sourced sourced allocation is one with doc_type of PO, TSF, ASN, BOL and ALLOC.
   cursor C_INBOUND_RCPT_ALLOC is
      select ah.alloc_no
        from alloc_header ah,
             wh w1
       where mod(ah.wh, I_num_threads)+1 = I_thread_no
         and ah.release_date >= L_vdate
         and ah.wh = w1.wh
         and ah.doc_type in ('PO', 'TSF', 'ASN', 'BOL', 'ALLOC')
         and exists (select 1
                       from alloc_detail ad,
                            wh w2
                      where ad.alloc_no = ah.alloc_no
                        and ad.to_loc = w2.wh
                        and ad.processed_ind = 'N'
                        and w2.physical_wh = w1.physical_wh
                        and ad.qty_allocated > NVL(ad.qty_received, 0)
                        and rownum = 1);

BEGIN

   if I_thread_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_thread_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_num_threads is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_num_threads',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   -- Fetch WH Source Allocation Numbers
   SQL_LIB.SET_MARK('OPEN',
                    'C_WH_SOURCE_ALLOC',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH',
                    NULL);
   open C_WH_SOURCE_ALLOC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_WH_SOURCE_ALLOC',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH',
                    NULL);
   fetch C_WH_SOURCE_ALLOC BULK COLLECT into L_wh_src_alloc_no_tbl;

   SQL_LIB.SET_MARK('FETCH',
                    'C_WH_SOURCE_ALLOC',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH',
                    NULL);
   close C_WH_SOURCE_ALLOC;

   -- Fetch Inbound Receipt Allocation Numbers
   SQL_LIB.SET_MARK('OPEN',
                    'C_INBOUND_RCPT_ALLOC',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH',
                    NULL);
   open C_INBOUND_RCPT_ALLOC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_INBOUND_RCPT_ALLOC',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH',
                    NULL);
   fetch C_INBOUND_RCPT_ALLOC BULK COLLECT into L_inb_rcpt_alloc_no_tbl;

   SQL_LIB.SET_MARK('FETCH',
                    'C_INBOUND_RCPT_ALLOC',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH',
                    NULL);
   close C_INBOUND_RCPT_ALLOC;


   if L_wh_src_alloc_no_tbl.COUNT > 0 then
      if PROCESS_WH_SOURCE_ALLOC (O_error_message,
                                  L_wh_src_alloc_no_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_inb_rcpt_alloc_no_tbl.COUNT > 0 then
      if PROCESS_RCPT_SOURCED_ALLOC (O_error_message,
                                     L_inb_rcpt_alloc_no_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_BOOK_TSF;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_WH_SOURCE_ALLOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_alloc_no_tbl    IN       ALLOC_NO_TBL)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ALLOC_BOOK_TSF_SQL.PROCESS_WH_SOURCE_ALLOC';
   L_date               DATE         := GET_VDATE();
   L_table              VARCHAR2(30);

   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(RECORD_LOCKED, -54);

   -- Keeps a running total of outstanding available qty by item/wh
   L_prev_item          ITEM_MASTER.ITEM%TYPE;
   L_prev_wh            WH.WH%TYPE;

   -- Holds the outstanding overage/shortage qty for adjustment
   L_adjust_qty         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_over_short_ind     VARCHAR2(1);   --'O'verage, 'S'horage, 'N'one

   cursor C_LOCK_ALLOC_HEADER is
      select ah.alloc_no
        from alloc_header ah,
             TABLE(CAST(I_alloc_no_tbl AS "ALLOC_NO_TBL")) allocs
       where ah.alloc_no = value(allocs)
         for update of ah.alloc_no nowait;

   cursor C_LOCK_ALLOC_DETAIL is
      select ad.alloc_no
        from alloc_detail ad,
             TABLE(CAST(I_alloc_no_tbl AS "ALLOC_NO_TBL")) allocs
       where ad.alloc_no = value(allocs)
         for update of ad.alloc_no nowait;

   -- WH-sourced allocations: cursor C_BOOK_TSF_QTY
   -- Retrieve alloc_details with outstanding allocation quantity and with from/to-locs in the same physical wh.
   -- These are alloc_details we will be creating book transfer for. Retrieve sourcing wh's available inventory.
   cursor C_BOOK_TSF_QTY is
      with alloc as (select ah.alloc_no,
                            ah.wh,
                            ah.item,
                            ad.to_loc,
                            ad.to_loc_type,
                            ad.qty_allocated - NVL(ad.qty_transferred, 0) alloc_qty,
                            --Part of the wh ils.tsf_reserved_qty is the allocation qty for this allocation,
                            --including both book transfer lines and non-book transfer lines. Subtracting
                            --ils.tsf_reserved_qty from wh available here achieves the requirement of using
                            --wh inventory to service non-book transfer lines first. The reserved qty for the
                            --book transfer lines will be later added back to wh available to calculate the
                            --true wh available for book transfers.
                            --wh_avail_inventory calculated as such can be negative.
                            ils.stock_on_hand - (ils.tsf_reserved_qty + ils.rtv_qty + ils.non_sellable_qty + ils.customer_resv) wh_avail_inventory,
                            ils.tsf_reserved_qty wh_rsvd_inventory,
                            ad.rowid rid
                      from  alloc_header ah,
                            alloc_detail ad,
                            wh from_wh,
                            wh to_wh,
                            item_loc_soh ils,
                            TABLE(CAST(I_alloc_no_tbl AS "ALLOC_NO_TBL")) allocs
                      where ah.alloc_no = value(allocs)
                        and ah.alloc_no = ad.alloc_no
                        and ah.item = ils.item
                        and ah.wh = ils.loc
                        and ah.wh = from_wh.wh
                        and ad.to_loc = to_wh.wh
                        and from_wh.physical_wh = to_wh.physical_wh
                        and ad.qty_allocated - NVL(ad.qty_transferred, 0) > 0),
          -- Retrieve sum of allocation qty that will be sourced from the sourcing wh for all book transfer locations.
          bt_qty as (select distinct item,
                            wh,
                            -- This is the outstanding qty to fulfill across all allocations for the item sourced from the same wh
                            sum(alloc_qty) over (partition by item, wh) as total_bt_qty
                       from alloc),
    -- Calculate the true wh available inventory for creating book transfers
    bt_avail_qty as (select distinct a.item,
                            a.wh,
                            bt.total_bt_qty,
                            --Add back outstanding book tsf qty to the wh available calculation because it's part of the
                            --ils.tsf_reserved_qty subtracted from the wh available in alloc view.
                            --Do NOT add back more than the original ils.tsf_reserved_qty.
                            GREATEST(a.wh_avail_inventory + LEAST(bt.total_bt_qty, a.wh_rsvd_inventory), 0) available_qty
                       from alloc a,
                            bt_qty bt
                      where a.item = bt.item
                        and a.wh = bt.wh),
    round_bt_qty as (select a.alloc_no,
                            a.wh,
                            a.item,
                            a.to_loc,
                            a.to_loc_type,
                            bt.available_qty,
                            a.alloc_qty,
                            case
                               when bt.available_qty >= bt.total_bt_qty then
                                  --sufficient available
                                  a.alloc_qty
                               when bt.available_qty <= 0 then
                                  --no available, nothing to allocate
                                  0
                               when bt.total_bt_qty <= 0 then
                                  --no outstanding requested qty to fulfill, nothing to allocate
                                  0
                               else
                                  --insufficient available, prorate the qty,
                                  --round the prorated qty, need to handle the over/short allocation due to rounding outside of cursor
                                  ROUND(bt.available_qty * a.alloc_qty / bt.total_bt_qty)
                            end as book_tsf_qty,
                            --flag to indicate if adjustment is needed to handle the over/short allocation due to proration and rounding
                            case
                               when bt.available_qty >= bt.total_bt_qty or
                                    bt.available_qty <= 0 or
                                    bt.total_bt_qty <= 0 then
                                    -- no proration/rounding, therefore no adjustment is needed
                                    'N'
                               else
                                    -- quantity is prorated and rounding, will need adjustment
                                    'Y'
                            end adjustment_base,
                            a.rid
                       from alloc a,
                            bt_avail_qty bt
                      where a.item = bt.item
                        and a.wh = bt.wh)
   -- main select
      select alloc_no,
             wh,
             item,
             to_loc,
             to_loc_type,
             available_qty,
             alloc_qty,
             book_tsf_qty,
             sum(book_tsf_qty) over (partition by wh, item) as total_book_tsf_qty,
             adjustment_base,
             rid
        from round_bt_qty
    order by wh, item, alloc_qty;

   TYPE rec_type_bt is TABLE of C_BOOK_TSF_QTY%ROWTYPE;
   c_rec_bt_tab rec_type_bt;

BEGIN

   L_table := 'ALLOC_HEADER';

   -- Lock alloc header records
   open C_LOCK_ALLOC_HEADER;
   close C_LOCK_ALLOC_HEADER;

   L_table := 'ALLOC_DETAIL';

   -- Lock alloc detail records
   open C_LOCK_ALLOC_DETAIL;
   close C_LOCK_ALLOC_DETAIL;

   SQL_LIB.SET_MARK('OPEN',
                    'C_BOOK_TSF_QTY',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH, ITEM_LOC_SOH',
                    NULL);
   open C_BOOK_TSF_QTY;

   SQL_LIB.SET_MARK('FETCH',
                    'C_BOOK_TSF_QTY',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH, ITEM_LOC_SOH',
                    NULL);
   fetch C_BOOK_TSF_QTY BULK COLLECT into c_rec_bt_tab;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_BOOK_TSF_QTY',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH, ITEM_LOC_SOH',
                    NULL);
   close C_BOOK_TSF_QTY;

   FOR i IN 1..c_rec_bt_tab.LAST LOOP

      if i = 1 or
         L_prev_wh != c_rec_bt_tab(i).wh or
         L_prev_item != c_rec_bt_tab(i).item then

         --initialize or reset local variables when processing a different item/wh
         L_prev_wh := c_rec_bt_tab(i).wh;
         L_prev_item := c_rec_bt_tab(i).item;

         --first set to no overage/shortage to adjust
         L_over_short_ind := 'N';
         L_adjust_qty := 0;

         --determine the starting point of overage/shortage to adjust
         if c_rec_bt_tab(i).adjustment_base = 'Y' then
            if c_rec_bt_tab(i).available_qty > c_rec_bt_tab(i).total_book_tsf_qty then
               --short allocated
               L_over_short_ind := 'S';
               L_adjust_qty := c_rec_bt_tab(i).available_qty - c_rec_bt_tab(i).total_book_tsf_qty;
            elsif c_rec_bt_tab(i).available_qty < c_rec_bt_tab(i).total_book_tsf_qty then
               --over allocated
               L_over_short_ind := 'O';
               L_adjust_qty := c_rec_bt_tab(i).total_book_tsf_qty - c_rec_bt_tab(i).available_qty;
            end if;
         end if;

      end if;

      if L_over_short_ind = 'S' then
         -- short allocated, add the shortage one unit at a time to alloc_details
         if L_adjust_qty > 0 then
            c_rec_bt_tab(i).book_tsf_qty := c_rec_bt_tab(i).book_tsf_qty + 1;
            L_adjust_qty := L_adjust_qty - 1;
         end if;
      elsif L_over_short_ind = 'O' then
         -- over allocated, subtract overage one unit at a time to alloc_details
         if L_adjust_qty > 0 then
            c_rec_bt_tab(i).book_tsf_qty := c_rec_bt_tab(i).book_tsf_qty - 1;
            L_adjust_qty := L_adjust_qty - 1;
         end if;
      end if;

      if c_rec_bt_tab(i).book_tsf_qty > 0 then

         if TRANSFER_SQL.AUTOMATIC_BT_EXECUTE (O_error_message,
                                               c_rec_bt_tab(i).item,
                                               c_rec_bt_tab(i).wh,
                                               c_rec_bt_tab(i).to_loc,
                                               c_rec_bt_tab(i).book_tsf_qty) = FALSE then
            return FALSE;
         end if;

         --When the allocation is created, tsf reserved/expected buckets are updated on
         --ITEM_LOC_SOH. That needs to be backed out with the alloc_qty. The reason we
         --are backing out the alloc_qty instead of the book_tsf_qty is because wh-sourced
         --book transfer alloc_details are only processed once and any unfulfilled alloc_qty
         --is cancelled. The cancelled quantity will need to be backed out as well.
         if TRANSFER_SQL.UPD_ITEM_RESV_EXP(O_error_message,
                                           c_rec_bt_tab(i).item,                 --I_item
                                           'A',                                  --I_tsf_type, pass in 'A' for allocation. PL transfers are a special case
                                           c_rec_bt_tab(i).alloc_qty*(-1),       --I_tsf_qty, reverse the sign to backout the reserved/expected qty
                                           'W',                                  --I_from_loc_type
                                           c_rec_bt_tab(i).wh,                   --I_from_loc
                                           'W',                                  --I_to_loc_type
                                           c_rec_bt_tab(i).to_loc) = FALSE then  --I_to_loc
            return FALSE;
         end if;

      end if;

   END LOOP;

   -- Each alloc_detail in a WH-sourced allocation will only be processed once for book transfer when the
   -- release date is reached. Therefore, the unfulfilled qty is cancelled and processed_ind is set to 'Y'.
   if c_rec_bt_tab is NOT NULL and c_rec_bt_tab.COUNT > 0 then
      FORALL i IN c_rec_bt_tab.FIRST.. c_rec_bt_tab.LAST
         update alloc_detail
            set qty_transferred = NVL(qty_transferred, 0) + GREATEST(c_rec_bt_tab(i).book_tsf_qty, 0),
                qty_received    = NVL(qty_received, 0)    + GREATEST(c_rec_bt_tab(i).book_tsf_qty, 0),
                qty_cancelled   = NVL(qty_allocated, 0)   - (NVL(qty_transferred,0) + GREATEST((c_rec_bt_tab(i).book_tsf_qty), 0)),
                processed_ind   = 'Y'
          where alloc_no = c_rec_bt_tab(i).alloc_no
            and to_loc   = c_rec_bt_tab(i).to_loc
            and rowid    = c_rec_bt_tab(i).rid;
   end if;

   FORALL i IN I_alloc_no_tbl.FIRST.. I_alloc_no_tbl.LAST
     update alloc_header ah
        set ah.status = 'C',
            close_date = L_date
      where not exists (select 'x'
                          from alloc_detail ad
                         where ad.alloc_no = ah.alloc_no
                           and ad.qty_allocated - NVL(ad.qty_cancelled, 0) > NVL(ad.qty_received, 0)
                           and rownum = 1)
        and ah.alloc_no = I_alloc_no_tbl(i);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED',
                                             L_table,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_WH_SOURCE_ALLOC;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_RCPT_SOURCED_ALLOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_alloc_no_tbl    IN       ALLOC_NO_TBL)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ALLOC_BOOK_TSF_SQL.PROCESS_RCPT_SOURCED_ALLOC';
   L_table              VARCHAR2(30);

   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_rowid              ROWID;

   -- Manage shortage assignment due to rounding
   L_prev_item          ITEM_MASTER.ITEM%TYPE;
   L_prev_wh            WH.WH%TYPE;
   L_prev_alloc         ALLOC_HEADER.ALLOC_NO%TYPE;

   -- Holds the outstanding overage/shortage qty for adjustment
   L_adjust_qty         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_over_short_ind     VARCHAR2(1);   --'O'verage, 'S'horage, 'N'one

   cursor C_LOCK_ALLOC_HEADER is
      select ah.alloc_no
        from alloc_header ah,
             TABLE(CAST(I_alloc_no_tbl AS "ALLOC_NO_TBL")) allocs
       where ah.alloc_no = value(allocs)
         for update of ah.alloc_no nowait;

   cursor C_LOCK_ALLOC_DETAIL is
      select ad.alloc_no
        from alloc_detail ad,
             TABLE(CAST(I_alloc_no_tbl AS "ALLOC_NO_TBL")) allocs
       where ad.alloc_no = value(allocs)
         for update of ad.alloc_no nowait;

   cursor C_BOOK_TSF_QTY is
      -- Get a list of book transfer allocation details
      with alloc as (select ah.alloc_no,
                            ah.order_no,
                            ah.doc,
                            ah.doc_type,
                            ah.wh,
                            ah.item,
                            ad.to_loc,
                            ad.to_loc_type,
                            from_wh.physical_wh from_loc_phys_wh,
                            ad.qty_allocated,
                            NVL(ad.qty_transferred, 0),
                            ad.qty_allocated - NVL(ad.qty_transferred, 0) alloc_qty,  --this is the outstanding qty to fulfill
                            NVL(ad.qty_received,0) qty_received,
                            ils.stock_on_hand - (ils.tsf_reserved_qty + ils.rtv_qty + ils.non_sellable_qty + ils.customer_resv) wh_avail_inventory,
                            ils.tsf_reserved_qty wh_rsvd_inventory,
                            ad.rowid rid
                       from alloc_header ah,
                            alloc_detail ad,
                            wh from_wh,
                            wh to_wh,
                            item_loc_soh ils,
                            TABLE(CAST(I_alloc_no_tbl as "ALLOC_NO_TBL")) allocs
                      where ah.alloc_no = value(allocs)
                        and ah.alloc_no = ad.alloc_no
                        and ah.item = ils.item
                        and ah.wh = ils.loc
                        and ah.wh = from_wh.wh
                        and ad.to_loc = to_wh.wh
                        and from_wh.physical_wh = to_wh.physical_wh),
      --Get a list of non-book transfer allocation details sourced from the same doc and from the same physical wh as the book tsf allocation.
      --This may include allocations that are NOT in I_alloc_no_tbl. This is to support scenarios where multiple allocations are sourced from 
      --the same PO/Tsf/Alloc/ASN/BOL receipt, with some including only non-book transfer locations.
      non_bt_alloc as (select alloc.alloc_no alloc_no,     --this is the book transfer alloc_no
                              ah.order_no,
                              ah.doc,
                              ah.doc_type,
                              ah.wh,
                              ah.item,
                              ad.qty_allocated
                         from alloc_header ah,
                              alloc_detail ad,
                              wh from_wh,
                              wh to_wh,
                              alloc
                        where ah.alloc_no = ad.alloc_no
                          and ah.wh = from_wh.wh
                          and ad.to_loc = to_wh.wh(+)  --outer join to also retrieve to_loc of stores
                          and from_wh.physical_wh != NVL(to_wh.physical_wh, -1)
                          and ah.status != 'C'
                          -- allocations sourced from the same doc as the BT alloc
                          and (   alloc.doc_type = 'PO'    and ah.doc_type = 'PO' and alloc.order_no = ah.order_no
                               or alloc.doc_type = 'TSF'   and ah.doc_type = 'TSF' and alloc.order_no = ah.order_no
                               or alloc.doc_type = 'ALLOC' and ah.doc_type = 'ALLOC' and alloc.order_no = ah.order_no
                               or alloc.doc_type = 'ASN'   and ah.doc_type = 'ASN' and alloc.doc = ah.doc
                               or alloc.doc_type = 'BOL'   and ah.doc_type = 'BOL' and alloc.doc = ah.doc)
                          and alloc.item = ah.item
                          and alloc.wh = ah.wh),
      --Total received quantity of item at wh on all shipments linked to an allocation source
            rcpt as (select distinct alloc.alloc_no,
                                     alloc.item,
                                     alloc.wh,
                                     --Include alloc.to_loc in the partition by clause to prevent sk.qty_received qty from being double counted
                                     --when alloc contains multiple details for the same alloc_no/item/wh.
                                     sum(NVL(sk.qty_received, 0)) over (partition by alloc.alloc_no, alloc.item, alloc.wh, alloc.to_loc) as total_rcpt_qty
                                from alloc,
                                     shipment sh,
                                     shipsku sk
                               where sh.shipment = sk.shipment
                                and (   alloc.doc_type = 'PO'    and alloc.order_no is NOT NULL and alloc.order_no = sh.order_no
                                     or alloc.doc_type = 'TSF'   and alloc.order_no is NOT NULL and alloc.order_no = sk.distro_no
                                     or alloc.doc_type = 'ALLOC' and alloc.order_no is NOT NULL and alloc.order_no = sk.distro_no
                                     or alloc.doc_type = 'ASN'   and alloc.doc is NOT NULL and alloc.doc = sh.asn
                                     or alloc.doc_type = 'BOL'   and alloc.doc is NOT NULL and alloc.doc = sh.bol_no)
                                and alloc.from_loc_phys_wh = sh.to_loc  -- for inbound receipt, shipment to-loc would match with physical wh of alloc_header.wh
                                and sk.item = alloc.item),
      --Total qty required for non-book transfer locations on the allocation that are sourced from the same wh.
      --Inbound Receipt inventory is used to service non-book transfer locations first.
        non_bt_qty as (select distinct alloc_no,
                                       item,
                                       wh,
                                       -- This is the qty allocated across all non-book transfer alloc_details for an
                                       -- allocation (including qty already transferred), used for available rcpt qty calculation.
                                       sum(qty_allocated) over (partition by doc_type, doc, order_no, item, wh) as total_non_bt_qty
                                  from non_bt_alloc),
     --Total fulfilled qty and outstanding qty yet to be fulfilled for all book transfer locations on an allocation.
            bt_qty as (select distinct alloc_no,
                                       item,
                                       wh,
                                       -- This is the total fulfilled qty across all book transfer alloc_details
                                       -- for an allocation, used for available rcpt qty calculation.
                                       sum(NVL(qty_received, 0)) over (partition by doc_type, doc, order_no, item, wh) as total_bt_received_qty,
                                       -- This is the total outstanding qty to fulfill across all book transfer alloc_details
                                       -- for an allocation, used for available rcpt qty proration.
                                       sum(alloc_qty) over (partition by doc_type, doc, order_no, item, wh) as total_bt_qty,
                                       -- This is the total outstanding qty to fulfill across all book transfer alloc_details
                                       -- for an item/wh, used for wh availability comparison for all allocations in a run.
                                       sum(alloc_qty) over (partition by item, wh) as total_wh_bt_qty
                                  from alloc),
     --Determine the true available qty to be allocated to book tsf locations based on wh available and additional receipt qty.
     --Additional receipt qty = sum(qty received on all shipments linked to alloc source) - (sum(qty required for non-book transfer lines for the allocation) + sum(qty fulfilled on book transfer lines for the allocation)).
     --Additional wh available qty = wh available + outstanding book tsf qty
     --Receipt qty is applicable to allocations sourced from the same doc;
     --wh available qty is applicable to all allocations with the same item/source wh in a given run.
     --Use least of (additonal receipt qty, additional wh available qty) as the true available qty to allocate to outstanding book transfer locations
     --to avoild driving item/wh's SOH to negative.
           bt_avail as (select a.alloc_no,
                               a.wh,
                               a.item,
                               a.to_loc,
                               a.to_loc_type,
                               GREATEST(a.wh_avail_inventory, 0)  additional_wh_avail_qty,
                               GREATEST(r.total_rcpt_qty - (NVL(nbt.total_non_bt_qty, 0) + bt.total_bt_received_qty), 0) additional_rcpt_qty,
                               --This is the true available qty to allocate to bt locations constrainted by wh available.
                               LEAST(GREATEST(a.wh_avail_inventory, 0),
                                     GREATEST(r.total_rcpt_qty - (NVL(nbt.total_non_bt_qty, 0) + bt.total_bt_received_qty), 0)) available,
                               --This is the total outstanding qty to fulfill for book transfer locations for item/wh
                               bt.total_wh_bt_qty,
                               --This is the total outstanding qty to fulfill for book transfer locations for an allocation
                               bt.total_bt_qty,
                               --This is the outstanding qty to fulfill for an alloc_detail line
                               a.alloc_qty
                          from alloc a,
                               rcpt r,
                               non_bt_qty nbt,
                               bt_qty bt
                         where a.alloc_no = r.alloc_no
                           and a.item = r.item
                           and a.wh = r.wh
                           and a.alloc_no = nbt.alloc_no(+)  --there may not be any non-book transfer locations on the allocation
                           and a.item = nbt.item(+)
                           and a.wh = nbt.wh(+)
                           and a.alloc_no = bt.alloc_no  --there should be at least 1 book transfer location on the allocation
                           and a.item = bt.item
                           and a.wh = bt.wh),
       -- Prorate and round book transfer quantity based on available.
       -- If available is constrained by the available receipt quantity, prorate over an allocation;
       -- If available is constrained by the wh available quantity, prorate over all allocations for the same item/source wh.
       round_bt_qty as (select a.alloc_no,
                               a.wh,
                               a.item,
                               a.to_loc,
                               a.to_loc_type,
                               b.available,
                               b.alloc_qty,
                               case
                                  when b.available >= b.total_bt_qty then
                                     --sufficient available
                                     b.alloc_qty
                                  when b.available <= 0 then
                                     --no available, nothing to fulfill
                                     0
                                  when b.total_bt_qty <= 0 then
                                     --no outstanding requested qty to fulfill
                                     0
                                  when b.additional_rcpt_qty <= b.additional_wh_avail_qty then
                                     --insufficient available, prorate based on rcpt qty over an allocation
                                     --round proration result to integer, need to handle the over/short outside of cursor
                                     ROUND(b.additional_rcpt_qty * b.alloc_qty / b.total_bt_qty)
                                  else
                                     -- insufficient available, prorate based on wh available over all allocations for the same item/source wh
                                     --round proration result to integer, need to handle the over/short outside of cursor
                                     ROUND(b.additional_wh_avail_qty * b.alloc_qty / b.total_wh_bt_qty)
                               end book_tsf_qty,   --this is the calculated qty to create book tsf for an alloc_detail, will require adjustement for overage/shorage due to rounding
                               --flag to indicate how to adjust the calculated book_tsf_qty due to proration and rounding for overage/shortage
                               case
                                  when b.available >= b.total_bt_qty or
                                       b.available <= 0 or
                                       b.total_bt_qty <= 0 then
                                       -- no proration/rounding, therefore no adjustment is needed
                                       'N'
                                  when b.additional_rcpt_qty <= b.additional_wh_avail_qty then
                                       --book_tsf_qty is constrained by available Receipt qty
                                       'R'
                                  else
                                       --book_tsf_qty is constrained by WH available qty
                                       'W'
                               end adjustment_base,
                               a.rid
                          from alloc a,
                               bt_avail b
                         where a.alloc_no = b.alloc_no
                           and a.wh = b.wh
                           and a.item = b.item
                           and a.to_loc = b.to_loc
                           and a.to_loc_type = b.to_loc_type)
      -- main select clause
         select alloc_no,
                wh,
                item,
                to_loc,
                to_loc_type,
                available,
                alloc_qty,
                book_tsf_qty,
                sum(book_tsf_qty) over (partition by alloc_no, wh, item) as total_book_tsf_qty_by_rcpt,
                sum(book_tsf_qty) over (partition by wh, item) as total_book_tsf_qty_by_wh,
                adjustment_base,
                rid
           from round_bt_qty
          order by wh, item, alloc_no, alloc_qty;

   TYPE rec_type_bt is TABLE of C_BOOK_TSF_QTY%ROWTYPE;
   c_rec_bt_tab rec_type_bt;

BEGIN

   L_table := 'ALLOC_HEADER';

   -- Lock alloc header records
   open C_LOCK_ALLOC_HEADER;
   close C_LOCK_ALLOC_HEADER;

   L_table := 'ALLOC_DETAIL';

   -- Lock alloc detail records
   open C_LOCK_ALLOC_DETAIL;
   close C_LOCK_ALLOC_DETAIL;

   SQL_LIB.SET_MARK('OPEN',
                    'C_BOOK_TSF_QTY',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH, ITEM_LOC_SOH',
                    NULL);
   open C_BOOK_TSF_QTY;

   SQL_LIB.SET_MARK('FETCH',
                    'C_BOOK_TSF_QTY',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH, ITEM_LOC_SOH',
                    NULL);
   fetch C_BOOK_TSF_QTY BULK COLLECT into c_rec_bt_tab;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_BOOK_TSF_QTY',
                    'ALLOC_HEADER, ALLOC_DETAIL, WH, ITEM_LOC_SOH',
                    NULL);
   close C_BOOK_TSF_QTY;

   FOR i IN 1..c_rec_bt_tab.LAST LOOP

      --When the calculated book_tsf_qty is a result of proration and rounding, the total book_tsf_qty
      --may go over or under the actual available due to rounding. The following code assigns the overage/shortage
      --in a round-robin fashion (i.e. 1 unit at a time until the overage/shortage is used up) to alloc_details.
      if i = 1 or
         c_rec_bt_tab(i).adjustment_base = 'W' and (L_prev_wh != c_rec_bt_tab(i).wh or L_prev_item != c_rec_bt_tab(i).item) or
         c_rec_bt_tab(i).adjustment_base = 'R' and (L_prev_wh != c_rec_bt_tab(i).wh or L_prev_item != c_rec_bt_tab(i).item or L_prev_alloc != c_rec_bt_tab(i).alloc_no) or
         c_rec_bt_tab(i).adjustment_base = 'N' and (L_prev_wh != c_rec_bt_tab(i).wh or L_prev_item != c_rec_bt_tab(i).item or L_prev_alloc != c_rec_bt_tab(i).alloc_no) then

         --initialize/reset variables when item or wh or alloc_no is changed depending on the type of adjustment
         L_prev_wh := c_rec_bt_tab(i).wh;
         L_prev_item := c_rec_bt_tab(i).item;
         L_prev_alloc := c_rec_bt_tab(i).alloc_no;

         --first set to no overage/shortage to adjust
         L_over_short_ind := 'N';
         L_adjust_qty := 0;

         --determine the starting point of overage/shortage qty to adjust based on the adjustment type
         if c_rec_bt_tab(i).adjustment_base = 'R' then
            -- Receipt-based adjustment, compare to the total book tsf qty by receipt for the total overage/shortage
            if c_rec_bt_tab(i).available > c_rec_bt_tab(i).total_book_tsf_qty_by_rcpt then
               L_over_short_ind := 'S';
               L_adjust_qty := c_rec_bt_tab(i).available - c_rec_bt_tab(i).total_book_tsf_qty_by_rcpt;
            elsif c_rec_bt_tab(i).available < c_rec_bt_tab(i).total_book_tsf_qty_by_rcpt then
               L_over_short_ind := 'O';
               L_adjust_qty := c_rec_bt_tab(i).total_book_tsf_qty_by_rcpt - c_rec_bt_tab(i).available;
            end if;
         elsif c_rec_bt_tab(i).adjustment_base = 'W' then
            -- Wh available-based adjustment, compare to the total book tsf qty by wh for the total overage/shortage
            if c_rec_bt_tab(i).available > c_rec_bt_tab(i).total_book_tsf_qty_by_wh then
               L_over_short_ind := 'S';
               L_adjust_qty := c_rec_bt_tab(i).available - c_rec_bt_tab(i).total_book_tsf_qty_by_wh;
            elsif c_rec_bt_tab(i).available < c_rec_bt_tab(i).total_book_tsf_qty_by_wh then
               L_over_short_ind := 'O';
               L_adjust_qty := c_rec_bt_tab(i).total_book_tsf_qty_by_wh - c_rec_bt_tab(i).available;
            end if;
         end if;
      end if;

      if L_over_short_ind = 'S' then
         -- short allocated, add the shortage one unit at a time to alloc_details
         if L_adjust_qty > 0 then
            c_rec_bt_tab(i).book_tsf_qty := c_rec_bt_tab(i).book_tsf_qty + 1;
            L_adjust_qty := L_adjust_qty - 1;
         end if;
      elsif L_over_short_ind = 'O' then
         -- over allocated, subtract overage one unit at a time to alloc_details
         if L_adjust_qty > 0 then
            c_rec_bt_tab(i).book_tsf_qty := c_rec_bt_tab(i).book_tsf_qty - 1;
            L_adjust_qty := L_adjust_qty - 1;
         end if;
      end if;

      if c_rec_bt_tab(i).book_tsf_qty > 0 then

         if TRANSFER_SQL.AUTOMATIC_BT_EXECUTE (O_error_message,
                                               c_rec_bt_tab(i).item,
                                               c_rec_bt_tab(i).wh,
                                               c_rec_bt_tab(i).to_loc,
                                               c_rec_bt_tab(i).book_tsf_qty) = FALSE then
            return FALSE;
         end if;

         -- Insert unique alloc_no for doc_close_queue
         merge into doc_close_queue
         using dual 
            on (    doc = c_rec_bt_tab(i).alloc_no
                and doc_type = 'A')
          when not matched then
            insert (doc,
                    doc_type)
            values (c_rec_bt_tab(i).alloc_no,
                    'A');

         --Note: do NOT call TRANSFER_SQL.UPD_ITEM_RESV_EXP to update tsf reserved/expected buckets
         --for Receipt Sourced Allocations. This is because receipt sourced allocations are all 
         --considered 'cross-dock' allocations (with order_no populated) and tsf reserved/expected 
         --buckets are NOT updated when these allocations are created.

      end if;

   END LOOP;

   --Unlike wh-sourced allocations where each alloc_detail will only be processed once for book transfer
   --when the release date is reached, Inbound Receipt sourced allocations can be processed for book transfer
   --many times after the release date is reached to allow multiple receipts for the same allocation.
   --Therefore, the unfilfilled qty is NOT cancelled and processed_ind is NOT set to 'Y'. We expect
   --these allocations to be closed by docclose.pc.
   if c_rec_bt_tab is NOT NULL and c_rec_bt_tab.COUNT > 0 then
      FORALL i IN c_rec_bt_tab.FIRST.. c_rec_bt_tab.LAST
         update alloc_detail
            set qty_transferred = NVL(qty_transferred, 0) + GREATEST(c_rec_bt_tab(i).book_tsf_qty, 0),
                qty_received    = NVL(qty_received, 0)    + GREATEST(c_rec_bt_tab(i).book_tsf_qty, 0)
          where alloc_no = c_rec_bt_tab(i).alloc_no
            and to_loc   = c_rec_bt_tab(i).to_loc
            and rowid    = c_rec_bt_tab(i).rid;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED',
                                             L_table,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_RCPT_SOURCED_ALLOC;
-------------------------------------------------------------------------------
END ALLOC_BOOK_TSF_SQL;
/
