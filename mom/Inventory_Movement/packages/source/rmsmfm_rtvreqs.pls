CREATE OR REPLACE PACKAGE RMSMFM_RTVREQ AUTHID CURRENT_USER AS
/*--- message type parameters ---*/

FAMILY      VARCHAR2(64) := 'RTVREQ';

HDR_ADD     VARCHAR2(64) := 'RtvReqCre';
HDR_UPD     VARCHAR2(64) := 'RtvReqMod';
HDR_DEL     VARCHAR2(64) := 'RtvReqDel';
DTL_ADD     VARCHAR2(64) := 'RtvReqDtlCre';
DTL_UPD     VARCHAR2(64) := 'RtvReqDtlMod';
DTL_DEL     VARCHAR2(64) := 'RtvReqDtlDel';

---------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_msg     IN OUT  VARCHAR2,
                I_message_type  IN      VARCHAR2,
                I_rtv_order_no  IN      RTV_HEAD.RTV_ORDER_NO%TYPE,
                I_status        IN      RTV_HEAD.STATUS_IND%TYPE,
                I_ext_ref_no    IN      RTV_HEAD.EXT_REF_NO%TYPE,
                I_rtv_seq_no    IN      RTV_DETAIL.SEQ_NO%TYPE,
                I_item          IN      RTV_DETAIL.ITEM%TYPE,
                I_publish_ind   IN      RTV_DETAIL.PUBLISH_IND%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1);
---------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT);
---------------------------------------------------------------------------
-- compile private functions as public for unit testing
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE';
$if $$UTPLSQL=TRUE $then
   FUNCTION BUILD_HEADER_OBJECT(O_error_msg           IN OUT         VARCHAR2,
                                O_rib_rtvreqdesc_rec  IN OUT nocopy  "RIB_RTVReqDesc_REC",
                                O_rib_rtvreqref_rec   IN OUT nocopy  "RIB_RTVReqRef_REC",
                                O_routing_info        IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                                I_rtv_order_no        IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE)
   RETURN BOOLEAN;
$end
END RMSMFM_RTVREQ;
/
