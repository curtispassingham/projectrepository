CREATE or REPLACE PACKAGE BODY CORESVC_REPL_EXT_SQL AS

LP_vdate_plus_one        DATE;
LP_vdate                 DATE;
LP_curr_weekday          VARCHAR2(1);
LP_wh_cross_link_ind     SYSTEM_OPTIONS.wh_cross_link_ind%TYPE;
LP_domain_level          SYSTEM_OPTIONS.domain_level%TYPE;

cursor C_GET_GLOBAL is
   select p.vdate + 1,
          p.vdate,
          to_char(p.vdate,'D'), /* Current day of week */
          s.wh_cross_link_ind,
          s.domain_level
     from period p,
          system_options s;

---------------------------------------------------------------------------------------------
FUNCTION SETUP_DATA_GTT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_thread_id        IN       NUMBER,
                        I_stock_cat        IN       REPL_ITEM_LOC.STOCK_CAT%TYPE DEFAULT NULL,
                        I_loc_type         IN       REPL_ITEM_LOC.LOC_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION PACK_ROUNDING(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_thread_id        IN       NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_CURRENT_UNIT_RETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_thread_id       IN       NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_DOMAIN_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_EXT_SQL.GET_ITEM_DOMAIN_INFO';
   L_plength   NUMBER(2)    := LENGTH('CORESVC_REPL_EXT_SQL.GET_ITEM_DOMAIN_INFO');

BEGIN
   --merge_1, Update for Domain "D" system_level
   if LP_domain_level = 'D' then
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';
      merge into svc_repl_roq target
         using (select distinct wrr.i_item,
                       wrr.i_locn,
                       im.pack_ind,
                       im.item_level,
                       im.tran_level,
                       im.deposit_item_type,
                       im.container_item,
                       dom.domain_id,
                       wrr.thread_id
                  from svc_repl_roq wrr,
                       item_master im,
                       domain_dept dom
                 where im.item = wrr.i_item
                   and im.dept = dom.dept (+)) qry_tbl
         on (    target.i_item    = qry_tbl.i_item
             and target.i_locn    = qry_tbl.i_locn
             and target.thread_id = qry_tbl.thread_id)
         when matched then
            update set target.i_pack_ind          = qry_tbl.pack_ind,
                       target.i_item_level        = qry_tbl.item_level,
                       target.i_tran_level        = qry_tbl.tran_level,
                       target.i_deposit_item_type = qry_tbl.deposit_item_type,
                       target.i_container_item    = qry_tbl.container_item,
                       target.i_domain_id         = qry_tbl.domain_id;

   --merge_2, Update for Domain "C" system_level
   elsif LP_domain_level = 'C' then
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';
      merge into svc_repl_roq target
         using (select distinct wrr.i_item,
                       wrr.i_locn,
                       im.pack_ind,
                       im.item_level,
                       im.tran_level,
                       im.deposit_item_type,
                       im.container_item,
                       dom.domain_id,
                       wrr.thread_id
                  from svc_repl_roq wrr,
                       item_master im,
                       domain_class dom
                 where im.item = wrr.i_item
                   and im.dept = dom.dept (+)
                   and im.class = dom.class (+)) qry_tbl
         on (    target.i_item    = qry_tbl.i_item
             and target.i_locn    = qry_tbl.i_locn
             and target.thread_id = qry_tbl.thread_id)
         when matched then
            update set target.i_pack_ind          = qry_tbl.pack_ind,
                       target.i_item_level        = qry_tbl.item_level,
                       target.i_tran_level        = qry_tbl.tran_level,
                       target.i_deposit_item_type = qry_tbl.deposit_item_type,
                       target.i_container_item    = qry_tbl.container_item,
                       target.i_domain_id         = qry_tbl.domain_id;

   --merge_3, Update for Domain "S" system_level
   elsif LP_domain_level = 'S' then
      L_program := substr(L_program,1,L_plength)||'::MERGE_3';
      merge into svc_repl_roq target
         using (select distinct wrr.i_item,
                       wrr.i_locn,
                       im.pack_ind,
                       im.item_level,
                       im.tran_level,
                       im.deposit_item_type,
                       im.container_item,
                       dom.domain_id,
                       wrr.thread_id
                  from svc_repl_roq wrr,
                       item_master im,
                       domain_subclass dom
                 where im.item = wrr.i_item
                   and im.dept = dom.dept (+)
                   and im.class = dom.class (+)
                   and im.subclass = dom.subclass (+)) qry_tbl
         on (    target.i_item    = qry_tbl.i_item
             and target.i_locn    = qry_tbl.i_locn
             and target.thread_id = qry_tbl.thread_id)
         when matched then
            update set target.i_pack_ind          = qry_tbl.pack_ind,
                       target.i_item_level        = qry_tbl.item_level,
                       target.i_tran_level        = qry_tbl.tran_level,
                       target.i_deposit_item_type = qry_tbl.deposit_item_type,
                       target.i_container_item    = qry_tbl.container_item,
                       target.i_domain_id         = qry_tbl.domain_id;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEM_DOMAIN_INFO;
---------------------------------------------------------------------------------------------
-- Function Name:  CALL_REPLROQ
---------------------------------------------------------------------------------------------
FUNCTION CALL_REPLROQ(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_thread_id           IN       NUMBER,
                      I_last_run_of_day     IN       VARCHAR2 DEFAULT 'Y')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_REPL_EXT_SQL.CALL_REPLROQ';
   L_plength   NUMBER(2)    := LENGTH('CORESVC_REPL_EXT_SQL.CALL_REPLROQ');

BEGIN
   LP_vdate := GET_VDATE;
   --#A.3. setup gtt for W loc and W stock cat and repl method in D,T
   L_program := substr(L_program,1,L_plength)||'::A.3';
   if SETUP_DATA_GTT(O_error_message,
                     I_thread_id,     --I_thread_id
                     'W',             --I_stock_cat
                     'W'              --I_loc_type
                     ) = FALSE then
      return FALSE;
   end if;

   --#A.4
   L_program := substr(L_program,1,L_plength)||'::A.4';
   if NOT CORESVC_REPL_ROQ_SQL.CONSUME (O_error_message,
                                        I_thread_id,        --I_thread_id
                                        I_last_run_of_day,
                                        'RPLEXT'            --I_calling_program
                                        ) then
      return FALSE;
   end if;
   --
   update svc_repl_roq_gtt
      set o_order_qty = 0
    where NVL(i_due_ord_process_ind,'N') = 'N'
      and o_order_qty < 0
      and thread_id = I_thread_id;
   --
   if PACK_ROUNDING(O_error_message,
                    I_thread_id) = FALSE then
      return FALSE;
   end if;

   --#A.5 to do pl/sql. Merge back data from svc_repl_roq_gtt to svc_repl_roq
   L_program := substr(L_program,1,L_plength)||'::A.5';
   merge into svc_repl_roq target
      using (select i_item,
                    i_source_wh,
                    max(o_due_ind) due_ind, --will get Y if at least 1 store have Y due ind under its source wh
                    sum(o_order_qty) store_need,
                    sum(o_aso) aso,
                    sum(o_eso) eso
               from svc_repl_roq_gtt
              group by i_item,
                       i_source_wh) use_this
      on (    target.i_item = use_this.i_item
          and target.i_locn = use_this.i_source_wh
          and target.i_locn_type = 'W'
          and target.i_stock_cat = 'W'
          and target.i_repl_method in ('D','T')
          and target.thread_id = I_thread_id)
      when matched then
         update set target.i_due_ind    = use_this.due_ind,
                    target.i_store_need = use_this.store_need,
                    target.i_aso        = use_this.aso,
                    target.i_eso        = use_this.eso;

   update svc_repl_roq
      set i_store_need = 0,
          i_aso = 0,
          i_eso = 0,
          i_due_ind = 'N'
    where i_store_need is NULL
      and thread_id = I_thread_id;

   --#A.6
   L_program := substr(L_program,1,L_plength)||'::A.6';
   delete from svc_repl_roq_gtt
         where thread_id = I_thread_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CALL_REPLROQ;
---------------------------------------------------------------------------------------------
-- Function Name:  SETUP_DATA
---------------------------------------------------------------------------------------------
--#A.
FUNCTION SETUP_DATA(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_chunk_size        IN       RMS_PLSQL_BATCH_CONFIG.MAX_CHUNK_SIZE%TYPE,
                    I_last_run_of_day   IN       VARCHAR2 DEFAULT 'Y')
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_EXT_SQL.SETUP_DATA';
   L_plength   NUMBER(2)    := LENGTH('CORESVC_REPL_EXT_SQL.SETUP_DATA');

   --#A.1


   cursor C_GET_EXT_DATA is
      select ril.item                                         I_item,
             ril.location                                     I_locn,
             ril.loc_type                                     I_locn_type,
             ril.primary_repl_supplier                        I_primary_repl_supplier,
             ril.origin_country_id                            I_origin_country_id,
             d.buyer                                          I_buyer,
             ril.review_cycle                                 I_review_cycle,
             ril.stock_cat                                    I_stock_cat,
             ril.repl_order_ctrl                              I_repl_order_ctrl,
             ril.source_wh                                    I_source_wh,
             /* physical location */
             DECODE(ril.source_wh,
                    NULL, NVL(wah2.physical_wh,ril.location),
                    wah.physical_wh)                          I_physical_locn,
             /* physical warehouse */
             DECODE(ril.stock_cat,
                    'C', wah.physical_wh,
                    NVL(wah2.physical_wh,NULL))               I_source_physical_wh,
             ril.activate_date                                I_activate_date,
             ril.deactivate_date                              I_deactivate_date,
             ril.repl_method                                  I_repl_method,
             ril.pres_stock                                   I_pres_stock,
             ril.demo_stock                                   I_demo_stock,
             ril.min_stock                                    I_min_stock,
             ril.max_stock                                    I_max_stock,
             ril.incr_pct                                     I_incr_pct,
             ril.min_supply_days                              I_min_supply_days,
             ril.max_supply_days                              I_max_supply_days,
             ril.time_supply_horizon                          I_time_supply_horizon,
             ril.inv_selling_days                             I_inv_selling_days,
             ril.service_level                                I_service_level,
             ril.lost_sales_factor                            I_lost_sales_factor,
             ril.pickup_lead_time                             I_pickup_lead_time,
             ril.wh_lead_time                                 I_wh_lead_time,
             ril.terminal_stock_qty                           I_terminal_stock_qty,
             ril.season_id                                    I_season_id,
             ril.phase_id                                     I_phase_id,
             ril.dept                                         I_dept,
             ril.class                                        I_class,
             ril.subclass                                     I_subclass,
             DECODE(wah.break_pack_ind,
                    'Y', DECODE(ril.stock_cat,
                                'C', ril.store_ord_mult,
                                'C'),
                    'C')                                      I_store_ord_mult,
             ril.primary_pack_no                              I_repl_pack,
             ril.primary_pack_qty                             I_repl_pack_qty,
             ril.unit_cost                                    I_unit_cost,
             ril.round_to_inner_pct                           I_round_to_inner_pct,
             ril.round_to_pallet_pct                          I_round_to_pallet_pct,
             ril.round_to_case_pct                            I_round_to_case_pct,
             ril.supp_pack_size                               I_supp_pack_size,
             ril.inner_pack_size                              I_inner_pack_size,
             ril.ti                                           I_ti,
             ril.hi                                           I_hi,
             NVL(ril.supp_lead_time,0)                        I_supp_lead_time,
             DECODE(ril.loc_type,
                    'W', sp.inv_mgmt_lvl,
                    'S', DECODE(sp.inv_mgmt_lvl,
                                'A', 'D',
                                'L', 'S',
                                sp.inv_mgmt_lvl))             I_sup_dept_lvl_ord,
             ril.store_ord_mult                               I_drc_store_ord_mult,
             ril.non_scaling_ind                              I_non_scaling_ind,
             ril.max_scale_value                              I_max_scale_value,
             sp.delivery_policy                               I_sup_delivery_policy,
             wah.delivery_policy                              I_wh_delivery_policy,
             ril.last_delivery_date                           I_last_delivery_date,
             ril.tsf_po_link_no                               I_tsf_po_link_no,
             ril.mbr_order_qty                                I_mbr_order_qty,
             ril.reject_store_ord_ind                         I_reject_store_ord_ind,
             ril.service_level_type                           I_service_level_type,
             I_last_run_of_day                                I_last_run_of_the_day,
             ceil(ROWNUM/I_chunk_size)                        thread_id,
             ril.add_lead_time_ind                            I_add_lead_time_ind
        from repl_item_loc ril,
             repl_day rdy,
             sups sp,
             wh wah,
             wh wah2,
             store s,
             deps d
       where ril.item = rdy.item
         and ril.status = 'A'
         and ril.location = rdy.location
         and NVL(ril.dept,0) = NVL(d.dept,0)
         and ril.primary_repl_supplier = sp.supplier
         and sp.sup_status = 'A'
         and rdy.weekday = TO_NUMBER(LP_curr_weekday)
         and (LP_vdate != ril.last_review_date
              or ril.last_review_date is NULL
             )
         and ril.location = wah2.wh(+)
         and NVL(ril.source_wh,-1) = wah.wh(+)
             /*
              * If the location is a store, review for Direct and Crossdock
              * replenishment.  If it's a warehouse, review for Warehouse
              * stocked replenishment.
              */
         and (ril.loc_type = 'S' and ril.stock_cat IN ('D','C')
              or (ril.loc_type = 'W' and ril.stock_cat = 'W'))
             /*
              * If the review_cycle is 1, review every day (all seven days will
              * be filled in on repl_day).  If it's 0, the record can be
              * reviewed multiple times per week (depending on the weekday).
              * Otherwise, an item-location will be reviewed once every
              * <review_cycle> number of weeks.
              */
         and LP_vdate >=
                NVL(ril.last_review_date,TO_DATE('19010101','YYYYMMDD')) +
                (DECODE(TO_NUMBER(ril.review_cycle),
                        1, 0,
                        ril.review_cycle) * 7)
             /*
              * If the location is a store ensure stock will arrive at an open store
              */
         and s.store(+) = ril.location
         and (
               (
                  (
                    /* Start of the lead time calculation */
                        (NVL(ril.supp_lead_time,0) +
                         NVL(ril.pickup_lead_time,0) +
                         NVL(ril.wh_lead_time,0)) +
                    /* End of the lead time calculation */
                    LP_vdate <=
                    (s.store_close_date - NVL(s.stop_order_days, 0))
                    or s.store_close_date is NULL
                  )
                  and s.store_open_date - s.start_order_days <=
                  LP_vdate +
                    /* Start of the lead time calculation */
                        (NVL(ril.supp_lead_time,0) +
                         NVL(ril.pickup_lead_time,0) +
                         NVL(ril.wh_lead_time,0))
                    /* End of the lead time calculation */
               )
               or s.store is NULL
             )
             /*
              * Make sure the item is on active replenishment
              */
         and (ril.activate_date <= LP_vdate
         and NVL(ril.deactivate_date, (LP_vdate + 1))
                 > LP_vdate)
         and I_last_run_of_day ='Y'
      ----------------------------------------------------------------------
      --for WH crosslink
      ----------------------------------------------------------------------
      union all
      (
      select I_item,
             I_locn,
             'W' I_locn_type,
             I_primary_repl_supplier,
             I_origin_country_id,
             I_buyer,
             NULL I_review_cycle,
             'L'  I_stock_cat,
             I_repl_order_ctrl,
             I_source_wh,
             I_physical_locn,
             I_source_physical_wh,
             NULL I_activate_date,
             NULL I_deactivate_date,
             NULL I_repl_method,
             NULL I_pres_stock,
             NULL I_demo_stock,
             NULL I_min_stock,
             NULL I_max_stock,
             NULL I_incr_pct,
             NULL I_min_supply_days,
             NULL I_max_supply_days,
             NULL I_time_supply_horizon,
             NULL I_inv_selling_days,
             NULL I_service_level,
             NULL I_lost_sales_factor,
             I_pickup_lead_time,
             NULL I_wh_lead_time,
             NULL I_terminal_stock_qty,
             NULL I_season_id,
             NULL I_phase_id,
             I_dept,
             I_class,
             I_subclass,
             NULL I_store_ord_mult,
             I_repl_pack,
             I_repl_pack_qty,
             I_unit_cost,
             NULL I_round_to_inner_pct,
             NULL I_round_to_pallet_pct,
             NULL I_round_to_case_pct,
             I_supp_pack_size,
             I_inner_pack_size,
             I_ti,
             I_hi,
             I_supp_lead_time,
             I_sup_dept_lvl_ord,
             NULL I_drc_store_ord_mult,
             'Y'  I_non_scaling_ind,
             NULL I_max_scale_value,
             NULL I_sup_delivery_policy,
             NULL I_wh_delivery_policy,
             NULL I_last_delivery_date,
             I_tsf_po_link_no,
             I_mbr_order_qty,
             NULL I_reject_store_ord_ind,
             NULL I_service_level_type,
             NULL I_last_run_of_the_day,
             ceil(ROWNUM/I_chunk_size)               thread_id,
             I_add_lead_time_ind
        from (select ril.item                                       I_item,
                     ril.source_wh                                  I_locn,
                     ril.primary_repl_supplier                      I_primary_repl_supplier,
                     ril.origin_country_id                          I_origin_country_id,
                     d.buyer                                        I_buyer,
                     ril.repl_order_ctrl                            I_repl_order_ctrl,
                     ril.source_wh                                  I_source_wh,
                     wah.physical_wh                                I_physical_locn,
                     wah.physical_wh                                I_source_physical_wh,
                     MIN(ril.adj_pickup_lead_time)                  I_pickup_lead_time,
                     ril.dept                                       I_dept,
                     ril.class                                      I_class,
                     ril.subclass                                   I_subclass,
                     ril.primary_pack_no                            I_repl_pack,
                     ril.primary_pack_qty                           I_repl_pack_qty,
                     ril.unit_cost                                  I_unit_cost,
                     ril.supp_pack_size                             I_supp_pack_size,
                     ril.inner_pack_size                            I_inner_pack_size,
                     ril.ti                                         I_ti,
                     ril.hi                                         I_hi,
                     NVL(MIN(ril.adj_supp_lead_time),0)             I_supp_lead_time,
                     /* Needed to retrieve vendor line information */
                     sp.inv_mgmt_lvl                                I_sup_dept_lvl_ord,
                     ril.tsf_po_link_no                             I_tsf_po_link_no,
                     NVL(SUM(ril.mbr_order_qty),0)                  I_mbr_order_qty,
                     ril.add_lead_time_ind                          I_add_lead_time_ind
                from repl_item_loc ril,
                     wh wah,
                     sups sp,
                     rpl_net_inventory_tmp rnit,
                     deps d
               where LP_wh_cross_link_ind = 'Y'
                 and ril.status = 'A'
                 and ril.primary_repl_supplier = sp.supplier
                 and sp.sup_status = 'A'
                 and NVL(ril.dept,0) = NVL(d.dept,0)
                 and ril.loc_type = 'S'
                 and ril.stock_cat = 'L'
                 and ril.source_wh = wah.wh(+)
                 and I_last_run_of_day='N'
                 and ril.item=rnit.item
                 and ril.location=rnit.location
                 and ril.loc_type=rnit.loc_type
                 and ril.mult_runs_per_day_ind='Y'
                 and NVL(ril.mbr_order_qty,0) > 0
            group by ril.item,
                     ril.source_wh, /* location */
                     ril.primary_repl_supplier,
                     ril.origin_country_id,
                     d.buyer,
                     ril.repl_order_ctrl,
                     ril.source_wh,
                     wah.physical_wh,  /* physical location */
                     wah.physical_wh,
                     ril.dept,
                     ril.class,
                     ril.subclass,
                     ril.primary_pack_no,
                     ril.primary_pack_qty,
                     ril.unit_cost,
                     ril.supp_pack_size,
                     ril.inner_pack_size,
                     ril.ti,
                     ril.hi,
                     /* Needed to retrieve vendor line information */
                     sp.inv_mgmt_lvl,
                     ril.tsf_po_link_no,
                     ril.add_lead_time_ind
              union all
              select ril.item                                       I_item,
                     ril.source_wh                                  I_locn,
                     ril.primary_repl_supplier                      I_primary_repl_supplier,
                     ril.origin_country_id                          I_origin_country_id,
                     d.buyer                                        I_buyer,
                     ril.repl_order_ctrl                            I_repl_order_ctrl,
                     ril.source_wh                                  I_source_wh,
                     wah.physical_wh                                I_physical_locn,
                     wah.physical_wh                                I_source_physical_wh,
                     MIN(ril.adj_pickup_lead_time)                  I_pickup_lead_time,
                     ril.dept                                       I_dept,
                     ril.class                                      I_class,
                     ril.subclass                                   I_subclass,
                     ril.primary_pack_no                            I_repl_pack,
                     ril.primary_pack_qty                           I_repl_pack_qty,
                     ril.unit_cost                                  I_unit_cost,
                     ril.supp_pack_size                             I_supp_pack_size,
                     ril.inner_pack_size                            I_inner_pack_size,
                     ril.ti                                         I_ti,
                     ril.hi                                         I_hi,
                     NVL(MIN(ril.adj_supp_lead_time),0)             I_supp_lead_time,
                     /* Needed to retrieve vendor line information */
                     sp.inv_mgmt_lvl                                I_sup_dept_lvl_ord,
                     ril.tsf_po_link_no                             I_tsf_po_link_no,
                     NVL(SUM(ril.mbr_order_qty),0)                  I_mbr_order_qty,
                     ril.add_lead_time_ind                          I_add_lead_time_ind
                from repl_item_loc ril,
                     wh wah,
                     sups sp,
                     rpl_net_inventory_tmp rnit,
                     deps d
               where LP_wh_cross_link_ind = 'Y'
                 and ril.status = 'A'
                 and ril.primary_repl_supplier = sp.supplier
                 and sp.sup_status = 'A'
                 and NVL(ril.dept,0) = NVL(d.dept,0)
                 and ril.loc_type = 'S'
                 and ril.stock_cat = 'L'
                 and ril.source_wh = wah.wh(+)
                 and I_last_run_of_day = 'Y'
                     /* Return store records that have been reviewed today
                      * for WH/Cross Link order quantities in reqext.pc. */
                 and LP_vdate = ril.last_review_date
                 and ril.item = rnit.item(+)
                 and ril.location = rnit.location(+)
                 and ril.loc_type = rnit.loc_type(+)
                 and NVL(rnit.roq,0)>0
                 and NVL(ril.mbr_order_qty,0) > 0
            group by ril.item,
                     ril.source_wh, /* location */
                     ril.primary_repl_supplier,
                     ril.origin_country_id,
                     d.buyer,
                     ril.repl_order_ctrl,
                     ril.source_wh,
                     wah.physical_wh,  /* physical location */
                     wah.physical_wh,
                     ril.dept,
                     ril.class,
                     ril.subclass,
                     ril.primary_pack_no,
                     ril.primary_pack_qty,
                     ril.unit_cost,
                     ril.supp_pack_size,
                     ril.inner_pack_size,
                     ril.ti,
                     ril.hi,
                     /* Needed to retrieve vendor line information */
                     sp.inv_mgmt_lvl,
                     ril.tsf_po_link_no,
                     ril.add_lead_time_ind))
    -- order by item, primary_repl_supplier, origin_country_id, source_wh, repl_order_ctrl
    order by 1,
             4,
             5,
             10,
             9;

   TYPE REPL_EXT_TYPE is TABLE of C_GET_EXT_DATA%ROWTYPE INDEX BY BINARY_INTEGER;
   L_repl_ext_tbl   REPL_EXT_TYPE;
   L_limit          NUMBER(10):= 100000;
BEGIN
   --#A.2
   L_program := substr(L_program,1,L_plength)||'::A.2';
   L_repl_ext_tbl.DELETE;

   open C_GET_GLOBAL;
   fetch C_GET_GLOBAL into LP_vdate_plus_one
                          ,LP_vdate
                          ,LP_curr_weekday
                          ,LP_wh_cross_link_ind
                          ,LP_domain_level;
   close C_GET_GLOBAL;

   open C_GET_EXT_DATA;
   LOOP
      fetch C_GET_EXT_DATA BULK COLLECT into L_repl_ext_tbl LIMIT L_limit;
      FORALL i in L_repl_ext_tbl.FIRST .. L_repl_ext_tbl.LAST
         insert into svc_repl_roq (I_item,
                                   I_locn,
                                   I_locn_type,
                                   I_primary_repl_supplier,
                                   I_origin_country_id,
                                   I_buyer,
                                   I_review_cycle,
                                   I_stock_cat,
                                   I_repl_order_ctrl,
                                   I_source_wh,
                                   I_physical_locn,
                                   I_source_physical_wh,
                                   I_activate_date,
                                   I_deactivate_date,
                                   I_repl_method,
                                   I_pres_stock,
                                   I_demo_stock,
                                   I_min_stock,
                                   I_max_stock,
                                   I_incr_pct,
                                   I_min_supply_days,
                                   I_max_supply_days,
                                   I_time_supply_horizon,
                                   I_inv_selling_days,
                                   I_service_level,
                                   I_lost_sales_factor,
                                   I_pickup_lead_time,
                                   I_wh_lead_time,
                                   I_terminal_stock_qty,
                                   I_season_id,
                                   I_phase_id,
                                   I_dept,
                                   I_class,
                                   I_subclass,
                                   I_store_ord_mult,
                                   I_repl_pack,
                                   I_repl_pack_qty,
                                   I_unit_cost,
                                   I_round_to_inner_pct,
                                   I_round_to_pallet_pct,
                                   I_round_to_case_pct,
                                   I_supp_pack_size,
                                   I_inner_pack_size,
                                   I_ti,
                                   I_hi,
                                   I_supp_lead_time,
                                   I_sup_dept_lvl_ord,
                                   I_drc_store_ord_mult,
                                   I_non_scaling_ind,
                                   I_max_scale_value,
                                   I_sup_delivery_policy,
                                   I_wh_delivery_policy,
                                   I_last_delivery_date,
                                   I_tsf_po_link_no,
                                   I_mbr_order_qty,
                                   I_reject_store_ord_ind,
                                   I_due_ind,
                                   I_aso,
                                   I_eso,
                                   I_date,
                                   I_service_level_type,
                                   I_last_run_of_the_day,
                                   thread_id,
                                   I_add_lead_time_ind)
                           values (L_repl_ext_tbl(i).I_item,
                                   L_repl_ext_tbl(i).I_locn,
                                   L_repl_ext_tbl(i).I_locn_type,
                                   L_repl_ext_tbl(i).I_primary_repl_supplier,
                                   L_repl_ext_tbl(i).I_origin_country_id,
                                   L_repl_ext_tbl(i).I_buyer,
                                   L_repl_ext_tbl(i).I_review_cycle,
                                   L_repl_ext_tbl(i).I_stock_cat,
                                   L_repl_ext_tbl(i).I_repl_order_ctrl,
                                   L_repl_ext_tbl(i).I_source_wh,
                                   L_repl_ext_tbl(i).I_physical_locn,
                                   L_repl_ext_tbl(i).I_source_physical_wh,
                                   L_repl_ext_tbl(i).I_activate_date,
                                   L_repl_ext_tbl(i).I_deactivate_date,
                                   L_repl_ext_tbl(i).I_repl_method,
                                   L_repl_ext_tbl(i).I_pres_stock,
                                   L_repl_ext_tbl(i).I_demo_stock,
                                   L_repl_ext_tbl(i).I_min_stock,
                                   L_repl_ext_tbl(i).I_max_stock,
                                   L_repl_ext_tbl(i).I_incr_pct,
                                   L_repl_ext_tbl(i).I_min_supply_days,
                                   L_repl_ext_tbl(i).I_max_supply_days,
                                   L_repl_ext_tbl(i).I_time_supply_horizon,
                                   L_repl_ext_tbl(i).I_inv_selling_days,
                                   L_repl_ext_tbl(i).I_service_level,
                                   L_repl_ext_tbl(i).I_lost_sales_factor,
                                   L_repl_ext_tbl(i).I_pickup_lead_time,
                                   L_repl_ext_tbl(i).I_wh_lead_time,
                                   L_repl_ext_tbl(i).I_terminal_stock_qty,
                                   L_repl_ext_tbl(i).I_season_id,
                                   L_repl_ext_tbl(i).I_phase_id,
                                   L_repl_ext_tbl(i).I_dept,
                                   L_repl_ext_tbl(i).I_class,
                                   L_repl_ext_tbl(i).I_subclass,
                                   L_repl_ext_tbl(i).I_store_ord_mult,
                                   L_repl_ext_tbl(i).I_repl_pack,
                                   L_repl_ext_tbl(i).I_repl_pack_qty,
                                   L_repl_ext_tbl(i).I_unit_cost,
                                   L_repl_ext_tbl(i).I_round_to_inner_pct,
                                   L_repl_ext_tbl(i).I_round_to_pallet_pct,
                                   L_repl_ext_tbl(i).I_round_to_case_pct,
                                   L_repl_ext_tbl(i).I_supp_pack_size,
                                   L_repl_ext_tbl(i).I_inner_pack_size,
                                   L_repl_ext_tbl(i).I_ti,
                                   L_repl_ext_tbl(i).I_hi,
                                   L_repl_ext_tbl(i).I_supp_lead_time,
                                   L_repl_ext_tbl(i).I_sup_dept_lvl_ord,
                                   L_repl_ext_tbl(i).I_drc_store_ord_mult,
                                   L_repl_ext_tbl(i).I_non_scaling_ind,
                                   L_repl_ext_tbl(i).I_max_scale_value,
                                   L_repl_ext_tbl(i).I_sup_delivery_policy,
                                   L_repl_ext_tbl(i).I_wh_delivery_policy,
                                   L_repl_ext_tbl(i).I_last_delivery_date,
                                   L_repl_ext_tbl(i).I_tsf_po_link_no,
                                   L_repl_ext_tbl(i).I_mbr_order_qty,
                                   L_repl_ext_tbl(i).I_reject_store_ord_ind,
                                   'N',  -- I_due_ind
                                   0,    -- I_aso
                                   0,    -- I_eso
                                   LP_vdate_plus_one,
                                   L_repl_ext_tbl(i).I_service_level_type,
                                   L_repl_ext_tbl(i).I_last_run_of_the_day,
                                   L_repl_ext_tbl(i).thread_id,
                                   L_repl_ext_tbl(i).I_add_lead_time_ind);
          exit when C_GET_EXT_DATA%NOTFOUND;

   END LOOP;
   close C_GET_EXT_DATA;

   L_repl_ext_tbl.DELETE;
   if GET_ITEM_DOMAIN_INFO (O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SETUP_DATA;
---------------------------------------------------------------------------------------------
FUNCTION GET_VENDOR_LINE_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_thread_id       IN       NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_EXT_SQL.GET_VENDOR_LINE_INFO';
   L_plength   NUMBER(2)    := LENGTH('CORESVC_REPL_EXT_SQL.GET_VENDOR_LINE_INFO');

BEGIN

   --merge_1
   L_program := substr(L_program,1,L_plength)||'::MERGE_1';

   merge into svc_repl_roq_gtt target
      using (select wrr.i_item,
                    wrr.i_locn,
                    wrr.i_physical_locn,
                    sim.due_ord_process_ind,
                    sim.due_ord_serv_basis,
                    sim.pool_supplier,
                    wrr.thread_id
               from svc_repl_roq_gtt wrr,
                    sup_inv_mgmt sim
              where wrr.i_primary_repl_supplier = sim.supplier
                and wrr.i_sup_dept_lvl_ord = 'S'
                and sim.dept is NULL
                and sim.location is NULL
              union all
             select wrr.i_item,
                    wrr.i_locn,
                    wrr.i_physical_locn,
                    sim.due_ord_process_ind,
                    sim.due_ord_serv_basis,
                    sim.pool_supplier,
                    wrr.thread_id
               from svc_repl_roq_gtt wrr,
                    sup_inv_mgmt sim
              where wrr.i_primary_repl_supplier = sim.supplier
                and wrr.i_sup_dept_lvl_ord = 'L'
                and sim.dept is NULL
                and wrr.i_physical_locn = sim.location
              union all
             select wrr.i_item,
                    wrr.i_locn,
                    wrr.i_physical_locn,
                    sim.due_ord_process_ind,
                    sim.due_ord_serv_basis,
                    sim.pool_supplier,
                    wrr.thread_id
               from svc_repl_roq_gtt wrr,
                    sup_inv_mgmt sim
              where wrr.i_primary_repl_supplier = sim.supplier
                and wrr.i_sup_dept_lvl_ord = 'D'
                and wrr.i_dept = sim.dept
                and sim.location is NULL
              union all
             select wrr.i_item,
                    wrr.i_locn,
                    wrr.i_physical_locn,
                    sim.due_ord_process_ind,
                    sim.due_ord_serv_basis,
                    sim.pool_supplier,
                    wrr.thread_id
               from svc_repl_roq_gtt wrr,
                    sup_inv_mgmt sim
              where wrr.i_primary_repl_supplier = sim.supplier
                and wrr.i_sup_dept_lvl_ord = 'A'
                and wrr.i_dept = sim.dept
                and wrr.i_physical_locn = sim.location
                ) qry_tbl
      on (    target.i_item            = qry_tbl.i_item
          and target.i_locn            = qry_tbl.i_locn
          and target.i_physical_locn   = qry_tbl.i_physical_locn
          and target.thread_id         = qry_tbl.thread_id)
      when matched then
         update set target.i_due_ord_process_ind   = NVL(qry_tbl.due_ord_process_ind,'N'),
                    target.i_due_ord_serv_basis    = NVL(qry_tbl.due_ord_serv_basis,'U'),
                    target.i_pool_supplier         = NVL(qry_tbl.pool_supplier, -1);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_VENDOR_LINE_INFO;
---------------------------------------------------------------------------------------------
FUNCTION GET_CURRENT_UNIT_RETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_thread_id       IN       NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_EXT_SQL.GET_CURRENT_UNIT_RETAIL';
   L_plength   NUMBER(2)    := LENGTH('CORESVC_REPL_EXT_SQL.GET_CURRENT_UNIT_RETAIL');
   L_limit     NUMBER(6)    := 100000;
   --
   L_tax_add_tbl OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();
   --
   cursor C_TAX_RETAIL is
      select OBJ_TAX_RETAIL_ADD_REMOVE_REC(i_item,          --I_ITEM
                                           i_dept,          --I_DEPT
                                           i_class,         --I_CLASS
                                           i_locn,          --I_LOCATION
                                           i_locn_type,     --I_LOC_TYPE
                                           LP_vdate,        --I_EFFECTIVE_from_DATE
                                           i_unit_retail,   --I_AMOUNT
                                           1,               --I_QTY
                                           NULL)            --O_AMOUNT
        from svc_repl_roq_gtt
       where thread_id = I_thread_id;

BEGIN

   --merge_1
   L_program := substr(L_program,1,L_plength)||'::MERGE_1';
   --
   merge into svc_repl_roq_gtt target
      using (select wrr.i_item,
                    wrr.i_locn,
                    wrr.i_locn_type,
                    ph.unit_retail,
                    ROW_NUMBER() OVER (PARTITION BY ph.item,
                                                    ph.loc,
                                                    ph.loc_type
                                           ORDER BY ph.action_date DESC NULLS LAST) rnum,
                    wrr.thread_id
               from svc_repl_roq_gtt wrr,
                    price_hist ph
              where wrr.i_item = ph.item
                and wrr.i_locn = ph.loc
                and wrr.i_locn_type = ph.loc_type
                and wrr.i_due_ord_serv_basis = 'P'
                and ph.action_date < (LP_vdate + 1)
                and ph.tran_type in (0, 4, 8, 9, 11)
                and wrr.thread_id = I_thread_id
           order by ph.item,
                    ph.loc,
                    ph.loc_type,
                    rnum) qry_tbl
      on (    target.i_item               = qry_tbl.i_item
          and target.i_locn               = qry_tbl.i_locn
          and target.i_locn_type          = qry_tbl.i_locn_type
          and target.thread_id            = qry_tbl.thread_id
          and target.i_due_ord_serv_basis = 'P'
          and qry_tbl.rnum                = 1)
      when matched then
         update set target.i_unit_retail   = qry_tbl.unit_retail;


   --merge_2. Get base unit_retail for those records not processed by merge_1 above and i_unit_retail is NULL
   L_program := substr(L_program,1,L_plength)||'::MERGE_2';
   --
   merge into svc_repl_roq_gtt target
      using (select wrr.i_item,
                    wrr.i_locn,
                    wrr.i_locn_type,
                    ph.unit_retail,
                    wrr.thread_id
               from svc_repl_roq_gtt wrr,
                    price_hist ph
              where wrr.i_item = ph.item
                and wrr.i_locn = ph.loc
                and wrr.i_locn_type = ph.loc_type
                and wrr.i_due_ord_serv_basis = 'P'
                and wrr.i_unit_retail is NULL
                and ph.tran_type = 0
                and wrr.thread_id = I_thread_id) qry_tbl
      on (    target.i_item               = qry_tbl.i_item
          and target.i_locn               = qry_tbl.i_locn
          and target.i_locn_type          = qry_tbl.i_locn_type
          and target.i_due_ord_serv_basis = 'P'
          and target.thread_id            = qry_tbl.thread_id)
      when matched then
         update set target.i_unit_retail = qry_tbl.unit_retail;
   --

   L_program := substr(L_program,1,L_plength);

   --compute tax inclusive
   open C_TAX_RETAIL;
   LOOP
      L_tax_add_tbl.delete;
      fetch C_TAX_RETAIL BULK COLLECT into L_tax_add_tbl LIMIT L_limit;
         if L_tax_add_tbl is NOT NULL and L_tax_add_tbl.count > 0 then
            --
            if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                                L_tax_add_tbl)= FALSE then
               return FALSE;
            end if;
            --merge_3.
            L_program := substr(L_program,1,L_plength)||'::MERGE_3';
            --
            merge into svc_repl_roq_gtt target
               using (select wrr.i_item,
                             wrr.i_locn,
                             wrr.i_locn_type,
                             wrr.i_dept,
                             wrr.i_class,
                             tbl.o_amount unit_retail,
                             wrr.thread_id
                        from svc_repl_roq_gtt wrr,
                             TABLE(CAST(L_tax_add_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) tbl
                       where wrr.i_item      = tbl.i_item
                         and wrr.i_locn      = tbl.i_location
                         and wrr.i_locn_type = tbl.i_loc_type
                         and wrr.i_dept      = tbl.i_dept
                         and wrr.i_class     = tbl.i_class
                         and wrr.i_due_ord_serv_basis = 'P'
                         and wrr.thread_id   = I_thread_id) qry_tbl
               on (    target.i_item        = qry_tbl.i_item
                   and target.i_locn        = qry_tbl.i_locn
                   and target.i_locn_type   = qry_tbl.i_locn_type
                   and target.i_dept        = qry_tbl.i_dept
                   and target.i_class       = qry_tbl.i_class
                   and target.i_due_ord_serv_basis = 'P'
                   and target.thread_id     = qry_tbl.thread_id)
               when matched then
                  update set target.i_unit_retail = qry_tbl.unit_retail;
         end if;
      exit when C_TAX_RETAIL%NOTFOUND;
   END LOOP;
   close C_TAX_RETAIL;
   --
   L_tax_add_tbl.delete;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CURRENT_UNIT_RETAIL;
---------------------------------------------------------------------------------------------
--#B.1
FUNCTION SETUP_DATA_GTT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_thread_id        IN       NUMBER,
                        I_stock_cat        IN       REPL_ITEM_LOC.STOCK_CAT%TYPE DEFAULT NULL,
                        I_loc_type         IN       REPL_ITEM_LOC.LOC_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_EXT_SQL.SETUP_DATA_GTT';

BEGIN

   if I_stock_cat is NULL and
      I_loc_type is NULL then -- all stock category except cross-link 'L'
      --#B.1.1
      insert into svc_repl_roq_gtt (I_item,
                                    I_locn,
                                    I_locn_type,
                                    I_primary_repl_supplier,
                                    I_origin_country_id,
                                    I_buyer,
                                    I_review_cycle,
                                    I_stock_cat,
                                    I_repl_order_ctrl,
                                    I_source_wh,
                                    I_physical_locn,
                                    I_source_physical_wh,
                                    I_activate_date,
                                    I_deactivate_date,
                                    I_repl_method,
                                    I_pres_stock,
                                    I_demo_stock,
                                    I_min_stock,
                                    I_max_stock,
                                    I_incr_pct,
                                    I_min_supply_days,
                                    I_max_supply_days,
                                    I_time_supply_horizon,
                                    I_inv_selling_days,
                                    I_service_level,
                                    I_lost_sales_factor,
                                    I_pickup_lead_time,
                                    I_wh_lead_time,
                                    I_terminal_stock_qty,
                                    I_season_id,
                                    I_phase_id,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    I_domain_id,
                                    I_store_ord_mult,
                                    I_repl_pack,
                                    I_repl_pack_qty,
                                    I_unit_cost,
                                    I_unit_retail,
                                    I_round_to_inner_pct,
                                    I_round_to_pallet_pct,
                                    I_round_to_case_pct,
                                    I_supp_pack_size,
                                    I_inner_pack_size,
                                    I_ti,
                                    I_hi,
                                    I_supp_lead_time,
                                    I_sup_dept_lvl_ord,
                                    I_drc_store_ord_mult,
                                    I_non_scaling_ind,
                                    I_max_scale_value,
                                    I_sup_delivery_policy,
                                    I_wh_delivery_policy,
                                    I_last_delivery_date,
                                    I_tsf_po_link_no,
                                    I_reject_store_ord_ind,
                                    I_due_ord_process_ind,
                                    I_due_ord_serv_basis,
                                    I_pool_supplier,
                                    I_deposit_item_type,
                                    I_container_item,
                                    I_store_need,
                                    I_due_ind,
                                    I_aso,
                                    I_eso,
                                    I_date,
                                    I_sub_forecast_ind,
                                    I_sub_stock_ind,
                                    I_service_level_type,
                                    I_last_run_of_the_day,
                                    I_ord_temp_seq_no,
                                    thread_id,
                                    I_add_lead_time_ind,
                                    I_pack_ind)
                             select srr.I_item,
                                    srr.I_locn,
                                    srr.I_locn_type,
                                    srr.I_primary_repl_supplier,
                                    srr.I_origin_country_id,
                                    srr.I_buyer,
                                    srr.I_review_cycle,
                                    srr.I_stock_cat,
                                    srr.I_repl_order_ctrl,
                                    srr.I_source_wh,
                                    srr.I_physical_locn,
                                    srr.I_source_physical_wh,
                                    srr.I_activate_date,
                                    srr.I_deactivate_date,
                                    srr.I_repl_method,
                                    srr.I_pres_stock,
                                    srr.I_demo_stock,
                                    srr.I_min_stock,
                                    srr.I_max_stock,
                                    srr.I_incr_pct,
                                    srr.I_min_supply_days,
                                    srr.I_max_supply_days,
                                    srr.I_time_supply_horizon,
                                    srr.I_inv_selling_days,
                                    srr.I_service_level,
                                    srr.I_lost_sales_factor,
                                    srr.I_pickup_lead_time,
                                    srr.I_wh_lead_time,
                                    srr.I_terminal_stock_qty,
                                    srr.I_season_id,
                                    srr.I_phase_id,
                                    srr.I_dept,
                                    srr.I_class,
                                    srr.I_subclass,
                                    srr.I_domain_id,
                                    srr.I_store_ord_mult,
                                    srr.I_repl_pack,
                                    srr.I_repl_pack_qty,
                                    srr.I_unit_cost,
                                    NULL,                     -- I_unit_retail
                                    srr.I_round_to_inner_pct,
                                    srr.I_round_to_pallet_pct,
                                    srr.I_round_to_case_pct,
                                    srr.I_supp_pack_size,
                                    srr.I_inner_pack_size,
                                    srr.I_ti,
                                    srr.I_hi,
                                    srr.I_supp_lead_time,
                                    srr.I_sup_dept_lvl_ord,
                                    srr.I_drc_store_ord_mult,
                                    srr.I_non_scaling_ind,
                                    srr.I_max_scale_value,
                                    srr.I_sup_delivery_policy,
                                    srr.I_wh_delivery_policy,
                                    srr.I_last_delivery_date,
                                    srr.I_tsf_po_link_no,
                                    srr.I_reject_store_ord_ind,
                                    NULL,                     -- I_due_ord_process_ind
                                    NULL,                     -- I_due_ord_serv_basis
                                    NULL,                     -- I_pool_supplier
                                    srr.I_deposit_item_type,
                                    srr.I_container_item,
                                    NVL(srr.I_store_need,0),
                                    srr.I_due_ind,
                                    srr.I_aso,
                                    srr.I_eso,
                                    srr.I_date,
                                    'Y',
                                    'Y',
                                    srr.I_service_level_type,
                                    srr.I_last_run_of_the_day,
                                    ord_temp_sequence.NEXTVAL,
                                    srr.thread_id,
                                    srr.I_add_lead_time_ind,
                                    srr.I_pack_ind
                               from svc_repl_roq srr
                              where srr.thread_id = I_thread_id
                                and srr.I_stock_cat != 'L';

   elsif I_stock_cat = 'L' then
      insert into svc_repl_roq_gtt (I_item,
                                    I_locn,
                                    I_locn_type,
                                    I_primary_repl_supplier,
                                    I_origin_country_id,
                                    I_buyer,
                                    I_review_cycle,
                                    I_stock_cat,
                                    I_repl_order_ctrl,
                                    I_source_wh,
                                    I_physical_locn,
                                    I_source_physical_wh,
                                    I_activate_date,
                                    I_deactivate_date,
                                    I_repl_method,
                                    I_pres_stock,
                                    I_demo_stock,
                                    I_min_stock,
                                    I_max_stock,
                                    I_incr_pct,
                                    I_min_supply_days,
                                    I_max_supply_days,
                                    I_time_supply_horizon,
                                    I_inv_selling_days,
                                    I_service_level,
                                    I_lost_sales_factor,
                                    I_pickup_lead_time,
                                    I_wh_lead_time,
                                    I_terminal_stock_qty,
                                    I_season_id,
                                    I_phase_id,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    I_domain_id,
                                    I_store_ord_mult,
                                    I_repl_pack,
                                    I_repl_pack_qty,
                                    I_unit_cost,
                                    I_unit_retail,
                                    I_round_to_inner_pct,
                                    I_round_to_pallet_pct,
                                    I_round_to_case_pct,
                                    I_supp_pack_size,
                                    I_inner_pack_size,
                                    I_ti,
                                    I_hi,
                                    I_supp_lead_time,
                                    I_sup_dept_lvl_ord,
                                    I_drc_store_ord_mult,
                                    I_non_scaling_ind,
                                    I_max_scale_value,
                                    I_sup_delivery_policy,
                                    I_wh_delivery_policy,
                                    I_last_delivery_date,
                                    I_tsf_po_link_no,
                                    I_reject_store_ord_ind,
                                    I_due_ord_process_ind,
                                    I_due_ord_serv_basis,
                                    I_pool_supplier,
                                    I_deposit_item_type,
                                    I_container_item,
                                    I_store_need,
                                    O_due_ind,
                                    O_order_qty,  --values from I_mbr_order_qty
                                    I_sub_forecast_ind,
                                    I_sub_stock_ind,
                                    I_service_level_type,
                                    I_last_run_of_the_day,
                                    I_ord_temp_seq_no,
                                    thread_id,
                                    I_add_lead_time_ind,
                                    I_pack_ind)
                             select srr.I_item,
                                    srr.I_locn,
                                    srr.I_locn_type,
                                    srr.I_primary_repl_supplier,
                                    srr.I_origin_country_id,
                                    srr.I_buyer,
                                    srr.I_review_cycle,
                                    srr.I_stock_cat,
                                    srr.I_repl_order_ctrl,
                                    srr.I_source_wh,
                                    srr.I_physical_locn,
                                    srr.I_source_physical_wh,
                                    srr.I_activate_date,
                                    srr.I_deactivate_date,
                                    srr.I_repl_method,
                                    srr.I_pres_stock,
                                    srr.I_demo_stock,
                                    srr.I_min_stock,
                                    srr.I_max_stock,
                                    srr.I_incr_pct,
                                    srr.I_min_supply_days,
                                    srr.I_max_supply_days,
                                    srr.I_time_supply_horizon,
                                    srr.I_inv_selling_days,
                                    srr.I_service_level,
                                    srr.I_lost_sales_factor,
                                    srr.I_pickup_lead_time,
                                    srr.I_wh_lead_time,
                                    srr.I_terminal_stock_qty,
                                    srr.I_season_id,
                                    srr.I_phase_id,
                                    srr.I_dept,
                                    srr.I_class,
                                    srr.I_subclass,
                                    srr.I_domain_id,
                                    srr.I_store_ord_mult,
                                    srr.I_repl_pack,
                                    srr.I_repl_pack_qty,
                                    srr.I_unit_cost,
                                    NULL,                     -- I_unit_retail
                                    srr.I_round_to_inner_pct,
                                    srr.I_round_to_pallet_pct,
                                    srr.I_round_to_case_pct,
                                    srr.I_supp_pack_size,
                                    srr.I_inner_pack_size,
                                    srr.I_ti,
                                    srr.I_hi,
                                    srr.I_supp_lead_time,
                                    srr.I_sup_dept_lvl_ord,
                                    srr.I_drc_store_ord_mult,
                                    srr.I_non_scaling_ind,
                                    srr.I_max_scale_value,
                                    srr.I_sup_delivery_policy,
                                    srr.I_wh_delivery_policy,
                                    srr.I_last_delivery_date,
                                    srr.I_tsf_po_link_no,
                                    srr.I_reject_store_ord_ind,
                                    NULL,                     -- I_due_ord_process_ind
                                    NULL,                     -- I_due_ord_serv_basis
                                    NULL,                     -- I_pool_supplier
                                    srr.I_deposit_item_type,
                                    srr.I_container_item,
                                    NVL(srr.I_store_need,0),
                                    case
                                       when srr.I_mbr_order_qty > 0 then
                                         'Y'
                                       else
                                         'N'
                                       end O_due_ind,
                                    srr.I_mbr_order_qty,
                                    'Y',
                                    'Y',
                                    srr.I_service_level_type,
                                    srr.I_last_run_of_the_day,
                                    ord_temp_sequence.NEXTVAL,
                                    srr.thread_id,
                                    srr.I_add_lead_time_ind,
                                    srr.I_pack_ind
                               from svc_repl_roq srr
                              where srr.thread_id = I_thread_id
                                and I_stock_cat = 'L';

   elsif I_stock_cat = 'W' and
         I_loc_type  = 'W' then
      insert into svc_repl_roq_gtt (I_item,
                                    I_locn,
                                    I_locn_type,
                                    I_source_wh,
                                    I_stock_cat,
                                    I_primary_repl_supplier,
                                    I_origin_country_id,
                                    I_review_cycle,
                                    I_pres_stock,
                                    I_demo_stock,
                                    I_repl_method,
                                    I_min_stock,
                                    I_max_stock,
                                    I_incr_pct,
                                    I_min_supply_days,
                                    I_max_supply_days,
                                    I_time_supply_horizon,
                                    I_inv_selling_days,
                                    I_service_level,
                                    I_lost_sales_factor,
                                    I_terminal_stock_qty,
                                    I_season_id,
                                    I_phase_id,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    I_store_ord_mult,
                                    I_repl_pack,
                                    I_repl_pack_qty,
                                    I_supp_pack_size,
                                    I_inner_pack_size,
                                    I_ti,
                                    I_hi,
                                    I_unit_cost,
                                    I_reject_store_ord_ind,
                                    I_pack_ind,
                                    I_item_level,
                                    I_tran_level,
                                    I_domain_id,
                                    I_due_ord_process_ind,
                                    I_due_ord_serv_basis,
                                    I_pool_supplier,
                                    I_unit_retail,
                                    I_store_need,
                                    O_order_qty,
                                    O_due_ind,
                                    O_aso,
                                    O_eso,
                                    I_date,
                                    I_sub_forecast_ind,
                                    I_sub_stock_ind,
                                    I_activate_date,
                                    I_aso,
                                    I_buyer,
                                    I_container_item,
                                    I_deactivate_date,
                                    I_deposit_item_type,
                                    I_drc_store_ord_mult,
                                    I_due_ind,
                                    I_eso,
                                    I_last_delivery_date,
                                    I_max_scale_value,
                                    I_non_scaling_ind,
                                    I_physical_locn,
                                    I_pickup_lead_time,
                                    I_repl_order_ctrl,
                                    I_round_to_case_pct,
                                    I_round_to_inner_pct,
                                    I_round_to_pallet_pct,
                                    I_source_physical_wh,
                                    I_sup_delivery_policy,
                                    I_sup_dept_lvl_ord,
                                    I_supp_lead_time,
                                    I_tsf_po_link_no,
                                    I_wh_delivery_policy,
                                    I_wh_lead_time,
                                    I_service_level_type,
                                    I_last_run_of_the_day,
                                    thread_id,
                                    I_add_lead_time_ind)
                             select /*+ index(ril, pk_repl_item_loc) */
                                    srr.i_item,
                                    ril.location,
                                    'S',
                                    srr.i_locn, --source_wh
                                    /*
                                     * hard code to 'C' to force program to round using the packs
                                     * store ord mult, and not defaulting to case
                                     */
                                    'C',
                                    srr.i_primary_repl_supplier,
                                    srr.i_origin_country_id,
                                    srr.i_review_cycle,
                                    ril.pres_stock,
                                    ril.demo_stock,
                                    srr.i_repl_method,
                                    ril.min_stock,
                                    ril.max_stock,
                                    ril.incr_pct,
                                    srr.i_min_supply_days,
                                    srr.i_max_supply_days,
                                    srr.i_time_supply_horizon,
                                    srr.i_inv_selling_days,
                                    srr.i_service_level,
                                    srr.i_lost_sales_factor,
                                    ril.terminal_stock_qty,
                                    ril.season_id,
                                    ril.phase_id,
                                    srr.i_dept,
                                    srr.i_class,
                                    srr.i_subclass,
                                    ril.store_ord_mult,
                                    srr.i_repl_pack,
                                    srr.i_repl_pack_qty,
                                    srr.i_supp_pack_size,
                                    srr.i_inner_pack_size,
                                    srr.i_ti,
                                    srr.i_hi,
                                    srr.i_unit_cost,
                                    ril.reject_store_ord_ind,
                                    srr.i_pack_ind,
                                    srr.i_item_level,
                                    srr.i_tran_level,
                                    srr.i_domain_id,
                                    NULL,                    -- I_due_ord_process_ind
                                    NULL,                    -- I_due_ord_serv_basis
                                    NULL,                    -- I_pool_supplier
                                    NULL,                    -- I_unit_retail
                                    NVL(srr.i_store_need,0),
                                    0,                       -- O_order_qty
                                    'N',                     -- O_due_ind
                                    0,                       -- O_aso
                                    0,                       -- O_eso
                                    srr.i_date,              -- vdate plus one for rplext
                                    'Y',
                                    'Y',
                                    srr.I_activate_date,     -- I_activate_date
                                    srr.I_aso,               -- I_aso
                                    srr.I_buyer,             -- I_buyer
                                    srr.I_container_item,
                                    srr.I_deactivate_date,   -- I_deactivate_date
                                    srr.I_deposit_item_type,
                                    srr.I_drc_store_ord_mult,
                                    srr.I_due_ind,
                                    srr.I_eso,
                                    srr.I_last_delivery_date,
                                    srr.I_max_scale_value,
                                    srr.I_non_scaling_ind,
                                    srr.I_physical_locn,
                                    srr.I_pickup_lead_time,
                                    srr.I_repl_order_ctrl,
                                    srr.I_round_to_case_pct,
                                    srr.I_round_to_inner_pct,
                                    srr.I_round_to_pallet_pct,
                                    srr.I_source_physical_wh,
                                    srr.I_sup_delivery_policy,
                                    srr.I_sup_dept_lvl_ord,
                                    srr.I_supp_lead_time,
                                    srr.I_tsf_po_link_no,
                                    srr.I_wh_delivery_policy,
                                    ril.wh_lead_time,
                                    srr.I_service_level_type,
                                    srr.I_last_run_of_the_day,
                                    srr.thread_id,            -- thread_id
                                    srr.I_add_lead_time_ind
                               from repl_item_loc ril,
                                    store s,
                                    svc_repl_roq srr
                              where ril.item = srr.i_item
                                and ril.status = 'A'
                                and ril.stock_cat = 'W'
                                and ril.source_wh = srr.i_locn
                                    /*
                                     * Ensure stock will arrive at an open store
                                     */
                                and s.store = ril.location
                                and (LP_vdate <=
                                       (NVL(s.store_close_date, LP_vdate) -
                                        NVL(s.stop_order_days, 0))
                                       or s.store_close_date is NULL)
                                and s.store_open_date - s.start_order_days <= LP_vdate
                                    /*
                                     * Make sure the item is on active replenishment
                                     */
                                and (ril.activate_date <= LP_vdate
                                and NVL(ril.deactivate_date, (LP_vdate + 1))
                                        > LP_vdate)
                                and srr.i_locn_type = 'W'
                                and srr.i_stock_cat = 'W'
                                and srr.i_repl_method in ('D','T')
                                and srr.thread_id = I_thread_id;
   end if;

   -- get vendor line info
   if GET_VENDOR_LINE_INFO (O_error_message,
                            I_thread_id) = FALSE then
      return FALSE;
   end if;

   -- get current unit retail except for Link/WH stock category
   if I_stock_cat is NULL or
      I_stock_cat != 'L' then
      if GET_CURRENT_UNIT_RETAIL (O_error_message,
                                  I_thread_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SETUP_DATA_GTT;
---------------------------------------------------------------------------------------------
--#B.2.3
FUNCTION PACK_ROUNDING(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_thread_id        IN       NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_EXT_SQL.PACK_ROUNDING';

BEGIN

   merge into svc_repl_roq_gtt target
      using (WITH
                v_pre_pack_rounding AS (
                   select srrg.i_item,
                          srrg.i_locn,
                          srrg.i_locn_type,
                          srrg.i_stock_cat,
                          srrg.i_sub_item_master,
                          srrg.i_lead_linked_wh,
                          srrg.i_repl_pack,
                          srrg.i_repl_pack_qty,
                          srrg.o_order_qty,
                          --   /* order quantities are rounded for each store. */
                          --   if(MATCH(oa_pack_dist->s_store_ord_mult, "C"))
                          --      ld_rounded_pct = *ia_repl_info.d_round_to_case_pct;
                          --   else if (MATCH(oa_pack_dist->s_store_ord_mult,"I"))
                          --      ld_rounded_pct = *ia_repl_info.d_round_to_inner_pct;
                          --   else /* eaches */
                          --      ld_rounded_pct = 0;
                          case
                             when srrg.i_store_ord_mult = 'C' then
                                srrg.i_round_to_case_pct
                              when srrg.i_store_ord_mult = 'I' then
                                srrg.i_round_to_inner_pct
                             else
                                0
                             end rounded_pct,
                          -- max scale value
                          case
                             -- (singular item)
                             when srrg.i_repl_pack is NULL or
                                  srrg.o_order_qty < 0 then
                                srrg.i_max_scale_value
                             else -- (packitem) srrg.i_repl_pack is NOT NULL and srrg.i_repl_pack_qty is NOT NULL then
                                case
                                   when srrg.i_repl_method in ('C','M','F') then
                                      srrg.i_max_scale_value/srrg.i_repl_pack_qty
                                   else
                                      srrg.i_max_scale_value
                                   end
                             end max_scale_value,
                          -- pack_item_qty
                          case
                             -- (singular item)
                             when srrg.o_order_qty < 0 then                                  
                                1
                             else -- 
                                case
                                   -- Store/Crossdock Loc and Stock_cat will use packsize base on store_ord_mult
                                   -- ,otherwise pack qty will be used in computation for pack rounding.
                                   when srrg.i_locn_type = 'S' and
                                        srrg.i_stock_cat = 'C' and
                                        srrg.o_order_qty > 0 then
                                      case
                                         when srrg.i_store_ord_mult = 'C' then
                                            srrg.I_supp_pack_size
                                         when srrg.i_store_ord_mult = 'I' then
                                            srrg.I_inner_pack_size
                                         else
                                           NVL(srrg.i_repl_pack_qty,1) --1
                                         end
                                   else
                                      NVL(srrg.i_repl_pack_qty,1)
                                   end
                             end pack_item_qty,
                          -- due_ind
                          case
                             -- for W/W loc/stock_cat
                             when srrg.i_locn_type = 'W' and
                                  srrg.i_stock_cat = 'W' and
                                  srrg.i_repl_method = 'D' and
                                  NVL(srrg.i_due_ord_process_ind,'N') = 'Y' then
                                   /* i_eso and i_aso are the summation of values from stores
                                      sourced under the same wh. */
                                case
                                   when srrg.i_eso > srrg.i_aso then
                                      'Y'
                                   else
                                      'N'
                                   end
                             when srrg.i_locn_type = 'W' and
                                  srrg.i_stock_cat = 'W' and
                                  srrg.i_repl_method != 'D' and
                                  NVL(srrg.i_due_ord_process_ind,'N') = 'Y' then
                                nvl(srrg.i_due_ind, 'N')  --this hold the flag value of Y if one or more store under the source wh is due. Please see SETUP_DATA for reference.
                             when srrg.i_locn_type = 'W' and
                                  srrg.i_stock_cat = 'W' and
                                  srrg.i_repl_method != 'D' then
                                srrg.o_due_ind
                             else
                                srrg.o_due_ind
                             end due_ind,
                          --aso
                          case
                             -- for W/W loc/stock_cat and D,T repl_method
                             when srrg.i_locn_type = 'W' and
                                  srrg.i_stock_cat = 'W' and
                                  srrg.i_repl_method in ('D','T') then
                               /* i_aso is the summation of values from stores
                                  sourced under the same wh. */
                                srrg.i_aso
                             else
                                srrg.o_aso
                             end aso,
                          --eso
                          case
                             -- for W/W loc/stock_cat and D,T repl_method
                             when srrg.i_locn_type = 'W' and
                                  srrg.i_stock_cat = 'W' and
                                  srrg.i_repl_method in ('D','T') then
                               /* i_eso is the summation of values from stores
                                  sourced under the same wh. */
                                srrg.i_eso
                             else
                                srrg.o_eso
                             end eso,
                             srrg.i_repl_order_ctrl,
                             srrg.i_repl_method
                     from svc_repl_roq_gtt srrg
                    where srrg.thread_id = I_thread_id
                      and srrg.i_sub_item_master is NULL
                      and srrg.i_lead_linked_wh is NULL),
                --
                v_rounding_qty AS (
                   select vppr.i_item,
                          vppr.i_locn,
                          vppr.i_locn_type,
                          vppr.i_stock_cat,
                          vppr.i_sub_item_master,
                          vppr.i_lead_linked_wh,
                          vppr.i_repl_pack,
                          vppr.o_order_qty,
                          vppr.max_scale_value,
                          vppr.pack_item_qty,
                          vppr.due_ind,
                          vppr.aso,
                          vppr.eso,
                          vppr.i_repl_pack_qty,
                          case
                             -- if the qty to round can be fully divided then no rounding
                             when MOD(vppr.o_order_qty, vppr.pack_item_qty) = 0 then
                                vppr.o_order_qty
                             else
                                case
                                   when ((vppr.o_order_qty - (floor(vppr.o_order_qty / vppr.pack_item_qty) * vppr.pack_item_qty) >=
                                         vppr.pack_item_qty * (vppr.rounded_pct / 100)) or
                                        (floor(vppr.o_order_qty / vppr.pack_item_qty) = 0)) then
                                      -- round-up
                                      (floor(vppr.o_order_qty / vppr.pack_item_qty) + 1) * vppr.pack_item_qty
                                   else
                                      -- round-down
                                      floor(vppr.o_order_qty / vppr.pack_item_qty) * vppr.pack_item_qty
                                   end
                          end rounded_qty,
                          vppr.i_repl_order_ctrl,
                          vppr.i_repl_method
                     from v_pre_pack_rounding vppr)
             -- end of WITH clause
             select w.i_item,
                    w.i_locn,
                    w.i_sub_item_master,
                    w.i_lead_linked_wh,
                    w.i_repl_pack,
                    w.o_order_qty,
                    w.max_scale_value,
                    w.pack_item_qty,
                    w.due_ind,
                    w.aso,
                    w.eso,                  
                    case
                       when w.o_order_qty < 0 then
                            w.o_order_qty
                       when w.i_repl_pack is NULL then
                            w.rounded_qty                             
                       else
                          w.rounded_qty/i_repl_pack_qty
                       end pack_dist,
                    w.i_repl_order_ctrl,
                    w.i_repl_method,
                    w.i_stock_cat
               from v_rounding_qty w) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.i_repl_order_ctrl      = use_this.i_repl_order_ctrl
          and target.i_stock_cat            = use_this.i_stock_cat
          and NVL(target.i_repl_method,'X') = NVL(use_this.i_repl_method,'X')
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.o_no_round_order_qty = use_this.o_order_qty,  --saved the original order_qty values before being changed to rounded pack below.
                    target.o_order_qty          = use_this.pack_dist,
                    target.i_repl_pack_qty      = use_this.pack_item_qty,
                    target.i_max_scale_value    = use_this.max_scale_value,
                    target.o_due_ind            = use_this.due_ind,
                    target.o_aso                = use_this.aso,
                    target.o_eso                = use_this.eso;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PACK_ROUNDING;
---------------------------------------------------------------------------------------------
--#B.2:
FUNCTION PROCESS_DATA(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_thread_id         IN       NUMBER,
                      I_last_run_of_day   IN       VARCHAR2 DEFAULT 'Y')
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_EXT_SQL.PROCESS_DATA';

BEGIN

   --#B.2.1: Call CORESVC_REPL_ROQ_SQL.CONSUME except for stock_cat != 'L'
   if NOT CORESVC_REPL_ROQ_SQL.CONSUME (O_error_message,
                                        I_thread_id,
                                        I_last_run_of_day,
                                        'RPLEXT')then --I_calling_program
      return FALSE;
   end if;

   --#B.2.2: Insert stock_cat = 'L' records (svc_repl_roq) to svc_repl_roq_gtt
   if SETUP_DATA_GTT(O_error_message,
                     I_thread_id,
                     'L') = FALSE then  --I_stock_cat
      return FALSE;
   end if;

   --#B.2.3: Round roq for pack items. Call pack rounding qty function.
   if PACK_ROUNDING(O_error_message,
                    I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_DATA;
---------------------------------------------------------------------------------------------
--#B.3
FUNCTION PERSIST_DATA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_thread_id       IN       NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_EXT_SQL.PERSIST_DATA';
   L_plength   NUMBER(2)    := LENGTH('CORESVC_REPL_EXT_SQL.PERSIST_DATA');

BEGIN

   -- insert_1: insert into ord_temp where
   -- I_repl_order_ctrl in ('S','A')
   -- (O_due_ind = 'Y' or I_due_ord_process_ind = 'Y') and O_order_qty > 0
   L_program := substr(L_program,1,L_plength)||'::INSERT_1';
   --#B.3.1
   insert into ord_temp (item,
                         pack_ind,
                         location,
                         loc_type,
                         crossdock_ind,
                         crossdock_store,
                         supplier,
                         origin_country_id,
                         unit_cost,
                         supp_pack_size,
                         qty_ordered,
                         pickup_lead_time,
                         supp_lead_time,
                         dept,
                         order_status,
                         ord_temp_seq_no,
                         due_ind)
                  select DECODE (i_repl_pack,
                                 NULL, i_item,
                                 i_repl_pack) item,
                         DECODE (i_repl_pack,
                                 NULL, 'N',
                                 'Y') pack_ind,
                         /* If this is a crossdock order, then it is actually being sent to
                            the sourcing warehouse, and from there being allocated to the
                            store. */
                         DECODE (i_stock_cat,
                                 'C', i_source_wh,
                                 i_locn) location,
                         DECODE (i_stock_cat,
                                 'C', 'W',
                                 i_locn_type) loc_type,
                         DECODE (i_stock_cat,
                                 'C', 'Y',
                                 'N') crossdock_ind,
                         DECODE (i_stock_cat,
                                 'C', i_locn,
                                 NULL) crossdock_store,
                         -- end crossdock order checking
                         i_primary_repl_supplier,
                         i_origin_country_id,
                         i_unit_cost,
                         i_supp_pack_size,
                         o_order_qty,
                         i_pickup_lead_time,
                         i_supp_lead_time,
                         i_dept,
                         DECODE (i_repl_order_ctrl,
                                 'S', 'W',
                                 'A') order_status,
                         i_ord_temp_seq_no,
                         o_due_ind
                    from svc_repl_roq_gtt
                   where i_repl_order_ctrl in ('S','A')
                     and (   O_due_ind = 'Y'
                          or NVL(I_due_ord_process_ind,'N') = 'Y')
                     and O_order_qty > 0
                     and thread_id = I_thread_id;

   -- insert_2: insert into ord_temp the container item for deposit_item_type = 'E' (content) in the svc_repl_roq_gtt.
   L_program := substr(L_program,1,L_plength)||'::INSERT_2';
   --#B.3.2
   insert into ord_temp (item,
                         supplier,
                         origin_country_id,
                         location,
                         unit_cost,
                         pack_ind,
                         loc_type,
                         crossdock_ind,
                         crossdock_store,
                         supp_pack_size,
                         qty_ordered,
                         pickup_lead_time,
                         supp_lead_time,
                         dept,
                         order_status,
                         ord_temp_seq_no,
                         due_ind)
                  select iscl.item,
                         iscl.supplier,
                         iscl.origin_country_id,
                         iscl.loc,
                         iscl.unit_cost,
                         DECODE (srrg.i_repl_pack,
                                 NULL, 'N',
                                 'Y') pack_ind,
                         /* If this is a crossdock order, then it is actually being sent to
                            the sourcing warehouse, and from there being allocated to the
                            store. */
                         DECODE (srrg.i_stock_cat,
                                 'C', 'W',
                                 srrg.i_locn_type) loc_type,
                         DECODE (srrg.i_stock_cat,
                                 'C', 'Y',
                                 'N') crossdock_ind,
                         DECODE (srrg.i_stock_cat,
                                 'C', srrg.i_locn,
                                 NULL) crossdock_store,
                         -- end crossdock order checking
                         srrg.i_supp_pack_size,
                         srrg.o_order_qty,
                         srrg.i_pickup_lead_time,
                         srrg.i_supp_lead_time,
                         srrg.i_dept,
                         DECODE (srrg.i_repl_order_ctrl,
                                 'S', 'W',
                                 'A') order_status,
                         srrg.i_ord_temp_seq_no,
                         srrg.o_due_ind
                    from svc_repl_roq_gtt srrg,
                         item_supp_country_loc iscl
                   where iscl.item = srrg.i_container_item
                     and iscl.supplier = srrg.i_primary_repl_supplier
                     and iscl.origin_country_id = srrg.i_origin_country_id
                     and iscl.loc = DECODE (srrg.i_stock_cat,
                                            'C', srrg.i_source_wh,
                                            srrg.i_locn)
                     and srrg.i_repl_order_ctrl in ('S','A')
                     and (   srrg.O_due_ind = 'Y'
                          or NVL(srrg.i_due_ord_process_ind,'N') = 'Y')
                     and srrg.O_order_qty > 0
                     and srrg.i_deposit_item_type = 'E'
                     and srrg.thread_id = I_thread_id;

   -- insert_3: insert into ord_temp for stock_cat = 'C'. Warehouse portion of the crossdock.
   L_program := substr(L_program,1,L_plength)||'::INSERT_3';
   --#B.3.3
   merge into ord_temp target
      using (WITH
                v_pre_pack_rounding AS (
                   select DECODE (i_repl_pack,
                                  NULL, srrg.i_item,
                                  srrg.i_repl_pack) item,
                          srrg.i_source_wh location,
                          DECODE (i_repl_pack,
                                  NULL, 'N',
                                  'Y') pack_ind,
                          srrg.i_primary_repl_supplier,
                          srrg.i_origin_country_id,
                          srrg.i_unit_cost,
                          srrg.i_supp_pack_size,
                          srrg.o_order_qty,
                          srrg.i_pickup_lead_time,
                          srrg.i_supp_lead_time,
                          srrg.i_dept,
                          DECODE (srrg.i_repl_order_ctrl,
                                  'S', 'W',
                                  'A') order_status,
                          srrg.i_locn,
                          srrg.i_sub_item_master,
                          srrg.i_lead_linked_wh,
                          srrg.i_repl_pack,
                          case
                             when srrg.i_store_ord_mult = 'C' then
                                srrg.i_round_to_case_pct
                              when srrg.i_store_ord_mult = 'I' then
                                srrg.i_round_to_inner_pct
                             else
                                0
                             end rounded_pct,
                          --
                          case
                             when srrg.i_store_ord_mult = 'C' then
                                srrg.I_supp_pack_size
                              when srrg.i_store_ord_mult = 'I' then
                                srrg.I_inner_pack_size
                             else
                                1
                             end pack_size
                     from svc_repl_roq_gtt srrg
                    where srrg.I_locn_type = 'S'
                      and srrg.I_stock_cat = 'C'
                      and srrg.o_order_qty > 0
                      and srrg.I_repl_order_ctrl in ('S','A')
                      and (NVL(srrg.I_due_ord_process_ind,'N') = 'Y'
                           or (NVL(srrg.I_due_ord_process_ind,'N') = 'N'
                               and srrg.O_due_ind = 'Y'))
                      and srrg.thread_id = I_thread_id
                      and srrg.i_sub_item_master is NULL
                      and srrg.i_lead_linked_wh is NULL),
                --
                v_wh_xdock_qty AS (
                   select vppr.item,
                          vppr.location,
                          vppr.pack_ind,
                          vppr.i_primary_repl_supplier,
                          vppr.i_origin_country_id,
                          vppr.i_unit_cost,
                          vppr.i_supp_pack_size,
                          case
                             -- if the qty to round can be fully divided then no rounding
                             when MOD(vppr.o_order_qty, vppr.pack_size) = 0 then
                                vppr.o_order_qty
                             else
                                case
                                   when ((vppr.o_order_qty - (floor(vppr.o_order_qty / vppr.pack_size) * vppr.pack_size) >=
                                          vppr.pack_size * (vppr.rounded_pct / 100)) or
                                          (floor(vppr.o_order_qty / vppr.pack_size) = 0)) then
                                      -- round-up
                                      (floor(vppr.o_order_qty / vppr.pack_size) + 1) * vppr.pack_size
                                   else
                                      -- round-down
                                      floor(vppr.o_order_qty / vppr.pack_size) * vppr.pack_size
                                   end
                             end rounded_order_qty,
                          vppr.i_pickup_lead_time,
                          vppr.i_supp_lead_time,
                          vppr.i_dept,
                          vppr.order_status
                     from v_pre_pack_rounding vppr)
             -- end of WITH clause
             select w.item,
                    w.location, -- i_source_wh
                    'W' loc_type,
                    MIN(w.pack_ind) pack_ind,
                    w.i_primary_repl_supplier supplier,
                    w.i_origin_country_id origin_country_id,
                    MIN(w.i_unit_cost) unit_cost,
                    MIN(w.i_supp_pack_size) supp_pack_size,
                    SUM(w.rounded_order_qty) qty_ordered,
                    MIN(w.i_pickup_lead_time) pickup_lead_time,
                    MIN(w.i_supp_lead_time) supp_lead_time,
                    MIN(w.i_dept) dept,
                    'Y'  crossdock_ind,
                    NULL crossdock_store,
                    'N'  due_ind,
                    w.order_status order_status
               from v_wh_xdock_qty w
              group by w.item, w.location, w.i_primary_repl_supplier, w.i_origin_country_id, w.order_status) use_this
      on (    target.item     = use_this.item
          and target.location = use_this.location
          and target.ord_temp_seq_no is NULL
          and target.origin_country_id = use_this.origin_country_id
          and target.supplier = use_this.supplier
          and target.order_status = use_this.order_status)
      when matched then
         /* add the summed qty_ordered into the target.qty_ordered as it is possible that item/loc(src_wh) is broken
            apart into other threads as well.*/
         update set target.qty_ordered  = NVL(target.qty_ordered,0) + use_this.qty_ordered
      when not matched then
         insert (target.item,
                 target.location,
                 target.loc_type,
                 target.pack_ind,
                 target.supplier,
                 target.origin_country_id,
                 target.unit_cost,
                 target.supp_pack_size,
                 target.qty_ordered,
                 target.pickup_lead_time,
                 target.supp_lead_time,
                 target.dept,
                 target.crossdock_ind,
                 target.crossdock_store,
                 target.due_ind,
                 target.order_status,
                 target.ord_temp_seq_no)
         values (use_this.item,
                 use_this.location,
                 use_this.loc_type,
                 use_this.pack_ind,
                 use_this.supplier,
                 use_this.origin_country_id,
                 use_this.unit_cost,
                 use_this.supp_pack_size,
                 use_this.qty_ordered,
                 use_this.pickup_lead_time,
                 use_this.supp_lead_time,
                 use_this.dept,
                 use_this.crossdock_ind,
                 use_this.crossdock_store,
                 use_this.due_ind,
                 use_this.order_status,
                 NULL);

   -- insert_4: insert into repl_result except for stock_cat = 'C'(wh side) and container item.
   L_program := substr(L_program,1,L_plength)||'::INSERT_4';
   --#B.3.4
   insert into repl_results(source_type,
                            status,
                            ord_temp_seq_no,
                            tsf_po_link_no,
                            master_item,
                            item,
                            item_type,
                            dept,
                            class,
                            subclass,
                            loc_type,
                            location,
                            physical_wh,
                            primary_repl_supplier,
                            origin_country_id,
                            buyer,
                            pool_supplier,
                            supp_unit_cost,
                            unit_cost,
                            pack_qty,
                            order_point,
                            order_up_to_point,
                            net_inventory,
                            safety_stock,
                            lost_sales,
                            review_time,
                            review_cycle,
                            stock_cat,
                            repl_order_ctrl,
                            source_wh,
                            activate_date,
                            deactivate_date,
                            pres_stock,
                            demo_stock,
                            repl_method,
                            min_stock,
                            max_stock,
                            incr_pct,
                            min_supply_days,
                            max_supply_days,
                            time_supply_horizon,
                            inv_selling_days,
                            service_level,
                            lost_sales_factor,
                            supp_lead_time,
                            pickup_lead_time,
                            wh_lead_time,
                            curr_order_lead_time,
                            next_order_lead_time,
                            colt_added,
                            nolt_added,
                            terminal_stock_qty,
                            season_id,
                            phase_id,
                            repl_date,
                            orig_raw_roq,
                            orig_raw_roq_pack,
                            raw_roq,
                            raw_roq_pack,
                            prescale_roq,
                            order_roq,
                            stock_on_hand,
                            pack_comp_soh,
                            on_order,
                            in_transit_qty,
                            pack_comp_intran,
                            tsf_resv_qty,
                            pack_comp_resv,
                            tsf_expected_qty,
                            pack_comp_exp,
                            rtv_qty,
                            alloc_in_qty,
                            alloc_out_qty,
                            non_sellable_qty,
                            min_supply_days_forecast,
                            max_supply_days_forecast,
                            time_supply_horizon_forecast,
                            inv_sell_days_forecast,
                            review_time_forecast,
                            order_lead_time_forecast,
                            next_lead_time_forecast,
                            accepted_stock_out,
                            estimated_stock_out,
                            due_ind,
                            non_scaling_ind,
                            max_scale_value,
                            inner_size,
                            case_size,
                            ti,
                            hi,
                            store_ord_mult,
                            recalc_type)
                    select  'R',
                            'W',
                            case
                               when i_repl_order_ctrl in ('S','A') and o_order_qty > 0 then
                                  i_ord_temp_seq_no
                               else
                                  NULL
                               end i_ord_temp_seq_no,
                            i_tsf_po_link_no,
                            i_item,  --master_item
                            DECODE (i_repl_pack,
                                    NULL ,i_item,
                                    i_repl_pack) item,
                            DECODE (i_repl_pack,
                                    NULL ,'M',
                                    'P') item_type,
                            i_dept,
                            i_class,
                            i_subclass,
                            i_locn_type,
                            i_locn,
                            i_source_physical_wh,
                            i_primary_repl_supplier,
                            i_origin_country_id,
                            i_buyer,
                            /* only write pool supplier to the table if
                               the order control is buyer worksheet and
                               the pool supplier is NOT NULL */
                            DECODE(i_repl_order_ctrl,
                                   'B', DECODE(i_pool_supplier,
                                               -1, NULL,
                                               i_pool_supplier),
                                   NULL),
                            i_unit_cost,
                            /* supplier unit cost is the same as the
                               unit cost in the driving cursor in this program */
                            i_unit_cost,
                            /* if not using simple pack(s) the packitem qty should
                               be NULL */
                            DECODE(i_repl_pack
                                   ,NULL ,NULL
                                   ,i_repl_pack_qty) pack_qty,
                            o_order_point,
                            o_order_up_to_point,
                            o_net_inventory,
                            o_safety_stock,
                            o_lost_sales,
                            i_review_lead_time,
                            i_review_cycle,
                            i_stock_cat,
                            i_repl_order_ctrl,
                            i_source_wh,
                            DECODE(i_stock_cat,
                                   'L', LP_vdate,
                                   i_activate_date) activate_date,
                            i_deactivate_date,
                            i_pres_stock,
                            i_demo_stock,
                            i_repl_method,
                            i_min_stock,
                            i_max_stock,
                            i_incr_pct,
                            i_min_supply_days,
                            i_max_supply_days,
                            i_time_supply_horizon,
                            i_inv_selling_days,
                            i_service_level,
                            i_lost_sales_factor,
                            i_supp_lead_time,
                            i_pickup_lead_time,
                            case
                               when i_wh_lead_time is NULL then
                                  case
                                     when i_next_order_lead_time is NULL then
                                        NULL
                                     else
                                        NVL(i_wh_lead_time,0)
                                     end
                               else
                                  NVL(i_wh_lead_time,0)
                               end,
                            i_curr_order_lead_time,
                            i_next_order_lead_time,
                            i_days_added_to_colt,
                            i_days_added_to_nolt,
                            i_terminal_stock_qty,
                            i_season_id,
                            i_phase_id,
                            LP_vdate,
                            --
                            case
                               when i_repl_pack is NOT NULL then
                                  case
                                     when i_locn_type   = 'W' and
                                          i_repl_method = 'D' then
                                        o_no_round_order_qty * i_repl_pack_qty
                                     else
                                        o_no_round_order_qty
                                  end
                               else
                                  o_no_round_order_qty
                               end orig_raw_roq,
                            --
                            case
                               when i_repl_pack is NOT NULL then
                                  case
                                     when i_locn_type   = 'W' and
                                          i_repl_method = 'D' then
                                        o_no_round_order_qty
                                  else
                                     o_no_round_order_qty/i_repl_pack_qty
                                  end
                               else
                                  NULL
                               end orig_raw_roq_pack,
                            --
                            case
                               when i_repl_pack is NOT NULL then
                                  case
                                     when i_locn_type   = 'W' and
                                          i_repl_method = 'D' then
                                        o_no_round_order_qty * i_repl_pack_qty
                                     else
                                        o_no_round_order_qty
                                     end
                               else
                                  o_no_round_order_qty
                               end raw_roq,
                            --
                            case
                               when i_repl_pack is NOT NULL then
                                  case
                                     when i_locn_type   = 'W' and
                                          i_repl_method = 'D' then
                                        o_no_round_order_qty
                                     else
                                        o_no_round_order_qty/i_repl_pack_qty
                                     end
                               else
                                  NULL
                               end raw_roq_pack,
                            o_order_qty,            --prescale_roq
                            o_order_qty,            --order_roq
                            o_stock_on_hand,
                            o_pack_comp_soh,
                            o_on_order,
                            o_in_transit_qty,
                            o_pack_comp_intran,
                            o_tsf_resv_qty,
                            o_pack_comp_resv,
                            o_tsf_expected_qty,
                            o_pack_comp_exp,
                            o_rtv_qty,
                            o_alloc_in_qty,
                            o_alloc_out_qty,
                            o_non_sellable_qty,
                            o_min_supply_days_forecast,
                            o_max_supply_days_forecast,
                            o_tsh_forecast,
                            o_isd_forecast,
                            o_review_time_forecast,
                            o_curr_olt_forecast,
                            o_next_olt_forecast,
                            NVL(o_aso,0),
                            NVL(o_eso,0),
                            o_due_ind,
                            i_non_scaling_ind,
                            case
                               when i_stock_cat       = 'L' or
                                    i_non_scaling_ind = 'Y' then
                                  NULL
                               else
                                  i_max_scale_value
                               end max_scale_value,
                            i_inner_pack_size,
                            i_supp_pack_size,       --case_size
                            i_ti,
                            i_hi,
                            i_drc_store_ord_mult,   --store_ord_mult
                            'N'                     --recalc_type
                        from svc_repl_roq_gtt
                       where thread_id = I_thread_id;

   --  update_1: update repl_item_loc except for stock_cat = 'L'.
   L_program := substr(L_program,1,L_plength)||'::UPDATE_1';
   --#B.3.5
   merge into repl_item_loc target
      using (select i_item,
                    i_locn,
                    o_order_qty,
                    LP_vdate last_review_date, --review_date
                    i_last_delivery_date,
                    i_next_delivery_date,
                    i_next_review_date
               from svc_repl_roq_gtt
              where i_stock_cat != 'L'
                and thread_id = I_thread_id
                and i_sub_item_master is NULL
                and i_lead_linked_wh is NULL) use_this
      on (    target.item     = use_this.i_item
          and target.location = use_this.i_locn)
      when matched then
         update set target.last_review_date   = use_this.last_review_date,
                    target.last_delivery_date = use_this.i_last_delivery_date,
                    target.last_roq           = use_this.o_order_qty,
                    target.next_delivery_date = use_this.i_next_delivery_date,
                    target.next_review_date   = use_this.i_next_review_date;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST_DATA;
---------------------------------------------------------------------------------------------
-- Function Name:  CONSUME
---------------------------------------------------------------------------------------------
--#B
FUNCTION CONSUME(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_thread_id           IN       NUMBER,
                 I_last_run_of_day     IN       VARCHAR2 DEFAULT 'Y')
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_EXT_SQL.CONSUME';

BEGIN
   if LP_vdate is NULL then
      open C_GET_GLOBAL;
      fetch C_GET_GLOBAL into LP_vdate_plus_one
                             ,LP_vdate
                             ,LP_curr_weekday
                             ,LP_wh_cross_link_ind
                             ,LP_domain_level;
      close C_GET_GLOBAL;
   end if;
   --#B.1. insert records from svc_repl_roq into svc_repl_roq_gtt excluding those 'L' stock_cat records.
   if SETUP_DATA_GTT(O_error_message,
                     I_thread_id) = FALSE then
      return FALSE;
   end if;

   --#B.2. PROCESS_DATA actions are:
   --      step 1) process order lead times, roq and other relevant order computations
   --      step 2) insert records from svc_repl_roq into svc_repl_roq_gtt where stock_cat = 'L' (previously excluded records).
   --      step 3) perform pack roundings
   if PROCESS_DATA(O_error_message,
                   I_thread_id,
                   I_last_run_of_day) = FALSE then
      return FALSE;
   end if;

   --#B.3
   if PERSIST_DATA(O_error_message,
                   I_thread_id) = FALSE then
      return FALSE;
   end if;

   -- delete records in the working table that were successfully persisted
   delete from svc_repl_roq where thread_id = I_thread_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CONSUME;
---------------------------------------------------------------------------------------------
END CORESVC_REPL_EXT_SQL;
/
