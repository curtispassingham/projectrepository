CREATE OR REPLACE PACKAGE BODY REPLENISHMENT_SQL AS

    LP_item                      REPL_ITEM_LOC.ITEM%TYPE;
    LP_item_desc                 ITEM_MASTER.DESC_UP%TYPE;
    LP_location                  REPL_ITEM_LOC.LOCATION%TYPE;
    LP_loc_desc                  STORE.STORE_NAME%TYPE;
    LP_review_cycle              REPL_ITEM_LOC.REVIEW_CYCLE%TYPE;
    LP_review_cycle_desc         CODE_DETAIL.CODE_DESC%TYPE;
    LP_stock_cat                 REPL_ITEM_LOC.STOCK_CAT%TYPE;
    LP_stock_cat_desc            CODE_DETAIL.CODE_DESC%TYPE;
    LP_repl_order_ctrl           REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE;
    LP_repl_order_ctrl_desc      CODE_DETAIL.CODE_DESC%TYPE;
    LP_source_wh                 REPL_ITEM_LOC.SOURCE_WH%TYPE;
    LP_source_wh_desc            WH.WH_NAME%TYPE;
    LP_repl_method               REPL_ITEM_LOC.REPL_METHOD%TYPE;
    LP_repl_method_desc          CODE_DETAIL.CODE_DESC%TYPE;
    LP_season                    REPL_ITEM_LOC.SEASON_ID%TYPE;
    LP_season_desc               SEASONS.SEASON_DESC%TYPE;
    LP_phase                     REPL_ITEM_LOC.PHASE_ID%TYPE;
    LP_phase_desc                PHASES.PHASE_DESC%TYPE;
    ---
    LP_supplier                  REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE;
    LP_supp_name                 SUPS.SUP_NAME%TYPE;
    LP_dept                      REPL_ITEM_LOC.DEPT%TYPE;
    LP_dept_name                 DEPS.DEPT_NAME%TYPE;
    LP_class                     REPL_ITEM_LOC.CLASS%TYPE;
    LP_class_name                CLASS.CLASS_NAME%TYPE;
    LP_subclass                  REPL_ITEM_LOC.SUBCLASS%TYPE;
    LP_subclass_name             SUBCLASS.SUB_NAME%TYPE;
    LP_pack                      REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE;
    LP_pack_name                 ITEM_MASTER.DESC_UP%TYPE;
    LP_sun                       VARCHAR2(1);
    LP_mon                       VARCHAR2(1);
    LP_tue                       VARCHAR2(1);
    LP_wed                       VARCHAR2(1);
    LP_thu                       VARCHAR2(1);
    LP_fri                       VARCHAR2(1);
    LP_sat                       VARCHAR2(1);
    ---
    LP_master_item               REPL_RESULTS.MASTER_ITEM%TYPE;
    LP_master_item_desc          ITEM_MASTER.DESC_UP%TYPE;
    LP_loc_type                  REPL_RESULTS.LOC_TYPE%TYPE;
    LP_loc_type_desc             CODE_DETAIL.CODE_DESC%TYPE;
    LP_store_ord_mult            REPL_RESULTS.STORE_ORD_MULT%TYPE;
    LP_store_ord_mult_desc       CODE_DETAIL.CODE_DESC%TYPE;
    LP_inner_name                ITEM_SUPPLIER.INNER_NAME%TYPE;
    LP_inner_name_desc           CODE_DETAIL.CODE_DESC%TYPE;
    LP_case_name                 ITEM_SUPPLIER.CASE_NAME%TYPE;
    LP_case_name_desc            CODE_DETAIL.CODE_DESC%TYPE;
    LP_pallet_name               ITEM_SUPPLIER.PALLET_NAME%TYPE;
    LP_pallet_name_desc          CODE_DETAIL.CODE_DESC%TYPE;

---------------------------------------------------------------------------------------------
FUNCTION GET_FORM_INFO
   (O_error_message        IN OUT VARCHAR2,
    O_item_desc            IN OUT ITEM_MASTER.DESC_UP%TYPE,
    O_loc_desc             IN OUT STORE.STORE_NAME%TYPE,
    O_review_cycle_desc    IN OUT CODE_DETAIL.CODE_DESC%TYPE,
    O_stock_cat_desc       IN OUT CODE_DETAIL.CODE_DESC%TYPE,
    O_repl_order_ctrl_desc IN OUT CODE_DETAIL.CODE_DESC%TYPE,
    O_source_wh_desc       IN OUT WH.WH_NAME%TYPE,
    O_repl_method_desc     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
    O_season_desc          IN OUT SEASONS.SEASON_DESC%TYPE,
    O_phase_desc           IN OUT PHASES.PHASE_DESC%TYPE,
    O_supp_name            IN OUT SUPS.SUP_NAME%TYPE,
    O_dept_name            IN OUT DEPS.DEPT_NAME%TYPE,
    O_class_name           IN OUT CLASS.CLASS_NAME%TYPE,
    O_subclass_name        IN OUT SUBCLASS.SUB_NAME%TYPE,
    O_pack_name            IN OUT ITEM_MASTER.DESC_UP%TYPE,
    O_sun                  IN OUT VARCHAR2,
    O_mon                  IN OUT VARCHAR2,
    O_tue                  IN OUT VARCHAR2,
    O_wed                  IN OUT VARCHAR2,
    O_thu                  IN OUT VARCHAR2,
    O_fri                  IN OUT VARCHAR2,
    O_sat                  IN OUT VARCHAR2,
    O_loc_type_desc        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
    O_master_item_desc     IN OUT ITEM_MASTER.DESC_UP%TYPE,
    O_store_ord_mult_desc  IN OUT CODE_DETAIL.CODE_DESC%TYPE,
    O_prescaled_cost       IN OUT ORDLOC.UNIT_COST%TYPE,
    O_actual_cost          IN OUT ORDLOC.UNIT_COST%TYPE,
    O_prescaled_qty        IN OUT ORDLOC.QTY_PRESCALED%TYPE,
    O_actual_qty           IN OUT ORDLOC.QTY_ORDERED%TYPE,
    O_stock_on_hand        IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    O_incoming_stock       IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    O_outgoing_stock       IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    O_nonsellable_stock    IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    O_inner_name_desc      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
    O_case_name_desc       IN OUT CODE_DETAIL.CODE_DESC%TYPE,
    O_pallet_name_desc     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
    I_form_name            IN     VARCHAR2,
    I_item                 IN     REPL_ITEM_LOC.ITEM%TYPE,
    I_location             IN     REPL_ITEM_LOC.LOCATION%TYPE,
    I_loc_type             IN     REPL_ITEM_LOC.LOC_TYPE%TYPE,
    I_review_cycle         IN     REPL_ITEM_LOC.REVIEW_CYCLE%TYPE,
    I_stock_cat            IN     REPL_ITEM_LOC.STOCK_CAT%TYPE,
    I_repl_order_ctrl      IN     REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE,
    I_source_wh            IN     REPL_ITEM_LOC.SOURCE_WH%TYPE,
    I_repl_method          IN     REPL_ITEM_LOC.REPL_METHOD%TYPE,
    I_season               IN     REPL_ITEM_LOC.SEASON_ID%TYPE,
    I_phase                IN     REPL_ITEM_LOC.PHASE_ID%TYPE,
    I_supplier             IN     REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE,
    I_dept                 IN     REPL_ITEM_LOC.DEPT%TYPE,
    I_class                IN     REPL_ITEM_LOC.CLASS%TYPE,
    I_subclass             IN     REPL_ITEM_LOC.SUBCLASS%TYPE,
    I_pack                 IN     REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE,
    I_order_no             IN     REPL_RESULTS.ORDER_NO%TYPE,
    I_alloc_no             IN     REPL_RESULTS.ALLOC_NO%TYPE,
    I_origin_country_id    IN     REPL_RESULTS.ORIGIN_COUNTRY_ID%TYPE,
    I_master_item          IN     REPL_RESULTS.MASTER_ITEM%TYPE,
    I_store_ord_mult       IN     REPL_RESULTS.STORE_ORD_MULT%TYPE,
    I_stock_on_hand        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_pack_comp_soh        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_in_transit_qty       IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_pack_comp_intran     IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_tsf_expected_qty     IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_pack_comp_exp        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_alloc_in_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_rtv_qty              IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_tsf_resv_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_pack_comp_resv       IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_alloc_out_qty        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_layaway_qty          IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_non_sellable_qty     IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
    I_on_order             IN     REPL_RESULTS.ON_ORDER%TYPE)

RETURN BOOLEAN IS

   L_inner_name           item_supplier.inner_name%TYPE;
   L_case_name            item_supplier.case_name%TYPE;
   L_pallet_name          item_supplier.pallet_name%TYPE;

   CURSOR C_item_supplier is
      select inner_name,
             case_name,
             pallet_name
        from item_supplier
       where item              = I_item
         and supplier          = I_supplier;

BEGIN

   if (LP_item is NULL) or (I_item != LP_item) then
      if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                  LP_item_desc,
                                  I_item) = FALSE then
         RETURN FALSE;
      end if;
      LP_item := I_item;
   end if;
   O_item_desc := LP_item_desc;
   ---
   if (LP_location is NULL) or (I_location != LP_location) then
      if LOCATION_ATTRIB_SQL.GET_NAME(O_error_message,
                                      LP_loc_desc,
                                      I_location,
                                      I_loc_type) = FALSE then
         RETURN FALSE;
      end if;
      LP_location := I_location;
   end if;
   O_loc_desc := LP_loc_desc;
   ---
   if I_review_cycle is not NULL then
      if (LP_review_cycle is NULL) or (I_review_cycle != LP_review_cycle) then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'RPRC',
                                       I_review_cycle,
                                       LP_review_cycle_desc) = FALSE then
            RETURN FALSE;
         end if;
         LP_review_cycle := I_review_cycle;
      end if;
      O_review_cycle_desc := LP_review_cycle_desc;
   else
      O_review_cycle_desc := NULL;
   end if;
   ---
   if (LP_stock_cat is NULL) or (I_stock_cat != LP_stock_cat) then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'SCST',
                                    I_stock_cat,
                                    LP_stock_cat_desc) = FALSE then
         RETURN FALSE;
      end if;
      LP_stock_cat := I_stock_cat;
   end if;
   O_stock_cat_desc := LP_stock_cat_desc;
   ---
   if (LP_repl_order_ctrl is NULL) or (I_repl_order_ctrl != LP_repl_order_ctrl) then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'ROCT',
                                    I_repl_order_ctrl,
                                    LP_repl_order_ctrl_desc) = FALSE then
         RETURN FALSE;
      end if;
      LP_repl_order_ctrl := I_repl_order_ctrl;
   end if;
   O_repl_order_ctrl_desc := LP_repl_order_ctrl_desc;
   ---
   if I_source_wh is NULL then
      O_source_wh_desc := NULL;
   else
      if (LP_source_wh is NULL) or (I_source_wh != LP_source_wh) then
         if LOCATION_ATTRIB_SQL.GET_NAME(O_error_message,
                                         LP_source_wh_desc,
                                         I_source_wh,
                                         'W') = FALSE then
            RETURN FALSE;
         end if;
         LP_source_wh := I_source_wh;
      end if;
      O_source_wh_desc := LP_source_wh_desc;
   end if;
   ---
   if (LP_repl_method is NULL) or (I_repl_method != LP_repl_method) then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'RM',
                                    I_repl_method,
                                    LP_repl_method_desc) = FALSE then
         RETURN FALSE;
      end if;
      LP_repl_method := I_repl_method;
   end if;
   O_repl_method_desc := LP_repl_method_desc;
   ---
   if I_season is NULL then
      O_season_desc := NULL;
   else
      if (LP_season is NULL) or (I_season != LP_season) then
         if SEASON_SQL.GET_SEASON_DESC(O_error_message,
                                       LP_season_desc,
                                       I_season) = FALSE then
            RETURN FALSE;
         end if;
         LP_season := I_season;
      end if;
      O_season_desc := LP_season_desc;
   end if;
   ---
   if I_phase is NULL then
      O_phase_desc := NULL;
   else
      if (LP_phase is NULL) or ((I_season != LP_season) or (I_phase != LP_phase)) then
         if SEASON_SQL.GET_PHASE_DESC(O_error_message,
                                      LP_phase_desc,
                                      I_season,
                                      I_phase) = FALSE then
            RETURN FALSE;
         end if;
         LP_phase := I_phase;
      end if;
      O_phase_desc := LP_phase_desc;
   end if;
   ---
   --- for replenishment attribute view form only
   if I_form_name = 'FM_RPLATTRV' then
      if I_supplier is NULL then
         O_supp_name := NULL;
      else
         if (LP_supplier is NULL) or (I_supplier != LP_supplier) then
            if SUPP_ATTRIB_SQL.GET_SUPP_DESC(O_error_message,
                                             I_supplier,
                                             LP_supp_name) = FALSE then
               RETURN FALSE;
            end if;
            LP_supplier := I_supplier;
         end if;
         O_supp_name := LP_supp_name;
      end if;
      ---
      if I_dept is NULL then
         O_dept_name := NULL;
         O_class_name := NULL;
         O_subclass_name := NULL;
      else
         if (LP_dept is NULL) or (I_dept != LP_dept) then
            if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                        I_dept,
                                        LP_dept_name) = FALSE then
               RETURN FALSE;
            end if;
            LP_dept     := I_dept;
            LP_class    := NULL;
            LP_subclass := NULL;
         end if;
         ---
         O_dept_name := LP_dept_name;
         ---
         if (LP_class is NULL) or (I_class != LP_class) then
            if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                         I_dept,
                                         I_class,
                                         LP_class_name) = FALSE then
               RETURN FALSE;
            end if;
            LP_class    := I_class;
            LP_subclass := NULL;
         end if;
         ---
         O_class_name := LP_class_name;
         ---
         if (LP_subclass is NULL) or (I_subclass != LP_subclass) then
            if SUBCLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                            I_dept,
                                            I_class,
                                            I_subclass,
                                            LP_subclass_name) = FALSE then
               RETURN FALSE;
            end if;
            LP_subclass := I_subclass;
         end if;
         ---
         O_subclass_name := LP_subclass_name;
         ---
      end if;
      ---
      if I_pack is NULL then
         O_pack_name := NULL;
      else
         if (LP_pack is NULL) or (I_pack != LP_pack) then
            if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                        LP_pack_name,
                                        I_pack) = FALSE then
               RETURN FALSE;
            end if;
            LP_pack := I_pack;
         end if;
         O_pack_name := LP_pack_name;
      end if;
      ---

      if I_review_cycle is not NULL then
         if REPLENISHMENT_DAYS_SQL.QUERY_DAY_ITEM(O_error_message,
                                                  I_item,
                                                  I_location,
                                                  LP_sun,
                                                  LP_mon,
                                                  LP_tue,
                                                  LP_wed,
                                                  LP_thu,
                                                  LP_fri,
                                                  LP_sat) = FALSE then
            RETURN FALSE;
         end if;
         O_sun := LP_sun;
         O_mon := LP_mon;
         O_tue := LP_tue;
         O_wed := LP_wed;
         O_thu := LP_thu;
         O_fri := LP_fri;
         O_sat := LP_sat;
      else
         O_sun := 'N';
         O_mon := 'N';
         O_tue := 'N';
         O_wed := 'N';
         O_thu := 'N';
         O_fri := 'N';
         O_sat := 'N';
         LP_sun := 'N';
         LP_mon := 'N';
         LP_tue := 'N';
         LP_wed := 'N';
         LP_thu := 'N';
         LP_fri := 'N';
         LP_sat := 'N';
      end if;

   --- for replenishment results detail and list form
   elsif I_form_name = 'FM_RPLRSLTD' or I_form_name = 'FM_RPLRSLTL' then
      if (LP_loc_type is NULL) or (I_loc_type != LP_loc_type) then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'LOTP',
                                       I_loc_type,
                                       LP_loc_type_desc) = FALSE then
            RETURN FALSE;
         end if;
         LP_loc_type := I_loc_type;
      end if;
      O_loc_type_desc := LP_loc_type_desc;

      ---
      if I_master_item = I_item then
         O_master_item_desc := LP_item_desc;
      else
         if (LP_master_item is NULL) or (I_master_item != LP_master_item) then
            if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                        LP_master_item_desc,
                                        I_master_item) = FALSE then
               RETURN FALSE;
            end if;
            LP_master_item := I_master_item;
         end if;
         O_master_item_desc := LP_master_item_desc;
      end if;
      ---
      if (LP_store_ord_mult is NULL) or (I_store_ord_mult != LP_store_ord_mult) then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'ORM2',
                                       I_store_ord_mult,
                                       LP_store_ord_mult_desc) = FALSE then
            RETURN FALSE;
         end if;
         LP_store_ord_mult := I_store_ord_mult;
      end if;
      O_store_ord_mult_desc := LP_store_ord_mult_desc;
      ---
      if I_order_no is not null then
         if ORDER_ITEM_ATTRIB_SQL.GET_UNITS_COST(O_error_message,
                                                 O_prescaled_qty,
                                                 O_actual_qty,
                                                 O_prescaled_cost,
                                                 O_actual_cost,
                                                 I_order_no,
                                                 I_alloc_no,
                                                 I_item,
                                                 I_location) = FALSE then
           RETURN FALSE;
         end if;
      end if;
      ---
      O_stock_on_hand     := I_stock_on_hand
                           + I_pack_comp_soh;
      O_incoming_stock    := I_in_transit_qty
                           + I_pack_comp_intran
                           + I_tsf_expected_qty
                           + I_pack_comp_exp
                           + I_alloc_in_qty
                           + I_on_order;
      O_outgoing_stock    := I_rtv_qty
                           + I_tsf_resv_qty
                           + I_pack_comp_resv
                           + I_alloc_out_qty;
      O_nonsellable_stock := I_layaway_qty
                           + I_non_sellable_qty;
   end if;
   ---
   --- for replenishment results detail form only
   --- fetch inner, case, pallet description
   if I_form_name = 'FM_RPLRSLTD' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_item_supplier',
                       'item_supplier',
                       'Item: '||I_item||
                       ', Supplier: '||to_char(I_supplier));

      open C_item_supplier;

      SQL_LIB.SET_MARK('FETCH',
                       'C_item_supplier',
                       'item_supplier',
                       'Item: '||I_item||
                       ', Supplier: '||to_char(I_supplier));

      fetch C_item_supplier into L_inner_name,
                                     L_case_name,
                                     L_pallet_name;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_item_supplier',
                       'item_supplier',
                       'Item: '||I_item||
                       ', Supplier: '||to_char(I_supplier));

      close C_item_supplier;
      ---
      if (LP_inner_name is NULL) or (L_inner_name != LP_inner_name) then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'INRN',
                                       L_inner_name,
                                       LP_inner_name_desc) = FALSE then
            RETURN FALSE;
         end if;
         LP_inner_name := L_inner_name;
      end if;
      O_inner_name_desc := LP_inner_name_desc;
      ---
      if (LP_case_name is NULL) or (L_case_name != LP_case_name) then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'CASN',
                                       L_case_name,
                                       LP_case_name_desc) = FALSE then
            RETURN FALSE;
         end if;
         LP_case_name := L_case_name;
      end if;
      O_case_name_desc := LP_case_name_desc;
      ---
      if (LP_pallet_name is NULL) or (L_pallet_name != LP_pallet_name) then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'PALN',
                                       L_pallet_name,
                                       LP_pallet_name_desc) = FALSE then
            RETURN FALSE;
         end if;
         LP_pallet_name := L_pallet_name;
      end if;
      O_pallet_name_desc := LP_pallet_name_desc;
   end if;
   ---

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPLENISHMENT_SQL.GET_FORM_INFO',
                                            to_char(SQLCODE));
   RETURN FALSE;

END GET_FORM_INFO;
---------------------------------------------------------------------------------------------
FUNCTION GET_REPL_ATTRIBUTES (O_error_message             IN OUT   VARCHAR2,
                              O_stock_cat                 IN OUT   REPL_ITEM_LOC.STOCK_CAT%TYPE,
                              O_order_ind                 IN OUT   REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE,
                              O_supplier                  IN OUT   REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE,
                              O_origin_country            IN OUT   REPL_ITEM_LOC.ORIGIN_COUNTRY_ID%TYPE,
                              O_review_cycle              IN OUT   REPL_ITEM_LOC.REVIEW_CYCLE%TYPE,
                              O_activate_date             IN OUT   REPL_ITEM_LOC.ACTIVATE_DATE%TYPE,
                              O_deactivate_date           IN OUT   REPL_ITEM_LOC.DEACTIVATE_DATE%TYPE,
                              O_source_wh                 IN OUT   REPL_ITEM_LOC.SOURCE_WH%TYPE,
                              O_repl_method               IN OUT   REPL_ITEM_LOC.REPL_METHOD%TYPE,
                              O_pres_stock                IN OUT   REPL_ITEM_LOC.PRES_STOCK%TYPE,
                              O_demo_stock                IN OUT   REPL_ITEM_LOC.DEMO_STOCK%TYPE,
                              O_min_stock                 IN OUT   REPL_ITEM_LOC.MIN_STOCK%TYPE,
                              O_max_stock                 IN OUT   REPL_ITEM_LOC.MAX_STOCK%TYPE,
                              O_incr_pct                  IN OUT   REPL_ITEM_LOC.INCR_PCT%TYPE,
                              O_min_supply_days           IN OUT   REPL_ITEM_LOC.MIN_SUPPLY_DAYS%TYPE,
                              O_max_supply_days           IN OUT   REPL_ITEM_LOC.MAX_SUPPLY_DAYS%TYPE,
                              O_time_supply_horizon       IN OUT   REPL_ITEM_LOC.TIME_SUPPLY_HORIZON%TYPE,
                              O_inv_selling_days          IN OUT   REPL_ITEM_LOC.INV_SELLING_DAYS%TYPE,
                              O_service_level             IN OUT   REPL_ITEM_LOC.SERVICE_LEVEL%TYPE,
                              O_lost_sales_factor         IN OUT   REPL_ITEM_LOC.LOST_SALES_FACTOR%TYPE,
                              O_non_scaling_ind           IN OUT   REPL_ITEM_LOC.NON_SCALING_IND%TYPE,
                              O_max_scale_value           IN OUT   REPL_ITEM_LOC.MAX_SCALE_VALUE%TYPE,
                              O_pickup_lead_time          IN OUT   REPL_ITEM_LOC.PICKUP_LEAD_TIME%TYPE,
                              O_wh_lead_time              IN OUT   REPL_ITEM_LOC.WH_LEAD_TIME%TYPE,
                              O_terminal_stock_qty        IN OUT   REPL_ITEM_LOC.TERMINAL_STOCK_QTY%TYPE,
                              O_season_id                 IN OUT   REPL_ITEM_LOC.SEASON_ID%TYPE,
                              O_phase_id                  IN OUT   REPL_ITEM_LOC.PHASE_ID%TYPE,
                              O_use_tolerance_ind         IN OUT   REPL_ITEM_LOC.USE_TOLERANCE_IND%TYPE,
                              O_unit_tolerance            IN OUT   REPL_ITEM_LOC.UNIT_TOLERANCE%TYPE,
                              O_pct_tolerance             IN OUT   REPL_ITEM_LOC.PCT_TOLERANCE%TYPE,
                              O_primary_pack_no           IN OUT   REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE,
                              O_reject_store_ord_ind      IN OUT   REPL_ITEM_LOC.REJECT_STORE_ORD_IND%TYPE,
                              O_service_level_type        IN OUT   REPL_ATTR_UPDATE_HEAD.SERVICE_LEVEL_TYPE%TYPE,
                              O_mra_update                IN OUT   REPL_ATTR_UPDATE_HEAD.MRA_UPDATE%TYPE,
                              O_mra_restore               IN OUT   REPL_ATTR_UPDATE_HEAD.MRA_RESTORE%TYPE,
                              O_Create_id                 IN OUT   REPL_ATTR_UPDATE_HEAD.CREATE_ID%TYPE,
                              O_Create_date               IN OUT   REPL_ATTR_UPDATE_HEAD.CREATE_DATE%TYPE,
                              O_Scheduled_Desc            IN OUT   REPL_ATTR_UPDATE_HEAD.SCH_RPL_DESC%TYPE,
                              O_stock_cat_exists          IN OUT   BOOLEAN,
                              O_item                      IN OUT   REPL_ATTR_UPDATE_ITEM.ITEM%TYPE,
                              O_size_profile_ind          IN OUT   REPL_ATTR_UPDATE_HEAD.SIZE_PROFILE_IND%TYPE,
                              O_dept                      IN OUT   REPL_ATTR_UPDATE_ITEM.DEPT%TYPE,
                              O_class                     IN OUT   REPL_ATTR_UPDATE_ITEM.CLASS%TYPE,
                              O_subclass                  IN OUT   REPL_ATTR_UPDATE_ITEM.SUBCLASS%TYPE,
                              O_diff_1                    IN OUT   REPL_ATTR_UPDATE_ITEM.DIFF_1%TYPE,
                              O_diff_2                    IN OUT   REPL_ATTR_UPDATE_ITEM.DIFF_2%TYPE,
                              O_diff_3                    IN OUT   REPL_ATTR_UPDATE_ITEM.DIFF_3%TYPE,
                              O_diff_4                    IN OUT   REPL_ATTR_UPDATE_ITEM.DIFF_4%TYPE,
                              O_add_lead_time_ind         IN OUT   REPL_ATTR_UPDATE_HEAD.ADD_LEAD_TIME_IND%TYPE,
                              O_multiple_runs_per_day_ind IN OUT   REPL_ITEM_LOC.MULT_RUNS_PER_DAY_IND%TYPE,
                              O_transfers_zero_soh_ind    IN OUT   REPL_ITEM_LOC.TSF_ZERO_SOH_IND%TYPE,
                              I_repl_id                   IN       REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE,
                              I_scheduled_active_date     IN       REPL_ATTR_UPDATE_HEAD.SCHEDULED_ACTIVE_DATE%TYPE,
                              I_update_from_mra           IN       VARCHAR2,
                              I_item                      IN       REPL_ITEM_LOC.ITEM%TYPE,
                              I_loc                       IN       REPL_ITEM_LOC.LOCATION%TYPE) RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'REPLENISHMENT_SQL.GET_REPL_ATTRIBUTES';
   cursor C_REPL_ITEM_LOC is
      select stock_cat,
             repl_order_ctrl,
             primary_repl_supplier,
             origin_country_id,
             review_cycle,
             activate_date,
             deactivate_date,
             source_wh,
             repl_method,
             pres_stock,
             demo_stock,
             min_stock,
             max_stock,
             incr_pct,
             min_supply_days,
             max_supply_days,
             time_supply_horizon,
             inv_selling_days,
             service_level,
             lost_sales_factor,
             non_scaling_ind,
             max_scale_value,
             pickup_lead_time,
             wh_lead_time,
             terminal_stock_qty,
             season_id,
             phase_id,
             use_tolerance_ind,
             unit_tolerance,
             pct_tolerance,
             primary_pack_no,
             reject_store_ord_ind,
             service_level_type,
             mult_runs_per_day_ind,
             tsf_zero_soh_ind,
             add_lead_time_ind
        from repl_item_loc
       where repl_item_loc.item     = I_item
         and repl_item_loc.location = I_loc;

   cursor C_MASTER_REPL_ATTR_LOC is
      select stock_cat,
             repl_order_ctrl,
             primary_repl_supplier,
             origin_country_id,
             review_cycle,
             source_wh,
             repl_method,
             pres_stock,
             demo_stock,
             min_stock,
             max_stock,
             incr_pct,
             min_supply_days,
             max_supply_days,
             time_supply_horizon,
             inv_selling_days,
             service_level,
             lost_sales_factor,
             non_scaling_ind,
             max_scale_value,
             pickup_lead_time,
             wh_lead_time,
             terminal_stock_qty,
             season_id,
             phase_id,
             use_tolerance_ind,
             unit_tolerance,
             pct_tolerance,
             primary_pack_no,
             reject_store_ord_ind,
             service_level_type,
             create_datetime,
             mult_runs_per_day_ind,
             tsf_zero_soh_ind
        from master_repl_attr
       where master_repl_attr.item     = I_item
         and master_repl_attr.location = I_loc;

   cursor C_REPL_UPDATE_ATTRIB_HEAD is
      select stock_cat,
             repl_order_ctrl,
             supplier,
             origin_country_id,
             review_cycle,
             activate_date,
             deactivate_date,
             sourcing_wh,
             repl_method,
             pres_stock,
             demo_stock,
             min_stock,
             max_stock,
             incr_pct,
             min_supply_days,
             max_supply_days,
             time_supply_horizon,
             inv_selling_days,
             service_level,
             lost_sales_factor,
             non_scaling_ind,
             max_scale_value,
             pickup_lead_time,
             wh_lead_time,
             terminal_stock_qty,
             season_id,
             phase_id,
             use_tolerance_ind,
             unit_tolerance,
             pct_tolerance,
             reject_store_ord_ind,
             service_level_type,
             mra_update,
             mra_restore,
             Create_id,
             Create_date,
             sch_rpl_desc,
             size_profile_ind,
             mult_runs_per_day_ind,
             tsf_zero_soh_ind,
             add_lead_time_ind
        from repl_attr_update_head
       where repl_attr_id = I_repl_id;

   cursor C_REPL_UPDATE_ATTRIB_ITEM is
      select item,
             dept,
             class,
             subclass,
             diff_1,
             diff_2,
             diff_3,
             diff_4
        from repl_attr_update_item
       where repl_attr_id = I_repl_id;

BEGIN
   if I_scheduled_active_date is NULL and I_update_from_mra = 'N' then

      SQL_LIB.SET_MARK('OPEN','C_REPL_ITEM_LOC','REPL_ITEM_LOC','ITEM: '||I_item||' LOCATION: '||I_loc );
      open C_REPL_ITEM_LOC;

      SQL_LIB.SET_MARK('FETCH','C_REPL_ITEM_LOC','REPL_ITEM_LOC','ITEM: '||I_item||' LOCATION: '||I_loc );
      fetch C_REPL_ITEM_LOC into O_stock_cat,
                                 O_order_ind,
                                 O_supplier,
                                 O_origin_country,
                                 O_review_cycle,
                                 O_activate_date,
                                 O_deactivate_date,
                                 O_source_wh,
                                 O_repl_method,
                                 O_pres_stock,
                                 O_demo_stock,
                                 O_min_stock,
                                 O_max_stock,
                                 O_incr_pct,
                                 O_min_supply_days,
                                 O_max_supply_days,
                                 O_time_supply_horizon,
                                 O_inv_selling_days,
                                 O_service_level,
                                 O_lost_sales_factor,
                                 O_non_scaling_ind,
                                 O_max_scale_value,
                                 O_pickup_lead_time,
                                 O_wh_lead_time,
                                 O_terminal_stock_qty,
                                 O_season_id,
                                 O_phase_id,
                                 O_use_tolerance_ind,
                                 O_unit_tolerance,
                                 O_pct_tolerance,
                                 O_primary_pack_no,
                                 O_reject_store_ord_ind,
                                 O_service_level_type,
                                 O_multiple_runs_per_day_ind,
                                 O_transfers_zero_soh_ind,
                                 O_add_lead_time_ind;
      if O_stock_cat is NULL then
         O_stock_cat_exists:= FALSE;
      else
         O_stock_cat_exists:= TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_REPL_ITEM_LOC','REPL_ITEM_LOC','ITEM: '||I_item||' LOCATION: '||I_loc );
      close C_REPL_ITEM_LOC;

   elsif I_scheduled_active_date is NULL and I_update_from_mra = 'Y' then
      SQL_LIB.SET_MARK('OPEN', 'C_MASTER_REPL_ATTR_LOC ', 'master_repl_item', 'Item: '||I_item||', Location: '||to_char(I_loc));
      open C_MASTER_REPL_ATTR_LOC;
      SQL_LIB.SET_MARK('FETCH', 'C_MASTER_REPL_ATTR_LOC ', 'master_repl_item', 'Item: '||I_item||', Location: '||to_char(I_loc));
      fetch C_MASTER_REPL_ATTR_LOC into O_stock_cat,
                                        O_order_ind,
                                        O_supplier,
                                        O_origin_country,
                                        O_review_cycle,
                                        O_source_wh,
                                        O_repl_method,
                                        O_pres_stock,
                                        O_demo_stock,
                                        O_min_stock,
                                        O_max_stock,
                                        O_incr_pct,
                                        O_min_supply_days,
                                        O_max_supply_days,
                                        O_time_supply_horizon,
                                        O_inv_selling_days,
                                        O_service_level,
                                        O_lost_sales_factor,
                                        O_non_scaling_ind,
                                        O_max_scale_value,
                                        O_pickup_lead_time,
                                        O_wh_lead_time,
                                        O_terminal_stock_qty,
                                        O_season_id,
                                        O_phase_id,
                                        O_use_tolerance_ind,
                                        O_unit_tolerance,
                                        O_pct_tolerance,
                                        O_primary_pack_no,
                                        O_reject_store_ord_ind,
                                        O_service_level_type,
                                        O_Create_date,
                                        O_multiple_runs_per_day_ind,
                                        O_transfers_zero_soh_ind;
      SQL_LIB.SET_MARK('CLOSE', 'C_master_repl_attr_LOC ', 'master_repl_item', 'Item: '||I_item||', Location: '||to_char(I_loc));
      close C_MASTER_REPL_ATTR_LOC;
      if O_stock_cat is NULL then
         O_stock_cat_exists:= FALSE;
      else
         O_stock_cat_exists:= TRUE;
      end if;

   elsif I_scheduled_active_date is NOT NULL and I_update_from_mra = 'N' and I_repl_id is NOT NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_REPL_UPDATE_ATTRIB_HEAD ', 'repl_attr_update_head', 'repl_id: '||I_repl_id);
      open C_REPL_UPDATE_ATTRIB_HEAD;
      SQL_LIB.SET_MARK('FETCH', 'C_REPL_UPDATE_ATTRIB_HEAD ', 'repl_attr_update_head', 'repl_id: '||I_repl_id);
      fetch C_REPL_UPDATE_ATTRIB_HEAD into O_stock_cat,
                                           O_order_ind,
                                           O_supplier,
                                           O_origin_country,
                                           O_review_cycle,
                                           O_activate_date,
                                           O_deactivate_date,
                                           O_source_wh,
                                           O_repl_method,
                                           O_pres_stock,
                                           O_demo_stock,
                                           O_min_stock,
                                           O_max_stock,
                                           O_incr_pct,
                                           O_min_supply_days,
                                           O_max_supply_days,
                                           O_time_supply_horizon,
                                           O_inv_selling_days,
                                           O_service_level,
                                           O_lost_sales_factor,
                                           O_non_scaling_ind,
                                           O_max_scale_value,
                                           O_pickup_lead_time,
                                           O_wh_lead_time,
                                           O_terminal_stock_qty,
                                           O_season_id,
                                           O_phase_id,
                                           O_use_tolerance_ind,
                                           O_unit_tolerance,
                                           O_pct_tolerance,
                                           O_reject_store_ord_ind,
                                           O_service_level_type,
                                           O_mra_update,
                                           O_mra_restore,
                                           O_Create_id,
                                           O_Create_date,
                                           O_Scheduled_Desc,
                                           O_size_profile_ind,
                                           O_multiple_runs_per_day_ind,
                                           O_transfers_zero_soh_ind,
                                           O_add_lead_time_ind;
      SQL_LIB.SET_MARK('CLOSE', 'C_REPL_UPDATE_ATTRIB_HEAD ', 'repl_attr_update_head', 'repl_id: '||I_repl_id);
      close C_REPL_UPDATE_ATTRIB_HEAD;
      if O_stock_cat is NULL then
         O_stock_cat_exists:= FALSE;
      else
         O_stock_cat_exists:= TRUE;
      end if;

      -- Fetch the Item and hierarchy details
      SQL_LIB.SET_MARK('OPEN', 'C_REPL_UPDATE_ATTRIB_ITEM ', 'repl_attr_update_item', 'repl_id: '||I_repl_id);
      open C_REPL_UPDATE_ATTRIB_ITEM;
      SQL_LIB.SET_MARK('FETCH', 'C_REPL_UPDATE_ATTRIB_ITEM ', 'repl_attr_update_item', 'repl_id: '||I_repl_id);
      fetch C_REPL_UPDATE_ATTRIB_ITEM into O_item,
                                           O_dept,
                                           O_class,
                                           O_subclass,
                                           O_diff_1,
                                           O_diff_2,
                                           O_diff_3,
                                           O_diff_4;
      SQL_LIB.SET_MARK('CLOSE', 'C_REPL_UPDATE_ATTRIB_ITEM ', 'repl_attr_update_item', 'repl_id: '||I_repl_id);
      close C_REPL_UPDATE_ATTRIB_ITEM;

   else
      O_stock_cat            := NULL;
      O_order_ind            := NULL;
      O_supplier             := NULL;
      O_origin_country       := NULL;
      O_review_cycle         := NULL;
      O_activate_date        := NULL;
      O_deactivate_date      := NULL;
      O_source_wh            := NULL;
      O_repl_method          := NULL;
      O_pres_stock           := NULL;
      O_demo_stock           := NULL;
      O_min_stock            := NULL;
      O_max_stock            := NULL;
      O_incr_pct             := NULL;
      O_min_supply_days      := NULL;
      O_max_supply_days      := NULL;
      O_time_supply_horizon  := NULL;
      O_inv_selling_days     := NULL;
      O_service_level        := NULL;
      O_lost_sales_factor    := NULL;
      O_non_scaling_ind      := NULL;
      O_max_scale_value      := NULL;
      O_pickup_lead_time     := NULL;
      O_wh_lead_time         := NULL;
      O_terminal_stock_qty   := NULL;
      O_season_id            := NULL;
      O_phase_id             := NULL;
      O_use_tolerance_ind    := NULL;
      O_unit_tolerance       := NULL;
      O_pct_tolerance        := NULL;
      O_primary_pack_no      := NULL;
      O_reject_store_ord_ind := NULL;
      O_service_level_type   := NULL;
      O_mra_update           := NULL;
      O_mra_restore          := NULL;
      O_Create_id            := NULL;
      O_Create_date          := NULL;
      O_Scheduled_Desc       := NULL;
      O_stock_cat_exists     := NULL;
      O_Item                 := NULL;
      O_dept                 := NULL;
      O_class                := NULL;
      O_subclass             := NULL;
      O_diff_1               := NULL;
      O_diff_2               := NULL;
      O_diff_3               := NULL;
      O_diff_4               := NULL;
      O_multiple_runs_per_day_ind := NULL;
      O_transfers_zero_soh_ind    := NULL;
      O_add_lead_time_ind         := NULL;

      O_error_message := SQL_LIB.CREATE_MSG('INVALID_DATA_COMBINATION', 'I_scheduled_active_date,I_update_from_mra,I_repl_id', L_program , NULL );
      return FALSE;
   end if;

   -- The output variables should be set to NULL if they are not being fetched into
   O_create_id      :=  nvl(O_create_id,NULL);
   O_create_date    :=  nvl(O_create_date,NULL);
   O_scheduled_desc :=  nvl(O_scheduled_desc,NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG
                        ('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;
END GET_REPL_ATTRIBUTES;
---------------------------------------------------------------------------------------------
FUNCTION ON_REPLENISHMENT (O_error_message IN OUT VARCHAR2,
                           I_item          IN     item_master.item%TYPE,
                           O_on_repl       IN OUT BOOLEAN)
RETURN BOOLEAN IS

   cursor C_ITEM is
   select 'x'
     from ( select   im1.item
              from   item_master im1
             where   im1.item = I_item
            union
            select   im2.item
              from   item_master im2
             where   im2.item_parent = I_item
            union
            select   im3.item
              from   item_master im3
             where   im3.item_grandparent = I_item ) im
    where exists
          (  select   'x'
               from   repl_item_loc ril
              where   ril.item = im.item
                and   ril.stock_cat is not null)
      and rownum = 1;

   L_repl_ind  VARCHAR2(1);

BEGIN

   O_on_repl := FALSE;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_item', 'NULL', 'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_ITEM', 'REPL_ITEM_LOC', 'Item: ' ||I_item);
   open C_ITEM;

   SQL_LIB.SET_MARK('FETCH', 'C_ITEM', 'REPL_ITEM_LOC', 'Item: ' ||I_item);
   fetch C_ITEM into L_repl_ind;

   if C_ITEM%FOUND then
      O_on_repl := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM', 'REPL_ITEM_LOC', 'Item: ' ||I_item);
   close C_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPLENISHMENT_SQL.ON_REPLENISHMENT',
                                            to_char(SQLCODE));
      RETURN FALSE;
END ON_REPLENISHMENT;
----------------------------------------------------------------------------
FUNCTION FORECAST_REPL_METHOD (O_error_message         IN OUT VARCHAR2,
                               I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                               O_forecast_repl_method  IN OUT BOOLEAN)

RETURN BOOLEAN IS

   cursor C_ITEM is
     select 'x'
       from repl_item_loc
      where (item = I_item or
             item_parent = I_item or
             item_grandparent = I_item)
        and repl_method in ('T', 'D', 'TI', 'DI');

   L_forecast_ind  VARCHAR2(1);

BEGIN

   O_forecast_repl_method := FALSE;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_item', 'NULL', 'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_ITEM', 'REPL_ITEM_LOC', 'Item: ' ||I_item);
   open C_ITEM;

   SQL_LIB.SET_MARK('FETCH', 'C_ITEM', 'REPL_ITEM_LOC', 'Item: ' ||I_item);
   fetch C_ITEM into L_forecast_ind;

   if C_ITEM%FOUND then
      O_forecast_repl_method := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM', 'REPL_ITEM_LOC', 'Item: ' ||I_item);
   close C_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPLENISHMENT_SQL.FORECAST_REPL_METHOD',
                                            to_char(SQLCODE));
      RETURN FALSE;
END FORECAST_REPL_METHOD;
-------------------------------------------------------------------------------------
FUNCTION SEASONAL_REPL_CHECK(O_error_message   IN OUT VARCHAR2,
                             O_seasonal_ind    IN OUT VARCHAR2,
                             I_item            IN     repl_item_loc.item%TYPE,
                             I_loc             IN     repl_item_loc.location%TYPE)
RETURN BOOLEAN IS

   L_item              item_master.item%TYPE           := I_item;
   L_loc               store.store%TYPE                := I_loc;
   L_dummy             VARCHAR2(1);

   cursor C_CHECK_SEASONAL_REPL is
      select 'x'
        from repl_item_loc
       where item = L_item
         and location = nvl(L_loc,location)
         and phase_id is NOT NULL;

BEGIN

   open  C_CHECK_SEASONAL_REPL;
   fetch C_CHECK_SEASONAL_REPL into L_dummy;
   if C_CHECK_SEASONAL_REPL%FOUND then
      O_seasonal_ind := 'Y';
   else
      O_seasonal_ind := 'N';
   end if;
   close C_CHECK_SEASONAL_REPL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPLENISHMENT_SQL.SEASONAL_REPL_CHECK',
                                            to_char(SQLCODE));
   RETURN FALSE;

END SEASONAL_REPL_CHECK;
-------------------------------------------------------------------------------------
FUNCTION GET_TOLERANCES(O_error_message    IN OUT VARCHAR2,
                        O_pct_tolerance    IN OUT repl_item_loc.pct_tolerance%TYPE,
                        O_unit_tolerance   IN OUT repl_item_loc.unit_tolerance%TYPE,
                        I_item             IN     repl_item_loc.item%TYPE,
                        I_location         IN     repl_item_loc.location%TYPE)
RETURN BOOLEAN is

    cursor C_GET_TOLERANCES is
       select pct_tolerance,
              unit_tolerance
         from repl_item_loc
        where item = I_item
          and location = I_location;
BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_TOLERANCES', 'REPL_ITEM_LOC', I_item);
   open C_GET_TOLERANCES;
   SQL_LIB.SET_MARK('FETCH','C_GET_TOLERANCES', 'REPL_ITEM_LOC', I_item);
   fetch C_GET_TOLERANCES into O_pct_tolerance, O_unit_tolerance;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TOLERANCES', 'REPL_ITEM_LOC', I_item);
   close C_GET_TOLERANCES;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPLENISHMENT_SQL.GET_TOLERANCES',
                                            to_char(SQLCODE));
      RETURN FALSE;



END GET_TOLERANCES;
------------------------------------------------------------------------------------------
FUNCTION NEXT_ORD_TEMP_SEQ_NO(O_error_message     IN OUT    VARCHAR2,
                              O_ord_temp_seq_no   IN OUT    ORD_TEMP.ORD_TEMP_SEQ_NO%TYPE)
RETURN BOOLEAN is

   L_program   VARCHAR2(64) := 'REPLENISHMENT_SQL.NEXT_ORD_TEMP_SEQ_NO';

   cursor C_NEXTVAL is
      select ord_temp_sequence.NEXTVAL
        from dual;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_NEXTVAL', 'ord_temp_sequence', NULL);
   open C_NEXTVAL;

   SQL_LIB.SET_MARK('FETCH', 'C_NEXTVAL', 'ord_temp_sequence', NULL);
   fetch C_NEXTVAL into O_ord_temp_seq_no;

   if C_NEXTVAL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_MORE_ORD_TEMP_SEQUENCE',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_NEXTVAL', 'ord_temp_sequence', NULL);
      close C_NEXTVAL;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_NEXTVAL', 'ord_temp_sequence', NULL);
   close C_NEXTVAL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;

END NEXT_ORD_TEMP_SEQ_NO;
------------------------------------------------------------------------------------------
FUNCTION WH_EXISTS_AS_SOURCE(O_error_message       OUT    VARCHAR2,
                             O_exists_as_source    OUT    BOOLEAN,
                             I_item                IN     ITEM_MASTER.ITEM%TYPE,
                             I_wh                  IN     WH.WH%TYPE)
   RETURN BOOLEAN is

   L_program   VARCHAR2(64) := 'REPLENISHMENT_SQL.WH_EXISTS_AS_SOURCE';
   L_dummy     VARCHAR2(1);
   L_current_date       period.vdate%TYPE  := GET_VDATE;
   
   cursor C_EXISTS_AS_SOURCE is
      select 'x'
        from repl_item_loc
       where item = I_item
         and source_wh = I_wh

         and stock_cat in ('C','L')
		 and (deactivate_date > L_current_date
	     or deactivate_date is null);
BEGIN

   open C_EXISTS_AS_SOURCE;
   fetch C_EXISTS_AS_SOURCE into L_dummy;
   O_exists_as_source := C_EXISTS_AS_SOURCE%FOUND;
   close C_EXISTS_AS_SOURCE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END WH_EXISTS_AS_SOURCE;
-----------------------------------------------------------------------------------------------
FUNCTION REPL_METHOD(O_error_message     IN OUT     VARCHAR,
                     O_repl_method       IN OUT     REPL_RESULTS.REPL_METHOD%TYPE,
                     I_item              IN         REPL_RESULTS.ITEM%TYPE,
                     I_location          IN         REPL_RESULTS.LOCATION%TYPE)
   return BOOLEAN IS

   cursor C_REPL_METHOD is
      select repl_method
        from repl_results
       where item = I_item
         and location = I_location;

BEGIN
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_REPL_METHOD', 'repl_results', 'Item: '||I_item||', Location: '||to_char(I_location));
   open C_REPL_METHOD;
   SQL_LIB.SET_MARK('FETCH', 'C_REPL_METHOD', 'repl_results', 'Item: '||I_item||', Location: '||to_char(I_location));
   fetch C_REPL_METHOD into O_repl_method;
   SQL_LIB.SET_MARK('CLOSE', 'C_REPL_METHOD', 'repl_results', 'Item: '||I_item||', Location: '||to_char(I_location));
   close C_REPL_METHOD;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              'REPLENISHMENT_SQL.REPL_METHOD',
                                              to_char(SQLCODE));
   return FALSE;

END REPL_METHOD;
-----------------------------------------------------------------------------------------------
FUNCTION IB_LINE_ITEM(O_error_message     IN OUT     VARCHAR,
                      O_exists            IN OUT     VARCHAR,
                      I_order_no          IN         REPL_RESULTS.ORDER_NO%TYPE,
                      I_item              IN         REPL_RESULTS.ITEM%TYPE,
                      I_location          IN         REPL_RESULTS.LOCATION%TYPE)
   return BOOLEAN IS

   cursor C_IB_LINE_ITEM is
      select 'Y'
        from ib_results ib
       where ib.order_no = I_order_no
         and ib.item = I_item
         and (ib.location = I_location or ib.repl_wh_link = I_location);

BEGIN
   ---
   O_exists := 'N';
   ---
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_IB_LINE_ITEM', 'repl_results', 'Order: '||I_order_no);
   open C_IB_LINE_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_IB_LINE_ITEM', 'repl_results', 'Order: '||I_order_no);
   fetch C_IB_LINE_ITEM into O_exists;
   SQL_LIB.SET_MARK('CLOSE', 'C_IB_LINE_ITEM', 'repl_results', 'Order: '||I_order_no);
   close C_IB_LINE_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              'REPLENISHMENT_SQL.IB_LINE_ITEM',
                                              to_char(SQLCODE));
   return FALSE;

END IB_LINE_ITEM;
------------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item              IN       REPL_ITEM_LOC.ITEM%TYPE,
                    I_location          IN       REPL_ITEM_LOC.LOCATION%TYPE,
                    I_function_key      IN       VARCHAR2,
                    I_seq_no            IN       NUMBER)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(75)   := 'REPLENISHMENT_SQL.CUSTOM_VAL';
   L_custom_obj_rec      CUSTOM_OBJ_REC :=  CUSTOM_OBJ_REC();

BEGIN

   L_custom_obj_rec.function_key:= I_function_key;
   L_custom_obj_rec.call_seq_no:= I_seq_no;
   L_custom_obj_rec.item:= I_item;
   L_custom_obj_rec.loc:= I_location;


   if CALL_CUSTOM_SQL.EXEC_FUNCTION(O_error_message,
                                    L_custom_obj_rec) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CUSTOM_VAL;
--------------------------------------------------------------------------------------------
END;
/
