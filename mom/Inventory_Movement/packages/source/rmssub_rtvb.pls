CREATE OR REPLACE PACKAGE BODY RMSSUB_RTV AS
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_RTV(O_error_message         OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_rtv_record            OUT   RTV_SQL.RTV_RECORD,
                   O_message            IN       RIB_OBJECT)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_RTV(O_error_message     IN OUT  VARCHAR2,
                     I_rtv_record        IN      RTV_SQL.RTV_RECORD)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEMS(O_error_message   IN OUT   VARCHAR2,
                     O_sellable_TBL    OUT      "RIB_RTVDtl_TBL",
                     O_detail_TBL      OUT      "RIB_RTVDtl_TBL",
                     O_im_row_TBL      OUT      RTV_SQL.ITEM_MASTER_TBL,
                     I_rib_detail_TBL  IN       "RIB_RTVDtl_TBL")
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ORDERABLE_ITEMS(O_error_message   IN OUT   VARCHAR2,
                             O_orderable_TBL   IN OUT   "RIB_RTVDtl_TBL",
                             I_sellable_TBL    IN       "RIB_RTVDtl_TBL",
                             I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE,
                             I_ext_ref_no      IN       RTV_HEAD.EXT_REF_NO%TYPE,
                             I_location        IN       ITEM_LOC.LOC%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CONSUME_RTV_CORE
-- Purpose: This private function contains the core logic of RTV consume process.
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME_RTV_CORE(O_error_message     IN OUT   VARCHAR2,
                          I_message           IN       RIB_OBJECT,
                          I_message_type      IN       VARCHAR2)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION --
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE --
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code       IN OUT   VARCHAR2,
                  O_error_message     IN OUT   VARCHAR2,
                  I_message           IN       RIB_OBJECT,
                  I_message_type      IN       VARCHAR2)
IS
   L_program        VARCHAR2(255) := 'RMSSUB_RTV.CONSUME';

   L_check_l10n_ind VARCHAR2(1) := 'Y';

   PROGRAM_ERROR  EXCEPTION;

BEGIN

   O_STATUS_CODE := API_CODES.SUCCESS;

   -- Perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Check message type
   if I_message_type is NULL or LOWER(I_message_type) != LP_cre_type then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;

   CONSUME_RTV(O_status_code,
               O_error_message,
               I_message,
               I_message_type,
               L_check_l10n_ind);  -- 'Y'

   if O_status_code != API_CODES.SUCCESS then
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END CONSUME;
-------------------------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE --
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME_RTV(O_status_code       IN OUT   VARCHAR2,
                      O_error_message     IN OUT   VARCHAR2,
                      I_message           IN       RIB_OBJECT,
                      I_message_type      IN       VARCHAR2,
                      I_check_l10n_ind    IN       VARCHAR2)
IS
   L_program         VARCHAR2(255)      := 'RMSSUB_RTV.CONSUME_RTV';

   L_rtvdesc_rec     "RIB_RTVDesc_REC";
   L_l10n_rib_rec    "L10N_RIB_REC"     := L10N_RIB_REC();
   L_l10n_obj        L10N_OBJ;

   PROGRAM_ERROR     EXCEPTION;

BEGIN
   --
   L_l10n_rib_rec.rib_msg         := I_message;
   L_l10n_rib_rec.rib_msg_type    := I_message_type;
   L_l10n_rib_rec.procedure_key   := 'CONSUME_RTV';
   --
   if I_message_type is NULL or LOWER(I_message_type) != LP_cre_type then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;
   ---
   if I_check_l10n_ind = 'Y' then
      --
      --- This code below is to find out the info need to find the countries in L10N_SQL.EXEC_FUNCTION
      ---
      L_rtvdesc_rec := treat(I_message as "RIB_RTVDesc_REC");
      --
      if L_rtvdesc_rec is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;
      --
      L_l10n_rib_rec.doc_type        := 'RTV';
      L_l10n_rib_rec.doc_id          := L_rtvdesc_rec.rtv_order_no;
      L_l10n_rib_rec.source_id       := L_rtvdesc_rec.dc_dest_id;
      L_l10n_rib_rec.source_type     := NULL;
      L_l10n_rib_rec.source_entity   := 'LOC';
      L_l10n_rib_rec.dest_id         := L_rtvdesc_rec.vendor_nbr;
      L_l10n_rib_rec.dest_type       := 'E';
      L_l10n_rib_rec.dest_entity     := 'SUPP';
      --
      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                 L_l10n_rib_rec)= FALSE then
         raise PROGRAM_ERROR;
      end if;
      --
   else
      -- When Localization Indicator is other than 'Y'.
      --- This is for the base functionality and also will be called for the country which is not localized.

      if RMSSUB_RTV.CONSUME_RTV(O_error_message,
                                L_l10n_rib_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      --
   end if;
   --
   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END CONSUME_RTV;
-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION --
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME_RTV(O_error_message     IN OUT  VARCHAR2,
                     IO_L10N_RIB_REC     IN OUT  L10N_OBJ)
RETURN BOOLEAN IS

   L_program         VARCHAR2(255) := 'RMSSUB_RTV.CONSUME_RTV';
   L_l10n_rib_rec    "L10N_RIB_REC" := L10N_RIB_REC();

BEGIN
   --
   L_l10n_rib_rec := treat (IO_L10N_RIB_REC as L10N_RIB_REC);
   if CONSUME_RTV_CORE(O_error_message,
                       L_l10n_rib_rec.rib_msg,
                       L_l10n_rib_rec.rib_msg_type) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONSUME_RTV;
-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION --
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME_RTV_CORE(O_error_message     IN OUT  VARCHAR2,
                          I_message           IN      RIB_OBJECT,
                          I_message_type      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(255) := 'RMSSUB_ASNOUT.CONSUME_RTV_CORE';

   L_rtv_record  RTV_SQL.RTV_RECORD;

BEGIN
   --
   if PARSE_RTV(O_error_message, 
                L_rtv_record, 
                I_message) = FALSE then
      return FALSE;
   end if;

   if PROCESS_RTV(O_error_message,
                  L_rtv_record) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONSUME_RTV_CORE;
-----------------------------------------------------------------------------------------
FUNCTION PARSE_RTV(O_error_message         OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   O_rtv_record            OUT  RTV_SQL.RTV_RECORD,
                   O_message            IN      RIB_OBJECT)
RETURN BOOLEAN IS

   L_rtvdesc_rec     "RIB_RTVDesc_REC";

   L_ribdtl_rec      "RIB_RTVDtl_REC";
   L_dtl_rec         RTV_SQL.RTV_DETAIL_REC;
   L_rib_dtl_TBL     "RIB_RTVDtl_TBL" := "RIB_RTVDtl_TBL"();
   L_rib_dtl_rec     "RIB_RTVDtl_REC";

   L_detail_TBL        "RIB_RTVDtl_TBL";
   L_sellable_TBL      "RIB_RTVDtl_TBL";
   L_orderable_TBL     "RIB_RTVDtl_TBL" := "RIB_RTVDtl_TBL"();
   L_im_row_TBL        RTV_SQL.ITEM_MASTER_TBL := RTV_SQL.ITEM_MASTER_TBL();

   L_dtl_count          NUMBER := 0;
   L_return_allowed     VARCHAR2(1) := NULL;
   ---
   L_add_1        RTV_HEAD.SHIP_TO_ADD_1%TYPE              := NULL;
   L_add_2        RTV_HEAD.SHIP_TO_ADD_2%TYPE              := NULL;
   L_add_3        RTV_HEAD.SHIP_TO_ADD_3%TYPE              := NULL;
   L_city         RTV_HEAD.SHIP_TO_CITY%TYPE               := NULL;
   L_state        RTV_HEAD.STATE%TYPE                      := NULL;
   L_country      RTV_HEAD.SHIP_TO_COUNTRY_ID%TYPE         := NULL;
   L_post         RTV_HEAD.SHIP_TO_PCODE%TYPE              := NULL;
   L_juris_code   RTV_HEAD.SHIP_TO_JURISDICTION_CODE%TYPE  := NULL;   

   cursor C_GET_JURIS_CODE is
      select jurisdiction_code
        from addr
       where module      = 'SUPP'
         and key_value_1 = L_rtvdesc_rec.vendor_nbr
         and add_1       = L_rtvdesc_rec.ship_address1
         and city        = L_rtvdesc_rec.city
         and country_id  = L_rtvdesc_rec.country
         and NVL(add_2,'-999') = NVL(L_rtvdesc_rec.ship_address2, '-999')
         and NVL(add_3,'-999') = NVL(L_rtvdesc_rec.ship_address3, '-999')
         and NVL(state,'-999') = NVL(L_rtvdesc_rec.state,         '-999')
         and NVL(post, '-999') = NVL(L_rtvdesc_rec.shipto_zip,    '-999')
         and rownum = 1;

   cursor C_GET_NEW_ADDR is
      select add_1,
             add_2,
             add_3,
             city,
             state,
             country_id,
             post,
             jurisdiction_code
        from addr
       where module = 'SUPP'
         and key_value_1 = L_rtvdesc_rec.vendor_nbr
         and (addr_type = '03' or 
             (addr_type = '01' and primary_addr_ind = 'Y'))
         and rownum = 1
    order by addr_type desc;
    
    cursor c_check_supp_attr is
       select ret_allow_ind
         from sups
        where supplier = L_rtvdesc_rec.vendor_nbr;


BEGIN

   L_rtvdesc_rec := treat(O_message as "RIB_RTVDesc_REC");

   if L_rtvdesc_rec is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      return FALSE;
   end if;

   --Retrieve jurisdiction code
   open C_GET_JURIS_CODE;
   fetch C_GET_JURIS_CODE into L_juris_code;
   close C_GET_JURIS_CODE;
   
   --If jurisdiction code is NULL
   if L_juris_code is NULL then
      --Retrieve supplier's return/business address
      open C_GET_NEW_ADDR;
      fetch C_GET_NEW_ADDR into L_add_1,
                                L_add_2,
                                L_add_3,
                                L_city,
                                L_state,
                                L_country,
                                L_post,
                                L_juris_code;
      close C_GET_NEW_ADDR;
   end if;

   --check fetched return/business address
   if L_add_1 is NOT NULL then
      L_rtvdesc_rec.ship_address1 := L_add_1;
      L_rtvdesc_rec.ship_address2 := L_add_2;
      L_rtvdesc_rec.ship_address3 := L_add_3;
      L_rtvdesc_rec.state         := L_state;
      L_rtvdesc_rec.country       := L_country;
      L_rtvdesc_rec.city          := L_city;
      L_rtvdesc_rec.shipto_zip    := L_post;
   end if;

   -- populate the RTV_SQL header record
   O_rtv_record.rtv_order_no      := L_rtvdesc_rec.rtv_order_no;
   O_rtv_record.loc               := L_rtvdesc_rec.dc_dest_id;
   O_rtv_record.ext_ref_no        := L_rtvdesc_rec.rtv_id;
   O_rtv_record.ret_auth_num      := L_rtvdesc_rec.rtn_auth_nbr;
   O_rtv_record.supplier          := L_rtvdesc_rec.vendor_nbr;
   O_rtv_record.ship_addr1        := L_rtvdesc_rec.ship_address1;
   O_rtv_record.ship_addr2        := L_rtvdesc_rec.ship_address2;
   O_rtv_record.ship_addr3        := L_rtvdesc_rec.ship_address3;
   O_rtv_record.state             := L_rtvdesc_rec.state;
   O_rtv_record.city              := L_rtvdesc_rec.city;
   O_rtv_record.pcode             := L_rtvdesc_rec.shipto_zip;
   O_rtv_record.country           := L_rtvdesc_rec.country;
   O_rtv_record.tran_date         := L_rtvdesc_rec.creation_ts;
   O_rtv_record.comments          := L_rtvdesc_rec.comments;
   O_rtv_record.jurisdiction_code := L_juris_code;

   --When SIM sends an RTV message in Approved status to modify
   --either an RMS-created or SIM-created RTV, RMS will set the RTV 
   --to 'In Progress' status right away. Once modified by SIM, RTV cannot be modified by RMS any more,
   --similar to an RTV that has been shipped.
   
   OPEN c_check_supp_attr;
   FETCH c_check_supp_attr INTO L_return_allowed;
   CLOSE c_check_supp_attr;
   
   if L_return_allowed != 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_SUP_RTN', NULL, NULL, NULL);
      return FALSE;
   end if;

   if L_rtvdesc_rec.status_ind = 'A' then
      O_rtv_record.status_ind := 12;  -- In progress status
   elsif (L_rtvdesc_rec.status_ind = 'S' or L_rtvdesc_rec.status_ind is NULL) then
      O_rtv_record.status_ind := 15;
   end if;

   L_rib_dtl_TBL := L_rtvdesc_rec.rtvdtl_TBL;

   -- initialize collection
   L_dtl_rec.seq_nos := RTV_SQL.SEQ_NO_TBL();
   L_dtl_rec.items := ITEM_TBL();
   L_dtl_rec.returned_qtys := QTY_TBL();
   L_dtl_rec.from_disps := RTV_SQL.INV_STATUS_CODES_TBL();
   L_dtl_rec.unit_cost_exts := UNIT_COST_TBL();
   L_dtl_rec.extended_base_cost  := UNIT_COST_TBL();
   L_dtl_rec.unit_cost_supps := UNIT_COST_TBL();
   L_dtl_rec.unit_cost_locs := UNIT_COST_TBL();
   L_dtl_rec.reasons := RTV_SQL.REASON_TBL();
   L_dtl_rec.restock_pcts := RTV_SQL.RESTOCK_PCT_TBL();
   L_dtl_rec.inv_statuses := INV_STATUS_TBL();
   L_dtl_rec.mc_returned_qtys := QTY_TBL();
   L_dtl_rec.weights := RTV_SQL.WEIGHT_TBL();
   L_dtl_rec.weight_uoms := RTV_SQL.UOM_CLASS_TBL();
   L_dtl_rec.weight_cuoms := RTV_SQL.WEIGHT_TBL();
   L_dtl_rec.mc_weight_cuoms := RTV_SQL.WEIGHT_TBL();
   L_dtl_rec.cuoms := RTV_SQL.COST_UOM_TBL();
   
   if L_rib_dtl_TBL is not null and
      L_rib_dtl_TBL.COUNT > 0 then
      if CHECK_ITEMS(O_error_message,
                     L_sellable_TBL,
                     L_detail_TBL,
                     L_im_row_TBL,
                     L_rib_dtl_TBL) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_detail_TBL is not NULL and
      L_detail_TBL.COUNT > 0 then
      FOR i IN L_detail_TBL.FIRST .. L_detail_TBL.LAST LOOP
         L_dtl_rec.seq_nos.EXTEND;
         L_dtl_rec.items.EXTEND;
         L_dtl_rec.returned_qtys.EXTEND;
         L_dtl_rec.from_disps.EXTEND;
         L_dtl_rec.unit_cost_exts.EXTEND;
         L_dtl_rec.extended_base_cost.EXTEND;
         L_dtl_rec.unit_cost_supps.EXTEND;
         L_dtl_rec.unit_cost_locs.EXTEND;
         L_dtl_rec.reasons.EXTEND;
         L_dtl_rec.restock_pcts.EXTEND;
         L_dtl_rec.inv_statuses.EXTEND;
         L_dtl_rec.mc_returned_qtys.EXTEND;
         L_dtl_rec.weights.EXTEND;
         L_dtl_rec.weight_uoms.EXTEND;
         L_dtl_rec.weight_cuoms.EXTEND;
         L_dtl_rec.mc_weight_cuoms.EXTEND;
         L_dtl_rec.cuoms.EXTEND;

         L_dtl_rec.items(i)          := L_detail_TBL(i).item_id;
         L_dtl_rec.returned_qtys(i)  := L_detail_TBL(i).unit_qty;
         L_dtl_rec.from_disps(i)     := L_detail_TBL(i).from_disposition;
         L_dtl_rec.unit_cost_exts(i) := NVL(L_detail_TBL(i).gross_cost,L_detail_TBL(i).unit_cost);
         L_dtl_rec.extended_base_cost(i) := L_detail_TBL(i).unit_cost;
         L_dtl_rec.reasons(i)        := L_detail_TBL(i).reason;
         L_dtl_rec.weights(i)        := L_detail_TBL(i).weight;
         L_dtl_rec.weight_uoms(i)    := L_detail_TBL(i).weight_uom;

         if L_dtl_rec.returned_qtys(i) = 0 and L_rtvdesc_rec.status_ind = 'A' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_ZERO_QTY', NULL, NULL, NULL);
            return FALSE;
         end if;

         L_dtl_count := L_dtl_count + 1;
      END LOOP;
   end if;

   L_dtl_count := L_dtl_count + 1;

   -- process Break to Sell items
   if L_sellable_TBL is not NULL and
      L_sellable_TBL.COUNT > 0 then
      if GET_ORDERABLE_ITEMS(O_error_message,
                             L_orderable_TBL, --- Output item_table of orderable items
                             L_sellable_TBL,
                             L_rtvdesc_rec.rtv_order_no,
                             L_rtvdesc_rec.rtv_id,
                             L_rtvdesc_rec.dc_dest_id) = FALSE then
          return FALSE;
      end if;

      if L_orderable_TBL is not NULL and
         L_orderable_TBL.COUNT > 0 then
         FOR i IN L_orderable_TBL.FIRST .. L_orderable_TBL.LAST LOOP
            -- extend all fields in the record because eventually each one will be populated
            -- in the main RTV processing.  It is easier to do it here than later
            L_dtl_rec.seq_nos.EXTEND;
            L_dtl_rec.items.EXTEND;
            L_dtl_rec.returned_qtys.EXTEND;
            L_dtl_rec.from_disps.EXTEND;
            L_dtl_rec.unit_cost_exts.EXTEND;
            L_dtl_rec.extended_base_cost.EXTEND;
            L_dtl_rec.unit_cost_supps.EXTEND;
            L_dtl_rec.unit_cost_locs.EXTEND;
            L_dtl_rec.reasons.EXTEND;
            L_dtl_rec.restock_pcts.EXTEND;
            L_dtl_rec.inv_statuses.EXTEND;
            L_dtl_rec.mc_returned_qtys.EXTEND;
            L_dtl_rec.weights.EXTEND;
            L_dtl_rec.weight_uoms.EXTEND;
            L_dtl_rec.weight_cuoms.EXTEND;
            L_dtl_rec.mc_weight_cuoms.EXTEND;
            L_dtl_rec.cuoms.EXTEND;

            L_dtl_rec.items(L_dtl_count)          := L_orderable_TBL(i).item_id;
            L_dtl_rec.returned_qtys(L_dtl_count)  := L_orderable_TBL(i).unit_qty;
            L_dtl_rec.from_disps(L_dtl_count)     := L_orderable_TBL(i).from_disposition;
            L_dtl_rec.unit_cost_exts(i)           := NVL(L_detail_TBL(i).gross_cost,L_detail_TBL(i).unit_cost);
            L_dtl_rec.extended_base_cost(L_dtl_count) := L_orderable_TBL(i).unit_cost;
            L_dtl_rec.reasons(L_dtl_count)        := L_orderable_TBL(i).reason;

            L_im_row_TBL.EXTEND;
            L_im_row_TBL(L_dtl_count).item_xform_ind := 'Y';
            L_im_row_TBL(L_dtl_count).orderable_ind  := 'Y';
            L_im_row_TBL(L_dtl_count).sellable_ind   := 'N';

            if L_dtl_rec.returned_qtys(i) = 0 and L_rtvdesc_rec.status_ind = 'A' then
               O_error_message := SQL_LIB.CREATE_MSG('NO_ZERO_QTY', NULL, NULL, NULL);
               return FALSE;
            end if;

            L_dtl_count := L_dtl_count + 1;

         END LOOP;
      end if;
   end if;

   O_rtv_record.detail_tbl := L_dtl_rec;
   O_rtv_record.im_row_tbl := L_im_row_tbl;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_RTV.PARSE_RTV',
                                            to_char(SQLCODE));
      return FALSE;
END PARSE_RTV;
------------------------------------------------------------------------------------------
FUNCTION PROCESS_RTV(O_error_message     IN OUT  VARCHAR2,
                     I_rtv_record        IN      RTV_SQL.RTV_RECORD)
return BOOLEAN IS

BEGIN

   if I_rtv_record.status_ind = 12 then
      if RTV_SQL.APPROVE_RTV(O_error_message,
                             I_rtv_record) = FALSE then
         return FALSE;
      end if;
   elsif I_rtv_record.status_ind = 15 then
      if RTV_SQL.APPLY_PROCESS(O_error_message,
                               I_rtv_record) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

  EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_RTV.PROCESS_RTV',
                                             to_char(SQLCODE));
      return FALSE;

END PROCESS_RTV;
----------------------------------------------------------------------------------------
FUNCTION CHECK_ITEMS(O_error_message   IN OUT   VARCHAR2,
                     O_sellable_TBL    OUT      "RIB_RTVDtl_TBL",
                     O_detail_TBL      OUT      "RIB_RTVDtl_TBL",
                     O_im_row_TBL      OUT      RTV_SQL.ITEM_MASTER_TBL,
                     I_rib_detail_TBL  IN       "RIB_RTVDtl_TBL")
RETURN BOOLEAN IS

   L_program      VARCHAR2(62) := 'RMSSUB_RTV.CHECK_ITEMS';

   L_dtl_count    NUMBER := 0;
   L_sell_count   NUMBER := 0;

   L_item_rec     ITEM_MASTER%ROWTYPE;
   L_im_row_TBL   RTV_SQL.ITEM_MASTER_TBL := RTV_SQL.ITEM_MASTER_TBL();

BEGIN
   O_sellable_TBL := "RIB_RTVDtl_TBL"();
   O_detail_TBL   := "RIB_RTVDtl_TBL"();
   ---
   FOR i in 1..I_rib_detail_TBL.COUNT LOOP
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                          L_item_rec,  --- output record
                                          I_rib_detail_TBL(i).item_id) = FALSE then
         return FALSE;
      end if;
      if L_item_rec.sellable_ind = 'Y'   and
         L_item_rec.orderable_ind = 'N'  and
         L_item_rec.item_xform_ind = 'Y' then
         --- add item to sellable table
         L_sell_count := L_sell_count + 1;

         O_sellable_TBL.EXTEND();
         O_sellable_TBL(L_sell_count) := I_rib_detail_TBL(i);

      else
         --- add item to a new detail table
         L_dtl_count := L_dtl_count + 1;
         O_detail_TBL.EXTEND();
         O_detail_TBL(L_dtl_count) := I_rib_detail_TBL(i);
         L_im_row_TBL.EXTEND;
         L_im_row_TBL(L_dtl_count) := L_item_rec;
      end if;

   END LOOP;

   O_im_row_TBL := L_im_row_TBL;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
      return FALSE;

END CHECK_ITEMS;
----------------------------------------------------------------------------------------
FUNCTION GET_ORDERABLE_ITEMS(O_error_message   IN OUT   VARCHAR2,
                             O_orderable_TBL   IN OUT   "RIB_RTVDtl_TBL",
                             I_sellable_TBL    IN       "RIB_RTVDtl_TBL",
                             I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE,
                             I_ext_ref_no      IN       RTV_HEAD.EXT_REF_NO%TYPE,
                             I_location        IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'RMSSUB_ASNOUT.GET_ORDERABLE_ITEMS';

   L_orditem_TBL            bts_orditem_qty_tbl;
   L_inv_status             INV_STATUS_CODES.INV_STATUS%TYPE := NULL;

   L_sell_item_tbl          ITEM_TBL := ITEM_TBL();
   L_sell_qty_tbl           QTY_TBL  := QTY_TBL();
   L_inv_status_tbl         INV_STATUS_TBL  := INV_STATUS_TBL();
   L_reason_tbl             RTV_SQL.REASON_TBL := RTV_SQL.REASON_TBL();

   L_orderable_detail_TBL   "RIB_RTVDtl_TBL" := "RIB_RTVDtl_TBL"();

   L_detail_no              NUMBER := 0;

   cursor C_ORD_ITEM_QTY (I_item  IN ITEM_MASTER.ITEM%TYPE) is
      select orderable_item,qty
        from TABLE(cast(L_orditem_TBL AS bts_orditem_qty_tbl))
       where sellable_item = I_item;

BEGIN
   O_orderable_TBL := "RIB_RTVDtl_TBL"();

   if I_sellable_TBL is not NULL and
      I_sellable_TBL.COUNT > 0 then

      FOR i in I_sellable_TBL.FIRST..I_sellable_TBL.LAST LOOP
         L_sell_item_tbl.EXTEND;
         L_sell_item_tbl(L_sell_item_tbl.COUNT) := I_sellable_TBL(i).item_id;
         ---
         L_sell_qty_tbl.EXTEND;
         L_sell_qty_tbl(L_sell_qty_tbl.COUNT) := I_sellable_TBL(i).unit_qty;
         ---
         L_reason_tbl.EXTEND;
         L_reason_tbl(L_reason_tbl.COUNT) := I_sellable_TBL(i).reason;

         /* convert the disposition to an invertory status */
         if I_sellable_TBL(i).from_disposition IS NOT NULL then
            if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                         L_inv_status,
                                         I_sellable_TBL(i).from_disposition) = FALSE then
               return FALSE;
            end if;
            if L_inv_status IS NULL then
               L_inv_status := -1;
            end if;
         else
            L_inv_status := -1;
         end if;

         L_inv_status_tbl.EXTEND;
         L_inv_status_tbl(L_inv_status_tbl.COUNT) := L_inv_status;

      END LOOP;

      if ITEM_XFORM_SQL.RTV_ORDERABLE_ITEM_INFO(O_error_message,
                                                L_orditem_TBL,
                                                L_sell_item_tbl,
                                                L_sell_qty_tbl,
                                                I_rtv_order_no,
                                                I_ext_ref_no,
                                                I_location,
                                                L_inv_status_tbl,
                                                L_reason_tbl)   = FALSE then
         return FALSE;
      end if;

      FOR i in I_sellable_TBL.FIRST..I_sellable_TBL.LAST LOOP
          FOR item_rec IN C_ORD_ITEM_QTY(I_sellable_TBL(i).item_id) LOOP
              L_detail_no := L_detail_no + 1;
              L_orderable_detail_TBL.EXTEND;
              L_orderable_detail_TBL(L_detail_no) := I_sellable_TBL(i); --- Get all message values
              L_orderable_detail_TBL(L_detail_no).item_id := item_rec.orderable_item;
              L_orderable_detail_TBL(L_detail_no).unit_qty := item_rec.qty;
         END LOOP;
      END LOOP;

   end if;

   O_orderable_TBL := L_orderable_detail_TBL;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
      return FALSE;

END GET_ORDERABLE_ITEMS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_RTV;
/
