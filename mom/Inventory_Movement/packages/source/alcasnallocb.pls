CREATE OR REPLACE PACKAGE BODY ALC_ASN_SQL AS

   LP_vdate               DATE := GET_VDATE;
   LP_currency_obl        CURRENCIES.CURRENCY_CODE%TYPE;
   LP_exchange_rate_obl   CURRENCY_RATES.EXCHANGE_RATE%TYPE;


   -- Pack Component Item Info
   TYPE packitem_record IS RECORD(item              SHIPSKU.ITEM%TYPE,
                                  comp_qty          V_PACKSKU_QTY.QTY%TYPE,
                                  comp_rec_qty      V_PACKSKU_QTY.QTY%TYPE,
                                  comp_cost         ORDLOC.UNIT_COST%TYPE,
                                  comp_rec_cost     ORDLOC.UNIT_COST%TYPE,
                                  alloc_basis_amt   NUMBER,
                                  alloc_percent     NUMBER);

   TYPE packitem_table_type IS TABLE OF packitem_record INDEX BY BINARY_INTEGER;

   -- Line Item Record
   TYPE lineitem_record IS RECORD(order_no          ORDHEAD.ORDER_NO%TYPE,
                                  shipment          SHIPSKU.SHIPMENT%TYPE,
                                  vessel_id         TRANSPORTATION.VESSEL_ID%TYPE,
                                  voyage_flt_id     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                  etd               TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                  item              SHIPSKU.ITEM%TYPE,
                                  pack_type         ITEM_MASTER.PACK_TYPE%TYPE,
                                  location          ORDLOC.LOCATION%TYPE,
                                  location_type     ORDLOC.LOC_TYPE%TYPE,
                                  std_qty           ALC_COMP_LOC.QTY%TYPE,
                                  uom               UOM_CLASS.UOM%TYPE,
                                  uom_qty           ALC_COMP_LOC.QTY%TYPE,
                                  supplier          ORDHEAD.SUPPLIER%TYPE,
                                  origin_country    ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                  unit_cost         ORDLOC.UNIT_COST%TYPE,
                                  item_rec_qty      ALC_COMP_LOC.QTY%TYPE,
                                  item_po_cost      ORDLOC.UNIT_COST%TYPE,
                                  item_supp_cost    ORDLOC.UNIT_COST%TYPE,
                                  pack_qty          ALC_COMP_LOC.QTY%TYPE,
                                  alloc_basis_amt   NUMBER,
                                  packitem_tbl      PACKITEM_TABLE_TYPE);

   TYPE lineitem_table_type IS TABLE OF lineitem_record INDEX BY BINARY_INTEGER;

   lineitem_table      lineitem_table_type;

   -- Private Functions
------------------------------------------------------------------------------------
FUNCTION INSERT_ALC_HEAD(O_error_message     IN OUT   VARCHAR2,
                         I_order_no          IN       ORDHEAD.ORDER_NO%TYPE,
                         I_shipment          IN       SHIPMENT.SHIPMENT%TYPE,
                         I_item              IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_item         IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_type         IN       ITEM_MASTER.PACK_TYPE%TYPE,
                         I_obligation_key    IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                         I_vessel_id         IN       TRANSPORTATION.VESSEL_ID%TYPE,
                         I_voyage_flt_id     IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                         I_etd               IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                         I_item_qty          IN       ORDLOC.QTY_RECEIVED%TYPE,
                         I_error_ind         IN       ALC_HEAD.ERROR_IND%TYPE)
RETURN BOOLEAN ;
------------------------------------------------------------------------------------
FUNCTION INSERT_ALC_COMP_LOCS(O_error_message     IN OUT   VARCHAR2,
                              I_order_no          IN       ORDHEAD.ORDER_NO%TYPE,
                              I_shipment          IN       SHIPMENT.SHIPMENT%TYPE,
                              I_item              IN       ITEM_MASTER.ITEM%TYPE,
                              I_pack_item         IN       ITEM_MASTER.ITEM%TYPE,
                              I_obligation_key    IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                              I_comp_id           IN       ELC_COMP.COMP_ID%TYPE,
                              I_location          IN       ORDLOC.LOCATION%TYPE,
                              I_loc_type          IN       ORDLOC.LOC_TYPE%TYPE,
                              I_act_value         IN       ALC_COMP_LOC.ACT_VALUE%TYPE,
                              I_qty               IN       ALC_COMP_LOC.QTY%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
FUNCTION CONVERT_OBL_AMT(O_error_message       IN OUT   VARCHAR2,
                         O_obl_comp_amt_prim   OUT      OBLIGATION_COMP.AMT%TYPE,
                         I_obl_comp_amt        IN       OBLIGATION_COMP.AMT%TYPE,
                         I_obligation_key      IN       OBLIGATION.OBLIGATION_KEY%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
FUNCTION ALLOC_ITEM(O_error_message       IN OUT   VARCHAR2,
                    IO_lineitem_tbl       IN OUT   lineitem_table_type,
                    I_obligation_key      IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                    I_obligation_level    IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                    I_key_value_1         IN       OBLIGATION.KEY_VALUE_1%TYPE,
                    I_key_value_2         IN       OBLIGATION.KEY_VALUE_2%TYPE,
                    I_key_value_3         IN       OBLIGATION.KEY_VALUE_3%TYPE,
                    I_key_value_4         IN       OBLIGATION.KEY_VALUE_4%TYPE,
                    I_key_value_5         IN       OBLIGATION.KEY_VALUE_5%TYPE,
                    I_key_value_6         IN       OBLIGATION.KEY_VALUE_6%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64)                  := 'ALC_ASN_SQL.ALLOC_ITEM';
   L_alloc_basis_uom        OBLIGATION_COMP.ALLOC_BASIS_UOM%TYPE := NULL;
   L_prev_alloc_basis_uom   OBLIGATION_COMP.ALLOC_BASIS_UOM%TYPE := NULL;
   L_comp_id                OBLIGATION_COMP.COMP_ID%TYPE;
   L_error_ind              ALC_HEAD.ERROR_IND%TYPE       := 'N';
   --
   L_item_qty        ORDLOC.QTY_ORDERED%TYPE := 0;
   L_item_rec_qty    ORDLOC.QTY_ORDERED%TYPE := 0;
   L_comp_qty        ORDLOC.QTY_ORDERED%TYPE := 0;
   L_comp_rec_qty    ORDLOC.QTY_ORDERED%TYPE := 0;
   L_total_rec_qty   ORDLOC.QTY_ORDERED%TYPE := 0;
   L_finalize_qty    ORDLOC.QTY_ORDERED%TYPE := 0;
   L_pack_qty        ORDLOC.QTY_ORDERED%TYPE := 0;
   --
   L_total_rec_cost      ORDLOC.UNIT_COST%TYPE       := 0;
   L_obl_comp_amt_prim   ALC_COMP_LOC.ACT_VALUE%TYPE := 0;
   L_unit_cost           ORDLOC.UNIT_COST%TYPE       := 0;
   L_supp_pack_cost      ORDLOC.UNIT_COST%TYPE       := 0;
   L_item_po_cost        ORDLOC.UNIT_COST%TYPE       := 0;
   L_comp_cost           ORDLOC.UNIT_COST%TYPE       := 0;
   L_comp_rec_cost           ORDLOC.UNIT_COST%TYPE         := 0;
   --
   L_alloc_basis_amt   ALC_COMP_LOC.ACT_VALUE%TYPE   := 0;
   --
   L_order_no            ORDHEAD.ORDER_NO%TYPE;
   L_item                ITEM_MASTER.ITEM%TYPE;
   L_pack_no             ITEM_MASTER.ITEM%TYPE;
   L_comp_item           ITEM_MASTER.ITEM%TYPE;
   L_supplier            SUPS.SUPPLIER%TYPE;
   L_origin_country_id   COUNTRY.COUNTRY_ID%TYPE;
   L_location            ORDLOC.LOCATION%TYPE;
   L_loc_type            ORDLOC.LOC_TYPE%TYPE;
   L_uom                 UOM_CLASS.UOM%TYPE;
   L_standard_uom        UOM_CLASS.UOM%TYPE;
   L_standard_class      UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor         UOM_CONVERSION.FACTOR%TYPE;
   ---
   L_pack_ind        ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind    ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind   ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type       ITEM_MASTER.PACK_TYPE%TYPE;
   ---
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_pack_cnt      NUMBER(5);
   li_first_time   BOOLEAN := TRUE;
   L_shipment      shipment.shipment%TYPE;
   --
   cursor C_GET_COMPS is
      select c.comp_id,
             c.alloc_basis_uom,
             c.per_count_uom,
             c.qty ,
             c.amt
        from obligation_comp c,
             obligation o
       where o.obligation_key     = c.obligation_key
         and o.obligation_key     = I_obligation_key
         and c.in_alc_ind         = 'Y'
       order by c.alloc_basis_uom;

   cursor C_GET_PACKITEMS is
      select item,
             qty
        from v_packsku_qty
       where pack_no = L_item;

BEGIN

   FOR i in IO_lineitem_tbl.first..IO_lineitem_tbl.last LOOP
      --
      L_item                := IO_lineitem_tbl(i).item;
      L_supplier            := IO_lineitem_tbl(i).supplier;
      L_origin_country_id   := IO_lineitem_tbl(i).origin_country;

      --
      if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                       L_pack_ind,
                                       L_sellable_ind,
                                       L_orderable_ind,
                                       L_pack_type,
                                       L_item) = FALSE then
         return FALSE;
      end if;
      IO_lineitem_tbl(i).pack_type := L_pack_type;
      L_pack_cnt := 0;
      --
      if NVL(L_pack_type,'N') = 'B' then
         FOR C_rec in C_GET_PACKITEMS LOOP
            --
            L_pack_cnt := L_pack_cnt + 1;
            L_comp_qty                                             := C_rec.qty;
            IO_lineitem_tbl(i).packitem_tbl(L_pack_cnt).item       := C_rec.item;
            IO_lineitem_tbl(i).packitem_tbl(L_pack_cnt).comp_qty   := C_rec.qty;
            --
            if INSERT_ALC_HEAD(O_error_message,
                               IO_lineitem_tbl(i).order_no,
                               IO_lineitem_tbl(i).shipment,
                               C_rec.item,
                               IO_lineitem_tbl(i).item,
                               IO_lineitem_tbl(i).pack_type,
                               I_obligation_key,
                               IO_lineitem_tbl(i).vessel_id,
                               IO_lineitem_tbl(i).voyage_flt_id,
                               IO_lineitem_tbl(i).etd,
                               (IO_lineitem_tbl(i).std_qty * L_comp_qty),
                               L_error_ind)   = FALSE then
               return FALSE;
            end if;
         end LOOP;
      else
         --
         if INSERT_ALC_HEAD(O_error_message,
                            IO_lineitem_tbl(i).order_no,
                            IO_lineitem_tbl(i).shipment,
                            IO_lineitem_tbl(i).item,
                            NULL,
                            IO_lineitem_tbl(i).pack_type,
                            I_obligation_key,
                            IO_lineitem_tbl(i).vessel_id,
                            IO_lineitem_tbl(i).voyage_flt_id,
                            IO_lineitem_tbl(i).etd,
                            IO_lineitem_tbl(i).std_qty,
                            L_error_ind)   = FALSE then
            return FALSE;
         end if;
      end if;
   end LOOP;
   --
   FOR comp_rec in C_GET_COMPS LOOP
      L_alloc_basis_uom := comp_rec.alloc_basis_uom;
      L_comp_id         := comp_rec.comp_id;
      --
      if CONVERT_OBL_AMT (O_error_message,
                          L_obl_comp_amt_prim,
                          comp_rec.amt,
                          I_obligation_key) = FALSE then
         return FALSE;
      end if;

      -- Start of  the allocation basis has changed
      if NVL(L_prev_alloc_basis_uom,'-1') <> NVL(L_alloc_basis_uom,'-1') or li_first_time  then
         L_total_rec_cost := 0;
         L_total_rec_qty  := 0;
         li_first_time := FALSE;
         FOR i in IO_lineitem_tbl.first..IO_lineitem_tbl.last LOOP
            --
            L_location  := IO_lineitem_tbl(i).location;
            L_order_no  := IO_lineitem_tbl(i).order_no;
            L_item      := IO_lineitem_tbl(i).item;
            L_item_qty  := IO_lineitem_tbl(i).std_qty;
            L_unit_cost := IO_lineitem_tbl(i).unit_cost;
            L_shipment  := IO_lineitem_tbl(i).shipment;
            --
            L_supp_pack_cost := 0;
            L_item_rec_qty   := 0;
            L_pack_qty        := 0;
            -- If the line item is  buyer's pack type or non pack item
            if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                                L_standard_uom,
                                                L_standard_class,
                                                L_conv_factor,
                                                IO_lineitem_tbl(i).item,
                                                'N') = FALSE then
               return FALSE;
            end if;
            --
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_rec_qty,
                               NVL(comp_rec.alloc_basis_uom, comp_rec.per_count_uom),
                               L_item_qty,
                               L_standard_uom,
                               IO_lineitem_tbl(i).item,
                               IO_lineitem_tbl(i).supplier,
                               IO_lineitem_tbl(i).origin_country) = FALSE then
               return FALSE;
            end if;
            ---
            IO_lineitem_tbl(i).item_po_cost := L_item_rec_qty * L_unit_cost;
            IO_lineitem_tbl(i).item_rec_qty := L_item_rec_qty ;
 
            if NVL(IO_lineitem_tbl(i).pack_type,'N') = 'B' then
               FOR j in IO_lineitem_tbl(i).packitem_tbl.FIRST .. IO_lineitem_tbl(i).packitem_tbl.LAST LOOP
                  L_comp_item   := IO_lineitem_tbl(i).packitem_tbl(j).item;
                  L_comp_qty    := IO_lineitem_tbl(i).packitem_tbl(j).comp_qty;
                  --
                  if SUPP_ITEM_SQL.GET_COST ( O_error_message,
                                             L_comp_cost,
                                             L_comp_item,
                                             L_supplier,
                                             L_origin_country_id,
                                             IO_lineitem_tbl(i).location) = FALSE then
                     return FALSE;
                  end if;
                  --
                  if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                                   L_standard_uom,
                                                   L_standard_class,
                                                   L_conv_factor,
                                                   L_comp_item,
                                                   'N') = FALSE then
                     return FALSE;
                  end if;
                  --
                  if UOM_SQL.CONVERT(O_error_message,
                                     L_comp_qty, -- Converted comp qty in alloc basis UOM or per count UOM
                                     NVL(comp_rec.alloc_basis_uom, comp_rec.per_count_uom),
                                     L_comp_qty,
                                     L_standard_uom,
                                     L_comp_item,
                                     IO_lineitem_tbl(i).supplier,
                                     IO_lineitem_tbl(i).origin_country) = FALSE then
                     return FALSE;
                  end if;
                  --
                  IO_lineitem_tbl(i).packitem_tbl(j).comp_cost :=  L_comp_cost * L_comp_qty ;
                  L_supp_pack_cost      := L_supp_pack_cost  + L_comp_cost * L_comp_qty ;
                  --
                  IO_lineitem_tbl(i).packitem_tbl(j).comp_rec_qty  :=  L_comp_qty * L_item_rec_qty;
                  IO_lineitem_tbl(i).packitem_tbl(j).comp_rec_cost :=  L_comp_cost * L_comp_qty * L_item_rec_qty;
                  L_pack_qty  :=  L_pack_qty + L_comp_qty ;
               end LOOP;

               IO_lineitem_tbl(i).item_supp_cost := L_supp_pack_cost;
               IO_lineitem_tbl(i).pack_qty       := L_pack_qty;
            end if;

            L_total_rec_qty   := L_total_rec_qty  +  L_item_rec_qty;
            L_total_rec_cost  :=  L_total_rec_cost + IO_lineitem_tbl(i).item_po_cost ;
         end LOOP; -- end of FOR LOOP IO_lineitem_tbl.first..IO_lineitem_tbl.last LOOP
      end if;  --  L_prev_alloc_basis_uom != L_alloc_basis_uom

      L_prev_alloc_basis_uom := L_alloc_basis_uom;
      -- End of  the allocation basis has changed
      if L_total_rec_qty = 0 or L_total_rec_cost = 0 then
         return TRUE;
      end if;
      L_item_rec_qty := 0;

      FOR i in IO_lineitem_tbl.first..IO_lineitem_tbl.last LOOP
             --
             L_item_rec_qty := IO_lineitem_tbl(i).item_rec_qty;
             L_supp_pack_cost := IO_lineitem_tbl(i).item_supp_cost;   
             L_item_po_cost   := IO_lineitem_tbl(i).item_po_cost;
             L_pack_qty       := IO_lineitem_tbl(i).pack_qty ;
             --
             if NVL(IO_lineitem_tbl(i).pack_type,'N') = 'B' then

                FOR j in IO_lineitem_tbl(i).packitem_tbl.FIRST .. IO_lineitem_tbl(i).packitem_tbl.LAST LOOP
                   L_comp_item := IO_lineitem_tbl(i).packitem_tbl(j).item;
                   L_comp_rec_qty  := IO_lineitem_tbl(i).packitem_tbl(j).comp_rec_qty;
                   L_comp_rec_cost := IO_lineitem_tbl(i).packitem_tbl(j).comp_rec_cost;
                   --
                   L_comp_cost  := IO_lineitem_tbl(i).packitem_tbl(j).comp_cost ;
                   L_comp_qty   := IO_lineitem_tbl(i).packitem_tbl(j).comp_qty;
                   --
                   if  L_alloc_basis_uom IS NULL then
                      L_alloc_basis_amt := L_obl_comp_amt_prim * (L_comp_cost /L_supp_pack_cost)*(L_item_po_cost/ L_total_rec_cost);
                   else
                      L_alloc_basis_amt := L_obl_comp_amt_prim * (L_comp_qty/L_pack_qty)*( L_item_rec_qty/ L_total_rec_qty);
                   end if;  --   L_alloc_basis_uom IS NOT NULL
                   --
                   if L_comp_rec_qty > 0 then
                      if INSERT_ALC_COMP_LOCS(O_error_message,
                                              IO_lineitem_tbl(i).order_no,
                                              IO_lineitem_tbl(i).shipment,
                                              L_comp_item,
                                              IO_lineitem_tbl(i).item,
                                              I_obligation_key,
                                              L_comp_id,
                                              IO_lineitem_tbl(i).location,
                                              IO_lineitem_tbl(i).location_type,
                                              (L_alloc_basis_amt/L_comp_rec_qty),
                                              L_comp_rec_qty) = FALSE then
                         return FALSE;
                      end if;
                   end if;
                end LOOP;
             else -- If the line item is not buyer's pack type
                if L_alloc_basis_uom IS NULL then
                   L_alloc_basis_amt := L_obl_comp_amt_prim * (L_item_po_cost / L_total_rec_cost);
                else
                   L_alloc_basis_amt := L_obl_comp_amt_prim * (L_item_rec_qty/ L_total_rec_qty);
                end if;  --   L_alloc_basis_uom IS NOT NULL
                --
                if L_item_rec_qty > 0 then
                   if INSERT_ALC_COMP_LOCS(O_error_message,
                                           IO_lineitem_tbl(i).order_no,
                                           IO_lineitem_tbl(i).shipment,
                                           IO_lineitem_tbl(i).item,
                                           NULL,
                                           I_obligation_key,
                                           L_comp_id,
                                           IO_lineitem_tbl(i).location,
                                           IO_lineitem_tbl(i).location_type,
                                           (L_alloc_basis_amt/L_item_rec_qty),
                                           L_item_rec_qty) = FALSE then
                      return FALSE;
                   end if;
                end if; --L_item_rec_qty > 0
             end if;
         end LOOP; -- end of FOR LOOP IO_lineitem_tbl.first..IO_lineitem_tbl.last LOOP

   end LOOP; -- end of FOR Loop comp_rec in C_GET_COMPS LOOP
   ---
   update alc_head a
            set a.alc_qty = (select NVL(sum(alc.qty),0)
                               from alc_comp_loc alc
                              where alc.order_no = a.order_no
                                and alc.seq_no   = a.seq_no
                                and alc.comp_id  = L_comp_id )
     where a.obligation_key = I_obligation_key;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ALLOC_ITEM;
-------------------------------------------------------------------------------------------------
/*Public Functions*/
-------------------------------------------------------------------------------------------------
FUNCTION ALLOC_ASN (O_error_message      IN OUT   VARCHAR2,
                    O_handled            IN OUT   BOOLEAN,
                    I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                    I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                    I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                    I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                    I_key_value_3        IN       OBLIGATION.KEY_VALUE_3%TYPE,
                    I_key_value_4        IN       OBLIGATION.KEY_VALUE_4%TYPE,
                    I_key_value_5        IN       OBLIGATION.KEY_VALUE_5%TYPE,
                    I_key_value_6        IN       OBLIGATION.KEY_VALUE_6%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ALC_ASN_SQL.ALLOC_ASN';
   L_order_no           ORDHEAD.ORDER_NO%TYPE :=NULL;
   L_item               ITEM_MASTER.ITEM%TYPE :=NULL;
   L_loc                ORDLOC.LOCATION%TYPE  := NULL;
   L_asn                SHIPMENT.ASN%TYPE     := NULL;
   L_carton             SHIPSKU.CARTON%TYPE   := NULL;
   L_vessel_id          TRANSPORTATION.VESSEL_ID%TYPE := NULL;
   L_voyage_flt_id      TRANSPORTATION.VOYAGE_FLT_ID%TYPE :=NULL;
   L_etd                TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE := NULL;
   L_container_id       TRANSPORTATION.CONTAINER_ID%TYPE := NULL;
   L_bl_awb_id          TRANSPORTATION.BL_AWB_ID%TYPE := NULL;
   L_obl_level_type     VARCHAR2(6);
   L_inv_obl_level      BOOLEAN := FALSE;
   --
   L_trnsprt_method RTM_UNIT_OPTIONS.RTM_TRNSPRT_OBL_ALLOC_METHOD%TYPE;
   --
   L_wh             WH.WH%TYPE;
   L_qty            SHIPSKU.QTY_EXPECTED%TYPE;
   L_prior_dist_tab DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   L_dist_table     DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   item_cnt         NUMBER(6)                  :=0;

   cursor C_GET_ASN_PO_RCPT_QTY is
        with matching_shipment
          as (select distinct t.vessel_id, t.voyage_flt_id, t.estimated_depart_date, s.*
                from transportation t,
                     transportation_shipment ts,
                     shipment s
               where L_obl_level_type = 'TR'
                  -- order no is required.  other fields might be NULL
                 and t.order_no = NVL(L_order_no, t.order_no)
                 and (t.vessel_id = NVL(L_vessel_id, t.vessel_id) or (t.vessel_id is NULL and L_vessel_id is NULL))
                 and (t.voyage_flt_id = NVL(L_voyage_flt_id, t.voyage_flt_id) or (t.voyage_flt_id is NULL and L_voyage_flt_id is NULL))
                 and (t.estimated_depart_date = NVL(L_etd, t.estimated_depart_date) or (t.estimated_depart_date is NULL and L_etd is NULL))
                 and (t.order_no = NVL(L_order_no, t.order_no) or (t.order_no is NULL and L_order_no is NULL))
                 and (t.bl_awb_id = NVL(L_bl_awb_id, t.bl_awb_id) or (t.bl_awb_id is NULL and L_bl_awb_id is NULL))
                 and (t.container_id = NVL(L_container_id, t.container_id) or (t.container_id is NULL and L_container_id is NULL))
                 and (t.item = NVL(L_item, t.item) or (t.item is NULL and L_item is NULL))
                 and ts.vessel_id (+)= t.vessel_id
                 and ts.voyage_flt_id (+)= t.voyage_flt_id
                 and ts.estimated_depart_date (+)= t.estimated_depart_date
                 and ts.order_no (+)= t.order_no
                 and s.order_no = t.order_no
                 and s.shipment = NVL(ts.shipment, s.shipment)
             union all
              select NULL vessel_id, NULL voyage_flt_id, NULL estimated_depart_date, s.*
                from shipment s
               where L_obl_level_type != 'TR'
                 and s.order_no = NVL(L_order_no, s.order_no)
                 and (s.asn = NVL(L_asn, s.asn) or (s.asn is NULL and L_asn is NULL)))
      select sh.order_no,
             sh.shipment,
             sk.item,
             sh.to_loc,
             sh.to_loc location,
             sh.to_loc_type,
             sum(NVL(decode(sh.status_code, 'I', 0, sk.weight_received),0)) weight,
             decode(sh.status_code, 'I', NULL, sk.weight_received_uom) weight_uom,
             oh.supplier,
             os.origin_country_id,
             sum(NVL(decode(sh.status_code, 'I', sk.qty_expected, sk.qty_received),0)) qty,
             0 prior_qty,
             (ol.unit_cost / oh.exchange_rate) unit_cost,
             sh.vessel_id,
             sh.voyage_flt_id,
             sh.estimated_depart_date
        from matching_shipment sh,
             shipsku  sk,
             ordhead oh,
             ordsku  os,
             ordloc  ol
       where sh.to_loc_type <> 'W'
         and sh.shipment = sk.shipment
         and os.item     = sk.item
         and os.order_no = oh.order_no
         and ol.location = sh.to_loc
         and ol.item     = sk.item
         and ol.order_no = oh.order_no
         and oh.order_no = sh.order_no
         and sk.item = NVL(L_item, sk.item)
         and (sk.carton = NVL(L_carton, sk.carton) or (sk.carton is NULL and L_carton is NULL))
         and not exists ( select 'x'
                            from alc_head ah
                           where ah.order_no = sh.order_no
                             and ah.shipment = sh.shipment
                             and ah.obligation_key = i_obligation_key
                             and ah.status != 'P')
    group by sh.order_no,
             sh.shipment,
             sk.item,
             sh.to_loc,
             sh.to_loc_type,
             decode(sh.status_code, 'I', NULL, sk.weight_received_uom),
             oh.supplier,
             os.origin_country_id,
             ol.unit_cost,
             oh.exchange_rate,
             sh.vessel_id,
             sh.voyage_flt_id,
             sh.estimated_depart_date
   union all
      select sh.order_no,
             sh.shipment,
             sk.item,
             sh.to_loc,
             NULL location,
             sh.to_loc_type,
             0 weight,
             NULL weight_uom,
             oh.supplier,
             os.origin_country_id,
             NVL(sum(NVL(sk.qty_expected,0)),0) qty,
             NVL(sum(sum(NVL(sk.qty_expected,0)))
                 over (partition by sh.order_no, sk.item
                           order by sh.shipment
                            rows between unbounded preceding and 1 preceding),0) prior_qty,
             (sk.unit_cost / oh.exchange_rate) unit_cost,
             sh.vessel_id,
             sh.voyage_flt_id,
             sh.estimated_depart_date
        from matching_shipment sh,
             shipsku  sk,
             ordhead oh,
             ordsku  os
       where sh.to_loc_type = 'W' and sh.status_code = 'I'
         and sh.shipment = sk.shipment
         and os.item     = sk.item
         and os.order_no = oh.order_no
         and oh.order_no = sh.order_no
         and sk.item = NVL(L_item, sk.item)
         and (sk.carton = NVL(L_carton, sk.carton) or (sk.carton is NULL and L_carton is NULL))
         and not exists ( select 'x'
                            from alc_head ah
                           where ah.order_no = sh.order_no
                             and ah.shipment = sh.shipment
                             and ah.obligation_key = i_obligation_key
                             and ah.status != 'P')
    group by sh.order_no,
             sh.shipment,
             sk.item,
             sh.to_loc,
             sh.to_loc_type,
             oh.supplier,
             os.origin_country_id,
             sk.unit_cost,
             oh.exchange_rate,
             sh.vessel_id,
             sh.voyage_flt_id,
             sh.estimated_depart_date
   union all
      select sh.order_no,
             sh.shipment,
             sk.item,
             sh.to_loc,
             ol.location,
             sh.to_loc_type,
             sum(NVL(sk.weight_received,0)) weight,
             sk.weight_received_uom weight_uom,
             oh.supplier,
             os.origin_country_id,
             sum(NVL(skl.qty_received,0)) qty,
             0 prior_qty,
             (ol.unit_cost / oh.exchange_rate) unit_cost,
             sh.vessel_id,
             sh.voyage_flt_id,
             sh.estimated_depart_date
        from matching_shipment sh,
             shipsku  sk,
             shipsku_loc  skl,
             ordhead oh,
             ordsku  os,
             ordloc  ol
       where sh.to_loc_type = 'W' and sh.status_code != 'I'
         and sk.shipment = skl.shipment
         and sk.item     = skl.item
         and sk.seq_no    = skl.seq_no
         and sh.shipment = sk.shipment
         and os.item     = sk.item
         and os.order_no = oh.order_no
         and ol.location = skl.to_loc
         and ol.item     = sk.item
         and ol.order_no = oh.order_no
         and oh.order_no = sh.order_no
         and sk.item = NVL(L_item, sk.item)
         and (sk.carton = NVL(L_carton, sk.carton) or (sk.carton is NULL and L_carton is NULL))
         and not exists ( select 'x'
                            from alc_head ah
                           where ah.order_no = sh.order_no
                             and ah.shipment = sh.shipment
                             and ah.obligation_key = i_obligation_key
                             and ah.status != 'P')
    group by sh.order_no,
             sh.shipment,
             sk.item,
             sh.to_loc,
             ol.location,
             sh.to_loc_type,
             sk.weight_received_uom,
             oh.supplier,
             os.origin_country_id,
             ol.unit_cost,
             oh.exchange_rate,
             sh.vessel_id,
             sh.voyage_flt_id,
             sh.estimated_depart_date
       order by 1,2;

   cursor C_TRNSPRT_METHOD is
      select rtm_trnsprt_obl_alloc_method
        from rtm_unit_options;

   TYPE rcpt_tbl_type IS TABLE OF C_GET_ASN_PO_RCPT_QTY%ROWTYPE INDEX BY BINARY_INTEGER;
   rcpt_tbl rcpt_tbl_type;

BEGIN

   O_handled := TRUE;

   if I_obligation_level like 'ASN%' then
      L_obl_level_type := 'ASN';
      L_asn := I_key_value_1;
      if I_obligation_level = 'ASNP' then
         L_order_no := I_key_value_2;
      elsif I_obligation_level = 'ASNC' then
         L_carton := I_key_value_2;
      elsif I_obligation_level = 'ASN' then
         NULL;
      else
         L_inv_obl_level := TRUE;
      end if;
   elsif I_obligation_level like 'PO%' then
      --
      open  C_TRNSPRT_METHOD;
      fetch C_TRNSPRT_METHOD into L_trnsprt_method;
      close C_TRNSPRT_METHOD;
      --
      if NVL(L_trnsprt_method, 'TRNSPRT') != 'ASN' then
         O_handled := FALSE;
         return TRUE;
      end if;
      --
      L_order_no := I_key_value_1;
      if I_obligation_level = 'POT' then
         -- If customer is using the transportation_shipment mapping, we would pick up the mapped shipments for the PO
         L_obl_level_type := 'TR';
      else
         L_obl_level_type := 'PO';
         if I_obligation_level = 'POIT' then
            L_item := I_key_value_2;
         elsif I_obligation_level = 'PO' then
            NULL;
         else
            L_inv_obl_level := TRUE;
         end if;
      end if;
   elsif I_obligation_level like 'TR%' then

      open  C_TRNSPRT_METHOD;
      fetch C_TRNSPRT_METHOD into L_trnsprt_method;
      close C_TRNSPRT_METHOD;
      --
      if NVL(L_trnsprt_method, 'TRNSPRT') != 'ASN' then
         O_handled := FALSE;
         return TRUE;
      end if;
      --
      L_obl_level_type := 'TR';
      if I_obligation_level like 'TRV%' then
         L_vessel_id := I_key_value_1;
         L_voyage_flt_id := I_key_value_2;
         L_etd := TO_DATE(I_key_value_3, 'DD-MON-RR');
         if I_obligation_level = 'TRVVEP' then
            L_order_no := I_key_value_4;
         elsif I_obligation_level = 'TRVP' then
            L_order_no := I_key_value_4;
            L_item := I_key_value_5;
         elsif I_obligation_level = 'TRVV' then
            NULL;
         else
            L_inv_obl_level := TRUE;
         end if;
      else
         L_vessel_id := I_key_value_2;
         L_voyage_flt_id := I_key_value_3;
         L_etd := TO_DATE(I_key_value_4, 'DD-MON-RR');
         if I_obligation_level = 'TRCO' then
            L_container_id := I_key_value_1;
         elsif I_obligation_level = 'TRCPO' then
            L_container_id := I_key_value_1;
            L_order_no := I_key_value_5;
         elsif I_obligation_level = 'TRBL' then
            L_bl_awb_id := I_key_value_1;
         elsif I_obligation_level = 'TRBLP' then
            L_bl_awb_id := I_key_value_1;
            L_order_no := I_key_value_5;
         elsif I_obligation_level in ('TRCP','TRBP') then
            L_order_no := I_key_value_5;
            L_item := I_key_value_6;
         else
            L_inv_obl_level := TRUE;
         end if;
      end if;
   elsif I_obligation_level = 'CUST' then
      O_handled := FALSE;
      return TRUE;
   else
      L_inv_obl_level := TRUE;
   end if;

   if L_inv_obl_level then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_obligation_level',
                                            I_obligation_level);
      return FALSE;
   end if;
   -- Get the maximum sequence number plus one.
   open  C_GET_ASN_PO_RCPT_QTY;
   fetch C_GET_ASN_PO_RCPT_QTY bulk collect into rcpt_tbl;
   close C_GET_ASN_PO_RCPT_QTY;
   --
   if rcpt_tbl.count > 0 then

      FOR i in rcpt_tbl.first..rcpt_tbl.last LOOP

         if ( rcpt_tbl(i).qty != 0 ) then
      --
            if rcpt_tbl(i).location is NULL then
               if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                              L_dist_table,          -- O_dist_tab: results table returned by distribution
                                              rcpt_tbl(i).item,      -- L_item
                                              rcpt_tbl(i).to_loc,  -- I_loc: phy_loc
                                              rcpt_tbl(i).qty + rcpt_tbl(i).prior_qty,       -- I_qty
                                              'ORDRCV',              -- I_CMI: calling module indicator
                                              NULL,                  -- I_inv_status
                                              NULL,                  -- I_to_loc_type,
                                              NULL,                  -- I_to_loc,
                                              rcpt_tbl(i).order_no,  -- I_order_no
                                              NULL,                  -- I_shipment
                                              NULL) = FALSE then     -- I_seq_no
                  return FALSE;
               end if;
               if rcpt_tbl(i).prior_qty != 0 then
                  if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                              L_prior_dist_tab,      -- O_dist_tab: results table returned by distribution
                                              rcpt_tbl(i).item,      -- L_item
                                              rcpt_tbl(i).to_loc,  -- I_loc: phy_loc
                                              rcpt_tbl(i).prior_qty, -- I_qty
                                              'ORDRCV',              -- I_CMI: calling module indicator
                                              NULL,                  -- I_inv_status
                                              NULL,                  -- I_to_loc_type,
                                              NULL,                  -- I_to_loc,
                                              rcpt_tbl(i).order_no,  -- I_order_no
                                              NULL,                  -- I_shipment
                                              NULL) = FALSE then     -- I_seq_no
                     return FALSE;
                  end if;
               end if;
               for v_loc in L_dist_table.first..L_dist_table.last loop
                  L_wh := L_dist_table(v_loc).wh;
                  L_qty := L_dist_table(v_loc).dist_qty;
                  --
                  if rcpt_tbl(i).prior_qty != 0 then
                     for v_prior in L_prior_dist_tab.first..L_prior_dist_tab.last loop
                        if L_prior_dist_tab(v_prior).wh = L_wh then
                           L_qty := L_qty - L_prior_dist_tab(v_prior).dist_qty;
                           exit;
                        end if;
                     end loop;
                  end if;
                  --
                  item_cnt                                := item_cnt + 1;
                  lineitem_table(item_cnt).order_no       := rcpt_tbl(i).order_no;
                  lineitem_table(item_cnt).shipment       := rcpt_tbl(i).shipment;
                  lineitem_table(item_cnt).vessel_id      := rcpt_tbl(i).vessel_id;
                  lineitem_table(item_cnt).voyage_flt_id  := rcpt_tbl(i).voyage_flt_id;
                  lineitem_table(item_cnt).etd            := rcpt_tbl(i).estimated_depart_date;
                  lineitem_table(item_cnt).item           := rcpt_tbl(i).item;
                  lineitem_table(item_cnt).location       := L_wh;
                  lineitem_table(item_cnt).location_type  := rcpt_tbl(i).to_loc_type;
                  lineitem_table(item_cnt).std_qty        := L_qty;
                  lineitem_table(item_cnt).uom_qty        := rcpt_tbl(i).weight;
                  lineitem_table(item_cnt).uom            := rcpt_tbl(i).weight_uom;
                  lineitem_table(item_cnt).supplier       := rcpt_tbl(i).supplier;
                  lineitem_table(item_cnt).origin_country := rcpt_tbl(i).origin_country_id;
                  lineitem_table(item_cnt).unit_cost      := rcpt_tbl(i).unit_cost;
               end loop;
            else
               item_cnt                                := item_cnt + 1;
               lineitem_table(item_cnt).order_no       := rcpt_tbl(i).order_no;
               lineitem_table(item_cnt).shipment       := rcpt_tbl(i).shipment;
               lineitem_table(item_cnt).vessel_id      := rcpt_tbl(i).vessel_id;
               lineitem_table(item_cnt).voyage_flt_id  := rcpt_tbl(i).voyage_flt_id;
               lineitem_table(item_cnt).etd            := rcpt_tbl(i).estimated_depart_date;
               lineitem_table(item_cnt).item           := rcpt_tbl(i).item;
               lineitem_table(item_cnt).location       := rcpt_tbl(i).location;
               lineitem_table(item_cnt).location_type  := rcpt_tbl(i).to_loc_type;
               lineitem_table(item_cnt).std_qty        := rcpt_tbl(i).qty;
               lineitem_table(item_cnt).uom_qty        := rcpt_tbl(i).weight;
               lineitem_table(item_cnt).uom            := rcpt_tbl(i).weight_uom;
               lineitem_table(item_cnt).supplier       := rcpt_tbl(i).supplier;
               lineitem_table(item_cnt).origin_country := rcpt_tbl(i).origin_country_id;
               lineitem_table(item_cnt).unit_cost      := rcpt_tbl(i).unit_cost;
            end if;
         end if;
         --
      end LOOP;
   end if;
   --
   if (item_cnt > 0) then
      if ALLOC_ITEM(O_error_message ,
                 lineitem_table,
                 I_obligation_key ,
                 I_obligation_level ,
                 I_key_value_1,
                 I_key_value_2,
                 I_key_value_3,
                 I_key_value_4,
                 I_key_value_5,
                 I_key_value_6 ) = FALSE then
          return FALSE;
      end if;
   elsif L_obl_level_type != 'ASN' then
      -- if it is not an ASN-level obligation we will give the other pacakage a chance to handle it
      O_handled := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ALLOC_ASN;
------------------------------------------------------------------------------------
FUNCTION INSERT_ALC_COMP_LOCS(O_error_message    IN OUT   VARCHAR2,
                              I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                              I_shipment         IN       SHIPMENT.SHIPMENT%TYPE,
                              I_item             IN       ITEM_MASTER.ITEM%TYPE,
                              I_pack_item        IN       ITEM_MASTER.ITEM%TYPE,
                              I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                              I_comp_id          IN       ELC_COMP.COMP_ID%TYPE,
                              I_location         IN       ORDLOC.LOCATION%TYPE,
                              I_loc_type         IN       ORDLOC.LOC_TYPE%TYPE,
                              I_act_value        IN       ALC_COMP_LOC.ACT_VALUE%TYPE,
                              I_qty              IN       ALC_COMP_LOC.QTY%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ALC_ASN_SQL.INSERT_ALC_COMP_LOCS';

BEGIN
   -- Expense comps will be reallocated at the end of the process
   if ALC_ALLOC_SQL.ADD_PO_TO_QUEUE(O_error_message,
                                    I_order_no) = FALSE then
      return FALSE;
   end if;

   merge into alc_comp_loc  acl
   using (select ah.order_no,
                 ah.seq_no,
                 I_comp_id   comp_id,
                 I_location  location,
                 I_loc_type  loc_type,
                 I_act_value act_value,
                 I_qty       qty,
                 LP_vdate    last_calc_date
            from alc_head ah
           where ah.order_no           = I_order_no
             and ah.item               = I_item
             and ((ah.pack_item        = I_pack_item) OR (nvl(I_pack_item,-1) = nvl(ah.pack_item,-1)))
             and shipment              = I_shipment
             and ah.obligation_key     = I_obligation_key) acl1
      on (    acl.order_no = acl1.order_no
          and acl.seq_no = acl1.seq_no
          and acl.comp_id = acl1.comp_id
          and acl.location = acl1.location)
   when matched then
      update set acl.act_value =  (acl.qty * acl.act_value + acl1.qty * acl1.act_value)/(acl1.qty + acl.qty)
   when not matched then
      insert (acl.order_no,
              acl.seq_no,
              acl.comp_id,
              acl.location,
              acl.loc_type,
              acl.act_value,
              acl.qty,
              acl.last_calc_date)
     values ( acl1.order_no,
              acl1.seq_no,
              acl1.comp_id,
              acl1.location,
              acl1.loc_type,
              acl1.act_value,
              acl1.qty,
              acl1.last_calc_date);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ALC_COMP_LOCS;
-----------------------------------------------------------------------------------------------
FUNCTION INSERT_ALC_HEAD(O_error_message    IN OUT   VARCHAR2,
                         I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                         I_shipment         IN       SHIPMENT.SHIPMENT%TYPE,
                         I_item             IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_item        IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_type        IN       ITEM_MASTER.PACK_TYPE%TYPE,
                         I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                         I_vessel_id        IN       TRANSPORTATION.VESSEL_ID%TYPE,
                         I_voyage_flt_id    IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                         I_etd              IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                         I_item_qty         IN       ORDLOC.QTY_RECEIVED%TYPE,
                         I_error_ind        IN       ALC_HEAD.ERROR_IND%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64)             := 'ALC_ASN_SQL.INSERT_ALC_HEAD';
   L_seq_no              ALC_HEAD.SEQ_NO%TYPE;
   L_status              ALC_HEAD.STATUS%TYPE;
   L_alc_head_exists     VARCHAR2(1)                   := 'N';

   --
   cursor C_ALC_HEAD_EXISTS is
      select 'Y'
        from alc_head
       where order_no = I_order_no
         and ((item             = I_item
               and pack_item   is NULL
               and NVL(I_pack_type,'N') != 'B')
           or (item             = I_item
               and pack_item    = I_pack_item
               and NVL(I_pack_type,'N') = 'B'))
         and shipment       = I_shipment
         and obligation_key = I_obligation_key
         for update nowait;
   --
   cursor C_GET_MAX_SEQ is
      select NVL(MAX(seq_no), 0) + 1
        from alc_head
       where order_no = I_order_no;

BEGIN

   -- Get the maximum sequence number plus one.
   open  C_GET_MAX_SEQ;
   fetch C_GET_MAX_SEQ into L_seq_no;
   close C_GET_MAX_SEQ;
   ---
   if I_obligation_key is NULL then
      L_status := 'E';  -- estimated expenses
   else
      L_status := 'P';  -- pending obligation
   end if;
   -- Check the existence of ALC_HEAD
   open C_ALC_HEAD_EXISTS;
   fetch C_ALC_HEAD_EXISTS into L_alc_head_exists;
   close C_ALC_HEAD_EXISTS;
   --
   if L_alc_head_exists = 'N' then
      -- insert into ALC_HEAD
      insert into alc_head (order_no,
                         item,
                         pack_item,
                         seq_no,
                         obligation_key,
                         vessel_id,
                         voyage_flt_id,
                         estimated_depart_date,
                         alc_qty,
                         status,
                         error_ind,
                         shipment)
                 values (I_order_no,
                         I_item,
                         I_pack_item,
                         L_seq_no,
                         I_obligation_key,
                         I_vessel_id,
                         I_voyage_flt_id,
                         I_etd,
                         I_item_qty,
                         L_status,            -- status = 'Pending'
                         I_error_ind,
                         I_shipment);   -- error flag
   else
      update alc_head a
         set a.alc_qty = a.alc_qty + I_item_qty
       where a.order_no     = I_order_no
         and shipment       = I_shipment
         and obligation_key = I_obligation_key
         and ((item = I_item
               and pack_item is NULL
               and NVL(I_pack_type,'N') != 'B')
              or (item = I_item
                  and pack_item = I_pack_item
                  and NVL(I_pack_type,'N') = 'B'));
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ALC_HEAD;
--------------------------------------------------------------------------------------------
FUNCTION CONVERT_OBL_AMT(O_error_message       IN OUT   VARCHAR2,
                         O_obl_comp_amt_prim   OUT      OBLIGATION_COMP.AMT%TYPE,
                         I_obl_comp_amt        IN       OBLIGATION_COMP.AMT%TYPE,
                         I_obligation_key      IN       OBLIGATION.OBLIGATION_KEY%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64)             := 'ALC_ASN_SQL.CONVERT_OBL_AMT';
   L_amt_prim           OBLIGATION_COMP.AMT%TYPE;
   L_uom_class          UOM_CLASS.UOM_CLASS%TYPE;
   L_currency_prim      CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate_prim CURRENCY_RATES.EXCHANGE_RATE%TYPE;

   cursor C_GET_CURR is
      select currency_code,
             exchange_rate
        from obligation
       where obligation_key = I_obligation_key;

BEGIN

   -- Get the Obligation's currency and exchange rate.
   open C_GET_CURR;
   fetch C_GET_CURR into LP_currency_obl,
                         LP_exchange_rate_obl;
   close C_GET_CURR;
   ---
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                       L_currency_prim) = FALSE then
      return FALSE;
   end if;
   ---
   if CURRENCY_SQL.GET_RATE(O_error_message,
                            L_exchange_rate_prim,
                            L_currency_prim,
                            NULL,
                            NULL) = FALSE then
      return FALSE;
   end if;
   ---
   -- Convert the obligation amount from obligation currency to primary currency.
   ---
   if CURRENCY_SQL.CONVERT(O_error_message,
                           I_obl_comp_amt,
                           LP_currency_obl,
                           L_currency_prim,  -- primary currency
                           O_obl_comp_amt_prim,
                           'N',
                           NULL,
                           NULL,
                           LP_exchange_rate_obl,
                           L_exchange_rate_prim) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONVERT_OBL_AMT;
-----------------------------------------------------------------------------------------------------
FUNCTION ASN_NO_EXISTS(O_error_message   IN OUT   VARCHAR2,
                       O_valid              OUT   BOOLEAN ,
                       I_asn_no          IN       SHIPMENT.ASN%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)       := 'ALC_ASN_SQL.ASN_NO_EXISTS';
   L_asn   SHIPMENT.ASN%TYPE;

   cursor C_CHECK_ASN_TB is
      select asn
        from shipment
       where asn = I_asn_no;

   cursor C_CHECK_ASN_V is
      select asn
        from v_shipment
       where asn = I_asn_no;

BEGIN

   if I_asn_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_asn_no,L_program,NULL);
      RETURN FALSE;
   end if;
   --
   O_valid := TRUE;
   --
   open  C_CHECK_ASN_V;
   fetch C_CHECK_ASN_V into L_asn;
   close C_CHECK_ASN_V;
   ---
   if L_asn is NULL then
      ---
      open  C_CHECK_ASN_TB;
      fetch C_CHECK_ASN_TB into L_asn;
      close C_CHECK_ASN_TB;
      ---
      if L_asn is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ASN', I_asn_no);
      end if;
       O_valid := FALSE;
      ---
   end if;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ASN_NO_EXISTS;
-----------------------------------------------------------------------------------------------------
END ALC_ASN_SQL;
/
