CREATE OR REPLACE PACKAGE TSF_ENTITY_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--Function Name: GET_ROW
--Purpose:       This function will check for the existence of tsf_entity and will populate
--               O_tsf_entity_row with the transfer entity row if it exists.
--Calls:         None
--Created:       26-JUNE-2003, by Rochelle Paz
--------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists          IN OUT  BOOLEAN,
                 O_tsf_entity_row  IN OUT  TSF_ENTITY%ROWTYPE,
                 I_tsf_entity_id   IN      TSF_ENTITY.TSF_ENTITY_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name: CHECK_DELETE
--Purpose:       This function will check if I_tsf_entity_id is associated with one or more
--               locations. If so, O_delete_ok = FALSE.
--Calls:         None
--Created:       26-JUNE-2003, by Rochelle Paz
--------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_delete_ok       IN OUT  BOOLEAN,
                      I_tsf_entity_id   IN      TSF_ENTITY.TSF_ENTITY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: CHECK_ORG_UNIT_LOC_EXISTS 
--Purpose:       This function will return TRUE if atleast one location is attached to the
--               input Tsf_entity and Org_unit.
--Calls:         None
--------------------------------------------------------------------
FUNCTION CHECK_ORG_UNIT_LOC_EXISTS (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists          IN OUT  BOOLEAN,
                                    I_tsf_entity_id   IN      TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                                    I_org_unit_id     IN      ORG_UNIT.ORG_UNIT_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: CHECK_TSF_ENT_WO_OU_EXISTS
--Purpose:       This function will check if all tsf entities are attached to an Org Unit
--               If not, O_exists = TRUE
--Calls:         None
--------------------------------------------------------------------
FUNCTION CHECK_TSF_ENT_WO_OU_EXISTS (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists          IN OUT  BOOLEAN)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: DELETE_TSF_ENTITY
--Purpose:       This function will delete all tsf entities not attached to an org unit
--Calls:         None
--------------------------------------------------------------------
FUNCTION DELETE_TSF_ENTITY (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--Function Name: DEL_TSF_ENT_FISCAL_ATTRIB
--Purpose:       This function will delete all fiscal attribute associated with the passed tsf_entity_id
--Calls:         None
--------------------------------------------------------------------
FUNCTION DEL_TSF_ENT_FISCAL_ATTRIB (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_tsf_entity_id   IN       TSF_ENTITY_L10N_EXT.TSF_ENTITY_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
END TSF_ENTITY_SQL;
/
