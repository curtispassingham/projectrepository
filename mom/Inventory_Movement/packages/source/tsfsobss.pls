
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TSFSOBS_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------------------
TYPE tsf_entity_org_unit_sob_rec IS RECORD(org_unit_id         TSF_ENTITY_ORG_UNIT_SOB.ORG_UNIT_ID%TYPE,
                                           org_unit_desc       ORG_UNIT.DESCRIPTION%TYPE,
                                           set_of_books_id     TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE,
                                           set_of_books_desc   FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE,
                                           return_code         VARCHAR2(5),
                                           error_message       RTK_ERRORS.RTK_TEXT%TYPE);

TYPE tsf_entity_org_unit_sob_tbl IS TABLE OF tsf_entity_org_unit_sob_rec INDEX BY BINARY_INTEGER;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE
--- Purpose:        This procedure will query TSF_ENTITY_ORG_UNIT_SOB table and populate the above declared
---                 table of records that will be used as the 'base table' in the Transfer Set of Books form.
------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (IO_tsf_entity_org_unit_sob_tbl IN OUT  TSFSOBS_SQL.TSF_ENTITY_ORG_UNIT_SOB_TBL,
                           I_tsf_entity                   IN      TSF_ENTITY_ORG_UNIT_SOB.TSF_ENTITY_ID%TYPE);
------------------------------------------------------------------------------------------------------------
--- Procedure Name: DELETE_PROCEDURE
--- Purpose:        This procedure will delete the corresponding records from TSF_ENTITY_ORG_UNIT_SOB table 
---                 for the inputs I_tsf_entity and the set_of_books_id, org_unit_id in the IN OUT table rec.
------------------------------------------------------------------------------------------------------------
PROCEDURE DELETE_PROCEDURE (IO_tsf_entity_org_unit_sob_tbl IN OUT TSFSOBS_SQL.TSF_ENTITY_ORG_UNIT_SOB_TBL,
                            I_tsf_entity                   IN     TSF_ENTITY_ORG_UNIT_SOB.TSF_ENTITY_ID%TYPE);
------------------------------------------------------------------------------------------------------------
--- Procedure Name: INSERT_PROCEDURE
--- Purpose:        This procedure will insert data in the TSF_ENTITY_ORG_UNIT_SOB table
---                 from the Transfer Set of Books form.
------------------------------------------------------------------------------------------------------------
PROCEDURE INSERT_PROCEDURE (IO_tsf_entity_org_unit_sob_tbl IN OUT TSFSOBS_SQL.TSF_ENTITY_ORG_UNIT_SOB_TBL,
                            I_tsf_entity                   IN     TSF_ENTITY_ORG_UNIT_SOB.TSF_ENTITY_ID%TYPE);
------------------------------------------------------------------------------------------------------------
--- Procedure Name: LOCK_PROCEDURE
--- Purpose:        This procedure will lock the queried records in TSF_ENTITY_ORG_UNIT_SOB table
------------------------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE (IO_tsf_entity_org_unit_sob_tbl IN OUT TSFSOBS_SQL.TSF_ENTITY_ORG_UNIT_SOB_TBL,
                          I_tsf_entity                   IN     TSF_ENTITY_ORG_UNIT_SOB.TSF_ENTITY_ID%TYPE);
------------------------------------------------------------------------------------------------------------
END TSFSOBS_SQL;
/