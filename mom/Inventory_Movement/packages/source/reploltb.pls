CREATE OR REPLACE PACKAGE BODY REPL_OLT_SQL AS

-- This varray is intended to hold delivery schedule days.
TYPE deliv_days_table_type IS TABLE OF source_dlvry_sched_days.day%TYPE;

-------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC_REVIEW_TIME(O_error_message    IN OUT VARCHAR2,
                                  O_review_time      IN OUT repl_item_loc.review_cycle%TYPE,
                                  I_item             IN     repl_item_loc.item%TYPE,
                                  I_loc              IN     repl_item_loc.location%TYPE,
                                  I_review_cycle     IN     repl_item_loc.review_cycle%TYPE,
                                  I_today            IN     sup_repl_day.weekday%TYPE)
RETURN BOOLEAN IS

   L_count             NUMBER(1)                 := 0;
   L_today             sup_repl_day.weekday%TYPE := I_today;
   L_min_day_greater   sup_repl_day.weekday%TYPE := NULL;
   L_min_day_less      sup_repl_day.weekday%TYPE := NULL;

   cursor C_GET_DAY_OF_WEEK is
      select to_number(to_char(period.vdate, 'D'))
        from period;

   cursor C_GET_COUNT is
      select count(*)
        from repl_day r
       where r.item      = I_item
         and r.location  = I_loc;

   cursor C_GET_MIN_GREATER is
      select nvl(min(r.weekday),0)
        from repl_day r
       where r.weekday  > L_today
         and r.item      = I_item
         and r.location  = I_loc;

   cursor C_GET_MIN_LESS is
      select nvl(min(r.weekday),0)
        from repl_day r
       where r.weekday  < L_today
         and r.item      = I_item
         and r.location  = I_loc;

BEGIN

   -- if review cycle is 1, then the review time is set to 1
   if I_review_cycle = '1' then
      O_review_time := '1';

      return TRUE;
   end if;

   -- if review cycle is 0, then the review time is dependent on review times/week
   if I_review_cycle = '0' then
      --
      -- Get the count from repl_day.  This represents the number of times the item/location
      -- is reviewed per week.
      --
      SQL_LIB.SET_MARK('OPEN', 'C_GET_COUNT', 'REPL_DAY',
                       'Item: ' ||I_item || 'Location: ' ||to_char(I_loc));
      open C_GET_COUNT;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_COUNT', 'REPL_DAY',
                       'Item: ' ||I_item || 'Location: ' ||to_char(I_loc));
      fetch C_GET_COUNT into L_count;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_COUNT', 'REPL_DAY',
                       'Item: ' ||I_item || 'Location: ' ||to_char(I_loc));
      close C_GET_COUNT;

      --
      -- If the count 1, then the item/location is reviewed once a week and the time to
      -- the next review is the review cycle time which is 7 days.
      --
      if L_count = 1 then

         O_review_time := '7';

         return TRUE;
      end if;
   ---
      if L_today is NULL then
         SQL_LIB.SET_MARK('OPEN', 'C_GET_DAY_OF_WEEK', 'PERIOD', NULL);
         open  C_GET_DAY_OF_WEEK;
         SQL_LIB.SET_MARK('FETCH', 'C_GET_DAY_OF_WEEK', 'PERIOD', NULL);
         fetch C_GET_DAY_OF_WEEK into L_today;
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_DAY_OF_WEEK', 'PERIOD', NULL);
         close C_GET_DAY_OF_WEEK;
      end if;
      --
      -- Get the min review day that is greater than today.
      --
      SQL_LIB.SET_MARK('OPEN', 'C_GET_MIN_GREATER', 'REPL_DAY',
                       'Item: ' ||I_item || 'Location: ' ||to_char(I_loc));
      open C_GET_MIN_GREATER;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_MIN_GREATER', 'REPL_DAY',
                       'Item: ' ||I_item || 'Location: ' ||to_char(I_loc));
      fetch C_GET_MIN_GREATER into L_min_day_greater;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_MIN_GREATER', 'REPL_DAY',
                       'Item: ' ||I_item || 'Location: ' ||to_char(I_loc));
      close C_GET_MIN_GREATER;

      --
      -- Get the min review day that is less than today.
      --
      SQL_LIB.SET_MARK('OPEN', 'C_GET_MIN_LESS', 'REPL_DAY',
                       'Item: ' ||I_item || 'Location: ' ||to_char(I_loc));
      open C_GET_MIN_LESS;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_MIN_LESS', 'REPL_DAY',
                       'Item: ' ||I_item || 'Location: ' ||to_char(I_loc));
      FETCH C_GET_MIN_LESS into L_min_day_less;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_MIN_LESS', 'REPL_DAY',
                       'Item: ' ||I_item || 'Location: ' ||to_char(I_loc));
      close C_GET_MIN_LESS;
      --
      -- If there is no review date greater than today, L_min_day_greater will
      -- have been set to zero.
      --
      if L_min_day_greater = 0 then
         O_review_time := (L_min_day_less - L_today) + 7;
      else
         O_review_time := L_min_day_greater - L_today;
      end if;

      return TRUE;
   end if;

   -- if review cycle is not 0 or 1, then the review time is set to 7 times the review cycle
   -- I_review_cycle != '1'  or '0'
   O_review_time := to_char(to_number(I_review_cycle) * 7);

   return TRUE;


EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                    SQLERRM,
                        'REPL_OLT_SQL.GET_ITEM_LOC_REVIEW_TIME',
                    to_char(SQLCODE));
   RETURN FALSE;

END GET_ITEM_LOC_REVIEW_TIME;
------------------------------------------------------------------------------------------
FUNCTION GET_DELIV_DAYS(O_error_message      IN OUT   VARCHAR2,
                        O_deliv_days_table   IN OUT   DELIV_DAYS_TABLE_TYPE,
                        O_start_date         IN OUT   SOURCE_DLVRY_SCHED.START_DATE%TYPE,
                        O_deliv_cycle        IN OUT   SOURCE_DLVRY_SCHED.DELIVERY_CYCLE%TYPE,
                        I_item               IN       ITEM_MASTER.ITEM%TYPE,
                        I_location           IN       WH.WH%TYPE,
                        I_source             IN       SOURCE_DLVRY_SCHED_DAYS.SOURCE%TYPE,
                        I_source_type        IN       SOURCE_DLVRY_SCHED_DAYS.SOURCE_TYPE%TYPE)
RETURN BOOLEAN IS

   L_loop_counter   NUMBER;

   cursor C_GET_START_DATE is
      select start_date,
             delivery_cycle
        from source_dlvry_sched
       where source = I_source
         and location = I_location
         and source_type = I_source_type;

   cursor C_GET_DELIV_DAYS is
      select day
        from source_dlvry_sched_days sld
       where location = I_location
         and source = I_source
         and source_type = I_source_type
         and NOT EXISTS
             (select 1
                from source_dlvry_sched_exc sle
               where sld.source = sle.source
                 and sld.source_type = sle.source_type
                 and sld.location = sle.location
                 and sld.day = sle.day
                 and sle.item = I_item)
       order by day;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_DELIV_DAYS', 'SOURCE_DLVRY_SCHED_DAYS', 'Location: ' ||TO_CHAR(I_location));
   open C_GET_DELIV_DAYS;

   L_loop_counter := 1;

   LOOP
      SQL_LIB.SET_MARK('FETCH', 'C_GET_DELIV_DAYS', 'SOURCE_DLVRY_SCHED_DAYS', 'Location: ' ||TO_CHAR(I_location));
      fetch C_GET_DELIV_DAYS into O_deliv_days_table(L_loop_counter);

      EXIT WHEN C_GET_DELIV_DAYS%NOTFOUND;

      L_loop_counter := L_loop_counter + 1;
   END LOOP;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_DELIV_DAYS', 'SOURCE_DLVRY_SCHED_DAYS', 'Location: ' ||TO_CHAR(I_location));
   close C_GET_DELIV_DAYS;

   SQL_LIB.SET_MARK('OPEN', 'C_GET_START_DATE', 'SOURCE_DLVRY_SCHED', 'Location: ' ||TO_CHAR(I_location));
   open C_GET_START_DATE;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_START_DATE', 'SOURCE_DLVRY_SCHED', 'Location: ' ||TO_CHAR(I_location));
   fetch C_GET_START_DATE into O_start_date,
                               O_deliv_cycle;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_START_DATE', 'SOURCE_DLVRY_SCHED', 'Location: ' ||TO_CHAR(I_location));
   close C_GET_START_DATE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.GET_DELIV_DAYS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_DELIV_DAYS;
------------------------------------------------------------------------------------------
FUNCTION GET_TRUE_START_DATE(O_error_message           IN OUT VARCHAR2,
                             O_true_start_date         IN OUT source_dlvry_sched.start_date%TYPE,
                             I_deliv_days_table        IN     deliv_days_table_type,
                             I_start_date              IN     source_dlvry_sched.start_date%TYPE)
RETURN BOOLEAN IS

L_deliv_day                  source_dlvry_sched_days.day%TYPE;
L_first_deliv_day            source_dlvry_sched_days.day%TYPE;

BEGIN
   O_true_start_date := NULL;

   if I_deliv_days_table.EXISTS(1) = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('DELIV_DAYS_TABLE_FAILED',
                                                                           null,
                                                                           null,
                                                                           null);
      return FALSE;
   end if;

   -- Find the true start date.

   FOR L_table_index IN 1..I_deliv_days_table.COUNT LOOP

      L_deliv_day := I_deliv_days_table(L_table_index);

      if L_deliv_day >= to_number(to_char(I_start_date, 'D')) then
         O_true_start_date := I_start_date + (L_deliv_day - to_number(to_char(I_start_date, 'D')));
         EXIT;
      elsif L_table_index = 1 then
         L_first_deliv_day := L_deliv_day;
      end if;

   END LOOP;

   if O_true_start_date is NULL then
      O_true_start_date := I_start_date + ((7 - (to_number(to_char(I_start_date, 'D')))) + L_first_deliv_day);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.GET_TRUE_START_DATE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_TRUE_START_DATE;
------------------------------------------------------------------------------------------
FUNCTION ADJUST_DDATE_FOR_DDAYS(O_error_message           IN OUT VARCHAR2,
                                O_colt                    IN OUT NUMBER,
                                O_delivery_date           IN OUT period.vdate%TYPE,
                                I_deliv_days_table        IN     deliv_days_table_type,
                                I_repl_date               IN     repl_results.repl_date%TYPE,
                                I_olt                     IN     NUMBER)
RETURN BOOLEAN IS

L_deliv_day                  source_dlvry_sched_days.day%TYPE;
L_first_deliv_day            source_dlvry_sched_days.day%TYPE;

BEGIN

   if I_deliv_days_table.EXISTS(1) = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('DELIV_DAYS_TABLE_FAILED',
                                                                           null,
                                                                           null,
                                                                           null);
      return FALSE;
   end if;

   FOR L_table_index IN 1..I_deliv_days_table.COUNT LOOP

      L_deliv_day := I_deliv_days_table(L_table_index);

      /********************************************************
      * The first user specified delivery day found that      *
      * is later in the week than the initially calculated    *
      * delivery day will be the next delivery day. Therefore *
      * the loop may be exited because the next delivery      *
      * day has been found.                                   *
      ********************************************************/
      if L_deliv_day >= to_number(to_char((I_repl_date + I_olt), 'D')) then

         -- The delivery date will be calculated by adding the number of days a user
         -- specified delivery day imposes onto the initially calculated delivery day.
         O_delivery_date := (I_repl_date + I_olt) + (L_deliv_day - to_number(to_char((I_repl_date + I_olt), 'D')));

         -- The current order lead time will also add on the number of days a user
         -- specified delivery day imposes onto the initial OLT.
         O_colt := I_olt + (L_deliv_day - to_number(to_char((I_repl_date + I_olt), 'D')));

         EXIT;

      /************************************************************
      * If this is the fist time through the loop and the first   *
      * user specified delivery day found is not later than       *
      * the calculated delivery day, then it will be preserved    *
      * in case no user specified delivery days are found which   *
      * occur later in the week than the calculated delivery day. *
      * In that case, the first delivery day of the next week     *
      * will be the next delivery day.                            *
      ************************************************************/
      elsif L_table_index = 1 then
         L_first_deliv_day := L_deliv_day;
      end if;

   END LOOP;

   /***************************************************************
   * Since we know the loop executed at least once because of our *
   * existence check, if no delivery date has been specified      *
   * the first delivery day found will be used to calculate the   *
   * delivery date. This will be in the next week.                *
   ***************************************************************/
   if O_delivery_date is NULL then
      O_delivery_date := (I_repl_date + I_olt) + ((7 - (to_number(to_char((I_repl_date + I_olt), 'D')))) + L_first_deliv_day);
      O_colt := I_olt + ((7 - (to_number(to_char((I_repl_date + I_olt), 'D')))) + L_first_deliv_day);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.ADJUST_DDATE_FOR_DDAYS',
                                            to_char(SQLCODE));
      return FALSE;
END ADJUST_DDATE_FOR_DDAYS;
------------------------------------------------------------------------------------------
FUNCTION ADJUST_DDATE_FOR_START_WEEK(O_error_message           IN OUT VARCHAR2,
                                     IO_colt                   IN OUT NUMBER,
                                     IO_delivery_date          IN OUT period.vdate%TYPE,
                                     I_true_start_date         IN     source_dlvry_sched.start_date%TYPE,
                                     I_delivery_cycle          IN     source_dlvry_sched.delivery_cycle%TYPE)
RETURN BOOLEAN IS

L_num_of_weeks_since_start   NUMBER;
L_loop_count                 NUMBER;

BEGIN

   -- If the start date has not occured yet, the delivery date will be
   -- the true start date.
   if IO_delivery_date < I_true_start_date then
      IO_colt := IO_colt + (I_true_start_date - IO_delivery_date);
      IO_delivery_date := I_true_start_date;

   /****************************************************
   * If the delivery cycle is more than once a week    *
   * we have to make sure that our delivery date is a  *
   * multiple of the delivery cycle away from the true *
   * start date.                                       *
   ****************************************************/
   elsif to_number(I_delivery_cycle) > 1 then

      L_num_of_weeks_since_start := (IO_delivery_date - I_true_start_date)/7;

      L_loop_count := 0;

      L_num_of_weeks_since_start := round(L_num_of_weeks_since_start);

      LOOP

         -- if the deliv date is in the right week, exit the loop
         if mod(L_num_of_weeks_since_start,to_number(I_delivery_cycle)) = 0 then
            IO_colt := IO_colt + (L_loop_count * 7);
            IO_delivery_date := IO_delivery_date + (L_loop_count * 7);
            EXIT;
         end if;

         -- if it's not in the right week, add a week and check again
         L_num_of_weeks_since_start := L_num_of_weeks_since_start + 1;

         L_loop_count := L_loop_count + 1;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.ADJUST_DDATE_FOR_START_WEEK',
                                            to_char(SQLCODE));
      return FALSE;
END ADJUST_DDATE_FOR_START_WEEK;
------------------------------------------------------------------------------------------
FUNCTION GET_DELIV_DATE(O_error_message           IN OUT VARCHAR2,
                        O_colt                    IN OUT NUMBER,
                        O_delivery_date           IN OUT period.vdate%TYPE,
                        I_deliv_days_table        IN     deliv_days_table_type,
                        I_repl_date               IN     repl_results.repl_date%TYPE,
                        I_olt                     IN     NUMBER,
                        I_start_date              IN     source_dlvry_sched.start_date%TYPE,
                        I_delivery_cycle          IN     source_dlvry_sched.delivery_cycle%TYPE)
RETURN BOOLEAN IS

BEGIN

               /*** IMPORTANT ASSUMPTION ***/
   /*** The delivery day table must be ordered by day ***/
   /*** Otherwise the following logic will not work ****/

   -- If there are any delivery days, find the next delivery date.
   if I_deliv_days_table(1) is not NULL then

      /*** Find the next delivery date ***/
      if ADJUST_DDATE_FOR_DDAYS(O_error_message,
                                O_colt,
                                O_delivery_date,
                                I_deliv_days_table,
                                I_repl_date,
                                I_olt) = FALSE then
         return FALSE;
      end if;

      /*** Determine if the delivery date is in the right week ***/

      if ADJUST_DDATE_FOR_START_WEEK(O_error_message,
                                     O_colt,
                                     O_delivery_date,
                                     I_start_date,
                                     I_delivery_cycle) = FALSE then
         return FALSE;
      end if;

   else -- No delivery days have been specified.
      O_delivery_date := I_repl_date + I_olt;
      O_colt := I_olt;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.GET_DELIV_DATE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_DELIV_DATE;
------------------------------------------------------------------------------------------
FUNCTION OPEN_FOR_RECEIPTS(O_error_message      IN OUT VARCHAR2,
                           O_open_ind           IN OUT BOOLEAN,
                           I_location           IN     wh.wh%TYPE,
                           I_date               IN     period.vdate%TYPE,
                           I_loc_activity_ind   IN     system_options.loc_activity_ind%TYPE)
RETURN BOOLEAN IS

L_dummy   VARCHAR2(1);

cursor C_LOC_CLOSED is
   select 'x'
     from location_closed
    where location = I_location
      and close_date = I_date
      and recv_ind = 'Y';

cursor C_COMP_CLOSED_EXCEP is
   select 'x'
     from company_closed
    where close_date = I_date
      and not exists(select 'x'
                      from company_closed_excep
                     where location = I_location
                       and close_date = I_date
                       and recv_ind = 'Y');

BEGIN

   -- The system option loc_activity_ind determines whether or
   -- not the calender tables will be used.
   if I_loc_activity_ind = 'Y' then
      SQL_LIB.SET_MARK('OPEN', 'C_LOC_CLOSED', 'LOCATION_CLOSED', 'Location: ' ||to_char(I_location));
      open C_LOC_CLOSED;
      SQL_LIB.SET_MARK('FETCH', 'C_LOC_CLOSED', 'LOCATION_CLOSED', 'Location: ' ||to_char(I_location));
      fetch C_LOC_CLOSED into L_dummy;

      if C_LOC_CLOSED%FOUND then

         /****************************************************
         * The loc closed table will override the exceptions *
         * table. If the location is specified as closed for *
         * receipts on this table, no further processing is  *
         * neccesary.                                        *
         ****************************************************/
         O_open_ind := FALSE;
      else
         SQL_LIB.SET_MARK('OPEN', 'C_COMP_CLOSED_EXCEP', 'COMPANY_CLOSED', 'Location: ' ||to_char(I_location));
         open C_COMP_CLOSED_EXCEP;
         SQL_LIB.SET_MARK('FETCH', 'C_COMP_CLOSED_EXCEP', 'COMPANY_CLOSED', 'Location: ' ||to_char(I_location));
         fetch C_COMP_CLOSED_EXCEP into L_dummy;

         if C_COMP_CLOSED_EXCEP%FOUND then
            O_open_ind := FALSE;
         else
            O_open_ind := TRUE;
         end if;

         SQL_LIB.SET_MARK('CLOSE', 'C_COMP_CLOSED_EXCEP', 'COMPANY_CLOSED', 'Location: ' ||to_char(I_location));
         close C_COMP_CLOSED_EXCEP;
      end if;

      SQL_LIB.SET_MARK('CLOSE', 'C_LOC_CLOSED', 'LOCATION_CLOSED', 'Location: ' ||to_char(I_location));
      close C_LOC_CLOSED;
   else

      -- The calender is not being used. Therefore all
      -- locs are always open.
      O_open_ind := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.OPEN_FOR_RECEIPTS',
                                            to_char(SQLCODE));
      return FALSE;
END OPEN_FOR_RECEIPTS;
------------------------------------------------------------------------------------------
FUNCTION GET_OPEN_DELIV_DATE_NEXT_DAY(O_error_message       IN OUT VARCHAR2,
                                      O_olt                 IN OUT NUMBER,
                                      O_deliv_date          IN OUT period.vdate%TYPE,
                                      O_days_added_to_olt   IN OUT NUMBER,
                                      IO_last_dd            IN OUT period.vdate%TYPE,
                                      I_deliv_days_table    IN     deliv_days_table_type,
                                      I_repl_date           IN     period.vdate%TYPE,
                                      I_olt                 IN     NUMBER,
                                      I_location            IN     wh.wh%TYPE,
                                      I_loc_activity_ind    IN     system_options.loc_activity_ind%TYPE,
                                      I_start_date          IN     source_dlvry_sched.start_date%TYPE,
                                      I_delivery_cycle      IN     source_dlvry_sched.delivery_cycle%TYPE)
RETURN BOOLEAN IS

L_open_ind        BOOLEAN;
L_loop_counter    NUMBER;
L_no_days_found   BOOLEAN;

L_start_date_temp DATE;
L_repl_date_temp  DATE;
L_olt_temp        NUMBER;
L_O_olt_temp      NUMBER;

L_run_again       NUMBER;

BEGIN

   L_start_date_temp := I_start_date;
   L_repl_date_temp  := I_repl_date;
   L_olt_temp        := I_olt;

   L_run_again := 0;

   LOOP

      O_deliv_date := null;
      L_O_olt_temp := null;

      if GET_DELIV_DATE(O_error_message,
                        L_O_olt_temp,          -- O_olt,
                        O_deliv_date,
                        I_deliv_days_table,
                        L_repl_date_temp,      -- I_repl_date,
                        L_olt_temp,            -- I_olt,
                        L_start_date_temp,     -- I_start_date,
                        I_delivery_cycle) = FALSE then
         return FALSE;
      end if;

      O_olt := O_olt + L_O_olt_temp;

      IO_last_dd := O_deliv_date;

      L_loop_counter := 0;

      LOOP
         if OPEN_FOR_RECEIPTS(O_error_message,
                              L_open_ind,
                              I_location,
                              O_deliv_date,
                              I_loc_activity_ind) = FALSE then
            return FALSE;
         end if;

         if L_open_ind = TRUE then
            O_olt := O_olt + L_loop_counter;
            O_days_added_to_olt := O_days_added_to_olt + L_loop_counter;

            if (L_loop_counter > 0) then
               L_run_again := 1;
            end if;

            EXIT;
         end if;

         O_deliv_date := O_deliv_date + 1;
         L_loop_counter := L_loop_counter + 1;

        if L_loop_counter = 365 then
            L_no_days_found :=TRUE;
            EXIT;
         end if;
      END LOOP;

      L_start_date_temp := L_start_date_temp + O_olt;
      L_repl_date_temp  := O_deliv_date;
      L_olt_temp        := 0;

      if L_no_days_found = TRUE then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DELIV_DAYS',
                                               to_char(I_location),
                                               'REPL_OLT_SQL.GET_OPEN_DELIV_DATE_NEXT_DAY',
                                                null);
         return FALSE;
      end if;

   EXIT when L_run_again = 0;
      L_run_again := 0;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.GET_OPEN_DELIV_DATE_NEXT_DAY',
                                            to_char(SQLCODE));
      return FALSE;
END GET_OPEN_DELIV_DATE_NEXT_DAY;
------------------------------------------------------------------------------------------
FUNCTION GET_OPEN_DELIV_DATE_NEXT_DD(O_error_message       IN OUT VARCHAR2,
                                     O_olt                 IN OUT NUMBER,
                                     O_deliv_date          IN OUT period.vdate%TYPE,
                                     O_days_added_to_olt   IN OUT NUMBER,
                                     IO_last_dd            IN OUT period.vdate%TYPE,
                                     I_deliv_days_table    IN     deliv_days_table_type,
                                     I_repl_date           IN     period.vdate%TYPE,
                                     I_olt                 IN     NUMBER,
                                     I_location            IN     wh.wh%TYPE,
                                     I_loc_activity_ind    IN     system_options.loc_activity_ind%TYPE,
                                     I_start_date          IN     source_dlvry_sched.start_date%TYPE,
                                     I_delivery_cycle      IN     source_dlvry_sched.delivery_cycle%TYPE)
RETURN BOOLEAN IS

L_orig_olt        NUMBER;
L_loop_count      NUMBER := 0;
L_date            period.vdate%TYPE;
L_olt             NUMBER;
L_open_ind        BOOLEAN;
L_no_days_found   BOOLEAN;
L_last_dd         period.vdate%TYPE;

BEGIN

   LOOP
      L_loop_count := L_loop_count + 1;

      if L_loop_count = 1 then
         L_date := I_repl_date;
         L_olt := I_olt;
      else
         L_date       := O_deliv_date;
         O_deliv_date := NULL;
         L_olt := 1;
      end if;

      if GET_DELIV_DATE(O_error_message,
                        O_olt,
                        O_deliv_date,
                        I_deliv_days_table,
                        L_date,
                        L_olt,
                        I_start_date,
                        I_delivery_cycle) = FALSE then
         return FALSE;
      end if;

      if L_loop_count = 1 then
         L_orig_olt := O_olt;
         IO_last_dd := O_deliv_date;
      end if;

      if OPEN_FOR_RECEIPTS(O_error_message,
                           L_open_ind,
                           I_location,
                           O_deliv_date,
                           I_loc_activity_ind) = FALSE then
         return FALSE;
      end if;

      if L_open_ind = TRUE then
         O_olt := O_deliv_date - I_repl_date;
         O_days_added_to_olt := O_olt - L_orig_olt;
         EXIT;
      end if;

      if L_loop_count = 365 then
         L_no_days_found :=TRUE;
         EXIT;
      end if;
   END LOOP;

   if L_no_days_found = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DELIV_DAYS',
                                            to_char(I_location),
                                            'REPL_OLT_SQL.GET_OPEN_DELIV_DATE_NEXT_DAY',
                                             null);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.GET_OPEN_DELIV_DATE_NEXT_DD',
                                            to_char(SQLCODE));
      return FALSE;
END GET_OPEN_DELIV_DATE_NEXT_DD;
------------------------------------------------------------------------------------------
FUNCTION CALC_OLTS_AND_RT(O_error_message        IN OUT   VARCHAR2,
                          O_colt                 IN OUT   NUMBER,
                          O_nolt                 IN OUT   NUMBER,
                          O_review_time          IN OUT   NUMBER,
                          O_days_added_to_colt   IN OUT   NUMBER,
                          O_days_added_to_nolt   IN OUT   NUMBER,
                          O_current_deliv_date   IN OUT   PERIOD.VDATE%TYPE,
                          O_next_deliv_date      IN OUT   PERIOD.VDATE%TYPE,
                          IO_last_dd             IN OUT   PERIOD.VDATE%TYPE,
                          I_repl_date            IN       PERIOD.VDATE%TYPE,
                          I_olt                  IN       NUMBER,
                          I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                          I_location             IN       WH.WH%TYPE,
                          I_xdock_store          IN       STORE.STORE%TYPE,
                          I_physical_loc         IN       WH.PHYSICAL_WH%TYPE,
                          I_stock_cat            IN       REPL_ITEM_LOC.STOCK_CAT%TYPE,
                          I_supplier             IN       SOURCE_DLVRY_SCHED_DAYS.SOURCE%TYPE,
                          I_review_cycle         IN       REPL_RESULTS.REVIEW_CYCLE%TYPE,
                          I_loc_activity_ind     IN       SYSTEM_OPTIONS.LOC_ACTIVITY_IND%TYPE,
                          I_loc_dlvry_ind        IN       SYSTEM_OPTIONS.LOC_DLVRY_IND%TYPE,
                          I_supp_dlvry_policy    IN       SUPS.DELIVERY_POLICY%TYPE,
                          I_thread_id            IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN IS

   L_next_repl_date       PERIOD.VDATE%TYPE;
   L_deliv_days_table     DELIV_DAYS_TABLE_TYPE;
   L_start_date           SOURCE_DLVRY_SCHED.START_DATE%TYPE;
   L_deliv_cycle          SOURCE_DLVRY_SCHED.DELIVERY_CYCLE%TYPE;
   L_dummy                PERIOD.VDATE%TYPE;
   L_review_loc           STORE.STORE%TYPE;
   L_colt                 RPL_NET_INVENTORY_TMP.CURR_ORDER_LEAD_TIME%TYPE;
   L_nolt                 RPL_NET_INVENTORY_TMP.NEXT_ORDER_LEAD_TIME%TYPE;
   L_curr_deliv_date      PERIOD.VDATE%TYPE;
   L_next_deliv_date      PERIOD.VDATE%TYPE;
   L_days_added_to_colt   RPL_NET_INVENTORY_TMP.DAYS_ADDED_TO_COLT%TYPE;
   L_days_added_to_nolt   RPL_NET_INVENTORY_TMP.DAYS_ADDED_TO_NOLT%TYPE;
   L_last_dd              REPL_ITEM_LOC.LAST_DELIVERY_DATE%TYPE;
   L_olt                  NUMBER;
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_location             ITEM_LOC.LOC%TYPE;
   L_supp_lead_time       RPL_NET_INVENTORY_TMP.SUPP_LEAD_TIME%TYPE;
   --
   L_obj_olts_rec         "OBJ_OLTS_REC" := NULL;
   L_obj_olts_tbl         "OBJ_OLTS_TBL" := NULL;
   cursor C_OLTS is
      select I_item,
             I_locn,
             case
             when I_locn_type = 'S' and I_stock_cat in ('C','L') then
                I_source_wh
             else
                I_locn
             end locn,
             I_last_delivery_date,
             I_day_1,
             I_day_2,
             I_day_3,
             I_day_4,
             I_day_5,
             I_day_6,
             I_day_7,
             I_date,
             I_sup_delivery_policy,
             I_true_start_date,
             I_deliv_cycle,
             I_supp_lead_time,
             I_pickup_lead_time,
             I_review_lead_time,
             thread_id
        from svc_repl_roq_gtt
       where thread_id = I_thread_id
         and ((I_locn_type = 'W')
               or (I_locn_type = 'S' and I_stock_cat in ('C','D','L')));

BEGIN
   -- This function is intended to calculate olts from the supplier
   -- to either a final destination or to a source warehouse
   -- for a crossdock order.

   L_deliv_days_table := deliv_days_table_type(NULL, NULL, NULL, NULL, NULL, NULL, NULL);

   if I_loc_dlvry_ind = 'Y' then
      --
      if I_thread_id is NULL then
         if GET_DELIV_DAYS(O_error_message,
                           L_deliv_days_table,
                           L_start_date,
                           L_deliv_cycle,
                           I_item,
                           I_physical_loc,
                           I_supplier,
                           'SUP') = FALSE then
            return FALSE;
         end if;

         if l_deliv_days_table(1) is not NULL then
            if GET_TRUE_START_DATE(O_error_message,
                                   L_start_date,
                                   L_deliv_days_table,
                                   L_start_date) = FALSE then
               return FALSE;
            end if;
         end if;

      else --do bulk processing
         merge into svc_repl_roq_gtt target
         using (
                select wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id,
                       sd.source,
                       sd.source_type,
                       sd.location,
                       /* values fetch from the GET_DELIV_DAYS function */
                       sd.start_date,
                       sd.delivery_cycle,
                       /* values fetch from GET_TRUE_START_DATE function */
                       case
                          when sld.day_1 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_1 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_2 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_2 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_3 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_3 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_4 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_4 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_5 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_5 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_6 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_6 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_7 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_7 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          else
                             sd.start_date + ((7 - (TO_NUMBER(TO_CHAR(sd.start_date, 'D'))))
                                              + case when sld.day_1 > 0 then sld.day_1
                                                     when sld.day_2 > 0 then sld.day_2
                                                     when sld.day_3 > 0 then sld.day_3
                                                     when sld.day_4 > 0 then sld.day_4
                                                     when sld.day_5 > 0 then sld.day_5
                                                     when sld.day_6 > 0 then sld.day_6
                                                     when sld.day_7 > 0 then sld.day_7
                                                 end)
                       end true_start_date,
                       /* values fetch from GET_DELIV_DAYS */
                       /* the pl/sql table type variable L_deliv_days_table */
                       /* have been replaced by these columns */
                       sld.day_1,
                       sld.day_2,
                       sld.day_3,
                       sld.day_4,
                       sld.day_5,
                       sld.day_6,
                       sld.day_7
                       --
                  from source_dlvry_sched sd,
                       svc_repl_roq_gtt wrr,
                       (select sldi.source,
                               sldi.source_type,
                               sldi.location,
                               sldi.loc_type,
                               SUM(DECODE(sldi.day, 1, 1, 0)) as day_1,
                               SUM(DECODE(sldi.day, 2, 2, 0)) as day_2,
                               SUM(DECODE(sldi.day, 3, 3, 0)) as day_3,
                               SUM(DECODE(sldi.day, 4, 4, 0)) as day_4,
                               SUM(DECODE(sldi.day, 5, 5, 0)) as day_5,
                               SUM(DECODE(sldi.day, 6, 6, 0)) as day_6,
                               SUM(DECODE(sldi.day, 7, 7, 0)) as day_7
                          from source_dlvry_sched_days sldi
                         where NOT exists (select 1
                                             from source_dlvry_sched_exc sle,
                                                  svc_repl_roq_gtt wrri
                                            where sldi.source = sle.source
                                              and sldi.source_type = sle.source_type
                                              and sldi.location = sle.location
                                              and sldi.day = sle.day
                                              and sle.item = wrri.i_item
                                              and wrri.thread_id = I_thread_id
                                              and rownum = 1)
                         group by sldi.source,
                                  sldi.source_type,
                                  sldi.location,
                                  sldi.loc_type) sld
                 where sd.source = wrr.i_primary_repl_supplier
                   and sd.source_type = 'SUP'
                   and sd.location = case
                                     when wrr.i_locn_type = 'S' and wrr.i_stock_cat = 'D' then
                                        wrr.i_locn
                                     else
                                        wrr.i_source_physical_wh
                                     end
                   and sd.source = sld.source(+)
                   and sd.source_type = sld.source_type(+)
                   and sd.location = sld.location(+)
                   and (wrr.i_locn_type = 'W' or
                        (wrr.i_locn_type = 'S' and wrr.i_stock_cat in ('C','D','L')))
                   and wrr.thread_id = I_thread_id) use_this
         on (    target.i_item    = use_this.i_item
             and target.i_locn    = use_this.i_locn
             and target.thread_id = use_this.thread_id)
         when matched then
            update set target.I_true_start_date = use_this.true_start_date,
                       target.I_deliv_cycle     = use_this.delivery_cycle,
                       target.I_day_1           = use_this.day_1,
                       target.I_day_2           = use_this.day_2,
                       target.I_day_3           = use_this.day_3,
                       target.I_day_4           = use_this.day_4,
                       target.I_day_5           = use_this.day_5,
                       target.I_day_6           = use_this.day_6,
                       target.I_day_7           = use_this.day_7;
      end if;
   end if;

   if I_stock_cat = 'C' then
      L_review_loc := I_xdock_store;
   else
      L_review_loc := I_location;
   end if;

   if I_thread_id is NULL then
      if GET_ITEM_LOC_REVIEW_TIME(O_error_message,
                                  O_review_time,
                                  I_item,
                                  L_review_loc,
                                  I_review_cycle,
                                  (TO_NUMBER(TO_CHAR(I_repl_date - 1, 'D')))) = FALSE then
         return FALSE;
      end if;

      L_next_repl_date := I_repl_date + O_review_time;

   else --do bulk processing
      merge into svc_repl_roq_gtt target
      using (
             select tbl.i_review_cycle,
                    tbl.i_item,
                    tbl.i_locn,
                    tbl.thread_id,
   NVL(TO_NUMBER(TO_CHAR(tbl.i_date - 1,'D')),
                        TO_NUMBER(TO_CHAR(p.vdate,'D'))) i_today,
                                     tbl2.minday,
                    tbl2.min_maxday,
                    tbl2.rcount,
                    /* values fetch from GET_ITEM_LOC_REVIEW_TIME function */
                    case
                       when tbl.i_review_cycle = 1 then 1
                       when tbl.i_review_cycle = 0 and tbl2.rcount = 1 then 7
                       when tbl.i_review_cycle = 0 and tbl2.rcount > 1
                        and tbl2.min_maxday = 0 then
                            (tbl2.minday - NVL(TO_NUMBER(TO_CHAR(tbl.i_date - 1,'D')),
                                              TO_NUMBER(TO_CHAR(p.vdate,'D')))) + 7
                       when tbl.i_review_cycle = 0 and tbl2.rcount > 1
                        and tbl2.min_maxday > 0 then
                           (tbl2.min_maxday - NVL(TO_NUMBER(TO_CHAR(tbl.i_date - 1,'D')),
                                                  TO_NUMBER(TO_CHAR(p.vdate,'D'))))
                       else (TO_NUMBER(tbl.i_review_cycle) * 7)
                    end o_review_time
               from svc_repl_roq_gtt tbl,
                    (select r.item,
                            r.location,
                            r.weekday,
                            COUNT(*) over (partition by r.item, r.location) rcount,
                            MIN(r.weekday) over (partition by r.item, r.location) minday,
                            lead(r.weekday,1,0) over (partition by r.item, r.location order by r.weekday) min_maxday
                       from repl_day r) tbl2,
                    period p
              where tbl.i_item = tbl2.item
                and tbl2.location = tbl.i_locn
                and tbl2.weekday = NVL(TO_NUMBER(TO_CHAR(tbl.i_date - 1,'D')),
                                       TO_NUMBER(TO_CHAR(p.vdate,'D')))
                and (tbl.i_locn_type = 'W' or
                     (tbl.i_locn_type = 'S' and tbl.i_stock_cat in ('C','D','L')))
                and tbl.thread_id = I_thread_id) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id)
      when matched then
         update set target.I_review_lead_time = use_this.o_review_time;
   end if;

   if I_thread_id is NULL then
      if I_supp_dlvry_policy = 'NEXT' then

         /*** Get the current open delivery date ***/
         if GET_OPEN_DELIV_DATE_NEXT_DAY(O_error_message,
                                         O_colt,
                                         O_current_deliv_date,
                                         O_days_added_to_colt,
                                         IO_last_dd,
                                         L_deliv_days_table,
                                         I_repl_date,
                                         I_olt,
                                         I_location,
                                         I_loc_activity_ind,
                                         L_start_date,
                                         L_deliv_cycle) = FALSE then
            return FALSE;
         end if;

         /*** Get the next open delivery date ***/

         if GET_OPEN_DELIV_DATE_NEXT_DAY(O_error_message,
                                         O_nolt,
                                         O_next_deliv_date,
                                         O_days_added_to_nolt,
                                         L_dummy,
                                         L_deliv_days_table,
                                         L_next_repl_date,
                                         I_olt,
                                         I_location,
                                         I_loc_activity_ind,
                                         L_start_date,
                                         L_deliv_cycle) = FALSE then
            return FALSE;
         end if;

      else

         /*** Get the current open delivery date ***/

         if GET_OPEN_DELIV_DATE_NEXT_DD(O_error_message,
                                        O_colt,
                                        O_current_deliv_date,
                                        O_days_added_to_colt,
                                        IO_last_dd,
                                        L_deliv_days_table,
                                        I_repl_date,
                                        I_olt,
                                        I_location,
                                        I_loc_activity_ind,
                                        L_start_date,
                                        L_deliv_cycle) = FALSE then
            return FALSE;
         end if;

         /*** Get the next open delivery date ***/

         if GET_OPEN_DELIV_DATE_NEXT_DD(O_error_message,
                                        O_nolt,
                                        O_next_deliv_date,
                                        O_days_added_to_nolt,
                                        L_dummy,
                                        L_deliv_days_table,
                                        L_next_repl_date,
                                        I_olt,
                                        I_location,
                                        I_loc_activity_ind,
                                        L_start_date,
                                        L_deliv_cycle) = FALSE then

            return FALSE;
         end if;
      end if;

   else --do bulk processing
      L_obj_olts_tbl := "OBJ_OLTS_TBL"();

      for rec in C_OLTS loop
         L_last_dd := rec.I_last_delivery_date;
         L_deliv_days_table := deliv_days_table_type(rec.I_day_1, rec.I_day_2, rec.I_day_3, rec.I_day_4, rec.I_day_5, rec.I_day_6, rec.I_day_7);
         L_olt := NVL(rec.I_supp_lead_time, 0) + NVL(rec.I_pickup_lead_time, 0);
         L_start_date := rec.I_true_start_date;
         L_deliv_cycle := rec.I_deliv_cycle;
         L_next_repl_date := rec.I_date + rec.I_review_lead_time;
         L_item := rec.I_item;
         L_location := rec.locn;
         L_supp_lead_time := NVL(rec.I_supp_lead_time, 0);
         L_colt := 0;
         L_nolt := 0;
         L_days_added_to_colt := 0;
         L_days_added_to_nolt := 0;

         if rec.I_sup_delivery_policy = 'NEXT' then

            /*** Get the current open delivery date ***/
            if GET_OPEN_DELIV_DATE_NEXT_DAY(O_error_message,
                                            L_colt,
                                            L_curr_deliv_date,
                                            L_days_added_to_colt,
                                            L_last_dd,
                                            L_deliv_days_table,
                                            rec.I_date,
                                            L_olt,
                                            L_location,
                                            I_loc_activity_ind,
                                            L_start_date,
                                            L_deliv_cycle) = FALSE then
               return FALSE;
            end if;

            /*** Get the next open delivery date ***/

            if GET_OPEN_DELIV_DATE_NEXT_DAY(O_error_message,
                                            L_nolt,
                                            L_next_deliv_date,
                                            L_days_added_to_nolt,
                                            L_dummy,
                                            L_deliv_days_table,
                                            L_next_repl_date,
                                            L_olt,
                                            L_location,
                                            I_loc_activity_ind,
                                            L_start_date,
                                            L_deliv_cycle) = FALSE then
               return FALSE;
            end if;

         else
            /*** Get the current open delivery date ***/
            if GET_OPEN_DELIV_DATE_NEXT_DD(O_error_message,
                                           L_colt,
                                           L_curr_deliv_date,
                                           L_days_added_to_colt,
                                           L_last_dd,
                                           L_deliv_days_table,
                                           rec.I_date,
                                           L_olt,
                                           L_location,
                                           I_loc_activity_ind,
                                           L_start_date,
                                           L_deliv_cycle) = FALSE then
               return FALSE;
            end if;

            /*** Get the next open delivery date ***/
            if GET_OPEN_DELIV_DATE_NEXT_DD(O_error_message,
                                           L_nolt,
                                           l_next_deliv_date,
                                           L_days_added_to_nolt,
                                           L_dummy,
                                           L_deliv_days_table,
                                           L_next_repl_date,
                                           L_olt,
                                           L_location,
                                           I_loc_activity_ind,
                                           L_start_date,
                                           L_deliv_cycle) = FALSE then

               return FALSE;
            end if;
         end if;

         --set to original location since the value of L_location may be
         --source_wh or loc. Here we need the loc value for the item/loc
         --join in the succeeding MERGE below.
         L_location := rec.I_locn;
         --
         L_obj_olts_rec := "OBJ_OLTS_REC"(L_item,
                                          L_location,
                                          I_thread_id,
                                          L_colt,
                                          L_nolt,
                                          L_curr_deliv_date,
                                          L_next_deliv_date,
                                          L_last_dd,
                                          L_days_added_to_colt,
                                          L_days_added_to_nolt,
                                          L_supp_lead_time,
                                          NULL);
         L_obj_olts_tbl.EXTEND();
         L_obj_olts_tbl(L_obj_olts_tbl.COUNT) := L_obj_olts_rec;
      end loop;

      merge into svc_repl_roq_gtt target
      using (
             select tbl.i_item,
                    tbl.i_locn,
                    tbl.thread_id,
                    tbl.i_curr_order_lead_time,
                    tbl.i_next_order_lead_time,
                    tbl.i_curr_delivery_date,
                    tbl.i_next_delivery_date,
                    tbl.i_last_delivery_date,
                    tbl.i_days_added_to_colt,
                    tbl.i_days_added_to_nolt,
                    tbl.i_supp_lead_time,
                    tbl.i_wh_lead_time
             from table(cast(L_obj_olts_tbl as "OBJ_OLTS_TBL")) tbl) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id)
      when matched then
         update set target.I_curr_order_lead_time = use_this.i_curr_order_lead_time,
                    target.I_next_order_lead_time = use_this.i_next_order_lead_time,
                    target.I_next_delivery_date   = use_this.i_next_delivery_date,
                    target.I_curr_delivery_date   = use_this.i_curr_delivery_date,
                    target.I_last_delivery_date   = use_this.i_last_delivery_date,
                    target.I_days_added_to_colt   = use_this.i_days_added_to_colt,
                    target.I_days_added_to_nolt   = use_this.i_days_added_to_nolt,
                    target.I_pickup_lead_time     = (use_this.i_curr_order_lead_time - use_this.i_supp_lead_time),
                    target.I_next_review_date     = target.i_review_lead_time + target.i_date - 1;
      L_obj_olts_tbl.DELETE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.CALC_OLTS_AND_RT',
                                            to_char(SQLCODE));
      return FALSE;
END CALC_OLTS_AND_RT;
------------------------------------------------------------------------------------------
FUNCTION CALC_WH_TO_STORE_OLTS_AND_RT(O_error_message           IN OUT   VARCHAR2,
                                      O_store_colt              IN OUT   NUMBER,
                                      O_store_nolt              IN OUT   NUMBER,
                                      O_days_added_to_st_colt   IN OUT   NUMBER,
                                      O_days_added_to_st_nolt   IN OUT   NUMBER,
                                      IO_review_time            IN OUT   NUMBER,
                                      IO_last_dd                IN OUT   PERIOD.VDATE%TYPE,
                                      I_current_wh_deliv_date   IN       PERIOD.VDATE%TYPE,
                                      I_next_wh_deliv_date      IN       PERIOD.VDATE%TYPE,
                                      I_repl_date               IN       PERIOD.VDATE%TYPE,
                                      I_olt                     IN       NUMBER,
                                      I_item                    IN       ITEM_MASTER.ITEM%TYPE,
                                      I_store                   IN       STORE.STORE%TYPE,
                                      I_physical_wh             IN       SOURCE_DLVRY_SCHED_DAYS.SOURCE%TYPE,
                                      I_review_cycle            IN       REPL_RESULTS.REVIEW_CYCLE%TYPE,
                                      I_loc_activity_ind        IN       SYSTEM_OPTIONS.LOC_ACTIVITY_IND%TYPE,
                                      I_loc_dlvry_ind           IN       SYSTEM_OPTIONS.LOC_DLVRY_IND%TYPE,
                                      I_wh_dlvry_policy         IN       WH.DELIVERY_POLICY%TYPE,
                                      I_thread_id               IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN IS

   L_next_repl_date             PERIOD.VDATE%TYPE;
   L_deliv_days_table           DELIV_DAYS_TABLE_TYPE;
   L_start_date                 SOURCE_DLVRY_SCHED.START_DATE%TYPE;
   L_deliv_cycle                SOURCE_DLVRY_SCHED.DELIVERY_CYCLE%TYPE;
   L_current_wh_deliv_date      PERIOD.VDATE%TYPE  := I_current_wh_deliv_date;
   L_next_wh_deliv_date         PERIOD.VDATE%TYPE  := I_next_wh_deliv_date;
   L_current_store_deliv_date   PERIOD.VDATE%TYPE;
   L_next_store_deliv_date      PERIOD.VDATE%TYPE;
   L_dummy                      PERIOD.VDATE%TYPE;
   L_colt                       RPL_NET_INVENTORY_TMP.CURR_ORDER_LEAD_TIME%TYPE;
   L_nolt                       RPL_NET_INVENTORY_TMP.NEXT_ORDER_LEAD_TIME%TYPE;
   L_curr_deliv_date            PERIOD.VDATE%TYPE;
   L_next_deliv_date            PERIOD.VDATE%TYPE;
   L_days_added_to_colt         RPL_NET_INVENTORY_TMP.DAYS_ADDED_TO_COLT%TYPE;
   L_days_added_to_nolt         RPL_NET_INVENTORY_TMP.DAYS_ADDED_TO_NOLT%TYPE;
   L_last_dd                    REPL_ITEM_LOC.LAST_DELIVERY_DATE%TYPE;
   L_olt                        NUMBER;
   L_item                       ITEM_MASTER.ITEM%TYPE;
   L_location                   ITEM_LOC.LOC%TYPE;
   L_supp_lead_time             RPL_NET_INVENTORY_TMP.SUPP_LEAD_TIME%TYPE;
   --
   L_obj_olts_rec               "OBJ_OLTS_REC" := NULL;
   L_obj_olts_tbl               "OBJ_OLTS_TBL" := NULL;

   cursor C_OLTS is
      select I_item,
             I_locn,
             I_last_delivery_date,
             I_source_physical_wh,
             I_day_1,
             I_day_2,
             I_day_3,
             I_day_4,
             I_day_5,
             I_day_6,
             I_day_7,
             I_date,
             I_curr_delivery_date,
             I_next_delivery_date,
             --I_sup_delivery_policy,
             I_wh_delivery_policy,
             I_true_start_date,
             I_deliv_cycle,
             I_wh_lead_time,
             I_review_lead_time,
             thread_id
        from svc_repl_roq_gtt
       where thread_id = I_thread_id
         and (I_locn_type = 'S'
              and I_stock_cat in ('W','C','L'))
          or (I_locn_type = 'W'
              and I_repl_method = 'D');

BEGIN
   -- This function is intended to calculate olts from
   -- the warehouse to the store for either crossdock
   -- orders or warehouse stocked store transfers.

   L_deliv_days_table := deliv_days_table_type(NULL, NULL, NULL, NULL, NULL, NULL, NULL);

   /* The source_dlvry_sched and source_dlvry_sched_days tables keep *
    * records at the physical warehouse level. */
   if I_loc_dlvry_ind = 'Y' then
      if I_thread_id is NULL then
         if GET_DELIV_DAYS(O_error_message,
                           L_deliv_days_table,
                           L_start_date,
                           L_deliv_cycle,
                           I_item,
                           I_store,
                           I_physical_wh,
                           'W') = FALSE then
            return FALSE;
         end if;

         if l_deliv_days_table(1) is not NULL then
            if GET_TRUE_START_DATE(O_error_message,
                                   L_start_date,
                                   L_deliv_days_table,
                                   L_start_date) = FALSE then
               return FALSE;
            end if;
         end if;

      else --do bulk processing
         merge into svc_repl_roq_gtt target
         using (
                select wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id,
                       sd.source,
                       sd.source_type,
                       sd.location,
                       /* values fetch from the GET_DELIV_DAYS function */
                       sd.start_date,
                       sd.delivery_cycle,
                       /* values fetch from GET_TRUE_START_DATE function */
                       case
                          when sld.day_1 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_1 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_2 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_2 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_3 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_3 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_4 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_4 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_5 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_5 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_6 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_6 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          when sld.day_7 >= TO_NUMBER(TO_CHAR(sd.start_date,'D')) then
                             sd.start_date + (sld.day_7 - TO_NUMBER(TO_CHAR(sd.start_date, 'D')))
                          else
                             sd.start_date + ((7 - (TO_NUMBER(TO_CHAR(sd.start_date, 'D'))))
                                              + case when sld.day_1 > 0 then sld.day_1
                                                     when sld.day_2 > 0 then sld.day_2
                                                     when sld.day_3 > 0 then sld.day_3
                                                     when sld.day_4 > 0 then sld.day_4
                                                     when sld.day_5 > 0 then sld.day_5
                                                     when sld.day_6 > 0 then sld.day_6
                                                     when sld.day_7 > 0 then sld.day_7
                                                 end)
                       end true_start_date,
                       /* values fetch from GET_DELIV_DAYS */
                       /* the pl/sql table type variable L_deliv_days_table */
                       /* have been replaced by these columns */
                       sld.day_1,
                       sld.day_2,
                       sld.day_3,
                       sld.day_4,
                       sld.day_5,
                       sld.day_6,
                       sld.day_7
                  from source_dlvry_sched sd,
                       svc_repl_roq_gtt wrr,
                       (select sldi.source,
                               sldi.source_type,
                               sldi.location,
                               sldi.loc_type,
                               SUM(DECODE(sldi.day, 1, 1, 0)) as day_1,
                               SUM(DECODE(sldi.day, 2, 2, 0)) as day_2,
                               SUM(DECODE(sldi.day, 3, 3, 0)) as day_3,
                               SUM(DECODE(sldi.day, 4, 4, 0)) as day_4,
                               SUM(DECODE(sldi.day, 5, 5, 0)) as day_5,
                               SUM(DECODE(sldi.day, 6, 6, 0)) as day_6,
                               SUM(DECODE(sldi.day, 7, 7, 0)) as day_7
                          from source_dlvry_sched_days sldi
                         where NOT exists (select 1
                                             from source_dlvry_sched_exc sle,
                                                  svc_repl_roq_gtt wrri
                                            where sldi.source = sle.source
                                              and sldi.source_type = sle.source_type
                                              and sldi.location = sle.location
                                              and sldi.day = sle.day
                                              and sle.item = wrri.i_item
                                              and wrri.thread_id = I_thread_id
                                              and rownum = 1)
                         group by sldi.source,
                                  sldi.source_type,
                                  sldi.location,
                                  sldi.loc_type) sld
                 where sd.source = NVL(wrr.i_source_physical_wh, wrr.i_source_wh)
                   and sd.source_type = 'W'
                   and sd.location = wrr.i_locn
                   and sd.source = sld.source(+)
                   and sd.source_type = sld.source_type(+)
                   and sd.location = sld.location(+)
                   and ((       wrr.i_locn_type = 'S'
                            and wrr.i_stock_cat in ('W','C','L'))
                        or (    wrr.i_locn_type = 'W'
                            and wrr.i_repl_method = 'D'))
                   and wrr.thread_id = I_thread_id) use_this
         on (    target.i_item    = use_this.i_item
             and target.i_locn    = use_this.i_locn
             and target.thread_id = use_this.thread_id)
         when matched then
            update set target.I_true_start_date = use_this.true_start_date,
                       target.I_deliv_cycle     = use_this.delivery_cycle,
                       target.I_day_1           = use_this.day_1,
                       target.I_day_2           = use_this.day_2,
                       target.I_day_3           = use_this.day_3,
                       target.I_day_4           = use_this.day_4,
                       target.I_day_5           = use_this.day_5,
                       target.I_day_6           = use_this.day_6,
                       target.I_day_7           = use_this.day_7;
      end if;
   end if;

   -- The current and next deliv date will be NULL
   -- if this is a warehouse stocked store.
   if I_thread_id is NULL then
      if L_current_wh_deliv_date is NULL then
         L_current_wh_deliv_date := I_repl_date;
      end if;

      if L_next_wh_deliv_date is NULL then
         if GET_ITEM_LOC_REVIEW_TIME(O_error_message,
                                     IO_review_time,
                                     I_item,
                                     I_store,
                                     I_review_cycle,
                                     (TO_NUMBER(TO_CHAR(I_repl_date-1, 'D')))) = FALSE then
            return FALSE;
         end if;

         L_next_wh_deliv_date := L_current_wh_deliv_date + IO_review_time;
      end if;

   else --do bulk processing
      merge into svc_repl_roq_gtt target
      using (WITH
                v_review_time AS
                (select tbl.i_review_cycle,
                        tbl.i_item,
                        tbl.i_locn,
                        tbl.thread_id,
                        tbl.i_curr_delivery_date,
						NVL(TO_NUMBER(TO_CHAR(tbl.i_date -1,'D')),
                            TO_NUMBER(TO_CHAR(p.vdate,'D'))) i_today,
                        r.minday,
                        r.min_maxday,
                        r.rcount,
                        p.vdate,
                        tbl.i_date
                   from svc_repl_roq_gtt tbl,
                       (select r.item,
                               r.location,
                               r.weekday,
                               COUNT(*) over (partition by r.item, r.location) rcount,
                               MIN(r.weekday) over (partition by r.item, r.location) minday,
                               lead(r.weekday,1,0) over (partition by r.item, r.location order by r.weekday) min_maxday
                          from repl_day r) r,
                        period p
                  where tbl.i_item = r.item
                    and tbl.i_locn = r.location
					and r.weekday  = NVL(TO_NUMBER(TO_CHAR(tbl.i_date - 1,'D')),
                                     TO_NUMBER(TO_CHAR(p.vdate,'D')))
                    and ((        tbl.i_locn_type = 'S'
                              and tbl.i_stock_cat in ('W','C','L'))
                          or (    tbl.i_locn_type = 'W'
                              and tbl.i_repl_method = 'D'))
                    and tbl.thread_id = I_thread_id)
             --end of WITH clause
             select qry_tbl.i_item,
                    qry_tbl.i_locn,
                    qry_tbl.thread_id,
                    case
                       when qry_tbl.i_review_cycle = 1 then 1
                       when qry_tbl.i_review_cycle = 0 and qry_tbl.rcount = 1 then 7
                       when qry_tbl.i_review_cycle = 0 and qry_tbl.rcount > 1
                        and qry_tbl.min_maxday = 0 then
						(qry_tbl.minday - NVL(TO_NUMBER(TO_CHAR(qry_tbl.i_date - 1,'D')),
                                              TO_NUMBER(TO_CHAR(qry_tbl.vdate,'D')))) + 7
                       when qry_tbl.i_review_cycle = 0 and qry_tbl.rcount > 1
                        and qry_tbl.min_maxday > 0 then
						(qry_tbl.min_maxday - NVL(TO_NUMBER(TO_CHAR(qry_tbl.i_date - 1,'D')),
												  TO_NUMBER(TO_CHAR(qry_tbl.vdate,'D'))))
                       else (TO_NUMBER(qry_tbl.i_review_cycle) * 7)
                    end review_time
              from (
                    select i_item,
                           i_locn,
                           thread_id,
                           i_review_cycle,
                           rcount,
                           min_maxday,
                           minday,
                           i_date,
                           vdate,
                           i_curr_delivery_date
                      from v_review_time) qry_tbl) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id)
      when matched then
         update set target.I_review_lead_time     = use_this.review_time,
                    target.I_curr_order_lead_time = NVL(target.i_curr_order_lead_time, 0);

   end if;

   if I_thread_id is NULL then
      if I_wh_dlvry_policy = 'NEXT' then

         /*** Get the current order lead time ***/

         if GET_OPEN_DELIV_DATE_NEXT_DAY(O_error_message,
                                         O_store_colt,
                                         L_current_store_deliv_date,
                                         O_days_added_to_st_colt,
                                         IO_last_dd,
                                         L_deliv_days_table,
                                         L_current_wh_deliv_date,
                                         I_olt,
                                         I_store,
                                         I_loc_activity_ind,
                                         L_start_date,
                                         L_deliv_cycle) = FALSE then
            return FALSE;
         end if;

         /*** Get the next order lead time ***/

         if GET_OPEN_DELIV_DATE_NEXT_DAY(O_error_message,
                                         O_store_nolt,
                                         L_next_store_deliv_date,
                                         O_days_added_to_st_nolt,
                                         L_dummy,
                                         L_deliv_days_table,
                                         L_next_wh_deliv_date,
                                         I_olt,
                                         I_store,
                                         I_loc_activity_ind,
                                         L_start_date,
                                         L_deliv_cycle) = FALSE then
            return FALSE;
         end if;

      else

         /*** Get the current order lead time ***/

         if GET_OPEN_DELIV_DATE_NEXT_DD(O_error_message,
                                        O_store_colt,
                                        L_current_store_deliv_date,
                                        O_days_added_to_st_colt,
                                        IO_last_dd,
                                        L_deliv_days_table,
                                        L_current_wh_deliv_date,
                                        I_olt,
                                        I_store,
                                        I_loc_activity_ind,
                                        L_start_date,
                                        L_deliv_cycle) = FALSE then
            return FALSE;
         end if;

         /*** Get the next order lead time ***/

         if GET_OPEN_DELIV_DATE_NEXT_DD(O_error_message,
                                        O_store_nolt,
                                        L_next_store_deliv_date,
                                        O_days_added_to_st_nolt,
                                        L_dummy,
                                        L_deliv_days_table,
                                        L_next_wh_deliv_date,
                                        I_olt,
                                        I_store,
                                        I_loc_activity_ind,
                                        L_start_date,
                                        L_deliv_cycle) = FALSE then
            return FALSE;
         end if;
      end if;

   else --do bulk processing
      L_obj_olts_tbl := "OBJ_OLTS_TBL"();

      for rec in C_OLTS loop
         L_last_dd := rec.I_last_delivery_date;
         L_deliv_days_table := deliv_days_table_type(rec.I_day_1, rec.I_day_2, rec.I_day_3, rec.I_day_4, rec.I_day_5, rec.I_day_6, rec.I_day_7);
         L_olt := nvl(rec.I_wh_lead_time,0);
         L_start_date := rec.I_true_start_date;
         L_deliv_cycle := rec.I_deliv_cycle;
         L_next_repl_date := NVL(rec.I_curr_delivery_date,rec.I_date) + rec.I_review_lead_time;
         L_current_wh_deliv_date := NVL(rec.I_curr_delivery_date,rec.I_date);
         L_item := rec.I_item;
         L_location := rec.I_locn;
         L_colt := 0;
         L_nolt := 0;
         L_days_added_to_colt := 0;
         L_days_added_to_nolt := 0;
         L_curr_deliv_date := NULL;
         L_next_deliv_date := NULL;

         if rec.I_wh_delivery_policy = 'NEXT' then
            /*** Get the current open delivery date ***/
            if GET_OPEN_DELIV_DATE_NEXT_DAY(O_error_message,
                                            L_colt,
                                            L_curr_deliv_date,
                                            L_days_added_to_colt,
                                            L_last_dd,
                                            L_deliv_days_table,
                                            L_current_wh_deliv_date,
                                            L_olt,
                                            L_location,
                                            I_loc_activity_ind,
                                            L_start_date,
                                            L_deliv_cycle) = FALSE then
               return FALSE;
            end if;

            /*** Get the next open delivery date ***/
            if GET_OPEN_DELIV_DATE_NEXT_DAY(O_error_message,
                                            L_nolt,
                                            L_next_deliv_date,
                                            L_days_added_to_nolt,
                                            L_dummy,
                                            L_deliv_days_table,
                                            L_next_repl_date,
                                            L_olt,
                                            L_location,
                                            I_loc_activity_ind,
                                            L_start_date,
                                            L_deliv_cycle) = FALSE then
               return FALSE;
            end if;

         else
            /*** Get the current open delivery date ***/
            if GET_OPEN_DELIV_DATE_NEXT_DD(O_error_message,
                                           L_colt,
                                           L_curr_deliv_date,
                                           L_days_added_to_colt,
                                           L_last_dd,
                                           L_deliv_days_table,
                                           L_current_wh_deliv_date,
                                           L_olt,
                                           L_location,
                                           I_loc_activity_ind,
                                           L_start_date,
                                           L_deliv_cycle) = FALSE then
               return FALSE;
            end if;

            /*** Get the next open delivery date ***/
            if GET_OPEN_DELIV_DATE_NEXT_DD(O_error_message,
                                           L_nolt,
                                           L_next_deliv_date,
                                           L_days_added_to_nolt,
                                           L_dummy,
                                           L_deliv_days_table,
                                           L_next_repl_date,
                                           L_olt,
                                           L_location,
                                           I_loc_activity_ind,
                                           L_start_date,
                                           L_deliv_cycle) = FALSE then

               return FALSE;
            end if;
         end if;

         L_obj_olts_rec := "OBJ_OLTS_REC"(L_item,
                                          L_location,
                                          I_thread_id,
                                          L_colt,
                                          L_nolt,
                                          L_curr_deliv_date,
                                          L_next_deliv_date,
                                          L_last_dd,
                                          L_days_added_to_colt,
                                          L_days_added_to_nolt,
                                          NULL,
                                          L_olt);
         L_obj_olts_tbl.EXTEND();
         L_obj_olts_tbl(L_obj_olts_tbl.COUNT) := L_obj_olts_rec;

      end loop;

      merge into svc_repl_roq_gtt target
      using (
             select tbl.i_item,
                    tbl.i_locn,
                    tbl.thread_id,
                    tbl.i_curr_order_lead_time i_wh_order_lead_time,
                    tbl.i_curr_order_lead_time,
                    tbl.i_next_order_lead_time,
                    tbl.i_curr_delivery_date,
                    tbl.i_next_delivery_date,
                    tbl.i_last_delivery_date,
                    tbl.i_days_added_to_colt,
                    tbl.i_days_added_to_nolt,
                    tbl.i_supp_lead_time,
                    tbl.i_wh_lead_time
               from table(cast(L_obj_olts_tbl as "OBJ_OLTS_TBL")) tbl
             ) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id)
      when matched then
         update set target.I_wh_lead_time         = use_this.i_wh_order_lead_time,
                    target.I_curr_order_lead_time = case when (target.i_locn_type = 'S' and target.i_stock_cat in ('W','C','L'))
                                                           or (target.i_locn_type = 'W' and target.i_repl_method = 'D') then
                                                       NVL(target.i_curr_order_lead_time,0) + use_this.i_curr_order_lead_time
                                                    else
                                                       use_this.i_curr_order_lead_time
                                                    end,
                    target.I_next_order_lead_time = case when (target.i_locn_type = 'S' and target.i_stock_cat in ('W','C','L'))
                                                           or (target.i_locn_type = 'W' and target.i_repl_method = 'D') then
                                                       NVL(target.i_next_order_lead_time,0) + use_this.i_next_order_lead_time
                                                    else
                                                       use_this.i_next_order_lead_time
                                                    end,
                    target.I_curr_delivery_date   = use_this.i_curr_delivery_date,
                    target.I_next_delivery_date   = use_this.i_next_delivery_date,
                    target.I_last_delivery_date   = use_this.i_last_delivery_date,
                    target.I_days_added_to_colt   = case when (target.i_locn_type = 'S' and target.i_stock_cat in ('W','C','L'))
                                                           or (target.i_locn_type = 'W' and target.i_repl_method = 'D') then
                                                       NVL(target.i_days_added_to_colt,0) + use_this.i_days_added_to_colt
                                                    else
                                                       use_this.i_days_added_to_colt
                                                    end,
                    target.I_days_added_to_nolt   = case when (target.i_locn_type = 'S' and target.i_stock_cat in ('W','C','L'))
                                                           or (target.i_locn_type = 'W' and target.i_repl_method = 'D') then
                                                       NVL(target.i_days_added_to_nolt,0) + use_this.i_days_added_to_nolt
                                                    else
                                                       use_this.i_days_added_to_nolt
                                                    end,
                    target.I_next_review_date     = target.i_review_lead_time + target.i_date - 1;

      L_obj_olts_tbl.DELETE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.CALC_WH_TO_STORE_OLTS_AND_RT',
                                            to_char(SQLCODE));
      return FALSE;
END CALC_WH_TO_STORE_OLTS_AND_RT;
------------------------------------------------------------------------------------------
--- VDATE + 1 is passed as I_repl_date from RPLEXT whereas VDATE is passed from OCIROQ.
------------------------------------------------------------------------------------------
FUNCTION GET_OLTS_AND_REVIEW_TIME(O_error_message        IN OUT   VARCHAR2,
                                  O_colt                 IN OUT   NUMBER,
                                  O_nolt                 IN OUT   NUMBER,
                                  O_days_added_to_colt   IN OUT   NUMBER,
                                  O_days_added_to_nolt   IN OUT   NUMBER,
                                  O_review_time          IN OUT   NUMBER,
                                  O_next_delivery_date   IN OUT   REPL_ITEM_LOC.NEXT_DELIVERY_DATE%TYPE,
                                  O_next_review_date     IN OUT   REPL_ITEM_LOC.NEXT_REVIEW_DATE%TYPE,
                                  IO_supp_lead_time      IN OUT   NUMBER,
                                  IO_pickup_lead_time    IN OUT   NUMBER,
                                  IO_wh_lead_time        IN OUT   NUMBER,
                                  IO_last_dd             IN OUT   PERIOD.VDATE%TYPE,
                                  I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                                  I_location             IN       WH.WH%TYPE,
                                  I_loc_type             IN       REPL_RESULTS.LOC_TYPE%TYPE,
                                  I_repl_date            IN       REPL_RESULTS.REPL_DATE%TYPE,
                                  I_stock_cat            IN       REPL_RESULTS.STOCK_CAT%TYPE,
                                  I_repl_method          IN       REPL_RESULTS.REPL_METHOD%TYPE,
                                  I_review_cycle         IN       REPL_RESULTS.REVIEW_CYCLE%TYPE,
                                  I_supplier             IN       SUPS.SUPPLIER%TYPE,
                                  I_supp_dlvry_policy    IN       SUPS.DELIVERY_POLICY%TYPE,
                                  I_source_wh            IN       WH.WH%TYPE,
                                  I_physical_wh          IN       WH.PHYSICAL_WH%TYPE,
                                  I_wh_dlvry_policy      IN       WH.DELIVERY_POLICY%TYPE,
                                  I_loc_activity_ind     IN       SYSTEM_OPTIONS.LOC_ACTIVITY_IND%TYPE,
                                  I_loc_dlvry_ind        IN       SYSTEM_OPTIONS.LOC_DLVRY_IND%TYPE,
                                  I_thread_id            IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN IS

   L_olt                     NUMBER;
   L_store_colt              NUMBER;
   L_store_nolt              NUMBER;
   L_days_added_to_st_colt   NUMBER;
   L_days_added_to_st_nolt   NUMBER;
   L_current_deliv_date      PERIOD.VDATE%TYPE;
   L_next_deliv_date         PERIOD.VDATE%TYPE;
   L_location                WH.WH%TYPE          := I_location;
   L_xdock_store             STORE.STORE%TYPE;
   L_physical_wh             WH.PHYSICAL_WH%TYPE := I_physical_wh;
   L_physical_loc            REPL_ITEM_LOC.LOCATION%TYPE;


   cursor C_GET_PHYSICAL_WH is
      select wh.physical_wh
        from wh wh
       where wh.wh = NVL(I_source_wh, I_location);

BEGIN
   if I_thread_id is NULL then
      /* intialize parameters  */
      O_colt := 0;
      O_nolt := 0;
      O_days_added_to_colt := 0;
      O_days_added_to_nolt := 0;
      O_review_time := 0;
      IO_supp_lead_time := nvl(IO_supp_lead_time,0);
      IO_pickup_lead_time := nvl(IO_pickup_lead_time,0);
      IO_wh_lead_time := nvl(IO_wh_lead_time,0);

      L_store_colt              := 0;
      L_store_nolt              := 0;
      L_days_added_to_st_colt   := 0;
      L_days_added_to_st_nolt   := 0;


      /*****************************************************************************
       * When replenishing directly to a WH or using                               *
       * crossdock replenishment, this program deals with both                     *
       * virtual and physical wh's. Repl_day, location_close and                   *
       * company_closed records areheld at the virtual wh level, thus the          *
       * wh location being replenished, which will always be a virtual wh, will be *
       * passed directly into the functions retrieving records from those          *
       * tables.  Source_dlvry_sched and source_dlvry_sched_days                   *
       * records, on the other hand, are held at the physical loc                  *
       * level, thus we will need to retrieve the physical wh when                 *
       * necessary and send it into those functions in order to                    *
       * retrieve the correct delivery schedule records.                           *
       *****************************************************************************/

      if L_physical_wh is NULL then
         if (I_loc_type = 'W') or
            (I_loc_type = 'S' and I_stock_cat != 'D') then
            SQL_LIB.SET_MARK('OPEN', 'C_GET_PHYSICAL_WH', 'WH', NULL);
            open  C_GET_PHYSICAL_WH;
            SQL_LIB.SET_MARK('FETCH', 'C_GET_PHYSICAL_WH', 'WH', NULL);
            fetch C_GET_PHYSICAL_WH into L_physical_wh;
            SQL_LIB.SET_MARK('CLOSE', 'C_GET_PHYSICAL_WH', 'PERIOD', NULL);
            close C_GET_PHYSICAL_WH;
         end if;
      end if;

      -- Both the physical and source
      -- warehouses need to be passed into the function.  Physical
      -- wh will be used to retrieve delivery schedules, whereas as
      -- the virtual wh will be used for review times. However, if the location
      -- is a store and the stock category is direct to store, then
      -- the physical location will be the store.
      if (I_loc_type != 'S') or
         (I_loc_type = 'S' and I_stock_cat != 'W') then

         -- If this is a crossdock store, then its warehouse olts
         -- must be determined first.
         if (I_loc_type = 'S' and I_stock_cat = 'C') or
            (I_loc_type = 'S' and I_stock_cat = 'L') then
            L_location := I_source_wh;
            L_xdock_store := I_location;
            L_physical_loc := L_physical_wh;
         elsif (I_loc_type = 'S' and I_stock_cat = 'D') then
            L_location := I_location;
            L_physical_loc := I_location;
         else
            L_location := I_location;
            L_physical_loc := L_physical_wh;
         end if;

         -- The third LT component is ignored in case this is a crossdock.
         L_olt := (IO_supp_lead_time + IO_pickup_lead_time);

         if CALC_OLTS_AND_RT(O_error_message,
                             O_colt,
                             O_nolt,
                             O_review_time,
                             O_days_added_to_colt,
                             O_days_added_to_nolt,
                             L_current_deliv_date,
                             L_next_deliv_date,
                             IO_last_dd,
                             I_repl_date,
                             L_olt,
                             I_item,
                             L_location,
                             L_xdock_store,
                             L_physical_loc,
                             I_stock_cat,
                             I_supplier,
                             I_review_cycle,
                             I_loc_activity_ind,
                             I_loc_dlvry_ind,
                             I_supp_dlvry_policy) = FALSE then
            return FALSE;
         end if;

         -- Any adjustments made to the leadtime will be added
         -- to the transit leadtime.
         IO_pickup_lead_time := (O_colt - IO_supp_lead_time);
      end if;

      -- If this is warehouse replenishment or the wh to store portion of the
      -- leadtime for a crossdock order, the following will calculate and adjust the OLT's.
      if (I_loc_type = 'S' and I_stock_cat = 'W') or
         (I_loc_type = 'S' and I_stock_cat = 'C') or
         (I_loc_type = 'S' and I_stock_cat = 'L') or
         (I_loc_type = 'W' and I_repl_method = 'D') then
         ---
         L_olt := IO_wh_lead_time;
         ---

         if CALC_WH_TO_STORE_OLTS_AND_RT(O_error_message,
                                         L_store_colt,
                                         L_store_nolt,
                                         L_days_added_to_st_colt,
                                         L_days_added_to_st_nolt,
                                         O_review_time,
                                         IO_last_dd,
                                         L_current_deliv_date,
                                         L_next_deliv_date,
                                         I_repl_date,
                                         L_olt,
                                         I_item,
                                         I_location,
                                         L_physical_wh,
                                         I_review_cycle,
                                         I_loc_activity_ind,
                                         I_loc_dlvry_ind,
                                         I_wh_dlvry_policy) = FALSE then
            return FALSE;
         end if;

         IO_wh_lead_time := L_store_colt;
         O_colt := O_colt + L_store_colt;
         O_nolt := O_nolt + L_store_nolt;
         O_days_added_to_colt := O_days_added_to_colt + L_days_added_to_st_colt;
         O_days_added_to_nolt := O_days_added_to_nolt + L_days_added_to_st_nolt;

      end if;

      -- Calculate next delivery date for the next review cycle.

      O_next_delivery_date := L_next_deliv_date;

      ---
      -- Calculate next review date.   The date is used to limit how far ahead
      -- Investment Buy opportunities can be calculated.
      ---
      O_next_review_date := (O_review_time + I_repl_date - 1);

   else --bulk processing
      if CALC_OLTS_AND_RT(O_error_message,
                          O_colt,
                          O_nolt,
                          O_review_time,
                          O_days_added_to_colt,
                          O_days_added_to_nolt,
                          L_current_deliv_date,
                          L_next_deliv_date,
                          IO_last_dd,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          I_loc_activity_ind,
                          I_loc_dlvry_ind,
                          NULL,
                          I_thread_id) = FALSE then
         return FALSE;
      end if;

      if CALC_WH_TO_STORE_OLTS_AND_RT(O_error_message,
                                      L_store_colt,
                                      L_store_nolt,
                                      L_days_added_to_st_colt,
                                      L_days_added_to_st_nolt,
                                      O_review_time,
                                      IO_last_dd,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      I_loc_activity_ind,
                                      I_loc_dlvry_ind,
                                      NULL,
                                      I_thread_id) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'REPL_OLT_SQL.GET_OLTS_AND_REVIEW_TIME',
                                            to_char(SQLCODE));
      return FALSE;
END GET_OLTS_AND_REVIEW_TIME;
------------------------------------------------------------------------------------------
END REPL_OLT_SQL;
/