CREATE OR REPLACE PACKAGE BODY RMSSUB_XTSF AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
   -- PUBLIC PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2) IS

   PROGRAM_ERROR    EXCEPTION;

   L_program               VARCHAR2(50)   := 'RMSSUB_XTSF.CONSUME';

   L_message_type          VARCHAR2(15)   := LOWER(I_message_type);
   L_ref_message           "RIB_XTsfRef_REC";
   L_message               "RIB_XTsfDesc_REC";
   L_tsf_rec               TSF_REC;

BEGIN

   O_STATUS_CODE := API_CODES.SUCCESS;

   -- Perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Check if the message is NULL
   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   if L_message_type in (LP_cre_type, LP_mod_type, LP_dtl_mod_type, LP_dtl_cre_type) then
      L_message := treat(I_MESSAGE AS "RIB_XTsfDesc_REC");
      
      if L_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;
      -- Validate Message Contents
      if RMSSUB_XTSF_VALIDATE.CHECK_MESSAGE(O_error_message,
                                            L_tsf_rec,
                                            L_message,
                                            L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- INSERT/UPDATE table
      if RMSSUB_XTSF_SQL.PERSIST(O_error_message,
                                 L_tsf_rec,
                                 L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   elsif L_message_type in (LP_del_type, LP_dtl_del_type) then
      L_ref_message := treat(I_MESSAGE AS "RIB_XTsfRef_REC");

      if L_ref_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      -- Validate Message Contents
      if RMSSUB_XTSF_VALIDATE.CHECK_MESSAGE(O_error_message,
                                              L_tsf_rec,
                                              L_ref_message,
                                              L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- DELETE from table
      if RMSSUB_XTSF_SQL.PERSIST(O_error_message,
                                   L_tsf_rec,
                                   L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(L_message_type, 'NULL'), NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
END CONSUME;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE BODY
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2) IS

   L_program VARCHAR2(50) := 'RMSSUB_XTSF.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);
EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XTSF;
/
