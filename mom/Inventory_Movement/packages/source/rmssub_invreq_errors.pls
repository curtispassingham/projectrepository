
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_INVREQ_ERROR AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
-- Usage:
--
-- In the item/store order request process, call INIT before processing any inv   
-- reqs. This will initialize all of the global variables.
--
-- For each invreq record, call BEGIN_INVREQ.  This function will hold the
-- header level values in global variables which may be used to build an error
-- record when necessary.
--
-- When an error is encountered in the inv req process, call ADD_ERROR.  This
-- function adds the error type/description and error object on the global error 
-- table.
--
-- At the end of the inv req process, call FINISH.  This will pass out a copy
-- of the global error table (if any errors exist) which is then sent to the
-- RIB for further processing.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- INIT
-- Initializes global variables.
-------------------------------------------------------------------------------
FUNCTION INIT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
              I_message_type   IN      VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- BEGIN_INVREQ
-- Copies header level info for a receipt into global variables.  Used to
-- create an error record during carton level receiving.
-------------------------------------------------------------------------------
FUNCTION BEGIN_INVREQ(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_request_id       IN      NUMBER,
                      I_store            IN      STORE_ORDERS.STORE%TYPE,
                      I_request_type     IN      VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- ADD_ERROR
-- Used whenever an item or carton error occurs within the receiving process.
-- Saves the error information to a temp PL/SQL table of errors.
-------------------------------------------------------------------------------
FUNCTION ADD_ERROR(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_error_desc     IN      VARCHAR2,
                   I_error_object   IN      RIB_OBJECT)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- FINISH
-- Copies global error table (if any errors exist) to the output parameter.
-- This table is then sent back to the RIB for further processing.
-------------------------------------------------------------------------------
FUNCTION FINISH(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                O_rib_error_tbl     OUT  RIB_ERROR_TBL)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
END RMSSUB_INVREQ_ERROR;
/
