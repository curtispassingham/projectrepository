
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_APPOINT AS


---------------------------------------------------------------------------------
--Local Specs
---------------------------------------------------------------------------------
PROCEDURE HDR_ADD_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointdesc_rec IN      "RIB_AppointDesc_REC");
---------------------------------------------------------------------------------
PROCEDURE HDR_UPD_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointdesc_rec IN      "RIB_AppointDesc_REC");
---------------------------------------------------------------------------------
PROCEDURE HDR_DEL_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointref_rec  IN      "RIB_AppointRef_REC");
---------------------------------------------------------------------------------
PROCEDURE DTL_ADD_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointdesc_rec IN      "RIB_AppointDesc_REC");
---------------------------------------------------------------------------------
PROCEDURE DTL_UPD_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointdesc_rec IN      "RIB_AppointDesc_REC");
---------------------------------------------------------------------------------
PROCEDURE DTL_DEL_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointref_rec  IN      "RIB_AppointRef_REC");
---------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  VARCHAR2,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2);
---------------------------------------------------------------------------------
                        
-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION -- 
-- Called by RMSSUB_RECEIVING.CONSUME when the message type corresponds to an Appointment
-------------------------------------------------------------------------------------------------------

FUNCTION CONSUME(O_status_code          IN OUT  VARCHAR2,
                 O_error_message        IN OUT  VARCHAR2,
                 I_message              IN      RIB_OBJECT,
                 I_message_type         IN      VARCHAR2)
return BOOLEAN IS

   L_module VARCHAR2(100) := 'RMSSUB_APPOINT.CONSUME';
 
BEGIN
   
   if lower(I_message_type) = RMSSUB_RECEIVING.APPOINT_HDR_ADD then
      HDR_ADD_CONSUME(O_status_code,
                      O_error_message,
                      treat (I_message as "RIB_AppointDesc_REC"));
      if O_status_code = API_CODES.UNHANDLED_ERROR then
         return FALSE;
      end if;

   elsif lower(I_message_type) = RMSSUB_RECEIVING.APPOINT_HDR_UPD then
      HDR_UPD_CONSUME(O_status_code,
                      O_error_message,
                      treat (I_message as "RIB_AppointDesc_REC"));
      if O_status_code = API_CODES.UNHANDLED_ERROR then
         return FALSE;
      end if;

   elsif lower(I_message_type) = RMSSUB_RECEIVING.APPOINT_HDR_DEL then
      HDR_DEL_CONSUME(O_status_code,
                      O_error_message,
                      treat (I_message as "RIB_AppointRef_REC"));
      if O_status_code = API_CODES.UNHANDLED_ERROR then
         return FALSE;
      end if;

   elsif lower(I_message_type) = RMSSUB_RECEIVING.APPOINT_DTL_ADD then
      DTL_ADD_CONSUME(O_status_code,
                      O_error_message,
                      treat (I_message as "RIB_AppointDesc_REC"));
      if O_status_code = API_CODES.UNHANDLED_ERROR then
         return FALSE;
      end if;

   elsif lower(I_message_type) = RMSSUB_RECEIVING.APPOINT_DTL_UPD then
      DTL_UPD_CONSUME(O_status_code,
                      O_error_message,
                      treat (I_message as "RIB_AppointDesc_REC"));
      if O_status_code = API_CODES.UNHANDLED_ERROR then
         return FALSE;
      end if;

   elsif lower(I_message_type) = RMSSUB_RECEIVING.APPOINT_DTL_DEL then
      DTL_DEL_CONSUME(O_status_code,
                      O_error_message,
                      treat (I_message as "RIB_AppointRef_REC"));
      if O_status_code = API_CODES.UNHANDLED_ERROR then
         return FALSE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_message_type',
                                            I_message_type,L_module);
      O_status_code := API_CODES.UNHANDLED_ERROR;
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
 when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'RMSSUB_RECEIVE.CONSUME',
                                          to_char(SQLCODE));   
    return FALSE;

END CONSUME;
----------------------------------------------------------------------------------------
/* internal functions */
----------------------------------------------------------------------------------------
PROCEDURE HDR_ADD_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointdesc_rec IN      "RIB_AppointDesc_REC")
IS
   L_module            VARCHAR2(100)  := 'RMSSUB_APPOINT.HDR_ADD_CONSUME';

   dtlCnt               BINARY_INTEGER := 0;

   L_appt_head         APPT_HEAD%ROWTYPE;
   L_appt_detail       APPOINTMENT_PROCESS_SQL.APPTDETAIL_TABLE;

   PROGRAM_ERROR       EXCEPTION;

BEGIN

   L_appt_head.appt     := I_rib_appointdesc_rec.appt_nbr;
   L_appt_head.loc      := to_number(I_rib_appointdesc_rec.from_location);
   L_appt_head.loc_type := NULL;
   L_appt_head.status   := I_rib_appointdesc_rec.appt_action_status;

   if (I_rib_appointdesc_rec.appointdtl_tbl is not NULL) then

      FOR dtlCnt IN I_rib_appointdesc_rec.appointdtl_tbl.FIRST..I_rib_appointdesc_rec.appointdtl_tbl.LAST LOOP

         L_appt_detail(dtlCnt).appt          := L_appt_head.appt;
         L_appt_detail(dtlCnt).loc           := L_appt_head.loc;
         L_appt_detail(dtlCnt).loc_type      := NULL;
         L_appt_detail(dtlCnt).doc           := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).po_nbr;
         if I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).document_type in ('T', 'D', 'V') then
            L_appt_detail(dtlCnt).doc_type      := 'T';
         else
            L_appt_detail(dtlCnt).doc_type      := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).document_type;      
         end if;
         L_appt_detail(dtlCnt).item          := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).item_id;
         L_appt_detail(dtlCnt).asn           := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).asn_nbr;
         L_appt_detail(dtlCnt).qty_appointed := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).unit_qty;
         L_appt_detail(dtlCnt).qty_received  := NULL;
         L_appt_detail(dtlCnt).receipt_no    := NULL;

      END LOOP;
   end if;
   if APPOINTMENT_PROCESS_SQL.PROC_SCH(O_error_message,
                                       L_appt_head,
                                       L_appt_detail) = FALSE then
       raise PROGRAM_ERROR;
   end if;
   
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);

END HDR_ADD_CONSUME;
----------------------------------------------------------------------------------------
PROCEDURE HDR_UPD_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointdesc_rec IN      "RIB_AppointDesc_REC")
IS
   L_module            VARCHAR2(100)  := 'RMSSUB_APPOINT.HDR_UPD_CONSUME';
 
   L_appt_head         APPT_HEAD%ROWTYPE;

   PROGRAM_ERROR       EXCEPTION;
  
BEGIN

   L_appt_head.appt     := I_rib_appointdesc_rec.appt_nbr;
   L_appt_head.loc      := to_number(I_rib_appointdesc_rec.from_location);
   L_appt_head.loc_type := NULL;
   L_appt_head.status   := I_rib_appointdesc_rec.appt_action_status;

   if APPOINTMENT_PROCESS_SQL.PROC_MODHED(O_error_message,
                                          L_appt_head) = FALSE then
       raise PROGRAM_ERROR;
   end if;

   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);

END HDR_UPD_CONSUME;
----------------------------------------------------------------------------------------
PROCEDURE HDR_DEL_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointref_rec  IN      "RIB_AppointRef_REC")
IS
   L_module         VARCHAR2(100)      := 'RMSSUB_APPOINT.HDR_DEL_CONSUME';
   L_appt_head      APPT_HEAD%ROWTYPE;
   PROGRAM_ERROR    EXCEPTION;

BEGIN
   L_appt_head.appt     := I_rib_appointref_rec.appt_nbr;
   L_appt_head.loc      := to_number(I_rib_appointref_rec.from_location);
   L_appt_head.loc_type := NULL;
   L_appt_head.status   := NULL;
   ---
   if APPOINTMENT_PROCESS_SQL.PROC_DEL(O_error_message,
                                       L_appt_head) = FALSE then
       raise PROGRAM_ERROR;
   end if;
   ---
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);

END HDR_DEL_CONSUME;
----------------------------------------------------------------------------------------
PROCEDURE DTL_ADD_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointdesc_rec IN      "RIB_AppointDesc_REC")
IS
   L_module            VARCHAR2(100)  := 'RMSSUB_APPOINT.DTL_ADD_CONSUME';

   dtlCnt               BINARY_INTEGER := 0;

   L_appt_detail       APPT_DETAIL%ROWTYPE;

   PROGRAM_ERROR       EXCEPTION;

BEGIN
if APPOINTMENT_PROCESS_SQL.INIT_APPT_DETAIL(O_error_message) = FALSE then
       raise PROGRAM_ERROR;
   end if;
   
   FOR dtlCnt IN I_rib_appointdesc_rec.appointdtl_tbl.FIRST..I_rib_appointdesc_rec.appointdtl_tbl.LAST LOOP

      L_appt_detail.appt          := I_rib_appointdesc_rec.appt_nbr;
      L_appt_detail.loc           := to_number(I_rib_appointdesc_rec.from_location);
      L_appt_detail.loc_type      := NULL;
      L_appt_detail.doc           := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).po_nbr;
      if I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).document_type in ('T', 'D', 'V') then
         L_appt_detail.doc_type      := 'T';
      else
         L_appt_detail.doc_type      := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).document_type;      
      end if;
      L_appt_detail.item          := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).item_id;
      L_appt_detail.asn           := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).asn_nbr;
      L_appt_detail.qty_appointed := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).unit_qty;
      L_appt_detail.qty_received  := NULL;
      L_appt_detail.receipt_no    := NULL;

      if APPOINTMENT_PROCESS_SQL.PROC_SCHDET(O_error_message,
                                             L_appt_detail) = FALSE then
          raise PROGRAM_ERROR;
      end if;

   END LOOP;

   if APPOINTMENT_PROCESS_SQL.FLUSH_APPT_DETAIL(O_error_message) = FALSE then
       raise PROGRAM_ERROR;
   end if;

   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);

END DTL_ADD_CONSUME;
----------------------------------------------------------------------------------------
PROCEDURE DTL_UPD_CONSUME(O_status_code         IN OUT  VARCHAR2,
                          O_error_message       IN OUT  VARCHAR2,
                          I_rib_appointdesc_rec IN      "RIB_AppointDesc_REC")
IS
   L_module            VARCHAR2(100)         := 'RMSSUB_APPOINT.DTL_UPD_CONSUME';
  
   dtlCnt               BINARY_INTEGER := 0;

   L_appt_detail       APPT_DETAIL%ROWTYPE;

   PROGRAM_ERROR       EXCEPTION;

BEGIN

   FOR dtlCnt IN I_rib_appointdesc_rec.appointdtl_tbl.FIRST..I_rib_appointdesc_rec.appointdtl_tbl.LAST LOOP

      L_appt_detail.appt          := I_rib_appointdesc_rec.appt_nbr;
      L_appt_detail.loc           := to_number(I_rib_appointdesc_rec.from_location);
      L_appt_detail.loc_type      := NULL;
      L_appt_detail.doc           := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).po_nbr;
      if I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).document_type in ('T', 'D', 'V') then
         L_appt_detail.doc_type      := 'T';
      else
         L_appt_detail.doc_type      := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).document_type;      
      end if;
      L_appt_detail.item          := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).item_id;
      L_appt_detail.asn           := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).asn_nbr;
      L_appt_detail.qty_appointed := I_rib_appointdesc_rec.appointdtl_tbl(dtlCnt).unit_qty;
      L_appt_detail.qty_received  := NULL;
      L_appt_detail.receipt_no    := NULL;

      if APPOINTMENT_PROCESS_SQL.PROC_MODDET(O_error_message,
                                             L_appt_detail) = FALSE then
          raise PROGRAM_ERROR;
      end if;

   END LOOP;

   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);

END DTL_UPD_CONSUME;
----------------------------------------------------------------------------------------
PROCEDURE DTL_DEL_CONSUME(O_status_code           IN OUT  VARCHAR2,
                          O_error_message         IN OUT  VARCHAR2,
                          I_rib_appointref_rec    IN      "RIB_AppointRef_REC")
IS
   L_module         VARCHAR2(100)      := 'RMSSUB_APPOINT.DTL_DEL_CONSUME';
   L_appt_detail    APPT_DETAIL%ROWTYPE;
   PROGRAM_ERROR    EXCEPTION;

BEGIN
 


   FOR dtlCnt IN I_rib_appointref_rec.appointdtlref_tbl.FIRST..I_rib_appointref_rec.appointdtlref_tbl.LAST LOOP

      L_appt_detail.appt          := I_rib_appointref_rec.appt_nbr;
      L_appt_detail.loc           := to_number(I_rib_appointref_rec.from_location);
      L_appt_detail.loc_type      := NULL;
      L_appt_detail.doc           := I_rib_appointref_rec.appointdtlref_tbl(dtlCnt).po_nbr;
      L_appt_detail.doc_type      := NULL;
      L_appt_detail.item          := I_rib_appointref_rec.appointdtlref_tbl(dtlCnt).item_id;
      L_appt_detail.asn           := I_rib_appointref_rec.appointdtlref_tbl(dtlCnt).asn_nbr;
      L_appt_detail.qty_appointed := NULL;
      L_appt_detail.qty_received  := NULL;
      L_appt_detail.receipt_no    := NULL;
      ---
      if APPOINTMENT_PROCESS_SQL.PROC_DELDET(O_error_message,
                                             L_appt_detail) = FALSE then
          raise PROGRAM_ERROR;
      end if;

   END LOOP;
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);

END DTL_DEL_CONSUME;
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  VARCHAR2,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2)
IS
   L_module  VARCHAR2(100)  := 'RMSSUB_APPOINTDTLDEL.HANDLE_ERRORS';

BEGIN   
   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);
   
EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_module,
                                             To_Char(SQLCODE));
      ---
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_module);

END HANDLE_ERRORS;
----------------------------------------------------------------------------------------
END RMSSUB_APPOINT;
/
