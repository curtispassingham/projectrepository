CREATE OR REPLACE PACKAGE BODY CORESVC_FULFILORD AS
-------------------------------------------------------------------------------------

-- Private Function
-------------------------------------------------------------------------------------
-- Procedure Name: SET_TRAN_TYPE
-- Purpose: This function will set the tran_type of SVC_FULFILORD and
--          SVC_FULFILORDREF table based on source_loc_type.
-------------------------------------------------------------------------------------
FUNCTION SET_TRAN_TYPE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE,
                        I_action_type     IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
FUNCTION CREATE_FULFILLMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_ordcust_ids     IN OUT   ID_TBL,
                            I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255) := 'CORESVC_FULFILORD.CREATE_FULFILLMENT';
   L_invalid_param    VARCHAR2(30)  := NULL;
   L_ordcust_ids      ID_TBL;
   
BEGIN

   if I_process_id is NULL then
      L_invalid_param := 'I_process_id';
   elsif I_chunk_id is NULL then
      L_invalid_param := 'I_chunk_id';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   -- Validate Create Message
   if CORESVC_FULFILORD_VALIDATE.CHECK_MESSAGE_CREATE(O_error_message,
                                                      I_process_id,
                                                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- Set Tran Type Based on Source Loc Type of Fulfillment
   if CORESVC_FULFILORD.SET_TRAN_TYPE(O_error_message,
                                      I_process_id,
                                      I_chunk_id,
                                      CORESVC_FULFILORD.ACTION_TYPE_CREATE) = FALSE then
      return FALSE;
   end if;

   O_ordcust_ids := ID_TBL();

   -- Create Transfer Fulfillment
   if CORESVC_FULFILORD_TSF.CREATE_TSF(O_error_message,
                                       O_ordcust_ids,
                                       I_process_id,
                                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- Create PO Fulfillment
   if CORESVC_FULFILORD_PO.CREATE_PO(O_error_message,
                                     L_ordcust_ids,
                                     I_process_id,
                                     I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- combine the ordcust_ids into a single collection as the output for building confirmation message
   if L_ordcust_ids is NOT NULL and L_ordcust_ids.COUNT > 0 then
      for i in L_ordcust_ids.FIRST .. L_ordcust_ids.LAST loop
          O_ordcust_ids.EXTEND;
          O_ordcust_ids(O_ordcust_ids.COUNT) := L_ordcust_ids(i);
      end loop;
   end if;

   -- Create Inventory Fulfillment
   if CORESVC_FULFILORD_INV.RESERVE_INVENTORY(O_error_message,
                                              I_process_id,
                                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_FULFILLMENT;
-------------------------------------------------------------------------------------
FUNCTION CANCEL_FULFILLMENT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255) := 'CORESVC_FULFILORD.CANCEL_FULFILLMENT';
   L_invalid_param    VARCHAR2(30)  := NULL;

BEGIN

   if I_process_id is NULL then
      L_invalid_param := 'I_process_id';
   elsif I_chunk_id is NULL then
      L_invalid_param := 'I_chunk_id';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   -- Validate Cancel Message
   if CORESVC_FULFILORD_VALIDATE.CHECK_MESSAGE_CANCEL(O_error_message,
                                                      I_process_id,
                                                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- Set Tran Type Based on Source Loc Type of Fulfillment
   if CORESVC_FULFILORD.SET_TRAN_TYPE(O_error_message,
                                      I_process_id,
                                      I_chunk_id,
                                      CORESVC_FULFILORD.ACTION_TYPE_CANCEL) = FALSE then
      return FALSE;
   end if;

   -- Cancel Transfer Fulfillment
   if CORESVC_FULFILORD_TSF.CANCEL_TSF(O_error_message,
                                      I_process_id,
                                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- Cancel PO Fulfillment
   if CORESVC_FULFILORD_PO.CANCEL_PO(O_error_message,
                                     I_process_id,
                                     I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- Cancel Inventory Fulfillment
   if CORESVC_FULFILORD_INV.RELEASE_INVENTORY(O_error_message,
                                              I_process_id,
                                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CANCEL_FULFILLMENT;
-------------------------------------------------------------------------------------
FUNCTION SET_TRAN_TYPE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE,
                        I_action_type     IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program         VARCHAR2(255) := 'CORESVC_FULFILORD.SET_TRAN_TYPE';
   L_invalid_param   VARCHAR2(30)  := NULL;

   L_table           VARCHAR2(30)  := NULL;
   L_key1            VARCHAR2(100) := TO_CHAR(I_process_id);
   L_key2            VARCHAR2(100) := TO_CHAR(I_chunk_id);

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_FULFILORD is
      select 'x'
        from svc_fulfilord
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         for update nowait;

   cursor C_LOCK_FULFILORDREF is
      select 'x'
        from svc_fulfilordref
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         for update nowait;

BEGIN

   if I_process_id is NULL then
      L_invalid_param := 'I_process_id';
   elsif I_chunk_id is NULL then
      L_invalid_param := 'I_chunk_id';
   elsif I_action_type is NULL then
     L_invalid_param := 'I_action_type';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   if LOWER(I_action_type) = LOWER(CORESVC_FULFILORD.ACTION_TYPE_CREATE) then
      L_table := 'svc_fulfilord';
      ---
      open C_LOCK_FULFILORD;
      close C_LOCK_FULFILORD;
      ---
      merge into svc_fulfilord sf
         using (select process_id,
                       chunk_id,
                       fulfilord_id,
                       case 
                          when source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_SUPPLIER then
                             RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
                          when source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE then
                             RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                          when source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH then
                             RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                          when source_loc_type is NULL then
                             RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_INV
                       end new_tran_type
                  from svc_fulfilord sf1
                 where process_id = I_process_id
                  and chunk_id = I_chunk_id) use_this
         on (sf.process_id   = use_this.process_id and
             sf.chunk_id     = use_this.chunk_id and
             sf.fulfilord_id = use_this.fulfilord_id)
      when matched then
         update
            set sf.tran_type = use_this.new_tran_type;

   end if;

   if LOWER(I_action_type) = LOWER(CORESVC_FULFILORD.ACTION_TYPE_CANCEL) then
      L_table := 'svc_fulfilordref';
      ---
      open C_LOCK_FULFILORDREF;
      close C_LOCK_FULFILORDREF;
      ---
      merge into svc_fulfilordref sf
         using (select process_id,
                       chunk_id,
                       fulfilordref_id,
                       case 
                          when source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_SUPPLIER then
                             RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
                          when source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE then
                             RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                          when source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH then
                             RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                          when source_loc_type is NULL then
                             RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_INV
                       end new_tran_type
                  from svc_fulfilordref sf1
                 where process_id = I_process_id
                  and chunk_id = I_chunk_id) use_this
         on (sf.process_id      = use_this.process_id and
             sf.chunk_id        = use_this.chunk_id and
             sf.fulfilordref_id = use_this.fulfilordref_id)
      when matched then
         update
            set sf.tran_type = use_this.new_tran_type;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_TRAN_TYPE;
-------------------------------------------------------------------------------------
END CORESVC_FULFILORD;
/
