CREATE OR REPLACE PACKAGE RMSSUB_FULFILORD AUTHID CURRENT_USER AS
----------------------------------------------------------------------------

-- Message Constant
MSG_FAMILY                       CONSTANT VARCHAR2(25) := 'fulfilord';
MSG_TYPE_CANCEL_APPROVE          CONSTANT VARCHAR2(25) := 'fulfilordapprdel';
MSG_TYPE_CANCEL_REQUEST          CONSTANT VARCHAR2(25) := 'fulfilordreqdel';
MSG_TYPE_CREATE_PO               CONSTANT VARCHAR2(25) := 'fulfilordpocre';
MSG_TYPE_CREATE_TSF              CONSTANT VARCHAR2(25) := 'fulfilordtsfcre';
MSG_TYPE_CREATE_STORE_DELIVERY   CONSTANT VARCHAR2(25) := 'fulfilordstdlvcre';

----------------------------------------------------------------------------
-- Procedure Name: CONSUME
-- Purpose: This public procedure is to be called from the RIB. 
----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_FULFILORD;
/
