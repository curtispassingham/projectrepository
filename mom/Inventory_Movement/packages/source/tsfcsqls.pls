CREATE OR REPLACE PACKAGE TRANSFER_COST_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: CALC_ACTIVITY_COSTS
-- Purpose      : Calculates the total qty and average tsf_price for items on
--                intercompany transfers and inserts the value into the tsf_item_cost table.
--                Calculates the average unit cost for work order activities applied to the
--                item and inserts the value into the tsf_item_wo_cost table.
---------------------------------------------------------------------------------------------
FUNCTION CALC_ACTIVITY_COSTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PCT_IN_PACK
-- Purpose      : Calculates the percentage of a component item unit value with respect to the
--                pack's unit value at a specified location. The item or pack are valued at 
--                the average cost or the unit cost depending on the std_av_ind in system 
--                options.
---------------------------------------------------------------------------------------------
FUNCTION PCT_IN_PACK(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_pct_in_pack   IN OUT NUMBER,
                     I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                     I_item          IN     ITEM_MASTER.ITEM%TYPE,
                     I_loc           IN     ITEM_LOC_SOH.LOC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PCT_IN_PACK_UNIT_COST
-- Purpose      : Calculates the percentage of a component item unit value with respect to the
--                pack's unit value at a specified location. The item or pack are valued at 
--                the unit cost. 
---------------------------------------------------------------------------------------------
FUNCTION PCT_IN_PACK_UNIT_COST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_pct_in_pack   IN OUT NUMBER,
                               I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE,
                               I_loc           IN     ITEM_LOC_SOH.LOC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: RECALC_WAC
-- Purpose      : Calculates the weighted average cost for the passed in item. For intercompany
--                transfers, the new cost will be retrieved from tsf_item_cost or tsfdetail
--                (tsf_price). For intracompany transfers, the new cost will be retrieved from 
--                tsfdetail (tsf_cost). If not found, normal 'from' loc wac are used. 
--
--                For franchise orders, fixed_cost or customer order (also known as 'sale price')
--                is used. Franchise returns are treated as customer returns. The stock is put back 
--                to the company location at the current WAC of the company location. So WAC is 
--                not recalculated for franchise returns.
--
--                The passed in to loc upcharges will be added to the new cost.
-- 
--                When the tsf is for the second leg of a multi-legged tsf the work order costs
--                will be added to the new cost along with the passed in to loc upcharges.
--
--                For a simple pack catch weight item, the weight of transferred qty and the 
--                pack's nominal weight (both in item's cost unit of measure) will be passed
--                in to derive the item's total cost.
--                
--                I_recalc_from_to_loc ('T' (to-loc) or 'F' (from-loc), default to 'T') indicates 
--                if the WAC recalculation is to be done for from-loc or to-loc.  WAC is only 
--                recalculated for the from-loc in case of a stock reconciliation, in which case,
--                shipsku.unit_cost should be used.
--
--                Output O_new_wac returns the new wac in either from-loc or to-loc's currency 
--                depending on I_recalc_from_to_loc. 
---------------------------------------------------------------------------------------------
FUNCTION RECALC_WAC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_new_wac             IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                    I_distro_no           IN       TSFHEAD.TSF_NO%TYPE,
                    I_distro_type         IN       VARCHAR2,
                    I_item                IN       TSFDETAIL.ITEM%TYPE,
                    I_pack_no             IN       ITEM_MASTER.ITEM%TYPE,
                    I_percent_in_pack     IN       NUMBER,
                    I_from_loc            IN       ITEM_LOC.LOC%TYPE,
                    I_from_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_to_loc              IN       ITEM_LOC.LOC%TYPE,
                    I_to_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                    I_weight_cuom         IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                    I_from_wac            IN       ITEM_LOC_SOH.AV_COST%TYPE, 
                    I_to_loc_charge       IN       ITEM_LOC_SOH.AV_COST%TYPE,
                    I_intercompany        IN       BOOLEAN,
                    I_recalc_from_to_loc  IN       VARCHAR2 DEFAULT 'T',
                    I_shipment            IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                    I_carton              IN       SHIPSKU.CARTON%TYPE DEFAULT NULL,
                    I_chrg_from_loc       IN       ITEM_LOC_SOH.AV_COST%TYPE DEFAULT NULL,
                    I_wrong_st_rcv_ind    IN       VARCHAR2 DEFAULT NULL,
                    I_tran_date           IN       TRAN_DATA.TRAN_DATE%TYPE DEFAULT NULL)
return BOOLEAN;
-----------------------------------------------------------------------------------
--Function Name:  RECALC_WAC
--Purpose: This overloaded function is created to support calls from STOCK_ORDER_RECONCILE_SQL. 
--         It gets the from loc's wac and in turn calls the other RECALC_WAC.
-----------------------------------------------------------------------------------
FUNCTION RECALC_WAC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_new_wac             IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                    I_distro_no           IN       TSFHEAD.TSF_NO%TYPE,
                    I_distro_type         IN       VARCHAR2,
                    I_item                IN       TSFDETAIL.ITEM%TYPE,
                    I_pack_no             IN       ITEM_MASTER.ITEM%TYPE,
                    I_percent_in_pack     IN       NUMBER,
                    I_from_loc            IN       ITEM_LOC.LOC%TYPE,
                    I_from_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_to_loc              IN       ITEM_LOC.LOC%TYPE,
                    I_to_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                    I_to_loc_charge       IN       ITEM_LOC_SOH.AV_COST%TYPE,
                    I_intercompany        IN       BOOLEAN,
                    I_recalc_from_to_loc  IN       VARCHAR2 DEFAULT 'T',
                    I_shipment            IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                    I_carton              IN       SHIPSKU.CARTON%TYPE DEFAULT NULL)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_TSF_COST_PRICE
-- Purpose      : Returns the transfer cost or transfer price of I_item in to loc's currency.
--                For intracompany transfers, it is the tsf_cost on tsfdetail. For intercompany
--                transfers, it is the tsf_price on tsfdetail for the first leg transfer. For
--                the second or single leg transfer, it is the tsf_avg_price on tsf_item_cost. 
--                For a second leg transfer, it also returns the total work order cost for I_item.
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_COST_PRICE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_tsf_cost_price     IN OUT TSFDETAIL.TSF_PRICE%TYPE,
                            O_total_wo_cost      IN OUT TSF_ITEM_WO_COST.AVG_UNIT_COST%TYPE,
                            I_tsf_no             IN     TSFHEAD.TSF_NO%TYPE,
                            I_item               IN     ITEM_MASTER.ITEM%TYPE,
                            I_pack_no            IN     PACKITEM.PACK_NO%TYPE,
                            I_percent_in_pack    IN     NUMBER,
                            I_from_loc           IN     ITEM_LOC.LOC%TYPE,
                            I_from_loc_type      IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_to_loc             IN     ITEM_LOC.LOC%TYPE,
                            I_to_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_intercompany       IN     BOOLEAN)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_TSF_AVG_PRICE
-- Purpose      : Gets the tsf_avg_price from tsf_item_cost for the passed in transfer and item.
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_AVG_PRICE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_avg_price     IN OUT TSF_ITEM_COST.TSF_AVG_PRICE%TYPE,
                           I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                           I_tsf_parent_no IN     TSFHEAD.TSF_NO%TYPE,
                           I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_TOTAL_WO_UPD_INV_COSTS
-- Purpose      : Sums the avg_unit_costs for all activities with cost_type of 'U'. Either the 
--                tsf_item_cost_id is not null or else tsf_no and item are not null.  Zero is 
--                returned when no activity cost are found or if tsf/item/tsf_item_cost_id is 
--                not found.
---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_WO_UPD_INV_COSTS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_sum_wo_uicosts   IN OUT TSF_ITEM_WO_COST.AVG_UNIT_COST%TYPE,
                                    I_tsf_item_cost_id IN     TSF_ITEM_WO_COST.TSF_ITEM_COST_ID%TYPE,
                                    I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE,
                                    I_item             IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_TSF_ALLOC_COST_PRICE
-- Purpose      : Returns the cost of a transfer or allocation for I_item in to loc's currency.
--                For a franchise order (transfer or allocation), the cost is based on fixed_cost
--                or customer_cost. For a franchise return (transfer), the cost is based on
--                return_unit_cost. For a non-franchise transfer, this function calls GET_TSF_COST_PRICE
--                to account for intercompany, intracompany and multi-legged transfers.
--                For intracompany transfers, it is the tsf_cost on tsfdetail. For intercompany
--                transfers, it is the tsf_price on tsfdetail for the first leg transfer. For
--                the second or single leg transfer, it is the tsf_avg_price on tsf_item_cost. 
--                For a second leg transfer, it also returns the total work order cost for I_item.
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_ALLOC_COST_PRICE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_tsf_cost_price        IN OUT TSFDETAIL.TSF_PRICE%TYPE,
                                  O_total_wo_cost         IN OUT TSF_ITEM_WO_COST.AVG_UNIT_COST%TYPE,
                                  I_distro_no             IN     SHIPSKU.DISTRO_NO%TYPE,
                                  I_distro_type           IN     SHIPSKU.DISTRO_TYPE%TYPE,
                                  I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                                  I_pack_no               IN     PACKITEM.PACK_NO%TYPE,
                                  I_percent_in_pack       IN     NUMBER,
                                  I_from_loc              IN     ITEM_LOC.LOC%TYPE,
                                  I_from_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                                  I_to_loc                IN     ITEM_LOC.LOC%TYPE,
                                  I_to_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE,
                                  I_intercompany          IN     BOOLEAN,
                                  I_franchise_ordret_ind  IN     VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END;
/

