CREATE OR REPLACE PACKAGE BODY RMSSUB_ASNOUT AS
TYPE distro_record IS RECORD (distro_no              TSFHEAD.TSF_NO%TYPE,
                              distro_type            VARCHAR2(2),
                              franchise_ordret_ind   VARCHAR2(1),  --'O' for franchise order, 'R' for franchise, 'N' for non-franchise
                              customer_order_no      ORDCUST.CUSTOMER_ORDER_NO%TYPE,
                              fulfill_order_no       ORDCUST.FULFILL_ORDER_NO%TYPE,
                              comments               TSFHEAD.COMMENT_DESC%TYPE);

-- Define a record to store item records
TYPE item_record IS RECORD (item                 ITEM_MASTER.ITEM%TYPE,
                            carton               SHIPSKU.CARTON%TYPE,
                            qty                  TSFDETAIL.TSF_QTY%TYPE,
                            weight               ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                            weight_uom           UOM_CLASS.UOM%TYPE,
                            inv_status           INV_STATUS_CODES.INV_STATUS%TYPE,
                            extended_base_cost   ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE,
                            base_cost            ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE);

-- Define a table based upon the item record defined above.
 TYPE item_table IS TABLE OF item_record INDEX BY BINARY_INTEGER;

LP_inv_status       INV_STATUS_CODES.INV_STATUS%TYPE;
LP_ils_item_tbl     ITEM_TABLE;

-- Define a table type to hold transfer number for repair context transfers
TYPE tsf_record IS RECORD  (tsf_no             TSFHEAD.TSF_NO%TYPE,
                            from_loc_type      TSFHEAD.FROM_LOC_TYPE%TYPE);
TYPE tsf_no_table IS TABLE OF tsf_record INDEX BY BINARY_INTEGER;
LP_repair_tsf_no_tbl        tsf_no_table;
LP_tbl_size                 BINARY_INTEGER := 0;

-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  VARCHAR2,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2);
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_BOL (O_error_message  IN OUT  VARCHAR2,
                    O_bol_record        OUT  shipment%rowtype,
                    I_asnout_message IN      "RIB_ASNOutDesc_REC",
                    IO_bol_exists    IN OUT  BOOLEAN)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_DISTRO(O_error_message  IN OUT  VARCHAR2,
                      O_distro_record     OUT  distro_record,
                      I_distro         IN      "RIB_ASNOutDistro_REC")
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_ITEM (O_error_message  IN OUT  VARCHAR2,
                     O_item_table     IN OUT  nocopy item_table,
                     I_carton         IN      shipsku.carton%TYPE,
                     I_item           IN      item_record)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_BOL (O_error_message    IN OUT VARCHAR2,
                      I_bol              IN     shipment%rowtype,
                      IO_bol_exists      IN OUT BOOLEAN)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DISTRO (O_error_message    IN OUT   VARCHAR2,
                         IO_distro          IN OUT   distro_record,
                         I_bol              IN       shipment%rowtype,
                         I_itemtable        IN       item_table)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tsf_no           IN       TSFHEAD.TSF_NO%TYPE,
                      I_to_loc           IN       SHIPMENT.TO_LOC%TYPE,
                      I_to_loc_type      IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                      I_from_loc         IN       SHIPMENT.TO_LOC%TYPE,
                      I_from_loc_type    IN       SHIPMENT.TO_LOC_TYPE%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEMS(O_error_message   IN OUT   VARCHAR2,
                     O_sellable_TBL    OUT      "RIB_ASNOutItem_TBL",
                     O_detail_TBL      OUT      "RIB_ASNOutItem_TBL",
                     I_rib_detail_TBL  IN       "RIB_ASNOutItem_TBL")
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION GET_ORDERABLE_ITEMS(O_error_message   IN OUT   VARCHAR2,
                             O_orderable_TBL   IN OUT   nocopy  item_table,
                             I_sellable_TBL    IN       "RIB_ASNOutItem_TBL",
                             I_distro          IN       distro_record,
                             I_to_loc          IN       ALLOC_DETAIL.TO_LOC%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CONSUME_SHIPMENT_CORE
-- Purpose: This private function contains the core logic of asnout consume process.
------------------------------------------------------------------------------------
FUNCTION CONSUME_SHIPMENT_CORE (O_error_message     IN OUT  VARCHAR2,
                                I_message           IN      RIB_OBJECT,
                                I_message_type      IN      VARCHAR2)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: PROCESS_FRANCHISE_ORDRET
-- Purpose: This private function will handle the franchise order/return logic.  Franchise orders/returns
-- may be created or updated within the call to SYNC_F_ORDER/SYNC_F_RETURN.
------------------------------------------------------------------------------------
FUNCTION PROCESS_FRANCHISE_ORDRET(O_error_message       IN OUT VARCHAR2,
                                  I_distro              IN     DISTRO_RECORD)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE --
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code       IN OUT   VARCHAR2,
                  O_error_message     IN OUT   VARCHAR2,
                  I_message           IN       RIB_OBJECT,
                  I_message_type      IN       VARCHAR2)
IS
   L_program        VARCHAR2(255) := 'RMSSUB_ASNOUT.CONSUME';

   L_check_l10n_ind VARCHAR2(1) := 'Y';

   PROGRAM_ERROR  EXCEPTION;
   L_event_run_type           COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE := NULL;

   cursor C_GET_RUN_TYPE is 
        select event_run_type 
           from cost_event_run_type_config 
         where event_type='NIL';  

BEGIN

   FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE:=NULL;
   O_STATUS_CODE := API_CODES.SUCCESS;

   open C_GET_RUN_TYPE;
   fetch C_GET_RUN_TYPE into L_event_run_type;
   close C_GET_RUN_TYPE;

   if L_event_run_type = FUTURE_COST_EVENT_SQL.SYNC_COST_EVENT_RUN_TYPE then
     FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE := FUTURE_COST_EVENT_SQL.ASYNC_COST_EVENT_RUN_TYPE;
   end if;

   -- Perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Check message type
   if I_message_type is NULL or LOWER(I_message_type) != LP_cre_type then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;

   CONSUME_SHIPMENT(O_status_code,
                    O_error_message,
                    I_message,
                    I_message_type,
                    L_check_l10n_ind);  -- 'Y'

   if O_status_code != API_CODES.SUCCESS then
      raise PROGRAM_ERROR;
   end if;

   FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE:=NULL;   
      
   return;

EXCEPTION
   when PROGRAM_ERROR then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END CONSUME;
-------------------------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE --
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME_SHIPMENT(O_status_code       IN OUT  VARCHAR2,
                           O_error_message     IN OUT  VARCHAR2,
                           I_message           IN      RIB_OBJECT,
                           I_message_type      IN      VARCHAR2,
                           I_check_l10n_ind    IN      VARCHAR2)
IS

   L_program            VARCHAR2(255) := 'RMSSUB_ASNOUT.CONSUME_SHIPMENT';

   L_asnout_message    "RIB_ASNOutDesc_REC";
   L_l10n_rib_rec      "L10N_RIB_REC"          := L10N_RIB_REC();
   PROGRAM_ERROR       EXCEPTION;

BEGIN
   --
   L_l10n_rib_rec.rib_msg         :=  I_message;
   L_l10n_rib_rec.rib_msg_type    :=  I_message_type;
   L_l10n_rib_rec.procedure_key   := 'CONSUME_SHIPMENT';
   --
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   --
   if I_message_type is NULL or LOWER(I_message_type) != LP_cre_type then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;
   ---
   L_asnout_message := treat(I_message as "RIB_ASNOutDesc_REC");

   --- "Store -> customer" fulfillment request will not have associated transfer in RMS.
   --- When SIM ships the customer order, SIM will generate an ASNOut message with NULL to_location or 'C' to_loc_type.
   --- Since there are no associated transfers in RMS, RMS will NOT process these ASNOut messages with NULL to_location or 'C' to_loc_type.
   --- The reserved inventory will be backed out in RMS when RMS processes a SALES transaction through posupld.
   if L_asnout_message.to_location is NULL or L_asnout_message.to_loc_type = 'C' then
      O_status_code := API_CODES.SUCCESS;
      return;
   else
      if I_check_l10n_ind = 'Y' then
         --
         --- This code below is to find out the info need to find the countries in L10N_SQL.EXEC_FUNCTION
         --
         if L_asnout_message is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
            raise PROGRAM_ERROR;
         end if;
         --
         L_l10n_rib_rec.doc_type        := 'TSF';
         L_l10n_rib_rec.source_id       := L_asnout_message.from_location;
         L_l10n_rib_rec.source_type     := NULL;
         L_l10n_rib_rec.source_entity   := 'LOC';
         L_l10n_rib_rec.dest_id         := L_asnout_message.to_location;
         L_l10n_rib_rec.dest_type       := NULL;
         L_l10n_rib_rec.dest_entity     := 'LOC';
         --
         if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                    L_l10n_rib_rec)= FALSE then
            -- This is to capture the 'CF' (compensation failure) status returned from the L10N_SQL and pass it out to the calling function.
            O_status_code := L_l10n_rib_rec.status_code;
            raise PROGRAM_ERROR;
         end if;
         --
      else
         -- When Localization Indicator is other than 'Y'.
         --- This is for the base functionality and also will be called for the country which is not localized.
         if RMSSUB_ASNOUT.CONSUME_SHIPMENT(O_error_message,
                                           L_l10n_rib_rec) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         --
      end if;
      --
   end if;

   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END CONSUME_SHIPMENT;
-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION --
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME_SHIPMENT (O_error_message     IN OUT  VARCHAR2,
                           IO_L10N_RIB_REC     IN OUT  L10N_OBJ)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255) := 'RMSSUB_ASNOUT.CONSUME_SHIPMENT';

   L_l10n_rib_rec    "L10N_RIB_REC" := L10N_RIB_REC();

BEGIN
   --
   L_l10n_rib_rec := treat (IO_L10N_RIB_REC as L10N_RIB_REC);
   if CONSUME_SHIPMENT_CORE(O_error_message,
                            L_l10n_rib_rec.rib_msg,
                            L_l10n_rib_rec.rib_msg_type) = FALSE then
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONSUME_SHIPMENT;
-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION --
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME_SHIPMENT_CORE(O_error_message     IN OUT  VARCHAR2,
                               I_message           IN      RIB_OBJECT,
                               I_message_type      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(255) := 'RMSSUB_ASNOUT.CONSUME_SHIPMENT_CORE';

   L_asnout_message    "RIB_ASNOutDesc_REC";

   L_bol_record        shipment%rowtype;
   L_distro_record     distro_record;
   L_item_table        item_table;
   L_bol_exists        BOOLEAN := FALSE;
   L_carton_exists     BOOLEAN := FALSE;

   L_finisher          BOOLEAN := FALSE;
   L_finisher_name     partner.partner_desc%TYPE := NULL;

   L_detail_TBL        "RIB_ASNOutItem_TBL";
   L_sellable_TBL      "RIB_ASNOutItem_TBL";
   L_orderable_TBL     item_table;
   L_carton            SHIPSKU.CARTON%TYPE := NULL;
   L_item_rec          item_record := NULL;

   L_uom_class         UOM_CLASS.UOM_CLASS%TYPE;

   L_shipment          SHIPMENT.SHIPMENT%TYPE;
   L_status_code       SHIPMENT.STATUS_CODE%TYPE := NULL;
   L_tsfhead_info      V_TSFHEAD%ROWTYPE;

   L_to_loc_type       ITEM_LOC.LOC_TYPE%TYPE  := NULL;

   L_finisher_loc_ind      VARCHAR2(1);
   L_finisher_entity_ind   VARCHAR2(1);
   L_tsf_no                TSFHEAD.TSF_NO%TYPE := NULL;
   L_second_leg_from_loc   TSFHEAD.FROM_LOC%TYPE := NULL;
   L_l10n_exists_rec       L10N_EXISTS_REC := L10N_EXISTS_REC();

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_pub_ship_ind        VARCHAR2(1) := 'N';
   L_chk_loc             ITEM_LOC.LOC%TYPE;
   L_phy_wh              BOOLEAN := FALSE;
   L_transformed_wo_ind  VARCHAR2(1) := 'N';

   cursor C_SHIPMENT is
      select shipment
        from shipment
       where bol_no = L_bol_record.bol_no;

   cursor C_CHECK_SHIPMENT_RECEIPT is
      select status_code
        from shipment
       where bol_no = L_bol_record.bol_no;

   cursor C_FROM_LOC_SECOND_LEG_TSF is
      select from_loc
        from tsfhead
       where tsf_no = L_tsf_no;

   cursor C_CHECK_FIRST_LEG_TRANSFORMED is
      select 'Y'
        from dual
       where EXISTS (select 'Y'
                       from tsf_wo_head
                      where tsf_no = (select tsf_parent_no from tsfhead where tsf_no = L_tsf_no))
          or EXISTS (select 'Y'
                       from tsf_xform
                      where tsf_no = (select tsf_parent_no from tsfhead where tsf_no = L_tsf_no));

BEGIN
   ---
   L_asnout_message := treat(I_message as "RIB_ASNOutDesc_REC");
   if L_asnout_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_to_loc_type,
                                   to_number(L_asnout_message.to_location)) = FALSE then
      return FALSE;
   end if;

   if PARSE_BOL(O_error_message,
                L_bol_record,
                L_asnout_message,
                L_bol_exists) = FALSE then
      return FALSE;
   else
      if L_bol_record.from_loc_type = 'W' then
         if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                         L_finisher,
                                         L_finisher_name,
                                         L_bol_record.from_loc) = FALSE then
            return FALSE;
         end if;
      end if;
      -- Upon shipment from a finisher location inventory will be removed from the available
      -- inventory bucket regardless of the from disposition specified in the shipment message.
      -- Likewise, inventory will be received at the finisher in the available inventory bucket
      -- regardless of the to disposition specified in the receipt message.
      if ((L_finisher) OR (L_bol_record.from_loc_type = 'E')) then

         /* convert the default 'ATS' disposition to an invertory status */
         if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                      LP_inv_status,
                                      'ATS') = FALSE then
            return FALSE;
         end if;
      else
         LP_inv_status := NULL;
      end if;

      ---
      -- the detail records are retrieved in a list
      -- in order to loop through and process one distro at a time.
      for i in L_asnout_message.ASNOutDistro_TBL.FIRST .. L_asnout_message.ASNOutDistro_TBL.LAST LOOP

         if PARSE_DISTRO(O_error_message,
                         L_distro_record,
                         L_asnout_message.ASNOutDistro_TBL(i)) = FALSE then
            return FALSE;
         end if;
         -- validate transfer from RMS
            if L_distro_record.distro_type = 'T' then
            if VALIDATE_TSF(O_error_message,
                            L_distro_record.distro_no,
                            L_bol_record.to_loc,
                            L_bol_record.to_loc_type,
                            L_bol_record.from_loc,
                            L_bol_record.from_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
         -- Delete items from previous distro from the item array.
         L_item_table.DELETE;

         -- Loop through cartons to retrieve items.  Carton level records aren't
         -- processed for BOL's in RMS, so we don't need to fetch the values.
         for j in L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL.FIRST .. L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL.LAST LOOP

            if L_bol_exists = TRUE then
               if BOL_SQL.CARTON_EXISTS(O_error_message,
                                        L_carton_exists,
                                        L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL(j).container_id,
                                        L_bol_record.bol_no) = FALSE then
                  return FALSE;
               end if;
            end if;

            -- L_item_table is only built and processed if shipment does not exist or
            -- if the carton is not on shipsku yet. This is to avoid accidental double
            -- processing of ASN in case the supplier sends an ASN multiple times.
            if (L_bol_exists = FALSE or
                (L_bol_exists = TRUE and L_carton_exists = FALSE)) then

               L_carton := L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL(j).container_id;

               if CHECK_ITEMS(O_error_message,
                              L_sellable_TBL,
                              L_detail_TBL,
                              L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL(j).ASNOutItem_TBL) = FALSE then
                  return FALSE;
               end if;

               if L_sellable_TBL is not NULL and
                  L_sellable_TBL.COUNT > 0 then
                  if GET_ORDERABLE_ITEMS(O_error_message,
                                         L_orderable_TBL, --- Output item_table of orderable items
                                         L_sellable_TBL,
                                         L_distro_record,
                                         L_bol_record.to_loc) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if L_detail_TBL is not NULL and
                  L_detail_TBL.COUNT > 0 then

                  for k in L_detail_TBL.FIRST .. L_detail_TBL.LAST LOOP

                     L_item_rec.item         := L_detail_TBL(k).item_id;
                     L_item_rec.qty          := L_detail_TBL(k).unit_qty;
                     L_item_rec.weight       := L_detail_TBL(k).weight;
                     L_item_rec.weight_uom   := UPPER(L_detail_TBL(k).weight_uom);
                     L_item_rec.extended_base_cost := L_detail_TBL(k).unit_cost;
                     L_item_rec.base_cost          := L_detail_TBL(k).base_cost;
                     ---
                     -- Assign the carton weight to item's weight if a carton only contains
                     -- 1 item, and if carton weight is defined but item's weight is not.
                     if L_detail_TBL.COUNT = 1 then
                        if (L_item_rec.weight is NULL or L_item_rec.weight_uom is NULL) and
                           (L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL(j).weight is NOT NULL and
                            L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL(j).weight_uom is NOT NULL) then

                           if not UOM_SQL.GET_CLASS(O_error_message,
                                                    L_uom_class,
                                                    UPPER(L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL(j).weight_uom)) then
                              return FALSE;
                           end if;

                           if L_uom_class != 'MASS' then
                              O_error_message := SQL_LIB.CREATE_MSG('INV_WGTUOM_CLASS',
                                                                    L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL(j).weight_uom,
                                                                    L_uom_class,
                                                                    NULL);
                              return FALSE;
                           end if;

                           L_item_rec.weight := L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL(j).weight;
                           L_item_rec.weight_uom := UPPER(L_asnout_message.ASNOutDistro_TBL(i).ASNOutCtn_TBL(j).weight_uom);
                        end if;
                     end if;
                     ---
                     if LP_inv_status is not NULL then
                        L_item_rec.inv_status := LP_inv_status;

                     /* convert the disposition to an invertory status */
                     elsif L_detail_TBL(k).from_disposition IS NOT NULL then
                        if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                                     L_item_rec.inv_status,
                                                     L_detail_TBL(k).from_disposition) = FALSE then
                           return FALSE;
                        end if;
                        if L_item_rec.inv_status IS NULL then
                           L_item_rec.inv_status := -1;
                        end if;
                     else
                        L_item_rec.inv_status := -1;
                     end if;

                     if PARSE_ITEM(O_error_message,
                                   L_item_table,
                                   L_carton,
                                   L_item_rec) = FALSE then
                        return FALSE;
                     end if;
                  end LOOP;
               end if;

               if L_orderable_TBL is not NULL and
                  L_orderable_TBL.COUNT > 0 then

                  for k in L_orderable_TBL.FIRST .. L_orderable_TBL.LAST LOOP

                     if PARSE_ITEM(O_error_message,
                                   L_item_table,
                                   L_carton,
                                   L_orderable_TBL(k)) = FALSE then
                        return FALSE;
                     end if;
                  end LOOP;
               end if;
            end if;
         end LOOP;

         if (L_bol_exists = FALSE or
             (L_bol_exists = TRUE and L_item_table.COUNT > 0)) then
            if PROCESS_DISTRO(O_error_message,
                              L_distro_record,
                              L_bol_record,
                              L_item_table) = FALSE then
               return FALSE;
            end if;
         else
            O_error_message := SQL_LIB.CREATE_MSG('DUP_BOL', L_bol_record.bol_no, NULL, NULL);
            return FALSE;
         end if;
      end LOOP;
   end if;

   if BOL_SQL.FLUSH_BOL_PROCESS(O_error_message) = FALSE then
      return FALSE;
   end if;

   if L_distro_record.distro_type = 'T' then
      if TRANSFER_SQL.GET_TSFHEAD_INFO(O_error_message,
                                       L_tsfhead_info,
                                       L_distro_record.distro_no) = FALSE then
         return FALSE;
      end if;
      ---
      if (L_tsfhead_info.tsf_type = 'EG' or
         (L_tsfhead_info.tsf_type = 'CO' and L_system_options_row.oms_ind = 'Y')) and
          (L_tsfhead_info.from_loc_type = 'W' or L_tsfhead_info.to_loc_type = 'W') then
          if L_tsfhead_info.from_loc_type = 'W' then
             L_chk_loc := L_tsfhead_info.from_loc;
          elsif L_tsfhead_info.to_loc_type = 'W' then
             L_chk_loc := L_tsfhead_info.to_loc;
          end if;
          if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                     L_phy_wh,
                                     L_chk_loc) = FALSE then
             return FALSE;
          end if;
      end if;

      if L_phy_wh = FALSE then
         if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                           L_finisher_loc_ind,
                                           L_finisher_entity_ind,
                                           L_distro_record.distro_no)= FALSE then
            return FALSE;
         end if;
      end if;
      ---

      FOR i IN 1..LP_ils_item_tbl.COUNT LOOP
         SQL_LIB.SET_MARK('OPEN',
                          'C_SHIPMENT',
                          'shipment',
                          NULL);
         open C_SHIPMENT;
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_SHIPMENT',
                          'shipment',
                          NULL);
         fetch C_SHIPMENT into L_shipment;
         --
         SQL_LIB.SET_MARK('CLOSE',
                          'C_SHIPMENT',
                          'shipment',
                          NULL);
         close C_SHIPMENT;
         ---

         if L_finisher_loc_ind is NOT NULL then
            if BOL_SQL.PUT_ILS_AV_RETAIL(O_error_message,
                                         L_bol_record.to_loc,
                                         L_bol_record.to_loc_type,
                                         LP_ils_item_tbl(i).item,
                                         L_shipment,
                                         L_distro_record.distro_no,
                                         L_tsfhead_info.tsf_type,
                                         NULL) = FALSE then
               return FALSE;
            end if;
         end if;
      end LOOP;

      LP_ils_item_tbl.DELETE;

   end if;

   --check if the shipment for the BOL was auto-received
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_SHIPMENT_RECEIPT',
                    'shipment',
                    NULL);
   open C_CHECK_SHIPMENT_RECEIPT;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_SHIPMENT_RECEIPT',
                    'shipment',
                    NULL);
   fetch C_CHECK_SHIPMENT_RECEIPT into L_status_code;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_SHIPMENT_RECEIPT',
                    'shipment',
                    NULL);
   close C_CHECK_SHIPMENT_RECEIPT;
   ---

   --if the shipment was auto-received then create shipments for any REPAIR context transfers in the ASN
   if NVL(L_status_code, 'I') = 'R' then
      if LP_repair_tsf_no_tbl is NOT NULL and LP_repair_tsf_no_tbl.COUNT > 0 then
         FOR i in LP_repair_tsf_no_tbl.first..LP_repair_tsf_no_tbl.last LOOP
            --
            L_tsf_no  := LP_repair_tsf_no_tbl(i).tsf_no;

            SQL_LIB.SET_MARK('OPEN',
                             'C_FROM_LOC_SECOND_LEG_TSF',
                             'tsfhead',
                             NULL);
            open C_FROM_LOC_SECOND_LEG_TSF;
            SQL_LIB.SET_MARK('FETCH',
                             'C_FROM_LOC_SECOND_LEG_TSF',
                             'tsfhead',
                             NULL);
            fetch C_FROM_LOC_SECOND_LEG_TSF into L_second_leg_from_loc;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_FROM_LOC_SECOND_LEG_TSF',
                             'tsfhead',
                             NULL);
            close C_FROM_LOC_SECOND_LEG_TSF;
            ---
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHECK_FIRST_LEG_TRANSFORMED',
                             'tsfhead',
                             NULL);
            open C_CHECK_FIRST_LEG_TRANSFORMED;
            SQL_LIB.SET_MARK('FETCH',
                             'C_CHECK_FIRST_LEG_TRANSFORMED',
                             'tsfhead',
                             NULL);
            fetch C_CHECK_FIRST_LEG_TRANSFORMED into L_transformed_wo_ind;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_FIRST_LEG_TRANSFORMED',
                             'tsfhead',
                             NULL);
            close C_CHECK_FIRST_LEG_TRANSFORMED;
            --

            L_l10n_exists_rec.procedure_key  := 'CHECK_IF_BRAZIL_LOCALIZED';
            L_l10n_exists_rec.doc_type       := 'TSF';
            L_l10n_exists_rec.source_id      :=  L_second_leg_from_loc;
            if LP_repair_tsf_no_tbl(i).from_loc_type ='E' then
               L_l10n_exists_rec.source_entity  := 'PTNR';
               L_l10n_exists_rec.source_type := 'E';
            else
               L_l10n_exists_rec.source_entity  := 'LOC';
            end if;

            if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                       L_l10n_exists_rec) = FALSE then
               return FALSE;
            end if;

            if (nvl(L_l10n_exists_rec.exists_ind,'N') = 'N' and L_transformed_wo_ind = 'N') then

               L_pub_ship_ind :='Y';

               if SHIPMENT_SQL.SHIP_TSF(O_error_message,
                                        LP_repair_tsf_no_tbl(i).tsf_no,
                                        L_pub_ship_ind) = FALSE then
                  return FALSE;
               end if;
            end if;
            --
         END LOOP;
      end if;
   end if;

   LP_repair_tsf_no_tbl.DELETE;
   
   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONSUME_SHIPMENT_CORE;
----------------------------------------------------------------------------------------
FUNCTION CHECK_ITEMS(O_error_message   IN OUT   VARCHAR2,
                     O_sellable_TBL    OUT      "RIB_ASNOutItem_TBL",
                     O_detail_TBL      OUT      "RIB_ASNOutItem_TBL",
                     I_rib_detail_TBL  IN       "RIB_ASNOutItem_TBL")
RETURN BOOLEAN IS

   L_program      VARCHAR2(62) := 'RMSSUB_ASNOUT.CHECK_ITEMS';

   L_dtl_count    NUMBER := 0;
   L_sell_count   NUMBER := 0;

   L_item_rec     ITEM_MASTER%ROWTYPE;

BEGIN
   O_sellable_TBL := "RIB_ASNOutItem_TBL"();
   O_detail_TBL := "RIB_ASNOutItem_TBL"();
   ---
   FOR i in 1..I_rib_detail_TBL.COUNT LOOP
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                          L_item_rec,  --- output record
                                          I_rib_detail_TBL(i).item_id) = FALSE then
         return FALSE;
      end if;

      if L_item_rec.sellable_ind = 'Y'   and
         L_item_rec.orderable_ind = 'N'  and
         L_item_rec.item_xform_ind = 'Y' then
         --- add item to sellable table
         L_sell_count := L_sell_count + 1;

         O_sellable_TBL.EXTEND;
         O_sellable_TBL(L_sell_count) := I_rib_detail_TBL(i);
      else
         --- add item to a new detail table
         L_dtl_count := L_dtl_count + 1;
         O_detail_TBL.EXTEND;
         O_detail_TBL(L_dtl_count) := I_rib_detail_TBL(i);
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
      return FALSE;

END CHECK_ITEMS;
----------------------------------------------------------------------------------------
FUNCTION GET_ORDERABLE_ITEMS(O_error_message   IN OUT   VARCHAR2,
                             O_orderable_TBL   IN OUT   nocopy  item_table,
                             I_sellable_TBL    IN       "RIB_ASNOutItem_TBL",
                             I_distro          IN       distro_record,
                             I_to_loc          IN       ALLOC_DETAIL.TO_LOC%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'RMSSUB_ASNOUT.GET_ORDERABLE_ITEMS';

   L_orditem_TBL            bts_orditem_qty_tbl;
   L_inv_status             INV_STATUS_CODES.INV_STATUS%TYPE := NULL;

   L_sell_items             ITEM_TBL := ITEM_TBL();
   L_sell_qty               QTY_TBL  := QTY_TBL();
   L_sell_inv_status        INV_STATUS_TBL  := INV_STATUS_TBL();

   L_index                  BINARY_INTEGER := 0;

BEGIN

   if I_sellable_TBL is not NULL and
      I_sellable_TBL.COUNT > 0 then

      FOR i in I_sellable_TBL.FIRST..I_sellable_TBL.LAST LOOP
         L_sell_items.EXTEND;
         L_sell_items(L_sell_items.COUNT) := I_sellable_TBL(i).item_id;
         ---
         L_sell_qty.EXTEND;
         L_sell_qty(L_sell_qty.COUNT) := I_sellable_TBL(i).unit_qty;
         ---
         if LP_inv_status is not NULL then
            L_inv_status := LP_inv_status;

         /* convert the disposition to an invertory status */
         elsif I_sellable_TBL(i).from_disposition IS NOT NULL then
            if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                         L_inv_status,
                                         I_sellable_TBL(i).from_disposition) = FALSE then
               return FALSE;
            end if;
            if L_inv_status IS NULL then
               L_inv_status := -1;
            end if;
         else
            L_inv_status := -1;
         end if;

         L_sell_inv_status.EXTEND;
         L_sell_inv_status(L_sell_inv_status.COUNT) := L_inv_status;
      END LOOP;

      if I_distro.distro_type in ('V', 'D', 'T') then
         if ITEM_XFORM_SQL.TSF_ORDERABLE_ITEM_INFO(O_error_message,
                                                   L_orditem_TBL,
                                                   L_sell_items,
                                                   L_sell_qty,
                                                   L_sell_inv_status,
                                                   I_distro.distro_no) = FALSE then
            return FALSE;
         end if;
      elsif I_distro.distro_type = 'A' then
         if ITEM_XFORM_SQL.ALLOC_ORDERABLE_ITEM_INFO(O_error_message,
                                                     L_orditem_TBL,
                                                     L_sell_items,
                                                     L_sell_qty,
                                                     I_distro.distro_no,
                                                     I_to_loc) = FALSE then
            return FALSE;
         end if;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_TYPE',NULL, NULL, NULL);
         return FALSE;
      end if;

      FOR i in L_orditem_TBL.FIRST..L_orditem_TBL.LAST LOOP

         L_index := O_orderable_TBL.COUNT;
         L_index := L_index + 1;

         O_orderable_TBL(L_index).item              := L_orditem_TBL(i).orderable_item;
         O_orderable_TBL(L_index).inv_status        := L_orditem_TBL(i).inv_status;
         O_orderable_TBL(L_index).qty               := L_orditem_TBL(i).qty;

         ---- break to sell items will not have any catchweight properties
         O_orderable_TBL(L_index).weight            := NULL;
         O_orderable_TBL(L_index).weight_uom        := NULL;

      END LOOP;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
      return FALSE;

END GET_ORDERABLE_ITEMS;
----------------------------------------------------------------------------------------
FUNCTION PARSE_BOL (O_error_message   IN OUT  VARCHAR2,
                    O_bol_record         OUT  shipment%rowtype,
                    I_asnout_message  IN      "RIB_ASNOutDesc_REC",
                    IO_bol_exists     IN OUT  BOOLEAN)
return BOOLEAN IS


BEGIN
   -- Use the incoming message ASN_NBR as the bol_no on the shipment table
   -- because at the time of receiving, the ASN_NBR is used to match the bol_no.
   O_bol_record.bol_no         := I_asnout_message.asn_nbr;
   O_bol_record.from_loc       := to_number(I_asnout_message.from_location);
   O_bol_record.to_loc         := to_number(I_asnout_message.to_location);
   O_bol_record.est_arr_date   := TO_DATE(TO_CHAR(I_asnout_message.est_arr_date, 'YYYYMMDD'), 'YYYYMMDD');
   O_bol_record.ship_date      := TO_DATE(TO_CHAR(I_asnout_message.shipment_date, 'YYYYMMDD'), 'YYYYMMDD');
   O_bol_record.no_boxes       := I_asnout_message.container_qty;
   O_bol_record.courier        := I_asnout_message.carrier_code;
   O_bol_record.ext_ref_no_out := I_asnout_message.bol_nbr;
   O_bol_record.comments       := I_asnout_message.comments;

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   O_bol_record.to_loc_type,
                                   O_bol_record.to_loc) = FALSE then
      return FALSE;
   end if;

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   O_bol_record.from_loc_type,
                                   O_bol_record.from_loc) = FALSE then
      return FALSE;
   end if;

   /* Process bol header info here to ensure that it only gets called once,
      regardless of the number of distros for the bol */
   if PROCESS_BOL(O_error_message,
                  O_bol_record,
                  IO_bol_exists) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_ASNOUT.PARSE_BOL',
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_BOL;
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_DISTRO(O_error_message  IN OUT  VARCHAR2,
                      O_distro_record     OUT  distro_record,
                      I_distro         IN      "RIB_ASNOutDistro_REC")
return BOOLEAN IS

BEGIN

   O_distro_record.distro_no            := to_number(I_distro.distro_nbr);
   O_distro_record.distro_type          := I_distro.distro_doc_type;
   O_distro_record.customer_order_no    := I_distro.cust_order_nbr;
   O_distro_record.fulfill_order_no     := I_distro.fulfill_order_nbr;
   O_distro_record.comments             := substr(I_distro.comments,1,300);

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_ASNOUT.PARSE_DISTRO',
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_DISTRO;
-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_ITEM (O_error_message  IN OUT  VARCHAR2,
                     O_item_table     IN OUT  nocopy item_table,
                     I_carton         IN      shipsku.carton%TYPE,
                     I_item           IN      item_record)
return BOOLEAN IS

   i    NUMBER;
   L_same_class BOOLEAN := FALSE;
   L_weight_cnvt ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_uom_class    UOM_CLASS.UOM_CLASS%TYPE;

BEGIN

   -- weight and weight_uom needs to be either both defined or both null
   if I_item.weight is NULL and I_item.weight_uom is NOT NULL or
      I_item.weight is NOT NULL and I_item.weight_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('WGT_WGTUOM_REQUIRED',NULL,NULL,NULL);
      return FALSE;
   end if;

   if I_item.weight_uom is NOT NULL then
      -- I_item.weight_uom will already be converted to UPPER case
      if not UOM_SQL.GET_CLASS(O_error_message,
                               L_uom_class,
                               I_item.weight_uom) then
         return FALSE;
      end if;
      if L_uom_class != 'MASS' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_WGTUOM_CLASS', I_item.weight_uom, L_uom_class, NULL);
         return FALSE;
      end if;
   end if;

   -- if already in O_item_table, increment the qty
   FOR i IN 1..O_item_table.COUNT LOOP

      if NVL(O_item_table(i).carton,' ')            = NVL(I_carton,' ') and
         O_item_table(i).item                       = I_item.item and
         NVL(O_item_table(i).inv_status, 999)  = NVL(I_item.inv_status, 999) then

         O_item_table(i).qty := O_item_table(i).qty + I_item.qty;

         -- For the same item on the distro, if any does not have weight info,
         -- do not use the weight info. (Average weight will be used instead.)
         if O_item_table(i).weight is NULL or I_item.weight is NULL then
            O_item_table(i).weight := NULL;
            O_item_table(i).weight_uom := NULL;
         else
            -- both defined, possibly convert the weight
            if O_item_table(i).weight_uom = I_item.weight_uom then
               O_item_table(i).weight := O_item_table(i).weight + I_item.weight;
            else
               if not UOM_SQL.CHECK_CLASS(O_error_message,
                                          L_same_class,
                                          I_item.weight_uom,
                                          O_item_table(i).weight_uom) then
                  return FALSE;
               end if;
               ---
               if L_same_class = TRUE then
                  if not UOM_SQL.CONVERT(O_error_message,
                                         L_weight_cnvt,
                                         O_item_table(i).weight_uom,
                                         I_item.weight,
                                         I_item.weight_uom,
                                         I_item.item,
                                         NULL,
                                         NULL) then
                     return FALSE;
                  end if;
               else
                  O_error_message := SQL_LIB.CREATE_MSG('CANNOT_CNVT_UOM',
                                                        UPPER(I_item.weight_uom),
                                                        O_item_table(i).weight_uom,
                                                        NULL);
                  return FALSE;
               end if;
               O_item_table(i).weight := O_item_table(i).weight + L_weight_cnvt;
            end if;
         end if;

         return TRUE;
      end if;
   END LOOP;

   -- if not already in O_item_table, add it
   i := (O_item_table.COUNT);
   i := i + 1;

   O_item_table(i).carton            := I_carton;
   O_item_table(i).item              := I_item.item;
   O_item_table(i).inv_status        := I_item.inv_status;
   O_item_table(i).qty               := I_item.qty;
   O_item_table(i).weight            := I_item.weight;
   O_item_table(i).weight_uom        := I_item.weight_uom;
   O_item_table(i).extended_base_cost:= I_item.extended_base_cost;
   O_item_table(i).base_cost         := I_item.base_cost;

   return TRUE;

EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_ASNOUT.PARSE_ITEM',
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_ITEM;
--------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_BOL (O_error_message    IN OUT VARCHAR2,
                      I_bol              IN     shipment%rowtype,
                      IO_bol_exists      IN OUT BOOLEAN)
return BOOLEAN IS

BEGIN
   if BOL_SQL.PUT_BOL(O_error_message,
                      IO_bol_exists,
                      I_bol.bol_no,
                      I_bol.from_loc,
                      I_bol.to_loc,
                      I_bol.ship_date,
                      I_bol.est_arr_date,
                      I_bol.no_boxes,
                      I_bol.courier,
                      I_bol.ext_ref_no_out,
                      I_bol.comments) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'RMSSUB_ASNOUT.PROCESS_BOL',
                                          to_char(SQLCODE));
      return FALSE;

END PROCESS_BOL;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_DISTRO (O_error_message    IN OUT   VARCHAR2,
                         IO_distro          IN OUT   distro_record,
                         I_bol              IN       shipment%ROWTYPE,
                         I_itemtable        IN       item_table)
return BOOLEAN IS

   L_alloc_head_from_loc  ALLOC_HEADER.WH%TYPE;
   L_tsfhead_to_loc       TSFHEAD.TO_LOC%TYPE;
   L_tsfhead_from_loc     ITEM_LOC.LOC%TYPE;
   L_tsf_type             TSFHEAD.TSF_TYPE%TYPE;
   L_tran_item            ITEM_MASTER.ITEM%TYPE;
   L_vdate                PERIOD.VDATE%TYPE := GET_VDATE;
   L_inv_status_code      INV_STATUS_CODES.INV_STATUS_CODE%TYPE;
   L_detail_created       BOOLEAN := FALSE;
   L_sync_f_ordret        BOOLEAN := FALSE;
   L_count                NUMBER(12);
   L_unavailable_flag     VARCHAR2(1) := NULL;

BEGIN
   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                IO_distro.franchise_ordret_ind,
                                                I_bol.from_loc,
                                                I_bol.from_loc_type,
                                                I_bol.to_loc,
                                                I_bol.to_loc_type) = FALSE then
      return FALSE;
   end if;

------distro_type of 'C' is not used in RMS at this time, so this type is ignored ---------
   if IO_distro.distro_type in ('V', 'D', 'T') then

      if I_itemtable.COUNT > 0 then
          if I_itemtable(1).inv_status is not NULL and I_itemtable(1).inv_status != '-1' then
            L_unavailable_flag := 'Y' ;
         end if;
      end if;  
       
      if BOL_SQL.PUT_TSF(O_error_message,
                         L_tsfhead_to_loc,
                         L_tsfhead_from_loc,
                         L_tsf_type,
                         /* input variables */
                         IO_distro.distro_no,
                         I_bol.from_loc,
                         I_bol.from_loc_type,
                         I_bol.to_loc,
                         I_bol.to_loc_type,
                         L_vdate,
                         IO_distro.comments,
                         L_unavailable_flag) = FALSE then
         return FALSE;
      end if;

      FOR i IN 1..I_itemtable.COUNT LOOP

         if BOL_SQL.PUT_TSF_ITEM(O_error_message,
                                 L_detail_created,
                                 IO_distro.distro_no,
                                 I_itemtable(i).item,
                                 I_itemtable(i).carton,
                                 I_itemtable(i).qty,
                                 I_itemtable(i).weight,
                                 I_itemtable(i).weight_uom,
                                 I_itemtable(i).inv_status,
                                 I_bol.from_loc,
                                 I_bol.from_loc_type,
                                 I_bol.to_loc,
                                 I_bol.to_loc_type,
                                 L_tsfhead_to_loc,
                                 L_tsfhead_from_loc,
                                 L_tsf_type,
                                 'N',
                                 I_itemtable(i).extended_base_cost,
                                 I_itemtable(i).base_cost) = FALSE then
            return FALSE;
         end if;
         L_count := LP_ils_item_tbl.COUNT + 1;
         LP_ils_item_tbl(L_count).item       := I_itemtable(i).item;
         LP_ils_item_tbl(L_count).carton     := I_itemtable(i).carton;
         LP_ils_item_tbl(L_count).qty        := I_itemtable(i).qty;
         LP_ils_item_tbl(L_count).weight     := I_itemtable(i).weight;
         LP_ils_item_tbl(L_count).weight_uom := I_itemtable(i).weight_uom;
         LP_ils_item_tbl(L_count).inv_status := I_itemtable(i).inv_status;
         LP_ils_item_tbl(L_count).extended_base_cost := I_itemtable(i).extended_base_cost;
         LP_ils_item_tbl(L_count).base_cost          := I_itemtable(i).base_cost;

         -- if any tsfdetail was added in BOL_SQL.PUT_TSF_ITEM, need to sync the f order/return
         if L_sync_f_ordret = FALSE then
            L_sync_f_ordret := L_detail_created;
         end if;
      end LOOP;

      if IO_distro.franchise_ordret_ind in (WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER,WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN) and
         L_sync_f_ordret then
         if PROCESS_FRANCHISE_ORDRET(O_error_message,
                                     IO_distro) = FALSE then
            return FALSE;
         end if;
      end if;

      if BOL_SQL.PROCESS_TSF(O_error_message) = FALSE then
         return FALSE;
      end if;

      if IO_distro.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then
         if WF_BOL_SQL.UPDATE_SHIPPED_WF_ORDER(O_error_message,
                                               IO_distro.distro_no,
                                               'T',
                                               NULL,
                                               NULL) = FALSE then
            return FALSE;
         end if;
      elsif IO_distro.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
         if WF_BOL_SQL.UPDATE_SHIPPED_WF_RETURN(O_error_message,
                                                IO_distro.distro_no) = FALSE then
            return FALSE;
         end if;
      end if;

   elsif IO_distro.distro_type = 'A' then
      FOR i IN 1..I_itemtable.COUNT LOOP

         /* returns transaction level item */
         if BOL_SQL.PUT_ALLOC(O_error_message,
                              L_tran_item,
                              L_alloc_head_from_loc,
                              IO_distro.distro_no,
                              I_bol.from_loc,
                              I_itemtable(i).item) = FALSE then
           return FALSE;
         end if;

         if BOL_SQL.PUT_ALLOC_ITEM(O_error_message,
                                   L_detail_created,
                                   IO_distro.distro_no,
                                   L_tran_item,
                                   I_itemtable(i).carton,
                                   I_itemtable(i).qty,
                                   I_itemtable(i).weight,
                                   I_itemtable(i).weight_uom,
                                   I_itemtable(i).inv_status,
                                   I_bol.to_loc,
                                   I_bol.to_loc_type,
                                   L_alloc_head_from_loc,
                                   I_itemtable(i).extended_base_cost,
                                   I_itemtable(i).base_cost) = FALSE then
            return FALSE;
         end if;

         -- if any alloc_detail was added in BOL_SQL.PUT_ALLOC_ITEM, need to sync the f order
         if L_sync_f_ordret = FALSE then
            L_sync_f_ordret := L_detail_created;
         end if;

         if BOL_SQL.PROCESS_ALLOC(O_error_message) = FALSE then
            return FALSE;
         end if;
      end LOOP;

      if IO_distro.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then
         if L_sync_f_ordret then
            if PROCESS_FRANCHISE_ORDRET(O_error_message,
                                        IO_distro) = FALSE then
               return FALSE;
            end if;
         end if;

         if WF_BOL_SQL.UPDATE_SHIPPED_WF_ORDER(O_error_message,
                                               IO_distro.distro_no,
                                               'A',
                                               I_bol.to_loc,
                                               I_bol.to_loc_type) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_TYPE',NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'RMSSUB_ASNOUT.PROCESS_DISTRO',
                                          to_char(SQLCODE));
      return FALSE;

END PROCESS_DISTRO;
-----------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  VARCHAR2,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2)
IS
   L_program  VARCHAR2(100)  := 'RMSSUB_APPOINTCRE.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             To_Char(SQLCODE));
      ---
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
END HANDLE_ERRORS;
-----------------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                      I_to_loc          IN       SHIPMENT.TO_LOC%TYPE,
                      I_to_loc_type     IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                      I_from_loc        IN       SHIPMENT.TO_LOC%TYPE,
                      I_from_loc_type   IN       SHIPMENT.TO_LOC_TYPE%TYPE)
return BOOLEAN IS

   L_program             VARCHAR2(64) := 'RMSSUB_ASNOUT.VALIDATE_TSF';

   L_tsf_type            TSFHEAD.TSF_TYPE%TYPE := NULL;
   L_tsfhead_row         TSFHEAD%ROWTYPE;
   L_second_leg_tsf_no   TSFHEAD.TSF_NO%TYPE := NULL;
   L_from_loc_type       TSFHEAD.FROM_LOC_TYPE%TYPE := NULL;

   cursor C_TSF_TYPE is
      select tsf_type
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_TSFHEAD is
      select *
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_SECOND_LEG_TSF is
      select tsf_no,
             from_loc_type
        from tsfhead
       where tsf_parent_no = I_tsf_no;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_TSF_TYPE',
                    'tsfhead',
                    NULL);
   open C_TSF_TYPE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_TSF_TYPE',
                    'tsfhead',
                    NULL);
   fetch C_TSF_TYPE into L_tsf_type;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_TSF_TYPE',
                    'tsfhead',
                    NULL);
   close C_TSF_TYPE;
   ---
   if L_tsf_type is NULL then
      if I_from_loc_type = 'W' and I_to_loc_type = 'W' then
         O_error_message := SQL_LIB.CREATE_MSG('EG_TSF_FR_TO_LOC_TYPE_WH',
                                               I_from_loc,
                                               I_to_loc,
                                               L_program);
         return FALSE;
      end if;
   end if;
   ---
   --retrieve the tsfhead row
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TSFHEAD',
                    'tsfhead',
                    NULL);
   open C_GET_TSFHEAD;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TSFHEAD',
                    'tsfhead',
                    NULL);
   fetch C_GET_TSFHEAD into L_tsfhead_row;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TSFHEAD',
                    'tsfhead',
                    NULL);
   close C_GET_TSFHEAD;
   --check if the transfer is a two legged tsf with a external finisher
   if L_tsfhead_row.to_loc_type = 'E' then
      --retrieve the 2nd leg transfer number
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_SECOND_LEG_TSF',
                       'tsfhead',
                       NULL);
      open C_GET_SECOND_LEG_TSF;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_SECOND_LEG_TSF',
                       'tsfhead',
                       NULL);
      fetch C_GET_SECOND_LEG_TSF into L_second_leg_tsf_no,
                                      L_from_loc_type;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SECOND_LEG_TSF',
                       'tsfhead',
                       NULL);
      close C_GET_SECOND_LEG_TSF;

      --copy the 2nd leg transfer number to the array
      LP_tbl_size := LP_tbl_size + 1;
      LP_repair_tsf_no_tbl(LP_tbl_size).tsf_no := L_second_leg_tsf_no;
      LP_repair_tsf_no_tbl(LP_tbl_size).from_loc_type := L_from_loc_type;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_TSF;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_FRANCHISE_ORDRET(O_error_message  IN OUT   VARCHAR2,
                                  I_distro         IN       DISTRO_RECORD)
return BOOLEAN IS

   L_input_status         TSFHEAD.STATUS%TYPE;
   L_new_tsf_status       TSFHEAD.STATUS%TYPE;
   L_tsf_type             TSFHEAD.TSF_TYPE%TYPE;
   L_wf_order_no          TSFHEAD.WF_ORDER_NO%TYPE;
   L_rma_no               TSFHEAD.RMA_NO%TYPE;
   L_alloc_status         ALLOC_HEADER.STATUS%TYPE;
   L_wf_order_tbl         OBJ_ALLOC_LINKED_F_ORD_TBL;
   L_new_alloc_status     ALLOC_HEADER.STATUS%TYPE;
   L_create_f_order       BOOLEAN := FALSE;
   L_create_f_return      BOOLEAN := FALSE;

   L_table               VARCHAR2(30);
   L_key1                VARCHAR2(100);
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_TSF_INFO is
      select status,
             tsf_type,
             wf_order_no,
             rma_no
        from tsfhead
       where tsf_no = I_distro.distro_no;

   cursor C_GET_ALLOC_INFO is
      select status
        from alloc_header
       where alloc_no = I_distro.distro_no;

   cursor C_LOCK_TSFHEAD is
      select 'x'
        from tsfhead
       where tsf_no = I_distro.distro_no
         for update nowait;

   cursor C_LOCK_ALLOC_DETAIL is
      select 'x'
        from alloc_detail ad,
             TABLE(CAST(L_wf_order_tbl AS OBJ_ALLOC_LINKED_F_ORD_TBL)) wfod
       where ad.alloc_no    = wfod.alloc_no
         and ad.to_loc      = wfod.to_loc
         and ad.to_loc_type = wfod.to_loc_type
         for update of ad.wf_order_no nowait;

BEGIN
   if I_distro.distro_type = 'T' then

      open C_GET_TSF_INFO;
      fetch C_GET_TSF_INFO into L_input_status,
                                L_tsf_type,
                                L_wf_order_no,
                                L_rma_no;
      close C_GET_TSF_INFO;

      if I_distro.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then

         if L_wf_order_no is NULL then
            L_create_f_order := TRUE;
         end if;

         -- create the franchise order
         if WF_TRANSFER_SQL.SYNC_F_ORDER(O_error_message,
                                         L_wf_order_no,
                                         L_new_tsf_status,
                                         RMS_CONSTANTS.WF_ACTION_SHIPPED,
                                         I_distro.distro_no,
                                         L_input_status,
                                         L_tsf_type) = FALSE then
            return FALSE;
         end if;

         if L_create_f_order then
            L_table := 'TSFHEAD';
            L_key1 := 'Tsf_no: '||TO_CHAR(I_distro.distro_no);

            open C_LOCK_TSFHEAD;
            close C_LOCK_TSFHEAD;

            update tsfhead
               set wf_order_no = L_wf_order_no
             where tsf_no = I_distro.distro_no;
         end if;

      elsif I_distro.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then

         if L_rma_no is NULL then
            L_create_f_return := TRUE;
         end if;

         -- create the franchise return
         if WF_TRANSFER_SQL.SYNC_F_RETURN(O_error_message,
                                          L_rma_no,
                                          RMS_CONSTANTS.WF_ACTION_SHIPPED,
                                          I_distro.distro_no,
                                          L_input_status,
                                          L_tsf_type) = FALSE then
            return FALSE;
         end if;

         if L_create_f_return then
            L_table := 'TSFHEAD';
            L_key1 := 'Tsf_no: '||TO_CHAR(I_distro.distro_no);

            open C_LOCK_TSFHEAD;
            close C_LOCK_TSFHEAD;

            update tsfhead
               set rma_no = L_rma_no
             where tsf_no = I_distro.distro_no;
         end if;
      end if;
   else -- allocation
      select status
        into L_alloc_status
        from alloc_header
       where alloc_no = I_distro.distro_no;

      -- create/update the franchise order
      if WF_ALLOC_SQL.SYNC_F_ORDER(O_error_message,
                                   L_wf_order_tbl,
                                   L_new_alloc_status,
                                   RMS_CONSTANTS.WF_ACTION_SHIPPED,
                                   I_distro.distro_no,
                                   L_alloc_status) = FALSE then
         return FALSE;
      end if;

      if L_wf_order_tbl is NOT NULL and L_wf_order_tbl.COUNT > 0 then
         L_table := 'ALLOC_DETAIL';
         L_key1 := 'Alloc_no: '||TO_CHAR(I_distro.distro_no);

         open C_LOCK_ALLOC_DETAIL;
         close C_LOCK_ALLOC_DETAIL;

         merge into alloc_detail ad
            using TABLE(CAST(L_wf_order_tbl AS OBJ_ALLOC_LINKED_F_ORD_TBL)) wfod
               on (ad.alloc_no    = wfod.alloc_no and
                   ad.to_loc      = wfod.to_loc and
                   ad.to_loc_type = wfod.to_loc_type)
             when matched then
                update set ad.wf_order_no = wfod.wf_order_no;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1);
      return FALSE;
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'RMSSUB_ASNOUT.PROCESS_FRANCHISE_ORDRET',
                                          to_char(SQLCODE));
      return FALSE;

END PROCESS_FRANCHISE_ORDRET;
-------------------------------------------------------------------------------------------------------
END RMSSUB_ASNOUT;
/
