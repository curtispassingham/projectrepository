CREATE OR REPLACE PACKAGE SHIPMENT_ATTRIB_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
--- Name: GET_INFO
--- Purpose: Returns shipment information.
-------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_order_no       IN OUT  SHIPMENT.ORDER_NO%TYPE,
                  O_bol_no         IN OUT  SHIPMENT.BOL_NO%TYPE,
                  O_asn            IN OUT  SHIPMENT.ASN%TYPE,
                  O_ship_date      IN OUT  SHIPMENT.SHIP_DATE%TYPE,
                  O_receive_date   IN OUT  SHIPMENT.RECEIVE_DATE%TYPE,
                  O_ship_origin    IN OUT  SHIPMENT.SHIP_ORIGIN%TYPE,
                  O_status_code    IN OUT  SHIPMENT.STATUS_CODE%TYPE,
                  O_to_loc         IN OUT  SHIPMENT.TO_LOC%TYPE,
                  O_to_loc_type    IN OUT  SHIPMENT.TO_LOC_TYPE%TYPE,
                  O_from_loc       IN OUT  SHIPMENT.FROM_LOC%TYPE,
                  O_from_loc_type  IN OUT  SHIPMENT.FROM_LOC_TYPE%TYPE,
                  I_shipment       IN      SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--- Name: NEXT_SHIPMENT
--- Purpose: Gets the next shipment number
-------------------------------------------------------------------------------------------
FUNCTION NEXT_SHIPMENT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_shipment_number  IN OUT  SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--- Name: CHECK_SHIPSKU
--- Purpose: Checks to see if shipment passed in exists in shipsku table.
-------------------------------------------------------------------------------------------
FUNCTION CHECK_SHIPSKU(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT  BOOLEAN,
                       I_shipment       IN      SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name   :   TSF_OR_ALLOC
-- Purpose         :   Checks the tsfhead and alloc_header tables to determine if the input
--                     distro_no is a transfer or allocation.  Returns O_found = TRUE when
--                     the distro_no is found on either table, and returns
--                     O_tsf_or_alloc = 'T' for transfer or 'A' for allocation.  Returns
--                     O_found = FALSE when distro_no is not found on either table.
-------------------------------------------------------------------------------------------
FUNCTION TSF_OR_ALLOC(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_tsf_or_alloc   IN OUT  VARCHAR2,
                      O_found          IN OUT  BOOLEAN,
                      I_distro_no      IN      SHIPSKU.DISTRO_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: RECD_SHIPMENT_EXISTS
-- Purpose      : Validates that the shipment has been received before an adjustment can be made.

-- note:
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_SHIPMENT
-- which only returns data that the user has permission to access.
-------------------------------------------------------------------------------------------
FUNCTION RECD_SHIPMENT_EXISTS(O_error_message         IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exist                 IN OUT     BOOLEAN,
                              I_order_no              IN         SHIPMENT.ORDER_NO%TYPE,
                              I_shipment              IN         SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: SHIPSKU_FILTER_LIST
-- Purpose      : Checks to see if the SHIPSKU records the user can see is filtered.

-- note:
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_SHIPSKU
-- which only returns data that the user has permission to access.
-------------------------------------------------------------------------------------------
FUNCTION SHIPSKU_FILTER_LIST(O_error_message         IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                             O_diff                  IN OUT     VARCHAR2,
                             I_shipment              IN         SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: BOL_SHIPSKU_FILTER_LIST
-- Purpose      : Checks to see if the BOL_SHIPSKU record is filtered by view V_BOL_SHIPSKU.
--                Assign O_diff 'Y' or 'N' based on check.
-------------------------------------------------------------------------------------------
FUNCTION BOL_SHIPSKU_FILTER_LIST(O_error_message         IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_diff                  IN OUT     VARCHAR2,
                                 I_bol_no                IN         BOL_SHIPMENT.BOL_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: CARTON_SHIPMENT_EXISTS
-- Purpose      : verifies if the carton and shipment information are valid and exist
--                in the database.
-------------------------------------------------------------------------------------------
FUNCTION CARTON_SHIPMENT_EXISTS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT  BOOLEAN,
                                I_shipment         IN      SHIPMENT.SHIPMENT%TYPE,
                                I_to_loc           IN      SHIPMENT.TO_LOC%TYPE,
                                I_to_loc_type      IN      SHIPMENT.TO_LOC_TYPE%TYPE,
                                I_asn              IN      SHIPMENT.ASN%TYPE,
                                I_bol_no           IN      SHIPMENT.BOL_NO%TYPE,
                                I_carton           IN      SHIPSKU.CARTON%TYPE,
                                I_distro_type      IN      SHIPSKU.DISTRO_TYPE%TYPE,
                                I_distro_no        IN      SHIPSKU.DISTRO_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: SHIP_LOC_EXISTS
-- Purpose      : This function is called by the receiving forms and validates that the
--                location passed in to the function is in a valid shipment before calling
--                the appropriate warehouse or store validation functions.
-------------------------------------------------------------------------------------------
FUNCTION SHIP_LOC_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_loc_name      IN OUT STORE.STORE_NAME%TYPE,
                         I_loc           IN     SHIPMENT.TO_LOC%TYPE,
                         I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                         I_ctn_ind       IN     VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: SHIP_ASN_EXISTS
-- Purpose      : This function is called by the receiving forms and validates that the
--                asn passed in to the function is associated to a valid shipment.
-------------------------------------------------------------------------------------------
FUNCTION SHIP_ASN_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists        IN OUT BOOLEAN,
                         I_asn           IN     SHIPMENT.ASN%TYPE,
                         I_ctn_ind       IN     VARCHAR2,
                         I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: SHIP_BOL_EXISTS
-- Purpose      : This function is called by the receiving forms and validates that the
--                bol passed in to the function is associated to a valid shipment.
-------------------------------------------------------------------------------------------
FUNCTION SHIP_BOL_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists        IN OUT BOOLEAN,
                         I_bol_no        IN     SHIPMENT.BOL_NO%TYPE,
                         I_ctn_ind       IN     VARCHAR2,
                         I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: CHECK_COMPLETE_SHIPMENT_RECEIVED
-- Purpose      : Checks to see if the complete shipment was received
-------------------------------------------------------------------------------------------
FUNCTION CHECK_COMPLETE_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_partial         IN OUT   BOOLEAN,
                                 I_shipment        IN       SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: CHECK_SHIP_COSTING_LOC
-- Purpose      : This function is called by the receiving unit adjustment form to validate
--                if the shipment is associated to the costing location of the franchise PO's
--                location.
-------------------------------------------------------------------------------------------
END SHIPMENT_ATTRIB_SQL;
/