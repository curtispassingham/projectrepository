CREATE OR REPLACE PACKAGE BODY RMSSUB_XALLOC_SQL AS

   L_sysdate             ITEM_LOC.CREATE_DATETIME%TYPE:=sysdate;
   L_user                ITEM_LOC.LAST_UPDATE_ID%TYPE :=get_user;

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CREATE_HEADER
   -- Purpose      : This function will create a HEADER record.
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: MODIFY_HEADER
   -- Purpose      : This function will call ALLOCATION_SQL.MODIFY_HEADER to update a HEADER record.
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: DELETE_HEADER
   -- Purpose      : This function will call ALLOCATION_SQL.DELETE_HEADER to add a HEADER record to
   --                the daily_purge_table.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CREATE_DETAIL
   -- Purpose      : This function will call ALLOCATION_SQL.DELETE_HEADER to add a HEADER record to
   --                the daily_purge_table.
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_alloc_status    IN OUT   ALLOC_HEADER.STATUS%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC,
                       I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: MODIFY_DETAIL
   -- Purpose      : This function will call ALLOCATION_SQL.DELETE_HEADER to add a HEADER record to
   --                the daily_purge_table.
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: DELETE_DETAIL
   -- Purpose      : This function will call ALLOCATION_SQL.DELETE_HEADER to add a HEADER record to
   --                the daily_purge_table.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_RESV_EXP(O_error_message  IN OUT  VARCHAR2,
                           I_alloc_rec      IN      ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION UPD_NEW_LOCS_RESV_EXP(O_error_message  IN OUT  VARCHAR2,
                               I_alloc_rec      IN      ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEMLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC,
                 I_message_type    IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XALLOC_SQL.PERSIST';
   L_alloc_status    ALLOC_HEADER.STATUS%TYPE;

BEGIN

   if LOWER(I_message_type) = RMSSUB_XALLOC.LP_cre_type then
      if NOT CREATE_HEADER(O_error_message,
                           I_alloc_rec) then
         return FALSE;
      end if;

   elsif LOWER(I_message_type) = RMSSUB_XALLOC.LP_mod_type then
      if NOT MODIFY_HEADER(O_error_message,
                           I_alloc_rec) then
         return FALSE;
      end if;

   elsif LOWER(I_message_type) = RMSSUB_XALLOC.LP_del_type then
      if NOT DELETE_HEADER(O_error_message,
                           I_alloc_rec) then
         return FALSE;
      end if;

   elsif LOWER(I_message_type) = RMSSUB_XALLOC.LP_dtl_cre_type then
      if NOT CREATE_DETAIL(O_error_message,
                           L_alloc_status,
                           I_alloc_rec,
                           I_message_type) then
         return FALSE;
      end if;

   elsif LOWER(I_message_type) = RMSSUB_XALLOC.LP_dtl_mod_type then
      if NOT MODIFY_DETAIL(O_error_message,
                           I_alloc_rec) then
         return FALSE;
      end if;

   elsif LOWER(I_message_type) = RMSSUB_XALLOC.LP_dtl_del_type then
      if NOT DELETE_DETAIL(O_error_message,
                           I_alloc_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-----------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-----------------------------------------------------------------------------------------------------
FUNCTION CREATE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XALLOC_SQL.CREATE_HEADER';
   L_alloc_status   ALLOC_HEADER.STATUS%TYPE;

BEGIN

   --For an allocation to be created in 'A'pproved status, first insert alloc_header in 'W'orksheet status
   --to prevent the allocation from being published to SIM/WMS in case the allocation fails credit check
   --and needs to be created in 'W'orksheet status. The credit check is done in CREATE_DETAIL through
   --call to WF_ALLOC_SQL.SYNC_F_ORDER.

   insert into ALLOC_HEADER(alloc_no,
                            order_no,
                            wh,
                            item,
                            status,
                            alloc_desc,
                            po_type,
                            alloc_method,
                            release_date,
                            order_type,
                            origin_ind)
                     values(I_alloc_rec.header.alloc_no,
                            I_alloc_rec.header.order_no,
                            I_alloc_rec.header.wh,
                            I_alloc_rec.header.item,
                            DECODE(I_alloc_rec.header.status,'A','W',I_alloc_rec.header.status),
                            I_alloc_rec.header.alloc_desc,
                            I_alloc_rec.header.po_type,
                            I_alloc_rec.header.alloc_method,
                            I_alloc_rec.header.release_date,
                            I_alloc_rec.header.order_type,
                            I_alloc_rec.header.origin_ind);

   if CREATE_DETAIL(O_error_message,
                    L_alloc_status,
                    I_alloc_rec,
                    RMSSUB_XALLOC.LP_cre_type) = FALSE then
      return FALSE;
   end if;

   update alloc_header
      set status = L_alloc_status
    where alloc_no = I_alloc_rec.header.alloc_no;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_HEADER;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XALLOC_SQL.MODIFY_HEADER';

   cursor C_LOCK_ALLOC_HEAD is
      select 'x'
        from alloc_header
       where alloc_no = I_alloc_rec.header.alloc_no
         for update nowait;
BEGIN

   open C_LOCK_ALLOC_HEAD;
   close C_LOCK_ALLOC_HEAD;

   update alloc_header
      set alloc_desc = I_alloc_rec.header.alloc_desc,
          release_date = I_alloc_rec.header.release_date
    where alloc_no = I_alloc_rec.header.alloc_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_HEADER;
--------------------------------------------------------------------------------------------------------
FUNCTION DELETE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XALLOC_SQL.DELETE_HEADER';

   cursor C_LOCK_ALLOC_HEAD is
      select 'x'
        from alloc_header
       where alloc_no = I_alloc_rec.header.alloc_no
         for update nowait;
BEGIN
   if SYSTEM_OPTIONS_SQL.GP_SYSTEM_OPTIONS_ROW.DEFAULT_ALLOC_CHRG_IND = 'Y' then
      if NOT ALLOC_CHARGE_SQL.DELETE_CHRGS(O_error_message,
                                           I_alloc_rec.header.alloc_no,
                                           NULL) then
         return FALSE;
      end if;
   end if;

   open C_LOCK_ALLOC_HEAD;
   close C_LOCK_ALLOC_HEAD;

   update alloc_header
      set status   = 'C',
          close_date = GET_VDATE
    where alloc_no = I_alloc_rec.header.alloc_no;

   if WF_ALLOC_SQL.CANCEL_F_ORDER(O_error_message,
                                  I_alloc_rec.header.alloc_no) = FALSE then
      return FALSE;
   end if;

   if I_alloc_rec.header.order_no is NULL and I_alloc_rec.header.status = 'A' then
      if NOT UPD_ITEM_RESV_EXP(O_error_message,
                               I_alloc_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_HEADER;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_alloc_status    IN OUT   ALLOC_HEADER.STATUS%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC,
                       I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'RMSSUB_XALLOC_SQL.CREATE_DETAIL';
   L_status              ORDHEAD.STATUS%TYPE;
   L_print_online_ind    TICKET_REQUEST.PRINT_ONLINE_IND%TYPE  := 'N';
   L_wf_order_no_tbl     OBJ_ALLOC_LINKED_F_ORD_TBL;
   L_table               VARCHAR2(30);
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   L_action_type         VARCHAR2(10);

   cursor C_CHECK_STATUS is
      select status
        from ordhead
       where order_no = I_alloc_rec.header.order_no;

   cursor C_LOCK_ALLOC_DETAIL is
      select 'x'
        from alloc_detail ad,
             TABLE(CAST(L_wf_order_no_tbl AS OBJ_ALLOC_LINKED_F_ORD_TBL)) wfo
       where ad.alloc_no    = wfo.alloc_no
         and ad.to_loc      = wfo.to_loc
         and ad.to_loc_type = wfo.to_loc_type
         for update of ad.wf_order_no nowait;

BEGIN

   FORALL i in I_alloc_rec.alloc_detail.to_locs.FIRST .. I_alloc_rec.alloc_detail.to_locs.LAST
      insert into alloc_detail(alloc_no,
                               to_loc,
                               to_loc_type,
                               qty_allocated,
                               qty_prescaled,
                               non_scale_ind,
                               in_store_date)
                        values (I_alloc_rec.header.alloc_no,
                               I_alloc_rec.alloc_detail.to_locs(i),
                               I_alloc_rec.alloc_detail.to_loc_types(i),
                               I_alloc_rec.alloc_detail.qtys(i),
                               I_alloc_rec.alloc_detail.qtys(i),
                               'N',
                               I_alloc_rec.alloc_detail.in_store_dates(i));

   if I_message_type = RMSSUB_XALLOC.LP_cre_type then
      L_action_type := RMS_CONSTANTS.WF_ACTION_CREATE;
   else
      L_action_type := RMS_CONSTANTS.WF_ACTION_UPDATE;
   end if;

   --WF_ORDER creation requires PRICING_COST to be populated in the future_cost tables.
   --Therefore it is required to range the item-locations on allocation before calling 
   --WF_ALLOC_SQL.SYNC_F_ORDER  
   if NOT CREATE_ITEMLOC(O_error_message,
                         I_alloc_rec) then
      return FALSE;
   end if;

   if WF_ALLOC_SQL.SYNC_F_ORDER(O_error_message,
                                L_wf_order_no_tbl,
                                O_alloc_status,
                                L_action_type,
                                I_alloc_rec.header.alloc_no,
                                I_alloc_rec.header.status) = FALSE then
      return FALSE;
   end if;

   if L_wf_order_no_tbl is NOT NULL and L_wf_order_no_tbl.COUNT > 0 then
      L_table := 'ALLOC_DETAIL';
      open C_LOCK_ALLOC_DETAIL;
      close C_LOCK_ALLOC_DETAIL;

      merge into alloc_detail ad
         using TABLE(CAST(L_wf_order_no_tbl AS OBJ_ALLOC_LINKED_F_ORD_TBL)) wfo
            on (ad.alloc_no    = wfo.alloc_no and
                ad.to_loc      = wfo.to_loc and
                ad.to_loc_type = wfo.to_loc_type)
          when matched then
             update set ad.wf_order_no = wfo.wf_order_no;
   end if;

   if SYSTEM_OPTIONS_SQL.GP_SYSTEM_OPTIONS_ROW.DEFAULT_ALLOC_CHRG_IND = 'Y' then
      FOR i in I_alloc_rec.alloc_detail.to_locs.FIRST .. I_alloc_rec.alloc_detail.to_locs.LAST LOOP
         if NOT ALLOC_CHARGE_SQL.DEFAULT_CHRGS(O_error_message,
                                               I_alloc_rec.header.alloc_no,
                                               I_alloc_rec.header.wh,
                                               I_alloc_rec.alloc_detail.to_locs(i),
                                               I_alloc_rec.alloc_detail.to_loc_types(i),
                                               I_alloc_rec.header.item) then
            return FALSE;
         end if;
      END LOOP;
   end if;

   if I_alloc_rec.header.order_no is NULL and O_alloc_status = 'A' then
      if NOT UPD_ITEM_RESV_EXP(O_error_message,
                               I_alloc_rec) then
         return FALSE;
      end if;

      if NOT UPD_NEW_LOCS_RESV_EXP(O_error_message,
                                   I_alloc_rec) then
         return FALSE;
      end if;
   end if;

   if I_alloc_rec.header.order_no is NOT NULL then
      open C_CHECK_STATUS;
      fetch C_CHECK_STATUS into L_status;
      close C_CHECK_STATUS;

      if L_status = 'A' then
         if NOT TICKET_SQL.APPROVE(O_error_message,
                                   I_alloc_rec.header.order_no,
                                   L_print_online_ind) then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_alloc_rec.header.alloc_no),
                                            NULL);
         return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_DETAIL;
--------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50) := 'RMSSUB_XALLOC_SQL.MODIFY_DETAIL';
   L_alloc_status      ALLOC_HEADER.STATUS%TYPE;
   L_wf_order_no_tbl   OBJ_ALLOC_LINKED_F_ORD_TBL;

   cursor C_LOCK_ALLOC_DETAIL(L_to_loc      ALLOC_DETAIL.TO_LOC%TYPE,
                              L_to_loc_type ALLOC_DETAIL.TO_LOC_TYPE%TYPE) is
      select 'x'
        from alloc_detail
       where alloc_no = I_alloc_rec.header.alloc_no
         and to_loc   = L_to_loc
         and to_loc_type = L_to_loc_type
         for update nowait;

BEGIN
   FOR i in I_alloc_rec.alloc_detail.to_locs.FIRST .. I_alloc_rec.alloc_detail.to_locs.LAST LOOP
      open C_LOCK_ALLOC_DETAIL(I_alloc_rec.alloc_detail.to_locs(i),
                               I_alloc_rec.alloc_detail.to_loc_types(i));
      close C_LOCK_ALLOC_DETAIL;
   END LOOP;

   FORALL i in I_alloc_rec.alloc_detail.to_locs.FIRST .. I_alloc_rec.alloc_detail.to_locs.LAST
      update alloc_detail set qty_allocated = qty_allocated + I_alloc_rec.alloc_detail.qtys(i),
                              qty_prescaled = qty_prescaled + I_alloc_rec.alloc_detail.qtys(i),
                              in_store_date = I_alloc_rec.alloc_detail.in_store_dates(i)
       where alloc_no = I_alloc_rec.header.alloc_no
         and to_loc   = I_alloc_rec.alloc_detail.to_locs(i)
         and to_loc_type = I_alloc_rec.alloc_detail.to_loc_types(i);

   if WF_ALLOC_SQL.SYNC_F_ORDER(O_error_message,
                                L_wf_order_no_tbl,
                                L_alloc_status,
                                RMS_CONSTANTS.WF_ACTION_UPDATE,
                                I_alloc_rec.header.alloc_no,
                                I_alloc_rec.header.status) = FALSE then
      return FALSE;
   end if;

   if I_alloc_rec.header.order_no is NULL and L_alloc_status = 'A' then
      if NOT UPD_ITEM_RESV_EXP(O_error_message,
                               I_alloc_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_DETAIL;
--------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XALLOC_SQL.DELETE_DETAIL';
   L_alloc_no         ALLOC_HEADER.ALLOC_NO%TYPE := I_alloc_rec.header.alloc_no;
   L_var              VARCHAR2(1);
   L_wf_order_no_tbl  OBJ_WF_ORDER_NO_TBL;

   cursor C_LOCK_ALLOC_DETAIL(L_to_loc   ALLOC_DETAIL.TO_LOC%TYPE) is
      select 'x'
        from alloc_detail
       where alloc_no = L_alloc_no
         and to_loc   = L_to_loc
         and rownum = 1
         for update nowait;

   cursor C_LOCK_ALLOC_CHRG(L_to_loc   ALLOC_DETAIL.TO_LOC%TYPE) is
      select 'x'
        from alloc_chrg
       where alloc_no = L_alloc_no
         and to_loc   = L_to_loc
         and rownum = 1
         for update nowait;

   cursor C_LOCK_ALLOC_HEAD is
      select 'x'
        from alloc_header
       where alloc_no = L_alloc_no
         and rownum = 1
         for update nowait;

   cursor C_ALLOC_DETAILS_EXIST is
      select 'x'
        from alloc_detail
       where alloc_no = L_alloc_no
         and rownum = 1;

   cursor C_GET_WF_ORDER_NOS is
      select wf_order_no
        from alloc_detail
       where to_loc in (select *
                          from TABLE(CAST(I_alloc_rec.alloc_detail.to_locs as LOC_TBL)))
         and alloc_no = L_alloc_no
         and wf_order_no is NOT NULL;
BEGIN

   if SYSTEM_OPTIONS_SQL.GP_SYSTEM_OPTIONS_ROW.DEFAULT_ALLOC_CHRG_IND = 'Y' then
      FOR i in I_alloc_rec.alloc_detail.to_locs.FIRST .. I_alloc_rec.alloc_detail.to_locs.LAST LOOP
         open C_LOCK_ALLOC_CHRG(I_alloc_rec.alloc_detail.to_locs(i));
         close C_LOCK_ALLOC_CHRG;
      END LOOP;

      FORALL i in I_alloc_rec.alloc_detail.to_locs.FIRST .. I_alloc_rec.alloc_detail.to_locs.LAST
         delete from alloc_chrg
          where alloc_no = L_alloc_no
            and to_loc   = I_alloc_rec.alloc_detail.to_locs(i);
   end if;

   FOR i in I_alloc_rec.alloc_detail.to_locs.FIRST .. I_alloc_rec.alloc_detail.to_locs.LAST LOOP
      open C_LOCK_ALLOC_DETAIL(I_alloc_rec.alloc_detail.to_locs(i));
      close C_LOCK_ALLOC_DETAIL;
   END LOOP;

   --get the wf_order_nos associated with alloc_details to be deleted.
   open C_GET_WF_ORDER_NOS;
   fetch C_GET_WF_ORDER_NOS BULK COLLECT into L_wf_order_no_tbl;
   close C_GET_WF_ORDER_NOS;

   FORALL i in I_alloc_rec.alloc_detail.to_locs.FIRST .. I_alloc_rec.alloc_detail.to_locs.LAST
      delete from alloc_detail
       where alloc_no = L_alloc_no
         and to_loc   = I_alloc_rec.alloc_detail.to_locs(i);

   --WF_ALLOC_SQL.SYNC_DELETED_ALLOC_LINES also internally calls
   --WF_ALLOC_SQL.SYNC_F_ORDER. A separate call is not required here.
   if L_wf_order_no_tbl is NOT NULL and L_wf_order_no_tbl.COUNT > 0 then
      if WF_ALLOC_SQL.SYNC_DELETED_ALLOC_LINES(O_error_message,
                                               I_alloc_rec.header.alloc_no,
                                               L_wf_order_no_tbl) = FALSE then
         return FALSE;
      end if;
   end if;
   -- check if any details on allocation
   -- if none, remove header record.
   open C_ALLOC_DETAILS_EXIST;
   fetch C_ALLOC_DETAILS_EXIST into L_var;

   if C_ALLOC_DETAILS_EXIST%NOTFOUND then
      open C_LOCK_ALLOC_HEAD;
      close C_LOCK_ALLOC_HEAD;

      update alloc_header
         set status   = 'C',
             close_date = GET_VDATE
       where alloc_no = I_alloc_rec.header.alloc_no;
   end if;

   close C_ALLOC_DETAILS_EXIST;

   --reset reserved and expected values.
   if I_alloc_rec.header.order_no is NULL and I_alloc_rec.header.status = 'A' then
      if NOT UPD_ITEM_RESV_EXP(O_error_message,
                               I_alloc_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_RESV_EXP(O_error_message  IN OUT  VARCHAR2,
                           I_alloc_rec      IN      ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(64)  := 'RMSSUB_XALLOC_SQL.UPD_ITEM_RESV_EXP';
   L_table               VARCHAR(30);
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   L_item                ITEM_MASTER.ITEM%TYPE := I_alloc_rec.header.item;
   L_loc                 ITEM_LOC.LOC%TYPE := I_alloc_rec.header.wh;
   L_rowid_tbl           ROWID_CHAR_TBL;
   L_qty_tbl             QTY_TBL;

   cursor C_LOCK_TO_ITEM_LOC_SOH(L_rowid   ROWID) is
     select 'X'
       from item_loc_soh
      where rowid = L_rowid
        for update nowait;

   cursor C_LOCK_FROM_ITEM_LOC_SOH is
     select 'X'
       from item_loc_soh
      where loc      = L_loc
        and loc_type = 'W'
        and item     =  L_item
        for update nowait;

   cursor C_LOCK_FROM_PACK_ILS is
     select ils.rowid,
            I_alloc_rec.total_qty_allocated * vpq.qty
       from item_loc_soh ils,
            v_packsku_qty vpq
      where ils.loc      = L_loc
        and ils.loc_type = 'W'
        and ils.item     = vpq.item
        and vpq.pack_no  = L_item
        for update of ils.pack_comp_resv nowait;
BEGIN

-- ALSO CONSIDER JOINING WITH ALLOCATION TABLES INSTEAD OF BULK PROCESSING.
   L_table   := 'ITEM_LOC_SOH';
   open C_LOCK_FROM_ITEM_LOC_SOH;
   close C_LOCK_FROM_ITEM_LOC_SOH;

   --reserve from location warehouse for total allocated qty to all to locs.
   update item_loc_soh
      set tsf_reserved_qty     = tsf_reserved_qty + I_alloc_rec.total_qty_allocated,
          last_update_datetime = L_sysdate,
          last_update_id       = L_user
    where loc      = L_loc
      and loc_type = 'W'
      and item     = L_item;
 ---

   if I_alloc_rec.itemloc.locs_to_update is NOT NULL and I_alloc_rec.itemloc.locs_to_update.COUNT >0 then
      FOR i in I_alloc_rec.itemloc.locs_to_update.FIRST .. I_alloc_rec.itemloc.locs_to_update.LAST LOOP
         L_loc := I_alloc_rec.itemloc.locs_to_update(i);
         L_item := I_alloc_rec.itemloc.items_to_update(i);
         open C_LOCK_TO_ITEM_LOC_SOH(I_alloc_rec.itemloc.rows_to_update(i));
         close C_LOCK_TO_ITEM_LOC_SOH;
      END LOOP;

      -- both pack items and and pack component items (with pack qty built in) already
      -- are in existing rows to update.
      if I_alloc_rec.to_loc_type = 'W' then
         FORALL i in I_alloc_rec.itemloc.locs_to_update.FIRST .. I_alloc_rec.itemloc.locs_to_update.LAST
            --Decode logic in the below update statement is to account for component items for a pack.
            --Components items at warehouses go to pack_comp_exp bucket instead of tsf_expected_qty.
            update item_loc_soh
               set tsf_expected_qty      = tsf_expected_qty + DECODE(I_alloc_rec.header.item,I_alloc_rec.itemloc.items_to_update(i),I_alloc_rec.itemloc.qtys_to_update(i),0),
                   pack_comp_exp         = pack_comp_exp + DECODE(I_alloc_rec.header.item,I_alloc_rec.itemloc.items_to_update(i),0,I_alloc_rec.itemloc.qtys_to_update(i)),
                   last_update_datetime  = L_sysdate,
                   last_update_id        = L_user
             where rowid                 = I_alloc_rec.itemloc.rows_to_update(i);

      elsif I_alloc_rec.to_loc_type = 'S' then
         --When allocating to stores, I_alloc_rec.itemloc collection will contain regular item or pack component items.
         --Both update tsf_expected_qty bucket.
         FORALL i in I_alloc_rec.itemloc.locs_to_update.FIRST .. I_alloc_rec.itemloc.locs_to_update.LAST
            update item_loc_soh
               set tsf_expected_qty     = tsf_expected_qty + I_alloc_rec.itemloc.qtys_to_update(i),
                   last_update_datetime = L_sysdate,
                   last_update_id       = L_user
             where rowid      = I_alloc_rec.itemloc.rows_to_update(i);
      end if;
   end if;

   if I_alloc_rec.pack_ind = 'Y' then
      L_item := I_alloc_rec.header.item;
      L_loc := I_alloc_rec.header.wh;

      open C_LOCK_FROM_PACK_ILS;
      fetch C_LOCK_FROM_PACK_ILS BULK COLLECT into L_rowid_tbl,L_qty_tbl;
      close C_LOCK_FROM_PACK_ILS;

      if L_rowid_tbl is NOT NULL and L_rowid_tbl.COUNT > 0 then
         FORALL i in L_rowid_tbl.FIRST .. L_rowid_tbl.LAST
            update item_loc_soh
               set pack_comp_resv       = pack_comp_resv + L_qty_tbl(i),
                   last_update_datetime = L_sysdate,
                   last_update_id       = L_user
             where rowid = L_rowid_tbl(i);
      end if;
   end if; --end if I_alloc_rec.pack_ind = 'Y'

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                             to_char(L_loc),
                                            L_item);
         return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_ITEM_RESV_EXP;
---------------------------------------------------------------------------------
FUNCTION CREATE_ITEMLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_alloc_rec       IN       ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'RMSSUB_XALLOC_SQL.CREATE_ITEMLOC';
   L_input_rec   NEW_ITEM_LOC_SQL.NIL_INPUT_RECORD;
   L_input       NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;

BEGIN

   if I_alloc_rec.new_itemloc.items is NULL or I_alloc_rec.new_itemloc.items.COUNT = 0 then
      return TRUE;
   end if;

   FOR i in I_alloc_rec.new_itemloc.items.FIRST..I_alloc_rec.new_itemloc.items.LAST LOOP
      L_input_rec.item                    := I_alloc_rec.new_itemloc.items(i);
      L_input_rec.item_parent             := NULL;
      L_input_rec.item_grandparent        := NULL;
      L_input_rec.item_desc               := NULL;
      L_input_rec.item_short_desc         := NULL;
      L_input_rec.dept                    := NULL;
      L_input_rec.item_class              := NULL;
      L_input_rec.subclass                := NULL;
      L_input_rec.item_level              := NULL;
      L_input_rec.tran_level              := NULL;
      L_input_rec.item_status             := NULL;
      L_input_rec.waste_type              := NULL;
      L_input_rec.sellable_ind            := NULL;
      L_input_rec.orderable_ind           := NULL;
      L_input_rec.pack_ind                := NULL;
      L_input_rec.pack_type               := NULL;
      L_input_rec.diff_1                  := NULL;
      L_input_rec.diff_2                  := NULL;
      L_input_rec.diff_3                  := NULL;
      L_input_rec.diff_4                  := NULL;
      L_input_rec.loc                     := I_alloc_rec.new_itemloc.locations(i);
      L_input_rec.loc_type                := I_alloc_rec.new_itemloc.loc_types(i);
      L_input_rec.daily_waste_pct         := NULL;
      L_input_rec.unit_cost_loc           := NULL;
      L_input_rec.unit_retail_loc         := NULL;
      L_input_rec.selling_retail_loc      := NULL;
      L_input_rec.selling_uom             := NULL;
      L_input_rec.multi_units             := NULL;
      L_input_rec.multi_unit_retail       := NULL;
      L_input_rec.multi_selling_uom       := NULL;
      L_input_rec.item_loc_status         := NULL;
      L_input_rec.taxable_ind             := NULL;
      L_input_rec.ti                      := NULL;
      L_input_rec.hi                      := NULL;
      L_input_rec.store_ord_mult          := NULL;
      L_input_rec.meas_of_each            := NULL;
      L_input_rec.meas_of_price           := NULL;
      L_input_rec.uom_of_price            := NULL;
      L_input_rec.primary_variant         := NULL;
      L_input_rec.primary_supp            := NULL;
      L_input_rec.primary_cntry           := NULL;
      L_input_rec.local_item_desc         := NULL;
      L_input_rec.local_short_desc        := NULL;
      L_input_rec.primary_cost_pack       := NULL;
      L_input_rec.receive_as_type         := NULL;
      L_input_rec.store_price_ind         := NULL;
      L_input_rec.uin_type                := NULL;
      L_input_rec.uin_label               := NULL;
      L_input_rec.capture_time            := NULL;
      L_input_rec.ext_uin_ind             := NULL;
      L_input_rec.source_method           := NULL;
      L_input_rec.source_wh               := NULL;
      L_input_rec.inbound_handling_days   := NULL;
      L_input_rec.currency_code           := NULL;
      L_input_rec.like_store              := NULL;
      L_input_rec.default_to_children_ind := NULL;
      L_input_rec.class_vat_ind           := NULL;
      L_input_rec.hier_level              := NULL;
      L_input_rec.hier_num_value          := NULL;
      L_input_rec.hier_char_value         := NULL;
      L_input_rec.ranged_ind              := 'Y';
      L_input_rec.costing_loc             := NULL;
      L_input_rec.costing_loc_type        := NULL;
      --
      L_input(i) := L_input_rec;
   END LOOP;

   if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message,
                                    L_input) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_ITEMLOC;
--------------------------------------------------------------------------------------------------------
FUNCTION UPD_NEW_LOCS_RESV_EXP(O_error_message  IN OUT  VARCHAR2,
                               I_alloc_rec      IN      ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(64)                   := 'RMSSUB_XALLOC_SQL.UPD_NEW_LOCS_RESV_EXP';

BEGIN

   if I_alloc_rec.new_itemloc.locations is NOT NULL and I_alloc_rec.new_itemloc.locations.COUNT > 0 then
      -- both pack items and and pack component items (with pack qty built in) already
      -- are in existing rows to update.

      if I_alloc_rec.to_loc_type = 'W' then
         FORALL i in I_alloc_rec.new_itemloc.locations.FIRST .. I_alloc_rec.new_itemloc.locations.LAST
            --Decode logic in the below update statement is to account for component items for a pack.
            --Components items at warehouses go to pack_comp_exp bucket instead of
            --tsf_expected_qty.
            update item_loc_soh
               set tsf_expected_qty     =  tsf_expected_qty + DECODE(I_alloc_rec.header.item,I_alloc_rec.new_itemloc.items(i),I_alloc_rec.new_itemloc.units(i),0),
                   pack_comp_exp         = pack_comp_exp + DECODE(I_alloc_rec.header.item,I_alloc_rec.new_itemloc.items(i),0,I_alloc_rec.new_itemloc.units(i)),
                   last_update_datetime =  L_sysdate,
                   last_update_id       =  L_user
             where item = I_alloc_rec.new_itemloc.items(i)
               and loc = I_alloc_rec.new_itemloc.locations(i);

      elsif I_alloc_rec.to_loc_type = 'S' then
         --When allocating to stores, I_alloc_rec.itemloc collection will contain regular item or pack component items.
         --Both update tsf_expected_qty bucket.
         --I_alloc_rec.new_itemloc.units will contain zero for pack items at stores.
         FORALL i in I_alloc_rec.new_itemloc.locations.FIRST .. I_alloc_rec.new_itemloc.locations.LAST
            update item_loc_soh
               set tsf_expected_qty     = tsf_expected_qty + I_alloc_rec.new_itemloc.units(i),
                   last_update_datetime = L_sysdate,
                   last_update_id       = L_user
             where item = I_alloc_rec.new_itemloc.items(i)
               and loc = I_alloc_rec.new_itemloc.locations(i);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_NEW_LOCS_RESV_EXP;
---------------------------------------------------------------------------------
END RMSSUB_XALLOC_SQL;
/