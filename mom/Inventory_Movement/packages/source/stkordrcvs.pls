CREATE OR REPLACE PACKAGE STOCK_ORDER_RCV_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--
-- Usage:
--  Due to performance concerns, bulk DML statements are used in
--   this package.  To facilitate that some additional work is necessary
--   by callers of this package.
--
--  Before calls to TSF_LINE_ITEM or ALLOC_LINE_ITEM a call must be made to
--   INIT_TSF_ALLOC_GROUP.  This ensures that there is no leftover data in
--   the structures used to the bulk DML.
--
--  After call to TSF_LINE_ITEM or ALLOC_LINE_ITEM a call must be made to
--   FINISH_TSF_ALLOC_GROUP.  The uses the data built up in the TSF_LINE_ITEM
--   and ALLOC_LINE_ITEM to perform the bulk DML.
--
--  The idea is to put as many calls to TSF_LINE_ITEM and/or ALLOC_LINE_ITEM
--   between calls to INIT_TSF_ALLOC_GROUP and FINISH_TSF_ALLOC_GROUP.  This will
--   minimize the number of times certain insert/update statement are performed
--   and speed up processing.
--
--  Even if one call is made to TSF_LINE_ITEM or ALLOC_LINE_ITEM,
--   INIT_TSF_ALLOC_GROUP and FINISH_TSF_ALLOC_GROUP must still be called.
--
--  See rmssub_receiveb.pls or bolsqlb.pls for examples of calls.
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

TYPE item_rcv_record IS RECORD(
   item                   ITEM_MASTER.ITEM%TYPE,              --passed parameter if tran lvl, else tran level lookup
   ref_item               ITEM_MASTER.ITEM%TYPE,              --passed item if below tran lvl, else null
   dept                   ITEM_MASTER.DEPT%TYPE,              --item_master lookup
   class                  ITEM_MASTER.CLASS%TYPE,             --item_master lookup
   subclass               ITEM_MASTER.SUBCLASS%TYPE,          --item_master lookup
   pack_ind               ITEM_MASTER.PACK_IND%TYPE,          --item_master lookup
   pack_type              ITEM_MASTER.PACK_TYPE%TYPE,         --item_master lookup
   simple_pack_ind        ITEM_MASTER.PACK_IND%TYPE,          --item_master lookup  -- Catch Weight
   catch_weight_ind       ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,  --item_master lookup  -- Catch Weight

   carton                 SHIPSKU.CARTON%TYPE,                --passed parameter
   distro_type            VARCHAR2(1),                        --'A'lloc or 'T'ransfer

   tran_date              PERIOD.VDATE%TYPE,                  --passed parameter
   transaction_type       VARCHAR2(1),                        --passed parameter

   alloc_no               ALLOC_HEADER.ALLOC_NO%TYPE,         --passed parameter
   alloc_status           ALLOC_HEADER.STATUS%TYPE,           --alloc_header lookup

   tsf_no                 TSFHEAD.TSF_NO%TYPE,                --passed parameter
   tsf_type               TSFHEAD.TSF_TYPE%TYPE,              --tsfhead lookup
   tsf_status             TSFHEAD.STATUS%TYPE,                --tsfhead lookup
   tsf_seq_no             TSFDETAIL.TSF_SEQ_NO%TYPE,          --tsfdetail lookup

   distro_from_loc        ITEM_LOC.LOC%TYPE,                  --tsfhead/alloc_header lookup
   from_loc_phy           ITEM_LOC.LOC%TYPE,                  --lookup
   from_loc_type          ITEM_LOC.LOC_TYPE%TYPE,             --lookup
   from_tsf_entity        TSF_ENTITY.TSF_ENTITY_ID%TYPE,      --lookup
   from_finisher          VARCHAR2(1) := 'N',                 --lookup for tsf only
   distro_to_loc          ITEM_LOC.LOC%TYPE,                  --tsfhead/alloc_detail lookup
   to_loc_phy             ITEM_LOC.LOC%TYPE,                  --passed parameter
   to_loc_type            ITEM_LOC.LOC_TYPE%TYPE,             --lookup
   to_tsf_entity          TSF_ENTITY.TSF_ENTITY_ID%TYPE,      --lookup
   to_finisher            VARCHAR2(1) := 'N',                 --lookup for tsf only
   tsf_parent_no          TSFHEAD.TSF_PARENT_NO%TYPE,         --tsfhead lookup
   mrt_no                 TSFHEAD.MRT_NO%TYPE,                --tsfhead lookup

   appt                   APPT_HEAD.APPT%TYPE,                --passed parameter
   receipt_no             APPT_DETAIL.RECEIPT_NO%TYPE,        --passed parameter

   inv_status             INV_STATUS_TYPES.INV_STATUS%TYPE,   --decoded from passed in disp

   bol_no                 SHIPMENT.BOL_NO%TYPE,               --passed parameter

   --- This variable is named 'ship_no' rather than 'shipment'
   --- because 'shipment' causes a compilation error.
   ship_no                SHIPMENT.SHIPMENT%TYPE,             --shipment lookup using bol_no
   ss_seq_no              SHIPSKU.SEQ_NO%TYPE,                --shipsku lookup using shipment/item

   -- for break to sell processing
   sellable_ind           ITEM_MASTER.sellable_ind%TYPE,
   item_xform_ind         ITEM_MASTER.item_xform_ind%TYPE,

   -- franchise transaction indicator ('O'-order; 'R'-return; 'N'-neither)
   franchise_ordret_ind   VARCHAR2(1)
);


TYPE comp_item IS RECORD(
   comp_item             ITEM_MASTER.ITEM%TYPE,            --vpackskuqty/im/ilsoh lookup
   comp_qty              ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,  --vpackskuqty/im/ilsoh lookup
   comp_from_loc_av_cost ITEM_LOC_SOH.UNIT_COST%TYPE,      --vpackskuqty/im/ilsoh lookup
   comp_pack_ind         ITEM_MASTER.PACK_IND%TYPE,        --vpackskuqty/im/ilsoh lookup
   comp_dept             ITEM_MASTER.DEPT%TYPE,            --vpackskuqty/im/ilsoh lookup
   comp_class            ITEM_MASTER.CLASS%TYPE,           --vpackskuqty/im/ilsoh lookup
   comp_subclass         ITEM_MASTER.SUBCLASS%TYPE,        --vpackskuqty/im/ilsoh lookup
   comp_inventory_ind    ITEM_MASTER.INVENTORY_IND%TYPE    --vpackskuqty/im/ilsoh lookup
);


TYPE comp_item_array IS TABLE of comp_item
 INDEX BY BINARY_INTEGER;


TYPE cost_retail_qty_record IS RECORD(

   receive_as_type      ITEM_LOC.RECEIVE_AS_TYPE%TYPE := 'E', --item_loc lookup for packs,
                                                              --non-pack items use default value of E
   -- Qtys
   input_qty            ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,   --passed parameter
   weight               ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,  --from message       -- Catch Weight
   weight_uom           UOM_CLASS.UOM%TYPE,                --from message       -- Catch Weight

   -- Costs
   from_loc_av_cost      ITEM_LOC_SOH.UNIT_COST%TYPE,      --shipsku lookup, pkg call if no shipsku
   pack_from_av_cost     ITEM_LOC_SOH.UNIT_COST%TYPE,      --pack's av_cost at from loc
                                                           --excludes any charges that my be on shipsku
   pack_av_cost_ratio    NUMBER(12,4)                      --ratio to av_cost when shipsku was written
);


TYPE inv_flow IS RECORD(
   vir_from_loc         ITEM_LOC.LOC%TYPE,                --shipitem_inv_flow or default
   vir_from_loc_type    ITEM_LOC.LOC_TYPE%TYPE,           --shipitem_inv_flow or default
   vir_to_loc           ITEM_LOC.LOC%TYPE,                --shipitem_inv_flow or default
   vir_to_loc_type      ITEM_LOC.LOC_TYPE%TYPE,           --shipitem_inv_flow or default
   exp_qty              ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,  --shipitem_inv_flow or default
   prev_rcpt_qty        ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,  --shipitem_inv_flow or default
   dist_qty             ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,  --shipitem_inv_flow or default
   upd_intran_qty       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,  --calculated
   overage_qty          ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,  --calculated
   dist_weight_cuom     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE, --calculated  -- Catch Weight
   overage_weight_cuom  ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE, --calculated  -- Catch Weight
   cuom                 ITEM_SUPP_COUNTRY.COST_UOM%TYPE   --calculated  -- Catch Weight
);


TYPE inv_flow_array IS TABLE of inv_flow
 INDEX BY BINARY_INTEGER;


TYPE cache_header_record IS RECORD(

   -- Tsf key
   tsf_check_input_loc          ITEM_LOC.LOC%TYPE,
   tsf_check_tsf_no             TSFHEAD.TSF_NO%TYPE,

   -- Tsf lookups
   tsf_check_tsf_type           TSFHEAD.TSF_TYPE%TYPE,
   tsf_check_tsf_status         TSFHEAD.STATUS%TYPE,
   tsf_check_from_loc_type      ITEM_LOC.LOC_TYPE%TYPE,
   tsf_check_from_loc_distro    ITEM_LOC.LOC%TYPE,
   tsf_check_from_loc_phy       ITEM_LOC.LOC%TYPE,
   tsf_check_from_tsf_entity    TSF_ENTITY.TSF_ENTITY_ID%TYPE,
   tsf_check_from_finisher      VARCHAR2(1),
   tsf_check_to_loc_type        ITEM_LOC.LOC_TYPE%TYPE,
   tsf_check_to_loc_distro      ITEM_LOC.LOC%TYPE,
   tsf_check_to_loc_phy         ITEM_LOC.LOC%TYPE,
   tsf_check_to_tsf_entity      TSF_ENTITY.TSF_ENTITY_ID%TYPE,
   tsf_check_to_finisher        VARCHAR2(1),
   tsf_check_tsf_parent_no      TSFHEAD.tsf_parent_no%TYPE,
   tsf_mrt_no                   TSFHEAD.mrt_no%TYPE,

   franchise_ordret_ind         VARCHAR2(1),

   -- Ship key
   ship_check_bol_no            SHIPMENT.BOL_NO%TYPE,
   ship_check_from_loc_phy      ITEM_LOC.loc%TYPE,
   ship_check_to_loc_phy        ITEM_LOC.loc%TYPE,

   -- Ship lookups
   ship_check_shipment          SHIPMENT.SHIPMENT%TYPE
);

LP_cache_header_info cache_header_record;


-- Types to be used for bulk collecting line item details for carton level receiving
TYPE shipment_tab     is TABLE OF SHIPMENT.SHIPMENT%TYPE INDEX BY BINARY_INTEGER;
TYPE item_tab         is TABLE OF SHIPSKU.ITEM%TYPE INDEX BY BINARY_INTEGER;
TYPE qty_tab          is TABLE OF SHIPSKU.QTY_EXPECTED%TYPE INDEX BY BINARY_INTEGER;
TYPE inv_status_tab   is TABLE OF SHIPSKU.INV_STATUS%TYPE INDEX BY BINARY_INTEGER;
TYPE carton_tab       is TABLE OF SHIPSKU.CARTON%TYPE INDEX BY BINARY_INTEGER;
TYPE distro_no_tab    is TABLE OF SHIPSKU.DISTRO_NO%TYPE INDEX BY BINARY_INTEGER;
TYPE adjust_type_tab  is TABLE OF SHIPSKU.ADJUST_TYPE%TYPE INDEX BY BINARY_INTEGER;
TYPE tampered_ind_tab is TABLE OF SHIPSKU.TAMPERED_IND%TYPE INDEX BY BINARY_INTEGER;


--------------------------------------------------------------------------------
-- Public functions
--------------------------------------------------------------------------------
FUNCTION INIT_TSF_ALLOC_GROUP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION FINISH_TSF_ALLOC_GROUP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION TSF_LINE_ITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_loc                IN       ITEM_LOC.LOC%TYPE,
                       I_item               IN       ITEM_MASTER.ITEM%TYPE,
                       I_qty                IN       TRAN_DATA.UNITS%TYPE,
                       I_weight             IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,       -- Catch Weight
                       I_weight_uom         IN       UOM_CLASS.UOM%TYPE,                     -- Catch Weight
                       I_transaction_type   IN       VARCHAR2, --(ADJ FLAG)
                       I_tran_date          IN       PERIOD.VDATE%TYPE,
                       I_receipt_number     IN       APPT_DETAIL.RECEIPT_NO%TYPE,
                       I_bol_no             IN       SHIPMENT.BOL_NO%TYPE,
                       I_appt               IN       APPT_HEAD.APPT%TYPE,
                       I_carton             IN       SHIPSKU.CARTON%TYPE,
                       I_distro_type        IN       VARCHAR2,
                       I_distro_number      IN       TSFHEAD.TSF_NO%TYPE,
                       I_disp               IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                       I_tampered_ind       IN       SHIPSKU.TAMPERED_IND%TYPE,
                       I_dummy_carton_ind   IN       VARCHAR2)

RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION ALLOC_LINE_ITEM(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loc                 IN       ITEM_LOC.LOC%TYPE,
                         I_item                IN       ITEM_MASTER.ITEM%TYPE,
                         I_qty                 IN       TRAN_DATA.UNITS%TYPE,
                         I_weight              IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,       -- Catch Weight
                         I_weight_uom          IN       UOM_CLASS.UOM%TYPE,                     -- Catch Weight
                         I_transaction_type    IN       VARCHAR2, --(ADJ FLAG)
                         I_tran_date           IN       PERIOD.VDATE%TYPE,
                         I_receipt_number      IN       APPT_DETAIL.RECEIPT_NO%TYPE,
                         I_bol_no              IN       SHIPMENT.BOL_NO%TYPE,
                         I_appt                IN       APPT_HEAD.APPT%TYPE,
                         I_carton              IN       SHIPSKU.CARTON%TYPE,
                         I_distro_type         IN       VARCHAR2,
                         I_distro_number       IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                         I_disp                IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                         I_tampered_ind        IN       SHIPSKU.TAMPERED_IND%TYPE,
                         I_dummy_carton_ind    IN       VARCHAR2,
                         I_function_call_ind   IN       VARCHAR2 DEFAULT 'R')
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION TSF_BOL_CARTON(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_appt                 IN       APPT_HEAD.APPT%TYPE,
                        I_shipment             IN       SHIPMENT.SHIPMENT%TYPE,
                        I_to_loc               IN       SHIPMENT.TO_LOC%TYPE,
                        I_bol_no               IN       SHIPMENT.BOL_NO%TYPE,
                        I_receipt_no           IN       APPT_DETAIL.RECEIPT_NO%TYPE,
                        I_disposition          IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                        I_tran_date            IN       PERIOD.VDATE%TYPE,
                        I_item_table           IN       ITEM_TAB,
                        I_qty_expected_table   IN       QTY_TAB,
                        I_weight               IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,       -- Catch Weight
                        I_weight_uom           IN       UOM_CLASS.UOM%TYPE,                     -- Catch Weight
                        I_inv_status_table     IN       INV_STATUS_TAB,
                        I_carton_table         IN       CARTON_TAB,
                        I_distro_no_table      IN       DISTRO_NO_TAB,
                        I_tampered_ind_table   IN       TAMPERED_IND_TAB,
                        I_wrong_store_ind      IN       VARCHAR2,
                        I_wrong_store          IN       SHIPMENT.TO_LOC%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION ALLOC_BOL_CARTON(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_appt                 IN       APPT_HEAD.APPT%TYPE,
                          I_shipment             IN       SHIPMENT.SHIPMENT%TYPE,
                          I_to_loc               IN       SHIPMENT.TO_LOC%TYPE,
                          I_bol_no               IN       SHIPMENT.BOL_NO%TYPE,
                          I_receipt_no           IN       APPT_DETAIL.RECEIPT_NO%TYPE,
                          I_disposition          IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                          I_tran_date            IN       PERIOD.VDATE%TYPE,
                          I_item_table           IN       ITEM_TAB,
                          I_qty_expected_table   IN       QTY_TAB,
                          I_weight               IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,       -- Catch Weight
                          I_weight_uom           IN       UOM_CLASS.UOM%TYPE,                     -- Catch Weight
                          I_inv_status_table     IN       INV_STATUS_TAB,
                          I_carton_table         IN       CARTON_TAB,
                          I_distro_no_table      IN       DISTRO_NO_TAB,
                          I_tampered_ind_table   IN       TAMPERED_IND_TAB,
                          I_wrong_store_ind      IN       VARCHAR2,
                          I_wrong_store          IN       SHIPMENT.TO_LOC%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- This function will check if the Carton, DISTRO_NO and BOL_NO match.
-- If they do not match then find correct BOL_NO for the DISTRO_NO.
-- If the correct BOL_NO can not be found then return FALSE.
--------------------------------------------------------------------------------
FUNCTION BOL_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   IO_bol_no         IN OUT   SHIPMENT.BOL_NO%TYPE,
                   I_carton          IN       SHIPSKU.CARTON%TYPE,
                   I_distro_no       IN       SHIPSKU.DISTRO_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION GET_INVENTORY_TREATMENT(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_inventory_treatment_ind   IN OUT   SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND%TYPE,
                                 I_from_loc_type             IN       ITEM_LOC.LOC_TYPE%TYPE,
                                 I_to_loc_type               IN       ITEM_LOC.LOC_TYPE%TYPE,
                                 I_distro_no                 IN       SHIPSKU.DISTRO_NO%TYPE,
                                 I_distro_type               IN       SHIPSKU.DISTRO_TYPE%TYPE,
                                 I_shipment                  IN       SHIPSKU.SHIPMENT%TYPE,
                                 I_ship_seq_no               IN       SHIPSKU.SEQ_NO%TYPE,
                                 I_so_reconcile_ind          IN       BOOLEAN,
                                 I_overage_ind               IN       BOOLEAN)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then
-- Private function prototypes
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION ITEM_CHECK(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_item               IN OUT   ITEM_MASTER.ITEM%TYPE,
                    O_ref_item           IN OUT   ITEM_MASTER.ITEM%TYPE,
                    O_dept               IN OUT   ITEM_MASTER.DEPT%TYPE,
                    O_class              IN OUT   ITEM_MASTER.CLASS%TYPE,
                    O_subclass           IN OUT   ITEM_MASTER.SUBCLASS%TYPE,
                    O_pack_ind           IN OUT   ITEM_MASTER.PACK_IND%TYPE,
                    O_pack_type          IN OUT   ITEM_MASTER.PACK_TYPE%TYPE,
                    O_simple_pack_ind    IN OUT   ITEM_MASTER.SIMPLE_PACK_IND%TYPE,  --Catch Weight
                    O_catch_weight_ind   IN OUT   ITEM_MASTER.CATCH_WEIGHT_IND%TYPE, --Catch Weight
                    O_sellable_ind       IN OUT   ITEM_MASTER.SELLABLE_IND%TYPE,
                    O_item_xform_ind     IN OUT   ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                    I_item               IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION SHIP_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_ship_found      IN OUT   BOOLEAN,
                    O_shipment        IN OUT   SHIPMENT.SHIPMENT%TYPE,
                    I_bol_no          IN       SHIPMENT.BOL_NO%TYPE,
                    I_phy_to_loc      IN       ITEM_LOC.LOC%TYPE,
                    I_phy_from_loc    IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- The shipment, distro_no, inv_status, and carton fields are passed into
-- this function because the values depend on whether it is called from the
-- tsf/alloc bol_carton functions or the tsf/alloc line_item functions
--------------------------------------------------------------------------------
FUNCTION CHECK_SS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_inv_flow_array     IN OUT   STOCK_ORDER_RCV_SQL.INV_FLOW_ARRAY,
                  O_ss_unit_cost       IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                  O_item_rec           IN OUT   STOCK_ORDER_RCV_SQL.ITEM_RCV_RECORD,
                  I_shipment           IN       SHIPMENT.SHIPMENT%TYPE,
                  I_distro_no          IN       SHIPSKU.DISTRO_NO%TYPE,
                  I_tsf_type           IN       TSFHEAD.TSF_TYPE%TYPE,
                  I_inv_status         IN       SHIPSKU.INV_STATUS%TYPE,
                  I_carton             IN       SHIPSKU.CARTON%TYPE,
                  I_qty                IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                  I_weight             IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,  -- Catch Weight
                  I_weight_uom         IN       UOM_CLASS.UOM%TYPE,                -- Catch Weight
                  I_tampered_ind       IN       SHIPSKU.TAMPERED_IND%TYPE,
                  I_is_wrong_store     IN       BOOLEAN,
                  I_from_inv_status    IN       TSFDETAIL.INV_STATUS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Catch Weight
FUNCTION DETERMINE_RECEIPT_WEIGHT(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_total_overage_qty        IN OUT   SHIPSKU.QTY_EXPECTED%TYPE,
                                  O_total_overage_wgt_cuom   IN OUT   SHIPSKU.WEIGHT_EXPECTED%TYPE,
                                  O_total_ss_rcpt_wgt_cuom   IN OUT   SHIPSKU.WEIGHT_RECEIVED%TYPE,
                                  O_rcpt_wgt_cuom            IN OUT   SHIPSKU.WEIGHT_RECEIVED%TYPE,
                                  O_cuom                     IN OUT   ITEM_SUPP_COUNTRY.COST_UOM%TYPE,
                                  I_ss_exp_qty               IN       SHIPSKU.QTY_EXPECTED%TYPE,
                                  I_ss_exp_wgt               IN       SHIPSKU.WEIGHT_EXPECTED%TYPE,
                                  I_ss_exp_wgt_uom           IN       SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE,
                                  I_ss_prev_rcpt_qty         IN       SHIPSKU.QTY_EXPECTED%TYPE,
                                  I_ss_prev_rcpt_wgt         IN       SHIPSKU.WEIGHT_RECEIVED%TYPE,
                                  I_ss_prev_rcpt_wgt_uom     IN       SHIPSKU.WEIGHT_RECEIVED_UOM%TYPE,
                                  I_rcpt_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                  I_rcpt_wgt                 IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                                  I_rcpt_wgt_uom             IN       UOM_CLASS.UOM%TYPE,
                                  I_item                     IN       ITEM_MASTER.ITEM%TYPE,
                                  I_shipment                 IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                                  I_distro_no                IN       SHIPSKU.DISTRO_NO%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION DIST_QTY_TO_FLOW(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_inv_flow_array   IN OUT   STOCK_ORDER_RCV_SQL.INV_FLOW_ARRAY,
                          I_item             IN       ITEM_MASTER.ITEM%TYPE,
                          I_shipment         IN       SHIPMENT.SHIPMENT%TYPE,
                          I_ss_seq_no        IN       SHIPSKU.SEQ_NO%TYPE,
                          I_tsf_qty          IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION APPT_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_appt            IN       APPT_DETAIL.APPT%TYPE,
                    I_distro          IN       APPT_DETAIL.DOC%TYPE,
                    I_distro_type     IN       APPT_DETAIL.DOC_TYPE%TYPE,
                    I_to_loc_phy      IN       ITEM_LOC.LOC%TYPE,
                    I_item            IN       ITEM_MASTER.ITEM%TYPE,
                    I_asn             IN       APPT_DETAIL.ASN%TYPE,
                    I_receipt_no      IN       APPT_DETAIL.RECEIPT_NO%TYPE,
                    I_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION TSF_CHECK(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_tsf_type              IN OUT   TSFHEAD.TSF_TYPE%TYPE,
                   O_tsf_status            IN OUT   TSFHEAD.STATUS%TYPE,
                   O_from_loc_type         IN OUT   ITEM_LOC.LOC_TYPE%TYPE,
                   O_from_loc_distro       IN OUT   ITEM_LOC.LOC%TYPE,
                   O_from_loc_phy          IN OUT   ITEM_LOC.LOC%TYPE,
                   O_from_tsf_entity       IN OUT   TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                   O_from_finisher         IN OUT   VARCHAR2,
                   O_to_loc_type           IN OUT   ITEM_LOC.LOC_TYPE%TYPE,
                   O_to_loc_distro         IN OUT   ITEM_LOC.LOC%TYPE,
                   O_to_loc_phy            IN OUT   ITEM_LOC.LOC%TYPE,
                   O_to_tsf_entity         IN OUT   TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                   O_to_finisher           IN OUT   VARCHAR2,
                   O_tsf_parent_no         IN OUT   TSFHEAD.TSF_PARENT_NO%TYPE,
                   O_mrt_no                IN OUT   TSFHEAD.MRT_NO%TYPE,
                   O_franchise_ordret_ind  IN OUT   VARCHAR2,
                   I_tsf_no                IN       TSFHEAD.TSF_NO%TYPE,
                   I_loc                   IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION TSF_DETAIL_CHECK(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_tsf_seq_no         IN OUT   TSFDETAIL.TSF_SEQ_NO%TYPE,
                          O_from_inv_status    IN OUT   TSFDETAIL.INV_STATUS%TYPE,
                          I_tsf_no             IN       TSFHEAD.TSF_NO%TYPE,
                          I_item               IN       ITEM_MASTER.ITEM%TYPE,
                          I_inv_status         IN       TSFDETAIL.INV_STATUS%TYPE,
                          I_recv_qty           IN       TSFDETAIL.RECEIVED_QTY%TYPE,
                          I_is_wrong_store     IN       BOOLEAN)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION ALLOC_CHECK(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_alloc_status      IN OUT   ALLOC_HEADER.STATUS%TYPE,
                     O_from_loc_type     IN OUT   ITEM_LOC.LOC_TYPE%TYPE,
                     O_distro_from_loc   IN OUT   ITEM_LOC.LOC%TYPE,
                     O_from_loc_phy      IN OUT   ITEM_LOC.LOC%TYPE,
                     O_from_tsf_entity   IN OUT   TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                     O_to_tsf_entity     IN OUT   TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                     I_alloc_no          IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                     I_item              IN       ITEM_MASTER.ITEM%TYPE,
                     I_to_loc            IN       ITEM_LOC.LOC%TYPE,
                     I_to_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION ALLOC_DETAIL_CHECK(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_alloc_no         IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                            I_to_loc           IN       ITEM_LOC.LOC%TYPE,
                            I_qty              IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            I_is_wrong_store   IN       BOOLEAN,
                            I_item             IN       ALLOC_HEADER.ITEM%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION DETAIL_PROCESSING(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item_rec          IN OUT   STOCK_ORDER_RCV_SQL.ITEM_RCV_RECORD,
                           I_values            IN OUT   STOCK_ORDER_RCV_SQL.COST_RETAIL_QTY_RECORD,
                           I_inv_flow_array    IN       STOCK_ORDER_RCV_SQL.INV_FLOW_ARRAY,
                           I_flow_cnt          IN       BINARY_INTEGER,
                           I_distro_no         IN       SHIPSKU.DISTRO_NO%TYPE,
                           I_distro_type       IN       APPT_DETAIL.DOC_TYPE%TYPE,
                           I_from_inv_status   IN       TSFDETAIL.INV_STATUS%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_STOCK(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_distro_no              IN       SHIPSKU.DISTRO_NO%TYPE,
                           I_distro_type            IN       APPT_DETAIL.DOC_TYPE%TYPE,
                           I_item                   IN       ITEM_MASTER.ITEM%TYPE,
                           I_dept                   IN       ITEM_MASTER.DEPT%TYPE,
                           I_class                  IN       ITEM_MASTER.CLASS%TYPE,
                           I_subclass               IN       ITEM_MASTER.SUBCLASS%TYPE,
                           I_inv_status             IN       SHIPSKU.INV_STATUS%TYPE,
                           I_pack_ind               IN       ITEM_MASTER.PACK_IND%TYPE,
                           I_pack_no                IN       ITEM_MASTER.ITEM%TYPE,
                           IO_pack_value            IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                           I_from_loc               IN       ITEM_LOC.LOC%TYPE,
                           I_from_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_from_loc_wac           IN       ITEM_LOC_SOH.AV_COST%TYPE,         -- Transfer and Item Valuation
                           I_to_loc                 IN       ITEM_LOC.LOC%TYPE,
                           I_to_loc_type            IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_receive_as_type        IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                           I_upd_intran_qty         IN       ITEM_LOC_SOH.IN_TRANSIT_QTY%TYPE,
                           I_upd_av_cost_qty        IN       TSFDETAIL.RECEIVED_QTY%TYPE,
                           I_upd_av_cost_wgt        IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,  -- Catch Weight
                           I_prim_charge            IN       ITEM_LOC_SOH.AV_COST%TYPE,
                           I_received_qty           IN       TSFDETAIL.RECEIVED_QTY%TYPE,
                           I_received_wgt_cuom      IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,  -- Catch Weight: distributed weight
                           I_cuom                   IN       ITEM_SUPP_COUNTRY.COST_UOM%TYPE,   -- Catch Weight: distributed weight
                           I_tran_date              IN       PERIOD.VDATE%TYPE,
                           I_intercompany           IN       BOOLEAN,
                           I_franchise_ordret_ind   IN       VARCHAR2,
                           I_inv_treatment_ind      IN       SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION UPD_INV_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN       ITEM_MASTER.ITEM%TYPE,
                        I_inv_status      IN       SHIPSKU.INV_STATUS%TYPE,
                        I_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        I_loc             IN       ITEM_LOC.LOC%TYPE,
                        I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                        I_tran_date       IN       PERIOD.VDATE%TYPE,
                        I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                        I_ref_no_1        IN       TRAN_DATA.REF_NO_1%TYPE DEFAULT NULL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION TRANDATA_OVERAGE(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_total_pack_value         IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                          I_pack_no                   IN       ITEM_MASTER.ITEM%TYPE,
                          I_item                      IN       ITEM_MASTER.ITEM%TYPE,
                          I_dept                      IN       ITEM_MASTER.DEPT%TYPE,
                          I_class                     IN       ITEM_MASTER.CLASS%TYPE,
                          I_subclass                  IN       ITEM_MASTER.SUBCLASS%TYPE,
                          I_to_loc                    IN       ITEM_LOC.LOC%TYPE,
                          I_to_loc_type               IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_to_tsf_entity             IN       TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                          I_to_finisher               IN       VARCHAR2,
                          I_from_loc                  IN       ITEM_LOC.LOC%TYPE,
                          I_from_loc_type             IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_from_tsf_entity           IN       TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                          I_from_finisher             IN       VARCHAR2,
                          I_rcv_qty                   IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_rcv_weight                IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE, -- Catch Weight
                          I_distro_no                 IN       SHIPSKU.DISTRO_NO%TYPE,
                          I_distro_type               IN       VARCHAR2,
                          I_shipment                  IN       SHIPMENT.SHIPMENT%TYPE,
                          I_tran_date                 IN       PERIOD.VDATE%TYPE,
                          I_from_wac                  IN       item_loc_soh.av_cost%TYPE,        -- Transfers and Item Valuation
                          I_profit_chrgs_to_loc       IN       NUMBER,
                          I_exp_chrgs_to_loc          IN       NUMBER,
                          I_intercompany              IN       BOOLEAN,                          -- Transfers and Item Valuation
                          I_inventory_treatment_ind   IN       SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND%TYPE,
                          I_franchise_ordret_ind      IN       VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION UPDATE_FROM_OVERAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_comp_ind        IN       VARCHAR2,
                             I_from_loc        IN       ITEM_LOC.LOC%TYPE,
                             I_from_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                             I_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_weight_cuom     IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,   -- Catch Weight
                             I_cuom            IN       ITEM_SUPP_COUNTRY.COST_UOM%TYPE)    -- Catch Weight
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION PROC_STK_CNT_TD_WRITE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_distro_no         IN       SHIPSKU.DISTRO_NO%TYPE,
                               I_cycle_count       IN       STAKE_HEAD.CYCLE_COUNT%TYPE,
                               I_item              IN       ITEM_MASTER.ITEM%TYPE,
                               I_dept              IN       ITEM_MASTER.DEPT%TYPE,
                               I_class             IN       ITEM_MASTER.CLASS%TYPE,
                               I_subclass          IN       ITEM_MASTER.SUBCLASS%TYPE,
                               I_to_loc            IN       ITEM_LOC.LOC%TYPE,
                               I_to_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                               I_qty               IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                               I_snapshot_retail   IN       ITEM_LOC_SOH.AV_COST%TYPE,
                               I_snapshot_cost     IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                               I_tran_date         IN       PERIOD.VDATE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION PACK_LEVEL_PROC(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_receive_as_type           IN OUT   ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         O_from_pack_av_cost         IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                         O_pack_av_cost_ratio        IN OUT   NUMBER,
                         I_distro_no                 IN       SHIPSKU.DISTRO_NO%TYPE,
                         I_pack_no                   IN       ITEM_MASTER.ITEM%TYPE,
                         I_dept                      IN       ITEM_MASTER.DEPT%TYPE,
                         I_class                     IN       ITEM_MASTER.CLASS%TYPE,
                         I_subclass                  IN       ITEM_MASTER.SUBCLASS%TYPE,
                         I_inv_status                IN       SHIPSKU.INV_STATUS%TYPE,
                         I_from_loc                  IN       ITEM_LOC.LOC%TYPE,
                         I_from_loc_type             IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_from_rcv_as_type          IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_to_loc                    IN       ITEM_LOC.LOC%TYPE,
                         I_to_loc_type               IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_tran_date                 IN       PERIOD.VDATE%TYPE,
                         I_rcv_qty                   IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         I_intran_qty                IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         I_overage_qty               IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         I_overage_weight_cuom       IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,   -- Catch Weight
                         I_cuom                      IN       ITEM_SUPP_COUNTRY.COST_UOM%TYPE,    -- CatchWeight
                         I_prim_charge               IN       ITEM_LOC_SOH.AV_COST%TYPE,
                         I_from_loc_av_cost          IN       ITEM_LOC_SOH.AV_COST%TYPE,
                         I_from_inv_status           IN       TSFDETAIL.INV_STATUS%TYPE,
                         I_inventory_treatment_ind   IN       SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION UPDATE_PACK_STOCK(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_pack_no               IN       ITEM_MASTER.ITEM%TYPE,
                           I_to_loc                IN       ITEM_LOC.LOC%TYPE,
                           I_to_loc_type           IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_stk_cnt_procd         IN       BOOLEAN,
                           I_rcv_qty               IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_intran_qty            IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_overage_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,   -- Catch Weight
                           I_overage_weight_cuom   IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,  -- Catch Weight
                           I_tran_date             IN       PERIOD.VDATE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION LOAD_COMPS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_comp_items      IN OUT   STOCK_ORDER_RCV_SQL.COMP_ITEM_ARRAY,
                    I_pack_no         IN       ITEM_MASTER.ITEM%TYPE,
                    I_from_loc        IN       ITEM_LOC.LOC%TYPE,
                    I_from_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_to_loc          IN       ITEM_LOC.LOC%TYPE,
                    I_to_loc_type     IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION FLUSH_APPT_DETAIL_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION FLUSH_DOC_CLOSE_QUEUE_INSERT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- This function is only here as a debug aid.  It should not be used
-- in production code.
--------------------------------------------------------------------------------
/*
FUNCTION DISPLAY_STRUCT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item_rec         IN       STOCK_ORDER_RCV_SQL.ITEM_RCV_RECORD,
                        I_values           IN       STOCK_ORDER_RCV_SQL.COST_RETAIL_QTY_RECORD,
                        I_inv_flow_array   IN       STOCK_ORDER_RCV_SQL.INV_FLOW_ARRAY)
RETURN BOOLEAN;*/

--------------------------------------------------------------------------------
FUNCTION UPD_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_shipment        IN       SHIPMENT.SHIPMENT%TYPE,
                      I_tran_date       IN       PERIOD.VDATE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- This function will check if the Carton is unwanded carton.
-- If the carton does not exist for any shipment in the system (not on shipsku),
-- we assume it is an unwanded carton, i.e. it was physically placed on the
-- truck at the shipping location but never scanned.
--------------------------------------------------------------------------------
FUNCTION UNWANDED_CARTON(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_unwanded        IN OUT   BOOLEAN,
                         I_carton          IN       SHIPSKU.CARTON%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- This function will return O_is_walk_through = TRUE if shipment.to_loc is
-- a walk through store for I_rcv_to_loc.
--------------------------------------------------------------------------------
FUNCTION WALK_THROUGH_STORE (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_is_walk_through   IN OUT   BOOLEAN,
                             O_shipment          IN OUT   SHIPMENT.SHIPMENT%TYPE,
                             O_intended_store    IN OUT   STORE.STORE%TYPE,
                             I_bol_no            IN       SHIPMENT.BOL_NO%TYPE,
                             I_rcv_to_loc        IN       STORE.STORE%TYPE,
                             I_carton            IN       SHIPSKU.CARTON%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- This function will reverse shipment transactions to the intended location
-- and create shipment transactions to the actual receiving location.
--------------------------------------------------------------------------------
FUNCTION WRONG_STORE_RECEIPT(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_shipment               IN OUT   SHIPMENT.SHIPMENT%TYPE,
                             O_intended_to_loc        IN OUT   ITEM_LOC.LOC%TYPE,
                             I_actual_to_loc          IN       ITEM_LOC.LOC%TYPE,
                             I_actual_to_tsf_entity   IN       TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                             I_from_loc               IN       ITEM_LOC.LOC%TYPE,
                             I_from_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                             I_from_tsf_entity        IN       TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                             I_from_finisher          IN       VARCHAR2,
                             I_item                   IN       ITEM_MASTER.ITEM%TYPE,
                             I_bol_no                 IN       SHIPMENT.BOL_NO%TYPE,
                             I_carton                 IN       SHIPSKU.CARTON%TYPE,
                             I_distro_type            IN       SHIPSKU.DISTRO_TYPE%TYPE,
                             I_distro_no              IN       SHIPSKU.DISTRO_NO%TYPE,
                             I_dept                   IN       ITEM_MASTER.DEPT%TYPE,
                             I_class                  IN       ITEM_MASTER.CLASS%TYPE,
                             I_subclass               IN       ITEM_MASTER.SUBCLASS%TYPE,
                             I_pack_ind               IN       ITEM_MASTER.PACK_IND%TYPE,
                             I_pack_type              IN       ITEM_MASTER.PACK_TYPE%TYPE,
                             I_tran_date              IN       TRAN_DATA.TRAN_DATE%TYPE,
                             I_tsf_type               IN       TSFHEAD.TSF_TYPE%TYPE)          -- Transfer and Item Valuation
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION UPD_TO_ITEM_LOC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_distro_no           IN       SHIPSKU.DISTRO_NO%TYPE,
                         I_distro_type         IN       APPT_DETAIL.DOC_TYPE%TYPE,
                         I_item                IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_no             IN       ITEM_MASTER.ITEM%TYPE,
                         I_percent_in_pack     IN       NUMBER,
                         I_receive_as_type     IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_to_loc              IN       ITEM_LOC.LOC%TYPE,
                         I_to_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         I_weight_cuom         IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,  -- Catch Weight
                         I_cuom                IN       UOM_CLASS.UOM%TYPE,                -- Catch Weight
                         I_from_loc            IN       ITEM_LOC.LOC%TYPE,
                         I_from_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_from_wac            IN       ITEM_LOC_SOH.AV_COST%TYPE,         -- changed from av_cost to wac for Transfers and Item Valuation
                         I_prim_charge         IN       ITEM_LOC_SOH.AV_COST%TYPE,
                         I_intercompany        IN       BOOLEAN)
                         I_recalc_from_to_loc  IN       VARCHAR2 DEFAULT 'T',
                         I_shipment            IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                         I_carton              IN       SHIPSKU.CARTON%TYPE DEFAULT NULL,
                         I_chrg_from_loc       IN       ITEM_LOC_SOH.AV_COST%TYPE DEFAULT NULL,
                         I_wrong_st_rcv_ind    IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- This function should be called when receiving at a finisher.  The function
-- will reserve the 'to' item quantity at the finisher and will increment the
-- 'to' item expected qty at the final receiving location.
--------------------------------------------------------------------------------
FUNCTION UPD_ITEM_RESV_EXP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                             I_recv_loc        IN       ITEM_LOC.LOC%TYPE,
                             I_recv_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                             I_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                             I_pack_no         IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION NEW_RECEIPT_ITEM(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_item_rec          IN OUT   STOCK_ORDER_RCV_SQL.ITEM_RCV_RECORD,
                          I_shipment          IN       SHIPMENT.SHIPMENT%TYPE,
                          I_from_inv_status   IN       SHIPSKU.INV_STATUS%TYPE,
                          I_carton            IN       SHIPSKU.CARTON%TYPE,
                          I_qty               IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_weight            IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,   -- Catch Weight
                          I_weight_uom        IN       UOM_CLASS.UOM%TYPE)                 -- CatchWeight
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_INV_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_inv_status      IN OUT   SHIPSKU.INV_STATUS%TYPE,
                        I_shipment        IN       SHIPSKU.SHIPMENT%TYPE,
                        I_distro_no       IN       SHIPSKU.DISTRO_NO%TYPE,
                        I_distro_type     IN       SHIPSKU.DISTRO_TYPE%TYPE,
                        I_carton          IN       SHIPSKU.CARTON%TYPE,
                        I_item            IN       SHIPSKU.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION MRT_LINE_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_mrt_no          IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                       I_item            IN       MRT_ITEM_LOC.ITEM%TYPE,
                       I_location        IN       MRT_ITEM_LOC.LOCATION%TYPE,
                       I_received_qty    IN       MRT_ITEM_LOC.RECEIVED_QTY%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION NEW_RECEIPT_ITEM_ALLOC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_item_rec          IN OUT   STOCK_ORDER_RCV_SQL.ITEM_RCV_RECORD,
                                I_shipment          IN       SHIPMENT.SHIPMENT%TYPE,
                                I_from_inv_status   IN       SHIPSKU.INV_STATUS%TYPE,
                                I_carton            IN       SHIPSKU.CARTON%TYPE,
                                I_qty               IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                I_weight            IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,   -- Catch Weight
                                I_weight_uom        IN       UOM_CLASS.UOM%TYPE)                 -- CatchWeight
RETURN BOOLEAN;
--------------------------------------------------------------------------------
$end

END STOCK_ORDER_RCV_SQL;
/

