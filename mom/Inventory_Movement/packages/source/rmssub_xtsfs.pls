CREATE OR REPLACE PACKAGE RMSSUB_XTSF AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   -- PACKAGE VARIABLES
----------------------------------------------------------------------------
   LP_cre_type       VARCHAR2(15) := 'xtsfcre';
   LP_mod_type       VARCHAR2(15) := 'xtsfmod';
   LP_del_type       VARCHAR2(15) := 'xtsfdel';
   LP_dtl_cre_type   VARCHAR2(15) := 'xtsfdtlcre';
   LP_dtl_mod_type   VARCHAR2(15) := 'xtsfdtlmod';
   LP_dtl_del_type   VARCHAR2(15) := 'xtsfdtldel';

TYPE SUPP_PACK_TBL is TABLE OF TSFDETAIL.SUPP_PACK_SIZE%TYPE;
TYPE INV_TBL is TABLE OF TSFDETAIL.INV_STATUS%TYPE;
TYPE SEQ_TBL is TABLE OF TSFDETAIL.TSF_SEQ_NO%TYPE;
TYPE PRICE_TBL is TABLE OF TSFDETAIL.TSF_PRICE%TYPE;

TYPE TSF_DETAIL_REC is RECORD(items             ITEM_TBL,
                              seq_nos           SEQ_TBL,
                              qtys              QTY_TBL,
                              existing_qtys     QTY_TBL,
                              supp_pack_sizes   SUPP_PACK_TBL,
                              inv_statuses      INV_TBL,
                              pack_inds         INDICATOR_TBL,
                              pack_types        INDICATOR_TBL,
                              cancelled_qtys    QTY_TBL,
                              selected_qtys     QTY_TBL,
                              tsf_price         PRICE_TBL);

TYPE ITEM_LOCS_REC IS RECORD(items                 ITEM_TBL,
                             pack_inds             INDICATOR_TBL,
                             pack_types            INDICATOR_TBL,
                             locs                  LOC_TBL,
                             loc_types             LOC_TYPE_TBL);

TYPE TSF_REC is RECORD (header         TSFHEAD%ROWTYPE,
                        tsf_detail     TSF_DETAIL_REC,
                        new_itemloc    ITEM_LOCS_REC);

TYPE ILS_UPD_REC IS RECORD(items               ITEM_TBL,
                           pack_inds           INDICATOR_TBL,
                           from_locs           LOC_TBL,
                           from_loc_types      INDICATOR_TBL,
                           to_locs             LOC_TBL,
                           to_loc_types        INDICATOR_TBL,
                           qtys                QTY_TBL,
                           existing_qtys       QTY_TBL,
                           inv_statuses        INV_TBL,
                           tsf_no              TSFHEAD.TSF_NO%TYPE,
                           create_id           TSFHEAD.CREATE_ID%TYPE);

TYPE TSF_TBL is TABLE OF TSF_REC;

----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_XTSF;
/