
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE UP_CHARGE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name: CALC_COMP
--Purpose      : Function will calculate the passed in item or transfer
--               up charge components.  The est value and the cost basis
--               are passed out in the component currency. I_item will contain
--               either the item or the component item within a buyer pack.  So
--               if there's a buyer pack on the transfer this function will be 
--               called for each item within the pack.
-------------------------------------------------------------------------------
FUNCTION CALC_COMP(O_error_message     IN OUT VARCHAR2,
                   O_est_value_comp    IN OUT NUMBER,
                   O_cost_basis_comp   IN OUT NUMBER,
                   I_comp_id           IN     ELC_COMP.COMP_ID%TYPE,
                   I_item              IN     ITEM_MASTER.ITEM%TYPE,
                   I_from_loc          IN     STORE.STORE%TYPE,
                   I_from_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                   I_to_loc            IN     STORE.STORE%TYPE,
                   I_to_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                   I_tsf_alloc_type    IN     VARCHAR2,
                   I_tsf_alloc_no      IN     TSFDETAIL_CHRG.TSF_NO%TYPE,
                   I_tsf_seq_no        IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                   I_shipment          IN     SHIPMENT.SHIPMENT%TYPE,
                   I_ship_seq_no       IN     SHIPSKU.SEQ_NO%TYPE,
                   I_from_loc_cost     IN     SHIPSKU.UNIT_COST%TYPE DEFAULT NULL,
                   I_wrong_st_rcv_ind  IN     VARCHAR2 DEFAULT 'N',
                   I_pack_item         IN     ITEM_MASTER.ITEM%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name: CALC_GROUP
--Purpose      : Function will calculate the passed in item or transfer
--               up charge groups.  The value is passed out in primary currency
-------------------------------------------------------------------------------
FUNCTION CALC_GROUP(O_error_message  IN OUT VARCHAR2,
                    O_value_prim     IN OUT ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE,
                    I_tsf_alloc_type IN     VARCHAR2,
                    I_tsf_alloc_no   IN     TSFHEAD.TSF_NO%TYPE,
                    I_tsf_type       IN     TSFHEAD.TSF_TYPE%TYPE,
                    I_tsf_seq_no     IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                    I_item           IN     ITEM_MASTER.ITEM%TYPE,
                    I_pack_item      IN     ITEM_MASTER.ITEM%TYPE,
                    I_from_loc       IN     STORE.STORE%TYPE,
                    I_from_loc_type  IN     ITEM_LOC.LOC_TYPE%TYPE,
                    I_to_loc         IN     STORE.STORE%TYPE,
                    I_to_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                    I_up_chrg_group  IN     ELC_COMP.UP_CHRG_GROUP%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name: CALC_TSF_ALLOC_ITEM_LOC_CHRGS
--Purpose      : Function will calculate the passed in transfer/item/location
--               up charges.  The Total Charges value is passed out in 
--               primary currency.  The Total Profit Charges and the
--               Total Expense Charges values are passed out in To Location
--               currency.  In order to calculate the Total Transfer/Item
--               charges of a Buyer pack, pass the Buyer Pack in I_item, and NULL
--               in I_pack_item.  To get the Total Transfer/Item charges of
--               a particular Component Item of a Buyer Pack, pass the Component
--               item in I_item and the Buyer Pack in I_pack_item.  To get the
--               Total Transfer/Item charges of any other item, pass the Item
--               in I_item and NULL in I_pack_item.
-------------------------------------------------------------------------------
FUNCTION CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message        IN OUT VARCHAR2,
                                       O_total_chrgs_prim     IN OUT ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE,
                                       O_profit_chrgs_to_loc  IN OUT NUMBER,
                                       O_exp_chrgs_to_loc     IN OUT NUMBER,
                                       I_tsf_alloc_type       IN     VARCHAR2,
                                       I_tsf_alloc_no         IN     TSFHEAD.TSF_NO%TYPE,
                                       I_tsf_seq_no           IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                                       I_shipment             IN     SHIPMENT.SHIPMENT%TYPE,
                                       I_ship_seq_no          IN     SHIPSKU.SEQ_NO%TYPE,
                                       I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                                       I_pack_item            IN     ITEM_MASTER.ITEM%TYPE,
                                       I_from_loc             IN     STORE.STORE%TYPE,
                                       I_from_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                                       I_to_loc               IN     STORE.STORE%TYPE,
                                       I_to_loc_type          IN     ITEM_LOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name: CALC_TSF_ALLOC_ITEM_CHRGS
--Purpose      : Function will calculate the passed in transfer/item
--               up charges.  The Total Charges value is passed out in 
--               primary currency.  The Total Profit Charges and the
--               Total Expense Charges values are passed out in To Location
--               currency.  In order to calculate the Total Transfer/Item
--               charges of a Buyer pack, pass the Buyer Pack in I_item, and NULL
--               in I_pack_item.  To get the Total Transfer/Item charges of
--               a particular Component Item of a Buyer Pack, pass the Component
--               item in I_item and the Buyer Pack in I_pack_item.  To get the
--               Total Transfer/Item charges of any other item, pass the Item
--               in I_item and NULL in I_pack_item.
----------------------------------------------------------------------------------
FUNCTION CALC_TSF_ALLOC_ITEM_CHRGS(O_error_message        IN OUT VARCHAR2,
                                   O_total_chrgs_prim     IN OUT ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE,
                                   O_profit_chrgs_to_loc  IN OUT NUMBER,
                                   O_exp_chrgs_to_loc     IN OUT NUMBER,
                                   I_tsf_alloc_type       IN     VARCHAR2,
                                   I_tsf_alloc_no         IN     TSFHEAD.TSF_NO%TYPE,
                                   I_tsf_type             IN     TSFHEAD.TSF_TYPE%TYPE,
                                   I_tsf_seq_no           IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                                   I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                                   I_pack_item            IN     ITEM_MASTER.ITEM%TYPE,
                                   I_from_loc             IN     STORE.STORE%TYPE,
                                   I_from_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                                   I_to_loc               IN     STORE.STORE%TYPE,
                                   I_to_loc_type          IN     ITEM_LOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name: CALC_ITEM_CHRGS
--Purpose      : Function will calculate the passed in transfer/item/location
--               up charges.  The Total Charges value is passed out in 
--               primary currency.  The Total Profit Charges and the
--               Total Expense Charges values are passed out in To Location
--               currency.  In order to calculate the Total Transfer/Item
--               charges of a Buyer pack, pass the Buyer Pack in I_item, and NULL
--               in I_pack_item.  To get the Total Transfer/Item charges of
--               a particular Component Item of a Buyer Pack, pass the Component
--               item in I_item and the Buyer Pack in I_pack_item.  To get the
--               Total Transfer/Item charges of any other item, pass the Item
--               in I_item and NULL in I_pack_item.
-------------------------------------------------------------------------------
FUNCTION CALC_ITEM_CHRGS(O_error_message         IN OUT   VARCHAR2,
                         O_total_chrgs_prim      IN OUT   ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE,
                         O_profit_chrgs_to_loc   IN OUT   NUMBER,
                         O_exp_chrgs_to_loc      IN OUT   NUMBER,
                         I_tsf_alloc_type        IN       VARCHAR2,
                         I_tsf_alloc_no          IN       TSFHEAD.TSF_NO%TYPE,
                         I_tsf_seq_no            IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                         I_shipment              IN       SHIPMENT.SHIPMENT%TYPE,
                         I_ship_seq_no           IN       SHIPSKU.SEQ_NO%TYPE,
                         I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_item             IN       ITEM_MASTER.ITEM%TYPE,
                         I_from_loc              IN       STORE.STORE%TYPE,
                         I_from_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_to_loc                IN       STORE.STORE%TYPE,
                         I_to_loc_type           IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_from_loc_cost         IN       SHIPSKU.UNIT_COST%TYPE)
   RETURN BOOLEAN;
END UP_CHARGE_SQL;
/


