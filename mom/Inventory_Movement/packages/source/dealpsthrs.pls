


CREATE OR REPLACE PACKAGE DEAL_PASSTHRU_SQL AUTHID CURRENT_USER AS

-- Declare record types / table types

TYPE deal_passthru_rec is RECORD(DEPT                DEAL_PASSTHRU.DEPT%TYPE,
                                 DEPT_NAME           DEPS.DEPT_NAME%TYPE,
                                 SUPPLIER            DEAL_PASSTHRU.SUPPLIER%TYPE,
                                 SUP_NAME            SUPS.SUP_NAME%TYPE,
                                 COSTING_LOC         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                                 COSTING_LOC_NAME    v_costing_loc.costing_loc_name%TYPE,
                                 LOCATION            DEAL_PASSTHRU.LOCATION%TYPE,
                                 LOCATION_NAME       STORE.STORE_NAME%TYPE,
                                 LOC_TYPE            DEAL_PASSTHRU.LOC_TYPE%TYPE,
                                 PASSTHRU_PCT        DEAL_PASSTHRU.PASSTHRU_PCT%TYPE,
                                 ERROR_MESSAGE       RTK_ERRORS.RTK_TEXT%TYPE,
                                 RETURN_CODE         VARCHAR2(5));

TYPE DEAL_PASSTHRU_TBL is TABLE OF deal_passthru_rec
  INDEX BY BINARY_INTEGER;

---------------------------------------------------------------------------------------
-- Procedure Name: GET_PASSTHRU
-- Purpose: This function will retrieve passthru records which meet
--          the input criteria passed in.
---------------------------------------------------------------------------------------
PROCEDURE GET_PASSTHRU(O_DEAL_PASSTHRU_TBL     IN OUT     DEAL_PASSTHRU_TBL,
                       I_DEPT                  IN         DEAL_PASSTHRU.DEPT%TYPE,
                       I_SUPPLIER              IN         DEAL_PASSTHRU.SUPPLIER%TYPE,
                       I_COSTING_LOC           IN         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                       I_ORG_LEVEL             IN         CODE_DETAIL.CODE%TYPE,
                       I_ORG_VALUE             IN         DEAL_PASSTHRU.LOCATION%TYPE);
---------------------------------------------------------------------------------------
-- Function Name: LOCATION_EXPLODE
-- Purpose: This function will take the org level type and org level value inputs and
--          explode them down to the store level, and then insert records into the
--          DEAL_PASSTHRU table for WF stores only.
---------------------------------------------------------------------------------------
FUNCTION LOCATION_EXPLODE(O_ERROR_MESSAGE     IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                          I_DEPT              IN         DEAL_PASSTHRU.DEPT%TYPE,
                          I_SUPPLIER          IN         DEAL_PASSTHRU.SUPPLIER%TYPE,
                          I_COSTING_LOC           IN         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                          I_ORG_LEVEL         IN         CODE_DETAIL.CODE%TYPE,
                          I_ORG_VALUE         IN         DEAL_PASSTHRU.LOCATION%TYPE,
                          I_PASSTHRU_PCT      IN         DEAL_PASSTHRU.PASSTHRU_PCT%TYPE,
                          I_OVERWRITE_IND     IN         VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: OVERLAP_CHECK
-- Purpose: This function will check and determine if records already exist for the
--          dept, supplier, wh, group type, value combination passed in on the
--          DEAL_PASSTHRU table.
---------------------------------------------------------------------------------------
FUNCTION OVERLAP_CHECK(O_ERROR_MESSAGE        IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                       O_OVERLAP_EXISTS       IN OUT     VARCHAR2,
                       I_DEPT                 IN         DEAL_PASSTHRU.DEPT%TYPE,
                       I_SUPPLIER             IN         DEAL_PASSTHRU.SUPPLIER%TYPE,
                       I_COSTING_LOC           IN         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                       I_ORG_LEVEL            IN         CODE_DETAIL.CODE%TYPE,
                       I_ORG_VALUE            IN         DEAL_PASSTHRU.LOCATION%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: PASSTHRU_MASS_UPDATE
-- Purpose: This function will, based on input parameters, either update existing
--          records for dept/supplier/wh combinations passed in, or leave the existing
--          values as is. New records will be inserted for any non-existing locations.
---------------------------------------------------------------------------------------
FUNCTION PASSTHRU_MASS_UPDATE(O_ERROR_MESSAGE      IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                              I_DEAL_PASSTHRU_TBL  IN         DEAL_PASSTHRU_TBL,
                              I_PASSTHRU_PCT       IN         DEAL_PASSTHRU.PASSTHRU_PCT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: UPD_PASSTHRU_VALUE
-- Purpose: This function will be used as the update procedure for the calling form's
--          multi-record block.
---------------------------------------------------------------------------------------
FUNCTION UPD_PASSTHRU_VALUE(O_ERROR_MESSAGE        IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                            I_DEPT                 IN         DEAL_PASSTHRU.DEPT%TYPE,
                            I_SUPPLIER             IN         DEAL_PASSTHRU.SUPPLIER%TYPE,
                            I_COSTING_LOC           IN         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                            I_ORG_LEVEL            IN         CODE_DETAIL.CODE%TYPE,
                            I_ORG_VALUE            IN         DEAL_PASSTHRU.LOCATION%TYPE,
                            I_PASSTHRU_PCT         IN         DEAL_PASSTHRU.PASSTHRU_PCT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: DELETE_ALL_PASSTHRU_RECS
-- Purpose: This function will be used as the delete procedure for the calling form's
--          multi-record block to delete all records from the DEAL_PASSTHRU table where
--          location is equal to records from the input table type.
---------------------------------------------------------------------------------------
FUNCTION DELETE_ALL_PASSTHRU_RECS(O_ERROR_MESSAGE      IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_DEAL_PASSTHRU_TBL  IN         DEAL_PASSTHRU_TBL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: DELETE_PASSTHRU_RECS
-- Purpose: This function will be used as the delete procedure for the calling form's
--          multi-record block to delete records from the DEAL_PASSTHRU table.
---------------------------------------------------------------------------------------
FUNCTION DELETE_PASSTHRU_RECS(O_ERROR_MESSAGE      IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                              I_DEPT               IN         DEAL_PASSTHRU.DEPT%TYPE,
                              I_SUPPLIER           IN         DEAL_PASSTHRU.SUPPLIER%TYPE,
                              I_COSTING_LOC           IN         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                              I_LOCATION           IN         DEAL_PASSTHRU.LOCATION%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: POPULATE_GTT
-- Purpose: This function will be used to populate the table GTT_DEAL_PASSTHRU with the
--          affected records along with type of modification 
FUNCTION POPULATE_GTT(O_ERROR_MESSAGE          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_DEAL_PASSTHRU_NEW_TBL  IN OUT OBJ_DEAL_PASSTHRU_EVENT_TBL, 
                      O_DEAL_PASSTHRU_MOD_TBL  IN OUT OBJ_DEAL_PASSTHRU_EVENT_TBL,
                      O_DEAL_PASSTHRU_REM_TBL  IN OUT OBJ_DEAL_PASSTHRU_EVENT_TBL,
                      I_DEAL_PASSTHRU_TBL      IN     DEAL_PASSTHRU_TBL,
                      I_NEW_PASSTHRU_PCT       IN     GTT_DEAL_PASSTHRU.NEW_PASSTHRU_PCT%TYPE,
                      I_ACTION                 IN     COST_EVENT.ACTION%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: FUTURE_COST_UPD
-- Purpose: This function will be used to call future cost engine with passed in object types
FUNCTION FUTURE_COST_UPD(O_ERROR_MESSAGE         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_NEW_DEAL_PASSTHRU_TBL IN     OBJ_DEAL_PASSTHRU_EVENT_TBL,
                         I_MOD_DEAL_PASSTHRU_TBL IN     OBJ_DEAL_PASSTHRU_EVENT_TBL,
                         I_REM_DEAL_PASSTHRU_TBL IN     OBJ_DEAL_PASSTHRU_EVENT_TBL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------


END DEAL_PASSTHRU_SQL;
/