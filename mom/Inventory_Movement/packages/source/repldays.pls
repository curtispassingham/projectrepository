
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE REPLENISHMENT_DAYS_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------
-- Function Name: get_day_offset
-- Purpose: This function will get the number of days
--          from Sunday for first day of week
FUNCTION GET_DAY_OFFSET(O_error_message IN OUT  VARCHAR2,
                        O_offset        OUT     NUMBER)
RETURN BOOLEAN;


-------------------------------------------------------------------
-- Function Name: insert_day_supp
-- Purpose: This function will insert review cycle day records
--          into the SUP_REPL_DAY table.  A record will be inserted 
--          for each day parameter inputted as 'Y'.
-------------------------------------------------------------------           
FUNCTION INSERT_DAY_SUPP(O_error_message IN OUT  VARCHAR2,
                         I_sequence_no   IN      sup_repl_day.sup_dept_seq_no%TYPE,
                         I_SUN           IN      VARCHAR2,
                         I_MON           IN      VARCHAR2,
                         I_TUE           IN      VARCHAR2,
                         I_WED           IN      VARCHAR2,
                         I_THU           IN      VARCHAR2,
                         I_FRI           IN      VARCHAR2,
                         I_SAT           IN      VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: insert_day_item
-- Purpose: This function will insert review cycle day records
--          into the REPL_DAY table.  A record will be inserted 
--          for each day parameter inputted as 'Y'.
-------------------------------------------------------------------
FUNCTION INSERT_DAY_ITEM(O_error_message IN OUT  VARCHAR2,
                         I_item          IN      repl_day.item%TYPE,
                         I_location      IN      repl_day.location%TYPE,
                         I_loc_type      IN      repl_day.loc_type%TYPE,
                         I_SUN           IN      VARCHAR2,
                         I_MON           IN      VARCHAR2,
                         I_TUE           IN      VARCHAR2,
                         I_WED           IN      VARCHAR2,
                         I_THU           IN      VARCHAR2,
                         I_FRI           IN      VARCHAR2,
                         I_SAT           IN      VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: update_day_supp
-- Purpose: This function will update the review cycle day records 
--          on the SUP_REPL_DAY table.  A record will be updated 
--          for each day parameter inputted as 'Y'.
-------------------------------------------------------------------           
FUNCTION UPDATE_DAY_SUPP(O_error_message IN OUT   VARCHAR2,
                         I_sequence_no   IN       sup_repl_day.sup_dept_seq_no%TYPE,
                         I_SUN           IN       VARCHAR2,
                         I_MON           IN       VARCHAR2,
                         I_TUE           IN       VARCHAR2,
                         I_WED           IN       VARCHAR2,
                         I_THU           IN       VARCHAR2,
                         I_FRI           IN       VARCHAR2,
                         I_SAT           IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: update_day_item
-- Purpose: This function will update the review cycle day records 
--          on the REPL_DAYS table.  A record will be updated 
--          for each day parameter inputted as 'Y'.
-------------------------------------------------------------------           
FUNCTION UPDATE_DAY_ITEM(O_error_message IN OUT   VARCHAR2,
                         I_item          IN       repl_day.item%TYPE,
                         I_location      IN       repl_day.location%TYPE,
                         I_loc_type      IN       repl_day.loc_type%TYPE,
                         I_SUN           IN       VARCHAR2,
                         I_MON           IN       VARCHAR2,
                         I_TUE           IN       VARCHAR2,
                         I_WED           IN       VARCHAR2,
                         I_THU           IN       VARCHAR2,
                         I_FRI           IN       VARCHAR2,
                         I_SAT           IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: query_day_supp
-- Purpose: This function will determine, from the SUP_REPL_DAY
--          tables, which of the seven days is being reviewed for 
--          the supplier/item location.
-------------------------------------------------------------------           
 FUNCTION QUERY_DAY_SUPP(O_error_message IN OUT  VARCHAR2,
                         I_sequence_no   IN      sup_repl_day.sup_dept_seq_no%TYPE,
                         O_SUN           IN OUT  VARCHAR2,
                         O_MON           IN OUT  VARCHAR2,
                         O_TUE           IN OUT  VARCHAR2,
                         O_WED           IN OUT  VARCHAR2,
                         O_THU           IN OUT  VARCHAR2,
                         O_FRI           IN OUT  VARCHAR2,
                         O_SAT           IN OUT  VARCHAR2)
  RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: query_day_item
-- Purpose: This function will determine, from the REPL_DAY
--          tables, which of the seven days is being reviewed for 
--          the supplier/item location.
-------------------------------------------------------------------           
 FUNCTION QUERY_DAY_ITEM(O_error_message IN OUT  VARCHAR2,
                         I_item          IN      repl_day.item%TYPE,
                         I_location      IN      repl_day.location%TYPE,
                         O_SUN           IN OUT  VARCHAR2,
  			 O_MON           IN OUT  VARCHAR2,
  			 O_TUE           IN OUT  VARCHAR2,
  			 O_WED           IN OUT  VARCHAR2,
  			 O_THU           IN OUT  VARCHAR2,
  			 O_FRI           IN OUT  VARCHAR2,
                         O_SAT           IN OUT  VARCHAR2,
                         I_repl_attr_id  IN      repl_attr_update_head.repl_attr_id%TYPE DEFAULT NULL, 
                         I_mra           IN      repl_attr_update_head.mra_update%TYPE   DEFAULT NULL
                        )
  RETURN BOOLEAN;   
-------------------------------------------------------------------
-- Function Name:  convert_to_offset_day
-- Purpose: This function will check the system offset day for replenishment,
--          retrieve the day information in number format and return the offset
--          day number.
--------------------------------------------------------------------
FUNCTION CONVERT_TO_OFFSET_DAY(O_error_message  IN OUT VARCHAR2,
                               O_offset_day     IN OUT SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE,
			       I_day		IN     SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE)
  RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function Name:  convert_from_offset_day
-- Purpose:  This function will check the system offset day and the offset day of 
--           week and will return the actual day of week where sunday = 1, 
--           monday = 2 etc.
------------------------------------------------------------------------
FUNCTION CONVERT_FROM_OFFSET_DAY(O_error_message   IN OUT VARCHAR2,
                                 O_day             IN OUT SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE,
                                 O_day_name	   IN OUT VARCHAR2,
                                 I_offset_day      IN     SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE)
  RETURN BOOLEAN;

--------------------------------------------------------------------------
END REPLENISHMENT_DAYS_SQL;
/


