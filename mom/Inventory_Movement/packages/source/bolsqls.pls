CREATE OR REPLACE PACKAGE BOL_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Global Variable Declarations
GP_ship_seq_no   SHIPSKU.SEQ_NO%TYPE := NULL;
---
TYPE bol_virtual_loc IS RECORD(
   vir_from_loc        ITEM_LOC.LOC%TYPE,
   vir_from_loc_type   ITEM_LOC.LOC_TYPE%TYPE,
   vir_from_store      ITEM_LOC.LOC%TYPE,
   vir_from_wh         ITEM_LOC.LOC%TYPE,
   from_av_cost        ITEM_LOC_SOH.AV_COST%TYPE,

   vir_to_loc          ITEM_LOC.LOC%TYPE,
   vir_to_loc_type     ITEM_LOC.LOC_TYPE%TYPE,
   vir_to_store        ITEM_LOC.LOC%TYPE,
   vir_to_wh           ITEM_LOC.LOC%TYPE,

   receive_as_type     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,

   vir_qty             ITEM_LOC_SOH.STOCK_ON_HAND%TYPE
);
TYPE bol_virtual_loc_array IS TABLE of bol_virtual_loc
 INDEX BY BINARY_INTEGER;

TYPE bol_item_rec IS RECORD(
   item            ITEM_MASTER.ITEM%TYPE,
  --if item in message is below tran level, this will contain that item
   ref_item        ITEM_MASTER.ITEM%TYPE,
   pack_ind        ITEM_MASTER.PACK_IND%TYPE,
   pack_type       ITEM_MASTER.PACK_TYPE%TYPE,
   supp_pack_size  ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
   dept            ITEM_MASTER.DEPT%TYPE,
   class           ITEM_MASTER.CLASS%TYPE,
   subclass        ITEM_MASTER.SUBCLASS%TYPE,
   carton          SHIPSKU.CARTON%TYPE,
   inv_status      SHIPSKU.INV_STATUS%TYPE,
   qty             ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,

   -- for catch weight processing
   simple_pack_ind  ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
   catch_weight_ind ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
   weight           ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
   weight_uom       ITEM_SUPP_COUNTRY.COST_UOM%TYPE,

   -- for break to sell processing
   sellable_ind     ITEM_MASTER.SELLABLE_IND%TYPE,
   item_xform_ind   ITEM_MASTER.ITEM_XFORM_IND%TYPE,

   td_tsf_seq_no    TSFDETAIL.TSF_SEQ_NO%TYPE,
   td_tsf_qty       TSFDETAIL.TSF_QTY%TYPE,
   td_ship_qty      TSFDETAIL.SHIP_QTY%TYPE,

   ad_to_loc_phy    ITEM_LOC.LOC%TYPE,
   ad_to_loc_vir    ITEM_LOC.LOC%TYPE,
   ad_tsf_qty       ALLOC_DETAIL.QTY_TRANSFERRED%TYPE,
   ad_alloc_qty     ALLOC_DETAIL.QTY_ALLOCATED%TYPE,
   ad_dist_qty      ALLOC_DETAIL.QTY_ALLOCATED%TYPE,
   ad_dist_weight   ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,

   ss_seq_no        SHIPSKU.SEQ_NO%TYPE,

   virtual_locs     BOL_VIRTUAL_LOC_ARRAY,
   extended_base_cost ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE,
   base_cost          ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE
);
TYPE bol_item_array IS TABLE of bol_item_rec
 INDEX BY BINARY_INTEGER;

TYPE bol_distro_rec IS RECORD(
   alloc_no             ALLOC_HEADER.ALLOC_NO%TYPE,
   alloc_status         ALLOC_HEADER.STATUS%TYPE,
   alloc_type           VARCHAR2(3), --'SA' (PO is null) or 'PRE' (PO is not NULL)
   new_alloc_detail_ind VARCHAR2(1),
   alloc_from_loc_phy   ALLOC_HEADER.WH%TYPE,
   alloc_from_loc_vir   ALLOC_HEADER.WH%TYPE,

   tsf_no               TSFHEAD.TSF_NO%TYPE,
   tsf_status           TSFHEAD.STATUS%TYPE,
   tsf_type             TSFHEAD.TSF_TYPE%TYPE,
   new_tsf_ind          VARCHAR2(1),

   bol_items            BOL_ITEM_ARRAY
);
TYPE bol_distro_array IS TABLE of bol_distro_rec
 INDEX BY BINARY_INTEGER;

TYPE bol_rec IS RECORD(
   bol_no                 SHIPMENT.BOL_NO%TYPE,
   ship_no                SHIPMENT.SHIPMENT%TYPE,
   phy_from_loc           ITEM_LOC.LOC%TYPE,
   phy_from_loc_type      ITEM_LOC.LOC_TYPE%TYPE,
   phy_to_loc             ITEM_LOC.LOC%TYPE,
   phy_to_loc_type        ITEM_LOC.LOC_TYPE%TYPE,
   tran_date              PERIOD.VDATE%TYPE,
   eow_date               PERIOD.VDATE%TYPE,
   franchise_ordret_ind   VARCHAR2(1),
   from_store_type        STORE.STORE_TYPE%TYPE,
   from_stockholding_ind  STORE.STOCKHOLDING_IND%TYPE,
   to_store_type          STORE.STORE_TYPE%TYPE,
   to_stockholding_ind    STORE.STOCKHOLDING_IND%TYPE,
   distros                BOL_DISTRO_ARRAY
);

TYPE bol_shipsku_rec IS RECORD(distro_no     SHIPSKU.DISTRO_NO%TYPE,
                               distro_type   SHIPSKU.DISTRO_TYPE%TYPE,
                               carton        SHIPSKU.CARTON%TYPE,
                               item          SHIPSKU.ITEM%TYPE,
                               ship_qty      SHIPSKU.QTY_EXPECTED%TYPE,
                               weight        SHIPSKU.WEIGHT_EXPECTED%TYPE,
                               weight_uom    SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE,
                               inv_status    INV_STATUS_CODES.INV_STATUS%TYPE);

TYPE bol_shipsku_tbl IS TABLE of bol_shipsku_rec index by binary_integer;

TYPE inv_flow_rec IS RECORD(
   phy_from_loc             ITEM_LOC.LOC%TYPE,
   from_loc                 ITEM_LOC.LOC%TYPE,
   from_loc_type            ITEM_LOC.LOC_TYPE%TYPE,

   phy_to_loc               ITEM_LOC.LOC%TYPE,
   to_loc                   ITEM_LOC.LOC%TYPE,
   to_loc_type              ITEM_LOC.LOC_TYPE%TYPE,

   from_receive_as_type     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
   to_receive_as_type       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
   qty                      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE
);
TYPE inv_flow_array IS TABLE of inv_flow_rec
 INDEX BY BINARY_INTEGER;

-------------------------------------------------------------------------------
FUNCTION PUT_BOL(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_bol_exists        IN OUT BOOLEAN,
                 I_bol_no            IN     SHIPMENT.BOL_NO%TYPE,
                 I_phy_from_loc      IN     SHIPMENT.FROM_LOC%TYPE,
                 I_phy_to_loc        IN     SHIPMENT.TO_LOC%TYPE,
                 I_ship_date         IN     PERIOD.VDATE%TYPE,
                 I_est_arr_date      IN     PERIOD.VDATE%TYPE,
                 I_no_boxes          IN     SHIPMENT.NO_BOXES%TYPE,
                 I_courier           IN     SHIPMENT.COURIER%TYPE,
                 I_ext_ref_no_out    IN     SHIPMENT.EXT_REF_NO_OUT%TYPE,
                 I_comments          IN     SHIPMENT.COMMENTS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PUT_TSF(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_tsfhead_to_loc   IN OUT TSFHEAD.TO_LOC%TYPE,
                 O_tsfhead_from_loc IN OUT ITEM_LOC.LOC%TYPE,
                 O_tsf_type         IN OUT TSFHEAD.TSF_TYPE%TYPE,
                 I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE,
                 I_phy_from_loc     IN     SHIPMENT.FROM_LOC%TYPE,
                 I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                 I_phy_to_loc       IN     SHIPMENT.TO_LOC%TYPE,
                 I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                 I_tran_date        IN     PERIOD.VDATE%TYPE,
                 I_comment_desc     IN     TSFHEAD.COMMENT_DESC%TYPE,
                 I_unavailable_flag IN     VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PUT_TSF_ITEM(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_detail_created   IN OUT BOOLEAN,
                      I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE,
                      I_item             IN     ITEM_MASTER.ITEM%TYPE,
                      I_carton           IN     SHIPSKU.CARTON%TYPE,
                      I_qty              IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_weight           IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                      I_weight_uom       IN     UOM_CLASS.UOM%TYPE,
                      I_inv_status       IN     INV_STATUS_CODES.INV_STATUS%TYPE,
                      I_phy_from_loc     IN     ITEM_LOC.LOC%TYPE,
                      I_from_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                      I_phy_to_loc       IN     ITEM_LOC.LOC%TYPE,
                      I_to_loc_type      IN     ITEM_LOC.LOC_TYPE%TYPE,
                      I_tsfhead_to_loc   IN     ITEM_LOC.LOC%TYPE,
                      I_tsfhead_from_loc IN     ITEM_LOC.LOC%TYPE,
                      I_tsf_type         IN     TSFHEAD.TSF_TYPE%TYPE,
                      I_called_from_form IN     VARCHAR2 default 'N',
                      I_extended_base_cost IN   ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE default NULL,
                      I_base_cost          IN   ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE default NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PROCESS_TSF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PUT_ALLOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_item                IN OUT ITEM_MASTER.ITEM%TYPE,
                   O_alloc_head_from_loc IN OUT ITEM_LOC.LOC%TYPE,
                   I_alloc_no            IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                   I_phy_from_loc        IN     ITEM_LOC.LOC%TYPE,
                   I_item                IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PUT_ALLOC_ITEM(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_detail_created      IN OUT BOOLEAN,
                        I_alloc_no            IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                        I_item                IN     ITEM_MASTER.ITEM%TYPE,
                        I_carton              IN     SHIPSKU.CARTON%TYPE,
                        I_qty                 IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        I_weight              IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                        I_weight_uom          IN     UOM_CLASS.UOM%TYPE,
                        I_inv_status          IN     INV_STATUS_CODES.INV_STATUS%TYPE,
                        I_phy_to_loc          IN     ITEM_LOC.LOC%TYPE,
                        I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                        I_alloc_head_from_loc IN     ITEM_LOC.LOC%TYPE,
                        I_extended_base_cost  IN     ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE default NULL,
                        I_base_cost           IN     ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE default NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PROCESS_ALLOC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_unexpected_flag   IN       BOOLEAN   DEFAULT FALSE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION FLUSH_BOL_PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION CARTON_EXISTS(O_error_message   IN OUT   VARCHAR2,
                       O_carton_exists   IN OUT   BOOLEAN,
                       I_carton          IN       SHIPSKU.CARTON%TYPE,
                       I_bol_no          IN       SHIPMENT.BOL_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION INIT_BOL_PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: RECEIPT_PUT_BOL
-- Purpose : To enable receiving to receive an item that was not on the shipment.
--
--           The function will populate the Global variables for the transfer
--           and shipment header level values, much like PUT_BOL.  The function
--           assumes that the BOL already exists.
--
--           The function will set the global variable LP_receipt_ind to 'Y'
--           to indicate that the subsequent functions are being called during
--           receipt processing.
-------------------------------------------------------------------------------
FUNCTION RECEIPT_PUT_BOL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_bol_no            IN       SHIPMENT.BOL_NO%TYPE,
                         I_phy_to_loc        IN       SHIPMENT.TO_LOC%TYPE,
                         I_ship_date         IN       PERIOD.VDATE%TYPE,
                         I_shipment          IN       SHIPMENT.SHIPMENT%TYPE,
                         I_phy_from_loc      IN       ITEM_LOC.LOC%TYPE,
                         I_to_loc_type       IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                         I_from_loc_type     IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                         I_tsf_no            IN       TSFHEAD.TSF_NO%TYPE,
                         I_status            IN       TSFHEAD.STATUS%TYPE,
                         I_tsf_type          IN       TSFHEAD.TSF_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : RECEIPT_PUT_TSF_ITEM
-- Purpose  : To enable receiving to receive an item that was not on the shipment.
--
--            The function will insert the item into tsfdetail with a no shipped qty.
--            It will then do normal item level processing.
-------------------------------------------------------------------------------
FUNCTION RECEIPT_PUT_TSF_ITEM(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_tsf_seq_no       IN OUT   TSFDETAIL.TSF_SEQ_NO%TYPE,
                              O_ss_seq_no        IN OUT   SHIPSKU.SEQ_NO%TYPE,
                              I_tsf_no           IN       TSFHEAD.TSF_NO%TYPE,
                              I_item             IN       ITEM_MASTER.ITEM%TYPE,
                              I_carton           IN       SHIPSKU.CARTON%TYPE,
                              I_qty              IN       TSFDETAIL.TSF_QTY%TYPE,
                              I_weight           IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_weight_uom       IN       UOM_CLASS.UOM%TYPE,
                              I_inv_status       IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                              I_phy_from_loc     IN       ITEM_LOC.LOC%TYPE,
                              I_from_loc_type    IN       ITEM_LOC.LOC_TYPE%TYPE,
                              I_phy_to_loc       IN       ITEM_LOC.LOC%TYPE,
                              I_to_loc_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                              I_tsfhead_to_loc   IN       ITEM_LOC.LOC%TYPE,
                              I_tsfhead_from_loc IN       ITEM_LOC.LOC%TYPE,
                              I_tsf_type         IN       TSFHEAD.TSF_TYPE%TYPE,
                              I_ref_item         IN       ITEM_MASTER.ITEM%TYPE,
                              I_dept             IN       ITEM_MASTER.DEPT%TYPE,
                              I_class            IN       ITEM_MASTER.CLASS%TYPE,
                              I_subclass         IN       ITEM_MASTER.SUBCLASS%TYPE,
                              I_pack_ind         IN       ITEM_MASTER.PACK_IND%TYPE,
                              I_pack_type        IN       ITEM_MASTER.PACK_TYPE%TYPE,
                              I_simple_pack_ind  IN       ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                              I_catch_weight_ind IN       ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                              I_supp_pack_size   IN       ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                              I_sellable_ind     IN       ITEM_MASTER.SELLABLE_IND%TYPE,
                              I_item_xform_ind   IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : PUT_TSF_AV_RETAIL
-- Purpose  : This function computes the transfer weighted average retail for an
--            item that is currently at or in transit to a particular finisher.
-------------------------------------------------------------------------------
FUNCTION PUT_TSF_AV_RETAIL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_shipment_retail   IN       TSFDETAIL.FINISHER_AV_RETAIL%TYPE,
                           I_shipment_qty      IN       TSFDETAIL.FINISHER_UNITS%TYPE,
                           I_tsf_no            IN       TSFDETAIL.TSF_NO%TYPE,
                           I_item              IN       TSFDETAIL.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : PUT_ILS_AV_RETAIL
-- Purpose  : This function computes the transfer weighted average retail for an
--            item that is currently at or in transit to a particular finisher
--            and updates the item_loc_soh table.
-------------------------------------------------------------------------------
FUNCTION PUT_ILS_AV_RETAIL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_location          IN       ITEM_LOC_SOH.LOC%TYPE,
                           I_loc_type          IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                           I_item              IN       TSFDETAIL.ITEM%TYPE,
                           I_shipment          IN       SHIPSKU.SHIPMENT%TYPE,
                           I_tsf_no            IN       SHIPSKU.DISTRO_NO%TYPE,
                           I_tsf_type          IN       TSFHEAD.TSF_TYPE%TYPE,
                           I_adj_qty           IN       INV_ADJ.ADJ_QTY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : PROCESS_ITEM
-- Purpose  : This function gets every orderable item in a sellable item and
--            stores it as an inventory item.
-------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_processed_item     IN OUT   BOL_SQL.BOL_SHIPSKU_TBL,
                      I_unprocessed_item   IN       BOL_SQL.BOL_SHIPSKU_TBL,
                      I_from_loc           IN       SHIPMENT.FROM_LOC%TYPE,
                      I_to_loc             IN       SHIPMENT.TO_LOC%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : GET_ALLOCDETAIL_ALLOC_QTY
-- Purpose  : This function retrieves the qty_allocated minus the qty_transfered
--            from alloc_detail table for a particular allocation number and item.
-------------------------------------------------------------------------------
FUNCTION GET_ALLOCDETAIL_ALLOC_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_alloc_qty       IN OUT   ALLOC_DETAIL.QTY_ALLOCATED%TYPE,
                                   I_alloc_no        IN       ALLOC_DETAIL.ALLOC_NO%TYPE,
                                   I_item            IN       ALLOC_HEADER.ITEM%TYPE,
                                   I_to_loc          IN       ALLOC_DETAIL.TO_LOC%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       LOCK_BOLSHIPMENT
-- Purpose:    This function will be called to lock the Bolshipment record of
--             the bol no being edited.
-------------------------------------------------------------------------------
FUNCTION LOCK_BOLSHIPMENT (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_bol_no        IN     BOL_SHIPMENT.BOL_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    VALIDATE_TSF_ALLOC_NO
-- Purpose: This function will validate the entered distro number (transfer or allocation no)
--          against the entered from and to locations.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_ALLOC_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid         IN OUT BOOLEAN,
                               I_distro        IN     BOL_SHIPSKU.DISTRO_NO%TYPE,
                               I_distro_type   IN     BOL_SHIPSKU.DISTRO_TYPE%TYPE,
                               I_from_loc      IN     BOL_SHIPMENT.FROM_LOC%TYPE,
                               I_from_loc_type IN     BOL_SHIPMENT.FROM_LOC_TYPE%TYPE,
                               I_to_loc        IN     BOL_SHIPMENT.TO_LOC%TYPE,
                               I_to_loc_type   IN     BOL_SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Name:    CHECK_ITEM_SELECTED
-- Purpose: This function will determine whether an item has been selected for a
--          distro number in the bol_distro_items_temp table.
----------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_SELECTED(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_item_selected   IN OUT   BOOLEAN,
                             I_bol_no          IN       BOL_SHIPMENT.BOL_NO%TYPE,
                             I_distro_no       IN       BOL_DISTRO_ITEMS_TEMP.DISTRO_NO%TYPE,
                             I_distro_type     IN       BOL_DISTRO_ITEMS_TEMP.DISTRO_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Name:    BOL_SHIPSKU_EXISTS
-- Purpose: This function will determine whether a bol_shipsku record exists for a
--          given bol, item and distro number
----------------------------------------------------------------------------------
FUNCTION BOL_SHIPSKU_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists        IN OUT BOOLEAN,
                            I_bol_no        IN     BOL_SHIPSKU.BOL_NO%TYPE,
                            I_distro_no     IN     BOL_SHIPSKU.DISTRO_NO%TYPE,
                            I_distro_type   IN     BOL_SHIPSKU.DISTRO_TYPE%TYPE,
                            I_item          IN     BOL_SHIPSKU.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then
   FUNCTION CREATE_TSF(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_no            IN     TSFHEAD.TSF_NO%TYPE,
                       I_tsf_type          IN     TSFHEAD.TSF_TYPE%TYPE,
                       I_phy_from_loc      IN     ITEM_LOC.LOC%TYPE,
                       I_from_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_phy_to_loc        IN     ITEM_LOC.LOC%TYPE,
                       I_to_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_tran_date         IN     PERIOD.VDATE%TYPE,
                       I_comment_desc      IN     TSFHEAD.COMMENT_DESC%TYPE,
                       I_unavailable_flag  IN     VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION NEW_LOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item           IN     ITEM_MASTER.ITEM%TYPE,
                    I_pack_ind       IN     ITEM_MASTER.PACK_IND%TYPE,
                    I_dept           IN     ITEM_MASTER.DEPT%TYPE,
                    I_class          IN     ITEM_MASTER.CLASS%TYPE,
                    I_subclass       IN     ITEM_MASTER.SUBCLASS%TYPE,
                    I_loc            IN     ITEM_LOC.LOC%TYPE,
                    I_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION ITEM_CHECK(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tran_item        IN OUT ITEM_MASTER.ITEM%TYPE,
                       O_ref_item         IN OUT ITEM_MASTER.ITEM%TYPE,
                       O_dept             IN OUT ITEM_MASTER.DEPT%TYPE,
                       O_class            IN OUT ITEM_MASTER.CLASS%TYPE,
                       O_subclass         IN OUT ITEM_MASTER.SUBCLASS%TYPE,
                       O_pack_ind         IN OUT ITEM_MASTER.PACK_IND%TYPE,
                       O_pack_type        IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                       O_simple_pack_ind  IN OUT ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                       O_catch_weight_ind IN OUT ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                       O_sellable_ind     IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                       O_item_xform_ind   IN OUT ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                       O_supp_pack_size   IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                       I_input_item       IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION INS_TSFDETAIL(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_tsf_seq_no        IN OUT TSFDETAIL.TSF_SEQ_NO%TYPE,
                          I_tsf_no            IN     TSFHEAD.TSF_NO%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_supp_pack_size    IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                          I_inv_status        IN     TSFDETAIL.INV_STATUS%TYPE,
                          I_tsf_qty           IN     TSFDETAIL.TSF_QTY%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION DIST_FROM_LOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_inv_flow_array      IN OUT BOL_SQL.INV_FLOW_ARRAY,
                          I_tsf_no              IN     TSFHEAD.TSF_NO%TYPE,
                          I_tsf_seq_no          IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                          I_item                IN     ITEM_MASTER.ITEM%TYPE,
                          I_inv_status          IN     SHIPSKU.INV_STATUS%TYPE,
                          I_tsf_qty             IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_phy_to_loc          IN     ITEM_LOC.LOC%TYPE,
                          I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                          I_phy_from_loc        IN     ITEM_LOC.LOC%TYPE,
                          I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                          I_dist_type           IN     VARCHAR2,
                          I_cust_order_loc_ind  IN     VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION CREATE_SHIP_INV_MAP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE,
                               I_item           IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_ind       IN     ITEM_MASTER.PACK_IND%TYPE,
                               I_inv_status     IN     SHIPSKU.INV_STATUS%TYPE,
                               I_tsf_seq_no     IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                               I_phy_from_loc   IN     ITEM_LOC.LOC%TYPE,
                               I_from_loc_type  IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_phy_to_loc     IN     ITEM_LOC.LOC%TYPE,
                               I_to_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_tsf_qty        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION INS_TSF_INV_FLOW(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION INS_SHIP_INV_FLOW(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION PUT_TSF_INV_MAP(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_from_loc        IN     ITEM_LOC.LOC%TYPE,
                            I_from_loc_type   IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_to_loc          IN     ITEM_LOC.LOC%TYPE,
                            I_to_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_receive_as_type IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                            I_tsf_qty         IN     TSFDETAIL.TSF_QTY%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION SEND_TSF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bol_no        IN     SHIPMENT.BOL_NO%TYPE,
                     I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                     I_tsf_type      IN     TSFHEAD.TSF_TYPE%TYPE,
                     I_new_tsf       IN     VARCHAR2,
                     I_tsf_to_loc    IN     ITEM_LOC.LOC%TYPE,
                     I_tsf_status    IN     TSFHEAD.STATUS%TYPE,
                     I_tran_date     IN     PERIOD.VDATE%TYPE,
                     I_ship_no       IN     SHIPMENT.SHIPMENT%TYPE,
                     I_eow_date      IN     PERIOD.VDATE%TYPE,
                     I_bol_items     IN     BOL_SQL.BOL_ITEM_ARRAY)
   return BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION AUTO_RCV_LINE_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   --------------------------------------------------------------------------------------
   --  Name   : GET_COSTS_AND_RETAILS
   --  Purpose: The function will get the costs for the from loc.  If the from loc is an
   --           external finisher the retail will be retreived for the to location.  Otherwise,
   --           the from location's retail will be returned.
   --------------------------------------------------------------------------------------
   FUNCTION GET_COSTS_AND_RETAILS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_av_cost               IN OUT ITEM_LOC_SOH.AV_COST%TYPE,
                                  O_unit_retail           IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                                  I_pack_ind              IN     VARCHAR2,
                                  I_sellable_ind          IN     ITEM_MASTER.SELLABLE_IND%TYPE,
                                  I_item_xform_ind        IN     ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                                  I_from_loc              IN     ITEM_LOC.LOC%TYPE,
                                  I_from_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                                  I_to_loc                IN     ITEM_LOC.LOC%TYPE,
                                  I_to_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION SEND_ALLOC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_no             IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                       I_alloc_status         IN       ALLOC_HEADER.STATUS%TYPE,
                       I_alloc_type           IN       VARCHAR2,
                       I_new_ad_ind           IN       VARCHAR2,
                       I_ship_no              IN       SHIPMENT.SHIPMENT%TYPE,
                       I_ss_seq_no            IN       SHIPSKU.SEQ_NO%TYPE,
                       I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                       I_ref_item             IN       ITEM_MASTER.ITEM%TYPE,
                       I_pack_ind             IN       ITEM_MASTER.PACK_IND%TYPE,
                       I_pack_type            IN       ITEM_MASTER.PACK_TYPE%TYPE,
                       I_dept                 IN       ITEM_MASTER.DEPT%TYPE,
                       I_class                IN       ITEM_MASTER.CLASS%TYPE,
                       I_subclass             IN       ITEM_MASTER.SUBCLASS%TYPE,
                       I_carton               IN       SHIPSKU.CARTON%TYPE,
                       I_inv_status           IN       SHIPSKU.INV_STATUS%TYPE,
                       I_ship_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_qty                  IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_simple_pack_ind      IN       ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                       I_catch_weight_ind     IN       ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                       I_ship_weight          IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                       I_weight               IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                       I_weight_uom           IN       UOM_CLASS.UOM%TYPE,
                       I_sellable_ind         IN       ITEM_MASTER.SELLABLE_IND%TYPE,
                       I_item_xform_ind       IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                       I_ad_alloc_qty         IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_ad_tsf_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_from_loc             IN       ITEM_LOC.LOC%TYPE,
                       I_from_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                       I_to_loc               IN       ITEM_LOC.LOC%TYPE,
                       I_to_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                       I_tran_date            IN       PERIOD.VDATE%TYPE,
                       I_eow_date             IN       PERIOD.VDATE%TYPE,
                       I_extended_base_cost   IN       ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE default NULL,
                       I_base_cost            IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE default NULL,
                       I_unexpected_flag      IN       BOOLEAN   DEFAULT FALSE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION INS_SHIPSKU(O_error_message  IN OUT VARCHAR2,
                        I_shipment       IN     SHIPMENT.SHIPMENT%TYPE,
                        I_seq_no         IN     SHIPSKU.SEQ_NO%TYPE,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE,
                        I_ref_item       IN     ITEM_MASTER.ITEM%TYPE,
                        I_distro_no      IN     SHIPSKU.DISTRO_NO%TYPE,
                        I_distro_type    IN     SHIPSKU.DISTRO_TYPE%TYPE,
                        I_carton         IN     SHIPSKU.CARTON%TYPE,
                        I_inv_status     IN     SHIPSKU.INV_STATUS%TYPE,
                        I_rcv_qty        IN     SHIPSKU.QTY_EXPECTED%TYPE,
                        I_cost           IN     SHIPSKU.UNIT_COST%TYPE,
                        I_retail         IN     SHIPSKU.UNIT_RETAIL%TYPE,
                        I_exp_qty        IN     SHIPSKU.QTY_EXPECTED%TYPE,
                        I_exp_weight     IN     SHIPSKU.WEIGHT_EXPECTED%TYPE,
                        I_exp_weight_uom IN     SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE,
                        I_adjust_type    IN     shipsku.adjust_type%TYPE default NULL)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION DEPT_LVL_CHK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_dept            IN     DEPS.DEPT%TYPE,
                         I_tsf_no          IN     TSFHEAD.TSF_NO%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION UPDATE_INV_STATUS(O_error_message IN OUT VARCHAR2,
                              I_item          IN     ITEM_MASTER.ITEM%TYPE,
                              I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                              I_from_loc      IN     ITEM_LOC.LOC%TYPE,
                              I_from_loc_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                              I_qty           IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_inv_status    IN     SHIPSKU.INV_STATUS%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION UPD_FROM_ITEM_LOC(O_error_message    IN OUT VARCHAR2,
                              O_from_av_cost     IN OUT ITEM_LOC_SOH.AV_COST%TYPE,
                              O_from_unit_retail IN OUT ITEM_LOC_SOH.AV_COST%TYPE,
                              I_item             IN     ITEM_MASTER.ITEM%TYPE,
                              I_comp_item        IN     VARCHAR2,
                              I_sellable_ind     IN     ITEM_MASTER.SELLABLE_IND%TYPE,
                              I_item_xform_ind   IN     ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                              I_inventory_ind    IN     ITEM_MASTER.INVENTORY_IND%TYPE,
                              I_from_loc         IN     ITEM_LOC.LOC%TYPE,
                              I_tsf_qty          IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_resv_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_tsf_weight       IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_tsf_weight_uom   IN     UOM_CLASS.UOM%TYPE,
                              I_tran_date        IN     PERIOD.VDATE%TYPE,
                              I_eow_date         IN     PERIOD.VDATE%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION UPD_TO_ITEM_LOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item            IN     ITEM_MASTER.ITEM%TYPE,
                            I_pack_no         IN     ITEM_MASTER.ITEM%TYPE,
                            I_percent_in_pack IN     NUMBER,
                            I_receive_as_type IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                            I_tsf_type        IN     TSFHEAD.TSF_TYPE%TYPE,
                            I_to_loc          IN     ITEM_LOC.LOC%TYPE,
                            I_to_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_tsf_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            I_exp_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            I_intran_qty      IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            I_weight_cuom     IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                            I_cuom            IN     UOM_CLASS.UOM%TYPE,
                            I_from_loc        IN     ITEM_LOC.LOC%TYPE,
                            I_from_loc_type   IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_prim_charge     IN     ITEM_LOC_SOH.AV_COST%TYPE,
                            I_distro_no       IN     SHIPSKU.DISTRO_NO%TYPE,
                            I_distro_type     IN     SHIPSKU.DISTRO_TYPE%TYPE,
                            I_intercompany    IN     BOOLEAN)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION UPDATE_PACK_LOCS(O_error_message       IN OUT VARCHAR2,
                             I_item                IN     ITEM_MASTER.ITEM%TYPE,
                             I_from_loc            IN     ITEM_LOC.LOC%TYPE,
                             I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_to_loc              IN     ITEM_LOC.LOC%TYPE,
                             I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_to_receive_as_type  IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                             I_tsf_type            IN     TSFHEAD.TSF_TYPE%TYPE,
                             I_tsf_qty             IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_tsf_weight_cuom     IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_intran_qty          IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_intran_weight_cuom  IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_resv_exp_qty        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_tran_date           IN     PERIOD.VDATE%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION UPD_TSF_ITEM_COST(O_error_message    IN OUT VARCHAR2,
                              I_item             IN     ITEM_MASTER.ITEM%TYPE,
                              I_ship_qty         IN     TSF_ITEM_COST.SHIPPED_QTY%TYPE,
                              I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION WRITE_ISSUES(O_error_message   IN OUT VARCHAR2,
                         I_item            IN     ITEM_MASTER.ITEM%TYPE,
                         I_from_wh         IN     ITEM_LOC.LOC%TYPE,
                         I_transferred_qty IN     TSFDETAIL.TSF_QTY%TYPE,
                         I_eow_date        IN     PERIOD.VDATE%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION NEXT_SS_SEQ_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_ss_seq_no     IN OUT SHIPSKU.SEQ_NO%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   --BULK HELPERS

   FUNCTION FLUSH_SHIPSKU_INSERT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION FLUSH_ILH_INSERT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION FLUSH_ILH_UPDATE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------------
   FUNCTION TSF_ITEM_COMMON(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE,
                            I_carton           IN     SHIPSKU.CARTON%TYPE,
                            I_qty              IN     TSFDETAIL.TSF_QTY%TYPE,
                            I_weight           IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                            I_weight_uom       IN     UOM_CLASS.UOM%TYPE,
                            I_phy_from_loc     IN     ITEM_LOC.LOC%TYPE,
                            I_from_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_phy_to_loc       IN     ITEM_LOC.LOC%TYPE,
                            I_to_loc_type      IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_tsfhead_to_loc   IN     ITEM_LOC.LOC%TYPE,
                            I_tsfhead_from_loc IN     ITEM_LOC.LOC%TYPE,
                            I_tsf_type         IN     TSFHEAD.TSF_TYPE%TYPE,
                            I_inv_status       IN     INV_STATUS_CODES.INV_STATUS%TYPE,
                            I_tsf_qty          IN     TSFDETAIL.TSF_QTY%TYPE,
                            I_ship_qty         IN     TSFDETAIL.SHIP_QTY%TYPE,
                            I_tsf_seq_no       IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                            I_item_cnt         IN     BINARY_INTEGER,
                            I_dist_cnt         IN     BINARY_INTEGER)
   RETURN BOOLEAN;
$end
----------------------------------------------------------------------------------------
FUNCTION RECEIPT_PUT_ALLOC_BOL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_bol_no          IN       SHIPMENT.BOL_NO%TYPE,
                               I_phy_to_loc      IN       SHIPMENT.TO_LOC%TYPE,
                               I_ship_date       IN       PERIOD.VDATE%TYPE,
                               I_shipment        IN       SHIPMENT.SHIPMENT%TYPE,
                               I_phy_from_loc    IN       ITEM_LOC.LOC%TYPE,
                               I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                               I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                               I_alloc_no        IN       TSFHEAD.TSF_NO%TYPE,
                               I_status          IN       TSFHEAD.STATUS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION UPDATE_SHIP_SEQ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
FUNCTION POPULATE_SHIPSKU(O_error_message IN OUT rtk_errors.rtk_text%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
END;
/
