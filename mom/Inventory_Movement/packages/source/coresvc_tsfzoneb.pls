CREATE OR REPLACE PACKAGE BODY CORESVC_TSFZONE AS
   cursor C_SVC_TSFZONE(I_process_id NUMBER,
                        I_chunk_id NUMBER) is
      select pk_tsfzone.rowid     as pk_tsfzone_rid,
             st.rowid as st_rid,
             st.description,
             st.transfer_zone,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)     as action,
             st.process$status
        from svc_tsfzone st,
             tsfzone pk_tsfzone,
             dual
       where st.process_id            = I_process_id
         and st.chunk_id              = I_chunk_id
         and st.transfer_zone         = pk_tsfzone.transfer_zone (+);
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type TSFZONE_TAB IS TABLE OF TSFZONE_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name        IN       VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets        s9t_pkg.names_map_typ;
   tsfzone_cols    s9t_pkg.names_map_typ;
   tsfzone_tl_cols s9t_pkg.names_map_typ;
BEGIN

   L_sheets               := s9t_pkg.get_sheet_names(I_file_id);
   tsfzone_cols           := s9t_pkg.get_col_names(I_file_id,TSFZONE_sheet);
   TSFZONE$Action         := tsfzone_cols('ACTION');
   TSFZONE$DESCRIPTION    := tsfzone_cols('DESCRIPTION');
   TSFZONE$TRANSFER_ZONE  := tsfzone_cols('TRANSFER_ZONE');

   tsfzone_TL_cols           := s9t_pkg.get_col_names(I_file_id,TSFZONE_TL_sheet);
   TSFZONE_TL$Action         := tsfzone_tl_cols('ACTION');
   TSFZONE_TL$LANG           := tsfzone_tl_cols('LANG');
   TSFZONE_TL$TRANSFER_ZONE  := tsfzone_tl_cols('TRANSFER_ZONE');
   TSFZONE_TL$DESCRIPTION    := tsfzone_tl_cols('DESCRIPTION');
END POPULATE_NAMES;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_TSFZONE( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = TSFZONE_sheet )
   select s9t_row(s9t_cells(CORESVC_TSFZONE.action_mod ,
                            transfer_zone,
                            description))
     from tsfzone ;
END POPULATE_TSFZONE;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_TSFZONE_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = TSFZONE_TL_SHEET )
   select s9t_row(s9t_cells(CORESVC_TSFZONE.action_mod ,
                            lang,
                            transfer_zone,
                            description))
     from tsfzone_tl ;
END POPULATE_TSFZONE_TL;
------------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(TSFZONE_sheet);
   L_file.sheets(l_file.get_sheet_index(TSFZONE_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                     'TRANSFER_ZONE',
                                                                                     'DESCRIPTION');

   L_file.add_sheet(TSFZONE_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(TSFZONE_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                       'LANG',
                                                                                       'TRANSFER_ZONE',
                                                                                       'DESCRIPTION');
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_TSFZONE.CREATE_S9T';
   L_file    s9t_file;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key)=FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_TSFZONE(O_file_id);
      POPULATE_TSFZONE_TL(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file)=FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TSFZONE( I_file_id     IN   S9T_FOLDER.FILE_ID%TYPE,
                               I_process_id  IN   SVC_TSFZONE.PROCESS_ID%TYPE) IS
   TYPE svc_tsfzone_col_typ IS TABLE OF SVC_TSFZONE%ROWTYPE;
   L_temp_rec      SVC_TSFZONE%ROWTYPE;
   svc_tsfzone_col svc_tsfzone_col_typ :=NEW svc_tsfzone_col_typ();
   L_process_id    SVC_TSFZONE.process_id%TYPE;
   L_error         BOOLEAN:=FALSE;
   L_default_rec   SVC_TSFZONE%ROWTYPE;
   cursor C_MANDATORY_IND is
      select
             description_mi,
             transfer_zone_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'TSFZONE'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('DESCRIPTION'   as description,
                                            'TRANSFER_ZONE' as transfer_zone,
                                             NULL           as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_TSFZONE';
   L_pk_columns    VARCHAR2(255)  := 'Transfer Zone';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  description_dv,
                       transfer_zone_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'TSFZONE'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('DESCRIPTION'   as description,
                                                     'TRANSFER_ZONE' as transfer_zone,
                                                     NULL            as dummy)))
   LOOP
      BEGIN
         L_default_rec.description := rec.description_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TSFZONE ' ,
                            NULL,
                           'DESCRIPTION ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.transfer_zone := rec.transfer_zone_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TSFZONE ' ,
                            NULL,
                           'TRANSFER_ZONE ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(TSFZONE$ACTION)                   as action,
          r.get_cell(TSFZONE$DESCRIPTION)              as description,
          r.get_cell(TSFZONE$TRANSFER_ZONE)            as transfer_zone,
          r.get_row_seq()                              as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(TSFZONE_sheet)
  )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSFZONE_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.description := rec.description;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSFZONE_sheet,
                            rec.row_seq,
                            'DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.transfer_zone := rec.transfer_zone;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSFZONE_sheet,
                            rec.row_seq,
                            'TRANSFER_ZONE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TSFZONE.action_new then
         L_temp_rec.description   := NVL( L_temp_rec.description,L_default_rec.description);
         L_temp_rec.transfer_zone := NVL( L_temp_rec.transfer_zone,L_default_rec.transfer_zone);
      end if;
      if not (L_temp_rec.transfer_zone is NOT NULL and
              1 = 1)then
         WRITE_S9T_ERROR(I_file_id,
                         TSFZONE_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_tsfzone_col.extend();
         svc_tsfzone_col(svc_tsfzone_col.COUNT()):= L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      forall i IN 1..svc_tsfzone_col.COUNT SAVE EXCEPTIONS
      merge into SVC_TSFZONE st
      using(select(case
                   when L_mi_rec.description_mi    = 'N'
                    and svc_tsfzone_col(i).action = CORESVC_TSFZONE.action_mod
                    and s1.description IS NULL
                   then mt.description
                   else s1.description
                   end) as description,
                  (case
                   when L_mi_rec.transfer_zone_mi    = 'N'
                    and svc_tsfzone_col(i).action = CORESVC_TSFZONE.action_mod
                    and s1.transfer_zone IS NULL
                   then mt.transfer_zone
                   else s1.transfer_zone
                   end) as transfer_zone,
                  null as dummy
              from (select svc_tsfzone_col(i).description as description,
                           svc_tsfzone_col(i).transfer_zone as transfer_zone,
                           null as dummy
                      from dual ) s1,
                           TSFZONE mt
             where mt.transfer_zone (+)     = s1.transfer_zone   and
                  1 = 1 )sq
                on (st.transfer_zone      = sq.transfer_zone and
                    svc_tsfzone_col(i).ACTION IN (CORESVC_TSFZONE.action_mod,CORESVC_TSFZONE.action_del))
      when matched then
      update
         set process_id      = svc_tsfzone_col(i).process_id ,
             chunk_id        = svc_tsfzone_col(i).chunk_id ,
             row_seq         = svc_tsfzone_col(i).row_seq ,
             action          = svc_tsfzone_col(i).action ,
             process$status  = svc_tsfzone_col(i).process$status ,
             description     = sq.description ,
             create_id       = svc_tsfzone_col(i).create_id ,
             create_datetime = svc_tsfzone_col(i).create_datetime ,
             last_upd_id     = svc_tsfzone_col(i).last_upd_id ,
             last_upd_datetime = svc_tsfzone_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             description ,
             transfer_zone ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_tsfzone_col(i).process_id ,
             svc_tsfzone_col(i).chunk_id ,
             svc_tsfzone_col(i).row_seq ,
             svc_tsfzone_col(i).action ,
             svc_tsfzone_col(i).process$status ,
             sq.description ,
             sq.transfer_zone ,
             svc_tsfzone_col(i).create_id ,
             svc_tsfzone_col(i).create_datetime ,
             svc_tsfzone_col(i).last_upd_id ,
             svc_tsfzone_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             TSFZONE_sheet,
                             svc_tsfzone_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TSFZONE;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TSFZONE_TL( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id   IN   SVC_TSFZONE_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_TSFZONE_TL_COL_TYP IS TABLE OF SVC_TSFZONE_TL%ROWTYPE;
   L_temp_rec           SVC_TSFZONE_TL%ROWTYPE;
   svc_tsfzone_TL_col   SVC_TSFZONE_TL_COL_TYP := NEW SVC_TSFZONE_TL_COL_TYP();
   L_process_id         SVC_TSFZONE_TL.PROCESS_ID%TYPE;
   L_error              BOOLEAN := FALSE;
   L_default_rec        SVC_TSFZONE_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select description_mi,
             transfer_zone_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'TSFZONE_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('DESCRIPTION' as description,
                                       'TRANSFER_ZONE'        as transfer_zone,
                                       'LANG'              as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_TSFZONE_TL';
   L_pk_columns    VARCHAR2(255)  := 'Transfer Zone, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select description_dv,
                      transfer_zone_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'TSFZONE_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('DESCRIPTION'   as description,
                                                'TRANSFER_ZONE' as transfer_zone,
                                                'LANG'          as lang)))
   LOOP
      BEGIN
         L_default_rec.description := rec.description_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSFZONE_TL_SHEET ,
                            NULL,
                           'DESCRIPTION' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.transfer_zone := rec.transfer_zone_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           TSFZONE_TL_SHEET ,
                            NULL,
                           'TRANSFER_ZONE' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSFZONE_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(tsfzone_tl$action))    as action,
                      r.get_cell(tsfzone_tl$description)      as description,
                      r.get_cell(tsfzone_tl$transfer_zone)    as transfer_zone,
                      r.get_cell(tsfzone_tl$lang)             as lang,
                      r.get_row_seq()                         as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(TSFZONE_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            tsfzone_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.description := rec.description;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSFZONE_TL_SHEET,
                            rec.row_seq,
                            'DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.transfer_zone := rec.transfer_zone;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSFZONE_TL_SHEET,
                            rec.row_seq,
                            'TRANSFER_ZONE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSFZONE_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TSFZONE.action_new then
         L_temp_rec.description      := NVL( L_temp_rec.description,L_default_rec.description);
         L_temp_rec.transfer_zone    := NVL( L_temp_rec.transfer_zone,L_default_rec.transfer_zone);
         L_temp_rec.lang             := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.transfer_zone is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         TSFZONE_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_tsfzone_TL_col.extend();
         svc_tsfzone_TL_col(svc_tsfzone_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_tsfzone_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_tsfzone_TL st
      using(select
                  (case
                   when l_mi_rec.description_mi = 'N'
                    and svc_tsfzone_TL_col(i).action = CORESVC_TSFZONE.action_mod
                    and s1.description IS NULL then
                        mt.description
                   else s1.description
                   end) as description,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_tsfzone_TL_col(i).action = CORESVC_TSFZONE.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.transfer_zone_mi = 'N'
                    and svc_tsfzone_TL_col(i).action = CORESVC_TSFZONE.action_mod
                    and s1.transfer_zone IS NULL then
                        mt.transfer_zone
                   else s1.transfer_zone
                   end) as transfer_zone
              from (select svc_tsfzone_TL_col(i).description as description,
                           svc_tsfzone_TL_col(i).transfer_zone        as transfer_zone,
                           svc_tsfzone_TL_col(i).lang              as lang
                      from dual) s1,
                   tsfzone_TL mt
             where mt.transfer_zone (+) = s1.transfer_zone
               and mt.lang (+)       = s1.lang) sq
                on (st.transfer_zone = sq.transfer_zone and
                    st.lang = sq.lang and
                    svc_tsfzone_TL_col(i).ACTION IN (CORESVC_TSFZONE.action_mod,CORESVC_TSFZONE.action_del))
      when matched then
      update
         set process_id        = svc_tsfzone_TL_col(i).process_id ,
             chunk_id          = svc_tsfzone_TL_col(i).chunk_id ,
             row_seq           = svc_tsfzone_TL_col(i).row_seq ,
             action            = svc_tsfzone_TL_col(i).action ,
             process$status    = svc_tsfzone_TL_col(i).process$status ,
             description = sq.description
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             description ,
             transfer_zone ,
             lang)
      values(svc_tsfzone_TL_col(i).process_id ,
             svc_tsfzone_TL_col(i).chunk_id ,
             svc_tsfzone_TL_col(i).row_seq ,
             svc_tsfzone_TL_col(i).action ,
             svc_tsfzone_TL_col(i).process$status ,
             sq.description ,
             sq.transfer_zone ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            TSFZONE_TL_SHEET,
                            svc_tsfzone_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TSFZONE_TL;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count     IN OUT   NUMBER,
                      I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_TSFZONE.PROCESS_S9T';
   L_file           s9t_file;
   L_sheets         s9t_pkg.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR     EXCEPTION;
   PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE)=FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_TSFZONE(I_file_id,I_process_id);
      PROCESS_S9T_TSFZONE_TL(I_file_id,I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
      insert into s9t_errors
         values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_TSFZONE_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error           IN OUT   BOOLEAN,
                             I_rec             IN       C_SVC_TSFZONE%ROWTYPE)

RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_TSFZONE.PROCESS_TSFZONE_VAL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSFZONE';
   L_store_exists VARCHAR2(1)                       := NULL;

   cursor C_STORE_EXISTS(L_transfer_zone NUMBER) is
      select 'Y'
        from store
       where transfer_zone = L_transfer_zone;
BEGIN
   if I_rec.action = action_new
      and I_rec.transfer_zone is NOT NULL
      and I_rec.transfer_zone<=0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                    'TRANSFER_ZONE',
                    'GREATER_0');
         O_error := TRUE;
   end if;
   if I_rec.action = action_del then
      open  C_STORE_EXISTS(I_rec.transfer_zone);
      fetch C_STORE_EXISTS into L_store_exists;
      close C_STORE_EXISTS;
      if L_store_exists is NOT NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                    'TRANSFER_ZONE',
                    'CANNOT_DELETE_TRA_ZN');
         O_error := TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_STORE_EXISTS%ISOPEN then
         close C_STORE_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TSFZONE_VAL;
----------------------------------------------------------------------------
FUNCTION EXEC_TSFZONE_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsfzone_temp_rec    IN       TSFZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_TSFZONE.EXEC_TSFZONE_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSFZONE';
BEGIN
   insert
     into tsfzone
   values I_tsfzone_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TSFZONE_INS;
------------------------------------------------------------------------------------------------
FUNCTION EXEC_TSFZONE_UPD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsfzone_temp_rec    IN       TSFZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_TSFZONE.EXEC_TSFZONE_UPD';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSFZONE';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   cursor C_TSFZONE_LOCK is
      select 'X'
        from tsfzone
       where transfer_zone = I_tsfzone_temp_rec.transfer_zone
         for update nowait;
BEGIN
   open C_TSFZONE_LOCK;
   close C_TSFZONE_LOCK;
   update tsfzone
      set row = I_tsfzone_temp_rec
    where 1 = 1
      and transfer_zone = I_tsfzone_temp_rec.transfer_zone;
   return TRUE;
EXCEPTION
    when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_tsfzone_temp_rec.transfer_zone,
                                             NULL);
       return FALSE;
    when OTHERS then
      if C_TSFZONE_LOCK%ISOPEN then
         close C_TSFZONE_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TSFZONE_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_TSFZONE_DEL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsfzone_temp_rec    IN       TSFZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_TSFZONE.EXEC_TSFZONE_UPD';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSFZONE';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_TSFZONE_TL_LOCK is
      select 'X'
        from tsfzone_tl
       where transfer_zone = I_tsfzone_temp_rec.transfer_zone
         for update nowait;

   cursor C_TSFZONE_LOCK is
      select 'X'
        from tsfzone
       where transfer_zone = I_tsfzone_temp_rec.transfer_zone
         for update nowait;
BEGIN
   open C_TSFZONE_TL_LOCK;
   close C_TSFZONE_TL_LOCK;

   delete
     from tsfzone_tl
    where transfer_zone = I_tsfzone_temp_rec.transfer_zone;

   open C_TSFZONE_LOCK;
   close C_TSFZONE_LOCK;

   delete
     from tsfzone
    where transfer_zone = I_tsfzone_temp_rec.transfer_zone;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_tsfzone_temp_rec.transfer_zone,
                                             NULL);
      return FALSE;
   when OTHERS then
     if C_TSFZONE_LOCK%ISOPEN then
        close C_TSFZONE_LOCK;
     end if;
     if C_TSFZONE_TL_LOCK%ISOPEN then
        close C_TSFZONE_TL_LOCK;
     end if;
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TSFZONE_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TSFZONE_TL_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsfzone_tl_ins_tab    IN       TSFZONE_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TSFZONE.EXEC_TSFZONE_TL_INS';

BEGIN
   if I_tsfzone_tl_ins_tab is NOT NULL and I_tsfzone_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_tsfzone_tl_ins_tab.COUNT()
         insert into tsfzone_TL
              values I_tsfzone_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_TSFZONE_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TSFZONE_TL_UPD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsfzone_tl_upd_tab   IN       TSFZONE_TAB,
                             I_tsfzone_tl_upd_rst   IN       ROW_SEQ_TAB,
                             I_process_id           IN       SVC_TSFZONE_TL.PROCESS_ID%TYPE,
                             I_chunk_id             IN       SVC_TSFZONE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_TSFZONE.EXEC_TSFZONE_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TSFZONE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_TSFZONE_TL_UPD(I_tsfzone  TSFZONE_TL.transfer_zone%TYPE,
                                I_lang     TSFZONE_TL.LANG%TYPE) is
      select 'x'
        from tsfzone_tl
       where transfer_zone = I_tsfzone
         and lang = I_lang
         for update nowait;

BEGIN
   if I_tsfzone_tl_upd_tab is NOT NULL and I_tsfzone_tl_upd_tab.count > 0 then
      for i in I_tsfzone_tl_upd_tab.FIRST..I_tsfzone_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_tsfzone_tl_upd_tab(i).lang);
            L_key_val2 := 'Transfer Zone: '||to_char(I_tsfzone_tl_upd_tab(i).transfer_zone);
            open C_LOCK_TSFZONE_TL_UPD(I_tsfzone_tl_upd_tab(i).transfer_zone,
                                       I_tsfzone_tl_upd_tab(i).lang);
            close C_LOCK_TSFZONE_TL_UPD;
            
            update tsfzone_TL
               set description = I_tsfzone_tl_upd_tab(i).description,
                   last_update_id = I_tsfzone_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_tsfzone_tl_upd_tab(i).last_update_datetime
             where lang = I_tsfzone_tl_upd_tab(i).lang
               and transfer_zone = I_tsfzone_tl_upd_tab(i).transfer_zone;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_TSFZONE_TL',
                           I_tsfzone_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_TSFZONE_TL_UPD%ISOPEN then
         close C_LOCK_TSFZONE_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TSFZONE_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TSFZONE_TL_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsfzone_tl_del_tab   IN       TSFZONE_TAB,
                             I_tsfzone_tl_del_rst   IN       ROW_SEQ_TAB,
                             I_process_id           IN       SVC_TSFZONE_TL.PROCESS_ID%TYPE,
                             I_chunk_id             IN       SVC_TSFZONE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_TSFZONE.EXEC_TSFZONE_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TSFZONE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_TSFZONE_TL_DEL(I_transfer_zone  TSFZONE_TL.transfer_zone%TYPE,
                                I_lang           TSFZONE_TL.LANG%TYPE) is
      select 'x'
        from tsfzone_tl
       where transfer_zone = I_transfer_zone
         and lang = I_lang
         for update nowait;

BEGIN
   if I_tsfzone_tl_del_tab is NOT NULL and I_tsfzone_tl_del_tab.count > 0 then
      for i in I_tsfzone_tl_del_tab.FIRST..I_tsfzone_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_tsfzone_tl_del_tab(i).lang);
            L_key_val2 := 'Transfer Zone: '||to_char(I_tsfzone_tl_del_tab(i).transfer_zone);
            open C_LOCK_TSFZONE_TL_DEL(I_tsfzone_tl_del_tab(i).transfer_zone,
                                       I_tsfzone_tl_del_tab(i).lang);
            close C_LOCK_TSFZONE_TL_DEL;
           
            delete tsfzone_tl
             where lang = I_tsfzone_tl_del_tab(i).lang
               and transfer_zone = I_tsfzone_tl_del_tab(i).transfer_zone;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_TSFZONE_TL',
                           I_tsfzone_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_TSFZONE_TL_DEL%ISOPEN then
         close C_LOCK_TSFZONE_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TSFZONE_TL_DEL;
------------------------------------------------------------------------------------------
FUNCTION PROCESS_TSFZONE( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id    IN     SVC_TSFZONE.PROCESS_ID%TYPE,
                          I_chunk_id      IN     SVC_TSFZONE.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_TSFZONE.PROCESS_TSFZONE';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSFZONE';
   L_error            BOOLEAN;
   L_process_error    BOOLEAN                           := FALSE;
   L_tsfzone_temp_rec TSFZONE%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_TSFZONE(I_process_id,
                            I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.pk_tsfzone_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'TRANSFER_ZONE',
                    'PK_TSFZONE');
         L_error := TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.pk_tsfzone_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'TRANSFER_ZONE',
                    'INV_TRAN_ZONE');
         L_error := TRUE;
      end if;
      if rec.description  IS NULL
         and rec.action in (action_new,action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'DESCRIPTION',
                    'ENT_TRAN_ZN_DESC');
         L_error := TRUE;
      end if;
      if PROCESS_TSFZONE_VAL(O_error_message,
                             L_error,
                             rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;
      if NOT L_error then
         L_tsfzone_temp_rec.description              := rec.description;
         L_tsfzone_temp_rec.transfer_zone            := rec.transfer_zone;
         L_tsfzone_temp_rec.create_id                := GET_USER;
         L_tsfzone_temp_rec.create_datetime          := SYSDATE;
         if rec.action = action_new then
            if EXEC_TSFZONE_INS( O_error_message,
                                       L_tsfzone_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_TSFZONE_UPD( O_error_message,
                                 L_tsfzone_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_TSFZONE_DEL( O_error_message,
                                 L_tsfzone_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TSFZONE;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TSFZONE_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_TSFZONE_TL.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_TSFZONE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_TSFZONE.PROCESS_TSFZONE_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TSFZONE_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TSFZONE';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSFZONE_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_tsfzone_TL_temp_rec  TSFZONE_TL%ROWTYPE;
   L_tsfzone_tl_upd_rst   ROW_SEQ_TAB;
   L_tsfzone_tl_del_rst   ROW_SEQ_TAB;

   cursor C_SVC_TSFZONE_TL(I_process_id NUMBER,
                           I_chunk_id NUMBER) is
      select pk_tsfzone_tl.rowid  as pk_tsfzone_tl_rid,
             fk_tsfzone.rowid     as fk_tsfzone_rid,
             fk_lang.rowid        as fk_lang_rid,
             st.lang,
             st.transfer_zone,
             st.description,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_tsfzone_tl  st,
             tsfzone         fk_tsfzone,
             tsfzone_tl      pk_tsfzone_tl,
             lang            fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.transfer_zone  =  fk_tsfzone.transfer_zone (+)
         and st.lang        =  pk_tsfzone_tl.lang (+)
         and st.transfer_zone  =  pk_tsfzone_tl.transfer_zone (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_TSFZONE_TL is TABLE OF C_SVC_TSFZONE_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_tsfzone_tab        SVC_TSFZONE_TL;

   L_tsfzone_TL_ins_tab         TSFZONE_TAB         := NEW TSFZONE_TAB();
   L_tsfzone_TL_upd_tab         TSFZONE_TAB         := NEW TSFZONE_TAB();
   L_tsfzone_TL_del_tab         TSFZONE_TAB         := NEW TSFZONE_TAB();

BEGIN
   if C_SVC_TSFZONE_TL%ISOPEN then
      close C_SVC_TSFZONE_TL;
   end if;

   open C_SVC_TSFZONE_TL(I_process_id,
                       I_chunk_id);
   LOOP
      fetch C_SVC_TSFZONE_TL bulk collect into L_svc_tsfzone_tab limit LP_bulk_fetch_limit;
      if L_svc_tsfzone_tab.COUNT > 0 then
         FOR i in L_svc_tsfzone_tab.FIRST..L_svc_tsfzone_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_tsfzone_tab(i).action is NULL
               or L_svc_tsfzone_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsfzone_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_tsfzone_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsfzone_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_tsfzone_tab(i).action = action_new
               and L_svc_tsfzone_tab(i).pk_tsfzone_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsfzone_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_tsfzone_tab(i).action IN (action_mod, action_del)
               and L_svc_tsfzone_tab(i).lang is NOT NULL
               and L_svc_tsfzone_tab(i).transfer_zone is NOT NULL
               and L_svc_tsfzone_tab(i).pk_tsfzone_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_tsfzone_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_tsfzone_tab(i).action = action_new
               and L_svc_tsfzone_tab(i).transfer_zone is NOT NULL
               and L_svc_tsfzone_tab(i).fk_tsfzone_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_tsfzone_tab(i).row_seq,
                            NULL,
                           'INV_TRAN_ZONE');
               L_error :=TRUE;
            end if;

            if L_svc_tsfzone_tab(i).action = action_new
               and L_svc_tsfzone_tab(i).lang is NOT NULL
               and L_svc_tsfzone_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsfzone_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_tsfzone_tab(i).action in (action_new, action_mod) then
               if L_svc_tsfzone_tab(i).description is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_tsfzone_tab(i).row_seq,
                              'DESCRIPTION',
                              'ENT_TRAN_ZN_DESC');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_tsfzone_tab(i).transfer_zone is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsfzone_tab(i).row_seq,
                           'TRANSFER_ZONE',
                           'ENT_TRAN_ZONE');
               L_error :=TRUE;
            end if;

            if L_svc_tsfzone_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsfzone_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_tsfzone_TL_temp_rec.lang := L_svc_tsfzone_tab(i).lang;
               L_tsfzone_TL_temp_rec.transfer_zone := L_svc_tsfzone_tab(i).transfer_zone;
               L_tsfzone_TL_temp_rec.description := L_svc_tsfzone_tab(i).description;
               L_tsfzone_TL_temp_rec.create_datetime := SYSDATE;
               L_tsfzone_TL_temp_rec.create_id := GET_USER;
               L_tsfzone_TL_temp_rec.last_update_datetime := SYSDATE;
               L_tsfzone_TL_temp_rec.last_update_id := GET_USER;

               if L_svc_tsfzone_tab(i).action = action_new then
                  L_tsfzone_TL_ins_tab.extend;
                  L_tsfzone_TL_ins_tab(L_tsfzone_TL_ins_tab.count()) := L_tsfzone_TL_temp_rec;
               end if;

               if L_svc_tsfzone_tab(i).action = action_mod then
                  L_tsfzone_TL_upd_tab.extend;
                  L_tsfzone_TL_upd_tab(L_tsfzone_TL_upd_tab.count()) := L_tsfzone_TL_temp_rec;
                  L_tsfzone_tl_upd_rst(L_tsfzone_TL_upd_tab.count()) := L_svc_tsfzone_tab(i).row_seq;
               end if;

               if L_svc_tsfzone_tab(i).action = action_del then
                  L_tsfzone_TL_del_tab.extend;
                  L_tsfzone_TL_del_tab(L_tsfzone_TL_del_tab.count()) := L_tsfzone_TL_temp_rec;
                  L_tsfzone_tl_del_rst(L_tsfzone_TL_del_tab.count()) := L_svc_tsfzone_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_TSFZONE_TL%NOTFOUND;
   END LOOP;
   close C_SVC_TSFZONE_TL;

   if EXEC_TSFZONE_TL_INS(O_error_message,
                          L_tsfzone_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_TSFZONE_TL_UPD(O_error_message,
                          L_tsfzone_TL_upd_tab,
                          L_tsfzone_tl_upd_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_TSFZONE_TL_DEL(O_error_message,
                          L_tsfzone_TL_del_tab,
                          L_tsfzone_tl_del_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TSFZONE_TL;
------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
     from svc_tsfzone_tl
    where process_id = I_process_id;

   delete
     from svc_tsfzone
    where process_id = I_process_id;
END;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_TSFZONE.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE   := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_TSFZONE(O_error_message,
                      I_process_id,
                      I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_TSFZONE_TL(O_error_message,
                         I_process_id,
                         I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
        set status = (CASE
                        when status = 'PE'
                        then 'PE'
                    else L_process_status
                    END),
          action_date = sysdate
    where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_TSFZONE;
/