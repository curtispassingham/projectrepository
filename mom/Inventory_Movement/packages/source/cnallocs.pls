CREATE OR REPLACE PACKAGE CANCEL_ALLOC_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------------------
--Function Name:  CANCEL_SINGLE_ALLOC
--Purpose      :  The function will accept an Order No. as an input parameter, and will cancel
--             :  Allocations for the Order.
-----------------------------------------------------------------------------------------------------
FUNCTION CANCEL_SINGLE_ALLOC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN       ALLOC_HEADER.ORDER_NO%TYPE,
                             I_cancel_id         IN       ORDLOC.CANCEL_ID%TYPE,
                             I_alloc_close_ind   IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:  CANCEL_ASN_ALLOC
--Purpose      :  Created a new function, CANCEL_ASN_ALLOC, which will cancel an ASN-based allocation
--             :  including its associated tiers.
-----------------------------------------------------------------------------------------------------
FUNCTION CANCEL_ASN_ALLOC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no          IN       ALLOC_HEADER.ORDER_NO%TYPE,
                          I_alloc_close_ind   IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:  UPDATE_ALLOC
--Purpose      :  Updates allocation details
-----------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:  CANCEL_ALLOC_ITEM
--Purpose      :  The function will accept Order No., Item, Wh as an input parameter, and will cancel
--             :  Allocations for the Order, Item and wh combination.
-----------------------------------------------------------------------------------------------------
FUNCTION CANCEL_ALLOC_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_location        IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
END CANCEL_ALLOC_SQL;
/