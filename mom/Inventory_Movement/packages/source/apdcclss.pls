CREATE OR REPLACE PACKAGE APPT_DOC_CLOSE_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------
-- Function Name: CHECK_OPEN_APPTS
-- Purpose: checks for open appointments 
----------------------------------------------------------------------------------------------
FUNCTION CHECK_OPEN_APPTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_open           IN OUT BOOLEAN,
                          I_doc            IN     APPT_DETAIL.DOC%TYPE,
                          I_doc_type       IN     APPT_DETAIL.DOC_TYPE%TYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: CLOSE_PO
-- Purpose: closes unappointed Orders
----------------------------------------------------------------------------------------------
FUNCTION CLOSE_PO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_closed         IN OUT BOOLEAN,
                  I_order          IN     APPT_DETAIL.DOC%TYPE)
RETURN BOOLEAN;    
----------------------------------------------------------------------------------------------
-- Function Name: REBUILD_2ND_LEG_DETAIL
--       Purpose: Rebuild the 2nd-leg transfer detail.
--                Step 1. Remove the initial 2nd-leg.
--                Step 2. Rebuild 2nd-leg with initial items in the 1st-leg and the new qty
--                        after being reconciled, update the existing item tranformation
--                        instructions with the new quantity.
--                Step 3. For each built pack initially existing in the 1st-leg, re-estimate
--                        the possibility to rebuild it based on the newly created 2nd-leg.
--                        If the newly created 2nd-leg has all the required components and
--                        enough quantity for each component, rebuild it and update the
--                        2nd-leg accordingly; Otherwise, remove the existing packing
--                        instruction.
----------------------------------------------------------------------------------------------
FUNCTION REBUILD_2ND_LEG_DETAIL(O_error_message         IN OUT  VARCHAR2,
                                I_1st_leg_tsf_no        IN      TSFDETAIL.TSF_NO%TYPE,
                                I_2nd_leg_tsf_no        IN      TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN;               
----------------------------------------------------------------------------------------------
-- Function Name: CLOSE_TSF
-- Purpose: closes unappointed Transfers
----------------------------------------------------------------------------------------------
FUNCTION CLOSE_TSF(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_closed         IN OUT BOOLEAN,
                   I_tsf_no         IN     APPT_DETAIL.DOC%TYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: CLOSE_ALLOC
-- Purpose: closes unappointed Allocations
----------------------------------------------------------------------------------------------
FUNCTION CLOSE_ALLOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_closed         IN OUT BOOLEAN,
                     I_alloc          IN     APPT_DETAIL.DOC%TYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: CLOSE_DOC
-- Purpose: closes Orders, Transfers and Allocations
----------------------------------------------------------------------------------------------
FUNCTION CLOSE_DOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_closed         IN OUT BOOLEAN,
                   I_appt_head      IN     APPT_HEAD%ROWTYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: CLOSE_ALL_ALLOCS
-- Purpose: closes all Allocations in doc_close_queue_temp.  Same functionality as CLOSE_ALLOC 
--          but Bulk processing.
----------------------------------------------------------------------------------------------
FUNCTION CLOSE_ALL_ALLOCS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: CHECK_OPEN_ALLOC_SOURCE
-- Purpose: Returns a collection of allocation numbers whose source docs
--          (PO/Transfer/Alloc/BOL/ASN) are still open
--          (i.e. either not closed or not fully received).
----------------------------------------------------------------------------------------------
FUNCTION CHECK_OPEN_ALLOC_SOURCE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_open_alloc_no_tbl   IN OUT   ALLOC_NO_TBL,
                                 I_alloc_no_tbl        IN OUT   ALLOC_NO_TBL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
END APPT_DOC_CLOSE_SQL;
/
