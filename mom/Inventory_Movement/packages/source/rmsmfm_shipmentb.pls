CREATE OR REPLACE PACKAGE BODY RMSMFM_SHIPMENT AS

   LP_error_status   VARCHAR2(1)   := NULL;

-------------------------------------------------------------------------------
                     /* Private Program Declarations */
-------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD (O_error_message IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY RIB_OBJECT,
                               O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                               O_bus_obj_id    IN OUT NOCOPY RIB_BUSOBJID_TBL,
                               I_shipment      IN            SHIPMENT.SHIPMENT%TYPE,
                               I_seq_no        IN            SHIPMENT_PUB_INFO.SEQ_NO%TYPE)
RETURN BOOLEAN;


FUNCTION BUILD_HEADER_OBJECT (O_error_message          IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                              O_rib_asnoutdesc_rec     IN OUT        "RIB_ASNOutDesc_REC",
                              O_routing_info           IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              I_shipment               IN            SHIPMENT_PUB_INFO.SHIPMENT%TYPE)
RETURN BOOLEAN;


FUNCTION BUILD_DETAIL_OBJECTS (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_rib_asnoutdistro_tbl   IN OUT   "RIB_ASNOutDistro_TBL",
                               I_shipment_rec           IN       SHIPMENT_REC)
RETURN BOOLEAN;


PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT      VARCHAR2,
                        O_error_message   IN  OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message         IN  OUT  NOCOPY RIB_OBJECT,
                        O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                        O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                        I_shipment        IN           SHIPMENT_PUB_INFO.SHIPMENT%TYPE,
                        I_seq_no          IN           SHIPMENT_PUB_INFO.SEQ_NO%TYPE);


FUNCTION LOCK_THE_BLOCK (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_queue_locked    IN OUT   BOOLEAN,
                         I_shipment        IN       SHIPMENT_PUB_INFO.SHIPMENT%TYPE)
RETURN BOOLEAN;


FUNCTION UPDATE_QUEUE_REC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_seq_no          IN       SHIPMENT_PUB_INFO.SEQ_NO%TYPE)
RETURN BOOLEAN;


-------------------------------------------------------------------------------
                      /*** Public Program Bodies ***/
-------------------------------------------------------------------------------
FUNCTION ADDTOQ (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message_type    IN       SHIPMENT_PUB_INFO.MESSAGE_TYPE%TYPE,
                 I_shipment        IN       SHIPMENT.SHIPMENT%TYPE,
                 I_to_loc          IN       SHIPMENT.TO_LOC%TYPE,
                 I_to_loc_type     IN       SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_system_options_row            SYSTEM_OPTIONS%ROWTYPE;
   L_pub_status                    SHIPMENT_PUB_INFO.PUB_STATUS%TYPE := 'U';
   L_max_details_to_publish        RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := NULL;
   L_minutes_time_lag              RIB_SETTINGS.MINUTES_TIME_LAG%TYPE       := NULL;
   L_num_threads                   RIB_SETTINGS.NUM_THREADS%TYPE            := NULL;
   L_program                       VARCHAR2(64) := 'RMSMFM_SHIPMENT.ADDTOQ';
   L_status_code                   VARCHAR2(1)  := NULL;
BEGIN

   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_shipment',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_message_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_message_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details_to_publish,
                                L_num_threads,
                                L_minutes_time_lag,
                                RMSMFM_SHIPMENT.FAMILY);

   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   insert into shipment_pub_info (seq_no,
                                  shipment,
                                  thread_no,
                                  message_type,
                                  family,
                                  custom_message_type,
                                  pub_status)
                          values (shipment_pubsequence.nextval,
                                  I_shipment,
                                  MOD(I_shipment,L_num_threads) + 1,
                                  I_message_type,
                                  RMSMFM_SHIPMENT.FAMILY,
                                  NULL,
                                  L_pub_status);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_SHIPMENT.ADDTOQ',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ADDTOQ;

-------------------------------------------------------------------------------
PROCEDURE GETNXT (O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_message_type    IN OUT   VARCHAR2,
                  O_message         IN OUT   RIB_OBJECT,
                  O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                  O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                  I_num_threads     IN       NUMBER DEFAULT 1,
                  I_thread_val      IN       NUMBER DEFAULT 1)
IS

   L_shipment           SHIPMENT_PUB_INFO.SHIPMENT%TYPE;
   L_pub_status         SHIPMENT_PUB_INFO.PUB_STATUS%TYPE;
   L_seq_no             SHIPMENT_PUB_INFO.SEQ_NO%TYPE;
   L_seq_limit          SHIPMENT_PUB_INFO.SEQ_NO%TYPE := 0;
   L_queue_locked       BOOLEAN     := FALSE;
   L_hosp               VARCHAR2(1) := 'N';

   PROGRAM_ERROR        EXCEPTION;

   cursor C_QUEUE is
      select seq_no,
             shipment,
             pub_status
        from shipment_pub_info q
       where q.seq_no = (select min(q2.seq_no)
                           from shipment_pub_info q2
                          where q2.thread_no  = I_thread_val
                            and q2.pub_status = 'U'
                            and q2.seq_no     > L_seq_limit)
         and q.thread_no = I_thread_val;

   cursor C_HOSP is
      select 'Y'
        from shipment_pub_info
       where shipment   = L_shipment
         and pub_status = API_CODES.HOSPITAL;

BEGIN

   LP_error_status := API_CODES.HOSPITAL;

   LOOP

      O_message      := NULL;
      O_bus_obj_id   := NULL;
      O_routing_info := NULL;

      open C_QUEUE;
      fetch C_QUEUE into L_seq_no,
                         L_shipment,
                         L_pub_status;

      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;

      close C_QUEUE;

      if LOCK_THE_BLOCK(O_error_message,
                        L_queue_locked,
                        L_shipment) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked = FALSE then
         open C_HOSP;
         fetch C_HOSP into L_hosp;
         close C_HOSP;

         if L_hosp = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            raise PROGRAM_ERROR;
         end if;

         if PROCESS_QUEUE_RECORD (O_error_message,
                                  O_message,
                                  O_routing_info,
                                  O_bus_obj_id,
                                  L_shipment,
                                  L_seq_no) = FALSE then
            raise PROGRAM_ERROR;
         end if;

         -- Unconditionally exit the loop assuming that shipment is published successfully.
         EXIT;

      else
         L_seq_limit := L_seq_no;
      end if;

   END LOOP;

   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_shipment);

      -- All shipment messages from RMS are create messages
      O_message_type := RMSMFM_SHIPMENT.LP_cre_type;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_shipment,
                    L_seq_no);
END GETNXT;
-------------------------------------------------------------------------------
PROCEDURE PUB_RETRY (O_status_code     IN OUT   VARCHAR2,
                     O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_message_type    IN OUT   VARCHAR2,
                     O_message         IN OUT   RIB_OBJECT,
                     O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                     O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                     I_ref_object      IN       RIB_OBJECT)
IS

   L_queue_locked     BOOLEAN     := FALSE;
   L_shipment         SHIPMENT_PUB_INFO.SHIPMENT%TYPE;
   L_seq_no           SHIPMENT_PUB_INFO.SEQ_NO%TYPE;

   PROGRAM_ERROR      EXCEPTION;

      cursor C_RETRY_QUEUE is
         select seq_no,
                shipment
           from shipment_pub_info
          where seq_no     = L_seq_no
            and pub_status = 'H';

BEGIN
   LP_error_status := API_CODES.HOSPITAL;

   -- Get info from routing info
   -- Assuming the only thing in the routing info is the seq_no
   L_seq_no  := O_routing_info(1).value;

   O_message := NULL;
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_seq_no,
                            L_shipment;

   if C_RETRY_QUEUE%NOTFOUND then
      close C_RETRY_QUEUE;
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   close C_RETRY_QUEUE;

   if L_seq_no is NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   if LOCK_THE_BLOCK(O_error_message,
                     L_queue_locked,
                     L_shipment) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then
      if PROCESS_QUEUE_RECORD(O_error_message,
                              O_message,
                              O_routing_info,
                              O_bus_obj_id,
                              L_shipment,
                              L_seq_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if O_message is NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         O_status_code := API_CODES.NEW_MSG;

         -- All shipment messages from RMS are create messages
         O_message_type := RMSMFM_SHIPMENT.LP_cre_type;
      end if;

      O_bus_obj_id  := RIB_BUSOBJID_TBL(L_shipment);
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_shipment,
                    L_seq_no);

END PUB_RETRY;
-------------------------------------------------------------------------------
                      /*** Private Program Bodies ***/
-------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD (O_error_message IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                               O_message       IN OUT NOCOPY RIB_OBJECT,
                               O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                               O_bus_obj_id    IN OUT NOCOPY RIB_BUSOBJID_TBL,
                               I_shipment      IN            SHIPMENT.SHIPMENT%TYPE,
                               I_seq_no        IN            SHIPMENT_PUB_INFO.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_status_code           VARCHAR2(1) := NULL;
   L_max_details           RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 12;
   L_num_threads           RIB_SETTINGS.NUM_THREADS%TYPE            := NULL;
   L_min_time_lag          RIB_SETTINGS.MINUTES_TIME_LAG%TYPE       := NULL;
   L_rib_asnout_desc_rec   "RIB_ASNOutDesc_REC"                       := NULL;

BEGIN

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_SHIPMENT.FAMILY);

   if L_status_code = API_CODES.UNHANDLED_ERROR then
      return FALSE;
   end if;

   if BUILD_HEADER_OBJECT(O_error_message,
                          L_rib_asnout_desc_rec,
                          O_routing_info,
                          I_shipment) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if UPDATE_QUEUE_REC(O_error_message,
                       I_seq_no) = FALSE then
      return FALSE;
   end if;

   O_message := L_rib_asnout_desc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_SHIPMENT.PROCESS_QUEUE_RECORD',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_RECORD;
-------------------------------------------------------------------------------
FUNCTION BUILD_HEADER_OBJECT (O_error_message          IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                              O_rib_asnoutdesc_rec     IN OUT        "RIB_ASNOutDesc_REC",
                              O_routing_info           IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              I_shipment               IN            SHIPMENT_PUB_INFO.SHIPMENT%TYPE)
RETURN BOOLEAN IS

   L_rib_asnoutdesc_rec     "RIB_ASNOutDesc_REC"   := NULL;
   L_rib_routinginfo_rec    RIB_ROUTINGINFO_REC  := NULL;
   L_rib_asnoutdistro_tbl   "RIB_ASNOutDistro_TBL" := NULL;
   L_add_1                  ADDR.ADD_1%TYPE;
   L_add_2                  ADDR.ADD_2%TYPE;
   L_city                   ADDR.CITY%TYPE;
   L_state                  ADDR.STATE%TYPE;
   L_country_id             ADDR.COUNTRY_ID%TYPE;
   L_wh_rec                 WH_ATTRIB_SQL.WH_RECTYPE;
   L_store_rec              STORE_ATTRIB_SQL.STORE_RECTYPE;
   L_shipment_rec           SHIPMENT_REC;
   L_asn_type               VARCHAR2(1);
   L_to_loc_type            TSFHEAD.TO_LOC_TYPE%TYPE;

   cursor C_GET_SHIPMENT is
      select shipment,
             order_no,
             bol_no,
             asn,
             ship_date,
             receive_date,
             est_arr_date,
             ship_origin,
             status_code,
             invc_match_status,
             invc_match_date,
             to_loc,
             to_loc_type,
             case
                when to_loc_type = 'S' then
                   (select store_type
                      from store
                     where store = to_loc)
                else
                   NULL
             end to_store_type,
             case
                when to_loc_type = 'S' then
                   (select stockholding_ind
                      from store
                     where store = to_loc)
                else
                   NULL
             end to_stockholding_ind,
             from_loc,
             from_loc_type,
             case
                when from_loc_type = 'S' then
                   (select store_type
                      from store
                     where store = from_loc)
                else
                   NULL
             end from_store_type,
             case
                when from_loc_type = 'S' then
                   (select stockholding_ind
                      from store
                     where store = from_loc)
                else
                   NULL
             end from_stockholding_ind,
             courier,
             no_boxes,
             ext_ref_no_in,
             ext_ref_no_out,
             comments,
             parent_shipment,
             bill_to_loc,
             bill_to_loc_type,
             ref_doc_no
        from shipment
       where shipment = I_shipment;

BEGIN

   O_rib_asnoutdesc_rec := NULL;
   O_routing_info       := NULL;

   open C_GET_SHIPMENT;
   fetch C_GET_SHIPMENT into L_shipment_rec;
   close C_GET_SHIPMENT;

   if L_shipment_rec.to_loc_type = 'S' then

      L_asn_type := 'C';

      if STORE_ATTRIB_SQL.GET_INFO(O_error_message,
                                   L_store_rec,
                                   L_shipment_rec.to_loc) = FALSE then
         return FALSE;
      end if;
      
      L_add_1       := L_store_rec.store_add1;
      L_add_2       := L_store_rec.store_add2;
      L_city        := L_store_rec.store_city;
      L_state       := L_store_rec.state;
      L_country_id  := L_store_rec.country_id;
      
   else
      L_asn_type := 'T';
      
      if WH_ATTRIB_SQL.GET_WH_INFO(O_error_message,
                                   L_wh_rec,
                                   L_shipment_rec.to_loc) = FALSE then
         return FALSE;
      end if;

      L_add_1       := L_wh_rec.wh_add1;
      L_add_2       := L_wh_rec.wh_add2;
      L_city        := L_wh_rec.wh_city;
      L_state       := L_wh_rec.state;
      L_country_id  := L_wh_rec.country_id;
      
      -- Irrespective of the location on shipment, RMS need to publish physical warehouse
      L_shipment_rec.to_loc := L_wh_rec.physical_wh;
   end if;

   L_rib_asnoutdistro_tbl := "RIB_ASNOutDistro_TBL"();

   if BUILD_DETAIL_OBJECTS (O_error_message,
                            L_rib_asnoutdistro_tbl,
                            L_shipment_rec) = FALSE then
      return FALSE;
   end if;

   -- Container_qty is a required field on the RIB object.
   -- RMS has to send 1 instead of NULL for container qty in header level.
   if L_shipment_rec.no_boxes is NULL then
      L_shipment_rec.no_boxes := 1;
   end if;

   L_rib_asnoutdesc_rec := "RIB_ASNOutDesc_REC"(0,                                    -- RIB_OID
                                                NULL,                                 -- SCHEDULE_NUMBER       NUMBER(8)
                                                'N',                                  -- AUTO_RECEIVE          VARCHAR2(1)
                                                L_shipment_rec.to_loc,                -- TO_LOCATION           VARCHAR2(10)
                                                L_shipment_rec.to_loc_type,           -- TO_LOC_TYPE           VARCHAR2(1)
                                                L_shipment_rec.to_store_type,         -- TO_STORE_TYPE         VARCHAR2(1)
                                                L_shipment_rec.to_stockholding_ind,   -- TO_STOCKHOLDING_IND   VARCHAR2(1)
                                                L_shipment_rec.from_loc,              -- FROM_LOCATION         VARCHAR2(10)
                                                L_shipment_rec.from_loc_type,         -- FROM_LOC_TYPE         VARCHAR2(1)
                                                L_shipment_rec.from_store_type,       -- FROM_STORE_TYPE       VARCHAR2(1)
                                                L_shipment_rec.from_stockholding_ind, -- FROM_STOCKHOLDING_IND VARCHAR2(1)
                                                /* Note: use shipment.bol_no for the ASN_NBR field on the message. */
                                                /* Shipments created in RMS has a NULL shipment.asn. */
                                                L_shipment_rec.bol_no,                -- ASN_NBR               VARCHAR2(30)
                                                L_asn_type,                           -- ASN_TYPE              VARCHAR2(1)
                                                L_shipment_rec.no_boxes,              -- CONTAINER_QTY         NUMBER(6)
                                                L_shipment_rec.bol_no,                -- BOL_NBR               VARCHAR2(17)
                                                L_shipment_rec.ship_date,             -- SHIPMENT_DATE         DATE
                                                L_shipment_rec.est_arr_date,          -- EST_ARR_DATE          DATE
                                                L_add_1,                              -- SHIP_ADDRESS1         VARCHAR2(240)
                                                L_add_2,                              -- SHIP_ADDRESS2         VARCHAR2(240)
                                                NULL,                                 -- SHIP_ADDRESS3         VARCHAR2(240)
                                                NULL,                                 -- SHIP_ADDRESS4         VARCHAR2(240)
                                                NULL,                                 -- SHIP_ADDRESS5         VARCHAR2(240)
                                                L_city,                               -- SHIP_CITY             VARCHAR2(120)
                                                L_state,                              -- SHIP_STATE            VARCHAR2(3)
                                                NULL,                                 -- SHIP_ZIP              VARCHAR2(30)
                                                L_country_id,                         -- SHIP_COUNTRY_ID       VARCHAR2(3)
                                                1,                                    -- TRAILER_NBR           VARCHAR2(12)
                                                NULL,                                 -- SEAL_NBR              VARCHAR2(12)
                                                NULL,                                 -- TRANSSHIPMENT_NBR     VARCHAR2(30)
                                                L_rib_asnoutdistro_tbl,               -- ASNOUTDISTRO_TBL      "RIB_ASNOutDistro_TBL"
                                                L_shipment_rec.comments,              -- COMMENTS              VARCHAR2(2000)
                                                'DC',                                 -- CARRIER_CODE          VARCHAR2(4)
                                                NULL);                                -- CARRIER_SERVICE_CODE  VARCHAR2(6)
                                              

   O_routing_info := RIB_ROUTINGINFO_TBL();

   L_rib_routinginfo_rec := RIB_ROUTINGINFO_REC('from_phys_loc',
                                                L_shipment_rec.from_loc,
                                                'from_phys_loc_type',
                                                L_shipment_rec.from_loc_type,
                                                NULL,
                                                NULL);

   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routinginfo_rec;
   
   -- Set the to locations type to V for the non stockholding virtual stores
   if L_shipment_rec.to_loc_type = 'S' and L_store_rec.store_type = 'C' and L_store_rec.stockholding_ind = 'N' then
      L_to_loc_type := 'V';
   else
      L_to_loc_type := L_shipment_rec.to_loc_type;
   end if;
   
   L_rib_routinginfo_rec := RIB_ROUTINGINFO_REC('to_phys_loc',
                                                L_shipment_rec.to_loc,
                                                'to_phys_loc_type',
                                                L_to_loc_type,
                                                NULL,
                                                NULL);

   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routinginfo_rec;

   /* RMS publishes ASNOut messages to etASNOut topic. */
   /* RMS subscribes to ASNOut messages from etASNOutAT topic. */
   /* Routing info 'source_app' is added for RIB to know that this ASNOut message */
   /* is published from RMS, therefore, should not be published back to RMS again. */
   /* RIB ASNOutToASNOutAT TAFR will drop messages if routing info 'source_app' is 'RMS'. */
   L_rib_routinginfo_rec := RIB_ROUTINGINFO_REC('source_app',
                                                'RMS',
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL);

   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routinginfo_rec;

   O_rib_asnoutdesc_rec := L_rib_asnoutdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_SHIPMENT.BUILD_HEADER_OBJECT',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END BUILD_HEADER_OBJECT;
-------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_OBJECTS (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_rib_asnoutdistro_tbl   IN OUT   "RIB_ASNOutDistro_TBL",
                               I_shipment_rec           IN       SHIPMENT_REC)
RETURN BOOLEAN IS

   cursor C_GET_SHIPSKU IS
      select DISTINCT distro_no,
                      distro_type
        from shipsku
       where shipment = I_shipment_rec.shipment_no;

   cursor C_GET_SHIPSKU_CTN(C_distro_no SHIPSKU.DISTRO_NO%TYPE) IS
      select DISTINCT carton
        from shipsku
       where shipment = I_shipment_rec.shipment_no
         and distro_no = C_distro_no;

   cursor C_GET_SHIPSKU_ITEM(C_distro_no SHIPSKU.DISTRO_NO%TYPE, C_carton SHIPSKU.CARTON%TYPE) IS
      select item,
             qty_expected,
             unit_cost,
             weight_expected,
             weight_expected_uom
        from shipsku
       where shipment = I_shipment_rec.shipment_no
         and distro_no = C_distro_no
         and carton = C_carton;

   L_distro_no   ALLOC_DETAIL.ALLOC_NO%TYPE;

   cursor C_GET_ALLOC_DETAIL is
      select in_store_date,
             rush_flag
        from alloc_detail
       where alloc_no = L_distro_no
         and to_loc = I_shipment_rec.to_loc;
         
   cursor C_GET_CUST_FULFILL_ORD is
      select customer_order_no,
             fulfill_order_no
        from ordcust
       where tsf_no = L_distro_no;
        
   TYPE shipsku_tbl is table of C_GET_SHIPSKU%ROWTYPE INDEX BY BINARY_INTEGER;

   L_to_loc               SHIPMENT.TO_LOC%TYPE;
   L_shipsku_tbl          shipsku_tbl;
   L_rib_asnoutitem_rec   "RIB_ASNOutItem_REC";
   L_rib_asnoutitem_tbl   "RIB_ASNOutItem_TBL"     := "RIB_ASNOutItem_TBL"();
   L_rib_asnoutctn_rec    "RIB_ASNOutCtn_REC";
   L_rib_asnoutctn_tbl    "RIB_ASNOutCtn_TBL"      := "RIB_ASNOutCtn_TBL"();
   L_rib_asnoutdistro_rec "RIB_ASNOutDistro_REC";
   L_expedite_flag        VARCHAR2(1);
   L_in_store_date        DATE;
   L_dept                 TSFHEAD.DEPT%TYPE;
   L_create_date          TSFHEAD.CREATE_DATE%TYPE;
   L_status               TSFHEAD.STATUS%TYPE;
   L_from_loc_type        TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_from_loc             TSFHEAD.FROM_LOC%TYPE;
   L_to_loc_type          TSFHEAD.TO_LOC_TYPE%TYPE;
   L_tsf_type             TSFHEAD.TSF_TYPE%TYPE;
   L_freight_code         TSFHEAD.FREIGHT_CODE%TYPE;
   L_routing_code         TSFHEAD.ROUTING_CODE%TYPE;
   L_comment_desc         TSFHEAD.COMMENT_DESC%TYPE;
   L_records_found        BOOLEAN := FALSE;
   L_customer_order_no    ORDCUST.CUSTOMER_ORDER_NO%TYPE;    
   L_fulfill_order_no     ORDCUST.FULFILL_ORDER_NO%TYPE;   

BEGIN

   open C_GET_SHIPSKU;
   fetch C_GET_SHIPSKU BULK COLLECT INTO L_shipsku_tbl;
   close C_GET_SHIPSKU;

   L_rib_asnoutitem_rec   := "RIB_ASNOutItem_REC"(0,      -- RIB_OID
                                                  NULL,   -- ITEM_ID
                                                  NULL,   -- UNIT_QTY
                                                  NULL,   -- PRIORITY_LEVEL
                                                  NULL,   -- ORDER_LINE_NBR
                                                  NULL,   -- LOT_NBR
                                                  NULL,   -- FINAL_LOCATION
                                                  NULL,   -- FROM_DISPOSITION
                                                  NULL,   -- TO_DISPOSITION
                                                  NULL,   -- VOUCHER_NUMBER
                                                  NULL,   -- VOUCHER_EXPIRATION_DATE
                                                  NULL,   -- CONTAINER_QTY
                                                  NULL,   -- COMMENTS
                                                  NULL,   -- UNIT_COST
                                                  NULL,   -- WEIGHT
                                                  NULL ); -- WEIGHT_UOM

   L_rib_asnoutctn_rec   :=   "RIB_ASNOutCtn_REC"(0,     -- RIB_OID
                                                  NULL,  -- FINAL_LOCATION
                                                  NULL,  -- CONTAINER_ID
                                                  NULL,  -- CONTAINER_WEIGHT
                                                  NULL,  -- CONTAINER_LENGTH
                                                  NULL,  -- CONTAINER_WIDTH
                                                  NULL,  -- CONTAINER_HEIGHT
                                                  NULL,  -- CONTAINER_CUBE
                                                  NULL,  -- EXPEDITE_FLAG
                                                  NULL,  -- IN_STORE_DATE
                                                  NULL,  -- TRACKING_NBR
                                                  NULL,  -- FREIGHT_CHARGE
                                                  NULL,  -- MASTER_CONTAINER_ID
                                                  NULL,  -- ASNOUTITEM_TBL
                                                  NULL,  -- COMMENTS
                                                  NULL,  -- WEIGHT
                                                  NULL,  -- WEIGHT_UOM
                                                  NULL); -- CARRIER_SHIPMENT_NBR

   L_rib_asnoutdistro_rec := "RIB_ASNOutDistro_REC"(0,     -- RIB_OID
                                                    NULL,  -- DISTRO_NBR
                                                    NULL,  -- DISTRO_DOC_TYPE
                                                    NULL,  -- CUST_ORDER_NBR,
                                                    NULL,  -- FULFILL_ORDER_NBR
                                                    NULL,  -- CONSUMER_DIRECT
                                                    NULL,  -- ASNOUTCTN_TBL
                                                    NULL); -- COMMENTS
   O_rib_asnoutdistro_tbl.DELETE;
   FOR i in 1..L_shipsku_tbl.COUNT LOOP

      L_records_found := TRUE;
      L_distro_no := L_shipsku_tbl(i).distro_no;

      if L_shipsku_tbl(i).distro_type = 'T' then

         L_in_store_date := NULL;

         if TSF_ATTRIB_SQL.GET_TSFHEAD_INFO(O_error_message,
                                            L_distro_no,
                                            L_dept,
                                            L_create_date,
                                            L_status,
                                            L_from_loc_type,
                                            L_from_loc,
                                            L_to_loc_type,
                                            L_to_loc,
                                            L_tsf_type,
                                            L_freight_code,
                                            L_routing_code,
                                            L_comment_desc) = FALSE then
            return FALSE;
         end if;
         ---
         if L_freight_code = 'E' then
            L_expedite_flag := 'Y';
         else
            L_expedite_flag := 'N';
         end if;
         ---
         if L_tsf_type = 'CO' then
            open C_GET_CUST_FULFILL_ORD;
            fetch C_GET_CUST_FULFILL_ORD into L_customer_order_no,
                                              L_fulfill_order_no;
                                              
            if C_GET_CUST_FULFILL_ORD%NOTFOUND then
               close C_GET_CUST_FULFILL_ORD;
               O_error_message := SQL_LIB.CREATE_MSG('NO_ORD_CUST',
                                                     L_distro_no,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;
            
            close C_GET_CUST_FULFILL_ORD;
         
            L_rib_asnoutdistro_rec.cust_order_nbr    := L_customer_order_no;
            L_rib_asnoutdistro_rec.fulfill_order_nbr := L_fulfill_order_no;
         end if;
         ---
      else
         open C_GET_ALLOC_DETAIL;
         fetch C_GET_ALLOC_DETAIL into L_in_store_date,
                                       L_expedite_flag;

         if C_GET_ALLOC_DETAIL%NOTFOUND then
            close C_GET_ALLOC_DETAIL;
            O_error_message := SQL_LIB.CREATE_MSG('NO_ALLOC_DETAIL_PUB',
                                                  L_distro_no,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;

         close C_GET_ALLOC_DETAIL;

      end if;

      l_rib_asnoutctn_tbl.DELETE;
      FOR L_ctn IN C_GET_SHIPSKU_CTN(L_distro_no) LOOP
         l_rib_asnoutitem_tbl.DELETE;
         FOR L_item IN C_GET_SHIPSKU_ITEM(L_distro_no, L_ctn.carton) LOOP
            L_rib_asnoutitem_rec.item_id        := L_item.item;
            L_rib_asnoutitem_rec.unit_qty       := L_item.qty_expected;
            L_rib_asnoutitem_rec.final_location := I_shipment_rec.to_loc;
            L_rib_asnoutitem_rec.unit_cost      := L_item.unit_cost;

            -- Container_qty is a required field on the RIB object.
            -- RMS has to send 1 instead of NULL.
            L_rib_asnoutitem_rec.container_qty  := 1;

            L_rib_asnoutitem_tbl.EXTEND;
            L_rib_asnoutitem_tbl(L_rib_asnoutitem_tbl.COUNT) := L_rib_asnoutitem_rec;
         END LOOP;
         --Container _id is required field on the RIB object
         --It identifies the carton number for shipments originating from the ASN
         --process as carton shipments

         L_rib_asnoutctn_rec.final_location := I_shipment_rec.to_loc;
         L_rib_asnoutctn_rec.container_id   := L_ctn.carton;
         L_rib_asnoutctn_rec.expedite_flag  := L_expedite_flag;
         L_rib_asnoutctn_rec.in_store_date  := L_in_store_date;

         L_rib_asnoutctn_rec.asnoutitem_tbl := L_rib_asnoutitem_tbl;

         L_rib_asnoutctn_tbl.EXTEND;
         L_rib_asnoutctn_tbl(L_rib_asnoutctn_tbl.COUNT) := L_rib_asnoutctn_rec;
      END LOOP;
      L_rib_asnoutdistro_rec.distro_nbr := L_distro_no;
      L_rib_asnoutdistro_rec.distro_doc_type := L_shipsku_tbl(i).distro_type;

      L_rib_asnoutdistro_rec.asnoutctn_tbl := L_rib_asnoutctn_tbl;

      O_rib_asnoutdistro_tbl.EXTEND;
      O_rib_asnoutdistro_tbl(O_rib_asnoutdistro_tbl.COUNT) := L_rib_asnoutdistro_rec;

   END LOOP;

   -- If no data is found in cursor, raise error
   if not L_records_found then
      O_error_message := SQL_LIB.CREATE_MSG('NO_SHIPSKU_RECORDS',
                                            I_shipment_rec.shipment_no,
                                            NULL,
                                            NULL);
         return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_SHIPMENT.BUILD_DETAIL_OBJECTS',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END BUILD_DETAIL_OBJECTS;
-------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN  OUT      VARCHAR2,
                        O_error_message   IN  OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message         IN  OUT  NOCOPY RIB_OBJECT,                        
                        O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                        O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                        I_shipment        IN           SHIPMENT_PUB_INFO.SHIPMENT%TYPE,
                        I_seq_no          IN           SHIPMENT_PUB_INFO.SEQ_NO%TYPE)
IS

   L_error_type         VARCHAR2(5) := NULL;
   L_shipment_rec       SHIPMENT_REC;

   L_rib_asnoutitem_tbl   "RIB_ASNOutItem_TBL";
   L_rib_asnoutitem_rec   "RIB_ASNOutItem_REC"    := NULL;
   L_rib_asnoutctn_tbl    "RIB_ASNOutCtn_TBL";
   L_rib_asnoutctn_rec    "RIB_ASNOutCtn_REC"     := NULL;
   L_rib_asnoutdistro_tbl "RIB_ASNOutDistro_TBL";
   L_rib_asnoutdistro_rec "RIB_ASNOutDistro_REC"  := NULL;
   L_rib_asnout_desc_rec  "RIB_ASNOutDesc_REC"    := NULL;

   cursor C_GET_SHIPMENT is
      select shipment,
             order_no,
             bol_no,
             asn,
             ship_date,
             receive_date,
             est_arr_date,
             ship_origin,
             status_code,
             invc_match_status,
             invc_match_date,
             to_loc,
             to_loc_type,
             case
                when to_loc_type = 'S' then
                   (select store_type
                      from store
                     where store = to_loc)
                else
                   NULL
             end to_store_type,
             case
                when to_loc_type = 'S' then
                   (select stockholding_ind
                      from store
                     where store = to_loc)
                else
                   NULL
             end to_stockholding_ind,
             from_loc,
             from_loc_type,
             case
                when from_loc_type = 'S' then
                   (select store_type
                      from store
                     where store = from_loc)
                else
                   NULL
             end from_store_type,
             case
                when from_loc_type = 'S' then
                   (select stockholding_ind
                      from store
                     where store = from_loc)
                else
                   NULL
             end from_stockholding_ind,
             courier,
             no_boxes,
             ext_ref_no_in,
             ext_ref_no_out,
             comments,
             parent_shipment,
             bill_to_loc,
             bill_to_loc_type,
             ref_doc_no
        from shipment
       where shipment = I_shipment;

BEGIN
   O_status_code := LP_error_status;
   
   open C_GET_SHIPMENT;
   fetch C_GET_SHIPMENT into L_shipment_rec;
   close C_GET_SHIPMENT; 

   if LP_error_status = API_CODES.HOSPITAL then
      O_bus_obj_id    := RIB_BUSOBJID_TBL(I_shipment);
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no',
                                                                 I_seq_no,
                                                                 NULL,
                                                                 NULL,
                                                                 NULL,
                                                                 NULL));
      L_rib_asnoutitem_rec := "RIB_ASNOutItem_REC"(0,
                                                   NULL,
                                                   NULL);

      L_rib_asnoutitem_tbl := "RIB_ASNOutItem_TBL"(L_rib_asnoutitem_rec);

      L_rib_asnoutctn_rec := "RIB_ASNOutCtn_REC"(0,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 L_rib_asnoutitem_tbl,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL);
                                                 
      L_rib_asnoutctn_tbl := "RIB_ASNOutCtn_TBL"(L_rib_asnoutctn_rec);

      L_rib_asnoutdistro_rec := "RIB_ASNOutDistro_REC"(0,
                                                       NULL,
                                                       NULL,
                                                       NULL,
                                                       NULL,
                                                       NULL,
                                                       L_rib_asnoutctn_tbl,
                                                       NULL);

      L_rib_asnoutdistro_tbl := "RIB_ASNOutDistro_TBL"(L_rib_asnoutdistro_rec);
      
      L_rib_asnout_desc_rec := "RIB_ASNOutDesc_REC"(0,                                   -- rib_oid               NUMBER
                                                    NULL,                                -- schedule_nbr          NUMBER(8)
                                                    NULL,                                -- auto_receive          VARCHAR2(1)
                                                    L_shipment_rec.to_loc,               -- to_location           VARCHAR2(10)
                                                    L_shipment_rec.to_loc_type,          -- to_loc_type           VARCHAR2(1)
                                                    L_shipment_rec.to_store_type,        -- to_store_type         VARCHAR2(1)
                                                    L_shipment_rec.to_stockholding_ind,  -- to_stockholding_ind   VARCHAR2(1)
                                                    L_shipment_rec.from_loc,             -- from_location         VARCHAR2(10)
                                                    L_shipment_rec.from_loc_type,        -- from_loc_type         VARCHAR2(1)
                                                    L_shipment_rec.from_store_type,      -- from_store_type       VARCHAR2(1)
                                                    L_shipment_rec.from_stockholding_ind,-- from_stockholding_ind VARCHAR2(1)
                                                    L_shipment_rec.bol_no,               -- asn_nbr               VARCHAR2(30)
                                                    NULL,                                -- asn_type              VARCHAR2(1)
                                                    NULL,                                -- container_qty         NUMBER(6)
                                                    L_shipment_rec.bol_no,               -- bol_nbr               VARCHAR2(17)
                                                    NULL,                                -- shipment_date         DATE
                                                    NULL,                                -- est_arr_date          DATE
                                                    NULL,                                -- ship_address1         VARCHAR2(240)
                                                    NULL,                                -- ship_address2         VARCHAR2(240)
                                                    NULL,                                -- ship_address3         VARCHAR2(240)
                                                    NULL,                                -- ship_address4         VARCHAR2(240)
                                                    NULL,                                -- ship_address5         VARCHAR2(240)
                                                    NULL,                                -- ship_city             VARCHAR2(120)
                                                    NULL,                                -- ship_state            VARCHAR2(3)
                                                    NULL,                                -- ship_zip              VARCHAR2(30)
                                                    NULL,                                -- ship_country_id       VARCHAR2(3)
                                                    NULL,                                -- trailer_nbr           VARCHAR2(12)
                                                    NULL,                                -- seal_nbr              VARCHAR2(12)
                                                    NULL,                                -- transshipment_nbr     VARCHAR2(30)
                                                    L_rib_asnoutdistro_tbl,              -- ASNOutDistro_TBL      "RIB_ASNOutDistro_TBL"
                                                    NULL,                                -- comments              VARCHAR2(2000)
                                                    NULL,                                -- carrier_code          VARCHAR2(4)
                                                    NULL);                               -- carrier_service_code  VARCHAR2(6)

      O_message := L_rib_asnout_desc_rec;
      update shipment_pub_info
         set pub_status = LP_error_status
       where seq_no     = I_seq_no;
   end if;

   SQL_LIB.API_MSG(L_error_type,
                   O_error_message);

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SHIPMENT');
END HANDLE_ERRORS;
-------------------------------------------------------------------------------
FUNCTION LOCK_THE_BLOCK (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_queue_locked    IN OUT   BOOLEAN,
                         I_shipment        IN       SHIPMENT_PUB_INFO.SHIPMENT%TYPE)
RETURN BOOLEAN IS

   L_table             VARCHAR2(30)  := 'SHIPMENT_PUB_INFO';
   L_key1              VARCHAR2(100) := I_shipment;
   L_key2              VARCHAR2(100) := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked,-54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from shipment_pub_info
       where shipment = I_shipment
         for update nowait;

BEGIN
   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      O_queue_locked := TRUE;
      return TRUE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_SHIPMENT.LOCK_THE_BLOCK',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END LOCK_THE_BLOCK;
-------------------------------------------------------------------------------
FUNCTION UPDATE_QUEUE_REC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_seq_no          IN       SHIPMENT_PUB_INFO.SEQ_NO%TYPE)
RETURN BOOLEAN IS

BEGIN

   update shipment_pub_info spi
      set spi.pub_status = 'P'
    where spi.seq_no = I_seq_no;      

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_SHIPMENT.UPDATE_QUEUE_REC',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END UPDATE_QUEUE_REC;
-------------------------------------------------------------------------------
END RMSMFM_SHIPMENT;
/

