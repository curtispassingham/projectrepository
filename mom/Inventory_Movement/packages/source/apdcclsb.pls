CREATE OR REPLACE PACKAGE BODY APPT_DOC_CLOSE_SQL AS
----------------------------------------------------------------------------------------------
FUNCTION CHECK_OPEN_APPTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_open           IN OUT BOOLEAN,
                          I_doc            IN     APPT_DETAIL.DOC%TYPE,
                          I_doc_type       IN     APPT_DETAIL.DOC_TYPE%TYPE)
RETURN BOOLEAN IS
   L_flag      VARCHAR2(1);
   L_function  VARCHAR2(50) := 'APPT_DOC_CLOSE_SQL.CHECK_OPEN_APPTS';
   ---
   cursor C_CHECK_OPEN_APPTS is
      select 'x'
        from appt_head ah, appt_detail ad
       where ah.appt     = ad.appt
         and ah.loc      = ad.loc
         and ah.loc_type = ad.loc_type
         and ad.doc      = I_doc
         and ad.doc_type = I_doc_type
         and ah.status  != 'AC'
         and rownum      = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_CHECK_OPEN_APPTS','APPT_HEAD, APPT_DETAIL',NULL);
   open C_CHECK_OPEN_APPTS;
   SQL_LIB.SET_MARK('OPEN','C_CHECK_APPTS','APPT_HEAD, APPT_DETAIL',NULL);
   fetch C_CHECK_OPEN_APPTS into L_flag;
   ---
   if C_CHECK_OPEN_APPTS%FOUND then
      O_open := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_APPTS','APPT_HEAD, APPT_DETAIL',NULL);
   close C_CHECK_OPEN_APPTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;

END CHECK_OPEN_APPTS;
----------------------------------------------------------------------------------------------
FUNCTION CLOSE_PO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_closed         IN OUT BOOLEAN,
                  I_order          IN     APPT_DETAIL.DOC%TYPE)
RETURN BOOLEAN IS
   L_function           VARCHAR2(50) := 'APPT_DOC_CLOSE_SQL.CLOSE_PO';
   L_flag               VARCHAR2(1);
   L_exist              BOOLEAN;
   L_appr_ind           VARCHAR2(3);
   L_import_ind         SYSTEM_OPTIONS.IMPORT_IND%TYPE;
   L_rtm_simplified_ind SYSTEM_OPTIONS.RTM_SIMPLIFIED_IND%TYPE;
   L_system_options_row SYSTEM_OPTIONS%ROWTYPE;
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_vdate              DATE         := GET_VDATE;
   ---
   cursor C_CHECK_QTYS is
      select 'x'
        from ordloc
       where order_no    = I_order
         and qty_ordered > NVL(qty_received, 0)
         and rownum = 1;

   cursor C_LOCK_ORDHEAD is
      select 'x'
        from ordhead
       where order_no = I_order
         for update nowait;

BEGIN
   O_closed := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_CHECK_QTYS','ORDLOC',NULL);
   open C_CHECK_QTYS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_QTYS','ORDLOC',NULL);
   fetch C_CHECK_QTYS into L_flag;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_QTYS','ORDLOC',NULL);
   close C_CHECK_QTYS;
   ---
   if L_flag is NULL then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDHEAD','ORDHEAD',NULL);
      open C_LOCK_ORDHEAD;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDHEAD','ORDHEAD',NULL);
      close C_LOCK_ORDHEAD;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ORDHEAD','ORDER_NO:'||I_order);
      update ordhead
         set status     = 'C',
             close_date = L_vdate,
             last_update_id = get_user,
             last_update_datetime = sysdate
       where order_no = I_order
         and status  != 'C';
      ---
      if DEAL_VALIDATE_SQL.DEAL_CALC_QUEUE_EXIST(O_error_message,
                                                 L_exist,
                                                 L_appr_ind,
                                                 I_order) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exist then
         if ORDER_STATUS_SQL.DELETE_DEAL_CALC_QUEUE(O_error_message,
                                                    I_order) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_row) = FALSE then
         return FALSE;
      end if;
      ---
      L_import_ind := L_system_options_row.import_ind;
      L_rtm_simplified_ind := L_system_options_row.rtm_simplified_ind;
      ---
      if (L_import_ind = 'Y' and L_rtm_simplified_ind = 'N') then
         if ALC_ALLOC_SQL.ALLOC_ALL_PO_OBL(O_error_message,
                                           I_order) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      O_closed := TRUE;

   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                             'ORDHEAD',
                                             'Order: '||to_char(I_order),
                                             L_function);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;

END CLOSE_PO;
-------------------------------------------------------------------------------------------
FUNCTION INS_UPD_TSFDETAIL(O_error_message    IN OUT  VARCHAR2,
                           I_tsf_no           IN      TSFDETAIL.TSF_NO%TYPE,
                           I_item             IN      TSFDETAIL.ITEM%TYPE,
                           I_tsf_qty          IN      TSFDETAIL.TSF_QTY%TYPE,
                           I_reconciled_qty   IN      TSFDETAIL.RECONCILED_QTY%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64) := 'APPT_DOC_CLOSE_SQL.INS_UPD_TSFDETAIL';

   L_rowid                    ROWID := NULL;

   L_table                    VARCHAR2(30) := 'TSFDETAIL';
   L_key1                     VARCHAR2(100) := I_tsf_no;
   L_key2                     VARCHAR2(100) := I_item;
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);

   L_item_exist               VARCHAR2(1) := 'N';
   L_tsf_seq_no               TSFDETAIL.TSF_SEQ_NO%TYPE := 0;
   L_supp_pack_size           TSFDETAIL.SUPP_PACK_SIZE%TYPE;
   L_inner_pack_size          item_supp_country.inner_pack_size%TYPE;

   cursor C_tsfdetail_lock is
      select rowid
        from tsfdetail
       where tsf_no = I_tsf_no
         and item = I_item
         for update nowait;

   cursor C_item_exist is
      select 'Y'
        from tsfdetail
       where tsf_no = I_tsf_no
         and item = I_item;

   cursor C_get_tsf_seq_no is
      select max(tsf_seq_no) + 1
        from tsfdetail
       where tsf_no = I_tsf_no;
BEGIN
   ---
   open C_item_exist;
   fetch C_item_exist into L_item_exist;
   close C_item_exist;
   ---
   if L_item_exist = 'Y' then
      ---
      open C_tsfdetail_lock;
      fetch C_tsfdetail_lock into L_rowid;
      close C_tsfdetail_lock;
      ---
      update tsfdetail
         set tsf_qty = tsf_qty + I_tsf_qty,
             reconciled_qty = nvl(reconciled_qty, 0) + nvl(I_reconciled_qty, 0),
             updated_by_rms_ind = 'Y'
       where rowid = L_rowid;
      ---
   else
      ---
      open C_get_tsf_seq_no;
      fetch C_get_tsf_seq_no into L_tsf_seq_no;
      ---
      if C_get_tsf_seq_no%NOTFOUND then
         L_tsf_seq_no := 1;
      end if;
      ---
      close C_get_tsf_seq_no;
      ---
      if L_tsf_seq_no is null then
         L_tsf_seq_no := 1;
      end if;
      ---
      if ITEM_SUPP_COUNTRY_SQL.DEFAULT_PRIM_CASE_SIZE(O_error_message,
                                                      L_supp_pack_size,
                                                      L_inner_pack_size,
                                                      I_item) = FALSE then
         return FALSE;
      end if;
      ---
      insert into tsfdetail(tsf_no,
                            tsf_seq_no,
                            item,
                            tsf_qty,
                            reconciled_qty,
                            supp_pack_size,
                            publish_ind,
                            updated_by_rms_ind)
                     values(I_tsf_no,
                            L_tsf_seq_no,
                            I_item,
                            I_tsf_qty,
                            nvl(I_reconciled_qty, 0),
                            L_supp_pack_size,
                            'N',
                            'Y');
      ---
   end if;
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INS_UPD_TSFDETAIL;
----------------------------------------------------------------------------------------------
FUNCTION REBUILD_2ND_LEG_DETAIL(O_error_message         IN OUT  VARCHAR2,
                                I_1st_leg_tsf_no        IN      TSFDETAIL.TSF_NO%TYPE,
                                I_2nd_leg_tsf_no        IN      TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64) := 'APPT_DOC_CLOSE_SQL.REBUILD_2ND_LEG_DETAIL';

   L_rowid                    ROWID;
   L_table                    VARCHAR2(30) := 'TSFDETAIL';
   L_key1                     VARCHAR2(100) := I_2nd_leg_tsf_no;
   L_key2                     VARCHAR2(100);
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);


   L_item_exist               VARCHAR2(1) := 'N';
   L_tsf_seq_no               TSFDETAIL.TSF_SEQ_NO%TYPE := 0;
   L_supp_pack_size           TSFDETAIL.SUPP_PACK_SIZE%TYPE;
   L_inner_pack_size          ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE;
   L_comp_not_exist           VARCHAR2(1) := 'N';
   L_pack_qty                 NUMBER;
   L_to_pack_qty              TSFDETAIL.TSF_QTY%TYPE;
   L_item_master              ITEM_MASTER%ROWTYPE;
   L_tsf_xform_detail_id      TSF_XFORM_DETAIL.TSF_XFORM_DETAIL_ID%TYPE;
   L_to_item                  TSF_XFORM_DETAIL.TO_ITEM%TYPE;
   L_from_item                TSF_PACKING_DETAIL.ITEM%TYPE;
   L_set_no                   TSF_PACKING.SET_NO%TYPE;
   L_tsf_packing_id           TSF_PACKING.TSF_PACKING_ID%TYPE;
   L_packing_detail_id        TSF_PACKING_DETAIL.TSF_PACKING_DETAIL_ID%TYPE;
   L_return_code              BOOLEAN;

   cursor C_calc_2nd_leg_tsfqty is
      select item, sum(tsf_qty) tsf_qty, sum(reconciled_qty) reconciled_qty, tsf_price, supp_pack_size, publish_ind
        from (select t.item,
                     ss.qty_received tsf_qty,
                     (ss.qty_expected - ss.qty_received) reconciled_qty,
                     t.tsf_price,
                     t.supp_pack_size,
                     t.publish_ind
                from tsfdetail t,
                     shipsku ss
               where ss.distro_type = 'T'
                 and ss.distro_no = t.tsf_no
       and ss.item = t.item
                 and t.tsf_no = I_1st_leg_tsf_no
                 and ss.adjust_type is not null
                 and ss.adjust_type != 'FR'
               union all
              select t.item,
                     ss.qty_expected tsf_qty,
                     (ss.qty_expected - ss.qty_received) reconciled_qty,
                     t.tsf_price,
                     t.supp_pack_size,
                     t.publish_ind
                from tsfdetail t,
                     shipsku ss
               where ss.distro_type = 'T'
                 and ss.distro_no = t.tsf_no
       and ss.item = t.item
                 and t.tsf_no = I_1st_leg_tsf_no
                 and ss.adjust_type = 'FR'
               union all
              select t.item,
                     ss.qty_expected tsf_qty,
                     0 reconciled_qty,
                     t.tsf_price,
                     t.supp_pack_size,
                     t.publish_ind
                from tsfdetail t,
                     shipsku ss
               where ss.distro_type = 'T'
                 and ss.distro_no = t.tsf_no
       and ss.item = t.item
                 and t.tsf_no = I_1st_leg_tsf_no
                 and ss.adjust_type is NULL)
       group by item, tsf_price, supp_pack_size, publish_ind;

   cursor C_tsfdetail_lock(cv_item   tsfdetail.item%type) is
      select rowid
        from tsfdetail
       where tsf_no = I_2nd_leg_tsf_no
         and item = cv_item
         for update nowait;

   cursor C_xform_exist(cv_item  tsfdetail.item%type) is
      select tsf_xform_detail_id,
             to_item
        from tsf_xform tx,
             tsf_xform_detail txd
       where tx.tsf_xform_id  = txd.tsf_xform_id
         and tx.tsf_no = I_1st_leg_tsf_no
         and txd.from_item = cv_item;

   cursor C_comp_in_pack(cv_pack_no  v_packsku_qty.pack_no%type) is
      select item,
             qty
        from v_packsku_qty
       where pack_no = cv_pack_no;

   cursor C_item_exist(cv_item   tsfdetail.item%type) is
      select 'Y'
        from tsfdetail
       where tsf_no = I_2nd_leg_tsf_no
         and item = cv_item;

   ---
   -- Pack_no previously built on packing table

   cursor C_built_pack is
      select tp.tsf_packing_id,
             tp.set_no,
             tpd.item
        from tsf_packing tp,
             tsf_packing_detail tpd,
             item_master im
       where tp.tsf_packing_id = tpd.tsf_packing_id
         and tp.tsf_no = I_1st_leg_tsf_no
         and tpd.record_type = 'R'
         and tpd.item = im.item
         and im.pack_ind = 'Y';

   ---
   -- 2nd-leg does not have all components to re-build the pack

   cursor C_comp_not_exist (cv_pack_no  v_packsku_qty.pack_no%type) is
      select 'Y'
        from v_packsku_qty
       where pack_no = cv_pack_no
         and item not in (select item
                            from tsfdetail
            where tsf_no = I_2nd_leg_tsf_no);

   cursor C_comp_for_pack(cv_pack_no  v_packsku_qty.pack_no%type) is
      select t.item,
             trunc(t.tsf_qty/v.qty) pack_qty
        from tsfdetail t,
             v_packsku_qty v
       where t.item = v.item
         and v.pack_no = cv_pack_no
         and t.tsf_no = I_2nd_leg_tsf_no;
   cursor C_pack_to_pack(cv_packing_id tsf_packing_detail.tsf_packing_id%type,
                         cv_item       tsf_packing_detail.item%type) is
      select distinct tpd.qty,
                      tpd.item
        from tsf_packing tp,
             tsf_packing_detail tpd,
             v_packsku_qty v
       where tp.tsf_no = I_1st_leg_tsf_no
         and tp.tsf_packing_id = tpd.tsf_packing_id
         and tp.tsf_packing_id = cv_packing_id
         and tpd.record_type = 'F'
         and tpd.item = v.pack_no
         and exists (select 'x'
             from tsf_packing_detail td,
             v_packsku_qty v
            where td.tsf_packing_id = cv_packing_id
              and td.item = cv_item
         and td.item = v.pack_no);

   cursor C_calc_left_item(cv_pack_no  v_packsku_qty.pack_no%type) is
      select v.item,
             t.tsf_qty,
             (t.tsf_qty - (L_pack_qty * v.qty)) left_qty
        from tsfdetail t,
             v_packsku_qty v
       where t.item = v.item
         and t.tsf_no = I_2nd_leg_tsf_no
         and v.pack_no = cv_pack_no;

   cursor C_tpd_lock(cv_item          tsf_packing_detail.item%type,
                     cv_record_type   tsf_packing_detail.record_type%type) is
      select tpd.rowid
        from tsf_packing tp,
             tsf_packing_detail tpd
       where tp.tsf_packing_id = tpd.tsf_packing_id
         and tp.tsf_no = I_1st_leg_tsf_no
         and tpd.record_type = cv_record_type
         and tpd.item = cv_item
         for update nowait;

   cursor C_tpd_to_lock(cv_tsf_packing_id    tsf_packing_detail.tsf_packing_id%type,
                        cv_item              tsf_packing_detail.item%type) is
      select rowid
        from tsf_packing_detail
       where tsf_packing_id = cv_tsf_packing_id
         and record_type = 'R'
         and item = cv_item
         for update nowait;

   cursor C_tpd_pack_to_lock(cv_packing_id tsf_packing_detail.tsf_packing_id%type,
                         cv_item       tsf_packing_detail.item%type) is
      select tpd.rowid
        from tsf_packing tp,
             tsf_packing_detail tpd
       where tp.tsf_no = I_1st_leg_tsf_no
         and tp.tsf_packing_id = tpd.tsf_packing_id
         and tp.tsf_packing_id = cv_packing_id
         and tpd.record_type = 'R'
         and tpd.item = cv_item
    for update nowait;

   cursor C_txd_lock(cv_item  tsf_xform_detail.from_item%type) is
      select txd.rowid
        from tsf_xform tx,
             tsf_xform_detail txd
       where tx.tsf_xform_id = txd.tsf_xform_id
         and tx.tsf_no = I_1st_leg_tsf_no
         and txd.from_item = cv_item
         for update nowait;

   cursor C_get_seq_no is
      select max(tsf_seq_no) + 1
        from tsfdetail
       where tsf_no = I_2nd_leg_tsf_no;

   ---
   -- The PL/SQL table L_xform is used for updating the transformation info,
   -- its initial value is coming from the cursor C_xform_detail

   TYPE REC_xform IS RECORD(
      from_item       tsf_xform_detail.from_item%type,
      to_item         tsf_xform_detail.to_item%type,
      from_qty        tsf_xform_detail.from_qty%type);

   TYPE TBL_xform   IS TABLE OF REC_xform
      INDEX BY BINARY_INTEGER;

   L_xform     TBL_xform;
   L_idx       BINARY_INTEGER;

   cursor C_xform_detail is
      select txd.from_item,
             txd.to_item,
             0
        from tsf_xform tx,
             tsf_xform_detail txd
       where tx.tsf_xform_id = txd.tsf_xform_id
         and tx.tsf_no = I_1st_leg_tsf_no;
   ---
   -- After rebuild 2nd-leg, if initially exists a exploded pack, the packing
   -- instruction for this pack must also be updated based on shortage/overage

   cursor C_explode_pack is
      select tp.set_no,
             tpd.tsf_packing_id,
             tpd.item,
             tpd.qty
        from tsf_packing tp,
             tsf_packing_detail tpd,
        item_master im
       where tp.tsf_packing_id = tpd.tsf_packing_id
         and tpd.item = im.item
         and im.pack_ind = 'Y'
         and tp.tsf_no = I_1st_leg_tsf_no
         and tpd.record_type = 'F'
         and not exists (select 'x'
                           from tsf_packing_detail tpd2,
                      item_master im2
           where tpd2.tsf_packing_id = tpd.tsf_packing_id
             and tpd2.item = im2.item
             and im2.pack_ind = 'Y'
             and tpd2.record_type = 'R');

   ---
   -- The PL/SQL table L_packing is used for updating the packing instruction for a built pack,
   -- its initial available components is coming from the cursor C_avail_comp, the rebuilding
   -- process happens after rebuild 2nd-leg.

   cursor C_avail_comp(cv_tsf_packing_id   tsf_packing.tsf_packing_id%type,
                       cv_pack_no          v_packsku_qty.pack_no%type) is

      select v.item,
             v.qty comp_qty,
             avl.qty,
             0 left_qty,
             trunc(avl.qty/v.qty) pack_qty
        from v_packsku_qty v,
             (select item, sum(qty) qty
                from (select tpd.item, td.received_qty qty
                        from tsf_packing_detail tpd,
                             item_master im,
                             tsfdetail td
                       where tpd.item = im.item
                         and td.item = tpd.item
                         and td.tsf_no = I_1st_leg_tsf_no
                         and im.pack_ind = 'N'
                         and tpd.tsf_packing_id = cv_tsf_packing_id
                         and tpd.record_type = 'F'
                         and not exists (select 'x'
                                           from tsf_xform tx,
                       tsf_xform_detail txd
                 where tx.tsf_xform_id = txd.tsf_xform_id
                   and tx.tsf_no = td.tsf_no
                   and txd.from_item = tpd.item)
                       union all
            select v.item, (v.qty * td.received_qty) qty
                        from tsf_packing_detail tpd,
                             v_packsku_qty v,
                             tsfdetail td
                       where tpd.item = v.pack_no
                         and tpd.item = td.item
                         and td.tsf_no = I_1st_leg_tsf_no
                         and tpd.tsf_packing_id = cv_tsf_packing_id
                         and tpd.record_type = 'F'
                         and not exists (select 'x'
                                           from tsf_xform tx,
                       tsf_xform_detail txd
                 where tx.tsf_xform_id = txd.tsf_xform_id
                   and tx.tsf_no = td.tsf_no
                   and txd.from_item = v.item)
                       union all
                      select txd.to_item item, td.received_qty qty
                        from tsf_xform tx,
                             tsf_xform_detail txd,
                             tsf_packing_detail tpd,
                             tsfdetail td
                       where tx.tsf_xform_id = txd.tsf_xform_id
                         and tx.tsf_no = td.tsf_no
                         and tx.tsf_no = I_1st_leg_tsf_no
                         and txd.to_item = tpd.item
                         and txd.from_item = td.item
                         and tpd.record_type = 'F'
                         and tpd.tsf_packing_id = cv_tsf_packing_id
                       union all
                      select txd.to_item item, (td.received_qty * v.qty) qty
                        from tsf_xform tx,
                             tsf_xform_detail txd,
                        tsf_packing_detail tpd,
                        v_packsku_qty v,
                             tsfdetail td
                       where tx.tsf_xform_id = txd.tsf_xform_id
                         and tx.tsf_no = td.tsf_no
                         and tx.tsf_no = I_1st_leg_tsf_no
                         and txd.to_item = v.item
                         and v.pack_no = td.item
                         and v.pack_no = tpd.item
                         and tpd.record_type = 'F'
                         and tpd.tsf_packing_id = cv_tsf_packing_id)
                group by item) avl
       where avl.item = v.item
         and v.pack_no = cv_pack_no;

   TYPE REC_packing IS RECORD(
      item          tsf_packing_detail.item%type,
      comp_qty      tsf_packing_detail.qty%type,
      avail_qty     tsf_packing_detail.qty%type,
      left_qty      tsf_packing_detail.qty%type,
      pack_qty      tsf_packing_detail.qty%type);

   TYPE TBL_packing IS TABLE OF REC_packing
      INDEX BY BINARY_INTEGER;

   L_packing        TBL_packing;

BEGIN
   ---
   if I_1st_leg_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_1st_leg_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_2nd_leg_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_2nd_leg_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   -- Step 1. remove the initial 2nd-leg
   ---
   delete from tsfdetail
         where tsf_no = I_2nd_leg_tsf_no;
   ---
   L_xform.DELETE;
   ---
   open C_xform_detail;
   fetch C_xform_detail BULK COLLECT INTO L_xform;
   close C_xform_detail;

   -- step 2. rebuild 2nd-leg with initial items in the 1st-leg and the new qty after being reconciled,
   --         update the existing item tranformation instructions with the new quantity
   ---
   L_tsf_seq_no := 0;
   ---
   FOR REC_item IN C_calc_2nd_leg_tsfqty LOOP
      ---
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_master,
                                         REC_item.item) = FALSE then
         return FALSE;
      end if;
      ---
      if L_item_master.pack_ind = 'N' then
         ---
         L_tsf_xform_detail_id := NULL;
         L_to_item := NULL;
         ---
         open C_xform_exist(REC_item.item);
         fetch C_xform_exist into L_tsf_xform_detail_id,
                                  L_to_item;
         close C_xform_exist;
         ---
         if L_tsf_xform_detail_id is not null then  -- item has been transformed
            ---
            -- accumulate the transformed qty with the new transferred quantity
            ---
            FOR L_idx IN L_xform.first..L_xform.last LOOP
               if L_xform(L_idx).from_item = REC_item.item then
                  L_xform(L_idx).from_qty := L_xform(L_idx).from_qty + REC_item.tsf_qty;
                  EXIT;
               end if;
            END LOOP;

            -- insert/accumulate 2nd-leg tsfdetail with L_to_item/REC_item.qty
            ---
            if INS_UPD_TSFDETAIL(O_error_message,
                                 I_2nd_leg_tsf_no,
                                 L_to_item,
                                 REC_item.tsf_qty,
                                 REC_item.reconciled_qty) = FALSE then
               return FALSE;
            end if;
            ---
         else   -- item hasn't been transformed
            ---
            -- insert/accumulate 2nd-leg tsfdetail with REC_item.item/REC_item.qty
            ---
            if INS_UPD_TSFDETAIL(O_error_message,
                                 I_2nd_leg_tsf_no,
                                 REC_item.item,
                                 REC_item.tsf_qty,
                                 REC_item.reconciled_qty) = FALSE then
               return FALSE;
            end if;
            ---
         end if;
         ---
      else  -- pack item
         ---
         FOR REC_comp IN C_comp_in_pack(REC_item.item) LOOP
            ---
            L_tsf_xform_detail_id := NULL;
            ---
            open C_xform_exist(REC_comp.item);
            fetch C_xform_exist into L_tsf_xform_detail_id,
                                     L_to_item;
            close C_xform_exist;
            ---
            if L_tsf_xform_detail_id is not null then  -- component item has been transformed
               ---
               -- insert/accumulate 2nd-leg tsfdetail with L_to_item/REC_item.qty * REC_comp.qty
               ---
               if INS_UPD_TSFDETAIL(O_error_message,
                                    I_2nd_leg_tsf_no,
                                    L_to_item,
                                    REC_item.tsf_qty * REC_comp.qty,
                                    REC_item.reconciled_qty) = FALSE then
                  return FALSE;
               end if;
               ---
               -- Accumulate transformed qty
               ---
               FOR L_idx IN L_xform.first..L_xform.last LOOP
                  if L_xform(L_idx).from_item = REC_comp.item then
                     L_xform(L_idx).from_qty := L_xform(L_idx).from_qty + REC_item.tsf_qty * REC_comp.qty;
                     EXIT;
                  end if;
               END LOOP;

            else   -- component hasn't been transformed
               ---
               -- insert/accumulate 2nd-leg tsfdetail with REC_comp.item/REC_item.qty * REC_comp.qty
               ---
               if INS_UPD_TSFDETAIL(O_error_message,
                                    I_2nd_leg_tsf_no,
                                    REC_comp.item,
                                    REC_item.tsf_qty * REC_comp.qty,
                                    REC_item.reconciled_qty) = FALSE then
                  return FALSE;
               end if;
               ---
            end if;
            ---
         END LOOP;
         ---
      end if;
      ---
      -- Update packing instruction for "from-item" with new qty
      ---
      L_table := 'TSF_PACKING_DETAIL';
      L_key1 := I_1st_leg_tsf_no;
      L_key2 := REC_item.item;
      ---
      L_rowid := NULL;
      ---
      open C_tpd_lock(REC_item.item, 'F');
      fetch C_tpd_lock into L_rowid;
      close C_tpd_lock;
      ---
      if REC_item.tsf_qty = 0 then
         delete from tsf_packing_detail
               where rowid = L_rowid;
      else
         update tsf_packing_detail
            set qty   = REC_item.tsf_qty
          where rowid = L_rowid;
      end if;
      ---
   END LOOP;
   ---
   FOR L_idx IN 1..L_xform.count LOOP
      ---
      L_table := 'TSF_XFORM_DETAIL';
      L_key1 := I_1st_leg_tsf_no;
      L_key2 := L_xform(L_idx).from_item;
      ---
      L_rowid := NULL;
      ---
      open C_txd_lock(L_xform(L_idx).from_item);
      fetch C_txd_lock into L_rowid;
      close C_txd_lock;
      ---
      if L_xform(L_idx).from_qty != 0 then
         update tsf_xform_detail
            set from_qty = L_xform(L_idx).from_qty,
                  to_qty = L_xform(L_idx).from_qty
          where rowid = L_rowid;
      else
         delete from tsf_xform_detail
          where rowid = L_rowid;
      end if;
      ---
   END LOOP;
   ---
   FOR REC_explode_pack IN C_explode_pack LOOP
      ---
      if REC_explode_pack.item is not null and REC_explode_pack.qty != 0 then
         ---
         if ITEM_XFORM_PACK_SQL.EXPLODE_PACK(O_error_message,
                                             REC_explode_pack.item,
                                             REC_explode_pack.tsf_packing_id,
                                             I_1st_leg_tsf_no,
                                             REC_explode_pack.qty) = FALSE then
            return FALSE;
         end if;
         ---
      elsif REC_explode_pack.qty = 0 then
         ---
         if ITEM_XFORM_PACK_SQL.DELETE_TSF_PACKING(O_error_message,
                                                   I_1st_leg_tsf_no,
                                                   REC_explode_pack.set_no) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
   END LOOP;
   ---
   -- Step 3. For each built pack initially existing in the 1st-leg, re-estimate the possibility to rebuild it
   --         based on the newly created 2nd-leg.
   --         If the newly created 2nd-leg has all the required components and enought quantity for each component,
   --         rebuild it and update the 2nd-leg accordingly; Otherwise, remove the existing packing instruction.
   ---
   FOR REC_pack IN C_built_pack LOOP
      ---
      L_comp_not_exist := 'N';
      ---
      open C_comp_not_exist(REC_pack.item);
      fetch C_comp_not_exist into L_comp_not_exist;
      close C_comp_not_exist;
      ---
      if L_comp_not_exist = 'Y' then    -- the 2nd-leg does not have all required components
         ---
         -- delete the existing packing instruction
         ---
         if ITEM_XFORM_PACK_SQL.DELETE_TSF_PACKING(O_error_message,
                                                   I_1st_leg_tsf_no,
                                                   REC_pack.set_no) = FALSE then
            return FALSE;
         end if;
         ---
         L_pack_qty := 0;
         ---
      else  -- the 2nd-leg has all the required components to rebuild the pack
         ---
         -- Calculate the quantity of the pack which can be built
         ---
         L_pack_qty := 0;
         ---
         FOR REC_comp IN C_comp_for_pack(REC_pack.item) LOOP
            ---
            if REC_comp.pack_qty = 0 then  -- the component in 2nd-leg has no enough quantity to rebuild the pack
               ---
               -- delete the existing packing instruction
               ---
               if ITEM_XFORM_PACK_SQL.DELETE_TSF_PACKING(O_error_message,
                                                         I_1st_leg_tsf_no,
                                                         REC_pack.set_no) = FALSE then
                  return FALSE;
               end if;
               ---
               L_pack_qty := 0;
               EXIT;  --skip the current pack checking
               ---
            elsif L_pack_qty = 0 then
               L_pack_qty := REC_comp.pack_qty;    -- the qty can be packed based on the first component's tsf_qty
            elsif L_pack_qty > REC_comp.pack_qty then -- the component in 2nd-leg has enough quantity to rebuild the pack
               L_pack_qty := REC_comp.pack_qty;
            end if;
            ---
         END LOOP;
         ---
      end if;
      ---
      -- if the newly created 2nd-leg is eligible to rebuild the pack, rebuild it and update the 2nd-leg accordingly
      ---
      if L_pack_qty > 0 then
         ---
         -- For each component of pack being built, recalculate its left quantity after building
         -- and update the tsf_qty with left quantity on the 2nd-leg tsfdetail.
         ---
         FOR REC_comp_left IN C_calc_left_item(REC_pack.item) LOOP
            ---
            -- Update packing instruction of the from-item with the new tsf_qty
            ---
            L_table := 'TSFDETAIL';
            L_key2 := REC_comp_left.item;
            ---
            L_rowid := NULL;
            ---
            open C_tsfdetail_lock(REC_comp_left.item);
            fetch C_tsfdetail_lock into L_rowid;
            close C_tsfdetail_lock;
            ---
            if REC_comp_left.left_qty = 0 then  -- after rebuilding, the component has no quanity left
               ---
               delete from tsfdetail
                     where rowid = L_rowid;
               ---
            else
               update tsfdetail
                  set tsf_qty = REC_comp_left.left_qty,
                      updated_by_rms_ind = 'Y'
                where rowid = L_rowid;
               ---
            end if;
            ---
         END LOOP;
         ---
         -- insert the pack into 2nd-leg
         ---
         open C_get_seq_no;
         fetch C_get_seq_no into L_tsf_seq_no;
         close C_get_seq_no;
         ---
         if L_tsf_seq_no is null then
            L_tsf_seq_no := 1;
         end if;
         ---
         if ITEM_SUPP_COUNTRY_SQL.DEFAULT_PRIM_CASE_SIZE(O_error_message,
                                                         L_supp_pack_size,
                                                         L_inner_pack_size,
                                                         REC_pack.item) = FALSE then
            return FALSE;
         end if;
         ---
         insert into tsfdetail(tsf_no,
                               tsf_seq_no,
                               item,
                               tsf_qty,
                               supp_pack_size,
                               publish_ind,
                               updated_by_rms_ind)
                        values(I_2nd_leg_tsf_no,
                               L_tsf_seq_no,
                               REC_pack.item,
                               L_pack_qty,
                               L_supp_pack_size,
                               'N',
                               'Y');
         ---
      end if;
      ---
   END LOOP;
   ---
   -- Step 4. Update packing instruction for built pack
   ---
   L_pack_qty := 0;
   ---
   FOR REC_pack IN C_built_pack LOOP
      ---
      L_packing.DELETE;
      ---
      open C_avail_comp(REC_pack.tsf_packing_id, REC_pack.item);
      fetch C_avail_comp BULK COLLECT into L_packing;
      close C_avail_comp;
      ---
      FOR L_idx IN 1..L_packing.count LOOP
         ---
         if L_packing(L_idx).pack_qty = 0 then
            ---
            if ITEM_XFORM_PACK_SQL.DELETE_TSF_PACKING(O_error_message,
                                                      I_1st_leg_tsf_no,
                                                      REC_pack.set_no) = FALSE then
               return FALSE;
            end if;
            ---
            L_pack_qty := 0;
            EXIT;  --skip the current pack checking
            ---
         elsif L_pack_qty = 0 then
            L_pack_qty := L_packing(L_idx).pack_qty;    -- the qty can be packed based on the first component's pack_qty
         elsif L_pack_qty > L_packing(L_idx).pack_qty then
            L_pack_qty := L_packing(L_idx).pack_qty;
         end if;
         ---
      END LOOP;
      ---
      if L_pack_qty > 0 then
         FOR L_idx IN 1..L_packing.count LOOP
            ---
            L_packing(L_idx).left_qty := L_packing(L_idx).avail_qty - L_packing(L_idx).comp_qty * L_pack_qty;
            ---
            L_table := 'TSF_PACKING_DETAIL';
            L_key1 := I_1st_leg_tsf_no;
            L_key2 := L_packing(L_idx).item;
            L_rowid := NULL;
            ---
            open C_tpd_to_lock(REC_pack.tsf_packing_id, L_packing(L_idx).item);
            fetch C_tpd_to_lock into L_rowid;
            close C_tpd_to_lock;
            ---
            if L_rowid is not NULL then  -- resultant component existed in initial packing instructions
               ---
               if L_packing(L_idx).left_qty != 0 then
                  update tsf_packing_detail
                     set qty = L_packing(L_idx).left_qty
                   where rowid = L_rowid;
               else
                  delete from tsf_packing_detail
                        where rowid = L_rowid;
               end if;
               ---
            else  -- resulatnt component didn't exist in initial packing instructions
               ---
               if L_packing(L_idx).left_qty != 0 then  -- New packing may have leftover components
                  ---
                  if ITEM_XFORM_PACK_SQL.NEXT_PACKING_DETAIL_ID(O_error_message,
                                                                L_return_code,
                                                                L_packing_detail_id) = FALSE
                                                             or L_return_code = FALSE then
                     return FALSE;
                  end if;
                  ---
                  insert into tsf_packing_detail(tsf_packing_detail_id,
                                                 tsf_packing_id,
                                                 record_type,
                                                 item,
                                                 qty)
                                          values(L_packing_detail_id,
                                                 REC_pack.tsf_packing_id,
                                                 'R',
                                                 L_packing(L_idx).item,
                                                 L_packing(L_idx).left_qty);
                  ---
               end if;
               ---
            end if;
            ---
         END LOOP;
         ---
         L_key2 := REC_pack.item;
         L_rowid := NULL;
         ---
         open C_tpd_to_lock(REC_pack.tsf_packing_id, REC_pack.item);
         fetch C_tpd_to_lock into L_rowid;
         close C_tpd_to_lock;
         ---
         update tsf_packing_detail
            set qty = L_pack_qty
          where rowid = L_rowid;
         ---
      end if;
      ---
      -- if transfer is a pack to pack transfer, copy resultant quantity
      L_to_pack_qty := 0;
      open C_pack_to_pack(REC_pack.tsf_packing_id, REC_pack.item);
      fetch C_pack_to_pack into L_to_pack_qty,
                                L_from_item;
      close C_pack_to_pack;
      if L_to_pack_qty != 0 then
         L_key2 := REC_pack.item;
         L_rowid := NULL;
         open C_tpd_pack_to_lock(REC_pack.tsf_packing_id, REC_pack.item);
         fetch C_tpd_pack_to_lock into L_rowid;
         close C_tpd_pack_to_lock;
         ---
         update tsf_packing_detail
            set qty = L_to_pack_qty
          where rowid = L_rowid;
      end if;
      -- Update packing instruction for "from-item" with new qty
      ---
      FOR L_idx IN 1..L_packing.count LOOP
         ---
         L_key2 := L_packing(L_idx).item;
         ---
         L_rowid := NULL;
         ---
         open C_tpd_lock(L_packing(L_idx).item, 'F');
         fetch C_tpd_lock into L_rowid;
         close C_tpd_lock;
         ---
         if L_rowid is NOT NULL then
            update tsf_packing_detail
               set qty = L_packing(L_idx).avail_qty
             where rowid = L_rowid;
         end if;
         ---
      END LOOP;
      ---
   END LOOP;
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END REBUILD_2ND_LEG_DETAIL;
----------------------------------------------------------------------------------------------
FUNCTION CLOSE_TSF(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_closed         IN OUT BOOLEAN,
                   I_tsf_no         IN     APPT_DETAIL.DOC%TYPE)
RETURN BOOLEAN IS

   L_function                 VARCHAR2(50) := 'APPT_DOC_CLOSE_SQL.CLOSE_TSF';

   L_table                    VARCHAR2(30);
   L_key1                     VARCHAR2(100);
   L_key2                     VARCHAR2(100);
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(RECORD_LOCKED, -54);

   L_shipping_shortage_ind    BOOLEAN;
   L_shipping_overage_ind     VARCHAR2(1) := 'N';
   L_reconcile_pending_ind    VARCHAR2(1) := 'N';
   --   
   L_reconcile_pending3_ind   VARCHAR2(1) := 'N'; -- used for comparing tsf_qty and ship_qty
   ---
   L_tsf_parent_no            TSFHEAD.TSF_PARENT_NO%TYPE;
   L_1st_leg_status           TSFHEAD.STATUS%TYPE;
   L_2nd_leg_status           TSFHEAD.STATUS%TYPE;
   L_finisher_loc_ind         VARCHAR2(1);
   L_finisher_entity_ind      VARCHAR2(1);
   L_2nd_leg_tsf_no           TSFHEAD.TSF_NO%TYPE;
   L_1st_leg_adjust_type      SHIPSKU.ADJUST_TYPE%TYPE;
   ---
   L_system_options_row       SYSTEM_OPTIONS%ROWTYPE;
   L_auto_close_days          SYSTEM_OPTIONS.SS_AUTO_CLOSE_DAYS%TYPE;
   L_to_loc_type              SHIPMENT.TO_LOC_TYPE%TYPE;
   L_from_loc_type            SHIPMENT.FROM_LOC_TYPE%TYPE;
   L_vdate                    PERIOD.VDATE%TYPE;
   L_last_receipt_date        SHIPMENT.RECEIVE_DATE%TYPE;
   L_from_loc                 TSFHEAD.FROM_LOC%TYPE;
   L_to_loc                   TSFHEAD.TO_LOC%TYPE;

   cursor C_CHECK_SHIP_QTYS is
      select 'Y'
        from shipsku s
       where s.distro_no = I_tsf_no
         and (NVL(s.qty_expected, 0) - NVL(s.qty_received, 0)) > 0
         and (s.adjust_type is null or s.adjust_type = 'RE')
         and rownum = 1;

   cursor C_LOCK_TSFHEAD is
      select 'x'
        from tsfhead
       where tsf_no = I_tsf_no
         for update nowait;

   cursor C_GET_TSF_PARENT_NO is
      select nvl(tsf_parent_no, -1)
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_TSF_STATUS(cv_tsf_no  tsfhead.tsf_no%type) is
      select status
        from tsfhead
       where tsf_no = cv_tsf_no;

   cursor C_1ST_LEG_RECON is
      select adjust_type
        from shipsku
       where distro_type = 'T'
         and distro_no = I_tsf_no
         and adjust_type is not null;

   cursor C_ITEM_OVER_SHIP is
      select 'Y'
        from tsfdetail
       where tsf_no = I_tsf_no
         and tsf_qty < NVL(ship_qty, 0);

   cursor C_CHECK_TSF_UNSHIP_QTYS is
      select 'X'
        from tsfdetail
       where tsf_no = I_tsf_no
         and NVL(tsf_qty, 0) > NVL(ship_qty, 0)
         and rownum = 1;

   ---
   cursor C_GET_AUTO_CLOSE_INFO is
      select sh.to_loc_type,
             sh.from_loc_type,
             MAX(NVL(sh.receive_date,sh.ship_date)) receive_date,
             thead.from_loc,
             thead.to_loc
        from shipment sh,
             shipsku  sk,
             tsfhead  thead
       where sk.distro_no = I_tsf_no
         and sk.distro_no = thead.tsf_no
         and sk.distro_type = 'T'
         and sh.shipment = sk.shipment
    group by sh.to_loc_type,
             sh.from_loc_type,
             thead.from_loc,
             thead.to_loc;

   cursor C_GET_VDATE is
      select vdate
        from period;

BEGIN
   ---
   if I_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_function,
                                             NULL);
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---

   SQL_LIB.SET_MARK('OPEN','C_GET_AUTO_CLOSE_INFO','SHIPMENT,SHIPSKU',NULL);
   open C_GET_AUTO_CLOSE_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_AUTO_CLOSE_INFO','SHIPMENT,SHIPSKU',NULL);
   fetch C_GET_AUTO_CLOSE_INFO into L_to_loc_type,
                                    L_from_loc_type,
                                    L_last_receipt_date,
                                    L_from_loc,
                                    L_to_loc;
   SQL_LIB.SET_MARK('CLOSE','C_GET_AUTO_CLOSE_INFO','SHIPMENT,SHIPSKU',NULL);
   close C_GET_AUTO_CLOSE_INFO;
   
   ---
   if L_from_loc_type = 'S' and L_to_loc_type = 'S' then
      L_auto_close_days := L_system_options_row.ss_auto_close_days;
   elsif L_from_loc_type = 'S' and L_to_loc_type = 'W' then
      L_auto_close_days := L_system_options_row.sw_auto_close_days;
   elsif L_from_loc_type = 'W' and L_to_loc_type = 'W' then
      L_auto_close_days := L_system_options_row.ww_auto_close_days;
   elsif L_from_loc_type = 'W' and L_to_loc_type = 'S' then
      L_auto_close_days := L_system_options_row.ws_auto_close_days;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_VDATE','PERIOD',NULL);
   open C_GET_VDATE;
   SQL_LIB.SET_MARK('FETCH','C_GET_VDATE','PERIOD',NULL);
   fetch C_GET_VDATE into L_vdate;
   SQL_LIB.SET_MARK('CLOSE','C_GET_VDATE','PERIOD',NULL);
   close C_GET_VDATE;

   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_TSF_PARENT_NO','TSFHEAD',NULL);
   open C_GET_TSF_PARENT_NO;
   SQL_LIB.SET_MARK('FETCH','C_GET_TSF_PARENT_NO','TSFHEAD',NULL);
   fetch C_GET_TSF_PARENT_NO into L_tsf_parent_no;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TSF_PARENT_NO','TSFHEAD',NULL);
   close C_GET_TSF_PARENT_NO;
   ---
   if L_tsf_parent_no != -1 then   -- the transfer is the 2nd-leg tsf
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_TSF_STATUS','TSFHEAD',NULL);
      open C_GET_TSF_STATUS(L_tsf_parent_no);
      SQL_LIB.SET_MARK('FETCH','C_GET_TSF_STATUS','TSFHEAD',NULL);
      fetch C_GET_TSF_STATUS into L_1st_leg_status;
      SQL_LIB.SET_MARK('CLOSE','C_GET_TSF_STATUS','TSFHEAD',NULL);
      close C_GET_TSF_STATUS;
      ---
      if L_1st_leg_status != 'C' then  -- 1st-leg hasn't been closed, exit without further processing
         ---
         O_closed := FALSE;
         return TRUE;
      end if;
   end if;

   -- For single leg, multi-leg (1st leg or 2nd leg if the first leg is already closed),
   -- perform 2 kinds of adjustments if closing condition is met: 
   -- 1) Adjust unshipped qty (through TRANSFER_SQL.UPD_QTYS_WHEN_CLOSE)
   -- 2) Adjust shipped but unreceived qty (through STOCK_ORDER_RECONCILE_SQL.APPLY_ADJUSTMENTS_DOC_CLOSE)
   -- 3) Update TSFDETAIL.cancelled_qty (increment with unshipped qty)
   -- 4) Update TSFHEAD to 'C'losed
   ---
   -- Reconcile check for Non-catch_weight simple pack component items
   --
   SQL_LIB.SET_MARK('OPEN','C_CHECK_SHIP_QTYS','SHIPSKU',NULL);
   open C_CHECK_SHIP_QTYS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_SHIP_QTYS','SHIPSKU',NULL);
   fetch C_CHECK_SHIP_QTYS into L_reconcile_pending_ind;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_SHIP_QTYS','SHIPSKU',NULL);
   close C_CHECK_SHIP_QTYS;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_TSF_UNSHIP_QTYS','TSFDETAIL',NULL);
   open C_CHECK_TSF_UNSHIP_QTYS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_TSF_UNSHIP_QTYS','TSFDETAIL',NULL);
   fetch C_CHECK_TSF_UNSHIP_QTYS into L_reconcile_pending3_ind;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_TSF_UNSHIP_QTYS','TSFDETAIL',NULL);
   close C_CHECK_TSF_UNSHIP_QTYS;

   -- Transfer will be closed in the following conditions
   -- 1. If the transfer is fully shipped and fully received or over received then will be closed (will be closed on receiving date).
   -- 2. If the transfer is fully shipped and partially received then close the transfer on close delay.
   -- 3. If the transfer is partially shipped and partially received then it will be closed on close delay.
   -- 4. If the transfer is partially shipped and fully received then it will be closed on close delay.
   -- CLOSE DELAY - date is equal to or greater than (last receipt date + L_auto_close_days)
   -- L_reconcile_pending3_ind - Partial / Full Shipping indicator
   -- L_reconcile_pending_ind - Partial / Full Receiving indicator

   if (L_reconcile_pending3_ind <> 'X' and L_reconcile_pending_ind <> 'Y') or                   -- fully shipped and fully received, no auto close condition
      (L_auto_close_days is NOT NULL and L_reconcile_pending_ind = 'Y' and
       L_reconcile_pending3_ind <> 'X' and L_vdate >= L_last_receipt_date + L_auto_close_days) or -- fully shipped and partially received and auto close
      (L_auto_close_days is NOT NULL and L_reconcile_pending_ind = 'Y' and
       L_reconcile_pending3_ind = 'X' and L_vdate >= L_last_receipt_date + L_auto_close_days) or -- Partially shipped and partially received  and auto close
      (L_auto_close_days is NOT NULL and L_reconcile_pending_ind <> 'Y' and
       L_reconcile_pending3_ind = 'X' and L_vdate >= L_last_receipt_date + L_auto_close_days) then -- Partially shipped and fully received  and auto close

      -- Perform adjustment for shipped but not received qty for the following 2 scenarios: 
      -- 1) Fully shipped and partially received and auto close date is met
      -- 2) Partially shipped and partially received and auto close date is met
      if (L_auto_close_days is NOT NULL and L_reconcile_pending_ind = 'Y'
           and L_vdate >= L_last_receipt_date + L_auto_close_days) then
         ---
         if STOCK_ORDER_RECONCILE_SQL.APPLY_ADJUSTMENTS_DOC_CLOSE (O_error_message,
                                                                   I_tsf_no,
                                                                   'T') = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      -- update tsf_reserved_qty of from loc and tsf_expected_qty of to loc for short shipped quantity
      ---
      if L_tsf_parent_no = -1 then
         if TRANSFER_SQL.UPD_QTYS_WHEN_CLOSE(O_error_message,
                                             L_shipping_shortage_ind,
                                             '1', --process 1st leg only
                                             I_tsf_no) = FALSE then
            return FALSE;
         end if;
      else
         if TRANSFER_SQL.UPD_QTYS_WHEN_CLOSE(O_error_message,
                                             L_shipping_shortage_ind,
                                             '2', --process 2nd leg only
                                             I_tsf_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      L_table := 'TSFHEAD';
      L_key1 := I_tsf_no;
      L_key2 := NULL;

      SQL_LIB.SET_MARK('OPEN','C_LOCK_TSFHEAD','TSFHEAD',NULL);
      open C_LOCK_TSFHEAD;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_TSFHEAD','TSFHEAD',NULL);
      close C_LOCK_TSFHEAD;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'TSFHEAD','TSF_NO:'||I_tsf_no);
      ---
      update tsfhead
         set status = 'C',
             close_date = L_vdate
       where tsf_no = I_tsf_no
         and status != 'C';
      ---
      O_closed := TRUE;
      ---
      -- Rebuild the 2nd leg if the 1st leg is adjusted through closing
      if L_tsf_parent_no = -1 then
         ---
         SQL_LIB.SET_MARK('OPEN','C_1ST_LEG_RECON','SHIPSKU',NULL);
         open C_1ST_LEG_RECON;
         SQL_LIB.SET_MARK('FETCH','C_1ST_LEG_RECON','SHIPSKU',NULL);
         fetch C_1ST_LEG_RECON into L_1st_leg_adjust_type;
         SQL_LIB.SET_MARK('CLOSE','C_1ST_LEG_RECON','SHIPSKU',NULL);
         close C_1ST_LEG_RECON;
         ---
         -- check whether the shipping overage exists
         ---
         SQL_LIB.SET_MARK('OPEN','C_ITEM_OVER_SHIP','TSFDETAIL',NULL);
         open C_ITEM_OVER_SHIP;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_OVER_SHIP','TSFDETAIL',NULL);
         fetch C_ITEM_OVER_SHIP into L_shipping_overage_ind;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_OVER_SHIP','TSFDETAIL',NULL);
         close C_ITEM_OVER_SHIP;
         ---
         if L_shipping_shortage_ind = TRUE
            or L_shipping_overage_ind = 'Y'
            or (L_1st_leg_adjust_type in ('FC','SL','RL')) then
            ---
            if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                              L_finisher_loc_ind,
                                              L_finisher_entity_ind,
                                              I_tsf_no) = FALSE then
               return FALSE;
            end if;
            ---
            if L_finisher_entity_ind is not NULL then   -- the transfer is the 1st-leg
               ---
               if TRANSFER_SQL.GET_CHILD_TSF(O_error_message,
                                             L_2nd_leg_tsf_no,
                                             I_tsf_no) = FALSE then
                  return FALSE;
               end if;
               ---
               SQL_LIB.SET_MARK('OPEN','C_GET_TSF_STATUS','TSFHEAD',NULL);
               open C_GET_TSF_STATUS(L_2nd_leg_tsf_no);
               SQL_LIB.SET_MARK('FETCH','C_GET_TSF_STATUS','TSFHEAD',NULL);
               fetch C_GET_TSF_STATUS into L_2nd_leg_status;
               SQL_LIB.SET_MARK('CLOSE','C_GET_TSF_STATUS','TSFHEAD',NULL);
               close C_GET_TSF_STATUS;
               ---
               if L_2nd_leg_status = 'A' then
                  ---
                  if REBUILD_2ND_LEG_DETAIL(O_error_message,
                                            I_tsf_no,
                                            L_2nd_leg_tsf_no) = FALSE then
                     return FALSE;
                  end if;
                  ---
               end if;
               ---
            end if;
            ---
         end if;
         ---
      end if;  -- L_tsf_parent_no != -1
   else
      -- If doesn't satisfy condn for autoclosing or auto_close param
      O_closed := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;

END CLOSE_TSF;
----------------------------------------------------------------------------------------------
FUNCTION CLOSE_ALLOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_closed         IN OUT BOOLEAN,
                     I_alloc          IN     APPT_DETAIL.DOC%TYPE)
RETURN BOOLEAN IS
   L_function     VARCHAR2(50) := 'APPT_DOC_CLOSE_SQL.CLOSE_ALLOC';
   L_flag         VARCHAR2(1)  := NULL;
   L_rowid        ROWID;
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   L_alloc_source         ALLOC_NO_TBL := NULL;
   L_alloc_open_source    ALLOC_NO_TBL := NULL;

   cursor C_CHECK_SHIP_QTYS is
      select 'x'
        from shipsku s,
             item_master im
       where s.distro_no = I_alloc
         and (NVL(s.qty_expected, 0) - NVL(s.qty_received, 0)) <> 0
         and (s.adjust_type is null or s.adjust_type = 'RE')
         and im.item = s.item
         and rownum = 1;

   cursor C_CHECK_SHIP_WEIGHTS is
      select 'x'
        from shipsku s,
             item_master im
       where s.distro_no = I_alloc
         and (NVL(s.weight_expected, 0) - NVL(s.weight_received, 0)) <> 0
         and (adjust_type is null or adjust_type = 'RE')
         and im.item = s.item
         and im.catch_weight_ind = 'Y'
         and im.simple_pack_ind = 'Y'
         and rownum = 1;

   cursor C_CHECK_ALLOC_REC_QTYS is
      select 'x'
        from alloc_detail
       where alloc_no = I_alloc
         and NVL(qty_allocated, 0) > NVL(qty_transferred, 0)
         and NVL(qty_allocated, 0) > NVL(qty_received, 0)
         and rownum = 1;

   cursor C_LOCK_ALLOC_HEADER is
      select rowid
        from alloc_header
       where alloc_no = I_alloc
         for update nowait;


BEGIN
   O_closed := FALSE;

   L_alloc_source := ALLOC_NO_TBL();
   L_alloc_source.extend();
   L_alloc_source(1) := I_alloc;

   if CHECK_OPEN_ALLOC_SOURCE(O_error_message,
                              L_alloc_open_source,
                              L_alloc_source) = FALSE then
      return FALSE;
   end if;

   -- if the collection has a value, the allocation source is still open
   -- and the allocation cannot be closed.
   if L_alloc_open_source is NOT NULL and L_alloc_open_source.COUNT > 0 then
      O_closed := FALSE;
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_SHIP_QTYS','SHIPSKU',NULL);
   open C_CHECK_SHIP_QTYS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_SHIP_QTYS','SHIPSKU',NULL);
   fetch C_CHECK_SHIP_QTYS into L_flag;
   ---
   if C_CHECK_SHIP_QTYS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_SHIP_QTYS','SHIPSKU',NULL);
      close C_CHECK_SHIP_QTYS;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_SHIP_QTYS','SHIPSKU',NULL);
   close C_CHECK_SHIP_QTYS;


   SQL_LIB.SET_MARK('OPEN','C_CHECK_SHIP_WEIGHTS','SHIPSKU',NULL);
   open C_CHECK_SHIP_WEIGHTS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_SHIP_WEIGHTS','SHIPSKU',NULL);
   fetch C_CHECK_SHIP_WEIGHTS into L_flag;
   ---
   if C_CHECK_SHIP_WEIGHTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_SHIP_WEIGHTS','SHIPSKU',NULL);
      close C_CHECK_SHIP_WEIGHTS;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_SHIP_WEIGHTS','SHIPSKU',NULL);
   close C_CHECK_SHIP_WEIGHTS;


   SQL_LIB.SET_MARK('OPEN','C_CHECK_ALLOC_REC_QTYS','ALLOC_DETAIL',NULL);
   open C_CHECK_ALLOC_REC_QTYS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_ALLOC_REC_QTYS','ALLOC_DETAIL',NULL);
   fetch C_CHECK_ALLOC_REC_QTYS into L_flag;
   ---
   if C_CHECK_ALLOC_REC_QTYS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_ALLOC_REC_QTYS','ALLOC_DETAIL',NULL);
      close C_CHECK_ALLOC_REC_QTYS;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_ALLOC_REC_QTYS','ALLOC_DETAIL',NULL);
   close C_CHECK_ALLOC_REC_QTYS;

   SQL_LIB.SET_MARK('UPDATE',NULL,'ALLOC_HEADER','ALLOC_NO:'||I_alloc);
   open C_LOCK_ALLOC_HEADER;
   fetch C_LOCK_ALLOC_HEADER into L_rowid;
   close C_LOCK_ALLOC_HEADER;
   update alloc_header
      set status = 'C',
          close_date = GET_VDATE
    where rowid  = L_rowid;

   O_closed := TRUE;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                             'ALLOC_HEADER',
                                             'Alloc.: '||to_char(I_alloc),
                                             L_function);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;

END CLOSE_ALLOC;
----------------------------------------------------------------------------------------------
FUNCTION CLOSE_DOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_closed         IN OUT BOOLEAN,
                   I_appt_head      IN     APPT_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_function VARCHAR2(50) := 'APPT_DOC_CLOSE_SQL.CLOSE_DOC';
   L_open     BOOLEAN      := FALSE;
   ---
   cursor C_FETCH_DOCS is
      select doc_type, doc
        from appt_detail
       where appt = I_appt_head.appt;

BEGIN
   FOR rec in C_FETCH_DOCS LOOP

      if CHECK_OPEN_APPTS(O_error_message,
                          L_open,
                          rec.doc,
                          rec.doc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if not L_open then
         if rec.doc_type = 'P' then
            if CLOSE_PO(O_error_message,
                        O_closed,
                        rec.doc) = FALSE then
               return FALSE;
            end if;
         elsif rec.doc_type = 'T' then
            if CLOSE_TSF(O_error_message,
                         O_closed,
                         rec.doc) = FALSE then
               return FALSE;
            end if;
         elsif rec.doc_type = 'A' then
            if CLOSE_ALLOC(O_error_message,
                           O_closed,
                           rec.doc) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;

END CLOSE_DOC;
----------------------------------------------------------------------------------------------
FUNCTION CLOSE_ALL_ALLOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   TYPE ROWID_TBL is table of ROWID;
   TYPE QTY_CANCELLED_TBL is table of ALLOC_DETAIL.QTY_CANCELLED%TYPE;

   L_function             VARCHAR2(50) := 'APPT_DOC_CLOSE_SQL.CLOSE_ALL_ALLOCS';

   L_rowids               ROWID_TBL;
   L_rowids2              ROWID_TBL;
   L_alloc_no1            ALLOC_NO_TBL;
   L_alloc_no2            ALLOC_NO_TBL;
   L_alloc_no_param       ALLOC_NO_TBL;
   L_alloc_source         ALLOC_NO_TBL;
   L_alloc_open_source    ALLOC_NO_TBL;
   L_qty_cancelled        QTY_CANCELLED_TBL;
   ---
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_ws_auto_close_days   SYSTEM_OPTIONS.WS_AUTO_CLOSE_DAYS%TYPE;
   L_ww_auto_close_days   SYSTEM_OPTIONS.WW_AUTO_CLOSE_DAYS%TYPE;
   L_auto_close_days      SYSTEM_OPTIONS.SS_AUTO_CLOSE_DAYS%TYPE;
   L_vdate                PERIOD.VDATE%TYPE;
   L_alloc_no             ALLOC_NO_TBL := ALLOC_NO_TBL();
   L_single_alloc         ALLOC_HEADER.ALLOC_NO%TYPE;
   L_last_receipt_date    SHIPMENT.RECEIVE_DATE%TYPE;

   L_table                VARCHAR2(30); 
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_CHECK_SHIP_QTYS is
     select dcq.doc
       from doc_close_queue_temp dcq
      where dcq.doc_type = 'A'
        and not exists (select /*+ first_rows(1) */ 'x'
                          from shipsku
                         where distro_no = dcq.doc
                           and NVL(qty_expected, 0) != NVL(qty_received,0)
                           and NVL(adjust_type, 'RE') = 'RE'
                           and rownum = 1);

   cursor C_CHECK_ALLOC_REC_QTYS is
     select /*+ CARDINALITY(I 100) */ ah.alloc_no, ah.rowid
       from TABLE(CAST(L_alloc_no1 as ALLOC_NO_TBL)) i,
            alloc_header ah
      where ah.alloc_no = value(i)
        and (not exists (select /*+ first_rows(1) */ 'x'
                          from alloc_detail ad
                         where ad.alloc_no = ah.alloc_no
                           and ad.qty_allocated > NVL(ad.qty_transferred, 0)
                           and ad.qty_allocated > NVL(ad.qty_received, 0)
                           and rownum = 1)
             or ((exists (select 'x'
                            from tsfhead th
                           where th.tsf_no = ah.order_no
                             and ah.doc_type = 'TSF'
                             and th.status = 'C')
               or exists (select 'x'
                            from ordhead oh
                           where oh.order_no = ah.order_no
                             and ah.doc_type = 'PO'
                             and oh.status = 'C')
               or exists (select 'x'
                            from alloc_header ah2
                           where ah2.alloc_no = ah.order_no
                             and ah.doc_type = 'ALLOC'
                             and ah2.status = 'C'))
            and exists (select /*+ first_rows(1) */ 'x'
                          from alloc_detail ad
                         where ad.alloc_no = ah.alloc_no
                           and ad.qty_allocated > NVL(ad.qty_transferred, 0)
                           and ad.qty_allocated > NVL(ad.qty_received, 0)
                           and rownum = 1)))
        for update of ah.status nowait;

   cursor C_ALLOC_DETAIL_QTY is
     select ad.qty_allocated - NVL(ad.qty_transferred, 0) qty_cancelled,
            ad.rowid rid
      from  TABLE(CAST(L_alloc_no2 as ALLOC_NO_TBL)) i,
            alloc_header ah,
            alloc_detail ad,
            wh from_wh,
            wh to_wh
      where ah.alloc_no = value(i)
        and ah.alloc_no = ad.alloc_no
        and ah.status = 'C'
        and ah.wh = from_wh.wh
        and ad.to_loc = to_wh.wh
        and from_wh.physical_wh = to_wh.physical_wh
        and ad.qty_allocated - NVL(ad.qty_transferred, 0) > 0
        for update of ad.qty_cancelled nowait;

   cursor C_LOCK_DCQ(L_alloc_no_param ALLOC_NO_TBL) is
     select 'x'
       from TABLE(CAST(L_alloc_no_param as ALLOC_NO_TBL)) i,
            doc_close_queue dcq
      where dcq.doc = value(i)
        for update of dcq.doc nowait;

   cursor C_GET_VDATE is
      select vdate
        from period;

   cursor C_GET_REMAINING_ALLOCS is
     select ah.alloc_no,
            acd.auto_close_days,
            acd.receive_date
       from (select dcqt.doc,
                     max(decode(sh.to_loc_type,'S',L_ws_auto_close_days,
                                               'W',L_ww_auto_close_days)) auto_close_days,
                     max(sh.receive_date) receive_date
                from (select doc
                        from doc_close_queue_temp
                       where doc_type = 'A') dcqt,
                     shipsku sk,
                     shipment sh
               where sk.distro_no = dcqt.doc
                 and sh.shipment = sk.shipment
                 and sh.status_code = 'R'
                 and (   (sh.to_loc_type = 'S' and L_ws_auto_close_days is not null)
                      or (sh.to_loc_type = 'W' and L_ww_auto_close_days is not null)
                      )
               group by dcqt.doc) acd,
             alloc_header ah
       where acd.doc = ah.alloc_no
        for update of ah.status nowait;

   --Returns allocations that are sourced from an inbound receipt of another document (e.g. PO, TSF, ALLOC, ASN, BOL).
   --These allocations cannot be closed until the sourcing document is closed.
   cursor C_ALLOC_SOURCE is
      select ah.alloc_no 
        from doc_close_queue_temp dq,
             alloc_header ah
       where dq.doc_type = 'A'
         and dq.doc = to_char(ah.alloc_no)
         and (   ah.doc_type = 'PO' and ah.order_no is NOT NULL
              or ah.doc_type = 'TSF' and ah.order_no is NOT NULL
              or ah.doc_type = 'ALLOC' and ah.order_no is NOT NULL
              or ah.doc_type = 'ASN' and ah.doc is NOT NULL
              or ah.doc_type = 'BOL' and ah.doc is NOT NULL);

BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   L_ws_auto_close_days := L_system_options_row.ws_auto_close_days;
   L_ww_auto_close_days := L_system_options_row.ww_auto_close_days;

   open C_GET_VDATE;
   fetch C_GET_VDATE into L_vdate;
   close C_GET_VDATE;

   --Allocations that are sourced from an inbound receipt of another document (e.g. PO, TSF, ALLOC, ASN, BOL)
   --cannot be closed until the sourcing document is closed. These allocations are bulk deleted from the
   --doc_close_queue_temp table to prevent them from being closed.
   SQL_LIB.SET_MARK('OPEN', 'C_ALLOC_SOURCE', 'DOC_CLOSE_QUEUE_TEMP', NULL);
   open C_ALLOC_SOURCE;
   SQL_LIB.SET_MARK('FETCH', 'C_ALLOC_SOURCE', 'DOC_CLOSE_QUEUE_TEMP', NULL);
   fetch C_ALLOC_SOURCE BULK COLLECT into L_alloc_source;
   SQL_LIB.SET_MARK('CLOSE', 'C_ALLOC_SOURCE', 'DOC_CLOSE_QUEUE_TEMP', NULL);
   close C_ALLOC_SOURCE;

   if CHECK_OPEN_ALLOC_SOURCE(O_error_message,
                              L_alloc_open_source,
                              L_alloc_source) = FALSE then
      return FALSE;
   end if;

   if L_alloc_open_source is NOT NULL and L_alloc_open_source.COUNT > 0 then
      SQL_LIB.SET_MARK('DELETE', NULL, 'DOC_CLOSE_QUEUE_TEMP', NULL);
      FORALL i in 1..L_alloc_open_source.COUNT
         delete doc_close_queue_temp
          where doc_type = 'A'
            and doc = L_alloc_open_source(i);
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SHIP_QTYS', 'SHIPSKU', NULL);
   open  C_CHECK_SHIP_QTYS;

   LOOP
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SHIP_QTYS', 'SHIPSKU', NULL);
      fetch C_CHECK_SHIP_QTYS BULK COLLECT INTO L_alloc_no1 LIMIT 100;

      if ( L_alloc_no1.COUNT > 0 ) then
      
         L_table := 'ALLOC_HEADER';
         SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ALLOC_REC_QTYS', 'ALLOC_DETAIL', NULL);
         open  C_CHECK_ALLOC_REC_QTYS;
         fetch C_CHECK_ALLOC_REC_QTYS BULK COLLECT INTO L_alloc_no2, L_rowids;
         close C_CHECK_ALLOC_REC_QTYS;

         SQL_LIB.SET_MARK('UPDATE', NULL, 'ALLOC_HEADER', NULL);
         forall i in 1..L_rowids.count
            update alloc_header
               set status = 'C',
                   close_date = L_vdate
             where ROWID = L_rowids(i);
         ---
         L_table := 'ALLOC_DETAIL';
         SQL_LIB.SET_MARK('OPEN', 'C_ALLOC_DETAIL_QTY', 'ALLOC_DETAIL', NULL);
         open  C_ALLOC_DETAIL_QTY;
         fetch C_ALLOC_DETAIL_QTY BULK COLLECT INTO L_qty_cancelled, L_rowids2;
         close C_ALLOC_DETAIL_QTY;

         SQL_LIB.SET_MARK('UPDATE', NULL, 'ALLOC_HEADER', NULL);
         forall i in 1..L_rowids2.count
            update alloc_detail
               set qty_cancelled = L_qty_cancelled(i)
             where ROWID = L_rowids2(i);
         ---
         L_table := 'DOC_CLOSE_QUEUE';
         SQL_LIB.SET_MARK('OPEN','C_LOCK_DCQ','DOC_CLOSE_QUEUE',NULL);
         open C_LOCK_DCQ(L_alloc_no2);
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_DCQ','DOC_CLOSE_QUEUE',NULL);
         close C_LOCK_DCQ;

         SQL_LIB.SET_MARK('DELETE', NULL, 'DOC_CLOSE_QUEUE', NULL);
         forall i in 1..L_alloc_no2.count
            delete doc_close_queue
             where doc_type = 'A'
               and doc = L_alloc_no2(i);

         SQL_LIB.SET_MARK('DELETE', NULL, 'DOC_CLOSE_QUEUE_TEMP', NULL);
         forall i in 1..L_alloc_no2.count
            delete doc_close_queue_temp
             where doc_type = 'A'
               and doc = L_alloc_no2(i);
      end if;

      exit when C_CHECK_SHIP_QTYS%NOTFOUND;
   END LOOP;

   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SHIP_QTYS', 'SHIPSKU', NULL);
   close C_CHECK_SHIP_QTYS;

   L_table := 'ALLOC_HEADER';
   FOR rec IN C_GET_REMAINING_ALLOCS LOOP
      L_single_alloc      := rec.alloc_no;
      L_auto_close_days   := rec.auto_close_days;
      L_last_receipt_date := rec.receive_date;

      if (L_vdate >= (L_last_receipt_date + L_auto_close_days)) then
         ---
         if STOCK_ORDER_RECONCILE_SQL.APPLY_ADJUSTMENTS_DOC_CLOSE (O_error_message,
                                                                   L_single_alloc,
                                                                   'A') = FALSE then
            return FALSE;
         end if;
         ---
         if ALLOC_ATTRIB_SQL.UPD_QTYS_WHEN_CLOSE(O_error_message,
                                                 L_single_alloc) = FALSE then
            return FALSE;
         end if;
         ---
         L_alloc_no.extend();
         L_alloc_no(L_alloc_no.COUNT) := L_single_alloc;
      end if;

   END LOOP;

   if ( L_alloc_no.COUNT > 0 ) then
      SQL_LIB.SET_MARK('UPDATE', NULL, 'ALLOC_HEADER', NULL);
      forall i in 1..L_alloc_no.COUNT
         update alloc_header ah
            set status = 'C',
                close_date = L_vdate
          where ah.alloc_no = L_alloc_no(i)
            and not exists (select 'x'
                              from shipment sh,
                                   shipsku ss
                             where ss.distro_no = ah.alloc_no
                               and ss.shipment = sh.shipment
                               and sh.status_code <> 'R'
                               and rownum = 1);

      ---
      L_table := 'DOC_CLOSE_QUEUE';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_DCQ','DOC_CLOSE_QUEUE',NULL);
      open C_LOCK_DCQ(L_alloc_no);
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_DCQ','DOC_CLOSE_QUEUE',NULL);
      close C_LOCK_DCQ;
         
      SQL_LIB.SET_MARK('DELETE', NULL, 'DOC_CLOSE_QUEUE', NULL);
      forall i in 1..L_alloc_no.COUNT
         delete doc_close_queue
          where doc_type = 'A'
            and doc = L_alloc_no(i);

      SQL_LIB.SET_MARK('DELETE', NULL, 'DOC_CLOSE_QUEUE_TEMP', NULL);
      forall i in 1..L_alloc_no.COUNT
         delete doc_close_queue_temp
          where doc_type = 'A'
            and doc = L_alloc_no(i);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_CHECK_SHIP_QTYS%ISOPEN then
         close C_CHECK_SHIP_QTYS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_MODULE',
                                            L_table,
                                            NULL,
                                            L_function);
      return FALSE;

   when OTHERS then
      if C_CHECK_SHIP_QTYS%ISOPEN then
         close C_CHECK_SHIP_QTYS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;

END CLOSE_ALL_ALLOCS;
----------------------------------------------------------------------------------------------
FUNCTION CHECK_OPEN_ALLOC_SOURCE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_open_alloc_no_tbl   IN OUT   ALLOC_NO_TBL,
                                 I_alloc_no_tbl        IN OUT   ALLOC_NO_TBL)
RETURN BOOLEAN IS
   L_function VARCHAR2(50) := 'APPT_DOC_CLOSE_SQL.CHECK_OPEN_ALLOC_SOURCE';

   cursor C_OPEN_ALLOC_SOURCE is
      select value(i)
        from TABLE(CAST(I_alloc_no_tbl as ALLOC_NO_TBL)) i,
             alloc_header ah,
             ordhead oh
       where ah.alloc_no = value(i)
         and ah.doc_type = 'PO'
         and ah.order_no is NOT NULL
         and ah.order_no = oh.order_no
         and oh.status != 'C'
       UNION
      select value(i)
        from TABLE(CAST(I_alloc_no_tbl as ALLOC_NO_TBL)) i,
             alloc_header ah,
             tsfhead tsf
       where ah.alloc_no = value(i)
         and ah.doc_type = 'TSF'
         and ah.order_no is NOT NULL
         and ah.order_no = tsf.tsf_no
         and tsf.status != 'C'
       UNION
      select value(i)
        from TABLE(CAST(I_alloc_no_tbl as ALLOC_NO_TBL)) i,
             alloc_header ah,
             alloc_header ah2
       where ah.alloc_no = value(i)
         and ah.doc_type = 'ALLOC'
         and ah.order_no is NOT NULL
         and ah.order_no = ah2.alloc_no
         and ah2.status != 'C'
       UNION
      select value(i)
        from TABLE(CAST(I_alloc_no_tbl as ALLOC_NO_TBL)) i,
             alloc_header ah,
             shipment s
       where ah.alloc_no = value(i)
         and ah.doc_type = 'ASN'
         and ah.doc is NOT NULL
         and ah.doc = s.asn
         and (s.status_code != 'R'
          or exists (select 'x'
                       from shipsku ss
                      where ss.shipment = s.shipment
                        and NVL(ss.qty_received,0) < ss.qty_expected
                        and rownum = 1))
       UNION
      select value(i)
        from TABLE(CAST(I_alloc_no_tbl as ALLOC_NO_TBL)) i,
             alloc_header ah,
             shipment s
       where ah.alloc_no = value(i)
         and ah.doc_type = 'BOL'
         and ah.doc is NOT NULL
         and ah.doc = s.bol_no
         and (s.status_code != 'R'
          or exists (select 'x'
                       from shipsku ss
                      where ss.shipment = s.shipment
                        and NVL(ss.qty_received,0) < ss.qty_expected
                        and rownum = 1));

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_OPEN_ALLOC_SOURCE', 'I_alloc_no_tbl', NULL);
   open C_OPEN_ALLOC_SOURCE;
   SQL_LIB.SET_MARK('FETCH', 'C_OPEN_ALLOC_SOURCE', 'I_alloc_no_tbl', NULL);
   fetch C_OPEN_ALLOC_SOURCE BULK COLLECT into O_open_alloc_no_tbl;
   SQL_LIB.SET_MARK('CLOSE', 'C_OPEN_ALLOC_SOURCE', 'I_alloc_no_tbl', NULL);
   close C_OPEN_ALLOC_SOURCE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_OPEN_ALLOC_SOURCE;
----------------------------------------------------------------------------------------------
END APPT_DOC_CLOSE_SQL;
/
