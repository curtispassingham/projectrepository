
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY STKWHATTR_SQL AS
--------------------------------------------------------------------------------


FUNCTION CHECK_REPLENISHMENT_WH_EXISTS    (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                           O_exist                 IN OUT BOOLEAN,
                                           I_vwh                   IN WH.WH%TYPE,
                                           I_pwh                   IN WH.WH%TYPE)         
   RETURN BOOLEAN IS
   L_program_name            VARCHAR2(50) := 'STKWHATTR_SQL.CHECK_REPLENISHMENT_WH_EXISTS';
   L_exists                  VARCHAR2(1);
   cursor C_REPL_WH is
      select 'x'
        from wh 
       where physical_wh = I_pwh
         and wh.wh = NVL(I_vwh,wh)
         and repl_ind = 'Y';  
BEGIN
   O_exist := FALSE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_REPL_WH ',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));   
   open C_REPL_WH;          
   SQL_LIB.SET_MARK('FETCH',
                    'C_REPL_WH ',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));
   fetch C_REPL_WH into L_exists;

   if C_REPL_WH%NOTFOUND then
      O_error_message := 'INV_WH';
      SQL_LIB.SET_MARK('CLOSE',
                       'C_REPL_WH ',
                       'WH',
                       'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));    
      close C_REPL_WH;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_REPL_WH ',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));    
   close C_REPL_WH;
   O_exist := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_REPLENISHMENT_WH_EXISTS;


---------------------------------------------------------------------------------------


FUNCTION CHECK_ROUNDING_WH_EXISTS         (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                           O_exist                 IN OUT BOOLEAN,
                                           I_vwh                   IN WH.WH%TYPE,
                                           I_pwh                   IN WH.WH%TYPE)         
   RETURN BOOLEAN IS
   L_program_name            VARCHAR2(50) := 'STKWHATTR_SQL.CHECK_ROUNDING_WH_EXISTS';
   L_exists                  VARCHAR2(1);
   cursor C_ROUNDING_WH is
      select 'x'
        from wh
       where physical_wh = I_pwh
         and stockholding_ind = 'Y'
         and wh.wh = I_vwh;
           
BEGIN
   O_exist := FALSE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_ROUNDING_WH',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));   
   open C_ROUNDING_WH;   
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_ROUNDING_WH ',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));
   fetch C_ROUNDING_WH into L_exists;

   if C_ROUNDING_WH%NOTFOUND then
      O_error_message := 'INV_WH';
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ROUNDING_WH',
                       'WH',
                       'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));    
      close C_ROUNDING_WH;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ROUNDING_WH',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));    
   close C_ROUNDING_WH;
   O_exist := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_ROUNDING_WH_EXISTS;


-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION CHECK_INVESTMENT_BUY_WH_EXISTS   (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                           O_exist                 IN OUT BOOLEAN,
                                           I_vwh                   IN WH.WH%TYPE,
                                           I_pwh                   IN WH.WH%TYPE)         
   RETURN BOOLEAN IS
   L_program_name            VARCHAR2(50) := 'STKWHATTR_SQL.CHECK_INVESTMENT_BUY_WH_EXISTS';
   L_exists                  VARCHAR2(1);
   cursor C_INVESTMENT_BUY_WH is
      select 'x'
        from wh 
       where physical_wh = I_pwh
         and wh.wh = I_vwh
         and ib_ind = 'Y';
           
BEGIN
   O_exist := FALSE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_INVESTMENT_BUY_WH',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));   
   open C_INVESTMENT_BUY_WH;          
   SQL_LIB.SET_MARK('FETCH',
                    'C_INVESTMENT_BUY_WH',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));
   fetch C_INVESTMENT_BUY_WH into L_exists;

   if C_INVESTMENT_BUY_WH%NOTFOUND then
      O_error_message := 'INV_WH';
      SQL_LIB.SET_MARK('CLOSE',
                       'C_INVESTMENT_BUY_WH',                       'WH',
                       'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));    
      close C_INVESTMENT_BUY_WH;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_INVESTMENT_BUY_WH',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));    
   close C_INVESTMENT_BUY_WH;
   O_exist := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_INVESTMENT_BUY_WH_EXISTS;

--------------------------------------------------------------------------------
FUNCTION CHECK_SOURCE_ORDER_EXISTS     (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                           O_exist                 IN OUT BOOLEAN,
                                           I_vwh                   IN WH.WH%TYPE,
                                           I_pwh                   IN WH.WH%TYPE,
                                           I_repl_wh_link          IN WH.REPL_WH_LINK%TYPE,
                                           I_source_order          IN WH.REPL_SRC_ORD%TYPE)         
   RETURN BOOLEAN IS
   L_program_name            VARCHAR2(50) := 'STKWHATTR_SQL.CHECK_SOURCE_ORDER_EXISTS';
   L_exists                  VARCHAR2(1);
   cursor C_SOURCE_ORDER is
      select 'x'
        from wh 
       where physical_wh = I_pwh
         and wh.wh != I_vwh
         and wh.repl_src_ord = I_source_order
         and wh.repl_wh_link = I_repl_wh_link;
BEGIN
   O_exist := FALSE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_SOURCE_ORDER ',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));   
   open C_SOURCE_ORDER;          
   SQL_LIB.SET_MARK('FETCH',
                    'C_SOURCE_ORDER ',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));
   fetch C_SOURCE_ORDER into L_exists;

   if C_SOURCE_ORDER%NOTFOUND then
      O_error_message := 'INV_WH';
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SOURCE_ORDER ',
                       'WH',
                       'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));    
      close C_SOURCE_ORDER;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SOURCE_ORDER',
                    'WH',
                    'Virtual WH: '||to_char(I_vwh)||' Physical WH: '||to_char (I_pwh));    
   close C_SOURCE_ORDER;
   O_exist := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_SOURCE_ORDER_EXISTS;

END STKWHATTR_SQL;
/
