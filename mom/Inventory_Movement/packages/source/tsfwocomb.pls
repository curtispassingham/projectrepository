CREATE OR REPLACE PACKAGE BODY TSF_WO_COMP_SQL AS

TYPE tsf_xform_dtl_id_TBL  is table of tsf_xform_detail.tsf_xform_detail_id%TYPE  INDEX BY BINARY_INTEGER;
TYPE tsf_xform_id_TBL      is table of tsf_xform_detail.tsf_xform_id%TYPE         INDEX BY BINARY_INTEGER;
TYPE item_TBL              is table of item_master.item%TYPE                      INDEX BY BINARY_INTEGER;
TYPE tsf_qty_TBL           is table of tsfdetail.tsf_qty%TYPE                     INDEX BY BINARY_INTEGER;
TYPE disposition_code_TBL  is table of inv_status_codes.inv_status_code%TYPE      INDEX BY BINARY_INTEGER;
TYPE av_cost_TBL           is table of item_loc_soh.av_cost%TYPE                  INDEX BY BINARY_INTEGER;
TYPE unit_retail_TBL       is table of item_loc.unit_retail%TYPE                  INDEX BY BINARY_INTEGER;

-- Item transformation variables
P_tsf_xform_detail_id    tsf_xform_dtl_id_TBL;
P_tsf_xform_id           tsf_xform_id_TBL;
P_from_item              item_TBL;
P_from_qty               tsf_qty_TBL;
P_to_item                item_TBL;
P_tsf_xform_qty          tsf_qty_TBL;
P_from_item_wac          av_cost_TBL;
P_from_item_unit_retail  unit_retail_TBL;
P_to_item_unit_retail    unit_retail_TBL;

-- From item inventory adjustment variables
-- In order to include the from and to items in the same array,
-- disposition codes must be included
P_invadj_item              item_TBL;
P_invadj_xform_qty         tsf_qty_TBL;
P_invadj_from_disposition  disposition_code_TBL;
P_invadj_to_disposition    disposition_code_TBL;
P_invadj_size              NUMBER;
P_invadj_wac               av_cost_TBL;
P_invadj_unit_retail       unit_retail_TBL;

-- Item transformation update variables
P_xform_tsf_xform_detail_id    tsf_xform_dtl_id_TBL;
P_xform_tsf_xform_id           tsf_xform_id_TBL;
P_xform_from_item              item_TBL;
P_xform_qty_xformed            tsf_qty_TBL;
P_xform_size                   NUMBER;

-- Recalc WAC variables
P_recalc_wac_item        item_TBL;
P_recalc_wac_qty         tsf_qty_TBL;
P_recalc_wac_av_cost     av_cost_TBL;
P_recalc_wac_size        NUMBER;

-- Update tsf_reserved_qty variables
P_tsf_reserved_item        item_TBL;
P_tsf_reserved_qty         tsf_qty_TBL;
P_tsf_reserved_size        NUMBER;

-- Global variables
LP_tsf_parent_no             tsfhead.tsf_no%TYPE;
LP_tsf_no                    tsfhead.tsf_no%TYPE;
LP_tsf_finisher              tsfhead.to_loc%TYPE;
LP_tsf_finisher_loc_type     TSFHEAD.FROM_LOC_TYPE%TYPE;
LP_tsf_finisher_loc_currency CURRENCIES.CURRENCY_CODE%TYPE;

LP_tsf_from_loc              TSFHEAD.FROM_LOC%TYPE;
LP_tsf_from_loc_type         TSFHEAD.FROM_LOC_TYPE%TYPE;
LP_tsf_from_loc_currency     CURRENCIES.CURRENCY_CODE%TYPE;

LP_tsf_to_loc                TSFHEAD.TO_LOC%TYPE;
LP_tsf_to_loc_type           TSFHEAD.TO_LOC_TYPE%TYPE;
LP_tsf_to_loc_currency       CURRENCIES.CURRENCY_CODE%TYPE;

LP_finisher_loc_ind          VARCHAR2(1);
LP_finisher_entity_ind       VARCHAR2(1);
LP_tran_code                 tran_data.tran_code%TYPE := NULL;
LP_tran_date                 tran_data.tran_date%TYPE := get_vdate;
LP_total_cost                tran_data.total_cost%TYPE := NULL;
LP_total_retail              tran_data.total_retail%TYPE := NULL;
LP_dept                      item_master.dept%TYPE;
LP_class                     item_master.class%TYPE;
LP_subclass                  item_master.subclass%TYPE;
LP_reason                    inv_adj_reason.reason%type := -1;
LP_complete_date             DATE;

---------------------------------------------------------------------------------------------
-- Function Name: GET_XFORM
-- Purpose      : Bulk collect all the item transformations that
--                apply to this transfer
---------------------------------------------------------------------------------------------
FUNCTION GET_XFORM(O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ITEM
-- Purpose      : Determine whether there are any items that will be transformed
--                and populate the P_invadj_ variables accordingly
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM(O_error_message IN OUT VARCHAR2,
                      I_item          IN     item_master.item%TYPE,
                      I_qty           IN     tsfdetail.tsf_qty%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PRORATE_ITEMS
-- Purpose      : Prorate the quantity to be transformed across the available items
---------------------------------------------------------------------------------------------
FUNCTION PRORATE_ITEMS(O_error_message    IN OUT VARCHAR2,
                       I_item             IN     item_master.item%TYPE,
                       L_total_xform_qty  IN     tsfdetail.tsf_qty%TYPE,
                       I_qty              IN     tsfdetail.tsf_qty%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: RECALC_WAC
-- Purpose      : Calculate the average cost of the from_items for a specified to_item.
--                This av_cost and qty will be used to update item_loc_soh.av_cost for
--                the to_item at the finisher.
---------------------------------------------------------------------------------------------
FUNCTION RECALC_WAC(O_error_message IN OUT VARCHAR2,
                    I_item          IN     item_master.item%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: FLUSH_ALL
-- Purpose      : Flush all the arrays, updating all the tables
---------------------------------------------------------------------------------------------
FUNCTION FLUSH_ALL(O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION WO_ITEM_COMP(O_error_message       IN OUT rtk_errors.rtk_text%TYPE,
                      I_tsf_no              IN     tsfhead.tsf_no%TYPE,
                      I_tsf_parent_no       IN     tsfhead.tsf_no%TYPE,
                      I_item                IN     item_master.item%TYPE,
                      I_finisher            IN     item_loc.loc%TYPE,
                      I_finisher_loc_type   IN     item_loc.loc_type%TYPE,
                      I_final_loc           IN     item_loc.loc%TYPE,
                      I_final_loc_type      IN     item_loc.loc_type%TYPE,
                      I_qty                 IN     tsfdetail.tsf_qty%TYPE,
                      I_complete_date       IN     DATE,
                      I_bk_tsf_exe          IN     BOOLEAN)
   RETURN BOOLEAN IS

   L_user_id        INV_ADJ.USER_ID%TYPE := GET_USER;
   L_bk_tsf_exe     BOOLEAN              := I_bk_tsf_exe;
   L_pack_ind       item_master.pack_ind%TYPE;

   L_code_desc      code_detail.code_desc%TYPE;
 

   cursor C_GET_ITEM_INFO is
      select pack_ind,
             dept,
             class,
             subclass
        from item_master
       where item = I_item;

   cursor C_GET_PACK_ITEM_INFO is
      select vpq.item,
             (vpq.qty * I_qty) qty
        from v_packsku_qty vpq,
             item_loc_soh ils
       where vpq.item    = ils.item
         and vpq.pack_no = I_item
         and ils.loc     = I_finisher;

   cursor C_VALIDATE_REASON is
      select reason
        from inv_adj_reason
       where reason = TO_NUMBER(L_code_desc);

   cursor C_GET_FROM_LOC is
      select from_loc,
             from_loc_type
        from tsfhead
       where tsf_no = I_tsf_parent_no;

BEGIN
   if I_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_tsf_no',
                                            'NULL','NOT NULL');
      return FALSE;
   elsif I_tsf_parent_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_tsf_parent_no',
                                            'NULL','NOT NULL');
      return FALSE;
   elsif I_item IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_item',
                                            'NULL','NOT NULL');
      return FALSE;
   elsif I_finisher IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_finisher',
                                            'NULL','NOT NULL');
      return FALSE;
   elsif I_final_loc IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_final_loc',
                                            'NULL','NOT NULL');
      return FALSE;
   elsif I_final_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_final_loc_type',
                                            'NULL','NOT NULL');
      return FALSE;
   elsif I_qty IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_qty',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;

   if I_bk_tsf_exe is NULL then
      L_bk_tsf_exe := FALSE;
   end if;

   if I_complete_date is NULL and LP_complete_date is NULL then
      LP_complete_date := GET_VDATE;
   elsif I_complete_date is NOT NULL then
      LP_complete_date := I_complete_date;
   end if;
   ---
   open C_GET_ITEM_INFO;
   fetch C_GET_ITEM_INFO into L_pack_ind,
                              LP_dept,
                              LP_class,
                              LP_subclass;
   close C_GET_ITEM_INFO;
   ---
   -- populate the fetch arrays
   if I_tsf_parent_no != nvl(LP_tsf_parent_no, -1) then
      ---
      open C_GET_FROM_LOC;
      fetch C_GET_FROM_LOC into LP_tsf_from_loc,
                                LP_tsf_from_loc_type;
      close C_GET_FROM_LOC;
      ---
      LP_tsf_to_loc := I_final_loc;
      LP_tsf_to_loc_type := I_final_loc_type;
      LP_tsf_finisher := I_finisher;
      LP_tsf_finisher_loc_type := I_finisher_loc_type;
      ---
      LP_tsf_parent_no  := I_tsf_parent_no;
      LP_tsf_no := I_tsf_no;
      ---
      if CURRENCY_SQL.GET_CURR_LOC (O_error_message,
                                    LP_tsf_from_loc,
                                    LP_tsf_from_loc_type,
                                    NULL,
                                    LP_tsf_from_loc_currency) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.GET_CURR_LOC (O_error_message,
                                    LP_tsf_finisher,
                                    LP_tsf_finisher_loc_type,
                                    NULL,
                                    LP_tsf_finisher_loc_currency) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.GET_CURR_LOC (O_error_message,
                                    LP_tsf_to_loc,
                                    LP_tsf_to_loc_type,
                                    NULL,
                                    LP_tsf_to_loc_currency) = FALSE then
         return FALSE;
      end if;
      ---
      if GET_XFORM(O_error_message) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   -- Retrieve the reason code for item transformations
   -- This is held in the code_desc on CODE_DETAIL
   ---
   if LP_reason = -1 then
      ---
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'ITRC',
                                    '1',
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;
      -- validate that the code fetched is a valid reason
      open C_VALIDATE_REASON;
      fetch C_VALIDATE_REASON into LP_reason;
      ---
      if C_VALIDATE_REASON%NOTFOUND then
         LP_reason := NULL;
      end if;
      ---
      close C_VALIDATE_REASON;
   end if;
   --- 
   P_invadj_size        := 0;
   P_xform_size         := 0;
   P_recalc_wac_size    := 0;
   P_tsf_reserved_size  := 0;

   if INVADJ_SQL.INIT_ALL(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if L_pack_ind = 'Y' then
      for rec in C_GET_PACK_ITEM_INFO LOOP
         if PROCESS_ITEM(O_error_message,
                         rec.item,
                         rec.qty) = FALSE then
            return FALSE;
         end if;
      end LOOP;
   else
      if PROCESS_ITEM(O_error_message,
                      I_item,
                      I_qty) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if P_invadj_size > 0 then
      ---
      for i in 1..P_invadj_size LOOP
         if P_invadj_xform_qty(i) > 0 then
            if INVADJ_SQL.BUILD_PROCESS_INVADJ(O_error_message,
                                               I_finisher,
                                               P_invadj_item(i),
                                               LP_reason,
                                               P_invadj_xform_qty(i),
                                               NULL,                 -- I_adj_weight
                                               NULL,                 -- I_adj_weight_uom
                                               P_invadj_from_disposition(i),
                                               P_invadj_to_disposition(i),
                                               L_user_id,
                                               LP_complete_date,
                                               NULL,
                                               NULL,
                                               P_invadj_wac(i),
                                               P_invadj_unit_retail(i)) = FALSE then 
               return FALSE;
            end if;
         end if;
      end LOOP;
      ---
      if FLUSH_ALL(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_bk_tsf_exe then
      if TSF_BT_SQL.BT_EXECUTE(O_error_message,
                               I_item,
                               L_pack_ind,
                               LP_dept,
                               LP_class,
                               LP_subclass,
                               I_finisher,
                               I_final_loc,
                               I_qty,
                               NULL,              -- inv_status
                               I_tsf_no) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'WO_ITEM_COMP',
                                            to_char(SQLCODE));
      return FALSE;
END WO_ITEM_COMP;
-----------------------------------------------------------------------------------------
FUNCTION GET_XFORM(O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   cursor C_GET_XFORM_ITEMS_1 is
      select td.tsf_xform_detail_id,
             td.tsf_xform_id,
             td.from_item,
             td.from_qty,
             td.to_item,
             td.from_qty,
             ils.av_cost,
             il.unit_retail from_retail,
             il.unit_retail to_retail
        from tsf_xform_detail td,
             tsf_xform t,
             item_loc_soh ils,
             item_loc il
       where t.tsf_xform_id = td.tsf_xform_id
         and t.tsf_no       = LP_tsf_parent_no
         and ils.item = td.from_item
         and ils.loc = LP_tsf_finisher
         and il.item = td.from_item
         and il.loc = LP_tsf_from_loc;

   cursor C_GET_XFORM_ITEMS_2 is
      select td.tsf_xform_detail_id,
             td.tsf_xform_id,
             td.from_item,
             td.from_qty,
             td.to_item,
             td.from_qty,
             ils.av_cost,
             il.unit_retail from_retail,
             il.unit_retail to_retail
        from tsf_xform_detail td,
             tsf_xform t,
             item_loc_soh ils,
             item_loc il
       where t.tsf_xform_id = td.tsf_xform_id
         and t.tsf_no       = LP_tsf_parent_no
         and ils.item = td.from_item
         and ils.loc = LP_tsf_finisher
         and il.item = td.to_item
         and il.loc = LP_tsf_to_loc
       order by td.from_qty desc;

   -- The first cursor is used for the cases when the 2nd-leg is ICT or the transfer is an Intra-company transfer,
   -- the original item's unit_retail is its unit_retail at from_loc; while the transformed item's
   -- unit_retail is its unit_retail at to_loc.
   --
   -- The second cursor is used for the case that the 1st-leg is ICT, in this case, both original/transformed
   -- item's unit_retail will use the transformed item's unit_retail at to_loc.

   --L_system_options_row        SYSTEM_OPTIONS%ROWTYPE;
   L_tsf_markdown_loc            ITEM_LOC.LOC%TYPE;
   ---
BEGIN
   ---
 
   if STKLEDGR_SQL.GET_TSF_MARKDOWN_LOC(O_error_message,
                                        L_tsf_markdown_loc,
                                        LP_tsf_no, 
                                        LP_tsf_from_loc,
                                        LP_tsf_to_loc,
                                        LP_tsf_from_loc_type,  
                                        LP_tsf_to_loc_type) = FALSE then
      return FALSE;
   end if;
   
   ---
   if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                     LP_finisher_loc_ind,
                                     LP_finisher_entity_ind,
                                     LP_tsf_parent_no) = FALSE then
      return FALSE;
   end if;
   ---
   if LP_finisher_entity_ind = 'R' then  -- finisher and recving loc in same entity, 1st-leg is ICT
      ---
      open C_GET_XFORM_ITEMS_2;
      fetch C_GET_XFORM_ITEMS_2 BULK COLLECT INTO P_tsf_xform_detail_id,
                                                  P_tsf_xform_id,
                                                  P_from_item,
                                                  P_from_qty,
                                                  P_to_item,
                                                  P_tsf_xform_qty,
                                                  P_from_item_wac,
                                                  P_from_item_unit_retail,
                                                  P_to_item_unit_retail;
      close C_GET_XFORM_ITEMS_2;
      ---
      -- The fetched retail is the to-item's retail at final to-loc for both original/transformed item,
      -- therefor both unit_retails need to convert to the currency at finisher location

      if LP_tsf_to_loc_currency != LP_tsf_finisher_loc_currency then
         FOR i IN 1..P_to_item.COUNT LOOP
            ---
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    P_from_item_unit_retail(i),
                                    LP_tsf_to_loc_currency,
                                    LP_tsf_finisher_loc_currency,
                                    P_from_item_unit_retail(i),
                                    'R',       -- cost_retail_ind
                                    LP_complete_date,      -- I_effective_date
                                    NULL) = FALSE then     -- I_exchange_type
               return FALSE;
            end if;
            ---
            P_to_item_unit_retail(i) := P_from_item_unit_retail(i);
            ---
         END LOOP;
         ---
      end if;
      ---
   elsif LP_finisher_entity_ind = 'S' then  -- 2nd-leg = ICT, 
      ---
      open C_GET_XFORM_ITEMS_1;
      fetch C_GET_XFORM_ITEMS_1 BULK COLLECT INTO P_tsf_xform_detail_id,
                                                  P_tsf_xform_id,
                                                  P_from_item,
                                                  P_from_qty,
                                                  P_to_item,
                                                  P_tsf_xform_qty,
                                                  P_from_item_wac,
                                                  P_from_item_unit_retail,
                                                  P_to_item_unit_retail;
      close C_GET_XFORM_ITEMS_1;
      ---
      -- The fetched retail is the from-item's retail at from-loc for both original/transformed item,
      -- therefor both unit_retails need to convert to the currency at finisher location

      if LP_tsf_from_loc_currency != LP_tsf_finisher_loc_currency then
         FOR i IN 1..P_to_item.COUNT LOOP
            ---
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    P_from_item_unit_retail(i),
                                    LP_tsf_from_loc_currency,
                                    LP_tsf_finisher_loc_currency,
                                    P_from_item_unit_retail(i),
                                    'R',       -- cost_retail_ind
                                    LP_complete_date,      -- I_effective_date
                                    NULL) = FALSE then     -- I_exchange_type
               return FALSE;
            end if;
            ---
            P_to_item_unit_retail(i) := P_from_item_unit_retail(i);
            ---
         END LOOP;
      end if;
      ---
   elsif LP_finisher_entity_ind = 'B' then  -- Intra-company transfer
      ---
      if LP_tsf_from_loc = L_tsf_markdown_loc then
         ---
         open C_GET_XFORM_ITEMS_2;
         fetch C_GET_XFORM_ITEMS_2 BULK COLLECT INTO P_tsf_xform_detail_id,
                                                     P_tsf_xform_id,
                                                     P_from_item,
                                                     P_from_qty,
                                                     P_to_item,
                                                     P_tsf_xform_qty,
                                                     P_from_item_wac,
                                                     P_from_item_unit_retail,
                                                     P_to_item_unit_retail;
         close C_GET_XFORM_ITEMS_2;
         ---
         -- The fetched retail is the to-item's retail at final to-loc for both original/transformed item,
         -- therefor both unit_retails need to convert to the currency at finisher location

         if LP_tsf_to_loc_currency != LP_tsf_finisher_loc_currency then
            FOR i IN 1..P_to_item.COUNT LOOP
               ---
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       P_from_item_unit_retail(i),
                                       LP_tsf_to_loc_currency,
                                       LP_tsf_finisher_loc_currency,
                                       P_from_item_unit_retail(i),
                                       'R',       -- cost_retail_ind
                                       LP_complete_date,      -- I_effective_date
                                       NULL) = FALSE then     -- I_exchange_type
                  return FALSE;
               end if;
               ---
               P_to_item_unit_retail(i) := P_from_item_unit_retail(i);
               ---
            END LOOP;
            ---
         end if;
         ---
      else   -- LP_tsf_to_loc = L_tsf_markdown_loc
         ---
         open C_GET_XFORM_ITEMS_1;
         fetch C_GET_XFORM_ITEMS_1 BULK COLLECT INTO P_tsf_xform_detail_id,
                                                     P_tsf_xform_id,
                                                     P_from_item,
                                                     P_from_qty,
                                                     P_to_item,
                                                     P_tsf_xform_qty,
                                                     P_from_item_wac,
                                                     P_from_item_unit_retail,
                                                     P_to_item_unit_retail;
         close C_GET_XFORM_ITEMS_1;
         ---
         -- The fetched retail is the from-item's retail at from-loc for both original/transformed item,
         -- therefor both unit_retails need to convert to the currency at finisher location

         if LP_tsf_from_loc_currency != LP_tsf_finisher_loc_currency then
            FOR i IN 1..P_to_item.COUNT LOOP
               ---
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       P_from_item_unit_retail(i),
                                       LP_tsf_from_loc_currency,
                                       LP_tsf_finisher_loc_currency,
                                       P_from_item_unit_retail(i),
                                       'R',       -- cost_retail_ind
                                       LP_complete_date,      -- I_effective_date
                                       NULL) = FALSE then     -- I_exchange_type
                  return FALSE;
               end if;
               ---
               P_to_item_unit_retail(i) := P_from_item_unit_retail(i);
               ---
            END LOOP;
         end if;
         ---
      end if;
      ---
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GET_XFORM',
                                            to_char(SQLCODE));
      return FALSE;
END GET_XFORM;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM(O_error_message IN OUT VARCHAR2,
                      I_item          IN     item_master.item%TYPE,
                      I_qty           IN     tsfdetail.tsf_qty%TYPE)
   RETURN BOOLEAN IS

   L_total_xform_qty     tsfdetail.tsf_qty%TYPE := 0;
   L_num_xform_items     NUMBER                 := 0;
   L_index               NUMBER;

BEGIN
   for i IN 1..P_to_item.COUNT LOOP
      if P_to_item(i) = I_item then
         ---
         L_total_xform_qty := L_total_xform_qty  + P_tsf_xform_qty(i);
         L_num_xform_items    := L_num_xform_items + 1;
         L_index := i;
         ---
      end if;
   end LOOP;

   if L_num_xform_items = 0 then
      -- Either the item is not the result of a transformation or all the items
      -- that will be transformed into the new item have already been transformed.
      -- In both cases, no further processing is necessary.
      return TRUE;
   elsif L_num_xform_items = 1 then
      -- This is a one to one transformation.  
      -- Populate the P_invadj_ variables with the input qty
            -- build invadj tran data for from-item
            P_invadj_size                            := P_invadj_size + 1;
            P_invadj_item(P_invadj_size)             := P_from_item(L_index);
            P_invadj_xform_qty(P_invadj_size)        := I_qty;
            P_invadj_from_disposition(P_invadj_size) := 'ATS';
            P_invadj_to_disposition(P_invadj_size)   := NULL;
            P_invadj_unit_retail(P_invadj_size)      := P_from_item_unit_retail(L_index);
            P_invadj_wac(P_invadj_size)              := P_from_item_wac(L_index);

            -- build invadj tran data for to-item
            P_invadj_size                            := P_invadj_size + 1;
            P_invadj_item(P_invadj_size)             := I_item;
            P_invadj_xform_qty(P_invadj_size)        := I_qty;
            P_invadj_from_disposition(P_invadj_size) := NULL;
            P_invadj_to_disposition(P_invadj_size)   := 'ATS';
            P_invadj_unit_retail(P_invadj_size)      := P_to_item_unit_retail(L_index);
            P_invadj_wac(P_invadj_size)              := P_from_item_wac(L_index);

            -- Populate the variables to update the tsf_xform_detail.qty_xformed field
            P_xform_size                              := P_xform_size + 1;
            P_xform_tsf_xform_detail_id(P_xform_size) := P_tsf_xform_detail_id(L_index);
            P_xform_tsf_xform_id(P_xform_size)        := P_tsf_xform_id(L_index);
            P_xform_from_item(P_xform_size)           := P_from_item(L_index);
            P_xform_qty_xformed(P_xform_size)         := I_qty;


            -- Populate the variables to update the from item's tsf_reserved_qty
            P_tsf_reserved_size                         := P_tsf_reserved_size + 1;
            P_tsf_reserved_item(P_tsf_reserved_size)    := P_from_item(L_index);
            P_tsf_reserved_qty(P_tsf_reserved_size)     := I_qty;
   else
      -- This is many to one transformation, prorate the quantity across all from items
      if PRORATE_ITEMS(O_error_message,
                       I_item,
                       L_total_xform_qty,
                       I_qty) = FALSE then
         return FALSE;
      end if;
   end if;

   -- if new items have been added, we will need to recalculate the WAC for the
   -- transformed item based on the old items
   if P_invadj_size > 0 then
      if RECALC_WAC(O_error_message,
                    I_item) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- Insert Markup/Markdown accordingly

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PROCESS_ITEM',
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_ITEM;
---------------------------------------------------------------------------------------------
FUNCTION PRORATE_ITEMS(O_error_message    IN OUT VARCHAR2,
                       I_item             IN     item_master.item%TYPE,
                       L_total_xform_qty  IN     tsfdetail.tsf_qty%TYPE,
                       I_qty              IN     tsfdetail.tsf_qty%TYPE)
   RETURN BOOLEAN IS

   L_avail_qty  tsfdetail.tsf_qty%TYPE := I_qty;
   L_xform_qty  NUMBER                 := 0;
   L_xform_ind  BOOLEAN                := FALSE;
   L_xform_cnt  NUMBER                 := 0;
BEGIN
   -- This loop will prorate the quantity across all from items.
   for i IN 1..P_to_item.COUNT LOOP
      if P_to_item(i) = I_item then
         -- transform quantity based on the percentage available, then round the result
         -- to the nearest integer
         L_xform_qty := round(((P_tsf_xform_qty(i) / L_total_xform_qty) * I_qty), 0);

         -- If the quantity to transform is more than is available, set them equal.
         if L_xform_qty > L_avail_qty then
            L_xform_qty := L_avail_qty;
         end if;
         --
         L_xform_cnt := L_xform_cnt + L_xform_qty;
         -- build invadj tran data for from-item
         P_invadj_size                            := P_invadj_size + 1;
         P_invadj_item(P_invadj_size)             := P_from_item(i);
         P_invadj_xform_qty(P_invadj_size)        := L_xform_qty;
         P_invadj_from_disposition(P_invadj_size) := 'ATS';
         P_invadj_to_disposition(P_invadj_size)   := NULL;
         P_invadj_unit_retail(P_invadj_size)      := P_from_item_unit_retail(i);
         P_invadj_wac(P_invadj_size)              := P_from_item_wac(i);
         -- build invadj tran data for to-item
         P_invadj_size                            := P_invadj_size + 1;
         P_invadj_item(P_invadj_size)             := I_item;
         P_invadj_xform_qty(P_invadj_size)        := L_xform_qty;
         P_invadj_from_disposition(P_invadj_size) := NULL;
         P_invadj_to_disposition(P_invadj_size)   := 'ATS';
         P_invadj_unit_retail(P_invadj_size)      := P_to_item_unit_retail(i);
         P_invadj_wac(P_invadj_size)              := P_from_item_wac(i);

         -- Populate the variables to update the tsf_xform_detail.qty_xformed field
         P_xform_size                              := P_xform_size + 1;
         P_xform_tsf_xform_detail_id(P_xform_size) := P_tsf_xform_detail_id(i);
         P_xform_tsf_xform_id(P_xform_size)        := P_tsf_xform_id(i);
         P_xform_from_item(P_xform_size)           := P_from_item(i);
         P_xform_qty_xformed(P_xform_size)         := L_xform_qty;

         -- Populate the variables to update the from item's tsf_reserved_qty
         P_tsf_reserved_size                         := P_tsf_reserved_size + 1;
         P_tsf_reserved_item(P_tsf_reserved_size)    := P_from_item(i);
         P_tsf_reserved_qty(P_tsf_reserved_size)     := L_xform_qty;

         L_avail_qty := L_avail_qty - L_xform_qty;
      end if;
   end LOOP;
   --
   if L_xform_cnt > 0 then
      L_xform_ind := TRUE;
      L_xform_cnt := 0;
   else
      L_xform_ind := FALSE;
   end if;
   -- If there are more items left to transform (due to rounding issues), add to
   -- transformed quantities until all available has been accounted form.
   while (L_avail_qty > 0) LOOP
      for i IN 1..P_to_item.COUNT LOOP
         if P_to_item(i) = I_item then
            -- Instead of adding new records to the array, update the old ones
            for j IN 1..P_invadj_size LOOP
               if P_invadj_item(j) = P_from_item(i) then
                  -- transform quantity based on the percentage available, then round
                  -- the result to the nearest integer
                   L_xform_qty := round(((P_tsf_xform_qty(i) / L_total_xform_qty)
                                         * L_avail_qty), 0);
                                         
                   L_xform_cnt := L_xform_cnt + L_xform_qty;
                   -- If the transformed quantity is 0, set it equal to the quantity to transform.
                   -- This is to avoid going thru an infinite loop.
                   if L_xform_qty = 0 and L_xform_ind = FALSE then
                      L_xform_qty := P_tsf_xform_qty(i);
                   end if;
                  -- If the quantity to transform is more than is available, set them equal.
                   if L_xform_qty > L_avail_qty then
                      L_xform_qty := L_avail_qty;
                   end if;

                  P_invadj_xform_qty(j) := P_invadj_xform_qty(j) + L_xform_qty;
                  P_invadj_xform_qty(j + 1) := P_invadj_xform_qty(j + 1) + L_xform_qty;
                  
                  -- Increase the associated array to update tsf_xform_detail
                  for k in 1..P_xform_size LOOP
                     if P_xform_from_item(k) = P_invadj_item(j) then
                        P_xform_qty_xformed(k) := P_xform_qty_xformed(k) + L_xform_qty;
                     end if;
                  end LOOP;

                  -- Increase the associated array to update tsf_reserved_qty
                  for k in 1..P_tsf_reserved_size LOOP
                     if P_tsf_reserved_item(k) = P_invadj_item(j) then
                        P_tsf_reserved_qty(k) := P_tsf_reserved_qty(k) + L_xform_qty;
                     end if;
                  end LOOP;
               end if;
            end LOOP;

            L_avail_qty := L_avail_qty - L_xform_qty;
         end if;
      end LOOP;
      --
      if L_xform_cnt > 0 then
         L_xform_ind := TRUE;
         L_xform_cnt := 0;
      else
         L_xform_ind := FALSE;
      end if;
      --      
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PRORATE_ITEMS',
                                            to_char(SQLCODE));
      return FALSE;
END PRORATE_ITEMS;
---------------------------------------------------------------------------------------------
FUNCTION RECALC_WAC(O_error_message    IN OUT VARCHAR2,
                    I_item             IN     item_master.item%TYPE)
   RETURN BOOLEAN IS

   L_new_wac              ITEM_LOC_SOH.AV_COST%TYPE;
   L_neg_soh_wac_adj      ITEM_LOC_SOH.AV_COST%TYPE;
   L_to_item_wac          ITEM_LOC_SOH.AV_COST%TYPE := 0;
   L_to_item_soh          ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_total_cost           ITEM_LOC_SOH.AV_COST%TYPE := 0;
   L_total_qty            ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;

   --variable for Wrapper Function
   L_l10n_fin_rec         L10N_FIN_REC := L10N_FIN_REC();

BEGIN
   P_recalc_wac_size                        := P_recalc_wac_size + 1;
   P_recalc_wac_item(P_recalc_wac_size)     := I_item;
   P_recalc_wac_qty(P_recalc_wac_size)      := 0;
   P_recalc_wac_av_cost(P_recalc_wac_size)  := 0;

   ---objects for L10N_FIN_REC
   L_l10n_fin_rec.procedure_key := 'GET_CURRENT_SOH_WAC';
   L_l10n_fin_rec.item          := I_item;
   L_l10n_fin_rec.doc_type      := 'TSF';
   L_l10n_fin_rec.source_entity := 'LOC';
   L_l10n_fin_rec.source_id     := LP_tsf_finisher;
   L_l10n_fin_rec.calc_fld_type := 'INTRAN';

   if L10N_SQL.EXEC_FUNCTION(O_error_message,
                             L_l10n_fin_rec)= FALSE then
      return FALSE;
   end if;
   ---

   L_to_item_wac  := L_l10n_fin_rec.av_cost;
   L_to_item_soh  := L_l10n_fin_rec.stock_on_hand;

   for i IN 1..P_to_item.COUNT LOOP
      if P_to_item(i) = I_item then
         ---
         for j IN 1..P_invadj_size LOOP
            if P_invadj_item(j) = P_from_item(i) then
               ---
               L_total_qty := L_total_qty + P_invadj_xform_qty(j);
               L_total_cost := L_total_cost + P_invadj_wac(j) * P_invadj_xform_qty(j);
               ---
            end if;
         end LOOP;
      end if;
   end LOOP;
   ---
   if STKLEDGR_ACCTING_SQL.WAC_CALC_QTY_CHANGE(O_error_message,
                                               L_new_wac,
                                               L_neg_soh_wac_adj,
                                               L_to_item_wac,
                                               L_to_item_soh,
                                               L_total_cost/L_total_qty,
                                               L_total_qty) = FALSE then
      return FALSE;
   end if;
   ---
   P_recalc_wac_av_cost(P_recalc_wac_size) := L_new_wac;
   P_recalc_wac_qty(P_recalc_wac_size) := L_total_qty;
   ---
   if L_neg_soh_wac_adj != 0 then
      ---
      LP_tran_code := 70;
      ---
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             LP_dept,
                                             LP_class,
                                             LP_subclass,
                                             LP_tsf_finisher,
                                             LP_tsf_finisher_loc_type,
                                             LP_complete_date,
                                             LP_tran_code,
                                             NULL,             -- I_adj_code,
                                             0,                -- I_units,
                                             L_neg_soh_wac_adj,         
                                             NULL,             -- I_total_retail,       
                                             LP_tsf_parent_no,     -- I_ref_no_1,
                                             NULL,            -- I_ref_no_2,
                                             NULL,              -- from_loc,      
                                             NULL,              -- from_loc_type,          
                                             NULL,       -- I_old_unit_retail,
                                             NULL,       -- I_new_unit_retail,
                                             NULL,                 -- I_source_dept,
                                             NULL,                 -- I_source_class,
                                             NULL,                 -- I_source_subclass,
                                             'TSF_WO_COMP_SQL.RECALC_WAC',   -- I_pgm_name,
                                             NULL) = FALSE then                -- I_gl_ref_no
         return FALSE;
      end if;      
      ---
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RECALC_WAC',
                                            to_char(SQLCODE));
      return FALSE;
END RECALC_WAC;
---------------------------------------------------------------------------------------------
FUNCTION FLUSH_ALL(O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   --variable for Wrapper Function
   L_l10n_fin_rec         L10N_FIN_REC := L10N_FIN_REC();

BEGIN
   -- first, flush all the arrays build by INVADJ_SQL
   if INVADJ_SQL.FLUSH_ALL(O_error_message) = FALSE then
      return FALSE;
   end if;

   ---objects for L10N_FIN_REC
   L_l10n_fin_rec.procedure_key := 'UPDATE_AV_COST';
   L_l10n_fin_rec.doc_type      := 'TSF';
   L_l10n_fin_rec.source_entity := 'LOC';
   L_l10n_fin_rec.source_id     := LP_tsf_finisher;

   -- next, recalc wac for all items that resulted from transformations
   -- since the stock on hand was updated by invadj_sql, need to subtract
   -- the quantity added here when calculating the previous cost
   -- also update the tsf_reserved_qty with the recalc_wac_qty (which is the
   -- total quantity of all of items that were transformed into this one)
   FORALL i IN 1..P_recalc_wac_size
      update item_loc_soh
         set tsf_reserved_qty = tsf_reserved_qty + P_recalc_wac_qty(i),
             av_cost = P_recalc_wac_av_cost(i)
       where item = P_recalc_wac_item(i)
         and loc  = LP_tsf_finisher;

   -- update the av_cost for all items and location having the same CNPJ
   FOR i IN 1..P_recalc_wac_size LOOP
      L_l10n_fin_rec.item          := P_recalc_wac_item(i);
      L_l10n_fin_rec.av_cost       := ROUND(P_recalc_wac_av_cost(i),4);

      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_fin_rec)= FALSE then
         return FALSE;
      end if;
   END LOOP;

   -- now, decrease the tsf_reserved_qty for the items that were transformed
   FORALL i IN 1..P_tsf_reserved_size
      update item_loc_soh
         set tsf_reserved_qty = tsf_reserved_qty - P_tsf_reserved_qty(i)
       where item = P_tsf_reserved_item(i)
         and loc  = LP_tsf_finisher;

   -- finally, update the tsf_xform_detail.qty_xformed
   FORALL i IN 1..P_xform_size
      update tsf_xform_detail
         set qty_xformed         = nvl(qty_xformed,0) + P_xform_qty_xformed(i)
       where tsf_xform_detail_id = P_xform_tsf_xform_detail_id(i)
         and tsf_xform_id        = P_xform_tsf_xform_id(i)
         and from_item           = P_xform_from_item(i);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLUSH_ALL',
                                            to_char(SQLCODE));
      return FALSE;
END FLUSH_ALL;
---------------------------------------------------------------------------------------------
FUNCTION DOC_CLOSE_QUEUE_INSERT(O_error_message  IN OUT  rtk_errors.rtk_text%TYPE,
                                I_tsf_no         IN      tsfhead.tsf_no%TYPE)
RETURN BOOLEAN IS

   L_function              VARCHAR2(60) := 'TSF_WO_COMP_SQL.DOC_CLOSE_QUEUE_INSERT';

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_tsf_no',L_function);
      return FALSE;
   end if;

   insert into doc_close_queue( doc,
                                doc_type)
                        values( I_tsf_no,
                                'T');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DOC_CLOSE_QUEUE_INSERT;
---------------------------------------------------------------------------------------------
END TSF_WO_COMP_SQL;
/
