-- File Name : CORESVC_DLVY_SLOT_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_DLVY_SLOT AUTHID CURRENT_USER AS

   template_key                     CONSTANT VARCHAR2(25)          := 'DLVY_SLOT_DATA';
   template_category                CONSTANT VARCHAR2(25)          := 'RMSINV';

   action_new                                VARCHAR2(25)          := 'NEW';
   action_mod                                VARCHAR2(25)          := 'MOD';
   action_del                                VARCHAR2(25)          := 'DEL';
   DELIVERY_SLOT_sheet                       VARCHAR2(25)          := 'DELIVERY_SLOT';
   DELIVERY_SLOT$Action                      NUMBER                :=1;
   DLVYSLT$DELIVERY_SLOT_DESC                NUMBER                :=3;
   DLVYSLT$DELIVERY_SLOT_SEQUENCE            NUMBER                :=4;
   DELIVERY_SLOT$DELIVERY_SLOT_ID            NUMBER                :=2;

   DELIVERY_SLOT_TL_sheet                       VARCHAR2(25)          := 'DELIVERY_SLOT_TL';
   DELIVERY_SLOT_TL$Action                      NUMBER                :=1;
   DELIVERY_SLOT_TL$Lang                        NUMBER                :=2;
   DLVYSLT_TL$DELIVERY_SLOT_ID                  NUMBER                :=3;
   DLVYSLT_TL$DELIVERY_SLOT_DESC                NUMBER                :=4;

   TYPE DELIVERY_SLOT_rec_tab IS TABLE OF DELIVERY_SLOT%ROWTYPE;

   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';

   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;

   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;

   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;

   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;

END CORESVC_DLVY_SLOT;
/
