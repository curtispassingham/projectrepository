CREATE OR REPLACE PACKAGE CORESVC_TSFZONE AUTHID CURRENT_USER AS
   template_key  CONSTANT VARCHAR2(255)             := 'TRANSFER_ZONE_DATA';
   action_new             VARCHAR2(25)              := 'NEW';
   action_mod             VARCHAR2(25)              := 'MOD';
   action_del             VARCHAR2(25)              := 'DEL';
   TSFZONE_sheet          VARCHAR2(255)             := 'TRANSFER_ZONE';
   TSFZONE$Action         NUMBER                    := 1;
   TSFZONE$DESCRIPTION    NUMBER                    := 3;
   TSFZONE$TRANSFER_ZONE  NUMBER                    := 2;
   TSFZONE_TL_sheet          VARCHAR2(255)          := 'TRANSFER_ZONE_TL';
   TSFZONE_TL$Action         NUMBER                 := 1;
   TSFZONE_TL$LANG           NUMBER                 := 2;
   TSFZONE_TL$TRANSFER_ZONE  NUMBER                 := 3;
   TSFZONE_TL$DESCRIPTION    NUMBER                 := 4;
   sheet_name_trans       S9T_PKG.TRANS_MAP_TYP;
   action_column          VARCHAR2(255)             := 'ACTION';
   template_category      CODE_DETAIL.CODE%TYPE     := 'RMSINV';
   TYPE TSFZONE_rec_tab IS TABLE OF TSFZONE%ROWTYPE;
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name      IN       VARCHAR2)
   RETURN VARCHAR2;
END CORESVC_TSFZONE;
/