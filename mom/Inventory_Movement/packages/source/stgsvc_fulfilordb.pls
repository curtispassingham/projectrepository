CREATE OR REPLACE PACKAGE BODY STGSVC_FULFILORD AS
-----------------------------------------------------------------------------------------------
LP_user             SVCPROV_CONTEXT.USER_NAME%TYPE     := get_user;
-----------------------------------------------------------------------------------------------
FUNCTION POP_CREATE_TABLES(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_process_id                 IN OUT   SVC_FULFILORD.PROCESS_ID%TYPE,
                           I_rib_fulfilordcoldesc_rec   IN       "RIB_FulfilOrdColDesc_REC",
                           I_action_type                IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255)   :=  'STGSVC_FULFILORD.POP_CREATE_TABLES';
   L_invalid_param    VARCHAR2(30)    := NULL;
   L_l10n_rib_rec     L10N_RIB_REC    := L10N_RIB_REC();
   L_service_name     VARCHAR2(30)    := 'createFulfilOrdColDesc';

   L_process_id       SVC_FULFILORD.PROCESS_ID%TYPE    := SVC_FULFILORD_PROC_SEQ.nextval;
   L_chunk_id         SVC_FULFILORD.CHUNK_ID%TYPE      := 1;
   L_fulfilord_id     SVC_FULFILORD.FULFILORD_ID%TYPE  := NULL;

   L_dtl_index        INTEGER   := 0;

   TYPE svc_fulfilord_tbl is table of SVC_FULFILORD%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_fulfilord_tbl svc_fulfilord_tbl;

   TYPE svc_fulfilordcust_tbl is table of SVC_FULFILORDCUST%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_fulfilordcust_tbl svc_fulfilordcust_tbl;

   TYPE svc_fulfilorddtl_tbl is table of SVC_FULFILORDDTL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_fulfilorddtl_tbl svc_fulfilorddtl_tbl;

   cursor C_LOCALIZED_COUNTRY is
      select country_id
        from country_attrib
       where localized_ind = 'Y';

BEGIN

   if I_action_type is NULL then
      L_invalid_param := 'I_action_type';
   elsif I_rib_fulfilordcoldesc_rec is NULL then
      L_invalid_param := 'I_rib_fulfilordcoldesc_rec';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_action_type != CORESVC_FULFILORD.ACTION_TYPE_CREATE then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SERVICE_OPERATION',
                                            I_action_type,
                                            L_service_name,
                                            NULL);
      return FALSE;
   end if;

   if I_rib_fulfilordcoldesc_rec is NULL or
      I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL is NULL or
      I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL.count <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('EMPTY_CUST_ORD_MSG',
                                            L_service_name,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   FOR i in I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL.FIRST..I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL.LAST LOOP
      --build a collection of svc_fulfilord rows
      L_svc_fulfilord_tbl(i).process_id := L_process_id;
      L_svc_fulfilord_tbl(i).chunk_id := L_chunk_id;
      L_svc_fulfilord_tbl(i).process_status := CORESVC_FULFILORD.PROCESS_STATUS_NEW;
      L_svc_fulfilord_tbl(i).action_type := I_action_type;

      --L_fulfilord_id will be passed between parent and child records to link a customer order
      L_fulfilord_id := SVC_FULFILORD_ID_SEQ.nextval;
      L_svc_fulfilord_tbl(i).fulfilord_id := L_fulfilord_id;

      -- set tran_type based on source location type
      if I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_SUPPLIER then
         L_svc_fulfilord_tbl(i).tran_type := RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO;
      elsif I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE then
         L_svc_fulfilord_tbl(i).tran_type := RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF;
      elsif I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH then
         L_svc_fulfilord_tbl(i).tran_type := RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF;
      elsif I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).source_loc_type is NULL then
         L_svc_fulfilord_tbl(i).tran_type := RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_INV;
      else
        O_error_message := SQL_LIB.CREATE_MSG('INV_SOURCE_LOC_TYPE',
                                              I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).customer_order_no,
                                              I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).fulfill_order_no,
                                              NULL);
         return FALSE;
      end if;

      L_svc_fulfilord_tbl(i).customer_order_no := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).customer_order_no;
      L_svc_fulfilord_tbl(i).fulfill_order_no := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).fulfill_order_no;
      L_svc_fulfilord_tbl(i).source_loc_type := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).source_loc_type;
      L_svc_fulfilord_tbl(i).source_loc_id := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).source_loc_id;
      L_svc_fulfilord_tbl(i).fulfill_loc_type := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).fulfill_loc_type;
      L_svc_fulfilord_tbl(i).fulfill_loc_id := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).fulfill_loc_id;
      L_svc_fulfilord_tbl(i).partial_delivery_ind := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).partial_delivery_ind;
      L_svc_fulfilord_tbl(i).delivery_type := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).delivery_type;
      L_svc_fulfilord_tbl(i).carrier_code := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).carrier_code;
      L_svc_fulfilord_tbl(i).carrier_service_code := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).carrier_service_code;
      L_svc_fulfilord_tbl(i).consumer_delivery_date := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).consumer_delivery_date;
      L_svc_fulfilord_tbl(i).consumer_delivery_time := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).consumer_delivery_time;
      L_svc_fulfilord_tbl(i).delivery_charges := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).delivery_charges;
      L_svc_fulfilord_tbl(i).delivery_charges_curr := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).delivery_charges_curr;
      L_svc_fulfilord_tbl(i).comments := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).comments;
      L_svc_fulfilord_tbl(i).error_msg := NULL;

      -- build a collection of svc_fulfilordcust rows
      if I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc is NOT NULL then

         L_svc_fulfilordcust_tbl(i).process_id := L_process_id;
         L_svc_fulfilordcust_tbl(i).chunk_id := L_chunk_id;
         L_svc_fulfilordcust_tbl(i).fulfilord_id := L_fulfilord_id;

         L_svc_fulfilordcust_tbl(i).customer_no := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.customer_no;
         L_svc_fulfilordcust_tbl(i).deliver_first_name := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_first_name;
         L_svc_fulfilordcust_tbl(i).deliver_phonetic_first := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_phonetic_first;
         L_svc_fulfilordcust_tbl(i).deliver_last_name := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_last_name;
         L_svc_fulfilordcust_tbl(i).deliver_phonetic_last := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_phonetic_last;
         L_svc_fulfilordcust_tbl(i).deliver_preferred_name := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_preferred_name;
         L_svc_fulfilordcust_tbl(i).deliver_company_name := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_company_name;
         L_svc_fulfilordcust_tbl(i).deliver_add1 := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_add1;
         L_svc_fulfilordcust_tbl(i).deliver_add2 := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_add2;
         L_svc_fulfilordcust_tbl(i).deliver_add3 := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_add3;
         L_svc_fulfilordcust_tbl(i).deliver_county := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_county;
         L_svc_fulfilordcust_tbl(i).deliver_city := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_city;
         L_svc_fulfilordcust_tbl(i).deliver_state := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_state;
         L_svc_fulfilordcust_tbl(i).deliver_country_id := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_country_id;
         L_svc_fulfilordcust_tbl(i).deliver_post := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_post;
         L_svc_fulfilordcust_tbl(i).deliver_jurisdiction := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_jurisdiction;
         L_svc_fulfilordcust_tbl(i).deliver_phone := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_phone;
         L_svc_fulfilordcust_tbl(i).deliver_email := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.deliver_email;

         L_svc_fulfilordcust_tbl(i).bill_first_name := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_first_name;
         L_svc_fulfilordcust_tbl(i).bill_phonetic_first := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_phonetic_first;
         L_svc_fulfilordcust_tbl(i).bill_last_name := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_last_name;
         L_svc_fulfilordcust_tbl(i).bill_phonetic_last := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_phonetic_last;
         L_svc_fulfilordcust_tbl(i).bill_preferred_name := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_preferred_name;
         L_svc_fulfilordcust_tbl(i).bill_company_name := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_company_name;
         L_svc_fulfilordcust_tbl(i).bill_add1 := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_add1;
         L_svc_fulfilordcust_tbl(i).bill_add2 := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_add2;
         L_svc_fulfilordcust_tbl(i).bill_add3 := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_add3;
         L_svc_fulfilordcust_tbl(i).bill_county := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_county;
         L_svc_fulfilordcust_tbl(i).bill_city := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_city;
         L_svc_fulfilordcust_tbl(i).bill_state := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_state;
         L_svc_fulfilordcust_tbl(i).bill_country_id := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_country_id;
         L_svc_fulfilordcust_tbl(i).bill_post := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_post;
         L_svc_fulfilordcust_tbl(i).bill_jurisdiction := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_jurisdiction;
         L_svc_fulfilordcust_tbl(i).bill_phone := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_phone;
         L_svc_fulfilordcust_tbl(i).bill_email := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdCustDesc.bill_email;

         L_svc_fulfilordcust_tbl(i).error_msg := NULL;
      end if;   -- Check if FulfilOrdCustDesc is NULL

      -- build a collection of svc_fulfilorddtl rows
      if I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL is NOT NULL and
         I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL.count > 0 then
         FOR j in I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL.FIRST..I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL.LAST LOOP
            L_dtl_index := L_dtl_index+1;

            L_svc_fulfilorddtl_tbl(L_dtl_index).process_id := L_process_id;
            L_svc_fulfilorddtl_tbl(L_dtl_index).chunk_id := L_chunk_id;
            L_svc_fulfilorddtl_tbl(L_dtl_index).fulfilord_id := L_fulfilord_id;

            L_svc_fulfilorddtl_tbl(L_dtl_index).item_status := NULL;
            L_svc_fulfilorddtl_tbl(L_dtl_index).item := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL(j).item;
            L_svc_fulfilorddtl_tbl(L_dtl_index).ref_item := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL(j).ref_item;
            L_svc_fulfilorddtl_tbl(L_dtl_index).order_qty_suom := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL(j).order_qty_suom;
            L_svc_fulfilorddtl_tbl(L_dtl_index).standard_uom := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL(j).standard_uom;
            L_svc_fulfilorddtl_tbl(L_dtl_index).transaction_uom := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL(j).transaction_uom;
            L_svc_fulfilorddtl_tbl(L_dtl_index).substitute_ind := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL(j).substitute_ind;
            L_svc_fulfilorddtl_tbl(L_dtl_index).unit_retail := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL(j).unit_retail;
            L_svc_fulfilorddtl_tbl(L_dtl_index).retail_curr := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL(j).retail_curr;
            L_svc_fulfilorddtl_tbl(L_dtl_index).comments := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i).FulfilOrdDtl_TBL(j).comments;
            L_svc_fulfilorddtl_tbl(L_dtl_index).available_qty := NULL;
            L_svc_fulfilorddtl_tbl(L_dtl_index).error_msg := NULL;
         END LOOP; -- FulfilOrdDtl_TBL
      end if; -- Check if FulfilOrdDtl_TBL is NULL
   END LOOP; -- FulfilOrdDesc_TBL

   --bulk insert staging table row collections into staging tables
   FORALL i in L_svc_fulfilord_tbl.FIRST .. L_svc_fulfilord_tbl.LAST
      insert into svc_fulfilord(process_id,
                                chunk_id,
                                process_status,
                                action_type,
                                tran_type,
                                fulfilord_id,
                                customer_order_no,
                                fulfill_order_no,
                                source_loc_type,
                                source_loc_id,
                                fulfill_loc_type,
                                fulfill_loc_id,
                                partial_delivery_ind,
                                delivery_type,
                                carrier_code,
                                carrier_service_code,
                                consumer_delivery_date,
                                consumer_delivery_time,
                                delivery_charges,
                                delivery_charges_curr,
                                comments,
                                error_msg,
                                create_datetime,
                                create_id,
                                last_update_datetime,
                                last_update_id)
                        values (L_svc_fulfilord_tbl(i).process_id,
                                L_svc_fulfilord_tbl(i).chunk_id,
                                L_svc_fulfilord_tbl(i).process_status,
                                L_svc_fulfilord_tbl(i).action_type,
                                L_svc_fulfilord_tbl(i).tran_type,
                                L_svc_fulfilord_tbl(i).fulfilord_id,
                                L_svc_fulfilord_tbl(i).customer_order_no,
                                L_svc_fulfilord_tbl(i).fulfill_order_no,
                                L_svc_fulfilord_tbl(i).source_loc_type,
                                L_svc_fulfilord_tbl(i).source_loc_id,
                                L_svc_fulfilord_tbl(i).fulfill_loc_type,
                                L_svc_fulfilord_tbl(i).fulfill_loc_id,
                                L_svc_fulfilord_tbl(i).partial_delivery_ind,
                                L_svc_fulfilord_tbl(i).delivery_type,
                                L_svc_fulfilord_tbl(i).carrier_code,
                                L_svc_fulfilord_tbl(i).carrier_service_code,
                                L_svc_fulfilord_tbl(i).consumer_delivery_date,
                                L_svc_fulfilord_tbl(i).consumer_delivery_time,
                                L_svc_fulfilord_tbl(i).delivery_charges,
                                L_svc_fulfilord_tbl(i).delivery_charges_curr,
                                L_svc_fulfilord_tbl(i).comments,
                                L_svc_fulfilord_tbl(i).error_msg,
                                SYSDATE,
                                LP_user,
                                SYSDATE,
                                LP_user);

   FORALL i in L_svc_fulfilordcust_tbl.FIRST .. L_svc_fulfilordcust_tbl.LAST
      insert into svc_fulfilordcust(process_id,
                                    chunk_id,
                                    fulfilord_id,
                                    customer_no,
                                    deliver_first_name,
                                    deliver_phonetic_first,
                                    deliver_last_name,
                                    deliver_phonetic_last,
                                    deliver_preferred_name,
                                    deliver_company_name,
                                    deliver_add1,
                                    deliver_add2,
                                    deliver_add3,
                                    deliver_county,
                                    deliver_city,
                                    deliver_state,
                                    deliver_country_id,
                                    deliver_post,
                                    deliver_jurisdiction,
                                    deliver_phone,
                                    deliver_email,
                                    bill_first_name,
                                    bill_phonetic_first,
                                    bill_last_name,
                                    bill_phonetic_last,
                                    bill_preferred_name,
                                    bill_company_name,
                                    bill_add1,
                                    bill_add2,
                                    bill_add3,
                                    bill_county,
                                    bill_city,
                                    bill_state,
                                    bill_country_id,
                                    bill_post,
                                    bill_jurisdiction,
                                    bill_phone,
                                    bill_email,
                                    error_msg,
                                    create_datetime,
                                    create_id,
                                    last_update_datetime,
                                    last_update_id)
                            values (L_svc_fulfilordcust_tbl(i).process_id,
                                    L_svc_fulfilordcust_tbl(i).chunk_id,
                                    L_svc_fulfilordcust_tbl(i).fulfilord_id,
                                    L_svc_fulfilordcust_tbl(i).customer_no,
                                    L_svc_fulfilordcust_tbl(i).deliver_first_name,
                                    L_svc_fulfilordcust_tbl(i).deliver_phonetic_first,
                                    L_svc_fulfilordcust_tbl(i).deliver_last_name,
                                    L_svc_fulfilordcust_tbl(i).deliver_phonetic_last,
                                    L_svc_fulfilordcust_tbl(i).deliver_preferred_name,
                                    L_svc_fulfilordcust_tbl(i).deliver_company_name,
                                    L_svc_fulfilordcust_tbl(i).deliver_add1,
                                    L_svc_fulfilordcust_tbl(i).deliver_add2,
                                    L_svc_fulfilordcust_tbl(i).deliver_add3,
                                    L_svc_fulfilordcust_tbl(i).deliver_county,
                                    L_svc_fulfilordcust_tbl(i).deliver_city,
                                    L_svc_fulfilordcust_tbl(i).deliver_state,
                                    L_svc_fulfilordcust_tbl(i).deliver_country_id,
                                    L_svc_fulfilordcust_tbl(i).deliver_post,
                                    L_svc_fulfilordcust_tbl(i).deliver_jurisdiction,
                                    L_svc_fulfilordcust_tbl(i).deliver_phone,
                                    L_svc_fulfilordcust_tbl(i).deliver_email,
                                    L_svc_fulfilordcust_tbl(i).bill_first_name,
                                    L_svc_fulfilordcust_tbl(i).bill_phonetic_first,
                                    L_svc_fulfilordcust_tbl(i).bill_last_name,
                                    L_svc_fulfilordcust_tbl(i).bill_phonetic_last,
                                    L_svc_fulfilordcust_tbl(i).bill_preferred_name,
                                    L_svc_fulfilordcust_tbl(i).bill_company_name,
                                    L_svc_fulfilordcust_tbl(i).bill_add1,
                                    L_svc_fulfilordcust_tbl(i).bill_add2,
                                    L_svc_fulfilordcust_tbl(i).bill_add3,
                                    L_svc_fulfilordcust_tbl(i).bill_county,
                                    L_svc_fulfilordcust_tbl(i).bill_city,
                                    L_svc_fulfilordcust_tbl(i).bill_state,
                                    L_svc_fulfilordcust_tbl(i).bill_country_id,
                                    L_svc_fulfilordcust_tbl(i).bill_post,
                                    L_svc_fulfilordcust_tbl(i).bill_jurisdiction,
                                    L_svc_fulfilordcust_tbl(i).bill_phone,
                                    L_svc_fulfilordcust_tbl(i).bill_email,
                                    L_svc_fulfilordcust_tbl(i).error_msg,
                                    SYSDATE,
                                    LP_user,
                                    SYSDATE,
                                    LP_user);

   FORALL i in L_svc_fulfilorddtl_tbl.FIRST .. L_svc_fulfilorddtl_tbl.LAST
      insert into svc_fulfilorddtl(process_id,
                                   chunk_id,
                                   fulfilord_id,
                                   item_status,
                                   item,
                                   ref_item,
                                   order_qty_suom,
                                   standard_uom,
                                   transaction_uom,
                                   substitute_ind,
                                   unit_retail,
                                   retail_curr,
                                   comments,
                                   available_qty,
                                   error_msg,
                                   create_datetime,
                                   create_id,
                                   last_update_datetime,
                                   last_update_id)
                           values (L_svc_fulfilorddtl_tbl(i).process_id,
                                   L_svc_fulfilorddtl_tbl(i).chunk_id,
                                   L_svc_fulfilorddtl_tbl(i).fulfilord_id,
                                   L_svc_fulfilorddtl_tbl(i).item_status,
                                   L_svc_fulfilorddtl_tbl(i).item,
                                   L_svc_fulfilorddtl_tbl(i).ref_item,
                                   L_svc_fulfilorddtl_tbl(i).order_qty_suom,
                                   L_svc_fulfilorddtl_tbl(i).standard_uom,
                                   L_svc_fulfilorddtl_tbl(i).transaction_uom,
                                   L_svc_fulfilorddtl_tbl(i).substitute_ind,
                                   L_svc_fulfilorddtl_tbl(i).unit_retail,
                                   L_svc_fulfilorddtl_tbl(i).retail_curr,
                                   L_svc_fulfilorddtl_tbl(i).comments,
                                   L_svc_fulfilorddtl_tbl(i).available_qty,
                                   L_svc_fulfilorddtl_tbl(i).error_msg,
                                   SYSDATE,
                                   LP_user,
                                   SYSDATE,
                                   LP_user);

   -- insert component items in SVC_FULFILORDDTL_COMP_ITEM for packs sourced from store
   insert into svc_fulfilorddtl_comp_item(process_id,
                                          chunk_id,
                                          fulfilord_id,
                                          item,
                                          component_item,
                                          component_qty,
                                          available_qty)
                                   select sfd.process_id,
                                          sfd.chunk_id,
                                          sfd.fulfilord_id,
                                          sfd.item,
                                          vpq.item,
                                          vpq.qty,
                                          NULL
                                     from svc_fulfilord sfo,
                                          svc_fulfilorddtl sfd,
                                          v_packsku_qty vpq
                                    where sfo.process_id = L_process_id
                                      and sfo.process_id = sfd.process_id
                                      and sfo.chunk_id = sfd.chunk_id
                                      and sfo.fulfilord_id = sfd.fulfilord_id
                                      and sfo.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                                      and sfo.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE
                                      and sfd.item = vpq.pack_no;

   FOR rec in C_LOCALIZED_COUNTRY LOOP
      FOR i in I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL.FIRST..I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL.LAST LOOP
         -- Populate localization object
         L_l10n_rib_rec.procedure_key := 'STAGE_SVC_FULFILORD_L10N';
         L_l10n_rib_rec.doc_type      := NULL;
         L_l10n_rib_rec.doc_id        := NULL;
         L_l10n_rib_rec.country_id    := rec.country_id;
         L_l10n_rib_rec.rib_msg       := I_rib_fulfilordcoldesc_rec.FulfilOrdDesc_TBL(i);

         L_l10n_rib_rec.process_id   := L_process_id;
         L_l10n_rib_rec.chunk_id     := L_chunk_id;
         L_l10n_rib_rec.reference_id := L_svc_fulfilord_tbl(i).fulfilord_id;
         L_l10n_rib_rec.status_code  := L_svc_fulfilord_tbl(i).process_status;
         L_l10n_rib_rec.rib_msg_type := I_action_type;
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_rib_rec) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   END LOOP;

   O_process_id := L_process_id;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POP_CREATE_TABLES;
-----------------------------------------------------------------------------------------------
FUNCTION POP_CANCEL_TABLES(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_process_id                 IN OUT   SVC_FULFILORDREF.PROCESS_ID%TYPE,
                           I_rib_fulfilordcolref_rec    IN       "RIB_FulfilOrdColRef_REC",
                           I_action_type                IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255) := 'STGSVC_FULFILORD.POP_CANCEL_TABLES';
   L_invalid_param    VARCHAR2(30)  := NULL;

   L_process_id       SVC_FULFILORDREF.PROCESS_ID%TYPE        := SVC_FULFILORD_PROC_SEQ.nextval;
   L_chunk_id         SVC_FULFILORDREF.CHUNK_ID%TYPE          := 1;
   L_fulfilordref_id  SVC_FULFILORDREF.FULFILORDREF_ID%TYPE   := NULL;
   L_service_name     VARCHAR2(30)                            := 'cancelFulfilOrdColRef';

   L_dtl_index        INTEGER := 0;

   TYPE svc_fulfilordref_tbl is table of SVC_FULFILORDREF%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_fulfilordref_tbl svc_fulfilordref_tbl;

   TYPE svc_fulfilorddtlref_tbl is table of SVC_FULFILORDDTLREF%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_fulfilorddtlref_tbl svc_fulfilorddtlref_tbl;

BEGIN

   if I_action_type is NULL then
      L_invalid_param := 'I_action_type';
   elsif I_rib_fulfilordcolref_rec is NULL then
      L_invalid_param := 'I_rib_fulfilordcolref_rec';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_action_type != CORESVC_FULFILORD.ACTION_TYPE_CANCEL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SERVICE_OPERATION',
                                            I_action_type,
                                            L_service_name,
                                            NULL);
      return FALSE;
   end if;

   if I_rib_fulfilordcolref_rec is NULL or
      I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL is NULL or
      I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL.count <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('EMPTY_CUST_ORD_MSG',
                                            L_service_name,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   FOR i in I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL.FIRST..I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL.LAST LOOP
      --build a collection of svc_fulfilordref rows
      L_svc_fulfilordref_tbl(i).process_id := L_process_id;
      L_svc_fulfilordref_tbl(i).chunk_id := L_chunk_id;
      L_svc_fulfilordref_tbl(i).process_status := CORESVC_FULFILORD.PROCESS_STATUS_NEW;
      L_svc_fulfilordref_tbl(i).action_type := I_action_type;

      --L_fulfilordref_id will be passed between parent and child records to link a customer order
      L_fulfilordref_id := SVC_FULFILORDREF_ID_SEQ.nextval;
      L_svc_fulfilordref_tbl(i).fulfilordref_id := L_fulfilordref_id;

      -- set tran_type based on source location type
      if I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_SUPPLIER then
         L_svc_fulfilordref_tbl(i).tran_type := RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO;
      elsif I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE then
         L_svc_fulfilordref_tbl(i).tran_type := RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF;
      elsif I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH then
         L_svc_fulfilordref_tbl(i).tran_type := RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF;
      elsif I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).source_loc_type is NULL then
         L_svc_fulfilordref_tbl(i).tran_type := RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_INV;
      else
        O_error_message := SQL_LIB.CREATE_MSG('INV_SOURCE_LOC_TYPE',
                                              I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).customer_order_no,
                                              I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).fulfill_order_no,
                                              NULL);
         return FALSE;
      end if;

      L_svc_fulfilordref_tbl(i).customer_order_no := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).customer_order_no;
      L_svc_fulfilordref_tbl(i).fulfill_order_no := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).fulfill_order_no;
      L_svc_fulfilordref_tbl(i).source_loc_type := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).source_loc_type;
      L_svc_fulfilordref_tbl(i).source_loc_id := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).source_loc_id;
      L_svc_fulfilordref_tbl(i).fulfill_loc_type := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).fulfill_loc_type;
      L_svc_fulfilordref_tbl(i).fulfill_loc_id := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).fulfill_loc_id;
      L_svc_fulfilordref_tbl(i).error_msg := NULL;

      -- build a collection of svc_fulfilorddtlref rows
      if I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).FulfilOrdDtlRef_TBL is NOT NULL and
         I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).FulfilOrdDtlRef_TBL.count > 0 then

         FOR j in I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).FulfilOrdDtlRef_TBL.FIRST..I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).FulfilOrdDtlRef_TBL.LAST LOOP
            L_dtl_index := L_dtl_index+1;

            L_svc_fulfilorddtlref_tbl(L_dtl_index).process_id := L_process_id;
            L_svc_fulfilorddtlref_tbl(L_dtl_index).chunk_id := L_chunk_id;
            L_svc_fulfilorddtlref_tbl(L_dtl_index).fulfilordref_id := L_fulfilordref_id;

            L_svc_fulfilorddtlref_tbl(L_dtl_index).item := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).FulfilOrdDtlRef_TBL(j).item;
            L_svc_fulfilorddtlref_tbl(L_dtl_index).ref_item := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).FulfilOrdDtlRef_TBL(j).ref_item;
            L_svc_fulfilorddtlref_tbl(L_dtl_index).cancel_qty_suom := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).FulfilOrdDtlRef_TBL(j).cancel_qty_suom;
            L_svc_fulfilorddtlref_tbl(L_dtl_index).standard_uom := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).FulfilOrdDtlRef_TBL(j).standard_uom;
            L_svc_fulfilorddtlref_tbl(L_dtl_index).transaction_uom := I_rib_fulfilordcolref_rec.FulfilOrdRef_TBL(i).FulfilOrdDtlRef_TBL(j).transaction_uom;
            L_svc_fulfilorddtlref_tbl(L_dtl_index).error_msg := NULL;
         END LOOP; -- FulfilOrdDtlRef_TBL
      end if; -- Check if FulfilOrdDtlRef_TBL is NULL
   END LOOP;

   --bulk insert staging table row collections into staging tables
   FORALL i in L_svc_fulfilordref_tbl.FIRST .. L_svc_fulfilordref_tbl.LAST
      insert into svc_fulfilordref(process_id,
                                   chunk_id,
                                   process_status,
                                   action_type,
                                   tran_type,
                                   fulfilordref_id,
                                   customer_order_no,
                                   fulfill_order_no,
                                   source_loc_type,
                                   source_loc_id,
                                   fulfill_loc_type,
                                   fulfill_loc_id,
                                   error_msg,
                                   create_datetime,
                                   create_id,
                                   last_update_datetime,
                                   last_update_id)
                           values (L_svc_fulfilordref_tbl(i).process_id,
                                   L_svc_fulfilordref_tbl(i).chunk_id,
                                   L_svc_fulfilordref_tbl(i).process_status,
                                   L_svc_fulfilordref_tbl(i).action_type,
                                   L_svc_fulfilordref_tbl(i).tran_type,
                                   L_svc_fulfilordref_tbl(i).fulfilordref_id,
                                   L_svc_fulfilordref_tbl(i).customer_order_no,
                                   L_svc_fulfilordref_tbl(i).fulfill_order_no,
                                   L_svc_fulfilordref_tbl(i).source_loc_type,
                                   L_svc_fulfilordref_tbl(i).source_loc_id,
                                   L_svc_fulfilordref_tbl(i).fulfill_loc_type,
                                   L_svc_fulfilordref_tbl(i).fulfill_loc_id,
                                   L_svc_fulfilordref_tbl(i).error_msg,
                                   SYSDATE,
                                   LP_user,
                                   SYSDATE,
                                   LP_user);

   FORALL i in L_svc_fulfilorddtlref_tbl.FIRST .. L_svc_fulfilorddtlref_tbl.LAST
      insert into svc_fulfilorddtlref(process_id,
                                      chunk_id,
                                      fulfilordref_id,
                                      item,
                                      ref_item,
                                      cancel_qty_suom,
                                      standard_uom,
                                      transaction_uom,
                                      error_msg,
                                      create_datetime,
                                      create_id,
                                      last_update_datetime,
                                      last_update_id)
                              values (L_svc_fulfilorddtlref_tbl(i).process_id,
                                      L_svc_fulfilorddtlref_tbl(i).chunk_id,
                                      L_svc_fulfilorddtlref_tbl(i).fulfilordref_id,
                                      L_svc_fulfilorddtlref_tbl(i).item,
                                      L_svc_fulfilorddtlref_tbl(i).ref_item,
                                      L_svc_fulfilorddtlref_tbl(i).cancel_qty_suom,
                                      L_svc_fulfilorddtlref_tbl(i).standard_uom,
                                      L_svc_fulfilorddtlref_tbl(i).transaction_uom,
                                      L_svc_fulfilorddtlref_tbl(i).error_msg,
                                      SYSDATE,
                                      LP_user,
                                      SYSDATE,
                                      LP_user);

   O_process_id := L_process_id;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POP_CANCEL_TABLES;
-----------------------------------------------------------------------------------------------
FUNCTION CLEANUP_TABLES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                        I_action_type     IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program          VARCHAR2(255)   := 'STGSVC_FULFILORD.CLEANUP_TABLES';
   L_invalid_param    VARCHAR2(30)    := NULL;
   L_l10n_obj         L10N_OBJ        := L10N_OBJ();

   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);

   L_table            VARCHAR2(30)  := NULL;
   L_key1             VARCHAR2(100) := TO_CHAR(I_process_id);
   L_key2             VARCHAR2(100) := TO_CHAR(I_action_type);

   cursor C_LOCK_FULFILORD is
      select 'x'
        from svc_fulfilord
       where process_id = I_process_id
         and process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
         for update nowait;

   cursor C_LOCK_FULFILORDDTL is
      select 'x'
        from svc_fulfilorddtl dtl
       where dtl.process_id = I_process_id
         and exists (select 'x'
                       from svc_fulfilord hdr
                      where hdr.process_id = I_process_id
                        and hdr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
                        and hdr.fulfilord_id = dtl.fulfilord_id
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_FULFILORDCUST is
      select 'x'
        from svc_fulfilordcust cst
       where cst.process_id = I_process_id
         and exists (select 'x'
                       from svc_fulfilord hdr
                      where hdr.process_id = I_process_id
                        and hdr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
                        and hdr.fulfilord_id = cst.fulfilord_id
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_FULFILORDDTL_COMP is
      select 'x'
        from svc_fulfilorddtl_comp_item fci
       where fci.process_id = I_process_id
         and exists (select 'x'
                       from svc_fulfilord hdr
                      where hdr.process_id = I_process_id
                        and hdr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
                        and hdr.fulfilord_id = fci.fulfilord_id
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_FULFILORDREF is
      select 'x'
        from svc_fulfilordref
       where process_id = I_process_id
         and process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
         for update nowait;

   cursor C_LOCK_FULFILORDDTLREF is
      select 'x'
        from svc_fulfilorddtlref dtl
       where process_id = I_process_id
         and exists (select 'x'
                       from svc_fulfilordref hdr
                      where hdr.process_id = I_process_id
                        and hdr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
                        and hdr.fulfilordref_id = dtl.fulfilordref_id
                        and rownum = 1)
         for update nowait;

   cursor C_LOCALIZED_COUNTRY is
      select country_id
        from country_attrib
       where localized_ind = 'Y';

BEGIN

   if I_action_type is NULL then
      L_invalid_param := 'I_action_type';
   elsif I_process_id is NULL then
      L_invalid_param := 'I_process_id';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   if LOWER(I_action_type) = LOWER(CORESVC_FULFILORD.ACTION_TYPE_CREATE) then

      L_table := 'svc_fulfilordcust';
      open C_LOCK_FULFILORDCUST;
      close C_LOCK_FULFILORDCUST;

      delete from svc_fulfilordcust cst
       where cst.process_id = I_process_id
         and exists (select 'x'
                       from svc_fulfilord hdr
                      where hdr.process_id = I_process_id
                        and hdr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
                        and hdr.fulfilord_id = cst.fulfilord_id
                        and rownum = 1);

      L_table := 'svc_fulfilorddtl_comp_item';
      open C_LOCK_FULFILORDDTL_COMP;
      close C_LOCK_FULFILORDDTL_COMP;

      delete from svc_fulfilorddtl_comp_item fci
       where fci.process_id = I_process_id
         and exists (select 'x'
                       from svc_fulfilord hdr
                      where hdr.process_id = I_process_id
                        and hdr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
                        and hdr.fulfilord_id = fci.fulfilord_id
                        and rownum = 1);

      L_table := 'svc_fulfilorddtl';
      open C_LOCK_FULFILORDDTL;
      close C_LOCK_FULFILORDDTL;

      delete from svc_fulfilorddtl dtl
       where dtl.process_id = I_process_id
         and exists (select 'x'
                       from svc_fulfilord hdr
                      where hdr.process_id = I_process_id
                        and hdr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
                        and hdr.fulfilord_id = dtl.fulfilord_id
                        and rownum = 1);

      L_table := 'svc_fulfilord';
      open C_LOCK_FULFILORD;
      close C_LOCK_FULFILORD;

      delete from svc_fulfilord
       where process_id = I_process_id
         and process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED;

   elsif LOWER(I_action_type) = LOWER(CORESVC_FULFILORD.ACTION_TYPE_CANCEL) then

      L_table := 'svc_fulfilorddtlref';
      open C_LOCK_FULFILORDDTLREF;
      close C_LOCK_FULFILORDDTLREF;

      delete from svc_fulfilorddtlref dtl
       where process_id = I_process_id
         and exists (select 'x'
                       from svc_fulfilordref hdr
                      where hdr.process_id = I_process_id
                        and hdr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
                        and hdr.fulfilordref_id = dtl.fulfilordref_id
                        and rownum = 1);

      L_table := 'svc_fulfilordref';
      open C_LOCK_FULFILORDREF;
      close C_LOCK_FULFILORDREF;

      delete from svc_fulfilordref
       where process_id = I_process_id
         and process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED;

   end if;


   FOR rec in C_LOCALIZED_COUNTRY LOOP

      -- Populate localization object
      L_l10n_obj.procedure_key := 'CLEAR_SVC_FULFILORD_L10N';
      L_l10n_obj.doc_type      := NULL;
      L_l10n_obj.doc_id        := NULL;
      L_l10n_obj.country_id    := rec.country_id;
      L_l10n_obj.process_id    := I_process_id;

      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_obj) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CLEANUP_TABLES;
-----------------------------------------------------------------------------------------------
END STGSVC_FULFILORD;
/
