-- File Name : CORESVC_TRANSIT_TIMES_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_TRANSIT_TIMES AUTHID CURRENT_USER AS
   template_key           CONSTANT VARCHAR2(255)         :='TRANTIME_DATA';
   action_new                      VARCHAR2(25)          := 'NEW';
   action_mod                      VARCHAR2(25)          := 'MOD';
   action_del                      VARCHAR2(25)          := 'DEL';
   TRANSIT_TIMES_sheet             VARCHAR2(255)         := 'TRANSIT_TIMES';
   TRANSIT_TIMES$Action            NUMBER                :=1;
   TRANSIT_TIMES$TRANSIT_TIME      NUMBER                :=10;
   TRANSIT_TIMES$DESTINATION_TYPE  NUMBER                :=9;
   TRANSIT_TIMES$ORIGIN_TYPE       NUMBER                :=8;
   TRANSIT_TIMES$DESTINATION       NUMBER                :=7;
   TRANSIT_TIMES$ORIGIN            NUMBER                :=6;
   TRANSIT_TIMES$SUBCLASS          NUMBER                :=5;
   TRANSIT_TIMES$CLASS             NUMBER                :=4;
   TRANSIT_TIMES$DEPT              NUMBER                :=3;
   TRANSIT_TIMES$TRANSIT_TIMES_ID  NUMBER                :=2;
   sheet_name_trans                S9T_PKG.trans_map_typ;
   action_column                   VARCHAR2(255)         := 'ACTION';
   template_category               CODE_DETAIL.CODE%TYPE := 'RMSINV';

   TYPE TRANSIT_TIMES_rec_tab IS TABLE OF TRANSIT_TIMES%ROWTYPE;
-------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
						I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
					I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------------
END CORESVC_TRANSIT_TIMES;
/
