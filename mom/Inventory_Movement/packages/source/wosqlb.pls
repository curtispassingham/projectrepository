
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY WO_SQL AS
------------------------------------------------------------------------
-- private function 'overloading' PUBLIC DELETE_WO
-- for use with null order_no and null tsfalloc_no
-- this function is used when called from the work order form only
------------------------------------------------------------------------
FUNCTION DELETE_WO_NEITHER(O_error_message  IN OUT  VARCHAR2,
                           I_wo_id          IN	    WO_HEAD.WO_ID%TYPE,
                           I_item           IN      WO_DETAIL.ITEM%TYPE,
                           I_location       IN      WO_DETAIL.LOCATION%TYPE)
RETURN BOOLEAN is
---
L_program      VARCHAR2(64)       := 'WO_SQL.DELETE_WO_NEITHER';
L_table        VARCHAR2(30);
RECORD_LOCKED  EXCEPTION;
PRAGMA         EXCEPTION_INIT(Record_Locked, -54);
L_exists       VARCHAR2(1)        := NULL;
---
cursor C_EXISTS is
   select 'x'
     from wo_detail
    where wo_id = I_wo_id;
---
cursor C_LOCK_WO_DETAIL is
   select 'x'
     from wo_detail
    where wo_id = I_wo_id
      and item = NVL(I_item, item)
      and location = NVL(I_location, location)
      for update nowait;
---
cursor C_LOCK_WO_DETAIL2 is
   select 'x'
     from wo_detail
    where wo_id = I_wo_id
      for update nowait;
---
cursor C_LOCK_WO_SKU_LOC is
   select 'x'
     from wo_sku_loc
    where wo_id = I_wo_id
      for update nowait;
---
cursor C_LOCK_WO_WIP is
   select 'x'
     from wo_wip
    where wo_id = I_wo_id
      for update nowait;
---
cursor C_LOCK_WO_HEAD is
   select 'x'
     from wo_head
    where wo_id = I_wo_id
      for update nowait;
---
BEGIN
   ---
   if I_item is not NULL or
      I_location is not NULL then
      ---
      --- record locking
      L_table := 'WO_DETAIL';
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WO_DETAIL', 'WO_DETAIL', 'item: '||I_item||', location: '||to_char(I_location));
      open C_LOCK_WO_DETAIL;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WO_DETAIL', 'WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      close C_LOCK_WO_DETAIL;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_DETAIL', 'item: '||I_item||', location: '||to_char(I_location));
      ---
      delete from wo_detail
            where wo_id = I_wo_id
              and item = NVL(I_item, item)
              and location = NVL(I_location, location);
      ---
      /* if deletion from wo_detail results in all items and locations being deleted for
      any work orders, the rest of the work order should be deleted.  */
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', 'WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      open C_EXISTS;
      SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', 'WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      fetch C_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', 'WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      close C_EXISTS;
      ---
      if L_exists is NULL then /* detail records are deleted,delete rest */
         --- record locking
         L_table := 'WO_HEAD';
         SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WO_HEAD', 'WO_HEAD', 'wo id: '||to_char(I_wo_id));
         open C_LOCK_WO_HEAD;
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WO_HEAD', 'WO_HEAD', 'wo id: '||to_char(I_wo_id));
         close C_LOCK_WO_HEAD;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'WO_HEAD', 'wo_id: '||to_char(I_wo_id));
         ---
         delete from wo_head
          where wo_id = I_wo_id;
         ---
      end if;
      ---
   else -- I_item is null and I_location is NULL
      --- record locking
      L_table := 'WO_SKU_LOC';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_WO_SKU_LOC','WO_SKU_LOC','item: '||I_item||', location: '||to_char(I_location));
      open C_LOCK_WO_SKU_LOC;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_WO_SKU_LOC','WO_SKU_LOC','item: '||I_item||', location: '||to_char(I_location));
      close C_LOCK_WO_SKU_LOC;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_SKU_LOC','item: '||I_item||', location: '||to_char(I_location));
      ---
      delete from wo_sku_loc
            where wo_id = I_wo_id;

      --- record locking
      L_table := 'WO_WIP';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_WO_WIP','WO_WIP','item: '||I_item||', location: '||to_char(I_location));
      open C_LOCK_WO_WIP;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_WO_WIP','WO_WIP','item: '||I_item||', location: '||to_char(I_location));
      close C_LOCK_WO_WIP;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_WIP','item: '||I_item||', location: '||to_char(I_location));
      ---
      delete from wo_wip
            where wo_id = I_wo_id;

      --- record locking
      L_table := 'WO_DETAIL';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_WO_DETAIL2','WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      open C_LOCK_WO_DETAIL2;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_WO_DETAIL2','WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      close C_LOCK_WO_DETAIL2;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      ---
      delete from wo_detail
            where wo_id = I_wo_id;

      --- record locking
      L_table := 'WO_HEAD';
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WO_HEAD', 'WO_HEAD','wo_id: '||to_char(I_wo_id));
      open C_LOCK_WO_HEAD;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WO_HEAD', 'WO_HEAD','wo_id: '||to_char(I_wo_id));
      close C_LOCK_WO_HEAD;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_HEAD','wo_id: '||to_char(I_wo_id));
      ---
      delete from wo_head
       where wo_id = I_wo_id;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   ---
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'wo_id: '||to_char(I_wo_id),
                                            NULL);
      RETURN FALSE;
   ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;

END DELETE_WO_NEITHER;
------------------------------------------------------------------------
-- private function 'overloading' PUBLIC DELETE_WO
-- this function should NEVER be called with a NULL I_TSFALLOC_NO
-- This function is called from P_exit.cancel_form_head in the tsfalloc
-- form -- so that if user creates a new transfer allocation, then presses
-- cancel (while in NEW or NEWE mode), all assoicated workorders will be deleted!
------------------------------------------------------------------------
FUNCTION DELETE_WO_TSFALLOC_NO(O_error_message  IN OUT  VARCHAR2,
                               I_tsfalloc_no    IN      WO_HEAD.TSFALLOC_NO%TYPE,
                               I_item           IN      WO_DETAIL.ITEM%TYPE,
                               I_location       IN      WO_DETAIL.LOCATION%TYPE)
RETURN BOOLEAN is
---
L_program      VARCHAR2(64)       := 'WO_SQL.DELETE_WO_TSFALLOC_NO';
L_table        VARCHAR2(30);
RECORD_LOCKED  EXCEPTION;
PRAGMA         EXCEPTION_INIT(Record_Locked, -54);
L_wo_id        WO_HEAD.WO_ID%TYPE := NULL;
---
cursor C_WO_ID is
   select wo_id
     from wo_head wh
    where tsfalloc_no = I_tsfalloc_no
      and NOT EXISTS (select 'x'
                        from wo_detail w
                       where w.wo_id = wh.wo_id);
---
cursor C_LOCK_WO_DETAIL is
   select 'x'
     from wo_detail wd,
          wo_head wh
    where wh.wo_id = wd.wo_id
      and wh.tsfalloc_no = I_tsfalloc_no
      and wd.item = NVL(I_item, item)
      and wd.location = NVL(I_location, location)
      for update nowait;
---
cursor C_LOCK_WO_HEAD is
   select 'x'
     from wo_head
    where wo_id = L_wo_id
      for update nowait;
---
cursor C_LOCK_WO_DETAIL2 is
   select 'x'
     from wo_detail wd,
          wo_head wh
    where wd.wo_id = wh.wo_id
      and wh.tsfalloc_no = I_tsfalloc_no
      for update nowait;
---
cursor C_LOCK_WO_HEAD2 is
   select 'x'
     from wo_head
    where tsfalloc_no = I_tsfalloc_no
      for update nowait;
---
BEGIN
   ---
   if I_item is not NULL or
      I_location is not NULL then
      ---
      --- record locking
      L_table := 'WO_DETAIL';
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WO_DETAIL', 'WO_DETAIL', 'item: '||I_item||', location: '||to_char(I_location));
      open C_LOCK_WO_DETAIL;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WO_DETAIL', 'WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      close C_LOCK_WO_DETAIL;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_DETAIL', 'item: '||I_item||', location: '||to_char(I_location));
      ---
      delete from wo_detail
            where item = NVL(I_item, item)
              and location = NVL(I_location, location)
              and wo_id in (select wo_id 
                             from wo_head 
                            where tsfalloc_no = I_tsfalloc_no);
      ---
      FOR rec in C_WO_ID LOOP
         ---
         /* if deletion from wo_detail results in all items and locations being deleted for
         any work orders, the rest of the work order should be deleted.  */
         ---
         L_wo_id  := rec.wo_id;
         ---
         --- record locking
         L_table := 'WO_HEAD';
         SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WO_HEAD', 'WO_HEAD', 'wo id: '||to_char(L_wo_id));
         open C_LOCK_WO_HEAD;
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WO_HEAD', 'WO_HEAD', 'wo id: '||to_char(L_wo_id));
         close C_LOCK_WO_HEAD;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'WO_HEAD', 'wo_id: '||to_char(L_wo_id));
         ---
         delete from wo_head
          where wo_id = L_wo_id;
      ---
      END LOOP;
      ---
   else
      --- record locking
      L_table := 'WO_DETAIL';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_WO_DETAIL2','WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      open C_LOCK_WO_DETAIL2;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_WO_DETAIL2','WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      close C_LOCK_WO_DETAIL2;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      ---
      delete from wo_detail
            where wo_id in (select wo_id 
                             from wo_head
                            where tsfalloc_no = I_tsfalloc_no);

      --- record locking
      L_table := 'WO_HEAD';
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WO_HEAD2', 'WO_HEAD','tsfalloc no: '||to_char(I_tsfalloc_no));

      open C_LOCK_WO_HEAD2;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WO_HEAD2', 'WO_HEAD','tsfalloc no: '||to_char(I_tsfalloc_no));
      close C_LOCK_WO_HEAD2;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_HEAD','tsfalloc no: '||to_char(I_tsfalloc_no));

      ---
      delete from wo_head
       where tsfalloc_no = I_tsfalloc_no;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   ---
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'tsfalloc no: '||to_char(I_tsfalloc_no),
                                            NULL);
      RETURN FALSE;
   ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;

END DELETE_WO_TSFALLOC_NO;
------------------------------------------------------------------------
-- private function 'overloading' PUBLIC DELETE_WO
-- this function should NEVER be called with a NULL I_ORDER_NO
------------------------------------------------------------------------
FUNCTION DELETE_WO_ORDER_NO(O_error_message  IN OUT  VARCHAR2,
                            I_order_no       IN      WO_HEAD.ORDER_NO%TYPE,
                            I_item           IN      WO_DETAIL.ITEM%TYPE,
                            I_location       IN      WO_DETAIL.LOCATION%TYPE)
RETURN BOOLEAN is
---
L_program      VARCHAR2(64)       := 'WO_SQL.DELETE_WO_ORDER_NO';
L_table        VARCHAR2(30);
RECORD_LOCKED  EXCEPTION;
PRAGMA         EXCEPTION_INIT(Record_Locked, -54);
L_wo_id        WO_HEAD.WO_ID%TYPE := NULL;
---
cursor C_WO_ID is
   select wo_id
     from wo_head wh
    where order_no = I_order_no
      and NOT EXISTS (select 'x'
                        from wo_detail w
                       where w.wo_id = wh.wo_id);
---
cursor C_LOCK_WO_DETAIL is
   select 'x'
     from wo_detail wd,
          wo_head wh
    where wd.wo_id = wh.wo_id
      and wh.order_no = I_order_no
      and wd.item = NVL(I_item, item)
      and wd.location = NVL(I_location, location)
      for update nowait;
---
cursor C_LOCK_WO_HEAD is
   select 'x'
     from wo_head
    where wo_id = L_wo_id
      for update nowait;
---
cursor C_LOCK_WO_DETAIL2 is
   select 'x'
     from wo_detail wd,
          wo_head wh
    where wh.wo_id = wd.wo_id
      and wh.order_no = I_order_no
      for update nowait;
---
cursor C_LOCK_WO_HEAD2 is
   select 'x'
     from wo_head
    where order_no = I_order_no
      for update nowait;
---
BEGIN
   if I_order_no is NULL then
      return FALSE;
   end if;
   ---
   if I_item is not NULL or
      I_location is not NULL then
      ---
      --- record locking
      L_table := 'WO_DETAIL';
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WO_DETAIL', 'WO_DETAIL', 'item: '||I_item||', location: '||to_char(I_location));
      open C_LOCK_WO_DETAIL;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WO_DETAIL', 'WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      close C_LOCK_WO_DETAIL;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_DETAIL', 'item: '||I_item||', location: '||to_char(I_location));
      ---
      delete from wo_detail
            where item = NVL(I_item, item)
              and location = NVL(I_location, location)
              and wo_id in (select wo_id 
                             from wo_head
                            where order_no = I_order_no);
      ---
      FOR rec in C_WO_ID LOOP
         /* if deletion from wo_detail results in all items and locations being deleted for
         any work orders, the rest of the work order should be deleted.  */
         ---
         L_wo_id  := rec.wo_id;
         ---
         --- record locking
         L_table := 'WO_HEAD';
         SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WO_HEAD', 'WO_HEAD', 'wo id: '||to_char(L_wo_id));
         open C_LOCK_WO_HEAD;
         SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WO_HEAD', 'WO_HEAD', 'wo id: '||to_char(L_wo_id));
         close C_LOCK_WO_HEAD;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'WO_HEAD', 'wo_id: '||to_char(L_wo_id));
         ---
         delete from wo_head
               where wo_id = L_wo_id;
         ---
      END LOOP;
      ---
   else
      --- record locking
      L_table := 'WO_DETAIL';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_WO_DETAIL2','WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      open C_LOCK_WO_DETAIL2;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_WO_DETAIL2','WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      close C_LOCK_WO_DETAIL2;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_DETAIL','item: '||I_item||', location: '||to_char(I_location));
      ---
      delete from wo_detail
            where wo_id in (select wo_id
                             from wo_head
                            where order_no = I_order_no);
      ---
      --- record locking
      L_table := 'WO_HEAD';
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WO_HEAD2', 'WO_HEAD','order no: '||to_char(I_order_no));
      open C_LOCK_WO_HEAD2;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WO_HEAD2', 'WO_HEAD','order no: '||to_char(I_order_no));
      close C_LOCK_WO_HEAD2;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'WO_HEAD','order no: '||to_char(I_order_no));
      ---
      delete from wo_head
       where order_no = I_order_no;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   ---
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'order_no: '||to_char(I_order_no),
                                            NULL);
      RETURN FALSE;
   ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;

END DELETE_WO_ORDER_NO;
------------------------------------------------------------------------
FUNCTION DELETE_WO (O_error_message  IN OUT  VARCHAR2,
                    I_wo_id	         IN      WO_HEAD.WO_ID%TYPE,
                    I_order_no       IN      WO_HEAD.ORDER_NO%TYPE,
                    I_tsfalloc_no    IN      WO_HEAD.TSFALLOC_NO%TYPE,
                    I_item           IN      WO_DETAIL.ITEM%TYPE,
                    I_location       IN      WO_DETAIL.LOCATION%TYPE)
RETURN BOOLEAN is

L_program     varchar2(20) := 'WO_SQL.DELETE_WO';

BEGIN
   --** at least one of order_no or tsfalloc_no must be null **--
   if ( I_order_no is NOT NULL AND I_tsfalloc_no is NOT NULL )then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            null,
                                            L_program,
                                            null);
      return FALSE;

   elsif ( I_order_no is NULL AND I_tsfalloc_no is NULL )then
      if I_wo_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                               null,
                                               L_program,
                                               null);
         return FALSE;
      end if;
      ---
      if DELETE_WO_NEITHER(O_error_message,
                           I_wo_id,
                           I_item,
                           I_location) = FALSE then
         return FALSE;
      end if;

   elsif ( I_order_no is NULL AND I_tsfalloc_no is NOT NULL )then
      if DELETE_WO_TSFALLOC_NO(O_error_message,
                               I_tsfalloc_no,
                               I_item,
                               I_location) = FALSE then
         return FALSE;
      end if;

   else --** I_order_no is NOT NULL AND I_tsfalloc_no is NULL **--
      if DELETE_WO_ORDER_NO(O_error_message,
                            I_order_no,
                            I_item,
                            I_location) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END DELETE_WO;
------------------------------------------------------------------------
FUNCTION INSERT_SKULOCS(O_error_message   IN OUT VARCHAR2,
                        I_wo_id           IN     WO_HEAD.WO_ID%TYPE,
                        I_order_alloc_ind IN     VARCHAR2,
                        I_order_alloc_no  IN     WO_HEAD.ORDER_NO%TYPE,
                        I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                        I_tsf_to_loc      IN     TSFHEAD.TO_LOC%TYPE,
                        I_tsf_itemlist    IN     SKULIST_HEAD.SKULIST%TYPE,
                        I_tsf_item        IN     TSFDETAIL.ITEM%TYPE,
                        I_tsf_parent      IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                        I_tsf_diff1       IN     ITEM_MASTER.DIFF_1%TYPE,
                        I_tsf_diff2       IN     ITEM_MASTER.DIFF_2%TYPE)
RETURN BOOLEAN is
---
L_program        VARCHAR2(64)                := 'WO_SQL.INSERT_SKULOCS';
L_order_alloc_no WO_HEAD.ORDER_NO%TYPE;
---
L_cursor1             INTEGER;
L_rows_processed      INTEGER;
L_where_clause        FILTER_TEMP.WHERE_CLAUSE%TYPE;
L_select_statement    VARCHAR2(9000);
TYPE L_rec_type IS RECORD(
   wo_id     WO_HEAD.WO_ID%TYPE,
   wh        WO_DETAIL.WO_ID%TYPE,
   item      ITEM_MASTER.ITEM%TYPE,
   location  WO_DETAIL.LOCATION%TYPE,
   loc_type  WO_DETAIL.LOC_TYPE%TYPE);

TYPE L_detail_tbl_typ IS TABLE OF L_rec_type;
L_detail_tbl L_detail_tbl_typ;
---
BEGIN

   --- check input parameter
   if I_wo_id is NULL or
      I_order_alloc_ind is NULL or
      I_order_alloc_no is NULL or
      I_order_alloc_ind not in ('A', 'O') then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_order_alloc_ind = 'O' then
      ---
      L_where_clause := I_where_clause;
      L_order_alloc_no := I_order_alloc_no;
      ---
      if L_where_clause is not NULL then
         L_where_clause := ' and '||L_where_clause;
      end if;
      ---
      L_select_statement := 'select :l_wo_id '|| ',' ||
                            'NVL(xdock_phys_wh, location), '||
                            'item, '||
                            'location, '||
                            'loc_type '||
                     'from ordloc_wksht '||
                     'where order_no = :l_order_alloc_no  '|| L_where_clause||
                            ' and NOT EXISTS (select '||'''x'''||' '||
                                          'from wo_detail,  '||
                                               'wo_head '||
                                         'where wo_detail.wo_id = wo_head.wo_id '||
                                           'and wo_detail.item = ordloc_wksht.item '||
                                           'and wo_detail.location = ordloc_wksht.location '||
                                           'and wo_detail.loc_type = ordloc_wksht.loc_type '||
                                           'and wo_detail.wh = NVL(ordloc_wksht.xdock_phys_wh, ordloc_wksht.location) '||
                                           'and wo_head.order_no = ordloc_wksht.order_no)';

      EXECUTE IMMEDIATE L_select_statement BULK COLLECT into L_detail_tbl USING I_wo_id,L_order_alloc_no;

      -- insert fetched data into wo_sku_loc table
      for wo_int in L_detail_tbl.FIRST..L_detail_tbl.LAST
      LOOP
         insert into wo_sku_loc(wo_id,
                                wh,
                                item,
                                location,
                                loc_type)
                         values(L_detail_tbl(wo_int).wo_id,
                                L_detail_tbl(wo_int).wh,
                                L_detail_tbl(wo_int).item,
                                L_detail_tbl(wo_int).location,
                                L_detail_tbl(wo_int).loc_type);
      
      
               ---
      END LOOP;

-- Transfer work order functionality was removed in RMS10.0 by allocation team, 
-- which might be brought back in future.
-- Tsfalloc_no field no longer exists in tsf_head table. 
-- Code referencing to this field has been commented out.
-- commented by Shaam Khan - 5/8/2001
/*
   elsif I_order_alloc_ind = 'A' then
      if I_tsf_item is not NULL then
         ---
         insert into wo_sku_loc(wo_id,
                                wh,
                                item,
                                qty,
                                location,
                                loc_type,
                                tsfalloc_no,
                                order_no)
            (select distinct I_wo_id,
                    th.from_loc,
                    NVL(I_tsf_item,td.item),
                    td.tsf_qty, 
                    th.to_loc,
                    th.to_loc_type,
                    I_order_alloc_no,
                    NULL
               from tsfhead th,
                    tsfdetail td
              where th.tsf_no = td.tsf_no
                and th.tsfalloc_no = I_order_alloc_no
                and th.to_loc = NVL(I_tsf_to_loc, th.to_loc)
                and NOT EXISTS (select 'x'
                                  from wo_sku_loc
                                 where wo_sku_loc.item = NVL(I_tsf_item,td.item)
                                   and wo_sku_loc.location = th.to_loc
                                   and wo_sku_loc.loc_type = th.to_loc_type
                                   and wo_sku_loc.tsfalloc_no = th.tsfalloc_no));
         ---
      elsif I_tsf_itemlist is not NULL then
         ---
         -- get items in itemlist, check skulist detail.item instead of
         -- tsfdetail.item, then join to item.tsfdetail
         ---
         insert into wo_sku_loc(wo_id,
                                wh,
                                item,
                                qty,
                                location,
                                loc_type,
                                tsfalloc_no,
                                order_no)
         (select distinct I_wo_id,
                 th.from_loc,
                 sd.item,
                 td.tsf_qty,
                 th.to_loc,
                 th.to_loc_type,
                 I_order_alloc_no,
                 NULL
            from tsfhead th,
                 tsfdetail td,
                 skulist_detail sd
           where th.tsf_no = td.tsf_no
             and th.tsfalloc_no = I_order_alloc_no
             and th.to_loc = NVL(I_tsf_to_loc, th.to_loc)
             and td.item = sd.item
             and sd.skulist = I_tsf_itemlist
             and NOT EXISTS (select 'x'
                               from wo_sku_loc
                              where wo_sku_loc.item = td.item
                                and wo_sku_loc.location = th.to_loc
                                and wo_sku_loc.loc_type = th.to_loc_type
                                and wo_sku_loc.tsfalloc_no = th.tsfalloc_no));
      ---

      elsif I_tsf_parent is not NULL then -- when parent parameter is not null
         ---
         insert into wo_sku_loc(wo_id,
                                wh,
                                item,
                                qty,
                                location,
                                loc_type,
                                tsfalloc_no,
                                order_no)
         (select distinct I_wo_id,
                 th.from_loc,
                 im.item,
                 td.tsf_qty,
                 th.to_loc,
                 th.to_loc_type,
                 I_order_alloc_no,
                 NULL
            from tsfhead th,
                 tsfdetail td,
                 item_master  im
           where th.tsf_no = td.tsf_no
             and th.tsfalloc_no = I_order_alloc_no
             and th.to_loc = NVL(I_tsf_to_loc, th.to_loc)
             and td.item = im.item
             and im.item_parent = I_tsf_parent
             and ((I_tsf_diff1 is NULL or im.diff_1 = I_tsf_diff1)
             and (I_tsf_diff2 is NULL or im.diff_2 = I_tsf_diff2))
             and NOT EXISTS (select 'x'
                               from wo_sku_loc
                              where wo_sku_loc.item = td.item
                                and wo_sku_loc.location = th.to_loc
                                and wo_sku_loc.loc_type = th.to_loc_type
                                and wo_sku_loc.tsfalloc_no = th.tsfalloc_no));
      ---
      end if;
      ---
*/
   end if; /* order alloc ind is 'O' or 'A' */
   --
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      if DBMS_SQL.IS_OPEN (L_cursor1) then
         DBMS_SQL.CLOSE_CURSOR (L_cursor1);
      end if;

      return FALSE;
END INSERT_SKULOCS;
------------------------------------------------------------------------
FUNCTION EXPLD_WO_PACK(O_error_message  IN OUT  VARCHAR2,
                       I_order_no       IN      WO_HEAD.ORDER_NO%TYPE)
RETURN BOOLEAN is
---
L_program   VARCHAR2(64)   := 'WO_SQL.EXPLD_WO_PACK';
---
BEGIN
   --- check input parameter
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   insert into wo_detail(wo_id,
                         wh,
                         item,
                         location,
                         loc_type,
                         seq_no,
                         wip_code)
   (select wsl1.wo_id,
           wsl1.wh,
           v_packsku_qty.item,
           wsl1.location,
           wsl1.loc_type,
           wsl1.seq_no,
           wsl1.wip_code
      from wo_detail wsl1,
           v_packsku_qty,
           item_master,
           wo_head wh
     where wh.wo_id = wsl1.wo_id
       and wh.order_no = I_order_no
       and wsl1.item     = item_master.item
       and item_master.item = v_packsku_qty.pack_no
       and item_master.order_as_type = 'E'
       and NOT EXISTS(select 'x'
                        from wo_detail wsl2,
                             wo_head wh2
                       where wsl2.wo_id = wh2.wo_id 
                         and v_packsku_qty.item = wsl2.item
                         and wsl1.location = wsl2.location
                         and wh.order_no = wh2.order_no));
   ---
   --  delete packrecords just exploded
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'WO_DETAIL','order number: '||to_char(I_order_no));
   ---
   delete from wo_detail wsl1
         where wo_id in (select wo_id
                           from wo_head
                           where order_no = I_order_no)
           and exists(select 'x'
                        from item_master im,
                             wo_detail wsl2,
                             wo_head wh2
                       where wh2.wo_id = wsl2.wo_id
                         and wh2.order_no = I_order_no
                         and wsl2.item = im.item
                         and wsl2.item = wsl1.item
                         and im.order_as_type = 'E');
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END EXPLD_WO_PACK;
------------------------------------------------------------------------
FUNCTION INS_WO_DETAIL(O_error_message  IN OUT  VARCHAR2)

RETURN BOOLEAN is
---
L_program   VARCHAR2(64)   := 'WO_SQL.INS_WO_DETAIL';
---
BEGIN


   MERGE into wo_detail USING
     (select /*+ NO_MERGE */ wo_wip.wo_id,
             wo_sku_loc.wh,
             wo_sku_loc.item,
             wo_sku_loc.location,
             wo_sku_loc.loc_type,
             wo_wip.seq_no,
             wo_wip.wip_code
        from wo_wip, 
             wo_sku_loc
       where wo_wip.wo_id = wo_sku_loc.wo_id)  s
          on (wo_detail.wo_id = s.wo_id
         and wo_detail.wh = s.wh
         and wo_detail.item = s.item
         and wo_detail.location = s.location
         and wo_detail.seq_no = s.seq_no)
when matched then update set wo_detail.wip_code = s.wip_code
when not matched then insert (wo_id, 
                              wh, 
                              item, 
                              location, 
                              loc_type, 
                              seq_no, 
                              wip_code)
                      values (s.wo_id, 
                              s.wh, 
                              s.item, 
                              s.location, 
                              s.loc_type, 
                              s.seq_no, 
                              s.wip_code);


   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END INS_WO_DETAIL;
------------------------------------------------------------------------
FUNCTION POP_WO_INFO(O_error_message  IN OUT  VARCHAR2,
                     I_wo_id          IN      WO_HEAD.WO_ID%TYPE)
RETURN BOOLEAN is
---
L_program   VARCHAR2(64)   := 'WO_SQL.POP_WO_INFO';
---
BEGIN

   --- check input parameter
   if I_wo_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---

   insert into wo_sku_loc(item,
                          wo_id,
                          wh,
                          location,
                          loc_type)
   (select distinct(item),
           wo_id,
           wh,
           location,
           loc_type
      from wo_detail
     where wo_id = I_wo_id);

   insert into wo_wip(seq_no,
                      wo_id,
                      wip_code)
   (select distinct(seq_no),
           wo_id,
           wip_code
      from wo_detail
     where wo_id = I_wo_id);


   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END POP_WO_INFO;
------------------------------------------------------------------------
FUNCTION DEL_WO_DETAIL(O_error_message  IN OUT  VARCHAR2,
                       I_wo_id          IN      WO_HEAD.WO_ID%TYPE,
                       I_seq_no         IN      WO_WIP.SEQ_NO%TYPE)
RETURN BOOLEAN is
---
L_program   VARCHAR2(64)   := 'WO_SQL.DEL_WO_DETAIL';
---
BEGIN

   --- check input parameter
   if I_wo_id is NULL or I_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---

   delete from wo_detail 
    where wo_id = I_wo_id
      and seq_no = I_seq_no;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END DEL_WO_DETAIL;
------------------------------------------------------------------------
FUNCTION DEL_WO_RECS(O_error_message  IN OUT  VARCHAR2,
                     I_wo_id          IN      WO_HEAD.WO_ID%TYPE)
RETURN BOOLEAN is
---
L_program   VARCHAR2(64)   := 'WO_SQL.DEL_WO_RECS';
---
BEGIN

   --- check input parameter
   if I_wo_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   delete from wo_sku_loc
    where wo_id = I_wo_id;
 
   delete from wo_wip
    where wo_id = I_wo_id;


   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END DEL_WO_RECS;
------------------------------------------------------------------------
FUNCTION DEL_WO_ITEM(O_error_message  IN OUT  VARCHAR2,
                     I_wo_id          IN      WO_HEAD.WO_ID%TYPE,
                     I_item           IN      WO_SKU_LOC.ITEM%TYPE,
                     I_location       IN      WO_SKU_LOC.LOCATION%TYPE)
RETURN BOOLEAN is
---
L_program   VARCHAR2(64)   := 'WO_SQL.DEL_WO_ITEM';
---
BEGIN

   --- check input parameter
   if I_wo_id is NULL or I_item is NULL or I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
 
   delete from wo_detail 
    where wo_id = I_wo_id
      and item = I_item
      and location = I_location; 

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END DEL_WO_ITEM;
------------------------------------------------------------------------
FUNCTION CK_WO_CODE(O_error_message  IN OUT  VARCHAR2,
                    I_wo_id          IN      WO_HEAD.WO_ID%TYPE)
RETURN BOOLEAN is
---
L_program      VARCHAR2(64)       := 'WO_SQL.CK_WO_CODE';
L_exists       VARCHAR2(1)        := NULL;

cursor C_EXISTS is
select 'x'
  from wo_wip a,
       wo_wip b
 where a.wo_id = I_wo_id
   and a.wo_id = b.wo_id
   and a.wip_code = b.wip_code
   and a.seq_no != b.seq_no;

---
BEGIN

   --- check input parameter
   if I_wo_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---

   open C_EXISTS;
   fetch C_EXISTS into L_exists;
   close C_EXISTS;
   ---
   if L_exists is NOT NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('WIP_CODE_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END CK_WO_CODE;
------------------------------------------------------------------------
END WO_SQL;
/
