CREATE OR REPLACE PACKAGE BODY RMSSUB_SOSTATUS AS

--used by the ASN subscribe packages and their dependent packages
TYPE qty_allocated_TBL is table of tsfdetail.tsf_qty%TYPE INDEX BY BINARY_INTEGER;
TYPE qty_selected_TBL is table of tsfdetail.selected_qty%TYPE INDEX BY BINARY_INTEGER;
TYPE qty_distro_TBL is table of tsfdetail.distro_qty%TYPE INDEX BY BINARY_INTEGER;
TYPE qty_cancelled_TBL is table of tsfdetail.cancelled_qty%TYPE INDEX BY BINARY_INTEGER;
TYPE rowids_TBL is table of ROWID INDEX BY BINARY_INTEGER;
TYPE status_TBL is table of tsfhead.status%TYPE INDEX BY BINARY_INTEGER;

LP_vdate   PERIOD.VDATE%TYPE        := get_vdate;
LP_user    VARCHAR2(30)             := get_user;

LP_qty_allocated qty_allocated_TBL;
LP_qty_selected qty_selected_TBL;
LP_qty_distro qty_distro_TBL;
LP_qty_cancelled qty_cancelled_TBL;
LP_header_rowids rowids_TBL;
LP_header_status status_TBL;
LP_detail_rowids rowids_TBL;
LP_header_rowids_reverse_A rowids_TBL;
LP_alloc_details_size NUMBER := 0;
LP_tsf_details_size NUMBER := 0;
LP_tsf_headers_size NUMBER := 0;
LP_tsf_headers_size_reverse_A NUMBER := 0;

--used by RESET_TRANSFER function to reset the qty
LP_item ITEM_TBL := ITEM_TBL();
LP_item_details_size NUMBER := 0;

--global cache for doc_close_queue insert
TYPE doc_close_queue_doc_TBL  is table of doc_close_queue.doc%TYPE INDEX BY BINARY_INTEGER;
TYPE doc_close_queue_doc_type_TBL  is table of doc_close_queue.doc_type%TYPE INDEX BY BINARY_INTEGER;
LP_doc_close_queue_doc   doc_close_queue_doc_TBL;
LP_doc_close_queue_doc_type   doc_close_queue_doc_type_TBL;
LP_doc_close_queue_size NUMBER := 0;

--global variable for Stock Order integration
LP_exists BOOLEAN;

--global variable for 2-legged transfers
LP_update_child_tsf   VARCHAR2(1) := 'N';

---------------------------------------------------------------------
FUNCTION PARSE_SOS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_sos             IN       "RIB_SOStatusDesc_REC")
RETURN BOOLEAN;
---------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
FUNCTION PROCESS_SOS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_distro_type      IN       VARCHAR2,
                     I_distro_number    IN       TSFHEAD.TSF_NO%TYPE,
                     I_to_location      IN       STORE.STORE%TYPE,
                     I_from_location    IN       ITEM_LOC_SOH.LOC%TYPE,
                     I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                     I_qty              IN       NUMBER,
                     I_status           IN       VARCHAR2,
                     I_inventory_type   IN       TSFHEAD.INVENTORY_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION DO_BULK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION RESET(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
$end
---------------------------------------------------------------------
-- FUNCTION VALIDATE is called to validate the distro number.
---------------------------------------------------------------------
FUNCTION VALIDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_exist           IN OUT   BOOLEAN,
                  I_distro_number   IN       VARCHAR2,
                  I_document_type   IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function UPDATE_TSF is called to update the transfer record.
---------------------------------------------------------------------
FUNCTION UPDATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_distro_number   IN       TSFHEAD.TSF_NO%TYPE,
                    I_item            IN       ITEM_LOC_SOH.ITEM%TYPE,
                    I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE,
                    I_selected_qty    IN       TSFDETAIL.SELECTED_QTY%TYPE,
                    I_distro_qty      IN       TSFDETAIL.DISTRO_QTY%TYPE,
                    I_cancelled_qty   IN       TSFDETAIL.CANCELLED_QTY%TYPE,
                    I_status          IN       VARCHAR2,
                    I_tsf_status      IN       TSFHEAD.STATUS%TYPE,
                    I_rowid           IN       ROWID)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function UPDATE_CHILD_TSF is called to update the 2nd leg of the
-- transfer record.
---------------------------------------------------------------------
FUNCTION UPDATE_CHILD_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_distro_number   IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function UPDATE_ALLOC is called to update the allocation record.
---------------------------------------------------------------------
FUNCTION UPDATE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_distro_number   IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                      I_location        IN       STORE.STORE%TYPE,
                      I_qty_allocated   IN       ALLOC_DETAIL.QTY_ALLOCATED%TYPE,
                      I_qty_selected    IN       ALLOC_DETAIL.QTY_SELECTED%TYPE,
                      I_qty_distro      IN       ALLOC_DETAIL.QTY_DISTRO%TYPE,
                      I_qty_cancelled   IN       ALLOC_DETAIL.QTY_CANCELLED%TYPE,
                      I_status          IN       VARCHAR2,
                      I_rowid           IN       ROWID)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function UPD_FROM_ITEM_LOC is called to update the record on
-- item_loc_soh for the allocation from location. I_reserved_qty
-- contains a positive or negative value depending on the sostatus.
---------------------------------------------------------------------
FUNCTION UPD_FROM_ITEM_LOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_from_location    IN       ITEM_LOC.LOC%TYPE,
                           I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                           I_reserved_qty     IN       ITEM_LOC_SOH.TSF_RESERVED_QTY%TYPE,
                           I_comp_level_upd   IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function UPD_TO_ITEM_LOC is called to update the record on
-- item_loc_soh for the allocation to location. I_expected_qty
-- contains a positive or negative value depending on the sostatus.
---------------------------------------------------------------------
FUNCTION UPD_TO_ITEM_LOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_to_location      IN       ITEM_LOC.LOC%TYPE,
                         I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                         I_expected_qty     IN       ITEM_LOC_SOH.TSF_EXPECTED_QTY%TYPE,
                         I_comp_level_upd   IN       VARCHAR2)

RETURN BOOLEAN;
--------------------------------------------------------------------
FUNCTION GET_RECEIVE_AS_TYPE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_receive_as_type   IN OUT   VARCHAR2,
                             I_location          IN       NUMBER,
                             I_item              IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------
FUNCTION POPULATE_DOC_CLOSE_QUEUE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_type            IN       VARCHAR2,
                                  I_distro_no       IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION CREATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_sos             IN       "RIB_SOStatusDesc_REC")
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION PROCESS_TSF(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_distro_type      IN       VARCHAR2,
                     I_distro_number    IN       TSFHEAD.TSF_NO%TYPE,
                     I_to_location      IN       STORE.STORE%TYPE,
                     I_from_location    IN       ITEM_LOC_SOH.LOC%TYPE,
                     I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                     I_qty              IN       ITEM_LOC_SOH.TSF_EXPECTED_QTY%TYPE,
                     I_status           IN       VARCHAR2,
                     I_inventory_type   IN       TSFHEAD.INVENTORY_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION PROCESS_ALLOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_distro_number    IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                       I_to_location      IN       STORE.STORE%TYPE,
                       I_from_location    IN       ITEM_LOC_SOH.LOC%TYPE,
                       I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                       I_qty              IN       ITEM_LOC_SOH.TSF_EXPECTED_QTY%TYPE,
                       I_status           IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
   FUNCTION BUILD_XTSFDESC(O_error_message   IN OUT          VARCHAR2,
                           O_xtsfdesc_rec       OUT NOCOPY   "RIB_XTsfDesc_REC",
                           I_sos_rec         IN              "RIB_SOStatusDesc_REC")
   RETURN BOOLEAN;
$end
-----------------------------------------------------------------------
FUNCTION PROCESS_SI_SD_CR_TSF(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_distro_no        IN       TSFHEAD.TSF_NO%TYPE,
                              I_tsf_type         IN       TSFHEAD.TSF_TYPE%TYPE,
                              I_inventory_type   IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                              I_item             IN       ITEM_LOC_SOH.ITEM%TYPE ,
                              I_qty              IN       NUMBER,
                              I_from_loc         IN       ITEM_LOC_SOH.LOC%TYPE,
                              I_from_loc_type    IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                              I_to_loc           IN       ITEM_LOC_SOH.LOC%TYPE,
                              I_to_loc_type      IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                              I_inv_status       IN       TSFDETAIL.INV_STATUS%TYPE,
                              I_status           IN       VARCHAR2,
                              I_delivery_date    IN       TSFHEAD.DELIVERY_DATE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION DIST_FROM_LOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_inv_flow_array   IN OUT   BOL_SQL.INV_FLOW_ARRAY,
                       I_tsf_no           IN       TSFHEAD.TSF_NO%TYPE,
                       I_tsf_seq_no       IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                       I_item             IN       ITEM_MASTER.ITEM%TYPE,
                       I_inv_status       IN       SHIPSKU.INV_STATUS%TYPE,
                       I_tsf_qty          IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_phy_to_loc       IN       ITEM_LOC.LOC%TYPE,
                       I_to_loc_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                       I_phy_from_loc     IN       ITEM_LOC.LOC%TYPE,
                       I_from_loc_type    IN       ITEM_LOC.LOC_TYPE%TYPE,
                       I_dist_type        IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status           IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
-----------------------------------------------------------------------
---Function RESET_TRANSFER is called for setting the tsf_qty to 0 for those items
---that exists on tsfdetail but not on sostatus record
------------------------------------------------------------------------------
FUNCTION RESET_TRANSFER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tsf             IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------
/* Function and Procedure Bodies */
-----------------------------------------------------------------------
PROCEDURE CONSUME(O_STATUS_CODE   IN OUT VARCHAR2,
                  O_ERROR_MESSAGE IN OUT VARCHAR2,
                  I_MESSAGE       IN     RIB_OBJECT,
                  I_MESSAGE_TYPE  IN     VARCHAR2)
IS
   L_pgm_name               VARCHAR2(61)           := 'RMSSUB_SOSTATUS.CONSUME';
   L_rib_sostatusdesc_rec   "RIB_SOStatusDesc_REC" := NULL;
   PROGRAM_ERROR            EXCEPTION;
BEGIN
   if I_message_type is NULL or
      lower(I_message_type) != 'sostatuscre' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE_TYPE', I_message_type, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   L_rib_sostatusdesc_rec := treat(I_MESSAGE AS "RIB_SOStatusDesc_REC");

   if L_rib_sostatusdesc_rec is NULL or
      L_rib_sostatusdesc_rec.SOStatusDtl_TBL is NULL or
      L_rib_sostatusdesc_rec.distro_nbr is NULL or
      L_rib_sostatusdesc_rec.distro_document_type is NULL then
      O_status_code := API_CODES.SUCCESS;
      return;
   end if;

   if RESET(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if PARSE_SOS(O_error_message,
                L_rib_sostatusdesc_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if DO_BULK(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Update 2nd leg AFTER data is inserted to tables for the 1st leg
   if LP_update_child_tsf = 'Y' then
      if UPDATE_CHILD_TSF(O_error_message,
                          to_number(L_rib_sostatusdesc_rec.distro_nbr)) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   end if;

   O_STATUS_CODE := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_STATUS_CODE,
                     O_ERROR_MESSAGE,
                     API_LIBRARY.FATAL_ERROR,
                     L_pgm_name);
   when OTHERS then
       HANDLE_ERRORS(O_STATUS_CODE,
                     O_ERROR_MESSAGE,
                     API_LIBRARY.FATAL_ERROR,
                     L_pgm_name);

END CONSUME;
-----------------------------------------------------------------------
FUNCTION PARSE_SOS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_sos             IN       "RIB_SOStatusDesc_REC")
RETURN BOOLEAN IS

   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.PARSE_SOS';
   L_to_location          STORE.STORE%TYPE;
   L_from_location        STORE.STORE%TYPE;      -- ignore for now
   L_distro_no            TSFHEAD.TSF_NO%TYPE;
   L_type                 VARCHAR2(2);
   L_dest_id              WH.WH%TYPE;
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_qty                  TSFDETAIL.TSF_QTY%TYPE;
   L_status               VARCHAR2(2);
   L_inventory_type       TSFHEAD.INVENTORY_TYPE%TYPE;
   L_tsf_inventory_type   TSFHEAD.INVENTORY_TYPE%TYPE;
   L_cust_order_nbr       ORDCUST.CUSTOMER_ORDER_NO%TYPE;
   L_fulfill_order_nbr    ORDCUST.FULFILL_ORDER_NO%TYPE;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_tsf_type             TSFHEAD.TSF_TYPE%TYPE;
   L_exists               VARCHAR2(1) := NULL;
   L_im_rec               ITEM_MASTER%ROWTYPE;
   L_CR_flag              VARCHAR2(1):='N';
   L_SI_SD_flag           VARCHAR2(1):='N';

   cursor C_GET_INVENTORY_TYPE is
      select inventory_type,
             tsf_type
        from tsfhead
       where tsf_no = L_distro_no;

   cursor C_CHECK_ORDCUST is
      select 'x'
        from ordcust
       where tsf_no = L_distro_no
         and customer_order_no = L_cust_order_nbr
         and fulfill_order_no = L_fulfill_order_nbr;

BEGIN
   L_distro_no         := to_number(I_sos.distro_nbr);
   L_type              := I_sos.distro_document_type;
   L_dest_id           := to_number(I_sos.dc_dest_id);
   L_inventory_type    := I_sos.inventory_type;
   L_cust_order_nbr    := I_sos.cust_order_nbr;
   L_fulfill_order_nbr := I_sos.fulfill_order_nbr;

   if L_type IN ('T','A','D','S') then
      if VALIDATE(O_error_message,
                  LP_exists,
                  L_distro_no,
                  L_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      -- Document types 'T' and 'A' are transfers and allocations created in RMS.
      -- They should already exist on tsfhead and alloc_header. Return error if it doesn't exist.
      -- Document types 'D' and 'S' are external transfers created in WMS and SIM respectively.
      -- SIM and WMS can use SOStatus message with these document types to create 'EG' type of transfers in RMS.
      if LP_exists = FALSE and
         L_type IN ('T','A') then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DISTRO_NUM_EXIST',
                                               L_distro_no,
                                               L_type,
                                               NULL);
         return FALSE;
      end if;

      if LP_exists = TRUE and
         L_type NOT IN ('D','A') then
         open C_GET_INVENTORY_TYPE;
         fetch C_GET_INVENTORY_TYPE into L_tsf_inventory_type,
                                         L_tsf_type;
         close C_GET_INVENTORY_TYPE;

         if L_inventory_type is NULL then
            L_inventory_type := L_tsf_inventory_type;
         elsif L_tsf_inventory_type <> L_inventory_type then
            O_error_message := SQL_LIB.CREATE_MSG('INV_TYPE_NOT_MATCH',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
         ---
         if L_tsf_type = 'CO' then
            if L_cust_order_nbr is NULL or
               L_fulfill_order_nbr is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('MISSING_ORDCUST_INFO',
                                                     L_distro_no,
                                                     NULL,
                                                     NULL);
               return FALSE;
            else
               open C_CHECK_ORDCUST;
               fetch C_CHECK_ORDCUST into L_exists;
               close C_CHECK_ORDCUST;
               if L_exists is NULL then
                  O_error_message := SQL_LIB.CREATE_MSG('CUST_FULFILL_ORD_MISMATCH',
                                                        L_distro_no,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;
            end if;
         end if; -- L_tsf_type = 'CO'
         ---
      end if; -- LP_exists = TRUE and L_type NOT IN ('D','A')
   end if; -- L_type IN ('T','A','D','S')

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   FOR i in I_sos.SOStatusDtl_TBL.FIRST .. I_sos.SOStatusDtl_TBL.LAST LOOP
      --To prevent RMS from processing the same cancellation message already sent by the external Order
      --Management System (OMS), skip processing of 'NI', 'EX' statuses sent by WMS and 'SD' and 'SI' statuses sent by SIM.
      if L_system_options_row.oms_ind = 'Y' and
         L_tsf_type = 'CO' and
         I_sos.SOStatusDtl_TBL(i).status IN ('NI', 'EX', 'SD', 'SI') then
         continue;
      end if;
      ---
      L_status             := I_sos.SOStatusDtl_TBL(i).status;
      LP_item_details_size := LP_item_details_size + 1;
      LP_item.extend;
      L_item                        := I_sos.SOStatusDtl_TBL(i).item_id;
      LP_item(LP_item_details_size) := L_item;
      ---
      if L_status = 'CR' then
         L_CR_flag :='Y';
      elsif L_status in ('SI', 'SD') then
         L_SI_SD_flag := 'Y';
      end if;
   END LOOP;

   if L_CR_flag = 'Y' and
      L_SI_SD_flag = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('SO_CR_SI_INV',
                                            L_distro_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if LP_exists = TRUE and
      L_CR_flag = 'Y' and
      L_SI_SD_flag = 'N' then
      if RESET_TRANSFER(O_error_message,
                        L_distro_no) = FALSE then
         return FALSE;
      END IF;
   end if;

   FOR i IN I_sos.SOStatusDtl_TBL.FIRST .. I_sos.SOStatusDtl_TBL.LAST LOOP
      --To prevent RMS from processing the same cancellation message already sent by the external Order
      --Management System (OMS), skip processing of 'NI', 'EX' statuses sent by WMS and 'SD' and 'SI' statuses sent by SIM.
      if L_system_options_row.oms_ind = 'Y' and
         L_tsf_type = 'CO' and
         I_sos.SOStatusDtl_TBL(i).status IN ('NI', 'EX', 'SD', 'SI') then
         continue;
      end if;

      L_item        := I_sos.SOStatusDtl_TBL(i).item_id;
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message, 
                                      L_im_rec, 
                                      I_sos.SOStatusDtl_TBL(i).item_id) = FALSE then 
      return FALSE; 
   end if; 
   
    if L_im_rec.item_level > L_im_rec.tran_level then 
          L_item := L_im_rec.item_parent; 
     end if; 



      L_qty         := I_sos.SOStatusDtl_TBL(i).unit_qty;
      L_status      := I_sos.SOStatusDtl_TBL(i).status;
      L_to_location := I_sos.SOStatusDtl_TBL(i).dest_id;

      -- Document type of 'D' and 'S' are external transfers created in WMS and SIM.
      -- They will have physical warehouses on the transfer. Inventory are held in virtual wh
      -- only in RMS. Physical to virtual warehouse qty distribution logic will be supported for
      -- 'SI' (Select/Insert) and 'SD' (Deselect) and 'NI' (No Inventory) status.
      ---
      if L_status = 'SD' and
         L_type IN ('T','A') then
         L_status := 'NI';
      end if;
      ---
      if L_type IN ('D', 'S') then -- External Transfer
         if L_status IN ('EX', 'RS') then -- Expired, Return to Stock
            O_error_message := SQL_LIB.CREATE_MSG('INV_SOSTATUS',
                                                  L_type,
                                                  L_status,
                                                  NULL);
            return FALSE;
         elsif L_status IN ('SI', 'CR') and -- Increase quantity
               LP_exists = FALSE then
            if CREATE_TSF(O_error_message,
                          I_sos) = FALSE then
               return FALSE;
            else
               return TRUE;
            end if;
         elsif L_status = 'SD' and -- Decrease quantity
               LP_exists = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('NO_TSF_TO_DEL', L_distro_no, NULL, NULL);
            return FALSE;
         end if;
      end if;
      ---
      -- Update quantity fields on ITEM_LOC_SOH and either TSFDETAIL or ALLOC_DETAIL
      if PROCESS_SOS(O_error_message,
                    L_type,
                    L_distro_no,
                    L_to_location,
                    L_dest_id,
                    L_item,
                    L_qty,
                    L_status,
                    L_inventory_type) = FALSE then
        return FALSE;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END PARSE_SOS;
-----------------------------------------------------------------------
FUNCTION PROCESS_SOS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_distro_type      IN       VARCHAR2,
                     I_distro_number    IN       TSFHEAD.TSF_NO%TYPE,
                     I_to_location      IN       STORE.STORE%TYPE,
                     I_from_location    IN       ITEM_LOC_SOH.LOC%TYPE,
                     I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                     I_qty              IN       NUMBER,
                     I_status           IN       VARCHAR2,
                     I_inventory_type   IN       TSFHEAD.INVENTORY_TYPE%TYPE)
RETURN BOOLEAN IS
   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.PROCESS_SOS';

BEGIN

   -- Check input
   if I_from_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_from_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_to_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_to_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   -- ignore records containing an invalid status
   if I_status NOT IN ('SI', 'SD', 'DS', 'DU', 'PP', 'PU', 'SR', 'NI', 'EX', 'RS', 'CR') then
      return TRUE;
   end if;

   if I_distro_type in ('T','S','D') then
      if PROCESS_TSF(O_error_message,
                     I_distro_type,
                     I_distro_number,
                     I_to_location,
                     I_from_location,
                     I_item,
                     I_qty,
                     I_status,
                     I_inventory_type) = FALSE then
         return FALSE;
      end if;
   elsif I_distro_type = 'A' then
      if PROCESS_ALLOC(O_error_message,
                       I_distro_number,
                       I_to_location,
                       I_from_location,
                       I_item,
                       I_qty,
                       I_status) = FALSE then
         return FALSE;
      end if;
   else /* I_distro_type in ('C', 'V') */
      /* Customer Orders that are not present in RMS, Virtual Distro */
      /* or SIM/WMS created transfer that doesn't exist in RMS yet. */
      return TRUE; -- stop, do not pass back an error
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_SOS;
---------------------------------------------------------------------
FUNCTION PROCESS_TSF(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_distro_type      IN       VARCHAR2,
                     I_distro_number    IN       TSFHEAD.TSF_NO%TYPE,
                     I_to_location      IN       STORE.STORE%TYPE,
                     I_from_location    IN       ITEM_LOC_SOH.LOC%TYPE,
                     I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                     I_qty              IN       ITEM_LOC_SOH.TSF_EXPECTED_QTY%TYPE,
                     I_status           IN       VARCHAR2,
                     I_inventory_type   IN       TSFHEAD.INVENTORY_TYPE%TYPE)
RETURN BOOLEAN IS

   L_pgm_name              TRAN_DATA.PGM_NAME%TYPE            := 'RMSSUB_SOSTATUS.PROCESS_TSF';

   L_from_loc_type         TSFHEAD.FROM_LOC_TYPE%TYPE         := NULL;
   L_to_loc_type           TSFHEAD.TO_LOC_TYPE%TYPE           := NULL;
   L_from_location         ITEM_LOC_SOH.LOC%TYPE              := NULL;
   L_to_location           STORE.STORE%TYPE                   := NULL;
   L_status                VARCHAR2(2)                        := I_status;
   L_qty                   NUMBER(12,4)                       := I_qty;
   L_selected_qty          TSFDETAIL.SELECTED_QTY%TYPE        := 0;
   L_distro_qty            TSFDETAIL.DISTRO_QTY%TYPE          := 0;
   L_cancelled_qty         TSFDETAIL.CANCELLED_QTY%TYPE       := 0;
   L_tsf_qty               TSFDETAIL.TSF_QTY%TYPE             := 0;
   L_shiptran_qty          TSFDETAIL.SHIP_QTY%TYPE            := NULL;

   L_distro_status         TSFHEAD.STATUS%TYPE                := NULL;
   L_tsf_type              TSFHEAD.TSF_TYPE%TYPE              := NULL;
   L_tsf_parent_no         TSFHEAD.TSF_PARENT_NO%TYPE         := NULL;
   L_inv_type              TSFHEAD.INVENTORY_TYPE%TYPE        := NULL;
   L_delivery_date         TSFHEAD.DELIVERY_DATE%TYPE         := NULL;
   L_inv_status            TSFDETAIL.INV_STATUS%TYPE          := NULL;
   L_tsf_seq_no            TSFDETAIL.TSF_SEQ_NO%TYPE          := NULL;

   L_found                 BOOLEAN                            := FALSE;
   L_vdate                 DATE                               := get_vdate();
   L_tran_code             TRAN_DATA.TRAN_CODE%TYPE           := NULL;
   L_tsf_is_first_leg      VARCHAR2(1)                        := 'N';
   L_dummy                 VARCHAR2(1)                        := NULL;
   L_xform_exists          BOOLEAN;
   L_packing_exists        BOOLEAN;
   L_inv_array             BOL_SQL.INV_FLOW_ARRAY;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_table        VARCHAR2(30) := 'TSFDETAIL';
   L_rowid        ROWID := NULL;

   cursor C_TSF is
      select NVL(td.tsf_qty, 0),
             NVL(td.ship_qty, 0),
             th.from_loc,
             th.from_loc_type,
             th.to_loc,
             th.to_loc_type,
             th.status,
             th.tsf_type,
             th.tsf_parent_no,
             th.inventory_type,
             th.delivery_date,
             td.inv_status,
             td.tsf_seq_no,
             td.rowid
        from tsfdetail td,
             tsfhead th
       where td.tsf_no(+) = th.tsf_no
         and th.tsf_no = I_distro_number
         and item(+) = I_item
         for update of td.tsf_qty nowait;

   cursor C_PARENT_TSF is
      select 'Y'
        from tsfhead
       where tsf_parent_no = I_distro_number;

   cursor C_CHECK_FROM_VWH is
      select 'x'
        from tsfhead th,
             wh
       where th.tsf_no = I_distro_number
         and th.from_loc = wh.wh
         and wh.physical_wh = I_from_location;

   cursor C_CHECK_TO_VWH is
      select 'x'
        from tsfhead th,
             wh
       where th.tsf_no = I_distro_number
         and th.to_loc = wh.wh
         and wh.physical_wh = I_to_location;

BEGIN

   open C_TSF;
   fetch C_TSF into L_tsf_qty,
                    L_shiptran_qty,
                    L_from_location,
                    L_from_loc_type,
                    L_to_location,
                    L_to_loc_type,
                    L_distro_status,
                    L_tsf_type,
                    L_tsf_parent_no,
                    L_inv_type,
                    L_delivery_date,
                    L_inv_status,
                    L_tsf_seq_no,
                    L_rowid;
   close C_TSF;

   /* If no transfer detail was fetched, message should fail. */
   if L_distro_status is NULL and
      I_distro_type NOT IN ('S', 'D') and
      L_status != 'SI' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_ITEM',
                                            I_item,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   /* If transfer header status is marked for delete and the message type
      is 'SD', then ignore; otherwise, error out */
   if L_distro_status IN ('D') then
      if L_status = 'SD' then
         return TRUE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('TSF_DEL_CLOSED',
                                               I_distro_number,
                                               NULL,
                                               NULL);
      end if;
   end if;

   -- perform location related validation
   if L_from_location is NULL then
      L_from_location := I_from_location;
   end if;

   if L_to_location is NULL then
      L_to_location := I_to_location;
   end if;

   if I_distro_type IN ('S', 'D') then  -- SIM/WMS generated transfers
      -- perform warehouse validations for external transfers created from SIM or WMS
      -- Warehouses on the message will always be a physical warehouse.
      -- For transfers initially created in SIM or RWMS (I_distro_type of 'S' or 'D'), physical wh will be
      -- on tsfhead and alloc_header. So L_from_location must match I_from_location and L_to_location
      -- must match I_to_location.
      if L_from_location != I_from_location then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_FROM_LOC',
                                               I_from_location,
                                               I_distro_number,
                                               NULL);
         return FALSE;
      end if;
      if L_to_location != I_to_location then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_TO_LOC',
                                               I_to_location,
                                               I_distro_number,
                                               NULL);
         return FALSE;
      end if;

   elsif I_distro_type = 'T' then  -- RMS generated transfers
      -- perform warehouse validations for transfers created in RMS
      -- Warehouses on the message will always be a physical wh, but virtual wh will be on TSFHEAD.
      -- Wh on the message must match the physical wh of the virtual whs on TSFHEAD.
      if L_from_loc_type is NOT NULL and
         L_from_loc_type = 'W' then
         open C_CHECK_FROM_VWH;
         fetch C_CHECK_FROM_VWH into L_dummy;
         close C_CHECK_FROM_VWH;

         if NVL(L_dummy,'y') != 'x' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_FROM_LOC',
                                                  I_from_location,
                                                  I_distro_number,
                                                  NULL);
            return FALSE;
         end if;
      end if;

      L_dummy := NULL;
      if L_to_loc_type is NOT NULL and
         L_to_loc_type = 'W' then
         open C_CHECK_TO_VWH;
         fetch C_CHECK_TO_VWH into L_dummy;
         close C_CHECK_TO_VWH;

         if NVL(L_dummy,'y') != 'x' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_TO_LOC',
                                                  I_to_location,
                                                  I_distro_number,
                                                  NULL);
            return FALSE;
         end if;
      end if;

      if L_from_loc_type is NOT NULL and
         L_from_loc_type = 'S' and
         L_from_location != I_from_location then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_FROM_LOC',
                                               I_from_location,
                                               I_distro_number,
                                               NULL);
         return FALSE;
      end if;

      if L_to_loc_type is NOT NULL and
         L_to_loc_type = 'S' and
         L_to_location != I_to_location then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_TO_LOC',
                                               I_to_location,
                                               I_distro_number,
                                               NULL);
         return FALSE;
      end if;
   end if;

   if L_status IN ('SI', 'SD', 'CR') then /* Increase quantity, Decrease quantity */
      -- Create XTSF message and update ITEM_LOC_SOH
      if PROCESS_SI_SD_CR_TSF(O_error_message,
                              I_distro_number,
                              L_tsf_type,
                              I_inventory_type,
                              I_item,
                              L_qty,
                              L_from_location,
                              L_from_loc_type,
                              L_to_location,
                              L_to_loc_type,
                              L_inv_status,
                              L_status,
                              L_delivery_date) = FALSE then
         return FALSE;
      end if;

      -- For transfers, SI/SD/CR statuses are completely handled in PROCESS_SI_SD_CR_TSF function.
      -- No further processing is needed.
      return TRUE;
   end if;

    if L_status = 'SR' AND SIGN(L_qty) = -1 then
       L_status       := 'NI';
       L_distro_qty   := L_qty;
      /* Making L_qty positive because if I change the message type from 'SR'  to 'NI' then it should work properly */
      L_qty          := L_qty * -1;
   end if;
   
   ---
   -- Perform inventory calculation based on status
   ---
   if L_status = 'DS' then /* Details Selected */
      L_selected_qty := L_qty;
      L_tsf_qty := 0;
   elsif L_status = 'DU' then /* Details Unselected */
      L_selected_qty := L_qty * -1;
      L_tsf_qty := 0;
   elsif L_status = 'PP' then /* Pick Pending */
      L_selected_qty := L_qty * -1;
      L_distro_qty   := L_qty;
      L_tsf_qty := 0;
   elsif L_status IN ('PU') then /*Un-Distribute */
      L_distro_qty   := L_qty * -1;
      L_tsf_qty := 0;
   elsif L_status = 'SR' then /* Store Reassign */
      /* RDM will send a negative number for the loc which will no longer */
      /* get the stock.  They will send a positive number for the loc     */
      /* which will get the stock.                                        */
      L_distro_qty   := L_qty;
      L_tsf_qty := 0;
   elsif L_status IN ('NI', 'EX', 'RS') then /* No Inventory, Expired, Return to Stock */
      if L_status = 'NI' then
         L_selected_qty := L_qty * -1;
      elsif L_status = 'RS' then
         L_distro_qty   := L_qty * -1;
      end if;
      ---
      if L_qty > (L_tsf_qty - L_shiptran_qty) then
         L_qty := (L_tsf_qty - L_shiptran_qty);
      end if;
      ---
      L_cancelled_qty := L_qty;
      L_tsf_qty := L_qty * -1;
      ---
   end if;
   ---

   /* Back out expected_qty at to loc and reserved qty at from loc for each item-loc. */
   /* Only 'NI'/'EX'/'RS' statuses can have an L_tsf_qty of non-zero, */
   /* which will affect tsf_qty and require ITEM_LOC_SOH update. */
   if L_status IN ('NI', 'RS', 'EX') then    /* No Inventory, Expired, Return to Stock */

      /* Do not update inventory for closed transfers since any unshipped */
      /* qty is already backed out at the time of closing the transfer. */
      if L_distro_status != 'C' then
         if I_distro_type = 'T' then
            if L_distro_status not in('I') then
               if TRANSFER_SQL.UPD_ITEM_RESV_EXP(O_error_message,
                                                 I_item,
                                                 L_tsf_type,
                                                 L_tsf_qty,
                                                 L_from_loc_type,
                                                 L_from_location,
                                                 L_to_loc_type,
                                                 L_to_location) = FALSE then
                  return FALSE;
               end if;
            end if;

            if L_tsf_parent_no is NULL then
               -- ----------------------------------------------------------------
               -- Increment the item's unavailable quantity at the From
               -- location. Do this if Inventory Type is not 'A'
               -- ----------------------------------------------------------------
               if L_inv_type != 'A' then

                  if INVADJ_SQL.ADJ_UNAVAILABLE(I_item,
                                                L_inv_status,
                                                L_from_loc_type,
                                                L_from_location,
                                                L_qty,
                                                O_error_message,
                                                L_found) = FALSE then
                     return FALSE;
                  end if;
                  -- insert a tran_data record (code 25)
                  L_tran_code := 25; -- for unavailable inventory transfer

                  if INVADJ_SQL.ADJ_TRAN_DATA(I_item,
                                              L_from_loc_type,
                                              L_from_location,
                                              L_qty,
                                              L_pgm_name,
                                              L_vdate,
                                              L_tran_code,
                                              NULL,
                                              L_inv_status,
                                              NULL,
                                              NULL,
                                              O_error_message,
                                              L_found) = FALSE then
                     return FALSE;
                  end if;
               end if;

               open C_PARENT_TSF;
               fetch C_PARENT_TSF into L_tsf_is_first_leg;
               close C_PARENT_TSF;

               if L_tsf_is_first_leg = 'Y' then
                  -- If this is the first leg of a transfer with finishing update tranformation and
                  -- packing quantities and update the transfer quantities of the second leg
                  if ITEM_XFORM_PACK_SQL.XFORM_PACKING_EXIST(O_error_message,
                                                             L_xform_exists,
                                                             L_packing_exists,
                                                             I_distro_number,
                                                             I_item) = FALSE then
                     return FALSE;
                  end if;

                  if L_xform_exists or
                     L_packing_exists then
                     if ITEM_XFORM_PACK_SQL.UPDATE_XFORM_PACK_RESULT(O_error_message,
                                                                     I_distro_number,
                                                                     I_item,
                                                                     L_tsf_qty) = FALSE then
                        return FALSE;
                     end if;
                  end if;
                  -- Update TSFDETAIL quantities for 2nd leg AFTER the call to DO_BULK, since the
                  -- 2nd leg quantities are populated based on the 1st leg quantities present in the tables
                  LP_update_child_tsf := 'Y';
               end if;
            end if; -- L_tsf_parent_no is NULL

         elsif I_distro_type IN ('D', 'S') then -- External Transfer
            -- Previous validation ensures that it will never be 'EX'/'RS' for distro_type of 'D'/'S' here
            if L_from_loc_type = 'S' and L_to_loc_type = 'S' then
               if TRANSFER_SQL.UPD_ITEM_RESV_EXP(O_error_message,
                                                 I_item,
                                                 L_tsf_type,
                                                 L_tsf_qty,
                                                 L_from_loc_type,
                                                 L_from_location,
                                                 L_to_loc_type,
                                                 L_to_location) = FALSE then
                  return FALSE;
               end if;
            else
               if DIST_FROM_LOC(O_error_message,
                                L_inv_array,
                                I_distro_number,
                                L_tsf_seq_no,
                                I_item,
                                L_inv_status,
                                I_qty,
                                I_to_location,
                                L_to_loc_type,
                                I_from_location,
                                L_from_loc_type,
                                'S') = FALSE then
                  return FALSE;
               end if;
            end if;
         end if; -- I_distro_type

         /* If RMS receives a status of EX that hasn't already been closed, RDM is telling */
         /* us they are done with the distro. Write to doc_close_queue to be processed by  */
         /* docclose.pc later                                                              */
         if L_status in ('EX', 'NI') then
            if POPULATE_DOC_CLOSE_QUEUE(O_error_message,
                                        'T',   -- for transfer
                                        I_distro_number) = FALSE then
               return FALSE;
            end if;
         end if;
      end if; -- I_distro_status != 'C'
   end if; -- L_status IN ('NI', 'RS', 'EX')

   -- Statuses of SI/SD/CR are excluded in earlier code
   if UPDATE_TSF(O_error_message,
                 I_distro_number,
                 I_item,
                 L_tsf_qty,
                 L_selected_qty,
                 L_distro_qty,
                 L_cancelled_qty,
                 L_status,
                 L_distro_status,
                 L_rowid) = FALSE then

      return FALSE;
   end if;
   ---
   --Code to handle so-status message PP and PU for transfers when Inventory type is unavailable
   --For 'PP' message Increase Unavailable quantity and post tran_data record
   --For 'PU' message Decrease Unavailable quantity and post tran_data record
   if L_status in ('PP','PU') and I_distro_type='T' then 
      if L_tsf_parent_no is NULL and L_inv_type='U' then
         if INVADJ_SQL.ADJ_UNAVAILABLE(I_item,
                                       L_inv_status,
                                       L_from_loc_type,
                                       L_from_location,
                                       L_distro_qty,
                                       O_error_message,
                                       L_found) = FALSE then
            return FALSE;
         end if;
         -- insert a tran_data record (code 25)
         L_tran_code := 25; -- for unavailable inventory transfer
         if INVADJ_SQL.ADJ_TRAN_DATA(I_item,
                                     L_from_loc_type,
                                     L_from_location,
                                     L_distro_qty,
                                     L_pgm_name,
                                     L_vdate,
                                     L_tran_code,
                                     NULL,
                                     L_inv_status,
                                     NULL,
                                     NULL,
                                     O_error_message,
                                     L_found) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;  
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'TSF_NO:'||to_char(I_distro_number),
                                             'ITEM:'||I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_TSF;
-----------------------------------------------------------------------
FUNCTION PROCESS_ALLOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_distro_number    IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                       I_to_location      IN       STORE.STORE%TYPE,
                       I_from_location    IN       ITEM_LOC_SOH.LOC%TYPE,
                       I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                       I_qty              IN       ITEM_LOC_SOH.TSF_EXPECTED_QTY%TYPE,
                       I_status           IN       VARCHAR2)
RETURN BOOLEAN IS

   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.PROCESS_ALLOC';

   L_from_location         ITEM_LOC.LOC%TYPE                  := NULL;
   L_to_location           ITEM_LOC.LOC%TYPE                  := NULL;

   L_qty                   NUMBER(12,4)                       := I_qty;
   L_selected_qty          ALLOC_DETAIL.QTY_SELECTED%TYPE     := 0;
   L_distro_qty            ALLOC_DETAIL.QTY_DISTRO%TYPE       := 0;
   L_cancelled_qty         ALLOC_DETAIL.QTY_CANCELLED%TYPE    := 0;
   L_alloc_qty             ALLOC_DETAIL.QTY_ALLOCATED%TYPE    := 0;
   L_shiptran_qty          ALLOC_DETAIL.QTY_TRANSFERRED%TYPE  := NULL;

   L_order_no              ALLOC_HEADER.ORDER_NO%TYPE         := NULL;
   L_distro_status         ALLOC_HEADER.STATUS%TYPE           := NULL;
   L_alloc_type            VARCHAR2(1)                        := NULL;

   L_pack_ind              ITEM_MASTER.PACK_IND%TYPE          := NULL;
   L_pack_item             ITEM_LOC_SOH.ITEM%TYPE             := NULL;
   L_orderable_ind         ITEM_MASTER.ORDERABLE_IND%TYPE     := NULL;
   L_sellable_ind          ITEM_MASTER.SELLABLE_IND%TYPE      := NULL;
   L_pack_type             ITEM_MASTER.PACK_TYPE%TYPE         := NULL;
   L_to_receive_as_type    ITEM_LOC.RECEIVE_AS_TYPE%TYPE      := NULL;
   L_pack_comp_resv        ITEM_LOC_SOH.PACK_COMP_RESV%TYPE   := NULL;
   L_pack_comp_exp         ITEM_LOC_SOH.PACK_COMP_EXP%TYPE    := NULL;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_table        VARCHAR2(30) := 'ALLOC_DETAIL';
   L_rowid        ROWID := NULL;

   cursor C_ALLOC is
      select NVL(ad.qty_allocated, 0),
             NVL(ad.qty_transferred, 0),
             ah.wh,
             ah.order_no,
             ah.status,
             ad.rowid
        from alloc_header ah,
             alloc_detail ad,
             wh w
       where ah.alloc_no = ad.alloc_no
         and ad.alloc_no = I_distro_number
         and to_loc =  NVL(w.wh, L_to_location)
         and w.wh (+) = to_loc
         and w.physical_wh (+) = L_to_location
         for update of ad.qty_allocated nowait;

   cursor C_GET_COMPS is
      select vpq.item,
             vpq.qty
        from v_packsku_qty vpq,
             item_master im
       where vpq.pack_no = I_item
         and vpq.item = im.item
         and im.inventory_ind = 'Y';

BEGIN

   L_to_location := I_to_location;

   open C_ALLOC;
   fetch C_ALLOC into L_alloc_qty,
                      L_shiptran_qty,
                      L_from_location,
                      L_order_no,
                      L_distro_status,
                      L_rowid;
   close C_ALLOC;
   ---
   /* If no allocation was fetched, message should fail. */
   if L_distro_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_ALLOC_DET',
                                            to_char(L_to_location),
                                            to_char(I_distro_number),
                                            NULL);
      return FALSE;
   end if;

   if L_order_no is NOT NULL then
      L_alloc_type := 'X'; --cross doc allocation
   end if;

   if L_from_location is NULL then
      L_from_location := I_from_location;
   end if;

   ---
   -- Perform inventory calculation based on status
   if I_status = 'DS' then /* Details Selected */
      L_selected_qty := L_qty;
      L_alloc_qty := 0;
   elsif I_status = 'DU' then /* Details Unselected */
      L_selected_qty := L_qty * -1;
      L_alloc_qty := 0;
   elsif I_status = 'PP' then /* Pick Pending */
      L_selected_qty := L_qty * -1;
      L_distro_qty   := L_qty;
      L_alloc_qty := 0;
   elsif I_status IN ('PU') then /*Un-Distribute */
      L_distro_qty   := L_qty * -1;
      L_alloc_qty := 0;
   elsif I_status = 'SR' then /* Store Reassign */
      /* RDM will send a negative number for the loc which will no longer */
      /* get the stock.  They will send a positive number for the loc     */
      /* which will get the stock.                                        */
      L_distro_qty   := L_qty;
      L_alloc_qty := 0;
   elsif I_status IN ('NI', 'EX', 'RS') then /* No Inventory, Expired, Return to Stock */
      --Note: for distro_type of 'T' and 'A', 'SD' status is switched to 'NI'
      if I_status = 'NI' then
         L_selected_qty := L_qty * -1;
      elsif I_status = 'RS' then
         L_distro_qty   := L_qty * -1;
      end if;
      ---
      if L_qty > (L_alloc_qty - L_shiptran_qty) then
         L_qty := (L_alloc_qty - L_shiptran_qty);
      end if;
      ---
      L_cancelled_qty := L_qty;
      L_alloc_qty := L_qty * -1;
      ---
   elsif I_status = 'SI' then   /* Stock Increase */
      L_selected_qty := 0;
      L_distro_qty   := 0;
      L_alloc_qty := L_qty;
   end if;
   ---

   /* Process ITEM_LOC_SOH updates for each item loc. */
   /* L_alloc_qty can only be non-zero for statuses NI/EX/RS/SI/SD, which will */
   /* affect alloc_qty and require updating ITEM_LOC_SOH for expected and reserved qty. */
   /* If an allocation is already closed, do NOT update ITEM_LOC_SOH since any unshipped */
   /* allocation quantity would been backed out when the allocation was closed. */

   if I_status in ('NI', 'EX', 'RS', 'SI', 'SD') and
      L_distro_status != 'C' then

      -- Qty was not reserved for a cross doc allocation.
      if L_alloc_type is NULL or
         L_alloc_type != 'X' then
         ---
         if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                          L_pack_ind,
                                          L_sellable_ind,
                                          L_orderable_ind,
                                          L_pack_type,
                                          I_item) = FALSE then
            return FALSE;
         end if;
         ---
         if UPD_FROM_ITEM_LOC(O_error_message,
                              L_from_location,
                              I_item,
                              L_alloc_qty,     -- Carries the correct sign based on status.
                              'N') = FALSE then
            return FALSE;
         end if;
         ---
         if L_pack_ind = 'N' then
            ---
            if UPD_TO_ITEM_LOC(O_error_message,
                               L_to_location,
                               I_item,
                               L_alloc_qty,    -- Carries the correct sign based on status.
                               'N') = FALSE then
               return FALSE;
            end if;
         elsif L_pack_ind = 'Y' then
            ---
            FOR C_rec IN C_GET_COMPS LOOP
               L_pack_item := C_rec.item;
               ---
               L_pack_comp_resv := C_rec.qty * L_alloc_qty;
               ---
               if UPD_FROM_ITEM_LOC(O_error_message,
                                    L_from_location,
                                    L_pack_item,
                                    L_pack_comp_resv,    -- Carries the correct sign based on status.
                                    'Y') = FALSE then
                  return FALSE;
               end if;
            END LOOP;
            ---
            if GET_RECEIVE_AS_TYPE(O_error_message,
                                   L_to_receive_as_type,
                                   L_to_location,
                                   I_item) = FALSE then
               return FALSE;
            end if;
            ---
            if L_to_receive_as_type = 'P' then

               if UPD_TO_ITEM_LOC(O_error_message,
                                  L_to_location,
                                  I_item,
                                  L_alloc_qty,    -- Carries the correct sign based on status.
                                  'N') = FALSE then
                  return FALSE;
               end if;
               ---
               FOR C_rec IN C_GET_COMPS LOOP
                  L_pack_item := C_rec.item;
                  ---
                  L_pack_comp_exp := C_rec.qty * L_alloc_qty;
                  ---
                  if UPD_TO_ITEM_LOC(O_error_message,
                                     L_to_location,
                                     L_pack_item,
                                     L_pack_comp_exp,    -- Carries the correct sign based on status.
                                     'Y') = FALSE then
                     return FALSE;
                  end if;
               END LOOP;
            else /* to_receive_as_type != 'P' */
               FOR C_rec IN C_GET_COMPS LOOP
                  L_pack_item := C_rec.item;
                  ---
                  L_pack_comp_exp := C_rec.qty * L_alloc_qty;
                  if UPD_TO_ITEM_LOC(O_error_message,
                                     L_to_location,
                                     L_pack_item,
                                     L_pack_comp_exp,    -- Carries the correct sign based on status.
                                     'N') = FALSE then
                        return FALSE;
                  end if;
               END LOOP;
            end if; -- L_to_receive_as_type
         end if; -- L_pack_ind
      end if; -- L_alloc_type is NULL or != 'X'

      /* If RMS receives a status of EX that hasn't already been closed, RDM is telling */
      /* us they are done with the distro. Write to doc_close_queue to be processed by  */
      /* docclose.pc later                                                              */
      if I_status in ('EX', 'NI') then
         if POPULATE_DOC_CLOSE_QUEUE(O_error_message,
                                    'A',
                                     I_distro_number) = FALSE then
            return FALSE;
         end if;
      end if;
   end if; -- I_status and L_distro_status

   -- Update ALLOC_DETAIL table
   if UPDATE_ALLOC(O_error_message,
                   I_distro_number,
                   I_to_location,
                   L_alloc_qty,
                   L_selected_qty,
                   L_distro_qty,
                   L_cancelled_qty,
                   I_status,
                   L_rowid) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'ALLOC_NO:'||to_char(I_distro_number),
                                             'ITEM:'||I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_ALLOC;
-----------------------------------------------------------------------
FUNCTION VALIDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_exist           IN OUT   BOOLEAN,
                  I_distro_number   IN       VARCHAR2,
                  I_document_type   IN       VARCHAR2)
RETURN BOOLEAN IS

   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.VALIDATE';
   L_exists VARCHAR2(1) := NULL;

   cursor C_VALIDATE is
      select 'x' from tsfhead
       where tsf_no = I_distro_number
         and I_document_type IN ('T','S','D')
      UNION
      select 'x' from alloc_header
       where alloc_no = I_distro_number
         and I_document_type = 'A';

BEGIN
   open C_VALIDATE;
   fetch C_VALIDATE into L_exists;
   close C_VALIDATE;

   if L_exists is NULL then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE;
---------------------------------------------------------------------
FUNCTION UPDATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_distro_number   IN       TSFHEAD.TSF_NO%TYPE,
                    I_item            IN       ITEM_LOC_SOH.ITEM%TYPE,
                    I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE,
                    I_selected_qty    IN       TSFDETAIL.SELECTED_QTY%TYPE,
                    I_distro_qty      IN       TSFDETAIL.DISTRO_QTY%TYPE,
                    I_cancelled_qty   IN       TSFDETAIL.CANCELLED_QTY%TYPE,
                    I_status          IN       VARCHAR2,
                    I_tsf_status      IN       TSFHEAD.STATUS%TYPE,
                    I_rowid           IN       ROWID)
RETURN BOOLEAN IS

   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.UPDATE_TSF';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_rowid       ROWID := NULL;
   L_table       VARCHAR2(30) := 'TSFHEAD';

   cursor C_LOCK_HEAD is
      select rowid
        from tsfhead
       where tsf_no = I_distro_number
         for update nowait;
BEGIN

   if I_rowid is NOT NULL then
      LP_tsf_details_size := LP_tsf_details_size + 1;

      LP_qty_allocated(LP_tsf_details_size) := I_tsf_qty;
      LP_qty_selected(LP_tsf_details_size)  := I_selected_qty;
      LP_qty_distro(LP_tsf_details_size)    := I_distro_qty;
      LP_qty_cancelled(LP_tsf_details_size) := I_cancelled_qty;
      LP_detail_rowids(LP_tsf_details_size) := I_rowid;
   end if;
   ---
   /* populate the variables to update tsfhead status */
   /* If the transfer has a status of 'A'pproved and items have been selected,
        set the status to se'L'ected. */
   /* If the transfer has a status of 'A'pproved or se'L'ected and items have been picked,
        set the status to 'P'icked. */
   if (I_tsf_status = 'A' and
       I_selected_qty > 0) or
       ((I_tsf_status = 'A' or
         I_tsf_status = 'L') and
        I_distro_qty > 0) then
      open C_LOCK_HEAD;
      fetch C_LOCK_HEAD into L_rowid;
      close C_LOCK_HEAD;
      ---
      LP_tsf_headers_size := LP_tsf_headers_size + 1;

      LP_header_rowids(LP_tsf_headers_size) := L_rowid;
      if I_tsf_status = 'A' and
         I_selected_qty > 0 then
         LP_header_status(LP_tsf_headers_size) := 'L';
      elsif (I_tsf_status = 'A' or
             I_tsf_status = 'L') and
            I_distro_qty > 0 then
         LP_header_status(LP_tsf_headers_size) := 'P';
      end if;
   end if;
if (I_status  = 'PU' or I_status = 'DU') then 
        open C_LOCK_HEAD; 
        fetch C_LOCK_HEAD into L_rowid; 
        close C_LOCK_HEAD;        
     LP_tsf_headers_size_reverse_A := LP_tsf_headers_size_reverse_A + 1; 
     LP_header_rowids_reverse_A(LP_tsf_headers_size_reverse_A) := L_rowid; 
end if; 
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'TSF_NO:'||to_char(I_distro_number),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_TSF;
---------------------------------------------------------------------
FUNCTION UPDATE_CHILD_TSF(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_distro_number    IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS
   L_pgm_name              TRAN_DATA.PGM_NAME%TYPE := 'RMSSUB_SOSTATUS.UPDATE_CHILD_TSF';

BEGIN
   -- Update the transfer quantities for the 2nd leg
   if TRANSFER_SQL.UPDATE_TSFDETAIL(O_error_message,
                                    I_distro_number) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_CHILD_TSF;
---------------------------------------------------------------------
FUNCTION UPDATE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_distro_number   IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                      I_location        IN       STORE.STORE%TYPE,
                      I_qty_allocated   IN       ALLOC_DETAIL.QTY_ALLOCATED%TYPE,
                      I_qty_selected    IN       ALLOC_DETAIL.QTY_SELECTED%TYPE,
                      I_qty_distro      IN       ALLOC_DETAIL.QTY_DISTRO%TYPE,
                      I_qty_cancelled   IN       ALLOC_DETAIL.QTY_CANCELLED%TYPE,
                      I_status          IN       VARCHAR2,
                      I_rowid           IN       ROWID)
RETURN BOOLEAN IS

   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.UPDATE_ALLOC';

BEGIN

   if I_rowid is NOT NULL then
      LP_alloc_details_size := LP_alloc_details_size + 1;

      LP_qty_allocated(LP_alloc_details_size) := I_qty_allocated;
      LP_qty_selected(LP_alloc_details_size) := I_qty_selected;
      LP_qty_distro(LP_alloc_details_size) := I_qty_distro;
      LP_qty_cancelled(LP_alloc_details_size) := I_qty_cancelled;
      LP_detail_rowids(LP_alloc_details_size) := I_rowid;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ALLOC;
---------------------------------------------------------------------
FUNCTION UPD_FROM_ITEM_LOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_from_location    IN       ITEM_LOC.LOC%TYPE,
                           I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                           I_reserved_qty     IN       ITEM_LOC_SOH.TSF_RESERVED_QTY%TYPE,    --can be positive or negative
                           I_comp_level_upd   IN       VARCHAR2)
RETURN BOOLEAN IS

   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.UPD_FROM_ITEM_LOC';

   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_table       VARCHAR2(30) := 'ITEM_LOC_SOH';
   L_rowid       ROWID;

   cursor C_LOCK_FROM is
      select rowid
        from item_loc_soh
       where item = I_item
         and loc  = I_from_location
         for update nowait;

BEGIN
   open C_LOCK_FROM;
   fetch C_LOCK_FROM into L_rowid;
   close C_LOCK_FROM;
   ---
   update item_loc_soh
      set tsf_reserved_qty     = DECODE(I_comp_level_upd,
                                        'N', (tsf_reserved_qty + I_reserved_qty),
                                        tsf_reserved_qty),
          pack_comp_resv       = DECODE(I_comp_level_upd,
                                        'Y', (pack_comp_resv + I_reserved_qty),
                                        pack_comp_resv),
          last_update_datetime = SYSDATE,
          last_update_id       = LP_user
    where rowid = L_rowid;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'ITEM:'||I_item,
                                            'LOC:'||to_char(I_from_location));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_FROM_ITEM_LOC;
----------------------------------------------------------------
FUNCTION UPD_TO_ITEM_LOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_to_location      IN       ITEM_LOC.LOC%TYPE,
                         I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                         I_expected_qty     IN       ITEM_LOC_SOH.TSF_EXPECTED_QTY%TYPE,
                         I_comp_level_upd   IN       VARCHAR2)
RETURN BOOLEAN IS

   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.UPD_TO_ITEM_LOC';

   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_rowid       ROWID;
   L_table       VARCHAR2(30) := 'ITEM_LOC_SOH';

   cursor C_LOCK_TO is
      select rowid
        from item_loc_soh
       where item = I_item
         and loc  = I_to_location
         for update nowait;
BEGIN
   open C_LOCK_TO;
   fetch C_LOCK_TO into L_rowid;
   close C_LOCK_TO;
   ---
   update item_loc_soh
      set tsf_expected_qty = DECODE(I_comp_level_upd,
                                    'N', (tsf_expected_qty + I_expected_qty),
                                    tsf_expected_qty),
          pack_comp_exp    = DECODE(I_comp_level_upd,
                                    'Y', (pack_comp_exp + I_expected_qty),
                                    pack_comp_exp),
          last_update_datetime = SYSDATE,
          last_update_id       = LP_user
    where rowid = L_rowid;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'ITEM:'||I_item,
                                            'LOC:'||to_char(I_to_location));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_TO_ITEM_LOC;
----------------------------------------------------------------
FUNCTION GET_RECEIVE_AS_TYPE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_receive_as_type   IN OUT   VARCHAR2,
                             I_location          IN       NUMBER,
                             I_item              IN       VARCHAR2)
RETURN BOOLEAN IS

   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.GET_RECEIVE_AS_TYPE';

   cursor C_RECEIVE_AS_TYPE is
      select receive_as_type
        from item_loc
       where item = I_item
         and loc  = I_location;
BEGIN

   open C_RECEIVE_AS_TYPE;
   fetch C_RECEIVE_AS_TYPE into O_receive_as_type;
   close C_RECEIVE_AS_TYPE;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END GET_RECEIVE_AS_TYPE;
-----------------------------------------------------------------------
FUNCTION POPULATE_DOC_CLOSE_QUEUE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_type            IN       VARCHAR2,
                                  I_distro_no       IN       VARCHAR2)
RETURN BOOLEAN IS
   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.POPULATE_DOC_CLOSE_QUEUE';
BEGIN

   if LP_doc_close_queue_size = 0 or
       LP_doc_close_queue_doc(LP_doc_close_queue_size) != I_distro_no then

      LP_doc_close_queue_size := LP_doc_close_queue_size + 1;
      LP_doc_close_queue_doc(LP_doc_close_queue_size)        := I_distro_no;
      LP_doc_close_queue_doc_type(LP_doc_close_queue_size)   := I_type;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_DOC_CLOSE_QUEUE;
-----------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status           IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2)
IS
   L_pgm_name  VARCHAR2(61) := 'RMSSUB_SOSTATUS.HANDLE_ERRORS';
BEGIN

   -- If a record is locked, a cause of FATAL_ERROR will be
   -- passed in along with the TABLE_LOCKED create_msg
   -- and the table locked status and fully parsed error message
   -- will be set in the API_LIBRARY.
   API_LIBRARY.HANDLE_ERRORS(O_status,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_pgm_name,
                                             to_char(SQLCODE));
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_pgm_name);

END HANDLE_ERRORS;
-----------------------------------------------------------------------
FUNCTION RESET(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_pgm_name   VARCHAR2(61)   := 'RMSSUB_SOSTATUS.RESET';
BEGIN
   LP_qty_allocated.DELETE;
   LP_qty_selected.DELETE;
   LP_qty_distro.DELETE;
   LP_qty_cancelled.DELETE;
   LP_detail_rowids.DELETE;
   LP_item.DELETE;

   LP_detail_rowids.DELETE;
   LP_header_rowids.DELETE;
   LP_header_rowids_reverse_A.DELETE;
   LP_doc_close_queue_doc.DELETE;
   LP_doc_close_queue_doc_type.DELETE;

   LP_alloc_details_size := 0;

   LP_tsf_details_size  := 0;
   LP_tsf_headers_size  := 0;
   LP_item_details_size := 0;
   LP_tsf_headers_size_reverse_A := 0;

   LP_doc_close_queue_size := 0;
   LP_exists := FALSE;

   LP_update_child_tsf := 'N';

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END RESET;
-----------------------------------------------------------------------
FUNCTION DO_BULK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_pgm_name   VARCHAR2(61)   := 'RMSSUB_SOSTATUS.DO_BULK';
BEGIN
   --Ensure selected_qty does not get decremented to less than 0.  If a qty is picked and never selected
   --allocdetail.qty_selected will remain 0 rather than driving it negative.
   if LP_alloc_details_size > 0 then
      FORALL i IN 1..LP_alloc_details_size
         update alloc_detail
            set qty_allocated = NVL(qty_allocated, 0) + LP_qty_allocated(i),
                qty_selected  = GREATEST(NVL(qty_selected, 0) + LP_qty_selected(i), 0),
                qty_distro    = NVL(qty_distro, 0) + LP_qty_distro(i),
                qty_cancelled = NVL(qty_cancelled, 0) + LP_qty_cancelled(i)
          where rowid = LP_detail_rowids(i);
   end if;

   LP_alloc_details_size := 0;

   --ensure selected_qty does not get decremented to less than 0.  If a qty is picked and never selected
   --tsfdetail.selected_qty will remain 0 rather than driving it negative.
   if LP_tsf_details_size > 0 then
      FORALL i IN 1..LP_tsf_details_size
         update tsfdetail
            set tsf_qty       = NVL(tsf_qty, 0) + LP_qty_allocated(i),
                selected_qty  = GREATEST(NVL(selected_qty, 0) + LP_qty_selected(i), 0),
                distro_qty    = NVL(distro_qty, 0) + LP_qty_distro(i),
                cancelled_qty = NVL(cancelled_qty, 0) + LP_qty_cancelled(i),
                updated_by_rms_ind = 'N'
          where rowid = LP_detail_rowids(i);
   end if;

   if LP_tsf_headers_size > 0 then
      FORALL i IN 1..LP_tsf_headers_size
         update tsfhead
            set status = LP_header_status(i)
          where rowid = LP_header_rowids(i);
   end if;
if LP_tsf_headers_size_reverse_A > 0 then 
     FORALL i in 1..LP_tsf_headers_size_reverse_A 
          Update tsfhead h set status = 'A' 
           Where status = 'P'  
            and rowid = LP_header_rowids_reverse_A(i) 
            and not exists (select 'x' from tsfdetail d where d.tsf_no = h.tsf_no and d.distro_qty > 0); 
end if; 
       
if LP_tsf_headers_size_reverse_A > 0 then 
      FORALL i in 1..LP_tsf_headers_size_reverse_A 
          Update tsfhead h set status = 'A' 
           Where status = 'L'  
           and rowid = LP_header_rowids_reverse_A(i) 
           and not exists (select 'x' from tsfdetail d where d.tsf_no = h.tsf_no and d.selected_qty > 0);    
end if; 
   LP_tsf_details_size := 0;
   LP_tsf_headers_size := 0;
   LP_tsf_headers_size_reverse_A := 0;

   --- insert into doc_close_queue for distro with 'EX' status
   if LP_doc_close_queue_size > 0 then
      FORALL i IN 1..LP_doc_close_queue_size
         insert into doc_close_queue(doc,
                                     doc_type)
                              values(LP_doc_close_queue_doc(i),
                                     LP_doc_close_queue_doc_type(i));
   end if;

   LP_doc_close_queue_size := 0;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END DO_BULK;
-----------------------------------------------------------------------
FUNCTION CREATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_sos             IN       "RIB_SOStatusDesc_REC")
RETURN BOOLEAN IS
   L_program            VARCHAR2(60) := 'RMSSUB_SOSTATUS.CREATE_TSF';
   L_sos_rec            "RIB_SOStatusDesc_REC";
   L_xtsfdesc_rec       "RIB_XTsfDesc_REC";
   L_message_type       VARCHAR2(50);
   O_status_code        VARCHAR2(2);
BEGIN

   L_sos_rec := I_sos;

   if BUILD_XTSFDESC(O_error_message,
                     L_xtsfdesc_rec,
                     L_sos_rec) = FALSE then
      return FALSE;
   end if;

   L_message_type := RMSSUB_XTSF.LP_cre_type;

   RMSSUB_XTSF.CONSUME(O_status_code,
                       O_error_message,
                       L_xtsfdesc_rec,
                       L_message_type);

   if O_status_code != 'S' then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_TSF;
-----------------------------------------------------------------------
FUNCTION BUILD_XTSFDESC(O_error_message IN OUT        VARCHAR2,
                        O_xtsfdesc_rec     OUT NOCOPY "RIB_XTsfDesc_REC",
                        I_sos_rec       IN            "RIB_SOStatusDesc_REC")
RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'RMSSUB_SOSTATUS.BUILD_XTSFDESC';

   L_tsf_no           TSFHEAD.TSF_NO%TYPE;
   L_message_status   TSFHEAD.STATUS%TYPE;

   L_xtsfdtl_rec      "RIB_XTsfDtl_REC";
   L_xtsfdtl_tbl      "RIB_XTsfDtl_TBL"  := NULL;

   L_item             ITEM_LOC_SOH.ITEM%TYPE                := NULL;
   L_qty              TSFDETAIL.TSF_QTY%TYPE                := NULL;
   L_from_loc         TSFHEAD.FROM_LOC%TYPE                 := NULL;
   L_to_loc           TSFHEAD.TO_LOC%TYPE                   := NULL;
   L_from_loc_type    TSFHEAD.FROM_LOC_TYPE%TYPE            := NULL;
   L_to_loc_type      TSFHEAD.TO_LOC_TYPE%TYPE              := NULL;
   L_context_type     TSFHEAD.CONTEXT_TYPE%TYPE             := NULL;
   L_context_value    TSFHEAD.CONTEXT_VALUE%TYPE            := NULL;
   L_inv_status       TSFDETAIL.INV_STATUS%TYPE             := NULL;
   L_vdate            DATE                                  := get_vdate();
   L_im_rec           ITEM_MASTER%ROWTYPE;
   L_check_flag       VARCHAR2(1)                           := 'N';

BEGIN

   L_from_loc := I_sos_rec.dc_dest_id;    -- From loc
   L_tsf_no := I_sos_rec.distro_nbr;      -- Transfer No
   L_context_type := I_sos_rec.context_type;
   L_context_value := I_sos_rec.context_value;

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_from_loc_type,
                                   L_from_loc) = FALSE then
      return FALSE;
   end if;

   if I_sos_rec.inventory_type = 'A' or
      I_sos_rec.inventory_type is NULL then
      L_inv_status := NULL;
   else
      L_inv_status := 1;
   end if;

   if I_sos_rec.SOstatusdtl_TBL is NOT NULL and
      I_sos_rec.SOstatusdtl_TBL.COUNT > 0 then

      L_xtsfdtl_tbl := "RIB_XTsfDtl_TBL"();

      FOR i IN I_sos_rec.SOstatusdtl_TBL.FIRST..I_sos_rec.SOstatusdtl_TBL.LAST LOOP
         L_check_flag := 'N';
         L_item := NULL;
         L_qty := 0;

         L_item     := I_sos_rec.SOstatusdtl_TBL(i).item_id; -- Item
if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message, 
                                    L_im_rec, 
                                    I_sos_rec.SOstatusdtl_TBL(i).item_id) = FALSE then 
        return FALSE; 
     end if; 
   
    if L_im_rec.item_level > L_im_rec.tran_level then 
       L_item := L_im_rec.item_parent; 
    end if; 


         L_to_loc   := I_sos_rec.SOstatusdtl_TBL(i).dest_id; -- To loc
         L_qty      := I_sos_rec.SOstatusdtl_TBL(i).unit_qty;-- Qty

         if L_item is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                  'item',
                                                  'NULL',
                                                  'NOT NULL');
            return FALSE;
         end if;

         if L_qty is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                  'quantity',
                                                  'NULL',
                                                  'NOT NULL');
            return FALSE;
         end if;
         -- Get the to location type
         if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                         L_to_loc_type,
                                         L_to_loc) = FALSE then
            return FALSE;
         end if;

         if L_xtsfdtl_tbl is NOT NULL and
            L_xtsfdtl_tbl.COUNT > 0 then

            FOR i IN L_xtsfdtl_tbl.FIRST..L_xtsfdtl_tbl.LAST LOOP
               if L_xtsfdtl_tbl(i).item = L_item then
                  L_xtsfdtl_tbl(i).tsf_qty := L_xtsfdtl_tbl(i).tsf_qty + L_qty;
                  L_check_flag := 'Y';
                  exit;
               end if;
            END LOOP;
         end if;
         ---
         if L_check_flag = 'N' then
            L_xtsfdtl_rec := "RIB_XTsfDtl_REC"(0,
                                               L_item,
                                               L_qty,              -- unit_qty
                                               NULL,               -- supp_pack_size,
                                               L_inv_status,       -- INV_STATUS
                                               NULL);              -- UNIT_COST

            L_xtsfdtl_tbl.extend;
            L_xtsfdtl_tbl(L_xtsfdtl_tbl.COUNT) := L_xtsfdtl_rec;
         end if;
      END LOOP;
   end if;

   L_message_status := 'A';

   O_xtsfdesc_rec := "RIB_XTsfDesc_REC"(0,
                                        L_tsf_no,
                                        L_from_loc_type, -- FROM_LOC_TYPE
                                        L_from_loc, -- FROM_LOC
                                        L_to_loc_type, -- TO_LOC_TYPE
                                        L_to_loc, -- TO_LOC
                                        L_vdate, --I_sos_rec.not_before_date, -- DELIVERY_DATE
                                        NULL, -- DEPT
                                        NULL, -- ROUTING_CODE
                                        NULL, -- FREIGHT_CODE
                                        'EG', -- TSF_TYPE
                                        L_xtsfdtl_tbl,
                                        L_message_status,
                                        NULL,
                                        NULL,
                                        L_context_type,
                                        L_context_value);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_XTSFDESC;
-----------------------------------------------------------------------
FUNCTION PROCESS_SI_SD_CR_TSF(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_distro_no        IN       TSFHEAD.TSF_NO%TYPE,
                              I_tsf_type         IN       TSFHEAD.TSF_TYPE%TYPE,
                              I_inventory_type   IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                              I_item             IN       ITEM_LOC_SOH.ITEM%TYPE ,
                              I_qty              IN       NUMBER,
                              I_from_loc         IN       ITEM_LOC_SOH.LOC%TYPE,
                              I_from_loc_type    IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                              I_to_loc           IN       ITEM_LOC_SOH.LOC%TYPE,
                              I_to_loc_type      IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                              I_inv_status       IN       TSFDETAIL.INV_STATUS%TYPE,
                              I_status           IN       VARCHAR2,
                              I_delivery_date    IN       TSFHEAD.DELIVERY_DATE%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'RMSSUB_SOSTATUS.PROCESS_SI_SD_CR_TSF';

   L_tsfdet_exists  VARCHAR2(1) := 'N';
   L_tsf_qty        TSFDETAIL.TSF_QTY%TYPE;
   L_inv_status     TSFDETAIL.INV_STATUS%TYPE;

   L_xtsfdesc_rec     "RIB_XTsfDesc_REC";
   L_xtsfdtl_rec      "RIB_XTsfDtl_REC";
   L_xtsfdtl_tbl      "RIB_XTsfDtl_TBL"  := NULL;

   L_xtsfref_rec      "RIB_XTsfRef_REC";
   L_xtsfdtlref_rec   "RIB_XTsfDtlRef_REC";
   L_xtsfdtlref_tbl   "RIB_XTsfDtlRef_TBL"  := NULL;

   L_supp_pack_size     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NULL;
   L_message_status     TSFHEAD.STATUS%TYPE;
   L_message_type       VARCHAR2(50);
   O_status_code        VARCHAR2(2);

   L_from_loc           ITEM_LOC_SOH.LOC%TYPE      := I_from_loc;
   L_from_loc_type      ITEM_LOC_SOH.LOC_TYPE%TYPE := I_from_loc_type;
   L_to_loc             ITEM_LOC_SOH.LOC%TYPE      := I_to_loc;
   L_to_loc_type        ITEM_LOC_SOH.LOC_TYPE%TYPE := I_to_loc_type;
   L_delivery_date      TSFHEAD.DELIVERY_DATE%TYPE := I_delivery_date;
   L_tsf_type           TSFHEAD.TSF_TYPE%TYPE      := I_tsf_type;

   cursor C_CHECK_TSF_DETAIL is
      select 'Y',
             tsf_qty
        from tsfdetail td
       where td.tsf_no = I_distro_no
         and td.item = I_item
         and NVL(inv_status, -1) = NVL(L_inv_status, -1);

   cursor C_GET_TSF_INFO is
      select from_loc,
             to_loc,
             tsf_type
        from tsfhead
       where tsf_no = I_distro_no;

BEGIN
   L_xtsfdtlref_tbl := "RIB_XTsfDtlRef_TBL"();
   L_xtsfdtl_tbl := "RIB_XTsfDtl_TBL"();
   L_inv_status := I_inv_status;

   open C_CHECK_TSF_DETAIL;
   fetch C_CHECK_TSF_DETAIL into L_tsfdet_exists, L_tsf_qty;
   close C_CHECK_TSF_DETAIL;

   if I_distro_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_distro_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_qty',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_from_loc_type is NULL then
      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_from_loc_type,
                                      L_from_loc) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_to_loc_type is NULL then
      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_to_loc_type,
                                      L_to_loc) = FALSE then
         return FALSE;
      end if;
   end if;
   -- Get pack size for primary supplier.
   if SUPP_ITEM_ATTRIB_SQL.GET_SUPP_PACK_SIZE(O_error_message,
                                              L_supp_pack_size,
                                              I_item,
                                              NULL,
                                              NULL) = FALSE then
      return FALSE;
   end if;

   if I_status IN ('SI', 'CR') and
      L_tsfdet_exists = 'N' then
      -- This is a transfer detail creation message.
      if I_inventory_type = 'U' then
         L_inv_status := 1;
      elsif I_inventory_type = 'A' then
         L_inv_status := NULL;
      end if;

      -- Get the virtual locations from tsfhead since C_TSF in PROCESS_SOS function
      -- does not fetch records for items not already in the transfer.
      if I_tsf_type is NULL then
         open C_GET_TSF_INFO;
         fetch C_GET_TSF_INFO into L_from_loc,
                                   L_to_loc,
                                   L_tsf_type;
         close C_GET_TSF_INFO;
      end if;

      L_xtsfdtl_rec := "RIB_XTsfDtl_REC"(0,
                                         I_item,
                                         I_qty,
                                         L_supp_pack_size, -- I_sos_rec.SOstatusdtl_TBL(i).pack_size,
                                         L_inv_status,     -- INV_STATUS
                                         NULL);            -- UNIT_COST

      L_xtsfdtl_tbl.extend;
      L_xtsfdtl_tbl(L_xtsfdtl_tbl.COUNT) := L_xtsfdtl_rec;

      L_message_status := 'A';

      L_xtsfdesc_rec := "RIB_XTsfDesc_REC"(0,
                                           I_distro_no,
                                           L_from_loc_type, -- FROM_LOC_TYPE
                                           L_from_loc,      -- FROM_LOC
                                           L_to_loc_type,   -- TO_LOC_TYPE
                                           L_to_loc,        -- TO_LOC
                                           NULL,            -- DELIVERY_DATE
                                           NULL,            -- DEPT
                                           NULL,            -- ROUTING_CODE
                                           NULL,            -- FREIGHT_CODE
                                           L_tsf_type,      -- TSF_TYPE
                                           L_xtsfdtl_tbl,
                                           L_message_status,
                                           NULL,            -- user_id
                                           NULL);

      L_message_type := RMSSUB_XTSF.LP_dtl_cre_type;

      RMSSUB_XTSF.CONSUME(O_status_code,
                          O_error_message,
                          L_xtsfdesc_rec,
                          L_message_type);

      if O_status_code != 'S' then
         return FALSE;
      end if;

      return TRUE;
   elsif I_status IN ('SI', 'CR') and
         L_tsfdet_exists = 'Y' then
      if (I_status = 'CR') then
         if (L_tsf_qty < I_qty) then
            L_tsf_qty := L_tsf_qty + (I_qty - L_tsf_qty);
         elsif (L_tsf_qty  >= I_qty) then
            return TRUE;
         end if;

         L_xtsfdtl_rec := "RIB_XTsfDtl_REC"(0,
                                            I_item,
                                            L_tsf_qty,         -- Status 'CR'
                                            L_supp_pack_size,  -- I_sos_rec.SOstatusdtl_TBL(i).pack_size
                                            I_inv_status,      -- INV_STATUS
                                            NULL);             -- UNIT_COST
      else
         -- This is a transfer detail update (increase) message.
         L_xtsfdtl_rec := "RIB_XTsfDtl_REC"(0,
                                            I_item,
                                            L_tsf_qty + I_qty, -- Status 'SI' and delta qty is added
                                            L_supp_pack_size,  -- I_sos_rec.SOstatusdtl_TBL(i).pack_size
                                            L_inv_status,      -- INV_STATUS
                                            NULL);             -- UNIT_COST
      end if;
      ---
      L_xtsfdtl_tbl.extend;
      L_xtsfdtl_tbl(L_xtsfdtl_tbl.COUNT) := L_xtsfdtl_rec;

      L_message_status := 'A';

      L_xtsfdesc_rec := "RIB_XTsfDesc_REC"(0,
                                           I_distro_no,
                                           L_from_loc_type, -- FROM_LOC_TYPE
                                           L_from_loc,      -- FROM_LOC
                                           L_to_loc_type,   -- TO_LOC_TYPE
                                           L_to_loc,        -- TO_LOC
                                           NULL,            -- DELIVERY_DATE
                                           NULL,            -- DEPT
                                           NULL,            -- ROUTING_CODE
                                           NULL,            -- FREIGHT_CODE
                                           I_tsf_type,      -- TSF_TYPE
                                           L_xtsfdtl_tbl,
                                           L_message_status,
                                           NULL,            -- user_id
                                           NULL);


      L_message_type := RMSSUB_XTSF.LP_dtl_mod_type;

      RMSSUB_XTSF.CONSUME(O_status_code,
                          O_error_message,
                          L_xtsfdesc_rec,
                          L_message_type);

      if O_status_code != 'S' then
         return FALSE;
      end if;

      return TRUE;
   elsif I_status = 'SD' and
         L_tsfdet_exists = 'Y' and
         I_qty = L_tsf_qty then
      -- This is a transfer detail delete message.

      L_xtsfdtlref_rec := "RIB_XTsfDtlRef_REC"(0,
                                             I_item);

      L_xtsfdtlref_tbl.extend;
      L_xtsfdtlref_tbl(L_xtsfdtlref_tbl.COUNT) := L_xtsfdtlref_rec;

      L_message_status := 'A';

      L_xtsfref_rec := "RIB_XTsfRef_REC"(0,
                                         I_distro_no,
                                         L_xtsfdtlref_tbl);

      L_message_type := RMSSUB_XTSF.LP_dtl_del_type;

      if L_xtsfref_rec.xtsfdtlref_tbl is NOT NULL and
         L_xtsfref_rec.xtsfdtlref_tbl.COUNT > 0 then
         RMSSUB_XTSF.CONSUME(O_status_code,
                             O_error_message,
                             L_xtsfref_rec,
                             L_message_type);

         if O_status_code != 'S' then
            return FALSE;
         end if;
      end if;

      return TRUE;
   elsif I_status = 'SD' and
         L_tsfdet_exists = 'Y' and
         I_qty != L_tsf_qty then
      -- This is a transfer detail update (decrease) message.
      L_xtsfdtl_rec := "RIB_XTsfDtl_REC"(0,
                                         I_item,
                                         L_tsf_qty - I_qty,      -- I_qty Status 'SD'
                                         L_supp_pack_size,       -- I_sos_rec.SOstatusdtl_TBL(i).pack_size,
                                         L_inv_status,           -- INV_STATUS
                                         NULL);                  -- UNIT_COST

      L_xtsfdtl_tbl.extend;
      L_xtsfdtl_tbl(L_xtsfdtl_tbl.COUNT) := L_xtsfdtl_rec;

      L_message_status := 'A';
      if I_tsf_type = 'EG' then
         L_delivery_date := NULL;
      end if;

      L_xtsfdesc_rec := "RIB_XTsfDesc_REC"(0,
                                           I_distro_no,
                                           L_from_loc_type, -- FROM_LOC_TYPE
                                           L_from_loc,      -- FROM_LOC
                                           L_to_loc_type,   -- TO_LOC_TYPE
                                           L_to_loc,        -- TO_LOC
                                           L_delivery_date, -- DELIVERY_DATE
                                           NULL, -- DEPT
                                           NULL, -- ROUTING_CODE
                                           NULL, -- FREIGHT_CODE
                                           I_tsf_type,      -- TSF_TYPE
                                           L_xtsfdtl_tbl,
                                           L_message_status,
                                           NULL, -- user_id
                                           NULL);

      L_message_type := RMSSUB_XTSF.LP_dtl_mod_type;


      RMSSUB_XTSF.CONSUME(O_status_code,
                          O_error_message,
                          L_xtsfdesc_rec,
                          L_message_type);

      if O_status_code != 'S' then
        return FALSE;
      end if;

      return TRUE;
   elsif I_status = 'SD' and
         L_tsfdet_exists = 'N' then
      -- Transfer detail does not exist, cannot delete.
      O_error_message := SQL_LIB.CREATE_MSG('NO_TSF_DET_TO_DEL', --No transfer details exist, therefore the transfer cannot be deleted.
                                            I_distro_no,
                                            I_item,
                                            NULL);
      return FALSE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_SI_SD_CR_TSF;
--------------------------------------------------------------------------------
FUNCTION DIST_FROM_LOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_inv_flow_array   IN OUT   BOL_SQL.INV_FLOW_ARRAY,
                       I_tsf_no           IN       TSFHEAD.TSF_NO%TYPE,
                       I_tsf_seq_no       IN       TSFDETAIL.TSF_SEQ_NO%TYPE,
                       I_item             IN       ITEM_MASTER.ITEM%TYPE,
                       I_inv_status       IN       SHIPSKU.INV_STATUS%TYPE,
                       I_tsf_qty          IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_phy_to_loc       IN       ITEM_LOC.LOC%TYPE,
                       I_to_loc_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                       I_phy_from_loc     IN       ITEM_LOC.LOC%TYPE,
                       I_from_loc_type    IN       ITEM_LOC.LOC_TYPE%TYPE,
                       I_dist_type        IN       VARCHAR2)
RETURN BOOLEAN IS
   L_pgm_name         VARCHAR2(61)                       := 'RMSSUB_SOSTATUS.DIST_FROM_LOC';
   L_table            VARCHAR2(30)                       := NULL;
   L_key1             VARCHAR2(60)                       := NULL;
   L_key2             VARCHAR2(60)                       := NULL;
   L_inv_status       SHIPSKU.INV_STATUS%TYPE            := NULL;
   L_dist_array       DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   dist_cnt           BINARY_INTEGER                     := 1;
   flow_cnt           BINARY_INTEGER                     := 1;
   L_from_loc         ITEM_LOC.LOC%TYPE;
   L_from_loc_type    ITEM_LOC.LOC_TYPE%TYPE;
   L_to_loc           ITEM_LOC.LOC%TYPE                  := I_phy_to_loc;
   L_to_loc_type      ITEM_LOC.LOC_TYPE%TYPE             := I_to_loc_type;
   L_pack_ind         ITEM_MASTER.PACK_IND%TYPE          := NULL;
   L_orderable_ind    ITEM_MASTER.ORDERABLE_IND%TYPE     := NULL;
   L_sellable_ind     ITEM_MASTER.SELLABLE_IND%TYPE      := NULL;
   L_pack_type        ITEM_MASTER.PACK_TYPE%TYPE         := NULL;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TSFITEM_INV_FLOW is
      select 'Y'
        from tsfitem_inv_flow tif
       where tif.tsf_no     = I_tsf_no
         and tif.tsf_seq_no = I_tsf_seq_no
         and tif.item       = I_item
         for update nowait;

   cursor C_LOCK_ILS is
     select 'x'
       from item_loc_soh
      where (item = I_item
        and  loc = L_from_loc
        and  loc_type = L_from_loc_type)
         or (item = I_item
        and  loc = L_to_loc
        and  loc_type = L_to_loc_type)
        for update nowait;

   cursor C_LOCK_ILS_COMPS is
      select 'x'
        from item_loc_soh ils,
             v_packsku_qty vpq,
             item_master im
       where ((vpq.pack_no = I_item
         and   vpq.item = ils.item
         and   ils.loc = L_from_loc
         and   ils.loc_type = L_from_loc_type)
          or  (vpq.pack_no = I_item
         and   vpq.item = ils.item
         and   ils.loc = L_to_loc
         and   ils.loc_type = L_to_loc_type))
         and vpq.item = im.item
         and im.inventory_ind = 'Y'
        for update of ils.tsf_reserved_qty, ils.tsf_expected_qty nowait;

   cursor C_PACK_COMP_QTYS is
      select vpq.item comp,
             vpq.qty qty
        from v_packsku_qty vpq,
             item_master im
       where vpq.pack_no = I_item
         and vpq.item = im.item
         and im.inventory_ind = 'Y';

BEGIN

   if I_inv_status is NOT NULL and
      I_inv_status != -1 then
      L_inv_status := I_inv_status;
   end if;

   -- Tsfitem_inv_flow already exists for the tsf/seq_no/item.
   --
   if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                  L_dist_array,
                                  I_item,
                                  I_phy_from_loc,
                                  I_tsf_qty,
                                  'TRANSFER',
                                  L_inv_status,
                                  I_to_loc_type,
                                  I_phy_to_loc,
                                  NULL,              --order_no
                                  NULL,              --shipment
                                  I_tsf_seq_no,      --seq_no
                                  NULL,              --cycle_count
                                  NULL,              --rtv_order_no
                                  NULL,              --rtv_seq_no
                                  I_tsf_no) = FALSE then --tsf_no
      return FALSE;
   end if;
   ---
   L_table := 'ITEM_LOC_SOH';
   L_key1  := 'ITEM:'||I_item;
   L_key2  := 'FROM_LOC:'||to_char(L_from_loc)||'/TO_LOC:'||to_char(L_to_loc);
   open C_LOCK_ILS;
   close C_LOCK_ILS;
   ---
   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack_ind,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_pack_type,
                                    I_item) = FALSE then
      return FALSE;
   end if;

   /*
    * Assign dist table qtys to flow array:
    *   -Walk through the array of distributed locations (L_dist_array)
    *    returned by the call to DISTRIBUTION_SQL.
    *   -Populate inv flow array
    */

   flow_cnt := 0;
   L_table := 'TSFITEM_INV_FLOW';
   L_key1  := 'TSF_NO:'||to_char(I_tsf_no)||'/SEQ_NO:'||to_char(I_tsf_seq_no);
   L_key2  := 'ITEM:'||I_item;
   open C_LOCK_TSFITEM_INV_FLOW;
   close C_LOCK_TSFITEM_INV_FLOW;

   FOR dist_cnt IN L_dist_array.FIRST..L_dist_array.LAST LOOP

      flow_cnt := flow_cnt + 1;
      I_inv_flow_array(flow_cnt).phy_from_loc := I_phy_from_loc;
      I_inv_flow_array(flow_cnt).from_loc := L_dist_array(dist_cnt).from_loc;
      I_inv_flow_array(flow_cnt).from_loc_type := I_from_loc_type;
      I_inv_flow_array(flow_cnt).qty := L_dist_array(dist_cnt).dist_qty;
      I_inv_flow_array(flow_cnt).phy_to_loc := I_phy_to_loc;
      I_inv_flow_array(flow_cnt).to_loc := L_dist_array(dist_cnt).to_loc;
      I_inv_flow_array(flow_cnt).to_loc_type := I_to_loc_type;
      L_from_loc := L_dist_array(dist_cnt).from_loc;
      L_from_loc_type := I_from_loc_type;
      L_to_loc := L_dist_array(dist_cnt).to_loc;
      L_to_loc_type := I_to_loc_type;

      update tsfitem_inv_flow tif
         set tif.tsf_qty    = NVL(tif.tsf_qty, 0) - I_inv_flow_array(flow_cnt).qty
       where tif.tsf_no         = I_tsf_no
         and tif.tsf_seq_no     = I_tsf_seq_no
         and tif.item           = I_item
         and tif.from_loc       = L_from_loc
         and tif.from_loc_type  = L_from_loc_type
         and tif.to_loc         = I_inv_flow_array(flow_cnt).to_loc
         and tif.to_loc_type    = I_to_loc_type;

      update item_loc_soh
         set tsf_reserved_qty = tsf_reserved_qty - I_inv_flow_array(flow_cnt).qty,
             last_update_datetime = LP_vdate,
             last_update_id = LP_user
       where item     = I_item
         and loc      = L_from_loc
         and loc_type = L_from_loc_type
         and (L_pack_ind = 'N' or
              L_from_loc_type != 'S');

      update item_loc_soh
         set tsf_expected_qty = tsf_expected_qty - I_inv_flow_array(flow_cnt).qty,
             last_update_datetime = LP_vdate,
             last_update_id = LP_user
       where item     = I_item
         and loc      = L_to_loc
         and loc_type = L_to_loc_type
         and (L_pack_ind = 'N' or
              L_to_loc_type != 'S');

      if L_pack_ind = 'Y' then

         L_table := 'ITEM_LOC_SOH';
         L_key1  := 'PACK_NO:'||I_item;
         L_key2  := 'FROM_LOC:'||to_char(L_from_loc)||'/TO_LOC:'||to_char(L_to_loc);
         open C_LOCK_ILS_COMPS;
         close C_LOCK_ILS_COMPS;

         FOR rec IN C_PACK_COMP_QTYS LOOP
            update item_loc_soh
               set pack_comp_resv = DECODE(L_from_loc_type, 'S',
                                           pack_comp_resv,
                                           pack_comp_resv - (I_inv_flow_array(flow_cnt).qty * rec.qty)),
                   tsf_reserved_qty = DECODE(L_from_loc_type,'S',
                                             tsf_reserved_qty - (I_inv_flow_array(flow_cnt).qty * rec.qty),
                                             tsf_reserved_qty),
                   last_update_datetime = LP_vdate,
                   last_update_id =  LP_user
             where item = rec.comp
               and loc = L_from_loc
               and loc_type = L_from_loc_type;

            update item_loc_soh
               set pack_comp_exp = DECODE(I_inv_flow_array(flow_cnt).to_loc_type, 'S',
                                          pack_comp_exp,
                                          pack_comp_exp - (I_inv_flow_array(flow_cnt).qty * rec.qty)),
                   tsf_expected_qty = DECODE(I_inv_flow_array(flow_cnt).to_loc_type,'S',
                                             tsf_expected_qty - (I_inv_flow_array(flow_cnt).qty * rec.qty),
                                             tsf_expected_qty),
                   last_update_datetime = LP_vdate,
                   last_update_id = LP_user
             where item = rec.comp
               and loc = I_inv_flow_array(flow_cnt).to_loc
               and loc_type = I_inv_flow_array(flow_cnt).to_loc_type;
         END LOOP;
     end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DIST_FROM_LOC;
-----------------------------------------------------------------------
FUNCTION RESET_TRANSFER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tsf             IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN is

   L_pgm_name   VARCHAR2(61)   := 'RMSSUB_SOSTATUS.RESET_TRANSFER';

   L_table      VARCHAR2(15);
   TAB_rowids   rowids_TBL;
   
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TSFD is
      select td.rowid
        from tsfdetail td
       where td.tsf_no = I_tsf
         and not exists (select 'x'
                           from TABLE(CAST(LP_item AS ITEM_TBL)) item
                          where value(item) = td.item)
         for update nowait;

BEGIN
   L_table := 'TSFDETAIL';

   open C_LOCK_TSFD;
   fetch C_LOCK_TSFD BULK COLLECT into TAB_rowids;
   close C_LOCK_TSFD;

   FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
      update tsfdetail
         set tsf_qty = 0
       where rowid = TAB_rowids(i);

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               TO_CHAR(I_tsf),
                                                               NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SOSTATUS.RESET_TRANSFER',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RESET_TRANSFER;
----------------------------------------------------------------------------------------------------
END RMSSUB_SOSTATUS;
/
