CREATE OR REPLACE PACKAGE BUYER_WORKSHEET_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------
TYPE buyer_wksht_record IS RECORD(source_type            REPL_RESULTS.SOURCE_TYPE%TYPE,
                                  source_type_desc       CODE_DETAIL.CODE_DESC%TYPE,
                                  item                   REPL_RESULTS.ITEM%TYPE,
                                  item_desc              ITEM_MASTER.ITEM_DESC%TYPE,
                                  dept                   REPL_RESULTS.DEPT%TYPE,
                                  dept_name              DEPS.DEPT_NAME%TYPE,
                                  class                  REPL_RESULTS.CLASS%TYPE,
                                  class_name             VARCHAR2(120),
                                  subclass               REPL_RESULTS.SUBCLASS%TYPE,
                                  subclass_name          VARCHAR2(120),
                                  buyer                  REPL_RESULTS.BUYER%TYPE,
                                  buyer_name             VARCHAR2(120),
                                  diff_1                 ITEM_MASTER.DIFF_1%TYPE,
                                  diff_1_desc            DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
                                  diff_2                 ITEM_MASTER.DIFF_2%TYPE,
                                  diff_2_desc            DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
                                  diff_3                 ITEM_MASTER.DIFF_3%TYPE,
                                  diff_3_desc            DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
                                  diff_4                 ITEM_MASTER.DIFF_4%TYPE,
                                  diff_4_desc            DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
                                  item_type              REPL_RESULTS.ITEM_TYPE%TYPE,
                                  comp_item              REPL_RESULTS.MASTER_ITEM%TYPE,
                                  comp_item_desc         ITEM_MASTER.ITEM_DESC%TYPE,
                                  supplier               REPL_RESULTS.PRIMARY_REPL_SUPPLIER%TYPE,
                                  sup_name               SUPS.SUP_NAME%TYPE,
                                  origin_country_id      REPL_RESULTS.ORIGIN_COUNTRY_ID%TYPE,
                                  pool_supplier          REPL_RESULTS.POOL_SUPPLIER%TYPE,
                                  pool_sup_name          SUPS.SUP_NAME%TYPE,
                                  loc_type               REPL_RESULTS.LOC_TYPE%TYPE,
                                  location               REPL_RESULTS.LOCATION%TYPE,
                                  loc_name               STORE.STORE_NAME%TYPE,
                                  physical_wh            REPL_RESULTS.PHYSICAL_WH%TYPE,
                                  phys_wh_name           WH.WH_NAME%TYPE,
                                  current_dscnt          IB_RESULTS.FUTURE_COST%TYPE,
                                  roi                    IB_RESULTS.ROI%TYPE,
                                  raw_roq                REPL_RESULTS.RAW_ROQ%TYPE,
                                  order_roq              REPL_RESULTS.ORDER_ROQ%TYPE,
                                  standard_uom           ITEM_MASTER.STANDARD_UOM%TYPE,
                                  ti                     REPL_RESULTS.TI%TYPE,
                                  hi                     REPL_RESULTS.HI%TYPE,
                                  terms                  SUPS.TERMS%TYPE,
                                  terms_code             VARCHAR2(50),
                                  total_lead_time        ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                                  supp_unit_cost         REPL_RESULTS.SUPP_UNIT_COST%TYPE,
                                  prim_supp_unit_cost    REPL_RESULTS.SUPP_UNIT_COST%TYPE,
                                  unit_cost              REPL_RESULTS.UNIT_COST%TYPE,
                                  currency_code          SUPS.CURRENCY_CODE%TYPE,
                                  prim_curr_code         SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                                  create_date            REPL_RESULTS.REPL_DATE%TYPE,
                                  days_to_event          IB_RESULTS.DAYS_TO_EVENT%TYPE,
                                  next_event_date        IB_RESULTS.NEXT_EVENT_DATE%TYPE,
                                  target_date            IB_RESULTS.TARGET_DATE%TYPE,
                                  status                 REPL_RESULTS.STATUS%TYPE,
                                  tsf_po_link_no         REPL_RESULTS.TSF_PO_LINK_NO%TYPE,
                                  audsid                 REPL_RESULTS.AUDSID%TYPE,
                                  row_id                 VARCHAR2(25),
                                  order_by               VARCHAR2(25),
                                  return_code            VARCHAR2(10),
                                  error_message          RTK_ERRORS.RTK_TEXT%TYPE);
TYPE buyer_wksht_tab IS TABLE OF buyer_wksht_record INDEX BY BINARY_INTEGER;
TYPE po_list_record IS RECORD(order_no             ORDHEAD.ORDER_NO%TYPE,
                              dept                 ORDHEAD.DEPT%TYPE,
                              written_date         ORDHEAD.WRITTEN_DATE%TYPE,
                              supplier             ORDHEAD.SUPPLIER%TYPE,
                              sup_name             SUPS.SUP_NAME%TYPE,
                              pool_supplier        ORD_INV_MGMT.POOL_SUPPLIER%TYPE,
                              loc_type             ORDLOC.LOC_TYPE%TYPE,
                              location             ORDLOC.LOCATION%TYPE,
                              loc_name             STORE.STORE_NAME%TYPE,
                              first_order_total    NUMBER,
                              second_order_total   NUMBER,
                              return_code          VARCHAR2(10),
                              error_message        RTK_ERRORS.RTK_TEXT%TYPE);
TYPE po_list_tab IS TABLE OF po_list_record INDEX BY BINARY_INTEGER;
------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE
--- Purpose:        This procedure will query REPL_RESULTS, IB_RESULTS and
---                 BUYER_WKSHT_MANUAL and populate the above declared table of records
---                 that will be used as the 'base table' in the Buyer Worksheet form.
------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (buyer_worksheet       IN OUT   BUYER_WKSHT_TAB,
                           I_source_type         IN       REPL_RESULTS.SOURCE_TYPE%TYPE,
                           I_item                IN       REPL_RESULTS.ITEM%TYPE,
                           I_dept                IN       REPL_RESULTS.DEPT%TYPE,
                           I_class               IN       REPL_RESULTS.CLASS%TYPE,
                           I_subclass            IN       REPL_RESULTS.SUBCLASS%TYPE,
                           I_buyer               IN       REPL_RESULTS.BUYER%TYPE,
                           I_before_date         IN       DATE,
                           I_after_date          IN       DATE,
                           I_supp_type           IN       VARCHAR2,
                           I_supplier            IN       REPL_RESULTS.PRIMARY_REPL_SUPPLIER%TYPE,
                           I_supplier_parent     IN       SUPS.SUPPLIER%TYPE,
                           I_origin_country_id   IN       REPL_RESULTS.ORIGIN_COUNTRY_ID%TYPE,
                           I_loc_type            IN       REPL_RESULTS.LOC_TYPE%TYPE,
                           I_location            IN       REPL_RESULTS.LOCATION%TYPE,
                           I_incl_zero           IN       VARCHAR2,
                           I_incl_non_due        IN       VARCHAR2,
                           I_incl_po             IN       VARCHAR2,
                           I_order_by            IN       NUMBER,
                           I_asc_desc            IN       VARCHAR2);
------------------------------------------------------------------------------
--- Procedure Name: LOCK_PROCEDURE
--- Purpose:        This procedure will lock records on REPL_RESULTS, IB_RESULTS
---                 or BUYER_WKSHT_MANUAL.
------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE (buyer_worksheet   IN OUT   buyer_wksht_tab);
------------------------------------------------------------------------------
--- Procedure Name: UPDATE_PROCEDURE
--- Purpose:        This procedure will update REPL_RESULTS, IB_RESULTS and BUYER_WKSHT_MANUAL
---                 by rowid, depending on the table record's source type.
------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE(buyer_worksheet   IN OUT   buyer_wksht_tab);
------------------------------------------------------------------------------
--- Procedure Name: DELETE_PROCEDURE
--- Purpose:        This procedure will update the status on the REPL_RESULTS and IB_RESULTS
---                 to 'D' and remove records from BUYER_WKSHT_MANUAL.
------------------------------------------------------------------------------
PROCEDURE DELETE_PROCEDURE(buyer_worksheet   IN OUT   buyer_wksht_tab);
------------------------------------------------------------------------------
--- Procedure Name: PO_QUERY_PROCEDURE
--- Purpose:        This procedure will query ORDHEAD, ORDSKU and ORDLOC based on the
---                 records selected from REPL_RESULTS, IB_RESULTS and BUYER_WKSHT_MANUAL
---                 and populate the above declared table of records (PO_LIST_TAB) that
---                 will be used as the 'base table' in the Buyer Worksheet form - PO List
---                 Window.
------------------------------------------------------------------------------
PROCEDURE PO_QUERY_PROCEDURE (po_list               IN OUT   PO_LIST_TAB,
                              I_audsid              IN       REPL_RESULTS.AUDSID%TYPE,
                              I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                              I_scale_cnstr_type1   IN       SUP_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                              I_scale_cnstr_uom1    IN       SUP_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                              I_scale_cnstr_curr1   IN       SUP_INV_MGMT.SCALE_CNSTR_CURR1%TYPE,
                              I_scale_cnstr_type2   IN       SUP_INV_MGMT.SCALE_CNSTR_TYPE2%TYPE,
                              I_scale_cnstr_uom2    IN       SUP_INV_MGMT.SCALE_CNSTR_UOM2%TYPE,
                              I_scale_cnstr_curr2   IN       SUP_INV_MGMT.SCALE_CNSTR_CURR2%TYPE);
------------------------------------------------------------------------------
--- Procedure Name: PO_QUERY_PROCEDURE
--- Purpose:        Overloaded function of PO_QUERY_PROCEDURE that returns a PLSQL collection
--                  PO_LIST_TAB. Instead of returning a PO_LIST_TAB, it writes the data to a gtt
--                  table BUYER_WKSHT_PO_LIST_GTT, which will be used as the 'base table' in the 
--                  ADF version of the Buyer Worksheet screen - Add to Order popup. The ADF
--                  Add to Order popup screen doesn't have order_no as part of the search 
--                  criteria, so order_no is not part of the function input.
------------------------------------------------------------------------------
FUNCTION PO_QUERY_PROCEDURE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_audsid              IN       REPL_RESULTS.AUDSID%TYPE,
                            I_scale_cnstr_type1   IN       SUP_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                            I_scale_cnstr_uom1    IN       SUP_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                            I_scale_cnstr_curr1   IN       SUP_INV_MGMT.SCALE_CNSTR_CURR1%TYPE,
                            I_scale_cnstr_type2   IN       SUP_INV_MGMT.SCALE_CNSTR_TYPE2%TYPE,
                            I_scale_cnstr_uom2    IN       SUP_INV_MGMT.SCALE_CNSTR_UOM2%TYPE,
                            I_scale_cnstr_curr2   IN       SUP_INV_MGMT.SCALE_CNSTR_CURR2%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: ADD_TO_PO
--- Purpose:       This function will add the selected worksheet line items to a
---                purchase order.
------------------------------------------------------------------------------
FUNCTION ADD_TO_PO(O_error_message   IN OUT   VARCHAR2,
                   I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                   I_audsid          IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: DELETE_REPL_RESULTS
--- Purpose:       This function will set the status to 'D' on REPL_RESULTS for the specified
---                rowid.
------------------------------------------------------------------------------
FUNCTION DELETE_REPL_RESULTS(O_error_message   IN OUT   VARCHAR2,
                             I_rowid           IN       ROWID)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- Function Name: DELETE_TSF_PO_LINK
--- Purpose:       This function will remove the link from the associated transfer
---                if the link does not exist on other worksheet line items in
---                Worksheet or PO-processed status. It will then update the linked
---                transfers' freight code if all of the transfers' line items
---                have been processed.
------------------------------------------------------------------------------
FUNCTION DELETE_TSF_PO_LINK(O_error_message    IN OUT   VARCHAR2,
                            I_tsf_po_link_no   IN       REPL_RESULTS.TSF_PO_LINK_NO%TYPE,
                            I_rowid            IN       ROWID)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- Function Name: INSERT_MANUAL
--- Purpose:       This function will insert a record into BUYER_WKSHT_MANUAL.
------------------------------------------------------------------------------
FUNCTION INSERT_MANUAL(O_error_message       IN OUT   VARCHAR2,
                       I_item                IN       BUYER_WKSHT_MANUAL.ITEM%TYPE,
                       I_supplier            IN       BUYER_WKSHT_MANUAL.SUPPLIER%TYPE,
                       I_origin_country_id   IN       BUYER_WKSHT_MANUAL.ORIGIN_COUNTRY_ID%TYPE,
                       I_loc_type            IN       BUYER_WKSHT_MANUAL.LOC_TYPE%TYPE,
                       I_location            IN       BUYER_WKSHT_MANUAL.LOCATION%TYPE,
                       I_aoq                 IN       BUYER_WKSHT_MANUAL.ORDER_ROQ%TYPE,
                       I_unit_cost           IN       BUYER_WKSHT_MANUAL.UNIT_COST%TYPE,
                       I_tsf_po_link_no      IN       BUYER_WKSHT_MANUAL.TSF_PO_LINK_NO%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: REMOVE_ID
--- Purpose:       This function will NULL out the audsid fields on REPL_RESULTS,
---                IB_RESULTS and BUYER_WKSHT_MANUAL for the specified audsid
---                and where the status is 'W'orksheet.
------------------------------------------------------------------------------
FUNCTION REMOVE_ID(O_error_message   IN OUT   VARCHAR2,
                   I_audsid          IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: SELECT_ALL
--- Purpose:       This function will update the audsid on REPL_RESULTS, IB_RESULTS and
---                BUYER_WKSHT_MANUAL to the specified audsid.
------------------------------------------------------------------------------
FUNCTION SELECT_ALL(O_error_message       IN OUT   VARCHAR,
                    I_audsid              IN       REPL_RESULTS.AUDSID%TYPE,
                    I_source_type         IN       REPL_RESULTS.SOURCE_TYPE%TYPE,
                    I_item                IN       REPL_RESULTS.ITEM%TYPE,
                    I_dept                IN       REPL_RESULTS.DEPT%TYPE,
                    I_class               IN       REPL_RESULTS.CLASS%TYPE,
                    I_subclass            IN       REPL_RESULTS.SUBCLASS%TYPE,
                    I_buyer               IN       REPL_RESULTS.BUYER%TYPE,
                    I_before_date         IN       DATE,
                    I_after_date          IN       DATE,
                    I_supp_type           IN       VARCHAR2,
                    I_supplier            IN       REPL_RESULTS.PRIMARY_REPL_SUPPLIER%TYPE,
                    I_origin_country_id   IN       REPL_RESULTS.ORIGIN_COUNTRY_ID%TYPE,
                    I_loc_type            IN       REPL_RESULTS.LOC_TYPE%TYPE,
                    I_location            IN       REPL_RESULTS.LOCATION%TYPE,
                    I_incl_zero           IN       VARCHAR2,
                    I_incl_non_due        IN       VARCHAR2,
                    I_incl_po             IN       VARCHAR2)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: UPDATE_AOQ_UNIT_COST
--- Purpose:       This function will update the AOQ and unit cost for the item,
---                supplier, country and location specified.
------------------------------------------------------------------------------
FUNCTION UPDATE_AOQ_UNIT_COST(O_error_message   IN OUT   VARCHAR2,
                              I_source_type     IN       BUYER_WKSHT_MANUAL.SOURCE_TYPE%TYPE,
                              I_rowid           IN       ROWID,
                              I_order_roq       IN       BUYER_WKSHT_MANUAL.ORDER_ROQ%TYPE,
                              I_unit_cost       IN       BUYER_WKSHT_MANUAL.UNIT_COST%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: UPDATE_LINK_TSF
--- Purpose:       This function will set the freight code to Normal for the entered transfer
---                if all of the transfer line items have been marked as processed.
------------------------------------------------------------------------------
FUNCTION UPDATE_LINK_TSF(O_error_message   IN OUT   VARCHAR2,
                         I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: UPDATE_PO_TSF
--- Purpose:       This function will set the transfer line item as processed if the links
---                associated with the entered audsid does not exist on other worksheet line
---                items in Worksheet status.  It will then update the linked transfers' freight
---                code if all of the transfers' line items have been processed.
------------------------------------------------------------------------------
FUNCTION UPDATE_PO_TSF(O_error_message   IN OUT   VARCHAR2,
                       I_audsid          IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: UPDATE_REPL_RESULTS
--- Purpose:       This function will update the unit cost and order ROQ on REPL_RESULTS
---                for the specified rowid.
------------------------------------------------------------------------------
FUNCTION UPDATE_REPL_RESULTS(O_error_message   IN OUT   VARCHAR2,
                             I_rowid           IN       ROWID,
                             I_unit_cost       IN       REPL_RESULTS.UNIT_COST%TYPE,
                             I_aoq             IN       REPL_RESULTS.ORDER_ROQ%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------   
--Function Name : SPLIT_SUPPLIER_DIFF
--Purpose       : This function wilL split the item/location ROQ values from REPL_RESULTS
--                acording to the dist_pct values in the table repl_item_loc_supp_dist
--                and will insert the row in table buyer_wksht_manual
------------------------------------------------------------------------------
FUNCTION SPLIT_SUPPLIER_DIFF(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_rowid            IN       ROWID,
                             I_item             IN       ITEM_MASTER.ITEM%TYPE,
                             I_location         IN       REPL_ITEM_LOC.LOCATION%TYPE,
                             I_roq              IN       BUYER_WKSHT_MANUAL.ORDER_ROQ%TYPE,
                             I_loc_type         IN       BUYER_WKSHT_MANUAL.LOC_TYPE%TYPE,
                             I_unit_cost        IN       BUYER_WKSHT_MANUAL.UNIT_COST%TYPE,
                             I_tsf_po_link_no   IN       BUYER_WKSHT_MANUAL.TSF_PO_LINK_NO%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
--Function Name : CHECK_SUPP_DIST
--Purpose       : This function will check for an entry in the table repl_item_loc_supp_dist
--                according to the input values of item and location.If found it will return
--                O_exists parameter as TRUE to the calling form, which will enable the
--                the button "Split to Supp Diff Ratio".
-------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_DIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_item            IN       ITEM_MASTER.ITEM%TYPE,
                         I_location        IN       REPL_ITEM_LOC.LOCATION%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : REPL_RECORD_EXISTS
--Purpose       : This function will check if there exists a record in repl_results 
--                with passed in rowid,roq and unit_cost
-------------------------------------------------------------------------------
FUNCTION REPL_RECORD_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            I_rowid           IN       ROWID,
                            I_roq             IN       BUYER_WKSHT_MANUAL.ORDER_ROQ%TYPE,
                            I_unit_cost       IN       BUYER_WKSHT_MANUAL.UNIT_COST%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : SINGLE_FRANCHISE_LOC_CHECK
--Purpose       : This function will check if there are multiple locations being added
--                to the PO and if any of these locations is a Franchise store.
--------------------------------------------------------------------------------
FUNCTION SINGLE_FRANCHISE_LOC_CHECK(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_single_loc             IN OUT   VARCHAR2,
                                    O_franchise_loc_exists   IN OUT   VARCHAR2,
                                    O_single_costing_loc     IN OUT   VARCHAR2,
                                    I_audsid                 IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
--Function Name : CHECK_INVALID_ITEMLOC_STATUS
--Purpose       : This function will check if any selected item-locs are in 
--                inactive (I), discountinued (C), or deleted (D) status.
--                If yes, return error message that an order cannot be created.
--------------------------------------------------------------------------------
FUNCTION CHECK_INVALID_ITEMLOC_STATUS(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_audsid        IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------                                                          
END BUYER_WORKSHEET_SQL;
/
