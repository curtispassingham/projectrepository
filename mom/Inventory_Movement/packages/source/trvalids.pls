
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TSF_VALIDATE_SQL AUTHID CURRENT_USER AS


  TYPE pack_no IS RECORD (pack_no ITEM_MASTER.ITEM%TYPE,
                          qty     TSFDETAIL.TSF_QTY%TYPE);
  TYPE rejected_pack_table  IS TABLE OF pack_no INDEX BY BINARY_INTEGER;
----------------------------------------------------------------------------
-- Function Name: EXIST
-- Purpose      : checks to see if a transfer number exists on
--                the tsfhead table.
-- Calls        : <none>
-- Input Values : I_transfer    -- transfer number
-- Return Values: O_exist       -- TRUE if exists, FALSE otherwise
-- Created      : 20-Aug-96 by Matt Sniffen
----------------------------------------------------------------------------
FUNCTION EXIST (O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_transfer      IN      NUMBER,
                O_exist         IN OUT  BOOLEAN)
RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function Name: VALID_TO_SHIP
-- Purpose:       This function will determine whether an existing 
--                transfer is valid to ship from a given store. The
--                existing transfer must be in approved status and be
--                from the given location.
-- Calls:         TSF_ATTRIB_SQL.GET_TSFHEAD_INFO
-- Created:       07-MAY-95, by Sonia Wong
-----------------------------------------------------------------------------
FUNCTION VALID_TO_SHIP(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid           IN OUT   BOOLEAN,
                       I_tsf_no          IN       tsfhead.tsf_no%TYPE,
                       I_from_loc        IN       tsfhead.from_loc%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------
-- Function Name: VALID_TO_SHIP_TOGETHER
-- Purpose:       This function will determine whether it is valid to 
--                ship two transfers together (on the same shipment, and
--                potentially in the same carton). 
--                For two transfers to be shipped together, 
--                the first (I_prev_tsf) must be in either 'S'hipped
--                or 'A'pproved status, and the second must be in 'A'pproved
--                status.  The assumption is that the first may already be marked
--                as shipped at the time of validation(as in RSS--Send Transfers),
--                and the question is can the second (I_curr_tsf) be included 
--                in the same shipment.
--                Also, they must be going to the same destination location.
--                I_from_loc is an optional parameter, but if it is passed a value, 
--                this function will validate that the value is 
--                equal to the from_locs of the two entered locations.
-- Calls:         TSF_ATTRIB_SQL.GET_TSFHEAD_INFO
-- Created:       15-MAY-98, by Sonia Wong
-- Updated:       26-MAR-99, Rich Davies
---------------------------------------------------------------------------------
FUNCTION VALID_TO_SHIP_TOGETHER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid           IN OUT   BOOLEAN,
                                I_prev_tsf        IN       tsfhead.tsf_no%TYPE,
                                I_curr_tsf        IN       tsfhead.tsf_no%TYPE,
                                I_from_loc        IN       tsfhead.from_loc%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: TSF_PO_LINK_EXISTS
-- Purpose:       This function verifies that the passed in transfer po link number
--                exists on the tsfdetail table. 
---------------------------------------------------------------------------------
FUNCTION TSF_PO_LINK_EXISTS(O_error_message     IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist             IN OUT     BOOLEAN,
                            I_tsf_po_link_no    IN         TSFDETAIL.TSF_PO_LINK_NO%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: NO_QTY_RECEIVED
-- Purpose:       This function will check to see if any items have been received  
--                for the given transfer.  If any item has 0 items received  
--                (received_qty + reconsiled_qty = 0) at the receiving location
--                or finisher, if applicable, O_no_qty_received will be TRUE.
---------------------------------------------------------------------------------
FUNCTION NO_QTY_RECEIVED(O_error_message     IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                         O_no_qty_received   IN OUT     BOOLEAN,
                         I_tsf_no            IN         TSFDETAIL.TSF_NO%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: PACK_REPACK_INSTR_EXISTS 
-- Purpose:       This function will be called during the tsf approval process 
--                for transfers that include a finisher and will ensure that any packs
--                being unpacked and transformed include repacking instructions.
---------------------------------------------------------------------------------
FUNCTION PACK_REPACK_INSTR_EXISTS(O_error_message        IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_repack_instr_exist   IN OUT     BOOLEAN,
                                  O_rejected_packs       IN OUT     REJECTED_PACK_TABLE, 
                                  I_tsf_no               IN         TSFDETAIL.TSF_NO%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: PACK_REPACK_INSTR_EXISTS 
-- Purpose:       This overloaded function is used by the ADF transfer screen. 
--                It returns after finding the first pack on tsfdetail that is
--                unpacked and transformed but without repacking instructions.
---------------------------------------------------------------------------------
FUNCTION PACK_REPACK_INSTR_EXISTS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_repack_instr_exist   IN OUT   BOOLEAN,
                                  I_tsf_no               IN       TSFDETAIL.TSF_NO%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: EXPLODE_TSF_PACKS 
-- Purpose:       This function will be called from tsf.fmb during the approval process
--                if any pack items do not contain repacking instructions, the user 
--                can choose to ship those items as bulk items.  This function
--                explodes those packs down to the component level. 
---------------------------------------------------------------------------------
FUNCTION EXPLODE_TSF_PACKS(O_error_message        IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                           I_rejected_packs_table IN         REJECTED_PACK_TABLE, 
                           I_tsf_no               IN         TSFDETAIL.TSF_NO%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: EXPLODE_TSF_PACKS 
-- Purpose:       This overloaded function is used by the ADF transfer screen.
--                It finds all packs on tsfdetail without repacking instruction
--                and explode those packs down to the component level for repacking. 
---------------------------------------------------------------------------------
FUNCTION EXPLODE_TSF_PACKS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: TSF_PRICE_EXCEED_WAC
-- Purpose      : This function compares tsf items' transfer price and WAC. O_wac_exceeded
--                is TRUE when an item's tsfdetail.tsf_price exceeds its av_cost 
--                (item_loc_soh.av_cost) at the from location.
-------------------------------------------------------------------------------------
FUNCTION TSF_PRICE_EXCEED_WAC(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_wac_exceeded     IN OUT BOOLEAN,
                              I_tsf_no           IN     TSFDETAIL.TSF_NO%TYPE,
                              I_from_loc         IN     TSFHEAD.FROM_LOC%TYPE,
                              I_from_loc_type    IN     TSFHEAD.FROM_LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: OVERWRITE_TSF_PRICE
-- Purpose      : This function will overwrite the tsf_price for any item where the 
--                tsf_price exceeds the item_loc_soh.av_cost for the from location.
-------------------------------------------------------------------------------------
FUNCTION OVERWRITE_TSF_PRICE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_no           IN     TSFDETAIL.TSF_NO%TYPE,
                             I_child_tsf_no     IN     TSFDETAIL.TSF_NO%TYPE,
                             I_from_loc         IN     TSFHEAD.FROM_LOC%TYPE,
                             I_from_loc_type    IN     TSFHEAD.FROM_LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: VALID_FROM_ITEM
-- Purpose      : Validates the from item at the from location and the to location. If the
--                Item is a pack then the component items are also validated.
-------------------------------------------------------------------------------------
FUNCTION VALID_FROM_ITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_desc          IN OUT   ITEM_MASTER.ITEM_DESC%TYPE,
                         I_item               IN       ITEM_MASTER.ITEM%TYPE,
                         I_item_type          IN       VARCHAR2,
                         I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                         I_from_loc_type      IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                         I_finisher           IN       TSFHEAD.FROM_LOC%TYPE,
                         I_finisher_type      IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                         I_to_loc             IN       TSFHEAD.TO_LOC%TYPE,
                         I_to_loc_type        IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                         I_dept               IN       DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: VALID_TO_ITEM
-- Purpose      : Validates the to item at the to location. If the Item is a pack
--                then the component items are also validated. 
-------------------------------------------------------------------------------------
FUNCTION VALID_TO_ITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item               IN       ITEM_MASTER.ITEM%TYPE,
                       I_loc                IN       TSFHEAD.FROM_LOC%TYPE,
                       I_loc_type           IN       TSFHEAD.FROM_LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: CHECK_IL_RANGING
-- Purpose      : Checks if item(s) is/are ranged to the location parameter.
--                If they are NOT ranged, then O_item_not_ranged returned as TRUE;
--                else, O_item_not_ranged returned as FALSE. For packs, all component
--                items are checked as well. An item, item list, item parent, or 
--                transfer number can be passed in. It is expected that only one of 
--                these will be passed in. However, if more than one is passed the 
--                function will still work: O_item_not_ranged will be TRUE if any  
--                one of the items is not ranged to the location, FALSE if all are 
--                ranged to the location.
-------------------------------------------------------------------------------------
FUNCTION CHECK_IL_RANGING(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_item_not_ranged IN OUT   BOOLEAN,
                          I_item            IN       TSFDETAIL.ITEM%TYPE,
                          I_itemlist        IN       SKULIST_DETAIL.SKULIST%TYPE,
                          I_item_parent     IN       ITEM_MASTER.ITEM%TYPE,
                          I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE,
                          I_dept            IN       DEPS.DEPT%TYPE,
                          I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                          I_location        IN       TSFHEAD.TO_LOC%TYPE,
                          I_loc_type        IN       TSFHEAD.TO_LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: CHECK_CUST_RECS
-- Purpose      : Checks if the transfer has existing ordcust records.
-------------------------------------------------------------------------------------
FUNCTION CHECK_CUST_RECS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_cust_exist_ind    IN OUT   VARCHAR2,
                          I_tsf_no            IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
END TSF_VALIDATE_SQL;
/


