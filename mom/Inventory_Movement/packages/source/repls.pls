CREATE OR REPLACE PACKAGE REPLENISHMENT_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: GET_FORM_INFO
-- Purpose: Retrieve and all non-base table fiels for the replenishment attribute view form,
--          replenishment results detail and replenihsment results list form.
--          I_form_name indicate the name of the calling form
--          (FM_RPLATTRV, FM_RPLRSLTD, FM_RPLRSLTL)
---------------------------------------------------------------------------------------------
FUNCTION GET_FORM_INFO(O_error_message        IN OUT VARCHAR2,
                       O_item_desc            IN OUT ITEM_MASTER.DESC_UP%TYPE,
                       O_loc_desc             IN OUT STORE.STORE_NAME%TYPE,
                       O_review_cycle_desc    IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                       O_stock_cat_desc       IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                       O_repl_order_ctrl_desc IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                       O_source_wh_desc       IN OUT WH.WH_NAME%TYPE,
                       O_repl_method_desc     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                       O_season_desc          IN OUT SEASONS.SEASON_DESC%TYPE,
                       O_phase_desc           IN OUT PHASES.PHASE_DESC%TYPE,
                       O_supp_name            IN OUT SUPS.SUP_NAME%TYPE,
                       O_dept_name            IN OUT DEPS.DEPT_NAME%TYPE,
                       O_class_name           IN OUT CLASS.CLASS_NAME%TYPE,
                       O_subclass_name        IN OUT SUBCLASS.SUB_NAME%TYPE,
                       O_pack_name            IN OUT ITEM_MASTER.DESC_UP%TYPE,
                       O_sun                  IN OUT VARCHAR2,
                       O_mon                  IN OUT VARCHAR2,
                       O_tue                  IN OUT VARCHAR2,
                       O_wed                  IN OUT VARCHAR2,
                       O_thu                  IN OUT VARCHAR2,
                       O_fri                  IN OUT VARCHAR2,
                       O_sat                  IN OUT VARCHAR2,
                       O_loc_type_desc        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                       O_master_item_desc     IN OUT ITEM_MASTER.DESC_UP%TYPE,
                       O_store_ord_mult_desc  IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                       O_prescaled_cost       IN OUT ORDLOC.UNIT_COST%TYPE,
                       O_actual_cost          IN OUT ORDLOC.UNIT_COST%TYPE,
                       O_prescaled_qty        IN OUT ORDLOC.QTY_PRESCALED%TYPE,
                       O_actual_qty           IN OUT ORDLOC.QTY_ORDERED%TYPE,
                       O_stock_on_hand        IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       O_incoming_stock       IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       O_outgoing_stock       IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       O_nonsellable_stock    IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       O_inner_name_desc      IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                       O_case_name_desc       IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                       O_pallet_name_desc     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                       I_form_name            IN     VARCHAR2,
                       I_item                 IN     REPL_ITEM_LOC.ITEM%TYPE,
                       I_location             IN     REPL_ITEM_LOC.LOCATION%TYPE,
                       I_loc_type             IN     REPL_ITEM_LOC.LOC_TYPE%TYPE,
                       I_review_cycle         IN     REPL_ITEM_LOC.REVIEW_CYCLE%TYPE,
                       I_stock_cat            IN     REPL_ITEM_LOC.STOCK_CAT%TYPE,
                       I_repl_order_ctrl      IN     REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE,
                       I_source_wh            IN     REPL_ITEM_LOC.SOURCE_WH%TYPE,
                       I_repl_method          IN     REPL_ITEM_LOC.REPL_METHOD%TYPE,
                       I_season               IN     REPL_ITEM_LOC.SEASON_ID%TYPE,
                       I_phase                IN     REPL_ITEM_LOC.PHASE_ID%TYPE,
                       I_supplier             IN     REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE,
                       I_dept                 IN     REPL_ITEM_LOC.DEPT%TYPE,
                       I_class                IN     REPL_ITEM_LOC.CLASS%TYPE,
                       I_subclass             IN     REPL_ITEM_LOC.SUBCLASS%TYPE,
                       I_pack                 IN     REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE,
                       I_order_no             IN     REPL_RESULTS.ORDER_NO%TYPE,
                       I_alloc_no             IN     REPL_RESULTS.ALLOC_NO%TYPE,
                       I_origin_country_id    IN     REPL_RESULTS.ORIGIN_COUNTRY_ID%TYPE,
                       I_master_item          IN     REPL_RESULTS.MASTER_ITEM%TYPE,
                       I_store_ord_mult       IN     REPL_RESULTS.STORE_ORD_MULT%TYPE,
                       I_stock_on_hand        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_pack_comp_soh        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_in_transit_qty       IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_pack_comp_intran     IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_tsf_expected_qty     IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_pack_comp_exp        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_alloc_in_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_rtv_qty              IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_tsf_resv_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_pack_comp_resv       IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_alloc_out_qty        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_layaway_qty          IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_non_sellable_qty     IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_on_order             IN     REPL_RESULTS.ON_ORDER%TYPE)

RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function Name:  GET_REPL_ATTRIBUTES
--- Purpose:        Fetches relevant fields from REPL_ITEM_LOC for the inputted item.
---------------------------------------------------------------------------------------------
FUNCTION GET_REPL_ATTRIBUTES(O_error_message             IN OUT   VARCHAR2,
                             O_stock_cat                 IN OUT   REPL_ITEM_LOC.STOCK_CAT%TYPE,
                             O_order_ind                 IN OUT   REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE,
                             O_supplier                  IN OUT   REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE,
                             O_origin_country            IN OUT   REPL_ITEM_LOC.ORIGIN_COUNTRY_ID%TYPE,
                             O_review_cycle              IN OUT   REPL_ITEM_LOC.REVIEW_CYCLE%TYPE,
                             O_activate_date             IN OUT   REPL_ITEM_LOC.ACTIVATE_DATE%TYPE,
                             O_deactivate_date           IN OUT   REPL_ITEM_LOC.DEACTIVATE_DATE%TYPE,
                             O_source_wh                 IN OUT   REPL_ITEM_LOC.SOURCE_WH%TYPE,
                             O_repl_method               IN OUT   REPL_ITEM_LOC.REPL_METHOD%TYPE,
                             O_pres_stock                IN OUT   REPL_ITEM_LOC.PRES_STOCK%TYPE,
                             O_demo_stock                IN OUT   REPL_ITEM_LOC.DEMO_STOCK%TYPE,
                             O_min_stock                 IN OUT   REPL_ITEM_LOC.MIN_STOCK%TYPE,
                             O_max_stock                 IN OUT   REPL_ITEM_LOC.MAX_STOCK%TYPE,
                             O_incr_pct                  IN OUT   REPL_ITEM_LOC.INCR_PCT%TYPE,
                             O_min_supply_days           IN OUT   REPL_ITEM_LOC.MIN_SUPPLY_DAYS%TYPE,
                             O_max_supply_days           IN OUT   REPL_ITEM_LOC.MAX_SUPPLY_DAYS%TYPE,
                             O_time_supply_horizon       IN OUT   REPL_ITEM_LOC.TIME_SUPPLY_HORIZON%TYPE,
                             O_inv_selling_days          IN OUT   REPL_ITEM_LOC.INV_SELLING_DAYS%TYPE,
                             O_service_level             IN OUT   REPL_ITEM_LOC.SERVICE_LEVEL%TYPE,
                             O_lost_sales_factor         IN OUT   REPL_ITEM_LOC.LOST_SALES_FACTOR%TYPE,
                             O_non_scaling_ind           IN OUT   REPL_ITEM_LOC.NON_SCALING_IND%TYPE,
                             O_max_scale_value           IN OUT   REPL_ITEM_LOC.MAX_SCALE_VALUE%TYPE,
                             O_pickup_lead_time          IN OUT   REPL_ITEM_LOC.PICKUP_LEAD_TIME%TYPE,
                             O_wh_lead_time              IN OUT   REPL_ITEM_LOC.WH_LEAD_TIME%TYPE,
                             O_terminal_stock_qty        IN OUT   REPL_ITEM_LOC.TERMINAL_STOCK_QTY%TYPE,
                             O_season_id                 IN OUT   REPL_ITEM_LOC.SEASON_ID%TYPE,
                             O_phase_id                  IN OUT   REPL_ITEM_LOC.PHASE_ID%TYPE,
                             O_use_tolerance_ind         IN OUT   REPL_ITEM_LOC.USE_TOLERANCE_IND%TYPE,
                             O_unit_tolerance            IN OUT   REPL_ITEM_LOC.UNIT_TOLERANCE%TYPE,
                             O_pct_tolerance             IN OUT   REPL_ITEM_LOC.PCT_TOLERANCE%TYPE,
                             O_primary_pack_no           IN OUT   REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE,
                             O_reject_store_ord_ind      IN OUT   REPL_ITEM_LOC.REJECT_STORE_ORD_IND%TYPE,
                             O_service_level_type        IN OUT   REPL_ATTR_UPDATE_HEAD.SERVICE_LEVEL_TYPE%TYPE,
                             O_mra_update                IN OUT   REPL_ATTR_UPDATE_HEAD.MRA_UPDATE%TYPE,
                             O_mra_restore               IN OUT   REPL_ATTR_UPDATE_HEAD.MRA_RESTORE%TYPE,
                             O_Create_id                 IN OUT   REPL_ATTR_UPDATE_HEAD.CREATE_ID%TYPE,
                             O_Create_date               IN OUT   REPL_ATTR_UPDATE_HEAD.CREATE_DATE%TYPE,
                             O_Scheduled_Desc            IN OUT   REPL_ATTR_UPDATE_HEAD.SCH_RPL_DESC%TYPE,
                             O_stock_cat_exists          IN OUT   BOOLEAN,
                             O_item                      IN OUT   REPL_ATTR_UPDATE_ITEM.ITEM%TYPE,
                             O_size_profile_ind          IN OUT   REPL_ATTR_UPDATE_HEAD.SIZE_PROFILE_IND%TYPE,
                             O_dept                      IN OUT   REPL_ATTR_UPDATE_ITEM.DEPT%TYPE,
                             O_class                     IN OUT   REPL_ATTR_UPDATE_ITEM.CLASS%TYPE,
                             O_subclass                  IN OUT   REPL_ATTR_UPDATE_ITEM.SUBCLASS%TYPE,
                             O_diff_1                    IN OUT   REPL_ATTR_UPDATE_ITEM.DIFF_1%TYPE,
                             O_diff_2                    IN OUT   REPL_ATTR_UPDATE_ITEM.DIFF_2%TYPE,
                             O_diff_3                    IN OUT   REPL_ATTR_UPDATE_ITEM.DIFF_3%TYPE,
                             O_diff_4                    IN OUT   REPL_ATTR_UPDATE_ITEM.DIFF_4%TYPE,
                             O_add_lead_time_ind         IN OUT   REPL_ATTR_UPDATE_HEAD.ADD_LEAD_TIME_IND%TYPE,
                             O_multiple_runs_per_day_ind IN OUT   REPL_ITEM_LOC.MULT_RUNS_PER_DAY_IND%TYPE,
                             O_transfers_zero_soh_ind    IN OUT   REPL_ITEM_LOC.TSF_ZERO_SOH_IND%TYPE,
                             I_repl_id                   IN       REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE,
                             I_scheduled_active_date     IN       REPL_ATTR_UPDATE_HEAD.SCHEDULED_ACTIVE_DATE%TYPE,
                             I_update_from_mra           IN       VARCHAR2,
                             I_item                      IN       REPL_ITEM_LOC.ITEM%TYPE,
                             I_loc                       IN       REPL_ITEM_LOC.LOCATION%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: ON_REPLENISHMENT
-- Purpose  : Checks for existence of replenishment information
--                at any location for a given ITEM or style.
---------------------------------------------------------------------------------------------
FUNCTION ON_REPLENISHMENT(O_error_message IN OUT VARCHAR2,
                          I_item          IN     item_master.item%TYPE,
                          O_on_repl       IN OUT BOOLEAN) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- Function Name: FORECAST_REPL_METHOD
-- Purpose  : Checks if locations for an item are using a forecast
--                replenishment method.
---------------------------------------------------------------------------------------------
FUNCTION FORECAST_REPL_METHOD(O_error_message        IN OUT VARCHAR2,
                              I_item                 IN     item_master.item%TYPE,
                              O_forecast_repl_method IN OUT BOOLEAN) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- Function Name:  SEASONAL_REPL_CHECK
-- Purpose      :  Checks if the item/location is on seasonal replenishment.
---------------------------------------------------------------------------------------------
FUNCTION SEASONAL_REPL_CHECK(O_error_message   IN OUT VARCHAR2,
                             O_seasonal_ind    IN OUT VARCHAR2,
                             I_item            IN     repl_item_loc.item%TYPE,
                             I_loc             IN     repl_item_loc.location%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_TOLERANCE
-- Purpose  : Select unit and percent tolerances from  the replenishment item location table.
--            If no record is found, null values will be passed back for both variables.
---------------------------------------------------------------------------------------------
FUNCTION GET_TOLERANCES(O_error_message     IN OUT VARCHAR2,
                        O_pct_tolerance     IN OUT repl_item_loc.pct_tolerance%TYPE,
                        O_unit_tolerance    IN OUT repl_item_loc.unit_tolerance%TYPE,
                        I_item              IN     repl_item_loc.item%TYPE,
                        I_location          IN     repl_item_loc.location%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: NEXT_ORD_TEMP_SEQ_NO
-- Purpose  : Retrieve and return sequence the next value from the ORD_TEMP_SEQUENCE sequence to
--            the calling module.
-------------------------------------------------------------------------------------------------
FUNCTION NEXT_ORD_TEMP_SEQ_NO(O_error_message     IN OUT    VARCHAR2,
                              O_ord_temp_seq_no   IN OUT    ORD_TEMP.ORD_TEMP_SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: WH_EXISTS_AS_SOURCE
-- Purpose: This function determines whether the passed in wh is a source wh for any crossdock
--          stores for the passed in item.
-------------------------------------------------------------------------------------------------
FUNCTION WH_EXISTS_AS_SOURCE(O_error_message    OUT VARCHAR2,
                             O_exists_as_source OUT BOOLEAN,
                             I_item             IN  item_master.item%TYPE,
                             I_wh               IN  wh.wh%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------
FUNCTION REPL_METHOD(O_error_message     IN OUT     VARCHAR,
                     O_repl_method       IN OUT     REPL_RESULTS.REPL_METHOD%TYPE,
                     I_item              IN         REPL_RESULTS.ITEM%TYPE,
                     I_location          IN         REPL_RESULTS.LOCATION%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: IB_LINE_ITEM
-- Purpose: This function will determine if a repl_results line item is associated with an ib_line_item.
--          If it is then recalc qty field on the repl results detail form should be disabled.
-------------------------------------------------------------------------------------------------
FUNCTION IB_LINE_ITEM(O_error_message     IN OUT     VARCHAR,
                      O_exists            IN OUT     VARCHAR,
                      I_order_no          IN         REPL_RESULTS.ORDER_NO%TYPE,
                      I_item              IN         REPL_RESULTS.ITEM%TYPE,
                      I_location          IN         REPL_RESULTS.LOCATION%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------
-- Function: CUSTOM_VAL
-- Purpose : This function will be used for validationg the custom function.
--------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item              IN       REPL_ITEM_LOC.ITEM%TYPE,
                    I_location          IN       REPL_ITEM_LOC.LOCATION%TYPE,
                    I_function_key      IN       VARCHAR2,
                    I_seq_no            IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END;
/

