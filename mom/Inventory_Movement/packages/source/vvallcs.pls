
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE VVE_ALLOC_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------
FUNCTION ALLOC_PO(O_error_message          IN OUT VARCHAR2,
                  I_obligation_key         IN     OBLIGATION.OBLIGATION_KEY%TYPE,
                  I_obligation_level       IN     OBLIGATION.OBLIGATION_LEVEL%TYPE,
                  I_vessel_id              IN     TRANSPORTATION.VESSEL_ID%TYPE,
                  I_voyage_flt_id          IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                  I_estimated_depart_date  IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                  I_order_no               IN     ORDHEAD.ORDER_NO%TYPE,
                  I_comp_id                IN     ELC_COMP.COMP_ID%TYPE,
                  I_alloc_basis_uom        IN     UOM_CLASS.UOM%TYPE,
                  I_qty                    IN     OBLIGATION_COMP.QTY%TYPE,
                  I_amt_prim               IN     OBLIGATION_COMP.AMT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
FUNCTION ALLOC_VVE(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_obligation_key          IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                   I_obligation_level        IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                   I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                   I_voyage_flt_id           IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                   I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                   I_comp_id                 IN       ELC_COMP.COMP_ID%TYPE,
                   I_alloc_basis_uom         IN       UOM_CLASS.UOM%TYPE,
                   I_qty                     IN       OBLIGATION_COMP.QTY%TYPE,
                   I_amt_prim                IN       OBLIGATION_COMP.AMT%TYPE,

                   I_order_no                IN       ORDHEAD.ORDER_NO%TYPE := NULL

                  )
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------

FUNCTION ALLOC_PO_VVE( O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                       I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                       I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                       I_comp_id            IN       ELC_COMP.COMP_ID%TYPE,
                       I_alloc_basis_uom    IN       UOM_CLASS.UOM%TYPE,
                       I_qty                IN       OBLIGATION_COMP.QTY%TYPE,
                       I_amt_prim           IN       OBLIGATION_COMP.AMT%TYPE)
   RETURN BOOLEAN;

----------------------------------------------------------------------------------------

END VVE_ALLOC_SQL;
/
