CREATE OR REPLACE PACKAGE BODY TAX_THREAD_SQL AS
------------------------------------------------------------------------------------------------
FUNCTION LOAD_REPL_ORDER_TAX_BREAKUP(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_thread_id         IN     NUMBER,
                                     I_number_of_threads IN     NUMBER)
   return BOOLEAN is

   L_program    VARCHAR2(50) := 'TAX_THREAD_SQL.LOAD_REPL_ORDER_TAX_BREAKUP';
   L_exists     VARCHAR2(1)  := 'N';
   L_l10n_obj   L10N_OBJ     := L10N_OBJ();

   -- Cursor to lock repl_apprv_gtax_queue for deletion of successfully processed POs
   cursor C_LOCK_REPL_PURCHASE_ORDERS is
      select 'Y'
        from repl_apprv_gtax_queue ra
       where MOD(ra.order_no, I_number_of_threads) + 1 = I_thread_id
         for update nowait;

   -- Cursor to retrieve purchase order numbers for the thread
   cursor C_GET_REPL_PURCHASE_ORDERS is
      select ra.order_no
        from repl_apprv_gtax_queue ra
       where MOD(ra.order_no, I_number_of_threads) + 1 = I_thread_id;

BEGIN

   -- Check required inputs
   if I_thread_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_thread_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_number_of_threads is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_number_of_threads',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- Lock rows on the repl_apprv_gtax_queue table for the current thread
   open C_LOCK_REPL_PURCHASE_ORDERS;
   fetch C_LOCK_REPL_PURCHASE_ORDERS into L_exists;
   close C_LOCK_REPL_PURCHASE_ORDERS;

   -- Continue processing only if there are records to process
   if NVL(L_exists, 'N') = 'N' then
      return TRUE;
   end if;

   -- Loop through the purchase orders and call
   FOR rec in C_GET_REPL_PURCHASE_ORDERS LOOP
       ---
       L_l10n_obj.procedure_key := 'LOAD_ORDER_TAX_OBJECT';
       L_l10n_obj.doc_type := 'PO';
       L_l10n_obj.doc_id := rec.order_no;
       ---
       if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                 L_l10n_obj) = FALSE then
          return FALSE;
       end if;
   END LOOP;

   -- Delete processed replenishment purchase orders from repl_apprv_gtax_queue
   delete from repl_apprv_gtax_queue ra
       where MOD(ra.order_no, I_number_of_threads) + 1 = I_thread_id;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;

END LOAD_REPL_ORDER_TAX_BREAKUP;
------------------------------------------------------------------------------------------------
END;
/
