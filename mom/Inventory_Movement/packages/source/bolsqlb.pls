CREATE OR REPLACE PACKAGE BODY BOL_SQL AS

--globals
   LP_vdate                    PERIOD.VDATE%TYPE := get_vdate;
   LP_user                     VARCHAR2(50)      := get_user;
   LP_dept_level_transfers     SYSTEM_OPTIONS.DEPT_LEVEL_TRANSFERS%TYPE := NULL;
   LP_bol_rec                  BOL_SQL.BOL_REC;
   LP_receipt_ind              VARCHAR2(1)       := 'N';
   LP_system_options           SYSTEM_OPTIONS%ROWTYPE;
   LP_tran_date                PERIOD.VDATE%TYPE := NULL;
--used for internal sequence number generator (counts items across multi distros)
   LP_ss_seq_no                SHIPSKU.SEQ_NO%TYPE := NULL;

--for bulk processing of loc attributes.
   TYPE loc_attrb_rec IS RECORD(locs              LOC_TBL,
                                loc_types         LOC_TYPE_TBL,
                                receive_as_types  INDICATOR_TBL);

--SHIPSKU BULK--
   TYPE ss_shipment_TBL            is table of SHIPSKU.SHIPMENT%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_seq_no_TBL              is table of SHIPSKU.SEQ_NO%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_item_TBL                is table of SHIPSKU.ITEM%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_distro_no_TBL           is table of SHIPSKU.DISTRO_NO%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_distro_type_TBL         is table of SHIPSKU.DISTRO_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_ref_item_TBL            is table of SHIPSKU.REF_ITEM%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_carton_TBL              is table of SHIPSKU.CARTON%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_inv_status_TBL          is table of SHIPSKU.INV_STATUS%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_qty_received_TBL        is table of SHIPSKU.QTY_RECEIVED%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_unit_cost_TBL           is table of SHIPSKU.UNIT_COST%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_unit_retail_TBL         is table of SHIPSKU.UNIT_RETAIL%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_qty_expected_TBL        is table of SHIPSKU.QTY_EXPECTED%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_weight_expected_TBL     is table of SHIPSKU.WEIGHT_EXPECTED%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_weight_expected_uom_TBL is table of SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_adjust_type_TBL         is table of SHIPSKU.ADJUST_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_reconcile_user_id_TBL   is table of SHIPSKU.RECONCILE_USER_ID%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_reconcile_date_TBL      is table of SHIPSKU.RECONCILE_DATE%TYPE INDEX BY BINARY_INTEGER;
   TYPE ss_idx_TBL                 is table of ITEM_MASTER.ITEM%TYPE INDEX BY VARCHAR2(30);
   ------
   P_ss_shipment              ss_shipment_TBL;
   P_ss_seq_no                ss_seq_no_TBL;
   P_ss_item                  ss_item_TBL;
   P_ss_distro_no             ss_distro_no_TBL;
   P_ss_distro_type           ss_distro_type_TBL;
   P_ss_ref_item              ss_ref_item_TBL;
   P_ss_carton                ss_carton_TBL;
   P_ss_inv_status            ss_inv_status_TBL;
   P_ss_qty_received          ss_qty_received_TBL;
   P_ss_unit_cost             ss_unit_cost_TBL;
   P_ss_unit_retail           ss_unit_retail_TBL;
   P_ss_qty_expected          ss_qty_expected_TBL;
   P_ss_weight_expected       ss_weight_expected_TBL;
   P_ss_weight_expected_uom   ss_weight_expected_uom_TBL;
   P_ss_size                  BINARY_INTEGER := 0;
   P_ss_size_auto             BINARY_INTEGER := 0;
   P_ss_adjust_type           ss_adjust_type_TBL;
   P_ss_reconcile_user_id     ss_reconcile_user_id_TBL;

   P_ss_reconcile_date        ss_reconcile_date_TBL;
   P_ss_idx                   ss_idx_TBL;

--ITEM_LOC_HIST BULK--
   TYPE ilh_item_TBL                  is table of ITEM_LOC_HIST.ITEM%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_loc_TBL                   is table of ITEM_LOC_HIST.LOC%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_loc_type_TBL              is table of ITEM_LOC_HIST.LOC_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_eow_date_TBL              is table of ITEM_LOC_HIST.EOW_DATE%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_week_454_TBL              is table of ITEM_LOC_HIST.WEEK_454%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_month_454_TBL             is table of ITEM_LOC_HIST.MONTH_454%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_year_454_TBL              is table of ITEM_LOC_HIST.YEAR_454%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_sales_type_TBL            is table of ITEM_LOC_HIST.SALES_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_sales_issues_TBL          is table of ITEM_LOC_HIST.SALES_ISSUES%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_create_datetime_TBL       is table of ITEM_LOC_HIST.CREATE_DATETIME%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_last_update_datetime_TBL  is table of ITEM_LOC_HIST.LAST_UPDATE_DATETIME%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_last_update_id_TBL        is table of ITEM_LOC_HIST.LAST_UPDATE_ID%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_rowid_TBL                 is table of ROWID INDEX BY BINARY_INTEGER;
   TYPE ilh_dept_TBL                  is table of ITEM_MASTER.DEPT%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_class_TBL                 is table of ITEM_MASTER.CLASS%TYPE INDEX BY BINARY_INTEGER;
   TYPE ilh_subclass_TBL              is table of ITEM_MASTER.SUBCLASS%TYPE INDEX BY BINARY_INTEGER;
   
   ------
   P_ilh_item                   ilh_item_TBL;
   P_ilh_loc                    ilh_loc_TBL;
   P_ilh_loc_type               ilh_loc_type_TBL;
   P_ilh_eow_date               ilh_eow_date_TBL;
   P_ilh_week_454               ilh_week_454_TBL;
   P_ilh_month_454              ilh_month_454_TBL;
   P_ilh_year_454               ilh_year_454_TBL;
   P_ilh_sales_type             ilh_sales_type_TBL;
   P_ilh_sales_issues           ilh_sales_issues_TBL;
   P_ilh_create_datetime        ilh_create_datetime_TBL;
   P_ilh_last_update_datetime   ilh_last_update_datetime_TBL;
   P_ilh_last_update_id         ilh_last_update_id_TBL;
   P_ilh_size                   BINARY_INTEGER := 0;
   ------
   P_upd_ilh_sales_issues       ilh_sales_issues_TBL;
   P_upd_ilh_rowid_TBL          ilh_rowid_TBL;
   P_upd_ilh_size               BINARY_INTEGER := 0;
   P_ilh_dept                   ilh_dept_TBL;
   P_ilh_class                  ilh_class_TBL;
   P_ilh_subclass               ilh_subclass_TBL;   

   cursor C_GET_STORE_TYPE(I_store  STORE.STORE%TYPE) is
      select store_type,
             stockholding_ind
        from store
       where store = I_store;

-------------------------------------------------------------------------------
--FUNCTION PROTOTYPES--
-------------------------------------------------------------------------------

$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then

   FUNCTION CREATE_TSF(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_no            IN     TSFHEAD.TSF_NO%TYPE,
                       I_tsf_type          IN     TSFHEAD.TSF_TYPE%TYPE,
                       I_phy_from_loc      IN     ITEM_LOC.LOC%TYPE,
                       I_from_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_phy_to_loc        IN     ITEM_LOC.LOC%TYPE,
                       I_to_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_tran_date         IN     PERIOD.VDATE%TYPE,
                       I_comment_desc      IN     TSFHEAD.COMMENT_DESC%TYPE,
                       I_unavailable_flag  IN     VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;

   FUNCTION NEW_LOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item           IN     ITEM_MASTER.ITEM%TYPE,
                    I_pack_ind       IN     ITEM_MASTER.PACK_IND%TYPE,
                    I_dept           IN     ITEM_MASTER.DEPT%TYPE,
                    I_class          IN     ITEM_MASTER.CLASS%TYPE,
                    I_subclass       IN     ITEM_MASTER.SUBCLASS%TYPE,
                    I_loc            IN     ITEM_LOC.LOC%TYPE,
                    I_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN;

   FUNCTION ITEM_CHECK(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tran_item        IN OUT ITEM_MASTER.ITEM%TYPE,
                       O_ref_item         IN OUT ITEM_MASTER.ITEM%TYPE,
                       O_dept             IN OUT ITEM_MASTER.DEPT%TYPE,
                       O_class            IN OUT ITEM_MASTER.CLASS%TYPE,
                       O_subclass         IN OUT ITEM_MASTER.SUBCLASS%TYPE,
                       O_pack_ind         IN OUT ITEM_MASTER.PACK_IND%TYPE,
                       O_pack_type        IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                       O_simple_pack_ind  IN OUT ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                       O_catch_weight_ind IN OUT ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                       O_sellable_ind     IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                       O_item_xform_ind   IN OUT ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                       O_supp_pack_size   IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,

                       I_input_item       IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;

   FUNCTION INS_TSFDETAIL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_tsf_seq_no            IN OUT TSFDETAIL.TSF_SEQ_NO%TYPE,
                          I_tsf_no                IN     TSFHEAD.TSF_NO%TYPE,
                          I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                          I_supp_pack_size        IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                          I_inv_status            IN     TSFDETAIL.INV_STATUS%TYPE,
                          I_tsf_qty               IN     TSFDETAIL.TSF_QTY%TYPE)
   RETURN BOOLEAN;

   FUNCTION DIST_FROM_LOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_inv_flow_array      IN OUT bol_sql.inv_flow_array,
                          I_tsf_no              IN     TSFHEAD.TSF_NO%TYPE,
                          I_tsf_seq_no          IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                          I_item                IN     ITEM_MASTER.ITEM%TYPE,
                          I_inv_status          IN     SHIPSKU.INV_STATUS%TYPE,
                          I_tsf_qty             IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_phy_to_loc          IN     ITEM_LOC.LOC%TYPE,
                          I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                          I_phy_from_loc        IN     ITEM_LOC.LOC%TYPE,
                          I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                          I_dist_type           IN     VARCHAR2,
                          I_cust_order_loc_ind  IN     VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;

   FUNCTION CREATE_SHIP_INV_MAP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE,
                               I_item           IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_ind       IN     ITEM_MASTER.PACK_IND%TYPE,
                               I_inv_status     IN     SHIPSKU.INV_STATUS%TYPE,
                               I_tsf_seq_no     IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                               I_phy_from_loc   IN     ITEM_LOC.LOC%TYPE,
                               I_from_loc_type  IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_phy_to_loc     IN     ITEM_LOC.LOC%TYPE,
                               I_to_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_tsf_qty        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
   RETURN BOOLEAN;

   FUNCTION INS_TSF_INV_FLOW(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;

   FUNCTION INS_SHIP_INV_FLOW(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;

   FUNCTION PUT_TSF_INV_MAP(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_from_loc        IN     ITEM_LOC.LOC%TYPE,
                            I_from_loc_type   IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_to_loc          IN     ITEM_LOC.LOC%TYPE,
                            I_to_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_receive_as_type IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                            I_tsf_qty         IN     TSFDETAIL.TSF_QTY%TYPE)
   RETURN BOOLEAN;

   FUNCTION SEND_TSF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_bol_no        IN     SHIPMENT.BOL_NO%TYPE,
                     I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                     I_tsf_type      IN     TSFHEAD.TSF_TYPE%TYPE,
                     I_new_tsf       IN     VARCHAR2,
                     I_tsf_to_loc    IN     ITEM_LOC.LOC%TYPE,
                     I_tsf_status    IN     TSFHEAD.STATUS%TYPE,
                     I_tran_date     IN     PERIOD.VDATE%TYPE,
                     I_ship_no       IN     SHIPMENT.SHIPMENT%TYPE,
                     I_eow_date      IN     PERIOD.VDATE%TYPE,
                     I_bol_items     IN     bol_sql.bol_item_array)
   return BOOLEAN;

   FUNCTION AUTO_RCV_LINE_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;

   --------------------------------------------------------------------------------------
   --  Name   : GET_COSTS_AND_RETAILS
   --  Purpose: The function will get the costs for the from loc.  If the from loc is an
   --           external finisher the retail will be retreived for the to location.  Otherwise,
   --           the from location's retail will be returned.
   --------------------------------------------------------------------------------------
   FUNCTION GET_COSTS_AND_RETAILS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_av_cost               IN OUT ITEM_LOC_SOH.AV_COST%TYPE,
                                  O_unit_retail           IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                                  I_pack_ind              IN     VARCHAR2,
                                  I_sellable_ind          IN     ITEM_MASTER.SELLABLE_IND%TYPE,
                                  I_item_xform_ind        IN     ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                                  I_from_loc              IN     ITEM_LOC.LOC%TYPE,
                                  I_from_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                                  I_to_loc                IN     ITEM_LOC.LOC%TYPE,
                                  I_to_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE)

   RETURN BOOLEAN;

   FUNCTION SEND_ALLOC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alloc_no             IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                       I_alloc_status         IN       ALLOC_HEADER.STATUS%TYPE,
                       I_alloc_type           IN       VARCHAR2,
                       I_new_ad_ind           IN       VARCHAR2,
                       I_ship_no              IN       SHIPMENT.SHIPMENT%TYPE,
                       I_ss_seq_no            IN       SHIPSKU.SEQ_NO%TYPE,
                       I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                       I_ref_item             IN       ITEM_MASTER.ITEM%TYPE,
                       I_pack_ind             IN       ITEM_MASTER.PACK_IND%TYPE,
                       I_pack_type            IN       ITEM_MASTER.PACK_TYPE%TYPE,
                       I_dept                 IN       ITEM_MASTER.DEPT%TYPE,
                       I_class                IN       ITEM_MASTER.CLASS%TYPE,
                       I_subclass             IN       ITEM_MASTER.SUBCLASS%TYPE,
                       I_carton               IN       SHIPSKU.CARTON%TYPE,
                       I_inv_status           IN       SHIPSKU.INV_STATUS%TYPE,
                       I_ship_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_qty                  IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_simple_pack_ind      IN       ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                       I_catch_weight_ind     IN       ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                       I_ship_weight          IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                       I_weight               IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                       I_weight_uom           IN       UOM_CLASS.UOM%TYPE,
                       I_sellable_ind         IN       ITEM_MASTER.SELLABLE_IND%TYPE,
                       I_item_xform_ind       IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                       I_ad_alloc_qty         IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_ad_tsf_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_from_loc             IN       ITEM_LOC.LOC%TYPE,
                       I_from_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                       I_to_loc               IN       ITEM_LOC.LOC%TYPE,
                       I_to_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                       I_tran_date            IN       PERIOD.VDATE%TYPE,
                       I_eow_date             IN       PERIOD.VDATE%TYPE,
                       I_extended_base_cost   IN       ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE default NULL,
                       I_base_cost            IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE default NULL,
                       I_unexpected_flag      IN       BOOLEAN   DEFAULT FALSE)
   RETURN BOOLEAN;

   FUNCTION INS_SHIPSKU(O_error_message  IN OUT VARCHAR2,
                        I_shipment       IN     SHIPMENT.SHIPMENT%TYPE,
                        I_seq_no         IN     SHIPSKU.SEQ_NO%TYPE,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE,
                        I_ref_item       IN     ITEM_MASTER.ITEM%TYPE,
                        I_distro_no      IN     SHIPSKU.DISTRO_NO%TYPE,
                        I_distro_type    IN     SHIPSKU.DISTRO_TYPE%TYPE,
                        I_carton         IN     SHIPSKU.CARTON%TYPE,
                        I_inv_status     IN     SHIPSKU.INV_STATUS%TYPE,
                        I_rcv_qty        IN     SHIPSKU.QTY_EXPECTED%TYPE,
                        I_cost           IN     SHIPSKU.UNIT_COST%TYPE,
                        I_retail         IN     SHIPSKU.UNIT_RETAIL%TYPE,
                        I_exp_qty        IN     SHIPSKU.QTY_EXPECTED%TYPE,
                        I_exp_weight     IN     SHIPSKU.WEIGHT_EXPECTED%TYPE,
                        I_exp_weight_uom IN     SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE,
                        I_adjust_type    IN     SHIPSKU.ADJUST_TYPE%TYPE default NULL)





   RETURN BOOLEAN;

   FUNCTION UPDATE_INV_STATUS(O_error_message IN OUT VARCHAR2,
                              I_item          IN     ITEM_MASTER.ITEM%TYPE,
                              I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                              I_from_loc      IN     ITEM_LOC.LOC%TYPE,
                              I_from_loc_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                              I_qty           IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_inv_status    IN     SHIPSKU.INV_STATUS%TYPE)
   RETURN BOOLEAN;

   FUNCTION UPD_FROM_ITEM_LOC(O_error_message    IN OUT VARCHAR2,
                              I_item             IN     ITEM_MASTER.ITEM%TYPE,
                              I_comp_item        IN     VARCHAR2,
                              I_sellable_ind     IN     ITEM_MASTER.SELLABLE_IND%TYPE,
                              I_item_xform_ind   IN     ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                              I_inventory_ind    IN     ITEM_MASTER.INVENTORY_IND%TYPE,
                              I_from_loc         IN     ITEM_LOC.LOC%TYPE,
                              I_tsf_qty          IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_resv_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_tsf_weight       IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_tsf_weight_uom   IN     UOM_CLASS.UOM%TYPE,
                              I_tran_date        IN     PERIOD.VDATE%TYPE,
                              I_eow_date         IN     PERIOD.VDATE%TYPE)
   RETURN BOOLEAN;

   FUNCTION UPD_TO_ITEM_LOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item            IN     ITEM_MASTER.ITEM%TYPE,
                            I_pack_no         IN     ITEM_MASTER.ITEM%TYPE,
                            I_percent_in_pack IN     NUMBER,
                            I_receive_as_type IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                            I_tsf_type        IN     TSFHEAD.TSF_TYPE%TYPE,
                            I_to_loc          IN     ITEM_LOC.LOC%TYPE,
                            I_to_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_tsf_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            I_exp_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            I_intran_qty      IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            I_weight_cuom     IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                            I_cuom            IN     UOM_CLASS.UOM%TYPE,
                            I_from_loc        IN     ITEM_LOC.LOC%TYPE,
                            I_from_loc_type   IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_prim_charge     IN     ITEM_LOC_SOH.AV_COST%TYPE,
                            I_distro_no       IN     SHIPSKU.DISTRO_NO%TYPE,
                            I_distro_type     IN     SHIPSKU.DISTRO_TYPE%TYPE,
                            I_intercompany    IN     BOOLEAN)
   RETURN BOOLEAN;

   FUNCTION UPDATE_PACK_LOCS(O_error_message       IN OUT VARCHAR2,
                             I_item                IN     ITEM_MASTER.ITEM%TYPE,
                             I_from_loc            IN     ITEM_LOC.LOC%TYPE,
                             I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_to_loc              IN     ITEM_LOC.LOC%TYPE,
                             I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_to_receive_as_type  IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                             I_tsf_type            IN     TSFHEAD.TSF_TYPE%TYPE,
                             I_tsf_qty             IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_tsf_weight_cuom     IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_intran_qty          IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_intran_weight_cuom  IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_resv_exp_qty        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_tran_date           IN     PERIOD.VDATE%TYPE)
   RETURN BOOLEAN;

   FUNCTION UPD_TSF_ITEM_COST(O_error_message    IN OUT VARCHAR2,
                              I_item             IN     ITEM_MASTER.ITEM%TYPE,
                              I_ship_qty         IN     TSF_ITEM_COST.SHIPPED_QTY%TYPE,
                              I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE)
   RETURN BOOLEAN;

   FUNCTION WRITE_ISSUES(O_error_message   IN OUT VARCHAR2,
                         I_item            IN     ITEM_MASTER.ITEM%TYPE,
                         I_from_wh         IN     ITEM_LOC.LOC%TYPE,
                         I_transferred_qty IN     TSFDETAIL.TSF_QTY%TYPE,
                         I_eow_date        IN     PERIOD.VDATE%TYPE,
                         I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                         I_class           IN     ITEM_MASTER.CLASS%TYPE,
                         I_subclass        IN     ITEM_MASTER.SUBCLASS%TYPE)
   RETURN BOOLEAN;

   FUNCTION NEXT_SS_SEQ_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_ss_seq_no     IN OUT SHIPSKU.SEQ_NO%TYPE)
   RETURN BOOLEAN;

   --BULK HELPERS

   FUNCTION FLUSH_SHIPSKU_INSERT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;

   FUNCTION FLUSH_ILH_INSERT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;

   FUNCTION FLUSH_ILH_UPDATE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;

   -------------------------------------------------------------------------------
   --- This internal function has common code that is used in both PUT_TSF_ITEM
   --- and RECEIPT_PUT_TSF_ITEM
   -------------------------------------------------------------------------------
   FUNCTION TSF_ITEM_COMMON(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE,
                            I_carton           IN     SHIPSKU.CARTON%TYPE,
                            I_qty              IN     TSFDETAIL.TSF_QTY%TYPE,
                            I_weight           IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                            I_weight_uom       IN     UOM_CLASS.UOM%TYPE,
                            I_phy_from_loc     IN     ITEM_LOC.LOC%TYPE,
                            I_from_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_phy_to_loc       IN     ITEM_LOC.LOC%TYPE,
                            I_to_loc_type      IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_tsfhead_to_loc   IN     ITEM_LOC.LOC%TYPE,
                            I_tsfhead_from_loc IN     ITEM_LOC.LOC%TYPE,
                            I_tsf_type         IN     TSFHEAD.TSF_TYPE%TYPE,
                            I_inv_status       IN     INV_STATUS_CODES.INV_STATUS%TYPE,
                            I_tsf_qty          IN     TSFDETAIL.TSF_QTY%TYPE,
                            I_ship_qty         IN     TSFDETAIL.SHIP_QTY%TYPE,
                            I_tsf_seq_no       IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                            I_item_cnt         IN     BINARY_INTEGER,
                            I_dist_cnt         IN     BINARY_INTEGER)
   RETURN BOOLEAN;
$end
-------------------------------------------------------------------------------
--public function--
FUNCTION PUT_BOL(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_bol_exists        IN OUT BOOLEAN,
                 I_bol_no            IN     SHIPMENT.BOL_NO%TYPE,
                 I_phy_from_loc      IN     SHIPMENT.FROM_LOC%TYPE,
                 I_phy_to_loc        IN     SHIPMENT.TO_LOC%TYPE,
                 I_ship_date         IN     PERIOD.VDATE%TYPE,
                 I_est_arr_date      IN     PERIOD.VDATE%TYPE,
                 I_no_boxes          IN     SHIPMENT.NO_BOXES%TYPE,
                 I_courier           IN     SHIPMENT.COURIER%TYPE,
                 I_ext_ref_no_out    IN     SHIPMENT.EXT_REF_NO_OUT%TYPE,
                 I_comments          IN     SHIPMENT.COMMENTS%TYPE)
RETURN BOOLEAN IS

   L_shipment                 SHIPMENT.SHIPMENT%TYPE := NULL;
   L_to_loc_type              ITEM_LOC.LOC_TYPE%TYPE := NULL;
   L_from_loc_type            ITEM_LOC.LOC_TYPE%TYPE := NULL;
   L_table                    VARCHAR2(30)          := 'SHIPMENT';
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);

  -- cursors
   cursor C_SHIPMENT is
      select sh.shipment,
             sh.to_loc_type,
             sh.from_loc_type
       from shipment sh
      where sh.bol_no = I_bol_no
        and sh.to_loc = I_phy_to_loc
        and sh.from_loc = I_phy_from_loc
        for update nowait;

   cursor C_BOL_EXISTS is
      select sh.shipment
        from shipment sh
       where sh.bol_no =  I_bol_no;

   cursor C_BOL_EXISTS_SEQ is
      select NVL(max(seq_no), 0)
        from shipsku
       where shipment =  L_shipment;

BEGIN

   if I_bol_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_bol_no','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_phy_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_phy_from_loc','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_phy_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_phy_to_loc','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_ship_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_ship_date','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_phy_from_loc = I_phy_to_loc then
      O_error_message := SQL_LIB.CREATE_MSG('SAME_LOC',I_phy_from_loc,null,null);
      return FALSE;
   end if;

   LP_bol_rec := NULL;

   if INIT_BOL_PROCESS(O_error_message) = FALSE then
      return FALSE;
   end if;

   open C_SHIPMENT;
   fetch C_SHIPMENT into L_shipment,
                         L_to_loc_type,
                         L_from_loc_type;
   close C_SHIPMENT;

   if L_shipment IS NOT NULL then
      -- processing previously 'unwanded' carton that was
      -- physically on a shipment but wasn't initially scanned.
      O_bol_exists := TRUE;

      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'SHIPMENT',
                       'Shipment: '||to_char(L_shipment));
      update shipment
         set no_boxes = NVL(no_boxes,0) + NVL(I_no_boxes,0)
       where shipment = L_shipment;

      open C_BOL_EXISTS_SEQ;
      fetch C_BOL_EXISTS_SEQ into LP_ss_seq_no;
      close C_BOL_EXISTS_SEQ;

   else
      open  C_BOL_EXISTS;
      fetch C_BOL_EXISTS into L_shipment;
      close C_BOL_EXISTS;

      -- If L_shipment is NOT NULL then the BOL number exists for a different from location and to location
      if L_shipment is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('BOL_FROM_TO_EXIST',null,null,null);
         return FALSE;
      end if;

   end if;

   if L_shipment IS NULL then
      O_bol_exists := FALSE;

      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_from_loc_type,
                                      I_phy_from_loc) = FALSE then
         return FALSE;
      end if;

      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_to_loc_type,
                                      I_phy_to_loc) = FALSE then
         return FALSE;
      end if;

      if SHIPMENT_ATTRIB_SQL.NEXT_SHIPMENT(O_error_message,
                                           L_shipment) = FALSE then
         return FALSE;
      end if;

      insert into shipment ( shipment,
                             order_no,
                             bol_no,
                             asn,
                             ship_date,
                             receive_date,
                             est_arr_date,
                             ship_origin,
                             status_code,
                             invc_match_status,
                             invc_match_date,
                             to_loc,
                             to_loc_type,
                             from_loc,
                             from_loc_type,
                             courier,
                             no_boxes,
                             ext_ref_no_in,
                             ext_ref_no_out,
                             comments )
                    values ( L_shipment,              --shipment
                             NULL,                    --order_no
                             I_bol_no,                --bol
                             NULL,                    --asn
                             I_ship_date,             --ship_date
                             NULL,                    --receive_date
                             I_est_arr_date,          --est_arr_date
                             3,                       --ship_origin       --external
                             'I',                     --status_code       --input
                             NULL,                    --invc_match_status --unmatched
                             NULL,                    --invc_match_date
                             I_phy_to_loc,            --to_loc
                             L_to_loc_type,           --to_loc_type
                             I_phy_from_loc,          --from_loc
                             L_from_loc_type,         --from_loc_type
                             I_courier,               --courier (carrier_code in bol msg)
                             I_no_boxes,              --no_boxes (container_qty in bol msg)
                             NULL,                    --ext_ref_no_in
                             I_ext_ref_no_out,        --ext_ref_no_out (bol_no in bol msg)
                             I_comments);             --comments
   end if;

   LP_bol_rec.bol_no               := I_bol_no;
   LP_bol_rec.ship_no              := L_shipment;
   LP_bol_rec.phy_from_loc         := I_phy_from_loc;
   LP_bol_rec.phy_from_loc_type    := L_from_loc_type;
   LP_bol_rec.phy_to_loc           := I_phy_to_loc;
   LP_bol_rec.phy_to_loc_type      := L_to_loc_type;
   LP_bol_rec.tran_date            := I_ship_date;

   if DATES_SQL.GET_EOW_DATE(O_error_message,
                             LP_bol_rec.eow_date,
                             I_ship_date) = FALSE then
      return FALSE;
   end if;

   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                LP_bol_rec.franchise_ordret_ind,
                                                I_phy_from_loc,
                                                L_from_loc_type,
                                                I_phy_to_loc,
                                                L_to_loc_type) = FALSE then
      return FALSE;
   end if;

   if L_from_loc_type = 'S' then
      open C_GET_STORE_TYPE(I_phy_from_loc);
      fetch C_GET_STORE_TYPE into LP_bol_rec.from_store_type,
                                  LP_bol_rec.from_stockholding_ind;
      close C_GET_STORE_TYPE;
   end if;

   if L_to_loc_type = 'S' then
      open C_GET_STORE_TYPE(I_phy_to_loc);
      fetch C_GET_STORE_TYPE into LP_bol_rec.to_store_type,
                                  LP_bol_rec.to_stockholding_ind;
      close C_GET_STORE_TYPE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'SHIPMENT:'||to_char(L_shipment),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.PUT_BOL',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PUT_BOL;
-------------------------------------------------------------------------------
--public function--
FUNCTION PUT_ALLOC(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_item                  IN OUT   ITEM_MASTER.ITEM%TYPE,
                   O_alloc_head_from_loc   IN OUT   ITEM_LOC.LOC%TYPE,
                   I_alloc_no              IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                   I_phy_from_loc          IN       ITEM_LOC.LOC%TYPE,
                   I_item                  IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_ah_wh              ITEM_LOC.LOC%TYPE          := NULL;
   L_status             ALLOC_HEADER.STATUS%TYPE   := NULL;
   L_order_no           ALLOC_HEADER.ORDER_NO%TYPE := NULL;
   dist_cnt             BINARY_INTEGER             := NULL;
   item_cnt             BINARY_INTEGER             := NULL;

   L_item               ITEM_MASTER.ITEM%TYPE                 := NULL;
   L_ref_item           ITEM_MASTER.ITEM%TYPE                 := NULL;
   L_pack_ind           ITEM_MASTER.PACK_IND%TYPE             := NULL;
   L_pack_type          ITEM_MASTER.PACk_TYPE%TYPE            := NULL;
   L_simple_pack_ind    ITEM_MASTER.SIMPLE_PACK_IND%TYPE      := NULL;
   L_catch_weight_ind   ITEM_MASTER.CATCH_WEIGHT_IND%TYPE     := NULL;
   L_sellable_ind       ITEM_MASTER.SELLABLE_IND%TYPE         := NULL;
   L_item_xform_ind     ITEM_MASTER.ITEM_XFORM_IND%TYPE       := NULL;
   L_supp_pack_size     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NULL;
   L_dept               ITEM_MASTER.DEPT%TYPE                 := NULL;
   L_class              ITEM_MASTER.CLASS%TYPE                := NULL;
   L_subclass           ITEM_MASTER.SUBCLASS%TYPE             := NULL;
   L_ss_seq_no          SHIPSKU.SEQ_NO%TYPE                   := NULL;
   L_rowid              ROWID;
   L_reopen             varchar2(1) :='N';

   -- cursors
   cursor C_ALLOC_HEADER is
      select ah.wh,
             ah.status,
             ah.order_no,
             ah.rowid
        from alloc_header ah,
             wh w
       where ah.alloc_no   = I_alloc_no
         and ah.wh         = w.wh
         and ah.item       = O_item
         and w.physical_wh = I_phy_from_loc
         for update of ah.status nowait;

BEGIN
   if I_alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_alloc_no','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_phy_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_phy_from_loc','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_item','NULL','NOT NULL');
      return FALSE;
   end if;

   /* reset the distro array */
   LP_bol_rec.distros.DELETE;
   dist_cnt := 1;

   LP_bol_rec.distros(dist_cnt).alloc_no := I_alloc_no;

   /* assumes that there is only one item per alloc */
   item_cnt := 1;

   if BOL_SQL.ITEM_CHECK(O_error_message,







                         L_item,
                         L_ref_item,
                         L_dept,
                         L_class,
                         L_subclass,
                         L_pack_ind,
                         L_pack_type,
                         L_simple_pack_ind,
                         L_catch_weight_ind,
                         L_sellable_ind,
                         L_item_xform_ind,
                         L_supp_pack_size,

                         I_item) = FALSE then
      return FALSE;
   end if;

   if NEXT_SS_SEQ_NO(O_error_message,
                     L_ss_seq_no) = FALSE then
      return FALSE;
   end if;

   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item             := L_item;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ref_item         := L_ref_item;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).dept             := L_dept;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).class            := L_class;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).subclass         := L_subclass;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_ind         := L_pack_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_type        := L_pack_type;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).simple_pack_ind  := L_simple_pack_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).catch_weight_ind := L_catch_weight_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).sellable_ind     := L_sellable_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item_xform_ind   := L_item_xform_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).supp_pack_size   := L_supp_pack_size;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ss_seq_no        := L_ss_seq_no;

   O_item := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item;

   open C_ALLOC_HEADER;
   fetch C_ALLOC_HEADER into L_ah_wh,
                             L_status,
                             L_order_no,
                             L_rowid;
   close C_ALLOC_HEADER;

   if L_ah_wh IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ALLOC_NUM', NULL, NULL, NULL);
      return FALSE;
   end if;


   if L_order_no IS NULL then
      LP_bol_rec.distros(dist_cnt).alloc_type := 'SA';
   else
      LP_bol_rec.distros(dist_cnt).alloc_type := 'PRE';
   end if;
    if L_status ='C' then
       update alloc_header ah
         set ah.status = 'A'
        where ah.rowid = L_rowid;

    /* If shipping is done for closed allocations , then reopen the allocation*/
        L_status :='A';
        L_reopen := 'Y';
    end if;

   LP_bol_rec.distros(dist_cnt).alloc_status := L_status;
   LP_bol_rec.distros(dist_cnt).alloc_from_loc_phy := I_phy_from_loc;
   LP_bol_rec.distros(dist_cnt).alloc_from_loc_vir := L_ah_wh;

   O_alloc_head_from_loc :=  L_ah_wh;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.PUT_ALLOC',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PUT_ALLOC;
-------------------------------------------------------------------------------
--public function--
FUNCTION PUT_ALLOC_ITEM(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_detail_created        IN OUT   BOOLEAN,
                        I_alloc_no              IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                        I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                        I_carton                IN       SHIPSKU.CARTON%TYPE,
                        I_qty                   IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        I_weight                IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                        I_weight_uom            IN       UOM_CLASS.UOM%TYPE,
                        I_inv_status            IN       INV_STATUS_CODES.INV_STATUS%TYPE,
                        I_phy_to_loc            IN       ITEM_LOC.LOC%TYPE,
                        I_to_loc_type           IN       ITEM_LOC.LOC_TYPE%TYPE,
                        I_alloc_head_from_loc   IN       ITEM_LOC.LOC%TYPE,
                        I_extended_base_cost    IN       ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE default NULL,
                        I_base_cost             IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE default NULL)
RETURN BOOLEAN IS

   L_loc           ITEM_LOC.LOC%TYPE               := NULL;
   L_alloc_qty     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := NULL;
   L_tsf_qty       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := NULL;
   L_dist_qty      ALLOC_DETAIL.QTY_ALLOCATED%TYPE := NULL;
   dist_cnt        BINARY_INTEGER := 1;
   item_cnt        BINARY_INTEGER := 1;
   L_rowid         ROWID;
   L_dist_tab      DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   L_exists        VARCHAR2(1) := NULL;
   L_table         VARCHAR2(30);
   L_key1          VARCHAR2(100);
   L_key2          VARCHAR2(100);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   -- cursors
   cursor C_ALLOC_EXISTS is
      select 'x'
        from alloc_detail ad,
             wh w
       where ad.alloc_no = I_alloc_no
         and ad.to_loc = NVL(w.wh, I_phy_to_loc)
         and w.wh(+) = ad.to_loc
         and w.physical_wh(+) = I_phy_to_loc

         for update of ad.qty_transferred, ad.qty_distro nowait;

   cursor C_ALLOC_DETAIL is
      select NVL(qty_allocated, 0),
             NVL(qty_transferred, 0),
             rowid
        from alloc_detail
       where alloc_no = I_alloc_no
         and to_loc = L_loc;

   -- return the store or the primary virtual wh in the physical wh
   -- to be used for creating ALLOC_DETAIL
   -- ALLOC_DETAILs are only created with virtual whs
   cursor C_LOC is
      select s.store
        from store s
       where s.store = I_phy_to_loc
       union all
      select w1.wh
        from wh w1,
             wh w2
       where w1.wh <> w1.physical_wh
         and w1.physical_wh = I_phy_to_loc
         and w1.physical_wh = w2.wh
         and w1.wh = w2.primary_vwh;

BEGIN
   if I_alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_alloc_no','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_item','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_qty','NULL','NOT NULL');
      return FALSE;
   elsif I_qty <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('QTY_GREATER_ZERO',
                                            ' Alloc No : '||I_alloc_no,
                                            ' Item : ' || I_item,
                                            ' Quantity : '||I_qty);
      return FALSE;
   end if;
   if I_phy_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_phy_to_loc','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_to_loc_type','NULL','NOT NULL');
      return FALSE;
   end if;
   --not possible to alloc non-sellable stock
   if I_inv_status != -1 then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INV_STATUS',I_inv_status,NULL,NULL);
      return FALSE;
   end if;

   dist_cnt := LP_bol_rec.distros.COUNT;
   item_cnt := 1; --there can only ever be one item per alloc.

   L_table := 'ALLOC_DETAIL';
   L_key1  := TO_CHAR(I_alloc_no);
   L_key2  := TO_CHAR(I_phy_to_loc);

   open C_ALLOC_EXISTS;
   fetch C_ALLOC_EXISTS into L_exists;
   close C_ALLOC_EXISTS;

   if L_exists is NULL then -- alloc_detail record does not exist
      LP_bol_rec.distros(dist_cnt).new_alloc_detail_ind := 'Y';
      O_detail_created := TRUE;
      L_alloc_qty      := 0;
      L_tsf_qty        := 0;

      open C_LOC;
      fetch C_LOC into L_loc;
      close C_LOC;

      if L_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOC');
         return FALSE;
      end if;

      if NEW_LOC(O_error_message,
                 LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item,
                 LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_ind,
                 LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).dept,
                 LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).class,
                 LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).subclass,
                 L_loc,
                 I_to_loc_type) = FALSE then
         return FALSE;
      end if;

      insert into alloc_detail (alloc_no,
                                to_loc,
                                to_loc_type,
                                qty_transferred,
                                qty_allocated,
                                qty_prescaled,
                                qty_distro,
                                qty_selected,
                                qty_cancelled,
                                qty_received,
                                po_rcvd_qty,
                                non_scale_ind,
                                wf_order_no)
                        values (I_alloc_no,
                                L_loc,
                                I_to_loc_type,
                                I_qty,    --qty_transferred
                                I_qty,    --qty_allocated
                                0,        --qty_prescaled
                                0,        --qty_distro
                                0,        --qty_selected
                                0,        --qty_cancelled
                                0,        --qty_received
                                NULL,     --po_rcvd_qty
                                'N',      --non_scale_ind
                                NULL);    --wf_order_no

      if ALLOC_CHARGE_SQL.DEFAULT_CHRGS(O_error_message,
                                        I_alloc_no,
                                        I_alloc_head_from_loc,
                                        L_loc,
                                        I_to_loc_type,
                                        I_item) = FALSE then
         return FALSE;
      end if;

      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_to_loc_phy      := I_phy_to_loc;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_to_loc_vir      := L_loc;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).qty                := I_qty;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).inv_status         := I_inv_status;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).carton             := I_carton;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_tsf_qty         := L_tsf_qty;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_alloc_qty       := L_alloc_qty;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).extended_base_cost := I_extended_base_cost;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).base_cost          := I_base_cost;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_dist_qty        := I_qty;

      -- set weight for a simple pack catch weight item
      if LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).simple_pack_ind = 'Y' and
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).catch_weight_ind = 'Y' and
         I_weight is NOT NULL and
         I_weight_uom is NOT NULL then
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).weight         := I_weight;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).weight_uom     := I_weight_uom;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_dist_weight := I_weight;
      end if;
   else --alloc_detail exists
      if I_to_loc_type = 'W' then
         if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                        L_dist_tab,
                                        I_item,
                                        I_phy_to_loc,
                                        I_qty,
                                        'ALLOC',
                                        NULL,     --inv_status
                                        NULL,     --to_loc_type
                                        NULL,     --to_loc
                                        NULL,     --order_no
                                        NULL,     --shipment
                                        NULL,     --seq_no
                                        NULL,     --cycle_count
                                        NULL,     --rtv_order_no
                                        NULL,     --rtv_seq_no
                                        NULL,     --tsf_no
                                        NULL,     --tsf_create_ind
                                        NULL,     --cust_order_loc_ind
                                        I_alloc_no) = FALSE then
            return FALSE;
         end if;
      else --alloc to store
         L_dist_tab(1).to_loc   := I_phy_to_loc;
         L_dist_tab(1).dist_qty := I_qty;
      end if;

      FOR i in L_dist_tab.FIRST..L_dist_tab.LAST LOOP
         L_loc      := L_dist_tab(i).to_loc;
         L_dist_qty := L_dist_tab(i).dist_qty;

         LP_bol_rec.distros(dist_cnt).new_alloc_detail_ind := 'N';
         O_detail_created := FALSE;

         open C_ALLOC_DETAIL;
         fetch C_ALLOC_DETAIL into L_alloc_qty,
                                   L_tsf_qty,
                                   L_rowid;
         close C_ALLOC_DETAIL;

         /* We do not update qty_allocated here since we do not know
          * if the alloc_detail record was originally on the allocation
          * or if it was added through a previous run of this module.  In
          * the case of transfers, we do know if the transfer was externally
          * generated.  For externally generated transfers we do increment
          * tsfdetail.tsf_qty (the analogous field to qty_allocated for a transfer).
          */
         update alloc_detail ad
            set ad.qty_transferred = NVL(ad.qty_transferred, 0) + L_dist_qty,
                ad.qty_distro = NVL(ad.qty_distro, 0) - L_dist_qty
          where ad.rowid = L_rowid;

         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_to_loc_phy      := I_phy_to_loc;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_to_loc_vir      := L_loc;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).qty                := I_qty;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).inv_status         := I_inv_status;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).carton             := I_carton;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_tsf_qty         := L_tsf_qty;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_alloc_qty       := L_alloc_qty;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).extended_base_cost := I_extended_base_cost;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).base_cost          := I_base_cost;
         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_dist_qty        := L_dist_qty;

         if item_cnt > 1 then
            --copy attributes from the first bol_items record to the current record
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item             := LP_bol_rec.distros(dist_cnt).bol_items(1).item;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ref_item         := LP_bol_rec.distros(dist_cnt).bol_items(1).ref_item;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).dept             := LP_bol_rec.distros(dist_cnt).bol_items(1).dept;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).class            := LP_bol_rec.distros(dist_cnt).bol_items(1).class;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).subclass         := LP_bol_rec.distros(dist_cnt).bol_items(1).subclass;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_ind         := LP_bol_rec.distros(dist_cnt).bol_items(1).pack_ind;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_type        := LP_bol_rec.distros(dist_cnt).bol_items(1).pack_type;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).simple_pack_ind  := LP_bol_rec.distros(dist_cnt).bol_items(1).simple_pack_ind;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).catch_weight_ind := LP_bol_rec.distros(dist_cnt).bol_items(1).catch_weight_ind;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).sellable_ind     := LP_bol_rec.distros(dist_cnt).bol_items(1).sellable_ind;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item_xform_ind   := LP_bol_rec.distros(dist_cnt).bol_items(1).item_xform_ind;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).supp_pack_size   := LP_bol_rec.distros(dist_cnt).bol_items(1).supp_pack_size;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ss_seq_no        := LP_bol_rec.distros(dist_cnt).bol_items(1).ss_seq_no;
         end if;

         -- set weight for a simple pack catch weight item
         if LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).simple_pack_ind = 'Y' and
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).catch_weight_ind = 'Y' and
            I_weight is NOT NULL and
            I_weight_uom is NOT NULL then
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).weight         := I_weight;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).weight_uom     := I_weight_uom;
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_dist_weight := I_weight * L_dist_qty/I_qty;
         end if;

         item_cnt := item_cnt + 1;

      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.PUT_ALLOC_ITEM',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PUT_ALLOC_ITEM;
-------------------------------------------------------------------------------
--public function--
FUNCTION PROCESS_ALLOC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_unexpected_flag   IN       BOOLEAN   DEFAULT FALSE)
RETURN BOOLEAN IS

   dist_cnt BINARY_INTEGER := 1;
   item_cnt BINARY_INTEGER := 1;

BEGIN

   --Allocs can have multiple details with to_locs in the same physical wh.
   --Multiple alloc_details with to_locs in the same physical wh are added as a single distros under LP_bol_rec
   --and multiple bol_items under distros (see PUT_ALLOC_ITEM). These bol_items will have the same item but different
   --virtual to_locs (ad_to_loc_vir) and different distributed qty (ad_dist_qty) and distributed weight (ad_dist_weight).

   FOR item_cnt IN LP_bol_rec.distros(dist_cnt).bol_items.FIRST..LP_bol_rec.distros(dist_cnt).bol_items.LAST LOOP
      if BOL_SQL.SEND_ALLOC(O_error_message,
                            LP_bol_rec.distros(dist_cnt).alloc_no,
                            LP_bol_rec.distros(dist_cnt).alloc_status,
                            LP_bol_rec.distros(dist_cnt).alloc_type,
                            LP_bol_rec.distros(dist_cnt).new_alloc_detail_ind,
                            LP_bol_rec.ship_no,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ss_seq_no,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ref_item,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_ind,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_type,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).dept,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).class,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).subclass,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).carton,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).inv_status,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).qty,    --I_ship_qty
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_dist_qty,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).simple_pack_ind,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).catch_weight_ind,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).weight, --I_ship_weight
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_dist_weight,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).weight_uom,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).sellable_ind,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item_xform_ind,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_alloc_qty,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_tsf_qty,
                            LP_bol_rec.distros(dist_cnt).alloc_from_loc_vir,
                            LP_bol_rec.phy_from_loc_type,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_to_loc_vir,
                            LP_bol_rec.phy_to_loc_type,
                            LP_bol_rec.tran_date,
                            LP_bol_rec.eow_date,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).extended_base_cost,
                            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).base_cost,
                            I_unexpected_flag) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'BOL_SQL.PROCESS_ALLOC',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_ALLOC;
-------------------------------------------------------------------------------
FUNCTION SEND_ALLOC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_alloc_no             IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                    I_alloc_status         IN       ALLOC_HEADER.STATUS%TYPE,
                    I_alloc_type           IN       VARCHAR2,
                    I_new_ad_ind           IN       VARCHAR2,
                    I_ship_no              IN       SHIPMENT.SHIPMENT%TYPE,
                    I_ss_seq_no            IN       SHIPSKU.SEQ_NO%TYPE,
                    I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                    I_ref_item             IN       ITEM_MASTER.ITEM%TYPE,
                    I_pack_ind             IN       ITEM_MASTER.PACK_IND%TYPE,
                    I_pack_type            IN       ITEM_MASTER.PACK_TYPE%TYPE,
                    I_dept                 IN       ITEM_MASTER.DEPT%TYPE,
                    I_class                IN       ITEM_MASTER.CLASS%TYPE,
                    I_subclass             IN       ITEM_MASTER.SUBCLASS%TYPE,
                    I_carton               IN       SHIPSKU.CARTON%TYPE,
                    I_inv_status           IN       SHIPSKU.INV_STATUS%TYPE,
                    I_ship_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                    I_qty                  IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                    I_simple_pack_ind      IN       ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                    I_catch_weight_ind     IN       ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                    I_ship_weight          IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                    I_weight               IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                    I_weight_uom           IN       UOM_CLASS.UOM%TYPE,
                    I_sellable_ind         IN       ITEM_MASTER.SELLABLE_IND%TYPE,
                    I_item_xform_ind       IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                    I_ad_alloc_qty         IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                    I_ad_tsf_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                    I_from_loc             IN       ITEM_LOC.LOC%TYPE,
                    I_from_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_to_loc               IN       ITEM_LOC.LOC%TYPE,
                    I_to_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_tran_date            IN       PERIOD.VDATE%TYPE,
                    I_eow_date             IN       PERIOD.VDATE%TYPE,
                    I_extended_base_cost   IN       ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE default NULL,
                    I_base_cost            IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE default NULL,
                    I_unexpected_flag      IN       BOOLEAN   DEFAULT FALSE)
RETURN BOOLEAN IS

   L_resv_exp_qty             ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := NULL;
   L_intran_qty               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := NULL;

   --This holds the unit retail used in stock ledger write, converted to from-loc currency
   L_from_wac                 item_loc_soh.av_cost%TYPE       := NULL;
   L_from_wac_to_loc_curr     item_loc_soh.av_cost%TYPE       := NULL;
   L_charge_to_loc            item_loc_soh.av_cost%TYPE       := NULL;
   L_from_unit_retail         ITEM_LOC.UNIT_RETAIL%TYPE       := NULL;

   -- for prorating pack's charges
   L_pack_no                  ITEM_MASTER.ITEM%TYPE       := NULL;
   L_pack_loc_av_cost         ITEM_LOC_SOH.AV_COST%TYPE   := NULL;
   L_pack_loc_retail          ITEM_LOC.UNIT_RETAIL%TYPE   := NULL;
   -- dummy
   L_pack_loc_cost            ITEM_LOC_SOH.UNIT_COST%TYPE := NULL;
   L_pack_selling_unit_retail ITEM_LOC.UNIT_RETAIL%TYPE   := NULL;
   L_pack_selling_uom         ITEM_LOC.SELLING_UOM%TYPE   := NULL;
   L_from_loc_av_cost         ITEM_LOC_SOH.AV_COST%TYPE   := NULL;
   L_from_loc_cost            ITEM_LOC_SOH.UNIT_COST%TYPE := NULL;
   L_from_selling_unit_retail ITEM_LOC.UNIT_RETAIL%TYPE   := NULL;
   L_from_selling_uom         ITEM_LOC.SELLING_UOM%TYPE   := NULL;

   -- for charges
   L_total_chrgs_prim         ITEM_LOC.UNIT_RETAIL%TYPE       := 0;
   L_profit_chrgs_to_loc      NUMBER                          := 0;
   L_exp_chrgs_to_loc         NUMBER                          := 0;
   L_pack_total_chrgs_prim    ITEM_LOC.UNIT_RETAIL%TYPE       := 0;
   L_pack_profit_chrgs_to_loc NUMBER                          := 0;
   L_pack_exp_chrgs_to_loc    NUMBER                          := 0;
   L_pack_receive_as_type     ITEM_LOC.RECEIVE_AS_TYPE%TYPE   := NULL;
   L_from_receive_as_type     item_loc.receive_as_type%TYPE   := NULL;

   L_to_wh                    ITEM_LOC.LOC%TYPE := NULL;
   L_to_store                 ITEM_LOC.LOC%TYPE := NULL;
   L_from_wh                  ITEM_LOC.LOC%TYPE := NULL;
   L_from_store               ITEM_LOC.LOC%TYPE := NULL;

   --Alloc may be intercompany
   L_intercompany             BOOLEAN := FALSE;

   --This holds the unit cost used in stock ledger write, converted to from-loc currency
   L_alloc_unit_cost          ITEM_LOC_SOH.AV_COST%TYPE := NULL;

   L_ss_cost                  ITEM_LOC_SOH.AV_COST%TYPE       := 0;
   L_ss_prim_chrgs            ITEM_LOC_SOH.AV_COST%TYPE       := 0;
   L_ss_from_chrgs            ITEM_LOC_SOH.AV_COST%TYPE       := 0;
   L_ss_retail                ITEM_LOC.UNIT_RETAIL%TYPE       := 0;
   L_ss_rcv_qty               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_ss_qty                   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;

   -- for simple pack catch weight processing
   L_weight_cuom              ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE := NULL;
   L_cuom                     ITEM_SUPP_COUNTRY.COST_UOM%TYPE := NULL;
   L_ss_exp_weight            SHIPSKU.WEIGHT_EXPECTED%TYPE := NULL;
   L_ss_exp_weight_uom        SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE := NULL;

   L_pct_in_pack              NUMBER := 0;
   L_extended_base_cost       ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE;
   L_base_cost                ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE;

   --- for store end auto receiving functionality
   L_auto_rcv_ind             VARCHAR2(1)         := 'N';
   L_store_auto_rcv_ind       STORE.AUTO_RCV%TYPE := NULL;
   L_store_row                STORE%ROWTYPE;
   L_exists                   BOOLEAN := FALSE;

   L_wf_order_no              WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_rma_no                   WF_RETURN_HEAD.RMA_NO%TYPE;

   -- cursors
   cursor C_PACK_RCV_AS_TYPE is
      select NVL(il.receive_as_type, 'E')
        from item_loc il
       where il.loc  = I_to_loc
         and il.item = L_pack_no;

   cursor C_ITEM_IN_PACK is
      select v.item,
             v.qty,
             im.dept,
             im.class,
             im.subclass,
             im.sellable_ind,
             im.item_xform_ind,
             im.inventory_ind
        from item_master im,
             v_packsku_qty v
       where v.pack_no = L_pack_no
         and im.item   = v.item;


   cursor C_FROM_RCV_AS_TYPE is
      select NVL(il.receive_as_type, 'E')
        from item_loc il
       where il.loc  = I_from_loc
         and il.item = L_pack_no;
BEGIN

   L_from_wh    := I_from_loc;
   L_from_store := -1;
   L_to_wh      := -1;
   L_to_store   := I_to_loc;

   if I_alloc_status = 'C' or
      I_alloc_type = 'PRE' or
      I_new_ad_ind = 'Y' then
      L_resv_exp_qty := 0;
   else
      if I_ad_tsf_qty >= I_ad_alloc_qty then
         L_resv_exp_qty := 0;
      elsif I_qty + I_ad_tsf_qty > I_ad_alloc_qty then
         L_resv_exp_qty := I_ad_alloc_qty - I_ad_tsf_qty;
      else
         L_resv_exp_qty := I_qty;
      end if;
   end if;

   L_intran_qty := I_qty;

   if TRANSFER_SQL.IS_INTERCOMPANY(O_error_message,
                                   L_intercompany,
                                   'A',  -- distro type
                                   NULL, -- transfer type
                                   I_from_loc,
                                   I_from_loc_type,
                                   I_to_loc,
                                   I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   -- write weight on the message to shipsku weight_expected
   if I_simple_pack_ind = 'Y' and I_catch_weight_ind = 'Y' then
      if I_ship_weight is NOT NULL and I_weight_uom is NOT NULL then
         L_ss_exp_weight := I_ship_weight;
         L_ss_exp_weight_uom := I_weight_uom;
      end if;
   end if;

   if I_pack_ind = 'N' then
   if(I_unexpected_flag = FALSE) then
      if BOL_SQL.UPD_FROM_ITEM_LOC(O_error_message,
                                   I_item,
                                   'N',
                                   I_sellable_ind,
                                   I_item_xform_ind,
                                   NULL,  -- inventory ind
                                   I_from_loc,
                                   I_qty,
                                   L_resv_exp_qty,
                                   NULL,  -- weight
                                   NULL,  -- weight uom
                                   I_tran_date,
                                   I_eow_date) = FALSE then
         return FALSE;
      end if;
   end if;
   if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                 L_from_wac,
                                 I_item,
                                 I_dept,
                                 I_class,
                                 I_subclass,
                                 I_from_loc,
                                 I_from_loc_type,
                                 I_tran_date) = FALSE then
        return FALSE;
   end if;
      if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                     L_total_chrgs_prim,
                                                     L_profit_chrgs_to_loc,
                                                     L_exp_chrgs_to_loc,
                                                     'A',                    --allocation
                                                     I_alloc_no,
                                                     NULL,                   --tsf_seq_no
                                                     NULL,                   --ship_no
                                                     NULL,                   --ship_seq_no,
                                                     I_item,
                                                     NULL,                   --pack_item
                                                     I_from_loc,
                                                     I_from_loc_type,
                                                     I_to_loc,
                                                     I_to_loc_type) = FALSE then
         return FALSE;
      end if;

      if I_from_loc_type = 'W' then
         if BOL_SQL.WRITE_ISSUES(O_error_message,
                                 I_item,
                                 I_from_loc,
                                 I_qty,
                                 I_eow_date,
                                 I_dept,
                                 I_class,
                                 I_subclass) = FALSE then
             return FALSE;
         end if;
      end if;

      if(I_unexpected_flag = FALSE) then
         if LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then
            if STKLEDGR_SQL.WF_WRITE_FINANCIALS(O_error_message,
                                                L_alloc_unit_cost,   --used to update shipsku.unit_cost
                                                L_from_unit_retail,  --used to update shipsku.unit_retail
                                                I_alloc_no,
                                                'A',
                                                I_tran_date,
                                                I_item,
                                                NULL,  --pack_no
                                                NULL,  --pct_in_pack
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_qty,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_to_loc,
                                                I_to_loc_type,
                                                NULL,
                                                NULL) = FALSE then      --weight_cuom
               return FALSE;
            end if;
         elsif LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_NONE then
            if STKLEDGR_SQL.WRITE_FINANCIALS(O_error_message,
                                             L_alloc_unit_cost,  --used to update shipsku.unit_cost
                                             'A',
                                             I_ship_no,   --shipment
                                             I_alloc_no,
                                             I_tran_date, --tran date
                                             I_item,
                                             NULL,        --pack_no
                                             NULL,        --pct_in_pack
                                             I_dept,
                                             I_class,
                                             I_subclass,
                                             I_qty,
                                             NULL,        --weight_cuom
                                             I_from_loc,
                                             I_from_loc_type,
                                             'N',         --from finsiher (alloc won't have finishing)
                                             I_to_loc,
                                             I_to_loc_type,
                                             'N',         --to finisher
                                             NULL,        -- pass in NULL as the WAC will be calculated in the write_financials function
                                             L_profit_chrgs_to_loc,
                                             L_exp_chrgs_to_loc,
                                             L_intercompany,
                                             I_extended_base_cost,
                                             I_base_cost) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;

      if BOL_SQL.UPD_TO_ITEM_LOC(O_error_message,
                                 I_item,
                                 NULL,  --pack_no
                                 NULL,  --percent in pack
                                 'E',   --receive_as_type
                                 'A',   --transfer_type
                                 I_to_loc,
                                 I_to_loc_type,
                                 I_qty,
                                 L_resv_exp_qty,
                                 L_intran_qty,
                                 NULL,   -- weight_cuom
                                 NULL,   -- cuom
                                 I_from_loc,
                                 I_from_loc_type,
                                 L_total_chrgs_prim,
                                 I_alloc_no,
                                 'A',
                                 L_intercompany) = FALSE then
         return FALSE;
      end if;

      if UPDATE_SNAPSHOT_SQL.EXECUTE(O_error_message,
                                     'TSFO',
                                     I_item,
                                     'N',
                                     L_to_store,
                                     L_to_wh,
                                     L_from_store,
                                     L_from_wh,
                                     I_tran_date,
                                     LP_vdate,
                                     I_qty) = FALSE then
         return FALSE;
      end if;

    -- Convert L_from_wac into to_loc currency
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       I_from_loc,
                                       I_from_loc_type,
                                       NULL,
                                       I_to_loc,
                                       I_to_loc_type,
                                       NULL,
                                       L_from_wac,--L_from_wac
                                       L_from_wac_to_loc_curr,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
            return FALSE;
   end if;

   --convert chrg from primary to to_loc's currency
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       NULL,
                                       NULL,
                                       NULL,
                                       I_to_loc,
                                       I_to_loc_type,
                                       NULL,
                                       L_total_chrgs_prim,
                                       L_charge_to_loc,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
              return FALSE;
   end if;

   -- Update snapshot cost for the to_loc
   if UPDATE_SNAPSHOT_SQL.UPDATE_SNAPSHOT_COST
                              (O_error_message,
                               I_item,
                               I_to_loc_type,
                               I_to_loc,
                               I_tran_date,
                               I_qty,
                               (L_from_wac_to_loc_curr + L_charge_to_loc)
                               ) = FALSE then
              return FALSE;
   end if;




      --L_from_unit_retail is not returned from WRITE_FINANCIALS. Retrieve it before writing the shipsku retail,
      --For an orderable BTS item, unit_retail is also not defined, in which case, get its sellable's retail.
      if L_from_unit_retail is NULL then
         if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                     I_item,
                                                     I_from_loc,
                                                     I_from_loc_type,
                                                     'Y', -- nonsellable_pack_retail_ind
                                                     L_from_loc_av_cost,
                                                     L_from_loc_cost,
                                                     L_from_unit_retail,  -- used to update shipsku.unit_retail for non-franchise transaction
                                                     L_from_selling_unit_retail,
                                                     L_from_selling_uom) = FALSE then
            return FALSE;
         end if;
      end if;

      L_ss_prim_chrgs := L_total_chrgs_prim;
      L_ss_cost       := NVL(L_alloc_unit_cost, NVL(L_from_loc_av_cost,0));
      -- For a sellable/non-orderable/inventory item, if non-transformed and non-pack,
      -- unit_retail won't be defined, default to 0 for shipsku insert.
      L_ss_retail     := NVL(L_from_unit_retail, 0);

   else -- item is a pack

      L_pack_no := I_item;

      -- Get the pack's unit retail for shipsku.unit_retail update for non-franchise transaction
      -- For a non-sellable pack, retrieve pack's unit retail based on components.
      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  L_pack_no,
                                                  I_from_loc,
                                                  I_from_loc_type,
                                                  'Y', -- nonsellable_pack_retail_ind
                                                  L_pack_loc_av_cost,
                                                  L_pack_loc_cost,
                                                  L_pack_loc_retail,
                                                  L_pack_selling_unit_retail,
                                                  L_pack_selling_uom) = FALSE then
         return FALSE;
      end if;

      -- for a simple pack catch weight item, get weight of I_qty.
      if I_simple_pack_ind = 'Y' and I_catch_weight_ind = 'Y' then
         if CATCH_WEIGHT_SQL.PRORATE_WEIGHT(O_error_message,
                                            L_weight_cuom,
                                            L_cuom,
                                            I_item, -- pack no
                                            I_from_loc,
                                            I_from_loc_type,
                                            I_weight,
                                            I_weight_uom,
                                            I_qty,
                                            I_qty) = FALSE then
            return FALSE;
         end if;

         -- write weight derived from average weight to shipsku
         if I_weight is NULL or I_weight_uom is NULL then
            L_ss_exp_weight := L_weight_cuom/I_qty * I_ship_qty;
            L_ss_exp_weight_uom := L_cuom;
         end if;
      end if;

      open C_PACK_RCV_AS_TYPE;
      fetch C_PACK_RCV_AS_TYPE into L_pack_receive_as_type;
      close C_PACK_RCV_AS_TYPE;
      if L_pack_receive_as_type is NULL then
         L_pack_receive_as_type := 'E';
      end if;

      if BOL_SQL.UPDATE_PACK_LOCS(O_error_message,
                                  L_pack_no,
                                  I_from_loc,
                                  I_from_loc_type,
                                  I_to_loc,
                                  I_to_loc_type,
                                  L_pack_receive_as_type,
                                  'A',            -- tsf_type
                                  I_qty,
                                  L_weight_cuom,
                                  I_qty,          -- intran qty
                                  L_weight_cuom,  -- intran weight
                                  L_resv_exp_qty,
                                  I_tran_date)= FALSE then
         return FALSE;
      end if;

      open C_FROM_RCV_AS_TYPE;
      fetch C_FROM_RCV_AS_TYPE into L_from_receive_as_type;
      close C_FROM_RCV_AS_TYPE;

      if UPDATE_SNAPSHOT_SQL.EXECUTE(O_error_message,
                                     'TSFO',
                                     L_pack_no,
                                     'P',
                                     L_to_store,
                                     L_to_wh,
                                     L_from_store,
                                     L_from_wh,
                                     I_tran_date,
                                     LP_vdate,
                                     I_qty,
                                     L_from_receive_as_type) = FALSE then
            return FALSE;

      end if;


      if I_pack_type != 'B' then
         if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                        L_pack_total_chrgs_prim,
                                                        L_pack_profit_chrgs_to_loc,
                                                        L_pack_exp_chrgs_to_loc,
                                                        'A',                    --allocation
                                                        I_alloc_no,
                                                        NULL,                   --tsf_seq_no
                                                        NULL,                   --ship_no
                                                        NULL,                   --ship_seq_no,
                                                        I_item,
                                                        NULL,                   --pack_item
                                                        I_from_loc,
                                                        I_from_loc_type,
                                                        I_to_loc,
                                                        I_to_loc_type) = FALSE then
            return FALSE;
         end if;

         L_ss_prim_chrgs := L_pack_total_chrgs_prim;
      end if;

      FOR rec in C_ITEM_IN_PACK LOOP

         if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                          L_pct_in_pack,
                                          I_item,
                                          rec.item,
                                          I_from_loc) = FALSE then
            return FALSE;
         end if;

         if BOL_SQL.UPD_FROM_ITEM_LOC(O_error_message,
                                      rec.item,
                                      'Y',
                                      rec.sellable_ind,
                                      rec.item_xform_ind,
                                      rec.inventory_ind,
                                      I_from_loc,
                                      I_qty * rec.qty,
                                      L_resv_exp_qty * rec.qty,
                                      L_weight_cuom,
                                      L_cuom,
                                      I_tran_date,
                                      I_eow_date) = FALSE then
            return FALSE;
         end if;

          if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                        L_from_wac,
                                        rec.item,
                                        rec.dept,
                                        rec.class,
                                        rec.subclass,
                                        I_from_loc,
                                        I_from_loc_type,
                                        I_tran_date) = FALSE then
                       return FALSE;
          end if;


         if I_pack_type != 'B' then
            --prorate the charges calculated at the pack level across the comp items
            --******************************************************************************
            -- Value returned in L_pack_profit_chrgs_to_loc, L_pack_exp_chrgs_to_loc, and
            -- L_pack_total_chrgs_prim are unit values for the entire pack.  Need to take
            -- a proportionate piece of the value for each component item in the pack
            -- The formula for this is:
            --       [Pack Value * (Comp Item Avg Cost * Comp Qty in the Pack) /
            --                     (Total Pack Avg Cost)] /
            --       Comp Qty in the Pack
            -- You must divide the value by the Component Item Qty in the pack because the
            -- value will be for one pack.  In order to get a true unit value you need to
            -- do the last division.  Since we multiple by Comp Qty and then divide by it,
            -- it can be removed from the calculation completely.
            -- L_pct_in_pack is the component av_cost / total pack av cost value
            --******************************************************************************
            L_profit_chrgs_to_loc := L_pack_profit_chrgs_to_loc * L_pct_in_pack;
            L_exp_chrgs_to_loc    := L_pack_exp_chrgs_to_loc    * L_pct_in_pack;
            L_total_chrgs_prim    := L_pack_total_chrgs_prim    * L_pct_in_pack;

         else
            --look up the charges at the comp level
            if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                           L_total_chrgs_prim,
                                                           L_profit_chrgs_to_loc,
                                                           L_exp_chrgs_to_loc,
                                                           'A',                    --allocation
                                                           I_alloc_no,
                                                           NULL,                   --tsf_seq_no
                                                           NULL,                   --ship_no
                                                           NULL,                   --ship_seq_no,
                                                           rec.item,               --item
                                                           I_item,                 --pack_no
                                                           I_from_loc,
                                                           I_from_loc_type,
                                                           I_to_loc,
                                                           I_to_loc_type) = FALSE then
               return FALSE;
            end if;

            L_ss_prim_chrgs := L_ss_prim_chrgs + (L_total_chrgs_prim * rec.qty);
         end if;

         if I_from_loc_type = 'W' then
            if BOL_SQL.WRITE_ISSUES(O_error_message,
                                    rec.item,
                                    I_from_loc,
                                    I_qty * rec.qty,
                                    I_eow_date,
                                    I_dept,
                                    I_class,
                                    I_subclass) = FALSE then
               return FALSE;
            end if;
         end if;

         L_extended_base_cost  := I_extended_base_cost * L_pct_in_pack;
         L_base_cost           := I_base_cost * L_pct_in_pack;

         if LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then
            if STKLEDGR_SQL.WF_WRITE_FINANCIALS(O_error_message,
                                                L_alloc_unit_cost,
                                                L_from_unit_retail,
                                                I_alloc_no,
                                                'A',
                                                I_tran_date,
                                                rec.item,
                                                L_pack_no,  --pack_no
                                                L_pct_in_pack,  --pct_in_pack
                                                I_dept,
                                                I_class,
                                                I_subclass,
                                                I_qty * rec.qty,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_to_loc,
                                                I_to_loc_type,
                                                NULL,
                                                NULL) = FALSE then  --weight_cuom
               return FALSE;
            end if;
            -- for franchise transactions, sum up component's unit_retail for shipsku.unit_retail
            L_ss_retail := L_ss_retail + (NVL(L_from_unit_retail,0) * rec.qty);

         elsif LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_NONE then
            if STKLEDGR_SQL.WRITE_FINANCIALS(O_error_message,
                                             L_alloc_unit_cost,
                                             'A',
                                             I_ship_no,   --shipment
                                             I_alloc_no,
                                             I_tran_date, --tran date
                                             rec.item,
                                             I_item,      --pack_no
                                             NULL,        --pct_in_pack
                                             rec.dept,
                                             rec.class,
                                             rec.subclass,
                                             I_qty * rec.qty,
                                             L_weight_cuom,
                                             I_from_loc,
                                             I_from_loc_type,
                                             'N',         --from finsiher (alloc won't have finishing)
                                             I_to_loc,
                                             I_to_loc_type,
                                             'N',         --to finisher
                                             NULL,        -- pass in NULL as the WAC will be calculated in the write_financials function
                                             L_profit_chrgs_to_loc,
                                             L_exp_chrgs_to_loc,
                                             L_intercompany,
                                             L_extended_base_cost,
                                             L_base_cost) = FALSE then
               return FALSE;
            end if;
         end if;

         if rec.inventory_ind = 'Y' then
            if BOL_SQL.UPD_TO_ITEM_LOC(O_error_message,
                                       rec.item,
                                       I_item,  --pack_no
                                       L_pct_in_pack,
                                       L_pack_receive_as_type,
                                       'A', --transfer_type
                                       I_to_loc,
                                       I_to_loc_type,
                                       I_qty * rec.qty,
                                       L_resv_exp_qty * rec.qty,
                                       L_intran_qty * rec.qty,
                                       L_weight_cuom,
                                       L_cuom,
                                       I_from_loc,
                                       I_from_loc_type,
                                       L_total_chrgs_prim,
                                       I_alloc_no,
                                       'A',
                                       L_intercompany) = FALSE then
               return FALSE;
            end if;
         end if;

         if UPDATE_SNAPSHOT_SQL.EXECUTE(O_error_message,
                                        'TSFO',
                                        rec.item,
                                        'C',
                                        L_to_store,
                                        L_to_wh,
                                        L_from_store,
                                        L_from_wh,
                                        I_tran_date,
                                        LP_vdate,
                                        I_qty * rec.qty,
                                        L_from_receive_as_type) = FALSE then
               return FALSE;

         end if;

           -- Convert L_from_wac into to_loc currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             L_from_wac,--L_from_wac,
                                             L_from_wac_to_loc_curr,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
                 return FALSE;
         end if;

          --convert chrg from primary to to_loc's currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             L_total_chrgs_prim,
                                             L_charge_to_loc,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
                return FALSE;
         end if;

          -- Update snapshot cost for the to_loc
         if UPDATE_SNAPSHOT_SQL.UPDATE_SNAPSHOT_COST
                                     (O_error_message,
                                      rec.item,
                                      I_to_loc_type,
                                      I_to_loc,
                                      I_tran_date,
                                      I_qty * rec.qty,
                                      (L_from_wac_to_loc_curr + L_charge_to_loc)
                                      ) = FALSE then
                 return FALSE;
         end if;



         -- shipsku.unit_cost for pack should be based on component's wac
         L_ss_cost := L_ss_cost + (NVL(L_alloc_unit_cost,0) * rec.qty);
      END LOOP;

      --for non-franchise transactions, use pack's unit_retail for shipsku.unit_retail
      if LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_NONE then
         L_ss_retail := NVL(L_pack_loc_retail, 0);
      end if;

   end if; -- item type

   if LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then
      if WF_TRANSFER_SQL.GET_WF_ORDER_RMA_NO(O_error_message,
                                             L_wf_order_no,
                                             L_rma_no,    --dummy
                                             'A',         --distro_type
                                             I_alloc_no,  --distro_no
                                             I_to_loc,
                                             I_to_loc_type) = FALSE then
         return FALSE;
      end if;

      if WF_BOL_SQL.WRITE_WF_BILLING_SALES(O_error_message,
                                           L_wf_order_no,
                                           I_item,
                                           I_from_loc,
                                           I_from_loc_type,
                                           I_to_loc,
                                           NULL,
                                           NULL,
                                           I_qty,
                                           I_tran_date) = FALSE then
         return FALSE;
      end if;
   end if;

   L_ss_prim_chrgs := L_ss_prim_chrgs;

   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       NULL,
                                       NULL,
                                       NULL,
                                       I_from_loc,
                                       I_from_loc_type,
                                       NULL,
                                       L_ss_prim_chrgs,
                                       L_ss_from_chrgs,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
      return FALSE;
   end if;
   ---
   if GP_ship_seq_no is NULL then
      GP_ship_seq_no := I_ss_seq_no;
   end if;
   ---
   -- LP_receipt_ind is set for when the BOL process is being called from the receiving process.
   --
   if LP_receipt_ind = 'Y' then
      L_ss_rcv_qty := I_ship_qty;
      L_ss_qty     := 0;
   else
      L_ss_rcv_qty := 0;
      L_ss_qty     := I_ship_qty;
   end if;

   if BOL_SQL.INS_SHIPSKU(O_error_message,
                           I_ship_no,
                           GP_ship_seq_no,
                           I_item,
                           I_ref_item,
                           I_alloc_no,
                           'A',
                           I_carton,
                           I_inv_status,
                           L_ss_rcv_qty,
                           L_ss_cost + L_ss_from_chrgs,
                           L_ss_retail,
                           L_ss_qty,
                           L_ss_exp_weight,
                           L_ss_exp_weight_uom) = FALSE then
      return FALSE;
   end if;

   GP_ship_seq_no := NULL;

   --perform store end auto-receiving functionality if needed
   L_auto_rcv_ind := 'N';

   if I_to_loc_type = 'S' then
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exists,
                                  L_store_row,
                                  I_to_loc) = FALSE then
         return FALSE;
      end if;

      if L_exists = TRUE then
         L_store_auto_rcv_ind := L_store_row.auto_rcv;
      end if;

      if (I_to_loc_type = 'S' and L_store_auto_rcv_ind = 'Y') or
         (I_to_loc_type = 'S' and L_store_auto_rcv_ind = 'D' and LP_system_options.auto_rcv_store = 'Y') then
         ---
         --Auto receive an allocation to STORE if conditions are met:
         --1) to-loc store's auto_rcv flag is 'Y', or
         --2) to-loc store's auto_rcv flag is 'D' for depending on system option auto_rcv_store setting, or
         L_auto_rcv_ind := 'Y';
         ---
      end if;
   end if;

   if L_auto_rcv_ind = 'Y' then
      if AUTO_RCV_LINE_ITEM(O_error_message) = FALSE then
         return FALSE;
      end if;

      LP_receipt_ind := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.SEND_ALLOC',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SEND_ALLOC;
-------------------------------------------------------------------------------
--public function--
FUNCTION PUT_TSF(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_tsfhead_to_loc   IN OUT TSFHEAD.TO_LOC%TYPE,
                 O_tsfhead_from_loc IN OUT ITEM_LOC.LOC%TYPE,
                 O_tsf_type         IN OUT TSFHEAD.TSF_TYPE%TYPE,
                 I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE,
                 I_phy_from_loc     IN     SHIPMENT.FROM_LOC%TYPE,
                 I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                 I_phy_to_loc       IN     SHIPMENT.TO_LOC%TYPE,
                 I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                 I_tran_date        IN     PERIOD.VDATE%TYPE,
                 I_comment_desc     IN     TSFHEAD.COMMENT_DESC%TYPE,
                 I_unavailable_flag IN     VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_exist               VARCHAR2(1)           := 'n';

   L_status              TSFHEAD.STATUS%TYPE   := NULL;
   L_vir_loc             ITEM_LOC.LOC%TYPE     := NULL;
   L_phy_loc             ITEM_LOC.LOC%TYPE     := NULL;
   L_to_loc              ITEM_LOC.LOC%TYPE     := NULL;
   L_from_loc            ITEM_LOC.LOC%TYPE     := NULL;

   dist_cnt              BINARY_INTEGER        := NULL;
   L_reopen              VARCHAR2(1)           := 'N';
   L_rowid               ROWID;

   L_table               VARCHAR2(30);
   L_key1                VARCHAR2(100);
   L_key2                VARCHAR2(100);
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   -- cursors
   cursor C_TSF is
      select th.status,
             th.tsf_type,
             th.from_loc,
             th.to_loc,
             th.rowid
        from tsfhead th
       where th.tsf_no = I_tsf_no
         for update nowait;

   cursor C_PHY_WH is
      select 'x'
        from wh w
       where w.physical_wh = L_phy_loc
         and w.wh          = L_vir_loc;

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_tsf_no','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_phy_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_phy_from_loc','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_from_loc_type','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_phy_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_phy_to_loc','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_to_loc_type','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_tran_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_tran_date','NULL','NOT NULL');
      return FALSE;
   end if;

   L_table := 'TSFHEAD';
   L_key1 := TO_CHAR(I_tsf_no);
   L_key2 := NULL;

   open C_TSF;
   fetch C_TSF into L_status,
                    O_tsf_type,
                    L_from_loc,
                    L_to_loc,
                    L_rowid;
   close C_TSF;

   /* if the passed in tsf not on table */
   if L_to_loc IS NULL then
      L_status := 'S';
      O_tsf_type := 'EG';

      if CREATE_TSF(O_error_message,
                    I_tsf_no,
                    O_tsf_type,
                    I_phy_from_loc,
                    I_from_loc_type,
                    I_phy_to_loc,
                    I_to_loc_type,
                    I_tran_date,
                    I_comment_desc,
                    I_unavailable_flag) = FALSE then
         return FALSE;
      end if;

   O_tsfhead_to_loc := I_phy_to_loc;
   O_tsfhead_from_loc := I_phy_from_loc;

   else -- the transfer exists

      -- either the locations must match (store or MC is off) or the
      -- loc on the tsf must exist in the passed phy loc

      -- verify locations match
      if L_from_loc != I_phy_from_loc then

         L_exist := 'n';
         L_phy_loc := I_phy_from_loc;
         L_vir_loc := L_from_loc;

         open C_PHY_WH;
         fetch C_PHY_WH into L_exist;
         close C_PHY_WH;
         if L_exist = 'n' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_FROM_LOC', TO_CHAR(I_phy_from_loc),TO_CHAR(I_tsf_no), NULL);
            return FALSE;
         end if;

      end if;

      -- verify locations match
      if L_to_loc != I_phy_to_loc then

         L_exist := 'n';
         L_phy_loc := I_phy_to_loc;
         L_vir_loc := L_to_loc;

         open C_PHY_WH;
         fetch C_PHY_WH into L_exist;
         close C_PHY_WH;

         if L_exist = 'n' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_TO_LOC', TO_CHAR(I_phy_to_loc), TO_CHAR(I_tsf_no), NULL);
            return FALSE;
         end if;

      end if;

      update tsfhead th
        set th.status = 'S'
       where th.rowid = L_rowid;


   /*If shipping a closed 'EG' or any other transfer, reopen the transfer*/
      if L_status = 'C' then
         L_status := 'S';
         L_reopen := 'Y';
      end if;


      if O_tsf_type != 'EG' or
         I_from_loc_type = 'S' and I_to_loc_type = 'S' then
         O_tsfhead_to_loc := L_to_loc;
         O_tsfhead_from_loc := L_from_loc;
      end if;

   end if;

   -- reset the distro array
   LP_bol_rec.distros.DELETE;
   dist_cnt := 1;

   LP_bol_rec.distros(dist_cnt).tsf_no := I_tsf_no;
   LP_bol_rec.distros(dist_cnt).tsf_status := L_status;
   LP_bol_rec.distros(dist_cnt).tsf_type := O_tsf_type;
   if L_to_loc IS NULL or L_reopen = 'Y' then
      LP_bol_rec.distros(dist_cnt).new_tsf_ind := 'Y';
   else
      LP_bol_rec.distros(dist_cnt).new_tsf_ind := 'N';
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.PUT_TSF',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PUT_TSF;
-------------------------------------------------------------------------------
FUNCTION CREATE_TSF(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_tsf_no            IN     TSFHEAD.TSF_NO%TYPE,
                    I_tsf_type          IN     TSFHEAD.TSF_TYPE%TYPE,
                    I_phy_from_loc      IN     ITEM_LOC.LOC%TYPE,
                    I_from_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                    I_phy_to_loc        IN     ITEM_LOC.LOC%TYPE,
                    I_to_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                    I_tran_date         IN     PERIOD.VDATE%TYPE,
                    I_comment_desc      IN     TSFHEAD.COMMENT_DESC%TYPE,
                    I_unavailable_flag  IN     VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_zone              VARCHAR2(1) := 'n';
   L_pgm               IF_ERRORS.PROGRAM_NAME%TYPE := 'BOL_SQL.CREATE_TSF';
   L_luw               IF_ERRORS.UNIT_OF_WORK%TYPE := NULL;

   L_status_code       VARCHAR2(1) := NULL;
   L_max_details       NUMBER      := 0;
   L_num_threads       NUMBER      := 0;
   L_min_time_lag      NUMBER      := 0;
   L_l10n_obj          L10n_obj    := L10n_obj();

  -- cursors
   cursor C_ZONE_CHECK is
      select 'y'
        from store a,
             store b
       where a.store         = I_phy_from_loc
         and b.store         = I_phy_to_loc
         and NVL(NVL(a.transfer_zone, b.transfer_zone),0) = NVL(NVL(b.transfer_zone, a.transfer_zone),0);

BEGIN

   if I_from_loc_type = 'S' and I_to_loc_type = 'S' then
      open C_ZONE_CHECK;
      fetch C_ZONE_CHECK into L_zone;
      close C_ZONE_CHECK;
      if L_zone != 'y' then
         O_error_message := SQL_LIB.CREATE_MSG('EG_TSF_ZONE', TO_CHAR(I_tsf_no), NULL, NULL);
         return FALSE;
      end if;
   end if;

   insert into tsfhead(tsf_no,
                       from_loc_type,
                       from_loc,
                       to_loc_type,
                       to_loc,
                       dept,
                       inventory_type,
                       tsf_type,
                       status,
                       freight_code,
                       routing_code,
                       create_date,
                       create_id,
                       approval_date,
                       approval_id,
                       delivery_date,
                       close_date,
                       ext_ref_no,
                       repl_tsf_approve_ind,
                       comment_desc,
                       wf_order_no,
                       rma_no)
                values(I_tsf_no,
                       I_from_loc_type,
                       I_phy_from_loc,
                       I_to_loc_type,
                       I_phy_to_loc,
                       NULL,          --dept--this will be updated later as needed
                       decode(I_unavailable_flag,'Y','U','A'),           
                       I_tsf_type,
                       'S',
                       'N',
                       NULL,          --rounting_code
                       I_tran_date,
                       'EXTERNAL',
                       I_tran_date,
                       'EXTERNAL',
                       NULL,          --delivery_date
                       NULL,          --close_date
                       NULL,          --ext_ref_no
                       'N',
                       I_comment_desc, --comment_desc
                       NULL,
                       NULL);

   L_l10n_obj.procedure_key := 'CREATE_TRAN_UTIL_CODE';
   L_l10n_obj.doc_type      := 'TSF';
   L_l10n_obj.doc_id        := I_tsf_no;
   L_l10n_obj.source_entity := 'LOC';
   L_l10n_obj.source_id     := I_phy_from_loc;

   --- get the default utilization code for transfer created.
   if L10N_SQL.EXEC_FUNCTION (O_error_message,
                              L_l10n_obj) = FALSE then
         return FALSE;
   end if;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_TRANSFERS.FAMILY);

   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   insert into transfers_pub_info (TSF_NO,
                                   TSF_TYPE,
                                   INITIAL_APPROVAL_IND,
                                   THREAD_NO,
                                   PHYSICAL_FROM_LOC,
                                   FROM_LOC,
                                   FROM_LOC_TYPE,
                                   PHYSICAL_TO_LOC,
                                   TO_LOC,
                                   TO_LOC_TYPE,
                                   FREIGHT_CODE,
                                   PUBLISHED)
                            values(I_tsf_no,
                                   I_tsf_type,
                                   'Y',
                                   L_num_threads,
                                   I_phy_from_loc,
                                   I_phy_from_loc,
                                   I_from_loc_type,
                                   I_phy_to_loc,
                                   I_phy_to_loc,
                                   I_to_loc_type,
                                   null,
                                   'Y');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_pgm,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_TSF;
-------------------------------------------------------------------------------
--public function--
FUNCTION PUT_TSF_ITEM(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_detail_created       IN OUT   BOOLEAN,
                      I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE,
                      I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                      I_carton               IN       SHIPSKU.CARTON%TYPE,
                      I_qty                  IN       TSFDETAIL.TSF_QTY%TYPE,
                      I_weight               IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                      I_weight_uom           IN       UOM_CLASS.UOM%TYPE,
                      I_inv_status           IN       INV_STATUS_CODES.INV_STATUS%TYPE,
                      I_phy_from_loc         IN       ITEM_LOC.LOC%TYPE,
                      I_from_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                      I_phy_to_loc           IN       ITEM_LOC.LOC%TYPE,
                      I_to_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                      I_tsfhead_to_loc       IN       ITEM_LOC.LOC%TYPE,
                      I_tsfhead_from_loc     IN       ITEM_LOC.LOC%TYPE,
                      I_tsf_type             IN       TSFHEAD.TSF_TYPE%TYPE,
                      I_called_from_form     IN       VARCHAR2 default 'N',
                      I_extended_base_cost   IN       ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE default NULL,
                      I_base_cost            IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE default NULL)
RETURN BOOLEAN IS

   L_tsf_qty            TSFDETAIL.TSF_QTY%TYPE           := NULL;
   L_ship_qty           TSFDETAIL.SHIP_QTY%TYPE          := NULL;
   L_tsf_seq_no         TSFDETAIL.TSF_SEQ_NO%TYPE        := NULL;
   L_distro_qty         TSFDETAIL.TSF_QTY%TYPE           := NULL;
   L_tsf_distro_qty     TSFDETAIL.DISTRO_QTY%TYPE        := NULL;
   L_upd_tsf_qty        TSFDETAIL.TSF_QTY%TYPE           := NULL;
   dist_cnt             BINARY_INTEGER := 1;
   item_cnt             BINARY_INTEGER := 1;
   L_item               ITEM_MASTER.ITEM%TYPE                 := NULL;
   L_ref_item           ITEM_MASTER.ITEM%TYPE                 := NULL;
   L_pack_ind           ITEM_MASTER.PACK_IND%TYPE             := NULL;
   L_pack_type          ITEM_MASTER.PACK_TYPE%TYPE            := NULL;
   L_simple_pack_ind    ITEM_MASTER.SIMPLE_PACK_IND%TYPE      := NULL;
   L_catch_weight_ind   ITEM_MASTER.CATCH_WEIGHT_IND%TYPE     := NULL;
   L_sellable_ind       ITEM_MASTER.SELLABLE_IND%TYPE         := NULL;
   L_item_xform_ind     ITEM_MASTER.ITEM_XFORM_IND%TYPE       := NULL;
   L_supp_pack_size     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NULL;
   L_dept               ITEM_MASTER.DEPT%TYPE                 := NULL;
   L_class              ITEM_MASTER.CLASS%TYPE                := NULL;
   L_subclass           ITEM_MASTER.SUBCLASS%TYPE             := NULL;
   L_ss_seq_no          SHIPSKU.SEQ_NO%TYPE                   := NULL;
   L_vir_to_loc         ITEM_LOC.LOC%TYPE;
   L_pwh_ind            VARCHAR2(1)                           := 'N';
   L_rowid              ROWID;
   L_table              VARCHAR2(30);
   L_key1               VARCHAR2(100);
   L_key2               VARCHAR2(100);

   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   -- cursors
   cursor C_TSF_QTYS is
      select NVL(td.tsf_qty,0),
             NVL(td.ship_qty,0),
             td.tsf_seq_no,
             td.distro_qty,
             td.rowid
        from tsfdetail td
       where td.tsf_no  = I_tsf_no
         and td.item = LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item
         for update nowait;

   cursor C_TO_LOC is
      select s.store
        from store s
       where s.store = I_phy_to_loc
       union
      select w.wh
        from wh w
       where w.wh != w.physical_wh
         and w.physical_wh = I_phy_to_loc
       union
      select to_number(partner_id)
        from partner
       where partner_id = to_char(I_phy_to_loc)
         and partner_type = 'E';

BEGIN
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_tsf_no','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_item','NULL','NOT NULL');
      return FALSE;
   end if;
   if I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_qty','NULL','NOT NULL');
      return FALSE;
   elsif I_qty <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('QTY_GREATER_ZERO',
                                            ' Transfer : '||I_tsf_no,
                                            ' Item : ' || I_item,
                                            ' Quantity : '||I_qty);
      return FALSE;
   end if;
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;

   dist_cnt := LP_bol_rec.distros.COUNT;
   item_cnt := LP_bol_rec.distros(dist_cnt).bol_items.COUNT;
   item_cnt := item_cnt + 1;

   if BOL_SQL.ITEM_CHECK(O_error_message,
                         L_item,
                         L_ref_item,
                         L_dept,
                         L_class,
                         L_subclass,
                         L_pack_ind,
                         L_pack_type,
                         L_simple_pack_ind,
                         L_catch_weight_ind,
                         L_sellable_ind,
                         L_item_xform_ind,
                         L_supp_pack_size,

                         I_item) = FALSE then
      return FALSE;
   end if;

   open C_TO_LOC;
   fetch C_TO_LOC into L_vir_to_loc;
   close C_TO_LOC;

   if L_vir_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC');
      return FALSE;
   end if;

   if NEXT_SS_SEQ_NO(O_error_message,
                     L_ss_seq_no) = FALSE then
      return FALSE;
   end if;

   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item               := L_item;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ref_item           := L_ref_item;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).dept               := L_dept;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).class              := L_class;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).subclass           := L_subclass;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_ind           := L_pack_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_type          := L_pack_type;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).simple_pack_ind    := L_simple_pack_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).catch_weight_ind   := L_catch_weight_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).sellable_ind       := L_sellable_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item_xform_ind     := L_item_xform_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).supp_pack_size     := L_supp_pack_size;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ss_seq_no          := L_ss_seq_no;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_to_loc_phy      := I_phy_to_loc;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_to_loc_vir      := L_vir_to_loc;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).extended_base_cost := I_extended_base_cost;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).base_cost          := I_base_cost;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).inv_status         := I_inv_status;

   L_table := 'TSFDETAIL';
   L_key1 := TO_CHAR(I_tsf_no);
   L_key2 := TO_CHAR(L_tsf_seq_no);

   open C_TSF_QTYS;
   fetch C_TSF_QTYS into L_tsf_qty,
                         L_ship_qty,
                         L_tsf_seq_no,
                         L_tsf_distro_qty,
                         L_rowid;
   close C_TSF_QTYS;

   if L_tsf_qty IS NULL then
      --this item is not on the transfer

      if LP_bol_rec.distros(dist_cnt).tsf_status = 'C' then
         O_error_message := SQL_LIB.CREATE_MSG('CANNOT_ADD_NEW_ITEM',
                                               I_item,
                                               I_tsf_no,
                                               NULL);
         return FALSE;
      end if;

      L_tsf_qty := 0;
      L_ship_qty := 0;

      if INS_TSFDETAIL(O_error_message,
                       L_tsf_seq_no,
                       I_tsf_no,
                       LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item,
                       LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).supp_pack_size,
                       I_inv_status,

                       I_qty) = FALSE then
         return FALSE;
      end if;

      O_detail_created := TRUE;

      --check for physical wh in CO transfers
      if I_tsf_type = 'CO' then
         if I_from_loc_type = 'W' and I_tsfhead_from_loc = I_phy_from_loc then
            L_pwh_ind := 'Y';
         end if;
         if L_pwh_ind = 'N' then
            if I_to_loc_type = 'W' and I_tsfhead_to_loc = I_phy_to_loc then
               L_pwh_ind := 'Y';
            end if;
         end if;
      end if;

      --not a franchise order or return shipment
      --write tsf_price or tsf_cost depending on transfer type (inter vs intra).
      if I_tsf_type NOT in ('EG','CO') or                          --non-EG/CO transfer will NOT have physical wh
         I_tsf_type = 'CO' and L_pwh_ind = 'N' or                  --CO transfers having no physical wh
         I_from_loc_type != 'W' and I_to_loc_type != 'W' then      --transfers do NOT involve a warehouse
         if TRANSFER_CHARGE_SQL.DEFAULT_CHRGS(O_error_message,
                                              I_tsf_no,
                                              I_tsf_type,
                                              L_tsf_seq_no,
                                              NULL,              --ship_no     --only populated for tsfs with physical WH
                                              NULL,              --ship_seq_no --only populated for tsfs with physical WH
                                              I_tsfhead_from_loc,
                                              I_from_loc_type,
                                              I_tsfhead_to_loc,
                                              I_to_loc_type,
                                              LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item) = FALSE then
            return FALSE;
         end if;
      end if;
   else -- tsfdetail does exist
      if (I_tsf_type = 'EG') then
         if (L_tsf_distro_qty is NULL or L_tsf_distro_qty > 0) then
            L_distro_qty := I_qty;
         else
            L_distro_qty := 0;
         end if;
         L_upd_tsf_qty := I_qty;
      elsif (I_tsf_type != 'EG' and I_called_from_form = 'N') then
         L_distro_qty := I_qty;
         L_upd_tsf_qty := 0;
      else
         L_distro_qty := 0;
         L_upd_tsf_qty := 0;
      end if;

      update tsfdetail td
         set td.ship_qty     = NVL(td.ship_qty, 0) + I_qty,
              td.distro_qty  = DECODE(I_from_loc_type,
                                      'W', NVL(td.distro_qty, 0) - L_distro_qty,
                                      td.distro_qty),
             td.received_qty = NVL(td.received_qty, 0),
             td.updated_by_rms_ind = 'N'
       where td.rowid = L_rowid;

      O_detail_created := FALSE;
   end if;

   if TSF_ITEM_COMMON(O_error_message,
                      I_tsf_no,
                      I_carton,
                      I_qty,
                      I_weight,
                      I_weight_uom,
                      I_phy_from_loc,
                      I_from_loc_type,
                      I_phy_to_loc,
                      I_to_loc_type,
                      I_tsfhead_to_loc,
                      I_tsfhead_from_loc,
                      I_tsf_type,
                      I_inv_status,
                      L_tsf_qty,
                      L_ship_qty,
                      L_tsf_seq_no,
                      item_cnt,
                      dist_cnt) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.PUT_TSF_ITEM',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PUT_TSF_ITEM;
-------------------------------------------------------------------------------
FUNCTION NEW_LOC(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_item           IN     ITEM_MASTER.ITEM%TYPE,
                 I_pack_ind       IN     ITEM_MASTER.PACK_IND%TYPE,
                 I_dept           IN     ITEM_MASTER.DEPT%TYPE,
                 I_class          IN     ITEM_MASTER.CLASS%TYPE,
                 I_subclass       IN     ITEM_MASTER.SUBCLASS%TYPE,
                 I_loc            IN     ITEM_LOC.LOC%TYPE,
                 I_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

BEGIN
   if NEW_ITEM_LOC(O_error_message,
                   I_item,
                   I_loc,
                   NULL, NULL, I_loc_type, NULL,
                   I_dept,
                   I_class,
                   I_subclass,
                   NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL,
                   I_pack_ind,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.NEW_LOC',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END NEW_LOC;
-------------------------------------------------------------------------------
FUNCTION ITEM_CHECK(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_tran_item          IN OUT   ITEM_MASTER.ITEM%TYPE,
                    O_ref_item           IN OUT   ITEM_MASTER.ITEM%TYPE,
                    O_dept               IN OUT   ITEM_MASTER.DEPT%TYPE,
                    O_class              IN OUT   ITEM_MASTER.CLASS%TYPE,
                    O_subclass           IN OUT   ITEM_MASTER.SUBCLASS%TYPE,
                    O_pack_ind           IN OUT   ITEM_MASTER.PACK_IND%TYPE,
                    O_pack_type          IN OUT   ITEM_MASTER.PACK_TYPE%TYPE,
                    O_simple_pack_ind    IN OUT   ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                    O_catch_weight_ind   IN OUT   ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                    O_sellable_ind       IN OUT   ITEM_MASTER.SELLABLE_IND%TYPE,
                    O_item_xform_ind     IN OUT   ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                    O_supp_pack_size     IN OUT   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,

                    I_input_item         IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_sellable_ind    ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind   ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_inventory_ind   ITEM_MASTER.INVENTORY_IND%TYPE;

   -- cursors
   cursor C_ITEM_EXIST is
      select im1.item,
             im1.dept,
             im1.class,
             im1.subclass,
             im1.pack_ind,
             NVL(im1.pack_type, 'N'),
             im1.simple_pack_ind,
             im1.catch_weight_ind,
             im1.sellable_ind,
             im1.item_xform_ind,
             im1.orderable_ind,
             im1.inventory_ind
        from item_master im1,
             item_master im2
       where (im2.item       = I_input_item and
              im2.item_level = im2.tran_level and
              im1.item       = im2.item)
              -- if item is below the tran level,
              -- get its tran level parent
          or (im2.item       = I_input_item and
              im2.item_level = im2.tran_level + 1 and
              im1.item      = im2.item_parent);

BEGIN

   open C_ITEM_EXIST;
   fetch C_ITEM_EXIST into O_tran_item,
                           O_dept,
                           O_class,
                           O_subclass,
                           O_pack_ind,
                           O_pack_type,
                           O_simple_pack_ind,
                           O_catch_weight_ind,
                           O_sellable_ind,
                           O_item_xform_ind,
                           L_orderable_ind,
                           L_inventory_ind;

   if C_ITEM_EXIST%NOTFOUND then
      close C_ITEM_EXIST;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM', I_input_item, NULL, NULL);
      return FALSE;
   end if;
   close C_ITEM_EXIST;
   ---
   if ( L_orderable_ind = 'Y' and
        L_sellable_ind  = 'N' and
        L_inventory_ind = 'N' ) or L_inventory_ind = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_NONINVENT_ITEM', O_tran_item, NULL, NULL);
      return FALSE;
   end if;

   --for non-orderable packs(pack_type of NULL), use 1 as supp_pack_size
   if O_pack_ind = 'Y' and O_pack_type = 'N' then
      O_supp_pack_size := 1;
   else
      if SUPP_ITEM_ATTRIB_SQL.GET_SUPP_PACK_SIZE(O_error_message,
                                                 O_supp_pack_size,
                                                 O_tran_item,
                                                 NULL,
                                                 NULL) = FALSE then
         return FALSE;
      end if;
   end if;

   if O_tran_item != I_input_item then
      O_ref_item := I_input_item;
   else
      O_ref_item := NULL;
   end if;






   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.ITEM_CHECK',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_CHECK;
-------------------------------------------------------------------------------
FUNCTION INS_TSFDETAIL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tsf_seq_no            IN OUT TSFDETAIL.TSF_SEQ_NO%TYPE,
                       I_tsf_no                IN     TSFHEAD.TSF_NO%TYPE,
                       I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                       I_supp_pack_size        IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                       I_inv_status            IN     TSFDETAIL.INV_STATUS%TYPE,
                       I_tsf_qty               IN     TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS

   L_inv_status        TSFDETAIL.INV_STATUS%TYPE := NULL;

   -- cursors
   cursor C_MAX_SEQ is
      select NVL(MAX(td.tsf_seq_no), 0)
        from tsfdetail td
       where td.tsf_no = I_tsf_no;

BEGIN

   if I_inv_status != -1 then
      L_inv_status := I_inv_status;
   end if;

   O_tsf_seq_no := 0;
   open C_MAX_SEQ;
   fetch C_MAX_SEQ into O_tsf_seq_no;
   close C_MAX_SEQ;
   O_tsf_seq_no := O_tsf_seq_no + 1;

   --Since external transfers will have physical locations written to TSFHEAD,
   --and physical wh does not belong to a transfer entity, we cannot determine
   --if a transfer is inter or intracompany, and determine if tsf_price or
   --tsf_cost should be written. Therefore, we will NOT write TSF_COST and TSF_PRICE.
   insert into tsfdetail (tsf_no,
                          tsf_seq_no,
                          item,
                          inv_status,
                          tsf_price,
                          tsf_cost,
                          tsf_qty,
                          fill_qty,
                          distro_qty,
                          selected_qty,
                          cancelled_qty,
                          ship_qty,
                          received_qty,
                          supp_pack_size,
                          tsf_po_link_no,
                          mbr_processed_ind,
                          publish_ind,
                          updated_by_rms_ind)
                   values(I_tsf_no,
                          O_tsf_seq_no,
                          I_item,
                          L_inv_status,
                          NULL,      --tsf_price
                          NULL,      --tsf_cost
                          I_tsf_qty, --tsf_qty
                          0,         --fill_qty
                          0,         --distro_qty
                          0,         --selected_qty
                          0,         --cancelled_qty
                          I_tsf_qty, --ship_qty
                          0,         --received_qty
                          I_supp_pack_size,
                          NULL,
                          NULL,
                          'Y',
                          'N');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.INS_TSFDETAIL',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INS_TSFDETAIL;
-------------------------------------------------------------------------------
FUNCTION CREATE_TSF_INV_MAP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                            I_tsf_type      IN     TSFHEAD.TSF_TYPE%TYPE,
                            I_item          IN     ITEM_MASTER.ITEM%TYPE,
                            I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                            I_inv_status    IN     SHIPSKU.INV_STATUS%TYPE,
                            I_tsf_seq_no    IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                            I_phy_from_loc  IN     ITEM_LOC.LOC%TYPE,
                            I_from_loc_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_phy_to_loc    IN     ITEM_LOC.LOC%TYPE,
                            I_to_loc_type   IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_tsf_qty       IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN IS

   L_inv_array          bol_sql.inv_flow_array;
   inv_cnt              BINARY_INTEGER := 1;
   L_cust_order_loc_ind WH.CUSTOMER_ORDER_LOC_IND%TYPE;

BEGIN

   L_cust_order_loc_ind := 'N';
   if I_tsf_type = 'CO' then
      L_cust_order_loc_ind := 'Y';
   end if;
   --use library to distribute from loc qtys
   if DIST_FROM_LOC(O_error_message,
                    L_inv_array,
                    I_tsf_no,
                    I_tsf_seq_no,
                    I_item,
                    I_inv_status,
                    I_tsf_qty,
                    I_phy_to_loc,
                    I_to_loc_type,
                    I_phy_from_loc,
                    I_from_loc_type,
                    'T',                 -- indicate it's a transfer create
                    L_cust_order_loc_ind) = FALSE then
      return FALSE;
   end if;

   --Reject if pack item is sent and the from location does
   --not stock packs (receive as Each wh).
   --One element will always exist on from flow array, receive_as_type
   --will be the same in all elements of the array so just look at first one.

   if I_pack_ind = 'Y' and
      L_inv_array(1).from_receive_as_type = 'E' and
      I_from_loc_type = 'W' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_PACK_STOCK',to_char(I_phy_from_loc),
                                             null,null);
      return FALSE;
   end if;

   FOR inv_cnt IN L_inv_array.FIRST..L_inv_array.LAST LOOP
      if PUT_TSF_INV_MAP(O_error_message,
                         L_inv_array(inv_cnt).from_loc,
                         L_inv_array(inv_cnt).from_loc_type,
                         L_inv_array(inv_cnt).to_loc,
                         L_inv_array(inv_cnt).to_loc_type,
                         L_inv_array(inv_cnt).to_receive_as_type,
                         L_inv_array(inv_cnt).qty) = FALSE then
         return FALSE;
      end if;
   END LOOP; -- End populating inv_flow

   if INS_TSF_INV_FLOW(O_error_message) = FALSE then
      return FALSE;
   end if;

   if INS_SHIP_INV_FLOW(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.CREATE_TSF_INV_MAP',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_TSF_INV_MAP;
-------------------------------------------------------------------------------
FUNCTION CREATE_SHIP_INV_MAP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE,
                            I_pack_ind       IN     ITEM_MASTER.PACK_IND%TYPE,
                            I_inv_status     IN     SHIPSKU.INV_STATUS%TYPE,
                            I_tsf_seq_no     IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                            I_phy_from_loc   IN     ITEM_LOC.LOC%TYPE,
                            I_from_loc_type  IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_phy_to_loc     IN     ITEM_LOC.LOC%TYPE,
                            I_to_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_tsf_qty        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN IS

   L_inv_array   bol_sql.inv_flow_array;

   dist_cnt     BINARY_INTEGER := 1;
   item_cnt     BINARY_INTEGER := 1;
   virloc_cnt   BINARY_INTEGER := 1;
   inv_cnt      BINARY_INTEGER := 1;

   L_item       ITEM_MASTER.ITEM%TYPE;
   L_tsf_seq_no TSFDETAIL.TSF_SEQ_NO%TYPE;
   L_from_loc   ITEM_LOC.LOC%TYPE;
   L_to_loc     ITEM_LOC.LOC%TYPE;

   cursor C_LOCK_TSFITEM_INV_FLOW is
      select 'Y'
        from tsfitem_inv_flow tif
       where tif.tsf_no     = I_tsf_no
         and tif.tsf_seq_no = L_tsf_seq_no
         and tif.item       = L_item
         for update nowait;

BEGIN

   --use library to distribute from loc qtys
   if DIST_FROM_LOC(O_error_message,
                    L_inv_array,
                    I_tsf_no,
                    I_tsf_seq_no,
                    I_item,
                    I_inv_status,
                    I_tsf_qty,
                    I_phy_to_loc,
                    I_to_loc_type,
                    I_phy_from_loc,
                    I_from_loc_type,
                    'S') = FALSE then
      return FALSE;
   end if;

   FOR inv_cnt IN L_inv_array.FIRST..L_inv_array.LAST LOOP
      if PUT_TSF_INV_MAP(O_error_message,
                         L_inv_array(inv_cnt).from_loc,
                         L_inv_array(inv_cnt).from_loc_type,
                         L_inv_array(inv_cnt).to_loc,
                         L_inv_array(inv_cnt).to_loc_type,
                         L_inv_array(inv_cnt).to_receive_as_type,
                         L_inv_array(inv_cnt).qty) = FALSE then
         return FALSE;
      end if;
   END LOOP; -- End populating inv_flow

   if INS_SHIP_INV_FLOW(O_error_message) = FALSE then
      return FALSE;
   end if;

   dist_cnt := LP_bol_rec.distros.COUNT;
   item_cnt := LP_bol_rec.distros(dist_cnt).bol_items.COUNT;
   virloc_cnt := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.COUNT;

   open C_LOCK_TSFITEM_INV_FLOW;
   close C_LOCK_TSFITEM_INV_FLOW;

   for virloc_cnt IN LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.FIRST..LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.LAST loop
      L_item       := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item;
      L_tsf_seq_no := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).td_tsf_seq_no;
      L_from_loc   := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_from_loc;
      L_to_loc     := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_to_loc;

         update tsfitem_inv_flow tif
            set tif.shipped_qty    = NVL(tif.shipped_qty, 0) + LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_qty
          where tif.tsf_no         = I_tsf_no
            and tif.tsf_seq_no     = L_tsf_seq_no
            and tif.item           = L_item
            and tif.from_loc       = L_from_loc
            and tif.to_loc         = L_to_loc;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.CREATE_SHIP_INV_MAP',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_SHIP_INV_MAP;
-------------------------------------------------------------------------------
FUNCTION DIST_FROM_LOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_inv_flow_array      IN OUT BOL_SQL.INV_FLOW_ARRAY,
                       I_tsf_no              IN     TSFHEAD.TSF_NO%TYPE,
                       I_tsf_seq_no          IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                       I_item                IN     ITEM_MASTER.ITEM%TYPE,
                       I_inv_status          IN     SHIPSKU.INV_STATUS%TYPE,
                       I_tsf_qty             IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       I_phy_to_loc          IN     ITEM_LOC.LOC%TYPE,
                       I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_phy_from_loc        IN     ITEM_LOC.LOC%TYPE,
                       I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_dist_type           IN     VARCHAR2,
                       I_cust_order_loc_ind  IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_inv_status  SHIPSKU.INV_STATUS%TYPE := NULL;

   L_dist_array  DISTRIBUTION_SQL.DIST_TABLE_TYPE;

   dist_cnt      BINARY_INTEGER := 1;
   flow_cnt      BINARY_INTEGER := 1;

   L_index_loc_attrb   BINARY_INTEGER := 1;
   L_loc_attrb_array   loc_attrb_rec;

   cursor C_GET_LOC_ATTRB is
      select w.wh loc,
             I_from_loc_type loc_type,
             NVL(il.receive_as_type, 'E') receive_as_type
        from wh w,
             item_loc il
       where w.physical_wh      = I_phy_from_loc
         and il.loc    = w.wh
         and il.item   = I_item
      union all
      select w.wh loc,
            I_to_loc_type loc_type,
            NVL(il.receive_as_type, 'E') receive_as_type
       from wh w,
            item_loc il
      where w.physical_wh      = I_phy_to_loc
        and il.loc    = w.wh
         and il.item   = I_item
      union all
      select s.store loc,
             I_from_loc_type loc_type,
             NVL(il.receive_as_type, 'E') receive_as_type
        from store s,
             item_loc il
       where s.store         = I_phy_from_loc
         and il.loc          = s.store
         and il.item         = I_item
      union all
      select s.store loc,
             I_to_loc_type loc_type,
             NVL(il.receive_as_type, 'E') receive_as_type
        from store s,
             item_loc il
       where s.store         = I_phy_to_loc
         and il.loc          = s.store
         and il.item         = I_item;

BEGIN

   if I_inv_status IS NOT NULL and I_inv_status != -1 then
      L_inv_status := I_inv_status;
   end if;

   if I_dist_type = 'T' then  -- When transfer to be created
      if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                     L_dist_array,
                                     I_item,
                                     I_phy_from_loc,
                                     I_tsf_qty,
                                     'TRANSFER',
                                     L_inv_status,
                                     I_to_loc_type,
                                     I_phy_to_loc,
                                     NULL,              --order_no
                                     NULL,              --shipment
                                     I_tsf_seq_no,      --seq_no
                                     NULL,              --cycle_count
                                     NULL,              --rtv_order_no
                                     NULL,              --rtv_seq_no
                                     I_tsf_no,          --tsf_no
                                    'Y',                --tsf_create_ind
                                     I_cust_order_loc_ind) = FALSE then
         return FALSE;
      end if;
   else
      -- Tsfitem_inv_flow already exists for the tsf/seq_no/item.
      --
      if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                     L_dist_array,
                                     I_item,
                                     I_phy_from_loc,
                                     I_tsf_qty,
                                     'TRANSFER',
                                     L_inv_status,
                                     I_to_loc_type,
                                     I_phy_to_loc,
                                     NULL,              --order_no
                                     NULL,              --shipment
                                     I_tsf_seq_no,      --seq_no
                                     NULL,              --cycle_count
                                     NULL,              --rtv_order_no
                                     NULL,              --rtv_seq_no
                                     I_tsf_no) = FALSE then --tsf_no
         return FALSE;
      end if;
   end if;
   /*
    * Assign dist table qtys to flow array:
    *   -Walk through the array of distributed locations (L_dist_array)
    *    returned by the call to DISTRIBUTION_SQL.
    *   -Populate inv flow array
    */

   if L_dist_array is NULL or L_dist_array.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_TSF_DIST_VWH', NULL, NULL, NULL);
      return FALSE;
   end if;

   flow_cnt := 0;
   FOR dist_cnt IN L_dist_array.FIRST..L_dist_array.LAST LOOP
      flow_cnt := flow_cnt + 1;
      I_inv_flow_array(flow_cnt).phy_from_loc := I_phy_from_loc;
      I_inv_flow_array(flow_cnt).from_loc := L_dist_array(dist_cnt).from_loc;
      I_inv_flow_array(flow_cnt).from_loc_type := I_from_loc_type;
      I_inv_flow_array(flow_cnt).qty := L_dist_array(dist_cnt).dist_qty;
      I_inv_flow_array(flow_cnt).phy_to_loc := I_phy_to_loc;
      I_inv_flow_array(flow_cnt).to_loc := L_dist_array(dist_cnt).to_loc;
      I_inv_flow_array(flow_cnt).to_loc_type := I_to_loc_type;
   END LOOP;

   L_loc_attrb_array.locs := LOC_TBL();
   L_loc_attrb_array.loc_types := LOC_TYPE_TBL();
   L_loc_attrb_array.receive_as_types := INDICATOR_TBL();

   open C_GET_LOC_ATTRB;
   fetch C_GET_LOC_ATTRB BULK COLLECT INTO L_loc_attrb_array;
   close C_GET_LOC_ATTRB;

   L_index_loc_attrb := 0;
   -- Populate I_inv_flow_array
   FOR flow_cnt IN I_inv_flow_array.FIRST..I_inv_flow_array.LAST LOOP
      FOR L_index_loc_attrb IN L_loc_attrb_array.locs.FIRST..L_loc_attrb_array.locs.LAST LOOP
         if I_inv_flow_array(flow_cnt).from_loc = L_loc_attrb_array.locs(L_index_loc_attrb) and
            I_inv_flow_array(flow_cnt).from_loc_type = L_loc_attrb_array.loc_types(L_index_loc_attrb) then
            I_inv_flow_array(flow_cnt).from_receive_as_type := L_loc_attrb_array.receive_as_types(L_index_loc_attrb);
         end if;

         if I_inv_flow_array(flow_cnt).to_loc = L_loc_attrb_array.locs(L_index_loc_attrb) and
            I_inv_flow_array(flow_cnt).to_loc_type = L_loc_attrb_array.loc_types(L_index_loc_attrb) then
            I_inv_flow_array(flow_cnt).to_receive_as_type := L_loc_attrb_array.receive_as_types(L_index_loc_attrb);
         end if;
      END LOOP;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.DIST_FROM_LOC',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DIST_FROM_LOC;
-------------------------------------------------------------------------------
FUNCTION INS_TSF_INV_FLOW(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   dist_cnt     BINARY_INTEGER := 1;
   item_cnt     BINARY_INTEGER := 1;
   virloc_cnt   BINARY_INTEGER := 1;

BEGIN

   dist_cnt := LP_bol_rec.distros.COUNT;
   item_cnt := LP_bol_rec.distros(dist_cnt).bol_items.COUNT;
   virloc_cnt := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.COUNT;

   FOR virloc_cnt IN LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.FIRST..LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.LAST LOOP
      insert into tsfitem_inv_flow (tsf_no,
                                    tsf_seq_no,
                                    item,
                                    from_loc,
                                    to_loc,
                                    from_loc_type,
                                    to_loc_type,
                                    tsf_qty,
                                    shipped_qty,
                                    dist_pct)
                            values (LP_bol_rec.distros(dist_cnt).tsf_no,
                                    LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).td_tsf_seq_no,
                                    LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item,
                                    LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_from_loc,
                                    LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_to_loc,
                                    LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_from_loc_type,
                                    LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_to_loc_type,
                                    LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_qty, --tsf_qty
                                    LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_qty, --shipped_qty
                                    LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_qty /
                                    LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).qty);

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.INS_TSF_INV_FLOW',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INS_TSF_INV_FLOW;
-------------------------------------------------------------------------------
FUNCTION INS_SHIP_INV_FLOW(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   dist_cnt     BINARY_INTEGER := 1;
   item_cnt     BINARY_INTEGER := 1;
   virloc_cnt   BINARY_INTEGER := 1;

BEGIN

   dist_cnt := LP_bol_rec.distros.COUNT;
   item_cnt := LP_bol_rec.distros(dist_cnt).bol_items.COUNT;
   virloc_cnt := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.COUNT;

   FOR virloc_cnt IN LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.FIRST..LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.LAST LOOP
      insert into shipitem_inv_flow(shipment,
                                    seq_no,
                                    item,
                                    from_loc,
                                    to_loc,
                                    from_loc_type,
                                    to_loc_type,
                                    tsf_no,
                                    tsf_seq_no,
                                    tsf_qty,
                                    received_qty,
                                    dist_pct)
                            values(LP_bol_rec.ship_no,
                                   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ss_seq_no,
                                   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item,
                                   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_from_loc,
                                   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_to_loc,
                                   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_from_loc_type,
                                   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_to_loc_type,
                                   LP_bol_rec.distros(dist_cnt).tsf_no,
                                   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).td_tsf_seq_no,
                                   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_qty,
                                   0,
                                   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_qty /
                                   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).qty);


      -- For transfers involving a physical wh (i.e. EG transfers or CO transfers with OMS_IND=Y, and transferring
      -- either from or to a warehouse), write transfer charge at shipment time instead of at transfer create time.
      -- Include shipment info and use virtual locations on tsfdetail_chrg
      if TRANSFER_CHARGE_SQL.DEFAULT_CHRGS(
                         O_error_message,
                         LP_bol_rec.distros(dist_cnt).tsf_no,
                         LP_bol_rec.distros(dist_cnt).tsf_type,
                         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).td_tsf_seq_no,
                         LP_bol_rec.ship_no,
                         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ss_seq_no,
                         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_from_loc,
                         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_from_loc_type,
                         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_to_loc,
                         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_to_loc_type,
                         LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.INS_SHIP_INV_FLOW',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INS_SHIP_INV_FLOW;
-------------------------------------------------------------------------------
FUNCTION PUT_TSF_INV_MAP(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_from_loc        IN     ITEM_LOC.LOC%TYPE,
                         I_from_loc_type   IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_to_loc          IN     ITEM_LOC.LOC%TYPE,
                         I_to_loc_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_receive_as_type IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_tsf_qty         IN     TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN IS

   L_found      BOOLEAN := FALSE;
   dist_cnt     BINARY_INTEGER := 1;
   item_cnt     BINARY_INTEGER := 1;
   virloc_cnt   BINARY_INTEGER := 1;

BEGIN

   dist_cnt := LP_bol_rec.distros.COUNT;
   item_cnt := LP_bol_rec.distros(dist_cnt).bol_items.COUNT;

   --if already in map, update qty
   virloc_cnt := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.COUNT;
   if virloc_cnt > 0 then
      FOR virloc_cnt IN LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.FIRST..LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.LAST LOOP

         if LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_from_loc = I_from_loc and
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_to_loc = I_to_loc then

            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_qty :=
            LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_qty + I_tsf_qty;
            L_found := TRUE;
            EXIT;
         end if;

      END LOOP;
   end if;

   virloc_cnt := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs.COUNT;

   --add new map
   if L_found = FALSE then

      virloc_cnt := virloc_cnt + 1;

      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_from_loc := I_from_loc;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_from_loc_type := I_from_loc_type;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_to_loc := I_to_loc;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_to_loc_type := I_to_loc_type;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).receive_as_type := I_receive_as_type;
      LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).virtual_locs(virloc_cnt).vir_qty := I_tsf_qty;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.PUT_TSF_INV_MAP',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PUT_TSF_INV_MAP;
-------------------------------------------------------------------------------
--public function--
FUNCTION PROCESS_TSF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   i BINARY_INTEGER := 1;

BEGIN
   --don't need to loop -- distros are processed one at a time

   if BOL_SQL.SEND_TSF(O_error_message,
                       LP_bol_rec.bol_no,
                       LP_bol_rec.distros(i).tsf_no,
                       LP_bol_rec.distros(i).tsf_type,
                       LP_bol_rec.distros(i).new_tsf_ind,
                       LP_bol_rec.phy_to_loc,
                       LP_bol_rec.distros(i).tsf_status,
                       LP_bol_rec.tran_date,
                       LP_bol_rec.ship_no,
                       LP_bol_rec.eow_date,
                       LP_bol_rec.distros(i).bol_items) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.PROCESS_TSF',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TSF;
-------------------------------------------------------------------------------
--- This internal function has common code that is used in both PUT_TSF_ITEM
--- and RECEIPT_PUT_TSF_ITEM
-------------------------------------------------------------------------------
FUNCTION TSF_ITEM_COMMON(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE,
                         I_carton           IN     SHIPSKU.CARTON%TYPE,
                         I_qty              IN     TSFDETAIL.TSF_QTY%TYPE,
                         I_weight           IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                         I_weight_uom       IN     UOM_CLASS.UOM%TYPE,
                         I_phy_from_loc     IN     ITEM_LOC.LOC%TYPE,
                         I_from_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_phy_to_loc       IN     ITEM_LOC.LOC%TYPE,
                         I_to_loc_type      IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_tsfhead_to_loc   IN     ITEM_LOC.LOC%TYPE,
                         I_tsfhead_from_loc IN     ITEM_LOC.LOC%TYPE,
                         I_tsf_type         IN     TSFHEAD.TSF_TYPE%TYPE,
                         I_inv_status       IN     INV_STATUS_CODES.INV_STATUS%TYPE,
                         I_tsf_qty          IN     TSFDETAIL.TSF_QTY%TYPE,
                         I_ship_qty         IN     TSFDETAIL.SHIP_QTY%TYPE,
                         I_tsf_seq_no       IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                         I_item_cnt         IN     BINARY_INTEGER,
                         I_dist_cnt         IN     BINARY_INTEGER)
RETURN BOOLEAN IS

   L_receive_as_type         ITEM_LOC.RECEIVE_AS_TYPE%TYPE := NULL;
   L_from_finisher           BOOLEAN := FALSE;
   L_finisher_name           PARTNER.PARTNER_DESC%TYPE := NULL;
   L_tsfhead_final_loc       TSFHEAD.TO_LOC%TYPE;
   L_tsfhead_final_loc_type  TSFHEAD.TO_LOC_TYPE%TYPE;
   L_pwh_ind                 VARCHAR2(1) := 'N';

   L_exist     VARCHAR2(1) := 'N';

   cursor C_GET_TSF_FINAL_LOC is
      select to_loc, to_loc_type
        from tsfhead
       where tsf_parent_no = I_tsf_no;

   cursor C_FROM_RCV_AS_TYPE is
      select NVL(il.receive_as_type, 'E')
        from item_loc il
       where il.item = LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).item
         and il.loc  = I_tsfhead_from_loc;

   cursor C_TO_RCV_AS_TYPE is
      select NVL(il.receive_as_type, 'E')
        from item_loc il
       where il.item = LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).item
         and il.loc  = I_tsfhead_to_loc;

   cursor C_GET_TSFITEM_INV_FLOW is
      select 'Y'
        from tsfitem_inv_flow tif
       where tif.tsf_no     = I_tsf_no
         and tif.tsf_seq_no = I_tsf_seq_no
         and tif.item = LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).item;

BEGIN

   LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).qty := I_qty;
   LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).inv_status := I_inv_status;
   LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).carton := I_carton;
   LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).td_tsf_seq_no := I_tsf_seq_no;
   LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).td_tsf_qty := I_tsf_qty;
   LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).td_ship_qty := I_ship_qty;

   -- set weight on object for a simple pack catch weight item
   if LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).simple_pack_ind = 'Y' and
      LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).catch_weight_ind = 'Y' and
      I_weight is NOT NULL and
      I_weight_uom is NOT NULL then
      LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).weight     := I_weight;
      LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).weight_uom := I_weight_uom;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;

   --check for physical wh in CO transfers
   if I_tsf_type = 'CO' then
      if I_from_loc_type = 'W' and I_tsfhead_from_loc = I_phy_from_loc then
         L_pwh_ind := 'Y';
      end if;
      if L_pwh_ind = 'N' then
         if I_to_loc_type = 'W' and I_tsfhead_to_loc = I_phy_to_loc then
            L_pwh_ind := 'Y';
         end if;
      end if;
   end if;

   -- Determines if distribution is needed in rmssub_xtsf_sql
   -- for transfers that do NOT involve a physical warehouse
   if I_tsf_type NOT in ('EG','CO') or                          --non-EG/CO transfer will NOT have physical wh
      I_tsf_type = 'CO' and L_pwh_ind = 'N' or                  --CO transfers having NO physical wh
      I_from_loc_type = 'S' and I_to_loc_type = 'S' then        --transfers do NOT involve a warehouse
      if NEW_LOC(O_error_message,
                 LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).item,
                 LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).pack_ind,
                 LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).dept,
                 LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).class,
                 LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).subclass,
                 I_tsfhead_to_loc,
                 I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      if NEW_LOC(O_error_message,
                 LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).item,
                 LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).pack_ind,
                 LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).dept,
                 LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).class,
                 LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).subclass,
                 I_tsfhead_from_loc,
                 I_from_loc_type) = FALSE then
         return FALSE;
      end if;

       open C_GET_TSF_FINAL_LOC;
      fetch C_GET_TSF_FINAL_LOC into l_tsfhead_final_loc, l_tsfhead_final_loc_type;
      if C_GET_TSF_FINAL_LOC%FOUND then
         if NEW_LOC(O_error_message,
                    LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).item,
                    LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).pack_ind,
                    LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).dept,
                    LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).class,
                    LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).subclass,
                    l_tsfhead_final_loc,
                    l_tsfhead_final_loc_type) = FALSE then
            return FALSE;
          end if;
      end if;
      close C_GET_TSF_FINAL_LOC;

      if LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).pack_ind = 'Y' then

         open C_FROM_RCV_AS_TYPE;
         fetch C_FROM_RCV_AS_TYPE into L_receive_as_type;
         close C_FROM_RCV_AS_TYPE;

         --If sending a pack and from loc receive_as_type is 'E' then
         --determine if from loc is a finisher.
         --Finishers are allowed to send packs but do not track stock at pack level.
         if L_receive_as_type = 'E' and I_from_loc_type = 'W' then
            if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                            L_from_finisher,
                                            L_finisher_name,
                                            I_tsfhead_from_loc) = FALSE then
               return FALSE;
            end if;

         --external finisher
         elsif I_from_loc_type = 'E' then
            L_from_finisher := TRUE;
         end if;

         --reject if pack item is sent and the from location does
         --not stock packs (receive as Each wh) unless from loc is a finisher.
         --allow transfer of packs from store location.
         --online validation should prevent packs from being added to RMS created tranfers.
         if (I_from_loc_type = 'W' and L_receive_as_type = 'E' and L_from_finisher = FALSE) then
            O_error_message := SQL_LIB.CREATE_MSG('NO_PACK_STOCK',to_char(I_tsfhead_from_loc),null,null);
            return FALSE;
         end if;

         --set the 'to' receive as type
         open C_TO_RCV_AS_TYPE;
         fetch C_TO_RCV_AS_TYPE into L_receive_as_type;
         close C_TO_RCV_AS_TYPE;
      else --not pack
         L_receive_as_type := 'E';
      end if;

      --create flow in struct using locations on tsfhead (virtual)
      --non-EG transfer do not get records inserted in TSFITEM_INV_FLOW or SHIPITEM_INV_FLOW
      if PUT_TSF_INV_MAP(O_error_message,
                         I_tsfhead_from_loc,
                         I_from_loc_type,
                         I_tsfhead_to_loc,
                         I_to_loc_type,
                         L_receive_as_type,
                         I_qty) = FALSE then
         return FALSE;
      end if;

   else
      -- for transfers involving a physical wh
      open C_GET_TSFITEM_INV_FLOW;
      fetch C_GET_TSFITEM_INV_FLOW into L_exist;
      close C_GET_TSFITEM_INV_FLOW;

      if L_exist != 'Y' then
         --create flow in struct and on TSFITEM_INV_FLOW/SHIPITEM_INV_FLOW
         --using locations (physical) passed in the message
         -- only EG transfer get records on TSFITEM_INV_FLOW/SHIPITEM_INV_FLOW
         if CREATE_TSF_INV_MAP(O_error_message,
                               I_tsf_no,
                               I_tsf_type,
                               LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).item,
                               LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).pack_ind,
                               LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).inv_status,
                               I_tsf_seq_no,
                               I_phy_from_loc,
                               I_from_loc_type,
                               I_phy_to_loc,
                               I_to_loc_type,
                               I_qty) = FALSE then
            return FALSE;
         end if;
      else
         --create flow in struct and on SHIPITEM_INV_FLOW using locations (physical)
         --passed in the message -- only EG transfer get records on SHIPITEM_INV_FLOW
         if CREATE_SHIP_INV_MAP(O_error_message,
                               I_tsf_no,
                               LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).item,
                               LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).pack_ind,
                               LP_bol_rec.distros(I_dist_cnt).bol_items(I_item_cnt).inv_status,
                               I_tsf_seq_no,
                               I_phy_from_loc,
                               I_from_loc_type,
                               I_phy_to_loc,
                               I_to_loc_type,
                               I_qty) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'BOL_SQL.TSF_ITEM_COMMON',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END TSF_ITEM_COMMON;
----------------------------------------------------------------------------------
FUNCTION SEND_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_bol_no          IN       SHIPMENT.BOL_NO%TYPE,
                  I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                  I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE,
                  I_new_tsf         IN       VARCHAR2,
                  I_tsf_to_loc      IN       ITEM_LOC.LOC%TYPE,
                  I_tsf_status      IN       TSFHEAD.STATUS%TYPE,
                  I_tran_date       IN       PERIOD.VDATE%TYPE,
                  I_ship_no         IN       SHIPMENT.SHIPMENT%TYPE,
                  I_eow_date        IN       PERIOD.VDATE%TYPE,
                  I_bol_items       IN       BOL_SQL.BOL_ITEM_ARRAY)
return BOOLEAN IS

   item_cnt BINARY_INTEGER                                                        := 1;
   loc_cnt  BINARY_INTEGER                                                        := 1;
   L_exists                         BOOLEAN                                       := FALSE;
   L_to_store_row                   STORE%ROWTYPE;

   L_resv_exp_qty                   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE               := 0;
   L_intran_qty                     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE               := 0;

   -- This holds the pack's av_cost and retail at from loc.
   -- L_pack_loc_av_cost is used for prorating the charge.
   -- L_pack_loc_retail is used for writing shipsku.unit_retail for non-franchise transaction.
   L_pack_no                        ITEM_MASTER.ITEM%TYPE;
   L_pack_loc_av_cost               ITEM_LOC_SOH.AV_COST%TYPE;
   L_pack_loc_retail                ITEM_LOC.UNIT_RETAIL%TYPE;

   -- This holds the from loc item cost depending on the accounting method.
   L_from_wac                       item_loc_soh.av_cost%TYPE                     := NULL;
   L_from_av_cost                   ITEM_LOC_SOH.AV_COST%TYPE;
   L_from_wac_to_loc_curr           item_loc_soh.av_cost%TYPE                     := NULL;
   L_total_wo_cost                  item_loc_soh.av_cost%TYPE                     := NULL;
   L_charge_to_loc                  item_loc_soh.av_cost%TYPE                     := NULL;
   L_from_unit_retail               ITEM_LOC.UNIT_RETAIL%TYPE                     := NULL;

   -- for charges
   L_total_chrgs_prim               ITEM_LOC.UNIT_RETAIL%TYPE                     := 0;
   L_profit_chrgs_to_loc            NUMBER                                        := 0;
   L_exp_chrgs_to_loc               NUMBER                                        := 0;
   L_pack_total_chrgs_prim          ITEM_LOC.UNIT_RETAIL%TYPE                     := NULL;
   L_pack_profit_chrgs_to_loc       NUMBER                                        := NULL;
   L_pack_exp_chrgs_to_loc          NUMBER                                        := NULL;

   -- This holds the item's (or component item's) transfer unit cost used for
   -- stock ledger. Shipsku.unit_cost should be based on the same unit cost.
   L_tsf_unit_cost                  ITEM_LOC_SOH.AV_COST%TYPE                     := NULL;

   -- This holds the pack's transfer unit cost, aggregated by components.
   L_pack_tsf_unit_cost             ITEM_LOC_SOH.AV_COST%TYPE                     := 0;

   -- These variables hold the item's (or pack's) total cost/retail/charges/qty
   -- across all virtual locations.
   L_ss_cost                        ITEM_LOC_SOH.AV_COST%TYPE                     := 0;
   L_ss_prim_chrgs                  ITEM_LOC_SOH.AV_COST%TYPE                     := 0;
   L_ss_from_chrgs                  ITEM_LOC_SOH.AV_COST%TYPE                     := 0;
   L_ss_retail                      ITEM_LOC.UNIT_RETAIL%TYPE                     := 0;
   L_ss_qty                         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE               := 0;
   L_ss_exp_weight                  SHIPSKU.WEIGHT_EXPECTED%TYPE                  := NULL;
   L_ss_exp_weight_uom              SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE              := NULL;

   L_ins_cost                       SHIPSKU.UNIT_COST%TYPE                        := NULL;
   L_ins_retail                     SHIPSKU.UNIT_RETAIL%TYPE                      := NULL;
   L_ss_rcv_qty                     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE               := 0;

   L_ship_no                        SHIPMENT.SHIPMENT%TYPE                        := NULL;
   L_ss_seq_no                      SHIPSKU.SEQ_NO%TYPE                           := NULL;

   L_tsf_parent_no                  TSFHEAD.TSF_PARENT_NO%TYPE;

   L_comp_item                      VARCHAR2(1)                                   := 'Y';
   L_pack_ind                       VARCHAR2(1)                                   := 'N';
   L_pct_in_pack                    NUMBER                                        := 0;
   L_from_loc                       item_loc.loc%TYPE;
   L_pack_receive_as_type           item_loc.receive_as_type%TYPE                 := NULL;

   L_intercompany                   BOOLEAN := FALSE;
   L_finisher                       BOOLEAN := FALSE;
   L_finisher_name                  WH.WH_NAME%TYPE;
   L_from_finisher_ind              WH.FINISHER_IND%TYPE                          := 'N';
   L_to_finisher_ind                WH.FINISHER_IND%TYPE                          := 'N';

   L_upd_inv_status_qty             ITEM_LOC_SOH.STOCK_ON_HAND%TYPE               := 0;
   L_from_tsf_qty                   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE               := 0;
   L_tsf_qty                        TSFDETAIL.SHIP_QTY%TYPE                       := 0;

   -- for simple pack catch weight processing
   L_cuom                           ITEM_SUPP_COUNTRY.COST_UOM%TYPE               := NULL;
   L_vir_weight_cuom                ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE              := NULL;
   L_intran_weight_cuom             ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE              := NULL;

   --variable for REPAIR functionality
   L_tsfhead_rec                    TSFHEAD%ROWTYPE                               := NULL;
   L_external_finisher_rec          PARTNER%ROWTYPE                               := NULL;

   L_bol_items_extended_base_cost   ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE := NULL;
   L_bol_items_base_cost            ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE          := NULL;

   L_auto_rcv_ind                   VARCHAR2(1)                                   := 'N';
   L_store_auto_rcv_ind             STORE.AUTO_RCV%TYPE                           := NULL;
   L_is_franchise_ordret            BOOLEAN                                       := FALSE;
   L_item                           ITEM_MASTER.ITEM%TYPE;
   L_vwh_ind                        VARCHAR2(1)                                   := 'N';

   -- cursors
   cursor C_ITEM_IN_PACK is
       select v.item,
              v.qty,
              im.dept,
              im.class,
              im.subclass,
              im.sellable_ind,
              im.item_xform_ind,
              im.inventory_ind
         from item_master im,
              v_packsku_qty v
        where v.pack_no = L_pack_no
          and im.item   = v.item;

   cursor C_GET_TSF_PARENT_NO is
      select tsf_parent_no
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_TSF_HEAD is
      select *
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_EXT_FINISHER_INFO is
      select *
        from partner
       where partner_type = 'E'
         and partner_id = L_tsfhead_rec.to_loc;

   cursor C_GET_TSF_QTY is
      select tsf_qty
        from tsfdetail
       where tsf_no = I_tsf_no
         and item = L_item;


   cursor C_PACK_RCV_AS_TYPE is
      select NVL(il.receive_as_type, 'E')
        from item_loc il
       where il.loc  = L_from_loc
         and il.item = L_pack_no;

   cursor C_CHECK_VWH(L_wh   IN   WH.WH%TYPE) is
      select 'Y'
        from wh
       where wh = L_wh
         and wh != physical_wh;

BEGIN
   LP_tran_date := I_tran_date;
   --retrieve tsfhead record as global variables have incomplete info
   open C_GET_TSF_HEAD;
   fetch C_GET_TSF_HEAD into L_tsfhead_rec;
   close C_GET_TSF_HEAD;

   --retrieve the details of the an external finisher location
   if L_tsfhead_rec.to_loc_type = 'E' then
      open C_GET_EXT_FINISHER_INFO;
      fetch C_GET_EXT_FINISHER_INFO into L_external_finisher_rec;
      close C_GET_EXT_FINISHER_INFO;
   end if;

   open C_GET_TSF_PARENT_NO;
   fetch C_GET_TSF_PARENT_NO into L_tsf_parent_no;
   close C_GET_TSF_PARENT_NO;

   if L_tsf_parent_no is NOT NULL then  --2nd leg of multi-legged tsf
      L_from_finisher_ind := 'Y';
      L_to_finisher_ind := 'N';
      --Finishers can send packs but do not track stock of packs or pack
      --components.  Set L_comp_item to N so upd_from_item_loc will update
      --the finisher 'from' loc correctly.
      L_comp_item := 'N';
   end if;

   if LP_bol_rec.franchise_ordret_ind in (WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER,WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN) then
      L_is_franchise_ordret := TRUE;
   end if;

   if L_from_finisher_ind = 'Y' then
     --if second leg of the transfer then from loc is a finisher.
     --Call work order complete to perform any necessary transformations.
     --flush all the arrays build by INVADJ_SQL
     if INVADJ_SQL.FLUSH_ALL(O_error_message) = FALSE then
        return FALSE;
     end if;
     ---
      FOR item_cnt IN I_bol_items.FIRST..I_bol_items.LAST LOOP
         FOR loc_cnt IN I_bol_items(item_cnt).virtual_locs.FIRST..I_bol_items(item_cnt).virtual_locs.LAST LOOP
            --Because wo_item_comp writes (inits and flushes) its own tran_data records
            --put it in it's own item/loc loop to be processed first.  That way all work order complete adjustments
            --and tran data records are flushed, and only one flush is necessary at the end for the transfer in/out
            --tran data records created by WRITE_FINANCIALS.
            if TSF_WO_COMP_SQL.WO_ITEM_COMP(O_error_message,
                                            I_tsf_no,
                                            L_tsf_parent_no,
                                            I_bol_items(item_cnt).item,
                                            I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                            I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                            I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                            I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                            I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                            I_tran_date,
                                            FALSE) = FALSE then
               return FALSE;
            end if;
         end LOOP;
      end LOOP;

      --init all the arrays build by INVADJ_SQL
      if INVADJ_SQL.INIT_ALL(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;

   --check for virtual wh in CO transfers
   if I_tsf_type = 'CO' then
      if L_tsfhead_rec.from_loc_type = 'W' then
          open C_CHECK_VWH(L_tsfhead_rec.from_loc);
         fetch C_CHECK_VWH into L_vwh_ind;
         close C_CHECK_VWH;
         if L_vwh_ind = 'N' then
            if L_tsfhead_rec.to_loc_type = 'W' then
                open C_CHECK_VWH(L_tsfhead_rec.to_loc);
               fetch C_CHECK_VWH into L_vwh_ind;
               close C_CHECK_VWH;
            end if;
         end if;
      end if;
   end if;

   FOR item_cnt IN I_bol_items.FIRST..I_bol_items.LAST LOOP
      if I_tsf_type = 'EG' or
        (I_tsf_type = 'CO' and LP_system_options.oms_ind = 'Y' and L_vwh_ind = 'N') then
         L_ship_no := I_ship_no;
         L_ss_seq_no := I_bol_items(item_cnt).ss_seq_no;
      else
         L_ship_no := NULL;
         L_ss_seq_no := NULL;
      end if;

      FOR loc_cnt IN I_bol_items(item_cnt).virtual_locs.FIRST..I_bol_items(item_cnt).virtual_locs.LAST LOOP
         --set qty to use when setting the resv / exp qtys on item_loc_soh
         if I_new_tsf = 'Y' or I_tsf_status = 'C' then
            L_resv_exp_qty := 0;
         else
            if I_bol_items(item_cnt).td_ship_qty >= I_bol_items(item_cnt).td_tsf_qty then
               L_resv_exp_qty := 0;
            elsif I_bol_items(item_cnt).qty + I_bol_items(item_cnt).td_ship_qty > I_bol_items(item_cnt).td_tsf_qty then
               L_resv_exp_qty := I_bol_items(item_cnt).td_tsf_qty - I_bol_items(item_cnt).td_ship_qty;
            else
               L_resv_exp_qty := I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;
            end if;
         end if;

         L_intran_qty := I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;

         if TRANSFER_SQL.IS_INTERCOMPANY(O_error_message,
                                         L_intercompany,
                                         'T',  -- distro type
                                         I_tsf_type, -- transfer type
                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
            return FALSE;
         end if;

         if L_tsf_parent_no is NULL then  --single leg or 1st leg of multi-legged tsf
            if I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type = 'W' then
               if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                               L_finisher,
                                               L_finisher_name,
                                               I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc) = FALSE then
                  return FALSE;
               end if;
            elsif I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type = 'E' then
               --External Finisher
               L_finisher := TRUE;
            end if;

            if L_finisher = TRUE then
               --1st leg of multi-legged tsf
               L_to_finisher_ind := 'Y';
            else
               --one leg only tsf
               L_to_finisher_ind := 'N';
            end if;
         end if;

         if I_bol_items(item_cnt).inv_status != -1 and
            (I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type = 'W' or
            I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type = 'S') then

            --Determine if more stock needs to be taken from the correct unavailable inv_status.
            --This assumes that EG is only for Available inventory and won't fall into here.  Therefore this will only be called once,
            --as there will only be one vir_loc in the loop.
            if I_bol_items(item_cnt).qty + I_bol_items(item_cnt).td_ship_qty > I_bol_items(item_cnt).td_tsf_qty then
               L_upd_inv_status_qty := (I_bol_items(item_cnt).qty + I_bol_items(item_cnt).td_ship_qty)
                                          - I_bol_items(item_cnt).td_tsf_qty;

               if BOL_SQL.UPDATE_INV_STATUS(O_error_message,
                                            I_bol_items(item_cnt).item,
                                            I_bol_items(item_cnt).pack_ind,
                                            I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                            I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                            L_upd_inv_status_qty,
                                            I_bol_items(item_cnt).inv_status) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;

         L_item := I_bol_items(item_cnt).item;
         open C_GET_TSF_QTY;
         fetch C_GET_TSF_QTY into L_tsf_qty;
         close C_GET_TSF_QTY;

         --if receiving an unexpected item and from location is an external finisher, do not update stock on hand.
         if L_tsfhead_rec.from_loc_type = 'E' and L_tsf_qty = 0 then
            L_from_tsf_qty := 0;
         else
            L_from_tsf_qty := I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;
         end if;

         if I_bol_items(item_cnt).pack_ind = 'N' then
            if BOL_SQL.UPD_FROM_ITEM_LOC(O_error_message,
                                         I_bol_items(item_cnt).item,
                                         'N',
                                         I_bol_items(item_cnt).sellable_ind,
                                         I_bol_items(item_cnt).item_xform_ind,
                                         NULL, -- inventory ind
                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                         L_from_tsf_qty,
                                         L_resv_exp_qty,
                                         NULL, -- weight
                                         NULL, -- weight uom
                                         I_tran_date,
                                         I_eow_date) = FALSE then
               return FALSE;
            end if;
            ---
         if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                       L_from_wac,
                                       I_bol_items(item_cnt).item,
                                       I_bol_items(item_cnt).dept,
                                       I_bol_items(item_cnt).class,
                                       I_bol_items(item_cnt).subclass,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                       I_tran_date,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                return FALSE;
         end if;


            /* if this is the intercompany leg of a transfer, the tsf_item_cost.shipped_qty */
            /* needs to be updated unless it is an intercompany transfer involving a franchise store*/
            -- franchise order/return will not have tsf_item_cost
            if L_intercompany and NOT L_is_franchise_ordret then
               if UPD_TSF_ITEM_COST(O_error_message,
                                    I_bol_items(item_cnt).item,
                                    I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                    I_tsf_no) = FALSE then
                  return FALSE;
               end if;
            end if;

            if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                           L_total_chrgs_prim,
                                                           L_profit_chrgs_to_loc,
                                                           L_exp_chrgs_to_loc,
                                                           'T',                    --transfer
                                                           I_tsf_no,
                                                           I_bol_items(item_cnt).td_tsf_seq_no,
                                                           L_ship_no,
                                                           L_ss_seq_no,
                                                           I_bol_items(item_cnt).item,
                                                           NULL,                   --pack_item
                                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
               return FALSE;
            end if;

            if I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type = 'W' then
               if BOL_SQL.WRITE_ISSUES(O_error_message,
                                       I_bol_items(item_cnt).item,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                       I_eow_date,
                                       I_bol_items(item_cnt).dept,
                                       I_bol_items(item_cnt).class,
                                       I_bol_items(item_cnt).subclass) = FALSE then
                  return FALSE;
               end if;
            end if;

            if L_is_franchise_ordret then
               if STKLEDGR_SQL.WF_WRITE_FINANCIALS(O_error_message,
                                                   L_tsf_unit_cost,      --used for shipsku.unit_cost update
                                                   L_from_unit_retail,   --used for shipsku.unit_retail update
                                                   I_tsf_no,
                                                   'T',
                                                   I_tran_date,
                                                   I_bol_items(item_cnt).item,
                                                   NULL,  --pack_no
                                                   NULL,  --pct_in_pack
                                                   I_bol_items(item_cnt).dept,
                                                   I_bol_items(item_cnt).class,
                                                   I_bol_items(item_cnt).subclass,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                   NULL,
                                                   NULL) = FALSE then                       --weight_cuom
                  return FALSE;
               end if;
            else
               if STKLEDGR_SQL.WRITE_FINANCIALS(O_error_message,
                                                L_tsf_unit_cost,  --used for shipsku.unit_cost update and recalc WAC at to-loc
                                                'T',
                                                I_ship_no,
                                                I_tsf_no,
                                                I_tran_date,
                                                I_bol_items(item_cnt).item,
                                                NULL,  --pack_no
                                                NULL,  --pct_in_pack
                                                I_bol_items(item_cnt).dept,
                                                I_bol_items(item_cnt).class,
                                                I_bol_items(item_cnt).subclass,
                                                I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                                NULL,   --weight_cuom
                                                I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                L_from_finisher_ind,
                                                I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                L_to_finisher_ind,
                                                NULL,  -- pass in NULL as the WAC will be calculated in the write_financials function
                                                L_profit_chrgs_to_loc,
                                                L_exp_chrgs_to_loc,
                                                L_intercompany,
                                                I_bol_items(item_cnt).extended_base_cost,
                                                I_bol_items(item_cnt).base_cost) = FALSE then
                  return FALSE;
               end if;
            end if;

            -- L_tsf_unit_cost is in from-loc's currency, returned from WF_WRITE_FINANICAL or WRITE_FINANCIAL.
            if BOL_SQL.UPD_TO_ITEM_LOC(O_error_message,
                                       I_bol_items(item_cnt).item,
                                       NULL, --pack_no
                                       NULL, --percent in pack
                                       'E',  --receive_as_type
                                       I_tsf_type,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                       L_resv_exp_qty,
                                       L_intran_qty,
                                       NULL,  --weight_cuom
                                       NULL,  --cuom
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                       L_total_chrgs_prim,
                                       I_tsf_no,
                                       'T',
                                       L_intercompany) = FALSE then
               return FALSE;
            end if;

            if UPDATE_SNAPSHOT_SQL.EXECUTE(O_error_message,
                                           'TSFO',
                                           I_bol_items(item_cnt).item,
                                           'N',
                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                           I_tran_date,
                                           LP_vdate,
                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty) = FALSE then
               return FALSE;
            end if;

             If L_intercompany then
                 if TRANSFER_COST_SQL.GET_TSF_COST_PRICE(O_error_message,
                                                         L_from_wac_to_loc_curr,
                                                         L_total_wo_cost,
                                                         I_tsf_no,
                                                         I_bol_items(item_cnt).item,
                                                         NULL,
                                                         NULL,
                                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                         L_intercompany)= FALSE then
                    return FALSE;
                 end if;
             else

         -- Convert L_from_wac into to_loc currency
             if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                 NULL,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                 NULL,
                                                 L_from_wac,--L_from_wac,
                                                 L_from_wac_to_loc_curr,
                                                 'C',
                                                 NULL,
                                                 NULL) = FALSE then
                 return FALSE;
             end if;
            end if;

         --convert chrg from primary to to_loc's currency
             if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                 NULL,
                                                 L_total_chrgs_prim,
                                                 L_charge_to_loc,
                                                 'C',
                                                 NULL,
                                                 NULL) = FALSE then
                 return FALSE;
             end if;

         -- Update snapshot cost for the to_loc
             if UPDATE_SNAPSHOT_SQL.UPDATE_SNAPSHOT_COST
                                    (O_error_message,
                                     I_bol_items(item_cnt).item,
                                     I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                     I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                     I_tran_date,
                                     I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                    (L_from_wac_to_loc_curr + L_charge_to_loc)
                                     ) = FALSE then
                 return FALSE;
             end if;



            --before writing the shipsku retail check if from loc is an external finisher.
            --if so, the unit retail is null. Get the correct retail.
            --For an orderable BTS item, unit_retail is also not defined, in which case, get its sellable's retail.

            if L_from_unit_retail is NULL then
               if BOL_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                L_from_av_cost,
                                                L_from_unit_retail,
                                                I_bol_items(item_cnt).item,
                                                'N',
                                                I_bol_items(item_cnt).sellable_ind,
                                                I_bol_items(item_cnt).item_xform_ind,
                                                I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                  return FALSE;
               end if;
            end if;

            --If unit retail is still NULL, it could be for an orderable/non-sellable non-pack/non-tranformed item.
            --Set unit_retail to 0 for writing to shipsku.
            if L_from_unit_retail is NULL then
               L_from_unit_retail := 0;
            end if;

            L_ss_prim_chrgs := L_ss_prim_chrgs +
               (L_total_chrgs_prim * I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);

            -- if tsf_cost is defined, shipsku.unit_cost should be based off tsf_cost
            -- sum up shipsku cost across all virtual locations
            L_ss_cost := L_ss_cost + NVL(L_tsf_unit_cost, 0) *
               I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;

            L_ss_retail := L_ss_retail + (L_from_unit_retail *
               I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);

            L_ss_qty := L_ss_qty + I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;

         else --pack

            L_pack_no := I_bol_items(item_cnt).item;

            -- Pack unit retail will be used for shipsku.unit_retail update for non-franchise scenarios
            if BOL_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                             L_pack_loc_av_cost,
                                             L_pack_loc_retail,
                                             L_pack_no,
                                             'Y',
                                             I_bol_items(item_cnt).sellable_ind,
                                             I_bol_items(item_cnt).item_xform_ind,
                                             I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                             I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                             I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                             I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
               return FALSE;
            end if;

            -- If weight is not defined for a simple pack catch weight item,
            -- use the virtual from loc's average weight for the pack to derive weight.
            if I_bol_items(item_cnt).simple_pack_ind = 'Y' and
               I_bol_items(item_cnt).catch_weight_ind = 'Y' then

               if NOT CATCH_WEIGHT_SQL.PRORATE_WEIGHT(O_error_message,
                                                      L_vir_weight_cuom,
                                                      L_cuom,
                                                      L_pack_no,
                                                      I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                      I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                      I_bol_items(item_cnt).weight,
                                                      I_bol_items(item_cnt).weight_uom,
                                                      I_bol_items(item_cnt).qty,
                                                      I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty) then
                  return FALSE;
               end if;

               -- L_intran_qty is either 0 or vir_qty
               if L_intran_qty != 0 then
                  L_intran_weight_cuom := L_vir_weight_cuom;
               end if;

               -- sum up shipsku weight_expected across all virtual locations
               if I_bol_items(item_cnt).weight is NULL or
                  I_bol_items(item_cnt).weight_uom is NULL then
                  if L_cuom = 'EA' then
                     L_ss_exp_weight := NVL(L_ss_exp_weight, 0) + I_bol_items(item_cnt).qty;
                  else
                     L_ss_exp_weight := NVL(L_ss_exp_weight, 0) + L_vir_weight_cuom;
                  end if;
               else
                  L_ss_exp_weight := NVL(L_ss_exp_weight, 0) + L_vir_weight_cuom;
               end if;
               L_ss_exp_weight_uom := L_cuom;
            else
               L_vir_weight_cuom    := NULL;
               L_intran_weight_cuom := NULL;
            end if;

            if BOL_SQL.UPDATE_PACK_LOCS(O_error_message,
                                        L_pack_no,
                                        I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                        I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                        I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                        I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                        I_bol_items(item_cnt).virtual_locs(loc_cnt).receive_as_type,
                                        I_tsf_type,
                                        I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                        L_vir_weight_cuom,
                                        L_intran_qty,
                                        L_intran_weight_cuom,
                                        L_resv_exp_qty,
                                        I_tran_date) = FALSE then
               return FALSE;
            end if;


            L_from_loc := I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc;
            open C_PACK_RCV_AS_TYPE;
            fetch C_PACK_RCV_AS_TYPE into L_pack_receive_as_type;
            close C_PACK_RCV_AS_TYPE;

               if UPDATE_SNAPSHOT_SQL.EXECUTE(O_error_message,
                                              'TSFO',
                                              L_pack_no,
                                              'P',
                                              I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                              I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                              I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                              I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                              I_tran_date,
                                              LP_vdate,
                                              I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                              L_pack_receive_as_type) = FALSE then
                  return FALSE;

            end if;


            if I_bol_items(item_cnt).pack_type != 'B' then
               if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                              L_pack_total_chrgs_prim,
                                                              L_pack_profit_chrgs_to_loc,
                                                              L_pack_exp_chrgs_to_loc,
                                                              'T',                         --transfer
                                                              I_tsf_no,
                                                              I_bol_items(item_cnt).td_tsf_seq_no,
                                                              L_ship_no,
                                                              L_ss_seq_no,
                                                              I_bol_items(item_cnt).item,  --item (send pack in item field)
                                                              NULL,                        --pack_no
                                                              I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                              I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                              I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                              I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                  return FALSE;
               end if;

               L_ss_prim_chrgs := L_ss_prim_chrgs + (L_pack_total_chrgs_prim *
                  I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);
            end if;

            L_pack_tsf_unit_cost := 0;
            FOR rec in C_ITEM_IN_PACK LOOP

               -- if returning from a franchise location use the to-loc's WAC, otherwise it will be the source location
               if LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
                  if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                                   L_pct_in_pack,
                                                   L_pack_no,
                                                   rec.item,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc) = FALSE then
                     return FALSE;
                  end if;
               else
                  if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                                   L_pct_in_pack,
                                                   L_pack_no,
                                                   rec.item,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if BOL_SQL.UPD_FROM_ITEM_LOC(O_error_message,
                                            rec.item,
                                            L_comp_item,
                                            rec.sellable_ind,
                                            rec.item_xform_ind,
                                            rec.inventory_ind,
                                            I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                            L_from_tsf_qty * rec.qty,
                                            L_resv_exp_qty * rec.qty,
                                            L_vir_weight_cuom, -- weight
                                            L_cuom, -- weight uom
                                            I_tran_date,
                                            I_eow_date) = FALSE then
                  return FALSE;
               end if;
             if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                           L_from_wac,
                                           rec.item,
                                           rec.dept,
                                           rec.class,
                                           rec.subclass,
                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                           I_tran_date,
                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                           I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                    return FALSE;
             end if;



               /* if this is the intercompany leg of a transfer, the tsf_item_cost.shipped_qty */
               /* needs to be updated */
               if L_intercompany and NOT L_is_franchise_ordret then
                  if UPD_TSF_ITEM_COST(O_error_message,
                                       rec.item,
                                       I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty * rec.qty,
                                       I_tsf_no) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if I_bol_items(item_cnt).pack_type != 'B' then
                  --******************************************************************************
                  -- Value returned in L_pack_profit_chrgs_to_loc, L_pack_exp_chrgs_to_loc, and
                  -- L_pack_total_chrgs_prim are unit values for the entire pack.  Need to take
                  -- a proportionate piece of the value for each component item in the pack
                  -- The formula for this is:
                  --       [Pack Value * (Comp Item Avg Cost * Comp Qty in the Pack) /
                  --                     (Total Pack Avg Cost)] /
                  --       Comp Qty in the Pack
                  -- You must divide the value by the Component Item Qty in the pack because the
                  -- value will be for one pack.  In order to get a true unit value you need to
                  -- do the last division.  Since we multiple by Comp Qty and then divide by it,
                  -- it can be removed from the calculation completely.
                  -- L_pct_in_pack is the component av_cost / total pack av cost value
                  --******************************************************************************
                  if L_pack_loc_av_cost = 0 then
                          L_profit_chrgs_to_loc := 0;
                          L_exp_chrgs_to_loc    := 0;
                          L_total_chrgs_prim    := 0;
                  else
                          L_profit_chrgs_to_loc := L_pack_profit_chrgs_to_loc * L_pct_in_pack;
                          L_exp_chrgs_to_loc    := L_pack_exp_chrgs_to_loc    * L_pct_in_pack;
                          L_total_chrgs_prim    := L_pack_total_chrgs_prim    * L_pct_in_pack;
                  end if;
               else
                  if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                                 L_total_chrgs_prim,
                                                                 L_profit_chrgs_to_loc,
                                                                 L_exp_chrgs_to_loc,
                                                                 'T',                         --transfer
                                                                 I_tsf_no,
                                                                 I_bol_items(item_cnt).td_tsf_seq_no,
                                                                 L_ship_no,
                                                                 L_ss_seq_no,
                                                                 rec.item,                    --item
                                                                 L_pack_no,  --pack_no
                                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                     return FALSE;
                  end if;

                  L_ss_prim_chrgs := L_ss_prim_chrgs + (L_total_chrgs_prim * rec.qty * I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);

               end if;

               if I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type = 'W' then
                  if BOL_SQL.WRITE_ISSUES(O_error_message,
                                          rec.item,
                                          I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                          I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty * rec.qty,
                                          I_eow_date,
                                          I_bol_items(item_cnt).dept,
                                          I_bol_items(item_cnt).class,
                                          I_bol_items(item_cnt).subclass) = FALSE then
                     return FALSE;
                  end if;
               end if;

               L_bol_items_extended_base_cost := I_bol_items(item_cnt).extended_base_cost * L_pct_in_pack;
               L_bol_items_base_cost          := I_bol_items(item_cnt).base_cost * L_pct_in_pack;

               if L_is_franchise_ordret then
                  if STKLEDGR_SQL.WF_WRITE_FINANCIALS(O_error_message,
                                                      L_tsf_unit_cost,
                                                      L_from_unit_retail,  --sum of component retail for shipsku.unit_retail, will add back to f order/return cost
                                                      I_tsf_no,
                                                      'T',
                                                      I_tran_date,
                                                      rec.item,
                                                      L_pack_no,      --pack_no
                                                      L_pct_in_pack,  --pct_in_pack
                                                      rec.dept,
                                                      rec.class,
                                                      rec.subclass,
                                                      I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty * rec.qty,
                                                      I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                      I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                      I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                      I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                      NULL,
                                                      NULL) = FALSE then  --weight_cuom
                     return FALSE;
                  end if;
                  -- sum up component's unit_retail for shipsku.unit_retail for franchise transaction
                  L_ss_retail := L_ss_retail + (NVL(L_from_unit_retail,0) * rec.qty * I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);
               else
                  if STKLEDGR_SQL.WRITE_FINANCIALS(O_error_message,
                                                   L_tsf_unit_cost,
                                                   'T',
                                                   I_ship_no,      --shipment
                                                   I_tsf_no,
                                                   I_tran_date,    --tran date
                                                   rec.item,
                                                   L_pack_no,      --pack_no
                                                   L_pct_in_pack,  --pct_in_pack
                                                   rec.dept,
                                                   rec.class,
                                                   rec.subclass,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty * rec.qty,
                                                   L_intran_weight_cuom,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                   L_from_finisher_ind,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                   I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                   L_to_finisher_ind,
                                                   NULL,   -- pass in NULL as the WAC will be calculated in the write_financials function
                                                   L_profit_chrgs_to_loc,
                                                   L_exp_chrgs_to_loc,
                                                   L_intercompany,
                                                   L_bol_items_extended_base_cost,
                                                   L_bol_items_base_cost) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if rec.inventory_ind = 'Y' then
                  if BOL_SQL.UPD_TO_ITEM_LOC(O_error_message,
                                             rec.item,
                                             L_pack_no,
                                             L_pct_in_pack,
                                             I_bol_items(item_cnt).virtual_locs(loc_cnt).receive_as_type,  --correct for all loc types!!
                                             I_tsf_type,
                                             I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                             I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                             I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty * rec.qty,
                                             L_resv_exp_qty * rec.qty,
                                             L_intran_qty * rec.qty,
                                             L_intran_weight_cuom,
                                             L_cuom,
                                             I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                             I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                             L_total_chrgs_prim,
                                             I_tsf_no,
                                             'T',
                                             L_intercompany) = FALSE then
                     return FALSE;
                  end if;






               end if;


               if I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type = 'S' then
                  L_pack_ind := 'N';
               else
                  L_pack_ind := 'C';
               end if;

                  if UPDATE_SNAPSHOT_SQL.EXECUTE(O_error_message,
                                                 'TSFO',
                                                 rec.item,
                                                 L_pack_ind,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                 I_tran_date,
                                                 LP_vdate,
                                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty * rec.qty,
                                                 L_pack_receive_as_type) = FALSE then
                     return FALSE;

               end if;

           If L_intercompany then
                 if TRANSFER_COST_SQL.GET_TSF_COST_PRICE(O_error_message,
                                                         L_from_wac_to_loc_curr,
                                                         L_total_wo_cost,
                                                         I_tsf_no,
                                                         rec.item,
                                                         I_bol_items(item_cnt).item,
                                                         L_pct_in_pack,
                                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                         I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                         L_intercompany)= FALSE then
                     return FALSE;
                 end if;
                 else
             -- Convert L_from_wac into to_loc currency
                 if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                     I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                     I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                     NULL,
                                                     I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                     I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                     NULL,
                                                     L_from_wac,--L_from_wac,
                                                     L_from_wac_to_loc_curr,
                                                     'C',
                                                     NULL,
                                                     NULL) = FALSE then
                     return FALSE;
                 end if;
           end if;

           --convert chrg from primary to to_loc's currency
           if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                               NULL,
                                               NULL,
                                               NULL,
                                               I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                               I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                               NULL,
                                               L_total_chrgs_prim,
                                               L_charge_to_loc,
                                               'C',
                                               NULL,
                                               NULL) = FALSE then
                 return FALSE;
           end if;

          -- Update snapshot cost for the to_loc
           if UPDATE_SNAPSHOT_SQL.UPDATE_SNAPSHOT_COST
                                (O_error_message,
                                 rec.item,
                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                 I_tran_date,
                                 I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty * rec.qty,
                                 (L_from_wac_to_loc_curr + L_charge_to_loc)
                                 ) = FALSE then
                 return FALSE;
           end if;


               -- sum up component item's transfer unit cost
               L_pack_tsf_unit_cost := L_pack_tsf_unit_cost + NVL(L_tsf_unit_cost, 0) * rec.qty;

            END LOOP; --pack comp

            -- transfer is at the virtual locations
            -- shipment is at the physical locations
            -- sum up transfer cost across all virtual locations

            L_ss_cost := L_ss_cost + L_pack_tsf_unit_cost *
               I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;

            --for non-franchise transactions, use pack's unit retail for shipsku.unit_retail update
            if NOT L_is_franchise_ordret then
               L_ss_retail := L_ss_retail + (NVL(L_pack_loc_retail, 0) *
                  I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);
            end if;

            L_ss_qty := L_ss_qty + I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;

         end if; --item type

         if LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then
            if WF_BOL_SQL.WRITE_WF_BILLING_SALES (O_error_message,
                                                  L_tsfhead_rec.wf_order_no,
                                                  I_bol_items(item_cnt).item,
                                                  I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                  I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                  I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                  NULL,
                                                  NULL,
                                                  I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                                  I_tran_date) = FALSE then
               return FALSE;
            end if;

         elsif LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
            if WF_BOL_SQL.WRITE_WF_BILLING_RETURNS (O_error_message,
                                                    L_tsfhead_rec.rma_no,
                                                    I_tsf_no,
                                                    I_bol_items(item_cnt).item,
                                                    L_tsfhead_rec.to_loc,
                                                    L_tsfhead_rec.to_loc_type,
                                                    I_bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty,
                                                    I_tran_date) = FALSE then
               return FALSE;
            end if;
         end if;

      END LOOP; --virtual loc

      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          /* since all virtuals in a phy must have
                                          same currency code, just grab the 1st one */
                                          I_bol_items(item_cnt).virtual_locs(1).vir_from_loc,
                                          I_bol_items(item_cnt).virtual_locs(1).vir_from_loc_type,
                                          NULL,
                                          L_ss_prim_chrgs,
                                          L_ss_from_chrgs,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;

      L_ins_cost := (L_ss_cost / L_ss_qty) + (L_ss_from_chrgs / L_ss_qty);
      L_ins_retail := L_ss_retail / L_ss_qty;
      -- LP_receipt_ind is set for when the BOL process is being called from the Receiving process.
      --
      if LP_receipt_ind = 'Y' then
         L_ss_rcv_qty := L_ss_qty;
         L_ss_qty := 0;
      else
         L_ss_rcv_qty := 0;
      end if;

      if BOL_SQL.INS_SHIPSKU(O_error_message,
                             I_ship_no,
                             I_bol_items(item_cnt).ss_seq_no,
                             I_bol_items(item_cnt).item,
                             I_bol_items(item_cnt).ref_item,
                             I_tsf_no,
                             'T',
                             I_bol_items(item_cnt).carton,
                             I_bol_items(item_cnt).inv_status,
                             L_ss_rcv_qty,
                             L_ins_cost,
                             L_ins_retail,
                             L_ss_qty,
                             L_ss_exp_weight,
                             L_ss_exp_weight_uom) = FALSE then
         return FALSE;
      end if;

      L_ss_prim_chrgs := 0;
      L_ss_cost := 0;
      L_ss_retail := 0;
      L_ss_qty := 0;
      L_ss_rcv_qty := 0;
      L_ss_exp_weight := NULL;
      L_ss_exp_weight_uom := NULL;
      L_ins_cost := 0;
      L_ins_retail := 0;
      L_from_unit_retail := NULL;
      L_pack_loc_retail := NULL;

   END LOOP; --items

   if L_tsfhead_rec.to_loc_type = 'S' then
      --perform store end auto-receiving functionality if needed
      L_auto_rcv_ind := 'N';

      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exists,
                                  L_to_store_row,
                                  I_tsf_to_loc) = FALSE then
         return FALSE;
      end if;

      if L_exists = TRUE then
         L_store_auto_rcv_ind := L_to_store_row.auto_rcv;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE');
         return FALSE;
      end if;

      if (L_store_auto_rcv_ind = 'Y') or
         (L_store_auto_rcv_ind = 'D' and LP_system_options.auto_rcv_store = 'Y') or
         (I_tsf_type = 'CO' and L_tsfhead_rec.from_loc_type = 'W' and
          L_to_store_row.store_type = 'C' and L_to_store_row.stockholding_ind = 'N') or
         (L_is_franchise_ordret and L_to_store_row.store_type = 'F' and L_to_store_row.stockholding_ind = 'N') then
          --Auto receive a transfer to STORE if conditions are met:
          --1) to-loc store's auto_rcv flag is 'Y', or
          --2) to-loc store's auto_rcv flag is 'D' for depending on system option auto_rcv_store setting, or
          --3) customer order transfer from a warehouse to a virtual store (i.e. non-stockholding company store)
          --4) the to-loc is a non-stockholding franchise store (for franchise orders)

          L_auto_rcv_ind := 'Y';
          ---
      end if;
   elsif L_tsfhead_rec.to_loc_type = 'E' and L_external_finisher_rec.auto_rcv_stock_ind = 'Y'  then
      --Auto receive for a REPAIR context transfer if conditions are met - auto_rcv_stock_ind = 'Y' for the external finisher
      L_auto_rcv_ind := 'Y';
      ---
   end if;

   if L_auto_rcv_ind = 'Y' then
      if AUTO_RCV_LINE_ITEM(O_error_message) = FALSE then
         return FALSE;
      end if;

      LP_receipt_ind := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.SEND_TSF',
                                            to_char(SQLCODE));
        return FALSE;
END SEND_TSF;
-------------------------------------------------------------------------------
FUNCTION GET_COSTS_AND_RETAILS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_av_cost               IN OUT ITEM_LOC_SOH.AV_COST%TYPE,
                               O_unit_retail           IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                               I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_ind              IN     VARCHAR2,
                               I_sellable_ind          IN     ITEM_MASTER.SELLABLE_IND%TYPE,
                               I_item_xform_ind        IN     ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                               I_from_loc              IN     ITEM_LOC.LOC%TYPE,
                               I_from_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_to_loc                IN     ITEM_LOC.LOC%TYPE,
                               I_to_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_unit_cost            ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_av_cost              ITEM_LOC_SOH.AV_COST%TYPE;
   L_selling_unit_retail  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom          ITEM_LOC.SELLING_UOM%TYPE;
   L_loc                  ITEM_LOC.LOC%TYPE;

   cursor C_ITEM_LOC_RETAIL(CV_loc item_loc.loc%TYPE) is
      select il.unit_retail
        from item_loc  il
       where il.item = I_item
         and il.loc = CV_loc;

BEGIN

   --External finisher does not have unit retail.  Use the unit retail of the to loc
   if I_from_loc_type = 'E' and I_pack_ind = 'Y' then
      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  I_item,
                                                  I_to_loc,
                                                  I_to_loc_type,
                                                  L_av_cost,
                                                  L_unit_cost,
                                                  O_unit_retail,
                                                  L_selling_unit_retail,
                                                  L_selling_uom) = FALSE then
         return FALSE;
      end if;

      if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                        I_item,
                                        I_from_loc,
                                        I_from_loc_type,
                                        O_av_cost) = FALSE then
         return FALSE;
      end if;

   elsif I_pack_ind = 'Y' then
      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  I_item,
                                                  I_from_loc,
                                                  I_from_loc_type,
                                                  O_av_cost,
                                                  L_unit_cost,
                                                  O_unit_retail,
                                                  L_selling_unit_retail,
                                                  L_selling_uom) = FALSE then
         return FALSE;
      end if;

   else
      if I_from_loc_type = 'E' then
         L_loc := I_to_loc;
      else
         L_loc := I_from_loc;
      end if;
      ---
      --- Non-sellable transformation item
      if I_sellable_ind = 'N' and I_item_xform_ind = 'Y' then

         if ITEM_XFORM_SQL.CALCULATE_RETAIL(O_error_message,
                                            I_item,
                                            L_loc,
                                            O_unit_retail) = FALSE then
            return FALSE;
         end if;
      ---
      ---Regular tran level item
      else

         open C_ITEM_LOC_RETAIL(L_loc);
         fetch C_ITEM_LOC_RETAIL into O_unit_retail;

         if C_ITEM_LOC_RETAIL%NOTFOUND then
            close C_ITEM_LOC_RETAIL;
            O_error_message:= SQL_LIB.CREATE_MSG('NO_ITEM_LOC', L_loc, NULL, NULL);
            return FALSE;
         end if;

         close C_ITEM_LOC_RETAIL;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.GET_COSTS_AND_RETAILS',
                                            to_char(SQLCODE));
        return FALSE;
END GET_COSTS_AND_RETAILS;
-------------------------------------------------------------------------------


























































FUNCTION INS_SHIPSKU(O_error_message  IN OUT VARCHAR2,
                     I_shipment       IN     SHIPMENT.SHIPMENT%TYPE,
                     I_seq_no         IN     SHIPSKU.SEQ_NO%TYPE,
                     I_item           IN     ITEM_MASTER.ITEM%TYPE,
                     I_ref_item       IN     ITEM_MASTER.ITEM%TYPE,
                     I_distro_no      IN     SHIPSKU.DISTRO_NO%TYPE,
                     I_distro_type    IN     SHIPSKU.DISTRO_TYPE%TYPE,
                     I_carton         IN     SHIPSKU.CARTON%TYPE,
                     I_inv_status     IN     SHIPSKU.INV_STATUS%TYPE,
                     I_rcv_qty        IN     SHIPSKU.QTY_EXPECTED%TYPE,
                     I_cost           IN     SHIPSKU.UNIT_COST%TYPE,
                     I_retail         IN     SHIPSKU.UNIT_RETAIL%TYPE,
                     I_exp_qty        IN     SHIPSKU.QTY_EXPECTED%TYPE,
                     I_exp_weight     IN     SHIPSKU.WEIGHT_EXPECTED%TYPE,
                     I_exp_weight_uom IN     SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE,
                     I_adjust_type    IN     SHIPSKU.ADJUST_TYPE%TYPE default NULL)
RETURN BOOLEAN IS

   L_ss_idx   VARCHAR2(30) := NULL;

BEGIN


   L_ss_idx := I_shipment||'/'||I_seq_no;

   --for allocs with multiple to-locations in the same physical wh, only one shipsku record should be created.
   --check if the shipment and seq no already exist in the collection.
   if P_ss_idx.EXISTS(L_ss_idx) then
      return TRUE;
   end if;

   P_ss_size                           := P_ss_size + 1;
   P_ss_size_auto                      := P_ss_size;
   P_ss_shipment(P_ss_size)            := I_shipment;
   P_ss_seq_no(P_ss_size)              := I_seq_no;
   P_ss_item(P_ss_size)                := I_item;
   P_ss_distro_no(P_ss_size)           := I_distro_no;
   P_ss_distro_type(P_ss_size)         := I_distro_type;
   P_ss_ref_item(P_ss_size)            := I_ref_item;
   P_ss_inv_status(P_ss_size)          := I_inv_status;
   P_ss_qty_received(P_ss_size)        := I_rcv_qty;
   P_ss_unit_cost(P_ss_size)           := I_cost;
   P_ss_unit_retail(P_ss_size)         := I_retail;
   P_ss_qty_expected(P_ss_size)        := I_exp_qty;
   P_ss_weight_expected(P_ss_size)     := I_exp_weight;
   P_ss_weight_expected_uom(P_ss_size) := I_exp_weight_uom;
   P_ss_adjust_type(P_ss_size)         := I_adjust_type;
   --
   P_ss_idx(L_ss_idx)                  := I_item;

   --if carton is not specified then it will always use the shipment number
   --or it will take the carton number

   if I_carton IS NULL then
      P_ss_carton(P_ss_size) := I_shipment;
   else
      P_ss_carton(P_ss_size) := I_carton;
   end if;

   --when adjust type is passed as a non-null value, reconcile date and user should be set
   if I_adjust_type IS NOT NULL then
      P_ss_reconcile_user_id(P_ss_size) := USER;
      P_ss_reconcile_date(P_ss_size) := get_vdate();
   else
      P_ss_reconcile_user_id(P_ss_size) := NULL;
      P_ss_reconcile_date(P_ss_size) := NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'BOL_SQL.INS_SHIPSKU',
                                            to_char(SQLCODE));
      return FALSE;
END INS_SHIPSKU;

-------------------------------------------------------------------------------
FUNCTION UPDATE_INV_STATUS(O_error_message IN OUT VARCHAR2,
                           I_item          IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                           I_from_loc      IN     ITEM_LOC.LOC%TYPE,
                           I_from_loc_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                           I_qty           IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_inv_status    IN     SHIPSKU.INV_STATUS%TYPE)
RETURN BOOLEAN IS

   L_pgm_name            TRAN_DATA.PGM_NAME%TYPE  := 'BOL_SQL.UPDATE_INV_STATUS';
   L_tran_code           TRAN_DATA.TRAN_CODE%TYPE := 25;
   L_neg_transferred_qty TSFDETAIL.TSF_QTY%TYPE   := NULL;
   L_found               BOOLEAN;

BEGIN
   -- make the transferred_qty negative
   L_neg_transferred_qty := (I_qty * -1);

   if INVADJ_SQL.BUILD_ADJ_UNAVAILABLE(O_error_message,
                                       L_found,
                                       I_item,
                                       I_inv_status,
                                       I_from_loc_type,
                                       I_from_loc,
                                       L_neg_transferred_qty) = FALSE then
      return FALSE;
   end if;

   -- insert a tran_data record (code 25)
   if INVADJ_SQL.BUILD_ADJ_TRAN_DATA(O_error_message,
                                     L_found,
                                     I_item,
                                     I_from_loc_type,
                                     I_from_loc,
                                     L_neg_transferred_qty,
                                     NULL,    -- weight
                                     NULL,    -- weight_uom
                                     NULL,    -- I_order_no
                                     L_pgm_name,
                                     LP_vdate,
                                     L_tran_code,
                                     NULL,
                                     I_inv_status,
                                     NULL,    -- wac
                                     NULL,    -- unit_retail
                                     I_pack_ind) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_INV_STATUS;
-------------------------------------------------------------------------------
FUNCTION UPD_FROM_ITEM_LOC(O_error_message    IN OUT VARCHAR2,
                           I_item             IN     ITEM_MASTER.ITEM%TYPE,
                           I_comp_item        IN     VARCHAR2,
                           I_sellable_ind     IN     ITEM_MASTER.SELLABLE_IND%TYPE,
                           I_item_xform_ind   IN     ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                           I_inventory_ind    IN     ITEM_MASTER.INVENTORY_IND%TYPE,
                           I_from_loc         IN     ITEM_LOC.LOC%TYPE,
                           I_tsf_qty          IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_resv_qty         IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_tsf_weight       IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                           I_tsf_weight_uom   IN     UOM_CLASS.UOM%TYPE,
                           I_tran_date        IN     PERIOD.VDATE%TYPE,
                           I_eow_date         IN     PERIOD.VDATE%TYPE)
RETURN BOOLEAN IS

   L_rowid                   ROWID;

   L_tsf_qty                 ITEM_LOC_SOH.STOCK_ON_HAND%TYPE; -- hold units or weight
   L_resv_qty                ITEM_LOC_SOH.STOCK_ON_HAND%TYPE; -- hold units or weight
   L_from_loc_type           ITEM_LOC_SOH.LOC_TYPE%TYPE;

   L_stock_count_processed   BOOLEAN := FALSE;
   L_cycle_count             stake_head.cycle_count%TYPE := NULL;
   L_snapshot_cost           item_loc_soh.av_cost%TYPE := 0;
   L_snapshot_retail         item_loc.unit_retail%TYPE := 0;

   L_table                   VARCHAR2(30);
   L_key1                    VARCHAR2(100);
   L_key2                    VARCHAR2(100);
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(Record_Locked, -54);

   -- cursors
   cursor C_LOCK_ILS is
      select ils.loc_type,
             ils.rowid
        from item_loc_soh ils,
             item_loc il
       where ils.item = I_item
         and ils.loc  = I_from_loc
         and il.item  = ils.item
         and il.loc   = ils.loc
      for update of ils.stock_on_hand,
                    ils.pack_comp_soh,
                    ils.tsf_reserved_qty,
                    ils.pack_comp_resv,
                    ils.last_hist_export_date nowait;

BEGIN
   -- skip calculations and update of item_loc for non-stockholding franchise from_locs
   if LP_bol_rec.from_store_type = 'F' and LP_bol_rec.from_stockholding_ind = 'N' then
      return TRUE;
   end if;

   -- update the simple pack catch weight component item's stock buckets with weight
   -- if component item's standard uom is MASS
   if I_tsf_weight is NOT NULL and
      I_tsf_weight_uom is NOT NULL then  -- a simple pack catch weight component item
      if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                               L_tsf_qty,
                                               I_item,
                                               I_tsf_qty,
                                               I_tsf_weight,
                                               I_tsf_weight_uom) = FALSE then
         return FALSE;
      end if;
      L_resv_qty := L_tsf_qty * (I_resv_qty/I_tsf_qty);
   else
      L_tsf_qty := I_tsf_qty;
      L_resv_qty := I_resv_qty;
   end if;

   L_table := 'ITEM_LOC_SOH';
   L_key1 := I_item;
   L_key2 := TO_CHAR(I_from_loc);
   open C_LOCK_ILS;
   fetch C_LOCK_ILS into L_from_loc_type,
                         L_rowid;
   close C_LOCK_ILS;

   if I_tran_date < LP_vdate then
      if STKCNT_ATTRIB_SQL.STOCK_COUNT_PROCESSED(O_error_message,
                                                 L_stock_count_processed,
                                                 L_cycle_count,
                                                 L_snapshot_cost,
                                                 L_snapshot_retail,
                                                 I_tran_date,
                                                 I_item,
                                                 L_from_loc_type,
                                                 I_from_loc) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_stock_count_processed then
      L_tsf_qty := 0;
   end if;

   update item_loc_soh ils
      set ils.stock_on_hand = DECODE(I_comp_item,
                                     'Y', DECODE(L_from_loc_type,
                                                 'S',
                                                 ils.stock_on_hand - L_tsf_qty,
                                                 ils.stock_on_hand),
                                     ils.stock_on_hand - L_tsf_qty),
          ils.pack_comp_soh = DECODE(I_comp_item,
                                     'Y', DECODE(L_from_loc_type,
                                                 'S',
                                                 ils.pack_comp_soh,
                                                 ils.pack_comp_soh - L_tsf_qty),
                                     ils.pack_comp_soh),
          ils.tsf_reserved_qty = DECODE(I_comp_item,
                                        'Y', DECODE(L_from_loc_type,
                                                    'S',
                                                    GREATEST(ils.tsf_reserved_qty - L_resv_qty, 0),
                                                    ils.tsf_reserved_qty),
                                        GREATEST(ils.tsf_reserved_qty - L_resv_qty, 0)),
          ils.pack_comp_resv = DECODE(I_comp_item,
                                      'Y', DECODE(L_from_loc_type,
                                                  'S',
                                                  ils.pack_comp_resv,
                                                  GREATEST(ils.pack_comp_resv - L_resv_qty, 0)),
                                      ils.pack_comp_resv),
          ils.last_hist_export_date = DECODE(ils.loc_type,'W',
                                             DECODE(SIGN(I_eow_date -
                                                         NVL(ils.last_hist_export_date,I_eow_date- 1)),
                                                    1, ils.last_hist_export_date,
                                                    I_eow_date - 7),
                                              ils.last_hist_export_date),
          ils.last_update_id        = LP_user,
          ils.last_update_datetime  = SYSDATE,
          ils.soh_update_datetime = DECODE(I_comp_item, 'Y',
                                           ils.soh_update_datetime,
                                           DECODE(L_tsf_qty, 0,
                                                  ils.soh_update_datetime,
                                                  SYSDATE))
    where ils.rowid = L_rowid
      and nvl(I_inventory_ind, 'Y') = 'Y';  -- I_inventory_ind is NULL for non-pack items

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.UPD_FROM_ITEM_LOC',
                                            to_char(SQLCODE));
      return FALSE;
END UPD_FROM_ITEM_LOC;
-------------------------------------------------------------------------------
FUNCTION UPD_TO_ITEM_LOC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item              IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_no           IN       ITEM_MASTER.ITEM%TYPE,
                         I_percent_in_pack   IN       NUMBER,
                         I_receive_as_type   IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_tsf_type          IN       TSFHEAD.TSF_TYPE%TYPE,
                         I_to_loc            IN       ITEM_LOC.LOC%TYPE,
                         I_to_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_tsf_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         I_exp_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         I_intran_qty        IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         I_weight_cuom       IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                         I_cuom              IN       UOM_CLASS.UOM%TYPE,
                         I_from_loc          IN       ITEM_LOC.LOC%TYPE,
                         I_from_loc_type     IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_prim_charge       IN       ITEM_LOC_SOH.AV_COST%TYPE,
                         I_distro_no         IN       SHIPSKU.DISTRO_NO%TYPE,
                         I_distro_type       IN       SHIPSKU.DISTRO_TYPE%TYPE,
                         I_intercompany      IN       BOOLEAN)
RETURN BOOLEAN IS

   L_upd_av_cost              ITEM_LOC_SOH.AV_COST%TYPE := NULL;
   L_charge_to_loc            ITEM_LOC_SOH.AV_COST%TYPE;
   L_avg_weight_to            ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE := NULL;
   L_soh_curr                 ITEM_LOC_SOH.STOCK_ON_HAND%TYPE  := NULL;
   L_avg_weight_new           ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE := NULL;
   L_tsf_qty                  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE; -- holds unit or weight
   L_intran_qty               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE; -- holds unit or weight
   L_exp_qty                  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_tsf_expected_qty         ITEM_LOC_SOH.TSF_EXPECTED_QTY%TYPE; -- holds unit or weight
   L_pack_comp_exp            ITEM_LOC_SOH.PACK_COMP_EXP%TYPE; -- holds unit or weight
   L_stockholding_ind         STORE.STOCKHOLDING_IND%TYPE;
   L_rowid                    ROWID;
   L_table                    VARCHAR2(30);
   L_key1                     VARCHAR2(100);
   L_key2                     VARCHAR2(100);
   L_finisher                 VARCHAR2(1) := 'N';
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);
   L_from_finisher            BOOLEAN := FALSE;
   L_finisher_name            PARTNER.PARTNER_DESC%TYPE := NULL;
   L_l10n_fin_rec             L10N_FIN_REC := L10N_FIN_REC();

   -- cursors
   cursor C_LOCK_TO_ILS is
      select ils.tsf_expected_qty,
             ils.pack_comp_exp,
             ils.average_weight,
             ils.stock_on_hand+ils.in_transit_qty+ils.pack_comp_intran+ils.pack_comp_soh total_soh,
             ils.rowid
        from item_loc_soh ils
       where ils.item = I_item
         and ils.loc  = I_to_loc
         for update nowait;

BEGIN
   -- skip calculations and update of item_loc for non-stockholding franchise to_locs
   if LP_bol_rec.to_store_type = 'F' and LP_bol_rec.to_stockholding_ind = 'N' then
      return TRUE;
   end if;

   -- for a simple pack catch weight component item, stock buckets will be updated
   -- by weight (in component's suom) if component's suom is MASS, and by units if
   -- component's suom is eaches.
   -- WAC calculation should be based on how stock buckets are updated.
   if I_weight_cuom is NOT NULL and
      I_cuom is NOT NULL then
      if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                               L_tsf_qty,
                                               I_item,
                                               I_tsf_qty,
                                               I_weight_cuom,
                                               I_cuom) = FALSE then
         return FALSE;
      end if;
   else
      L_tsf_qty := I_tsf_qty;
   end if;

   --convert chrg from primary to to_loc's currency
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       NULL,
                                       NULL,
                                       NULL,
                                       I_to_loc,
                                       I_to_loc_type,
                                       NULL,
                                       I_prim_charge,
                                       L_charge_to_loc,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
      return FALSE;
   end if;

   -- skip WAC recalculation for franchise returns
   if LP_bol_rec.franchise_ordret_ind <> WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
      if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                      L_upd_av_cost,
                                      I_distro_no,
                                      I_distro_type,
                                      I_item,
                                      I_pack_no,
                                      I_percent_in_pack,
                                      I_from_loc,
                                      I_from_loc_type,
                                      I_to_loc,
                                      I_to_loc_type,
                                      L_tsf_qty,
                                      I_weight_cuom,
                                      NULL,         --- From Loc WAC
                                      L_charge_to_loc,
                                      I_intercompany,
                                      'T',
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      LP_tran_date) = FALSE then
         return FALSE;
      end if;
   end if;

   L_table := 'ITEM_LOC_SOH';
   L_key1 := I_item;
   L_key2 := TO_CHAR(I_to_loc);
   open C_LOCK_TO_ILS;
   fetch C_LOCK_TO_ILS into L_tsf_expected_qty,
                            L_pack_comp_exp,
                            L_avg_weight_to,
                            L_soh_curr,
                            L_rowid;
   close C_LOCK_TO_ILS;
   if I_tsf_type = 'PL' then
      L_exp_qty := 0;
   else
      L_exp_qty := I_exp_qty;
   end if;

   --If 1st leg went to an external finisher the expected_qty was updated for the component
   --items of a pack rather then pack_comp_exp.
   --Therefore if sending second leg out of the EF we want to decrement tsf_expected_qty for the comp items.
   if I_from_loc_type = 'E' then
      L_tsf_expected_qty :=  L_tsf_expected_qty - L_exp_qty;
   else
      --Do normal processing:  If receiving as type 'P' then decrement pack_comp_exp otherwise tsf_expected_qty.
      if I_receive_as_type = 'P' then
         if I_from_loc_type = 'W' then
            if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                            L_from_finisher,
                                            L_finisher_name,
                                            I_from_loc) = FALSE then
               return FALSE;
            end if;
         end if;
         if (L_from_finisher =TRUE) OR (I_to_loc_type ='E') then
            L_finisher := 'Y';
         end if;
         if L_finisher ='Y' then
            L_tsf_expected_qty := L_tsf_expected_qty - L_exp_qty;
         else
            if I_weight_cuom is NULL and I_cuom is NULL then
               L_pack_comp_exp := L_pack_comp_exp - L_exp_qty;
            end if;
         end if;
      else
         L_tsf_expected_qty := L_tsf_expected_qty - L_exp_qty;

         if (I_pack_no is NOT NULL) and (I_to_loc_type = 'E')  then
            L_pack_comp_exp := L_pack_comp_exp - L_exp_qty;
         end if;

      end if;
   end if;

   -- update the simple pack catch weight component item's stock buckets with weight
   -- if component item's standard uom is MASS

   if I_weight_cuom is NOT NULL and
      I_cuom is NOT NULL then
      -- a simple pack catch weight component item
      L_intran_qty := L_tsf_qty * (I_intran_qty/I_tsf_qty);
      L_pack_comp_exp := L_pack_comp_exp - L_tsf_qty;
      L_tsf_expected_qty := L_tsf_qty * (L_tsf_expected_qty/I_tsf_qty);
     -- calculate new average weight at the receiving location
      if not CATCH_WEIGHT_SQL.CALC_AVERAGE_WEIGHT(O_error_message,
                                                  L_avg_weight_new,
                                                  I_item,
                                                  I_to_loc,
                                                  I_to_loc_type,
                                                  L_soh_curr,
                                                  L_avg_weight_to,
                                                  I_intran_qty,
                                                  I_weight_cuom,
                                                  NULL) then
         return FALSE;
      end if;
   else
      L_intran_qty := I_intran_qty;
   end if;

   update item_loc_soh ils
      set ils.in_transit_qty = DECODE(I_receive_as_type,
                                      'P', DECODE(I_to_loc_type,'E',ils.in_transit_qty + L_intran_qty,ils.in_transit_qty),
                                      ils.in_transit_qty + L_intran_qty),
          ils.pack_comp_intran = DECODE(I_receive_as_type,
                                        'P', DECODE(I_to_loc_type,'E',0, ils.pack_comp_intran + L_intran_qty),
                                        ils.pack_comp_intran),
          ils.tsf_expected_qty = GREATEST(L_tsf_expected_qty, 0),
          ils.pack_comp_exp = GREATEST(L_pack_comp_exp, 0),
          ils.av_cost = NVL(ROUND(L_upd_av_cost, 4), ils.av_cost),
          ils.average_weight = NVL(L_avg_weight_new,ils.average_weight),
          ils.last_update_id        = LP_user,
          ils.last_update_datetime  = SYSDATE
    where ils.rowid = L_rowid;

   if LP_bol_rec.franchise_ordret_ind <> WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
      L_l10n_fin_rec.procedure_key := 'UPDATE_AV_COST';
      L_l10n_fin_rec.source_entity := 'LOC';
      L_l10n_fin_rec.source_id     := I_to_loc;
      L_l10n_fin_rec.source_type   := I_to_loc_type;
      L_l10n_fin_rec.item          := I_item;
      L_l10n_fin_rec.av_cost       := ROUND(L_upd_av_cost, 4);

      --For localized country Brazil, the L10N_SQL.EXEC_FUNCTION will call L10N_BR_FIN_SQL.UPDATE_AV_COST to
      --update the av_cost for all the virtual locations having the same CNPJ number.
      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                 L_l10n_fin_rec) = FALSE then
         return FALSE;
      end if;
   end if;

  return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
         return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.UPD_TO_ITEM_LOC',
                                            to_char(SQLCODE));
     return FALSE;
END UPD_TO_ITEM_LOC;
-------------------------------------------------------------------------------
FUNCTION UPD_TSF_ITEM_COST(O_error_message    IN OUT VARCHAR2,
                           I_item             IN     ITEM_MASTER.ITEM%TYPE,
                           I_ship_qty         IN     TSF_ITEM_COST.SHIPPED_QTY%TYPE,
                           I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE)
   RETURN BOOLEAN IS

   L_rowid                 ROWID;

   L_table                 VARCHAR2(30);
   L_key1                  VARCHAR2(100);
   L_key2                  VARCHAR2(100);
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   -- cursors
   cursor C_LOCK_TIC is
      select rowid
        from tsf_item_cost
       where tsf_no = I_tsf_no
         and item   = I_item
         and (ict_leg_ind = 'Y'
              or ict_leg_ind is NULL)
      for update of shipped_qty nowait;

BEGIN
   L_table := 'TSF_ITEM_COST';
   L_key1 := I_item;
   L_key2 := TO_CHAR(I_tsf_no);
   open C_LOCK_TIC;
   fetch C_LOCK_TIC into L_rowid;
   close C_LOCK_TIC;

   update tsf_item_cost
      set shipped_qty = nvl(shipped_qty,0) + I_ship_qty
    where rowid = L_rowid;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.UPD_TSF_ITEM_COST',
                                            to_char(SQLCODE));
      return FALSE;
END UPD_TSF_ITEM_COST;
-------------------------------------------------------------------------------
FUNCTION UPDATE_PACK_LOCS(O_error_message       IN OUT VARCHAR2,
                          I_item                IN     ITEM_MASTER.ITEM%TYPE,
                          I_from_loc            IN     ITEM_LOC.LOC%TYPE,
                          I_from_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                          I_to_loc              IN     ITEM_LOC.LOC%TYPE,
                          I_to_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                          I_to_receive_as_type  IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                          I_tsf_type            IN     TSFHEAD.TSF_TYPE%TYPE,
                          I_tsf_qty             IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_tsf_weight_cuom     IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                          I_intran_qty          IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_intran_weight_cuom  IN     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                          I_resv_exp_qty        IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_tran_date           IN     PERIOD.VDATE%TYPE)
RETURN BOOLEAN IS

   L_exp_qty                 ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;

   L_table                   VARCHAR2(30);
   L_key1                    VARCHAR2(100);
   L_key2                    VARCHAR2(100);
   L_finisher                VARCHAR2(1) :='N';
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(Record_Locked, -54);

   L_rowid                   ROWID;
   L_avg_weight_from         ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE := NULL;
   L_avg_weight_to           ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE := NULL;
   L_soh_curr                ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := NULL;
   L_avg_weight_new          ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE := NULL;
   L_from_finisher           BOOLEAN := FALSE;
   L_finisher_name           PARTNER.PARTNER_DESC%TYPE := NULL;
   L_update_from_ils         BOOLEAN;
   L_update_to_ils           BOOLEAN;

   L_stock_count_processed   BOOLEAN := FALSE;
   L_cycle_count             stake_head.cycle_count%TYPE := NULL;
   L_snapshot_cost           item_loc_soh.av_cost%TYPE := 0;
   L_snapshot_retail         item_loc.unit_retail%TYPE := 0;
   L_tsf_qty                 ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;

   L_from_receive_as_type    ITEM_LOC.RECEIVE_AS_TYPE%TYPE;

   -- cursors
   cursor C_LOCK_FROM_ITEM_LOC is
      select ils.average_weight,
             ils.stock_on_hand+ils.in_transit_qty+ils.pack_comp_intran+ils.pack_comp_soh total_soh,
             ils.rowid
        from item_loc_soh ils
       where ils.loc  = I_from_loc
         and ils.item = I_item
         for update nowait;

   cursor C_LOCK_TO_ITEM_LOC is
      select ils.average_weight,
             ils.stock_on_hand+ils.in_transit_qty+ils.pack_comp_intran+ils.pack_comp_soh total_soh,
             ils.rowid
        from item_loc_soh ils
       where ils.loc  = I_to_loc
         and ils.item = I_item
         for update nowait;

   cursor C_FROM_RCV_AS_TYPE is
      select nvl(receive_as_type, 'E')
        from item_loc
       where item = I_item
         and loc = I_from_loc;
BEGIN

   L_table := 'ITEM_LOC_SOH';
   L_key1 := I_item;
   L_key2 := TO_CHAR(I_from_loc);

   open C_FROM_RCV_AS_TYPE;
   fetch C_FROM_RCV_AS_TYPE into L_from_receive_as_type;
   close C_FROM_RCV_AS_TYPE;

   if LP_bol_rec.from_store_type = 'F' and LP_bol_rec.from_stockholding_ind = 'N' then
      L_update_from_ils := FALSE;
   else
      L_update_from_ils := TRUE;
   end if;

   if LP_bol_rec.to_store_type = 'F' and LP_bol_rec.to_stockholding_ind = 'N' then
      L_update_to_ils := FALSE;
   else
      L_update_to_ils := TRUE;
   end if;

   if L_update_from_ils = TRUE then
      if L_from_receive_as_type = 'P' then
         if I_from_loc_type = 'W' then
            if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                            L_from_finisher,
                                            L_finisher_name,
                                            I_from_loc) = FALSE then
               return FALSE;
            end if;
         end if;
         if (L_from_finisher =TRUE) OR (I_from_loc_type ='E') then
            L_finisher := 'Y';
         end if;
         open  C_LOCK_FROM_ITEM_LOC;
         fetch C_LOCK_FROM_ITEM_LOC into L_avg_weight_from,
                                         L_soh_curr,
                                         L_rowid;
         close C_LOCK_FROM_ITEM_LOC;

         if I_tran_date < LP_vdate then
            if STKCNT_ATTRIB_SQL.STOCK_COUNT_PROCESSED(O_error_message,
                                                       L_stock_count_processed,
                                                       L_cycle_count,
                                                       L_snapshot_cost,
                                                       L_snapshot_retail,
                                                       I_tran_date,
                                                       I_item,
                                                       I_from_loc_type,
                                                       I_from_loc) = FALSE then
               return FALSE;
            end if;
         end if;

         if L_stock_count_processed then
            L_tsf_qty := 0;
         else
            L_tsf_qty := I_tsf_qty;
         end if;

         update item_loc_soh ils
            set ils.stock_on_hand    = DECODE(L_finisher, 'Y',GREATEST(ils.stock_on_hand - L_tsf_qty,0),
                                              ils.stock_on_hand - L_tsf_qty),
                ils.tsf_reserved_qty = GREATEST(ils.tsf_reserved_qty - I_resv_exp_qty, 0),
                ils.soh_update_datetime = DECODE(L_tsf_qty,
                                                 0, soh_update_datetime,
                                                 SYSDATE),
                ils.last_update_datetime = SYSDATE,
                ils.last_update_id = LP_user
          where ils.rowid = L_rowid;
      end if;
   end if;

   if L_update_to_ils = TRUE then
      --If 'to' loc is IF/EF, the I_to_receive_as_type will be 'E', so it will not execute this
      if I_to_loc_type = 'W' and I_to_receive_as_type = 'P' then

         L_key2 := TO_CHAR(I_to_loc);
         if I_tsf_type = 'PL' then
            L_exp_qty := 0;
         else
            L_exp_qty := I_resv_exp_qty;
         end if;

         open  C_LOCK_TO_ITEM_LOC;
         fetch C_LOCK_TO_ITEM_LOC into L_avg_weight_to,
                                       L_soh_curr,
                                       L_rowid;
         close C_LOCK_TO_ITEM_LOC;

         -- Update average weight for a simple pack catch weight item at to loc.
         L_avg_weight_new := NULL;
         if I_intran_weight_cuom is NOT NULL then
            if not CATCH_WEIGHT_SQL.CALC_AVERAGE_WEIGHT(O_error_message,
                                                        L_avg_weight_new,
                                                        I_item,
                                                        I_to_loc,
                                                        I_to_loc_type,
                                                        L_soh_curr,
                                                        L_avg_weight_to,
                                                        I_intran_qty,
                                                        I_intran_weight_cuom,
                                                        NULL) then
               return FALSE;
            end if;
         end if;

         --If from loc is an external finisher the expected qty was updated for the
         --pack comp items rather than the pack.  If from_loc is EF then don't decrement the pack's exp qty.
         update item_loc_soh ils
            set ils.in_transit_qty   = ils.in_transit_qty + I_intran_qty,
                ils.tsf_expected_qty = DECODE(I_from_loc_type, 'E',
                                              ils.tsf_expected_qty,
                                              GREATEST(ils.tsf_expected_qty - L_exp_qty, 0)),
                ils.average_weight   = NVL(L_avg_weight_new, ils.average_weight),
                ils.last_update_id        = LP_user,
                ils.last_update_datetime  = SYSDATE
          where ils.rowid = L_rowid;

      elsif I_to_loc_type = 'E' then

         open  C_LOCK_TO_ITEM_LOC;
         fetch C_LOCK_TO_ITEM_LOC into L_avg_weight_to,
                                       L_soh_curr,
                                       L_rowid;
         close C_LOCK_TO_ITEM_LOC;

         update item_loc_soh ils
            set ils.tsf_expected_qty      = NVL(GREATEST(ils.tsf_expected_qty - L_exp_qty, 0),0),
                ils.last_update_id        = LP_user,
                ils.last_update_datetime  = SYSDATE
          where ils.rowid = L_rowid;

      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
       O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
        return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.UPDATE_PACK_LOCS',
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_PACK_LOCS;
-------------------------------------------------------------------------------------------
FUNCTION CARTON_EXISTS(O_error_message IN OUT VARCHAR2,
                       O_carton_exists IN OUT BOOLEAN,
                       I_carton        IN     SHIPSKU.CARTON%TYPE,
                       I_bol_no        IN     SHIPMENT.BOL_NO%TYPE)

RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'BOL_SQL.CARTON_EXISTS';
   L_tmp     VARCHAR2(1);

   -- cursors
   cursor C_CARTON_EXISTS is
      select 'x'
        from shipment sh,
             shipsku ss
       where sh.shipment = ss.shipment
         and sh.bol_no = I_bol_no
         and ss.carton = I_carton;

BEGIN

   if I_carton is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_carton','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_bol_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_bol_no','NULL','NOT NULL');
      return FALSE;
   end if;

   open C_CARTON_EXISTS;
   fetch C_CARTON_EXISTS into L_tmp;
   ---
   if (C_CARTON_EXISTS%FOUND) then
      O_carton_exists := TRUE;
   else
      O_carton_exists := FALSE;
   end if;
   ---
   close C_CARTON_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CARTON_EXISTS;
-------------------------------------------------------------------------------
FUNCTION WRITE_ISSUES(O_error_message   IN OUT VARCHAR2,
                      I_item            IN     ITEM_MASTER.ITEM%TYPE,
                      I_from_wh         IN     ITEM_LOC.LOC%TYPE,
                      I_transferred_qty IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_eow_date        IN     PERIOD.VDATE%TYPE,
                      I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                      I_class           IN     ITEM_MASTER.CLASS%TYPE,
                      I_subclass        IN     ITEM_MASTER.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_day           NUMBER(2);
   L_mth           NUMBER(2);
   L_year          NUMBER(4);
   L_day_454       NUMBER(2);
   L_week_454      NUMBER(2);
   L_month_454     NUMBER(2);
   L_year_454      NUMBER(4);
   L_return_code   VARCHAR2(5) := 'none';

   L_issues        ITEM_LOC_HIST.SALES_ISSUES%TYPE := ROUND(I_transferred_qty);

   L_rowid         ROWID;

   L_not_in_array  BOOLEAN := TRUE;

   L_table         VARCHAR2(30);
   L_key1          VARCHAR2(100);
   L_key2          VARCHAR2(100);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_LOC_HIST is
      select ilh.rowid
        from item_loc_hist ilh
       where ilh.item      = I_item
         and ilh.loc       = I_from_wh
         and ilh.eow_date  = I_eow_date
         for update nowait;

BEGIN

   L_day  := TO_NUMBER(TO_CHAR(I_eow_date, 'DD'));
   L_mth  := TO_NUMBER(TO_CHAR(I_eow_date, 'MM'));
   L_year := TO_NUMBER(TO_CHAR(I_eow_date, 'YYYY'));

   CAL_TO_454(L_day,
              L_mth,
              L_year,
              L_day_454,
              L_week_454,
              L_month_454,
              L_year_454,
              L_return_code,
              O_error_message);
   if L_return_code = 'FALSE' then
      return FALSE;
   end if;

   L_table := 'ITEM_LOC_HIST';
   L_key1 := I_item;
   L_key2 := TO_CHAR(I_from_wh);
   open  C_LOCK_ITEM_LOC_HIST;
   fetch C_LOCK_ITEM_LOC_HIST into L_rowid;
   if C_LOCK_ITEM_LOC_HIST%FOUND then
      close C_LOCK_ITEM_LOC_HIST;

      P_upd_ilh_size := P_upd_ilh_size + 1;
      P_upd_ilh_sales_issues(P_upd_ilh_size)   := L_issues;
      P_upd_ilh_rowid_TBL(P_upd_ilh_size)      := L_rowid;

   else
      close C_LOCK_ITEM_LOC_HIST;

      FOR i IN 1..P_ilh_size LOOP
         if P_ilh_item(i) = I_item and
            P_ilh_loc(i) = I_from_wh and
            P_ilh_eow_date(i) = I_eow_date then
            P_ilh_sales_issues(i) := P_ilh_sales_issues(i) + L_issues;
            L_not_in_array := FALSE;
         end if;
      END LOOP;

      if L_not_in_array then
         P_ilh_size := P_ilh_size + 1;
         P_ilh_item(P_ilh_size)                  := I_item;
         P_ilh_loc(P_ilh_size)                   := I_from_wh;
         P_ilh_loc_type(P_ilh_size)              := 'W';
         P_ilh_eow_date(P_ilh_size)              := I_eow_date;
         P_ilh_week_454(P_ilh_size)              := L_week_454;
         P_ilh_month_454(P_ilh_size)             := L_month_454;
         P_ilh_year_454(P_ilh_size)              := L_year_454;
         P_ilh_sales_type(P_ilh_size)            := 'I';
         P_ilh_sales_issues(P_ilh_size)          := L_issues;
         P_ilh_create_datetime(P_ilh_size)       := sysdate;
         P_ilh_last_update_datetime(P_ilh_size)  := sysdate;
         P_ilh_last_update_id(P_ilh_size)        := LP_user;
         P_ilh_dept(P_ilh_size)                  := I_dept;
         P_ilh_class(P_ilh_size)                 := I_class;
         P_ilh_subclass(P_ilh_size)              := I_subclass;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.WRITE_ISSUES',
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_ISSUES;
-------------------------------------------------------------------------------
--public function--
FUNCTION RECEIPT_PUT_BOL(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_bol_no            IN     SHIPMENT.BOL_NO%TYPE,
                         I_phy_to_loc        IN     SHIPMENT.TO_LOC%TYPE,
                         I_ship_date         IN     PERIOD.VDATE%TYPE,
                         I_shipment          IN     SHIPMENT.SHIPMENT%TYPE,
                         I_phy_from_loc      IN     ITEM_LOC.LOC%TYPE,
                         I_to_loc_type       IN     TSFHEAD.TO_LOC_TYPE%TYPE,
                         I_from_loc_type     IN     TSFHEAD.FROM_LOC_TYPE%TYPE,
                         I_tsf_no            IN     TSFHEAD.TSF_NO%TYPE,
                         I_status            IN     TSFHEAD.STATUS%TYPE,
                         I_tsf_type          IN     TSFHEAD.TSF_TYPE%TYPE)
RETURN BOOLEAN IS

   dist_cnt    BINARY_INTEGER        := NULL;
   L_program   VARCHAR2(50)          := 'BOL_SQL.RECEIPT_PUT_BOL';

BEGIN

   if I_bol_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_bol_no',
                                            L_program,NULL);
      return FALSE;
   end if;
   if I_phy_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_phy_from_loc',
                                            L_program,NULL);
      return FALSE;
   end if;
   if I_phy_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_phy_to_loc',
                                            L_program, NULL);
      return FALSE;
   end if;
   if I_ship_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_ship_date',
                                            L_program,NULL );
      return FALSE;
   end if;

   if I_phy_from_loc = I_phy_to_loc then
      O_error_message := SQL_LIB.CREATE_MSG('SAME_LOC',I_phy_from_loc,null,null);
      return FALSE;
   end if;

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_tsf_no',
                                            L_program, NULL);
      return FALSE;
   end if;

   LP_bol_rec := NULL;

   if INIT_BOL_PROCESS(O_error_message) = FALSE then
      return FALSE;
   end if;

   LP_bol_rec.bol_no            := I_bol_no;
   LP_bol_rec.ship_no           := I_shipment;
   LP_bol_rec.phy_from_loc      := I_phy_from_loc;
   LP_bol_rec.phy_from_loc_type := I_from_loc_type;
   LP_bol_rec.phy_to_loc        := I_phy_to_loc;
   LP_bol_rec.phy_to_loc_type   := I_to_loc_type;
   LP_bol_rec.tran_date         := I_ship_date;

   if DATES_SQL.GET_EOW_DATE(O_error_message,
                             LP_bol_rec.eow_date,
                             I_ship_date) = FALSE then
      return FALSE;
   end if;

   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                LP_bol_rec.franchise_ordret_ind,
                                                I_phy_from_loc,
                                                I_from_loc_type,
                                                I_phy_to_loc,
                                                I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   if I_from_loc_type = 'S' then
      open C_GET_STORE_TYPE(I_phy_from_loc);
      fetch C_GET_STORE_TYPE into LP_bol_rec.from_store_type,
                                  LP_bol_rec.from_stockholding_ind;
      close C_GET_STORE_TYPE;
   end if;

   if I_to_loc_type = 'S' then
      open C_GET_STORE_TYPE(I_phy_to_loc);
      fetch C_GET_STORE_TYPE into LP_bol_rec.to_store_type,
                                  LP_bol_rec.to_stockholding_ind;
      close C_GET_STORE_TYPE;
   end if;

   /* reset the distro array */
   LP_bol_rec.distros.DELETE;
   dist_cnt := 1;

   LP_bol_rec.distros(dist_cnt).tsf_no := I_tsf_no;
   LP_bol_rec.distros(dist_cnt).tsf_status := I_status;
   LP_bol_rec.distros(dist_cnt).tsf_type := I_tsf_type;
   LP_bol_rec.distros(dist_cnt).new_tsf_ind := 'N';

   LP_receipt_ind := 'Y';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END RECEIPT_PUT_BOL;
-------------------------------------------------------------------------------
--public function--
FUNCTION RECEIPT_PUT_TSF_ITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_tsf_seq_no         IN OUT   TSFDETAIL.TSF_SEQ_NO%TYPE,
                              O_ss_seq_no          IN OUT   SHIPSKU.SEQ_NO%TYPE,
                              I_tsf_no             IN       TSFHEAD.TSF_NO%TYPE,
                              I_item               IN       ITEM_MASTER.ITEM%TYPE,
                              I_carton             IN       SHIPSKU.CARTON%TYPE,
                              I_qty                IN       TSFDETAIL.TSF_QTY%TYPE,
                              I_weight             IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_weight_uom         IN       UOM_CLASS.UOM%TYPE,
                              I_inv_status         IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                              I_phy_from_loc       IN       ITEM_LOC.LOC%TYPE,
                              I_from_loc_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                              I_phy_to_loc         IN       ITEM_LOC.LOC%TYPE,
                              I_to_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                              I_tsfhead_to_loc     IN       ITEM_LOC.LOC%TYPE,
                              I_tsfhead_from_loc   IN       ITEM_LOC.LOC%TYPE,
                              I_tsf_type           IN       TSFHEAD.TSF_TYPE%TYPE,
                              I_ref_item           IN       ITEM_MASTER.ITEM%TYPE,
                              I_dept               IN       ITEM_MASTER.DEPT%TYPE,
                              I_class              IN       ITEM_MASTER.CLASS%TYPE,
                              I_subclass           IN       ITEM_MASTER.SUBCLASS%TYPE,
                              I_pack_ind           IN       ITEM_MASTER.PACK_IND%TYPE,
                              I_pack_type          IN       ITEM_MASTER.PACK_TYPE%TYPE,
                              I_simple_pack_ind    IN       ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                              I_catch_weight_ind   IN       ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                              I_supp_pack_size     IN       ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                              I_sellable_ind       IN       ITEM_MASTER.SELLABLE_IND%TYPE,
                              I_item_xform_ind     IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(50)            := 'BOL_SQL.RECEIPT_PUT_TSF_ITEM';

   L_tsf_qty               TSFDETAIL.TSF_QTY%TYPE  := NULL;
   L_ship_qty              TSFDETAIL.SHIP_QTY%TYPE := NULL;
   L_tsf_exist             VARCHAR2(1)             := NULL;
   L_wf_order_no           TSFHEAD.WF_ORDER_NO%TYPE;
   L_rma_no                TSFHEAD.RMA_NO%TYPE;
   L_tsf_status            TSFHEAD.STATUS%TYPE;
   L_new_tsf_status        TSFHEAD.STATUS%TYPE;
   L_tsf_type              TSFHEAD.TSF_TYPE%TYPE;
   L_pwh_ind               VARCHAR2(1)             := 'N';

   dist_cnt                BINARY_INTEGER          := 1;
   item_cnt                BINARY_INTEGER          := 1;

   L_system_options_row    SYSTEM_OPTIONS%ROWTYPE;

   cursor C_BOL_EXISTS_SEQ is
      select NVL(max(seq_no +1), 1)
        from shipsku
       where shipment =  LP_bol_rec.ship_no
         and item = I_item;

   cursor C_TSFDETAIL_EXISTS is
      select 'x'
        from tsfdetail
       where tsf_no = I_tsf_no
         and item   = I_item;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_tsf_no',
                                            L_program,NULL);
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',
                                            L_program,NULL);
      return FALSE;
   end if;
   if I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_qty',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   dist_cnt := LP_bol_rec.distros.COUNT;
   item_cnt := LP_bol_rec.distros(dist_cnt).bol_items.COUNT;
   item_cnt := item_cnt + 1;

   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item           := I_item;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ref_item       := I_ref_item;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).dept           := I_dept;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).class          := I_class;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).subclass       := I_subclass;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_ind       := I_pack_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).pack_type      := I_pack_type;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).simple_pack_ind  := I_simple_pack_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).catch_weight_ind := I_catch_weight_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).supp_pack_size := I_supp_pack_size;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).sellable_ind  := I_sellable_ind;
   LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).item_xform_ind := I_item_xform_ind;

      -- Perform check to avoid shipsku PK violation.
   open C_BOL_EXISTS_SEQ;
   fetch C_BOL_EXISTS_SEQ into LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ss_seq_no;
   if C_BOL_EXISTS_SEQ%NOTFOUND then
      if NEXT_SS_SEQ_NO(O_error_message,
                        LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ss_seq_no) = FALSE then
         close C_BOL_EXISTS_SEQ;
         return FALSE;
      end if;
   end if;

   close C_BOL_EXISTS_SEQ;

   O_ss_seq_no := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ss_seq_no;

   L_tsf_qty := 0;
   L_ship_qty := 0;

   open C_TSFDETAIL_EXISTS;
   fetch C_TSFDETAIL_EXISTS into L_tsf_exist;
   close C_TSFDETAIL_EXISTS;

   if L_tsf_exist is NULL then
      --this item is not on the transfer, insert to TSFDETAIL
      if INS_TSFDETAIL(O_error_message,
                       O_tsf_seq_no,
                       I_tsf_no,
                       I_item,
                       I_supp_pack_size,
                       I_inv_status,

                       L_tsf_qty) = FALSE then
         return FALSE;
      end if;
   end if; -- item does not exist on transfer

   --check for physical wh in CO transfers
   if I_tsf_type = 'CO' then
      if I_from_loc_type = 'W' and I_tsfhead_from_loc = I_phy_from_loc then
         L_pwh_ind := 'Y';
      end if;
      if L_pwh_ind = 'N' then
         if I_to_loc_type = 'W' and I_tsfhead_to_loc = I_phy_to_loc then
            L_pwh_ind := 'Y';
         end if;
      end if;
   end if;

   if I_tsf_type NOT in ('EG','CO') or                              --non-EG/CO transfer will NOT have physical wh
      I_tsf_type = 'CO' and L_pwh_ind = 'N' or                      --CO transfers having NO physical wh
      I_from_loc_type != 'W' and I_to_loc_type != 'W' then          --transfers do NOT involve a warehouse
      if TRANSFER_CHARGE_SQL.DEFAULT_CHRGS(
                        O_error_message,
                        I_tsf_no,
                        I_tsf_type,
                        O_tsf_seq_no,
                        NULL,              --ship_no     --only populated for tsfs with physical WH
                        NULL,              --ship_seq_no --only populated for tsfs with physical WH
                        I_tsfhead_from_loc,
                        I_from_loc_type,
                        I_tsfhead_to_loc,
                        I_to_loc_type,
                        I_item) = FALSE then
         return FALSE;
      end if;
   end if;

   if TSF_ITEM_COMMON(O_error_message,
                      I_tsf_no,
                      I_carton,
                      I_qty,
                      I_weight,
                      I_weight_uom,
                      I_phy_from_loc,
                      I_from_loc_type,
                      I_phy_to_loc,
                      I_to_loc_type,
                      I_tsfhead_to_loc,
                      I_tsfhead_from_loc,
                      I_tsf_type,
                      I_inv_status,
                      L_tsf_qty,
                      L_ship_qty,
                      O_tsf_seq_no,
                      item_cnt,
                      dist_cnt) = FALSE then
      return FALSE;
   end if;

   if LP_bol_rec.franchise_ordret_ind in (WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER,WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN) then

      select status,
             tsf_type
        into L_tsf_status,
             L_tsf_type
        from tsfhead
       where tsf_no = I_tsf_no;

      if LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then -- receipt of a franchise order
         if WF_TRANSFER_SQL.SYNC_F_ORDER(O_error_message,
                                         L_wf_order_no,
                                         L_new_tsf_status,
                                         RMS_CONSTANTS.WF_ACTION_SHIPPED,
                                         I_tsf_no,
                                         L_tsf_status,
                                         L_tsf_type) = FALSE then
            return FALSE;
         end if;

      elsif LP_bol_rec.franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then -- receipt of a franchise return
         if WF_TRANSFER_SQL.SYNC_F_RETURN(O_error_message,
                                          L_rma_no,
                                          RMS_CONSTANTS.WF_ACTION_SHIPPED,
                                          I_tsf_no,
                                          L_tsf_status,
                                          L_tsf_type) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_BOL_EXISTS_SEQ%isopen then
         close C_BOL_EXISTS_SEQ;
      end if;
      if C_TSFDETAIL_EXISTS%isopen then
         close C_TSFDETAIL_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END RECEIPT_PUT_TSF_ITEM;
-------------------------------------------------------------------------------
FUNCTION INIT_BOL_PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN

   LP_ss_seq_no := 0;
   if STKLEDGR_SQL.INIT_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   if INVADJ_SQL.INIT_ALL(O_error_message) = FALSE then
      return FALSE;
   end if;

   P_ss_size               := 0;
   P_ilh_size              := 0;
   P_upd_ilh_size          := 0;

   P_ss_idx.DELETE;

   LP_receipt_ind := 'N';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.INIT_BOL_PROCESS',
                                            to_char(SQLCODE));
      return FALSE;
END INIT_BOL_PROCESS;
-------------------------------------------------------------------------------
FUNCTION FLUSH_BOL_PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN

   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   if INVADJ_SQL.FLUSH_ALL(O_error_message) = FALSE then
      return FALSE;
   end if;

   if FLUSH_SHIPSKU_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   if FLUSH_ILH_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   if FLUSH_ILH_UPDATE(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.FLUSH_BOL_PROCESS',
                                            to_char(SQLCODE));
      return FALSE;
END FLUSH_BOL_PROCESS;
-------------------------------------------------------------------------------
FUNCTION AUTO_RCV_LINE_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_ss_exp_weight            SHIPSKU.WEIGHT_EXPECTED%TYPE := NULL;
   L_ss_exp_weight_uom        SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE := NULL;
   dist_cnt                   BINARY_INTEGER := 1;
   item_cnt                   BINARY_INTEGER := 1;

BEGIN
   -- before auto receiving, first flush the result from shipment process so that receiving can act on the correct data
   if BOL_SQL.FLUSH_BOL_PROCESS(O_error_message) = FALSE then
      return FALSE;
   end if;

   -- write weight on the message to shipsku weight_expected
   if (LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).simple_pack_ind = 'Y'
      and LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).catch_weight_ind = 'Y') then
      if (LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).weight is NOT NULL
         and LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).weight_uom is NOT NULL) then
         L_ss_exp_weight := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).weight;
         L_ss_exp_weight_uom := LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).weight_uom;
      end if;
   end if;

   for i in 1..P_ss_size_auto LOOP
      --transfer only, exclude customer orders that are directly shipped to customers
      if P_ss_distro_type(i) = 'T' then
         if STOCK_ORDER_RCV_SQL.INIT_TSF_ALLOC_GROUP(O_error_message) = FALSE then
            return FALSE;
         end if;
         ---
         if STOCK_ORDER_RCV_SQL.TSF_LINE_ITEM(O_error_message,
                                              LP_bol_rec.phy_to_loc,
                                              NVL(P_ss_ref_item(i),P_ss_item(i)),
                                              P_ss_qty_expected(i),
                                              P_ss_weight_expected(i),
                                              P_ss_weight_expected_uom(i),
                                              'R',
                                              LP_bol_rec.tran_date,
                                              NULL,         --I_RECEIPT_NUMBER,
                                              LP_bol_rec.bol_no,
                                              NULL,         --I_APPT_NO,
                                              P_ss_carton(i),
                                              'T',
                                              P_ss_distro_no(i),
                                              NULL,        --I_DISP,
                                              NULL,
                                              NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if STOCK_ORDER_RCV_SQL.FINISH_TSF_ALLOC_GROUP(O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;
      --allocation only
      if P_ss_distro_type(i) = 'A' then
         ---
         if STOCK_ORDER_RCV_SQL.INIT_TSF_ALLOC_GROUP(O_error_message) = FALSE then
            return FALSE;
         end if;
         ---
         if STOCK_ORDER_RCV_SQL.ALLOC_LINE_ITEM(O_error_message,
                                                LP_bol_rec.distros(dist_cnt).bol_items(item_cnt).ad_to_loc_phy,
                                                NVL(P_ss_ref_item(i),P_ss_item(i)),
                                                P_ss_qty_expected(i),
                                                L_ss_exp_weight,
                                                L_ss_exp_weight_uom,
                                                'R',
                                                LP_bol_rec.tran_date,
                                                NULL,                       --I_RECEIPT_NUMBER,
                                                LP_bol_rec.bol_no,
                                                NULL,                       --I_APPT_NO,
                                                P_ss_carton(i),
                                                'A',
                                                P_ss_distro_no(i),
                                                NULL,                        --I_DISP,
                                                NULL,
                                                NULL,
                                                NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if STOCK_ORDER_RCV_SQL.FINISH_TSF_ALLOC_GROUP(O_error_message) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.AUTO_RCV_LINE_ITEM',
                                            to_char(SQLCODE));
      return FALSE;
END AUTO_RCV_LINE_ITEM;
-------------------------------------------------------------------------------
FUNCTION NEXT_SS_SEQ_NO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_ss_seq_no       IN OUT   SHIPSKU.SEQ_NO%TYPE)
RETURN BOOLEAN IS

BEGIN
   LP_ss_seq_no := LP_ss_seq_no + 1;
   O_ss_seq_no := LP_ss_seq_no;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.NEXT_SS_SEQ_NO',
                                            to_char(SQLCODE));
      return FALSE;
END NEXT_SS_SEQ_NO;
-------------------------------------------------------------------------------
FUNCTION FLUSH_SHIPSKU_INSERT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN
   if P_ss_size > 0 then
      FORALL i IN 1..P_ss_size
         insert into shipsku(shipment,
                             seq_no,
                             item,
                             distro_no,
                             distro_type,
                             ref_item,
                             carton,
                             inv_status,
                             status_code,
                             qty_received,
                             unit_cost,
                             unit_retail,
                             qty_expected,
                             match_invc_id,
                             weight_expected,
                             weight_expected_uom,
                             adjust_type,
                             reconcile_user_id,
                             reconcile_date)
                      values(P_ss_shipment(i),
                             P_ss_seq_no(i),
                             P_ss_item(i),
                             P_ss_distro_no(i),
                             P_ss_distro_type(i),
                             P_ss_ref_item(i),
                             P_ss_carton(i),
                             P_ss_inv_status(i),
                             'A',
                             P_ss_qty_received(i),
                             P_ss_unit_cost(i),
                             P_ss_unit_retail(i),
                             P_ss_qty_expected(i),
                             NULL,
                             P_ss_weight_expected(i),
                             P_ss_weight_expected_uom(i),
                             P_ss_adjust_type(i),
                             P_ss_reconcile_user_id(i),
                             P_ss_reconcile_date(i));
   end if;

   P_ss_size := 0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.FLUSH_SHIPSKU_INSERT',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FLUSH_SHIPSKU_INSERT;
-------------------------------------------------------------------------------
FUNCTION FLUSH_ILH_INSERT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN

   if P_ilh_size > 0 then
      FORALL i IN 1..P_ilh_size
         insert into item_loc_hist(item,
                                   loc,
                                   loc_type,
                                   eow_date,
                                   week_454,
                                   month_454,
                                   year_454,
                                   sales_type,
                                   sales_issues,
                                   value,
                                   gp,
                                   stock,
                                   retail,
                                   av_cost,
                                   create_datetime,
                                   last_update_datetime,
                                   last_update_id,
                                   dept,
                                   class,
                                   subclass)
                            values(P_ilh_item(i),
                                   P_ilh_loc(i),
                                   P_ilh_loc_type(i),
                                   P_ilh_eow_date(i),
                                   P_ilh_week_454(i),
                                   P_ilh_month_454(i),
                                   P_ilh_year_454(i),
                                   'I',
                                   P_ilh_sales_issues(i),
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   P_ilh_create_datetime(i),
                                   P_ilh_create_datetime(i),
                                   P_ilh_last_update_id(i),
                                   P_ilh_dept(i),
                                   P_ilh_class(i),
                                   P_ilh_subclass(i));
   end if;

   P_ilh_size := 0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.FLUSH_ILH_INSERT',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FLUSH_ILH_INSERT;
-------------------------------------------------------------------------------
FUNCTION FLUSH_ILH_UPDATE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN

   if P_upd_ilh_size > 0 then
      FORALL i IN 1..P_upd_ilh_size
         update item_loc_hist ilh
            set sales_issues = ilh.sales_issues + P_upd_ilh_sales_issues(i)
          where ilh.rowid = P_upd_ilh_rowid_TBL(i);

   end if;

   P_upd_ilh_size := 0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BOL_SQL.FLUSH_ILH_UPDATE',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FLUSH_ILH_UPDATE;
-----------------------------------------------------------------------------------------------
FUNCTION PUT_TSF_AV_RETAIL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_shipment_retail   IN       TSFDETAIL.FINISHER_AV_RETAIL%TYPE,
                           I_shipment_qty      IN       TSFDETAIL.FINISHER_UNITS%TYPE,
                           I_tsf_no            IN       TSFDETAIL.TSF_NO%TYPE,
                           I_item              IN       TSFDETAIL.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'BOL_SQL.PUT_TSF_AV_RETAIL';
   L_old_retail        TSFDETAIL.FINISHER_AV_RETAIL%TYPE;
   L_new_retail        TSFDETAIL.FINISHER_AV_RETAIL%TYPE;
   L_old_qty           TSFDETAIL.FINISHER_UNITS%TYPE;
   L_new_qty           TSFDETAIL.FINISHER_UNITS%TYPE;
   L_shipment_retail   TSFDETAIL.FINISHER_AV_RETAIL%TYPE;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_OLD_TSF_RETAIL is
      select nvl(finisher_av_retail,0),
             nvl(finisher_units,0)
        from tsfdetail
       where tsf_no = I_tsf_no
         and item   = I_item
         for update nowait;

BEGIN

   if I_shipment_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_shipment_qty',
                                            'NULL');
      return FALSE;
   elsif I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_tsf_no',
                                            'NULL');
      return FALSE;
   elsif I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_item',
                                            'NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_OLD_TSF_RETAIL',
                    'TSFDETAIL',
                    'Tsf_no: '||to_char(I_tsf_no)||',Item: '||I_item);
   open C_OLD_TSF_RETAIL;

   SQL_LIB.SET_MARK('FETCH',
                    'C_OLD_TSF_RETAIL',
                    'TSFDETAIL',
                    'Tsf_no: '||to_char(I_tsf_no)||',Item: '||I_item);
   fetch C_OLD_TSF_RETAIL into L_old_retail,
                               L_old_qty;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_OLD_TSF_RETAIL',
                    'TSFDETAIL',
                    'Tsf_no: '||to_char(I_tsf_no)||',Item: '||I_item);
   close C_OLD_TSF_RETAIL;

   if I_shipment_retail is NULL then
      L_shipment_retail := 0;
   else
      L_shipment_retail := I_shipment_retail;
   end if;

   L_new_qty   := L_old_qty + I_shipment_qty;

   if L_new_qty = 0 then
      L_new_retail := NULL;
      L_new_qty    := NULL;
   else
      L_new_retail := ((L_old_qty*L_old_retail) + (I_shipment_qty*L_shipment_retail))/L_new_qty;
      if L_new_retail = 0 then
         L_new_retail := NULL;
      end if;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                    'TSDETAIL',
                    'Tsf_no: '||to_char(I_tsf_no)||',Item: '||I_item,
                    NULL);
   update tsfdetail
      set finisher_av_retail = L_new_retail,
          finisher_units     = L_new_qty,
          updated_by_rms_ind = 'N'
    where tsf_no = I_tsf_no
      and item   = I_item;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORDER_LOCKED',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PUT_TSF_AV_RETAIL;
-----------------------------------------------------------------------------------------------
FUNCTION PUT_ILS_AV_RETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_location        IN       ITEM_LOC_SOH.LOC%TYPE,
                           I_loc_type        IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                           I_item            IN       TSFDETAIL.ITEM%TYPE,
                           I_shipment        IN       SHIPSKU.SHIPMENT%TYPE,
                           I_tsf_no          IN       SHIPSKU.DISTRO_NO%TYPE,
                           I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE,
                           I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE)
   RETURN BOOLEAN is

   L_program                   VARCHAR2(64) := 'BOL_SQL.PUT_ILS_AV_RETAIL';
   L_old_retail                TSFDETAIL.FINISHER_AV_RETAIL%TYPE;
   L_new_retail                TSFDETAIL.FINISHER_AV_RETAIL%TYPE;
   L_old_qty                   TSFDETAIL.FINISHER_UNITS%TYPE;
   L_new_qty                   TSFDETAIL.FINISHER_UNITS%TYPE;
   L_qty_rcv                   SHIPSKU.QTY_EXPECTED%TYPE;
   L_qty_snd                   SHIPSKU.QTY_EXPECTED%TYPE;
   L_shipment_qty              TSFDETAIL.FINISHER_UNITS%TYPE := 0;
   L_shipment_retail           SHIPSKU.UNIT_RETAIL%TYPE := 0;
   L_ship_unit_retail          SHIPSKU.UNIT_RETAIL%TYPE := 0;
   L_pack_unit_retail          SHIPSKU.UNIT_RETAIL%TYPE := 0;
   L_temp_unit_retail          SHIPSKU.UNIT_RETAIL%TYPE := 0;
   L_ship_qty_received         SHIPSKU.QTY_RECEIVED%TYPE := 0;
   L_ship_qty_expected         SHIPSKU.QTY_EXPECTED%TYPE := 0;
   L_finisher_loc              ITEM_LOC_SOH.LOC%TYPE;
   L_finisher_loc_type         ITEM_LOC_SOH.LOC_TYPE%TYPE;
   L_finisher_entity           WH.TSF_ENTITY_ID%TYPE;
   L_finisher                  VARCHAR2(1)  :=  NULL;
   L_first_leg_ind             VARCHAR2(1)  :=  'N';
   L_tsf_mkdn_code             SYSTEM_OPTIONS.TSF_MD_STORE_TO_STORE_SND_RCV%TYPE;
   L_md_store_to_store         SYSTEM_OPTIONS.TSF_MD_STORE_TO_STORE_SND_RCV%TYPE;
   L_md_store_to_wh            SYSTEM_OPTIONS.TSF_MD_STORE_TO_WH_SND_RCV%TYPE;
   L_md_wh_to_store            SYSTEM_OPTIONS.TSF_MD_WH_TO_STORE_SND_RCV%TYPE;
   L_md_wh_to_wh               SYSTEM_OPTIONS.TSF_MD_WH_TO_WH_SND_RCV%TYPE;
   L_from_loc_type             TSFHEAD.FROM_LOC_TYPE%TYPE;
   L_final_loc_type            TSFHEAD.TO_LOC_TYPE%TYPE;
   L_xform_ind                 VARCHAR2(1)  :=  'N';
   L_item                      ITEM_MASTER.ITEM%TYPE  := I_item;
   L_item_qty                  PACKITEM.PACK_QTY%TYPE;
   L_xform_item                ITEM_MASTER.ITEM%TYPE;
   L_tsf_no                    TSFHEAD.TSF_NO%TYPE    := I_tsf_no;
   L_pack_ind                  VARCHAR2(1)  := 'N';
   L_item_ship_ind             VARCHAR2(1)  := 'N';
   L_ship_status               SHIPMENT.STATUS_CODE%TYPE;
   L_pack_comp_count           NUMBER(12)   := 1;
   L_pack_item                 ITEM_MASTER.ITEM%TYPE;
   L_tsf_item                  ITEM_MASTER.ITEM%TYPE;
   L_snd_loc                   ITEM_LOC.LOC%TYPE;
   L_snd_loc_type              ITEM_LOC.LOC_TYPE%TYPE;
   L_rcv_loc                   ITEM_LOC.LOC%TYPE;
   L_rcv_loc_type              ITEM_LOC.LOC_TYPE%TYPE;
   L_first_pack_comp           ITEM_MASTER.ITEM%TYPE;
   L_sum_shipped_tsf           SHIPSKU.QTY_EXPECTED%TYPE := 0;
   L_sum_received_tsf          SHIPSKU.QTY_RECEIVED%TYPE := 0;
   L_tsf_units                 TSFDETAIL.FINISHER_UNITS%TYPE := 0;
   L_itemloc                   ITEM_LOC%ROWTYPE;
   L_comp_bulk_ind             VARCHAR2(1) := 'N';
   L_bulk_qty_received         SHIPSKU.QTY_RECEIVED%TYPE := 0;
   L_bulk_qty_expected         SHIPSKU.QTY_EXPECTED%TYPE := 0;
   L_comp_qty_received         SHIPSKU.QTY_RECEIVED%TYPE := 0;
   L_comp_qty_expected         SHIPSKU.QTY_EXPECTED%TYPE := 0;
   L_bulk_unit_retail          SHIPSKU.UNIT_RETAIL%TYPE := 0;
   L_comp_unit_retail          SHIPSKU.UNIT_RETAIL%TYPE := 0;
   L_pack_tsf_units            TSFDETAIL.FINISHER_UNITS%TYPE := 0;
   L_bulk_tsf_units            TSFDETAIL.FINISHER_UNITS%TYPE := 0;
   L_pack_sum_shipped_tsf      SHIPSKU.QTY_EXPECTED%TYPE := 0;
   L_pack_sum_received_tsf     SHIPSKU.QTY_RECEIVED%TYPE := 0;
   L_pack_shipment_retail      SHIPSKU.UNIT_RETAIL%TYPE := 0;
   L_pack_shipment_qty         SHIPSKU.QTY_RECEIVED%TYPE := 0;
   L_bulk_shipment_retail      SHIPSKU.UNIT_RETAIL%TYPE := 0;
   L_bulk_shipment_qty         SHIPSKU.QTY_RECEIVED%TYPE := 0;
   L_av_cost                   ITEM_LOC_SOH.AV_COST%TYPE := 0;
   L_unit_cost                 ITEM_LOC_SOH.UNIT_COST%TYPE := 0;
   L_selling_unit_retail       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE := 0;
   L_selling_uom               ITEM_LOC.SELLING_UOM%TYPE := 0;
   RECORD_LOCKED               EXCEPTION;
   PRAGMA                      EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_base_unit_retail          ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom_zon          ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_zon   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_zon           ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_zon           ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_zon     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_zon     ITEM_LOC.MULTI_SELLING_UOM%TYPE;
   L_tsf_shipment_ind          VARCHAR2(1) := 'Y';
   L_tsf_unit_retail           TSFDETAIL.FINISHER_AV_RETAIL%TYPE := 0;
   L_comp_item                 ITEM_MASTER.ITEM%TYPE;
   L_sum_pack_comp             NUMBER(12) := 0;
   L_tsf_qty                   TSFDETAIL.TSF_QTY%TYPE := 0;
   L_phy_wh                    BOOLEAN := FALSE;
   L_vir_wh                    ITEM_LOC.LOC%TYPE;

   cursor C_GET_TSF_MKDN_CODE is
      select tsf_md_store_to_store_snd_rcv,
             tsf_md_store_to_wh_snd_rcv,
             tsf_md_wh_to_store_snd_rcv,
             tsf_md_wh_to_wh_snd_rcv
        from system_options;

   cursor C_GET_FROM_LOC_TYPE is
     select from_loc_type
       from tsfhead
      where tsf_parent_no is NULL
        and tsf_no = L_tsf_no;

   cursor C_GET_FINAL_LOC_TYPE is
      select to_loc_type
        from tsfhead
       where tsf_parent_no = L_tsf_no;

   cursor C_CHECK_SHIP_STATUS is
      select status_code
        from shipment
       where shipment = I_shipment;

   cursor C_CHECK_PACK_IND is
      select pack_ind
        from item_master
       where item = I_item;

   cursor C_COUNT_COMP is
      select count(*)
        from packitem
       where pack_no = I_item;

   cursor C_CHECK_ITEM_SHIP is
      select 'Y'
        from shipsku
       where item = I_item
         and shipment = I_shipment;

   cursor C_GET_PACK_ITEM is
      select p.pack_no ,
             p.pack_qty
        from packitem p,
             shipsku sh
       where p.item = I_item
         and p.pack_no = sh.item
         and sh.shipment = I_shipment;

   cursor C_GET_COMP_ITEMS is
      select item,
             pack_qty
        from packitem
       where pack_no = I_item;

   cursor C_FINISHER_ENTITY is
      select temp.tsf_entity_id
        from (select to_number(partner_id) loc, partner_type loc_type, tsf_entity_id
                from partner
               where partner_type = 'E'
               union all
              select wh loc, 'W' loc_type, tsf_entity_id
                from wh
               union all
              select store loc, 'S' loc_type, tsf_entity_id
                from store) temp,
                tsfhead t
             where t.tsf_no = I_tsf_no
               and t.from_loc_type = temp.loc_type
               and t.from_loc = temp.loc
       minus
      select temp.tsf_entity_id
        from (select to_number(partner_id) loc, partner_type loc_type, tsf_entity_id
                from partner
               where partner_type = 'E'
               union all
              select wh loc, 'W' loc_type, tsf_entity_id
                from wh
               union all
              select store loc, 'S' loc_type, tsf_entity_id
                from store) temp,
             tsfhead t
       where t.tsf_no = I_tsf_no
         and t.to_loc_type = temp.loc_type
         and t.to_loc = temp.loc;

   cursor C_CHECK_XFORM is
      select 'Y'
        from tsf_xform tx,
             tsf_xform_detail txd
       where tx.tsf_no = L_tsf_no
         and tx.tsf_xform_id = txd.tsf_xform_id
         and (txd.from_item = I_item or
              txd.to_item   = I_item)
         and rownum = 1;

   cursor C_GET_LOCS is
      select tsf.from_loc,
             tsf.from_loc_type,
             tsf1.to_loc,
             tsf1.to_loc_type
        from tsfhead tsf,
             tsfhead tsf1
       where tsf.tsf_parent_no is NULL
         and tsf.tsf_no = tsf1.tsf_parent_no
         and (tsf.tsf_no = I_tsf_no or
              tsf1.tsf_no = I_tsf_no);

   cursor C_GET_RETAIL_QTY_SHIP is
      select ss.unit_retail,
             nvl(ss.qty_expected,0),
             nvl(ss.qty_received,0)
        from shipsku ss
       where ss.shipment = I_shipment
         and ss.item     = I_item;

   cursor C_GET_RETAIL_QTY_SHIP_PACK_SND is
      select il.unit_retail,
             nvl(ss.qty_expected,0)*p.pack_qty,
             nvl(ss.qty_received,0)*p.pack_qty,
             ss.unit_retail
        from shipsku ss,
             packitem p,
             item_loc il
       where ss.shipment = I_shipment
         and ss.item    = p.pack_no
         and (p.pack_no  = I_item or
              (L_pack_item is NOT NULL and
               p.pack_no = L_pack_item))
         and p.item  = L_item
         and il.item = p.item
         and il.loc  = L_snd_loc
         and il.loc_type = L_snd_loc_type;

   cursor C_GET_RETAIL_QTY_SHIP_PACK_RCV is
      select il.unit_retail,
             nvl(ss.qty_expected,0)*p.pack_qty,
             nvl(ss.qty_received,0)*p.pack_qty,
             ss.unit_retail
        from shipsku ss,
             packitem p,
             item_loc il
       where ss.shipment = I_shipment
         and ss.item    = p.pack_no
         and (p.pack_no  = I_item or
              (L_pack_item is NOT NULL and
               p.pack_no = L_pack_item))
         and p.item  = L_item
         and il.item = p.item
         and il.loc  = L_rcv_loc
         and il.loc_type = L_rcv_loc_type;

   cursor C_GET_OLD_TSF_AVE_RETAIL is
      select nvl(finisher_av_retail, 0)
        from tsfdetail
       where tsf_no = L_tsf_no
         and item   = I_item;

   cursor C_GET_OLD_TSF_AVE_RETAIL_PACK is
      select nvl(il.unit_retail,0),
             nvl(tsf.finisher_av_retail, 0)
        from item_loc il,
             tsfdetail tsf
       where il.item = L_item
         and il.loc  = L_snd_loc
         and il.loc_type = L_snd_loc_type
         and tsf.tsf_no = L_tsf_no
         and tsf.item = L_pack_item;

   cursor C_GET_RETAIL_QTY_XFORM is
      select ss.unit_retail,
             txd.from_qty
        from shipsku ss,
             tsf_xform_detail txd,
             tsf_xform tx
       where ss.shipment       = I_shipment
         and ss.item           = I_item
         and ss.distro_type     = tx.tsf_no
         and tx.tsf_xform_id   = txd.tsf_xform_id
         and txd.from_item = ss.item;

   cursor C_GET_RETAIL_QTY_SEND is
      select il.unit_retail,
             nvl(ss.qty_expected,0),
             nvl(ss.qty_received,0)
        from item_loc il,
             shipsku ss
       where il.item = I_item
         and ss.item = il.item
         and ss.shipment = I_shipment
         and il.loc       = L_snd_loc
         and il.loc_type  = L_snd_loc_type;

   cursor C_GET_RETAIL_QTY_SEND_PACK is
      select il.unit_retail,
             nvl(ss.qty_expected,0)*p.pack_qty,
             nvl(ss.qty_received,0)*p.pack_qty
        from packitem p,
             item_loc il,
             shipsku ss
       where (p.pack_no = I_item or
              (L_pack_item is NOT NULL and
               p.pack_no = L_pack_item))
         and il.item      = p.item
         and p.item       = L_item
         and ss.item      = p.pack_no
         and ss.shipment  = I_shipment
         and il.loc       = L_snd_loc
         and il.loc_type  = L_snd_loc_type;

   cursor C_GET_RETAIL_QTY_RCV is
      select il.unit_retail,
             nvl(ss.qty_expected,0),
             nvl(ss.qty_received,0)
        from item_loc il,
             shipsku ss
       where il.item =I_item
         and il.item = ss.item
         and ss.shipment  = I_shipment
         and il.loc       = L_rcv_loc
         and il.loc_type  = L_rcv_loc_type;

   cursor C_GET_RETAIL_QTY_RCV_PACK is
      select il.unit_retail,
             nvl(ss.qty_expected,0)*p.pack_qty,
             nvl(ss.qty_received,0)*p.pack_qty
        from packitem p,
             item_loc il,
             shipsku ss
       where (p.pack_no = I_item or
              (L_pack_item is NOT NULL and
               p.pack_no = L_pack_item))
         and il.item      = p.item
         and p.item       = L_item
         and ss.item      = p.pack_no
         and ss.shipment  = I_shipment
         and il.loc       = L_rcv_loc
         and il.loc_type  = L_rcv_loc_type;

   cursor C_GET_XFORM_ITEM is
      select txd.from_item
        from tsf_xform_detail txd,
             tsf_xform tx
       where txd.tsf_xform_id = tx.tsf_xform_id
         and tx.tsf_no        = L_tsf_no
         and txd.to_item      = I_item;

   cursor C_GET_QTY_XFORM is
      select nvl(ss.qty_expected,0),
             nvl(ss.qty_received,0)
        from shipsku ss
       where ss.shipment = I_shipment
         and ss.item = I_item
         and ss.distro_type = 'T'
         and ss.distro_no = I_tsf_no;

   cursor C_OLD_RETAIL is
      select nvl(finisher_av_retail,0),
             nvl(finisher_units,0)
        from item_loc_soh
       where item     = L_item
         and loc      = L_finisher_loc
         and loc_type = L_finisher_loc_type
         for update nowait;

   cursor C_GET_FIRST_LEG_IND is
      select 'Y'
        from tsfhead
       where tsf_no = I_tsf_no
         and tsf_parent_no is NULL;

   cursor C_GET_FINISHER is
      select loc, loc_type
        from (select to_number(partner_id) loc, partner_type loc_type
                from partner
               where partner_type = 'E'
               union all
              select wh loc, 'W' loc_type
                from wh
               where finisher_ind = 'Y') temp,
             tsfhead t
       where t.tsf_no = I_tsf_no
         and t.from_loc_type = temp.loc_type
         and t.from_loc = temp.loc
       union all
      select loc, loc_type
        from (select to_number(partner_id) loc, partner_type loc_type
                from partner
               where partner_type = 'E'
               union all
              select wh loc, 'W' loc_type
                from wh
               where finisher_ind = 'Y') temp,
             tsfhead t
       where t.tsf_no = I_tsf_no
         and t.to_loc_type = temp.loc_type
         and t.to_loc = temp.loc;

   cursor C_GET_FIRST_LEG_TSF is
      select tsf_parent_no
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_TSF_RETAIL_PACK is
      select nvl(finisher_av_retail,0)
        from tsfdetail
       where tsf_no = L_tsf_no
         and item   = L_pack_item;

   cursor C_GET_FIRST_COMP_PACK is
      select item
        from packitem
       where pack_no = L_pack_item
         and rownum = 1
       order by item desc;

   cursor C_GET_SUM_SHIPPED_TSF is
      select nvl(sum(nvl(sk.qty_expected,0)),0)
        from shipsku sk,
             shipment sh
       where sk.item = L_item
         and sk.distro_no = I_tsf_no
         and sh.shipment = sk.shipment
         and sh.status_code != 'R'
         and sh.shipment != I_shipment;

   cursor C_GET_SUM_SHIPPED_TSF_PACK is
      select nvl(sum(nvl(sk.qty_expected,0)),0)
        from shipsku sk,
             shipment sh
       where sk.item = L_pack_item
         and sk.distro_no = I_tsf_no
         and sh.shipment = sk.shipment
         and sh.status_code != 'R'
         and sh.shipment != I_shipment;

   cursor C_GET_SUM_RECEIVED_TSF is
      select nvl(sum(nvl(sk.qty_received,0)),0)
        from shipsku sk,
             shipment sh
       where sk.item = L_item
         and sk.distro_no = I_tsf_no
         and sh.shipment = sk.shipment
         and sh.status_code = 'R'
         and sh.shipment != I_shipment;

   cursor C_GET_SUM_RECEIVED_TSF_PACK is
      select nvl(sum(nvl(sk.qty_received,0)),0)
        from shipsku sk,
             shipment sh
       where sk.item = L_pack_item
         and sk.distro_no = I_tsf_no
         and sh.shipment = sk.shipment
         and sh.status_code = 'R'
         and sh.shipment != I_shipment;

   cursor C_GET_TSF_FINISHER_UNITS is
      select nvl(finisher_units,0)
        from tsfdetail
       where tsf_no = L_tsf_no
         and item   = L_item;

   cursor C_GET_TSF_FINISHER_UNITS_PACK is
      select nvl(finisher_units,0)
        from tsfdetail
       where tsf_no = L_tsf_no
         and item = L_pack_item;

   cursor C_OLD_TSF_RETAIL is
      select nvl(finisher_av_retail,0)
        from tsfdetail
       where tsf_no = L_tsf_no
         and item   = L_item;

   cursor C_CHECK_COMP_IN_BULK is
      select p.pack_no,
             p.pack_qty
        from shipsku sk,
             packitem p
       where sk.shipment = I_shipment
         and sk.item = p.pack_no
         and p.item = I_item
         and exists (select 'x'
                       from shipsku sk1
                      where sk1.shipment = sk.shipment
                        and sk1.item     = p.item
                        and rownum = 1);

   cursor C_GET_SUM_PACK_COMP is
      select sum(pack_qty)
        from packitem
       where pack_no = L_pack_item;

   cursor C_GET_TSF_QTY is
      select tsf_qty
        from tsfdetail
       where tsf_no = I_tsf_no
         and item = I_item;

   cursor C_OLD_RETAIL_XFORM is
      select NVL(finisher_av_retail,0),
             NVL(finisher_units,0)
        from item_loc_soh
       where item     = I_item
         and loc      = L_finisher_loc
         and loc_type = L_finisher_loc_type
         for update nowait;

   cursor C_VWH is
      select from_loc
        from tsfitem_inv_flow
       where from_loc_type = 'W'
         and tsf_no        = I_tsf_no
         and item          = I_item
         and ROWNUM        = 1;

   cursor C_VWH_2 is
      select to_loc
        from tsfitem_inv_flow
       where to_loc_type = 'W'
         and tsf_no        = I_tsf_no
         and item          = I_item
         and ROWNUM        = 1;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_item',
                                            'NULL');
      return FALSE;
   elsif I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_location',
                                            'NULL');
      return FALSE;
   elsif I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_loc_type',
                                            'NULL');
      return FALSE;
   end if;

   if I_tsf_no is NOT NULL then
      if I_shipment is NOT NULL then
         --check status of shipment
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_SHIP_STATUS',
                          'SHIPMENT',
                          'Shipment: '||I_shipment);
         open C_CHECK_SHIP_STATUS;

         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_SHIP_STATUS',
                          'SHIPMENT',
                          'Shipment: '||I_shipment);
         fetch C_CHECK_SHIP_STATUS into L_ship_status;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_SHIP_STATUS',
                          'SHIPMENT',
                          'Shipment: '||I_shipment);
         close C_CHECK_SHIP_STATUS;
      else
         L_tsf_shipment_ind := 'N';
      end if;
      --This section is needed to handle pack items
      --In shipment, the pack item will be passed to this function but in receiving the component items
      -- of the pack will be used.
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_PACK_IND',
                       'ITEMMASTER',
                       'Item: '||I_item);
      open C_CHECK_PACK_IND;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_PACK_IND',
                       'ITEMMASTER',
                       'Item: '||I_item);
      fetch C_CHECK_PACK_IND into L_pack_ind;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_PACK_IND',
                       'ITEMMASTER',
                       'Item: '||I_item);
      close C_CHECK_PACK_IND;

      --get sending and receiving locations
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_LOCS',
                       'TSFHEAD',
                       'Tsf_no: '||to_char(I_tsf_no));
      open C_GET_LOCS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_LOCS',
                       'TSFHEAD',
                       'Tsf_no: '||to_char(I_tsf_no));
      fetch C_GET_LOCS into L_snd_loc,
                            L_snd_loc_type,
                            L_rcv_loc,
                            L_rcv_loc_type;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_LOCS',
                       'TSFHEAD',
                       'Tsf_no: '||to_char(I_tsf_no));
      close C_GET_LOCS;

      --get leg indicator
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_FIRST_LEG_IND',
                       'TSFHEAD',
                       'Tsf_no: '||to_char(I_tsf_no));
      open C_GET_FIRST_LEG_IND;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_FIRST_LEG_IND',
                       'TSFHEAD',
                       'Tsf_no: '||to_char(I_tsf_no));
      fetch C_GET_FIRST_LEG_IND into L_first_leg_ind;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_FIRST_LEG_IND',
                       'TSFHEAD',
                       'Tsf_no: '||to_char(I_tsf_no));
      close C_GET_FIRST_LEG_IND;

      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_FINISHER',
                       'TSFHEAD',
                       'Tsf_no: '||to_char(I_tsf_no)||', Item: '||I_item);
      open C_GET_FINISHER;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_FINISHER',
                       'TSFHEAD',
                       'Tsf_no: '||to_char(I_tsf_no)||', Item: '||I_item);
      fetch C_GET_FINISHER into L_finisher_loc,
                                L_finisher_loc_type;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_FINISHER',
                       'TSFHEAD',
                       'Tsf_no: '||to_char(I_tsf_no)||', Item: '||I_item);
      close C_GET_FINISHER;

      if L_first_leg_ind = 'N' then

         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_FIRST_LEG_TSF',
                          'TSFHEAD',
                          'Tsf_no: '||to_char(I_tsf_no));
         open C_GET_FIRST_LEG_TSF;

         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_FIRST_LEG_TSF',
                          'TSFHEAD',
                          'Tsf_no: '||to_char(I_tsf_no));
         fetch C_GET_FIRST_LEG_TSF into L_tsf_no;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_FIRST_LEG_TSF',
                          'TSFHEAD',
                          'Tsf_no: '||to_char(I_tsf_no));
         close C_GET_FIRST_LEG_TSF;

         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_XFORM',
                          'TSF_XFORM, TSF_XFORM_DETAIL',
                          'Tsf_no: '||to_char(L_tsf_no)||', Item: '||I_item);
         open C_CHECK_XFORM;

         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_XFORM',
                          'TSF_XFORM, TSF_XFORM_DETAIL',
                          'Tsf_no: '||to_char(L_tsf_no)||', Item: '||I_item);
         fetch C_CHECK_XFORM into L_xform_ind;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_XFORM',
                          'TSF_XFORM, TSF_XFORM_DETAIL',
                          'Tsf_no: '||to_char(L_tsf_no)||', Item: '||I_item);
         close C_CHECK_XFORM;

         if L_xform_ind = 'Y' then
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_XFORM_ITEM',
                             'TSF_XFORM, TSF_XFORM_DETAIL',
                             'Tsf_no: '||to_char(L_tsf_no)||', Item: '||I_item);
            open C_GET_XFORM_ITEM;

            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_XFORM_ITEM',
                             'TSF_XFORM, TSF_XFORM_DETAIL',
                             'Tsf_no: '||to_char(L_tsf_no)||', Item: '||I_item);
            fetch C_GET_XFORM_ITEM into L_xform_item;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_XFORM_ITEM',
                             'TSF_XFORM, TSF_XFORM_DETAIL',
                             'Tsf_no: '||to_char(L_tsf_no)||', Item: '||I_item);
            close C_GET_XFORM_ITEM;

            L_item := L_xform_item;
         end if;
      end if;

      if L_tsf_shipment_ind = 'Y' then
         if L_pack_ind = 'Y' then
            L_pack_item := I_item;
            --get the number of component items of the pack item.
            SQL_LIB.SET_MARK('OPEN',
                             'C_COUNT_COMP',
                             'PACKITEM',
                             'Item: '||I_item);
            open C_COUNT_COMP;

            SQL_LIB.SET_MARK('FETCH',
                             'C_COUNT_COMP',
                             'PACKITEM',
                             'Item: '||I_item);
            fetch C_COUNT_COMP into L_pack_comp_count;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_COUNT_COMP',
                             'PACKITEM',
                             'Item: '||I_item);
            close C_COUNT_COMP;

            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_COMP_ITEMS',
                             'PACKITEM',
                             'Pack_no: '||I_item);
            open C_GET_COMP_ITEMS;

         else
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHECK_ITEM_SHIP',
                             'SHIPSKU',
                             'Shipment: '|| to_char(I_shipment) || ', Item: '||I_item);
            open C_CHECK_ITEM_SHIP;

            SQL_LIB.SET_MARK('FETCH',
                             'C_CHECK_ITEM_SHIP',
                             'SHIPSKU',
                             'Shipment: '|| to_char(I_shipment) || ', Item: '||I_item);
            fetch C_CHECK_ITEM_SHIP into L_item_ship_ind;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_ITEM_SHIP',
                             'SHIPSKU',
                             'Shipment: '|| to_char(I_shipment) || ', Item: '||I_item);
            close C_CHECK_ITEM_SHIP;

            if L_item_ship_ind = 'N' then --item is a component item
               --get pack item
               SQL_LIB.SET_MARK('OPEN',
                                'C_GET_PACK_ITEM',
                                'SHIPSKU, PACKITEM',
                                'Shipment: '|| to_char(I_shipment) || ', Item: '||I_item);
               open C_GET_PACK_ITEM;

               SQL_LIB.SET_MARK('FETCH',
                                'C_GET_PACK_ITEM',
                                'SHIPSKU, PACKITEM',
                                'Shipment: '|| to_char(I_shipment) || ', Item: '||I_item);
               fetch C_GET_PACK_ITEM into L_pack_item,
                                          L_item_qty;

               SQL_LIB.SET_MARK('CLOSE',
                                'C_GET_PACK_ITEM',
                                'SHIPSKU, PACKITEM',
                                'Shipment: '|| to_char(I_shipment) || ', Item: '||I_item);
               close C_GET_PACK_ITEM;
            elsif L_ship_status = 'R' then -- check if the item is a component and a bulk sku for receiving only
               SQL_LIB.SET_MARK('OPEN',
                                'C_CHECK_COMP_IN_BULK',
                                'SHIPSKU, PACKITEM',
                                'Shipment: '|| to_char(I_shipment) || ', Item: '||I_item);
               open C_CHECK_COMP_IN_BULK;

               SQL_LIB.SET_MARK('FETCH',
                                'C_CHECK_COMP_IN_BULK',
                                'SHIPSKU, PACKITEM',
                                'Shipment: '|| to_char(I_shipment) || ', Item: '||I_item);
               fetch C_CHECK_COMP_IN_BULK into L_pack_item,
                                               L_item_qty;

               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_COMP_IN_BULK',
                                'SHIPSKU, PACKITEM',
                                'Shipment: '|| to_char(I_shipment) || ', Item: '||I_item);
               close C_CHECK_COMP_IN_BULK;

               if L_pack_item is NOT NULL then
                  L_comp_bulk_ind := 'Y';
               else
                  L_comp_bulk_ind := 'N';
               end if;
            end if;
         end if;

         --get the sum of all the components of pack to be able to get the ratio
         --of each component for the computation of the unit retail
         if L_pack_ind = 'Y' or
            L_item_ship_ind = 'N' or
            L_comp_bulk_ind = 'Y' then

            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_SUM_PACK_COMP',
                             'PACKITEM',
                             'Pack_no: '||L_pack_item);
            open C_GET_SUM_PACK_COMP;

            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_SUM_PACK_COMP',
                             'PACKITEM',
                             'Pack_no: '||L_pack_item);
            fetch C_GET_SUM_PACK_COMP into L_sum_pack_comp;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_SUM_PACK_COMP',
                             'PACKITEM',
                             'Pack_no: '||L_pack_item);
            close C_GET_SUM_PACK_COMP;
         end if;
      end if;

      if I_tsf_type = 'IC' then  --intercompany transfer
         SQL_LIB.SET_MARK('OPEN',
                          'C_FINISHER_ENTITY',
                          'TSFHEAD',
                          'Tsf_no: '||to_char(I_tsf_no));
         open C_FINISHER_ENTITY;

         SQL_LIB.SET_MARK('FETCH',
                          'C_FINISHER_ENTITY',
                          'TSFHEAD',
                          'Tsf_no: '||to_char(I_tsf_no));
         fetch C_FINISHER_ENTITY into L_finisher_entity;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_FINISHER_ENTITY',
                          'TSFHEAD',
                          'Tsf_no: '||to_char(I_tsf_no));
         close C_FINISHER_ENTITY;

         if L_finisher_entity is NULL then
            if L_first_leg_ind = 'Y' then
               L_finisher := 'S';
            else
               L_finisher := 'R';
            end if;
         else
            if L_first_leg_ind = 'Y' then
               L_finisher := 'R';
            else
               L_finisher := 'S';
            end if;
         end if;
      else --intracompany transfer
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_TSF_MKDN_CODE',
                             'SYSTEM_OPTIONS',
                             NULL);
            open C_GET_TSF_MKDN_CODE;

            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_TSF_MKDN_CODE',
                             'SYSTEM_OPTIONS',
                             NULL);
            fetch C_GET_TSF_MKDN_CODE into L_md_store_to_store,
                                           L_md_store_to_wh,
                                           L_md_wh_to_store,
                                           L_md_wh_to_wh;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_TSF_MKDN_CODE',
                             'SYSTEM_OPTIONS',
                             NULL);
            close C_GET_TSF_MKDN_CODE;
            --
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_FROM_LOC_TYPE',
                             'TSFHEAD',
                             'Tsf no: '||to_char(L_tsf_no));
            open C_GET_FROM_LOC_TYPE;

            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_FROM_LOC_TYPE',
                             'TSFHEAD',
                             'Tsf no: '||to_char(L_tsf_no));
            fetch C_GET_FROM_LOC_TYPE into L_from_loc_type;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_FROM_LOC_TYPE',
                             'TSFHEAD',
                             'Tsf no: '||to_char(L_tsf_no));
            close C_GET_FROM_LOC_TYPE;
            --
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_FINAL_LOC_TYPE',
                             'TSFHEAD',
                             'Tsf no: '||to_char(L_tsf_no));
            open C_GET_FINAL_LOC_TYPE;

            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_FINAL_LOC_TYPE',
                             'TSFHEAD',
                             'Tsf no: '||to_char(L_tsf_no));
            fetch C_GET_FINAL_LOC_TYPE into L_final_loc_type;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_FINAL_LOC_TYPE',
                             'TSFHEAD',
                             'Tsf no: '||to_char(L_tsf_no));
            close C_GET_FINAL_LOC_TYPE;
            --
            if L_from_loc_type = 'W' then
               if L_final_loc_type = 'W' then
                  L_tsf_mkdn_code := L_md_wh_to_wh;
               else
                  L_tsf_mkdn_code := L_md_wh_to_store;
               end if;
            else
               if L_final_loc_type = 'W' then
                  L_tsf_mkdn_code := L_md_store_to_wh;
               else
                  L_tsf_mkdn_code := L_md_store_to_store;
               end if;
            end if;
      end if;
   end if;

   if L_tsf_shipment_ind = 'Y' then -- both transfers with shipment and invetory adjustment will be processed
      FOR i in 1..L_pack_comp_count LOOP
         if I_tsf_no is NOT NULL then
            if L_pack_ind = 'Y' then
               SQL_LIB.SET_MARK('FETCH',
                                'C_GET_COMP_ITEMS',
                                'PACKITEM',
                                'Pack_no: '||I_item);
               fetch C_GET_COMP_ITEMS into L_item,
                                           L_item_qty;
            end if;

            if I_tsf_type = 'IC' then
               if L_pack_ind = 'Y' or L_item_ship_ind = 'N' or L_comp_bulk_ind = 'Y' then
                  if L_finisher = 'R' then
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_RETAIL_QTY_SHIP_PACK_RCV',
                                      'PACKITEM, SHIPSKU, ITEM_LOC',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     open C_GET_RETAIL_QTY_SHIP_PACK_RCV;

                     SQL_LIB.SET_MARK('FETCH',
                                      'C_GET_RETAIL_QTY_SHIP_PACK_RCV',
                                      'PACKITEM, SHIPSKU, ITEM_LOC',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     fetch C_GET_RETAIL_QTY_SHIP_PACK_RCV into L_ship_unit_retail,
                                                               L_ship_qty_expected,
                                                               L_ship_qty_received,
                                                               L_pack_unit_retail;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_GET_RETAIL_QTY_SHIP_PACK_RCV',
                                      'PACKITEM, SHIPSKU, ITEM_LOC',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     close C_GET_RETAIL_QTY_SHIP_PACK_RCV;
                  else
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_RETAIL_QTY_SHIP_PACK_SND',
                                      'PACKITEM, SHIPSKU, ITEM_LOC',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     open C_GET_RETAIL_QTY_SHIP_PACK_SND;

                     SQL_LIB.SET_MARK('FETCH',
                                      'C_GET_RETAIL_QTY_SHIP_PACK_SND',
                                      'PACKITEM, SHIPSKU, ITEM_LOC',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     fetch C_GET_RETAIL_QTY_SHIP_PACK_SND into L_ship_unit_retail,
                                                               L_ship_qty_expected,
                                                               L_ship_qty_received,
                                                               L_pack_unit_retail;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_GET_RETAIL_QTY_SHIP_PACK_SND',
                                      'PACKITEM, SHIPSKU, ITEM_LOC',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     close C_GET_RETAIL_QTY_SHIP_PACK_SND;
                  end if;

                  if L_comp_bulk_ind = 'Y' then
                     L_comp_qty_received := L_ship_qty_received;
                     L_comp_qty_expected := L_ship_qty_expected;
                     L_comp_unit_retail  := L_ship_unit_retail;

                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_RETAIL_QTY_SHIP',
                                      'SHIPSKU',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     open C_GET_RETAIL_QTY_SHIP;

                     SQL_LIB.SET_MARK('FETCH',
                                      'C_GET_RETAIL_QTY_SHIP',
                                      'SHIPSKU',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     fetch C_GET_RETAIL_QTY_SHIP into L_ship_unit_retail,
                                                      L_ship_qty_expected,
                                                      L_ship_qty_received;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_GET_RETAIL_QTY_SHIP',
                                      'SHIPSKU',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     close C_GET_RETAIL_QTY_SHIP;

                     L_bulk_qty_received := L_ship_qty_received;
                     L_bulk_qty_expected := L_ship_qty_expected;
                     L_bulk_unit_retail  := L_ship_unit_retail;
                  end if;
               else
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_GET_RETAIL_QTY_SHIP',
                                   'SHIPSKU',
                                   'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                  open C_GET_RETAIL_QTY_SHIP;

                  SQL_LIB.SET_MARK('FETCH',
                                   'C_GET_RETAIL_QTY_SHIP',
                                   'SHIPSKU',
                                   'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                  fetch C_GET_RETAIL_QTY_SHIP into L_ship_unit_retail,
                                                   L_ship_qty_expected,
                                                   L_ship_qty_received;

                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_GET_RETAIL_QTY_SHIP',
                                   'SHIPSKU',
                                   'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                  close C_GET_RETAIL_QTY_SHIP;
               end if;

               if L_first_leg_ind = 'Y' then
                  if L_finisher = 'R' then
                     --retrieve the retail at the receiving location and transfer quantity
                     if L_pack_ind = 'Y' or L_item_ship_ind = 'N' or L_comp_bulk_ind = 'Y' then
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_RETAIL_QTY_RCV_PACK',
                                         'PACKITEM, ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        open C_GET_RETAIL_QTY_RCV_PACK;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_RETAIL_QTY_RCV_PACK',
                                         'PACKITEM, ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        fetch C_GET_RETAIL_QTY_RCV_PACK into L_ship_unit_retail,
                                                             L_ship_qty_expected,
                                                             L_ship_qty_received;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_RETAIL_QTY_RCV_PACK',
                                         'PACKITEM, ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        close C_GET_RETAIL_QTY_RCV_PACK;

                        if L_comp_bulk_ind = 'Y' then
                           L_comp_qty_received := L_ship_qty_received;
                           L_comp_qty_expected := L_ship_qty_expected;
                           L_comp_unit_retail  := L_ship_unit_retail;

                           SQL_LIB.SET_MARK('OPEN',
                                            'C_GET_RETAIL_QTY_RCV',
                                            'ITEM_LOC, SHIPSKU, SHIPMENT',
                                            'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                           open C_GET_RETAIL_QTY_RCV;

                           SQL_LIB.SET_MARK('FETCH',
                                            'C_GET_RETAIL_QTY_RCV',
                                            'ITEM_LOC, SHIPSKU, SHIPMENT',
                                            'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                           fetch C_GET_RETAIL_QTY_RCV into L_ship_unit_retail,
                                                           L_ship_qty_expected,
                                                           L_ship_qty_received;

                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_GET_RETAIL_QTY_RCV',
                                            'ITEM_LOC, SHIPSKU, SHIPMENT',
                                            'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                           close C_GET_RETAIL_QTY_RCV;

                           L_bulk_qty_received := L_ship_qty_received;
                           L_bulk_qty_expected := L_ship_qty_expected;
                           L_bulk_unit_retail  := L_ship_unit_retail;
                        end if;
                     else
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_RETAIL_QTY_RCV',
                                         'ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        open C_GET_RETAIL_QTY_RCV;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_RETAIL_QTY_RCV',
                                         'ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        fetch C_GET_RETAIL_QTY_RCV into L_ship_unit_retail,
                                                        L_ship_qty_expected,
                                                        L_ship_qty_received;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_RETAIL_QTY_RCV',
                                         'ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        close C_GET_RETAIL_QTY_RCV;
                     end if;
                  else -- L_finisher = 'S'
                     --retrieve the retail at the sending location and the transfer quantity
                     if L_pack_ind = 'Y' or L_item_ship_ind = 'N' or L_comp_bulk_ind = 'Y' then
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_RETAIL_QTY_SEND_PACK',
                                         'PACKITEM, ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        open C_GET_RETAIL_QTY_SEND_PACK;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_RETAIL_QTY_SEND_PACK',
                                         'PACKITEM, ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        fetch C_GET_RETAIL_QTY_SEND_PACK into L_ship_unit_retail,
                                                              L_ship_qty_expected,
                                                              L_ship_qty_received;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_RETAIL_QTY_SEND_PACK',
                                         'PACKITEM, ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        close C_GET_RETAIL_QTY_SEND_PACK;

                        if L_comp_bulk_ind = 'Y' then
                           L_comp_qty_received := L_ship_qty_received;
                           L_comp_qty_expected := L_ship_qty_expected;
                           L_comp_unit_retail := L_ship_unit_retail;

                           SQL_LIB.SET_MARK('OPEN',
                                            'C_GET_RETAIL_QTY_SEND',
                                            'ITEM_LOC, SHIPSKU, SHIPMENT',
                                            'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                           open C_GET_RETAIL_QTY_SEND;

                           SQL_LIB.SET_MARK('FETCH',
                                            'C_GET_RETAIL_QTY_SEND',
                                            'ITEM_LOC, SHIPSKU, SHIPMENT',
                                            'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                           fetch C_GET_RETAIL_QTY_SEND into L_ship_unit_retail,
                                                            L_ship_qty_expected,
                                                            L_ship_qty_received;

                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_GET_RETAIL_QTY_SEND',
                                            'ITEM_LOC, SHIPSKU, SHIPMENT',
                                            'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                           close C_GET_RETAIL_QTY_SEND;

                           L_bulk_qty_received := L_ship_qty_received;
                           L_bulk_qty_expected := L_ship_qty_expected;
                           L_bulk_unit_retail  := L_ship_unit_retail;
                        end if;
                     else
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_RETAIL_QTY_SEND',
                                         'ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        open C_GET_RETAIL_QTY_SEND;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_RETAIL_QTY_SEND',
                                         'ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        fetch C_GET_RETAIL_QTY_SEND into L_ship_unit_retail,
                                                         L_ship_qty_expected,
                                                         L_ship_qty_received;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_RETAIL_QTY_SEND',
                                         'ITEM_LOC, SHIPSKU, SHIPMENT',
                                         'Shipment: '||to_char(I_shipment)||', Item: '||I_item);
                        close C_GET_RETAIL_QTY_SEND;
                     end if;
                  end if;
               else  --second leg of the transfer
                  --retrieve
                  if L_pack_ind = 'Y' or L_item_ship_ind = 'N' or L_comp_bulk_ind = 'Y' then
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_RETAIL_QTY_RCV_PACK',
                                      'PACKITEM, ITEM_LOC, SHIPSKU, SHIPMENT',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                     open C_GET_RETAIL_QTY_RCV_PACK;

                     SQL_LIB.SET_MARK('FETCH',
                                      'C_GET_RETAIL_QTY_RCV_PACK',
                                      'PACKITEM, ITEM_LOC, SHIPSKU, SHIPMENT',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                     fetch C_GET_RETAIL_QTY_RCV_PACK into L_ship_unit_retail,
                                                          L_ship_qty_expected,
                                                          L_ship_qty_received;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_GET_RETAIL_QTY_RCV_PACK',
                                      'PACKITEM, ITEM_LOC, SHIPSKU, SHIPMENT',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                     close C_GET_RETAIL_QTY_RCV_PACK;

                     if L_comp_bulk_ind = 'Y' then
                        L_comp_qty_received := L_ship_qty_received;
                        L_comp_qty_expected := L_ship_qty_expected;
                        L_comp_unit_retail  := L_ship_unit_retail;

                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_RETAIL_QTY_RCV',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                        open C_GET_RETAIL_QTY_RCV;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_RETAIL_QTY_RCV',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                        fetch C_GET_RETAIL_QTY_RCV into L_ship_unit_retail,
                                                        L_ship_qty_expected,
                                                        L_ship_qty_received;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_RETAIL_QTY_RCV',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                        close C_GET_RETAIL_QTY_RCV;

                        L_bulk_qty_received := L_ship_qty_received;
                        L_bulk_qty_expected := L_ship_qty_expected;
                        L_bulk_unit_retail  := L_ship_unit_retail;
                     end if;
                  else
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_RETAIL_QTY_RCV',
                                      'SHIPSKU',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                     open C_GET_RETAIL_QTY_RCV;

                     SQL_LIB.SET_MARK('FETCH',
                                      'C_GET_RETAIL_QTY_RCV',
                                      'SHIPSKU',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                     fetch C_GET_RETAIL_QTY_RCV into L_ship_unit_retail,
                                                     L_ship_qty_expected,
                                                     L_ship_qty_received;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_GET_RETAIL_QTY_RCV',
                                      'SHIPSKU',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                     close C_GET_RETAIL_QTY_RCV;
                  end if;
                  if L_finisher = 'S' then
                     -- retrieve the old transfer retail;
                     if L_pack_ind = 'Y' or L_item_ship_ind = 'N' or L_comp_bulk_ind = 'Y' then
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_OLD_TSF_AVE_RETAIL_PACK',
                                         'ITEM_LOC',
                                         'Item: '||L_item ||', Location: '||L_snd_loc);
                        open C_GET_OLD_TSF_AVE_RETAIL_PACK;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_OLD_TSF_AVE_RETAIL_PACK',
                                         'ITEM_LOC',
                                         'Item: '||L_item ||', Location: '||L_snd_loc);
                        fetch C_GET_OLD_TSF_AVE_RETAIL_PACK into L_ship_unit_retail,
                                                                 L_pack_unit_retail;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_OLD_TSF_AVE_RETAIL_PACK',
                                         'ITEM_LOC',
                                         'Item: '||L_item ||', Location: '||L_snd_loc);
                        close C_GET_OLD_TSF_AVE_RETAIL_PACK;

                        if L_ship_unit_retail = 0 then
                           if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                  L_itemloc,
                                                                  I_item,
                                                                  L_snd_loc) = FALSE then
                              return FALSE;
                           end if;
                           L_ship_unit_retail := L_itemloc.unit_retail;
                        end if;

                        if L_pack_unit_retail = 0 then
                           if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message ,
                                                                       L_pack_item,
                                                                       L_snd_loc,
                                                                       L_snd_loc_type,
                                                                       L_av_cost,
                                                                       L_unit_cost,
                                                                       L_pack_unit_retail,
                                                                       L_selling_unit_retail,
                                                                       L_selling_uom) = FALSE then
                              return FALSE;
                           end if;
                        end if;

                        if L_comp_bulk_ind = 'Y' then
                           L_comp_unit_retail := L_ship_unit_retail;

                           SQL_LIB.SET_MARK('OPEN',
                                            'C_GET_OLD_TSF_AVE_RETAIL',
                                            'SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                           open C_GET_OLD_TSF_AVE_RETAIL;

                           SQL_LIB.SET_MARK('FETCH',
                                            'C_GET_OLD_TSF_AVE_RETAIL',
                                            'SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                           fetch C_GET_OLD_TSF_AVE_RETAIL into L_ship_unit_retail;

                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_GET_OLD_TSF_AVE_RETAIL',
                                            'SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                           close C_GET_OLD_TSF_AVE_RETAIL;

                           if L_ship_unit_retail = 0 then
                              if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                     L_itemloc,
                                                                     I_item,
                                                                     L_snd_loc) = FALSE then
                                 return FALSE;
                              end if;
                              L_ship_unit_retail := L_itemloc.unit_retail;
                           end if;

                           L_bulk_unit_retail := L_ship_unit_retail;
                        end if;
                     else
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_OLD_TSF_AVE_RETAIL',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                        open C_GET_OLD_TSF_AVE_RETAIL;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_OLD_TSF_AVE_RETAIL',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                        fetch C_GET_OLD_TSF_AVE_RETAIL into L_ship_unit_retail;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_OLD_TSF_AVE_RETAIL',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
                        close C_GET_OLD_TSF_AVE_RETAIL;

                        if L_ship_unit_retail = 0 then
                           if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                  L_itemloc,
                                                                  I_item,
                                                                  L_snd_loc) = FALSE then
                              return FALSE;
                           end if;
                           L_ship_unit_retail := L_itemloc.unit_retail;
                        end if;
                     end if;
                  end if;
               end if;
            else --intracompany transfer
               if L_first_leg_ind = 'Y' then
                  if L_tsf_mkdn_code = 'R' then

                     --C_GET_RETAIL_QTY_SEND that will retrieve the retail at the
                     --sending location and the transfer quantity.
                     if L_pack_ind = 'Y' or L_item_ship_ind = 'N' or L_comp_bulk_ind = 'Y' then
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_RETAIL_QTY_SEND_PACK',
                                         'PACKITEM, ITEM_LOC, SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        open C_GET_RETAIL_QTY_SEND_PACK;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_RETAIL_QTY_SEND_PACK',
                                         'PACKITEM, ITEM_LOC, SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        fetch C_GET_RETAIL_QTY_SEND_PACK into L_ship_unit_retail,
                                                              L_ship_qty_expected,
                                                              L_ship_qty_received;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_RETAIL_QTY_SEND_PACK',
                                         'PACKITEM, ITEM_LOC, SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        close C_GET_RETAIL_QTY_SEND_PACK;

                        if L_comp_bulk_ind = 'Y' then
                           L_comp_qty_received := L_ship_qty_received;
                           L_comp_qty_expected := L_ship_qty_expected;
                           L_comp_unit_retail  := L_ship_unit_retail;

                           SQL_LIB.SET_MARK('OPEN',
                                            'C_GET_RETAIL_QTY_SEND',
                                            'ITEM_LOC, SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           open C_GET_RETAIL_QTY_SEND;

                           SQL_LIB.SET_MARK('FETCH',
                                            'C_GET_RETAIL_QTY_SEND',
                                            'ITEM_LOC,SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           fetch C_GET_RETAIL_QTY_SEND into L_ship_unit_retail,
                                                            L_ship_qty_expected,
                                                            L_ship_qty_received;

                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_GET_RETAIL_QTY_SEND',
                                            'ITEM_LOC, SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           close C_GET_RETAIL_QTY_SEND;

                           L_bulk_qty_received := L_ship_qty_received;
                           L_bulk_qty_expected := L_ship_qty_expected;
                           L_bulk_unit_retail  := L_ship_unit_retail;
                        end if;
                     else
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_RETAIL_QTY_SEND',
                                         'ITEM_LOC, SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        open C_GET_RETAIL_QTY_SEND;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_RETAIL_QTY_SEND',
                                         'ITEM_LOC,SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        fetch C_GET_RETAIL_QTY_SEND into L_ship_unit_retail,
                                                         L_ship_qty_expected,
                                                         L_ship_qty_received;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_RETAIL_QTY_SEND',
                                         'ITEM_LOC, SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        close C_GET_RETAIL_QTY_SEND;
                     end if;
                  else --markdown at sending location
                     if L_pack_ind = 'Y' or L_item_ship_ind = 'N' or L_comp_bulk_ind = 'Y' then
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_RETAIL_QTY_SHIP_PACK_RCV',
                                         'PACKITEM, SHIPSKU, ITEM_LOC',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        open C_GET_RETAIL_QTY_SHIP_PACK_RCV;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_RETAIL_QTY_SHIP_PACK_RCV',
                                         'PACKITEM, SHIPSKU, ITEM_LOC',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        fetch C_GET_RETAIL_QTY_SHIP_PACK_RCV into L_ship_unit_retail,
                                                                  L_ship_qty_expected,
                                                                  L_ship_qty_received,
                                                                  L_pack_unit_retail;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_RETAIL_QTY_SHIP_PACK_RCV',
                                         'PACKITEM, SHIPSKU, ITEM_LOC',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        close C_GET_RETAIL_QTY_SHIP_PACK_RCV;

                        if L_comp_bulk_ind = 'Y' then
                           L_comp_qty_received := L_ship_qty_received;
                           L_comp_qty_expected := L_ship_qty_expected;
                           L_comp_unit_retail  := L_ship_unit_retail;

                           SQL_LIB.SET_MARK('OPEN',
                                            'C_GET_RETAIL_QTY_SHIP',
                                            'SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           open C_GET_RETAIL_QTY_SHIP;

                           SQL_LIB.SET_MARK('FETCH',
                                            'C_GET_RETAIL_QTY_SHIP',
                                            'SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           fetch C_GET_RETAIL_QTY_SHIP into L_ship_unit_retail,
                                                            L_ship_qty_expected,
                                                            L_ship_qty_received;

                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_GET_RETAIL_QTY_SHIP',
                                            'SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           close C_GET_RETAIL_QTY_SHIP;

                           if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                  L_itemloc,
                                                                  I_item,
                                                                  L_rcv_loc) = FALSE then
                              return FALSE;
                           end if;

                           L_bulk_qty_received := L_ship_qty_received;
                           L_bulk_qty_expected := L_ship_qty_expected;
                           L_bulk_unit_retail  := L_itemloc.unit_retail;
                        end if;
                     else
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_RETAIL_QTY_SHIP',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        open C_GET_RETAIL_QTY_SHIP;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_RETAIL_QTY_SHIP',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        fetch C_GET_RETAIL_QTY_SHIP into L_ship_unit_retail,
                                                         L_ship_qty_expected,
                                                         L_ship_qty_received;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_RETAIL_QTY_SHIP',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        close C_GET_RETAIL_QTY_SHIP;

                        if I_tsf_type = 'EG' and  L_rcv_loc_type = 'W' then -- Fetch the Virtual Warehouse
                           ---
                           if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                                      L_phy_wh,
                                                      L_rcv_loc) = FALSE then
                               return FALSE;
                           end if;
                           ---
                           if L_phy_wh = TRUE then
                             SQL_LIB.SET_MARK('OPEN',
                                              'C_VWH',
                                               NULL,
                                               NULL);
                             open C_VWH;
                             SQL_LIB.SET_MARK('FETCH',
                                              'C_VWH',
                                               NULL,
                                               NULL);
                             fetch C_VWH into L_vir_wh;
                             SQL_LIB.SET_MARK('CLOSE',
                                              'C_VWH',
                                               NULL,
                                               NULL);
                             close C_VWH;
                             if L_vir_wh is NOT NULL then
                                if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                       L_itemloc,
                                                                       I_item,
                                                                       L_vir_wh) = FALSE then
                                   return FALSE;
                                end if;
                             end if;
                           end if;
                        else
                           if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                  L_itemloc,
                                                                  I_item,
                                                                  L_rcv_loc) = FALSE then
                              return FALSE;
                           end if;
                        end if;
                        ---
                        L_ship_unit_retail := L_itemloc.unit_retail;
                     end if;
                  end if;
               else --second leg of the transfer
                  if L_xform_ind = 'Y' then
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_QTY_XFORM',
                                      'SHIPSKU',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     open C_GET_QTY_XFORM;

                     SQL_LIB.SET_MARK('FETCH',
                                      'C_GET_QTY_XFORM',
                                      'SHIPSKU',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     fetch C_GET_QTY_XFORM into L_ship_qty_expected,
                                                L_ship_qty_received;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_GET_QTY_XFORM',
                                      'SHIPSKU',
                                      'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                     close C_GET_QTY_XFORM;

                      --get the unit retail of the parent transfer
                      SQL_LIB.SET_MARK('OPEN',
                                       'C_OLD_TSF_RETAIL',
                                       'TSFDETAIL',
                                       'Tsf no: '||to_char(L_tsf_no) ||', Item: '||L_item);
                      open C_OLD_TSF_RETAIL;

                      SQL_LIB.SET_MARK('FETCH',
                                       'C_OLD_TSF_RETAIL',
                                       'TSFDETAIL',
                                       'Tsf no: '||to_char(L_tsf_no) ||', Item: '||L_item);
                      fetch C_OLD_TSF_RETAIL into L_ship_unit_retail;

                      SQL_LIB.SET_MARK('CLOSE',
                                       'C_OLD_TSF_RETAIL',
                                       'TSFDETAIL',
                                       'Tsf no: '||to_char(L_tsf_no) ||', Item: '||L_item);
                      close C_OLD_TSF_RETAIL;

                      if L_ship_unit_retail = 0 then
                         if L_tsf_mkdn_code = 'R' then
                            if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                   L_itemloc,
                                                                   L_item,
                                                                   L_snd_loc) = FALSE then
                               return FALSE;
                            end if;
                         else
                            if I_tsf_type = 'EG' and  L_rcv_loc_type = 'W' then -- Fetch the Virtual Warehouse
                               ---
                               if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                                          L_phy_wh,
                                                          L_rcv_loc) = FALSE then
                                  return FALSE;
                              end if;
                              ---
                              if L_phy_wh = TRUE then
                                 SQL_LIB.SET_MARK('OPEN',
                                                  'C_VWH_2',
                                                   NULL,
                                                   NULL);
                                 open C_VWH_2;
                                 SQL_LIB.SET_MARK('FETCH',
                                                  'C_VWH_2',
                                                   NULL,
                                                   NULL);
                                 fetch C_VWH_2 into L_vir_wh;
                                 SQL_LIB.SET_MARK('CLOSE',
                                                  'C_VWH_2',
                                                   NULL,
                                                   NULL);
                                 close C_VWH_2;
                                 if L_vir_wh is NOT NULL then
                                    if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                           L_itemloc,
                                                                           I_item,
                                                                           L_vir_wh) = FALSE then
                                       return FALSE;
                                    end if;
                                 end if;
                              end if;
                            else
                               if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                      L_itemloc,
                                                                      I_item,
                                                                      L_rcv_loc) = FALSE then
                                  return FALSE;
                               end if;
                            end if;
                         end if;
                         L_ship_unit_retail := L_itemloc.unit_retail;
                      end if;
                  else
                     if L_pack_ind = 'Y' or L_item_ship_ind = 'N' or L_comp_bulk_ind = 'Y' then
                        if L_tsf_mkdn_code = 'R' then
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_GET_RETAIL_QTY_SHIP_PACK_SND',
                                            'PACKITEM, SHIPSKU, ITEM_LOC',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           open C_GET_RETAIL_QTY_SHIP_PACK_SND;

                           SQL_LIB.SET_MARK('FETCH',
                                            'C_GET_RETAIL_QTY_SHIP_PACK_SND',
                                            'PACKITEM, SHIPSKU, ITEM_LOC',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           fetch C_GET_RETAIL_QTY_SHIP_PACK_SND into L_ship_unit_retail,
                                                                     L_ship_qty_expected,
                                                                     L_ship_qty_received,
                                                                     L_pack_unit_retail;

                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_GET_RETAIL_QTY_SHIP_PACK_SND',
                                            'PACKITEM, SHIPSKU, ITEM_LOC',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           close C_GET_RETAIL_QTY_SHIP_PACK_SND;
                        else
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_GET_RETAIL_QTY_SHIP_PACK_RCV',
                                            'PACKITEM, SHIPSKU, ITEM_LOC',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           open C_GET_RETAIL_QTY_SHIP_PACK_RCV;

                           SQL_LIB.SET_MARK('FETCH',
                                            'C_GET_RETAIL_QTY_SHIP_PACK_RCV',
                                            'PACKITEM, SHIPSKU, ITEM_LOC',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           fetch C_GET_RETAIL_QTY_SHIP_PACK_RCV into L_ship_unit_retail,
                                                                     L_ship_qty_expected,
                                                                     L_ship_qty_received,
                                                                     L_pack_unit_retail;

                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_GET_RETAIL_QTY_SHIP_PACK_RCV',
                                            'PACKITEM, SHIPSKU, ITEM_LOC',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           close C_GET_RETAIL_QTY_SHIP_PACK_RCV;
                        end if;

                        if L_comp_bulk_ind = 'Y' then
                           L_comp_qty_received := L_ship_qty_received;
                           L_comp_qty_expected := L_ship_qty_expected;
                           L_comp_unit_retail  := L_ship_unit_retail;

                           SQL_LIB.SET_MARK('OPEN',
                                            'C_GET_RETAIL_QTY_SHIP',
                                            'SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           open C_GET_RETAIL_QTY_SHIP;

                           SQL_LIB.SET_MARK('FETCH',
                                            'C_GET_RETAIL_QTY_SHIP',
                                            'SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           fetch C_GET_RETAIL_QTY_SHIP into L_ship_unit_retail,
                                                            L_ship_qty_expected,
                                                            L_ship_qty_received;

                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_GET_RETAIL_QTY_SHIP',
                                            'SHIPSKU',
                                            'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                           close C_GET_RETAIL_QTY_SHIP;

                           --get the unit retail of the parent transfer
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_OLD_TSF_RETAIL',
                                            'TSFDETAIL',
                                            'Tsf no: '||to_char(L_tsf_no) ||', Item: '||L_item);
                           open C_OLD_TSF_RETAIL;

                           SQL_LIB.SET_MARK('FETCH',
                                            'C_OLD_TSF_RETAIL',
                                            'TSFDETAIL',
                                            'Tsf no: '||to_char(L_tsf_no) ||', Item: '||L_item);
                           fetch C_OLD_TSF_RETAIL into L_ship_unit_retail;

                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_OLD_TSF_RETAIL',
                                            'TSFDETAIL',
                                            'Tsf no: '||to_char(L_tsf_no) ||', Item: '||L_item);
                           close C_OLD_TSF_RETAIL;

                           if L_ship_unit_retail = 0 then
                              if L_tsf_mkdn_code = 'R' then
                                 if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                        L_itemloc,
                                                                        I_item,
                                                                        L_snd_loc) = FALSE then
                                    return FALSE;
                                 end if;
                              else
                                 if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                        L_itemloc,
                                                                        I_item,
                                                                        L_rcv_loc) = FALSE then
                                    return FALSE;
                                 end if;
                              end if;
                              L_ship_unit_retail := L_itemloc.unit_retail;
                           end if;

                           L_bulk_qty_received := L_ship_qty_received;
                           L_bulk_qty_expected := L_ship_qty_expected;
                           L_bulk_unit_retail  := L_ship_unit_retail;
                        end if;
                     else
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_RETAIL_QTY_SHIP',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        open C_GET_RETAIL_QTY_SHIP;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_RETAIL_QTY_SHIP',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        fetch C_GET_RETAIL_QTY_SHIP into L_ship_unit_retail,
                                                         L_ship_qty_expected,
                                                         L_ship_qty_received;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_RETAIL_QTY_SHIP',
                                         'SHIPSKU',
                                         'Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        close C_GET_RETAIL_QTY_SHIP;

                        --get the unit retail of the parent transfer
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_OLD_TSF_RETAIL',
                                         'TSFDETAIL',
                                         'Tsf no: '||to_char(L_tsf_no) ||', Item: '||L_item);
                        open C_OLD_TSF_RETAIL;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_OLD_TSF_RETAIL',
                                         'TSFDETAIL',
                                         'Tsf no: '||to_char(L_tsf_no) ||', Item: '||L_item);
                        fetch C_OLD_TSF_RETAIL into L_ship_unit_retail;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_OLD_TSF_RETAIL',
                                         'TSFDETAIL',
                                         'Tsf no: '||to_char(L_tsf_no) ||', Item: '||L_item);
                        close C_OLD_TSF_RETAIL;

                        if L_ship_unit_retail = 0 then
                           if L_tsf_mkdn_code = 'R' then
                              if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                     L_itemloc,
                                                                     I_item,
                                                                     L_snd_loc) = FALSE then
                                 return FALSE;
                              end if;
                           else
                              if I_tsf_type = 'EG' and L_rcv_loc_type = 'W' then -- Fetch the Virtual Warehouse
                                  ---
                                  if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                                             L_phy_wh,
                                                             L_rcv_loc) = FALSE then
                                     return FALSE;
                                 end if;
                                 ---
                                 if L_phy_wh = TRUE then
                                    ---
                                    SQL_LIB.SET_MARK('OPEN',
                                                     'C_VWH_2',
                                                      NULL,
                                                      NULL);
                                    open C_VWH_2;
                                    SQL_LIB.SET_MARK('FETCH',
                                                     'C_VWH_2',
                                                      NULL,
                                                      NULL);
                                    fetch C_VWH_2 into L_vir_wh;
                                    SQL_LIB.SET_MARK('CLOSE',
                                                     'C_VWH_2',
                                                      NULL,
                                                      NULL);
                                    close C_VWH_2;
                                    if L_vir_wh is NOT NULL then
                                       if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                              L_itemloc,
                                                                              I_item,
                                                                              L_vir_wh) = FALSE then
                                          return FALSE;
                                       end if;
                                    end if;
                                 end if;
                              else
                                 if ITEMLOC_ATTRIB_SQL.GET_ITEMLOC_INFO(O_error_message,
                                                                        L_itemloc,
                                                                        I_item,
                                                                        L_rcv_loc) = FALSE then
                                    return FALSE;
                                 end if;
                              end if;
                           end if;
                           L_ship_unit_retail := L_itemloc.unit_retail;
                        end if;
                     end if;
                  end if;
               end if;
            end if;
            if L_ship_status = 'R' then
               if L_comp_bulk_ind = 'Y' then
                  L_ship_qty_expected := L_bulk_qty_expected + L_comp_qty_expected;
                  L_ship_qty_received := L_bulk_qty_received + L_comp_qty_received;
                  if L_ship_qty_received != 0 then
                     L_ship_unit_retail  := ((L_bulk_unit_retail*L_bulk_qty_received) +(L_comp_unit_retail*L_comp_qty_received))/L_ship_qty_received;
                  else
                     L_ship_unit_retail := 0;
                  end if;
               end if;

               if L_item_ship_ind = 'N' or L_comp_bulk_ind = 'Y' then
                  if L_first_leg_ind = 'Y' then
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_SUM_SHIPPED_TSF_PACK',
                                      'SHIPSKU, SHIPMENT',
                                      'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||L_pack_item);
                     open C_GET_SUM_SHIPPED_TSF_PACK;

                     SQL_LIB.SET_MARK('FETCH',
                                      'C_GET_SUM_SHIPPED_TSF_PACK',
                                      'SHIPSKU, SHIPMENT',
                                      'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||L_pack_item);
                     fetch C_GET_SUM_SHIPPED_TSF_PACK into L_pack_sum_shipped_tsf;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_GET_SUM_SHIPPED_TSF_PACK',
                                      'SHIPSKU, SHIPMENT',
                                      'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||L_pack_item);
                     close C_GET_SUM_SHIPPED_TSF_PACK;

                     if L_pack_sum_shipped_tsf is NULL then
                        L_pack_sum_shipped_tsf := 0;
                     end if;
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_SUM_RECEIVED_TSF_PACK',
                                      'SHIPSKU, SHIPMENT',
                                      'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||L_pack_item);
                     open C_GET_SUM_RECEIVED_TSF_PACK;

                     SQL_LIB.SET_MARK('FETCH',
                                      'C_GET_SUM_RECEIVED_TSF_PACK',
                                      'SHIPSKU, SHIPMENT',
                                      'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||L_pack_item);
                     fetch C_GET_SUM_RECEIVED_TSF_PACK into L_pack_sum_received_tsf;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_GET_SUM_RECEIVED_TSF_PACK',
                                      'SHIPSKU, SHIPMENT',
                                      'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||L_pack_item);
                     close C_GET_SUM_RECEIVED_TSF_PACK;

                     if L_pack_sum_received_tsf is NULL then
                        L_pack_sum_received_tsf := 0;
                     end if;
                  end if;

                  SQL_LIB.SET_MARK('OPEN',
                                   'C_GET_TSF_FINISHER_UNITS_PACK',
                                   'TSFDETAIL',
                                   'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_pack_item);
                  open C_GET_TSF_FINISHER_UNITS_PACK;

                  SQL_LIB.SET_MARK('FETCH',
                                   'C_GET_TSF_FINISHER_UNITS_PACK',
                                   'TSFDETAIL',
                                   'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_pack_item);
                  fetch C_GET_TSF_FINISHER_UNITS_PACK into L_pack_tsf_units;

                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_GET_TSF_FINISHER_UNITS_PACK',
                                   'TSFDETAIL',
                                   'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_pack_item);
                  close C_GET_TSF_FINISHER_UNITS_PACK;

                  L_pack_tsf_units := (L_pack_tsf_units - (L_pack_sum_shipped_tsf + L_pack_sum_received_tsf))*L_item_qty;
                  L_tsf_units      := L_pack_tsf_units;
                  if L_comp_bulk_ind = 'Y' then
                     if L_first_leg_ind = 'Y' then
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_SUM_SHIPPED_TSF',
                                         'SHIPSKU, SHIPMENT',
                                         'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        open C_GET_SUM_SHIPPED_TSF;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_SUM_SHIPPED_TSF',
                                         'SHIPSKU, SHIPMENT',
                                         'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        fetch C_GET_SUM_SHIPPED_TSF into L_sum_shipped_tsf;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_SUM_SHIPPED_TSF',
                                         'SHIPSKU, SHIPMENT',
                                         'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        close C_GET_SUM_SHIPPED_TSF;

                        SQL_LIB.SET_MARK('OPEN',
                                         'C_GET_SUM_RECEIVED_TSF',
                                         'SHIPSKU, SHIPMENT',
                                         'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        open C_GET_SUM_RECEIVED_TSF;

                        SQL_LIB.SET_MARK('FETCH',
                                         'C_GET_SUM_RECEIVED_TSF',
                                         'SHIPSKU, SHIPMENT',
                                         'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        fetch C_GET_SUM_RECEIVED_TSF into L_sum_received_tsf;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_GET_SUM_RECEIVED_TSF',
                                         'SHIPSKU, SHIPMENT',
                                         'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                        close C_GET_SUM_RECEIVED_TSF;
                     end if;
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_GET_TSF_FINISHER_UNITS',
                                      'TSFDETAIL',
                                      'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_item);
                     open C_GET_TSF_FINISHER_UNITS;

                     SQL_LIB.SET_MARK('FETCH',
                                      'C_GET_TSF_FINISHER_UNITS',
                                      'TSFDETAIL',
                                      'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_item);
                     fetch C_GET_TSF_FINISHER_UNITS into L_bulk_tsf_units;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_GET_TSF_FINISHER_UNITS',
                                      'TSFDETAIL',
                                      'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_item);
                     close C_GET_TSF_FINISHER_UNITS;

                     L_bulk_tsf_units := L_bulk_tsf_units - (L_sum_shipped_tsf + L_sum_received_tsf);
                     L_tsf_units      := L_pack_tsf_units + L_bulk_tsf_units;
                  end if;
               elsif L_first_leg_ind = 'Y' then
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_GET_SUM_SHIPPED_TSF',
                                   'SHIPSKU, SHIPMENT',
                                   'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                  open C_GET_SUM_SHIPPED_TSF;

                  SQL_LIB.SET_MARK('FETCH',
                                   'C_GET_SUM_SHIPPED_TSF',
                                   'SHIPSKU, SHIPMENT',
                                   'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                  fetch C_GET_SUM_SHIPPED_TSF into L_sum_shipped_tsf;

                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_GET_SUM_SHIPPED_TSF',
                                   'SHIPSKU, SHIPMENT',
                                   'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                  close C_GET_SUM_SHIPPED_TSF;

                  SQL_LIB.SET_MARK('OPEN',
                                   'C_GET_SUM_RECEIVED_TSF',
                                   'SHIPSKU, SHIPMENT',
                                   'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                  open C_GET_SUM_RECEIVED_TSF;

                  SQL_LIB.SET_MARK('FETCH',
                                   'C_GET_SUM_RECEIVED_TSF',
                                   'SHIPSKU, SHIPMENT',
                                   'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                  fetch C_GET_SUM_RECEIVED_TSF into L_sum_received_tsf;

                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_GET_SUM_RECEIVED_TSF',
                                   'SHIPSKU, SHIPMENT',
                                   'Tsf no: '||to_char(I_tsf_no)||', Shipment: '||to_char(I_shipment) ||', Item: '||I_item);
                  close C_GET_SUM_RECEIVED_TSF;

                  SQL_LIB.SET_MARK('OPEN',
                                   'C_GET_TSF_FINISHER_UNITS',
                                   'TSFDETAIL',
                                   'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_item);
                  open C_GET_TSF_FINISHER_UNITS;

                  SQL_LIB.SET_MARK('FETCH',
                                   'C_GET_TSF_FINISHER_UNITS',
                                   'TSFDETAIL',
                                   'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_item);
                  fetch C_GET_TSF_FINISHER_UNITS into L_tsf_units;

                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_GET_TSF_FINISHER_UNITS',
                                   'TSFDETAIL',
                                   'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_item);
                  close C_GET_TSF_FINISHER_UNITS;

                  L_tsf_units := L_tsf_units - (L_sum_shipped_tsf + L_sum_received_tsf);
               else
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_GET_TSF_FINISHER_UNITS',
                                   'TSFDETAIL',
                                   'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_item);
                  open C_GET_TSF_FINISHER_UNITS;

                  SQL_LIB.SET_MARK('FETCH',
                                   'C_GET_TSF_FINISHER_UNITS',
                                   'TSFDETAIL',
                                   'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_item);
                  fetch C_GET_TSF_FINISHER_UNITS into L_tsf_units;

                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_GET_TSF_FINISHER_UNITS',
                                   'TSFDETAIL',
                                   'Tsf no: '||to_char(I_tsf_no)||', Item: '||L_item);
                  close C_GET_TSF_FINISHER_UNITS;
               end if;

               if L_tsf_units is NULL then
                  L_tsf_units := 0;
               end if;
               ---
               if (L_tsf_units = L_ship_qty_expected) or (L_tsf_units = 0 and L_first_leg_ind = 'N') then
                  L_shipment_qty := L_ship_qty_received - L_ship_qty_expected;
               elsif L_tsf_units != 0 and L_first_leg_ind = 'N' then
                  L_shipment_qty := L_ship_qty_received - (L_ship_qty_expected - L_tsf_units);
               else
                  L_shipment_qty := L_ship_qty_received - L_tsf_units;
               end if;

               if L_comp_bulk_ind = 'Y' then
                  if (L_bulk_tsf_units = L_bulk_qty_expected) or
                     (L_bulk_tsf_units = 0 and L_first_leg_ind = 'N') then
                     if L_bulk_qty_received != 0 then
                        L_bulk_shipment_qty := L_bulk_qty_received - L_bulk_qty_expected;
                     end if;
                  elsif L_bulk_tsf_units != 0 and L_first_leg_ind = 'N' then
                     if L_bulk_qty_received != 0 then
                        L_bulk_shipment_qty := L_bulk_qty_received - (L_bulk_qty_expected - L_bulk_tsf_units);
                     end if;
                  else
                     if L_bulk_qty_received != 0 then
                        L_bulk_shipment_qty := L_bulk_qty_received - L_bulk_tsf_units;
                     end if;
                  end if;
                  ---
                  if (L_pack_tsf_units = L_comp_qty_expected) or
                     (L_pack_tsf_units = 0 and L_first_leg_ind = 'N') then
                     if L_comp_qty_received != 0 then
                        L_pack_shipment_qty := L_comp_qty_received - L_comp_qty_expected;
                     end if;
                  elsif L_pack_tsf_units != 0 and L_first_leg_ind = 'N' then
                     if L_comp_qty_received != 0 then
                        L_pack_shipment_qty := L_comp_qty_received - (L_comp_qty_expected - L_pack_tsf_units);
                     end if;
                  else
                     if L_comp_qty_received != 0 then
                        L_pack_shipment_qty := L_comp_qty_received - L_pack_tsf_units;
                     end if;
                  end if;

                  L_shipment_qty := L_bulk_shipment_qty + L_pack_shipment_qty;
               end if;
            else
               if L_first_leg_ind = 'N' and L_comp_bulk_ind = 'Y' then
                  L_ship_qty_expected := L_bulk_qty_expected + L_comp_qty_expected;
               end if;
               ---
               L_shipment_qty := L_ship_qty_expected;
            end if;

            if L_first_leg_ind != 'Y' then
               L_shipment_qty := (L_shipment_qty)*(-1);
               if L_comp_bulk_ind = 'Y' then
                  L_bulk_shipment_qty := (L_bulk_shipment_qty)*(-1);
                  L_pack_shipment_qty := (L_pack_shipment_qty)*(-1);
               end if;
            end if;
            L_shipment_retail := L_ship_unit_retail;

         else --Inventory adjustment
            L_shipment_qty        := I_adj_qty;
            L_finisher_loc        := I_location;
            L_finisher_loc_type   := I_loc_type;

            -- get unit retail defaults for inventory adjusments if finisher_av_retail will be NULL
            if L_finisher_loc_type = 'E' then
               ---
               if PRICING_ATTRIB_SQL.GET_EXTERNAL_FINISHER_RETAIL(O_error_message,
                                                                  L_shipment_retail,
                                                                  L_item,
                                                                  L_finisher_loc) = FALSE then
                  return FALSE;
               end if;
               ---
            else -- internal finisher
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           L_item,
                                                           L_finisher_loc,
                                                           L_finisher_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_shipment_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
               ---
            end if;

         end if;
         ---
         if L_xform_ind != 'Y' then
            SQL_LIB.SET_MARK('OPEN',
                             'C_OLD_RETAIL',
                             'ITEM_LOC_SOH',
                             'Item: '||I_item);
            open C_OLD_RETAIL;

            SQL_LIB.SET_MARK('FETCH',
                             'C_OLD_RETAIL',
                             'ITEM_LOC_SOH',
                             'Item: '||I_item);
            fetch C_OLD_RETAIL into L_old_retail,
                                    L_old_qty;

            if L_pack_ind = 'Y' or
               L_item_ship_ind = 'N' or
               L_comp_bulk_ind = 'Y' then
               L_shipment_retail := L_pack_unit_retail*(L_item_qty/L_sum_pack_comp);
            end if;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_OLD_RETAIL',
                             'ITEM_LOC_SOH',
                             'Item: '||I_item);
            close C_OLD_RETAIL;
         else
            SQL_LIB.SET_MARK('OPEN',
                             'C_OLD_RETAIL_XFORM',
                             'ITEM_LOC_SOH',
                             'Item: '||I_item);
            open C_OLD_RETAIL_XFORM;

            SQL_LIB.SET_MARK('FETCH',
                             'C_OLD_RETAIL_XFORM',
                             'ITEM_LOC_SOH',
                             'Item: '||I_item);
            fetch C_OLD_RETAIL_XFORM into L_old_retail,
                                          L_old_qty;

            if L_pack_ind = 'Y' or
               L_item_ship_ind = 'N' or
               L_comp_bulk_ind = 'Y' then
               L_shipment_retail := L_pack_unit_retail * (L_item_qty / L_sum_pack_comp);
            end if;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_OLD_RETAIL_XFORM',
                             'ITEM_LOC_SOH',
                             'Item: '||I_item);
            close C_OLD_RETAIL_XFORM;
         end if;
         ---
         L_new_qty   := L_old_qty + L_shipment_qty;

         if L_new_qty = 0 then
            L_new_retail := NULL;
            L_new_qty    := NULL;
         else
            L_new_retail := ((L_old_qty*L_old_retail) + (L_shipment_qty*L_shipment_retail))/L_new_qty;
            if L_new_retail = 0 then
               L_new_retail := NULL;
            end if;
         end if;
         ---
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'ITEM_LOC_SOH',
                          NULL);
         if L_xform_ind = 'N' then
            update item_loc_soh
               set finisher_av_retail = L_new_retail,
                   finisher_units  = L_new_qty
             where item     = L_item
               and loc      = L_finisher_loc
               and loc_type = L_finisher_loc_type;
         elsif L_xform_ind = 'Y' and L_ship_status = 'I' then
            update item_loc_soh
               set finisher_av_retail = L_new_retail,
                   finisher_units  = L_new_qty
             where item = I_item
               and loc = L_finisher_loc
               and loc_type = L_finisher_loc_type;
         end if;
         ---
         if I_tsf_no is NOT NULL then
            if L_pack_ind = 'Y' then
               L_temp_unit_retail := L_temp_unit_retail + L_shipment_retail*L_item_qty;
            end if;
            if L_item_ship_ind = 'N' and L_ship_status = 'R' then
            ---C_GET_FIRST_COMP_PACK
               SQL_LIB.SET_MARK('OPEN',
                                'C_GET_FIRST_COMP_PACK',
                                'PACKITEM',
                                'Pack_no: '||L_pack_item);
               open C_GET_FIRST_COMP_PACK;

               SQL_LIB.SET_MARK('FETCH',
                                'C_GET_FIRST_COMP_PACK',
                                'PACKITEM',
                                'Pack_no: '||L_pack_item);
               fetch C_GET_FIRST_COMP_PACK into L_first_pack_comp;

               SQL_LIB.SET_MARK('CLOSE',
                                'C_GET_FIRST_COMP_PACK',
                                'PACKITEM',
                                'Pack_no: '||L_pack_item);
               close C_GET_FIRST_COMP_PACK;
            end if;
            if (L_pack_ind = 'Y' or (L_item_ship_ind = 'N' and L_first_pack_comp = I_item)) and i = L_pack_comp_count  then
               L_tsf_item := L_pack_item;
               ---
               if L_ship_status = 'R' and L_pack_unit_retail is NULL then
               --retrieve current average retail from transfer
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_GET_TSF_RETAIL_PACK',
                                   'TSFDETAIL',
                                   'Tsf_no: '||to_char(I_tsf_no)||', Item: '||L_pack_item);
                  open C_GET_TSF_RETAIL_PACK;

                  SQL_LIB.SET_MARK('FETCH',
                                   'C_GET_TSF_RETAIL_PACK',
                                   'TSFDETAIL',
                                   'Tsf_no: '||to_char(I_tsf_no)||', Item: '||L_pack_item);
                  fetch C_GET_TSF_RETAIL_PACK into L_pack_unit_retail;

                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_GET_TSF_RETAIL_PACK',
                                   'TSFDETAIL',
                                   'Tsf_no: '||to_char(I_tsf_no)||', Item: '||L_pack_item);
                  close C_GET_TSF_RETAIL_PACK;
               elsif L_pack_unit_retail is NULL then
                  L_pack_unit_retail := L_temp_unit_retail;
               end if;
               ---
               if PUT_TSF_AV_RETAIL(O_error_message,
                                    L_pack_unit_retail,
                                    (L_shipment_qty/L_item_qty),
                                    L_tsf_no,
                                    L_tsf_item) = FALSE then
                  return FALSE;
               end if;
            elsif L_comp_bulk_ind = 'Y' and L_ship_status = 'R' then
               if PUT_TSF_AV_RETAIL(O_error_message,
                                    L_pack_unit_retail,
                                    L_pack_shipment_qty/L_item_qty,
                                    L_tsf_no,
                                    L_pack_item) = FALSE then
                  return FALSE;
               end if;

               if PUT_TSF_AV_RETAIL(O_error_message,
                                    L_bulk_unit_retail,
                                    L_bulk_shipment_qty,
                                    L_tsf_no,
                                    L_item) = FALSE then
                  return FALSE;
               end if;

            else
               if PUT_TSF_AV_RETAIL(O_error_message,
                                    L_shipment_retail,
                                    L_shipment_qty,
                                    L_tsf_no,
                                    L_item) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         end if;
      END LOOP;
      ---
      if L_pack_ind = 'Y' then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_COMP_ITEMS',
                          'PACKITEM',
                          'Pack_no: '||I_item);
         close C_GET_COMP_ITEMS;
      end if;
   else --no shipment
      if L_first_leg_ind = 'Y' then
         if I_tsf_type = 'IC' then --intercompany transfer
            if L_finisher = 'R' then --tsf entity of finisher is the same as receiving location
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message ,
                                                           L_item,
                                                           L_rcv_loc,
                                                           L_rcv_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_tsf_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
            else  --tsf entity of finisher is the same as sending location
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message ,
                                                           L_item,
                                                           L_snd_loc,
                                                           L_snd_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_tsf_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
            end if;
         else --intracompany transfer
            if L_tsf_mkdn_code = 'R' then --markdown at receiving location
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message ,
                                                           L_item,
                                                           L_snd_loc,
                                                           L_snd_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_tsf_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
            else  --markdown at sending location
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message ,
                                                           L_item,
                                                           L_rcv_loc,
                                                           L_rcv_loc_type,
                                                           L_av_cost,
                                                           L_unit_cost,
                                                           L_tsf_unit_retail,
                                                           L_selling_unit_retail,
                                                           L_selling_uom) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
      else --2nd leg of transfer
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_OLD_TSF_AVE_RETAIL',
                          'SHIPSKU',
                          'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
         open C_GET_OLD_TSF_AVE_RETAIL;

         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_OLD_TSF_AVE_RETAIL',
                          'SHIPSKU',
                          'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
         fetch C_GET_OLD_TSF_AVE_RETAIL into L_tsf_unit_retail;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_OLD_TSF_AVE_RETAIL',
                          'SHIPSKU',
                          'Shipment: '||to_char(I_shipment) ||', Item: '||L_item);
         close C_GET_OLD_TSF_AVE_RETAIL;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_TSF_QTY',
                       'TSFDETAIL',
                       'Tsf_no: '||I_tsf_no||' ,Item: '||I_item);
      open C_GET_TSF_QTY;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_TSF_QTY',
                       'TSFDETAIL',
                       'Tsf_no: '||I_tsf_no||' ,Item: '||I_item);
      fetch C_GET_TSF_QTY into L_tsf_qty;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_TSF_QTY',
                       'TSFDETAIL',
                       'Tsf_no: '||I_tsf_no||' ,Item: '||I_item);
      close C_GET_TSF_QTY;

      if L_first_leg_ind = 'N' then
         if I_adj_qty is NOT NULL then
            L_tsf_qty := I_adj_qty;
         end if;
         L_tsf_qty := (-1)*L_tsf_qty;
      end if;

      --update item_loc_soh records of the component items
      if L_pack_ind = 'Y' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_COUNT_COMP',
                          'PACKITEM',
                          'Item: '||L_pack_item);
         open C_COUNT_COMP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_COUNT_COMP',
                          'PACKITEM',
                          'Item: '||L_pack_item);
         fetch C_COUNT_COMP into L_pack_comp_count;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_COUNT_COMP',
                          'PACKITEM',
                          'Item: '||L_pack_item);
         close C_COUNT_COMP;

         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_COMP_ITEMS',
                          'PACKITEM',
                          'Item: '||L_pack_item);
         open C_GET_COMP_ITEMS;

         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_SUM_PACK_COMP',
                          'PACKITEM',
                          'Pack_no: '||L_pack_item);
         open C_GET_SUM_PACK_COMP;

         FOR i in 1..L_pack_comp_count LOOP
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_COMP_ITEMS',
                             'PACKITEM',
                             'Item: '||L_pack_item);
            fetch C_GET_COMP_ITEMS into L_comp_item,
                                        L_item_qty;

            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_SUM_PACK_COMP',
                             'PACKITEM',
                             'Pack_no: '||L_pack_item);
            fetch C_GET_SUM_PACK_COMP into L_sum_pack_comp;

            L_pack_unit_retail := L_tsf_unit_retail*(L_item_qty/L_sum_pack_comp);

            --get old item_loc_soh.finisher
            SQL_LIB.SET_MARK('OPEN',
                             'C_OLD_RETAIL',
                             'ITEM_LOC_SOH',
                             'Tsf_no: '||to_char(I_tsf_no)||', Item: '||I_item);
            open C_OLD_RETAIL;

            SQL_LIB.SET_MARK('FETCH',
                             'C_OLD_RETAIL',
                             'ITEM_LOC_SOH',
                             'Item: '||I_item);
            fetch C_OLD_RETAIL into L_old_retail,
                                    L_old_qty;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_OLD_RETAIL',
                             'ITEM_LOC_SOH',
                             'Item: '||I_item);
            close C_OLD_RETAIL;

            L_new_qty   := L_old_qty + L_tsf_qty;

            if L_new_qty = 0 then
               L_new_retail := NULL;
               L_new_qty    := NULL;
            else
               L_new_retail := ((L_old_qty*L_old_retail) + (L_tsf_qty*L_pack_unit_retail))/L_new_qty;
               if L_new_retail = 0 then
                  L_new_retail := NULL;
               end if;
            end if;

            SQL_LIB.SET_MARK('UPDATE',
                             'ITEM_LOC_SOH',
                             'Tsf_no: '||to_char(I_tsf_no)||',Item: '||I_item,
                             NULL);
            update item_loc_soh
               set finisher_av_retail = L_new_retail,
                   finisher_units  = L_new_qty
             where item     = L_item
               and loc      = L_finisher_loc
               and loc_type = L_finisher_loc_type;

         END LOOP;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_COMP_ITEMS',
                          'PACKITEM',
                          'Pack_no: '||I_item);
         close C_GET_COMP_ITEMS;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_SUM_PACK_COMP',
                          'PACKITEM',
                          'Pack_no: '||L_pack_item);
         close C_GET_SUM_PACK_COMP;
      else
         --get old item_loc_soh.finisher
         SQL_LIB.SET_MARK('OPEN',
                          'C_OLD_RETAIL',
                          'ITEM_LOC_SOH',
                          'Tsf_no: '||to_char(I_tsf_no)||', Item: '||I_item);
         open C_OLD_RETAIL;

         SQL_LIB.SET_MARK('FETCH',
                          'C_OLD_RETAIL',
                          'ITEM_LOC_SOH',
                          'Item: '||I_item);
         fetch C_OLD_RETAIL into L_old_retail,
                                 L_old_qty;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_OLD_RETAIL',
                          'ITEM_LOC_SOH',
                          'Item: '||I_item);
         close C_OLD_RETAIL;

         L_new_qty   := L_old_qty + L_tsf_qty;

         if L_new_qty = 0 then
            L_new_retail := NULL;
            L_new_qty    := NULL;
         else
            L_new_retail := ((L_old_qty*L_old_retail) + (L_tsf_qty*L_tsf_unit_retail))/L_new_qty;
            if L_new_retail = 0 then
               L_new_retail := NULL;
            end if;
         end if;

         SQL_LIB.SET_MARK('UPDATE',
                          'ITEM_LOC_SOH',
                          'Tsf_no: '||to_char(I_tsf_no)||',Item: '||I_item,
                          NULL);
         update item_loc_soh
            set finisher_av_retail = L_new_retail,
                finisher_units  = L_new_qty
          where item     = L_item
            and loc      = L_finisher_loc
            and loc_type = L_finisher_loc_type;
      end if;

      --update tsfdetail.finisher_av_retail and finisher_units
      if PUT_TSF_AV_RETAIL(O_error_message,
                           L_tsf_unit_retail,
                           L_tsf_qty,
                           L_tsf_no,
                           L_item) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORDER_LOCKED',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PUT_ILS_AV_RETAIL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_processed_item     IN OUT   BOL_SQL.BOL_SHIPSKU_TBL,
                      I_unprocessed_item   IN       BOL_SQL.BOL_SHIPSKU_TBL,
                      I_from_loc           IN       SHIPMENT.FROM_LOC%TYPE,
                      I_to_loc             IN       SHIPMENT.TO_LOC%TYPE)
RETURN BOOLEAN IS
   L_item_rec            ITEM_MASTER%ROWTYPE;
   L_program             VARCHAR2(50) := 'BOL_SQL.PROCESS_ITEM';
   L_inventory_ctr       NUMBER :=0;
   L_sellable_ctr        NUMBER :=0;
   L_index               NUMBER := 0;
   L_same_class          BOOLEAN := FALSE;
   L_weight_cnvt         ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_uom_class           UOM_CLASS.UOM_CLASS%TYPE;
   L_sellable_item_tbl   BOL_SQL.bol_shipsku_tbl;
   L_inventory_item_tbl  BOL_SQL.bol_shipsku_tbl;
   L_orditem_TBL         BTS_ORDITEM_QTY_TBL := BTS_ORDITEM_QTY_TBL();
   L_sell_items          ITEM_TBL        := ITEM_TBL();
   L_sell_qty            QTY_TBL         := QTY_TBL();
   L_sell_inv_status     INV_STATUS_TBL  := INV_STATUS_TBL();
   L_proc_item_added     BOOLEAN := FALSE;

BEGIN

   if I_unprocessed_item.first is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_unprocessed_item ',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   for b in I_unprocessed_item.first..I_unprocessed_item.last LOOP
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_rec,
                                         I_unprocessed_item(b).item) = FALSE then
         return FALSE;
      end if;

      if L_item_rec.sellable_ind = 'Y'   and
         L_item_rec.orderable_ind = 'N'  and
         L_item_rec.item_xform_ind = 'Y' then

         L_sellable_ctr := L_sellable_item_tbl.COUNT + 1;
         L_sellable_item_tbl(L_sellable_ctr).distro_no    := I_unprocessed_item(b).distro_no;
         L_sellable_item_tbl(L_sellable_ctr).distro_type  := I_unprocessed_item(b).distro_type;
         L_sellable_item_tbl(L_sellable_ctr).carton       := I_unprocessed_item(b).carton;
         L_sellable_item_tbl(L_sellable_ctr).item         := I_unprocessed_item(b).item;
         L_sellable_item_tbl(L_sellable_ctr).ship_qty     := I_unprocessed_item(b).ship_qty;
         L_sellable_item_tbl(L_sellable_ctr).weight       := I_unprocessed_item(b).weight;
         L_sellable_item_tbl(L_sellable_ctr).weight_uom   := I_unprocessed_item(b).weight_uom;
         L_sellable_item_tbl(L_sellable_ctr).inv_status   := I_unprocessed_item(b).inv_status;

      else

         L_inventory_ctr := L_inventory_item_tbl.COUNT + 1;
         L_inventory_item_tbl(L_inventory_ctr).distro_no    := I_unprocessed_item(b).distro_no;
         L_inventory_item_tbl(L_inventory_ctr).distro_type  := I_unprocessed_item(b).distro_type;
         L_inventory_item_tbl(L_inventory_ctr).carton       := I_unprocessed_item(b).carton;
         L_inventory_item_tbl(L_inventory_ctr).item         := I_unprocessed_item(b).item;
         L_inventory_item_tbl(L_inventory_ctr).ship_qty     := I_unprocessed_item(b).ship_qty;
         L_inventory_item_tbl(L_inventory_ctr).weight       := I_unprocessed_item(b).weight;
         L_inventory_item_tbl(L_inventory_ctr).weight_uom   := I_unprocessed_item(b).weight_uom;
         L_inventory_item_tbl(L_inventory_ctr).inv_status   := I_unprocessed_item(b).inv_status;

      end if;

   end loop;

   if L_sellable_item_tbl.first is not NULL then
      for a in L_sellable_item_tbl.first..L_sellable_item_tbl.last LOOP

         L_sell_items.EXTEND;
         L_sell_items(L_sell_items.COUNT) := L_sellable_item_tbl(a).item;

         L_sell_qty.EXTEND;
         L_sell_qty(L_sell_qty.COUNT) := L_sellable_item_tbl(a).ship_qty;

         L_sell_inv_status.EXTEND;
         L_sell_inv_status(L_sell_inv_status.COUNT) := L_sellable_item_tbl(a).inv_status;


         if L_sellable_item_tbl(1).distro_type IN ('V', 'D', 'T') then
            if ITEM_XFORM_SQL.TSF_ORDERABLE_ITEM_INFO(O_error_message,
                                                      L_orditem_tbl,
                                                      L_sell_items,
                                                      L_sell_qty,
                                                      L_sell_inv_status,
                                                      L_sellable_item_tbl(1).distro_no) = FALSE then
               return FALSE;
            end if;
         elsif L_sellable_item_tbl(1).distro_type = 'A' then
            if ITEM_XFORM_SQL.ALLOC_ORDERABLE_ITEM_INFO(O_error_message,
                                                        L_orditem_TBL,
                                                        L_sell_items,
                                                        L_sell_qty,
                                                        L_sellable_item_tbl(1).distro_no,
                                                        I_to_loc) = FALSE then
               return FALSE;
            end if;
         else
            O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_TYPE',NULL, NULL, NULL);
            return FALSE;
         end if;

         L_index := L_inventory_item_tbl.COUNT + 1;

         if L_orditem_tbl.first is not NULL then
            for i in L_orditem_tbl.first..L_orditem_tbl.last LOOP

              L_inventory_item_tbl(L_index).item                 := L_orditem_tbl(i).orderable_item;
              L_inventory_item_tbl(L_index).inv_status           := L_orditem_tbl(i).inv_status;
              L_inventory_item_tbl(L_index).ship_qty             := L_orditem_tbl(i).qty;
              L_inventory_item_tbl(L_index).weight               := NULL;
              L_inventory_item_tbl(L_index).weight_uom           := NULL;
              L_inventory_item_tbl(L_index).distro_no            := L_sellable_item_tbl(a).distro_no;
              L_inventory_item_tbl(L_index).distro_type          := L_sellable_item_tbl(a).distro_type;
              L_inventory_item_tbl(L_index).carton               := L_sellable_item_tbl(a).carton;
              L_index := L_index + 1;

           end LOOP;
         end if;
         L_orditem_tbl.delete;
         L_sell_items.delete;
      end loop;
   end if;

   L_sellable_item_tbl.delete;

   -- parse item processing
   if L_inventory_item_tbl.first is not NULL then
      for k in L_inventory_item_tbl.first .. L_inventory_item_tbl.last LOOP
         -- weight and weight_uom needs to be either both defined or both null
         L_proc_item_added := FALSE;
         if (L_inventory_item_tbl(k).weight IS NULL AND L_inventory_item_tbl(k).weight_uom is not NULL) or
            (L_inventory_item_tbl(k).weight IS NOT NULL AND L_inventory_item_tbl(k).weight_uom is NULL) then

            O_error_message := SQL_LIB.CREATE_MSG('WGT_WGTUOM_REQUIRED',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;

         end if;

         if L_inventory_item_tbl(k).weight_uom is not NULL then

         -- L_detail_tbl.weight_uom will already be converted to UPPER case
            if NOT UOM_SQL.GET_CLASS(O_error_message,
                                     L_uom_class,
                                     L_inventory_item_tbl(k).weight_uom) then
               return FALSE;
            end if;

            if L_uom_class != 'MASS' then
               O_error_message := SQL_LIB.CREATE_MSG('INV_WGTUOM_CLASS',
                                                     L_inventory_item_tbl(k).weight_uom,
                                                     L_uom_class,
                                                     NULL);
               return FALSE;
            end if;

         end if;

         if O_processed_item.first is not NULL then
            for item_proc in O_processed_item.first..O_processed_item.last LOOP

               if (L_inventory_item_tbl(k).carton = O_processed_item(item_proc).carton) and
                  (L_inventory_item_tbl(k).item = O_processed_item(item_proc).item) and
                  (L_inventory_item_tbl(k).inv_status = O_processed_item(item_proc).inv_status) then
                  L_proc_item_added := TRUE;
                  O_processed_item(item_proc).ship_qty := O_processed_item(item_proc).ship_qty + L_inventory_item_tbl(k).ship_qty;

                  if O_processed_item(item_proc).weight is NULL or
                     O_processed_item(item_proc).weight_uom is NULL then

                     O_processed_item(item_proc).weight := NULL;
                     O_processed_item(item_proc).weight_uom := NULL;

                  else
                     if O_processed_item(item_proc).weight_uom = L_inventory_item_tbl(k).weight_uom then
                        O_processed_item(item_proc).weight := O_processed_item(item_proc).weight + L_inventory_item_tbl(k).weight;
                     else
                        if NOT UOM_SQL.CHECK_CLASS(O_error_message,
                                                   L_same_class,
                                                   L_inventory_item_tbl(k).weight_uom,
                                                   O_processed_item(item_proc).weight_uom) then
                           return FALSE;
                        end if;

                        if L_same_class = TRUE then
                           if NOT UOM_SQL.CONVERT(O_error_message,
                                                  L_weight_cnvt,
                                                  O_processed_item(item_proc).weight_uom,
                                                  L_inventory_item_tbl(k).weight,
                                                  L_inventory_item_tbl(k).weight_uom,
                                                  L_inventory_item_tbl(k).item,
                                                  NULL,
                                                  NULL) then
                              return FALSE;
                           end if;
                           O_processed_item(item_proc).weight := O_processed_item(item_proc).weight + L_inventory_item_tbl(k).weight;
                        else
                           O_error_message := SQL_LIB.CREATE_MSG('CANNOT_CNVT_UOM',
                                                                 UPPER(L_inventory_item_tbl(k).weight_uom),
                                                                 O_processed_item(item_proc).weight_uom,
                                                                 NULL);
                           return FALSE;
                        end if;
                     end if;
                  end if;

               end if;

            end loop;
         end if;

         if L_proc_item_added = FALSE then
            -- if not already in O_item_table, add it
            if O_processed_item.first is NULL then
               L_index := 1;
            else
               L_index := O_processed_item.COUNT + 1;
            end if;
            O_processed_item(L_index).carton          := L_inventory_item_tbl(k).carton;
            O_processed_item(L_index).item            := L_inventory_item_tbl(k).item;
            O_processed_item(L_index).ship_qty        := L_inventory_item_tbl(k).ship_qty;
            O_processed_item(L_index).weight          := L_inventory_item_tbl(k).weight;
            O_processed_item(L_index).weight_uom      := L_inventory_item_tbl(k).weight_uom;
            O_processed_item(L_index).distro_type     := L_inventory_item_tbl(k).distro_type;
            O_processed_item(L_index).distro_no       := L_inventory_item_tbl(k).distro_no;
            O_processed_item(L_index).inv_status      := L_inventory_item_tbl(k).inv_status;
         end if;
      end loop;

   else
      O_error_message := SQL_LIB.CREATE_MSG('NO_REC_TO_PROCESS',
                                            NULL,
                                            L_program,
                                            NULL);
         return FALSE;
   end if;
   L_inventory_item_tbl.delete;
   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_ITEM;
-----------------------------------------------------------------------------------------------
FUNCTION GET_ALLOCDETAIL_ALLOC_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_alloc_qty       IN OUT   ALLOC_DETAIL.QTY_ALLOCATED%TYPE,
                                   I_alloc_no        IN       ALLOC_DETAIL.ALLOC_NO%TYPE,
                                   I_item            IN       ALLOC_HEADER.ITEM%TYPE,
                                   I_to_loc          IN       ALLOC_DETAIL.TO_LOC%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'BOL_SQL.GET_ALLOCDETAIL_ALLOC_QTY';

   cursor C_ALLOC_QTY is
      select SUM(ad.qty_allocated - NVL(ad.qty_transferred,0)) qty
        from alloc_detail ad,
             alloc_header ah,
             (select wh location
                from wh
               where physical_wh = I_to_loc
               union
              select store
                from store
               where store = I_to_loc) loc
       where ah.alloc_no = I_alloc_no
         and ah.alloc_no = ad.alloc_no
         and ah.item     = I_item
         and ad.to_loc   = loc.location;

BEGIN
   if I_alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_alloc_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_ALLOC_QTY;
   fetch C_ALLOC_QTY into O_alloc_qty;
   close C_ALLOC_QTY;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ALLOCDETAIL_ALLOC_QTY;
---------------------------------------------------------------------------------
FUNCTION LOCK_BOLSHIPMENT (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_bol_no        IN     BOL_SHIPMENT.BOL_NO%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_BOLSHIPMENT is
      select 'x'
        from bol_shipment
       where bol_no = I_bol_no
         for update nowait;

BEGIN

   open C_LOCK_BOLSHIPMENT;
   close C_LOCK_BOLSHIPMENT;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('BOL_REC_LOCK',
                                             I_bol_no,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_SETUP_SQL.LOCK_ORDHEAD',
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_BOLSHIPMENT;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_ALLOC_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid         IN OUT BOOLEAN,
                               I_distro        IN     BOL_SHIPSKU.DISTRO_NO%TYPE,
                               I_distro_type   IN     BOL_SHIPSKU.DISTRO_TYPE%TYPE,
                               I_from_loc      IN     BOL_SHIPMENT.FROM_LOC%TYPE,
                               I_from_loc_type IN     BOL_SHIPMENT.FROM_LOC_TYPE%TYPE,
                               I_to_loc        IN     BOL_SHIPMENT.TO_LOC%TYPE,
                               I_to_loc_type   IN     BOL_SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'BOL_SQL.VALIDATE_TSF_ALLOC_NO';
   L_dummy     VARCHAR2(1);

   cursor C_TRANSFER is
      select 'X'
        from v_tsfhead vt,
             tsfdetail t,
             product_config_options pco
       where vt.tsf_no = t.tsf_no
         and vt.tsf_no = I_distro
         and nvl(vt.overall_status,'X') in ('A','N')
         and nvl(vt.leg_1_freight_code,'X') <> 'H'
         and nvl(vt.leg_2_freight_code,'X') <> 'H'
         and nvl(vt.finisher_type, 'X') <> 'E'
         and t.tsf_qty - NVL(t.ship_qty,0) > 0
         and (vt.from_loc_type = I_from_loc_type
              and (vt.from_loc = I_from_loc
                   or vt.from_loc in (select wh from wh where physical_wh = I_from_loc)))
         and (vt.to_loc_type = I_to_loc_type
              and (vt.to_loc = I_to_loc
                   or vt.to_loc in (select wh from wh where physical_wh = I_to_loc )))
       and (pco.oms_ind = 'N'
            or (pco.oms_ind = 'Y'
                and vt.tsf_type <> 'CO'))
         and rownum = 1
       union
      select 'X'
        from v_tsfhead vt,
             tsfdetail t,
             product_config_options pco
       where vt.child_tsf_no = t.tsf_no
         and vt.tsf_no = I_distro
         and nvl(vt.overall_status,'X') = 'N'
         and nvl(vt.leg_1_freight_code,'X') <> 'H'
         and nvl(vt.leg_2_freight_code,'X') <> 'H'
         and nvl(vt.finisher_type, 'X') <> 'E'
         and t.tsf_qty - NVL(t.ship_qty,0) > 0
         and vt.leg_1_status = 'C'
         and vt.leg_2_status in ('A','S')
         and (vt.from_loc_type = I_from_loc_type
              and (vt.from_loc = I_from_loc
                   or vt.from_loc in (select wh from wh where physical_wh = I_from_loc)))
         and (vt.to_loc_type = I_to_loc_type
              and (vt.to_loc = I_to_loc
                   or vt.to_loc in (select wh from wh where physical_wh = I_to_loc )))
         and (pco.oms_ind = 'N'
              or (pco.oms_ind = 'Y'
                  and vt.tsf_type <> 'CO'))
         and rownum = 1;

  cursor C_WH_ALLOC is
     select 'X'
       from alloc_header ah,
            v_item_master vitm
      where ah.alloc_no = I_distro
        and ah.status = 'A'
        and ah.item = vitm.item
        and ah.wh in (select wh from v_wh where physical_wh =  I_from_loc)
        and exists (select 'x'
                      from alloc_detail ad, v_wh vwh
                     where ad.alloc_no = ah.alloc_no
                       and ad.qty_allocated - NVL(ad.qty_transferred,0) > 0
                       and (ad.to_loc = I_to_loc or
                           (ad.to_loc = vwh.wh and physical_wh = I_to_loc))
                       and rownum = 1)
        and rownum = 1;

   cursor C_STORE_ALLOC is
      select 'X'
        from alloc_header ah,
             v_item_master vitm
       where ah.alloc_no = I_distro
         and ah.status = 'A'
         and ah.item = vitm.item
         and ah.wh in (select wh from v_wh where physical_wh = I_from_loc)
         and exists (select 'x'
                       from v_alloc_detail ad
                      where ad.alloc_no = ah.alloc_no
                        and ad.qty_allocated - NVL(ad.qty_transferred,0) > 0
                        and ad.to_loc = I_to_loc
                        and rownum = 1)
         and rownum = 1;

BEGIN

   if I_distro is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_distro',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_distro_type is NULL or I_distro_type not in ('T','A') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'I_distro_type',
                                             I_distro_type,
                                             'T or A');
      return FALSE;
   end if;

   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_from_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_to_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_to_loc_type is NULL or I_to_loc_type not in ('W','S') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'I_to_loc_type',
                                             I_to_loc_type,
                                             'W or S');
      return FALSE;
   end if;

   if I_distro_type = 'T' then
      open C_TRANSFER;
      fetch C_TRANSFER into L_dummy;
      if C_TRANSFER%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('SEL_DIFF_TSF', NULL, NULL, NULL);
         O_valid := FALSE;
      end if;
      close C_TRANSFER;
   else
      if I_to_loc_type = 'W' then
         open C_WH_ALLOC;
         fetch C_WH_ALLOC into L_dummy;
         if C_WH_ALLOC%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('SEL_DIFF_ALLOC', NULL, NULL, NULL);
            O_valid := FALSE;
         end if;
         close C_WH_ALLOC;
      else
         open C_STORE_ALLOC;
         fetch C_STORE_ALLOC into L_dummy;
         if C_STORE_ALLOC%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('SEL_DIFF_ALLOC', NULL, NULL, NULL);
            O_valid := FALSE;
         end if;
         close C_STORE_ALLOC;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_TSF_ALLOC_NO;
----------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_SELECTED(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_item_selected   IN OUT   BOOLEAN,
                             I_bol_no          IN       BOL_SHIPMENT.BOL_NO%TYPE,
                             I_distro_no       IN       BOL_DISTRO_ITEMS_TEMP.DISTRO_NO%TYPE,
                             I_distro_type     IN       BOL_DISTRO_ITEMS_TEMP.DISTRO_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'BOL_SQL.CHECK_ITEM_SELECTED';
   L_dummy     VARCHAR2(1);

   cursor C_ITEM_SELECTED is
      select 'X'
        from bol_distro_items_temp
       where bol_no = I_bol_no
         and distro_no = I_distro_no
         and distro_type = I_distro_type
         and select_ind = 'Y'
         and rownum = 1;

BEGIN
   open C_ITEM_SELECTED;
   fetch C_ITEM_SELECTED into L_dummy;

   if C_ITEM_SELECTED%FOUND then
      O_item_selected := TRUE;
   else
      O_item_selected := FALSE;
   end if;
   close C_ITEM_SELECTED;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_ITEM_SELECTED;
----------------------------------------------------------------------------------------------
FUNCTION BOL_SHIPSKU_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists        IN OUT BOOLEAN,
                            I_bol_no        IN     BOL_SHIPSKU.BOL_NO%TYPE,
                            I_distro_no     IN     BOL_SHIPSKU.DISTRO_NO%TYPE,
                            I_distro_type   IN     BOL_SHIPSKU.DISTRO_TYPE%TYPE,
                            I_item          IN     BOL_SHIPSKU.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'BOL_SQL.BOL_SHIPSKU_EXISTS';
   L_dummy     VARCHAR2(1);

   cursor C_BOL_SHIPSKU_EXISTS is
      select 'X'
        from bol_shipsku
       where bol_no = I_bol_no
         and distro_no = I_distro_no
         and distro_type = I_distro_type
         and item = I_item
         and rownum = 1;

BEGIN
   open C_BOL_SHIPSKU_EXISTS;
   fetch C_BOL_SHIPSKU_EXISTS into L_dummy;

   if C_BOL_SHIPSKU_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   close C_BOL_SHIPSKU_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END BOL_SHIPSKU_EXISTS;
----------------------------------------------------------------------------------------------
--public function--
FUNCTION RECEIPT_PUT_ALLOC_BOL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_bol_no          IN       SHIPMENT.BOL_NO%TYPE,
                               I_phy_to_loc      IN       SHIPMENT.TO_LOC%TYPE,
                               I_ship_date       IN       PERIOD.VDATE%TYPE,
                               I_shipment        IN       SHIPMENT.SHIPMENT%TYPE,
                               I_phy_from_loc    IN       ITEM_LOC.LOC%TYPE,
                               I_to_loc_type     IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                               I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                               I_alloc_no        IN       TSFHEAD.TSF_NO%TYPE,
                               I_status          IN       TSFHEAD.STATUS%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(50)              := 'BOL_SQL.RECEIPT_PUT_ALLOC_BOL';

BEGIN
   if I_bol_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_bol_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_phy_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_phy_from_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_phy_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_phy_to_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_ship_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ship_date',
                                            L_program,
                                            NULL );
      return FALSE;
   end if;
   ---
   if I_phy_from_loc = I_phy_to_loc then
      O_error_message := SQL_LIB.CREATE_MSG('SAME_LOC',
                                            I_phy_from_loc,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   LP_bol_rec := NULL;
   ---
   if INIT_BOL_PROCESS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   LP_bol_rec.bol_no            := I_bol_no;
   LP_bol_rec.ship_no           := I_shipment;
   LP_bol_rec.phy_from_loc      := I_phy_from_loc;
   LP_bol_rec.phy_from_loc_type := I_from_loc_type;
   LP_bol_rec.phy_to_loc        := I_phy_to_loc;
   LP_bol_rec.phy_to_loc_type   := I_to_loc_type;
   LP_bol_rec.tran_date         := I_ship_date;
   ---
   if DATES_SQL.GET_EOW_DATE(O_error_message,
                             LP_bol_rec.eow_date,
                             I_ship_date) = FALSE then
      return FALSE;
   end if;
   ---
   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                LP_bol_rec.franchise_ordret_ind,
                                                I_phy_from_loc,
                                                I_from_loc_type,
                                                I_phy_to_loc,
                                                I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   if I_from_loc_type = 'S' then
      open C_GET_STORE_TYPE(I_phy_from_loc);
      fetch C_GET_STORE_TYPE into LP_bol_rec.from_store_type,
                                  LP_bol_rec.from_stockholding_ind;
      close C_GET_STORE_TYPE;
   end if;

   if I_to_loc_type = 'S' then
      open C_GET_STORE_TYPE(I_phy_to_loc);
      fetch C_GET_STORE_TYPE into LP_bol_rec.to_store_type,
                                  LP_bol_rec.to_stockholding_ind;
      close C_GET_STORE_TYPE;
   end if;

   LP_receipt_ind := 'Y';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RECEIPT_PUT_ALLOC_BOL;
-------------------------------------------------------------------------------
FUNCTION UPDATE_SHIP_SEQ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'BOL_SQL.UPDATE_SHIP_SEQ';
   L_dummy     VARCHAR2(1);

   cursor C_ALLOC_BOL_EXISTS_SEQ is
      select NVL(MAX(seq_no + 1), 1)
        from shipsku
       where shipment =  LP_bol_rec.ship_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_ALLOC_BOL_EXISTS_SEQ',
                    'SHIPSKU',
                    'Ship No: '||TO_CHAR(LP_bol_rec.ship_no));
   open C_ALLOC_BOL_EXISTS_SEQ;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ALLOC_BOL_EXISTS_SEQ',
                    'SHIPSKU',
                    'Ship No: '||TO_CHAR(LP_bol_rec.ship_no));
   fetch C_ALLOC_BOL_EXISTS_SEQ into GP_ship_seq_no;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ALLOC_BOL_EXISTS_SEQ',
                    'SHIPSKU',
                    'Ship No: '||TO_CHAR(LP_bol_rec.ship_no));
   close C_ALLOC_BOL_EXISTS_SEQ;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_SHIP_SEQ;
----------------------------------------------------------------------------------------------
FUNCTION POPULATE_SHIPSKU(O_error_message IN OUT rtk_errors.rtk_text%TYPE)

RETURN BOOLEAN IS

   item_cnt   BINARY_INTEGER := 1;
   loc_cnt    BINARY_INTEGER := 1;
   i          BINARY_INTEGER := 1;

   L_intran_qty                 item_loc_soh.stock_on_hand%TYPE := 0;

   -- This holds the pack's av_cost and retail at from loc.
   -- L_pack_loc_av_cost is used for prorating the charge.
   -- L_pack_loc_retail is used for writing shipsku.unit_retail.
   L_pack_no                    item_master.item%TYPE;
   L_pack_loc_av_cost           item_loc_soh.av_cost%TYPE;
   L_pack_loc_retail            item_loc.unit_retail%TYPE;

   -- This holds the from loc item cost depending on the accounting method.
   L_from_wac                   item_loc_soh.av_cost%TYPE := NULL;
   L_from_av_cost               item_loc_soh.av_cost%TYPE := NULL;
   L_from_unit_retail           item_loc.unit_retail%TYPE := NULL;

   -- for charges
   L_total_chrgs_prim           item_loc.unit_retail%TYPE := 0;
   L_profit_chrgs_to_loc        NUMBER                    := 0;
   L_exp_chrgs_to_loc           NUMBER                    := 0;
   L_pack_total_chrgs_prim      item_loc.unit_retail%TYPE := NULL;
   L_pack_profit_chrgs_to_loc   NUMBER                    := NULL;
   L_pack_exp_chrgs_to_loc      NUMBER                    := NULL;

   -- This holds the item's (or component item's) transfer unit cost used for
   -- stock ledger. Shipsku.unit_cost should be based on the same unit cost.
   L_tsf_unit_cost              item_loc_soh.av_cost%TYPE := NULL;

   -- This holds the pack's transfer unit cost, aggregated by components.
   L_pack_tsf_unit_cost         item_loc_soh.av_cost%TYPE := 0;

   -- These variables hold the item's (or pack's) total cost/retail/charges/qty
   -- across all virtual locations.
   L_ss_cost                    item_loc_soh.av_cost%TYPE := 0;
   L_ss_prim_chrgs              item_loc_soh.av_cost%TYPE := 0;
   L_ss_from_chrgs              item_loc_soh.av_cost%TYPE := 0;
   L_ss_retail                  item_loc.unit_retail%TYPE := 0;
   L_ss_qty                     item_loc_soh.stock_on_hand%TYPE := 0;
   L_ss_exp_weight              shipsku.weight_expected%TYPE := NULL;
   L_ss_exp_weight_uom          shipsku.weight_expected_uom%TYPE := NULL;

   L_ins_cost                   shipsku.unit_cost%TYPE   := NULL;
   L_ins_retail                 shipsku.unit_retail%TYPE := NULL;
   L_ss_rcv_qty                 item_loc_soh.stock_on_hand%TYPE := 0;

   L_ship_no                    shipment.shipment%TYPE := NULL;
   L_ss_seq_no                  shipsku.seq_no%TYPE    := NULL;

   -- for simple pack catch weight processing
   L_cuom                       item_supp_country.cost_uom%TYPE := NULL;
   L_vir_weight_cuom            item_loc_soh.average_weight%TYPE := NULL;
   L_intran_weight_cuom         item_loc_soh.average_weight%TYPE := NULL;
   L_inventory_treatment_ind    system_options.tsf_force_close_ind%TYPE := NULL;

   -- cursors
   cursor C_ITEM_IN_PACK is
       select v.item,
              v.qty,
              im.dept,
              im.class,
              im.subclass,
              im.sellable_ind,
              im.item_xform_ind,
              im.inventory_ind
         from item_master im,
              v_packsku_qty v
        where v.pack_no = L_pack_no
          and im.item   = v.item;

   cursor C_SHIPSKU_COST_RETAIL is
       select unit_cost,
              unit_retail
         from shipsku
        where shipment  = LP_bol_rec.ship_no
          and distro_no = LP_bol_rec.distros(i).tsf_no
          and item      = LP_bol_rec.distros(i).bol_items(item_cnt).item
          and rownum    = 1;

BEGIN

   FOR item_cnt IN LP_bol_rec.distros(i).bol_items.FIRST..LP_bol_rec.distros(i).bol_items.LAST LOOP

      if LP_bol_rec.distros(i).tsf_type = 'EG' then
         L_ship_no := LP_bol_rec.ship_no;
         L_ss_seq_no := LP_bol_rec.distros(i).bol_items(item_cnt).ss_seq_no;
      else
         L_ship_no := NULL;
         L_ss_seq_no := NULL;
      end if;

      open C_SHIPSKU_COST_RETAIL;
      fetch C_SHIPSKU_COST_RETAIL into L_ins_cost,L_ins_retail;
      close C_SHIPSKU_COST_RETAIL;

      FOR loc_cnt IN LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs.FIRST..LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs.LAST LOOP

         if LP_bol_rec.distros(i).bol_items(item_cnt).pack_ind = 'N' then

            if(L_ins_cost is NULL or L_ins_retail is NULL) then
               if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                             L_from_wac,
                                             LP_bol_rec.distros(i).bol_items(item_cnt).item,
                                             LP_bol_rec.distros(i).bol_items(item_cnt).dept,
                                             LP_bol_rec.distros(i).bol_items(item_cnt).class,
                                             LP_bol_rec.distros(i).bol_items(item_cnt).subclass,
                                             LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                             LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                             LP_bol_rec.tran_date,
                                             LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                             LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                  return FALSE;
               end if;

               L_tsf_unit_cost:= L_from_wac;
               if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(
                                      O_error_message,
                                      L_total_chrgs_prim,
                                      L_profit_chrgs_to_loc,
                                      L_exp_chrgs_to_loc,
                                      'T',                    --transfer
                                      LP_bol_rec.distros(i).tsf_no,
                                      LP_bol_rec.distros(i).bol_items(item_cnt).td_tsf_seq_no,
                                      L_ship_no,
                                      L_ss_seq_no,
                                      LP_bol_rec.distros(i).bol_items(item_cnt).item,
                                      NULL,                   --pack_item
                                      LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                      LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                      LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                      LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                  return FALSE;
               end if;

               --before writing the shipsku retail check if from loc is an external finisher.
               --if so, the unit retail is null. Get the correct retail.
               --For an orderable BTS item, unit_retail is also not defined, in which case, get its sellable's retail.
               if BOL_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                L_from_av_cost,
                                                L_from_unit_retail,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).item,
                                                'N',
                                                LP_bol_rec.distros(i).bol_items(item_cnt).sellable_ind,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).item_xform_ind,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                  return FALSE;
               end if;

               --If unit retail is still NULL, it could be for an orderable/non-sellable non-pack/non-tranformed item.
               --Set unit_retail to 0 for writing to shipsku.
               if L_from_unit_retail is NULL then
                  L_from_unit_retail := 0;
               end if;

               L_ss_prim_chrgs := L_ss_prim_chrgs +
                  (L_total_chrgs_prim * LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);

               -- if tsf_cost is defined, shipsku.unit_cost should be based off tsf_cost
               -- sum up shipsku cost across all virtual locations


               L_ss_cost := L_ss_cost + L_tsf_unit_cost *
                  LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;

               L_ss_retail := L_ss_retail + (L_from_unit_retail *
                  LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);
            end if;
            L_ss_qty := L_ss_qty + LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;

         else --pack
            if(L_ins_cost is NULL or L_ins_retail is NULL) then
               L_pack_no := LP_bol_rec.distros(i).bol_items(item_cnt).item;
               if BOL_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                L_pack_loc_av_cost,
                                                L_pack_loc_retail,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).item,
                                                'Y',
                                                LP_bol_rec.distros(i).bol_items(item_cnt).sellable_ind,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).item_xform_ind,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                  return FALSE;
               end if;

               -- If weight is not defined for a simple pack catch weight item,
               -- use the virtual from loc's average weight for the pack to derive weight.
               if LP_bol_rec.distros(i).bol_items(item_cnt).simple_pack_ind = 'Y' and
                  LP_bol_rec.distros(i).bol_items(item_cnt).catch_weight_ind = 'Y' then

                  if not CATCH_WEIGHT_SQL.PRORATE_WEIGHT(O_error_message,
                                                         L_vir_weight_cuom,
                                                         L_cuom,
                                                         LP_bol_rec.distros(i).bol_items(item_cnt).item,
                                                         LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                         LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                         LP_bol_rec.distros(i).bol_items(item_cnt).weight,
                                                         LP_bol_rec.distros(i).bol_items(item_cnt).weight_uom,
                                                         LP_bol_rec.distros(i).bol_items(item_cnt).qty,
                                                         LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty) then
                     return FALSE;
                  end if;

                  -- L_intran_qty is either 0 or vir_qty
                  if L_intran_qty != 0 then
                     L_intran_weight_cuom := L_vir_weight_cuom;
                  end if;

                  -- sum up shipsku weight_expected across all virtual locations
                  if LP_bol_rec.distros(i).bol_items(item_cnt).weight is NULL or
                     LP_bol_rec.distros(i).bol_items(item_cnt).weight_uom is NULL then
                     if L_cuom = 'EA' then
                        L_ss_exp_weight := NVL(L_ss_exp_weight, 0) + LP_bol_rec.distros(i).bol_items(item_cnt).qty;
                    else
                        L_ss_exp_weight := NVL(L_ss_exp_weight, 0) + L_vir_weight_cuom;
                     end if;
                  else
                     L_ss_exp_weight := NVL(L_ss_exp_weight, 0) + L_vir_weight_cuom;
                  end if;
                  L_ss_exp_weight_uom := L_cuom;
               else
                  L_vir_weight_cuom    := NULL;
                  L_intran_weight_cuom := NULL;
               end if;


               if LP_bol_rec.distros(i).bol_items(item_cnt).pack_type != 'B' then

                  if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(
                                          O_error_message,
                                          L_pack_total_chrgs_prim,
                                          L_pack_profit_chrgs_to_loc,
                                          L_pack_exp_chrgs_to_loc,
                                          'T',                         --transfer
                                          LP_bol_rec.distros(i).tsf_no,
                                          LP_bol_rec.distros(i).bol_items(item_cnt).td_tsf_seq_no,
                                          L_ship_no,
                                          L_ss_seq_no,
                                          LP_bol_rec.distros(i).bol_items(item_cnt).item,  --item (send pack in item field)
                                          NULL,                        --pack_no
                                          LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                          LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                          LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                          LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                     return FALSE;
                  end if;

                  L_ss_prim_chrgs := L_ss_prim_chrgs + (L_pack_total_chrgs_prim *
                     LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);
               end if;

               L_pack_tsf_unit_cost := 0;
               FOR rec in C_ITEM_IN_PACK LOOP

                  if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                                L_from_wac,
                                                rec.item,
                                                rec.dept,
                                                rec.class,
                                                rec.subclass,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                                LP_bol_rec.tran_date,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                                LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                     return FALSE;
                  end if;
                  L_tsf_unit_cost:= L_from_wac;

                  if LP_bol_rec.distros(i).bol_items(item_cnt).pack_type != 'B' then
                     --******************************************************************************
                     -- Value returned in L_pack_profit_chrgs_to_loc, L_pack_exp_chrgs_to_loc, and
                     -- L_pack_total_chrgs_prim are unit values for the entire pack.  Need to take
                     -- a proportionate piece of the value for each component item in the pack
                     -- The formula for this is:
                     --       [Pack Value * (Comp Item Avg Cost * Comp Qty in the Pack) /
                     --                     (Total Pack Avg Cost)] /
                     --       Comp Qty in the Pack
                     -- You must divide the value by the Component Item Qty in the pack because the
                     -- value will be for one pack.  In order to get a true unit value you need to
                     -- do the last division.  Since we multiple by Comp Qty and then divide by it,
                     -- it can be removed from the calculation completely.
                     --******************************************************************************
                     if L_pack_loc_av_cost = 0 then
                        L_profit_chrgs_to_loc := 0;
                        L_exp_chrgs_to_loc    := 0;
                        L_total_chrgs_prim    := 0;
                     else
                        L_profit_chrgs_to_loc := L_pack_profit_chrgs_to_loc * L_from_av_cost / L_pack_loc_av_cost;
                        L_exp_chrgs_to_loc    := L_pack_exp_chrgs_to_loc    * L_from_av_cost / L_pack_loc_av_cost;
                        L_total_chrgs_prim    := L_pack_total_chrgs_prim    * L_from_av_cost / L_pack_loc_av_cost;
                     end if;

                  else
                     if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(
                                           O_error_message,
                                           L_total_chrgs_prim,
                                           L_profit_chrgs_to_loc,
                                           L_exp_chrgs_to_loc,
                                           'T',                         --transfer
                                           LP_bol_rec.distros(i).tsf_no,
                                           LP_bol_rec.distros(i).bol_items(item_cnt).td_tsf_seq_no,
                                           L_ship_no,
                                           L_ss_seq_no,
                                           rec.item,                    --item
                                           LP_bol_rec.distros(i).bol_items(item_cnt).item,  --pack_no
                                           LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc,
                                           LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_from_loc_type,
                                           LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc,
                                           LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type) = FALSE then
                        return FALSE;
                     end if;

                     L_ss_prim_chrgs := L_ss_prim_chrgs + (L_total_chrgs_prim * rec.qty *
                        LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);

                  end if;

                  -- sum up component item's transfer unit cost
                  L_pack_tsf_unit_cost := L_pack_tsf_unit_cost + L_tsf_unit_cost * rec.qty;
               END LOOP; --pack comp

               -- transfer is at the virtual locations
               -- shipment is at the physical locations
               -- sum up transfer cost across all virtual locations

               L_ss_cost := L_ss_cost + L_pack_tsf_unit_cost *
                  LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;

               L_ss_retail := L_ss_retail + (NVL(L_pack_loc_retail, 0) *
                  LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty);
            end if;

            L_ss_qty := L_ss_qty + LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_qty;

         end if; --item type

      END LOOP; --virtual loc

      /* since all virtuals in a phy must have same currency code, just grab the 1st one */
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(1).vir_from_loc,
                                          LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(1).vir_from_loc_type,
                                          NULL,
                                          L_ss_prim_chrgs,
                                          L_ss_from_chrgs,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      if(L_ins_cost IS NULL) then
         L_ins_cost := (L_ss_cost / L_ss_qty) + (L_ss_from_chrgs / L_ss_qty);
      end if;

      if(L_ins_retail IS NULL) then
          L_ins_retail := L_ss_retail / L_ss_qty;
      end if;
      -- LP_receipt_ind is set for when the BOL process is being called from the Receiving process.
      --
      if LP_receipt_ind = 'Y' then
         L_ss_rcv_qty := L_ss_qty;
         L_ss_qty := 0;
      else
         L_ss_rcv_qty := 0;
      end if;

      if STOCK_ORDER_RCV_SQL.GET_INVENTORY_TREATMENT(O_error_message,
                                                     L_inventory_treatment_ind,
                                                     LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(1).vir_from_loc_type,
                                                     LP_bol_rec.distros(i).bol_items(item_cnt).virtual_locs(loc_cnt).vir_to_loc_type,
                                                     LP_bol_rec.distros(i).tsf_no,
                                                     'T',
                                                     LP_bol_rec.ship_no,
                                                     LP_bol_rec.distros(i).bol_items(item_cnt).ss_seq_no,
                                                     FALSE,
                                                     TRUE) = FALSE then
         return FALSE;
      end if;

      if BOL_SQL.INS_SHIPSKU(O_error_message,
                             LP_bol_rec.ship_no,
                             LP_bol_rec.distros(i).bol_items(item_cnt).ss_seq_no,
                             LP_bol_rec.distros(i).bol_items(item_cnt).item,
                             LP_bol_rec.distros(i).bol_items(item_cnt).ref_item,
                             LP_bol_rec.distros(i).tsf_no,
                             'T',
                             LP_bol_rec.distros(i).bol_items(item_cnt).carton,
                             LP_bol_rec.distros(i).bol_items(item_cnt).inv_status,
                             L_ss_rcv_qty,
                             L_ins_cost,
                             L_ins_retail,
                             L_ss_qty,
                             L_ss_exp_weight,
                             L_ss_exp_weight_uom,
                             L_inventory_treatment_ind) = FALSE then
         return FALSE;
      end if;

      L_ss_prim_chrgs := 0;
      L_ss_cost := 0;
      L_ss_retail := 0;
      L_ss_qty := 0;
      L_ss_rcv_qty := 0;
      L_ss_exp_weight := NULL;
      L_ss_exp_weight_uom := NULL;
      L_ins_cost := 0;
      L_ins_retail := 0;

      if FLUSH_SHIPSKU_INSERT(O_error_message) = FALSE then
         return FALSE;
      end if;

   END LOOP; --items

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_SHIPSKU_COST_RETAIL%isopen then
         close C_SHIPSKU_COST_RETAIL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STOCK_ORDER_RCV_SQL.POPULATE_SHIPSKU',
                                            to_char(SQLCODE));
        return FALSE;
END POPULATE_SHIPSKU;
----------------------------------------------------------------------------------------------
END BOL_SQL;
/