CREATE OR REPLACE PACKAGE RMSSUB_XTSF_VALIDATE AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tsf_rec         OUT    NOCOPY   RMSSUB_XTSF.TSF_REC,
                       I_message         IN              "RIB_XTsfDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tsf_rec         OUT    NOCOPY   RMSSUB_XTSF.TSF_REC,
                       I_message         IN              "RIB_XTsfRef_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XTSF_VALIDATE;
/
