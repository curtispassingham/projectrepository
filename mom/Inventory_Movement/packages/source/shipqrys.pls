
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SHIP_QUERY_SQL AUTHID CURRENT_USER AS

------------------------------------------------------------------------------------------------
FUNCTION SET_PARAMETER_VALUE(O_error_message   IN OUT   VARCHAR2,
                             I_parm_name       IN   VARCHAR2,
                             I_parm_type       IN   VARCHAR2,
                             I_parm_value      IN   VARCHAR2)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION INSERT_SHIP_TO_TMP(O_error_message  IN OUT  VARCHAR2,
                            I_main_query     IN      VARCHAR2)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
END SHIP_QUERY_SQL;
/
