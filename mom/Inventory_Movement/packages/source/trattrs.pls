
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TSF_ATTRIB_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------
-- Function Name: TSF_ZONE_DESC
-- Purpose: take tsf zone number and return description
---------------------------------------------------------------
FUNCTION TSF_ZONE_DESC(O_error_message	IN OUT VARCHAR2,
                       I_zone           IN     TSFZONE.TRANSFER_ZONE%TYPE,
                       O_desc           IN OUT TSFZONE.DESCRIPTION%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------
-- Function Name: GET_TSFHEAD_INFO
-- Purpose	: to get back all the information on tsfhead for
--		  a given transfer number
---------------------------------------------------------------
FUNCTION GET_TSFHEAD_INFO(O_error_message IN OUT VARCHAR2,
                          I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE,
                          O_dept           IN OUT TSFHEAD.DEPT%TYPE,
                          O_create_date    IN OUT TSFHEAD.CREATE_DATE%TYPE,
                          O_status         IN OUT TSFHEAD.STATUS%TYPE,
                          O_from_loc_type  IN OUT TSFHEAD.FROM_LOC_TYPE%TYPE,
                          O_from_loc       IN OUT TSFHEAD.FROM_LOC%TYPE,
                          O_to_loc_type    IN OUT TSFHEAD.TO_LOC_TYPE%TYPE,
                          O_to_loc         IN OUT TSFHEAD.TO_LOC%TYPE,
                          O_tsf_type       IN OUT TSFHEAD.TSF_TYPE%TYPE,
                          O_freight_code   IN OUT TSFHEAD.FREIGHT_CODE%TYPE,
                          O_routing_code   IN OUT TSFHEAD.ROUTING_CODE%TYPE,
                          O_comment_desc   IN OUT TSFHEAD.COMMENT_DESC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------
-- FUNCTION: GET_MAX_SEQ_NO
-- PURPOSE:  This function will take a transfer number and find the
--           maximum tsf_seq_no for the transfer on tsfdetail.  If
--           no tsfdetail records exist, it will return a 0 for the
--           tsf_seq_no.
------------------------------------------------------------------
FUNCTION GET_MAX_SEQ_NO(O_error_message IN OUT VARCHAR2,
                        I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                        O_seq_no        IN OUT TSFDETAIL.TSF_SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- FUNCTION:  GET_QUANTITIES
-- PURPOSE:  This function will fetch the quantities from tsfdetail for the given
--           transfer/sku.
-------------------------------------------------------------------------------------------
FUNCTION GET_QUANTITIES(O_error_message  IN OUT VARCHAR2,
                        I_tsf_no         IN     TSFHEAD.TSF_NO%TYPE,
                        I_item           IN     TSFDETAIL.ITEM%TYPE,
                        O_seq_no         IN OUT TSFDETAIL.TSF_SEQ_NO%TYPE,
                        O_fill_qty       IN OUT TSFDETAIL.FILL_QTY%TYPE,
                        O_tsf_qty        IN OUT TSFDETAIL.TSF_QTY%TYPE,
                        O_ship_qty       IN OUT TSFDETAIL.SHIP_QTY%TYPE,
                        O_received_qty   IN OUT TSFDETAIL.RECEIVED_QTY%TYPE,
                        O_supp_pack_size IN OUT TSFDETAIL.SUPP_PACK_SIZE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- FUNCTION:  GET_STATUS
-- PURPOSE:   This function will return the status of a transfer from TSFHEAD.
-- CALLS:     None

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_TSFHEAD
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------------------
FUNCTION GET_STATUS(O_error_message       IN OUT VARCHAR2,
                    O_leg_1_status        IN OUT TSFHEAD.STATUS%TYPE,
                    O_leg_2_status        IN OUT TSFHEAD.STATUS%TYPE,
                    I_tsf_no              IN     TSFHEAD.TSF_NO%TYPE) RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- FUNCTION:  GET_SUPP_PACK_SIZE
-- PURPOSE:   This function will return the supplier pack size for an item on a transfer.
-- CALLS:     None
--------------------------------------------------------------------------------------------
FUNCTION GET_SUPP_PACK_SIZE(O_error_message       IN OUT VARCHAR2,
                            O_supp_pack_size      IN OUT TSFDETAIL.SUPP_PACK_SIZE%TYPE,
                            I_tsf_no              IN     TSFDETAIL.TSF_NO%TYPE,
                            I_item                IN     TSFDETAIL.ITEM%TYPE) RETURN BOOLEAN;

------------------------------------------------------------------------------------------------
-- Function Name: TSF_CHILD_ITEMS_EXIST
-- Purpose	: checks to see if skus or a pack that contains fashion skus  exist on a transfer
-- Calls	: <none>
-- Input Value/s: I_tsf_no -- transfer number
-- Return Values: O_exist  -- TRUE if exists, FALSE otherwise
-- Created on	: 20-July-99 by Randip Medhi
------------------------------------------------------------------------------------------------
   FUNCTION TSF_CHILD_ITEMS_EXIST(O_error_message IN OUT VARCHAR2,
                                  O_exist         IN OUT BOOLEAN,
                                  I_tsf_no        IN TSFHEAD.TSF_NO%TYPE)
            RETURN BOOLEAN;

------------------------------------------------------------------------------------------------
-- Function Name: TSF_FILTER_LIST
-- Purpose	: checks to see if the # of records in V_TSFDETAIL is different then what's in TSFDETAIL
--            for a specific transfer.  If it is, it'll return Y, else it'll return N.
-- Calls	: <none>
-- Input Value/s: I_tsf_no -- transfer number
-- Return Values: O_diff  -- Y if different, N if same
-- Created on	: 20-May-2003 by Joe Strano

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_TSFDETAIL
-- which only returns data that the user has permission to access.  
------------------------------------------------------------------------------------------------
   FUNCTION TSF_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_diff          IN OUT VARCHAR2,
                            I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE)
            RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END TSF_ATTRIB_SQL;
/
