CREATE OR REPLACE PACKAGE BODY CORESVC_FULFILORD_TSF AS

   LP_user                   SVCPROV_CONTEXT.USER_NAME%TYPE   := get_user;
   LP_ordcust_tab            ORDCUST_TBL := ORDCUST_TBL();
   LP_ordcust_detail_tab     ORDCUST_DETAIL_TBL := ORDCUST_DETAIL_TBL();
   LP_ordcust_l10n_ext_tab   L10N_ORDCUST_EXT_TBL := L10N_ORDCUST_EXT_TBL();
   LP_transfer_tab           RMSSUB_XTSF.TSF_TBL := RMSSUB_XTSF.TSF_TBL();
---------------------------------------------------------------------------------------------------
-- Name   : VALIDATE_CREATE_TSF
-- Purpose: This is a private function that performs transfer specific validations on customer order
--          fulfillment requests that will result in creating 'CO' type of transfers. It returns
--          an error if any validation error is found and logs the error on ERROR_MSG column of the
--          corresponding staging table records. It also queries the available inventory of an item
--          at the sourcing location and populates the field on the staging table.
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CREATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : POPULATE_CREATE_REC
-- Purpose: This is a private function that processes validated data in the staging tables and
--          populates pl/sql global collections to facilitate bulk persistence.
---------------------------------------------------------------------------------------------------
FUNCTION POPULATE_CREATE_REC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_ordcust_ids     IN OUT   ID_TBL,
                             I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : PERSIST_CREATE_TSF
-- Purpose: This is a private function that inserts data from the pl/sql global collections to
--          database tables.
---------------------------------------------------------------------------------------------------
FUNCTION PERSIST_CREATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : VALIDATE_CANCEL_TSF
-- Purpose: This is a private function that performs transfer specific validation on customer
--          order fulfillment cancellation requests that will result in cancelling quantity on 'CO'
--          type of transfers and related customer orders (ORDCUST_DETAIL). It returns an error if
--          any validation error is found and logs the error on ERROR_MSG column of the corresponding
--          staging table records.
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CANCEL_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : POPULATE_CANCEL_REC
-- Purpose: This is a private function that processes validated data in the staging tables and
--          populates pl/sql global collections to facilitate bulk persistence.
---------------------------------------------------------------------------------------------------
FUNCTION POPULATE_CANCEL_REC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : PERSIST_CANCEL_TSF
-- Purpose: This is a private function that inserts data from the pl/sql global collections
--          to database tables.
---------------------------------------------------------------------------------------------------
FUNCTION PERSIST_CANCEL_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_ordcust_ids     IN OUT   ID_TBL,
                    I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                    I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'CORESVC_FULFILORD_TSF.CREATE_TSF';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOC_SVC_FULFILORD is
      select 'x'
        from svc_fulfilord
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and process_status in (CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED, CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE)
         for update nowait;

BEGIN

   -- initialize package global collections to prevent data from being persistent between calls
   LP_ordcust_tab := ORDCUST_TBL();
   LP_ordcust_detail_tab := ORDCUST_DETAIL_TBL();
   LP_ordcust_l10n_ext_tab := L10N_ORDCUST_EXT_TBL();
   LP_transfer_tab := RMSSUB_XTSF.TSF_TBL();

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_chunk_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if VALIDATE_CREATE_TSF(O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if POPULATE_CREATE_REC(O_error_message,
                          O_ordcust_ids,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CREATE_TSF(O_error_message) = FALSE then
      return FALSE;
   end if;

   -- delete package global collections to prevent data from being persistent between calls
   LP_ordcust_tab.DELETE;
   LP_ordcust_detail_tab.DELETE;
   LP_ordcust_l10n_ext_tab.DELETE;
   LP_transfer_tab.DELETE;

   L_table := 'SVC_FULFILORD';
   open C_LOC_SVC_FULFILORD;
   close C_LOC_SVC_FULFILORD;

   update svc_fulfilord
      set process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED,
          last_update_id = LP_user,
          last_update_datetime = sysdate
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
      and process_status in (CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED, CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_TSF;
-------------------------------------------------------------------------------------------------------
FUNCTION CANCEL_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                    I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'CORESVC_FULFILORD_TSF.CANCEL_TSF';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_FULFILORDREF is
      select 'x'
        from svc_fulfilordref
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         for update nowait;

BEGIN
   -- initialize package global collections to prevent data from being persistent between calls
   LP_ordcust_tab := ORDCUST_TBL();
   LP_ordcust_detail_tab := ORDCUST_DETAIL_TBL();
   LP_ordcust_l10n_ext_tab := L10N_ORDCUST_EXT_TBL();
   LP_transfer_tab := RMSSUB_XTSF.TSF_TBL();

   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_chunk_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if VALIDATE_CANCEL_TSF(O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if POPULATE_CANCEL_REC(O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CANCEL_TSF(O_error_message) = FALSE then
      return FALSE;
   end if;

   -- delete package global collections to prevent data from being persistent between calls
   LP_ordcust_tab.DELETE;
   LP_ordcust_detail_tab.DELETE;
   LP_ordcust_l10n_ext_tab.DELETE;
   LP_transfer_tab.DELETE;

   L_table := 'SVC_FULFILORDREF';
   open C_LOCK_SVC_FULFILORDREF;
   close C_LOCK_SVC_FULFILORDREF;

   update svc_fulfilordref
      set process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED,
          last_update_id = LP_user,
          last_update_datetime = sysdate
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
      and process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CANCEL_TSF;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CREATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'CORESVC_FULFILORD_TSF.VALIDATE_CREATE_TSF';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_STORE_AVAIL_INV is
      select svc.item,
             svc.loc,
             NVL(greatest(ils.stock_on_hand, 0) -
                         (ils.tsf_reserved_qty + ils.rtv_qty + greatest(ils.non_sellable_qty, 0) + ils.customer_resv), 0) available_inventory
        from item_loc_soh ils,
             (select sfd.item,
                     sfo.source_loc_id loc
                from svc_fulfilord sfo,
                     svc_fulfilorddtl sfd
               where sfo.process_id = I_process_id
                 and sfo.chunk_id = I_chunk_id
                 and sfo.process_id = sfd.process_id
                 and sfo.chunk_id = sfd.chunk_id
                 and sfo.fulfilord_id = sfd.fulfilord_id
                 and sfo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                 and sfo.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                 and sfo.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE) svc
       where svc.item = ils.item(+)
         and svc.loc = ils.loc(+)
         and ils.loc_type(+) = 'S';

   cursor C_PACK_STORE_AVAIL_INV is
      select svc.pack_no,
             svc.loc,
             svc.component_item,
             NVL(greatest(ils.stock_on_hand, 0) -
                         (ils.tsf_reserved_qty + ils.rtv_qty + greatest(ils.non_sellable_qty, 0) + ils.customer_resv), 0) available_inventory
        from item_loc_soh ils,
             (select sfc.item pack_no,
                     sfo.source_loc_id loc,
                     sfc.component_item
                from svc_fulfilord sfo,
                     svc_fulfilorddtl_comp_item sfc
               where sfo.process_id = I_process_id
                 and sfo.chunk_id = I_chunk_id
                 and sfo.process_id = sfc.process_id
                 and sfo.chunk_id = sfc.chunk_id
                 and sfo.fulfilord_id = sfc.fulfilord_id
                 and sfo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                 and sfo.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                 and sfo.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE) svc
       where svc.component_item = ils.item(+)
         and svc.loc = ils.loc(+)
         and ils.loc_type(+) = 'S';

   cursor C_WH_AVAIL_INV is
      with svc_result as (select sfd.item,
                                 sfo.source_loc_id loc,
                                 st.channel_id
                            from svc_fulfilord sfo,
                                 svc_fulfilorddtl sfd,
                                 store st
                           where sfo.process_id = I_process_id
                             and sfo.chunk_id = I_chunk_id
                             and sfo.process_id = sfd.process_id
                             and sfo.chunk_id = sfd.chunk_id
                             and sfo.fulfilord_id = sfd.fulfilord_id
                             and sfo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                             and sfo.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                             and sfo.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH
                             and sfo.fulfill_loc_id = st.store)
      select wh_inv.item,
             wh_inv.loc,
             sum(wh_inv.available_inventory) from
       (select svc.item,
             svc.loc,
             NVL(ilsw.stock_on_hand - (ilsw.tsf_reserved_qty + ilsw.rtv_qty + ilsw.non_sellable_qty + ilsw.customer_resv), 0) available_inventory
        from (select ils.item,
                     w.physical_wh final_wh,
                     w.channel_id,
                     sum(greatest(ils.stock_on_hand, 0)) stock_on_hand,
                     sum(ils.tsf_reserved_qty) tsf_reserved_qty,
                     sum(ils.rtv_qty) rtv_qty,
                     sum(greatest(ils.non_sellable_qty,0)) non_sellable_qty,
                     sum(ils.customer_resv) customer_resv
                from item_loc_soh ils,
                     wh w
               where w.wh = ils.loc
                 and ils.loc_type = 'W'
                 and w.customer_order_loc_ind = 'Y'
                 and w.wh <> w.physical_wh
               group by ils.item,
                        w.physical_wh,
                        w.channel_id) ilsw,
             svc_result svc
       where svc.item = ilsw.item(+)
             and svc.loc = ilsw.final_wh(+)
             and svc.channel_id = ilsw.channel_id(+)
       UNION
      select svc.item,
             svc.loc,
             NVL(ilsw.stock_on_hand - (ilsw.tsf_reserved_qty + ilsw.rtv_qty + ilsw.non_sellable_qty + ilsw.customer_resv), 0) available_inventory
        from (select ils.item,
                     w.wh final_wh,
                     NULL as channel_id,
                     greatest(ils.stock_on_hand, 0) stock_on_hand,
                     ils.tsf_reserved_qty tsf_reserved_qty,
                     ils.rtv_qty rtv_qty,
                     greatest(ils.non_sellable_qty,0) non_sellable_qty,
                     ils.customer_resv customer_resv
                from item_loc_soh ils,
                     wh w
               where w.wh = ils.loc
                 and ils.loc_type = 'W'
                 and w.customer_order_loc_ind = 'Y'
                 and w.wh <> w.physical_wh) ilsw,
             svc_result svc
       where svc.item = ilsw.item(+)
         and svc.loc = ilsw.final_wh(+)) wh_inv
       group by item,
                loc;

   cursor C_LOCK_SVC_FULFILORD is
      select 'x'
        from svc_fulfilord sf
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtl sd
                      where sd.error_msg is NOT NULL
                        and sd.fulfilord_id = sf.fulfilord_id
                        and sd.process_id = sf.process_id
                        and sd.chunk_id = sf.chunk_id
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_SVC_FULFILORDDTL(I_item       SVC_FULFILORDDTL.ITEM%TYPE,
                                  I_loc        SVC_FULFILORD.SOURCE_LOC_ID%TYPE,
                                  I_loc_type   SVC_FULFILORD.SOURCE_LOC_TYPE%TYPE) is
      select 'x'
        from svc_fulfilorddtl sd
       where sd.process_id = I_process_id
         and sd.chunk_id = I_chunk_id
         and sd.item = I_item
         and exists (select 'x'
                       from svc_fulfilord sf
                      where sd.fulfilord_id = sf.fulfilord_id
                        and sd.process_id = sf.process_id
                        and sd.chunk_id = sf.chunk_id
                        and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                        and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                        and sf.source_loc_id = I_loc
                        and sf.source_loc_type = I_loc_type
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_SVC_FULFILORDDTL_COMP(I_item       SVC_FULFILORDDTL.ITEM%TYPE,
                                       I_loc        SVC_FULFILORD.SOURCE_LOC_ID%TYPE,
                                       I_loc_type   SVC_FULFILORD.SOURCE_LOC_TYPE%TYPE) is
      select 'x'
        from svc_fulfilorddtl_comp_item sfc
       where sfc.process_id = I_process_id
         and sfc.chunk_id = I_chunk_id
         and sfc.item = I_item
         and exists (select 'x'
                       from svc_fulfilord sf
                      where sfc.fulfilord_id = sf.fulfilord_id
                        and sfc.process_id = sf.process_id
                        and sfc.chunk_id = sf.chunk_id
                        and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                        and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                        and sf.source_loc_id = I_loc
                        and sf.source_loc_type = I_loc_type
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_ITEM_LOC_SOH_ST is
      select 'x'
        from item_loc_soh ils,
             svc_fulfilord sfo,
             svc_fulfilorddtl sfd
       where sfo.process_id = I_process_id
         and sfo.chunk_id = I_chunk_id
         and sfo.process_id = sfd.process_id
         and sfo.chunk_id = sfd.chunk_id
         and sfo.fulfilord_id = sfd.fulfilord_id
         and sfo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sfo.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and sfo.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE
         and ils.loc_type = 'S'
         and sfo.source_loc_id = ils.loc
         and sfd.item = ils.item
         for update of ils.item nowait;

   cursor C_LOCK_PACK_ILS_ST is
      select 'x'
        from item_loc_soh ils,
             svc_fulfilord sfo,
             svc_fulfilorddtl_comp_item sfc
       where sfo.process_id = I_process_id
         and sfo.chunk_id = I_chunk_id
         and sfo.process_id = sfc.process_id
         and sfo.chunk_id = sfc.chunk_id
         and sfo.fulfilord_id = sfc.fulfilord_id
         and sfo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sfo.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and sfo.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE
         and ils.loc_type = 'S'
         and sfo.source_loc_id = ils.loc
         and sfc.component_item = ils.item
         for update of ils.item nowait;

   cursor C_LOCK_ITEM_LOC_SOH_WH is
      select 'x'
        from item_loc_soh ils,
             svc_fulfilord sfo,
             svc_fulfilorddtl sfd,
             wh w
       where sfo.process_id = I_process_id
         and sfo.chunk_id = I_chunk_id
         and sfo.process_id = sfd.process_id
         and sfo.chunk_id = sfd.chunk_id
         and sfo.fulfilord_id = sfd.fulfilord_id
         and sfo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sfo.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and sfo.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH
         and sfo.source_loc_id = w.physical_wh
         and sfd.item = ils.item
         and ils.loc_type = 'W'
         and ils.loc = w.wh -- virtual warehouse
         and w.customer_order_loc_ind = 'Y'
         and w.wh <> w.physical_wh
         for update of ils.item nowait;

   TYPE ILS_INFO_TAB is TABLE OF C_STORE_AVAIL_INV%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE ILS_PACK_INFO_TAB is TABLE OF C_PACK_STORE_AVAIL_INV%ROWTYPE INDEX BY BINARY_INTEGER;
   L_store_tab        ILS_INFO_TAB;
   L_wh_tab           ILS_INFO_TAB;
   L_pack_store_tab   ILS_PACK_INFO_TAB;

BEGIN
   merge into svc_fulfilorddtl sfd
   using (select *
            from (select svfd.process_id,
                         svfd.chunk_id,
                         svfd.fulfilord_id,
                         svfd.item,
                         -- validate that the item is sellable
                         case
                            when itm.sellable_ind = 'N' then
                               SQL_LIB.GET_MESSAGE_TEXT('FULFILL_ITEM_NOT_SELLABLE', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the item is an inventory item
                         case
                            when itm.inventory_ind = 'N' then
                               SQL_LIB.GET_MESSAGE_TEXT('ITEM_NOT_INVENTORY', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that receive_as_type is 'P'ack if the item is a pack and source_loc is a warehouse
                         case
                            when itm.pack_ind = 'Y' and
                                 svo.source_loc_type is NOT NULL and svo.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH and
                                 exists (select 'x'
                                           from item_loc il,
                                                wh w
                                          where NVL(il.receive_as_type, 'P') <> 'P'
                                            and il.item = itm.item
                                            and il.loc_type = 'W'
                                            and il.loc = w.wh
                                            and svo.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH
                                            and w.wh = svo.source_loc_id
                                            and rownum = 1) then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_RCV_AS_TYPE', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end error_msg
                    from svc_fulfilorddtl svfd,
                         svc_fulfilord svo,
                         inv_move_unit_options i,
                         item_master itm
                   where svfd.process_id = I_process_id
                     and svfd.chunk_id = I_chunk_id
                     and svfd.process_id = svo.process_id
                     and svfd.chunk_id = svo.chunk_id
                     and svfd.fulfilord_id = svo.fulfilord_id
                     and svfd.item = itm.item
                     and svo.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                     and svo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED)
           where error_msg is NOT NULL) inner
   on (sfd.process_id = I_process_id
       and sfd.chunk_id = I_chunk_id
       and sfd.fulfilord_id = inner.fulfilord_id
       and sfd.item = inner.item)
   when matched then
      update set sfd.error_msg = substr(sfd.error_msg || inner.error_msg, 1, 2000),
                 sfd.last_update_id = LP_user,
                 sfd.last_update_datetime = sysdate;

   if SQL%ROWCOUNT > 0 then
      L_table := 'SVC_FULFILORD';
      open C_LOCK_SVC_FULFILORD;
      close C_LOCK_SVC_FULFILORD;

      update svc_fulfilord sf
         set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
             sf.error_msg = substr(sf.error_msg || SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_DETAIL', sf.customer_order_no, sf.fulfill_order_no, NULL)||';', 1, 2000),
             sf.last_update_id = LP_user,
             sf.last_update_datetime = sysdate
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtl sd
                      where sf.fulfilord_id = sd.fulfilord_id
                        and sf.process_id = sd.process_id
                        and sf.chunk_id = sd.chunk_id
                        and sd.error_msg is NOT NULL
                        and rownum = 1);

      select substr(sd.error_msg, 1, instr(sd.error_msg, ';')-1)
        into L_error_message
        from svc_fulfilorddtl sd
       where sd.process_id = I_process_id
         and sd.chunk_id = I_chunk_id
         and sd.error_msg is NOT NULL
         and exists (select 'x'
                       from svc_fulfilord sf
                      where sf.fulfilord_id = sd.fulfilord_id
                        and sf.process_id = sd.process_id
                        and sf.chunk_id = sd.chunk_id
                        and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                        and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR
                        and rownum = 1)
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   L_table := 'ITEM_LOC_SOH';
   open C_LOCK_ITEM_LOC_SOH_ST;
   close C_LOCK_ITEM_LOC_SOH_ST;

   open C_STORE_AVAIL_INV;
   fetch C_STORE_AVAIL_INV bulk collect into L_store_tab;
   close C_STORE_AVAIL_INV;

   -- Update the available_inventory for store sourced transfers.
   -- Since inventory is not tracked for pack items at stores, it will be updated to 0.
   -- Availability check will be done at the component level via the svc_fulfilorddtl_comp_item table.
   if L_store_tab.COUNT > 0 then
      L_table := 'SVC_FULFILORDDTL';
      FOR i in L_store_tab.FIRST..L_store_tab.LAST LOOP
         open C_LOCK_SVC_FULFILORDDTL(L_store_tab(i).item,
                                      L_store_tab(i).loc,
                                      RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE);
         close C_LOCK_SVC_FULFILORDDTL;
      END LOOP;

      FORALL i in L_store_tab.FIRST..L_store_tab.LAST
         update svc_fulfilorddtl sd
            set sd.available_qty = L_store_tab(i).available_inventory,
                sd.last_update_id = LP_user,
                sd.last_update_datetime = sysdate
          where sd.process_id = I_process_id
            and sd.chunk_id = I_chunk_id
            and sd.item = L_store_tab(i).item
            and exists (select 'x'
                          from svc_fulfilord sf
                         where sd.fulfilord_id = sf.fulfilord_id
                           and sd.process_id = sf.process_id
                           and sd.chunk_id = sf.chunk_id
                           and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                           and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                           and sf.source_loc_id = L_store_tab(i).loc
                           and sf.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE
                           and rownum = 1);
   end if;

   L_table := 'ITEM_LOC_SOH';
   open C_LOCK_PACK_ILS_ST;
   close C_LOCK_PACK_ILS_ST;

   open C_PACK_STORE_AVAIL_INV;
   fetch C_PACK_STORE_AVAIL_INV bulk collect into L_pack_store_tab;
   close C_PACK_STORE_AVAIL_INV;

   -- update the available_inventory of component items for store sourced transfers containing pack items
   if L_pack_store_tab.COUNT > 0 then
      L_table := 'SVC_FULFILORDDTL_COMP_ITEM';
      FOR i in L_pack_store_tab.FIRST..L_pack_store_tab.LAST LOOP
         open C_LOCK_SVC_FULFILORDDTL_COMP(L_pack_store_tab(i).pack_no,
                                           L_pack_store_tab(i).loc,
                                           RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE);
         close C_LOCK_SVC_FULFILORDDTL_COMP;
      END LOOP;

      FORALL i in L_pack_store_tab.FIRST..L_pack_store_tab.LAST
         update svc_fulfilorddtl_comp_item sfc
            set sfc.available_qty = L_pack_store_tab(i).available_inventory
          where sfc.process_id = I_process_id
            and sfc.chunk_id = I_chunk_id
            and sfc.item = L_pack_store_tab(i).pack_no
            and sfc.component_item = L_pack_store_tab(i).component_item
            and exists (select 'x'
                          from svc_fulfilord sf
                         where sfc.fulfilord_id = sf.fulfilord_id
                           and sfc.process_id = sf.process_id
                           and sfc.chunk_id = sf.chunk_id
                           and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                           and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                           and sf.source_loc_id = L_pack_store_tab(i).loc
                           and sf.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE
                           and rownum = 1);
   end if;

   L_table := 'ITEM_LOC_SOH';
   open C_LOCK_ITEM_LOC_SOH_WH;
   close C_LOCK_ITEM_LOC_SOH_WH;

   open C_WH_AVAIL_INV;
   fetch C_WH_AVAIL_INV bulk collect into L_wh_tab;
   close C_WH_AVAIL_INV;

   -- update the available_inventory for warehouse sourced transfers
   if L_wh_tab.COUNT > 0 then
      L_table := 'SVC_FULFILORDDTL';
      FOR i in L_wh_tab.FIRST..L_wh_tab.LAST LOOP
         open C_LOCK_SVC_FULFILORDDTL(L_wh_tab(i).item,
                                      L_wh_tab(i).loc,
                                      RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH);
         close C_LOCK_SVC_FULFILORDDTL;
      END LOOP;

      FORALL i in L_wh_tab.FIRST..L_wh_tab.LAST
         update svc_fulfilorddtl sd
            set sd.available_qty = L_wh_tab(i).available_inventory,
                sd.last_update_id = LP_user,
                sd.last_update_datetime = sysdate
          where sd.process_id = I_process_id
            and sd.chunk_id = I_chunk_id
            and sd.item = L_wh_tab(i).item
            and exists (select 'x'
                          from svc_fulfilord sf
                         where sd.fulfilord_id = sf.fulfilord_id
                           and sd.process_id = sf.process_id
                           and sd.chunk_id = sf.chunk_id
                           and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                           and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                           and sf.source_loc_id = L_wh_tab(i).loc
                           and sf.source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH
                           and rownum = 1);
   end if;

   -- update process_status to 'I'
   merge into svc_fulfilord sf
   using (-- partial_delivery_ind = 'N', available_qty < order_qty_suom on ANY of its corresponding item detail records
          -- exclude pack items sourced from stores as available_qty will always be 0.
          select distinct sfo.process_id,
                          sfo.chunk_id,
                          sfo.fulfilord_id
                     from svc_fulfilorddtl sfd,
                          svc_fulfilord sfo
                    where sfd.process_id = I_process_id
                      and sfd.chunk_id = I_chunk_id
                      and sfd.process_id = sfo.process_id
                      and sfd.chunk_id = sfo.chunk_id
                      and sfd.fulfilord_id = sfo.fulfilord_id
                      and sfo.partial_delivery_ind = 'N'
                      and sfo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                      and sfo.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                      and sfd.available_qty < sfd.order_qty_suom
                      and NOT exists (select 'x'
                                        from svc_fulfilorddtl_comp_item sci
                                       where sci.process_id = sfd.process_id
                                         and sci.chunk_id = sfd.chunk_id
                                         and sci.fulfilord_id = sfd.fulfilord_id
                                         and sci.item = sfd.item
                                         and rownum = 1)
       union all
          -- partial_delivery_ind = 'Y', available is <= 0 on ALL corresponding item detail records
          -- exclude orders containing pack items sourced from stores as available_qty will always be 0.
          select sfo.process_id,
                 sfo.chunk_id,
                 sfo.fulfilord_id
            from svc_fulfilord sfo
           where sfo.process_id = I_process_id
             and sfo.chunk_id = I_chunk_id
             and sfo.partial_delivery_ind = 'Y'
             and sfo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
             and sfo.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
             and NOT exists (select 'x'
                               from svc_fulfilorddtl_comp_item sci
                              where sci.process_id = sfo.process_id
                                and sci.chunk_id = sfo.chunk_id
                                and sci.fulfilord_id = sfo.fulfilord_id
                                and rownum = 1)
             and NOT exists (select 'x'
                               from svc_fulfilorddtl sd
                              where sd.fulfilord_id = sfo.fulfilord_id
                                and sd.process_id = sfo.process_id
                                and sd.chunk_id = sfo.chunk_id
                                and sd.available_qty > 0
                                and rownum = 1)
         ) inner
   on (sf.process_id = inner.process_id
       and sf.chunk_id = inner.chunk_id
       and sf.fulfilord_id = inner.fulfilord_id)
   when matched then
      update set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE,
                 sf.last_update_id = LP_user,
                 sf.last_update_datetime = sysdate;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_CREATE_TSF;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_CREATE_REC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_ordcust_ids     IN OUT   ID_TBL,
                             I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(50) := 'CORESVC_FULFILORD_TSF.POPULATE_CREATE_REC';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_return_code             VARCHAR2(10);
   L_table                   VARCHAR2(30);
   L_tsf_no                  TSFHEAD.TSF_NO%TYPE;
   L_order_qty_suom_total    SVC_FULFILORDDTL.ORDER_QTY_SUOM%TYPE := 0;
   L_actual_avail_qty        SVC_FULFILORDDTL.ORDER_QTY_SUOM%TYPE := 0;
   L_actual_avail_qty_init   SVC_FULFILORDDTL.ORDER_QTY_SUOM%TYPE := 0;
   L_actual_order_qty        SVC_FULFILORDDTL.ORDER_QTY_SUOM%TYPE := 0;
   L_detail_recs_created     BOOLEAN := FALSE;
   L_details_count           NUMBER := 0;
   L_tsfdetail_seq           BINARY_INTEGER := 0;
   L_idx                     BINARY_INTEGER := 0;
   L_tsfdetail_tab_idx       BINARY_INTEGER := 0;
   L_newitemloc_tab_idx      BINARY_INTEGER := 0;
   L_itemloc_idx             VARCHAR2(36);
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(Record_Locked, -54);

   -- this cursor will prioritize processing for orders containing non-pack items or pack items
   -- sourced from a warehouse before pack items sourced from a store.
   cursor C_GET_SVC_HEADER_RECS is
      select '1' order_helper,
             sf.fulfilord_id fulfilord_id,
             sf.process_status,
             sf.source_loc_type,
             sf.source_loc_id,
             sf.fulfill_loc_type,
             sf.fulfill_loc_id,
             sc.customer_no,
             sf.customer_order_no,
             sf.fulfill_order_no,
             sf.partial_delivery_ind,
             sf.delivery_type,
             sf.carrier_code,
             sf.carrier_service_code,
             sf.consumer_delivery_date,
             sf.consumer_delivery_time,
             sc.bill_first_name,
             sc.bill_phonetic_first,
             sc.bill_last_name,
             sc.bill_phonetic_last,
             sc.bill_preferred_name,
             sc.bill_company_name,
             sc.bill_add1,
             sc.bill_add2,
             sc.bill_add3,
             sc.bill_county,
             sc.bill_city,
             sc.bill_state,
             sc.bill_country_id,
             sc.bill_post,
             sc.bill_jurisdiction,
             sc.bill_phone,
             sc.deliver_first_name,
             sc.deliver_phonetic_first,
             sc.deliver_last_name,
             sc.deliver_phonetic_last,
             sc.deliver_preferred_name,
             sc.deliver_company_name,
             sc.deliver_add1,
             sc.deliver_add2,
             sc.deliver_add3,
             sc.deliver_city,
             sc.deliver_state,
             sc.deliver_country_id,
             sc.deliver_post,
             sc.deliver_county,
             sc.deliver_jurisdiction,
             sc.deliver_phone,
             sf.delivery_charges,
             sf.delivery_charges_curr,
             sf.comments
        from svc_fulfilord sf,
             svc_fulfilordcust sc
       where sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and sf.process_id = sc.process_id
         and sf.chunk_id = sc.chunk_id
         and sf.fulfilord_id = sc.fulfilord_id
         and sf.process_status in (CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED, CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE)
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and NOT exists (select 'x'
                           from svc_fulfilorddtl_comp_item si
                          where sf.process_id = si.process_id
                            and sf.chunk_id = si.chunk_id
                            and sf.fulfilord_id = si.fulfilord_id
                            and rownum = 1)
       union
      select '2' order_helper,
             sf.fulfilord_id fulfilord_id,
             sf.process_status,
             sf.source_loc_type,
             sf.source_loc_id,
             sf.fulfill_loc_type,
             sf.fulfill_loc_id,
             sc.customer_no,
             sf.customer_order_no,
             sf.fulfill_order_no,
             sf.partial_delivery_ind,
             sf.delivery_type,
             sf.carrier_code,
             sf.carrier_service_code,
             sf.consumer_delivery_date,
             sf.consumer_delivery_time,
             sc.bill_first_name,
             sc.bill_phonetic_first,
             sc.bill_last_name,
             sc.bill_phonetic_last,
             sc.bill_preferred_name,
             sc.bill_company_name,
             sc.bill_add1,
             sc.bill_add2,
             sc.bill_add3,
             sc.bill_county,
             sc.bill_city,
             sc.bill_state,
             sc.bill_country_id,
             sc.bill_post,
             sc.bill_jurisdiction,
             sc.bill_phone,
             sc.deliver_first_name,
             sc.deliver_phonetic_first,
             sc.deliver_last_name,
             sc.deliver_phonetic_last,
             sc.deliver_preferred_name,
             sc.deliver_company_name,
             sc.deliver_add1,
             sc.deliver_add2,
             sc.deliver_add3,
             sc.deliver_city,
             sc.deliver_state,
             sc.deliver_country_id,
             sc.deliver_post,
             sc.deliver_county,
             sc.deliver_jurisdiction,
             sc.deliver_phone,
             sf.delivery_charges,
             sf.delivery_charges_curr,
             sf.comments
        from svc_fulfilord sf,
             svc_fulfilordcust sc,
             svc_fulfilorddtl_comp_item si
       where sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and sf.process_id = sc.process_id
         and sf.chunk_id = sc.chunk_id
         and sf.fulfilord_id = sc.fulfilord_id
         and sf.process_id = si.process_id
         and sf.chunk_id = si.chunk_id
         and sf.fulfilord_id = si.fulfilord_id
         and sf.process_status in (CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED, CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE)
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
    order by order_helper,
             fulfilord_id;

   cursor C_GET_SVC_DETAIL_RECS(I_fulfilord_id   SVC_FULFILORD.FULFILORD_ID%TYPE) is
      select sd.item_status,
             sd.item,
             sd.ref_item,
             sd.order_qty_suom,
             sd.standard_uom,
             sd.transaction_uom,
             sd.substitute_ind,
             sd.unit_retail,
             sd.retail_curr,
             sd.comments,
             sd.available_qty,
             itm.pack_ind,
             itm.pack_type,
             itm.orderable_ind,
             decode(itm.orderable_ind, 'Y', isc.supp_pack_size, 1) supp_pack_size,
             decode(sil.item, NULL, 'N', 'Y') itemloc_exists
        from svc_fulfilorddtl sd,
             item_master itm,
             item_supp_country isc,
             (select il.item,
                     sf.process_id,
                     sf.chunk_id,
                     sf.fulfilord_id
                from svc_fulfilord sf,
                     item_loc il
               where sf.fulfill_loc_id = il.loc
                 and decode(sf.fulfill_loc_type, 'V', 'S', 'S') = il.loc_type
                 and sf.process_id = I_process_id
                 and sf.chunk_id = I_chunk_id
                 and sf.fulfilord_id = I_fulfilord_id) sil
       where sd.process_id = I_process_id
         and sd.chunk_id = I_chunk_id
         and sd.fulfilord_id = I_fulfilord_id
         and sd.process_id = sil.process_id(+)
         and sd.chunk_id = sil.chunk_id(+)
         and sd.fulfilord_id = sil.fulfilord_id(+)
         and sd.item = sil.item(+)
         and sd.item = itm.item
         and itm.item = isc.item(+)
         and isc.primary_supp_ind(+) = 'Y'
         and isc.primary_country_ind(+) = 'Y';

   cursor C_GET_SVC_DETAIL_COMP_RECS(I_fulfilord_id   SVC_FULFILORD.FULFILORD_ID%TYPE,
                                     I_item           SVC_FULFILORDDTL.ITEM%TYPE) is
      select item,
             component_item,
             component_qty,
             available_qty
        from svc_fulfilorddtl_comp_item
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and fulfilord_id = I_fulfilord_id
         and item = I_item;

   cursor C_LOCK_SVC_FULFILORD(I_fulfilord_id  SVC_FULFILORD.FULFILORD_ID%TYPE) is
      select 'x'
        from svc_fulfilord
       where fulfilord_id = I_fulfilord_id
         and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         for update nowait;

   TYPE HEADER_REC_TBL is TABLE OF C_GET_SVC_HEADER_RECS%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE DETAIL_REC_TBL is TABLE OF C_GET_SVC_DETAIL_RECS%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE COMP_ITEM_REC_TBL is TABLE OF C_GET_SVC_DETAIL_COMP_RECS%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE ORDER_QTY_TBL is TABLE OF SVC_FULFILORDDTL.ORDER_QTY_SUOM%TYPE INDEX BY VARCHAR2(36);
   TYPE FULFILORD_ID_TBL is TABLE OF SVC_FULFILORD.FULFILORD_ID%TYPE INDEX BY BINARY_INTEGER;
   L_header_rec_tab           HEADER_REC_TBL;
   L_detail_rec_tab           DETAIL_REC_TBL;
   L_order_qty_tab            ORDER_QTY_TBL;
   L_comp_item_tab            COMP_ITEM_REC_TBL;
   L_inactive_fulfilord_tab   FULFILORD_ID_TBL;

BEGIN

   open C_GET_SVC_HEADER_RECS;
   fetch C_GET_SVC_HEADER_RECS bulk collect into L_header_rec_tab;
   close C_GET_SVC_HEADER_RECS;

   if L_header_rec_tab is NOT NULL and L_header_rec_tab.COUNT > 0 then

      LP_ordcust_tab := ORDCUST_TBL();
      LP_transfer_tab := RMSSUB_XTSF.TSF_TBL();
      LP_ordcust_l10n_ext_tab := L10N_ORDCUST_EXT_TBL();
      LP_ordcust_detail_tab := ORDCUST_DETAIL_TBL();
      O_ordcust_ids := ID_TBL();

      FOR i in L_header_rec_tab.FIRST..L_header_rec_tab.LAST LOOP
         O_ordcust_ids.EXTEND();
         O_ordcust_ids(O_ordcust_ids.COUNT) := ordcust_seq.nextval;

         LP_ordcust_tab.EXTEND();
         LP_ordcust_tab(LP_ordcust_tab.COUNT).ordcust_no := O_ordcust_ids(O_ordcust_ids.COUNT);
         LP_ordcust_tab(LP_ordcust_tab.COUNT).source_loc_type := L_header_rec_tab(i).source_loc_type;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).source_loc_id := L_header_rec_tab(i).source_loc_id;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).fulfill_loc_type := L_header_rec_tab(i).fulfill_loc_type;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).fulfill_loc_id := L_header_rec_tab(i).fulfill_loc_id;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).customer_no := L_header_rec_tab(i).customer_no;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).customer_order_no := L_header_rec_tab(i).customer_order_no;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).fulfill_order_no := L_header_rec_tab(i).fulfill_order_no;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).partial_delivery_ind := L_header_rec_tab(i).partial_delivery_ind;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).delivery_type := L_header_rec_tab(i).delivery_type;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).carrier_code := L_header_rec_tab(i).carrier_code;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).carrier_service_code := L_header_rec_tab(i).carrier_service_code;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).consumer_delivery_date := L_header_rec_tab(i).consumer_delivery_date;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).consumer_delivery_time := L_header_rec_tab(i).consumer_delivery_time;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_first_name := L_header_rec_tab(i).bill_first_name;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_phonetic_first := L_header_rec_tab(i).bill_phonetic_first;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_last_name := L_header_rec_tab(i).bill_last_name;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_phonetic_last := L_header_rec_tab(i).bill_phonetic_last;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_preferred_name := L_header_rec_tab(i).bill_preferred_name;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_company_name := L_header_rec_tab(i).bill_company_name;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_add1 := L_header_rec_tab(i).bill_add1;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_add2 := L_header_rec_tab(i).bill_add2;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_add3 := L_header_rec_tab(i).bill_add3;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_county := L_header_rec_tab(i).bill_county;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_city := L_header_rec_tab(i).bill_city;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_state := L_header_rec_tab(i).bill_state;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_country_id := L_header_rec_tab(i).bill_country_id;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_post := L_header_rec_tab(i).bill_post;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_jurisdiction := L_header_rec_tab(i).bill_jurisdiction;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).bill_phone := L_header_rec_tab(i).bill_phone;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_first_name := L_header_rec_tab(i).deliver_first_name;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_phonetic_first := L_header_rec_tab(i).deliver_phonetic_first;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_last_name := L_header_rec_tab(i).deliver_last_name;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_phonetic_last := L_header_rec_tab(i).deliver_phonetic_last;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_preferred_name := L_header_rec_tab(i).deliver_preferred_name;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_company_name := L_header_rec_tab(i).deliver_company_name;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_add1 := L_header_rec_tab(i).deliver_add1;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_add2 := L_header_rec_tab(i).deliver_add2;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_add3 := L_header_rec_tab(i).deliver_add3;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_city := L_header_rec_tab(i).deliver_city;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_state := L_header_rec_tab(i).deliver_state;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_country_id := L_header_rec_tab(i).deliver_country_id;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_post := L_header_rec_tab(i).deliver_post;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_county := L_header_rec_tab(i).deliver_county;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_jurisdiction := L_header_rec_tab(i).deliver_jurisdiction;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_phone := L_header_rec_tab(i).deliver_phone;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_charge := L_header_rec_tab(i).delivery_charges;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).deliver_charge_curr := L_header_rec_tab(i).delivery_charges_curr;
         LP_ordcust_tab(LP_ordcust_tab.COUNT).comments := L_header_rec_tab(i).comments;

         if L_header_rec_tab(i).process_status = CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE then
            LP_ordcust_tab(LP_ordcust_tab.COUNT).status := CORESVC_FULFILORD.ORDCUST_STATUS_NOT_CRE;
            LP_ordcust_tab(LP_ordcust_tab.COUNT).tsf_no := NULL;
         else
            NEXT_TRANSFER_NUMBER(L_tsf_no,
                                 L_return_code,
                                 O_error_message);

            if L_return_code = 'FALSE' then
               return FALSE;
            end if;

            -- First set the order status to COMPLETED. If it is later determined that there is not enough
            -- available inventory to fulfill the order, order status will be switched to PARTIAL or INACTIVE.
            LP_ordcust_tab(LP_ordcust_tab.COUNT).status := CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED;
            LP_ordcust_tab(LP_ordcust_tab.COUNT).tsf_no := L_tsf_no;

            --build a transfer header record
            LP_transfer_tab.EXTEND();
            LP_transfer_tab(LP_transfer_tab.COUNT).header.tsf_no := L_tsf_no;

            if L_header_rec_tab(i).source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_WH then
               LP_transfer_tab(LP_transfer_tab.COUNT).header.from_loc_type := 'W';
            elsif L_header_rec_tab(i).source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE then
               LP_transfer_tab(LP_transfer_tab.COUNT).header.from_loc_type := 'S';
            end if;

            LP_transfer_tab(LP_transfer_tab.COUNT).header.from_loc := L_header_rec_tab(i).source_loc_id;
            -- only fulfill_loc_type of 'S'/'V' is currently supported, which are both stores
            LP_transfer_tab(LP_transfer_tab.COUNT).header.to_loc_type := 'S';
            LP_transfer_tab(LP_transfer_tab.COUNT).header.to_loc := L_header_rec_tab(i).fulfill_loc_id;
            LP_transfer_tab(LP_transfer_tab.COUNT).header.dept := NULL;
            LP_transfer_tab(LP_transfer_tab.COUNT).header.inventory_type := 'A';
            LP_transfer_tab(LP_transfer_tab.COUNT).header.tsf_type := 'CO';
            LP_transfer_tab(LP_transfer_tab.COUNT).header.status := 'A';
            LP_transfer_tab(LP_transfer_tab.COUNT).header.freight_code := 'N';
            LP_transfer_tab(LP_transfer_tab.COUNT).header.routing_code := NULL;
            LP_transfer_tab(LP_transfer_tab.COUNT).header.delivery_date := NVL(L_header_rec_tab(i).consumer_delivery_time, L_header_rec_tab(i).consumer_delivery_date);
            LP_transfer_tab(LP_transfer_tab.COUNT).header.ext_ref_no := substr(L_header_rec_tab(i).customer_order_no, 1, 14);
            LP_transfer_tab(LP_transfer_tab.COUNT).header.repl_tsf_approve_ind := 'N';
            LP_transfer_tab(LP_transfer_tab.COUNT).header.create_id := LP_user;

            LP_ordcust_l10n_ext_tab.EXTEND();
            LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT) := L10N_ORDCUST_EXT_REC();
            LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).procedure_key := 'CREATE_ORDCUST_L10N_EXT';
            LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).ordcust_no := LP_ordcust_tab(LP_ordcust_tab.COUNT).ordcust_no;
            LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).process_id := I_process_id;
            LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).chunk_id := I_chunk_id;
            LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).reference_id := L_header_rec_tab(i).fulfilord_id;
            LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).source_entity := 'LOC';
            LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).source_id := L_header_rec_tab(i).source_loc_id;
            LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).doc_type := 'TSF';
            LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).doc_id := L_tsf_no;
            open C_GET_SVC_DETAIL_RECS(L_header_rec_tab(i).fulfilord_id);
            fetch C_GET_SVC_DETAIL_RECS bulk collect into L_detail_rec_tab;
            close C_GET_SVC_DETAIL_RECS;

            L_detail_recs_created := FALSE;
            L_tsfdetail_seq := 0;
            L_details_count := 0;

            if L_detail_rec_tab is NOT NULL and L_detail_rec_tab.COUNT > 0 then
               -- initialize the transfer detail and new_itemloc collection
               LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.items := ITEM_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.pack_inds := INDICATOR_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.pack_types := INDICATOR_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.locs := LOC_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.loc_types := LOC_TYPE_TBL();

               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.items := ITEM_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.seq_nos := RMSSUB_XTSF.SEQ_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.qtys := QTY_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.existing_qtys := QTY_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.supp_pack_sizes := RMSSUB_XTSF.SUPP_PACK_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.inv_statuses := RMSSUB_XTSF.INV_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_inds := INDICATOR_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_types := INDICATOR_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.cancelled_qtys := QTY_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.selected_qtys := QTY_TBL();
               LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.tsf_price := RMSSUB_XTSF.PRICE_TBL();
               FOR j in L_detail_rec_tab.FIRST..L_detail_rec_tab.LAST LOOP
                  -- if pack item is sourced from store, loop through the detail records and determine the available quantity for each component item.
                  -- if there are multiple component items, the least available quantity will be used as the common available quantity among the component items
                  -- and will be checked against the order quantity for the pack item.
                  if L_detail_rec_tab(j).pack_ind = 'Y' and L_header_rec_tab(i).source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE then
                     open C_GET_SVC_DETAIL_COMP_RECS(L_header_rec_tab(i).fulfilord_id,
                                                     L_detail_rec_tab(j).item);
                     fetch C_GET_SVC_DETAIL_COMP_RECS bulk collect into L_comp_item_tab;
                     close C_GET_SVC_DETAIL_COMP_RECS;

                     if L_comp_item_tab is NOT NULL and L_comp_item_tab.COUNT > 0 then
                        FOR k in L_comp_item_tab.FIRST..L_comp_item_tab.LAST LOOP
                           -- find the running total for order_qty in the L_order_qty_tab collection for the component item/source loc combination
                           L_itemloc_idx := L_comp_item_tab(k).component_item||'/'||L_header_rec_tab(i).source_loc_id;
                           if L_order_qty_tab.EXISTS(L_itemloc_idx) then
                              L_order_qty_suom_total := L_order_qty_tab(L_itemloc_idx);
                              L_actual_avail_qty_init := FLOOR((L_comp_item_tab(k).available_qty - L_order_qty_suom_total)/L_comp_item_tab(k).component_qty);
                           else
                              L_actual_avail_qty_init := FLOOR(L_comp_item_tab(k).available_qty/L_comp_item_tab(k).component_qty);
                           end if;

                           -- set available qty to the value for the first component item
                           if k = 1 then
                              L_actual_avail_qty := L_actual_avail_qty_init;
                           else
                              -- determine the least available quantity between the component items
                              L_actual_avail_qty := LEAST(L_actual_avail_qty, L_actual_avail_qty_init);
                           end if;
                        END LOOP;
                     end if;
                  else
                     -- find the running total for order_qty in the L_order_qty_tab collection for the item/source loc combination
                     L_itemloc_idx := L_detail_rec_tab(j).item||'/'||L_header_rec_tab(i).source_loc_id;
                     if L_order_qty_tab.EXISTS(L_itemloc_idx) then
                        L_order_qty_suom_total := L_order_qty_tab(L_itemloc_idx);
                        L_actual_avail_qty := L_detail_rec_tab(j).available_qty - L_order_qty_suom_total;
                     else
                        L_actual_avail_qty := L_detail_rec_tab(j).available_qty;
                     end if;
                  end if;

                  -- if partial_delivery_ind is 'N' and at least one of the items cannot be completely fulfilled, exit the loop
                  -- and set the status of the order to 'X'(order not created).
                  if (L_header_rec_tab(i).partial_delivery_ind = 'N' and L_actual_avail_qty < L_detail_rec_tab(j).order_qty_suom) then
                     L_detail_recs_created := FALSE;
                     exit;
                  elsif L_actual_avail_qty > 0 then

                     L_actual_order_qty := LEAST(L_actual_avail_qty, L_detail_rec_tab(j).order_qty_suom);

                     -- build the ordcust_detail collection
                     LP_ordcust_detail_tab.EXTEND();
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).ordcust_no := LP_ordcust_tab(LP_ordcust_tab.COUNT).ordcust_no;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).item := L_detail_rec_tab(j).item;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).ref_item := L_detail_rec_tab(j).ref_item;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).original_item := NULL;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).qty_ordered_suom := L_detail_rec_tab(j).order_qty_suom;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).qty_cancelled_suom := 0;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).standard_uom := L_detail_rec_tab(j).standard_uom;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).transaction_uom := L_detail_rec_tab(j).transaction_uom;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).substitute_allowed_ind := L_detail_rec_tab(j).substitute_ind;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).unit_retail := L_detail_rec_tab(j).unit_retail;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).retail_currency_code := L_detail_rec_tab(j).retail_curr;
                     LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).comments := L_detail_rec_tab(j).comments;

                     -- build the transfer detail collection
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.items.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.seq_nos.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.qtys.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.existing_qtys.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.supp_pack_sizes.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.inv_statuses.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_inds.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_types.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.cancelled_qtys.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.selected_qtys.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.tsf_price.EXTEND();
                     L_tsfdetail_tab_idx := LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.items.COUNT;

                     L_tsfdetail_seq := L_tsfdetail_seq + 1;
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.items(L_tsfdetail_tab_idx) := L_detail_rec_tab(j).item;
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.seq_nos(L_tsfdetail_tab_idx) := L_tsfdetail_seq;
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.qtys(L_tsfdetail_tab_idx) := L_actual_order_qty;
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.existing_qtys(L_tsfdetail_tab_idx) := 0;
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.supp_pack_sizes(L_tsfdetail_tab_idx) := L_detail_rec_tab(j).supp_pack_size;
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.inv_statuses(L_tsfdetail_tab_idx) := NULL;
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_inds(L_tsfdetail_tab_idx) := L_detail_rec_tab(j).pack_ind;
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_types(L_tsfdetail_tab_idx) := L_detail_rec_tab(j).pack_type;
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.cancelled_qtys(L_tsfdetail_tab_idx) := 0;
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.tsf_price(L_tsfdetail_tab_idx) := NULL;
                     -- if item/fulfillment loc does not exist, populate the new_item_loc record within the transfer table
                     if L_detail_rec_tab(j).itemloc_exists = 'N' then -- item is not ranged to the fulfill location, create a new item/loc record
                        LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.items.EXTEND();
                        LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.pack_inds.EXTEND();
                        LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.pack_types.EXTEND();
                        LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.locs.EXTEND();
                        LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.loc_types.EXTEND();

                        L_newitemloc_tab_idx := LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.items.COUNT;

                        LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.items(L_newitemloc_tab_idx) := L_detail_rec_tab(j).item;
                        LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.pack_inds(L_newitemloc_tab_idx) := L_detail_rec_tab(j).pack_ind;
                        LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.pack_types(L_newitemloc_tab_idx) := L_detail_rec_tab(j).pack_type;
                        LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.locs(L_newitemloc_tab_idx) := L_header_rec_tab(i).fulfill_loc_id;
                        LP_transfer_tab(LP_transfer_tab.COUNT).new_itemloc.loc_types(L_newitemloc_tab_idx) := LP_transfer_tab(LP_transfer_tab.COUNT).header.to_loc_type;
                     end if;

                     -- update/add a new record to the L_order_qty_tab
                     if L_detail_rec_tab(j).pack_ind = 'Y' and L_header_rec_tab(i).source_loc_type = RMS_CONSTANTS.CUST_ORD_SRC_LOC_TYPE_STORE then
                        if L_comp_item_tab is NOT NULL and L_comp_item_tab.COUNT > 0 then
                           FOR k in L_comp_item_tab.FIRST..L_comp_item_tab.LAST LOOP
                              -- if a record already exists for the component item/source_loc in the collection, increment the total order with the actual_order_qty,
                              -- else add a new record to the L_order_qty_tab.
                              L_itemloc_idx := L_comp_item_tab(k).component_item||'/'||L_header_rec_tab(i).source_loc_id;
                              if L_order_qty_tab.EXISTS(L_itemloc_idx) then
                                 L_order_qty_tab(L_itemloc_idx) := NVL(L_order_qty_tab(L_itemloc_idx),0) + L_actual_order_qty * L_comp_item_tab(k).component_qty;
                              else
                                 -- no existing record for the component item/source_loc was found in the order qty table, create a new one
                                 L_order_qty_tab(L_itemloc_idx) := L_actual_order_qty * L_comp_item_tab(k).component_qty;
                              end if;
                           END LOOP;
                        end if;
                     else
                        -- if a record already exists for the item/source_loc in the collection, increment the total order with the actual_order_qty,
                        -- else add a new record to the L_order_qty_tab.
                        L_itemloc_idx := L_detail_rec_tab(j).item||'/'||L_header_rec_tab(i).source_loc_id;
                        if L_order_qty_tab.EXISTS(L_itemloc_idx) then
                           L_order_qty_tab(L_itemloc_idx) := NVL(L_order_qty_tab(L_itemloc_idx),0) + L_actual_order_qty;
                        else
                           -- no existing record for the item/source_loc was found in the order qty table, create a new one
                           L_order_qty_tab(L_itemloc_idx) := L_actual_order_qty;
                        end if;
                     end if;

                     L_details_count := L_details_count + 1;
                     L_detail_recs_created := TRUE;
                  end if; -- actual_avail_qty > 0

                  -- set the status of the ordcust record to 'P' if not completely fulfilled
                  if LP_ordcust_tab(LP_ordcust_tab.COUNT).status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED then
                     if L_actual_avail_qty <= 0  then
                        LP_ordcust_tab(LP_ordcust_tab.COUNT).status := CORESVC_FULFILORD.ORDCUST_STATUS_PARTIAL;
                     elsif L_actual_order_qty < LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).qty_ordered_suom then
                        LP_ordcust_tab(LP_ordcust_tab.COUNT).status := CORESVC_FULFILORD.ORDCUST_STATUS_PARTIAL;
                     end if;
                  end if;
               END LOOP; -- detail records
            end if;

            -- for orders that cannot be created at all due to partial_delivery_ind of 'N' or 0 availability for all order lines
            -- 1) backout the transfer record for this order
            -- 2) backout the ordcust_l10n_ext record for this order
            -- 3) backout the ordcust_detail records created so far for this order
            -- 4) reset status and null out tsf_no on ordcust record for this order
            -- 5) build a collection of inactive orders for returning 'X' ordcust status
            if L_detail_recs_created = FALSE then
                L_idx := L_idx + 1;
                L_inactive_fulfilord_tab(L_idx) := L_header_rec_tab(i).fulfilord_id;
                LP_ordcust_tab(LP_ordcust_tab.COUNT).status := CORESVC_FULFILORD.ORDCUST_STATUS_NOT_CRE;
                LP_ordcust_tab(LP_ordcust_tab.COUNT).tsf_no := NULL;
                LP_transfer_tab.TRIM;
                if L_details_count > 0 then
                   LP_ordcust_detail_tab.TRIM(L_details_count);
                end if;
                LP_ordcust_l10n_ext_tab.TRIM;
            end if;
         end if; -- if process_status <> 'I'
      END LOOP; -- header records
   end if;

   if L_inactive_fulfilord_tab.COUNT > 0 then
      L_table := 'SVC_FULFILORD';
      FOR i in L_inactive_fulfilord_tab.FIRST..L_inactive_fulfilord_tab.LAST LOOP
         open C_LOCK_SVC_FULFILORD(L_inactive_fulfilord_tab(i));
         close C_LOCK_SVC_FULFILORD;
      END LOOP;

      FORALL i in L_inactive_fulfilord_tab.FIRST..L_inactive_fulfilord_tab.LAST
         update svc_fulfilord
            set process_status = CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE,
                last_update_id = LP_user,
                last_update_datetime = sysdate
          where fulfilord_id = L_inactive_fulfilord_tab(i)
            and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_CREATE_REC;
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST_CREATE_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'CORESVC_FULFILORD_TSF.PERSIST_CREATE_TSF';
   L_message_type   VARCHAR2(15) := RMSSUB_XTSF.LP_cre_type;

BEGIN
   if LP_transfer_tab is NOT NULL and LP_transfer_tab.COUNT > 0 then
      if RMSSUB_XTSF_SQL.PERSIST(O_error_message,
                                 LP_transfer_tab,
                                 L_message_type) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_ordcust_tab is NOT NULL and LP_ordcust_tab.COUNT > 0 then
      FORALL i in LP_ordcust_tab.FIRST..LP_ordcust_tab.LAST
         insert into ordcust(ordcust_no,
                             status,
                             order_no,
                             tsf_no,
                             source_loc_type,
                             source_loc_id,
                             fulfill_loc_type,
                             fulfill_loc_id,
                             customer_no,
                             customer_order_no,
                             fulfill_order_no,
                             partial_delivery_ind,
                             delivery_type,
                             carrier_code,
                             carrier_service_code,
                             consumer_delivery_date,
                             consumer_delivery_time,
                             bill_first_name,
                             bill_phonetic_first,
                             bill_last_name,
                             bill_phonetic_last,
                             bill_preferred_name,
                             bill_company_name,
                             bill_add1,
                             bill_add2,
                             bill_add3,
                             bill_county,
                             bill_city,
                             bill_state,
                             bill_country_id,
                             bill_post,
                             bill_jurisdiction,
                             bill_phone,
                             deliver_first_name,
                             deliver_phonetic_first,
                             deliver_last_name,
                             deliver_phonetic_last,
                             deliver_preferred_name,
                             deliver_company_name,
                             deliver_add1,
                             deliver_add2,
                             deliver_add3,
                             deliver_city,
                             deliver_state,
                             deliver_country_id,
                             deliver_post,
                             deliver_county,
                             deliver_jurisdiction,
                             deliver_phone,
                             deliver_charge,
                             deliver_charge_curr,
                             comments,
                             create_datetime,
                             create_id,
                             last_update_datetime,
                             last_update_id)
                      values (LP_ordcust_tab(i).ordcust_no,
                              LP_ordcust_tab(i).status,
                              LP_ordcust_tab(i).order_no,
                              LP_ordcust_tab(i).tsf_no,
                              LP_ordcust_tab(i).source_loc_type,
                              LP_ordcust_tab(i).source_loc_id,
                              LP_ordcust_tab(i).fulfill_loc_type,
                              LP_ordcust_tab(i).fulfill_loc_id,
                              LP_ordcust_tab(i).customer_no,
                              LP_ordcust_tab(i).customer_order_no,
                              LP_ordcust_tab(i).fulfill_order_no,
                              LP_ordcust_tab(i).partial_delivery_ind,
                              LP_ordcust_tab(i).delivery_type,
                              LP_ordcust_tab(i).carrier_code,
                              LP_ordcust_tab(i).carrier_service_code,
                              LP_ordcust_tab(i).consumer_delivery_date,
                              LP_ordcust_tab(i).consumer_delivery_time,
                              LP_ordcust_tab(i).bill_first_name,
                              LP_ordcust_tab(i).bill_phonetic_first,
                              LP_ordcust_tab(i).bill_last_name,
                              LP_ordcust_tab(i).bill_phonetic_last,
                              LP_ordcust_tab(i).bill_preferred_name,
                              LP_ordcust_tab(i).bill_company_name,
                              LP_ordcust_tab(i).bill_add1,
                              LP_ordcust_tab(i).bill_add2,
                              LP_ordcust_tab(i).bill_add3,
                              LP_ordcust_tab(i).bill_county,
                              LP_ordcust_tab(i).bill_city,
                              LP_ordcust_tab(i).bill_state,
                              LP_ordcust_tab(i).bill_country_id,
                              LP_ordcust_tab(i).bill_post,
                              LP_ordcust_tab(i).bill_jurisdiction,
                              LP_ordcust_tab(i).bill_phone,
                              LP_ordcust_tab(i).deliver_first_name,
                              LP_ordcust_tab(i).deliver_phonetic_first,
                              LP_ordcust_tab(i).deliver_last_name,
                              LP_ordcust_tab(i).deliver_phonetic_last,
                              LP_ordcust_tab(i).deliver_preferred_name,
                              LP_ordcust_tab(i).deliver_company_name,
                              LP_ordcust_tab(i).deliver_add1,
                              LP_ordcust_tab(i).deliver_add2,
                              LP_ordcust_tab(i).deliver_add3,
                              LP_ordcust_tab(i).deliver_city,
                              LP_ordcust_tab(i).deliver_state,
                              LP_ordcust_tab(i).deliver_country_id,
                              LP_ordcust_tab(i).deliver_post,
                              LP_ordcust_tab(i).deliver_county,
                              LP_ordcust_tab(i).deliver_jurisdiction,
                              LP_ordcust_tab(i).deliver_phone,
                              LP_ordcust_tab(i).deliver_charge,
                              LP_ordcust_tab(i).deliver_charge_curr,
                              LP_ordcust_tab(i).comments,
                              sysdate,
                              LP_user,
                              sysdate,
                              LP_user);
   end if;

   if LP_ordcust_detail_tab is NOT NULL and LP_ordcust_detail_tab.COUNT > 0 then
      FORALL i in LP_ordcust_detail_tab.FIRST..LP_ordcust_detail_tab.LAST
         insert into ordcust_detail(ordcust_no,
                                    item,
                                    ref_item,
                                    original_item,
                                    qty_ordered_suom,
                                    qty_cancelled_suom,
                                    standard_uom,
                                    transaction_uom,
                                    substitute_allowed_ind,
                                    unit_retail,
                                    retail_currency_code,
                                    comments,
                                    create_datetime,
                                    create_id,
                                    last_update_datetime,
                                    last_update_id)
                             values(LP_ordcust_detail_tab(i).ordcust_no,
                                    LP_ordcust_detail_tab(i).item,
                                    LP_ordcust_detail_tab(i).ref_item,
                                    LP_ordcust_detail_tab(i).original_item,
                                    LP_ordcust_detail_tab(i).qty_ordered_suom,
                                    LP_ordcust_detail_tab(i).qty_cancelled_suom,
                                    LP_ordcust_detail_tab(i).standard_uom,
                                    LP_ordcust_detail_tab(i).transaction_uom,
                                    LP_ordcust_detail_tab(i).substitute_allowed_ind,
                                    LP_ordcust_detail_tab(i).unit_retail,
                                    LP_ordcust_detail_tab(i).retail_currency_code,
                                    LP_ordcust_detail_tab(i).comments,
                                    sysdate,
                                    LP_user,
                                    sysdate,
                                    LP_user);
   end if;

   if LP_ordcust_l10n_ext_tab is NOT NULL and LP_ordcust_l10n_ext_tab.COUNT > 0 then
      if ORDCUST_ATTRIB_SQL.PERSIST_ORDCUST_L10N_EXT(O_error_message,
                                                     LP_ordcust_l10n_ext_tab) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_CREATE_TSF;
---------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CANCEL_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'CORESVC_FULFILORD_TSF.VALIDATE_CANCEL_TSF';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_FULFILORDREF_C is
      select 'x'
        from svc_fulfilordref sf
       where sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and exists (select 'x'
                       from tsfhead tsf,
                            ordcust oc
                      where tsf.tsf_no = oc.tsf_no
                        and oc.customer_order_no = sf.customer_order_no
                        and oc.fulfill_order_no = sf.fulfill_order_no
                        and oc.source_loc_type = sf.source_loc_type
                        and oc.source_loc_id = sf.source_loc_id
                        and oc.fulfill_loc_type = sf.fulfill_loc_type
                        and oc.fulfill_loc_id = sf.fulfill_loc_id
                        and tsf.status = 'C'
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_SVC_FULFILORDREF is
      select 'x'
        from svc_fulfilordref sf
       where sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and exists (select 'x'
                       from svc_fulfilorddtlref sr
                      where sr.process_id = sf.process_id
                        and sr.chunk_id = sf.chunk_id
                        and sr.fulfilordref_id = sf.fulfilordref_id
                        and sr.error_msg is NOT NULL
                        and rownum = 1)
         for update nowait;

BEGIN
   -- set process_status to 'C'ompleted if tsfhead.status = 'C'
   L_table := 'SVC_FULFILORDREF';
   open C_LOCK_SVC_FULFILORDREF_C;
   close C_LOCK_SVC_FULFILORDREF_C;

   update svc_fulfilordref sf
      set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
    where sf.process_id = I_process_id
      and sf.chunk_id = I_chunk_id
      and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
      and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
      and exists (select 'x'
                    from tsfhead tsf,
                         ordcust oc
                   where tsf.tsf_no = oc.tsf_no
                     and oc.customer_order_no = sf.customer_order_no
                     and oc.fulfill_order_no = sf.fulfill_order_no
                     and oc.source_loc_type = sf.source_loc_type
                     and oc.source_loc_id = sf.source_loc_id
                     and oc.fulfill_loc_type = sf.fulfill_loc_type
                     and oc.fulfill_loc_id = sf.fulfill_loc_id
                     and tsf.status = 'C'
                     and rownum = 1);

   -- validate header records
   merge into svc_fulfilordref sfr
   using (select *
            from (select svfd.process_id,
                         svfd.chunk_id,
                         svfd.fulfilordref_id,
                         -- validate that a tsfhead record exists for the ordcust.tsf_no associated with the svc_fulfilordref record
                         case
                            when tsf.tsf_no is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_TSF_REC_EXISTS_CO', svfd.customer_order_no, svfd.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that an ordcust record exists for the same customer order fulfillment number and source/fulfill locations
                         case
                            when oc.ordcust_no is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_OC_REC_EXISTS_CO', svfd.customer_order_no, svfd.fulfill_order_no, NULL)||';'
                         end error_msg
                    from svc_fulfilordref svfd,
                         ordcust oc,
                         tsfhead tsf
                   where svfd.process_id = I_process_id
                     and svfd.chunk_id = I_chunk_id
                     and svfd.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                     and svfd.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                     and svfd.customer_order_no = oc.customer_order_no(+)
                     and svfd.fulfill_order_no = oc.fulfill_order_no(+)
                     and svfd.source_loc_type = oc.source_loc_type(+)
                     and svfd.source_loc_id = oc.source_loc_id(+)
                     and svfd.fulfill_loc_type = oc.fulfill_loc_type(+)
                     and svfd.fulfill_loc_id = oc.fulfill_loc_id(+)
                     and oc.tsf_no = tsf.tsf_no(+))
              where error_msg is NOT NULL) inner
   on (sfr.process_id = inner.process_id
       and sfr.chunk_id = inner.chunk_id
       and sfr.fulfilordref_id = inner.fulfilordref_id)
   when matched then
      update set sfr.error_msg = substr(sfr.error_msg || inner.error_msg, 1, 2000),
                 sfr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
                 sfr.last_update_id = LP_user,
                 sfr.last_update_datetime = sysdate;

   if SQL%ROWCOUNT > 0 then
      select substr(error_msg, 1, instr(error_msg, ';')-1)
        into L_error_message
        from svc_fulfilordref
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   -- validate detail records
   merge into svc_fulfilorddtlref sfr
   using (select *
            from (select svfr.process_id,
                         svfr.chunk_id,
                         svfr.fulfilordref_id,
                         svfr.item,
                         -- for records with a corresponding tsfhead record, validate that the item exists in tsfdetail
                         case
                            when NOT exists (select 'x'
                                               from tsfdetail tsd
                                              where tsd.item = svfr.item
                                                and tsd.tsf_no = tsf.tsf_no
                                                and rownum = 1) then
                         SQL_LIB.GET_MESSAGE_TEXT('ITEM_NOT_EXIST_TSFDETAIL', svfr.item, svfd.customer_order_no, svfd.fulfill_order_no)||';'
                         end ||
                         -- validate that the cancel quantity in the input is less or equal to the unselected or undistributed or un-shipped quantity on
                         -- the transfer.
                         case
                            when exists(select 'x'
                                          from tsfdetail td
                                         where td.item = svfr.item
                                           and td.tsf_no = tsf.tsf_no
                                           and (td.tsf_qty - greatest(NVL(ABS(td.distro_qty), 0), NVL(td.selected_qty, 0), NVL(td.ship_qty, 0))) <  svfr.cancel_qty_suom) then
                         SQL_LIB.GET_MESSAGE_TEXT('TSF_NO_DEL_S_N',tsf.tsf_no, svfd.customer_order_no||',' ||'Fulfill Order no: ' ||svfd.fulfill_order_no)||';'
                         end error_msg              
                    from svc_fulfilorddtlref svfr,
                         svc_fulfilordref svfd,
                         ordcust oc,
                         tsfhead tsf
                   where svfr.process_id = I_process_id
                     and svfr.chunk_id = I_chunk_id
                     and svfr.process_id = svfd.process_id
                     and svfr.chunk_id = svfd.chunk_id
                     and svfr.fulfilordref_id = svfd.fulfilordref_id
                     and svfd.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                     and svfd.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
                     and svfd.customer_order_no = oc.customer_order_no
                     and svfd.fulfill_order_no = oc.fulfill_order_no
                     and svfd.source_loc_type = oc.source_loc_type
                     and svfd.source_loc_id = oc.source_loc_id
                     and svfd.fulfill_loc_type = oc.fulfill_loc_type
                     and svfd.fulfill_loc_id = oc.fulfill_loc_id
                     and oc.tsf_no = tsf.tsf_no)
              where error_msg is NOT NULL) inner
   on (sfr.process_id = inner.process_id
       and sfr.chunk_id = inner.chunk_id
       and sfr.fulfilordref_id = inner.fulfilordref_id
       and sfr.item = inner.item)
   when matched then
      update set sfr.error_msg = substr(sfr.error_msg || inner.error_msg, 1, 2000),
                 sfr.last_update_id = LP_user,
                 sfr.last_update_datetime = sysdate;

   if SQL%ROWCOUNT > 0 then
      L_table := 'SVC_FULFILORDREF';
      open C_LOCK_SVC_FULFILORDREF;
      close C_LOCK_SVC_FULFILORDREF;

      update svc_fulfilordref sf
         set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
             sf.error_msg = substr(sf.error_msg || SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_DETAIL', sf.customer_order_no, sf.fulfill_order_no, NULL)||';', 1, 2000),
             sf.last_update_id = LP_user,
             sf.last_update_datetime = sysdate
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtlref sd
                      where sd.fulfilordref_id = sf.fulfilordref_id
                        and sd.process_id = sf.process_id
                        and sd.chunk_id = sf.chunk_id
                        and sd.error_msg is NOT NULL
                        and rownum = 1);

      select substr(sd.error_msg, 1, instr(sd.error_msg, ';')-1)
        into L_error_message
        from svc_fulfilorddtlref sd,
             svc_fulfilordref sf
       where sd.process_id = I_process_id
         and sd.chunk_id = I_chunk_id
         and sf.process_id = sd.process_id
         and sf.chunk_id = sd.chunk_id
         and sf.fulfilordref_id = sd.fulfilordref_id
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF
         and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR
         and sd.error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_CANCEL_TSF;
---------------------------------------------------------------------------------------------------
FUNCTION POPULATE_CANCEL_REC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'CORESVC_FULFILORD_TSF.POPULATE_CANCEL_REC';
   L_qty_to_cancel          SVC_FULFILORDDTLREF.CANCEL_QTY_SUOM%TYPE;
   L_index                  BINARY_INTEGER := 0;

   cursor C_GET_HEADER_RECS is
      select sfr.fulfilordref_id
        from svc_fulfilordref sfr
       where sfr.process_id = I_process_id
         and sfr.chunk_id = I_chunk_id
         and sfr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_TSF;

   cursor C_GET_DETAIL_RECS(I_fulfilordref_id   SVC_FULFILORDREF.FULFILORDREF_ID%TYPE) is
      select tsf.tsf_type,
             tsf.from_loc,
             tsf.from_loc_type,
             tsf.to_loc,
             tsf.to_loc_type,
             tsf.status,
             tsf.create_id,
             sfd.item,
             itm.pack_ind,
             itm.pack_type,
             tsd.inv_status,
             NVL(sfd.cancel_qty_suom, 0) cancel_qty_suom,
             sfd.standard_uom,
             sfd.transaction_uom,
             tsd.tsf_no,
             NVL(tsd.tsf_qty, 0) tsf_qty,
             NVL(tsd.cancelled_qty, 0) cancelled_qty,
             NVL(tsd.selected_qty, 0) selected_qty,
             NVL(tsd.tsf_qty, 0) - NVL(tsd.ship_qty, 0) max_cancel_qty,
             tsd.tsf_seq_no,
             oc.ordcust_no
        from svc_fulfilorddtlref sfd,
             svc_fulfilordref sfr,
             tsfdetail tsd,
             tsfhead tsf,
             ordcust oc,
             item_master itm
       where sfr.process_id = I_process_id
         and sfr.chunk_id = I_chunk_id
         and sfr.fulfilordref_id = I_fulfilordref_id
         and sfd.process_id = sfr.process_id
         and sfd.chunk_id = sfr.chunk_id
         and sfd.fulfilordref_id = sfr.fulfilordref_id
         and sfr.customer_order_no = oc.customer_order_no
         and sfr.fulfill_order_no = oc.fulfill_order_no
         and sfr.source_loc_type = oc.source_loc_type
         and sfr.source_loc_id = oc.source_loc_id
         and sfr.fulfill_loc_type = oc.fulfill_loc_type
         and sfr.fulfill_loc_id = oc.fulfill_loc_id
         and oc.tsf_no = tsd.tsf_no
         and tsd.tsf_no = tsf.tsf_no
         and tsd.item = sfd.item
         and sfd.item = itm.item;

   TYPE HEADER_RECS_TBL is TABLE OF C_GET_HEADER_RECS%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE DETAIL_RECS_TBL is TABLE OF C_GET_DETAIL_RECS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_header_cancel_recs_tab   HEADER_RECS_TBL;
   L_detail_cancel_recs_tab   DETAIL_RECS_TBL;

BEGIN
   open C_GET_HEADER_RECS;
   fetch C_GET_HEADER_RECS bulk collect into L_header_cancel_recs_tab;
   close C_GET_HEADER_RECS;

   if L_header_cancel_recs_tab is NOT NULL and L_header_cancel_recs_tab.COUNT > 0 then

      LP_transfer_tab := RMSSUB_XTSF.TSF_TBL();
      LP_ordcust_detail_tab := ORDCUST_DETAIL_TBL();

      FOR i in L_header_cancel_recs_tab.FIRST..L_header_cancel_recs_tab.LAST LOOP
         open C_GET_DETAIL_RECS(L_header_cancel_recs_tab(i).fulfilordref_id);
         fetch C_GET_DETAIL_RECS bulk collect into L_detail_cancel_recs_tab;
         close C_GET_DETAIL_RECS;

         if L_detail_cancel_recs_tab is NOT NULL and L_detail_cancel_recs_tab.COUNT > 0 then
            --
            FOR j in L_detail_cancel_recs_tab.FIRST..L_detail_cancel_recs_tab.LAST LOOP
               if L_detail_cancel_recs_tab(j).max_cancel_qty > 0 then
                  if L_detail_cancel_recs_tab(j).cancel_qty_suom <= L_detail_cancel_recs_tab(j).max_cancel_qty then
                     L_qty_to_cancel := L_detail_cancel_recs_tab(j).cancel_qty_suom;
                  else
                     L_qty_to_cancel := L_detail_cancel_recs_tab(j).max_cancel_qty;
                  end if;

                  -- build the ordcust_detail collection
                  LP_ordcust_detail_tab.EXTEND();
                  LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).ordcust_no := L_detail_cancel_recs_tab(j).ordcust_no;
                  LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).item := L_detail_cancel_recs_tab(j).item;
                  LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).qty_cancelled_suom := L_qty_to_cancel;
                  LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).standard_uom := L_detail_cancel_recs_tab(j).standard_uom;
                  LP_ordcust_detail_tab(LP_ordcust_detail_tab.COUNT).transaction_uom := L_detail_cancel_recs_tab(j).transaction_uom;

                  -- if this is the first detail record, build the transfer header and initialize the transfer detail collection
                  if j = 1 then
                     LP_transfer_tab.EXTEND();
                     LP_transfer_tab(LP_transfer_tab.COUNT).header.tsf_no := L_detail_cancel_recs_tab(j).tsf_no;
                     LP_transfer_tab(LP_transfer_tab.COUNT).header.tsf_type := L_detail_cancel_recs_tab(j).tsf_type;
                     LP_transfer_tab(LP_transfer_tab.COUNT).header.from_loc := L_detail_cancel_recs_tab(j).from_loc;
                     LP_transfer_tab(LP_transfer_tab.COUNT).header.from_loc_type := L_detail_cancel_recs_tab(j).from_loc_type;
                     LP_transfer_tab(LP_transfer_tab.COUNT).header.to_loc := L_detail_cancel_recs_tab(j).to_loc;
                     LP_transfer_tab(LP_transfer_tab.COUNT).header.to_loc_type := L_detail_cancel_recs_tab(j).to_loc_type;
                     LP_transfer_tab(LP_transfer_tab.COUNT).header.status := L_detail_cancel_recs_tab(j).status;
                     LP_transfer_tab(LP_transfer_tab.COUNT).header.create_id := L_detail_cancel_recs_tab(j).create_id;
                     ---
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.items := ITEM_TBL();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.seq_nos := RMSSUB_XTSF.SEQ_TBL();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.qtys := QTY_TBL();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.existing_qtys := QTY_TBL();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.supp_pack_sizes := RMSSUB_XTSF.SUPP_PACK_TBL();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.inv_statuses := RMSSUB_XTSF.INV_TBL();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_inds := INDICATOR_TBL();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_types := INDICATOR_TBL();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.cancelled_qtys := QTY_TBL();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.selected_qtys := QTY_TBL();
                     LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.tsf_price := RMSSUB_XTSF.PRICE_TBL();
                  end if;

                  -- build the transfer detail collection
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.items.EXTEND();
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.seq_nos.EXTEND();
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.qtys.EXTEND();
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.existing_qtys.EXTEND();
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.supp_pack_sizes.EXTEND();
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.inv_statuses.EXTEND();
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_inds.EXTEND();
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_types.EXTEND();
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.cancelled_qtys.EXTEND();
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.selected_qtys.EXTEND();
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.tsf_price.EXTEND();
                  L_index := LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.items.COUNT;

                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.items(L_index) := L_detail_cancel_recs_tab(j).item;
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.seq_nos(L_index) := L_detail_cancel_recs_tab(j).tsf_seq_no;
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.qtys(L_index) := L_detail_cancel_recs_tab(j).tsf_qty - L_qty_to_cancel;
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.existing_qtys(L_index) := L_detail_cancel_recs_tab(j).tsf_qty;
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.inv_statuses(L_index) := L_detail_cancel_recs_tab(j).inv_status;
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_inds(L_index) := L_detail_cancel_recs_tab(j).pack_ind;
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.pack_types(L_index) := L_detail_cancel_recs_tab(j).pack_type;
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.cancelled_qtys(L_index) := L_detail_cancel_recs_tab(j).cancelled_qty + L_qty_to_cancel;
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.selected_qtys(L_index) := L_detail_cancel_recs_tab(j).selected_qty - L_qty_to_cancel;
                  LP_transfer_tab(LP_transfer_tab.COUNT).tsf_detail.tsf_price(L_index) := NULL;
               end if;
            END LOOP;
         end if; -- if L_detail_cancel_recs_tab.COUNT > 0
      END LOOP;
   end if; -- L_header_cancel_recs_tab.COUNT > 0

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_CANCEL_REC;
---------------------------------------------------------------------------------------------------
FUNCTION PERSIST_CANCEL_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'CORESVC_FULFILORD_TSF.PERSIST_CANCEL_TSF';
   L_message_type   VARCHAR2(15) := RMSSUB_XTSF.LP_dtl_mod_type;
   L_table          VARCHAR2(30);
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDCUST_DETAIL(I_ordcust_no   ORDCUST_DETAIL.ORDCUST_NO%TYPE,
                                I_item         ORDCUST_DETAIL.ITEM%TYPE) is
      select 'x'
        from ordcust_detail
       where ordcust_no = I_ordcust_no
         and item = I_item
         for update nowait;

BEGIN
   if LP_ordcust_detail_tab is NOT NULL and LP_ordcust_detail_tab.COUNT > 0 then
      L_table := 'ORDCUST_DETAIL';
      FOR i in LP_ordcust_detail_tab.FIRST..LP_ordcust_detail_tab.LAST LOOP
         open C_LOCK_ORDCUST_DETAIL(LP_ordcust_detail_tab(i).ordcust_no,
                                    LP_ordcust_detail_tab(i).item);
         close C_LOCK_ORDCUST_DETAIL;
      END LOOP;

      FORALL i in LP_ordcust_detail_tab.FIRST..LP_ordcust_detail_tab.LAST
         update ordcust_detail
            set qty_cancelled_suom = NVL(qty_cancelled_suom, 0) + LP_ordcust_detail_tab(i).qty_cancelled_suom,
                standard_uom = LP_ordcust_detail_tab(i).standard_uom,
                transaction_uom = LP_ordcust_detail_tab(i).transaction_uom,
                last_update_id = LP_user,
                last_update_datetime = sysdate
          where ordcust_no = LP_ordcust_detail_tab(i).ordcust_no
            and item = LP_ordcust_detail_tab(i).item;
   end if;

   if LP_transfer_tab is NOT NULL and LP_transfer_tab.COUNT > 0 then
      if RMSSUB_XTSF_SQL.PERSIST(O_error_message,
                                 LP_transfer_tab,
                                 L_message_type) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_CANCEL_TSF;
---------------------------------------------------------------------------------------------------
END CORESVC_FULFILORD_TSF;
/
