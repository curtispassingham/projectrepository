CREATE OR REPLACE PACKAGE ALC_ALLOC_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name: ALLOC_ALL_PO_OBL
--Purpose:       Allocates all Obligations for a given Purchase Order
--               and inserts them into the Actual Landed Cost tables.
--               This function is only used for Obligations at the
--               Purchase Order Header or Purchase Order/Item levels.
--               Obligations at the PO Header level must wait to be
--               allocated until the PO has been closed.  Obligations
--               at the PO/Item level must be re-allocated once the PO
--               is closed to pick up the final quantities that were
--               received.  This function will be called upon PO closure.
--               Obligations at all other levels will be allocated upon
--               Obligation creation by calling the ALLOC_ALLOC_ALL_OBL_COMPS
--               function.
-------------------------------------------------------------------------------
FUNCTION ALLOC_ALL_PO_OBL(O_error_message IN OUT VARCHAR2,
                          I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name: ALLOC_ALL_OBL_COMPS
--Purpose:       Allocates expenses and assessments for a given
--               Obligation and inserts them into the Actual
--               Landed Cost tables.
-------------------------------------------------------------------------------
FUNCTION ALLOC_ALL_OBL_COMPS(O_error_message     IN OUT VARCHAR2,
                             I_obligation_key    IN     OBLIGATION.OBLIGATION_KEY%TYPE,
                             I_obligation_level  IN     OBLIGATION.OBLIGATION_LEVEL%TYPE,
                             I_key_value_1       IN     OBLIGATION.KEY_VALUE_1%TYPE,
                             I_key_value_2       IN     OBLIGATION.KEY_VALUE_2%TYPE,
                             I_key_value_3       IN     OBLIGATION.KEY_VALUE_3%TYPE,
                             I_key_value_4       IN     OBLIGATION.KEY_VALUE_4%TYPE,
                             I_key_value_5       IN     OBLIGATION.KEY_VALUE_5%TYPE,
                             I_key_value_6       IN     OBLIGATION.KEY_VALUE_6%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Inserts an alc_comp_loc record for each expense and assessment component that
--- has no corresponding obligation or customs entry.
--- act_value is 0 if nom_flag_5 = 'N', otherwise the act_value is determined by
--- the component's est_exp_value/est_assess_value.  Any previously existing
--- alc_comp_loc records for expenses and assessments are deleted.
--- All components are logged under a single alc_head record with no corresponding
--- obligation or customs entry.  Components that are already logged under an
--- obligation or customs entry will be skipped.
------------------------------------------------------------------------------------
FUNCTION INSERT_ELC_COMPS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Adds the order number to the queue
--- This is called for every PO affected by ALC allocation
--- ie. ALC_ALLOC_SQL.ALLOC_ALL_OBL_COMPS and CE_ALLOC_SQL.ALLOC_CE_DETAIL
--- At the end of the allocation process, INSERT_ELC_COMPS_FOR_QUEUE
--- calls INSERT_ELC_COMPS for each affected PO.
------------------------------------------------------------------------------------
FUNCTION ADD_PO_TO_QUEUE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Calls INSERT_ELC_COMPS for each PO in the queue.
------------------------------------------------------------------------------------
FUNCTION INSERT_ELC_COMPS_FOR_QUEUE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Re-allocates ALC.  
------------------------------------------------------------------------------------
FUNCTION REALLOC_ALC (O_error_message      IN OUT VARCHAR2,
                      I_order_no           IN     ORDHEAD.ORDER_NO%TYPE,
                      I_shipment           IN     SHIPMENT.SHIPMENT%TYPE) 
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
END ALC_ALLOC_SQL;
/
