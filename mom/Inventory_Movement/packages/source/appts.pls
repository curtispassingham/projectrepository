
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE APPOINTMENTS_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------
-- Function Name:  VALIDATE_APPOINTMENT
-- Purpose      :  This function will validate whether or not an appointment exists on the appointment
--                 tables.
--------------------------------------------------------
FUNCTION VALIDATE_APPOINTMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_appt            IN       APPT_HEAD.APPT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------
-- Function Name:  VALIDATE_DOC
-- Purpose      :  This function will validate whether or not the document number for the specified document type
--                 exists in RMS.  
--------------------------------------------------------
FUNCTION VALIDATE_DOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists          IN OUT   BOOLEAN,
                      I_doc             IN       APPT_DETAIL.DOC%TYPE,
                      I_doc_type        IN       APPT_DETAIL.DOC_TYPE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------
-- Function Name:  VALIDATE_LOC
-- Purpose      :  This function will validate whether or not the location for the specified location type exists
--                 in RMS.
--------------------------------------------------------
FUNCTION VALIDATE_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists          IN OUT   BOOLEAN,
                      I_loc             IN       APPT_HEAD.LOC%TYPE,
                      I_loc_type        IN       APPT_HEAD.LOC_TYPE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------  
-- Function Name:  VALIDATE_ASN
-- Purpose      :  This function will validate whether or not the ASN passed in exists on an Appointment in RMS.
--------------------------------------------------------
FUNCTION VALIDATE_ASN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists          IN OUT   BOOLEAN,
                      I_asn             IN       SHIPMENT.ASN%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------
-- Function: OPEN_APPOINTMENTS
-- Purpose:  This function will check to see if there are any open Appointments on the input Document
--           Number and Document Type.
--------------------------------------------------------
FUNCTION OPEN_APPOINTMENTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT BOOLEAN,
                           I_doc             IN     APPT_DETAIL.DOC%TYPE,
                           I_doc_type        IN     APPT_DETAIL.DOC_TYPE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------
END;
/
                              
