CREATE OR REPLACE PACKAGE BODY MRT_SQL AS
----------------------------------------------------------------------------------
FUNCTION DELETE_MRT_ITEM (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_mrt_no          IN       MRT_ITEM.MRT_NO%TYPE,
                          I_item            IN       MRT_ITEM.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'MRT_SQL.DELETE_MRT_ITEM';
   L_table     VARCHAR2(20)   := 'MRT_ITEM';

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_MRT_ITEMS is
      select 'x'
        from mrt_item
       where mrt_no = I_mrt_no
         and item   = I_item
         for update nowait;

BEGIN
   ---
   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   --
   if MRT_SQL.DEL_MRT_ITEM_LOC(O_error_message,
                               'A',
                               I_mrt_no,
                               NULL,
                               NULL,
                               I_item,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL) = FALSE then
      return FALSE;
   end if;
   --
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_MRT_ITEMS',
                    'MRT_ITEM',
                    'MRT_NO: '||TO_CHAR(I_mrt_no));
   open C_LOCK_MRT_ITEMS;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_MRT_ITEMS',
                    'MRT_ITEM',
                    'MRT_NO: '||TO_CHAR(I_mrt_no));
   close C_LOCK_MRT_ITEMS;
   --
   delete from mrt_item
    where mrt_no = I_mrt_no
      and item   = I_item;
   --
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'I_mrt_no = '||TO_CHAR(I_mrt_no),
                                            'I_item = '||I_item);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DELETE_MRT_ITEM;
----------------------------------------------------------------------------------
FUNCTION DEL_MRT_ITEM_LOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_delete_type     IN       VARCHAR2,
                           I_mrt_no          IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                           I_loc_type        IN       MRT_ITEM_LOC.LOC_TYPE%TYPE,
                           I_location        IN       MRT_ITEM_LOC.LOCATION%TYPE,
                           I_item            IN       MRT_ITEM_LOC.ITEM%TYPE,
                           I_item_parent     IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                           I_diff1           IN       ITEM_MASTER.DIFF_1%TYPE,
                           I_diff2           IN       ITEM_MASTER.DIFF_2%TYPE,
                           I_diff3           IN       ITEM_MASTER.DIFF_3%TYPE,
                           I_diff4           IN       ITEM_MASTER.DIFF_4%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64)   := 'MRT_SQL.DEL_MRT_ITEM_LOC';
   L_table         VARCHAR2(20)   := 'MRT_ITEM_LOC';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_MRT_ITEM_LOC_SINGLE is
      select 'x'
        from mrt_item_loc
       where mrt_no   = I_mrt_no
         and location = I_location
         and loc_type = I_loc_type
         and item     = I_item
         for update nowait;

   cursor C_LOCK_MRT_ITEM_LOC_ALL_LOCS is
      select 'x'
        from mrt_item_loc
       where mrt_no   = I_mrt_no
         and item     = I_item
         for update nowait;

   cursor C_LOCK_MRT_ITEM_LOC_ALL_ITEMS is
      select 'x'
        from mrt_item_loc
       where mrt_no    = I_mrt_no
         and loc_type  = I_loc_type
         and location  = I_location
         and item in (select mrt.item
                        from item_master ite, mrt_item mrt
                       where (I_item_parent is null or ite.item_parent = I_item_parent)
                         and (I_diff1 is null or ite.diff_1 = I_diff1)
                         and (I_diff2 is null or ite.diff_2 = I_diff2)
                         and (I_diff3 is null or ite.diff_3 = I_diff3)
                         and (I_diff4 is null or ite.diff_4 = I_diff4)
                         and ite.item         = mrt.item)
         for update nowait;

   cursor C_LOCK_MRT_ALL_LOC_ALL_ITEMS is
      select 'x'
        from mrt_item_loc
       where mrt_no    = I_mrt_no
         and item in (select mrt.item
                        from item_master ite, mrt_item mrt
                       where (I_item is null or ite.item = I_item)
                         and (I_item_parent is null or ite.item_parent = I_item_parent)
                         and (I_diff1 is null or ite.diff_1 = I_diff1)
                         and (I_diff2 is null or ite.diff_2 = I_diff2)
                         and (I_diff3 is null or ite.diff_3 = I_diff3)
                         and (I_diff4 is null or ite.diff_4 = I_diff4)
                         and ite.item         = mrt.item)
         for update nowait;

BEGIN
   ---
   if I_delete_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_delete_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_delete_type = 'S' then --type is 'S'ingle delete
      if I_item is NULL then
         O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              'I_item',
                                              L_program,
                                              NULL);
         return FALSE;
      end if;

      if I_loc_type is NULL then
         O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              'I_loc_type',
                                              L_program,
                                              NULL);
         return FALSE;
      end if;

      if I_location is NULL then
         O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              'I_location',
                                              L_program,
                                              NULL);
         return FALSE;
      end if;
      --
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_MRT_ITEM_LOC_SINGLE',
                       'MRT_ITEM_LOC',
                       'MRT_NO: '||TO_CHAR(I_mrt_no)||' LOCATION: '||TO_CHAR(I_location)||' ITEM: '||I_item);
      open C_LOCK_MRT_ITEM_LOC_SINGLE;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_MRT_ITEM_LOC_SINGLE',
                       'MRT_ITEM_LOC',
                       'MRT_NO: '||TO_CHAR(I_mrt_no)||' LOCATION: '||TO_CHAR(I_location)||' ITEM: '||I_item);
      close C_LOCK_MRT_ITEM_LOC_SINGLE;
      --
      delete from mrt_item_loc
       where mrt_no   = I_mrt_no
         and location = I_location
         and loc_type = I_loc_type
         and item     = I_item;

   else -- type is 'delete A'll
      if I_location is NULL  and I_item is not NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_MRT_ITEM_LOC_ALL_LOCS',
                          'MRT_ITEM_LOC',
                          'MRT_NO: '||TO_CHAR(I_mrt_no));
         open C_LOCK_MRT_ITEM_LOC_ALL_LOCS;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_MRT_ITEM_LOC_ALL_LOCS',
                          'MRT_ITEM_LOC',
                          'MRT_NO: '||TO_CHAR(I_mrt_no));
         close C_LOCK_MRT_ITEM_LOC_ALL_LOCS;
         --
         --Delete all locations from a single item

         delete from mrt_item_loc
          where mrt_no   = I_mrt_no
            and item     = I_item;

      elsif I_location is not NULL  and I_item is NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_MRT_ITEM_LOC_ALL_ITEMS',
                          'MRT_ITEM_LOC',
                          'MRT_NO: '||TO_CHAR(I_mrt_no));
         open C_LOCK_MRT_ITEM_LOC_ALL_ITEMS;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_MRT_ITEM_LOC_ALL_ITEMS',
                          'MRT_ITEM_LOC',
                          'MRT_NO: '||TO_CHAR(I_mrt_no));
         close C_LOCK_MRT_ITEM_LOC_ALL_ITEMS;

         --Delete location from all filtered items
         delete from mrt_item_loc
               where mrt_no    = I_mrt_no
                 and loc_type  = I_loc_type
                 and location  = I_location
                 and item in (select mrt.item
                                from item_master ite, mrt_item mrt
                               where (I_item_parent is null or ite.item_parent = I_item_parent)
                                 and (I_diff1 is null or ite.diff_1 = I_diff1)
                                 and (I_diff2 is null or ite.diff_2 = I_diff2)
                                 and (I_diff3 is null or ite.diff_3 = I_diff3)
                                 and (I_diff4 is null or ite.diff_4 = I_diff4)
                                 and ite.item         = mrt.item);

      elsif I_location is NULL  and I_item is NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_MRT_ALL_LOC_ALL_ITEMS',
                          'MRT_ITEM_LOC',
                          'MRT_NO: '||TO_CHAR(I_mrt_no));
         open C_LOCK_MRT_ALL_LOC_ALL_ITEMS;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_MRT_ALL_LOC_ALL_ITEMS',
                          'MRT_ITEM_LOC',
                          'MRT_NO: '||TO_CHAR(I_mrt_no));
         close C_LOCK_MRT_ALL_LOC_ALL_ITEMS;

         --Delete all location from all filtered items
         delete from mrt_item_loc
               where mrt_no    = I_mrt_no
                 and item in (select mrt.item
                                from item_master ite, mrt_item mrt
                               where (I_item is null or ite.item = I_item)
                                 and (I_item_parent is null or ite.item_parent = I_item_parent)
                                 and (I_diff1 is null or ite.diff_1 = I_diff1)
                                 and (I_diff2 is null or ite.diff_2 = I_diff2)
                                 and (I_diff3 is null or ite.diff_3 = I_diff3)
                                 and (I_diff4 is null or ite.diff_4 = I_diff4)
                                 and ite.item         = mrt.item);
      end if;
      ---
   end if;
   ---

   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'I_mrt_no = '||TO_CHAR(I_mrt_no),
                                            'I_item = '||I_item);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DEL_MRT_ITEM_LOC;
----------------------------------------------------------------------------------
FUNCTION UPDATE_TSF_COST_PRICE_QTY      (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_apply_type            IN       VARCHAR2,
                                         I_item_loc_ind          IN       VARCHAR2,
                                         I_group_loc_type        IN       MRT_ITEM_LOC.LOC_TYPE%TYPE,
                                         I_group_loc_value       IN       MRT_ITEM_LOC.LOCATION%TYPE,
                                         I_mrt_no                IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                                         I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                         I_item_parent           IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                                         I_diff1                 IN       ITEM_MASTER.DIFF_1%TYPE,
                                         I_diff2                 IN       ITEM_MASTER.DIFF_2%TYPE,
                                         I_diff3                 IN       ITEM_MASTER.DIFF_3%TYPE,
                                         I_diff4                 IN       ITEM_MASTER.DIFF_4%TYPE,
                                         I_from_loc_unit_cost    IN       MRT_ITEM_LOC.TSF_COST%TYPE,
                                         I_from_loc_unit_price   IN       MRT_ITEM_LOC.TSF_PRICE%TYPE,
                                         I_from_loc_tsf_qty      IN       MRT_ITEM_LOC.TSF_QTY%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)    := 'MRT_SQL.UPDATE_TFROM_LOC_TSF_COST_PRICE';
   L_table     VARCHAR2(20)    := NULL;
   L_mrt_row   MRT%ROWTYPE;
   L_exists    BOOLEAN;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SINGLE_ITEM_SINGLE_LOC is
      select mrt_no
        from mrt_item_loc
       where mrt_no    = I_mrt_no
         and loc_type  = I_group_loc_type
         and location  = I_group_loc_value
         and item      = I_item
         for update nowait;

   cursor C_LOCK_ALL_ITEMS_SINGLE_LOC is
      select mrt_no
        from mrt_item_loc
       where mrt_no    = I_mrt_no
         and loc_type  = I_group_loc_type
         and location  = I_group_loc_value
         and item in (select mrt.item
                        from item_master ite, mrt_item mrt
                       where (I_item is null or ite.item = I_item)
                         and (I_item_parent is null or ite.item_parent = I_item_parent)
                         and (I_diff1 is null or ite.diff_1 = I_diff1)
                         and (I_diff2 is null or ite.diff_2 = I_diff2)
                         and (I_diff3 is null or ite.diff_3 = I_diff3)
                         and (I_diff4 is null or ite.diff_4 = I_diff4)
                         and ite.item         = mrt.item
                         and mrt.selected_ind = 'Y')
         for update nowait;

   cursor C_LOCK_ALL_LOCS_SINGLE_ITEM is
      select mrt_no
        from mrt_item_loc
       where mrt_no    = I_mrt_no
         and item      = I_item
         for update nowait;


   cursor C_LOCK_ALL_ITEMS_ALL_LOCS is
      select mrt_no
        from mrt_item_loc
       where mrt_no    = I_mrt_no
         and item in (select mrt.item
                        from item_master ite, mrt_item mrt
                       where (I_item is null or ite.item = I_item)
                         and (I_item_parent is null or ite.item_parent = I_item_parent)
                         and (I_diff1 is null or ite.diff_1 = I_diff1)
                         and (I_diff2 is null or ite.diff_2 = I_diff2)
                         and (I_diff3 is null or ite.diff_3 = I_diff3)
                         and (I_diff4 is null or ite.diff_4 = I_diff4)
                         and ite.item         = mrt.item)
         for update nowait;

BEGIN

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_apply_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_apply_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item_loc_ind is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_apply_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_apply_type = 'S' then
      if I_group_loc_type is NULL then
         O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              'I_group_loc_type',
                                              L_program,
                                              NULL);
         return FALSE;
      end if;

      if I_group_loc_value is NULL then
         O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              'I_group_loc_value',
                                              L_program,
                                              NULL);
         return FALSE;
      end if;
      ---
   end if;
   L_table := 'MRT_ITEM_LOC';

   --Get the Mrt attributes
   if MRT_ATTRIB_SQL.GET_MRT_INFO (O_error_message,
                                   L_mrt_row,
                                   L_exists,
                                   I_mrt_no) = FALSE then
      return FALSE;
   end if;
   if not L_exists then
      O_error_message:= SQL_LIB.CREATE_MSG('INV_MRT_NO',
                                           I_mrt_no,
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_apply_type = 'S' and I_item_loc_ind = 'I' then
   --Update all locations for a single item
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_SINGLE_ITEM_SINGLE_LOC',
                       L_table,
                       'MRT_NO: '||TO_CHAR(I_mrt_no));
      open C_LOCK_SINGLE_ITEM_SINGLE_LOC;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_SINGLE_ITEM_SINGLE_LOC',
                       L_table,
                       'MRT_NO: '||TO_CHAR(I_mrt_no));
      close C_LOCK_SINGLE_ITEM_SINGLE_LOC;
      ---
      update mrt_item_loc
         set tsf_cost  = nvl(I_from_loc_unit_cost, tsf_cost),
             tsf_price = nvl(I_from_loc_unit_price, tsf_price),
             tsf_qty   = DECODE(L_mrt_row.quantity_type, 'A',return_avail_qty,
                                LEAST(nvl(I_from_loc_tsf_qty, tsf_qty),return_avail_qty))
       where mrt_no    = I_mrt_no
         and loc_type  = I_group_loc_type
         and location  = I_group_loc_value
         and item      = I_item;
   elsif I_apply_type = 'A' and I_item_loc_ind = 'L' then
   --Update all locations for a single item
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ALL_LOCS_SINGLE_ITEM',
                       L_table,
                       'MRT_NO: '||TO_CHAR(I_mrt_no));
      open C_LOCK_ALL_LOCS_SINGLE_ITEM;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ALL_LOCS_SINGLE_ITEM',
                       L_table,
                       'MRT_NO: '||TO_CHAR(I_mrt_no));
      close C_LOCK_ALL_LOCS_SINGLE_ITEM;
      ---
      update mrt_item_loc
         set tsf_cost  = nvl(I_from_loc_unit_cost, tsf_cost),
             tsf_price = nvl(I_from_loc_unit_price, tsf_price),
             tsf_qty   = DECODE(L_mrt_row.quantity_type, 'A',return_avail_qty,
                                LEAST(nvl(I_from_loc_tsf_qty, tsf_qty),return_avail_qty))
       where mrt_no    = I_mrt_no
         and item      = I_item;
   elsif I_apply_type = 'A' and I_item_loc_ind = 'I' then
   --Update single locations for all items
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ALL_ITEMS_SINGLE_LOC',
                       L_table,
                       'MRT_NO: '||TO_CHAR(I_mrt_no));
      open C_LOCK_ALL_ITEMS_SINGLE_LOC;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ALL_ITEMS_SINGLE_LOC',
                       L_table,
                       'MRT_NO: '||TO_CHAR(I_mrt_no));
      close C_LOCK_ALL_ITEMS_SINGLE_LOC;
      ---
      update mrt_item_loc
         set tsf_cost  = nvl(I_from_loc_unit_cost, tsf_cost),
             tsf_price = nvl(I_from_loc_unit_price, tsf_price),
             tsf_qty   = DECODE(L_mrt_row.quantity_type, 'A',return_avail_qty,
                                LEAST(nvl(I_from_loc_tsf_qty, tsf_qty),return_avail_qty))
       where mrt_no    = I_mrt_no
         and loc_type  = I_group_loc_type
         and location  = I_group_loc_value
         and item in (select mrt.item
                        from item_master ite, mrt_item mrt
                       where (I_item_parent is null or ite.item_parent = I_item_parent)
                         and (I_diff1 is null or ite.diff_1 = I_diff1)
                         and (I_diff2 is null or ite.diff_2 = I_diff2)
                         and (I_diff3 is null or ite.diff_3 = I_diff3)
                         and (I_diff4 is null or ite.diff_4 = I_diff4)
                         and ite.item         = mrt.item);
   elsif I_apply_type = 'A' and I_item_loc_ind = 'A' then
   --Update single locations for all items
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ALL_ITEMS_ALL_LOCS',
                       L_table,
                       'MRT_NO: '||TO_CHAR(I_mrt_no));
      open C_LOCK_ALL_ITEMS_ALL_LOCS;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ALL_ITEMS_ALL_LOCS',
                       L_table,
                       'MRT_NO: '||TO_CHAR(I_mrt_no));
      close C_LOCK_ALL_ITEMS_ALL_LOCS;
      ---
      update mrt_item_loc
         set tsf_cost  = nvl(I_from_loc_unit_cost, tsf_cost),
             tsf_price = nvl(I_from_loc_unit_price, tsf_price),
             tsf_qty   = DECODE(L_mrt_row.quantity_type, 'A',return_avail_qty,
                                LEAST(nvl(I_from_loc_tsf_qty, tsf_qty),return_avail_qty))
       where mrt_no    = I_mrt_no
         and item in (select mrt.item
                        from item_master ite, mrt_item mrt
                       where (I_item is null or ite.item = I_item)
                         and (I_item_parent is null or ite.item_parent = I_item_parent)
                         and (I_diff1 is null or ite.diff_1 = I_diff1)
                         and (I_diff2 is null or ite.diff_2 = I_diff2)
                         and (I_diff3 is null or ite.diff_3 = I_diff3)
                         and (I_diff4 is null or ite.diff_4 = I_diff4)
                         and ite.item         = mrt.item);
   end if;

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'I_mrt_no   = '||TO_CHAR(I_mrt_no),
                                            'I_location = '||TO_CHAR(I_group_loc_value));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_TSF_COST_PRICE_QTY;
-----------------------------------------------------------------------------------
FUNCTION CREATE_MRT_ITEM (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_mrt_no             IN       MRT_ITEM.MRT_NO%TYPE,
                          I_item_type          IN       VARCHAR2,
                          I_value              IN       ITEM_MASTER.ITEM%TYPE,
                          I_unit_cost          IN       MRT_ITEM_LOC.UNIT_COST%TYPE,
                          I_restocking_pct     IN       MRT_ITEM.RESTOCK_PCT%TYPE,
                          I_selected_ind       IN       MRT_ITEM.SELECTED_IND%TYPE,
                          I_supplier           IN       ITEM_SUPPLIER.SUPPLIER%TYPE DEFAULT NULL) ---new code
   RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)   := 'MRT_SQL.CREATE_MRT_ITEM';
   L_table                    VARCHAR2(20)   := 'MRT';
   L_exists                   BOOLEAN;
   L_mrt_rec                  MRT%ROWTYPE;
   L_ph_wh                    WH.PHYSICAL_WH%TYPE;
   L_pwh_flag                 BOOLEAN;
   L_wh_for_last_receipt_cost NUMBER(10);
   L_rtv_cost                 MRT_ITEM.RTV_COST%TYPE;
   L_orig_ctry_id             ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_wh_currency_code         WH.CURRENCY_CODE%TYPE;
   L_supp_currency_code       SUPS.CURRENCY_CODE%TYPE;

   cursor C_LOC_ITEM_LIST is
      select im.item
        from skulist_detail sd,
             item_master    im,
             item_supplier  its,
             mrt            mr
       where sd.skulist    = I_value
         and ((sd.item = im.item and im.item_level = im.tran_level)
              or (sd.item = im.item_parent
                  and im.item_level = im.tran_level
                  and not exists (select 1 from skulist_detail sd2
                                   where sd2.item = im.item
                                     and sd2.skulist = sd.skulist))
              or (sd.item = im.item_grandparent
                  and im.item_level = im.tran_level
                  and not exists (select 1 from skulist_detail sd3
                                   where sd3.item = im.item
                                     and sd3.skulist = sd.skulist)))
         and im.item       = its.item
         and im.status     = 'A'
         and mr.mrt_no     = I_mrt_no
         and im.inventory_ind = 'Y'
         and its.supplier = I_supplier
         and not exists (select 'x'
                           from mrt_item mi
                          where mi.mrt_no = mr.mrt_no
                            and mi.item   = im.item);

   cursor C_LOC_ITEM_PARENT is
      select im.item
        from item_master im,
             item_supplier its,
             mrt           mr
       where im.item_parent   = I_value
         and mr.mrt_no        = I_mrt_no
         and im.item          = its.item
         and im.status        = 'A'
         and im.item_level    = im.tran_level
         and im.inventory_ind = 'Y'
         and its.supplier     = I_supplier
         and not exists(select 'x'
                           from mrt_item mi
                          where mi.mrt_no = mr.mrt_no
                            and mi.item   = im.item);

   cursor C_LOC_ITEM_GRANDPARENT is
   select im.item
     from item_master   im,
          item_supplier its,
          mrt           mr
    where im.item_grandparent = I_value
      and mr.mrt_no           = I_mrt_no
      and im.status           = 'A'
      and im.item_level       = im.tran_level
      and im.item             = its.item
      and im.inventory_ind    = 'Y'
      and its.supplier        = I_supplier
      and not exists(select'x'
                      from mrt_item mi
                     where mi.mrt_no = mr.mrt_no
                       and mi.item   = im.item);

BEGIN
   ---
   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_value is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_value',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_selected_ind is NULL then
         O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              'I_selected_ind',
                                              L_program,
                                              NULL);
         return FALSE;
   end if;
   --
   --Need to get the mrt wh for the mrt to use in retrieving the rtv cost
   --for an RTV type mrt (mrt with supplier) and check the mrt exists
   if MRT_ATTRIB_SQL.GET_MRT_INFO(O_error_message,
                                  L_mrt_rec,
                                  L_exists,
                                  I_mrt_no) = FALSE then
      return FALSE;
   end if;
   ---
   if not L_exists then
       O_error_message := SQL_LIB.CREATE_MSG('INV_MRT', NULL, NULL, NULL);
       return FALSE;
   end if;
   ---
   --To get the last receipt cost we need to use the physical warehouse linked
   --to the mrt warehouse. The mrt warehouse could be a physical or virtual wh.
   --If this is a virtual warehouse. A receipt will never
   --come in as the value of a virtual warehouse so get the physical wh for the
   --virtual wh
   --Check the mrt wh is a physical wh
   if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                              L_pwh_flag,
                              L_mrt_rec.wh) = FALSE then
      return FALSE;
   end if;
   --If mrt wh not phsical go and get the physical. This is then used in the
   --insert/selects statements below to get the last recipt cost.
   if not L_pwh_flag then --Its a virtual warehouse so get the physical
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_wh_for_last_receipt_cost,
                                       L_mrt_rec.wh) = FALSE then
         return FALSE;
      end if;
   else
      L_wh_for_last_receipt_cost := L_mrt_rec.wh;
   end if;
   --Get the wh currency to be used in currency conversion later
   if WH_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                      L_wh_currency_code,
                                      L_wh_for_last_receipt_cost) =  FALSE then
      return FALSE;
   end if;

   if I_supplier is not null then
      --Get the supplier currency to be used in currency conversion later
      if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                           L_supp_currency_code,
                                           I_supplier) =  FALSE then
         return FALSE;
      end if;
   end if;

   if I_item_type = 'IL' then

      if I_supplier is not NULL then

         FOR rec in C_LOC_ITEM_LIST LOOP
            L_rtv_cost := I_unit_cost;

            if L_rtv_cost is NULL then
               --get primary origin country
               if ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                            L_exists,
                                                            L_orig_ctry_id,
                                                            rec.item,
                                                            I_supplier) = FALSE then
                  return FALSE;
               end if;

               --get unit_cost
               if ITEM_SUPP_COUNTRY_SQL.GET_UNIT_COST(O_error_message,
                                                      L_rtv_cost,--will be in supplier currency
                                                      rec.item,
                                                      I_supplier,
                                                      L_orig_ctry_id) = FALSE then
                  return FALSE;
               end if;
               --convert to Mrt Currency
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_rtv_cost,
                                       L_supp_currency_code,
                                       L_mrt_rec.currency_code,
                                       L_rtv_cost,
                                      'C',
                                      NULL,
                                      NULL) = FALSE then
                  return FALSE;
                end if;
            end if;

            SQL_LIB.SET_MARK('INSERT',NULL, 'MRT_ITEM' ,NULL);
            insert into mrt_item (mrt_no,
                                  item,
                                  restock_pct,
                                  selected_ind,
                                  rtv_cost)
                          values (I_mrt_no,
                                  rec.item,
                                  I_restocking_pct,
                                  I_selected_ind,
                                  L_rtv_cost);

         END LOOP;

      else

         SQL_LIB.SET_MARK('INSERT',NULL, 'MRT_ITEM' ,NULL);
            insert into mrt_item(mrt_no,
                                 item,
                                 restock_pct,
                                 selected_ind,
                                 rtv_cost)
                          select I_mrt_no,
                                 im.item,
                                 I_restocking_pct,
                                 I_selected_ind,
                                 I_unit_cost
                            from skulist_detail sd,
                                 item_master im,
                                 mrt
                            where sd.skulist = I_value
                              and ((sd.item = im.item and im.item_level = im.tran_level)
                                    or (sd.item = im.item_parent
                                        and im.item_level = im.tran_level
                                        and not exists (select 1 from skulist_detail sd2
                                                         where sd2.item = im.item
                                                           and sd2.skulist = sd.skulist))
                                    or (sd.item = im.item_grandparent
                                        and im.item_level = im.tran_level
                                        and not exists (select 1 from skulist_detail sd3
                                                         where sd3.item = im.item
                                                           and sd3.skulist = sd.skulist)))
                              and mrt.mrt_no  = I_mrt_no
                              and im.status = 'A'
                              and not exists(select 'x'
                                               from mrt_item mi
                                              where mi.mrt_no = I_mrt_no
                                                and mi.item  = im.item)
                              and im.inventory_ind = 'Y';
      end if;

   elsif  I_item_type = 'IP' then

      if I_supplier is not NULL then

         FOR rec in C_LOC_ITEM_PARENT LOOP
            L_rtv_cost := I_unit_cost;

            if L_rtv_cost is NULL then
               --get primary origin country
               if ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                            L_exists,
                                                            L_orig_ctry_id,
                                                            rec.item,
                                                            I_supplier) = FALSE then
                  return FALSE;
               end if;

               --get unit_cost
               if ITEM_SUPP_COUNTRY_SQL.GET_UNIT_COST(O_error_message,
                                                      L_rtv_cost,--will be in supplier currency
                                                      rec.item,
                                                      I_supplier,
                                                      L_orig_ctry_id) = FALSE then
                  return FALSE;
               end if;
               --convert to Mrt Currency
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_rtv_cost,
                                       L_supp_currency_code,
                                       L_mrt_rec.currency_code,
                                       L_rtv_cost,
                                      'C',
                                      NULL,
                                      NULL) = FALSE then
                  return FALSE;
                end if;

            end if;


            SQL_LIB.SET_MARK('INSERT',NULL, 'MRT_ITEM' ,NULL);

            insert into mrt_item (mrt_no,
                                  item,
                                  restock_pct,
                                  selected_ind,
                                  rtv_cost)
                          values (I_mrt_no,
                                  rec.item,
                                  I_restocking_pct,
                                  I_selected_ind,
                                  L_rtv_cost);

         END LOOP;

      else

         SQL_LIB.SET_MARK('INSERT',NULL, 'MRT_ITEM' ,NULL);
         insert into mrt_item(mrt_no,
                              item,
                              restock_pct,
                              selected_ind,
                              rtv_cost)
                       select I_mrt_no,
                              im.item,
                              I_restocking_pct,
                              I_selected_ind,
                              I_unit_cost
                         from item_master im,
                              mrt
                        where im.item_parent = I_value
                          and mrt.mrt_no = I_mrt_no
                          and im.item_level = im.tran_level
                          and im.inventory_ind = 'Y'
                          and im.status = 'A'
                          and not exists(select 'x'
                                           from mrt_item mi
                                          where mi.mrt_no = I_mrt_no
                                            and mi.item = im.item);
      end if;

   elsif I_item_type = 'IG' then

      if I_supplier is not NULL then

         FOR rec in C_LOC_ITEM_GRANDPARENT LOOP
            L_rtv_cost := I_unit_cost;

            if L_rtv_cost is NULL then
               --get primary origin country
               if ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                            L_exists,
                                                            L_orig_ctry_id,
                                                            rec.item,
                                                            I_supplier) = FALSE then
                  return FALSE;
               end if;

               --get unit_cost
               if ITEM_SUPP_COUNTRY_SQL.GET_UNIT_COST(O_error_message,
                                                      L_rtv_cost,--will be in supplier currency
                                                      rec.item,
                                                      I_supplier,
                                                      L_orig_ctry_id) = FALSE then
                  return FALSE;
               end if;
               --convert to Mrt Currency
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_rtv_cost,
                                       L_supp_currency_code,
                                       L_mrt_rec.currency_code,
                                       L_rtv_cost,
                                      'C',
                                      NULL,
                                      NULL) = FALSE then
                  return FALSE;
                end if;
            end if;

            SQL_LIB.SET_MARK('INSERT',NULL, 'MRT_ITEM' ,NULL);
            insert into mrt_item (mrt_no,
                                  item,
                                  restock_pct,
                                  selected_ind,
                                  rtv_cost)
                          values (I_mrt_no,
                                  rec.item,
                                  I_restocking_pct,
                                  I_selected_ind,
                                  L_rtv_cost);

         END LOOP;

      else

         SQL_LIB.SET_MARK('INSERT',NULL, 'MRT_ITEM' ,NULL);
          insert into mrt_item(mrt_no,
                            item,
                            restock_pct,
                            selected_ind,
                            rtv_cost)
                     select I_mrt_no,
                            im.item,
                            I_restocking_pct,
                            I_selected_ind,
                            I_unit_cost
                       from item_master im,
                            mrt
                      where im.item_grandparent = I_value
                        and mrt.mrt_no = I_mrt_no
                        and im.item_level = im.tran_level
                        and im.status = 'A'
                        and not exists(select'x'
                                        from mrt_item mi
                                       where mi.mrt_no = I_mrt_no
                                         and mi.item      = im.item)
                        and im.inventory_ind = 'Y';
      end if;

   elsif I_item_type = 'I' then

      if I_supplier is not NULL then

         L_rtv_cost := I_unit_cost;

         if L_rtv_cost is NULL then
            --get primary origin country
            if ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                         L_exists,
                                                         L_orig_ctry_id,
                                                         I_value,
                                                         I_supplier) = FALSE then
               return FALSE;
            end if;

            --get unit_cost
            if ITEM_SUPP_COUNTRY_SQL.GET_UNIT_COST(O_error_message,
                                                   L_rtv_cost,--will be in supplier currency
                                                   I_value,
                                                   I_supplier,
                                                   L_orig_ctry_id) = FALSE then
               return FALSE;
            end if;
            --convert to Mrt Currency
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_rtv_cost,
                                    L_supp_currency_code,
                                    L_mrt_rec.currency_code,
                                    L_rtv_cost,
                                   'C',
                                   NULL,
                                   NULL) = FALSE then
               return FALSE;
             end if;
         end if;

         SQL_LIB.SET_MARK('INSERT',NULL, 'MRT_ITEM' ,NULL);
         insert into mrt_item (mrt_no,
                               item,
                               restock_pct,
                               selected_ind,
                               rtv_cost)
                       values (I_mrt_no,
                               I_value,
                               I_restocking_pct,
                               I_selected_ind,
                               L_rtv_cost);

      else
         SQL_LIB.SET_MARK('INSERT',NULL, 'MRT_ITEM' ,NULL);
         insert into mrt_item(mrt_no,
                              item,
                              restock_pct,
                              selected_ind,
                              rtv_cost)
                       values(I_mrt_no,
                              I_value,
                              I_restocking_pct,
                              I_selected_ind,
                              I_unit_cost);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CREATE_MRT_ITEM;
-------------------------------------------------------------------------------------------
FUNCTION CREATE_MRT_ITEM_LOC (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_stockholding_exists   IN OUT   BOOLEAN,
                              O_soh_exists            IN OUT   BOOLEAN,
                              O_location_exists       IN OUT  BOOLEAN,
                              I_item_apply_type       IN       VARCHAR2,
                              I_group_loc_type        IN       MRT_ITEM_LOC.LOC_TYPE%TYPE,
                              I_group_loc_value       IN       VARCHAR2,
                              I_mrt_no                IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                              I_item                  IN       MRT_ITEM_LOC.ITEM%TYPE,
                              I_item_parent           IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                              I_diff_1                IN       ITEM_MASTER.DIFF_1%TYPE,
                              I_diff_2                IN       ITEM_MASTER.DIFF_2%TYPE,
                              I_diff_3                IN       ITEM_MASTER.DIFF_3%TYPE,
                              I_diff_4                IN       ITEM_MASTER.DIFF_4%TYPE,
                              I_inv_status            IN       VARCHAR2,
                              I_tsf_qty               IN       MRT_ITEM_LOC.TSF_QTY%TYPE,
                              I_tsf_cost              IN       MRT_ITEM_LOC.TSF_COST%TYPE,
                              I_tsf_price             IN       MRT_ITEM_LOC.TSF_PRICE%TYPE,
                              I_inter_intra_ind       IN       VARCHAR2,
                              I_tsf_entity_id         IN       V_TRANSFER_FROM_LOC.TSF_ENTITY_ID%TYPE,
                              I_set_of_books_id       IN       TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE)
   RETURN BOOLEAN IS

   L_location      LOC_TBL  := LOC_TBL();
   L_item          ITEM_TBL := ITEM_TBL();

   L_itemloc_table OBJ_ITEMLOC_TBL := OBJ_ITEMLOC_TBL();

   L_system_options_rec             SYSTEM_OPTIONS%ROWTYPE;
   L_intercompany_transfer_basis    SYSTEM_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE;

   CURSOR C_MRT_INFO IS
      select m.supplier, m.mrt_type, m.currency_code, m.inventory_type
        from mrt m
       where m.mrt_no = I_mrt_no;

   CURSOR C_LOC_STORE IS
   --The validation of the store passed in has been done through
   --the mrtloc.fmb so we dont need to make the cursor overly complicated
   --and therefore more efficient
      select distinct s.store,I_item
        from item_loc_soh ils,
             v_store s
       where s.store           = I_group_loc_value
         and ils.item          = I_item
         and ils.loc           = s.store
         and ils.loc_type      = 'S'
         and ils.stock_on_hand > 0;

   CURSOR C_LOC_WH IS
   --The validation of the warehouse passed in has been done through
   --the mrtloc.fmb so we dont need to make the cursor overly complicated
   --and therefore more efficient
      select  w.wh, I_item
        from  item_loc_soh ils,
              v_wh w
       where w.wh = I_group_loc_value
         and ils.item          = I_item
         and ils.loc           = w.wh
         and ils.loc_type      = 'W'
         and ils.stock_on_hand > 0;

   CURSOR C_LOC_ALL_STORE IS
      select distinct s.store,I_item
        from item_loc_soh ils,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and ils.item           = I_item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = I_item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
        and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_LOC_ALL_WH IS
      select  w.wh, I_item
        from  item_loc_soh ils,
              v_wh w
       where stockholding_ind = 'Y'
         and ils.item          = I_item
         and ils.loc           = w.wh
         and ils.loc_type      = 'W'
         and ils.stock_on_hand > 0
         and not exists (select 1
                          from mrt_item_loc mrl
                         where mrl.mrt_no   = I_mrt_no
                           and mrl.item     = I_item
                           and mrl.location = w.wh
                           and mrl.loc_type = 'W')
        and not exists (select 1
                          from mrt m
                         where m.mrt_no = I_mrt_no
                           and m.wh = w.wh)
        and (I_inter_intra_ind is NULL
            or (I_inter_intra_ind = 'A'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where L_intercompany_transfer_basis = 'T'
                               and vw.tsf_entity_id = I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id = I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh))
            or (I_inter_intra_ind = 'E'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where vw.tsf_entity_id != I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id != I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh)));

   CURSOR C_LOC_ALL_LOCS IS
      select distinct s.store,I_item
        from item_loc_soh ils,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and ils.item           = I_item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = I_item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
        and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)))
      union all
      select  w.wh, I_item
        from  item_loc_soh ils,
              v_wh w
       where stockholding_ind = 'Y'
         and ils.item          = I_item
         and ils.loc           = w.wh
         and ils.loc_type      = 'W'
         and ils.stock_on_hand > 0
         and not exists (select 1
                          from mrt_item_loc mrl
                         where mrl.mrt_no   = I_mrt_no
                           and mrl.item     = I_item
                           and mrl.location = w.wh
                           and mrl.loc_type = 'W')
        and not exists (select 1
                          from mrt m
                         where m.mrt_no = I_mrt_no
                           and m.wh = w.wh)
        and (I_inter_intra_ind is NULL
            or (I_inter_intra_ind = 'A'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where L_intercompany_transfer_basis = 'T'
                               and vw.tsf_entity_id = I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id = I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh))
            or (I_inter_intra_ind = 'E'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where vw.tsf_entity_id != I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id != I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh)));

   CURSOR C_LOC_LIST_STORE IS
      select  distinct l.location , I_item
        from item_loc_soh ils,
             loc_list_detail l,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and l.location         = s.store
         and l.loc_list         = I_group_loc_value
         and l.loc_type         = 'S'
         and ils.item           = I_item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = I_item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
        and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_LOC_LIST_WH IS
      select distinct I_item, w.wh
        from item_loc_soh ils,
             loc_list_detail l,
             v_wh w
       where (l.location        = w.wh
              or w.physical_wh  = l.location)
         and w.stockholding_ind = 'Y'
         and l.loc_list         = I_group_loc_value
         and l.loc_type         = 'W'
         and ils.item          = I_item
         and ils.loc           = w.wh
         and ils.loc_type      = 'W'
         and ils.stock_on_hand > 0
         and not exists (select 1
                          from mrt_item_loc mrl
                         where mrl.mrt_no   = I_mrt_no
                           and mrl.item     = I_item
                           and mrl.location = w.wh
                           and mrl.loc_type = 'W')
        and not exists (select 1
                          from mrt m
                         where m.mrt_no = I_mrt_no
                           and m.wh = w.wh)
        and (I_inter_intra_ind is NULL
            or (I_inter_intra_ind = 'A'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where L_intercompany_transfer_basis = 'T'
                               and vw.tsf_entity_id = I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id = I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh))
            or (I_inter_intra_ind = 'E'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where vw.tsf_entity_id != I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id != I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh)));

   CURSOR C_LOC_TRANSFER_ZONE IS
      select distinct s.store ,I_item
        from item_loc_soh ils,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and transfer_zone      = I_group_loc_value
         and ils.item           = I_item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = I_item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_LOC_AREA IS
      select distinct s.store ,I_item
        from item_loc_soh ils,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and area               = I_group_loc_value
         and ils.item           = I_item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = I_item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_LOC_REGION IS
      select distinct s.store ,I_item
        from item_loc_soh ils,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and region             = I_group_loc_value
         and ils.item           = I_item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = I_item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_LOC_DISTRICT IS
      select distinct s.store ,I_item
        from item_loc_soh ils,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and district           = I_group_loc_value
         and ils.item           = I_item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = I_item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_LOC_LOCATION_TRAIT IS
      select distinct l.store ,I_item
        from item_loc_soh ils,
             loc_traits_matrix l,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and l.store            = s.store
         and l.loc_trait        = I_group_loc_value
         and ils.item           = I_item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = I_item
                            and mrl.location = l.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_LOC_STORE_CLASS IS
      select distinct s.store ,I_item
        from item_loc_soh ils,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and s.store_class      = I_group_loc_value
         and ils.item           = I_item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = I_item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_ITEM_LOC_STORE IS
      select distinct s.store,mi.item
        from item_loc_soh ils,
             mrt_item mi,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and s.store = I_group_loc_value
         and mi.mrt_no = I_mrt_no
         and mi.selected_ind = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item          = mi.item
         and ils.loc           = s.store
         and ils.loc_type      = 'S'
         and ils.stock_on_hand > 0
         and not exists (select 1
                          from mrt_item_loc mrl
                         where mrl.mrt_no   = I_mrt_no
                           and mrl.mrt_no   = mi.mrt_no
                           and mrl.item     = mi.item
                           and mrl.location = s.store
                           and mrl.loc_type = 'S');

   CURSOR C_ITEM_LOC_WH IS
      select  w.wh, mi.item
        from item_loc_soh ils,
             mrt_item mi,
             v_wh w
       where w.stockholding_ind = 'Y'
         and w.wh               = I_group_loc_value
         and mi.mrt_no          = I_mrt_no
         and mi.selected_ind    = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item           = mi.item
         and ils.loc            = w.wh
         and ils.loc_type       = 'W'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                          from mrt_item_loc mrl
                         where mrl.mrt_no   = I_mrt_no
                           and mrl.mrt_no   = mi.mrt_no
                           and mrl.item     = mi.item
                           and mrl.location = w.wh
                           and mrl.loc_type = 'W')
        and not exists (select 1
                          from mrt m
                         where m.mrt_no = I_mrt_no
                           and m.wh = w.wh);

   CURSOR C_ITEM_LOC_ALL_STORE IS
      select distinct s.store, mi.item
        from item_loc_soh ils,
             mrt_item mi,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and mi.mrt_no          = I_mrt_no
         and mi.selected_ind    = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item           = mi.item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.mrt_no   = mi.mrt_no
                            and mrl.item     = mi.item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_ITEM_LOC_ALL_WH IS
      select distinct mi.item, w.wh
        from item_loc_soh ils,
             mrt_item mi,
             v_wh w
       where stockholding_ind = 'Y'
         and mi.mrt_no = I_mrt_no
         and mi.selected_ind = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item          = mi.item
         and ils.loc           = w.wh
         and ils.loc_type      = 'W'
         and ils.stock_on_hand > 0
         and not exists (select 1
                          from mrt_item_loc mrl
                         where mrl.mrt_no   = I_mrt_no
                           and mrl.mrt_no   = mi.mrt_no
                           and mrl.item     = mi.item
                           and mrl.location = w.wh
                           and mrl.loc_type = 'W')
        and not exists (select 1
                          from mrt m
                         where m.mrt_no = I_mrt_no
                           and m.wh = w.wh)
        and (I_inter_intra_ind is NULL
            or (I_inter_intra_ind = 'A'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where L_intercompany_transfer_basis = 'T'
                               and vw.tsf_entity_id = I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id = I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh))
            or (I_inter_intra_ind = 'E'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where vw.tsf_entity_id != I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id != I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh)));

   CURSOR C_ITEM_LOC_ALL_LOCS IS
      select distinct s.store, mi.item
        from item_loc_soh ils,
             mrt_item mi,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and mi.mrt_no          = I_mrt_no
         and mi.selected_ind    = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item           = mi.item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.mrt_no   = mi.mrt_no
                            and mrl.item     = mi.item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)))
      union all
      select distinct w.wh,mi.item
        from item_loc_soh ils,
             mrt_item mi,
             v_wh w
       where w.stockholding_ind = 'Y'
         and mi.mrt_no          = I_mrt_no
         and mi.selected_ind    = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item           = mi.item
         and ils.loc            = w.wh
         and ils.loc_type       = 'W'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                          from mrt_item_loc mrl
                         where mrl.mrt_no   = I_mrt_no
                           and mrl.mrt_no   = mi.mrt_no
                           and mrl.item     = mi.item
                           and mrl.location = w.wh
                           and mrl.loc_type = 'W')
        and not exists (select 1
                          from mrt m
                         where m.mrt_no = I_mrt_no
                           and m.wh = w.wh)
        and (I_inter_intra_ind is NULL
            or (I_inter_intra_ind = 'A'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where L_intercompany_transfer_basis = 'T'
                               and vw.tsf_entity_id = I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id = I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh))
            or (I_inter_intra_ind = 'E'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where vw.tsf_entity_id != I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id != I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh)));

   CURSOR C_ITEM_LOC_LIST_STORE IS
      select distinct l.location, mi.item
        from item_loc_soh ils,
             loc_list_detail l,
             mrt_item mi,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and l.location         = s.store
         and l.loc_list         = I_group_loc_value
         and l.loc_type         = 'S'
         and mi.mrt_no          = I_mrt_no
         and mi.selected_ind = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item           = mi.item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = nvl(I_item, mrl.item)
                            and mrl.mrt_no   = mi.mrt_no
                            and mrl.item     = mi.item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_ITEM_LOC_LIST_WH IS
      select distinct mi.item, w.wh
        from item_loc_soh ils,
             loc_list_detail l,
             mrt_item mi,
             v_wh w
       where (l.location        = w.wh
              or w.physical_wh  = l.location)
         and l.loc_list         = I_group_loc_value
         and l.loc_type         = 'W'
         and w.stockholding_ind = 'Y'
         and mi.mrt_no = I_mrt_no
         and mi.selected_ind = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item          = mi.item
         and ils.loc           = w.wh
         and ils.loc_type      = 'W'
         and ils.stock_on_hand > 0
         and not exists (select 1
                          from mrt_item_loc mrl
                         where mrl.mrt_no   = I_mrt_no
                           and mrl.mrt_no   = mi.mrt_no
                           and mrl.item     = mi.item
                           and mrl.location = w.wh
                           and mrl.loc_type = 'W')
        and not exists (select 1
                          from mrt m
                         where m.mrt_no = I_mrt_no
                           and m.wh = w.wh)
        and (I_inter_intra_ind is NULL
            or (I_inter_intra_ind = 'A'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where L_intercompany_transfer_basis = 'T'
                               and vw.tsf_entity_id = I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id = I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh))
            or (I_inter_intra_ind = 'E'
                and exists (select 1
                              from v_transfer_from_wh vw
                             where vw.tsf_entity_id != I_tsf_entity_id
                               and w.wh = vw.wh
                             union all
                            select 1
                              from v_transfer_from_wh vw,
                                   tsf_entity_org_unit_sob teo
                             where L_intercompany_transfer_basis = 'B'
                               and teo.set_of_books_id != I_set_of_books_id
                               and teo.tsf_entity_id = vw.tsf_entity_id
                               and teo.org_unit_id = vw.org_unit_id
                               and w.wh = vw.wh)));

   CURSOR C_ITEM_LOC_TRANSFER_ZONE IS
      select distinct s.store, mi.item
        from item_loc_soh ils,
             mrt_item mi,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and s.transfer_zone    = I_group_loc_value
         and mi.mrt_no          = I_mrt_no
         and mi.selected_ind    = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item          = mi.item
         and ils.loc           = s.store
         and ils.loc_type      = 'S'
         and ils.stock_on_hand > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = nvl(I_item, mrl.item)
                            and mrl.mrt_no   = mi.mrt_no
                            and mrl.item     = mi.item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_ITEM_LOC_AREA IS
      select distinct s.store, mi.item
        from item_loc_soh ils,
             mrt_item mi,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and s.area             = I_group_loc_value
         and mi.mrt_no          = I_mrt_no
         and mi.selected_ind    = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item          = mi.item
         and ils.loc           = s.store
         and ils.loc_type      = 'S'
         and ils.stock_on_hand > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = nvl(I_item, mrl.item)
                            and mrl.mrt_no   = mi.mrt_no
                            and mrl.item     = mi.item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_ITEM_LOC_REGION IS
      select distinct s.store, mi.item
        from item_loc_soh ils,
             mrt_item mi,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and s.region           = I_group_loc_value
         and mi.mrt_no          = I_mrt_no
         and mi.selected_ind    = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item          = mi.item
         and ils.loc           = s.store
         and ils.loc_type      = 'S'
         and ils.stock_on_hand > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = nvl(I_item, mrl.item)
                            and mrl.mrt_no   = mi.mrt_no
                            and mrl.item     = mi.item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
          and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_ITEM_LOC_DISTRICT IS
      select distinct s.store, mi.item
        from item_loc_soh ils,
             mrt_item mi,
             v_store s
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and s.district         = I_group_loc_value
         and mi.mrt_no          = I_mrt_no
         and mi.selected_ind    = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item           = mi.item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = nvl(I_item, mrl.item)
                            and mrl.mrt_no   = mi.mrt_no
                            and mrl.item     = mi.item
                            and mrl.location = s.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_ITEM_LOC_LOCATION_TRAIT IS
      select distinct l.store, mi.item
        from item_loc_soh ils,
             loc_traits_matrix l,
             v_store s,
             mrt_item mi
       where s.stockholding_ind = 'Y'
         and s.store_type      != 'F'
         and l.store            = s.store
         and l.loc_trait        = I_group_loc_value
         and mi.mrt_no          = I_mrt_no
         and mi.selected_ind    = 'Y'
         and mi.item in (select item
                         from item_master
                        where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                          and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                          and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                          and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                          and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
         and ils.item           = mi.item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and not exists (select 1
                           from mrt_item_loc mrl
                          where mrl.mrt_no   = I_mrt_no
                            and mrl.item     = nvl(I_item, mrl.item)
                            and mrl.mrt_no   = mi.mrt_no
                            and mrl.item     = mi.item
                            and mrl.location = l.store
                            and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_ITEM_LOC_STORE_CLASS IS
   select distinct s.store, mi.item
     from item_loc_soh ils,
          mrt_item mi,
          v_store s
    where s.stockholding_ind = 'Y'
      and s.store_type      != 'F'
      and s.store_class      = I_group_loc_value
      and mi.mrt_no          = I_mrt_no
      and mi.selected_ind    = 'Y'
      and mi.item in (select item
                      from item_master
                     where NVL(item_parent, 'x') = NVL(I_item_parent, NVL(item_parent, 'x'))
                       and NVL(diff_1, 'x')      = NVL(I_diff_1, NVL(diff_1, 'x'))
                       and NVL(diff_2, 'x')      = NVL(I_diff_2, NVL(diff_2, 'x'))
                       and NVL(diff_3, 'x')      = NVL(I_diff_3, NVL(diff_3, 'x'))
                       and NVL(diff_4, 'x')      = NVL(I_diff_4, NVL(diff_4, 'x')))
      and ils.item           = mi.item
      and ils.loc            = s.store
      and ils.loc_type       = 'S'
      and ils.stock_on_hand  > 0
      and not exists (select 1
                        from mrt_item_loc mrl
                       where mrl.mrt_no   = I_mrt_no
                         and mrl.item     = nvl(I_item, mrl.item)
                         and mrl.mrt_no   = mi.mrt_no
                         and mrl.item     = mi.item
                         and mrl.location = s.store
                         and mrl.loc_type = 'S')
      and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));

   CURSOR C_RTV_INTRA_VALUES(I_item IN ITEM_MASTER.ITEM%TYPE, I_location IN ITEM_LOC.LOC%TYPE, I_supplier IN SUPS.SUPPLIER%TYPE) IS
   select NVL(av_cost,0), NVL(unit_retail,0), NVL(rtv_cost,0)
     from (select(select NVL(mri.rtv_cost,0)   --in MRT currency
                    from mrt_item mri,
                         mrt mr
                   where mr.mrt_no  = I_mrt_no
                     and mri.mrt_no = mr.mrt_no
                     and mri.item   = I_item) rtv_cost,
                  NVL(il.unit_retail,0) unit_retail,  --in location currency
                  NVL(ils.av_cost,0) av_cost   --in location currency
             from item_loc_soh ils,
                  item_loc il
            where ils.item = I_item
              and ils.loc  = I_location
              and ils.item = il.item
              and ils.loc  = il.loc);

   CURSOR C_RTV_INTER_VALUES(I_item IN ITEM_MASTER.ITEM%TYPE, I_location IN ITEM_LOC.LOC%TYPE, I_supplier IN SUPS.SUPPLIER%TYPE) IS
   select NVL(av_cost,0), NVL(unit_retail,0), NVL(rtv_cost,0)
     from (select(select NVL(mri.rtv_cost,0)   --in MRT currency
                    from mrt_item mri,
                         mrt mr
                   where mr.mrt_no  = I_mrt_no
                     and mri.mrt_no = mr.mrt_no
                     and mri.item   = I_item) rtv_cost,
                  NVL(il.unit_retail,0) unit_retail,  --in location currency
                  NVL(ils.av_cost,0) av_cost          --in location currency
             from item_loc_soh ils,
                  item_loc il
            where ils.item = I_item
              and ils.loc  = I_location
              and ils.item = il.item
              and ils.loc  = il.loc);

   cursor C_COUNT_MRT_ITEM is
      select count(item)
        from mrt_item
       where mrt_no = I_mrt_no;

   cursor C_GET_MRT_ITEM is
      select item
        from mrt_item
       where mrt_no = I_mrt_no;

   cursor C_STOCK_IND_S (I_loc ITEM_LOC_SOH.LOC%TYPE) is
      select stockholding_ind
        from v_store
       where store = I_loc;

   cursor C_STOCK_IND_W (I_loc ITEM_LOC_SOH.LOC%TYPE) is
      select stockholding_ind
        from v_wh
       where wh = I_loc;

   cursor C_STOCK_ON_HAND (I_loc ITEM_LOC_SOH.LOC%TYPE) is
      select ils.stock_on_hand
        from item_loc_soh ils,
             mrt_item mi
       where mi.mrt_no = I_mrt_no
         and ils.loc = I_loc
         and ((I_item is NULL and ils.item = mi.item)
              or (ils.item = I_item and mi.item = ils.item))
       order by ils.stock_on_hand desc;

   cursor C_LOC_LIST_S is
      select location
        from loc_list_detail
       where loc_list = I_group_loc_value
         and loc_type = 'S';

   cursor C_LOC_LIST_W is
      select location
        from loc_list_detail
       where loc_list = I_group_loc_value
         and loc_type = 'W';

   cursor C_TRANSFER_ZONE is
      select store
        from v_store
       where transfer_zone = I_group_loc_value;

   cursor C_AREA is
      select store
        from v_store
       where area = I_group_loc_value;

   cursor C_REGION is
      select store
        from v_store
       where region = I_group_loc_value;

   cursor C_DISTRICT is
      select store
        from v_store
       where district = I_group_loc_value;

   cursor C_LOC_TRAIT is
      select store
        from loc_traits_matrix
       where loc_trait = I_group_loc_value;

   cursor C_STORE_CLASS is
      select store
        from v_store
       where store_class = I_group_loc_value;

   cursor C_INV_LOC_ALL_STORE is
      select distinct s.stockholding_ind,
             ils.stock_on_hand
        from v_store s,
             item_loc_soh ils,
             mrt_item mi
       where ils.loc = s.store
         and ((I_item is NULL and ils.item = mi.item)
             or (ils.item = I_item and mi.item = ils.item))
         and mi.mrt_no = I_mrt_no
         and ils.loc_type = 'S'
         and NOT EXISTS (select 'x'
                           from mrt_item_loc mri
                          where mri.mrt_no = I_mrt_no
                            and mri.item = mi.item
                            and s.store = mri.location)
         and exists (select 1
                       from v_transfer_from_store vs
                      where L_intercompany_transfer_basis = 'T'
                        and vs.tsf_entity_id = I_tsf_entity_id
                        and s.store = vs.store
                      union all
                     select 1
                       from v_transfer_from_store vs,
                            tsf_entity_org_unit_sob teo
                      where L_intercompany_transfer_basis = 'B'
                        and teo.set_of_books_id = I_set_of_books_id
                        and teo.tsf_entity_id = vs.tsf_entity_id
                        and teo.org_unit_id = vs.org_unit_id
                        and vs.store = s.store);

   cursor C_INV_LOC_ALL_WH is
      select distinct w.stockholding_ind,
             ils.stock_on_hand
        from v_wh w,
             item_loc_soh ils,
             mrt_item mi
       where ils.loc = w.wh
         and ((I_item is NULL and ils.item = mi.item)
             or (ils.item = I_item and mi.item = ils.item))
         and mi.mrt_no = I_mrt_no
         and ils.loc_type = 'W'
         and NOT EXISTS (select 'x'
                           from mrt_item_loc mri
                          where mri.mrt_no = I_mrt_no
                            and mri.item = mi.item
                            and w.wh = mri.location);

   cursor C_INV_LOC_LLS (I_item   MRT_ITEM.ITEM%TYPE) is
      select 'x'
        from loc_list_detail l,
             v_store s
       where l.loc_list = I_group_loc_value
         and s.STORE = l.location
         and l.loc_type = 'S'
         and s.stockholding_ind = 'Y'
         and NOT EXISTS (select ils.loc
                           from item_loc_soh ils,
                                mrt_item mi
                          where mi.mrt_no = I_mrt_no
                            and mi.item = ils.item
                            and ils.item = I_item
                            and mi.item = ils.item
                            and ils.loc_type = l.loc_type
                            and ils.loc = l.location)
         and rownum = 1;

   cursor C_INV_LOC_LLW (I_item   MRT_ITEM.ITEM%TYPE) is
      select 'x'
        from loc_list_detail l,
             v_wh w
       where l.loc_list = I_group_loc_value
         and w.wh = l.location
         and l.loc_type = 'W'
         and w.stockholding_ind = 'Y'
         and NOT EXISTS (select ils.loc
                           from item_loc_soh ils,
                                mrt_item mi
                          where mi.mrt_no = I_mrt_no
                            and mi.item = ils.item
                            and ils.item = I_item
                            and mi.item = ils.item
                            and ils.loc_type = l.loc_type
                            and ils.loc = l.location)
         and rownum = 1;

   cursor C_INV_LOC_TRANSFER_ZONE (I_item   MRT_ITEM.ITEM%TYPE) is
      select 'x'
        from v_store s
       where s.transfer_zone = I_group_loc_value
         and s.stockholding_ind = 'Y'
         and NOT EXISTS (select ils.loc
                           from item_loc_soh ils,
                                mrt_item mi
                          where mi.mrt_no = I_mrt_no
                            and mi.item = ils.item
                            and ils.item = I_item
                            and mi.item = ils.item
                            and ils.loc_type = 'S'
                            and ils.loc = s.store)
         and rownum = 1;

   cursor C_INV_LOC_AREA (I_item   MRT_ITEM.ITEM%TYPE) is
      select 'x'
        from v_store s
       where s.area = I_group_loc_value
         and s.stockholding_ind = 'Y'
         and NOT EXISTS (select ils.loc
                           from item_loc_soh ils,
                                mrt_item mi
                          where mi.mrt_no = I_mrt_no
                            and mi.item = ils.item
                            and ils.item = I_item
                            and mi.item = ils.item
                            and ils.loc_type = 'S'
                            and ils.loc = s.store)
         and rownum = 1;

   cursor C_INV_LOC_REGION (I_item   MRT_ITEM.ITEM%TYPE) is
      select 'x'
        from v_store s
       where s.region = I_group_loc_value
         and s.stockholding_ind = 'Y'
         and NOT EXISTS (select ils.loc
                           from item_loc_soh ils,
                                mrt_item mi
                          where mi.mrt_no = I_mrt_no
                            and mi.item = ils.item
                            and ils.item = I_item
                            and mi.item = ils.item
                            and ils.loc_type = 'S'
                            and ils.loc = s.store)
         and rownum = 1;

   cursor C_INV_LOC_DISTRICT (I_item   MRT_ITEM.ITEM%TYPE) is
      select 'x'
        from v_store s
       where s.district = I_group_loc_value
         and s.stockholding_ind = 'Y'
         and NOT EXISTS (select ils.loc
                           from item_loc_soh ils,
                                mrt_item mi
                          where mi.mrt_no = I_mrt_no
                            and mi.item = ils.item
                            and ils.item = I_item
                            and mi.item = ils.item
                            and ils.loc_type = 'S'
                            and ils.loc = s.store)
         and rownum = 1;

   cursor C_INV_LOC_LOCTRAIT (I_item   MRT_ITEM.ITEM%TYPE) is
      select 'x'
        from loc_traits_matrix l,
             v_store s
       where l.loc_trait = I_group_loc_value
         and s.store = l.store
         and s.stockholding_ind = 'Y'
         and NOT EXISTS (select ils.loc
                           from item_loc_soh ils,
                                mrt_item mi
                          where mi.mrt_no = I_mrt_no
                            and mi.item = ils.item
                            and ils.item = I_item
                            and mi.item = ils.item
                            and ils.loc_type = 'S'
                            and ils.loc = l.STORE)
         and rownum = 1;

   cursor C_INV_LOC_STORE_CLASS (I_item   MRT_ITEM.ITEM%TYPE) is
      select 'x'
        from v_store s
       where s.store_class = I_group_loc_value
         and s.stockholding_ind = 'Y'
         and NOT EXISTS (select ils.loc
                           from item_loc_soh ils,
                                mrt_item mi
                          where mi.mrt_no = I_mrt_no
                            and mi.item = ils.item
                            and ils.item = I_item
                            and mi.item = ils.item
                            and ils.loc_type = 'S'
                            and ils.loc = s.store)
         and rownum = 1;

   cursor C_ITEM_LOC_RETURN_VALS IS
      select il.item,
             il.loc,
             ils.loc_type,
             lc.currency_code,
             0 return_avail_qty,
             0 tsf_qty,
             0 received_qty,
             nvl(il.unit_retail,0),
             nvl(ils.av_cost,0) unit_cost,
             0 tsf_cost,
             0 tsf_price,
             nvl(ils.av_cost,0) v_tsf_cost,
             nvl(ils.av_cost,0) v_tsf_price
        from item_loc il,
             TABLE(CAST(L_itemloc_table AS OBJ_ITEMLOC_TBL)) itl,
             item_loc_soh ils,
             (select wh loc, currency_code
                from wh
              UNION ALL
              select store loc, currency_code
                from store) lc
               where il.item  = itl.item
                 and il.loc   = itl.loc
                 and itl.loc  = ils.loc
                 and itl.item = ils.item
                 and ils.loc  = lc.loc;

   TYPE  LOC_TYPE_TAB IS TABLE OF TSFHEAD.FROM_LOC_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE  CURRENCY_CODE_TAB IS TABLE OF V_STORE.CURRENCY_CODE%TYPE INDEX BY BINARY_INTEGER;
   TYPE  RETURN_AVAIL_QTY_TAB IS TABLE OF MRT_ITEM_LOC.RETURN_AVAIL_QTY%TYPE INDEX BY BINARY_INTEGER;
   TYPE  TSF_QTY_TAB IS TABLE OF MRT_ITEM_LOC.TSF_QTY%TYPE INDEX BY BINARY_INTEGER;
   TYPE  RECEIVED_QTY_TAB IS TABLE OF MRT_ITEM_LOC.RECEIVED_QTY%TYPE INDEX BY BINARY_INTEGER;
   TYPE  UNIT_RETAIL_TAB IS TABLE OF MRT_ITEM_LOC.UNIT_RETAIL%TYPE INDEX BY BINARY_INTEGER;
   TYPE  UNIT_COST_TAB IS TABLE OF MRT_ITEM_LOC.UNIT_COST%TYPE INDEX BY BINARY_INTEGER;
   TYPE  TSF_COST_TAB IS TABLE OF MRT_ITEM_LOC.TSF_COST%TYPE INDEX BY BINARY_INTEGER;
   TYPE  TSF_PRICE_TAB IS TABLE OF MRT_ITEM_LOC.TSF_PRICE%TYPE INDEX BY BINARY_INTEGER;

   L_loc_type                       LOC_TYPE_TAB;
   L_currency_code                  CURRENCY_CODE_TAB;
   L_return_avail_qty               RETURN_AVAIL_QTY_TAB;
   L_tsf_qty                        TSF_QTY_TAB;
   L_received_qty                   RECEIVED_QTY_TAB;
   L_unit_retail                    UNIT_RETAIL_TAB;
   L_unit_cost                      UNIT_COST_TAB;
   L_tsf_cost                       TSF_COST_TAB;
   L_tsf_price                      TSF_PRICE_TAB;
   L_v_tsf_cost                     TSF_COST_TAB;
   L_v_tsf_price                    TSF_PRICE_TAB;

   L_mrt_type                       MRT.MRT_TYPE%TYPE;
   L_inventory_type                 MRT.INVENTORY_TYPE%TYPE;
   L_item_inv_status                INV_STATUS_QTY.INV_STATUS%TYPE;
   L_avail_qty                      MRT_ITEM_LOC.RETURN_AVAIL_QTY%TYPE;
   L_mrt_currency_code              MRT.CURRENCY_CODE%TYPE;
   L_supplier                       MRT.SUPPLIER%TYPE;
   L_inv_status                     MRT.INVENTORY_TYPE%TYPE;

   L_stockholding_ind               V_STORE.STOCKHOLDING_IND%TYPE := NULL;
   L_stock_on_hand                  VARCHAR2(1);
   L_get_item                       MRT_ITEM.ITEM%TYPE;
   L_cnt_mrt_item_loc               NUMBER := 0;
   L_cnt_mrt_item                   NUMBER := 0;
   L_loc                            ITEM_LOC_SOH.LOC%TYPE;
   L_stock_ind                      VARCHAR2(1);
   L_inv_loc                        VARCHAR2(1);

   L_program   VARCHAR(64)    := 'MRT_SQL.CREATE_MRT_ITEM_LOC';
   L_table     VARCHAR(20)    := NULL;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

BEGIN
   /* make sure required parameters are populated */
   if I_item_apply_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item_apply_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_group_loc_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_group_loc_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_group_loc_type in ('S','W') then
      if I_group_loc_value is NULL then
         O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              I_group_loc_value,
                                              L_program,
                                              NULL);
         return FALSE;
      end if;
   end if;

   if I_mrt_no is NULL then
   O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                        'I_mrt_no',
                                        L_program,
                                        NULL);
      return FALSE;
   end if;

   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;
   L_intercompany_transfer_basis := L_system_options_rec.intercompany_transfer_basis;
   ---


   SQL_LIB.SET_MARK('OPEN','C_MRT_INFO', 'MRT' ,'Mrt: '||(I_mrt_no));
   open C_MRT_INFO;
   SQL_LIB.SET_MARK('FETCH','C_MRT_INFO', 'MRT' ,'Mrt: '||(I_mrt_no));
   fetch C_MRT_INFO into L_supplier, L_mrt_type, L_mrt_currency_code, L_inventory_type;
   SQL_LIB.SET_MARK('CLOSE','C_MRT_INFO', 'MRT' ,'Mrt: '||(I_mrt_no));
   close C_MRT_INFO;

   if I_item_apply_type = 'S' then

      if I_group_loc_type = 'S' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_STORE;
         SQL_LIB.SET_MARK('FETCH','C_LOC_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_STORE BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_LOC_STORE;

      elsif I_group_loc_type = 'W' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_WH;
         SQL_LIB.SET_MARK('FETCH','C_LOC_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_WH BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         close C_LOC_WH;

      elsif I_group_loc_type = 'AS' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_ALL_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_ALL_STORE;
         SQL_LIB.SET_MARK('FETCH','C_LOC_ALL_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_ALL_STORE BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_ALL_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_LOC_ALL_STORE;

      elsif I_group_loc_type = 'AW' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_ALL_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_ALL_WH;
         SQL_LIB.SET_MARK('FETCH','C_LOC_ALL_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_ALL_WH BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_ALL_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         close C_LOC_ALL_WH;

      elsif I_group_loc_type = 'AL' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_ALL_LOCS', 'V_STORE and V_WH' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_ALL_LOCS;
         SQL_LIB.SET_MARK('OPEN','C_LOC_ALL_LOCS', 'V_STORE and V_WH' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_ALL_LOCS BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('OPEN','C_LOC_ALL_LOCS', 'V_STORE and V_WH' ,'Mrt: '||(I_mrt_no));
         close C_LOC_ALL_LOCS;

      elsif I_group_loc_type = 'LLS' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_LIST_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_LIST_STORE;
         SQL_LIB.SET_MARK('FETCH','C_LOC_LIST_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_LIST_STORE BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_LIST_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_LOC_LIST_STORE;

      elsif I_group_loc_type = 'LLW' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_LIST_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_LIST_WH;
         SQL_LIB.SET_MARK('FETCH','C_LOC_LIST_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_LIST_WH BULK COLLECT into L_item, L_location;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_LIST_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         close C_LOC_LIST_WH;

      elsif I_group_loc_type = 'T' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_TRANSFER_ZONE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_TRANSFER_ZONE;
         SQL_LIB.SET_MARK('FETCH','C_LOC_TRANSFER_ZONE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_TRANSFER_ZONE BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_TRANSFER_ZONE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_LOC_TRANSFER_ZONE;

      elsif I_group_loc_type = 'A' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_AREA', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_AREA;
         SQL_LIB.SET_MARK('FETCH','C_LOC_AREA', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_AREA BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_AREA', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_LOC_AREA;

      elsif I_group_loc_type = 'R' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_REGION', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_REGION;
         SQL_LIB.SET_MARK('FETCH','C_LOC_REGION', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_REGION BULK COLLECT into L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_REGION', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_LOC_REGION;

      elsif I_group_loc_type = 'D' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_DISTRICT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_DISTRICT;
         SQL_LIB.SET_MARK('FETCH','C_LOC_DISTRICT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_DISTRICT BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_DISTRICT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_LOC_DISTRICT;

      elsif I_group_loc_type = 'L' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_LOCATION_TRAIT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_LOCATION_TRAIT;
         SQL_LIB.SET_MARK('FETCH','C_LOC_LOCATION_TRAIT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_LOCATION_TRAIT BULK COLLECT into L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_LOC_LOCATION_TRAIT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_LOC_LOCATION_TRAIT;

      elsif I_group_loc_type = 'C' then
         SQL_LIB.SET_MARK('OPEN','C_LOC_STORE_CLASS', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_LOC_STORE_CLASS;
         SQL_LIB.SET_MARK('OPEN','C_LOC_STORE_CLASS', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_LOC_STORE_CLASS BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('OPEN','C_LOC_STORE_CLASS', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_LOC_STORE_CLASS;
      end if;

   else

      if I_group_loc_type = 'S' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_STORE;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_STORE BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_STORE;

      elsif I_group_loc_type = 'W' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_WH;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_WH BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_WH;

      elsif I_group_loc_type = 'AS' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_ALL_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_ALL_STORE;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_ALL_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_ALL_STORE BULK COLLECT into L_location,L_item;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_ALL_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_ALL_STORE;

      elsif I_group_loc_type = 'AW' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_ALL_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_ALL_WH;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_ALL_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_ALL_WH BULK COLLECT into L_item, L_location;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_ALL_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_ALL_WH;

      elsif I_group_loc_type = 'AL' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_ALL_LOCS', 'V_STORE and V_WH' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_ALL_LOCS;
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_ALL_LOCS', 'V_STORE and V_WH' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_ALL_LOCS BULK COLLECT into L_location,L_item;
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_ALL_LOCS', 'V_STORE and V_WH' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_ALL_LOCS;

      elsif I_group_loc_type = 'LLS' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_LIST_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_LIST_STORE;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_LIST_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_LIST_STORE BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_LIST_STORE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_LIST_STORE;

      elsif I_group_loc_type = 'LLW' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_LIST_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_LIST_WH;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_LIST_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_LIST_WH BULK COLLECT into L_item, L_location;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_LIST_WH', 'V_WH' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_LIST_WH;

      elsif I_group_loc_type = 'T' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_TRANSFER_ZONE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_TRANSFER_ZONE;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_TRANSFER_ZONE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_TRANSFER_ZONE BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_TRANSFER_ZONE', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_TRANSFER_ZONE;

      elsif I_group_loc_type = 'A' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_AREA', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_AREA;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_AREA', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_AREA BULK COLLECT into L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_AREA', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_AREA;

      elsif I_group_loc_type = 'R' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_REGION', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_REGION;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_REGION', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_REGION BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_REGION', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_REGION;

      elsif I_group_loc_type = 'D' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_DISTRICT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_DISTRICT;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_DISTRICT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_DISTRICT BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_DISTRICT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_DISTRICT;

      elsif I_group_loc_type = 'L' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_LOCATION_TRAIT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_LOCATION_TRAIT;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_LOCATION_TRAIT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_LOCATION_TRAIT BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_LOCATION_TRAIT', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_LOCATION_TRAIT;

      elsif I_group_loc_type = 'C' then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_STORE_CLASS', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         open  C_ITEM_LOC_STORE_CLASS;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_STORE_CLASS', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         fetch C_ITEM_LOC_STORE_CLASS BULK COLLECT into  L_location, L_item;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_STORE_CLASS', 'V_STORE' ,'Mrt: '||(I_mrt_no));
         close C_ITEM_LOC_STORE_CLASS;
      end if;
   end if;

   if L_location is NULL or L_location.count = 0 then
      -- One or more locations were not applied since the stock on hand of the item is zero.
      O_soh_exists := TRUE;
      O_stockholding_exists := FALSE;
      return TRUE;
   end if;
   --
   FOR L_loop_index IN 1..L_item.COUNT LOOP
       L_itemloc_table.EXTEND;
       L_itemloc_table(L_itemloc_table.COUNT) := OBJ_ITEMLOC_REC(L_item(L_loop_index),
                                                                 L_location(L_loop_index));
   END LOOP;
   --
   SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_RETURN_VALS', 'ITEM_LOC_SOH' ,'Mrt: '||(I_mrt_no));
   open C_ITEM_LOC_RETURN_VALS;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_RETURN_VALS', 'ITEM_LOC_SOH' ,'Mrt: '||(I_mrt_no));
   fetch C_ITEM_LOC_RETURN_VALS BULK COLLECT into L_item,
                                                  L_location,
                                                  L_loc_type,
                                                  L_currency_code,
                                                  L_return_avail_qty,
                                                  L_tsf_qty,
                                                  L_received_qty,
                                                  L_unit_retail,  --in loc currency
                                                  L_unit_cost,    --in loc currency
                                                  L_tsf_cost,     --0
                                                  L_tsf_price,    --0
                                                  L_v_tsf_cost,   --in loc currency
                                                  L_v_tsf_price;  --in loc currency 
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_RETURN_VALS', 'ITEM_LOC_SOH' ,'Mrt: '||(I_mrt_no));
   close C_ITEM_LOC_RETURN_VALS;

   --
   FOR L_loop_index IN 1..L_item.COUNT LOOP

      if I_inv_status = 'A' then
         L_inv_status := NULL;
      else
         L_inv_status := I_inv_status;
      end if;

      if TRANSFER_SQL.QUANTITY_AVAIL(  O_error_message,
                                       L_return_avail_qty(L_loop_index),
                                       L_item(L_loop_index),
                                       L_inv_status,
                                       L_loc_type(L_loop_index),
                                       L_location(L_loop_index)
                                    ) = FALSE then
         return FALSE;
      end if;
      if L_return_avail_qty(L_loop_index) < 0 then
         L_return_avail_qty(L_loop_index) := 0;
      end if;
      if NVL(I_tsf_qty,0) > 0 then
         L_tsf_qty(L_loop_index) := LEAST(L_return_avail_qty(L_loop_index),I_tsf_qty);
      else
         L_tsf_qty(L_loop_index) := L_return_avail_qty(L_loop_index);
      end if;

      if L_mrt_type = 'A' then -- cost based
         if L_supplier is NULL then
            L_tsf_cost(L_loop_index)    := L_v_tsf_cost(L_loop_index);    --this is in local currency
         else
            --when fetched from C_RTV_INTRA_VALUES, L_tsf_cost is in MRT currency, L_unit_retail and L_unit_cost are in local currency
            SQL_LIB.SET_MARK('OPEN','C_RTV_INTRA_VALUES', 'MRT_ITEM' ,'Mrt: '||(I_mrt_no) || ' Item: ' || (L_item(L_loop_index)) || ' Loc: ' || (L_location(L_loop_index)));
            open C_RTV_INTRA_VALUES(L_item(L_loop_index), L_location(L_loop_index), L_supplier);
            SQL_LIB.SET_MARK('FETCH','C_RTV_INTRA_VALUES', 'MRT_ITEM' ,'Mrt: '||(I_mrt_no) || ' Item: ' || (L_item(L_loop_index)) || ' Loc: ' || (L_location(L_loop_index)));
            fetch C_RTV_INTRA_VALUES into L_unit_cost(L_loop_index), L_unit_retail(L_loop_index), L_tsf_cost(L_loop_index);
            SQL_LIB.SET_MARK('CLOSE','C_RTV_INTRA_VALUES', 'MRT_ITEM' ,'Mrt: '||(I_mrt_no) || ' Item: ' || (L_item(L_loop_index)) || ' Loc: ' || (L_location(L_loop_index)));
            close C_RTV_INTRA_VALUES;
         end if;
      elsif L_mrt_type = 'E' then --price based
         if L_supplier is NULL then
            L_tsf_price(L_loop_index)    := L_v_tsf_price(L_loop_index);   --this is in local currency
         else
            --when fetched from C_RTV_INTER_VALUES, L_tsf_price is in MRT currency, L_unit_retail and L_unit_cost are in local currency
            SQL_LIB.SET_MARK('OPEN','C_RTV_INTER_VALUES', 'MRT_ITEM' ,'Mrt: '||(I_mrt_no) || ' Item: ' || (L_item(L_loop_index)) || ' Loc: ' || (L_location(L_loop_index)));
            open C_RTV_INTER_VALUES(L_item(L_loop_index), L_location(L_loop_index), L_supplier);
            SQL_LIB.SET_MARK('FETCH','C_RTV_INTER_VALUES', 'MRT_ITEM' ,'Mrt: '||(I_mrt_no) || ' Item: ' || (L_item(L_loop_index)) || ' Loc: ' || (L_location(L_loop_index)));
            fetch C_RTV_INTER_VALUES into L_unit_cost(L_loop_index), L_unit_retail(L_loop_index), L_tsf_price(L_loop_index);
            SQL_LIB.SET_MARK('CLOSE','C_RTV_INTER_VALUES', 'MRT_ITEM' ,'Mrt: '||(I_mrt_no) || ' Item: ' || (L_item(L_loop_index)) || ' Loc: ' || (L_location(L_loop_index)));
            close C_RTV_INTER_VALUES;
         end if;
      end if;

      --L_unit_cost is in local currency at this point, convert to MRT currency
      if L_unit_cost(L_loop_index) > 0 then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_unit_cost(L_loop_index),
                                 L_currency_code(L_loop_index),
                                 L_mrt_currency_code,
                                 L_unit_cost(L_loop_index),
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;

      --L_unit_retail is in local currency at this point, convert to MRT currency
      if L_unit_retail(L_loop_index) > 0 then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_unit_retail(L_loop_index),
                                 L_currency_code(L_loop_index),
                                 L_mrt_currency_code,
                                 L_unit_retail(L_loop_index),
                                 'R',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;

      --If a tsf_price or tsf_cost passed in is > 0 use this in the insert
      --but dont convert as its been passed already in mrt currency
      if I_tsf_cost is not NULL then
         if I_tsf_cost > 0 then
            L_tsf_cost(L_loop_index) := I_tsf_cost;
         end if;
      else
         if L_supplier is NULL then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_tsf_cost(L_loop_index),
                                    L_currency_code(L_loop_index),
                                    L_mrt_currency_code,
                                    L_tsf_cost(L_loop_index),
                                    'C',
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
                  return FALSE;
            end if;
         end if;
      end if;
      if I_tsf_price is not NULL then
         if I_tsf_price > 0 then
            L_tsf_price(L_loop_index) := I_tsf_price;
         end if;
      else
         if L_tsf_price(L_loop_index) > 0 then
            if L_supplier is NULL then
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_tsf_price(L_loop_index),
                                       L_currency_code(L_loop_index),
                                       L_mrt_currency_code,
                                       L_tsf_price(L_loop_index),
                                       'R',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
      end if;
   END LOOP;

   --unit_retail, unit_cost, tsf_cost, tsf_price are all in MRT currency when written to MRT_ITEM_LOC
   FORALL i IN 1..L_item.COUNT
      insert into mrt_item_loc ( mrt_no,
                                 item,
                                 location,
                                 loc_type,
                                 return_avail_qty,
                                 tsf_qty,
                                 received_qty,
                                 unit_retail,
                                 unit_cost,
                                 tsf_cost,
                                 tsf_price)
                         values( I_mrt_no,
                                 L_item(i),
                                 L_location(i),
                                 L_loc_type(i),
                                 L_return_avail_qty(i),
                                 L_tsf_qty(i),
                                 L_received_qty(i),
                                 L_unit_retail(i),
                                 L_unit_cost(i),
                                 L_tsf_cost(i),
                                 L_tsf_price(i));

   --Now delete any rows that were inserted with available qty
   --Less than or equal to zero
   FORALL i IN 1..L_item.COUNT
      delete mrt_item_loc
       where mrt_no   = I_mrt_no
         and item     = L_item(i)
         and location = L_location(i)
         and loc_type = L_loc_type(i)
         and L_return_avail_qty(i) <= 0;

   if I_group_loc_type = 'S' or I_group_loc_type = 'W' then
      if I_item is NULL then
         -- APPLY ALL button pressed
         SQL_LIB.SET_MARK('OPEN',
                          'C_COUNT_MRT_ITEM',
                          'MRT_ITEM',
                          'Mrt: '||(I_mrt_no));
         open C_COUNT_MRT_ITEM;

         SQL_LIB.SET_MARK('FETCH',
                          'C_COUNT_MRT_ITEM',
                          'MRT_ITEM',
                          'Mrt: '||(I_mrt_no));
         fetch C_COUNT_MRT_ITEM into L_cnt_mrt_item;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_COUNT_MRT_ITEM',
                          'MRT_ITEM',
                          'Mrt: '||(I_mrt_no));
         close C_COUNT_MRT_ITEM;

         L_loc := I_group_loc_value;

         FOR rec IN C_STOCK_ON_HAND(L_loc) LOOP
            if rec.stock_on_hand > 0 then
               L_cnt_mrt_item_loc := L_cnt_mrt_item_loc + 1; -- Count the items inserted in MRT_ITEM_LOC
            end if;
         END LOOP;

         -- compare the record count of MRT_ITEM and the inserted record in MRT_ITEM_LOC
         if L_cnt_mrt_item != L_cnt_mrt_item_loc then
            L_stock_on_hand := 'Y';--Some locations was not applied
         end if;

      else
         -- Appropriate warning message is handled in the form if involving single mrt_item
         O_location_exists := FALSE;
      end if;
   elsif I_group_loc_type = 'AS' then
      FOR rec IN C_INV_LOC_ALL_STORE LOOP
         if (rec.stockholding_ind = 'N') then
            L_stockholding_ind := 'N';
         elsif (rec.stockholding_ind = 'Y' and rec.stock_on_hand <= 0) then
            L_stock_on_hand := 'Y';  -- zero or negative SOH
         elsif (rec.stockholding_ind = 'Y' and rec.stock_on_hand > 0) then
            L_cnt_mrt_item_loc := 1; -- Item is valid
         end if;

         if (L_stockholding_ind = 'N' and L_stock_on_hand = 'Y') then
            EXIT; -- Exit loop if both flags were set
         end if;
      END LOOP;
   elsif I_group_loc_type = 'AW' then
      FOR rec IN C_INV_LOC_ALL_WH LOOP
         if (rec.stockholding_ind = 'N') then
            L_stockholding_ind := 'N';
         elsif (rec.stockholding_ind = 'Y' and rec.stock_on_hand <= 0) then
            L_stock_on_hand := 'Y';  -- zero or negative SOH
         elsif (rec.stockholding_ind = 'Y' and rec.stock_on_hand > 0) then
            L_cnt_mrt_item_loc := 1; -- item is valid
         end if;

         if (L_stockholding_ind = 'N' and L_stock_on_hand = 'Y') then
            EXIT; -- Exit loop when all flags are set
         end if;
      END LOOP;
   elsif I_group_loc_type = 'LLS' then
      -- Check stockholding warehouse in LOC_LIST_DETAIL table that doesn't exist in
      -- ITEM_LOC_SOH table for an item included in the MRT
      if I_item is NOT NULL then
         L_get_item := I_item;
         SQL_LIB.SET_MARK('OPEN',
                          'C_INV_LOC_LLS',
                          'LOC_LIST_DETAIL'||' V_STORE',
                          'Loc List: '||(I_group_loc_value));
         open C_INV_LOC_LLS (L_get_item);

         SQL_LIB.SET_MARK('FETCH',
                          'C_INV_LOC_LLS',
                          'LOC_LIST_DETAIL'||' V_STORE',
                          'Loc List: '||(I_group_loc_value));
         fetch C_INV_LOC_LLS into L_inv_loc;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_INV_LOC_LLS',
                          'LOC_LIST_DETAIL'||' V_STORE',
                          'Loc List: '||(I_group_loc_value));
         close C_INV_LOC_LLS;

         if L_inv_loc is NOT NULL then
            L_stock_on_hand := 'Y';
         end if;

      else
         FOR rec in C_GET_MRT_ITEM LOOP
            L_get_item := rec.item;

            SQL_LIB.SET_MARK('OPEN',
                             'C_INV_LOC_LLS',
                             'LOC_LIST_DETAIL'||' V_STORE',
                             'Loc List: '||(I_group_loc_value));
            open C_INV_LOC_LLS (L_get_item);

            SQL_LIB.SET_MARK('FETCH',
                             'C_INV_LOC_LLS',
                             'LOC_LIST_DETAIL'||' V_STORE',
                             'Loc List: '||(I_group_loc_value));
            fetch C_INV_LOC_LLS into L_inv_loc;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_INV_LOC_LLS',
                             'LOC_LIST_DETAIL'||' V_STORE',
                             'Loc List: '||(I_group_loc_value));
            close C_INV_LOC_LLS;

            if L_inv_loc is NOT NULL then
               L_stock_on_hand := 'Y';
            end if;

            if L_stock_on_hand  = 'Y' then
               EXIT;
            end if;
         END LOOP;
      end if;

      FOR rec IN C_LOC_LIST_S LOOP
         L_loc := rec.location;

         SQL_LIB.SET_MARK('OPEN',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         open C_STOCK_IND_S (L_loc);

         SQL_LIB.SET_MARK('FETCH',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         fetch C_STOCK_IND_S into L_stock_ind;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         close C_STOCK_IND_S;

         if L_stock_ind = 'N' then
            L_stockholding_ind := 'N';
         else
            FOR rec IN C_STOCK_ON_HAND(L_loc) LOOP
               if rec.stock_on_hand <= 0 then
                  L_stock_on_hand := 'Y';  -- zero or negative SOH
               elsif rec.stock_on_hand > 0 then
                  L_cnt_mrt_item_loc := 1; -- Item is valid
               end if;
            END LOOP;
         end if;

         if (L_stockholding_ind = 'N' and L_stock_on_hand = 'Y') then
            EXIT; -- exit loop when all flags are set
         end if;

         L_loc := NULL; -- next location
      END LOOP;
   elsif I_group_loc_type = 'LLW' then
      -- Check stockholding warehouse in LOC_LIST_DETAIL table that doesn't exist in
      -- ITEM_LOC_SOH table for an item included in the MRT
      if I_item is NOT NULL then
         L_get_item := I_item;
         SQL_LIB.SET_MARK('OPEN',
                          'C_INV_LOC_LLW',
                          'LOC_LIST_DETAIL'||'V_WH',
                          'Loc List: '||(I_group_loc_value));
         open C_INV_LOC_LLW (L_get_item);

         SQL_LIB.SET_MARK('FETCH',
                          'C_INV_LOC_LLW',
                          'LOC_LIST_DETAIL'||'V_WH',
                          'Loc List: '||(I_group_loc_value));
         fetch C_INV_LOC_LLW into L_inv_loc;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_INV_LOC_LLW',
                          'LOC_LIST_DETAIL'||'V_WH',
                          'Loc List: '||(I_group_loc_value));
         close C_INV_LOC_LLW;

         if L_inv_loc is NOT NULL then
            L_stock_on_hand := 'Y';
         end if;

      else
         FOR rec in C_GET_MRT_ITEM LOOP
            L_get_item := rec.item;

            SQL_LIB.SET_MARK('OPEN',
                             'C_INV_LOC_LLW',
                             'LOC_LIST_DETAIL'||'V_WH',
                             'Loc List: '||(I_group_loc_value));
            open C_INV_LOC_LLW (L_get_item);

            SQL_LIB.SET_MARK('FETCH',
                             'C_INV_LOC_LLW',
                             'LOC_LIST_DETAIL'||'V_WH',
                             'Loc List: '||(I_group_loc_value));
            fetch C_INV_LOC_LLW into L_inv_loc;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_INV_LOC_LLW',
                             'LOC_LIST_DETAIL'||'V_WH',
                             'Loc List: '||(I_group_loc_value));
            close C_INV_LOC_LLW;

            if L_inv_loc is NOT NULL then
               L_stock_on_hand := 'Y';
            end if;

            if L_stock_on_hand  = 'Y' then
               EXIT;
            end if;
         END LOOP;
      end if;/*End of checking of invalid locations 'Y' per item*/

      FOR rec IN C_LOC_LIST_W LOOP
         L_loc := rec.location;
         SQL_LIB.SET_MARK('OPEN',
                          'C_STOCK_IND_W',
                          'V_WH',
                          'wh: '||(L_loc));
         open C_STOCK_IND_W (L_loc);

         SQL_LIB.SET_MARK('FETCH',
                          'C_STOCK_IND_W',
                          'V_WH',
                          'wh: '||(L_loc));
         fetch C_STOCK_IND_W into L_stock_ind;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_STOCK_IND_W',
                          'V_WH',
                          'wh: '||(L_loc));
         close C_STOCK_IND_W;

         if L_stock_ind = 'N' then
            L_stockholding_ind := 'N';
         else
            FOR rec IN C_STOCK_ON_HAND(L_loc) LOOP
               if rec.stock_on_hand <= 0 then
                  L_stock_on_hand := 'Y';  -- zero or negative SOH
               elsif rec.stock_on_hand > 0 then
                  L_cnt_mrt_item_loc := 1; -- Item is valid
               end if;
            END LOOP;
         end if;

         if (L_stockholding_ind = 'N' and L_stock_on_hand = 'Y') then
            EXIT; -- exit loop when all flags are set
         end if;

         L_loc := NULL; -- next location
      END LOOP;
   elsif I_group_loc_type = 'T' then
      -- Check stockholding store belonging to the TRANSFER_ZONE that doesn't exist in
      -- ITEM_LOC_SOH table for an item included in the MRT
      if I_item is NOT NULL then
         L_get_item := I_item;
         SQL_LIB.SET_MARK('OPEN',
                          'C_INV_LOC_TRANSFER_ZONE',
                          'V_STORE',
                          'Transfer Zone: '||(I_group_loc_value));
         open C_INV_LOC_TRANSFER_ZONE (L_get_item);

         SQL_LIB.SET_MARK('FETCH',
                          'C_INV_LOC_TRANSFER_ZONE',
                          'V_STORE',
                          'Transfer Zone: '||(I_group_loc_value));
         fetch C_INV_LOC_TRANSFER_ZONE into L_inv_loc;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_INV_LOC_TRANSFER_ZONE',
                          'V_STORE',
                          'Transfer Zone: '||(I_group_loc_value));
         close C_INV_LOC_TRANSFER_ZONE;

         if L_inv_loc is NOT NULL then
            L_stock_on_hand := 'Y';
         end if;

      else
         FOR rec in C_GET_MRT_ITEM LOOP
            L_get_item := rec.item;

            SQL_LIB.SET_MARK('OPEN',
                             'C_INV_LOC_TRANSFER_ZONE',
                             'V_STORE',
                             'Transfer Zone: '||(I_group_loc_value));
            open C_INV_LOC_TRANSFER_ZONE (L_get_item);

            SQL_LIB.SET_MARK('FETCH',
                             'C_INV_LOC_TRANSFER_ZONE',
                             'V_STORE',
                             'Transfer Zone: '||(I_group_loc_value));
            fetch C_INV_LOC_TRANSFER_ZONE into L_inv_loc;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_INV_LOC_TRANSFER_ZONE',
                             'V_STORE',
                             'Transfer Zone: '||(I_group_loc_value));
            close C_INV_LOC_TRANSFER_ZONE;

            if L_inv_loc is NOT NULL then
               L_stock_on_hand := 'Y';
            end if;

            if L_stock_on_hand  = 'Y' then
               EXIT;
            end if;
         END LOOP;
      end if;/*End of checking of invalid locations 'Y' per item*/

      FOR rec IN C_TRANSFER_ZONE LOOP
         L_loc := rec.store;
         SQL_LIB.SET_MARK('OPEN',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         open C_STOCK_IND_S (L_loc);

         SQL_LIB.SET_MARK('FETCH',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         fetch C_STOCK_IND_S into L_stock_ind;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         close C_STOCK_IND_S;

         if L_stock_ind = 'N' then
            L_stockholding_ind := 'N';
         else
            FOR rec IN C_STOCK_ON_HAND(L_loc) LOOP
               if rec.stock_on_hand <= 0 then
                  L_stock_on_hand := 'Y';  -- zero or negative SOH
               elsif rec.stock_on_hand > 0 then
                  L_cnt_mrt_item_loc := 1; -- Item is valid
               end if;
            END LOOP;
         end if;

         if (L_stockholding_ind = 'N' and L_stock_on_hand = 'Y') then
            EXIT; -- exit loop when all flags are set
         end if;

         L_loc := NULL; -- next location
      END LOOP;
   elsif I_group_loc_type = 'A' then
      -- Check stockholding store belonging to the AREA that doesn't exist in
      -- ITEM_LOC_SOH table for an item included in the MRT
      if I_item is NOT NULL then
         L_get_item := I_item;
         SQL_LIB.SET_MARK('OPEN',
                          'C_INV_LOC_AREA',
                          'V_STORE',
                          'Area: '||(I_group_loc_value));
         open C_INV_LOC_AREA (L_get_item);

         SQL_LIB.SET_MARK('FETCH',
                          'C_INV_LOC_AREA',
                          'V_STORE',
                          'Area: '||(I_group_loc_value));
         fetch C_INV_LOC_AREA into L_inv_loc;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_INV_LOC_AREA',
                          'V_STORE',
                          'Area: '||(I_group_loc_value));
         close C_INV_LOC_AREA;

         if L_inv_loc is NOT NULL then
            L_stock_on_hand := 'Y';
         end if;

      else
         FOR rec in C_GET_MRT_ITEM LOOP
            L_get_item := rec.item;

            SQL_LIB.SET_MARK('OPEN',
                             'C_INV_LOC_AREA',
                             'V_STORE',
                             'Area: '||(I_group_loc_value));
            open C_INV_LOC_AREA (L_get_item);

            SQL_LIB.SET_MARK('FETCH',
                             'C_INV_LOC_AREA',
                             'V_STORE',
                             'Area: '||(I_group_loc_value));
            fetch C_INV_LOC_AREA into L_inv_loc;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_INV_LOC_AREA',
                             'V_STORE',
                             'Area: '||(I_group_loc_value));
            close C_INV_LOC_AREA;

            if L_inv_loc is NOT NULL then
               L_stock_on_hand := 'Y';
            end if;

            if L_stock_on_hand  = 'Y' then
               EXIT;
            end if;
         END LOOP;
      end if;/*End of checking of invalid locations 'Y' per item*/

      FOR rec IN C_AREA LOOP
         L_loc := rec.store;

         SQL_LIB.SET_MARK('OPEN',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         open C_STOCK_IND_S (L_loc);

         SQL_LIB.SET_MARK('FETCH',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         fetch C_STOCK_IND_S into L_stock_ind;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         close C_STOCK_IND_S;

         if L_stock_ind = 'N' then
            L_stockholding_ind := 'N';
         else
            FOR rec IN C_STOCK_ON_HAND(L_loc) LOOP
               if rec.stock_on_hand <= 0 then
                  L_stock_on_hand := 'Y';  -- zero or negative SOH
               elsif rec.stock_on_hand > 0 then
                  L_cnt_mrt_item_loc := 1; -- Item is valid
               end if;
            END LOOP;
         end if;

         if (L_stockholding_ind = 'N' and L_stock_on_hand = 'Y') then
            EXIT; -- exit loop when all flags are set
         end if;

         L_loc := NULL; -- next location
      END LOOP;
   elsif I_group_loc_type = 'R' then
      -- Check stockholding store belonging to the REGION that doesn't exist in
      -- ITEM_LOC_SOH table for an item included in the MRT
      if I_item is NOT NULL then
         L_get_item := I_item;
         SQL_LIB.SET_MARK('OPEN',
                          'C_INV_LOC_REGION',
                          'V_STORE',
                          'Region: '||(I_group_loc_value));
         open C_INV_LOC_REGION (L_get_item);

         SQL_LIB.SET_MARK('FETCH',
                          'C_INV_LOC_REGION',
                          'V_STORE',
                          'Region: '||(I_group_loc_value));
         fetch C_INV_LOC_REGION into L_inv_loc;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_INV_LOC_REGION',
                          'V_STORE',
                          'Region: '||(I_group_loc_value));
         close C_INV_LOC_REGION;

         if L_inv_loc is NOT NULL then
            L_stock_on_hand := 'Y';
         end if;

      else
         FOR rec in C_GET_MRT_ITEM LOOP
            L_get_item := rec.item;

            SQL_LIB.SET_MARK('OPEN',
                             'C_INV_LOC_REGION',
                             'V_STORE',
                             'Region: '||(I_group_loc_value));
            open C_INV_LOC_REGION (L_get_item);

            SQL_LIB.SET_MARK('FETCH',
                             'C_INV_LOC_REGION',
                             'V_STORE',
                             'Region: '||(I_group_loc_value));
            fetch C_INV_LOC_REGION into L_inv_loc;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_INV_LOC_REGION',
                             'V_STORE',
                             'Region: '||(I_group_loc_value));
            close C_INV_LOC_REGION;

            if L_inv_loc is NOT NULL then
               L_stock_on_hand := 'Y';
            end if;

            if L_stock_on_hand  = 'Y' then
               EXIT;
            end if;
         END LOOP;
      end if;/*End of checking of invalid locations 'Y' per item*/

      FOR rec IN C_REGION LOOP
         L_loc := rec.store;

         SQL_LIB.SET_MARK('OPEN',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         open C_STOCK_IND_S (L_loc);

         SQL_LIB.SET_MARK('FETCH',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         fetch C_STOCK_IND_S into L_stock_ind;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         close C_STOCK_IND_S;

         if L_stock_ind = 'N' then
            L_stockholding_ind := 'N';
         else
            FOR rec IN C_STOCK_ON_HAND(L_loc) LOOP
               if rec.stock_on_hand <= 0 then
                  L_stock_on_hand := 'Y';  -- zero or negative SOH
               elsif rec.stock_on_hand > 0 then
                  L_cnt_mrt_item_loc := 1; -- Item is valid
               end if;
            END LOOP;
         end if;

         if (L_stockholding_ind = 'N' and L_stock_on_hand = 'Y') then
            EXIT; -- exit loop when all flags are set
         end if;

         L_loc := NULL; -- next location
      END LOOP;
   elsif I_group_loc_type = 'D' then
      -- Check stockholding store belonging to the DISTRICT that doesn't exist in
      -- ITEM_LOC_SOH table for an item included in the MRT
      if I_item is NOT NULL then
         L_get_item := I_item;
         SQL_LIB.SET_MARK('OPEN',
                          'C_INV_LOC_DISTRICT',
                          'V_STORE',
                          'District: '||(I_group_loc_value));
         open C_INV_LOC_DISTRICT (L_get_item);

         SQL_LIB.SET_MARK('FETCH',
                          'C_INV_LOC_DISTRICT',
                          'V_STORE',
                          'District: '||(I_group_loc_value));
         fetch C_INV_LOC_DISTRICT into L_inv_loc;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_INV_LOC_DISTRICT',
                          'V_STORE',
                          'District: '||(I_group_loc_value));
         close C_INV_LOC_DISTRICT;

         if L_inv_loc is NOT NULL then
            L_stock_on_hand := 'Y';
         end if;

      else
         FOR rec in C_GET_MRT_ITEM LOOP
            L_get_item := rec.item;

            SQL_LIB.SET_MARK('OPEN',
                             'C_INV_LOC_DISTRICT',
                             'V_STORE',
                             'District: '||(I_group_loc_value));
            open C_INV_LOC_DISTRICT (L_get_item);

            SQL_LIB.SET_MARK('FETCH',
                             'C_INV_LOC_DISTRICT',
                             'V_STORE',
                             'District: '||(I_group_loc_value));
            fetch C_INV_LOC_DISTRICT into L_inv_loc;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_INV_LOC_DISTRICT',
                             'V_STORE',
                             'District: '||(I_group_loc_value));
            close C_INV_LOC_DISTRICT;

            if L_inv_loc is NOT NULL then
               L_stock_on_hand := 'Y';
            end if;

            if L_stock_on_hand  = 'Y' then
               EXIT;
            end if;
         END LOOP;
      end if;/*End of checking of invalid locations 'Y' per item*/

      FOR rec IN C_DISTRICT LOOP
         L_loc := rec.store;

         SQL_LIB.SET_MARK('OPEN',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         open C_STOCK_IND_S (L_loc);

         SQL_LIB.SET_MARK('FETCH',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         fetch C_STOCK_IND_S into L_stock_ind;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         close C_STOCK_IND_S;

         if L_stock_ind = 'N' then
            L_stockholding_ind := 'N';
         else
            FOR rec IN C_STOCK_ON_HAND(L_loc) LOOP
               if rec.stock_on_hand <= 0 then
                  L_stock_on_hand := 'Y';  -- zero or negative SOH
               end if;
            END LOOP;
         end if;

         if (L_stockholding_ind = 'N' and L_stock_on_hand = 'Y') then
            EXIT; -- exit loop when all flags are set
         end if;

         L_loc := NULL; -- next location
      END LOOP;
   elsif I_group_loc_type = 'L' then
      -- Check stockholding locations in LOC_TRAITS_MATRIX table that doesn't exist in
      -- ITEM_LOC_SOH table for an item included in the MRT
      if I_item is NOT NULL then
         L_get_item := I_item;
         SQL_LIB.SET_MARK('OPEN',
                          'C_INV_LOC_LOCTRAIT',
                          'LOC_TRAITS_MATRIX'||' V_STORE',
                          'Loc Trait: '||(I_group_loc_value));
         open C_INV_LOC_LOCTRAIT (L_get_item);

         SQL_LIB.SET_MARK('FETCH',
                          'C_INV_LOC_LOCTRAIT',
                          'LOC_TRAITS_MATRIX'||' V_STORE',
                          'Loc Trait: '||(I_group_loc_value));
         fetch C_INV_LOC_LOCTRAIT into L_inv_loc;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_INV_LOC_LOCTRAIT',
                          'LOC_TRAITS_MATRIX'||' V_STORE',
                          'Loc Trait: '||(I_group_loc_value));
         close C_INV_LOC_LOCTRAIT;

         if L_inv_loc is NOT NULL then
            L_stock_on_hand := 'Y';
         end if;

      else
         FOR rec in C_GET_MRT_ITEM LOOP
            L_get_item := rec.item;

            SQL_LIB.SET_MARK('OPEN',
                             'C_INV_LOC_LOCTRAIT',
                             'LOC_TRAITS_MATRIX'||' V_STORE',
                             'Loc Trait: '||(I_group_loc_value));
            open C_INV_LOC_LOCTRAIT (L_get_item);

            SQL_LIB.SET_MARK('FETCH',
                             'C_INV_LOC_LOCTRAIT',
                             'LOC_TRAITS_MATRIX'||' V_STORE',
                             'Loc Trait: '||(I_group_loc_value));
            fetch C_INV_LOC_LOCTRAIT into L_inv_loc;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_INV_LOC_LOCTRAIT',
                             'LOC_TRAITS_MATRIX'||' V_STORE',
                             'Loc Trait: '||(I_group_loc_value));
            close C_INV_LOC_LOCTRAIT;

            if L_inv_loc is NOT NULL then
               L_stock_on_hand := 'Y';
            end if;

            if L_stock_on_hand  = 'Y' then
               EXIT;
            end if;
         END LOOP;
      end if;/*End of checking of invalid locations 'Y' per item*/

      FOR rec IN C_LOC_TRAIT LOOP
         L_loc := rec.store;

         SQL_LIB.SET_MARK('OPEN',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         open C_STOCK_IND_S (L_loc);

         SQL_LIB.SET_MARK('FETCH',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         fetch C_STOCK_IND_S into L_stock_ind;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         close C_STOCK_IND_S;

         if L_stock_ind = 'N' then
            L_stockholding_ind := 'N';
         else
            FOR rec IN C_STOCK_ON_HAND(L_loc) LOOP
               if rec.stock_on_hand <= 0 then
                  L_stock_on_hand := 'Y';  -- zero or negative SOH
               elsif rec.stock_on_hand > 0 then
                  L_cnt_mrt_item_loc := 1; -- Item is valid
               end if;
            END LOOP;
         end if;

         if (L_stockholding_ind = 'N' and L_stock_on_hand = 'Y') then
            EXIT;
         end if;

         L_loc := NULL; -- next location
      END LOOP;
   elsif I_group_loc_type = 'C' then
      -- Check stockholding store belonging to the STORE CLASS that doesn't exist in
      -- ITEM_LOC_SOH table for an item included in the MRT
      if I_item is NOT NULL then
         L_get_item := I_item;
         SQL_LIB.SET_MARK('OPEN',
                          'C_INV_LOC_STORE_CLASS',
                          'V_STORE',
                          'Store Class: '||(I_group_loc_value));
         open C_INV_LOC_STORE_CLASS (L_get_item);

         SQL_LIB.SET_MARK('FETCH',
                          'C_INV_LOC_STORE_CLASS',
                          'V_STORE',
                          'Store Class: '||(I_group_loc_value));
         fetch C_INV_LOC_STORE_CLASS into L_inv_loc;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_INV_LOC_STORE_CLASS',
                          'V_STORE',
                          'Store Class: '||(I_group_loc_value));
         close C_INV_LOC_STORE_CLASS;

         if L_inv_loc is NOT NULL then
            L_stock_on_hand := 'Y';
         end if;

      else
         FOR rec in C_GET_MRT_ITEM LOOP
            L_get_item := rec.item;

            SQL_LIB.SET_MARK('OPEN',
                             'C_INV_LOC_STORE_CLASS',
                             'V_STORE',
                             'Store Class: '||(I_group_loc_value));
            open C_INV_LOC_STORE_CLASS (L_get_item);

            SQL_LIB.SET_MARK('FETCH',
                             'C_INV_LOC_STORE_CLASS',
                             'V_STORE',
                             'Store Class: '||(I_group_loc_value));
            fetch C_INV_LOC_STORE_CLASS into L_inv_loc;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_INV_LOC_STORE_CLASS',
                             'V_STORE',
                             'Store Class: '||(I_group_loc_value));
            close C_INV_LOC_STORE_CLASS;

            if L_inv_loc is NOT NULL then
               L_stock_on_hand := 'Y';
            end if;

            if L_stock_on_hand  = 'Y' then
               EXIT;
            end if;
         END LOOP;
      end if;/*End of checking of invalid locations 'Y' per item*/

      FOR rec IN C_STORE_CLASS LOOP
         L_loc := rec.store;
         SQL_LIB.SET_MARK('OPEN',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         open C_STOCK_IND_S (L_loc);

         SQL_LIB.SET_MARK('FETCH',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         fetch C_STOCK_IND_S into L_stock_ind;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_STOCK_IND_S',
                          'V_STORE',
                          'Store: '||(L_loc));
         close C_STOCK_IND_S;

         if L_stock_ind = 'N' then
            L_stockholding_ind := 'N';
         else
            FOR rec IN C_STOCK_ON_HAND(L_loc) LOOP
               if rec.stock_on_hand <= 0 then
                  L_stock_on_hand := 'Y';  -- zero or negative SOH
               elsif rec.stock_on_hand > 0 then
                  L_cnt_mrt_item_loc := 1; -- Item is valid
               end if;
            END LOOP;
         end if;

         if (L_stockholding_ind = 'N' and L_stock_on_hand = 'Y') then
            EXIT; -- exit loop when all flags are set
         end if;

         L_loc := NULL; -- next location
      END LOOP;
   end if;

   if I_group_loc_type = 'LLS' or I_group_loc_type = 'LLW' then
      if L_cnt_mrt_item_loc = 0 then
         -- All locations in LOC_LIST was not applied
         O_soh_exists := FALSE;
         O_stockholding_exists := FALSE;
      elsif (L_stockholding_ind = 'N' and L_stock_on_hand is NULL) then
         -- One or more locations were not applied since they are non stockholding locations.
         O_stockholding_exists := TRUE;
         O_soh_exists := FALSE;
      elsif (L_stockholding_ind is NULL and L_stock_on_hand = 'Y') then
         -- One or more locations were not applied since the stock on hand of the item is zero.
         O_soh_exists := TRUE;
         O_stockholding_exists := FALSE;
      elsif (L_stockholding_ind ='N' and L_stock_on_hand = 'Y') then
         -- One or more locations were not applied since they are non stockholding
         -- locations and the stock on hand of the item is zero.
         O_soh_exists := TRUE;
         O_stockholding_exists :=TRUE;
      end if;
   elsif I_group_loc_type = 'AS' then
      if L_stockholding_ind is NULL and L_stock_on_hand is NULL then
         if L_cnt_mrt_item_loc = 0 then
            FOR rec IN C_INV_LOC_ALL_STORE LOOP
               if rec.stock_on_hand is NOT NULL or rec.stockholding_ind is NOT NULL then
                  O_soh_exists := TRUE; -- There is an invalid location(Store location)
                  O_stockholding_exists := FALSE;
                  EXIT;
               end if;
            END LOOP;
         else
            O_location_exists := FALSE; -- All locations are valid
         end if;
      else
         if (L_stockholding_ind = 'N' and L_stock_on_hand is NULL) then
            -- One or more locations were not applied since they are non stockholding locations.
            O_stockholding_exists := TRUE;
            O_soh_exists := FALSE;
         elsif (L_stockholding_ind is NULL and L_stock_on_hand = 'Y') then
            -- One or more locations were not applied since the stock on hand of the item is zero.
            O_soh_exists := TRUE;
            O_stockholding_exists := FALSE;
         elsif (L_stockholding_ind ='N' and L_stock_on_hand = 'Y') then
            -- One or more locations were not applied since they are non stockholding
            -- locations and the stock on hand of the item is zero.
            O_soh_exists := TRUE;
            O_stockholding_exists :=TRUE;
         end if;
      end if;
   elsif I_group_loc_type = 'AW' then
      if L_stockholding_ind is NULL and L_stock_on_hand is NULL then
         if L_cnt_mrt_item_loc = 0 then
            FOR rec IN C_INV_LOC_ALL_WH LOOP
               if rec.stock_on_hand is NOT NULL or rec.stockholding_ind is NOT NULL then
                  O_soh_exists := TRUE; -- There is an invalid location(WH location)
                  O_stockholding_exists := FALSE;
                  EXIT;
               end if;
            END LOOP;
         else
            O_location_exists := FALSE; -- All locations are valid
         end if;
      else
         if (L_stockholding_ind = 'N' and L_stock_on_hand is NULL) then
            -- One or more locations were not applied since they are non stockholding locations.
            O_stockholding_exists := TRUE;
            O_soh_exists := FALSE;
         elsif (L_stockholding_ind is NULL and L_stock_on_hand = 'Y') then
            -- One or more locations were not applied since the stock on hand of the item is zero.
            O_soh_exists := TRUE;
            O_stockholding_exists := FALSE;
         elsif (L_stockholding_ind ='N' and L_stock_on_hand = 'Y') then
            -- One or more locations were not applied since they are non stockholding
            -- locations and the stock on hand of the item is zero.
            O_soh_exists := TRUE;
            O_stockholding_exists :=TRUE;
         end if;
      end if;
   else
      if (L_stockholding_ind = 'N' and L_stock_on_hand is NULL) then
         -- One or more locations were not applied since they are non stockholding locations.
         O_stockholding_exists := TRUE;
         O_soh_exists := FALSE;
      elsif (L_stockholding_ind is NULL and L_stock_on_hand = 'Y') then
         -- One or more locations were not applied since the stock on hand of the item is zero.
         O_soh_exists := TRUE;
         O_stockholding_exists := FALSE;
      elsif (L_stockholding_ind ='N' and L_stock_on_hand = 'Y') then
         -- One or more locations were not applied since they are non stockholding
         -- locations and the stock on hand of the item is zero.
         O_soh_exists := TRUE;
         O_stockholding_exists :=TRUE;
      elsif L_stockholding_ind is NULL and L_stock_on_hand is NULL then
         O_location_exists := FALSE; -- All locations are valid
      end if;
   end if;

   return true;

EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;

END CREATE_MRT_ITEM_LOC;
----------------------------------------------------------------------------------
FUNCTION UPDATE_TRANSFER_QTY (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_mrt_no          IN       MRT.MRT_NO%TYPE,
                              I_location        IN       MRT_ITEM_LOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(64)   := 'MRT_SQL.UPDATE_TRANSFER_QTY';
   L_table       VARCHAR2(20)   := NULL;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_MRT_ITEM_LOC is
      select 'x'
        from mrt_item_loc
       where location   = I_location
         and mrt_no     = I_mrt_no
         for update nowait;

   cursor C_GET_TSF_QTY is
      select distinct td.tsf_qty,
             mil.item
        from tsfdetail td,
             tsfhead   th,
             mrt_item_loc mil
       where td.item = mil.item
             and td.tsf_no   = th.tsf_no
             and th.mrt_no   = mil.mrt_no
             and th.from_loc = mil.location
             and th.mrt_no   = I_mrt_no
             and th.from_loc = I_location;

BEGIN

   /* make sure required parameters are populated */
   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_location',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   ---
   L_table := 'MRT_ITEM_LOC';

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_MRT_ITEM_LOC',
                    L_table,
                    'MRT_NO: '||TO_CHAR(I_mrt_no)||
                    '/ LOCATION: '||TO_CHAR(I_location));
   open C_LOCK_MRT_ITEM_LOC;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_MRT_ITEM_LOC',
                    L_table,
                    'MRT_NO: '||TO_CHAR(I_mrt_no)||
                    '/ LOCATION: '||TO_CHAR(I_location));
   close C_LOCK_MRT_ITEM_LOC;
   ---
   FOR rec in C_GET_TSF_QTY LOOP

      update mrt_item_loc mil
         set mil.tsf_qty  = rec.tsf_qty
       where mil.mrt_no   = I_mrt_no
         and mil.location = I_location
         and mil.item     = rec.item;

   END LOOP;

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'I_mrt_no = '||I_mrt_no,
                                            'I_location = '||I_location);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_TRANSFER_QTY;
--------------------------------------------------------------------------------------------
-- Function: NEXT_MRT_NUMBER
-- Purpose : This function will generate the next unique ID sequence used for the MRT table.
-- This function will use the MRT_NUMBER_SEQ
--------------------------------------------------------------------------------------------

FUNCTION NEXT_MRT_NUMBER (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_mrt_no          IN OUT   MRT.MRT_NO%TYPE)
   RETURN BOOLEAN IS

   L_program           VARCHAR2( 64 )  := 'MRT_SQL.NEXT_MRT_NUMBER';
   L_mrt_no            MRT.MRT_NO%TYPE := 0;
   L_prev_mrt_no       MRT.MRT_NO%TYPE := 0;
   L_mrt_no_exists     VARCHAR2(1)     := 'N';
   L_first_fetch_ind   VARCHAR2(1)     := 'Y';

   cursor C_CHECK_MRT_NO is
      select 'X'
        from mrt
       where mrt_no = L_mrt_no
         and rownum = 1;

   cursor C_NEXT_SEQUENCE is
      select mrt_seq.nextval
        from sys.dual;

BEGIN
   LOOP
      SQL_LIB.SET_MARK('OPEN',
                       'C_NEXT_SEQUENCE',
                       NULL,
                       NULL);
      open C_NEXT_SEQUENCE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_NEXT_SEQUENCE',
                       NULL,
                       NULL);
      fetch C_NEXT_SEQUENCE into L_mrt_no;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_NEXT_SEQUENCE',
                       NULL,
                       NULL);
      close C_NEXT_SEQUENCE;

      if L_first_fetch_ind = 'Y' then
         L_prev_mrt_no := L_mrt_no;
         L_first_fetch_ind := 'N';
      elsif L_mrt_no = L_prev_mrt_no then
         O_error_message := SQL_LIB.CREATE_MSG( 'NO_SEQ_NO_AVAIL',
                                                NULL,
                                                NULL,
                                                NULL );
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_MRT_NO',
                       NULL,
                       NULL);
      open C_CHECK_MRT_NO;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_MRT_NO',
                       NULL,
                       NULL);
      fetch C_CHECK_MRT_NO into L_mrt_no_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_MRT_NO',
                       NULL,
                       NULL);
      close C_CHECK_MRT_NO;

      if L_mrt_no_exists = 'N' then
         O_mrt_no := L_mrt_no;
         return TRUE;
      end if;
    END LOOP;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char( SQLCODE ) );
      return FALSE;
END NEXT_MRT_NUMBER;
------------------------------------------------------------------------------
FUNCTION LOCK_MRT_ITEM ( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_mrt_no        IN     MRT_ITEM.MRT_NO%TYPE,
                         I_item          IN     MRT_ITEM.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)    := 'MRT_SQL.LOCK_MRT_ITEM';
   L_table           VARCHAR(20)     := 'MRT_ITEM';

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_MRT_ITEM IS
      select 'x'
       from mrt_item
      where mrt_no = I_mrt_no
        and item   = I_item
       for update nowait;
BEGIN

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_LOCK_MRT_ITEM', L_table ,'Mrt: '||(I_mrt_no));
   OPEN C_LOCK_MRT_ITEM;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_MRT_ITEM', L_table ,'Mrt: '||(I_mrt_no));
   CLOSE C_LOCK_MRT_ITEM;

   return true;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'I_mrt_no = '||I_mrt_no,
                                            'I_item = '||I_item);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char( SQLCODE ));
      return FALSE;

END LOCK_MRT_ITEM;
------------------------------------------------------------------------------
FUNCTION UPDATE_MRT_ITEM ( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_mrt_no        IN     MRT_ITEM.MRT_NO%TYPE,
                           I_item          IN     MRT_ITEM.ITEM%TYPE,
                           I_restock_pct   IN     MRT_ITEM.RESTOCK_PCT%TYPE,
                           I_selected_ind  IN     MRT_ITEM.SELECTED_IND%TYPE,
                           I_rtv_cost      IN     MRT_ITEM.RTV_COST%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program           VARCHAR2( 64 )  := 'MRT_SQL.UPDATE_MRT_ITEM';
   L_table             VARCHAR(20)     := 'MRT_ITEM';

BEGIN

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, 'Mrt: '||(I_mrt_no));
   update mrt_item
      set restock_pct  = nvl(I_restock_pct, restock_pct),
          selected_ind = nvl(I_selected_ind, selected_ind),
          rtv_cost     = nvl(I_rtv_cost, rtv_cost)
    where mrt_no       = I_mrt_no
      and item         = I_item;

   return true;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char( SQLCODE ));

      return FALSE;

END UPDATE_MRT_ITEM;

------------------------------------------------------------------------------
FUNCTION UPDATE_RESTOCK_PCT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_mrt_no          IN     MRT_ITEM.MRT_NO%TYPE,
                            I_restock_pct     IN     MRT_ITEM.RESTOCK_PCT%TYPE,
                            I_diff            IN     ITEM_MASTER.DIFF_1%TYPE DEFAULT NULL,
                            I_vpn             IN     ITEM_SUPPLIER.VPN%TYPE DEFAULT NULL,
                            I_item_parent     IN     ITEM_MASTER.ITEM_PARENT%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   L_program      VARCHAR2(64)  := 'MRT_SQL.UPDATE_RESTOCK_PCT';

   cursor C_LOCK_MRT_ITEM is
      select 'Y'
        from mrt_item
       where mrt_no = I_mrt_no
         for update nowait;

BEGIN

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   open C_LOCK_MRT_ITEM;
   close C_LOCK_MRT_ITEM;

   update mrt_item mri
      set mri.restock_pct = I_restock_pct
    where mri.mrt_no      = I_mrt_no
      and item in (select im.item
                     from item_master im,
                          item_supplier isu,
                          v_mrt_item vmri
                    where im.item=isu.item
                      and vmri.item = im.item
                      and vmri.mrt_no = mri.mrt_no
                      and (nvl(im.diff_1,'-999')= nvl(I_diff,nvl(im.diff_1,'-999'))
                            or nvl(im.diff_2,'-999')= nvl(I_diff,nvl(im.diff_2,'-999'))
                             or nvl(im.diff_3,'-999')= nvl(I_diff,nvl(im.diff_3,'-999'))
                              or nvl(im.diff_4,'-999')= nvl(I_diff,nvl(im.diff_4,'-999')))
                      and nvl(isu.vpn,'-999')= nvl(I_vpn,nvl(isu.vpn,'-999')) 
                      and (nvl(im.item_parent,'-999') = nvl(I_item_parent,nvl(im.item_parent,'-999')))) ;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'MRT_ITEM',
                                            'I_mrt_no = '||I_mrt_no,
                                             null);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char( SQLCODE ));

      return FALSE;

END UPDATE_RESTOCK_PCT;
----------------------------------------------------------------------------------
FUNCTION UPDATE_MRT_QTYS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_mrt_no          IN     MRT.MRT_NO%TYPE,
                         I_quantity_type   IN     MRT.QUANTITY_TYPE%TYPE,
                         I_inventory_type  IN     MRT.INVENTORY_TYPE%TYPE,
                         I_item            IN     MRT_ITEM.ITEM%TYPE DEFAULT NULL,
                         I_location        IN     MRT_ITEM_LOC.LOCATION%TYPE DEFAULT NULL)
   RETURN BOOLEAN is

   L_program          VARCHAR2(64)              := 'MRT_SQL.UPDATE_MRT_QTYS';
   L_table            VARCHAR(20)               := 'MRT_ITEM_LOC';
   L_inv_status       MRT.INVENTORY_TYPE%TYPE;
   L_return_avail_qty MRT_ITEM_LOC.RETURN_AVAIL_QTY%TYPE;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_MRT_ITEM_LOC is
      select item, loc_type, location
        from mrt_item_loc
       where mrt_no   = I_mrt_no
         and item     = NVL(I_item,item)
         and location = NVL(I_location,location);

   TYPE  MRT_ITEM_TAB   IS TABLE OF MRT_ITEM_LOC.ITEM%TYPE INDEX BY BINARY_INTEGER;
   TYPE  NEW_QTY_TAB    IS TABLE OF MRT_ITEM_LOC.RETURN_AVAIL_QTY%TYPE ;--INDEX BY BINARY_INTEGER;
   TYPE  LOC_TYPE_TAB   IS TABLE OF MRT_ITEM_LOC.LOC_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE  LOCATION_TAB   IS TABLE OF MRT_ITEM_LOC.LOCATION%TYPE INDEX BY BINARY_INTEGER;

   L_item        MRT_ITEM_TAB ;
   L_new_qty     NEW_QTY_TAB  := NEW_QTY_TAB();
   L_loc_type    LOC_TYPE_TAB ;
   L_location    LOCATION_TAB ;

BEGIN
   --check for nulls
   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   ---
   if I_quantity_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_quantity_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   ---
   if I_inventory_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_inventory_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   ---
   --open cursor, lock records and bulk collect all items
   open C_LOCK_MRT_ITEM_LOC;
   fetch C_LOCK_MRT_ITEM_LOC BULK COLLECT into L_item, L_loc_type, L_location;
   close C_LOCK_MRT_ITEM_LOC;

   if L_item.FIRST is NOT NULL then
      --set inv status
      if I_inventory_type = 'A' then
         L_inv_status := NULL;
      else
         L_inv_status := I_inventory_type;
      end if;
      --get new qtys from function
      FOR i in L_item.FIRST..L_item.LAST LOOP
         L_new_qty.extend;
         if TRANSFER_SQL.QUANTITY_AVAIL(  O_error_message,
                                          L_return_avail_qty,
                                          L_item(i),
                                          L_inv_status,
                                          L_loc_type(i),
                                          L_location(i)) = FALSE then
            return FALSE;

         end if;
         if L_return_avail_qty < 0 then
            L_return_avail_qty := 0;
         end if;
         L_new_qty(L_new_qty.last) := L_return_avail_qty;
      end LOOP;

      --update all items for MRT
      FORALL i IN L_item.FIRST..L_item.LAST
         update MRT_ITEM_LOC
            set return_avail_qty = L_new_qty(i),
                tsf_qty          = decode(I_quantity_type, 'A', L_new_qty(i), tsf_qty)
          where mrt_no = I_mrt_no
            and item = L_item(i)
            and loc_type = L_loc_type(i)
            and location = L_location(i);
   end if;

return TRUE;
---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'I_mrt_no = '||I_mrt_no,
                                            NULL);
   return FALSE;
   ---
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char( SQLCODE ));

      return FALSE;

END UPDATE_MRT_QTYS;
------------------------------------------------------------------------------------
FUNCTION LOCK_MRT_ITEM_LOC (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_mrt_no        IN     MRT_ITEM_LOC.MRT_NO%TYPE,
                            I_item          IN     MRT_ITEM_LOC.ITEM%TYPE,
                            I_location      IN     MRT_ITEM_LOC.lOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64)    := 'MRT_SQL.LOCK_MRT_ITEM_LOC';
   L_table           VARCHAR(20)     := 'MRT_ITEM_LOC';

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_MRT_ITEM_LOC IS
      select 'x'
       from mrt_item_loc
      where mrt_no   = I_mrt_no
        and item     = I_item
        and location = I_location
       for update nowait;
BEGIN

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_location',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN','C_LOCK_MRT_ITEM_LOC', L_table ,'Mrt: '||(I_mrt_no));
   OPEN C_LOCK_MRT_ITEM_LOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_MRT_ITEM_LOC', L_table ,'Mrt: '||(I_mrt_no));
   CLOSE C_LOCK_MRT_ITEM_LOC;

   return true;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'I_mrt_no = '||I_mrt_no,
                                            'I_item = '||I_item||
                                            ' I_location = '||I_location);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char( SQLCODE ));
      return FALSE;

END LOCK_MRT_ITEM_LOC;
------------------------------------------------------------------------------
FUNCTION UPDATE_MRT_ITEM_LOC (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_mrt_no        IN     MRT_ITEM_LOC.MRT_NO%TYPE,
                              I_item          IN     MRT_ITEM_LOC.ITEM%TYPE,
                              I_location      IN     MRT_ITEM_LOC.LOCATION%TYPE,
                              I_tsf_qty       IN     MRT_ITEM_LOC.TSF_QTY%TYPE,
                              I_tsf_cost      IN     MRT_ITEM_LOC.TSF_QTY%TYPE,
                              I_tsf_price     IN     MRT_ITEM_LOC.TSF_QTY%TYPE)
   RETURN BOOLEAN IS

   L_program           VARCHAR2( 64 )  := 'MRT_SQL.UPDATE_MRT_ITEM_LOC';
   L_table             VARCHAR(20)     := 'MRT_ITEM_LOC';

BEGIN

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, 'Mrt: '||(I_mrt_no));
   update mrt_item_loc
      set tsf_qty      = nvl(I_tsf_qty, tsf_qty),
          tsf_cost     = nvl(I_tsf_cost, tsf_cost),
          tsf_price    = nvl(I_tsf_price, tsf_price)
    where mrt_no       = I_mrt_no
      and item         = I_item
      and location     = I_location;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char( SQLCODE ));

      return FALSE;

END UPDATE_MRT_ITEM_LOC;

FUNCTION UPDATE_MRT_ITEM_LOC (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_mrt_no        IN     MRT.MRT_NO%TYPE,
                              I_status        IN     MRT.MRT_STATUS%TYPE )
RETURN BOOLEAN IS

   L_status mrt.mrt_status%TYPE;
   L_program           VARCHAR2( 64 )  := 'MRT_SQL.UPDATE_MRT_STATUS';
   L_table             VARCHAR(20)     := 'MRT';

   ---------------------------------
   -- Lock the relevent mrt record
   ---------------------------------
   cursor c_upd_mrt IS
   select mrt_status
     from mrt
    where mrt_no = I_mrt_no
      for update of mrt_status nowait;

   RECORD_LOCKED                 EXCEPTION;
   PRAGMA                        EXCEPTION_INIT(Record_Locked, -54);

BEGIN

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;


   if I_status is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_status',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   open c_upd_mrt;
   fetch c_upd_mrt into L_status;
   if c_upd_mrt%FOUND then
      update mrt
         set mrt_status = I_status
       where current of c_upd_mrt;
   end if;
   close c_upd_mrt;


   RETURN TRUE;

EXCEPTION
    WHEN RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'RTV_DETAIL',
                                             TO_CHAR(I_mrt_no),
                                             null);
      RETURN FALSE;
   WHEN OTHERS THEN
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);

      RETURN FALSE;
END UPDATE_MRT_ITEM_LOC ;
------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_LIST (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tracking_level_ind IN OUT BOOLEAN,
                             O_inventory_ind      IN OUT BOOLEAN,
                             O_unapproved_ind     IN OUT BOOLEAN,
                             I_item_list          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64)    := 'MRT_SQL.VALIDATE_ITEM_LIST';

   CURSOR C_CHECK_TRACKING_LEVEL IS
      select 'x'
        from skulist_detail sd,
             item_master    im
       where sd.skulist    = I_item_list
         and sd.item = im.item
         and im.item_level < im.tran_level;

   CURSOR C_CHECK_INVENTORY IS
      select 'x'
        from skulist_detail sd,
             item_master    im
       where sd.skulist    = I_item_list
         and sd.item = im.item
         and im.inventory_ind = 'N';

   CURSOR C_CHECK_UNAPPROVED IS
      select 'x'
        from skulist_detail sd,
             item_master    im
       where sd.skulist    = I_item_list
         and sd.item = im.item
         and im.status != 'A';

   L_dummy VARCHAR2(1);

BEGIN

   if I_item_list is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item_list',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_TRACKING_LEVEL',NULL,NULL);
   open C_CHECK_TRACKING_LEVEL;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_TRACKING_LEVEL',NULL,NULL);
   fetch C_CHECK_TRACKING_LEVEL into L_dummy;
   if C_CHECK_TRACKING_LEVEL%NOTFOUND then
      O_tracking_level_ind := FALSE;
   else
      O_tracking_level_ind := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_TRACKING_LEVEL',NULL,NULL);
   close C_CHECK_TRACKING_LEVEL;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_INVENTORY',NULL,NULL);
   open C_CHECK_INVENTORY;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_INVENTORY',NULL,NULL);
   fetch C_CHECK_INVENTORY into L_dummy;
   if C_CHECK_INVENTORY%NOTFOUND then
      O_inventory_ind := FALSE;
   else
      O_inventory_ind := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_INVENTORY',NULL,NULL);
   close C_CHECK_INVENTORY;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_UNAPPROVED',NULL,NULL);
   open C_CHECK_UNAPPROVED;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_UNAPPROVED',NULL,NULL);
   fetch C_CHECK_UNAPPROVED into L_dummy;
   if C_CHECK_UNAPPROVED%NOTFOUND then
      O_unapproved_ind := FALSE;
   else
      O_unapproved_ind := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_UNAPPROVED',NULL,NULL);
   close C_CHECK_UNAPPROVED;

   return TRUE;

EXCEPTION

   WHEN OTHERS THEN
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);

      RETURN FALSE;

END VALIDATE_ITEM_LIST;
------------------------------------------------------------------------------
FUNCTION UPDATE_RECEIVED_QTY (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_mrt_no        IN     MRT_ITEM_LOC.MRT_NO%TYPE,
                              I_tsf_no        IN     TSFDETAIL.TSF_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'MRT_SQL.UPDATE_RECEIVED_QTY';
   L_table     VARCHAR2(20)   := 'MRT_ITEM_LOC';

BEGIN

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_tsf_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_tsf_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, L_table, 'Mrt: '||(I_mrt_no));
   update mrt_item_loc mil
      set mil.received_qty = nvl(mil.received_qty,0) +
         (select td.received_qty
            from tsfhead th,
                 tsfdetail td
           where th.tsf_no = I_tsf_no
             and th.tsf_no = td.tsf_no
             and th.mrt_no = mil.mrt_no
             and td.item = mil.item
             and th.from_loc = mil.location)
    where mil.mrt_no = I_mrt_no;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char( SQLCODE ));

      return FALSE;


END UPDATE_RECEIVED_QTY;
------------------------------------------------------------------------------
FUNCTION LOCK_MRT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_mrt_no          IN       MRT.MRT_NO%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)    := 'MRT_SQL.LOCK_MRT';
   L_table           VARCHAR(20)     := 'MRT';

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_MRT IS
      select 'x'
       from mrt
      where mrt_no = I_mrt_no
        for update nowait;
BEGIN

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_MRT',
                    L_table ,
                    'Mrt: '||(I_mrt_no));
   OPEN C_LOCK_MRT;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_MRT',
                    L_table ,
                    'Mrt: '||(I_mrt_no));
   CLOSE C_LOCK_MRT;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_VIEW',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char( SQLCODE ));
      return FALSE;

END LOCK_MRT;
------------------------------------------------------------------------------
FUNCTION INSERT_RTV_INFO(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_rtv_order_no         IN OUT   RTV_HEAD.RTV_ORDER_NO%TYPE,
                         I_mrt_no               IN       MRT.MRT_NO%TYPE,
                         I_rtv_create_status    IN       MRT.CREATE_RTV_STATUS%TYPE,
                         I_supplier             IN       SUPS.SUPPLIER%TYPE,
                         I_ret_auth_num         IN       RTV_HEAD.RET_AUTH_NUM%TYPE,
                         I_rtv_wh               IN       RTV_HEAD.WH%TYPE,
                         I_comment_desc         IN       RTV_HEAD.COMMENT_DESC%TYPE,
                         I_rtv_not_after_date   IN       RTV_HEAD.NOT_AFTER_DATE%TYPE,
                         I_rtv_restock_pct      IN       RTV_HEAD.RESTOCK_PCT%TYPE,
                         I_rtv_reason           IN       MRT.RTV_REASON%TYPE,
                         I_include_wh_inv_ind   IN       MRT.INCLUDE_WH_INV%TYPE,
                         I_inventory_type       IN       MRT.INVENTORY_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64)    := 'MRT_SQL.INSERT_RTV_INFO';

   L_return_code       VARCHAR2(25);
   L_rtv_order_no      RTV_HEAD.RTV_ORDER_NO%TYPE  := 0;
   L_wh_available_qty  NUMBER := 0;
   L_seq_no            NUMBER := 0;
   L_qty_requested     NUMBER := 0;
   L_mrt_currency_code MRT.CURRENCY_CODE%TYPE;
   L_supp_currency_code SUPS.CURRENCY_CODE%TYPE;
   L_supp_unit_cost    MRT_ITEM.RTV_COST%TYPE :=0;
   L_inventory_type    MRT.INVENTORY_TYPE%TYPE ;
   L_sysdate           DATE;
   L_user              VARCHAR2(50);

   cursor C_ITEM_SEQ_NO is
      select nvl(MAX(seq_no),0) + 1 mrtv_seq_no
        from rtv_detail
       where rtv_order_no = L_rtv_order_no;

   cursor C_MRT_ITEMS is
      select mi.item mrtv_item,
             mi.rtv_cost mrtv_cost,
             sum(nvl(mil.received_qty,0)) mrtv_received_qty
        from mrt_item mi,
             mrt_item_loc mil
       where mi.mrt_no = I_mrt_no
         and mil.mrt_no = mi.mrt_no
         and mil.item = mi.item
    group by mi.item,
             mi.restock_pct,
             mi.rtv_cost;

   cursor C_MRT_CURR_CODE is
      select mr.currency_code
        from mrt mr
       where mr.mrt_no = I_mrt_no;


BEGIN
   L_sysdate := sysdate;
   L_user    := get_user; 
   -- Check required input parameters
   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_rtv_create_status is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_rtv_create_status',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_supplier',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_rtv_wh is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_rtv_wh',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_rtv_not_after_date is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_rtv_not_after_date',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_rtv_restock_pct is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_rtv_restock_pct',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_inventory_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_inventory_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   -- Generate a new rtv order no
   NEXT_RTV_ORDER_NO(L_rtv_order_no,
                     L_return_code,
                     O_error_message);

   -- Insert a new record into the rtv_head table
   -- for the passed in mrt no

   -- MRT currency is retrieved to be used for currency conversion to supplier currency

   open C_MRT_CURR_CODE;
   fetch C_MRT_CURR_CODE into L_mrt_currency_code;
   close C_MRT_CURR_CODE;
   ---
   insert into rtv_head(rtv_order_no,
                        supplier,
                        status_ind,
                        store,
                        wh,
                        total_order_amt,
                        ship_to_add_1,
                        ship_to_add_2,
                        ship_to_add_3,
                        ship_to_city,
                        state,
                        ship_to_country_id,
                        ship_to_pcode,
                        ret_auth_num,
                        courier,
                        freight,
                        created_date,
                        ext_ref_no,
                        comment_desc,
                        mrt_no,
                        not_after_date,
                        restock_pct,
                        restock_cost)
              (select L_rtv_order_no,
                      s.supplier,
                      DECODE(I_rtv_create_status, 'A', 10, 05),
                      -1,
                      I_rtv_wh,
                      NULL,
                      a.add_1,
                      a.add_2,
                      a.add_3,
                      a.city,
                      a.state,
                      a.country_id,
                      a.post,
                      I_ret_auth_num,
                      s.ret_courier,
                      NULL,
                      get_vdate,
                      NULL,
                      I_comment_desc,
                      I_mrt_no,
                      I_rtv_not_after_date,
                      I_rtv_restock_pct,
                      NULL
                 from sups s,
                      addr a
                where s.supplier             = I_supplier
                      and a.key_value_1      = TO_CHAR(s.supplier)
                      and module             = 'SUPP'
                      and s.ret_allow_ind    = 'Y'
                      and a.addr_type        = 3
                      and a.primary_addr_ind = 'Y');

   -- Loop through all the items of the passed in mrt no
   for rec in C_MRT_ITEMS LOOP

      if rec.mrtv_item is NOT NULL then
         -- if include_wh_inv is set to 'Y', include all the
         -- available inventory from the WH
         if I_include_wh_inv_ind = 'Y' then

            if I_inventory_type ='A' then
               L_inventory_type := NULL;
            else
               L_inventory_type := I_inventory_type;
            end if;

            if TRANSFER_SQL.QUANTITY_AVAIL(O_error_message,
                                           L_wh_available_qty,
                                           rec.mrtv_item,
                                           L_inventory_type,
                                           'W',
                                           I_rtv_wh) = FALSE then
               return FALSE;
            end if;
            -- use the available qty of the WH
            -- as the quantity requested
            L_qty_requested := L_wh_available_qty;
         else
            L_qty_requested := rec.mrtv_received_qty;
         end if;

         -- retrieve the the sequence numbers to be used
         SQL_LIB.SET_MARK('OPEN',
                          'C_ITEM_SEQ_NO',
                          'RTV_DETAIL',
                          NULL);
         open C_ITEM_SEQ_NO;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ITEM_SEQ_NO',
                          'RTV_DETAIL',
                          NULL);
         fetch C_ITEM_SEQ_NO into L_seq_no;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ITEM_SEQ_NO',
                          'RTV_DETAIL',
                          NULL);
         close C_ITEM_SEQ_NO;
         ---
         if I_supplier is not null then
          --Get the supplier currency to be used in currency conversion later
          if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                               L_supp_currency_code,
                                               I_supplier) =  FALSE then
             return FALSE;
          end if;
         end if;
         -- Unit code and Original_unit_cost need to be in supplier currency
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 rec.mrtv_cost,
                                 L_mrt_currency_code,
                                 L_supp_currency_code,
                                 L_supp_unit_cost,
                                 'C',
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;

         -- insert each of the item from the mrt
         -- into the rtv_detail table
         insert into rtv_detail(rtv_order_no,
                                seq_no,
                                item,
                                shipment,
                                inv_status,
                                qty_requested,
                                unit_cost,
                                reason,
                                publish_ind,
                                restock_pct,
                                original_unit_cost,
                                updated_by_rms_ind)
                         values(L_rtv_order_no,
                                L_seq_no,
                                rec.mrtv_item,
                                NULL,
                                NULL,
                                L_qty_requested,
                                L_supp_unit_cost,
                                I_rtv_reason,
                                'N',
                                I_rtv_restock_pct,
                                L_supp_unit_cost,
                                'Y');
          if (I_rtv_create_status='A') then
            update item_loc_soh
               set rtv_qty = rtv_qty + L_qty_requested,
                   last_update_datetime = L_sysdate,
                   last_update_id       = L_user
             where item = rec.mrtv_item
               and loc = I_rtv_wh
               and loc_type = 'W';
          end if;                       

      end if;

   end LOOP;

   O_rtv_order_no := L_rtv_order_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      return FALSE;
END INSERT_RTV_INFO;
------------------------------------------------------------------------------------
-- This function will validate existence of an unapproved child items in the given
-- Parent and GrandParent Items.
------------------------------------------------------------------------------------
FUNCTION VALIDATE_IP_IG_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_approved_ind    IN OUT   BOOLEAN,
                              I_item_type       IN       CODE_DETAIL.CODE%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64)    := 'MRT_SQL.VALIDATE_IP_IG_ITEMS';
   L_dummy           VARCHAR2(1)     := NULL;

   cursor C_CHECK_PARENT_ITEM is
      select 'x'
        from v_item_master i
       where i.status                     = 'A'
         and i.inventory_ind              = 'Y'
         and i.tran_level - i.item_level  = 1
         and i.item                       = I_item
         and exists(select 'x'
                      from v_item_master i2
                     where i2.item_parent = i.item
                       and i2.status      = 'A'
                       and rownum         = 1)
         and rownum                       = 1;

   cursor C_CHECK_GRANDPARENT_ITEM is
      select 'x'
        from v_item_master i
       where i.status                          = 'A'
         and i.inventory_ind                   = 'Y'
         and i.tran_level - i.item_level       = 2
         and i.item                            = I_item
         and exists(select 'x'
                      from v_item_master i2
                     where i2.item_grandparent = i.item
                       and i2.status           = 'A'
                       and rownum              = 1)
         and rownum                            = 1;

BEGIN

   if I_item_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item_type = 'IP' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_PARENT_ITEM',
                       NULL,
                       NULL);
      open C_CHECK_PARENT_ITEM;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_PARENT_ITEM',
                       NULL,
                       NULL);
      fetch C_CHECK_PARENT_ITEM into L_dummy;

      if L_dummy is NULL then
         O_approved_ind := FALSE;
      else
         O_approved_ind := TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_PARENT_ITEM',
                       NULL,
                       NULL);
      close C_CHECK_PARENT_ITEM;
   elsif I_item_type = 'IG' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_GRANDPARENT_ITEM',
                       NULL,
                       NULL);
      open C_CHECK_GRANDPARENT_ITEM;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_GRANDPARENT_ITEM',
                       NULL,
                       NULL);
      fetch C_CHECK_GRANDPARENT_ITEM into L_dummy;

      if L_dummy is NULL then
         O_approved_ind := FALSE;
      else
         O_approved_ind := TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_GRANDPARENT_ITEM',
                       NULL,
                       NULL);
      close C_CHECK_GRANDPARENT_ITEM;
   end if;

   return TRUE;

EXCEPTION

   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);

      RETURN FALSE;

END VALIDATE_IP_IG_ITEMS;
------------------------------------------------------------------------------
FUNCTION MRT_ITEM_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_mrt_no          IN       MRT.MRT_NO%TYPE,
                         I_item            IN       MRT_ITEM.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'MRT_SQL.MRT_ITEM_EXISTS';

   L_child        BOOLEAN := FALSE;
   L_gchild       BOOLEAN := FALSE;
   L_item_level   ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level   ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_dummy        VARCHAR2(1) := NULL;

   cursor C_ITEM is
      select item_level,
             tran_level
        from v_item_master
       where item = I_item
         and status = 'A'
         and inventory_ind = 'Y';

   cursor C_V_MRT_ITEM is
      select 'x'
        from v_mrt_item
       where mrt_no = I_mrt_no
         and item_parent = I_item;

BEGIN
   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM',
                    'ITEM_MASTER',
                    'ITEM: '||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM',
                    'ITEM_MASTER',
                    'ITEM: '||I_item);
   fetch C_ITEM into L_item_level,
                     L_tran_level;
   if L_item_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                             NULL,
                                             NULL,
                                             NULL);
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM',
                       'ITEM_MASTER',
                       'ITEM: '||I_item);
      close C_ITEM;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM',
                    'ITEM_MASTER',
                    'ITEM: '||I_item);
   close C_ITEM;
   ---
   if ITEM_VALIDATE_SQL.CHILD_GCHILD_EXIST(O_error_message,
                                           L_child,
                                           L_gchild,
                                           I_item) = FALSE then
      return FALSE;
   end if;
   if L_child = FALSE then
      return FALSE;
   end if;
   ---
   if L_item_level < L_tran_level then
      SQL_LIB.SET_MARK('OPEN',
                       'C_V_MRT_ITEM',
                       'V_MRT_ITEM',
                       'MRT_NO: '||to_char(I_mrt_no)||','||
                       'ITEM_PARENT: '||I_item);
      open C_V_MRT_ITEM;
      SQL_LIB.SET_MARK('FETCH',
                       'C_V_MRT_ITEM',
                       'V_MRT_ITEM',
                       'MRT_NO: '||to_char(I_mrt_no)||','||
                       'ITEM_PARENT: '||I_item);
      fetch C_V_MRT_ITEM into L_dummy;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_V_MRT_ITEM',
                       'V_MRT_ITEM',
                       'MRT_NO: '||to_char(I_mrt_no)||','||
                       'ITEM_PARENT: '||I_item);
      close C_V_MRT_ITEM;
   else
      O_error_message := SQL_LIB.CREATE_MSG('ABOVE_TRAN_LEVEL',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   if L_dummy is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END MRT_ITEM_EXISTS;
------------------------------------------------------------------------------
FUNCTION INSERT_MRT_INFO(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_mrt_itl_track           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_mrt_itl_inv             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_mrt_itl_app             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_mrt_no                  IN       MRT.MRT_NO%TYPE,
                         I_item                    IN       MRT_ITEM.ITEM%TYPE,
                         I_item_parent             IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                         I_item_list               IN       SKULIST_HEAD.SKULIST%TYPE ,
                         I_item_type               IN       VARCHAR2,
                         I_restock_pct             IN       MRT.RESTOCK_PCT%TYPE,
                         I_rtv_cost                IN       MRT_ITEM.RTV_COST%TYPE,
                         I_selected_ind            IN       MRT_ITEM.SELECTED_IND%TYPE,
                         I_item_list_rebuild_ind   IN       VARCHAR2,
                         I_supplier                IN       MRT.SUPPLIER%TYPE,
                         I_location                IN       MRT_ITEM_LOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'MRT_SQL.INSERT_MRT_INFO';

   L_item_value            VARCHAR2(25) := NULL;
   L_rec_exists            BOOLEAN;
   L_mrt_item_rec          MRT_ITEM%ROWTYPE;
   L_item_record           ITEM_MASTER%ROWTYPE;
   L_cursor_item           VARCHAR2(50)   := NULL;
   L_tran_level_ind        BOOLEAN := FALSE;
   L_inventory_ind         BOOLEAN := FALSE;
   L_unapproved_ind        BOOLEAN := FALSE;
   L_av_cost               MRT_ITEM_LOC.UNIT_COST%TYPE := NULL;
   L_unit_cost             MRT_ITEM_LOC.UNIT_COST%TYPE := NULL;
   L_status_code           VARCHAR2(255) := NULL;
   L_no_items              NUMBER;
   L_system_options_rec    SYSTEM_OPTIONS%ROWTYPE;
   L_rtv_cost              MRT_ITEM.RTV_COST%TYPE;
   L_multi_currency_exists BOOLEAN;
   L_currency_code         CURRENCIES.CURRENCY_CODE%TYPE;
   L_mrt_currency_code     CURRENCIES.CURRENCY_CODE%TYPE;

   cursor C_MRT_CURRENCY_CODE is
      select currency_code
        from mrt
       where mrt_no = I_mrt_no;

BEGIN

   if I_mrt_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_mrt_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   
   if I_item_type  is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   
   if I_item_type = 'IL' then
      L_item_value := I_item_list;
   elsif I_item_type = 'IP' then
       L_item_value := I_item_parent;
   else -- single item ('I')
       L_item_value := I_item;
   end if;

   if I_item_type = 'I' then
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                          L_item_record,
                                          L_item_value)= FALSE then
         return FALSE;
      end if;
      ---
      if L_item_record.inventory_ind = 'N' then
         
         O_error_message:= SQL_LIB.CREATE_MSG('MRT_INVENTORY_ITEM',
                                               NULL,
                                               NULL,
                                               NULL);
          return FALSE;

      end if;

      if MRT_ATTRIB_SQL.GET_MRT_ITEM_ROW(O_error_message,
                                         L_rec_exists,
                                         L_mrt_item_rec,
                                         I_mrt_no,
                                         L_item_value) = FALSE then
         return FALSE;
      end if;
      ---
      if L_rec_exists then

         O_error_message:= SQL_LIB.CREATE_MSG('MRT_ITEM_EXISTS',
                                               to_char(I_mrt_no),
                                               NULL,
                                               NULL);  
         return FALSE;
      end if;
   ---
   end if;
      ---
   if (I_item_type = 'IL') and (I_item_list_rebuild_ind ='Y') then
      if ITEMLIST_BUILD_SQL.REBUILD_LIST (O_error_message,
                                          L_no_items,
                                          I_item_list)= FALSE then
         return FALSE;
      end if;
   end if;
      
   if I_item_type = 'IL' then
      if MRT_SQL.VALIDATE_ITEM_LIST(O_error_message,
                                    L_tran_level_ind,
                                    L_inventory_ind,
                                    L_unapproved_ind,
                                    I_item_list) = FALSE then
         return FALSE;
      end if;
      if L_tran_level_ind then
         O_mrt_itl_track := SQL_LIB.GET_MESSAGE_TEXT('MRT_ITL_TRACK',
                                                      NULL,
                                                      NULL,
                                                      NULL);  
      end if;
      if L_inventory_ind then
         O_mrt_itl_inv := SQL_LIB.GET_MESSAGE_TEXT('MRT_ITL_INV',
                                                    NULL,
                                                    NULL,
                                                    NULL);
      end if;
      if L_unapproved_ind then
         O_mrt_itl_app := SQL_LIB.GET_MESSAGE_TEXT('MRT_ITL_APP',
                                                    NULL,
                                                    NULL,
                                                    NULL);
       
      end if;
   end if;
             
   if I_item is NOT NULL or
      I_item_list is NOT NULL or
      I_item_parent is NOT NULL then
      if I_rtv_cost is NULL then
         if ITEMLOC_ATTRIB_SQL.GET_AV_UNIT_COST(O_error_message,
                                                L_av_cost,
                                                L_unit_cost,
                                                L_item_value,
                                                I_location) = FALSE then
            return FALSE;
         end if;

         if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                  L_system_options_rec) = FALSE then
            return FALSE;
         end if;

         if L_system_options_rec.std_av_ind = 'A' then
            L_rtv_cost := L_av_cost;
         elsif L_system_options_rec.std_av_ind = 'S' then
            L_rtv_cost := L_unit_cost;
         end if;

         --Get the currency of the location
         if LOCATION_ATTRIB_SQL.GET_LOCATION_CURRENCY(O_error_message,
                                                      L_multi_currency_exists,
                                                      L_currency_code,
                                                      'W',               --location will always be Warehouse
                                                      I_location) = FALSE then
            return FALSE;
         end if;
         
         open C_MRT_CURRENCY_CODE;         
         fetch C_MRT_CURRENCY_CODE into L_mrt_currency_code;         
         close C_MRT_CURRENCY_CODE;

         --Convert the values to mrt currency
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_rtv_cost,
                                 L_currency_code,
                                 L_mrt_currency_code,
                                 L_rtv_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
      else
         L_rtv_cost  := I_rtv_cost;
      end if;

      if MRT_SQL.CREATE_MRT_ITEM(O_error_message,
                                 I_mrt_no,
                                 I_item_type,
                                 L_item_value,
                                 L_rtv_cost,
                                 NVL(I_restock_pct,0),
                                 NVL(I_selected_ind,'Y'),
                                 I_supplier) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END INSERT_MRT_INFO;
------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_mrt_no            IN OUT   MRT.MRT_NO%TYPE,
                    I_function_key      IN OUT   VARCHAR2,
                    I_seq_no            IN OUT   NUMBER)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(75)   := 'MRT_SQL.CUSTOM_VAL';
   L_custom_obj_rec      CUSTOM_OBJ_REC :=  CUSTOM_OBJ_REC();

BEGIN

   L_custom_obj_rec.function_key:= I_function_key;
   L_custom_obj_rec.call_seq_no:= I_seq_no;
   L_custom_obj_rec.mrt_no:= I_mrt_no;

   --call the custom code for client specific order approval
   if CALL_CUSTOM_SQL.EXEC_FUNCTION(O_error_message,
                                    L_custom_obj_rec) = FALSE then
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CUSTOM_VAL;
------------------------------------------------------------------------------
END MRT_SQL;
/
