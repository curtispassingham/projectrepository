CREATE OR REPLACE PACKAGE BODY RMSSUB_STKORD_RECEIPT_VALIDATE AS

-------------------------------------------------------------------------------
-- Global values
-------------------------------------------------------------------------------
LP_invalid_param       VARCHAR2(100);
LP_invalid_value       VARCHAR2(10);
LP_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
INVALID_ERROR          EXCEPTION;
PARAM_ERROR            EXCEPTION;

-- Used to check for duplicate cartons within one receipt
TYPE carton_duplicate_tbl is table of SHIPSKU.CARTON%TYPE;
LP_ctn_dup_tbl  carton_duplicate_tbl;


-------------------------------------------------------------------------------
-- Public functions
-------------------------------------------------------------------------------
FUNCTION CHECK_RECEIPT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid                OUT   BOOLEAN,
                       O_validation_code      OUT   VARCHAR2,
                       I_rib_receipt_rec   IN       "RIB_Receipt_REC")
return BOOLEAN IS

   L_program                  VARCHAR2(61) := 'RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_RECEIPT';
   L_exists                   VARCHAR2(1)  := 'N';
   L_to_loc                   TSFHEAD.TO_LOC%TYPE;
   L_to_loc_type              TSFHEAD.TO_LOC_TYPE%TYPE;
   L_tsf_type                 TSFHEAD.TSF_TYPE%TYPE;
   L_physical_wh              WH.PHYSICAL_WH%TYPE;
   L_loc_type                 ITEM_LOC.LOC_TYPE%TYPE;

   cursor C_CHECK_TSF is
      select 'Y',
             th.to_loc,
             th.to_loc_type,
             th.tsf_type
        from tsfhead th
       where th.tsf_no = I_rib_receipt_rec.po_nbr;

   cursor C_CHECK_ALLOC is
      select 'Y'
        from alloc_header ah
       where ah.alloc_no = I_rib_receipt_rec.po_nbr;

   cursor C_CHECK_SHIPMENT is
      select 'Y'
        from shipment sh,
             shipsku sk
       where sh.shipment = sk.shipment
         and sh.to_loc_type = 'S'
         and sh.bol_no = I_rib_receipt_rec.asn_nbr
         and sk.distro_no = I_rib_receipt_rec.po_nbr
         and sk.qty_received > 0
         and NVL(sk.actual_receiving_store, sh.to_loc) != I_rib_receipt_rec.dc_dest_id
         and rownum = 1;


BEGIN
   -- Initialize output parameter.
   O_valid           := TRUE;
   O_validation_code := NULL;

   -- Check for required input parameters
   if I_rib_receipt_rec is NULL then
      LP_invalid_param := 'I_rib_receipt_rec';
      LP_invalid_value := 'NULL';
      raise PARAM_ERROR;
   end if;

   -- Check for required receipt record values
   if I_rib_receipt_rec.dc_dest_id is NULL then
      O_validation_code := 'RECEIPT_NULL_LOC';
      O_error_message := SQL_LIB.CREATE_MSG('RECEIPT_NULL_LOC',
                                            NULL,
                                            NULL,
                                            NULL);
      raise INVALID_ERROR;
   elsif I_rib_receipt_rec.document_type is NULL and I_rib_receipt_rec.receipt_type ='SK' then
      O_validation_code := 'RECEIPT_NULL_DOC_TYPE';
      O_error_message := SQL_LIB.CREATE_MSG('RECEIPT_NULL_DOC_TYPE',
                                            NULL,
                                            NULL,
                                            NULL);
      raise INVALID_ERROR;
   elsif I_rib_receipt_rec.asn_nbr is NULL then
      O_validation_code := 'RECEIPT_NULL_BOL';
      O_error_message := SQL_LIB.CREATE_MSG('RECEIPT_NULL_BOL',
                                            NULL,
                                            NULL,
                                            NULL);
      raise INVALID_ERROR;
   elsif I_rib_receipt_rec.receipt_type is NULL then
      O_validation_code := 'RECEIPT_NULL_RECEIPT_TYPE';
      O_error_message := SQL_LIB.CREATE_MSG('RECEIPT_NULL_RECEIPT_TYPE',
                                            NULL,
                                            NULL,
                                            NULL);
      raise INVALID_ERROR;
   -- Doc Type T is for Tranfers.  RDM also uses D and V for transfers
   elsif I_rib_receipt_rec.document_type not in ('T','A','D','V') then
      O_validation_code := 'INV_DOC_TYPE_01';
      O_error_message := SQL_LIB.CREATE_MSG('INV_DOC_TYPE_01',
                                            I_rib_receipt_rec.document_type,
                                            'T, A, D, or V',
                                            NULL);
      raise INVALID_ERROR;
   elsif I_rib_receipt_rec.receipt_type NOT in ('BL','SK') then
      O_validation_code := 'INV_RECEIPT_TYPE';
      O_error_message := SQL_LIB.CREATE_MSG('INV_RECEIPT_TYPE',
                                            I_rib_receipt_rec.receipt_type,
                                            'BL or SK',
                                            NULL);
      raise INVALID_ERROR;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;

   --check if loc passed in is valid
   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_loc_type,
                                   I_rib_receipt_rec.dc_dest_id) = FALSE then
      O_validation_code := 'INV_LOC_01';
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_01',
                                            I_rib_receipt_rec.dc_dest_id,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if LP_system_options_row.wrong_st_receipt_ind = 'N' then
      --check if the transfer has already been received at a location different from the loc passed
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SHIPMENT', 'SHIPMENT', NULL);
      open C_CHECK_SHIPMENT;

      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SHIPMENT', 'SHIPMENT', NULL);
      fetch C_CHECK_SHIPMENT into L_exists;

      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SHIPMENT', 'SHIPMENT', NULL);
      close C_CHECK_SHIPMENT;

      if L_exists = 'Y' then
         O_validation_code := 'INV_DISTRO_LOC_RCV';
         O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_LOC_RCV',
                                               I_rib_receipt_rec.po_nbr,
                                               I_rib_receipt_rec.dc_dest_id,
                                               NULL);
         raise INVALID_ERROR;
      end if;
   end if;

   -- For each receipt, clean out the duplicate carton table
   if LP_system_options_row.duplicate_receiving_ind = 'Y' then
      if LP_ctn_dup_tbl is NULL then
         LP_ctn_dup_tbl := carton_duplicate_tbl();
      else
         LP_ctn_dup_tbl.DELETE;
      end if;
   end if;

   -- BL = carton carton level receipt
   if  I_rib_receipt_rec.receipt_type = 'BL' then

      -- Carton level receipts will not validate the distro number

      -- Item level records must NOT exist
      -- (carton records may or may not exist)
      if  I_rib_receipt_rec.receiptdtl_tbl IS NOT NULL
      and I_rib_receipt_rec.receiptdtl_tbl.COUNT > 0 then
         O_validation_code := 'CTN_RCPT_HAS_ITEMS';
         O_error_message := SQL_LIB.CREATE_MSG('CTN_RCPT_HAS_ITEMS',
                                               NULL,
                                               NULL,
                                               NULL);
         raise INVALID_ERROR;
      end if;


   -- SK = item level receipt
   elsif I_rib_receipt_rec.receipt_type = 'SK' then
      -- Carton level records must NOT exist
      if  I_rib_receipt_rec.receiptcartondtl_tbl IS NOT NULL
      and I_rib_receipt_rec.receiptcartondtl_tbl.COUNT > 0 then
         O_validation_code := 'ITEM_RCPT_HAS_CARTONS';
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_RCPT_HAS_CARTONS',
                                               NULL,
                                               NULL,
                                               NULL);
         raise INVALID_ERROR;
      end if;

      -- Validate transfer if it is not null
      -- (it may be null for items in dummy cartons)
      if  I_rib_receipt_rec.po_nbr is NOT NULL
      and I_rib_receipt_rec.document_type = 'T' then

         L_exists := 'N';
         open  C_CHECK_TSF;
         fetch C_CHECK_TSF into L_exists, L_to_loc, L_to_loc_type, L_tsf_type;
         close C_CHECK_TSF;
         --
         if L_exists = 'N' then
            O_validation_code := 'INV_DISTRO';
            O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO',
                                                  I_rib_receipt_rec.po_nbr,
                                                  NULL,
                                                  NULL);
            raise INVALID_ERROR;
         end if;

         -- Validate transfer/location combination.
         -- Do not validate stores since this might be a wrong_store_receipt
         -- or walk_through_store, which are handled in stock_order_rcv_sql.
         if L_to_loc_type = 'W' then
            if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                             L_physical_wh,
                                             L_to_loc) = FALSE then
               return FALSE;
            end if;
            if L_physical_wh != I_rib_receipt_rec.dc_dest_id then
               O_validation_code := 'INV_DISTRO_LOC';
               O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_LOC',
                                                     I_rib_receipt_rec.po_nbr,
                                                     I_rib_receipt_rec.dc_dest_id,
                                                     NULL);
               raise INVALID_ERROR;
            end if;

         elsif L_to_loc_type = 'E'
         and   L_to_loc != I_rib_receipt_rec.dc_dest_id then
            O_validation_code := 'INV_DISTRO_LOC';
            O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_LOC',
                                                  I_rib_receipt_rec.po_nbr,
                                                  I_rib_receipt_rec.dc_dest_id,
                                                  NULL);
            raise INVALID_ERROR;
         end if;

      -- Validate allocation if it is not null.
      -- (it may be null for items in dummy cartons)
      elsif I_rib_receipt_rec.po_nbr is NOT NULL
      and   I_rib_receipt_rec.document_type = 'A' then

         -- Check if alloc is valid.  Since allocations are always
         -- to stores, do not validate the location since this might be a
         -- wrong_store_receipt or walk_through_store, which are handled
         -- in stock_order_rcv_sql.
         L_exists := 'N';
         open  C_CHECK_ALLOC;
         fetch C_CHECK_ALLOC into L_exists;
         close C_CHECK_ALLOC;
         --
         if L_exists = 'N' then
            O_validation_code := 'INV_DISTRO';
            O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO',
                                                  I_rib_receipt_rec.po_nbr,
                                                  NULL,
                                                  NULL);
            raise INVALID_ERROR;
         end if;

      end if;

   end if;

   -- Receipt is valid at this point
   return TRUE;

EXCEPTION
   when INVALID_ERROR then
      O_valid := FALSE;
      return TRUE;
   when PARAM_ERROR then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            LP_invalid_param,
                                            LP_invalid_value);
      return FALSE;
   when OTHERS then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_RECEIPT;

-------------------------------------------------------------------------------
FUNCTION CHECK_BOL(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   O_valid               IN OUT  BOOLEAN,
                   O_validation_code     IN OUT  VARCHAR2,
                   O_shipment            IN OUT  SHIPMENT.SHIPMENT%TYPE,
                   O_item_table          IN OUT  STOCK_ORDER_RCV_SQL.ITEM_TAB,
                   O_qty_expected_table  IN OUT  STOCK_ORDER_RCV_SQL.QTY_TAB,
                   O_inv_status_table    IN OUT  STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                   O_carton_table        IN OUT  STOCK_ORDER_RCV_SQL.CARTON_TAB,
                   O_distro_no_table     IN OUT  STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                   O_tampered_ind_table  IN OUT  STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB,
                   I_bol_no              IN      SHIPMENT.BOL_NO%TYPE,
                   I_to_loc              IN      SHIPMENT.TO_LOC%TYPE)
return BOOLEAN IS

   L_program                  VARCHAR2(61) := 'RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_BOL';
   L_adjust_type_table        STOCK_ORDER_RCV_SQL.ADJUST_TYPE_TAB;

   cursor C_SHIPMENT is
      select shipment
        from shipment
       where bol_no = I_bol_no
         and to_loc = I_to_loc;

   cursor C_SHIPSKU is
      select item,
             qty_expected,
             inv_status,
             carton,
             distro_no,
             adjust_type,
             tampered_ind
        from shipsku
       where shipment = O_shipment;

BEGIN

   -- Initialize output parameters.
   O_valid           := TRUE;
   O_validation_code := NULL;
   O_shipment        := NULL;
   --
   O_item_table.DELETE;
   O_qty_expected_table.DELETE;
   O_inv_status_table.DELETE;
   O_carton_table.DELETE;
   O_distro_no_table.DELETE;
   O_tampered_ind_table.DELETE;

   -- Check for required input
   if I_bol_no is NULL then
      LP_invalid_param := 'I_bol_no';
      LP_invalid_value := 'NULL';
   elsif I_to_loc is NULL then
      LP_invalid_param := 'I_to_loc';
      LP_invalid_value := 'NULL';
   end if;
   ---
   if LP_invalid_param is NOT NULL then
      raise PARAM_ERROR;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;

   -- Get shipment header info
   open  C_SHIPMENT;
   fetch C_SHIPMENT into O_shipment;
   close C_SHIPMENT;

   -- If O_shipment is NULL then no record was found for this BOL/to_loc
   if O_shipment is NULL then
      O_validation_code := 'INV_BOL_LOC';
      O_error_message := SQL_LIB.CREATE_MSG('INV_BOL_LOC',
                                            I_bol_no,
                                            I_to_loc,
                                            NULL);
      raise INVALID_ERROR;
   end if;

   -- Get shipment detail info
   open  C_SHIPSKU;
   fetch C_SHIPSKU BULK COLLECT into O_item_table,
                                     O_qty_expected_table,
                                     O_inv_status_table,
                                     O_carton_table,
                                     O_distro_no_table,
                                     L_adjust_type_table,
                                     O_tampered_ind_table;
   close C_SHIPSKU;

   -- Error out if no shipsku records were found.
   if L_adjust_type_table is NULL or L_adjust_type_table.COUNT < 1 then
      O_validation_code := 'INV_BOL_SHIPSKU';
      O_error_message := SQL_LIB.CREATE_MSG('INV_BOL_SHIPSKU',
                                            I_bol_no,
                                            NULL,
                                            NULL);
      raise INVALID_ERROR;
   end if;

   -- Error out if any shipsku records have been adjusted (reconciled)
   if LP_system_options_row.duplicate_receiving_ind = 'Y' then
      for i in L_adjust_type_table.FIRST..L_adjust_type_table.LAST loop
         if L_adjust_type_table(i) is NOT NULL then
            O_validation_code := 'BOL_HAS_RECONCILED_ITEM';
            O_error_message := SQL_LIB.CREATE_MSG('BOL_HAS_RECONCILED_ITEM',
                                                  I_bol_no,
                                                  NULL,
                                                  NULL);
            raise INVALID_ERROR;
         end if;
      end loop;
   end if;

   -- BOL is valid and all info has been collected.
   return TRUE;

EXCEPTION
   when INVALID_ERROR then
      O_valid := FALSE;
      return TRUE;
   when PARAM_ERROR then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            LP_invalid_param,
                                            LP_invalid_value);
      return FALSE;
   when OTHERS then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_BOL;

-------------------------------------------------------------------------------
FUNCTION CHECK_BOL_CARTON (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_received_ind    IN OUT   VARCHAR2,
                           I_bol_no          IN       SHIPMENT.BOL_NO%TYPE,
                           I_carton          IN       SHIPSKU.CARTON%TYPE)
return BOOLEAN IS

   L_qty_received    VARCHAR2(1)  := NULL;
   L_program         VARCHAR2(61) := 'RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_BOL_CARTON';

   cursor C_CTN_SHIPSKU is
      select 'x'
        from shipsku ss,
             shipment sh
       where ss.shipment = sh.shipment
         and sh.bol_no = I_bol_no
         and ss.carton = I_carton
         and (nvl(ss.qty_received,0) > 0
              or ss.adjust_type is NOT NULL
              or ss.actual_receiving_store is NOT NULL)
         and rownum = 1;

BEGIN

   -- Initialize output parameter.
   O_received_ind := 'N';

   -- Check for required input parameter.
   if I_bol_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_bol_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_carton is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_carton',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CTN_SHIPSKU',
                    'SHIPSKU',
                    'Bol_no: ' ||I_bol_no ||', Carton: '||I_carton);
   open C_CTN_SHIPSKU;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CTN_SHIPSKU',
                    'SHIPSKU',
                    'Bol_no: ' ||I_bol_no ||', Carton: '||I_carton);
   fetch C_CTN_SHIPSKU into L_qty_received;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CTN_SHIPSKU',
                    'SHIPSKU',
                    'Bol_no: ' ||I_bol_no ||', Carton: '||I_carton);
   close C_CTN_SHIPSKU;

   if L_qty_received = 'x' then
      O_received_ind := 'Y';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_BOL_CARTON;

-------------------------------------------------------------------------------
FUNCTION CHECK_CARTON(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_valid                     IN OUT  BOOLEAN,
                      O_validation_code           IN OUT  VARCHAR2,
                      O_ctn_shipment              IN OUT  SHIPMENT.SHIPMENT%TYPE,
                      O_ctn_to_loc                IN OUT  SHIPMENT.TO_LOC%TYPE,
                      O_ctn_bol_no                IN OUT  SHIPMENT.BOL_NO%TYPE,
                      O_item_table                IN OUT  STOCK_ORDER_RCV_SQL.ITEM_TAB,
                      O_qty_expected_table        IN OUT  STOCK_ORDER_RCV_SQL.QTY_TAB,
                      O_inv_status_table          IN OUT  STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                      O_carton_table              IN OUT  STOCK_ORDER_RCV_SQL.CARTON_TAB,
                      O_distro_no_table           IN OUT  STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                      O_tampered_ind_table        IN OUT  STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB,
                      O_wrong_store_ind           IN OUT  VARCHAR2,
                      O_wrong_store               IN OUT  SHIPMENT.TO_LOC%TYPE,
                      I_bol_no                    IN      SHIPMENT.BOL_NO%TYPE,
                      I_to_loc                    IN      SHIPMENT.TO_LOC%TYPE,
                      I_from_loc                  IN      SHIPMENT.FROM_LOC%TYPE,
                      I_from_loc_type             IN      SHIPMENT.FROM_LOC_TYPE%TYPE,
                      I_distro_type               IN      SHIPSKU.DISTRO_TYPE%TYPE,
                      I_rib_receiptcartondtl_rec  IN      "RIB_ReceiptCartonDtl_REC",
                      I_flag                      IN      VARCHAR2 DEFAULT NULL)
return BOOLEAN IS

   L_program                     VARCHAR2(61)  := 'RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_CARTON';
   --
   L_adjust_type_table           STOCK_ORDER_RCV_SQL.ADJUST_TYPE_TAB;
   L_shipment_table              STOCK_ORDER_RCV_SQL.SHIPMENT_TAB;
   --
   L_ctn_adjust_type             SHIPSKU.ADJUST_TYPE%TYPE;
   L_ctn_walk_through_store      SHIPMENT.TO_LOC%TYPE;
   --
   L_bol_exists                  VARCHAR2(1) := 'N';
   L_bol_to_loc                  SHIPMENT.TO_LOC%TYPE;

   TYPE act_rcv_store_tab IS TABLE OF SHIPSKU.ACTUAL_RECEIVING_STORE%TYPE INDEX BY BINARY_INTEGER;

   L_qty_received_table          STOCK_ORDER_RCV_SQL.QTY_TAB;
   L_act_rcv_store_table         ACT_RCV_STORE_TAB;
   
   L_qty_expected_table          STOCK_ORDER_RCV_SQL.QTY_TAB;
   L_item_table                  STOCK_ORDER_RCV_SQL.ITEM_TAB;
   L_inv_status_table            STOCK_ORDER_RCV_SQL.INV_STATUS_TAB;
   L_carton_table                STOCK_ORDER_RCV_SQL.CARTON_TAB;
   L_distro_no_table             STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB;
   L_tampered_ind_table          STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB;
   j                             NUMBER := 1;
   TYPE bol_no_tab IS TABLE OF SHIPMENT.BOL_NO%TYPE INDEX BY BINARY_INTEGER;
   L_bol_no_table   bol_no_tab;
   

   cursor C_CTN_SHIPSKU is
      select shs.shipment,
             shs.item,
             shs.qty_expected,
             shs.inv_status,
             shs.carton,
             shs.distro_no,
             shs.adjust_type,
             shs.tampered_ind,
             shs.qty_received,
             shs.actual_receiving_store,
             shp.bol_no
        from shipment shp,
             shipsku shs 
       where shs.carton = I_rib_receiptcartondtl_rec.container_id
             and shs.shipment=shp.shipment
             and shs.distro_type=I_distro_type
             and shp.order_no is null;

   cursor C_CTN_SHIPMENT is
      select bol_no,
             to_loc
        from shipment
       where shipment = O_ctn_shipment;

   cursor C_CHECK_WTS is
      select walk_through_store
        from walk_through_store wts
       where wts.walk_through_store = I_to_loc
         and wts.store              = O_ctn_to_loc;

BEGIN

   -- Initialize output parameters.
   O_valid           := TRUE;
   O_validation_code := NULL;
   O_ctn_shipment    := NULL;
   O_ctn_to_loc      := NULL;
   O_ctn_bol_no      := NULL;
   O_wrong_store_ind := 'N';
   O_wrong_store     := NULL;
   --
   O_item_table.DELETE;
   O_qty_expected_table.DELETE;
   O_inv_status_table.DELETE;
   O_carton_table.DELETE;
   O_distro_no_table.DELETE;
   O_tampered_ind_table.DELETE;

   -- Check for required input
   if I_rib_receiptcartondtl_rec is NULL then
      LP_invalid_param := 'I_rib_receiptcartondtl_rec';
      LP_invalid_value := 'NULL';
   elsif I_rib_receiptcartondtl_rec.carton_status_ind is NULL then
      LP_invalid_param := 'I_rib_receiptcartondtl_rec.carton_status_ind';
      LP_invalid_value := 'NULL';
   elsif I_rib_receiptcartondtl_rec.container_id is NULL then
      LP_invalid_param := 'I_rib_receiptcartondtl_rec.container_id';
      LP_invalid_value := 'NULL';
   elsif I_to_loc is NULL then
      LP_invalid_param := 'I_to_loc';
      LP_invalid_value := 'NULL';
   elsif I_bol_no is NULL then
      LP_invalid_param := 'I_bol_no';
      LP_invalid_value := 'NULL';
  -- Catch weight
   elsif I_rib_receiptcartondtl_rec.weight is NULL then
      if I_rib_receiptcartondtl_rec.weight_uom is not NULL then
         LP_invalid_param := 'I_rib_receiptcartondtl_rec.weight';
         LP_invalid_value := 'NULL';
      end if;
   elsif I_rib_receiptcartondtl_rec.weight_uom is NULL then
      LP_invalid_param := 'I_rib_receiptcartondtl_rec.weight_uom';
      LP_invalid_value := 'NULL';
   end if;
   -- Catch weight end

   if LP_invalid_param is NOT NULL then
      raise PARAM_ERROR;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;

   -- Check for duplicates
   if LP_system_options_row.duplicate_receiving_ind = 'Y' then

      if LP_ctn_dup_tbl is NOT NULL
      and LP_ctn_dup_tbl.COUNT > 0 then

         for i in LP_ctn_dup_tbl.FIRST..LP_ctn_dup_tbl.LAST loop
            if I_rib_receiptcartondtl_rec.container_id = LP_ctn_dup_tbl(i) then
               O_validation_code := 'DUP_CTN';
               O_error_message := SQL_LIB.CREATE_MSG('DUP_CTN',
                                                     I_rib_receiptcartondtl_rec.container_id,
                                                     NULL,
                                                     NULL);
               raise INVALID_ERROR;
            end if;
         end loop;

      end if;

      -- Carton is not a duplicate so add it to dup table
     if I_flag ='Y' or I_flag is NULL then
      LP_ctn_dup_tbl.EXTEND;
      LP_ctn_dup_tbl(LP_ctn_dup_tbl.COUNT) := I_rib_receiptcartondtl_rec.container_id;
      end if;

   end if;

   -- Get all line item info for this carton
   open  C_CTN_SHIPSKU;
   fetch C_CTN_SHIPSKU BULK COLLECT into L_shipment_table,
                                         L_item_table,
                                         L_qty_expected_table,
                                         L_inv_status_table,
                                         L_carton_table,
                                         L_distro_no_table,
                                         L_adjust_type_table,
                                         L_tampered_ind_table,
                                         L_qty_received_table,
                                         L_act_rcv_store_table,
                                         L_bol_no_table;
   close C_CTN_SHIPSKU;

   -- Check if carton is unwanded (does not exist on shipsku)
   if L_shipment_table is NULL or L_shipment_table.COUNT < 1 then
      O_validation_code := 'INV_CTN';
      O_error_message := SQL_LIB.CREATE_MSG('INV_CTN',
                                            I_rib_receiptcartondtl_rec.container_id,
                                            NULL,
                                            NULL);
      raise INVALID_ERROR;
   end if;

   -- Check if any item in the carton has been adjusted
   -- and carton was processed using wrong store receipt
   if LP_system_options_row.duplicate_receiving_ind = 'Y' then
      for i in L_adjust_type_table.FIRST..L_adjust_type_table.LAST loop
         if L_bol_no_table(i) = I_bol_no then
            if L_adjust_type_table(i) is NOT NULL then
               O_validation_code := 'CTN_HAS_RECONCILED_ITEM';
               O_error_message := SQL_LIB.CREATE_MSG('CTN_HAS_RECONCILED_ITEM',
                                                     I_rib_receiptcartondtl_rec.container_id,
                                                     NULL,
                                                     NULL);
               raise INVALID_ERROR;
            end if;
         end if;
      end loop;
   elsif LP_system_options_row.duplicate_receiving_ind = 'N' then
      for i in L_adjust_type_table.FIRST..L_adjust_type_table.LAST loop
         if L_bol_no_table(i) = I_bol_no then
            if L_adjust_type_table(i) is NOT NULL
            or L_qty_received_table(i) > 0
            or L_act_rcv_store_table(i) is NOT NULL then

               if RMSSUB_STKORD_RECEIPT_SQL.PERSIST_INSERT_DUP_RECEIPT(O_error_message,
                                                                       I_bol_no,
                                                                       L_carton_table(i),
                                                                       I_rib_receiptcartondtl_rec.carton_status_ind,
                                                                       I_to_loc,
                                                                       I_rib_receiptcartondtl_rec.receipt_xactn_type,
                                                                       I_rib_receiptcartondtl_rec.receipt_date,
                                                                       I_rib_receiptcartondtl_rec.receipt_nbr) = FALSE then
                  return FALSE;
               end if;

               O_validation_code := 'DUP_CTN';
               O_error_message := SQL_LIB.CREATE_MSG('DUP_CTN',
                                                     I_rib_receiptcartondtl_rec.container_id,
                                                     NULL,
                                                     NULL);
               raise INVALID_ERROR;

            end if;
         end if;
      end loop;
   end if;

   -- Get shipment info for carton
   if L_shipment_table is NOT NULL and L_shipment_table.COUNT > 0 then 
      for i IN 1 .. L_shipment_table.COUNT LOOP 
         if L_bol_no_table(i) = I_bol_no then
            O_ctn_shipment := L_shipment_table(i);      
            L_bol_exists := 'Y';
            exit;
         end if;
      end loop;
   end if;

   if I_rib_receiptcartondtl_rec.carton_status_ind = 'A' then
      if L_bol_exists = 'Y' then
   
         open  C_CTN_SHIPMENT;
         fetch C_CTN_SHIPMENT into O_ctn_bol_no,
                                   O_ctn_to_loc;
         close C_CTN_SHIPMENT;

         if O_ctn_to_loc != I_to_loc then
            O_validation_code := 'INV_BOL_LOC';
            O_error_message := SQL_LIB.CREATE_MSG('INV_BOL_LOC',
                                                  I_bol_no,
                                                  I_to_loc,
                                                  NULL);
            raise INVALID_ERROR;
         end if;
         ---
      else
      -- Validate 'actual' carton:  BOL must exist for input
      -- location and carton must exist for the BOL (carton's
      -- BOL must be the same as input BOL)
   
         O_validation_code := 'INV_CTN_BOL';
         O_error_message := SQL_LIB.CREATE_MSG('INV_CTN_BOL',
                                            I_bol_no,
                                            I_rib_receiptcartondtl_rec.container_id,
                                            NULL);
         raise INVALID_ERROR;
      end if;

   -- Validate 'overage' carton: BOL must exist for input
   -- location and carton must NOT exist for the BOL (carton's
   -- BOL must be different than input BOL).
   elsif I_rib_receiptcartondtl_rec.carton_status_ind = 'O' then

      if L_bol_exists = 'N'
      or L_bol_to_loc != I_to_loc then
         O_validation_code := 'INV_BOL_LOC';
         O_error_message := SQL_LIB.CREATE_MSG('INV_BOL_LOC',
                                               I_bol_no,
                                               I_to_loc,
                                               NULL);
         raise INVALID_ERROR;
      end if;
      ---
      if O_ctn_bol_no = I_bol_no then
         O_validation_code := 'BOL_CTN_NOT_OVERAGE';
         O_error_message := SQL_LIB.CREATE_MSG('BOL_CTN_NOT_OVERAGE',
                                               I_bol_no,
                                               I_rib_receiptcartondtl_rec.container_id,
                                               NULL);
         raise INVALID_ERROR;
      end if;

   -- Validate 'dummy' carton/BOL:  BOL must NOT exist
   -- for any location (dummy BOL), and from_loc/from_loc_type
   -- must be provided to handle error processing since these
   -- values may not be deduced from the BOL number.
   elsif I_rib_receiptcartondtl_rec.carton_status_ind = 'D' then
      if L_bol_exists = 'Y' then
         O_validation_code := 'BOL_NOT_DUMMY';
         O_error_message := SQL_LIB.CREATE_MSG('BOL_NOT_DUMMY',
                                               I_bol_no,
                                               NULL,
                                               NULL);
         raise INVALID_ERROR;
      end if;
      ---
      if I_from_loc is NULL
      or I_from_loc_type is NULL then
         O_validation_code := 'BOL_NULL_FROM_LOC';
         O_error_message := SQL_LIB.CREATE_MSG('BOL_NULL_FROM_LOC',
                                               I_bol_no,
                                               NULL,
                                               NULL);
         raise INVALID_ERROR;
      end if;

   -- carton_status_ind NOT in ('A','O','D')
   else

      O_validation_code := 'INV_CTN_ST_IND';
      O_error_message := SQL_LIB.CREATE_MSG('INV_CTN_ST_IND',
                                            I_rib_receiptcartondtl_rec.carton_status_ind,
                                            'A, O, or D',
                                            NULL);
      raise INVALID_ERROR;

   end if;

   ---------------------------------------------------------
   -- Carton is valid at this point.
   ---------------------------------------------------------

   -- For overage/dummy carton, if carton does not exist at this
   -- receipt location, then check if carton's intended location
   -- is a walk-through store for the receipt location.  If not,
   -- then we must do wrong store processing.
   if  I_rib_receiptcartondtl_rec.carton_status_ind != 'A'
   and O_ctn_to_loc != I_to_loc then
      ---
      open  C_CHECK_WTS;
      fetch C_CHECK_WTS into L_ctn_walk_through_store;
      close C_CHECK_WTS;
      --
      if L_ctn_walk_through_store is NULL then
         O_wrong_store_ind := 'Y';
         O_wrong_store     := I_to_loc;
      end if;
      ---
   end if;
   
   if L_shipment_table is NOT NULL and L_shipment_table.COUNT > 0 then 
      for i IN 1 .. L_shipment_table.COUNT LOOP 
         if L_bol_no_table(i) = I_bol_no then
            O_item_table(j) := L_item_table(i);
            O_qty_expected_table(j) := L_qty_expected_table(i);
            O_inv_status_table(j) := L_inv_status_table(i);
            O_carton_table(j) := L_carton_table(i);
            O_distro_no_table(j) := L_distro_no_table(i);
            O_tampered_ind_table(j) := L_tampered_ind_table(i);
            j:=j+1;
         end if;
   
      end loop;
   end if;
   

   return TRUE;

EXCEPTION
   when INVALID_ERROR then
      O_valid := FALSE;
      return TRUE;
   when PARAM_ERROR then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            LP_invalid_param,
                                            LP_invalid_value);
      return FALSE;
   when OTHERS then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_CARTON;

-------------------------------------------------------------------------------
FUNCTION GET_ITEMS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   O_valid            IN OUT  BOOLEAN,
                   O_validation_code  IN OUT  VARCHAR2,
                   O_receiptdtl_tbl   IN OUT  "RIB_ReceiptDtl_TBL",
                   I_receiptdtl_tbl   IN      "RIB_ReceiptDtl_TBL",
                   I_to_loc           IN      SHIPMENT.TO_LOC%TYPE,
                   I_distro_type      IN      SHIPSKU.DISTRO_TYPE%TYPE,
                   I_distro_no        IN      SHIPSKU.DISTRO_NO%TYPE)
return BOOLEAN IS

   L_program                  VARCHAR2(61)         := 'RMSSUB_STKORD_RECEIPT_VALIDATE.GET_ITEMS';
   L_BTS_sellable_TBL         "RIB_ReceiptDtl_TBL" := "RIB_ReceiptDtl_TBL"();
   L_BTS_sellable_count       NUMBER               := 0;
   L_receiptdtl_count         NUMBER               := 0;
   L_item_rec                 ITEM_MASTER%ROWTYPE;
   --
   L_BTS_orditem_qty_TBL      BTS_ORDITEM_QTY_TBL;
   L_inv_status               INV_STATUS_CODES.INV_STATUS%TYPE := NULL;
   L_sellable_item_TBL        ITEM_TBL                         := ITEM_TBL();
   L_sellable_qty_TBL         QTY_TBL                          := QTY_TBL();
   L_sellable_inv_status_TBL  INV_STATUS_TBL                   := INV_STATUS_TBL();

BEGIN

   -- Check for required input
   if I_distro_no is NULL then
      LP_invalid_param := 'I_distro_no';
      LP_invalid_value := 'NULL';
   elsif I_distro_type is NULL then
      LP_invalid_param := 'I_distro_type';
      LP_invalid_value := 'NULL';
   elsif I_to_loc is NULL then
      LP_invalid_param := 'I_to_loc';
      LP_invalid_value := 'NULL';
   elsif I_to_loc is NULL then
      LP_invalid_param := 'I_to_loc';
      LP_invalid_value := 'NULL';
   elsif I_receiptdtl_tbl is NULL then
      LP_invalid_param := 'I_receiptdtl_tbl';
      LP_invalid_value := 'NULL';
   end if;

   if LP_invalid_param is NOT NULL then
      raise PARAM_ERROR;
   end if;

   -- 1) Separate items in the message into 2 groups: BTS sellable items
   --    and the rest (O_receiptdtl_TBL).
   for i in I_receiptdtl_tbl.FIRST..I_receiptdtl_tbl.LAST loop

      -- Validate the item
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_rec,
                                         I_receiptdtl_tbl(i).item_id) = FALSE then
         O_validation_code := 'INV_ITEM_01';
         O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_01',
                                               I_receiptdtl_tbl(i).item_id,
                                               NULL,
                                               NULL);
         raise INVALID_ERROR;
      end if;

      if  L_item_rec.sellable_ind = 'Y'
      and L_item_rec.orderable_ind = 'N'
      and L_item_rec.item_xform_ind = 'Y' then
         --- Add item to BTS sellable table
         L_BTS_sellable_count := L_BTS_sellable_count + 1;
         L_BTS_sellable_TBL.EXTEND;
         L_BTS_sellable_TBL(L_BTS_sellable_count) := I_receiptdtl_tbl(i);
      else
         --- Add item to a receiptdtl table
         L_receiptdtl_count := L_receiptdtl_count + 1;
         O_receiptdtl_tbl.EXTEND;
         O_receiptdtl_tbl(L_receiptdtl_count) := I_receiptdtl_tbl(i);
      end if;

   end loop;

   -- 2) Find the orderables of the BTS sellables and add them to
   --    O_receiptdtl_TBL. This is the table of receipt records to process.
   if  L_BTS_sellable_TBL is not NULL
   and L_BTS_sellable_TBL.COUNT > 0 then

      for i in L_BTS_sellable_TBL.FIRST..L_BTS_sellable_TBL.LAST loop

         -- Convert the disposition to an inventory status
         if L_BTS_sellable_TBL(i).from_disposition IS NOT NULL then
            L_inv_status := NULL;
            if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                         L_inv_status,
                                         L_BTS_sellable_TBL(i).from_disposition) = FALSE then
               return FALSE;
            end if;
            if L_inv_status IS NULL then
               L_inv_status := -1;
            end if;
         else
            L_inv_status := -1;
         end if;

         L_sellable_item_TBL.EXTEND;
         L_sellable_item_TBL(L_sellable_item_TBL.COUNT) := L_BTS_sellable_TBL(i).item_id;
         ---
         L_sellable_qty_TBL.EXTEND;
         L_sellable_qty_TBL(L_sellable_qty_TBL.COUNT) := L_BTS_sellable_TBL(i).unit_qty;
         ---
         L_sellable_inv_status_TBL.EXTEND;
         L_sellable_inv_status_TBL(L_sellable_inv_status_TBL.COUNT) := L_inv_status;

      end loop;

      if I_distro_type in ('T','D','V') then
         if ITEM_XFORM_SQL.TSF_ORDERABLE_ITEM_INFO(O_error_message,
                                                   L_BTS_orditem_qty_TBL,
                                                   L_sellable_item_TBL,
                                                   L_sellable_qty_TBL,
                                                   L_sellable_inv_status_TBL,
                                                   I_distro_no) = FALSE then
            return FALSE;
         end if;
      else -- I_distro_type = 'A'
         if ITEM_XFORM_SQL.ALLOC_ORDERABLE_ITEM_INFO(O_error_message,
                                                     L_BTS_orditem_qty_TBL,
                                                     L_sellable_item_TBL,
                                                     L_sellable_qty_TBL,
                                                     I_distro_no,
                                                     I_to_loc) = FALSE then
            return FALSE;
         end if;
      end if;

      <<outer_loop>>
      for i in L_BTS_orditem_qty_TBL.FIRST..L_BTS_orditem_qty_TBL.LAST loop
         -- Find the corresponding orderable items of the sellable items.
         -- Add these to O_receiptdtl_tbl.
         <<inner_loop>>
         for j in L_BTS_sellable_TBL.FIRST .. L_BTS_sellable_TBL.LAST loop
            if  L_BTS_orditem_qty_TBL(i).sellable_item = L_BTS_sellable_TBL(j).item_id
            and L_BTS_orditem_qty_TBL(i).inv_status = L_sellable_inv_status_TBL(j) then
               --
               L_receiptdtl_count := L_receiptdtl_count + 1;
               O_receiptdtl_tbl.extend();
               O_receiptdtl_tbl(L_receiptdtl_count) := L_BTS_sellable_TBL(j);
               -- Replace original item detail with the orderable item
               O_receiptdtl_tbl(L_receiptdtl_count).item_id  := L_BTS_orditem_qty_TBL(i).orderable_item;
               O_receiptdtl_tbl(L_receiptdtl_count).unit_qty := L_BTS_orditem_qty_TBL(i).qty;

               exit inner_loop; -- break loop
            end if;
         end loop inner_loop;
      end loop outer_loop;

   end if;

   return TRUE;

EXCEPTION
   when INVALID_ERROR then
      O_valid := FALSE;
      return TRUE;
   when PARAM_ERROR then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            LP_invalid_param,
                                            LP_invalid_value);
      return FALSE;
   when OTHERS then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMS;

-------------------------------------------------------------------------------
FUNCTION CHECK_ITEM(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_valid               IN OUT  BOOLEAN,
                    O_validation_code     IN OUT  VARCHAR2,
                    I_rib_receipt_rec     IN      "RIB_Receipt_REC",
                    I_rib_receiptdtl_rec  IN      "RIB_ReceiptDtl_REC")
return BOOLEAN IS

   L_program              VARCHAR2(61) := 'RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_ITEM';
   L_tampered_ind         VARCHAR2(1);
   L_dummy_carton_ind     VARCHAR2(1);
   L_bol_exists_ind       VARCHAR2(1)  := null;
   L_shipment_exists_ind  VARCHAR2(1)  := null;
   L_carton_exists_ind    VARCHAR2(1)  := null;
   L_Item_exists_ind      VARCHAR2(1)  := null;

   cursor C_BOL is
      select 'Y'
        from shipment
       where bol_no = I_rib_receipt_rec.asn_nbr;

   cursor C_SHIPMENT is
      select 'Y'
        from shipment
       where bol_no = I_rib_receipt_rec.asn_nbr
         and to_loc = I_rib_receipt_rec.dc_dest_id;

   cursor C_CARTON_EXISTS is
      select 'x'
        from shipsku
       where carton = I_rib_receiptdtl_rec.container_id
         and rownum = 1;

   cursor C_ITEM_EXISTS is
    select 'x'
      from shipsku
     where item = I_rib_receiptdtl_rec.item_id
       and distro_no = I_rib_receipt_rec.po_nbr;

BEGIN

   O_valid := TRUE;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;

   L_tampered_ind     := NVL(I_rib_receiptdtl_rec.tampered_carton_ind, 'N');
   L_dummy_carton_ind := NVL(I_rib_receiptdtl_rec.dummy_carton_ind, 'N');

   if (L_tampered_ind = 'N' and L_dummy_carton_ind != 'Y' AND I_rib_receiptdtl_rec.container_id is NULL) then
      -- validate BOL
      L_bol_exists_ind := 'N';
      SQL_LIB.SET_MARK('OPEN', 'C_BOL', 'SHIPMENT', NULL);
      open C_BOL;

      SQL_LIB.SET_MARK('FETCH', 'C_BOL', 'SHIPMENT', NULL);
      fetch C_BOL into L_bol_exists_ind;

      SQL_LIB.SET_MARK('CLOSE', 'C_BOL', 'SHIPMENT', NULL);
      close C_BOL;
      --
      if L_bol_exists_ind = 'N' then
         O_validation_code := 'INV_BOL_01';
         O_error_message := SQL_LIB.CREATE_MSG('INV_BOL_01',
                                               I_rib_receipt_rec.asn_nbr,
                                               NULL,
                                               NULL);
         raise INVALID_ERROR;
      end if;

      -- Validate BOL/location
      L_shipment_exists_ind := 'N';
      SQL_LIB.SET_MARK('OPEN', 'C_SHIPMENT', 'SHIPMENT', NULL);
      open C_SHIPMENT;

      SQL_LIB.SET_MARK('FETCH', 'C_SHIPMENT', 'SHIPMENT', NULL);
      fetch C_SHIPMENT into L_shipment_exists_ind;

      SQL_LIB.SET_MARK('CLOSE', 'C_SHIPMENT', 'SHIPMENT', NULL);
      close C_SHIPMENT;
      --
      if L_shipment_exists_ind = 'N' then
         if NOT(LP_system_options_row.wrong_st_receipt_ind = 'Y' and I_rib_receipt_rec.document_type in ('A', 'T')) then
            O_validation_code := 'INV_BOL_LOC';
            O_error_message := SQL_LIB.CREATE_MSG('INV_BOL_LOC',
                                                  I_rib_receipt_rec.asn_nbr,
                                                  I_rib_receipt_rec.dc_dest_id,
                                                  NULL);
            raise INVALID_ERROR;
         end if;
      end if;
   end if;

   -- Packs will be received as packs, distro number is required for all non-dummy cartons.
   if LP_system_options_row.store_pack_comp_rcv_ind = 'N' then
      if  L_dummy_carton_ind != 'Y'
      and I_rib_receipt_rec.po_nbr is NULL then
         O_validation_code := 'ITEM_NULL_DISTRO';
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_NULL_DISTRO',
                                               NULL,
                                               NULL,
                                               NULL);
         raise INVALID_ERROR;
      end if;
   else
   -- Packs are received at the component level and tampered cartons will be
   -- processed via the batch program. In this case, distro is not required
   -- for dummy and tampered cartons.
      if  L_tampered_ind = 'N'
      and L_dummy_carton_ind != 'Y'
      and I_rib_receipt_rec.po_nbr is NULL then
         O_validation_code := 'ITEM_NULL_DISTRO';
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_NULL_DISTRO',
                                               NULL,
                                               NULL,
                                               NULL);
         raise INVALID_ERROR;
      end if;
   end if;

   -- If carton is not a 'dummy' carton and carton ID is NOT null then check if
   -- it is unwanded (does not exist on shipsku).
   if ((L_dummy_carton_ind != 'Y') AND (I_rib_receiptdtl_rec.container_id is NOT NULL)) then
      L_carton_exists_ind := 'N';
      SQL_LIB.SET_MARK('OPEN', 'C_CARTON_EXISTS', 'SHIPSKU', 'CONTAINER_ID: '||I_rib_receiptdtl_rec.container_id);
      open C_CARTON_EXISTS;

      SQL_LIB.SET_MARK('FETCH', 'C_CARTON_EXISTS', 'SHIPSKU', 'CONTAINER_ID: '||I_rib_receiptdtl_rec.container_id);
      fetch C_CARTON_EXISTS into L_carton_exists_ind;

      SQL_LIB.SET_MARK('CLOSE', 'C_CARTON_EXISTS', 'SHIPSKU', 'CONTAINER_ID: '||I_rib_receiptdtl_rec.container_id);
      close C_CARTON_EXISTS;

      if (L_carton_exists_ind = 'N') then
         L_item_exists_ind := 'N';
         SQL_LIB.SET_MARK('OPEN', 'C_ITEM_EXISTS', 'SHIPSKU', 'ITEM_ID: '||I_rib_receiptdtl_rec.item_id);
         open C_ITEM_EXISTS;

         SQL_LIB.SET_MARK('FETCH', 'C_ITEM_EXISTS', 'SHIPSKU', 'ITEM_ID: '||I_rib_receiptdtl_rec.item_id);
         fetch C_ITEM_EXISTS into L_item_exists_ind;

         SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_EXISTS', 'SHIPSKU', 'ITEM_ID: '||I_rib_receiptdtl_rec.item_id);
         close C_ITEM_EXISTS;
         if (L_item_exists_ind = 'N') then
            O_validation_code := 'INV_CTN';
            O_error_message := SQL_LIB.CREATE_MSG('INV_CTN',
                                                  I_rib_receiptdtl_rec.container_id,
                                                  NULL,
                                                  NULL);
            raise INVALID_ERROR;
         end if;
      end if;
   end if;

   -- Item-level details are valid at this point
   return TRUE;

EXCEPTION
   when INVALID_ERROR then
      O_valid := FALSE;
      return TRUE;
   when PARAM_ERROR then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            LP_invalid_param,
                                            LP_invalid_value);
      return FALSE;
   when OTHERS then
      if C_BOL%isopen then
         close C_BOL;
      end if;
      if C_SHIPMENT%isopen then
         close C_SHIPMENT;
      end if;
      if C_CARTON_EXISTS%isopen then
         close C_CARTON_EXISTS;
      end if;
      if C_ITEM_EXISTS%isopen then
         close C_ITEM_EXISTS;
      end if;
	  O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ITEM;

--------------------------------------------------------------------------------
END RMSSUB_STKORD_RECEIPT_VALIDATE;
/