CREATE OR REPLACE PACKAGE TRANSFER_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
-- Function Name: RESERVE
-- Purpose:       This package will retrieve all the line items of a
--                transfer and determine if the transfer quantities are
--                valid for transfer approval (i.e. transfer qty <
--                O_available).
-- Calls:         ITEMLOC_QUANTITY_SQL.GET_WH_CURRENT_AVAIL,
--                ITEMLOC_QUANTITY_SQL.GET_STORE_CURRENT_AVAIL
--                ITEMLOC_QUANTITY_SQL.GET_UNAVAILABLE
------------------------------------------------------------------------------
FUNCTION RESERVE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_item            IN OUT   ITEM_MASTER.ITEM%TYPE,
                 O_inv_status      IN OUT   TSFDETAIL.INV_STATUS%TYPE,
                 O_approve_flag    IN OUT   VARCHAR2,
                 O_available       IN OUT   NUMBER,
                 I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                 I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                 I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function Name: QUANTITY_AVAIL
-- Purpose:       This package will retrieve all of the approved transfer and RTV
--                quantities and subtract them from the total unavailable inventory
--                for the item/loc/inv_status combination when the inv_status is NOT NULL.
--                If the inv_status IS NULL then ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL
--                is called and it returns the current available inventory for the
--                item/loc/inv_status.
-- Calls:         ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL
-----------------------------------------------------------------------------
FUNCTION QUANTITY_AVAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_available       IN OUT   NUMBER,
                        I_item            IN       ITEM_MASTER.ITEM%TYPE,
                        I_inv_status      IN       TSFDETAIL.INV_STATUS%TYPE,
                        I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                        I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION: QUANTITY_BACKORDER
-- PURPOSE:  This private function will return the CUSTOMER_BACKORDER 
--           value from the ITEM_LOC_SOH table for the given item or
--           item parent/diff combination. It is called by the 
--           P_GET.QTY_BACKORDER procedure in the tsfdetail form in 
--           order to populate the B_APPLY.TI_backorder_qty field.
---------------------------------------------------------------------
FUNCTION QUANTITY_BACKORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_backorder_qty   IN OUT   ITEM_LOC_SOH.CUSTOMER_BACKORDER%TYPE,
                            I_item            IN       ITEM_LOC_SOH.ITEM%TYPE,
                            I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE,
                            I_to_loc          IN       ITEM_LOC_SOH.LOC%TYPE,
                            I_to_loc_type     IN       ITEM_LOC_SOH.LOC_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
$if $$UTPLSQL=TRUE $then
FUNCTION GET_CONV_INFOR(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_conv_factor      IN OUT   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        O_supp_pack_size   IN OUT   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        I_item             IN       TSFDETAIL.ITEM%TYPE,
                        I_uot              IN       VARCHAR2)
RETURN BOOLEAN;
$end
----------------------------------------------------------------------
-- FUNCTION: CHECK_CLEARANCE
-- PURPOSE:  This function will check to see if the item or style/store which was passed
--           to it is on clearance.  If a list of items is passed to the function
--           then the first item that is found on clearance in the list will
--           cause the program to return to the calling function.  If no items
--           in the list are on clearance at the location, the O_clearance
--           parameter will return = 'N'.
---------------------------------------------------------------------
FUNCTION CHECK_CLEARANCE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item            IN       TSFDETAIL.ITEM%TYPE,
                          I_itemlist        IN       SKULIST_DETAIL.SKULIST%TYPE,
                          I_item_parent     IN       ITEM_MASTER.ITEM%TYPE,
                          I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE,
                          I_location        IN       TSFHEAD.TO_LOC%TYPE,
                          O_clearance       IN OUT   ITEM_LOC.CLEAR_IND%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
$if $$UTPLSQL=TRUE $then
FUNCTION UPD_ITEM_RESV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                       I_from_loc        IN       TSFHEAD.TO_LOC%TYPE,
                       I_from_loc_type   IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                       I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION UPD_ITEM_EXP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_item              IN       ITEM_MASTER.ITEM%TYPE,
                      I_tsf_type          IN       TSFHEAD.TSF_TYPE%TYPE,
                      I_receive_as_type   IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                      I_to_loc            IN       TSFHEAD.TO_LOC%TYPE,
                      I_to_loc_type       IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                      I_tsf_qty           IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN;
$end
---------------------------------------------------------------------
-- FUNCTION: UPD_ITEM_RESV_EXP
-- PURPOSE:  This function will be used to update reserved qty and
--           expected qty's when tsfdetail.tsf_qty is inserted, updated or
--           deleted for one item.
---------------------------------------------------------------------
FUNCTION UPD_ITEM_RESV_EXP (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item            IN       tsfdetail.item%TYPE,
                            I_tsf_type        IN       tsfhead.tsf_type%TYPE,
                            I_tsf_qty         IN       tsfdetail.tsf_qty%TYPE,
                            I_from_loc_type   IN       tsfhead.from_loc_type%TYPE,
                            I_from_loc        IN       tsfhead.from_loc%TYPE,
                            I_to_loc_type     IN       tsfhead.to_loc_type%TYPE,
                            I_to_loc          IN       tsfhead.to_loc%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION: UPD_TSF_RESV_EXP
-- PURPOSE:  This function will be used to update reserved qty and
--           expected qty's for an entire tsf_no by calling
--           UPD_ITEM_RESV_EXP for each tsfdetail for the tsf_no.
---------------------------------------------------------------------
FUNCTION UPD_TSF_RESV_EXP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_no            IN       tsfdetail.tsf_no%TYPE,
                          I_add_delete_ind    IN       VARCHAR2,
                          I_appr_second_leg   IN       BOOLEAN)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- FUNCTION: UPD_NON_SELLABLE_QTYS_WHEN_DEL
-- PURPOSE: This function will update non sellable qty when the transfer get deleted.
---------------------------------------------------------------------------------------------
FUNCTION UPD_NON_SELLABLE_QTYS_WHEN_DEL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
I_tsf_no                 IN     tsfdetail.tsf_no%TYPE)
RETURN BOOLEAN;  

---------------------------------------------------------------------
-- FUNCTION: UPD_QTYS_WHEN_CLOSE
-- PURPOSE:  When a transfer is closed this function will be used to do
--           the following:
--           This function will be used to update tsf_reserved_qty for
--           source location, tsf_expected_qty for destination location
--           for each tsfdetail.tsf_qty(or fill_qty if ts_type = 'SA')
--           record found for the transfer by calling UPD_ITEM_RESV_EXP.
---------------------------------------------------------------------

FUNCTION UPD_QTYS_WHEN_CLOSE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_no          IN       tsfdetail.tsf_no%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION INSERT_TSFDETAIL_APPLY(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_invalid_item_ind   IN OUT   BOOLEAN,
                                O_invalid_qty        IN OUT   BOOLEAN,
                                I_item_type          IN       VARCHAR2,
                                I_tsf_no             IN       TSFDETAIL.TSF_NO%TYPE,
                                I_item               IN       ITEM_MASTER.ITEM%TYPE,
                                I_from_loc_type      IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                                I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                                I_to_loc_type        IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                                I_to_loc             IN       TSFHEAD.TO_LOC%TYPE,
                                I_qty_type           IN       VARCHAR2,
                                I_adjust_type        IN       VARCHAR2,
                                I_adjust_value       IN       NUMBER,
                                I_tsf_qty            IN       TSFDETAIL.TSF_QTY%TYPE,
                                I_inv_status         IN       TSFDETAIL.INV_STATUS%TYPE,
                                I_tsf_type           IN       TSFHEAD.TSF_TYPE%TYPE,
                                I_entity_type        IN       VARCHAR2 DEFAULT 'E',
                                I_restock_pct        IN       TSFDETAIL.RESTOCK_PCT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION UPD_QTYS_WHEN_CLOSE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_shipping_shortage_ind   IN OUT BOOLEAN,
                             I_process_leg             IN     VARCHAR2,
                             I_tsf_no                  IN     tsfdetail.tsf_no%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION: FUNCTION APPLY_PARENT_ITEMLIST
-- PURPOSE:  This function was initially written for the apply button in tsf.fmb.
--           It is called if the user wishes to transfer an item list or an item
--           parent/diff id combination.
---------------------------------------------------------------------
FUNCTION APPLY_PARENT_ITEMLIST(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_invalid_item_ind   IN OUT   BOOLEAN,
                               O_invalid_qty        IN OUT   BOOLEAN,
                               I_tsf_no             IN       TSFDETAIL.TSF_NO%TYPE,
                               I_item_type          IN       VARCHAR2,
                               I_item               IN       ITEM_MASTER.ITEM%TYPE,
                               I_diff_id            IN       DIFF_IDS.DIFF_ID%TYPE,
                               I_item_list          IN       SKULIST_HEAD.SKULIST%TYPE,
                               I_tsf_type           IN       TSFHEAD.TSF_TYPE%TYPE,
                               I_inv_status         IN       TSFDETAIL.INV_STATUS%TYPE,
                               I_qty_type           IN       VARCHAR2,
                               I_tsf_qty            IN       TSFDETAIL.TSF_QTY%TYPE,
                               I_adjust_type        IN       VARCHAR2,
                               I_adjust_value       IN       NUMBER,
                               I_from_loc_type      IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                               I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                               I_to_loc_type        IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                               I_to_loc             IN       TSFHEAD.TO_LOC%TYPE,
                               I_dept               IN       TSFHEAD.DEPT%TYPE,
                               I_entity_type        IN       VARCHAR2 DEFAULT 'E',
                               I_restock_pct        IN       TSFDETAIL.RESTOCK_PCT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION   : PACK_TRANSFERS_EXIST
-- PURPOSE    : This function checks whether or not tranfers exist
--              in status (I,A,E,S) for a particular pack item and
--              destination warehouse.
---------------------------------------------------------------------
FUNCTION PACK_TRANSFERS_EXIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_transfers_exist   IN OUT   BOOLEAN,
                              I_pack_no           IN       PACKITEM.PACK_NO%TYPE,
                              I_loc               IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: DETAILS_EXIST
-- Purpose      : This function will check if records exist on the Transfer Detail
--                table (tsfdetail) for the given transfer.
------------------------------------------------------------------------------------
FUNCTION DETAILS_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists          IN OUT   BOOLEAN,
                       I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: TSF_ITEM_EXIST
-- Purpose      : This function will check if a record exists on the Transfer Detail
--                table (tsfdetail) for the given item and inventory status
--                (inventory status parameter is optional).
--                This function is called by the tsfdetail form.
------------------------------------------------------------------------------------
FUNCTION TSF_ITEM_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists          IN OUT   BOOLEAN,
                        I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                        I_item            IN       TSFDETAIL.ITEM%TYPE,
                        I_inv_status      IN       TSFDETAIL.INV_STATUS%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: CREATE_FROM_EXISTING
-- Purpose      : This function will create a new transfer using the information
--                from the given transfer.
------------------------------------------------------------------------------------
FUNCTION CREATE_FROM_EXISTING(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_new_tsf_no      IN OUT   TSFHEAD.TSF_NO%TYPE,
                              I_old_tsf_no      IN       TSFHEAD.TSF_NO%TYPE,
                              I_status          IN       TSFHEAD.STATUS%TYPE,
                              I_username        IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name: TOTAL_COST_RETAIL
-- Purpose:       This function calculates the total cost, total retail,
--                and total up charges for the given transfer/from loc/to loc.
-------------------------------------------------------------------------
FUNCTION TOTAL_COST_RETAIL(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_total_cost_from              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_cost_to                IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_cost_prim              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_chrg_from              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_chrg_to                IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_chrg_prim              IN OUT   ORDLOC.UNIT_COST%TYPE,
                           O_total_retail_incl_vat_from   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           O_total_retail_incl_vat_to     IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           O_total_retail_incl_vat_prim   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           I_tsf_no                       IN       TSFHEAD.TSF_NO%TYPE,
                           I_tsf_type                     IN       TSFHEAD.TSF_TYPE%TYPE,
                           I_from_loc                     IN       TSFHEAD.FROM_LOC%TYPE,
                           I_from_loc_type                IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                           I_to_loc                       IN       TSFHEAD.TO_LOC%TYPE,
                           I_to_loc_type                  IN       TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: TOTAL_COST_RETAIL
-- Purpose:       This function calculates the total cost, total retail,
--                and total up charges for the given transfer.
--------------------------------------------------------------------------------
FUNCTION TOTAL_COST_RETAIL(
                  O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_total_cost_from              IN OUT   ORDLOC.UNIT_COST%TYPE,
                  O_total_cost_to                IN OUT   ORDLOC.UNIT_COST%TYPE,
                  O_total_cost_prim              IN OUT   ORDLOC.UNIT_COST%TYPE,
                  O_total_chrg_from              IN OUT   ORDLOC.UNIT_COST%TYPE,
                  O_total_chrg_to                IN OUT   ORDLOC.UNIT_COST%TYPE,
                  O_total_chrg_prim              IN OUT   ORDLOC.UNIT_COST%TYPE,
                  O_total_retail_incl_vat_from   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                  O_total_retail_incl_vat_to     IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                  O_total_retail_incl_vat_prim   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                  I_tsf_no                       IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- FUNCTION: GET_STATUS
-- PURPOSE:  This function retrieves the status, and transfer type
--           for a specific transfer number.
--------------------------------------------------------------------
FUNCTION GET_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_transfer_type   IN OUT   TSFHEAD.TSF_TYPE%TYPE,
                    O_status          IN OUT   TSFHEAD.STATUS%TYPE,
                    I_tsf_no          IN OUT   TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------
-- FUNCTION: VALIDATE_LOCATIONS
-- PURPOSE:  This function checks to see if the two locations passed
--           reside in the same physical warehouse.  The function
--           returns a 'Y' if they do, and a 'N' if they don't.  It
--           will be used to verify that the locations being placed
--           on a book transfer reside in the same physical warehouse.
--------------------------------------------------------------------
FUNCTION VALIDATE_LOCATIONS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_same_physical_wh   IN OUT   VARCHAR2,
                            I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                            I_to_loc             IN       TSFHEAD.TO_LOC%TYPE)

RETURN BOOLEAN;
--------------------------------------------------------------------
-- FUNCTION: VALIDATE_F_STORES
-- PURPOSE:  This function checks to see if the two locations passed
--           are both stockholding franchise stores. The function
--           returns a 'Y' if they are, and a 'N' if they aren't. If
--           both locations are warehouses, it returns NULL for 
--           O_both_sf_stores. It will be used to verify that if one
--           location on a transfer is a stockholding franchise store,
--           then the other is a stockholding franchise store also.
--------------------------------------------------------------------
FUNCTION VALIDATE_F_STORES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_sf_store            IN OUT   VARCHAR2,
                            I_from_loc            IN       TSFHEAD.FROM_LOC%TYPE,
                            I_to_loc              IN       TSFHEAD.TO_LOC%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------
-- FUNCTION: MANUAL_BT_EXECUTE
-- PURPOSE:  This new function will be used when a transfer is
--           being created manually between two virtual warehouses within
--           the same physical warehouse.  It will be called from
--           the transfer form when the transfer type is 'BT' (book
--           transfer).  The function will automatically handle
--           inventory updates and close out a transfer by calling
--           two new functions, TRANSFER_OUT_SQL.EXECUTE_BT, and
--           TRANSFER_IN_SQL.EXECUTE_BT.
--------------------------------------------------------------------
FUNCTION MANUAL_BT_EXECUTE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE,
                           I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                           I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                           I_transfer_type   IN       TSFHEAD.TSF_TYPE%TYPE)

RETURN BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION: AUTOMATIC_BT_EXECUTE
-- PURPOSE:  This new function will be used when a transfer is
--           being created automatically between two virtual warehouses within
--           the same physical warehouse. The function will automatically handle
--           inventory updates and close out a transfer by calling
--           two new functions, TRANSFER_OUT_SQL.EXECUTE_BT, and
--           TRANSFER_IN_SQL.EXECUTE_BT.
--------------------------------------------------------------------
FUNCTION AUTOMATIC_BT_EXECUTE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item            IN       TSFDETAIL.ITEM%TYPE,
                              I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                              I_to_loc          IN       TSFHEAD.TO_LOC%TYPE,
                              I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE)

RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name: NEXT_TSF_PO_LINK_NO
-- Purpose:       This package generates the next sequence number.
-----------------------------------------------------------------------
FUNCTION NEXT_TSF_PO_LINK_NO(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tsf_po_link_no   IN OUT   TSFDETAIL.TSF_PO_LINK_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name: CHECK_DETAILS
-- Purpose      : This function will check to see if this item is the last item on
--                an approved transfer.
------------------------------------------------------------------------------------
FUNCTION CHECK_DETAILS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name: DELETE_DOC_CLOSE_QUEUE
-- Purpose      : This function will delete the transfer record from the doc_close_queue
--                table when the transfer is closed online.
------------------------------------------------------------------------------------
FUNCTION DELETE_DOC_CLOSE_QUEUE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: UOT_EXIST
-- Purpose      : This function checks whether or not the entered
--                unit of transfer is valid.
------------------------------------------------------------------------------------
FUNCTION UOT_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists          IN OUT   BOOLEAN,
                   I_item            IN       TSFDETAIL.ITEM%TYPE,
                   I_uot             IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: WORK_ORDER_EXISTS
-- Purpose      : This function will be called when a finisher exists on a transfer.
--                It will verify that all items on the transfer have at least one finishing
--                activity associated with them.
------------------------------------------------------------------------------------
FUNCTION WORK_ORDER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_tsf_no          IN       TSF_WO_HEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name: GET_TSFHEAD_INFO
-- Purpose:       This function will retrieve the transfer header information
--                for a transfer that was entered. It retrieves information
--                from the view V_TSFHEAD and returns the whole row as an
--                output.  Because this function retrieves information from
--                V_TSFHEAD users can only view it if their security allows it.
FUNCTION GET_TSFHEAD_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_tsfhead         IN OUT   V_TSFHEAD%ROWTYPE,
                          I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_F_TSF_SHIPMENT
-- Purpose:       This function will check whether the given shipment is
--                associated with a Franchise Order or Franchise Return 
--                transfer by checking views V_TSFHEAD and V_SHIPSKU. If it is
--                associated with an FO/FR transfer, O_f_tsf_ind is returned as 
--                TRUE; if not, FALSE. Because this function retrieves 
--                information from V_TSFHEAD and V_SHIPSKU, users can only view 
--                the shipment and transfer(s) if their security allows it.
FUNCTION CHECK_F_TSF_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_f_tsf_ind       IN OUT   BOOLEAN,
                              I_shipment_no     IN       V_SHIPSKU.SHIPMENT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CREATE_TSFHEAD
-- Purpose:       This new function will create the transfer header information.
--                It will either create 1 or 2 transfer header records depends on the
--                presence of a finisher. If a finisher exists, call the function
--                CREATE_CHILD_TSFHEAD to create the child transfer header.
--                This function is called by Transfer Header form.
FUNCTION CREATE_TSFHEAD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tsf_no                  IN       TSFHEAD.TSF_NO%TYPE,
                        I_tsf_type                IN       TSFHEAD.TSF_TYPE%TYPE,
                        I_exp_dc_date             IN       TSFHEAD.EXP_DC_DATE%TYPE,
                        I_dept                    IN       TSFHEAD.DEPT%TYPE,
                        I_inventory_type          IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                        I_comments                IN       TSFHEAD.COMMENT_DESC%TYPE,
                        I_ext_ref_no              IN       TSFHEAD.EXT_REF_NO%TYPE,
                        I_from_loc                IN       TSFHEAD.FROM_LOC%TYPE,
                        I_from_loc_type           IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                        I_from_freight_code       IN       TSFHEAD.FREIGHT_CODE%TYPE,
                        I_from_routing_code       IN       TSFHEAD.ROUTING_CODE%TYPE,
                        I_finisher                IN       WH.WH%TYPE,
                        I_finisher_type           IN       VARCHAR2,
                        I_finisher_freight_code   IN       TSFHEAD.FREIGHT_CODE%TYPE,
                        I_finisher_routing_code   IN       TSFHEAD.ROUTING_CODE%TYPE,
                        I_to_loc                  IN       TSFHEAD.TO_LOC%TYPE,
                        I_to_loc_type             IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                        I_delivery_date           IN       TSFHEAD.DELIVERY_DATE%TYPE,
                        I_mrt_no                  IN       TSFHEAD.MRT_NO%TYPE         DEFAULT NULL,
                        I_not_after_date          IN       TSFHEAD.NOT_AFTER_DATE%TYPE DEFAULT NULL,
                        I_context_type            IN       TSFHEAD.CONTEXT_TYPE%TYPE   DEFAULT NULL,
                        I_context_value           IN       TSFHEAD.CONTEXT_VALUE%TYPE  DEFAULT NULL
                        )
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function Name: CREATE_CHILD_TSFHEAD
-- Purpose:       This function will create the child transfer header record for a
--                parent transfer. The from location and to location of the child are
--                the finisher and the final to location respectively.
--                This function is called only by the function CREATE_TSFHEAD and
--                UPDATE_TSFHEAD.
FUNCTION CREATE_CHILD_TSFHEAD(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tsf_parent_no     IN       TSFHEAD.TSF_PARENT_NO%TYPE,
                              I_tsf_type          IN       TSFHEAD.TSF_TYPE%TYPE,
                              I_exp_dc_date       IN       TSFHEAD.EXP_DC_DATE%TYPE,
                              I_exp_dc_eow_date   IN       TSFHEAD.EXP_DC_EOW_DATE%TYPE,
                              I_dept              IN       TSFHEAD.DEPT%TYPE,
                              I_inventory_type    IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                              I_comments          IN       TSFHEAD.COMMENT_DESC%TYPE,
                              I_ext_ref_no        IN       TSFHEAD.EXT_REF_NO%TYPE,
                              I_freight_code      IN       TSFHEAD.FREIGHT_CODE%TYPE,
                              I_routing_code      IN       TSFHEAD.ROUTING_CODE%TYPE,
                              I_from_loc          IN       TSFHEAD.FROM_LOC%TYPE,
                              I_from_loc_type     IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                              I_to_loc            IN       TSFHEAD.TO_LOC%TYPE,
                              I_to_loc_type       IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                              I_delivery_date     IN       TSFHEAD.DELIVERY_DATE%TYPE,
                              I_mrt_no            IN       TSFHEAD.MRT_NO%TYPE         DEFAULT NULL,
                              I_not_after_date    IN       TSFHEAD.NOT_AFTER_DATE%TYPE DEFAULT NULL,
                              I_context_type      IN       TSFHEAD.CONTEXT_TYPE%TYPE   DEFAULT NULL,
                              I_context_value     IN       TSFHEAD.CONTEXT_VALUE%TYPE  DEFAULT NULL
                             )
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: UPDATE_TSFHEAD
-- Purpose:       This function will update the transfer header record information of
--                both legs of the transfer. This includes field value changes: (i.e.
--                from, finisher and to loc) or even the creation or deletion of a
--                child transfer record. (depending upon the existence of a finisher
--                and/or the transfer status).
--                Call CREATE_CHILD_TSFHEAD, UPDATE_CHILD_TSFHEAD, or DELETE_CHILD_TSFHEAD
--                to either create, update, or delete child transfer header records.
FUNCTION UPDATE_TSFHEAD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tsf_no                  IN       TSFHEAD.TSF_NO%TYPE,
                        I_inventory_type          IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                        I_tsf_type                IN       TSFHEAD.TSF_TYPE%TYPE,
                        I_leg_1_status            IN       TSFHEAD.STATUS%TYPE,
                        I_leg_2_status            IN       TSFHEAD.STATUS%TYPE,
                        I_exp_dc_date             IN       TSFHEAD.EXP_DC_DATE%TYPE,
                        I_comments                IN       TSFHEAD.COMMENT_DESC%TYPE,
                        I_ext_ref_no              IN       TSFHEAD.EXT_REF_NO%TYPE,
                        I_from_loc                IN       TSFHEAD.FROM_LOC%TYPE,
                        I_from_loc_type           IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                        I_from_freight_code       IN       TSFHEAD.FREIGHT_CODE%TYPE,
                        I_from_routing_code       IN       TSFHEAD.ROUTING_CODE%TYPE,
                        I_finisher                IN       WH.WH%TYPE,
                        I_finisher_type           IN       VARCHAR2,
                        I_finisher_freight_code   IN       TSFHEAD.FREIGHT_CODE%TYPE,
                        I_finisher_routing_code   IN       TSFHEAD.ROUTING_CODE%TYPE,
                        I_in_process_leg_1        IN       TSFHEAD.STATUS%TYPE,
                        I_to_loc                  IN       TSFHEAD.TO_LOC%TYPE,
                        I_to_loc_type             IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                        I_in_process_leg_2        IN       TSFHEAD.STATUS%TYPE,
                        I_delivery_date           IN       TSFHEAD.DELIVERY_DATE%TYPE,
                        I_mrt_no                  IN       TSFHEAD.MRT_NO%TYPE,
                        I_not_after_date          IN       TSFHEAD.NOT_AFTER_DATE%TYPE,
                        I_context_type            IN       TSFHEAD.CONTEXT_TYPE%TYPE,
                        I_context_value           IN       TSFHEAD.CONTEXT_VALUE%TYPE,
                        I_dept                    IN       TSFHEAD.DEPT%TYPE
                       ) 
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: UPDATE_CHILD_TSFHEAD
-- Purpose:       This function will update the transfer header child record when the
--                parent is changed through the Transfer Maintenance form.
FUNCTION UPDATE_CHILD_TSFHEAD(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_child_tsf_no      IN       TSFHEAD.TSF_NO%TYPE,
                              I_tsfhead           IN       V_TSFHEAD%ROWTYPE,
                              I_inventory_type    IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                              I_status            IN       TSFHEAD.STATUS%TYPE,
                              I_exp_dc_date       IN       TSFHEAD.EXP_DC_DATE%TYPE,
                              I_exp_dc_eow_date   IN       TSFHEAD.EXP_DC_EOW_DATE%TYPE,
                              I_tsf_type          IN       TSFHEAD.TSF_TYPE%TYPE,
                              I_comments          IN       TSFHEAD.COMMENT_DESC%TYPE,
                              I_ext_ref_no        IN       TSFHEAD.EXT_REF_NO%TYPE,
                              I_from_loc          IN       TSFHEAD.FROM_LOC%TYPE,
                              I_from_loc_type     IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                              I_freight_code      IN       TSFHEAD.FREIGHT_CODE%TYPE,
                              I_routing_code      IN       TSFHEAD.ROUTING_CODE%TYPE,
                              I_to_loc            IN       TSFHEAD.TO_LOC%TYPE,
                              I_to_loc_type       IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                              I_delivery_date     IN       TSFHEAD.DELIVERY_DATE%TYPE,
                              I_mrt_no            IN       TSFHEAD.MRT_NO%TYPE,
                              I_not_after_date    IN       TSFHEAD.NOT_AFTER_DATE%TYPE,
                              I_context_type      IN       TSFHEAD.CONTEXT_TYPE%TYPE,
                              I_context_value     IN       TSFHEAD.CONTEXT_VALUE%TYPE,
                              I_dept              IN       TSFHEAD.DEPT%TYPE                            
                             ) 
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: DELETE_TSFHEAD
-- Purpose:       This function delete both the parent and child transfer(if exists).
FUNCTION DELETE_TSFHEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: DELETE_CHILD_TSFHEAD
-- Purpose:       This function will delete the child transfer header information.
--                A user can choose to remove a finisher on a header record if details
--                have not been added. Therefore, the existing child transfer needs to
--                be removed accordingly.
FUNCTION DELETE_CHILD_TSFHEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_child_tsf_no    IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: RESET_CHILD_TSF_DETAIL
-- Purpose:       When a multi-legged transfer's overall status is changed from Approved
--                to Input, the 2nd leg's (child's) detail records will be reset to the
--                original transfer items. Means the 2nd leg's detail will be deleted and
--                re-created. The items from the 1st leg will be copied to the 2nd leg,
--                the 2nd leg's inv_status, if the transfer's inventory tyep is 'U'navailable,
--                will be set equal to the the 1st leg's inv_status.
FUNCTION RESET_CHILD_TSF_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: GET_CHILD_TSF
-- Purpose:       Retrieves the child transfer of the passed in transfer. Used with
--                'multi-legged' transfers, being defined as transfers that pass through
--                a finisher. Function will throw an error if no child transfer is found.
-----------------------------------------------------------------------------------------
FUNCTION GET_CHILD_TSF(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_child_tsf_no    IN OUT TSFHEAD.TSF_NO%TYPE,
                       I_tsf_no          IN     TSFHEAD.TSF_NO%TYPE)

RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: GET_LOWEST_AVAIL_QTY
-- Purpose:       When and Item List, Parent Item, or Parent Item/Diff is included on a
--                transfer, this function will be called to loop through all items associated
--                and run each item through QUANTITY_AVAIL and then determine the lowest
--                quantity available of the items and return that number to the tsfdetail form.
-----------------------------------------------------------------------------------------
FUNCTION GET_LOWEST_AVAIL_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_lowest_avail    IN OUT   TSFDETAIL.TSF_QTY%TYPE,
                              I_item_list       IN       SKULIST_HEAD.SKULIST%TYPE,
                              I_item_parent     IN       TSFDETAIL.ITEM%TYPE,
                              I_diff_id         IN       ITEM_MASTER.DIFF_1%TYPE,
                              I_inv_status      IN       TSFDETAIL.INV_STATUS%TYPE,
                              I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                              I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: APPROVE
-- Purpose:       This function will be called to make appropriate stock_on_hand, tsf_reserved_qty
--                (or pack_comp_reserved), tsf_expected_qty(or pack_comp_exp) adjustments for sending
--                location, finisher and receiving location when a transfer with finishing is approved.
--                The appr_second_leg input is TRUE when ONLY the 2nd tsfleg is being approved
---------------------------------------------------------------------------------------------------
FUNCTION APPROVE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_leg_1_status      IN OUT   TSFHEAD.STATUS%TYPE,
                 I_tsf_no            IN       TSFHEAD.TSF_NO%TYPE,
                 I_child_tsf_no      IN       TSFHEAD.TSF_NO%TYPE,
                 I_tsf_type          IN       TSFHEAD.TSF_TYPE%TYPE,
                 I_appr_second_leg   IN       BOOLEAN,
                 I_inventory_type    IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                 I_from_loc_type     IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                 I_from_loc          IN       TSFHEAD.FROM_LOC%TYPE,
                 I_finisher_type     IN       V_TSFHEAD.FINISHER_TYPE%TYPE,
                 I_finisher          IN       TSFHEAD.TO_LOC%TYPE,
                 I_to_loc_type       IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                 I_to_loc            IN       TSFHEAD.TO_LOC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: GET_FINISHER_INFO
--
-- Purpose:
--    Determines whether a finisher is residing at the sending location or the
--    receiving location:
--
--    O_finisher_loc_ind = 'R'   --  finisher at receiving location
--                         'S'   --  finisher at sending location
--                         'B'   --  finisher at both locations
--                         'E'   --  external finisher
--                         NULL  --  no finishing
--
--    Also determines whether the finisher is in the same transfer entity as
--    the sending location or the receiving location or both:
--
--    O_finisher_entity_ind  = 'R'   -- finisher and recving loc in same entity
--                             'S'   -- finisher and sending loc in same entity
--                             'B'   -- intra transfer
--                             NULL  --  no finishing
------------------------------------------------------------------------------------------
FUNCTION GET_FINISHER_INFO(
            O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
            O_finisher_loc_ind      IN OUT   VARCHAR2,
            O_finisher_entity_ind   IN OUT   VARCHAR2,
            I_tsf_no                IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
--
-- Function Name: UPDATE_TSFDETAIL
--
-- Purpose:
--    This function is responsible for modifying the second leg of a multi-legged
--    transfer with Item Transformation and/or Item Packing.
------------------------------------------------------------------------------------------
FUNCTION UPDATE_TSFDETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: DELETE_CANCELLED_TSF
-- Purpose:
--    Deletes a newly created transfer that has been cancelled.  If you want to
--    delete a transfer that has already been approved, you should look into
--    the transfer purge batch program.
------------------------------------------------------------------------------------------
FUNCTION DELETE_CANCELLED_TSF(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tsf_no           IN       TSFHEAD.TSF_NO%TYPE,
                              I_child_tsf_no     IN       TSFHEAD.TSF_NO%TYPE,
                              I_overall_status   IN       VARCHAR2,
                              I_tsf_type         IN       TSFHEAD.TSF_TYPE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: CHECK_FOR_CHILD_TSF
--       Purpose: This function will determine if the tsf_no is for a single ('N') leg
--                tsf, the first ('F') leg, or the second ('S') leg.  If the tsf_no is
--                for the first leg of a multi-legged transfer the child tsf_no, child
--                to_loc, and child to_loc_type will be populated.  Otherwise only
--                O_tsf_leg will be populated.
------------------------------------------------------------------------------------------
FUNCTION CHECK_FOR_CHILD_TSF(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tsf_leg             IN OUT   VARCHAR2,
                             O_child_tsf_no        IN OUT   TSFHEAD.TSF_NO%TYPE,
                             O_child_to_loc        IN OUT   ITEM_LOC.LOC%TYPE,
                             O_child_to_loc_type   IN OUT   ITEM_LOC.LOC_TYPE%TYPE,
                             I_tsf_no              IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: UNAPPROVE
------------------------------------------------------------------------------------------
FUNCTION UNAPPROVE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_tsf_no             IN       TSFHEAD.TSF_NO%TYPE,
                   I_first_leg_status   IN       TSFHEAD.STATUS%TYPE,
                   I_inv_type           IN       TSFHEAD.INVENTORY_TYPE%TYPE,
                   I_from_loc           IN       TSFHEAD.FROM_LOC%TYPE,
                   I_from_loc_type      IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                   I_finisher           IN       TSFHEAD.TO_LOC%TYPE,
                   I_finisher_type      IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                   I_to_loc             IN       TSFHEAD.TO_LOC%TYPE,
                   I_to_loc_type        IN       TSFHEAD.TO_LOC_TYPE%TYPE,
                   I_tsf_type           IN       TSFHEAD.TSF_TYPE%TYPE)

RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_BASIC_TSFHEAD_INFO
-- Purpose:       This function will retrieve basic information from tsfhead
--                based on the input transfer and return a record of type
--                tsfhead_info.
--------------------------------------------------------------------------------
TYPE tsfhead_info is RECORD
     (tsf_no                   TSFHEAD.TSF_NO%TYPE,
      from_loc                 TSFHEAD.FROM_LOC%TYPE,
      from_loc_type            TSFHEAD.FROM_LOC_TYPE%TYPE,
      from_tsf_entity          WH.TSF_ENTITY_ID%TYPE,
      from_set_of_books_id     V_TSFHEAD.FROM_LOC_SOB_ID%TYPE,
      tsf_child_no             TSFHEAD.TSF_NO%TYPE,
      finisher                 TSFHEAD.FROM_LOC%TYPE,
      finisher_type            TSFHEAD.FROM_LOC_TYPE%TYPE,
      finisher_tsf_entity      WH.TSF_ENTITY_ID%TYPE,
      finisher_set_of_books_id V_TSFHEAD.FINISHER_SOB_ID%TYPE,
      to_loc                   TSFHEAD.FROM_LOC%TYPE,
      to_loc_type              TSFHEAD.FROM_LOC_TYPE%TYPE,
      to_tsf_entity            WH.TSF_ENTITY_ID%TYPE,
      to_set_of_books_id       V_TSFHEAD.TO_LOC_SOB_ID%TYPE);

FUNCTION GET_BASIC_TSFHEAD_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_tsfhead_info    IN OUT   TRANSFER_SQL.TSFHEAD_INFO,
                                I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: DELETE_WO_XFORM_PACK
--       Purpose: This function will delete the work order, item transformation and packing
--                record for an item deleted from the tsf detail form.
------------------------------------------------------------------------------------------
FUNCTION DELETE_WO_XFORM_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE,
                              I_qty             IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: IS_INTERCOMPANY
--       Purpose: This function will determine if an allocation or transfer should be 
--                treated as an inter- or intra-company transfer based on a set of 
--                system options (intercompany_transfer_ind, rtv_rac_tsf_ind), transfer
--                type (IC, RAC, RV) and the sending and receiving location's legal entities.
--                I_distro_type should be 'A' for allocation, 'T' for transfer. 
--                I_tsf_type is only passed in for transfer.
------------------------------------------------------------------------------------------
FUNCTION IS_INTERCOMPANY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_intercompany      IN OUT BOOLEAN, 
                         I_distro_type       IN     VARCHAR2,
                         I_tsf_type          IN     TSFHEAD.TSF_TYPE%TYPE,
                         I_from_loc          IN     TSFHEAD.FROM_LOC%TYPE,
                         I_from_loc_type     IN     TSFHEAD.FROM_LOC_TYPE%TYPE,
                         I_to_loc            IN     TSFHEAD.TO_LOC%TYPE,
                         I_to_loc_type       IN     TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_OPEN_ALLOC
--      Puropose: This function will check for any open allocations in ALLOC_HEADER
--                that is related to the transfer that is being closed or deleted.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_OPEN_ALLOC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists           IN OUT   BOOLEAN,
                          I_tsf_no           IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CLOSE_ALLOC
--      Puropose: This function will validate and close the allocation associated to the 
--                transfer and its lower level tiers if existing.
---------------------------------------------------------------------------------------------
FUNCTION CLOSE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_transfer_no     IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_QTY_AVAIL
--       Purpose: This function will check if any of the items in a given transfer 
--                has a transfer quantity greater than the available quantity.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_QTY_AVAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE,
                         I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                         I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_COST_RETAIL_VWH
--       Purpose: This function will return the averaged out cost and retail for the 
--                distributed warehouses, for Externally generated transfers in the Aprroved 
--                Status. 
---------------------------------------------------------------------------------------------
FUNCTION GET_COST_RETAIL_VWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_loc_cost        IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                             O_loc_retail      IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                             I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_tsf_qty         IN       TSFDETAIL.TSF_QTY%TYPE)
RETURN BOOLEAN;                             
---------------------------------------------------------------------------------------------
-- Function Name: GET_TSF_BOL
--       Purpose: This function will return the bol number for a given transfer
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_BOL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_bol_no          IN OUT   BOL_SHIPSKU.BOL_NO%TYPE,
                     I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;                             
---------------------------------------------------------------------------------------------
-- Function Name: CREATE_TRANSFER_HEAD_RECORDS
-- Purpose      : This function will fetch all those records from REPL_ITEM_LOC and     
--                RPL_NET_INVENTORY_TEMP tables for which the transfers need to be 
--                generated via replenishment.
-----------------------------------------------------------------------------------------
FUNCTION CREATE_TRANSFER_HEAD_RECORDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Name:       LOCK_TSFHEAD
-- Purpose:    This function will be called to lock the TSFHEAD record(s) of the tsf no 
--             being edited and those of its children. It is used by the form tsf.fmb.
---------------------------------------------------------------------------------------
FUNCTION LOCK_TSFHEAD (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Name   :  BULK_LOCK_TSFDETAIL
-- Purpose:  This function will lock the rows on tsfdetail table for all the
--           items on rowid_temp table. This function is called by reqext.pc
-------------------------------------------------------------------------------
FUNCTION BULK_LOCK_TSFDETAIL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;  
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_TSF_COST
--       Purpose: This function will update transfer cost when import PO related to transfer
--                has received cost adjustments.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_TSF_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                         I_item            IN       TSFDETAIL.ITEM%TYPE,
                         I_tsf_cost        IN       TSFDETAIL.TSF_COST%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_TSF_ORDER_NO
--       Purpose: This function will get order_no attached to an SG transfer.
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_ORDER_NO(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_order_no       OUT      TSFHEAD.ORDER_NO%TYPE,
                          I_tsf_no         IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DEL_UTIL_TSF
--       Purpose: This function will delete the util code for the transfer if the locatio 
--                is not localized  .
---------------------------------------------------------------------------------------------
FUNCTION DEL_UTIL_TSF(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_loc            IN      ITEM_LOC.LOC%TYPE,
                      I_tsf_no         IN      TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: POP_ORDCUST_DETAIL
--       Purpose: This function will create ordcust detail records for CO transfer.
---------------------------------------------------------------------------------------------
FUNCTION POP_ORDCUST_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
   
-- Bulk TSFHEAD insert tables
TYPE tsf_no_TBL                 is table of TSFHEAD.TSF_NO%TYPE                               INDEX BY BINARY_INTEGER;
TYPE source_wh_TBL              is table of REPL_ITEM_LOC.SOURCE_WH%TYPE                      INDEX BY BINARY_INTEGER;
TYPE location_TBL               is table of REPL_ITEM_LOC.LOCATION%TYPE                       INDEX BY BINARY_INTEGER;
TYPE repl_order_ctrl_TBL        is table of REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE                INDEX BY BINARY_INTEGER;
TYPE dept_TBL                   is table of REPL_ITEM_LOC.DEPT%TYPE                           INDEX BY BINARY_INTEGER;
TYPE tsf_type_TBL               is table of TSFHEAD.TSF_TYPE%TYPE                             INDEX BY BINARY_INTEGER;
TYPE curr_order_lead_time_TBL   is table of RPL_NET_INVENTORY_TMP.CURR_ORDER_LEAD_TIME%TYPE   INDEX BY BINARY_INTEGER;
TYPE delivery_slot_id_TBL       is table of STORE_ORDERS.DELIVERY_SLOT_ID%TYPE                INDEX BY BINARY_INTEGER;
TYPE need_date_TBL              is table of STORE_ORDERS.NEED_DATE%TYPE                       INDEX BY BINARY_INTEGER;  

P_tsf_no                   tsf_no_TBL;
P_source_wh                source_wh_TBL;  
P_location                 location_TBL;
P_repl_order_ctrl          repl_order_ctrl_TBL;
P_dept                     dept_TBL;
P_tsf_type                 tsf_type_TBL;
P_curr_order_lead_time     curr_order_lead_time_TBL;
P_delivery_slot_id         delivery_slot_id_TBL;
P_need_date                need_date_TBL;

P_tsfhead_size    NUMBER  := 0;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_RANGED_IND
--       Purpose: This function will update the item_loc ranged_ind to 'Y'.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_RANGED_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,  
                           I_item            IN       TSFDETAIL.ITEM%TYPE,
                           I_loc             IN       TSFHEAD.TO_LOC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_ATTRIB(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_from_finisher_ind   IN OUT WH.FINISHER_IND%TYPE,
                        O_to_finisher_ind     IN OUT WH.FINISHER_IND%TYPE,
                        O_intercompany        IN OUT BOOLEAN,
                        I_tsf_alloc_type      IN     SHIPSKU.DISTRO_TYPE%TYPE,
                        I_tsf_alloc_no        IN     SHIPSKU.DISTRO_NO%TYPE,
                        I_from_loc            IN     TSFHEAD.FROM_LOC%TYPE,
                        I_from_loc_type       IN     TSFHEAD.FROM_LOC_TYPE%TYPE,
                        I_to_loc              IN     TSFHEAD.TO_LOC%TYPE,
                        I_to_loc_type         IN     TSFHEAD.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN; 
---------------------------------------------------------------------------------------------
-- GET_TSF_TYPE
-- Queries tsfhead for the tsf_type.
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_TYPE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_tsf_type        IN OUT   TSFHEAD.TSF_TYPE%TYPE,
                      I_tsf_no          IN       SHIPSKU.DISTRO_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- FUNCTION GET_TOTAL_VAT 
--- Purposr: Get the total vat assigned to tsf
----------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_VAT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_total_vat       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                       O_total_cost      IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                       I_tsf_no          IN       SHIPSKU.DISTRO_NO%TYPE,
                       I_tsf_type        IN       TSFHEAD.TSF_TYPE%TYPE,
                       I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                       I_from_loc_type   IN       TSFHEAD.FROM_LOC_TYPE%TYPE,
                       I_to_loc          IN       TSFHEAD.FROM_LOC%TYPE,
                       I_to_loc_type     IN       TSFHEAD.FROM_LOC_TYPE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function: CUSTOM_VAL
-- Purpose : This function will be used for validationg the custom function.
--------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_tsf_no            IN OUT   TSFHEAD.TSF_NO%TYPE,
                    I_function_key      IN OUT   VARCHAR2,
                    I_seq_no            IN OUT   NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END TRANSFER_SQL;
/