
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY REPLENISHMENT_DAYS_SQL AS
-------------------------------------------------------------------
FUNCTION GET_DAY_OFFSET(O_error_message IN OUT  VARCHAR2,
                        O_offset        OUT     NUMBER)
RETURN BOOLEAN IS
   L_program  VARCHAR2(64) := 'REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET';

   -- date in cursor is a Sunday.  If week begins with Sunday, Offset = 0 
   CURSOR c_offset IS
      SELECT TO_NUMBER(TO_CHAR(TO_DATE('01191964','MMDDYYYY'),'D')) -1
        FROM DUAL;

BEGIN
   OPEN c_offset;
   FETCH c_offset into O_offset;
   CLOSE c_offset;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                L_program,to_char(SQLCODE));
      return FALSE;

END GET_DAY_OFFSET;
-------------------------------------------------------------------           
FUNCTION INSERT_DAY_SUPP(O_error_message IN OUT  VARCHAR2,
                         I_sequence_no   IN      sup_repl_day.sup_dept_seq_no%TYPE,
                         I_SUN           IN      VARCHAR2,
                         I_MON           IN      VARCHAR2,
                         I_TUE           IN      VARCHAR2,
                         I_WED           IN      VARCHAR2,
                         I_THU           IN      VARCHAR2,
                         I_FRI           IN      VARCHAR2,
                         I_SAT           IN      VARCHAR2)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'REPLENISHMENT_DAYS_SQL.INSERT_DAY_SUPP';
   L_offset        NUMBER(1);
   L_day           NUMBER(2) := 0;

BEGIN
   -- l_offset is between 0 and 6
   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset) = FALSE
   then
      return FALSE;
   end if;
   if I_sun  = 'Y' then
       insert into sup_repl_day
                    (sup_dept_seq_no,
                     weekday)
              values(I_sequence_no,
                     L_offset + 1);
   end if;          
   if I_mon = 'Y' then
      L_day := L_offset + 2;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into sup_repl_day
                   (sup_dept_seq_no,
                    weekday)
             values(I_sequence_no,
                    L_day);
   end if;       
   if I_tue = 'Y' then
      L_day := L_offset + 3;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into sup_repl_day
                   (sup_dept_seq_no,
                    weekday)
             values(I_sequence_no,
                    L_day);
   end if;
   if I_wed = 'Y' then
      L_day := L_offset + 4;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into sup_repl_day
                   (sup_dept_seq_no,
                    weekday)
             values(I_sequence_no,
                    L_day);
   end if;
   if I_thu = 'Y' then
      L_day := L_offset + 5;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into sup_repl_day
                   (sup_dept_seq_no,
                    weekday)
             values(I_sequence_no,
                    L_day);
   end if;
   if I_fri = 'Y' then
      L_day := L_offset + 6;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into sup_repl_day
                   (sup_dept_seq_no,
                    weekday)
             values(I_sequence_no,
                    L_day);
   end if;       
   if I_sat = 'Y' then
      L_day := L_offset + 7;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into sup_repl_day
                   (sup_dept_seq_no,
                    weekday)
             values(I_sequence_no,
                    L_day);
   end if;

   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                L_program,to_char(SQLCODE));
      return FALSE;
END INSERT_DAY_SUPP;
-------------------------------------------------------------------
------------------------------------------------------------------- 
FUNCTION INSERT_DAY_ITEM(O_error_message IN OUT  VARCHAR2,
                         I_item          IN      repl_day.item%TYPE,
                         I_location      IN      repl_day.location%TYPE,
                         I_loc_type      IN      repl_day.loc_type%TYPE,
                         I_SUN           IN      VARCHAR2,
                         I_MON           IN      VARCHAR2,
                         I_TUE           IN      VARCHAR2,
                         I_WED           IN      VARCHAR2,
                         I_THU           IN      VARCHAR2,
                         I_FRI           IN      VARCHAR2,
                         I_SAT           IN      VARCHAR2)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'REPLENISHMENT_DAYS_SQL.INSERT_DAY_ITEM';
   L_offset        NUMBER(1);
   L_day           NUMBER(2) := 0;
BEGIN

   SQL_LIB.SET_MARK('INSERT' , NULL, 'REPL_DAY', 'Item: ' || I_item ||
                    'Location: ' || to_char(I_location));

   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset) = FALSE
   then
      return FALSE;
   end if;
   if I_sun  = 'Y' then
      L_day := L_offset + 1;
      insert into repl_day
                   (item,
                    location,
                    loc_type,
                    weekday)
             values(I_item,
                    I_location,
                    I_loc_type,
                    L_day);
   end if;          
   if I_mon = 'Y' then
      L_day := L_offset + 2;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into repl_day
                   (item,
                    location,
                    loc_type,
                    weekday)
             values(I_item,
                    I_location,
                    I_loc_type,
                    L_day);
   end if;       
   if I_tue = 'Y' then
      L_day := L_offset + 3;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into repl_day
                   (item,
                    location,
                    loc_type,
                    weekday)
             values(I_item,
                    I_location,
                    I_loc_type,
                    L_day);
   end if;
   if I_wed = 'Y' then
      L_day := L_offset + 4;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into repl_day
                   (item,
                    location,
                    loc_type,
                    weekday)
             values(I_item,
                    I_location,
                    I_loc_type,
                    L_day);
   end if;
   if I_thu = 'Y' then
      L_day := L_offset + 5;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into repl_day
                   (item,
                    location,
                    loc_type,
                    weekday)
             values(I_item,
                    I_location,
                    I_loc_type,
                    L_day);
   end if;
   if I_fri = 'Y' then
      L_day := L_offset + 6;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into repl_day
                   (item,
                    location,
                    loc_type,
                    weekday)
             values(I_item,
                    I_location,
                    I_loc_type,
                    L_day);
   end if;       
   if I_sat = 'Y' then
      L_day := L_offset + 7;
      if L_day > 7
         then L_day := L_day - 7;
      end if; 
      insert into repl_day
                   (item,
                    location,
                    loc_type,
                    weekday)
             values(I_item,
                    I_location,
                    I_loc_type,
                    L_day);
   end if;                     

   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                L_program,to_char(SQLCODE));
      return FALSE;
END INSERT_DAY_ITEM;
-------------------------------------------------------------------
-------------------------------------------------------------------           
FUNCTION UPDATE_DAY_SUPP(O_error_message IN OUT   VARCHAR2,
                         I_sequence_no   IN       sup_repl_day.sup_dept_seq_no%TYPE,
                         I_SUN           IN       VARCHAR2,
                         I_MON           IN       VARCHAR2,
                         I_TUE           IN       VARCHAR2,
                         I_WED           IN       VARCHAR2,
                         I_THU           IN       VARCHAR2,
                         I_FRI           IN       VARCHAR2,
                         I_SAT           IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program       VARCHAR(50)  := 'REPLENISHMENT_DAYS_SQL.UPDATE_DAY_SUPP';
   L_table         VARCHAR2(30) := 'SUP_REPL_DAY';
   L_rowid         ROWID;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_offset        NUMBER(1);
   L_temp          VARCHAR2(1);
   L_base_day      NUMBER(2);

   cursor C_EXISTS is
    select rowid
      from sup_repl_day
     where sup_dept_seq_no = I_sequence_no
       and weekday = L_base_day
       for update nowait;

BEGIN

   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset) = FALSE
   then
      return FALSE;
   end if;

   FOR L_counter IN 1..7 LOOP

      if L_counter = 1 then L_temp := I_SUN; end if;
      if L_counter = 2 then L_temp := I_MON; end if;
      if L_counter = 3 then L_temp := I_TUE; end if;
      if L_counter = 4 then L_temp := I_WED; end if;
      if L_counter = 5 then L_temp := I_THU; end if;
      if L_counter = 6 then L_temp := I_FRI; end if;
      if L_counter = 7 then L_temp := I_SAT; end if;

      L_base_day := L_counter + L_offset;
      if L_base_day > 7
      then
         L_base_day := L_base_day - 7;
      end if;


      SQL_LIB.SET_MARK('OPEN' ,'C_EXISTS', 'SUP_REPL_DAY', NULL);
      open  C_EXISTS;
      SQL_LIB.SET_MARK('FETCH' ,'C_EXISTS', 'SUP_REPL_DAY', NULL);
      fetch C_EXISTS into L_rowid;
      if C_EXISTS%FOUND and L_temp = 'N' then
         SQL_LIB.SET_MARK('DELETE' ,NULL, 'SUP_REPL_DAY', NULL);
         delete from sup_repl_day
          where rowid = L_rowid;
      elsif C_EXISTS%NOTFOUND and L_temp = 'Y' then
         SQL_LIB.SET_MARK('INSERT' ,NULL, 'SUP_REPL_DAY', NULL);
         insert into sup_repl_day
               (sup_dept_seq_no,
                weekday)
         values(I_sequence_no,
                L_base_day);
      end if;
      SQL_LIB.SET_MARK('CLOSE' ,'C_EXISTS', 'SUP_REPL_DAY', NULL);
      close C_EXISTS;

   END LOOP;
RETURN TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_sequence_no));
      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                L_program,to_char(SQLCODE));
      return FALSE;
END UPDATE_DAY_SUPP;
-------------------------------------------------------------------
-------------------------------------------------------------------           
FUNCTION UPDATE_DAY_ITEM(O_error_message IN OUT   VARCHAR2,
                         I_item          IN       repl_day.item%TYPE,
                         I_location      IN       repl_day.location%TYPE,
                         I_loc_type      IN       repl_day.loc_type%TYPE,
                         I_SUN           IN       VARCHAR2,
                         I_MON           IN       VARCHAR2,
                         I_TUE           IN       VARCHAR2,
                         I_WED           IN       VARCHAR2,
                         I_THU           IN       VARCHAR2,
                         I_FRI           IN       VARCHAR2,
                         I_SAT           IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'REPLENISHMENT_DAYS_SQL.UPDATE_DAY_ITEM';
   L_wkday         SUP_REPL_DAY.WEEKDAY%TYPE;
   L_rowid         ROWID;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_offset        NUMBER(1);
   L_base_day      NUMBER(2);
   L_temp          VARCHAR2(1);

   cursor C_EXISTS is
    select rowid
      from repl_day
     where item     = I_item
       and location = I_location
       and weekday  = L_base_day
       for update nowait;

BEGIN

   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset) = FALSE
   then
      return FALSE;
   end if;

   FOR L_counter IN 1..7 LOOP

      if L_counter = 1 then L_temp := I_SUN; end if;
      if L_counter = 2 then L_temp := I_MON; end if;
      if L_counter = 3 then L_temp := I_TUE; end if;
      if L_counter = 4 then L_temp := I_WED; end if;
      if L_counter = 5 then L_temp := I_THU; end if;
      if L_counter = 6 then L_temp := I_FRI; end if;
      if L_counter = 7 then L_temp := I_SAT; end if;

      L_base_day := L_counter + L_offset;
      if L_base_day > 7
      then
         L_base_day := L_base_day - 7;
      end if;


      SQL_LIB.SET_MARK('OPEN' ,'C_EXISTS', 'REPL_DAY', NULL);
      open  C_EXISTS;
      SQL_LIB.SET_MARK('FETCH' ,'C_EXISTS', 'REPL_DAY', NULL);
      fetch C_EXISTS into L_rowid;
      if C_EXISTS%FOUND and L_temp = 'N' then
         SQL_LIB.SET_MARK('DELETE' ,NULL, 'REPL_DAY', NULL);
         delete from repl_day
          where rowid = L_rowid;
      elsif C_EXISTS%NOTFOUND and L_temp = 'Y' then
         SQL_LIB.SET_MARK('INSERT' ,NULL, 'REPL_DAY', NULL);
         insert into repl_day
                    (item,
                    location,
                    loc_type,
                    weekday)
             values (I_item,
                    I_location,
                    I_loc_type,
                    L_base_day);
      end if;
      SQL_LIB.SET_MARK('CLOSE' ,'C_EXISTS', 'REPL_DAY', NULL);
      close C_EXISTS;

   END LOOP;

RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            I_item,
                                            to_char(I_location),
                                            to_char(L_wkday));
      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;
END UPDATE_DAY_ITEM;
-------------------------------------------------------------------
-------------------------------------------------------------------           
FUNCTION QUERY_DAY_SUPP(O_error_message IN OUT  VARCHAR2,
                        I_sequence_no   IN      sup_repl_day.sup_dept_seq_no%TYPE,
                        O_SUN           IN OUT  VARCHAR2,
                        O_MON           IN OUT  VARCHAR2,
                        O_TUE           IN OUT  VARCHAR2,
                        O_WED           IN OUT  VARCHAR2,
                        O_THU           IN OUT  VARCHAR2,
                        O_FRI           IN OUT  VARCHAR2,
                        O_SAT           IN OUT  VARCHAR2)
 RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'REPLENISHMENT_DAYS_SQL.QUERY_DAY_SUPP';
   L_offset        NUMBER(1);
   L_base_day      NUMBER(1)    := 0;

   cursor C_EXISTS is
      select weekday
        from sup_repl_day
       where sup_dept_seq_no = I_sequence_no;

 BEGIN

   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset) = FALSE
   then
      return FALSE;
   end if;

   O_SUN :='N';
   O_MON :='N';
   O_TUE :='N';
   O_WED :='N';
   O_THU :='N';
   O_FRI :='N';
   O_SAT :='N';

   FOR rec in C_EXISTS LOOP
      L_base_day := rec.weekday-L_offset;
      if L_base_day <= 0
      then
         L_base_day := L_base_day+7;
      end if;   

      if L_base_day = 1 
         then O_sun := 'Y'; 
      elsif L_base_day = 2 
         then O_MON := 'Y';
      elsif L_base_day = 3 
         then O_TUE := 'Y';
      elsif L_base_day = 4 
         then O_WED := 'Y';
      elsif L_base_day = 5 
         then O_THU := 'Y';
      elsif L_base_day = 6 
         then O_FRI := 'Y';
      else 
         O_SAT := 'Y'; 
      end if;

   END LOOP;
    ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                             L_program ,to_char(SQLCODE));
   return FALSE;
END QUERY_DAY_SUPP;
-------------------------------------------------------------------
-------------------------------------------------------------------           
FUNCTION QUERY_DAY_ITEM(O_error_message IN OUT  VARCHAR2,
                        I_item          IN      repl_day.item%TYPE,
                        I_location      IN      repl_day.location%TYPE,
                        O_SUN           IN OUT  VARCHAR2,
                        O_MON           IN OUT  VARCHAR2,
                        O_TUE           IN OUT  VARCHAR2,
                        O_WED           IN OUT  VARCHAR2,
                        O_THU           IN OUT  VARCHAR2,
                        O_FRI           IN OUT  VARCHAR2,
                        O_SAT           IN OUT  VARCHAR2,
                        I_repl_attr_id  IN      repl_attr_update_head.repl_attr_id%TYPE DEFAULT NULL, 
                        I_mra           IN      repl_attr_update_head.mra_update%TYPE   DEFAULT NULL
                        )
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64) := 'REPLENISHMENT_DAYS_SQL.QUERY_DAY_ITEM';
   L_offset                     NUMBER(1);
   L_base_day                   NUMBER(1) := 0;
   cursor C_EXISTS is
      select weekday
        from repl_day
       where item     = I_item
         and location = I_location;

   cursor C_MASTER_REPL_ATTR is
      select * 
        from master_repl_attr
       where item     = I_item
         and location = I_location;
   cursor C_REPL_ATTR_UPDATE_HEAD is
      select *
        from repl_attr_update_head
       where repl_attr_id     = I_repl_attr_id;
   L_master_repl_attr_rec       C_MASTER_REPL_ATTR%ROWTYPE      :=  NULL;
   L_repl_attr_update_head_rec  C_REPL_ATTR_UPDATE_HEAD%ROWTYPE :=  NULL;   
BEGIN
   if I_repl_attr_id is NULL AND NVL(I_mra, 'N') = 'N' then
      if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                               L_offset) = FALSE
         then
         return FALSE;
      end if;
      O_SUN :='N';
      O_MON :='N';
      O_TUE :='N';
      O_WED :='N';
      O_THU :='N';
      O_FRI :='N';
      O_SAT :='N';
      FOR rec in C_EXISTS LOOP
         L_base_day := rec.weekday-L_offset;
         if L_base_day <= 0
         then
            L_base_day := L_base_day+7;
         end if;   

         if L_base_day = 1 
            then O_sun := 'Y'; 
         elsif L_base_day = 2 
            then O_MON := 'Y';
         elsif L_base_day = 3 
            then O_TUE := 'Y';
         elsif L_base_day = 4 
            then O_WED := 'Y';
         elsif L_base_day = 5 
            then O_THU := 'Y';
         elsif L_base_day = 6 
            then O_FRI := 'Y';
         else 
            O_SAT := 'Y'; 
         end if;
      END LOOP;
   elsif I_repl_attr_id IS NULL AND I_mra = 'Y' then
      SQL_LIB.SET_MARK('OPEN' ,'C_MASTER_REPL_ATTR', 'MASTER_REPL_ATTR', NULL);
      open  C_MASTER_REPL_ATTR;
      SQL_LIB.SET_MARK('FETCH' ,'C_MASTER_REPL_ATTR', 'MASTER_REPL_ATTR', NULL);
      fetch C_MASTER_REPL_ATTR into L_master_repl_attr_rec;
      SQL_LIB.SET_MARK('CLOSE' ,'C_MASTER_REPL_ATTR', 'MASTER_REPL_ATTR', NULL);
      close C_MASTER_REPL_ATTR;      
      O_SUN := nvl(L_master_repl_attr_rec.sunday_ind, 'N');
      O_MON := nvl(L_master_repl_attr_rec.monday_ind, 'N');
      O_TUE := nvl(L_master_repl_attr_rec.tuesday_ind, 'N');
      O_WED := nvl(L_master_repl_attr_rec.wednesday_ind, 'N');
      O_THU := nvl(L_master_repl_attr_rec.thursday_ind, 'N');
      O_FRI := nvl(L_master_repl_attr_rec.friday_ind, 'N');
      O_SAT := nvl(L_master_repl_attr_rec.saturday_ind, 'N');  
   elsif I_repl_attr_id IS NOT NULL AND I_mra = 'N' then 
      SQL_LIB.SET_MARK('OPEN' ,'C_REPL_ATTR_UPDATE_HEAD', 'REPL_ATTR_UPDATE_HEAD', NULL);
      open  C_REPL_ATTR_UPDATE_HEAD;
      SQL_LIB.SET_MARK('FETCH' ,'C_REPL_ATTR_UPDATE_HEAD', 'REPL_ATTR_UPDATE_HEAD', NULL);
      fetch C_REPL_ATTR_UPDATE_HEAD into L_repl_attr_update_head_rec;
      SQL_LIB.SET_MARK('CLOSE' ,'C_REPL_ATTR_UPDATE_HEAD', 'REPL_ATTR_UPDATE_HEAD', NULL);
      close C_REPL_ATTR_UPDATE_HEAD;
      O_SUN := nvl(L_repl_attr_update_head_rec.sunday_ind, 'N');
      O_MON := nvl(L_repl_attr_update_head_rec.monday_ind, 'N');
      O_TUE := nvl(L_repl_attr_update_head_rec.tuesday_ind, 'N');
      O_WED := nvl(L_repl_attr_update_head_rec.wednesday_ind, 'N');
      O_THU := nvl(L_repl_attr_update_head_rec.thursday_ind, 'N');
      O_FRI := nvl(L_repl_attr_update_head_rec.friday_ind, 'N');
      O_SAT := nvl(L_repl_attr_update_head_rec.saturday_ind, 'N');
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_REPL_DATA',
                                            'NULL',
                                            L_program,
                                            'NULL');                                          
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                            L_program ,to_char(SQLCODE));
      return FALSE;
END QUERY_DAY_ITEM;

-------------------------------------------------------------------------
FUNCTION CONVERT_TO_OFFSET_DAY(O_error_message  IN OUT VARCHAR2,
                               O_offset_day     IN OUT SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE,
                I_day      IN     SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE)
RETURN BOOLEAN IS
   L_offset_day       NUMBER(1);
BEGIN
   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset_day) = FALSE
   then
      return FALSE;
   end if;

   O_offset_day := I_day - L_offset_day;

   if O_offset_day < 1 then
      O_offset_day := O_offset_day + 7;
   end if;

   RETURN TRUE;
EXCEPTION
   when OTHERS then 
     O_error_message := SQL_LIB.CREATE_MSG
                     ('PACKAGE_ERROR',
                      SQLERRM,
             'REPLENISHMENT_DAYS_SQL.CONVERT_TO_OFFSET_DAY',
                      to_char(SQLCODE));
     RETURN FALSE;
END CONVERT_TO_OFFSET_DAY;
------------------------------------------------------------------------
FUNCTION CONVERT_FROM_OFFSET_DAY (O_error_message   IN OUT VARCHAR2,
                                  O_day             IN OUT SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE,
                                  O_day_name     IN OUT VARCHAR2,
                                  I_offset_day      IN SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE)
RETURN BOOLEAN IS
   L_day            NUMBER(2);
   L_offset               NUMBER(1);
   L_error_message        VARCHAR2(255);
BEGIN
   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset) = FALSE then
      return FALSE;
   end if;
   L_day := I_offset_day + L_offset;

   if L_day > 7 then
      L_day := (L_day - 7);
   end if;
   O_day := L_day;
   if LANGUAGE_SQL.GET_CODE_DESC (o_error_message,
                        'DAYS',
                                  O_day,
                   O_day_name) = FALSE then
      return FALSE;
   end if;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG
                     ('PACKAGE_ERROR',
                      SQLERRM,
                      'REPLENISHMENT_DAYS_SQL.CONVERT_FROM_OFFSET_DAY',
                      to_char (SQLCODE));
      RETURN FALSE;

END CONVERT_FROM_OFFSET_DAY ;
------------------------------------------------------------------------------
END REPLENISHMENT_DAYS_SQL; 
/


