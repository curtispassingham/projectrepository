CREATE OR REPLACE PACKAGE BODY RMSMFM_ORDCUST AS

LP_error_status   VARCHAR2(1) := NULL;

--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                              O_message         IN OUT NOCOPY   RIB_OBJECT,
                              O_routing_info    IN OUT NOCOPY   RIB_ROUTINGINFO_TBL,
                              I_ordcust_no      IN              ORDCUST_PUB_INFO.ORDCUST_NO%TYPE,
                              I_seq_no          IN              ORDCUST_PUB_INFO.SEQ_NO%TYPE)
RETURN BOOLEAN;

FUNCTION BUILD_MSG_OBJECT(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                          O_message         IN OUT NOCOPY   "RIB_FulfilOrdCfmDesc_REC",
                          O_routing_info    IN OUT NOCOPY   RIB_ROUTINGINFO_TBL,
                          I_ordcust_no      IN              ORDCUST_PUB_INFO.ORDCUST_NO%TYPE)
RETURN BOOLEAN;

FUNCTION LOCK_THE_BLOCK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_queue_locked    IN OUT   BOOLEAN,
                        I_seq_no          IN       ORDCUST_PUB_INFO.SEQ_NO%TYPE)
RETURN BOOLEAN;

PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT   VARCHAR2,
                        O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message         IN OUT   RIB_OBJECT,
                        O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                        O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                        I_ordcust_no      IN       ORDCUST_PUB_INFO.ORDCUST_NO%TYPE,
                        I_seq_no          IN       ORDCUST_PUB_INFO.SEQ_NO%TYPE);
--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_message_type   IN       ORDCUST_PUB_INFO.MESSAGE_TYPE%TYPE,
                I_ordcust_no     IN       ORDCUST.ORDCUST_NO%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)                             := 'RMSMFM_ORDCUST.ADDTOQ';
   L_status_code              VARCHAR2(1)                              := NULL;
   L_max_details_to_publish   RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := NULL;
   L_minutes_time_lag         RIB_SETTINGS.MINUTES_TIME_LAG%TYPE       := NULL;
   L_num_threads              RIB_SETTINGS.NUM_THREADS%TYPE            := NULL;

BEGIN

   if I_message_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_message_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_ordcust_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ordcust_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details_to_publish,
                                L_num_threads,
                                L_minutes_time_lag,
                                RMSMFM_ORDCUST.FAMILY);

   if L_status_code = API_CODES.UNHANDLED_ERROR then
      return FALSE;
   end if;

   insert into ordcust_pub_info(seq_no,
                                ordcust_no,
                                thread_no,
                                message_type,
                                family,
                                pub_status)
                         values(ordcust_mfsequence.NEXTVAL,
                                I_ordcust_no,
                                MOD(I_ordcust_no, L_num_threads)+1,
                                I_message_type,
                                RMSMFM_ORDCUST.FAMILY,
                                'U');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code     IN OUT   VARCHAR2,
                 O_error_message   IN OUT   VARCHAR2,
                 O_message_type    IN OUT   VARCHAR2,
                 O_message         IN OUT   RIB_OBJECT,
                 O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                 O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                 I_num_threads     IN       NUMBER DEFAULT 1,
                 I_thread_val      IN       NUMBER DEFAULT 1)
IS

   L_program        VARCHAR2(64)                     := 'RMSMFM_ORDCUST.GETNXT';
   L_seq_no         ORDCUST_PUB_INFO.SEQ_NO%TYPE     := NULL;
   L_ordcust_no     ORDCUST_PUB_INFO.ORDCUST_NO%TYPE := NULL;
   L_seq_limit      ORDCUST_PUB_INFO.SEQ_NO%TYPE     := 0;
   L_queue_locked   BOOLEAN                          := FALSE;
   PROGRAM_ERROR    EXCEPTION;

   cursor C_QUEUE is
      select o.seq_no,
             o.ordcust_no
        from ordcust_pub_info o
       where o.seq_no = (select min(o2.seq_no)
                           from ordcust_pub_info o2
                          where o2.thread_no  = I_thread_val
                            and o2.pub_status = 'U'
                            and o2.seq_no     > L_seq_limit)
         and o.thread_no = I_thread_val;

BEGIN

   -- status of 'H'ospital
   LP_error_status := API_CODES.HOSPITAL;

   LOOP
      L_seq_no       := NULL;
      L_ordcust_no   := NULL;
      O_message      := NULL;
      ---
      open C_QUEUE;
      fetch C_QUEUE into L_seq_no,
                         L_ordcust_no;
      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;

      close C_QUEUE;

      if LOCK_THE_BLOCK(O_error_message,
                        L_queue_locked,
                        L_seq_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked = FALSE then
         if PROCESS_QUEUE_RECORD(O_error_message,
                                 O_message,
                                 O_routing_info,
                                 L_ordcust_no,
                                 L_seq_no) = FALSE then
            raise PROGRAM_ERROR;
         end if;

         exit;
      else
         L_seq_limit := L_seq_no;
      end if;

   END LOOP;

   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code  := API_CODES.NEW_MSG;
      O_bus_obj_id   := RIB_BUSOBJID_TBL(L_ordcust_no);
      O_message_type := RMSMFM_ORDCUST.LP_cre_type;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_ordcust_no,
                    L_seq_no);
END GETNXT;
--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                              O_message         IN OUT NOCOPY   RIB_OBJECT,
                              O_routing_info    IN OUT NOCOPY   RIB_ROUTINGINFO_TBL,
                              I_ordcust_no      IN              ORDCUST_PUB_INFO.ORDCUST_NO%TYPE,
                              I_seq_no          IN              ORDCUST_PUB_INFO.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(64)               := 'RMSMFM_ORDCUST.PROCESS_QUEUE_RECORD';
   L_status_code                VARCHAR2(1)                := NULL;
   L_max_details_to_publish     NUMBER                     := NULL;
   L_num_threads                NUMBER                     := NULL;
   L_minutes_time_lag           NUMBER                     := NULL;
   L_rib_fulfilordcfmdesc_rec   "RIB_FulfilOrdCfmDesc_REC" := NULL;
   L_table                      VARCHAR2(30)               := 'ORDCUST_PUB_TEMP';
   L_key1                       VARCHAR2(30)               := I_ordcust_no;
   RECORD_LOCKED                EXCEPTION;
   PRAGMA                       EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_PUB_TEMP is
      select 'x'
        from ordcust_pub_temp
       where ordcust_no = I_ordcust_no
         for update nowait;

BEGIN

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details_to_publish,
                                L_num_threads,
                                L_minutes_time_lag,
                                RMSMFM_ORDCUST.FAMILY);

   if L_status_code = API_CODES.UNHANDLED_ERROR then
      return FALSE;
   end if;

   if BUILD_MSG_OBJECT(O_error_message,
                       L_rib_fulfilordcfmdesc_rec,
                       O_routing_info,
                       I_ordcust_no) = FALSE then
      return FALSE;
   end if;

   O_message :=  L_rib_fulfilordcfmdesc_rec;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   update ordcust_pub_info
      set pub_status = 'P'
    where seq_no = I_seq_no;

   open C_LOCK_PUB_TEMP;
   close C_LOCK_PUB_TEMP;

   delete from ordcust_pub_temp
      where ordcust_no = I_ordcust_no;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_QUEUE_RECORD;
--------------------------------------------------------------------------------
FUNCTION BUILD_MSG_OBJECT(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                          O_message         IN OUT NOCOPY   "RIB_FulfilOrdCfmDesc_REC",
                          O_routing_info    IN OUT NOCOPY   RIB_ROUTINGINFO_TBL,
                          I_ordcust_no      IN              ORDCUST_PUB_INFO.ORDCUST_NO%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64)        := 'RMSMFM_ORDCUST.BUILD_MSG_OBJECT';

   cursor C_BUILD_MSG is
      select "RIB_FulfilOrdCfmDesc_REC"(0,                      --rib_oid
                                        oc.customer_order_no,   --customer_order_no varchar2(48),
                                        oc.fulfill_order_no,    --fulfill_order_no varchar2(48),
                                        oc.status,              --confirm_type varchar2(1),
                                        th.tsf_no,              --confirm_no number(12),
                                        --FulfilOrdCfmDtl_TBL 
                                        CAST(MULTISET(select "RIB_FulfilOrdCfmDtl_REC"(0,               --rib_oid
                                                                                       od.item,         --item varchar2(25),
                                                                                       od.ref_item,     --ref_item varchar2(25),
                                                                                       td.tsf_qty,      --confirm_qty number(12,4),
                                                                                       od.standard_uom) --confirm_qty_uom varchar2(4)
                                                        from ordcust_detail od,
                                                             tsfdetail td
                                                       where od.ordcust_no = oc.ordcust_no
                                                         and td.tsf_no = th.tsf_no
                                                         and td.item = od.item) as "RIB_FulfilOrdCfmDtl_TBL"))
        from ordcust oc,
             tsfhead th
       where oc.ordcust_no = I_ordcust_no
         and oc.tsf_no = th.tsf_no(+) --outer join to account for ORDCUST created in 'X' status with no associated tsf
         and oc.source_loc_type in ('ST', 'WH')  --store or warehouse
         and oc.status in ('X', 'P') --only publish exceptional cases where orders that are NOT created or only partially created
       UNION ALL
      select "RIB_FulfilOrdCfmDesc_REC"(0,
                                        oc.customer_order_no,   --customer_order_no varchar2(48),
                                        oc.fulfill_order_no,    --fulfill_order_no varchar2(48),
                                        oc.status,              --confirm_type varchar2(1),
                                        oh.order_no,            --confirm_no number(12),
                                        --FulfilOrdCfmDtl_TBL 
                                        CAST(MULTISET(select "RIB_FulfilOrdCfmDtl_REC"(0,               --rib_oid
                                                                                       od.item,         --item varchar2(25),
                                                                                       od.ref_item,     --ref_item varchar2(25),
                                                                                       ol.qty_ordered,  --confirm_qty number(12,4),
                                                                                       od.standard_uom) --confirm_qty_uom varchar2(4)
                                                        from ordcust_detail od,
                                                             ordloc ol
                                                       where od.ordcust_no = oc.ordcust_no
                                                         and ol.order_no = oh.order_no
                                                         and ol.item = od.item
                                                         and ol.location = oc.fulfill_loc_id) as "RIB_FulfilOrdCfmDtl_TBL"))
        from ordcust oc,
             ordhead oh
       where oc.ordcust_no = I_ordcust_no
         and oc.order_no = oh.order_no(+) --outer join to account for ORDCUST created in 'X' status with no associated order;
         and oc.source_loc_type = 'SU'   --supplier
         and oc.status in ('X', 'P');  --only publish exceptional cases where orders that are NOT created or only partially created

BEGIN

   open C_BUILD_MSG;
   fetch C_BUILD_MSG into O_message;
   close C_BUILD_MSG;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_MSG_OBJECT;
--------------------------------------------------------------------------------
FUNCTION LOCK_THE_BLOCK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_queue_locked    IN OUT   BOOLEAN,
                        I_seq_no          IN       ORDCUST_PUB_INFO.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'RMSMFM_ORDCUST.LOCK_THE_BLOCK';
   L_table         VARCHAR2(30) := 'ORDCUST_PUB_INFO';
   L_key1          VARCHAR2(30) := I_seq_no;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from ordcust_pub_info
       where seq_no = I_seq_no
         for update nowait;

BEGIN

   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            NULL);
      O_queue_locked := TRUE;
      return TRUE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_THE_BLOCK;
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code     IN OUT   VARCHAR2,
                    O_error_message   IN OUT   VARCHAR2,
                    O_message_type    IN OUT   VARCHAR2,
                    O_message         IN OUT   RIB_OBJECT,
                    O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                    O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                    I_ref_object      IN       RIB_OBJECT)
IS

   L_program        VARCHAR2(64)                     := 'RMSMFM_ORDCUST.PUB_RETRY';
   L_seq_no         ORDCUST_PUB_INFO.SEQ_NO%TYPE     := NULL;
   L_ordcust_no     ORDCUST_PUB_INFO.ORDCUST_NO%TYPE := NULL;
   L_queue_locked   BOOLEAN                          := FALSE;
   PROGRAM_ERROR    EXCEPTION;

   cursor C_RETRY_QUEUE is
      select ordcust_no
        from ordcust_pub_info
       where seq_no = L_seq_no
         and pub_status = API_CODES.HOSPITAL;

BEGIN
   -- status of 'H'ospital
   LP_error_status := API_CODES.HOSPITAL;

   L_seq_no := O_routing_info(1).value;
   O_message := NULL;

   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_ordcust_no;

   if C_RETRY_QUEUE%NOTFOUND then
      close C_RETRY_QUEUE;
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   close C_RETRY_QUEUE;

   if LOCK_THE_BLOCK(O_error_message,
                     L_queue_locked,
                     L_seq_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then
      if PROCESS_QUEUE_RECORD(O_error_message,
                              O_message,
                              O_routing_info,
                              L_ordcust_no,
                              L_seq_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if O_message is NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id := RIB_BUSOBJID_TBL(L_ordcust_no);
         O_message_type := RMSMFM_ORDCUST.LP_cre_type;
      end if;
   else
      O_status_code := API_CODES.HOSPITAL; 
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_ordcust_no,
                    L_seq_no);
END PUB_RETRY;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT   VARCHAR2,
                        O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message         IN OUT   RIB_OBJECT,
                        O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                        O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                        I_ordcust_no      IN       ORDCUST_PUB_INFO.ORDCUST_NO%TYPE,
                        I_seq_no          IN       ORDCUST_PUB_INFO.SEQ_NO%TYPE)
IS

   L_program            VARCHAR2(64)                   := 'RMSMFM_ORDCUST.HANDLE_ERRORS';
   L_error_type         RTK_ERRORS.RTK_TEXT%TYPE       := NULL;
   L_cust_order_no      ORDCUST.CUSTOMER_ORDER_NO%TYPE := NULL;
   L_fulfill_order_no   ORDCUST.FULFILL_ORDER_NO%TYPE  := NULL;
   L_confirm_type       ORDCUST.STATUS%TYPE            := NULL;

   cursor C_ORDCUST is
      select customer_order_no,
             fulfill_order_no,
             status confirm_type
        from ordcust
       where ordcust_no = I_ordcust_no;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then
      O_bus_obj_id := RIB_BUSOBJID_TBL(I_ordcust_no);
      O_routing_info := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no',
                                                                I_seq_no,
                                                                NULL,
                                                                NULL,
                                                                NULL,
                                                                NULL));

      open C_ORDCUST;
      fetch C_ORDCUST into L_cust_order_no,
                           L_fulfill_order_no,
                           L_confirm_type;
      close C_ORDCUST;

      O_message := "RIB_FulfilOrdCfmDesc_REC"(0,
                                              L_cust_order_no,
                                              L_fulfill_order_no,
                                              L_confirm_type,
                                              NULL,
                                              NULL);

      update ordcust_pub_info
         set pub_status = LP_error_status
       where seq_no = I_seq_no;
   end if;

   SQL_LIB.API_MSG(L_error_type,
                   O_error_message);

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_ORDCUST');
END HANDLE_ERRORS;
--------------------------------------------------------------------------------
END RMSMFM_ORDCUST;
/
