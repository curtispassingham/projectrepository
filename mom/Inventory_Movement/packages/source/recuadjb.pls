CREATE OR REPLACE PACKAGE BODY REC_UNIT_ADJ_SQL AS
-------------------------------------------------------------------------------------
FUNCTION CHECK_RECORDS (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_shipment            IN     SHIPMENT.SHIPMENT%TYPE,
                        I_item                IN     ITEM_MASTER.ITEM%TYPE,
                        I_seq_no              IN     SHIPSKU.SEQ_NO%TYPE,
                        I_adjusted_qty        IN     SHIPSKU.QTY_RECEIVED%TYPE,
                        I_carton              IN     SHIPSKU.CARTON%TYPE,
                        I_calling_prg         IN     VARCHAR2,
                        I_adjusted_weight     IN     SHIPSKU.WEIGHT_RECEIVED%TYPE DEFAULT NULL,
                        I_adjusted_weight_uom IN     SHIPSKU.WEIGHT_RECEIVED_UOM%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'REC_UNIT_ADJ_SQL.CHECK_RECORDS';
   L_unmatched_qty   SHIPSKU.QTY_RECEIVED%TYPE := NULL;
   L_shipment        SHIPMENT.SHIPMENT%TYPE := I_shipment;
   L_parent_shipment SHIPMENT.SHIPMENT%TYPE;
   L_reject_reason   CODE_DETAIL.CODE_DESC%TYPE;
   L_exists          VARCHAR2(1) := NULL;
   ---
   L_item_rec         ITEM_MASTER%ROWTYPE;
   L_adjusted_wt      SHIPSKU.WEIGHT_RECEIVED%TYPE := NULL;
   L_adjusted_qty     SHIPSKU.QTY_RECEIVED%TYPE    := NULL;
   L_matched_weight   SHIPSKU.QTY_MATCHED%TYPE := NULL;
   L_unmatched_weight SHIPSKU.QTY_MATCHED%TYPE := NULL;
   L_cuom             UOM_CLASS.UOM%TYPE;
   L_conv_value       UOM_CONVERSION.FACTOR%TYPE;
   L_uom_class        UOM_CLASS.UOM_CLASS%TYPE;

   L_qty_received     SHIPSKU.QTY_RECEIVED%TYPE := NULL;
   L_qty_matched      SHIPSKU.QTY_MATCHED%TYPE := NULL;
   L_weight_rcvd      SHIPSKU.WEIGHT_RECEIVED%TYPE := NULL;
   L_weight_rcvd_uom  SHIPSKU.WEIGHT_RECEIVED_UOM%TYPE := NULL;

   L_tsf_no           TSFHEAD.TSF_NO%TYPE;
   L_unit_cost        SHIPSKU.UNIT_COST%TYPE;
   L_new_cost         TSFDETAIL.TSF_COST%TYPE;

   cursor C_SHIP_INFO is
      select  s.to_loc,
              s.to_loc_type,
              s.order_no,
              s.ext_ref_no_in,
              s.asn,
              ss.carton,
              s.receive_date
        from  shipment s,
              shipsku ss
       where  s.shipment = ss.shipment
         and  s.shipment = I_shipment
         and  ss. item   = I_item
         and  NVL(ss.carton,-1) = NVL(I_carton,-1);

   cursor C_GET_UNMATCHED_QTY is
      select  NVL(qty_received,0) - NVL(qty_matched,0)
        from  shipsku
       where  shipment = I_shipment
         and  seq_no   = I_seq_no
         and  item     = I_item
         and  NVL(carton,-1) = NVL(I_carton,-1)
         for update of qty_received NOWAIT;

   cursor C_GET_MATCHED_WEIGHT is
      select NVL(ss.qty_received, 0) qty_received,
             NVL(ss.qty_matched, 0) qty_matched,
             ss.weight_received,
             ss.weight_received_uom
        from shipsku ss
       where ss.shipment = I_shipment
         and ss.seq_no   = I_seq_no
         and ss.item     = I_item
         and NVL(ss.carton,-1) = NVL(I_carton,-1)
         for update of ss.weight_received NOWAIT;

   cursor C_CHECK_CHILD is
      select  parent_shipment
        from  shipment
       where  shipment = I_shipment;

    cursor C_TSF_COST_INFO is
      select th.tsf_no,
             ss.unit_cost
        from tsfhead th,
             tsfdetail td,
             shipment s,
             shipsku ss
       where s.shipment = I_shipment
         and td.item = I_item
         and th.tsf_type = 'ICB'
         and s.shipment = ss.shipment
         and s.order_no = th.order_no
         and th.tsf_no = td.tsf_no
         and td.item = ss.item;

   c_ship_info_rec     C_SHIP_INFO%ROWTYPE;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_adjusted_qty is NULL
      and (I_adjusted_weight is NULL
           or I_adjusted_weight_uom is NULL) then
      if I_adjusted_weight is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_adjusted_weight',
                                                L_program,
                                                NULL);
         return FALSE;
      elsif I_adjusted_weight_uom is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_adjusted_weight_uom',
                                                L_program,
                                                NULL);
         return FALSE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                'I_adjusted_qty',
                                                 L_program,
                                                 NULL);
         return FALSE;
      end if;
   end if;

   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_shipment',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_calling_prg is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_calling_prg',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CHECK_CHILD;
   fetch C_CHECK_CHILD into L_parent_shipment;
   close C_CHECK_CHILD;

   open C_SHIP_INFO;
   fetch C_SHIP_INFO into c_ship_info_rec;
   close C_SHIP_INFO;

   L_adjusted_qty := NVL(I_adjusted_qty, 0);

   SQL_LIB.SET_MARK('OPEN',
                    'C_TSF_COST_INFO',
                    'TSFHEAD, SHIPMENT, SHIPSKU',
                    'Shipment: '|| TO_CHAR(I_shipment));
   open C_TSF_COST_INFO;

   SQL_LIB.SET_MARK('FETCH',
                    'C_TSF_COST_INFO',
                    'TSFHEAD, SHIPMENT, SHIPSKU',
                    'Shipment: '|| TO_CHAR(I_shipment));
   fetch C_TSF_COST_INFO into L_tsf_no,
                              L_unit_cost;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_TSF_COST_INFO',
                    'TSFHEAD, SHIPMENT, SHIPSKU',
                    'Shipment: '|| TO_CHAR(I_shipment));
   close C_TSF_COST_INFO;

   if ((L_tsf_no is NOT NULL) and (I_item is NOT NULL )and (L_unit_cost is NOT NULL)) then
      L_new_cost := L_adjusted_qty * L_unit_cost;
      -- update transfer cost
      if TRANSFER_SQL.UPDATE_TSF_COST (O_error_message,
                                       L_tsf_no,
                                       I_item,
                                       L_new_cost) = FALSE then
         return FALSE;
      end if;
   end if;

   if UPPER(I_calling_prg) = 'REIM' then
     if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_rec,
                                         I_item) = FALSE then
         return FALSE;
      end if;

      --For catch weight type 2 or 4, ReIM does RUA by weight instead of unit qty.
      --In that case, CHECK_RECORD will be called with I_adjusted_weight and
      --I_adjusted_weight_uom, but not I_adjusted_qty.
      --Similarly, for catch weight type 2 or 4, shipsku.qty_matched (if defined) contains
      --the matched weight in cost UOM instead of unit qty. When comparing qty_matched
      --with I_adjusted_weight, UOM conversion needs to happen.
      --Negative qty or weight adjustment from ReIM cannot exceed unmatched qty.
      if L_item_rec.catch_weight_type in ('2','4') then
         if I_adjusted_weight is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_adjusted_weight',
                                                   L_program,
                                                   NULL);
            return FALSE;
         end if;

         if I_adjusted_weight_uom is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_adjusted_weight_uom',
                                                   L_program,
                                                   NULL);
            return FALSE;
         end if;

         open C_GET_MATCHED_WEIGHT;
         fetch C_GET_MATCHED_WEIGHT into L_qty_received,
                                         L_qty_matched,
                                         L_weight_rcvd,
                                         L_weight_rcvd_uom;
         close C_GET_MATCHED_WEIGHT;

         if not ITEM_SUPP_COUNTRY_SQL.GET_COST_UOM(O_error_message,
                                                   L_cuom,
                                                   I_item) then
            return FALSE;
         end if;
         -- Get the Cost UOM Class
         if UOM_SQL.GET_CLASS(O_error_message,
                              L_uom_class,
                              L_cuom) = FALSE then
            return FALSE;
         end if;

         -- convert the matched qty (which is in cuom) into received_weight uom

         if L_cuom = L_weight_rcvd_uom then
            L_matched_weight := L_qty_matched;
         else
            if UOM_SQL.WITHIN_CLASS(O_error_message,
                                    L_matched_weight,
                                    L_weight_rcvd_uom,
                                    L_qty_matched,
                                    L_cuom,
                                    L_uom_class) = FALSE then
               return FALSE;
            end if;
         end if;
         L_unmatched_weight := L_weight_rcvd - L_matched_weight;

         -- convert I_adjusted_weight from I_adjusted_weight_uom to weight_received_uom for comparison
         if L_weight_rcvd_uom != I_adjusted_weight_uom then
            if UOM_SQL.WITHIN_CLASS(O_error_message,
                                    L_adjusted_wt,
                                    L_weight_rcvd_uom,
                                    I_adjusted_weight,
                                    I_adjusted_weight_uom,
                                    L_uom_class) = FALSE then
               return FALSE;
            end if;
         else
            L_adjusted_wt := I_adjusted_weight;
         end if;

         -- negative inventory adjustment of weight cannot exceed unmatched weight
         if L_adjusted_wt < 0.00001 and
            (abs(L_adjusted_wt) > L_unmatched_weight) then
            O_error_message := SQL_LIB.CREATE_MSG('UNMATCH_WEIGHT_LESS',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      else
         -- Non type 2/4 simple pack catch weighht item, adjust by QTY only.
         -- No need to process if adjusted_qty is 0.
         if L_adjusted_qty = 0 then
            return TRUE;
         end if;
         L_adjusted_wt     := NULL;
         L_weight_rcvd_uom := NULL;

         -- Negative adjustment cannot be more than unmatched qty from ReIM.
         open C_GET_UNMATCHED_QTY;
         fetch C_GET_UNMATCHED_QTY into L_unmatched_qty;
         close C_GET_UNMATCHED_QTY;

         if L_adjusted_qty < 0 and
            (abs(L_adjusted_qty) > L_unmatched_qty) then
            O_error_message := SQL_LIB.CREATE_MSG('UNMATCH_QTY_LESS',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            return FALSE;
         end if;
      end if;
   else
      -- RMS RUA cannot adjust against child shipments
      if L_parent_shipment is not null and UPPER(I_calling_prg) != 'REIM' then
         O_error_message := SQL_LIB.CREATE_MSG('CHILD_SHIP',NULL,NULL,NULL);
         return FALSE;
      end if;

      if L_adjusted_qty = 0 and I_adjusted_weight = 0 then 
         return TRUE;
      end if;
      L_adjusted_wt := I_adjusted_weight;
      L_weight_rcvd_uom := I_adjusted_weight_uom;
   end if;

   -- adjust the shipment. ReIM RUA will not create new shipments. It will update
   -- the existing shipment. Other RUA may create child shipments if needed.

   if ORDER_RCV_SQL.PO_LINE_ITEM_ONLINE(O_error_message,
                                        c_ship_info_rec.to_loc,     -- loc
                                        c_ship_info_rec.order_no,   -- order_no
                                        I_item,                     -- item
                                        L_adjusted_qty,             -- qty
                                        'A'  ,                      -- tran_type
                                        c_ship_info_rec.receive_date,    -- tran_date
                                        c_ship_info_rec.ext_ref_no_in,   -- receipt_number
                                        c_ship_info_rec.asn,        -- asn
                                        NULL,                       -- appt
                                        I_carton,                   -- carton
                                        NULL,                       -- distro_type
                                        NULL,                       -- distro_number
                                        NULL,                       -- destination
                                        NULL,                       -- disp
                                        NULL,                       -- unit_cost
                                        'Y',                        -- online_ind
                                        L_shipment,
                                        L_adjusted_wt,
                                        L_weight_rcvd_uom) = FALSE then    -- shipment
      return FALSE;
   end if;

   -- Adjustment has been successful. Insert into RUA_RIB_INTERFACE
   -- to be published to store.
   -- Note: only SIM subscribes to RUA messages, not RWMS.
   -- SIM's receipt number is written to shipment.ext_ref_no_in by the order/dsd receiving processes.
   -- SIM's receipt number needs to be published back to SIM in the RUA message through the ASN field.
   -- That's why shipment.ext_ref_no_in takes precedence over shipment.asn in the insert statement.

   if UPPER(I_calling_prg) in ('REIM', 'RECUTADJ') then
      if NVL(I_adjusted_qty, 0) != 0 then
         insert into RUA_RIB_INTERFACE (seq_no,
                                        order_no,
                                        asn,
                                        location,
                                        loc_type,
                                        item,
                                        carton,
                                        adj_qty,
                                        pgm_name)
                                values (I_seq_no,
                                        c_ship_info_rec.order_no,
                                        NVL(c_ship_info_rec.ext_ref_no_in, c_ship_info_rec.asn),
                                        c_ship_info_rec.to_loc,
                                        c_ship_info_rec.to_loc_type,
                                        I_item,
                                        I_carton,
                                        I_adjusted_qty,
                                        UPPER(I_calling_prg));
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
         return FALSE;
END CHECK_RECORDS;
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_SHIPSKU_RECORD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_shipment        IN       SHIPMENT.SHIPMENT%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE,
                              I_carton          IN       SHIPSKU.CARTON%TYPE)
   RETURN BOOLEAN IS

   L_lock_flag   varchar2(1)    := NULL;
   L_program     VARCHAR2(50)   := 'REC_UNIT_ADJ_SQL.LOCK_SHIPSKU_RECORD';

   cursor C_LOC_SHIPSKU is
      select 'x'
        from shipsku ss,
             shipment s
       where s.shipment = ss.shipment
         and nvl(parent_shipment, s.shipment) = I_shipment
         and ss.item = I_item
         and nvl(ss.carton,-1) = NVL(I_carton,-1)
         for update of ss.qty_received nowait;
BEGIN

   open C_LOC_SHIPSKU;
   fetch C_LOC_SHIPSKU into L_lock_flag;

   if L_lock_flag is NULL then
      close C_LOC_SHIPSKU;
      O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_DATA',
                                            'I_shipment',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   close C_LOC_SHIPSKU;

   return TRUE;
EXCEPTION
      when OTHERS then
            O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                   SQLERRM,
                                                   L_program,
                                                   TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_SHIPSKU_RECORD;
--------------------------------------------------------------------------------------
END REC_UNIT_ADJ_SQL;
/
