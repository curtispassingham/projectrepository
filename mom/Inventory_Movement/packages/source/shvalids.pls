
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SHIPMENT_VALIDATE_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------
---    Name: EXIST
--- Purpose: Returns boolean parameter whether or not the SHIPMENT exists.
--------------------------------------------------------------------
FUNCTION EXIST(O_error_message   IN OUT   VARCHAR2,
               I_shipment        IN       SHIPMENT.SHIPMENT%TYPE,
               O_exist           IN OUT   BOOLEAN)
RETURN BOOLEAN;

--------------------------------------------------------------------
---    Name: ORDER_NO_EXISTS
--- Purpose: Returns boolean parameter whether or not the order no exists on the ship ment table.
--------------------------------------------------------------------
FUNCTION ORDER_NO_EXISTS(O_error_message   IN OUT   VARCHAR2,
                         I_order_no        IN       SHIPMENT.ORDER_NO%TYPE,
                         O_exist           IN OUT   BOOLEAN)
RETURN BOOLEAN;

--------------------------------------------------------------------
---    Name: BOL_NO_EXISTS
--- Purpose: Returns boolean parameter whether or not the BOL_NO
---          exists on the shipment table.
--------------------------------------------------------------------
FUNCTION BOL_NO_EXISTS(O_error_message   IN OUT   VARCHAR2,
                       O_exists          IN OUT   BOOLEAN,
                       I_bol_no          IN       SHIPMENT.BOL_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------
---    Name: ASN_EXISTS
--- Purpose: Returns boolean parameter whether or not the ASN
---          exists on the shipment table.
--------------------------------------------------------------------
FUNCTION ASN_EXISTS(O_error_message   IN OUT   VARCHAR2,
                    O_exists          IN OUT   BOOLEAN,
                    I_asn             IN       SHIPMENT.ASN%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------
---    Name: CHECK_SHIPMENTS
--- Purpose: Returns boolean parameter whether or not a shipment
---          exists on the shipment table for the parent shipment
---          value passed in.
---  Author: Kathleen Latorre
--------------------------------------------------------------------
FUNCTION CHECK_SHIPMENTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_shipment        IN       SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
---    Name: PARENT_SHIPMENT_EXIST
--- Purpose: Returns boolean parameter whether or not a shipment parent
---          exists on the shipment table for a shipment value passed in.
--------------------------------------------------------------------
FUNCTION PARENT_SHIPMENT_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               I_shipment        IN       SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
END SHIPMENT_VALIDATE_SQL;
/


