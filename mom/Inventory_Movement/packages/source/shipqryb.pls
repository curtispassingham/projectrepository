
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SHIP_QUERY_SQL AS
-----------------------------------------------------------------------------------------
FUNCTION SET_PARAMETER_VALUE(O_error_message   IN OUT   VARCHAR2,
                             I_parm_name       IN       VARCHAR2,
                             I_parm_type       IN       VARCHAR2,
                             I_parm_value      IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(40) := 'SHIP_QUERY_SQL.SET_PARAMETER_VALUE';

BEGIN

   insert into generic_global_temp(col1_var60,
                                   col2_var100,
                                   col3_num30,
                                   col4_date)
      values(I_parm_name,
             DECODE(I_parm_type, 'S', I_parm_value, NULL),
             DECODE(I_parm_type, 'N', TO_NUMBER(I_parm_value), NULL),
             DECODE(I_parm_type, 'D', TO_DATE(I_parm_value, 'YYYYMMDDHH24MISS'), NULL));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;


END SET_PARAMETER_VALUE;
------------------------------------------------------------------------------------------------
FUNCTION INSERT_SHIP_TO_TMP(O_error_message  IN OUT  VARCHAR2,
                            I_main_query     IN      VARCHAR2)
   RETURN BOOLEAN IS

   TYPE      TYP_SHIP_TBL   is TABLE OF SHIPMENT.SHIPMENT%TYPE; --INDEX BY BINARY_INTEGER;

   TBL_MAIN  TYP_SHIP_TBL;

   L_program       VARCHAR2(40) := 'SHIP_QUERY_SQL.INSERT_SHIP_TO_TMP';

BEGIN

   -- Get Shipments from Main Query
   if I_main_query is NOT NULL then
      execute immediate I_main_query
         bulk collect into TBL_MAIN;

      if TBL_MAIN is NOT NULL and TBL_MAIN.count > 0 then
         FORALL i in TBL_MAIN.first..TBL_MAIN.last
            insert into shipment_tmp
               values (TBL_MAIN(i));
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SHIP_TO_TMP;
-----------------------------------------------------------------------------------------
END SHIP_QUERY_SQL;
/
