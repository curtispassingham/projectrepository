
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_WOSTATUS AUTHID CURRENT_USER AS

WOSTATUS_ADD       CONSTANT  VARCHAR2(30) := 'wostatuscre';

/* Function and Procedure Bodies */
-------------------------------------------------------------------------
PROCEDURE CONSUME (O_status_code          IN OUT  VARCHAR2,
                   O_error_message        IN OUT  VARCHAR2,
                   I_message              IN      RIB_OBJECT,
                   I_message_type         IN      VARCHAR2);
-----------------------------------------------------------------------------------------
END RMSSUB_WOSTATUS;
/
