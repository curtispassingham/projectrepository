
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_DSRCPT_VALIDATE AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PUBLIC PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION CHECK_MESSAGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dsrcpt_rec    IN OUT RMSSUB_DSRCPT.DSRCPT_REC_TYPE,
                       I_message       IN     "RIB_DSRcptDesc_REC",
                       I_message_type  IN     VARCHAR2)
RETURN BOOLEAN;


END RMSSUB_DSRCPT_VALIDATE;
/
