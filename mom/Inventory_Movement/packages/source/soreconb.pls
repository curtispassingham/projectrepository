CREATE OR REPLACE PACKAGE BODY STOCK_ORDER_RECONCILE_SQL AS

LP_user                     ITEM_LOC_SOH.LAST_UPDATE_ID%TYPE      := get_user;
L_receipt_date              SHIPMENT.RECEIVE_DATE%TYPE            := NULL;
L_ship_date                 SHIPMENT.SHIP_DATE%TYPE               := NULL;
L_tran_date                 DATE := get_vdate;
LP_system_options_row       SYSTEM_OPTIONS%ROWTYPE;
------------------------------------------------------------------------------------------
-- Function Name: UPDATE_STAKE_SKU_LOC
-- Purpose: This private function will update the snapshot_on_hand_qty and snapshot_in_transit_qty for the item/loc record.
------------------------------------------------------------------------------------------
FUNCTION UPDATE_STAKE_SKU_LOC(O_error_message IN OUT VARCHAR2,
                              I_item          IN     item_loc.item%TYPE,
                              I_loc_type      IN     item_loc.loc_type%TYPE,
                              I_location      IN     item_loc.loc%TYPE,
                              I_tran_date     IN     DATE,
                              I_adj_soh       IN     item_loc_soh.stock_on_hand%TYPE,
                              I_adj_intran    IN     item_loc_soh.stock_on_hand%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: UPDATE_FINISHER_RETAIL_UNITS
-- Purpose: This private function will update the finisher_av_retail and finisher_units for the item/finisher record.
------------------------------------------------------------------------------------------
FUNCTION UPDATE_FINISHER_RETAIL_UNITS( O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item                IN      ITEM_LOC_SOH.ITEM%TYPE,
                                       I_loc                 IN      ITEM_LOC_SOH.LOC%TYPE,
                                       I_loc_type            IN      ITEM_LOC_SOH.LOC_TYPE%TYPE,
                                       I_tsf_no              IN      TSFHEAD.TSF_NO%TYPE,
                                       I_reconciled_qty      IN      TSFDETAIL.RECONCILED_QTY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function name: WRITE_INVENTORY_ADJUSTMENT
--Purpose: This function retrieves location's current wac and retail in case of a NL/RL/SL adjustment
--and writes tran_data 22. TRAN_DATA 22 is always written at item-loc's current wac and retail.
--Input: I_loc expects virtual warehouses instead of physical warehouse.
--       I_item expects a component item transferring a pack.
--       I_qty can be a positive or negative quantity.
-------------------------------------------------------------------------------------------
FUNCTION WRITE_INVENTORY_ADJUSTMENT(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_shipment           IN     SHIPMENT.SHIPMENT%TYPE,
                                    I_tsf_alloc_no       IN     SHIPSKU.DISTRO_NO%TYPE,
                                    I_loc                IN     SHIPMENT.FROM_LOC%TYPE,
                                    I_loc_type           IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                                    I_item               IN     SHIPSKU.ITEM%TYPE,
                                    I_qty                IN     TSFDETAIL.RECONCILED_QTY%TYPE,
                                    I_tran_date          IN     TRAN_DATA.TRAN_DATE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function name: ADJUST_SHORTAGE
--Purpose: This function processes under receiving for adjustment types NL/BL/RL/SL for one item.
--         It updates inventory, recalc wac, write inventory adjustment (tran_data 22), and
--         write a reverse transaction for franchise orders, franchise returns and non-franchise
--         transfers.
-------------------------------------------------------------------------------------------
FUNCTION ADJUST_SHORTAGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                         I_carton           IN     SHIPSKU.CARTON%TYPE,
                         I_tsf_alloc_type   IN     SHIPSKU.DISTRO_TYPE%TYPE,
                         I_tsf_alloc_no     IN     SHIPSKU.DISTRO_NO%TYPE,
                         I_tsf_seq_no       IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                         I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                         I_from_loc         IN     SHIPMENT.FROM_LOC%TYPE,
                         I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                         I_to_loc           IN     SHIPMENT.TO_LOC%TYPE,
                         I_item             IN     SHIPSKU.ITEM%TYPE,
                         I_shortage_qty     IN     TSFDETAIL.RECONCILED_QTY%TYPE,
                         I_adjust_type      IN     SHIPSKU.ADJUST_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Name:    FORCED_SHORTAGE
-- Purpose: This function performs the shortage stock order receipt reconciliation
--          when the stock order is marked as shortage(received quantity is less
--          than shipped quantity).
--          The function will increase stock_on_hand and decrease in_transit for to_loc
---------------------------------------------------------------------------------------
FUNCTION FORCED_SHORTAGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                         I_carton           IN     SHIPSKU.CARTON%TYPE,
                         I_tsf_alloc_type   IN     SHIPSKU.DISTRO_TYPE%TYPE,
                         I_tsf_alloc_no     IN     SHIPSKU.DISTRO_NO%TYPE,
                         I_tsf_seq_no       IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                         I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                         I_from_loc         IN     SHIPMENT.FROM_LOC%TYPE,
                         I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                         I_to_loc           IN     SHIPMENT.TO_LOC%TYPE,
                         I_item             IN     SHIPSKU.ITEM%TYPE,
                         I_shortage_qty     IN     TSFDETAIL.RECONCILED_QTY%TYPE)

RETURN BOOLEAN IS

   L_program                            VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.FORCED_SHORTAGE';
   L_item_record                        ITEM_MASTER%ROWTYPE;
   L_receive_as_type                    ITEM_LOC.RECEIVE_AS_TYPE%TYPE;

   L_rowid                              ROWID;
   L_table                              VARCHAR2(30) := 'ITEM_LOC_SOH';
   L_key1                               VARCHAR2(100);
   L_key2                               VARCHAR2(100) := to_char(I_to_loc);
   RECORD_LOCKED                        EXCEPTION;
   PRAGMA                               EXCEPTION_INIT(Record_Locked, -54);
   L_from_finisher_ind                  WH.FINISHER_IND%TYPE := 'N';
   L_to_finisher_ind                    WH.FINISHER_IND%TYPE := 'N';
   L_intercompany                       BOOLEAN;
   L_to_loc_vwh                         SHIPMENT.TO_LOC%TYPE := NULL;
   L_tsf_type                           TSFHEAD.TSF_TYPE%TYPE := NULL;
   L_rdw_tran_code                      TRAN_DATA.tran_code%TYPE := 44;
   L_tran_date                          DATE := get_vdate;
   L_dummy_cost                         item_loc_soh.av_cost%TYPE := NULL;
   L_from_loc_vwh                       SHIPMENT.FROM_LOC%TYPE := NULL;
   L_sk_from_loc_unit_cost              SHIPSKU.UNIT_COST%TYPE := 0;
   L_sk_to_loc_unit_cost                SHIPSKU.UNIT_COST%TYPE:= 0;
   L_unit_retail                        ITEM_LOC.UNIT_RETAIL%TYPE := NULL;
   L_pct_in_pack                        NUMBER := 0;
   L_total_cost                         TRAN_DATA.TOTAL_COST%TYPE;
   L_total_retail                       TRAN_DATA.TOTAL_RETAIL%TYPE;

   CURSOR C_comp_item IS
      select v.item,
             v.qty,
             im.dept,
             im.class,
             im.subclass
        from item_master im,
             v_packsku_qty v
       where im.item = v.item
         and v.pack_no = I_item
         and im.inventory_ind = 'Y';

   CURSOR C_lock_to_ils(cv_item    item_loc.item%type) IS
      SELECT rowid
        FROM item_loc_soh
       WHERE item = cv_item
         AND loc = NVL(L_to_loc_vwh,I_to_loc)
         FOR UPDATE NOWAIT;

   CURSOR C_SHIPSKU_COST is
       select unit_cost
         from shipsku
        where shipment  = I_shipment
          and distro_no = I_tsf_alloc_no
          and item      = I_item;

   CURSOR C_ITEM_LOC_RETAIL (I_item  in ITEM_LOC.ITEM%TYPE,
                             I_loc  in ITEM_LOC.LOC%TYPE) is
      select unit_retail
        from item_loc
       where item = I_item
         and loc = I_loc;

BEGIN
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                       L_item_record,
                                       I_item) = FALSE then
      return FALSE;
   end if;
   ---
   open C_SHIPSKU_COST;
   fetch C_SHIPSKU_COST into L_sk_from_loc_unit_cost;
   close C_SHIPSKU_COST;

    if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                        I_from_loc,
                                        I_from_loc_type,
                                        NULL,
                                        I_to_loc,
                                        I_to_loc_type,
                                        NULL,
                                        L_sk_from_loc_unit_cost,
                                        L_sk_to_loc_unit_cost,
                                        'C',
                                        L_tran_date,
                                        NULL) = FALSE then
       return FALSE;
    end if;

   if I_tsf_alloc_type = 'T' then
      ---
      if TRANSFER_SQL.GET_TSF_TYPE(O_error_message,
                                   L_tsf_type,
                                   I_tsf_alloc_no)= FALSE then
         return FALSE;
      end if;
      ---
      if L_tsf_type = 'EG' or (L_tsf_type = 'CO' and LP_system_options_row.oms_ind = 'Y') then
         if I_to_loc_type = 'W' then
            if GET_TO_LOC_VWH(O_error_message,
                              L_to_loc_vwh,
                              I_tsf_alloc_no,
                              I_to_loc_type)= FALSE then
               return FALSE;
            end if;
         end if;
         if I_from_loc_type = 'W' then
            if GET_FROM_LOC_VWH(O_error_message,
                                 L_from_loc_vwh,
                                 I_tsf_alloc_no,
                                 I_from_loc_type)= FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      if TRANSFER_SQL.GET_TSF_ATTRIB(O_error_message,
                                     L_from_finisher_ind,
                                     L_to_finisher_ind,
                                     L_intercompany,
                                     I_tsf_alloc_type,
                                     I_tsf_alloc_no,
                                     NVL(L_from_loc_vwh, I_from_loc),
                                     I_from_loc_type,
                                     NVL(L_to_loc_vwh, I_to_loc),
                                     I_to_loc_type) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_item_record.pack_ind = 'N' then   -- Single item
      ---
      -- Increase stock_on_hand and Devrease in_transit for to_loc
      L_key1 := I_item;
      ---
      open C_lock_to_ils(I_item);
      fetch C_lock_to_ils into L_rowid;
      close C_lock_to_ils;
      ---
     open C_ITEM_LOC_RETAIL(I_item,I_to_loc);
     fetch C_ITEM_LOC_RETAIL into L_unit_retail;
     close C_ITEM_LOC_RETAIL;

      UPDATE item_loc_soh
         SET stock_on_hand = stock_on_hand + I_shortage_qty,
             in_transit_qty = in_transit_qty - I_shortage_qty,
             last_update_id        = LP_user,
             last_update_datetime  = SYSDATE,
             soh_update_datetime   = SYSDATE
       WHERE rowid = L_rowid;
      ---
      if I_tsf_alloc_type = 'T' then
         if L_to_finisher_ind = 'Y' then
            if UPDATE_FINISHER_RETAIL_UNITS (O_error_message,
                                             I_item,
                                             NVL(L_to_loc_vwh, I_to_loc),
                                             I_to_loc_type,
                                             I_tsf_alloc_no,
                                             I_shortage_qty) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      L_total_cost :=  L_sk_to_loc_unit_cost*I_shortage_qty ;
      L_total_retail :=L_unit_retail*I_shortage_qty;

      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             L_item_record.item,
                                             L_item_record.dept,
                                             L_item_record.class,
                                             L_item_record.subclass,
                                             NVL(L_to_loc_vwh, I_to_loc),
                                             I_to_loc_type,
                                             L_tran_date,
                                             L_rdw_tran_code,
                                             NULL,
                                             I_shortage_qty,
                                             L_total_cost,
                                             L_total_retail,
                                             I_tsf_alloc_no,  --ref_no_1
                                             I_shipment,  --ref_no_2
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_program) = FALSE then
          return FALSE;
      end if;
   else   -- pack item
      ---
      if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                L_receive_as_type,
                                                I_item,
                                                NVL(L_to_loc_vwh, I_to_loc)) = FALSE then
         return FALSE;
      end if;
      ---
      FOR REC_comp_item IN C_comp_item LOOP
         ---
         L_key1 := REC_comp_item.item;
         ---
         open C_lock_to_ils(REC_comp_item.item);
         fetch C_lock_to_ils into L_rowid;
         close C_lock_to_ils;
         ---
         open C_ITEM_LOC_RETAIL(REC_comp_item.item,I_to_loc);
         fetch C_ITEM_LOC_RETAIL into L_unit_retail;
         close C_ITEM_LOC_RETAIL;

         if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                          L_pct_in_pack,
                                          I_item,
                                          REC_comp_item.item,
                                          I_to_loc) = FALSE then
            return FALSE;
         end if;
         ---

         L_total_cost:=I_shortage_qty*REC_comp_item.qty*L_pct_in_pack*L_sk_to_loc_unit_cost;
         L_total_retail:=I_shortage_qty*REC_comp_item.qty*L_unit_retail;

         UPDATE item_loc_soh
            SET in_transit_qty = DECODE(L_receive_as_type,
                                        'P', in_transit_qty,
                                        in_transit_qty - REC_comp_item.qty * I_shortage_qty),
                pack_comp_intran = DECODE(L_receive_as_type,
                                          'P', pack_comp_intran - REC_comp_item.qty * I_shortage_qty,
                                          pack_comp_intran),
                stock_on_hand = DECODE(L_receive_as_type,
                                       'P', stock_on_hand,
                                       stock_on_hand + REC_comp_item.qty * I_shortage_qty),
                pack_comp_soh = DECODE(L_receive_as_type,
                                       'P', pack_comp_soh + REC_comp_item.qty * I_shortage_qty,
                                       pack_comp_soh),
                last_update_id        = LP_user,
                last_update_datetime  = SYSDATE,
                soh_update_datetime   = DECODE(L_receive_as_type,'P',soh_update_datetime,SYSDATE)
           WHERE rowid = L_rowid;
          ---
         if I_tsf_alloc_type = 'T' then
            if L_to_finisher_ind = 'Y' then
               if UPDATE_FINISHER_RETAIL_UNITS (O_error_message,
                                                REC_comp_item.item,
                                                NVL(L_to_loc_vwh, I_to_loc),
                                                I_to_loc_type,
                                                I_tsf_alloc_no,
                                                REC_comp_item.qty * I_shortage_qty) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
         ---
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                REC_comp_item.item,
                                                REC_comp_item.dept,
                                                REC_comp_item.class,
                                                REC_comp_item.subclass,
                                                NVL(L_to_loc_vwh, I_to_loc),
                                                I_to_loc_type,
                                                L_tran_date,
                                                L_rdw_tran_code,
                                                NULL,
                                                REC_comp_item.qty * I_shortage_qty,
                                                L_total_cost,
                                                L_total_retail,
                                                I_tsf_alloc_no,  --ref_no_1
                                                I_shipment,  --ref_no_2
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_program) = FALSE then
            return FALSE;
         end if;

      END LOOP;
      ---
      if I_to_loc_type = 'W' and L_receive_as_type = 'P' then
         ---
         L_key1 := I_item;
         ---
         open C_lock_to_ils(I_item);
         fetch C_lock_to_ils into L_rowid;
         close C_lock_to_ils;
         ---
         UPDATE item_loc_soh
            SET stock_on_hand = stock_on_hand + I_shortage_qty,
                in_transit_qty = in_transit_qty - I_shortage_qty,
                last_update_id        = LP_user,
                last_update_datetime  = SYSDATE,
                soh_update_datetime   = SYSDATE
          WHERE rowid = L_rowid;
         ---
         if I_tsf_alloc_type = 'T' then
            if L_to_finisher_ind = 'Y' then
               if UPDATE_FINISHER_RETAIL_UNITS (O_error_message,
                                                I_item,
                                                NVL(L_to_loc_vwh, I_to_loc),
                                                I_to_loc_type,
                                                I_tsf_alloc_no,
                                                I_shortage_qty) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
      end if;
      ---
   end if;
   ---
   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END FORCED_SHORTAGE;
--------------------------------------------------------------------------------------
-- Name:    FORCED_OVERAGE
-- Purpose: This function performs the overage stock order receipt reconciliation
--          when the stock order is marked as overage(received quantity is more
--          than shipped quantity).
--          The function will increase from_loc's stock_on_hand and decrease to_loc's
--          stock_on_hand, then create a tran_data for transfer out/in.
---------------------------------------------------------------------------------------
FUNCTION FORCED_OVERAGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                        I_carton           IN     SHIPSKU.CARTON%TYPE,
                        I_tsf_alloc_type   IN     SHIPSKU.DISTRO_TYPE%TYPE,
                        I_tsf_alloc_no     IN     SHIPSKU.DISTRO_NO%TYPE,
                        I_tsf_seq_no       IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                        I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                        I_from_loc         IN     SHIPMENT.FROM_LOC%TYPE,
                        I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                        I_to_loc           IN     SHIPMENT.TO_LOC%TYPE,
                        I_item             IN     SHIPSKU.ITEM%TYPE,
                        I_overage_qty      IN     TSFDETAIL.RECONCILED_QTY%TYPE)

RETURN BOOLEAN IS

   L_program                         VARCHAR2(64)   := 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE';
   L_item_record                     ITEM_MASTER%ROWTYPE;
   L_receive_as_type                 ITEM_LOC.RECEIVE_AS_TYPE%TYPE;

   L_av_cost                         item_loc_soh.av_cost%TYPE;
   L_total_av_cost                   item_loc_soh.av_cost%TYPE;
   L_unit_retail                     item_loc.unit_retail%TYPE;
   L_total_retail                    item_loc.unit_retail%TYPE;
   L_tran_date                       DATE := get_vdate;
   L_tran_code                       tran_data.tran_code%type;

   L_found                           BOOLEAN;

   L_from_av_cost                    item_loc_soh.av_cost%TYPE;
   L_from_av_cost_toloc_currency     item_loc_soh.av_cost%TYPE;
   L_profit_chrgs_to_loc             NUMBER;
   L_exp_chrgs_to_loc                NUMBER;
   L_total_chrgs_prim                ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE;
   L_charge_to_loc                   item_loc_soh.av_cost%TYPE;
   L_to_av_cost                      item_loc_soh.av_cost%TYPE;
   L_to_soh                          item_loc_soh.stock_on_hand%TYPE;
   L_to_pack_comp_soh                item_loc_soh.pack_comp_soh%TYPE;
   L_upd_to_av_cost                  item_loc_soh.av_cost%TYPE;

   L_pack_no                         item_master.item%TYPE       := NULL;
   L_pack_loc_av_cost                item_loc_soh.av_cost%TYPE   := NULL;
   L_pack_loc_cost                   item_loc_soh.unit_cost%TYPE := NULL;
   L_pack_loc_retail                 item_loc.unit_retail%TYPE   := NULL;
   L_pack_selling_unit_retail        item_loc.unit_retail%TYPE   := NULL;
   L_pack_selling_uom                item_loc.selling_uom%TYPE   := NULL;
   L_pack_total_chrgs_prim           item_loc.unit_retail%TYPE  := 0;
   L_pack_profit_chrgs_to_loc        NUMBER                     := 0;
   L_pack_exp_chrgs_to_loc           NUMBER                     := 0;

   L_from_rowid                      ROWID;
   L_to_rowid                        ROWID;
   L_table                           VARCHAR2(30) := 'ITEM_LOC_SOH';
   L_key1                            VARCHAR2(100);
   L_key2                            VARCHAR2(100);
   RECORD_LOCKED                     EXCEPTION;
   PRAGMA                            EXCEPTION_INIT(Record_Locked, -54);
   L_to_loc_vwh                      SHIPMENT.TO_LOC%TYPE := NULL;
   L_tsf_type                        TSFHEAD.TSF_TYPE%TYPE := NULL;
   L_rdw_tran_code                   TRAN_DATA.TRAN_CODE%TYPE := 44;
   L_dummy_cost                      ITEM_LOC_SOH.AV_COST%TYPE := NULL;
   L_from_loc_vwh                    SHIPMENT.FROM_LOC%TYPE := NULL;
   L_l10n_fin_rec                    "L10N_FIN_REC" := L10N_FIN_REC();

   CURSOR C_comp_item IS
      SELECT v.item,
             v.qty,
             im.dept,
             im.class,
             im.subclass
        FROM item_master im,
             v_packsku_qty v
       WHERE im.item   = v.item
         AND v.pack_no = I_item
         AND im.inventory_ind = 'Y';

   CURSOR C_lock_to_ils(cv_item    item_loc.item%type) IS
      SELECT rowid
        FROM item_loc_soh
       WHERE item = cv_item
         AND loc = NVL(L_to_loc_vwh, I_to_loc)
         FOR UPDATE NOWAIT;

   CURSOR C_lock_from_ils(cv_item    item_loc.item%type) IS
      SELECT rowid
        FROM item_loc_soh
       WHERE item = cv_item
         AND loc = I_from_loc
         FOR UPDATE NOWAIT;


   L_pct_in_pack              NUMBER;
   L_from_finisher_ind        wh.finisher_ind%TYPE := 'N';
   L_to_finisher_ind          wh.finisher_ind%TYPE := 'N';
   L_intercompany             BOOLEAN;
   L_rma_no                   WF_RETURN_HEAD.RMA_NO%TYPE;
   L_wf_order_no              WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_action_type              VARCHAR2(1);
   L_dummy_tcost              TRAN_DATA.TOTAL_COST%TYPE;
   L_dummy_retail             TRAN_DATA.TOTAL_RETAIL%TYPE;
   L_frm_non_stkhldg_f_store  STORE.STOCKHOLDING_IND%TYPE := 'N';
   L_store_row                STORE%ROWTYPE;
   L_exists                   BOOLEAN;


BEGIN
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                       L_item_record,
                                       I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                L_action_type,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_to_loc,
                                                I_to_loc_type) = FALSE then
      return FALSE;
   end if;
   ---
   --Item_loc_soh should not be updated for non-stockholding franchise stores.
   --It is only possible for returns from non-stockholding franchise stores to
   --be reconciled. Hence, retrieve from-loc's store_type and stockholding indicator.
   L_frm_non_stkhldg_f_store := 'N';
   if I_from_loc_type = 'S' then
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exists,     --dummy
                                  L_store_row,
                                  I_from_loc) = FALSE then
         return FALSE;
      end if;
      if L_store_row.store_type = 'F' and L_store_row.stockholding_ind = 'N' then
         L_frm_non_stkhldg_f_store := 'Y';
      end if;
   end if;
   ---
   if I_tsf_alloc_type = 'T' then
      ---
      if TRANSFER_SQL.GET_TSF_TYPE(O_error_message,
                                   L_tsf_type,
                                   I_tsf_alloc_no)= FALSE then
         return FALSE;
      end if;
      ---
      if (L_tsf_type = 'EG' or (L_tsf_type = 'CO' and LP_system_options_row.oms_ind = 'Y')) and I_to_loc_type = 'W' then
         if GET_TO_LOC_VWH(O_error_message,
                           L_to_loc_vwh,
                           I_tsf_alloc_no,
                           I_to_loc_type)= FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if TRANSFER_SQL.GET_TSF_ATTRIB(O_error_message,
                                     L_from_finisher_ind,
                                     L_to_finisher_ind,
                                     L_intercompany,
                                     I_tsf_alloc_type,
                                     I_tsf_alloc_no,
                                     I_from_loc,
                                     I_from_loc_type,
                                     NVL(L_to_loc_vwh, I_to_loc),
                                     I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
   elsif I_tsf_alloc_type = 'A' then
      if TRANSFER_SQL.IS_INTERCOMPANY(O_error_message,
                                      L_intercompany,
                                      I_tsf_alloc_type,
                                      NULL, -- L_tsf_type
                                      I_from_loc,
                                      I_from_loc_type,
                                      NVL(L_to_loc_vwh, I_to_loc),
                                      I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   L_l10n_fin_rec.procedure_key  :='UPDATE_AV_COST';
   L_l10n_fin_rec.country_id     := NULL;
   L_l10n_fin_rec.source_entity  := 'LOC';
   L_l10n_fin_rec.source_id      := I_from_loc;
   L_l10n_fin_rec.source_type    := I_from_loc_type;
   L_l10n_fin_rec.item           := I_item;
   L_l10n_fin_rec.dest_entity    := 'LOC';
   L_l10n_fin_rec.dest_id        := NVL(L_to_loc_vwh, I_to_loc);
   L_l10n_fin_rec.dest_type      := I_to_loc_type;

   -- Decrease stock_on_hand for to_loc, Increase stock_on_hand for from_loc
   ---
   if L_item_record.pack_ind = 'N' then   -- Single item
      ---
      -- Calculate up chrgs
      ---
      if I_tsf_alloc_type = 'A' then   -- shipment for allocation
         if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                        L_total_chrgs_prim,
                                                        L_profit_chrgs_to_loc,
                                                        L_exp_chrgs_to_loc,
                                                        'A',
                                                        I_tsf_alloc_no,
                                                        NULL,          -- tsf_seq_no
                                                        NULL,          -- shipment
                                                        NULL,          -- ship_seq_no
                                                        I_item,
                                                        NULL,          -- pack_item
                                                        I_from_loc,
                                                        I_from_loc_type,
                                                        I_to_loc,
                                                        I_to_loc_type) = FALSE then
            return FALSE;
         end if;
      else  -- shipment for transfer
         if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                        L_total_chrgs_prim,
                                                        L_profit_chrgs_to_loc,
                                                        L_exp_chrgs_to_loc,
                                                        'T',
                                                        I_tsf_alloc_no,
                                                        I_tsf_seq_no,
                                                        null,          -- shipment, for non 'EG' tsf_type
                                                        null,
                                                        I_item,
                                                        NULL,          -- pack_item
                                                        I_from_loc,
                                                        I_from_loc_type,
                                                        I_to_loc,
                                                        I_to_loc_type) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- Convert total chrg from primary currency to to_loc's currency
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_total_chrgs_prim,
                                          L_charge_to_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                      L_upd_to_av_cost,
                                      I_tsf_alloc_no,
                                      I_tsf_alloc_type,
                                      I_item,
                                      NULL,
                                      NULL,
                                      I_from_loc,
                                      I_from_loc_type,
                                      NVL(L_to_loc_vwh, I_to_loc),
                                      I_to_loc_type,
                                      -I_overage_qty,
                                      L_charge_to_loc,
                                      L_intercompany) = FALSE then
         return FALSE;
      end if;
      ---
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             L_item_record.item,
                                             L_item_record.dept,
                                             L_item_record.class,
                                             L_item_record.subclass,
                                             NVL(L_to_loc_vwh, I_to_loc),
                                             I_to_loc_type,
                                             L_tran_date,
                                             L_rdw_tran_code,
                                             NULL,
                                             -I_overage_qty,
                                             L_dummy_cost,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_program) = FALSE then
         return FALSE;
      end if;

      -- To_loc
      L_key1 := I_item;
      L_key2 := to_char(I_to_loc);
      ---
      open C_lock_to_ils(I_item);
      fetch C_lock_to_ils into L_to_rowid;
      close C_lock_to_ils;
      ---
      UPDATE item_loc_soh
         SET stock_on_hand = stock_on_hand - I_overage_qty,
             av_cost = ROUND(L_upd_to_av_cost, 4),
             tsf_reserved_qty = DECODE(L_to_finisher_ind, 'Y', tsf_reserved_qty - I_overage_qty, tsf_reserved_qty),
             last_update_id        = LP_user,
             last_update_datetime  = SYSDATE,
             soh_update_datetime   = SYSDATE
       WHERE rowid = L_to_rowid;
      ---
      if I_tsf_alloc_type = 'T' then
         if L_to_finisher_ind = 'Y' then
            if UPDATE_FINISHER_RETAIL_UNITS (O_error_message,
                                             I_item,
                                             I_to_loc,
                                             I_to_loc_type,
                                             I_tsf_alloc_no,
                                             -1 * I_overage_qty) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      if UPDATE_STAKE_SKU_LOC(O_error_message,
                              I_item,
                              I_to_loc_type,
                              NVL(L_to_loc_vwh, I_to_loc),
                              L_tran_date,
                              I_overage_qty*-1,
                              0)= FALSE then
         return FALSE;
      end if;
      ---
      L_l10n_fin_rec.av_cost   := ROUND(L_upd_to_av_cost, 4);
      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_fin_rec) = FALSE then
         return FALSE;
      end if;

      -- From_loc
      L_key1 := I_item;
      L_key2 := to_char(I_from_loc);
      ---
      if L_frm_non_stkhldg_f_store = 'N' then
         open C_lock_from_ils(I_item);
         fetch C_lock_from_ils into L_from_rowid;
         close C_lock_from_ils;
         ---
         UPDATE item_loc_soh ils
            SET stock_on_hand = stock_on_hand + I_overage_qty,
                last_update_id        = LP_user,
                last_update_datetime  = SYSDATE,
                soh_update_datetime   = SYSDATE
          WHERE rowid = L_from_rowid;
         ---
         if I_tsf_alloc_type = 'T' then
            ---
            if L_from_finisher_ind = 'Y' then
               ---
               if UPDATE_FINISHER_RETAIL_UNITS (O_error_message,
                                                I_item,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_tsf_alloc_no,
                                                I_overage_qty) = FALSE then
                  return FALSE;
               end if;
               ---
            end if;
         end if;
      end if;
      ---
      L_profit_chrgs_to_loc := L_profit_chrgs_to_loc * (-I_overage_qty);
      L_exp_chrgs_to_loc := L_exp_chrgs_to_loc * (-I_overage_qty);
      ---
      -- call STKLEDGR_SQL.WF_WRITE_FINANCIALS for a franchise transaction.
      ---
      if L_action_type IN ('O', 'R') then
         if STKLEDGR_SQL.WF_WRITE_FINANCIALS(O_error_message,
                                             L_dummy_tcost,
                                             L_dummy_retail,
                                             I_tsf_alloc_no,
                                             I_tsf_alloc_type,
                                             L_tran_date,
                                             I_item,
                                             NULL,                            -- pack no
                                             NULL,                            --- pct_in_pack
                                             L_item_record.dept,
                                             L_item_record.class,
                                             L_item_record.subclass,
                                             -I_overage_qty,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NVL(L_to_loc_vwh, I_to_loc),
                                             I_to_loc_type,
                                             NULL,                            --- Weight UOM
                                             NULL) = FALSE then               --- RMA No
            return FALSE;
         end if;
      else -- not a franchise transaction
         ---
         -- call STKLEDGR_SQL.WRITE_FINANCIALS to write profit up-charge(tran code 28), expense up-charge(tran code 29),
         -- transfer-in/out (tran code 30/32) or intercompany in/out (tran code 37/38)
         ---
         if STKLEDGR_SQL.WRITE_FINANCIALS(O_error_message,
                                          NULL,             -- call type
                                          I_shipment,
                                          I_tsf_alloc_no,
                                          L_tran_date,
                                          I_item,
                                          NULL,             -- pack no
                                          NULL,             -- pct_in_pack
                                          L_item_record.dept,
                                          L_item_record.class,
                                          L_item_record.subclass,
                                          -I_overage_qty,
                                          I_from_loc,
                                          I_from_loc_type,
                                          L_from_finisher_ind,
                                          NVL(L_to_loc_vwh, I_to_loc),
                                          I_to_loc_type,
                                          L_to_finisher_ind,
                                          L_profit_chrgs_to_loc,
                                          L_exp_chrgs_to_loc,
                                          L_intercompany) = FALSE then
            return FALSE;
         end if;
      end if;
   else   -- pack item
      ---
      L_pack_no := I_item;
      ---
      if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                L_receive_as_type,
                                                I_item,
                                                I_to_loc) = FALSE then
         return FALSE;
      end if;
      ---
      L_receive_as_type := nvl(L_receive_as_type, 'E');
      ---
      -- Get from_loc's av_cost for the whole pack
      if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                        I_item,
                                        I_from_loc,
                                        I_from_loc_type,
                                        L_pack_loc_av_cost) = FALSE then
         return FALSE;
      end if;
      ---
      if L_item_record.pack_type != 'B' then  -- Vendor pack
         ---
         -- Calculate pack up chrgs
         ---
         if I_tsf_alloc_type = 'A' then   -- shipment for allocation
            if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                           L_pack_total_chrgs_prim,
                                                           L_pack_profit_chrgs_to_loc,
                                                           L_pack_exp_chrgs_to_loc,
                                                           'A',
                                                           I_tsf_alloc_no,
                                                           NULL,          -- tsf_seq_no
                                                           NULL,          -- shipment
                                                           NULL,          -- ship_seq_no
                                                           I_item,
                                                           NULL,          -- pack_item
                                                           I_from_loc,
                                                           I_from_loc_type,
                                                           I_to_loc,
                                                           I_to_loc_type) = FALSE then
               return FALSE;
            end if;
         else  -- shipment for transfer
            if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                           L_pack_total_chrgs_prim,
                                                           L_pack_profit_chrgs_to_loc,
                                                           L_pack_exp_chrgs_to_loc,
                                                           'T',
                                                           I_tsf_alloc_no,
                                                           I_tsf_seq_no,
                                                           null,          -- shipment, for non 'EG' tsf_type
                                                           null,
                                                           I_item,
                                                           NULL,          -- pack_item
                                                           I_from_loc,
                                                           I_from_loc_type,
                                                           I_to_loc,
                                                           I_to_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      end if; -- end of Vendor pack
      ---
      FOR REC_comp_item IN C_comp_item LOOP
         ---
         --- In Case of frachise return use destination location WAC for prorating pack cost to
         --- component items
         if L_action_type = 'R' then
            if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                             L_pct_in_pack,
                                             L_pack_no,
                                             REC_comp_item.item,
                                             I_to_loc) = FALSE then
               return FALSE;
            end if;
         else
            if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                             L_pct_in_pack,
                                             L_pack_no,
                                             REC_comp_item.item,
                                             I_from_loc) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         -- Get from_loc's av_cost
         if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                           REC_comp_item.item,
                                           I_from_loc,
                                           I_from_loc_type,
                                           L_from_av_cost) = FALSE then
            return FALSE;
         end if;
         ---
         if L_item_record.pack_type != 'B' then  -- Vendor pack
            L_total_chrgs_prim := L_pack_total_chrgs_prim * L_pct_in_pack;
            L_profit_chrgs_to_loc := L_pack_profit_chrgs_to_loc * L_pct_in_pack;
            L_exp_chrgs_to_loc    := L_pack_exp_chrgs_to_loc    * L_pct_in_pack;
         else  -- Buyer pack
            --- look up chrgs at comp level
            if I_tsf_alloc_type = 'A' then   -- shipment for allocation
               if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                              L_total_chrgs_prim,
                                                              L_profit_chrgs_to_loc,
                                                              L_exp_chrgs_to_loc,
                                                              'A',
                                                              I_tsf_alloc_no,
                                                              NULL,          -- tsf_seq_no
                                                              NULL,          -- shipment
                                                              NULL,          -- ship_seq_no
                                                              REC_comp_item.item,
                                                              L_pack_no,          -- pack_item
                                                              I_from_loc,
                                                              I_from_loc_type,
                                                              I_to_loc,
                                                              I_to_loc_type) = FALSE then
                  return FALSE;
               end if;
            else  -- shipment for transfer
               if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                              L_total_chrgs_prim,
                                                              L_profit_chrgs_to_loc,
                                                              L_exp_chrgs_to_loc,
                                                              'T',
                                                              I_tsf_alloc_no,
                                                              I_tsf_seq_no,
                                                              null,          -- shipment, for non 'EG' tsf_type
                                                              null,
                                                              REC_comp_item.item,
                                                              L_pack_no,          -- pack_item
                                                              I_from_loc,
                                                              I_from_loc_type,
                                                              I_to_loc,
                                                              I_to_loc_type) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         end if;
         ---
         -- Convert total chrg from primary currency to to_loc's currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             L_total_chrgs_prim,
                                             L_charge_to_loc,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                         L_upd_to_av_cost,
                                         I_tsf_alloc_no,
                                         I_tsf_alloc_type,
                                         REC_comp_item.item,
                                         L_pack_no,
                                         L_pct_in_pack,
                                         I_from_loc,
                                         I_from_loc_type,
                                         I_to_loc,
                                         I_to_loc_type,
                                         (-I_overage_qty) * REC_comp_item.qty,
                                         L_charge_to_loc,
                                         L_intercompany) = FALSE then
            return FALSE;
         end if;
         ---
         L_key1 := REC_comp_item.item;
         L_key2 := to_char(I_to_loc);
         ---
         open C_lock_to_ils(REC_comp_item.item);
         fetch C_lock_to_ils into L_to_rowid;
         close C_lock_to_ils;
         ---
         UPDATE item_loc_soh
            SET stock_on_hand = DECODE(L_receive_as_type,
                                       'P', stock_on_hand,
                                       stock_on_hand - REC_comp_item.qty * I_overage_qty),
                pack_comp_soh = DECODE(L_receive_as_type,
                                       'P', pack_comp_soh - REC_comp_item.qty * I_overage_qty,
                                       pack_comp_soh),
                av_cost = ROUND(L_upd_to_av_cost, 4),
                last_update_id        = LP_user,
                last_update_datetime  = SYSDATE,
                soh_update_datetime   = DECODE(L_receive_as_type,'P',soh_update_datetime,SYSDATE)
          WHERE rowid = L_to_rowid;
         ---
         L_l10n_fin_rec.av_cost   := ROUND(L_upd_to_av_cost, 4);
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec) = FALSE then
            return FALSE;
         end if;
         ---
         ---
         if L_to_finisher_ind = 'Y' then
            ---
            if UPDATE_FINISHER_RETAIL_UNITS (O_error_message,
                                             REC_comp_item.item,
                                             I_to_loc,
                                             I_to_loc_type,
                                             I_tsf_alloc_no,
                                             -1 * REC_comp_item.qty * I_overage_qty) = FALSE then
               return FALSE;
            end if;
            ---
            UPDATE item_loc_soh
               SET tsf_reserved_qty = DECODE(L_receive_as_type,
                                             'P', tsf_reserved_qty, tsf_reserved_qty + REC_comp_item.qty * I_overage_qty),
                   pack_comp_resv = DECODE(L_receive_as_type,
                                           'P', tsf_reserved_qty - REC_comp_item.qty * I_overage_qty, pack_comp_resv),
                  last_update_id        = LP_user,
                  last_update_datetime  = SYSDATE,
                  soh_update_datetime   = DECODE(L_receive_as_type,'P',soh_update_datetime,SYSDATE)
             WHERE rowid = L_to_rowid;
            ---
         end if;
         ---
         L_key1 := REC_comp_item.item;
         L_key2 := to_char(I_from_loc);
         ---
         if L_frm_non_stkhldg_f_store = 'N' then
            open C_lock_from_ils(REC_comp_item.item);
            fetch C_lock_from_ils into L_from_rowid;
            close C_lock_from_ils;
            ---
            UPDATE item_loc_soh
               SET stock_on_hand = DECODE(L_from_finisher_ind,
                                          'N', stock_on_hand,
                                          stock_on_hand + REC_comp_item.qty * I_overage_qty),
                   pack_comp_soh = DECODE(L_from_finisher_ind,
                                          'N', pack_comp_soh + REC_comp_item.qty * I_overage_qty,
                                          pack_comp_soh),
                   last_update_id        = LP_user,
                   last_update_datetime  = SYSDATE,
                   soh_update_datetime   = DECODE(L_from_finisher_ind,'N',soh_update_datetime,SYSDATE)
             WHERE rowid = L_from_rowid;
         end if;
         ---
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                REC_comp_item.item,
                                                REC_comp_item.dept,
                                                REC_comp_item.class,
                                                REC_comp_item.subclass,
                                                NVL(L_to_loc_vwh, I_to_loc),
                                                I_to_loc_type,
                                                L_tran_date,
                                                L_rdw_tran_code,
                                                NULL,
                                                -REC_comp_item.qty * I_overage_qty,
                                                L_dummy_cost,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_program) = FALSE then
            return FALSE;
         end if;

         ---
         if L_from_finisher_ind = 'Y' then
            if UPDATE_FINISHER_RETAIL_UNITS (O_error_message,
                                             REC_comp_item.item,
                                             I_from_loc,
                                             I_from_loc_type,
                                             I_tsf_alloc_no,
                                             REC_comp_item.qty * I_overage_qty) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         L_profit_chrgs_to_loc := L_profit_chrgs_to_loc * (-I_overage_qty) * REC_comp_item.qty;
         L_exp_chrgs_to_loc := L_exp_chrgs_to_loc * (-I_overage_qty) * REC_comp_item.qty;
         ---
         -- call STKLEDGR_SQL.WF_WRITE_FINANCIALS for a franchise transaction
         ---
         if L_action_type IN ('O', 'R') then
            if STKLEDGR_SQL.WF_WRITE_FINANCIALS(O_error_message,
                                                L_dummy_tcost,
                                                L_dummy_retail,
                                                I_tsf_alloc_no,
                                                I_tsf_alloc_type,
                                                L_tran_date,
                                                REC_comp_item.item,
                                                L_pack_no,                             -- pack no
                                                L_pct_in_pack,                         -- pct_in_pack
                                                REC_comp_item.dept,
                                                REC_comp_item.class,
                                                REC_comp_item.subclass,
                                                -REC_comp_item.qty * I_overage_qty,
                                                I_from_loc,
                                                I_from_loc_type,
                                                NVL(L_to_loc_vwh, I_to_loc),
                                                I_to_loc_type,
                                                NULL,                                  --- Weight UOM
                                                NULL) = FALSE then                     --- WF_order or RMA No
               return FALSE;
            end if;
         else -- not a franchise transaction
            ---
            -- call STKLEDGR_SQL.WRITE_FINANCIALS to write profit up-charge(tran code 28), expense up-charge(tran code 29),
            -- transfer-in/out (tran code 30/32) or intercompany in/out (tran code 37/38)
            ---
            if STKLEDGR_SQL.WRITE_FINANCIALS(O_error_message,
                                             NULL,                 -- call type
                                             I_shipment,
                                             I_tsf_alloc_no,
                                             L_tran_date,
                                             REC_comp_item.item,
                                             L_pack_no,
                                             L_pct_in_pack,
                                             REC_comp_item.dept,
                                             REC_comp_item.class,
                                             REC_comp_item.subclass,
                                             -REC_comp_item.qty * I_overage_qty,
                                             I_from_loc,
                                             I_from_loc_type,
                                             L_from_finisher_ind,
                                             I_to_loc,
                                             I_to_loc_type,
                                             L_to_finisher_ind,
                                             L_profit_chrgs_to_loc,
                                             L_exp_chrgs_to_loc,
                                             L_intercompany) = FALSE then
               return FALSE;
            end if;
         end if;
      END LOOP;
      ---
      if I_to_loc_type = 'W' and L_receive_as_type = 'P' then
         ---
         L_key1 := I_item;
         L_key2 := to_char(I_to_loc);
         ---
         open C_lock_to_ils(I_item);
         fetch C_lock_to_ils into L_to_rowid;
         close C_lock_to_ils;
         ---
         UPDATE item_loc_soh
            SET stock_on_hand = stock_on_hand - I_overage_qty,
                tsf_reserved_qty = DECODE(L_to_finisher_ind, 'Y', tsf_reserved_qty - I_overage_qty, tsf_reserved_qty),
                last_update_id        = LP_user,
                last_update_datetime  = SYSDATE,
                soh_update_datetime   = SYSDATE
          WHERE rowid = L_to_rowid;
         ---
         if L_to_finisher_ind = 'Y' then
            if UPDATE_FINISHER_RETAIL_UNITS (O_error_message,
                                             I_item,
                                             I_to_loc,
                                             I_to_loc_type,
                                             I_tsf_alloc_no,
                                             -1 * I_overage_qty) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      --- Update from_loc's stock_on_hand for the pack
      L_key1 := I_item;
      L_key2 := to_char(I_from_loc);
      ---
      if L_frm_non_stkhldg_f_store = 'N' then
         open C_lock_from_ils(I_item);
         fetch C_lock_from_ils into L_from_rowid;
         close C_lock_from_ils;
         ---
         UPDATE item_loc_soh
            SET stock_on_hand = DECODE(L_from_finisher_ind, 'Y', stock_on_hand,
                                       stock_on_hand + I_overage_qty),
                last_update_id        = LP_user,
                last_update_datetime  = SYSDATE,
                soh_update_datetime   = DECODE(L_from_finisher_ind,'Y',soh_update_datetime,SYSDATE)
          WHERE rowid = L_from_rowid;
         ---
      end if;
   end if;  -- end of pack item
   ---
   --- Post WF returns for forced overage quantity
   if L_action_type IN ('O', 'R') then
      if WF_TRANSFER_SQL.GET_WF_ORDER_RMA_NO(O_error_message,
                                             L_wf_order_no,
                                             L_rma_no,
                                             I_tsf_alloc_type,
                                             I_tsf_alloc_no,
                                             I_to_loc,
                                             I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if L_action_type = 'R' then
         if WF_BOL_SQL.WRITE_WF_BILLING_RETURNS(O_error_message,
                                                L_rma_no,
                                                I_tsf_alloc_no,
                                                I_item,
                                                I_to_loc,
                                                I_to_loc_type,
                                                -I_overage_qty,
                                                L_tran_date) = FALSE then
            return FALSE;
         end if;
      elsif  L_action_type = 'O' then
         if WF_BOL_SQL.WRITE_WF_BILLING_SALES(O_error_message,
                                              L_wf_order_no,
                                              I_item,
                                              I_from_loc,
                                              I_from_loc_type,
                                              I_to_loc,
                                              NULL,
                                              NULL,
                                              -I_overage_qty,
                                              L_tran_date) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END FORCED_OVERAGE;
-----------------------------------------------------------------------------------
FUNCTION FORCED_RECEIVE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                        I_carton           IN     SHIPSKU.CARTON%TYPE,
                        I_adjust_level     IN     VARCHAR2,
                        I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                        I_from_loc         IN     SHIPMENT.FROM_LOC%TYPE,
                        I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                        I_to_loc           IN     SHIPMENT.TO_LOC%TYPE)

RETURN BOOLEAN IS

   L_program       VARCHAR2(64)   := 'STOCK_ORDER_RECONCILE_SQL.FORCED_RECEIVE';
   L_adj_qty       TSFDETAIL.RECONCILED_QTY%TYPE;
   L_adj_user      SHIPSKU.RECONCILE_USER_ID%TYPE := GET_USER;
   L_adj_date      SHIPSKU.RECONCILE_DATE%TYPE := get_vdate;
   L_tsf_seq_no    TSFDETAIL.TSF_SEQ_NO%TYPE;

   L_rowid         ROWID;
   L_table         VARCHAR2(30) := 'SHIPSKU';
   L_key1          VARCHAR2(100) := to_char(I_shipment);
   L_key2          VARCHAR2(100);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_ITEM IS
      SELECT td.item,
             td.tsf_no distro_no,
             td.tsf_seq_no,
             ss.distro_type,
             s.bol_no,
             ss.carton,
             ss.qty_received received_qty,
             ss.qty_expected ship_qty,
             ss.inv_status,
             t.from_loc,
             DECODE(I_to_loc_type, 'S', NVL(ss.actual_receiving_store,t.to_loc), t.to_loc) to_loc
        FROM shipment s,
             shipsku ss,
             tsfhead t,
             tsfdetail td
       WHERE s.shipment = ss.shipment
         AND ss.distro_no = t.tsf_no
         AND t.tsf_no = td.tsf_no
         AND ss.distro_type = 'T'
         AND ss.item = td.item
         AND s.status_code in ('I','R')
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         AND (ss.adjust_type is null or ss.adjust_type not in ('SL','RL','FC','FR','NL','BL'))
         AND s.shipment = I_shipment
       UNION ALL
      SELECT ah.item,
             ah.alloc_no distro_no,
             null tsf_seq_no,
             ss.distro_type,
             s.bol_no,
             ss.carton,
             ss.qty_received received_qty,
             ss.qty_expected ship_qty,
             ss.inv_status,
             ah.wh from_loc,
             DECODE(I_to_loc_type, 'S', NVL(ss.actual_receiving_store,ad.to_loc), ad.to_loc) to_loc
        FROM shipment s,
             shipsku ss,
             wh wh,
             alloc_header ah,
             alloc_detail ad
       WHERE s.shipment = ss.shipment
         AND ss.distro_no = ah.alloc_no
         AND ah.alloc_no = ad.alloc_no
         AND ss.distro_type = 'A'
         AND ad.to_loc = DECODE(ad.to_loc_type, 'W', wh.wh, I_to_loc)
         AND wh.wh(+) = ad.to_loc
         AND wh.physical_wh(+) = I_to_loc
         AND s.status_code in ('I','R')
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         AND (ss.adjust_type is null or ss.adjust_type not in ('SL','RL','FC','FR','NL','BL'))
         AND s.shipment = I_shipment;

   CURSOR C_shipsku_lock(cv_item        shipsku.item%type,
                         cv_distro_no   shipsku.distro_no%type) IS
      SELECT rowid
        FROM shipsku ss
       WHERE ss.shipment = I_shipment
         AND ss.item = cv_item
         AND ss.distro_no = cv_distro_no
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         FOR UPDATE NOWAIT;

   CURSOR C_inv_status_code(cv_inv_status  shipsku.inv_status%type) IS
      SELECT inv_status_code
        FROM inv_status_codes
       WHERE NVL(inv_status,-1) = cv_inv_status;

   CURSOR C_shipment_received IS
      SELECT 'Y'
        FROM shipment sh,
             shipsku  sk
       WHERE sh.shipment = I_shipment
         AND sh.shipment = sk.shipment
         AND NVL(sk.carton,0) = NVL(I_carton,0)
         AND sk.qty_received >= 0
         AND sh.receive_date is not null;

   L_shipment_received_ind   VARCHAR2(1) := 'N';
   L_inv_status_code         INV_STATUS_CODES.INV_STATUS_CODE%TYPE;

   cursor C_SHIP_EXIST is
      select s.rowid
        from shipment s
       where s.shipment = I_shipment
         for update nowait;

   L_doc_exist        VARCHAR2(1) := 'N';

BEGIN
   ---
   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ENTER_SHIP_NUM',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_adjust_level = 'C' and I_carton is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ENT_CARTON',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_shipment_received;
   fetch C_shipment_received into L_shipment_received_ind;
   close C_shipment_received;

    -- stock order receipt reconcile
   FOR REC_ITEM IN C_ITEM LOOP
      if REC_ITEM.received_qty < REC_ITEM.ship_qty then
         ---
            L_adj_qty := REC_ITEM.ship_qty - REC_ITEM.received_qty;

         if FORCED_SHORTAGE(O_error_message,
                            I_shipment,
                            I_carton,
                            REC_ITEM.distro_type,
                            REC_ITEM.distro_no,
                            REC_ITEM.tsf_seq_no,
                            I_from_loc_type,
                            REC_ITEM.from_loc,
                            I_to_loc_type,
                            REC_ITEM.to_loc,
                            REC_ITEM.item,
                            L_adj_qty) = FALSE then
            return FALSE;
         end if;
         ---
      elsif REC_ITEM.received_qty > REC_ITEM.ship_qty then
         ---
            L_adj_qty := REC_ITEM.received_qty - REC_ITEM.ship_qty;

         if FORCED_OVERAGE(O_error_message,
                           I_shipment,
                           I_carton,
                           REC_ITEM.distro_type,
                           REC_ITEM.distro_no,
                           REC_ITEM.tsf_seq_no,
                           I_from_loc_type,
                           REC_ITEM.from_loc,
                           I_to_loc_type,
                           REC_ITEM.to_loc,
                           REC_ITEM.item,
                           L_adj_qty) = FALSE then
            return FALSE;
          end if;
         end if;
         ---
         if REC_ITEM.received_qty != REC_ITEM.ship_qty then
         ---
         L_key2 := REC_ITEM.item;
         ---
         open C_shipsku_lock(REC_ITEM.item,
                             REC_ITEM.distro_no);
         fetch C_shipsku_lock into L_rowid;
         close C_shipsku_lock;
         ---
         update shipsku ss
            set ss.adjust_type = 'FR',
                ss.reconcile_user_id = L_adj_user,
                ss.reconcile_date = L_adj_date
         where ss.rowid = L_rowid;
         ---
      end if;
      ---
     if L_shipment_received_ind ='N' then
         open C_SHIP_EXIST;
         fetch C_SHIP_EXIST into L_rowid;
         close C_SHIP_EXIST;
         ---
         update shipment s
          set s.status_code  = 'R',
              s.receive_date = TO_DATE(TO_CHAR(L_adj_date, 'YYYYMMDD'), 'YYYYMMDD')
         where s.rowid = L_rowid;
      end if;
      ---
   END LOOP;
   ---
   ---
   INSERT INTO doc_close_queue
   SELECT DISTINCT distro_no, distro_type
     FROM shipsku s
    WHERE shipment = I_shipment
      AND adjust_type IS NOT NULL
      AND reconcile_user_id = L_adj_user
      AND reconcile_date = L_adj_date
      AND NOT EXISTS (SELECT 'Y'
                        FROM doc_close_queue
                       WHERE doc = s.distro_no
                         AND doc_type = s.distro_type);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END FORCED_RECEIVE;
--------------------------------------------------------------------------------------
-- Name:    FREIGHT_CLAIM_SHORTAGE
-- Purpose: This function performs the FREIGHT_CLAIM for shortage adjustment
--          when the stock order is marked as shortage(received quantity is less
--          than shipped quantity).
--          The function will decrease in_transit quantity for to_loc and create
--          a tran_data with tran code '62'(Freight Claim).
---------------------------------------------------------------------------------------
FUNCTION FREIGHT_CLAIM_SHORTAGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                                I_carton           IN     SHIPSKU.CARTON%TYPE,
                                I_tsf_alloc_type   IN     SHIPSKU.DISTRO_TYPE%TYPE,
                                I_tsf_alloc_no     IN     SHIPSKU.DISTRO_NO%TYPE,
                                I_tsf_seq_no       IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                                I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                                I_from_loc         IN     SHIPMENT.FROM_LOC%TYPE,
                                I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                                I_to_loc           IN     SHIPMENT.TO_LOC%TYPE,
                                I_item             IN     SHIPSKU.ITEM%TYPE,
                                I_shortage_qty     IN     TSFDETAIL.RECONCILED_QTY%TYPE)

RETURN BOOLEAN IS

   L_program                            VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.FREIGHT_CLAIM_SHORTAGE';

   L_item_record                        ITEM_MASTER%ROWTYPE;
   L_receive_as_type                    ITEM_LOC.RECEIVE_AS_TYPE%TYPE;

   L_total_chrgs_prim                   ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE;
   L_profit_chrgs_to_loc                NUMBER;
   L_exp_chrgs_to_loc                   NUMBER;
   L_pack_total_chrgs_prim              item_loc.unit_retail%TYPE := 0;
   L_pack_profit_chrgs_to_loc           NUMBER                    := 0;
   L_pack_exp_chrgs_to_loc              NUMBER                    := 0;

   L_from_av_cost                       item_loc_soh.av_cost%TYPE;
   L_from_av_cost_toloc_currency        item_loc_soh.av_cost%TYPE;
   L_charge_to_loc                      item_loc_soh.av_cost%TYPE;
   L_av_cost                            item_loc_soh.av_cost%TYPE;
   L_total_av_cost                      item_loc_soh.av_cost%TYPE;
   L_to_av_cost                         item_loc_soh.av_cost%TYPE;
   L_to_soh                             item_loc_soh.stock_on_hand%TYPE;
   L_to_pack_comp_soh                   item_loc_soh.pack_comp_soh%TYPE;
   L_upd_to_av_cost                     item_loc_soh.av_cost%TYPE;

   L_total_retail                       item_loc.unit_retail%TYPE;
   L_unit_retail                        item_loc.unit_retail%TYPE;

   L_pack_no                            item_master.item%TYPE       := NULL;
   L_pack_loc_av_cost                   item_loc_soh.av_cost%TYPE   := NULL;
   L_pack_loc_retail                    item_loc.unit_retail%TYPE   := NULL;
   L_tran_date                          DATE := get_vdate;
   L_tran_code                          tran_data.tran_code%type;

   L_rowid                              ROWID;
   L_table                              VARCHAR2(30) := 'ITEM_LOC_SOH';
   L_key1                               VARCHAR2(100);
   L_key2                               VARCHAR2(100) := to_char(I_to_loc);
   RECORD_LOCKED                        EXCEPTION;
   PRAGMA                               EXCEPTION_INIT(Record_Locked, -54);
   L_to_loc_vwh                         SHIPMENT.TO_LOC%TYPE := NULL;
   L_tsf_type                           TSFHEAD.TSF_TYPE%TYPE := NULL;
   L_l10n_fin_rec                       "L10N_FIN_REC" := L10N_FIN_REC();
   L_from_loc_vwh                       SHIPMENT.FROM_LOC%TYPE := NULL;

   CURSOR C_comp_item IS
      SELECT v.item,
             v.qty,
             im.dept,
             im.class,
             im.subclass
        FROM item_master im,
             v_packsku_qty v
       WHERE im.item = v.item
         AND v.pack_no = I_item
         AND im.inventory_ind = 'Y';

   CURSOR C_lock_to_ils(cv_item    item_loc.item%type) IS
      SELECT rowid
        FROM item_loc_soh
       WHERE item = cv_item
         AND loc = NVL(L_to_loc_vwh,I_to_loc)
         FOR UPDATE NOWAIT;

   L_from_finisher_ind        WH.FINISHER_IND%TYPE := 'N';
   L_to_finisher_ind          WH.FINISHER_IND%TYPE := 'N';
   L_intercompany             BOOLEAN;

   L_pct_in_pack              NUMBER;
   L_to_unit_retail           ITEM_LOC.UNIT_RETAIL%TYPE;
   L_action_type              VARCHAR2(1);

BEGIN
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                       L_item_record,
                                       I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if I_tsf_alloc_type = 'T' then
      ---
      if TRANSFER_SQL.GET_TSF_TYPE(O_error_message,
                                   L_tsf_type,
                                   I_tsf_alloc_no)= FALSE then
         return FALSE;
      end if;
      ---
      if L_tsf_type = 'EG' or (L_tsf_type = 'CO' and LP_system_options_row.oms_ind = 'Y') then
         if I_to_loc_type = 'W' then
            if GET_TO_LOC_VWH(O_error_message,
                              L_to_loc_vwh,
                              I_tsf_alloc_no,
                              I_to_loc_type)= FALSE then
               return FALSE;
            end if;
         end if;
         if I_from_loc_type = 'W' then
            if GET_FROM_LOC_VWH(O_error_message,
                                 L_from_loc_vwh,
                                 I_tsf_alloc_no,
                                 I_from_loc_type)= FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      if TRANSFER_SQL.GET_TSF_ATTRIB(O_error_message,
                                     L_from_finisher_ind,
                                     L_to_finisher_ind,
                                     L_intercompany,
                                     I_tsf_alloc_type,
                                     I_tsf_alloc_no,
                                     NVL(L_from_loc_vwh,I_from_loc),
                                     I_from_loc_type,
                                     NVL(L_to_loc_vwh, I_to_loc),
                                     I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                L_action_type,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_to_loc,
                                                I_to_loc_type) = FALSE then
       return FALSE;
   end if;
   ---
   L_l10n_fin_rec.procedure_key  :='UPDATE_AV_COST';
   L_l10n_fin_rec.country_id     := NULL;
   L_l10n_fin_rec.source_entity  := 'LOC';
   L_l10n_fin_rec.source_id      := I_from_loc;
   L_l10n_fin_rec.source_type    := I_from_loc_type;
   L_l10n_fin_rec.item           := I_item;
   L_l10n_fin_rec.dest_entity    := 'LOC';
   L_l10n_fin_rec.dest_id        := NVL(L_to_loc_vwh, I_to_loc);
   L_l10n_fin_rec.dest_type      := I_to_loc_type;
   ---
   if L_item_record.pack_ind = 'N' then   -- Single item
      ---
      -- Calculate total chrg in primary currency
      ---
      if I_tsf_alloc_type = 'A' then   -- shipment for allocation
         if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                        L_total_chrgs_prim,
                                                        L_profit_chrgs_to_loc,
                                                        L_exp_chrgs_to_loc,
                                                        'A',
                                                        I_tsf_alloc_no,
                                                        NULL,          -- tsf_seq_no
                                                        NULL,          -- shipment
                                                        NULL,          -- ship_seq_no
                                                        I_item,
                                                        NULL,          -- pack_item
                                                        I_from_loc,
                                                        I_from_loc_type,
                                                        I_to_loc,
                                                        I_to_loc_type) = FALSE then
            return FALSE;
         end if;
      else  -- shipment for transfer
         if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                        L_total_chrgs_prim,
                                                        L_profit_chrgs_to_loc,
                                                        L_exp_chrgs_to_loc,
                                                        'T',
                                                        I_tsf_alloc_no,
                                                        I_tsf_seq_no,
                                                        null,          -- shipment, for non 'EG' tsf_type
                                                        null,
                                                        I_item,
                                                        NULL,          -- pack_item
                                                        I_from_loc,
                                                        I_from_loc_type,
                                                        I_to_loc,
                                                        I_to_loc_type) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- Convert total chrg from primary currency to to_loc's currency
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          NULL,
                                          NULL,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_total_chrgs_prim,
                                          L_charge_to_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                      L_upd_to_av_cost,
                                      I_tsf_alloc_no,
                                      I_tsf_alloc_type,
                                      I_item,
                                      NULL,
                                      NULL,
                                      NVL(L_from_loc_vwh,I_from_loc),
                                      I_from_loc_type,
                                      NVL(L_to_loc_vwh, I_to_loc),
                                      I_to_loc_type,
                                      I_shortage_qty * -1,
                                      L_charge_to_loc,
                                      L_intercompany) = FALSE then
         return FALSE;
      end if;
      ---
      -- Decrease in_transit qty for to_loc
      L_key1 := I_item;
      ---
      open C_lock_to_ils(I_item);
      fetch C_lock_to_ils into L_rowid;
      close C_lock_to_ils;
      ---
      UPDATE item_loc_soh
         SET in_transit_qty = in_transit_qty - I_shortage_qty,
             av_cost = ROUND(L_upd_to_av_cost, 4)
       WHERE rowid = L_rowid;
      ---
      L_l10n_fin_rec.av_cost   := ROUND(L_upd_to_av_cost, 4);
      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_fin_rec) = FALSE then
         return FALSE;
      end if;
      ---
      -- Stock Ledger, Freight Claim for to_loc
      if STKLEDGR_SQL.GET_TSF_COSTS_RETAILS(O_error_message,
                                            L_av_cost,
                                            L_unit_retail,
                                            L_to_unit_retail,
                                            I_tsf_alloc_no,
                                            L_intercompany,
                                            L_from_finisher_ind,
                                            L_to_finisher_ind,
                                            I_item,
                                            NVL(L_from_loc_vwh,I_from_loc),
                                            I_from_loc_type,
                                            NVL(L_to_loc_vwh, I_to_loc),
                                            I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      L_total_av_cost := I_shortage_qty * L_av_cost;
      L_total_retail := I_shortage_qty * L_to_unit_retail;  -- use unit_retail at to_loc
      ---
      L_tran_code := 62;  -- Freight Claim for to_loc
      ---
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_item_record.dept,
                                             L_item_record.class,
                                             L_item_record.subclass,
                                             NVL(L_to_loc_vwh, I_to_loc),
                                             I_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,              -- adj_code
                                             I_shortage_qty,    -- units
                                             L_total_av_cost,   -- total_cost
                                             L_total_retail,
                                             I_tsf_alloc_no,    -- ref_no_1
                                             I_shipment,        -- ref_no_2
                                             NULL,              -- tsf_source_store
                                             NULL,              -- tsf_source_wh
                                             NULL,              -- old_unit_retail
                                             NULL,              -- new_unit_retail
                                             NULL,              -- source_dept
                                             NULL,              -- source_class
                                             NULL,              -- source_subclass
                                             L_program) = FALSE then
         return FALSE;
      end if;
      ---
   else   -- pack item
      ---
      L_pack_no := I_item;
      ---
      if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                L_receive_as_type,
                                                I_item,
                                                I_to_loc) = FALSE then
         return FALSE;
      end if;
      ---
      L_receive_as_type := NVL(L_receive_as_type, 'E');
      ---
      -- Get from_loc's av_cost for the whole pack
      if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                        I_item,
                                        NVL(L_from_loc_vwh,I_from_loc),
                                        I_from_loc_type,
                                        L_pack_loc_av_cost) = FALSE then
         return FALSE;
      end if;
      ---
      if L_item_record.pack_type != 'B' then  -- Vendor pack
         ---
         -- Calculate pack chrg in primary currency
         ---
         if I_tsf_alloc_type = 'A' then   -- shipment for allocation
            if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                           L_pack_total_chrgs_prim,
                                                           L_pack_profit_chrgs_to_loc,
                                                           L_pack_exp_chrgs_to_loc,
                                                           'A',
                                                           I_tsf_alloc_no,
                                                           NULL,          -- tsf_seq_no
                                                           NULL,          -- shipment
                                                           NULL,          -- ship_seq_no
                                                           I_item,
                                                           NULL,          -- pack_item
                                                           I_from_loc,
                                                           I_from_loc_type,
                                                           I_to_loc,
                                                           I_to_loc_type) = FALSE then
               return FALSE;
            end if;
         else  -- shipment for transfer
            if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                           L_pack_total_chrgs_prim,
                                                           L_pack_profit_chrgs_to_loc,
                                                           L_pack_exp_chrgs_to_loc,
                                                           'T',
                                                           I_tsf_alloc_no,
                                                           I_tsf_seq_no,
                                                           null,          -- shipment, for non 'EG' tsf_type
                                                           null,
                                                           I_item,
                                                           NULL,          -- pack_item
                                                           I_from_loc,
                                                           I_from_loc_type,
                                                           I_to_loc,
                                                           I_to_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      end if; -- end of Vendor pack
      ---
      FOR REC_comp_item IN C_comp_item LOOP
         ---
         --- In Case of frachise return use destination location WAC for prorating pack cost to
         --- component items
         if L_action_type = 'R' then
            if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                             L_pct_in_pack,
                                             L_pack_no,
                                             REC_comp_item.item,
                                             I_to_loc) = FALSE then
               return FALSE;
            end if;
         else
            if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                             L_pct_in_pack,
                                             L_pack_no,
                                             REC_comp_item.item,
                                             I_from_loc) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         -- Get from_loc's av_cost
         if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                           REC_comp_item.item,
                                           NVL(L_from_loc_vwh,I_from_loc),
                                           I_from_loc_type,
                                           L_from_av_cost) = FALSE then
            return FALSE;
         end if;
         ---
         if L_item_record.pack_type != 'B' then  -- Vendor pack
            L_total_chrgs_prim := L_pack_total_chrgs_prim * L_pct_in_pack;
            L_profit_chrgs_to_loc := L_pack_profit_chrgs_to_loc * L_pct_in_pack;
            L_exp_chrgs_to_loc    := L_pack_exp_chrgs_to_loc    * L_pct_in_pack;
         else  -- Buyer pack
            --- look up chrgs at comp level
            if I_tsf_alloc_type = 'A' then   -- shipment for allocation
               if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                              L_total_chrgs_prim,
                                                              L_profit_chrgs_to_loc,
                                                              L_exp_chrgs_to_loc,
                                                              'A',
                                                              I_tsf_alloc_no,
                                                              NULL,          -- tsf_seq_no
                                                              NULL,          -- shipment
                                                              NULL,          -- ship_seq_no
                                                              REC_comp_item.item,
                                                              L_pack_no,          -- pack_item
                                                              I_from_loc,
                                                              I_from_loc_type,
                                                              I_to_loc,
                                                              I_to_loc_type) = FALSE then
                  return FALSE;
               end if;
            else  -- shipment for transfer
               if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                              L_total_chrgs_prim,
                                                              L_profit_chrgs_to_loc,
                                                              L_exp_chrgs_to_loc,
                                                              'T',
                                                              I_tsf_alloc_no,
                                                              I_tsf_seq_no,
                                                              null,          -- shipment, for non 'EG' tsf_type
                                                              null,
                                                              REC_comp_item.item,
                                                              L_pack_no,          -- pack_item
                                                              I_from_loc,
                                                              I_from_loc_type,
                                                              I_to_loc,
                                                              I_to_loc_type) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         end if;
         ---
         -- Convert total chrg from primary currency to to_loc's currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             L_total_chrgs_prim,
                                             L_charge_to_loc,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                         L_upd_to_av_cost,
                                         I_tsf_alloc_no,
                                         I_tsf_alloc_type,
                                         REC_comp_item.item,
                                         L_pack_no,
                                         L_pct_in_pack,
                                         NVL(L_from_loc_vwh,I_from_loc),
                                         I_from_loc_type,
                                         NVL(L_to_loc_vwh, I_to_loc),
                                         I_to_loc_type,
                                         REC_comp_item.qty * I_shortage_qty * -1,
                                         L_charge_to_loc,
                                         L_intercompany) = FALSE then
            return FALSE;
         end if;
         ---
         L_key1 := REC_comp_item.item;
         ---
         open C_lock_to_ils(REC_comp_item.item);
         fetch C_lock_to_ils into L_rowid;
         close C_lock_to_ils;
         ---
         UPDATE item_loc_soh
            SET in_transit_qty = DECODE(L_receive_as_type,
                                        'P', in_transit_qty,
                                        in_transit_qty - REC_comp_item.qty * I_shortage_qty),
                pack_comp_intran = DECODE(L_receive_as_type,
                                          'P', pack_comp_intran - REC_comp_item.qty * I_shortage_qty,
                                          pack_comp_intran),
                av_cost = ROUND(L_upd_to_av_cost, 4)
          WHERE rowid = L_rowid;
         ---
         L_l10n_fin_rec.av_cost   := ROUND(L_upd_to_av_cost, 4);
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec) = FALSE then
            return FALSE;
         end if;
         ---
         -- Stock Ledger, Freight Claim for to_loc
         if STKLEDGR_SQL.GET_TSF_COSTS_RETAILS(O_error_message,
                                               L_av_cost,
                                               L_unit_retail,
                                               L_to_unit_retail,
                                               I_tsf_alloc_no,
                                               L_intercompany,
                                               L_from_finisher_ind,
                                               L_to_finisher_ind,
                                               REC_comp_item.item,
                                               NVL(L_from_loc_vwh,I_from_loc),
                                               I_from_loc_type,
                                               NVL(L_to_loc_vwh, I_to_loc),
                                               I_to_loc_type) = FALSE then
            return FALSE;
         end if;
         ---
         L_total_av_cost := I_shortage_qty * REC_comp_item.qty * L_av_cost;
         L_total_retail := I_shortage_qty * REC_comp_item.qty * L_to_unit_retail;  -- use unit_retail at to_loc
         ---
         L_tran_code := 62;  -- Freight Claim for to_loc
         ---
         if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                REC_comp_item.item,
                                                REC_comp_item.dept,
                                                REC_comp_item.class,
                                                REC_comp_item.subclass,
                                                NVL(L_to_loc_vwh, I_to_loc),
                                                I_to_loc_type,
                                                L_tran_date,
                                                L_tran_code,
                                                NULL,              -- adj_code
                                                I_shortage_qty * REC_comp_item.qty,    -- units
                                                L_total_av_cost,   -- total_cost
                                                L_total_retail,
                                                I_tsf_alloc_no,    -- ref_no_1
                                                I_shipment,        -- ref_no_2
                                                NULL,              -- tsf_source_store
                                                NULL,              -- tsf_source_wh
                                                NULL,              -- old_unit_retail
                                                NULL,              -- new_unit_retail
                                                NULL,              -- source_dept
                                                NULL,              -- source_class
                                                NULL,              -- source_subclass
                                                L_program) = FALSE then
            return FALSE;
         end if;
         ---
      END LOOP;
      ---
      if I_to_loc_type = 'W' and L_receive_as_type = 'P' then
         ---
         L_key1 := I_item;
         ---
         open C_lock_to_ils(I_item);
         fetch C_lock_to_ils into L_rowid;
         close C_lock_to_ils;
         ---
         UPDATE item_loc_soh
            SET in_transit_qty = in_transit_qty - I_shortage_qty
          WHERE rowid = L_rowid;
         ---
      end if;
      ---
   end if;  -- end of pack item
   ---
   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END FREIGHT_CLAIM_SHORTAGE;
-----------------------------------------------------------------------------------
FUNCTION FREIGHT_CLAIM(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                       I_carton           IN     SHIPSKU.CARTON%TYPE,
                       I_adjust_level     IN     VARCHAR2,
                       I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                       I_from_loc         IN     SHIPMENT.FROM_LOC%TYPE,
                       I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                       I_to_loc           IN     SHIPMENT.TO_LOC%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)   := 'STOCK_ORDER_RECONCILE_SQL.FREIGHT_CLAIM';
   L_invalid_param   VARCHAR2(20);
   L_adj_qty         TSFDETAIL.RECONCILED_QTY%TYPE;
   L_adj_type_desc   CODE_DETAIL.CODE_DESC%TYPE;
   L_overage_exist   VARCHAR2(1) := 'N';
   L_adj_user        SHIPSKU.RECONCILE_USER_ID%TYPE := GET_USER;
   L_adj_date        SHIPSKU.RECONCILE_DATE%TYPE := get_vdate;
   L_rowid           ROWID;
   L_table           VARCHAR2(30)  := 'SHIPSKU';
   L_key1            VARCHAR2(100) := to_char(I_shipment);
   L_key2            VARCHAR2(100);

   L_doc_exist       VARCHAR2(1) := 'N';

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);


   CURSOR C_ITEM IS
      SELECT td.item,
             td.tsf_no distro_no,
             td.tsf_seq_no,
             ss.distro_type,
             ss.qty_received,
             ss.qty_expected,
             t.from_loc,
             DECODE(I_to_loc_type, 'S', NVL(ss.actual_receiving_store,t.to_loc), t.to_loc) to_loc
        FROM shipment s,
             shipsku ss,
             tsfhead t,
             tsfdetail td
       WHERE s.shipment = ss.shipment
         AND ss.distro_no = t.tsf_no
         AND t.tsf_no = td.tsf_no
         AND ss.distro_type = 'T'
         AND ss.item = td.item
         AND s.status_code = 'R'
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         AND (ss.adjust_type is null or ss.adjust_type not in ('SL','RL','FC','FR','NL','BL'))
         AND ss.qty_received < ss.qty_expected
         AND s.shipment = I_shipment
       UNION ALL
      SELECT ah.item,
             ah.alloc_no distro_no,
             null tsf_seq_no,
             ss.distro_type,
             ss.qty_received,
             ss.qty_expected,
             ah.wh from_loc,
             DECODE(I_to_loc_type, 'S', NVL(ss.actual_receiving_store,ad.to_loc), ad.to_loc) to_loc
        FROM shipment s,
             shipsku ss,
             wh wh,
             alloc_header ah,
             alloc_detail ad
       WHERE s.shipment = ss.shipment
         AND ss.distro_no = ah.alloc_no
         AND ah.alloc_no = ad.alloc_no
         AND ss.distro_type = 'A'
         AND ad.to_loc = DECODE(ad.to_loc_type, 'W', wh.wh, I_to_loc)
         AND wh.wh(+) = ad.to_loc
         AND wh.physical_wh(+) = I_to_loc
         AND s.status_code = 'R'
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         AND (ss.adjust_type is null or ss.adjust_type not in ('SL','RL','FC','FR','NL','BL'))
         AND ss.qty_received < ss.qty_expected
         AND s.shipment = I_shipment;

   CURSOR C_shipsku_lock(cv_item        shipsku.item%type,
                         cv_distro_no   shipsku.distro_no%type) IS
      SELECT rowid
        FROM shipsku ss
       WHERE ss.shipment = I_shipment
         AND ss.item = cv_item
         AND ss.distro_no = cv_distro_no
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         FOR UPDATE NOWAIT;

BEGIN
   ---
   if I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   elsif I_adjust_level = 'C' and I_carton is NULL then
      L_invalid_param := 'I_carton';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                             L_program,
                                             L_invalid_param,
                                             'NULL');
      return FALSE;
   end if;
   ---
   FOR rec IN C_ITEM LOOP
      ---
         L_adj_qty := rec.qty_expected - rec.qty_received;

      if FREIGHT_CLAIM_SHORTAGE(O_error_message,
                                I_shipment,
                                I_carton,
                                rec.distro_type,
                                rec.distro_no,
                                rec.tsf_seq_no,
                                I_from_loc_type,
                                rec.from_loc,
                                I_to_loc_type,
                                rec.to_loc,
                                rec.item,
                                L_adj_qty) = FALSE then
         return FALSE;
      end if;
      ---
      L_key2 := rec.item;
      ---
      open C_shipsku_lock(rec.item,
                          rec.distro_no);
      fetch C_shipsku_lock into L_rowid;
      close C_shipsku_lock;
      ---
      update shipsku ss
         set ss.adjust_type = 'FC',
             ss.reconcile_user_id = L_adj_user,
             ss.reconcile_date = L_adj_date
        where rowid = L_rowid;
      ---
   END LOOP;
   ---
   INSERT INTO doc_close_queue
   SELECT DISTINCT distro_no, distro_type
     FROM shipsku s
    WHERE shipment = I_shipment
      AND adjust_type IS NOT NULL
      AND reconcile_user_id = L_adj_user
      AND reconcile_date = L_adj_date
      AND NOT EXISTS (SELECT 'Y'
                        FROM doc_close_queue
                       WHERE doc = s.distro_no
                         AND doc_type = s.distro_type);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END FREIGHT_CLAIM;
-----------------------------------------------------------------------------------
FUNCTION FROM_LOC_ADJUST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_shipment         IN       SHIPMENT.SHIPMENT%TYPE,
                         I_carton           IN       SHIPSKU.CARTON%TYPE,
                         I_adjust_level     IN       VARCHAR2,
                         I_from_loc_type    IN       SHIPMENT.FROM_LOC_TYPE%TYPE,
                         I_from_loc         IN       SHIPMENT.FROM_LOC%TYPE,
                         I_to_loc_type      IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                         I_to_loc           IN       SHIPMENT.TO_LOC%TYPE,
                         I_distro_no        IN       TSFHEAD.TSF_NO%TYPE DEFAULT NULL,
                         I_document_type    IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)   := 'STOCK_ORDER_RECONCILE_SQL.FROM_LOC_ADJUST';

   L_adj_qty       TSFDETAIL.RECONCILED_QTY%TYPE;
   L_adj_user      SHIPSKU.RECONCILE_USER_ID%TYPE := GET_USER;
   L_adj_date      SHIPSKU.RECONCILE_DATE%TYPE := get_vdate;

   L_rowid         ROWID;
   L_table         VARCHAR2(30) := 'SHIPSKU';
   L_key1          VARCHAR2(100) := to_char(I_shipment);
   L_key2          VARCHAR2(100);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_ITEM IS
      SELECT td.item,
             td.tsf_no distro_no,
             td.tsf_seq_no,
             ss.distro_type,
             NVL(ss.qty_received,0) received_qty,
             ss.qty_expected ship_qty,
             t.from_loc,
             DECODE(I_to_loc_type, 'S', NVL(ss.actual_receiving_store,t.to_loc), t.to_loc) to_loc,
             ss.rowid
        FROM shipment s,
             shipsku ss,
             tsfhead t,
             tsfdetail td
       WHERE s.shipment = ss.shipment
         AND ss.distro_no = t.tsf_no
         AND t.tsf_no = td.tsf_no
         AND ss.distro_type = 'T'
         AND ss.item = td.item
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         AND (ss.adjust_type is null or ss.adjust_type not in ('SL','RL','FC','FR','NL','BL'))
         AND s.shipment = I_shipment
         AND ss.distro_no = NVL(I_distro_no, ss.distro_no)
         AND ss.distro_type = NVL(I_document_type,'T')
       UNION ALL
      SELECT ah.item,
             ah.alloc_no distro_no,
             null tsf_seq_no,
             ss.distro_type,
             ss.qty_received received_qty,
             ss.qty_expected ship_qty,
             ah.wh from_loc,
             DECODE(I_to_loc_type, 'S', NVL(ss.actual_receiving_store,ad.to_loc), ad.to_loc) to_loc,
             ss.rowid
        FROM shipment s,
             shipsku ss,
             wh wh,
             alloc_header ah,
             alloc_detail ad
       WHERE s.shipment = ss.shipment
         AND ss.distro_no = ah.alloc_no
         AND ah.alloc_no = ad.alloc_no
         AND ss.distro_type = 'A'
         AND ad.to_loc = DECODE(ad.to_loc_type, 'W', wh.wh, I_to_loc)
         AND wh.wh(+) = ad.to_loc
         AND wh.physical_wh(+) = I_to_loc
         AND s.status_code = 'R'
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         AND (ss.adjust_type is null or ss.adjust_type not in ('SL','RL','FC','FR','NL','BL'))
         AND s.shipment = I_shipment
         AND ss.distro_no = NVL(I_distro_no, ss.distro_no)
         AND ss.distro_type = NVL(I_document_type,'A');

   CURSOR C_shipsku_lock(cv_rowid    rowid) IS
      SELECT rowid
        FROM shipsku ss
       WHERE ss.shipment = I_shipment
         AND ss.rowid = cv_rowid
         FOR UPDATE NOWAIT;

   L_doc_exist        VARCHAR2(1) := 'N';

BEGIN
   ---
   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ENTER_SHIP_NUM',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_adjust_level = 'C' and I_carton is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ENT_CARTON',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   FOR REC_ITEM IN C_ITEM LOOP
      ---
      if REC_ITEM.received_qty < REC_ITEM.ship_qty then
         ---
            L_adj_qty := REC_ITEM.ship_qty - REC_ITEM.received_qty;

         if ADJUST_SHORTAGE(O_error_message,
                            I_shipment,
                            I_carton,
                            REC_ITEM.distro_type,
                            REC_ITEM.distro_no,
                            REC_ITEM.tsf_seq_no,
                            I_from_loc_type,
                            REC_ITEM.from_loc,
                            I_to_loc_type,
                            REC_ITEM.to_loc,
                            REC_ITEM.item,
                            L_adj_qty,
                            'SL') = FALSE then  --adjust_type
            return FALSE;
         end if;
      end if;
      ---
      L_key2 := REC_ITEM.item;
      ---
      open C_shipsku_lock(REC_ITEM.rowid);
      close C_shipsku_lock;
      ---
      update shipsku ss
         set ss.adjust_type = 'SL',
             ss.reconcile_user_id = L_adj_user,
             ss.reconcile_date = L_adj_date
       where rowid = REC_ITEM.rowid;
      ---
   END LOOP;
   ---
   INSERT INTO doc_close_queue
   SELECT DISTINCT distro_no, distro_type
     FROM shipsku s
    WHERE shipment = I_shipment
      AND adjust_type IS NOT NULL
      AND reconcile_user_id = L_adj_user
      AND reconcile_date = L_adj_date
      AND distro_no = NVL(I_distro_no, distro_no)
      AND distro_type = NVL(I_document_type,distro_type)
      AND NOT EXISTS (SELECT 'Y'
                        FROM doc_close_queue
                       WHERE doc = s.distro_no
                         AND doc_type = s.distro_type);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                        L_program,
                        to_char(SQLCODE));
      return FALSE;

END FROM_LOC_ADJUST;
-----------------------------------------------------------------------------------
FUNCTION TO_LOC_ADJUST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_shipment         IN       SHIPMENT.SHIPMENT%TYPE,
                       I_carton           IN       SHIPSKU.CARTON%TYPE,
                       I_adjust_level     IN       VARCHAR2,
                       I_from_loc_type    IN       SHIPMENT.FROM_LOC_TYPE%TYPE,
                       I_from_loc         IN       SHIPMENT.FROM_LOC%TYPE,
                       I_to_loc_type      IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                       I_to_loc           IN       SHIPMENT.TO_LOC%TYPE,
                       I_distro_no        IN       TSFHEAD.TSF_NO%TYPE DEFAULT NULL,
                       I_document_type    IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)   := 'STOCK_ORDER_RECONCILE_SQL.TO_LOC_ADJUST';
   L_invalid_param   VARCHAR2(20);
   L_adj_qty         TSFDETAIL.RECONCILED_QTY%TYPE;
   L_adj_type_desc   CODE_DETAIL.CODE_DESC%TYPE;
   L_overage_exist   VARCHAR2(1) := 'N';
   L_adj_user        SHIPSKU.RECONCILE_USER_ID%TYPE := GET_USER;
   L_adj_date        SHIPSKU.RECONCILE_DATE%TYPE := get_vdate;
   L_rowid           ROWID;
   L_table           VARCHAR2(30) := 'SHIPSKU';
   L_key1            VARCHAR2(100) := to_char(I_shipment);
   L_key2            VARCHAR2(100);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);


   CURSOR C_ITEM IS
      SELECT td.item,
             td.tsf_no distro_no,
             td.tsf_seq_no,
             ss.distro_type,
             NVL(ss.qty_received,0) qty_received,
             ss.qty_expected,
             t.from_loc,
             DECODE(I_to_loc_type, 'S', NVL(ss.actual_receiving_store,t.to_loc), t.to_loc) to_loc,
             ss.rowid
        FROM shipment s,
             shipsku ss,
             tsfhead t,
             tsfdetail td
       WHERE s.shipment = ss.shipment
         AND ss.distro_no = t.tsf_no
         AND t.tsf_no = td.tsf_no
         AND ss.distro_type = 'T'
         AND ss.item = td.item
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         AND (ss.adjust_type is null or ss.adjust_type not in ('SL','RL','FC','FR','NL','BL'))
         AND NVL(ss.qty_received,0) < ss.qty_expected
         AND s.shipment = I_shipment
         AND ss.distro_no = NVL(I_distro_no, ss.distro_no)
         AND ss.distro_type = NVL(I_document_type,'T')
       UNION ALL
      SELECT ah.item,
             ah.alloc_no distro_no,
             null tsf_seq_no,
             ss.distro_type,
             ss.qty_received,
             ss.qty_expected,
             ah.wh from_loc,
             DECODE(I_to_loc_type, 'S', NVL(ss.actual_receiving_store,ad.to_loc), ad.to_loc) to_loc,
             ss.rowid
        FROM shipment s,
             shipsku ss,
             wh wh,
             alloc_header ah,
             alloc_detail ad
       WHERE s.shipment = ss.shipment
         AND ss.distro_no = ah.alloc_no
         AND ah.alloc_no = ad.alloc_no
         AND ss.distro_type = 'A'
         AND ad.to_loc = DECODE(ad.to_loc_type, 'W', wh.wh, I_to_loc)
         AND wh.wh(+) = ad.to_loc
         AND wh.physical_wh(+) = I_to_loc
         AND s.status_code = 'R'
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         AND (ss.adjust_type is null or ss.adjust_type not in ('SL','RL','FC','FR','NL','BL'))
         AND ss.qty_received < ss.qty_expected
         AND s.shipment = I_shipment
         AND ss.distro_no = NVL(I_distro_no, ss.distro_no)
         AND ss.distro_type = NVL(I_document_type,'A');


   CURSOR C_shipsku_lock(cv_rowid   ROWID) IS
      SELECT rowid
        FROM shipsku ss
       WHERE ss.shipment = I_shipment
         AND ss.rowid = cv_rowid
         FOR UPDATE NOWAIT;

   L_doc_exist        VARCHAR2(1) := 'N';

BEGIN

   if I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   elsif I_adjust_level = 'C' and I_carton is NULL then
      L_invalid_param := 'I_carton';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                             L_program,
                                             L_invalid_param,
                                             'NULL');
      return FALSE;
   end if;

   FOR rec IN C_ITEM LOOP
         L_adj_qty := rec.qty_expected - rec.qty_received;

      if ADJUST_SHORTAGE(O_error_message,
                         I_shipment,
                         I_carton,
                         rec.distro_type,
                         rec.distro_no,
                         rec.tsf_seq_no,
                         I_from_loc_type,
                         rec.from_loc,
                         I_to_loc_type,
                         rec.to_loc,
                         rec.item,
                         L_adj_qty,
                         'RL') = FALSE then  --adjust_type
         return FALSE;
      end if;
      ---
      L_key2 := rec.item;
      ---
      open C_shipsku_lock(rec.rowid);
      close C_shipsku_lock;
      ---
      update shipsku ss
         set ss.adjust_type       = 'RL',
             ss.reconcile_user_id = L_adj_user,
             ss.reconcile_date    = L_adj_date
        where rowid = rec.rowid;
      ---
   END LOOP;
   ---
   INSERT INTO doc_close_queue
   SELECT DISTINCT distro_no, distro_type
     FROM shipsku s
    WHERE shipment = I_shipment
      AND adjust_type IS NOT NULL
      AND reconcile_user_id = L_adj_user
      AND reconcile_date = L_adj_date
      AND distro_no = NVL(I_distro_no, distro_no)
      AND distro_type = NVL(I_document_type,distro_type)
      AND NOT EXISTS (SELECT 'Y'
                        FROM doc_close_queue
                       WHERE doc = s.distro_no
                         AND doc_type = s.distro_type);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END TO_LOC_ADJUST;
----------------------------------------------------------------------------------
FUNCTION POPULATE_TEMP_TABLES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_where_clause   IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(61) := 'STOCK_ORDER_RECONCILE_SQL.POPULATE_TEMP_TABLES';
   L_from_loc_desc            PARTNER.PARTNER_DESC%TYPE;
   L_to_loc_desc              PARTNER.PARTNER_DESC%TYPE;
   L_primary_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_from_loc_currency        CURRENCIES.CURRENCY_CODE%TYPE;
   L_local_currency           CURRENCIES.CURRENCY_CODE%TYPE; -- to_loc currency
   L_total_cost_primary       NUMBER;
   L_total_cost_local         NUMBER;
   L_total_retail_primary     NUMBER;
   L_total_retail_local       NUMBER;
   L_previous_from_loc        SO_SHIPMENT_TEMP.FROM_LOC%TYPE;
   L_previous_to_loc          SO_SHIPMENT_TEMP.TO_LOC%TYPE;
   L_so_shipsku_temp_stmt     VARCHAR2(4000);
   L_from_loc_type            VARCHAR2(1);
   L_to_loc_type              VARCHAR2(1);

   cursor C_SHIPSKU is
      select sk.rowid
           , sk.shipment
           , sk.carton
           , sk.total_cost_from_loc
           , sk.total_retail_from_loc
           , sh.to_loc_type
           , sh.to_loc
           , sh.from_loc_type
           , sh.from_loc
        from so_shipsku_temp sk
           , shipment        sh
       where sk.shipment = sh.shipment;


   cursor C_SHIPMENT is
      select sh.shipment
           , sh.to_loc_type
           , sh.to_loc
           , sh.from_loc_type
           , sh.from_loc
        from so_shipment_temp sh;

BEGIN

  if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                       L_primary_currency) = FALSE then
      return FALSE;
   end if;

   --- This cursor selects shipsku records which meet these criteria:
   ---    records for transfers or allocations
   ---    not yet adjusted
   ---    there is an overage or shortage
   ---    the shipment is not closed
   ---
   --- The cursor rolls these records up to the shipment/carton level and
   --- sums up the qty, total cost, and total retail for the overages/shortages.
   ---
   --- The cursor sets the tampered_ind to 'Y' if any one of the rolled up
   --- records has tampered_ind = 'Y' ('N' otherwise).
   ---
   --- The cursor sets the carton_over_short_ind to:
   ---     1 if all item level records are overages
   ---    -1 if all item level records are shortages
   ---     0 if both overage and shortage item level records exist
   --- If the minimum over_short value is greater than zero then we know the
   --- maximum is also, so all records are overages (1). If that is not true,
   --- then if the maximumn over_short value is less than zero we know that
   --- the minimum is also and all records are shortages (-1).  If neither
   --- of those statements are true then there are both overages and shortages (0).
   ---
   --- If any of the shipsku line items for this carton have not been
   --- received then the carton_unreceived_ind is set to 'Y' ('N' otherwise)
   --- (carton_unreceived_ind is not used in the form - this column can be dropped later).
   --- A shipment is unreceived only if all line items of the shipment have not been received.
   ---
   --- If adjust type is 'RE' (Received Elsewhere) then set it to NULL in temp table.
   ---
   --- The wrong_store_ind is set to 'Y' if any line item of the carton was received
   --- at wrong store. ('N' otherwise)
   ---
   L_so_shipsku_temp_stmt :=
     'insert into so_shipsku_temp '||
     ' ( shipment'||
     '  , carton'||
     '  , adjust_type'||
     '  , over_short'||
     '  , total_cost_from_loc'||
     '  , total_retail_from_loc'||
     '  , tampered_ind'||
     '  , carton_over_short_ind'||
     '  , carton_unreceived_ind'||
     '  , wrong_store_ind)'||
     ' select sk.shipment'||
     ' , sk.carton'||
     ' , NULL'||
     ' , SUM(NVL(sk.qty_received,0)) - sum(NVL(sk.qty_expected,0))'||
     ' , SUM(sk.unit_cost * (NVL(sk.qty_received, 0) - NVL(sk.qty_expected, 0)))'||
     ' , SUM(sk.unit_retail * (NVL(sk.qty_received, 0) - NVL(sk.qty_expected, 0)))'||
     ' , MAX(NVL(sk.tampered_ind,''N''))'||
     ' , DECODE(SIGN(MIN(NVL(sk.qty_received,0) - NVL(sk.qty_expected,0))),'||
     '            1, 1,'||
     '            DECODE(SIGN(MAX(NVL(sk.qty_received,0) - NVL(sk.qty_expected,0))),'||
     '                   -1, -1,'||
     '                   0))'||
     ' , DECODE(MIN(NVL(sk.qty_received,0)),'||
     '            0, ''Y'','||
     '            ''N'')'||
     ' , MAX(DECODE(sk.actual_receiving_store, NULL, ''N'', ''Y''))'||
     ' from shipsku sk,'||
     '      v_shipment sh'||
     ' where NVL(sk.distro_type,''-*-'') in (''T'',''A'')'||
     ' and (sk.adjust_type           is NULL'||
     '      or adjust_type            = ''RE'')'||
     ' and NVL(sk.qty_expected,0)    != NVL(sk.qty_received,0)'||
     ' and sk.shipment                = sh.shipment'||
     ' and sh.status_code            != ''C''';
   ---
   if I_where_clause is not null then
      L_so_shipsku_temp_stmt := L_so_shipsku_temp_stmt||' and '||I_where_clause;
   end if;
   ---

   L_so_shipsku_temp_stmt := L_so_shipsku_temp_stmt||' group by sk.shipment, sk.carton';
   ---
   EXECUTE IMMEDIATE L_so_shipsku_temp_stmt;

   --- Convert totals to primary and local currencies for each shipsku record.

   L_previous_from_loc := -1;
   L_previous_to_loc   := -1;

   for rec in C_SHIPSKU loop

      L_total_cost_primary     := NULL;
      L_total_cost_local       := NULL;
      L_total_retail_primary   := NULL;
      L_total_retail_local     := NULL;

      --- Get from_loc currency
      if rec.from_loc != L_previous_from_loc then

         if rec.from_loc_type = 'I' then
            L_from_loc_type := 'W';
         else
            L_from_loc_type := rec.from_loc_type;
         end if;

         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                      rec.from_loc,
                                      L_from_loc_type,
                                      NULL,
                                      L_from_loc_currency)= FALSE then
            return FALSE;
         end if;
      end if;

      --- Get local currency (to_loc currency)
      if rec.to_loc != L_previous_to_loc then

         if rec.to_loc_type = 'I' then
            L_to_loc_type := 'W';
         else
            L_to_loc_type := rec.to_loc_type;
         end if;

         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                      rec.to_loc,
                                      L_to_loc_type,
                                      NULL,
                                      L_local_currency)= FALSE then
            return FALSE;
         end if;
      end if;

      --- Convert total_cost_from_loc to primary currency
      if CURRENCY_SQL.CONVERT(O_error_message,
                              rec.total_cost_from_loc,
                              L_from_loc_currency,
                              L_primary_currency,
                              L_total_cost_primary,
                              'C',
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;

      --- Convert total_cost_primary to local currency (to_loc currency)
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_total_cost_primary,
                              L_primary_currency,
                              L_local_currency,
                              L_total_cost_local,
                              'C',
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;

      --- Convert total_retail_from_loc to primary currency
      if CURRENCY_SQL.CONVERT(O_error_message,
                              rec.total_retail_from_loc,
                              L_from_loc_currency,
                              L_primary_currency,
                              L_total_retail_primary,
                              'R',
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;

      --- Convert total_retail_primary to local currency (to_loc currency)
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_total_retail_primary,
                              L_primary_currency,
                              L_local_currency,
                              L_total_retail_local,
                              'R',
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;

      update so_shipsku_temp
         set total_cost_primary   = L_total_cost_primary
           , total_cost_local     = L_total_cost_local
           , total_retail_primary = L_total_retail_primary
           , total_retail_local   = L_total_retail_local
       where rowid                = rec.rowid;

      L_previous_from_loc := rec.from_loc;
      L_previous_to_loc   := rec.to_loc;

   end loop;


   --- This cursor inserts into so_shipment_temp by selecting records from the
   --- so_shipsku_temp table, rolling them up to the shipment level,  and getting
   --- shipment info for them.
   ---
   --- It sums up the over/short quantities, cost, and retail values.
   ---
   --- If any one of the cartons is tampered then the tampered_ind
   --- is set to 'Y' ('N' otherwise).
   ---
   --- The cursor sets the shipment_over_short_ind to:
   ---     1 if all carton level records are overages
   ---    -1 if all carton level records are shortages
   ---     0 if both overage and shortage carton level records exist
   --- If the minimum carton_over_short_ind is greater than zero then we know the
   --- maximum is also, so all records are overages (1). If that is not true,
   --- then if the maximumn carton_over_short_ind value is less than zero we know that
   --- the minimum is also and all records are shortages (-1).  If neither
   --- of those statements are true then there are both overages and shortages (0).
   ---
   --- If any of the so_shipsku_temp records for this shipment has an associated
   --- carton, then the has_carton_ind is set to 'Y' ('N' otherwise).
   ---
   --- If any of the cartons for this shipment have line items that have not been
   --- received then the shipment_unreceived_ind is set to 'Y' ('N' otherwise)
   --- (shipment_unreceived_ind is not used in the form - this column can be dropped later).
   --- A shipment is unreceived only if all line items of the shipment have not been received.
   ---
   --- The wrong_store_ind is set to 'Y' if any line item of the shipment was received
   --- at wrong store. ('N' otherwise)
   ---
   insert into so_shipment_temp
        ( shipment
        , bol_no
        , to_loc
        , to_loc_type
        , from_loc
        , from_loc_type
        , ship_date
        , receive_date
        , over_short
        , total_cost_from_loc
        , total_cost_primary
        , total_cost_local
        , total_retail_from_loc
        , total_retail_primary
        , total_retail_local
        , carton_disp_ind
        , tampered_ind
        , shipment_over_short_ind
        , has_carton_ind
        , shipment_unreceived_ind
        , wrong_store_ind)
   select sh.shipment
        , sh.bol_no
        , sh.to_loc
        , sh.to_loc_type
        , sh.from_loc
        , sh.from_loc_type
        , sh.ship_date
        , sh.receive_date
        , SUM(NVL(sk.over_short, 0))
        , SUM(NVL(sk.total_cost_from_loc, 0))
        , SUM(NVL(sk.total_cost_primary, 0))
        , SUM(NVL(sk.total_cost_local, 0))
        , SUM(NVL(sk.total_retail_from_loc, 0))
        , SUM(NVL(sk.total_retail_primary, 0))
        , SUM(NVL(sk.total_retail_local, 0))
        , 'N'
        , MAX(NVL(sk.tampered_ind,'N'))
        , DECODE(SIGN(MIN(sk.carton_over_short_ind)),
                 1, 1,
                 DECODE(SIGN(MAX(sk.carton_over_short_ind)),
                        -1, -1,
                        0))
        , DECODE(SUM(DECODE(sk.carton, NULL, 0, 1)), 0, 'N', 'Y')
        , MAX(sk.carton_unreceived_ind)
        , MAX(sk.wrong_store_ind)
     from v_shipment        sh
        , so_shipsku_temp sk
    where sk.shipment = sh.shipment
    group by sh.shipment
           , sh.bol_no
           , sh.to_loc
           , sh.to_loc_type
           , sh.from_loc
           , sh.from_loc_type
           , sh.ship_date
           , sh.receive_date;
   ---
   -- if from_loc/to_loc is a internal finisher, set loc_type to 'I'
   update so_shipment_temp sst
      set sst.from_loc_type = 'I'
    where sst.from_loc_type = 'W'
      and exists (select 1
                    from v_internal_finisher vif
                   where vif.finisher_id = sst.from_loc);

   update so_shipment_temp sst
      set sst.to_loc_type = 'I'
    where sst.to_loc_type = 'W'
      and exists (select 1
                    from v_internal_finisher vif
                   where vif.finisher_id = sst.to_loc);
   ---
   --- Set local_currency and location descriptions

   L_previous_from_loc := -1;
   L_previous_to_loc   := -1;
   L_local_currency    := NULL;
   L_from_loc_desc     := NULL;
   L_to_loc_desc       := NULL;

   for rec in C_SHIPMENT loop

      --- Get from_loc description
      if rec.from_loc != L_previous_from_loc then
         if rec.from_loc_type = 'I' then
            L_from_loc_type := 'W';
         else
            L_from_loc_type := rec.from_loc_type;
         end if;

         if LOCATION_ATTRIB_SQL.GET_NAME(O_error_message,
                                         L_from_loc_desc,
                                         rec.from_loc,
                                         L_from_loc_type) = FALSE then
            return FALSE;
         end if;
      end if;

      --- Get to_loc description and local currency (to_loc currency)
      if rec.to_loc != L_previous_to_loc then

         if rec.to_loc_type = 'I' then
            L_to_loc_type := 'W';
         else
            L_to_loc_type := rec.to_loc_type;
         end if;

         if LOCATION_ATTRIB_SQL.GET_NAME(O_error_message,
                                         L_to_loc_desc,
                                         rec.to_loc,
                                         L_to_loc_type) = FALSE then
            return FALSE;
         end if;
         ---
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                      rec.to_loc,
                                      L_to_loc_type,
                                      NULL,
                                      L_local_currency)= FALSE then
            return FALSE;
         end if;
      end if;

      update so_shipment_temp
         set local_currency        = L_local_currency
           , from_loc_desc         = L_from_loc_desc
           , to_loc_desc           = L_to_loc_desc
       where shipment              = rec.shipment;

      L_previous_from_loc := rec.from_loc;
      L_previous_to_loc   := rec.to_loc;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_TEMP_TABLES;
--------------------------------------------------------------------------------
FUNCTION CLEAN_TEMP_TABLES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'STOCK_ORDER_RECONCILE_SQL.CLEAN_TEMP_TABLES';

BEGIN

   delete so_shipment_temp;
   delete so_shipsku_temp;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CLEAN_TEMP_TABLES;
--------------------------------------------------------------------------------
FUNCTION GET_CARTON_INDS(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_carton_disp_ind        IN OUT  SO_SHIPMENT_TEMP.CARTON_DISP_IND%TYPE,
                         I_shipment               IN      SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(61)   := 'STOCK_ORDER_RECONCILE_SQL.GET_CARTON_INDS';

   cursor C_CARTON_DISP_IND is
      select 'Y'
        from so_shipsku_temp
       where adjust_type is NOT NULL
         and shipment    = I_shipment;

BEGIN

   O_carton_disp_ind       := 'N';

   open  C_CARTON_DISP_IND;
   fetch C_CARTON_DISP_IND into O_carton_disp_ind;
   close C_CARTON_DISP_IND;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CARTON_INDS;
--------------------------------------------------------------------------------
FUNCTION APPLY_ADJUSTMENTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.APPLY_ADJUSTMENTS';
   L_carton_ind  VARCHAR2(1);
   L_tran_date   tran_data.tran_date%type := get_vdate;

   CURSOR C_SHIPMENT_CARTON_DETAILS IS
      select sk.shipment
           , sk.carton
           , sh.to_loc
           , sh.to_loc_type
           , sh.from_loc
           , sh.from_loc_type
           , sk.adjust_type
           , sh.ship_date
        from so_shipsku_temp  sk
           , so_shipment_temp sh
       where sk.shipment    = sh.shipment
         and sk.adjust_type is NOT NULL
       UNION ALL
      select sk.shipment
           , sk.carton
           , sh.to_loc
           , sh.to_loc_type
           , sh.from_loc
           , sh.from_loc_type
           , sh.adjust_type
           , sh.ship_date
        from so_shipsku_temp sk
           , so_shipment_temp sh
       where sh.adjust_type is NOT NULL
         and sk.shipment    = sh.shipment
         and sk.adjust_type is NULL;

BEGIN

   L_receipt_date := L_tran_date;
  
   for rec in C_SHIPMENT_CARTON_DETAILS loop
         L_ship_date:=NVL(rec.ship_date,L_tran_date);
      --- Note: No adjustment is made for adjustment
      --- type RE (Received Elsewhere)

      if rec.carton is NULL then
         L_carton_ind := NULL;
      else
         L_carton_ind := 'C';
      end if;

      if rec.adjust_type = 'FC' then
       if FREIGHT_CLAIM(O_error_message,
                          rec.shipment,
                          rec.carton,
                          L_carton_ind,
                          rec.from_loc_type,
                          rec.from_loc,
                          rec.to_loc_type,
                          rec.to_loc) = FALSE then
            return FALSE;
         end if;
      elsif rec.adjust_type = 'FR' then
        if FORCED_RECEIVE(O_error_message,
                           rec.shipment,
                           rec.carton,
                           L_carton_ind,
                           rec.from_loc_type,
                           rec.from_loc,
                           rec.to_loc_type,
                           rec.to_loc) = FALSE then
            return FALSE;
         end if;
      elsif rec.adjust_type = 'RL' then
         if TO_LOC_ADJUST(O_error_message,
                          rec.shipment,
                          rec.carton,
                          L_carton_ind,
                          rec.from_loc_type,
                          rec.from_loc,
                          rec.to_loc_type,
                          rec.to_loc) = FALSE then
            return FALSE;
         end if;
       elsif rec.adjust_type = 'SL' then
         if FROM_LOC_ADJUST(O_error_message,
                            rec.shipment,
                            rec.carton,
                            L_carton_ind,
                            rec.from_loc_type,
                            rec.from_loc,
                            rec.to_loc_type,
                            rec.to_loc) = FALSE then
            return FALSE;
        end if;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END APPLY_ADJUSTMENTS;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_BOL_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid          IN OUT  BOOLEAN,
                         I_bol_no         IN      SO_SHIPMENT_TEMP.BOL_NO%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.VALIDATE_BOL_NO';
   L_exists   VARCHAR2(1)  := 'N';

   cursor C_BOL_NO_EXISTS is
      select 'Y'
        from shipsku sk,
             v_shipment sh
       where NVL(sk.distro_type,'-*-') in ('T','A')
         and (sk.adjust_type           is NULL
             or adjust_type            = 'RE')
         and NVL(sk.qty_expected,0)    != NVL(sk.qty_received,0)
         and sk.shipment                = sh.shipment
         and sh.status_code            != 'C'
         and sh.bol_no = I_bol_no
         and rownum = 1;

BEGIN

   open  C_BOL_NO_EXISTS;
   fetch C_BOL_NO_EXISTS into L_exists;
   close C_BOL_NO_EXISTS;

   O_valid := (L_exists = 'Y');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_BOL_NO;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_DISTRO_NO(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid           IN OUT  BOOLEAN,
                            I_distro_no       IN      SHIPSKU.DISTRO_NO%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.VALIDATE_DISTRO_NO';
   L_exists   VARCHAR2(1)  := 'N';

   cursor C_DISTRO_NO_EXISTS is
      select 'Y'
        from shipsku sk,
             v_shipment sh
       where NVL(sk.distro_type,'-*-') in ('T','A')
         and (sk.adjust_type           is NULL
             or adjust_type            = 'RE')
         and NVL(sk.qty_expected,0)    != NVL(sk.qty_received,0)
         and sk.shipment                = sh.shipment
         and sh.status_code            != 'C'
         and sk.distro_no = I_distro_no
         and rownum = 1;

BEGIN

   open  C_DISTRO_NO_EXISTS;
   fetch C_DISTRO_NO_EXISTS into L_exists;
   close C_DISTRO_NO_EXISTS;

   O_valid := (L_exists = 'Y');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_DISTRO_NO;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_CARTON(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid          IN OUT  BOOLEAN,
                         I_carton         IN      SO_SHIPSKU_TEMP.CARTON%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.VALIDATE_CARTON';
   L_exists   VARCHAR2(1)  := 'N';

   cursor C_CARTON_EXISTS is
      select 'Y'
        from shipsku sk,
             v_shipment sh
       where NVL(sk.distro_type,'-*-') in ('T','A')
         and (sk.adjust_type           is NULL
              or adjust_type            = 'RE')
         and NVL(sk.qty_expected,0)    != NVL(sk.qty_received,0)
         and sk.shipment                = sh.shipment
         and sh.status_code            != 'C'
         and sk.carton = I_carton
         and rownum = 1;

BEGIN

   open  C_CARTON_EXISTS;
   fetch C_CARTON_EXISTS into L_exists;
   close C_CARTON_EXISTS;

   O_valid := (L_exists = 'Y');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_CARTON;
--------------------------------------------------------------------------------
FUNCTION LOCK_SHIPSKU(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_shipment       IN      SO_SHIPMENT_TEMP.SHIPMENT%TYPE,
                      I_carton         IN      SO_SHIPSKU_TEMP.CARTON%TYPE,
                      I_adjust_type    IN      SO_SHIPSKU_TEMP.ADJUST_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.LOCK_SHIPSKU';
   L_check_data       VARCHAR2(2);
   RECORD_UPDATED     EXCEPTION;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_lock_shipsku (cv_shipment    shipsku.shipment%type,
                          cv_carton      shipsku.carton%type) IS
      SELECT 'x'
        FROM shipsku
       WHERE shipment = cv_shipment
         AND NVL(carton, '_*_') = NVL(cv_carton, NVL(carton, '_*_'))
         AND qty_expected <> NVL(qty_received,0)
         AND (adjust_type is null or adjust_type = 'RE')
         FOR UPDATE NOWAIT;

BEGIN
   if I_adjust_type is not NULL then

      open C_lock_shipsku (I_shipment, I_carton);
      fetch C_lock_shipsku into L_check_data;
      if C_LOCK_SHIPSKU%NOTFOUND then
         close C_lock_shipsku;
         raise RECORD_UPDATED;
      end if;
      close C_lock_shipsku;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_UPDATED then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_CHG_ADJUST_TYPE',
                                             I_shipment,
                                             NVL(I_carton,'all'),
                                             NULL);
   return FALSE;
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('SHIPMENT_REC_LOCK',
                                             I_shipment,
                                             NULL,
                                             NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END LOCK_SHIPSKU;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ADJUST_TYPE(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_ajust_type_allowed  IN OUT  BOOLEAN,
                              I_shipment            IN      SO_SHIPMENT_TEMP.SHIPMENT%TYPE,
                              I_carton              IN      SO_SHIPSKU_TEMP.CARTON%TYPE,
                              I_adjust_type         IN      SO_SHIPSKU_TEMP.ADJUST_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(61)   := 'STOCK_ORDER_RECONCILE_SQL.VALIDATE_ADJUST_TYPE';
   L_distro_type              shipsku.distro_type%type;
   L_tsf_parent_no            tsfhead.tsf_parent_no%type;
   L_1st_leg_status           tsfhead.status%type;
   L_1st_leg_not_reconciled   VARCHAR2(1) := 'N';

   cursor C_get_distro_type is
      select distinct distro_type
        from shipsku
       where shipment = I_shipment;

   cursor C_tsf_parent_no is
      select distinct NVL(th.tsf_parent_no, -1)
        from shipment s,
             shipsku ss,
             tsfhead th
       where s.shipment = ss.shipment
         and ss.distro_type = 'T'
         and th.tsf_no = ss.distro_no
         and s.shipment = I_shipment
         and (I_carton is NULL
              or (I_carton is not null and ss.carton = I_carton));

   cursor C_1st_leg_status is
      select status
        from tsfhead
       where tsf_no = L_tsf_parent_no;

   cursor C_1st_leg_not_reconciled is
      select 'Y'
        from shipsku
       where distro_type = 'T'
         and distro_no = L_tsf_parent_no
         and qty_received != qty_expected
         and adjust_type is null;

BEGIN
   ---
   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ENTER_SHIP_NUM',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_get_distro_type;
   fetch C_get_distro_type into L_distro_type;
   close C_get_distro_type;
   ---
   if L_distro_type = 'A' then  -- if the shipment associated with an allocation, return TRUE;
      O_ajust_type_allowed := TRUE;
      return TRUE;
   end if;
   ---
   open C_tsf_parent_no;
   fetch C_tsf_parent_no into L_tsf_parent_no;
   close C_tsf_parent_no;
   ---
   if L_tsf_parent_no = -1 then  -- The associated transfer is a regular tsf or is the 1st-leg tsf
      ---
      O_ajust_type_allowed := TRUE;
      ---
   else  -- the associated transfer is the 2nd-leg tsf
      ---
      open C_1st_leg_status;
      fetch C_1st_leg_status into L_1st_leg_status;
      close C_1st_leg_status;
      ---
      if L_1st_leg_status = 'C' then
         O_ajust_type_allowed := TRUE;
      else   -- 1st-leg hasn't been closed
         ---
         -- check to see whether the 1st-leg has been reconciled
         open C_1st_leg_not_reconciled;
         fetch C_1st_leg_not_reconciled into L_1st_leg_not_reconciled;
         close C_1st_leg_not_reconciled;
         ---
         if L_1st_leg_not_reconciled = 'Y' then
            O_ajust_type_allowed := FALSE;
            O_error_message := SQL_LIB.CREATE_MSG('1ST_LEG_NOT_RECONCILED',
                                                  NULL,
                                                  NULL,
                                                  NULL);
         else
            O_ajust_type_allowed := TRUE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_ADJUST_TYPE;
--------------------------------------------------------------------------------
-- Function Name: UPDATE_FINISHER_RETAIL_UNITS
--       Purpose: This function will update the finisher_av_retail and finisher_units for the item/finisher record.

FUNCTION UPDATE_FINISHER_RETAIL_UNITS( O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item                IN      ITEM_LOC_SOH.ITEM%TYPE,
                                       I_loc                 IN      ITEM_LOC_SOH.LOC%TYPE,
                                       I_loc_type            IN      ITEM_LOC_SOH.LOC_TYPE%TYPE,
                                       I_tsf_no              IN      TSFHEAD.TSF_NO%TYPE,
                                       I_reconciled_qty      IN      TSFDETAIL.RECONCILED_QTY%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(61)   := 'STOCK_ORDER_RECONCILE_SQL.UPDATE_FINISHER_RETAIL_UNITS';
   L_old_retail            ITEM_LOC_SOH.FINISHER_AV_RETAIL%TYPE;
   L_new_retail            TSFDETAIL.FINISHER_AV_RETAIL%TYPE;
   L_old_qty               ITEM_LOC_SOH.FINISHER_UNITS%TYPE;
   L_new_qty               TSFDETAIL.FINISHER_UNITS%TYPE;
   L_location              ITEM_LOC_SOH.LOC%TYPE;
   L_finisher_loc_type     ITEM_LOC_SOH.LOC_TYPE%TYPE;
   L_rowid                 ROWID;
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   cursor C_OLD_RETAIL_LOC is
      select NVL(finisher_av_retail,0),
             NVL(finisher_units,0),
             rowid
        from item_loc_soh
       where item     = I_item
         and loc      = I_loc
         and loc_type = I_loc_type
         FOR UPDATE NOWAIT;

   cursor C_TSF_FIN_RETAIL_LOC is
      select NVL(finisher_av_retail,0)
        from tsfhead th,
             tsfdetail td
       where th.tsf_no = I_tsf_no
         and th.tsf_no = td.tsf_no
         and td.item = I_item
         and th.to_loc = I_loc
         and th.to_loc_type = I_loc_type;

BEGIN
   ---
   L_old_retail := 0;
   L_old_qty := 0;
   L_new_retail := 0;
   L_new_qty := 0;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_OLD_RETAIL_LOC',
                    'ITEM_LOC_SOH',
                    'Item: '||I_item||','||'I_loc: '||I_loc);
   open C_OLD_RETAIL_LOC;
   --
   SQL_LIB.SET_MARK('FETCH',
                    'C_OLD_RETAIL',
                    'ITEM_LOC_SOH',
                    'Item: '||I_item||','||'I_loc: '||I_loc);
   fetch C_OLD_RETAIL_LOC into L_old_retail,
                               L_old_qty,
                               L_rowid;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_OLD_RETAIL_LOC',
                    'ITEM_LOC_SOH',
                    'Item: '||I_item||','||'I_loc: '||I_loc);
   close C_OLD_RETAIL_LOC;
   --

   SQL_LIB.SET_MARK('OPEN',
                    'C_TSF_FIN_RETAIL_LOC',
                    'TSFDETAIL',
                    'Item: '||I_item||','||'I_loc: '||I_loc);
   open C_TSF_FIN_RETAIL_LOC;
   --
   SQL_LIB.SET_MARK('FETCH',
                    'C_TSF_FIN_RETAIL_LOC',
                    'TSFDETAIL',
                    'Item: '||I_item||','||'I_loc: '||I_loc);
   fetch C_TSF_FIN_RETAIL_LOC into L_new_retail;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_TSF_FIN_RETAIL_LOC',
                    'TSFDETAIL',
                    'Item: '||I_item||','||'I_loc: '||I_loc);
   close C_TSF_FIN_RETAIL_LOC;
   --

   L_new_qty   := L_old_qty + I_reconciled_qty;
   --
   if L_new_qty = 0 then
      L_new_retail := NULL;
      L_new_qty    := NULL;
   else
      L_new_retail := ((L_old_qty*L_old_retail) + (I_reconciled_qty*L_new_retail))/L_new_qty;
      if L_new_retail = 0 then
         L_new_retail := NULL;
      end if;
   end if;
   ---
   UPDATE item_loc_soh
      SET finisher_av_retail = L_new_retail,
          finisher_units = L_new_qty
    WHERE rowid = L_rowid;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'ITEM_LOC_SOH',
                                             I_item,
                                             I_loc);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_FINISHER_RETAIL_UNITS;
---------------------------------------------------------------------------------------------------
FUNCTION GET_TO_LOC_VWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_loc_vwh         IN OUT   SHIPMENT.TO_LOC%TYPE,
                        I_distro_no       IN       SHIPSKU.DISTRO_NO%TYPE,
                        I_loc_type        IN       SHIPMENT.TO_LOC_TYPE%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.GET_TO_LOC_VWH';

   cursor C_vwh is
      select to_loc
        from shipitem_inv_flow
       where to_loc_type = I_loc_type
         and tsf_no      = I_distro_no
         and ROWNUM      = 1;

BEGIN
   O_loc_vwh := NULL;

   if I_distro_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_distro_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_loc_type = 'W' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_vwh',
                       'shipitem_inv_flow',
                       NULL);
      open C_vwh;
      SQL_LIB.SET_MARK('FETCH',
                       'C_vwh',
                       'shipitem_inv_flow',
                       NULL);
      fetch C_vwh into O_loc_vwh;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_vwh',
                       'shipitem_inv_flow',
                       NULL);
      close C_vwh;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_TO_LOC_VWH;
------------------------------------------------------------------------------
FUNCTION GET_FROM_LOC_VWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_loc_vwh         IN OUT   SHIPMENT.TO_LOC%TYPE,
                          I_distro_no       IN       SHIPSKU.DISTRO_NO%TYPE,
                          I_loc_type        IN       SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.GET_FROM_LOC_VWH';

   cursor C_from_vwh is
      select from_loc
        from shipitem_inv_flow
       where from_loc_type = I_loc_type
         and tsf_no      = I_distro_no
         and ROWNUM      = 1;

BEGIN
   O_loc_vwh := NULL;

   if I_distro_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_distro_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_loc_type = 'W' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_from_vwh',
                       'shipitem_inv_flow',
                       NULL);
      open C_from_vwh;
      SQL_LIB.SET_MARK('FETCH',
                       'C_from_vwh',
                       'shipitem_inv_flow',
                       NULL);
      fetch C_from_vwh into O_loc_vwh;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_from_vwh',
                       'shipitem_inv_flow',
                       NULL);
      close C_from_vwh;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_FROM_LOC_VWH;
-------------------------------------------------------------------------------------------
FUNCTION WRITE_INVENTORY_ADJUSTMENT(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_shipment           IN     SHIPMENT.SHIPMENT%TYPE,
                                    I_tsf_alloc_no       IN     SHIPSKU.DISTRO_NO%TYPE,
                                    I_loc                IN     SHIPMENT.FROM_LOC%TYPE,
                                    I_loc_type           IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                                    I_item               IN     SHIPSKU.ITEM%TYPE,
                                    I_qty                IN     TSFDETAIL.RECONCILED_QTY%TYPE,
                                    I_tran_date          IN     TRAN_DATA.TRAN_DATE%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.WRITE_INVENTORY_ADJUSTMENT';

   L_item_record            ITEM_MASTER%ROWTYPE;

   L_av_cost                ITEM_LOC_SOH.AV_COST%TYPE;
   L_unit_retail            ITEM_LOC.UNIT_RETAIL%TYPE;
   L_unit_cost              ITEM_LOC_SOH.AV_COST%TYPE;         --dummy
   L_selling_unit_retail    ITEM_LOC.SELLING_UNIT_RETAIL%TYPE; --dummy
   L_selling_uom            ITEM_LOC.SELLING_UOM%TYPE;         --dummy

   L_total_cost             TRAN_DATA.TOTAL_COST%TYPE;
   L_total_retail           TRAN_DATA.TOTAL_RETAIL%TYPE;
   L_tran_code              TRAN_DATA.TRAN_CODE%TYPE;

BEGIN

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                       L_item_record,
                                       I_item) = FALSE then
      return FALSE;
   end if;

   if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                               I_item,
                                               I_loc,
                                               I_loc_type,
                                               L_av_cost,
                                               L_unit_cost,
                                               L_unit_retail,
                                               L_selling_unit_retail,
                                               L_selling_uom) = FALSE then
      return FALSE;
   end if;
   ---
   L_total_cost := L_av_cost * I_qty;
   L_total_retail := L_unit_retail * I_qty;
   ---
   -- write tran_data 22
   L_tran_code := 22;
   if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                          I_item,
                                          L_item_record.dept,
                                          L_item_record.class,
                                          L_item_record.subclass,
                                          I_loc,
                                          I_loc_type,
                                          I_tran_date,
                                          L_tran_code,
                                          NULL,                 -- adj_code
                                          I_qty,                -- units
                                          L_total_cost,         -- total_cost
                                          L_total_retail,
                                          I_tsf_alloc_no,       -- ref_no_1
                                          I_shipment,           -- ref_no_2
                                          NULL,
                                          NULL,
                                          NULL,                 -- old_unit_retail
                                          NULL,                 -- new_unit_retail
                                          NULL,                 -- source_dept
                                          NULL,                 -- source_class
                                          NULL,                 -- source_subclass
                                          L_program,           -- pgm_name,
                                          NULL) = FALSE then    -- gl_ref_no
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END WRITE_INVENTORY_ADJUSTMENT;
-------------------------------------------------------------------------------------------
FUNCTION ADJUST_SHORTAGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                         I_carton           IN     SHIPSKU.CARTON%TYPE,
                         I_tsf_alloc_type   IN     SHIPSKU.DISTRO_TYPE%TYPE,
                         I_tsf_alloc_no     IN     SHIPSKU.DISTRO_NO%TYPE,
                         I_tsf_seq_no       IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                         I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                         I_from_loc         IN     SHIPMENT.FROM_LOC%TYPE,
                         I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                         I_to_loc           IN     SHIPMENT.TO_LOC%TYPE,
                         I_item             IN     SHIPSKU.ITEM%TYPE,
                         I_shortage_qty     IN     TSFDETAIL.RECONCILED_QTY%TYPE,
                         I_adjust_type      IN     SHIPSKU.ADJUST_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.ADJUST_SHORTAGE';

   L_item_record              ITEM_MASTER%ROWTYPE;

   L_action_type              VARCHAR2(1);
   L_tsf_type                 TSFHEAD.TSF_TYPE%TYPE := NULL;

   L_from_loc_vwh             SHIPMENT.TO_LOC%TYPE := NULL;
   L_to_loc_vwh               SHIPMENT.TO_LOC%TYPE := NULL;
   L_from_finisher_ind        wh.finisher_ind%TYPE := 'N';
   L_to_finisher_ind          wh.finisher_ind%TYPE := 'N';
   L_intercompany             BOOLEAN;

   L_frm_non_stkhldg_f_store  STORE.STOCKHOLDING_IND%TYPE := 'N';
   L_store_row                STORE%ROWTYPE;
   L_exists                   BOOLEAN;

   L_total_chrgs_prim         ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE;
   L_profit_chrgs_to_loc      NUMBER;
   L_exp_chrgs_to_loc         NUMBER;
   L_pack_total_chrgs_prim    ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_pack_profit_chrgs_to_loc NUMBER                    := 0;
   L_pack_exp_chrgs_to_loc    NUMBER                    := 0;
   L_charge_to_loc            ITEM_LOC_SOH.AV_COST%TYPE;
   L_charge_from_loc          ITEM_LOC_SOH.AV_COST%TYPE;
   L_shipment_cost            ITEM_LOC_SOH.AV_COST%TYPE;
   L_inv_status               SHIPSKU.INV_STATUS%TYPE;
   L_tsf_cost                 TSFDETAIL.TSF_COST%TYPE := NULL;

   L_l10n_fin_rec             "L10N_FIN_REC" := L10N_FIN_REC();
   L_upd_to_av_cost           ITEM_LOC_SOH.AV_COST%TYPE;
   L_upd_from_av_cost         ITEM_LOC_SOH.AV_COST%TYPE;

   L_pack_no                  ITEM_MASTER.ITEM%TYPE := NULL;
   L_pct_in_pack              NUMBER;
   L_receive_as_type          ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
   L_sign                     NUMBER;

   L_tran_date                DATE := get_vdate;
   L_dummy_cost               TRAN_DATA.TOTAL_COST%TYPE;
   L_dummy_retail             TRAN_DATA.TOTAL_RETAIL%TYPE;

   L_wf_order_no              WF_ORDER_HEAD.WF_ORDER_NO%TYPE;
   L_rma_no                   WF_RETURN_HEAD.RMA_NO%TYPE;

   L_rowid                    ROWID;
   L_table                    VARCHAR2(30) := 'ITEM_LOC_SOH';
   L_key1                     VARCHAR2(100);
   L_key2                     VARCHAR2(100);
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_comp_item IS
      SELECT v.item,
             v.qty,
             im.dept,
             im.class,
             im.subclass
        FROM item_master im,
             v_packsku_qty v
       WHERE im.item = v.item
         AND v.pack_no = I_item
         AND im.inventory_ind = 'Y';

   CURSOR C_lock_ils(cv_item    ITEM_LOC.ITEM%TYPE,
                     cv_loc     ITEM_LOC.LOC%TYPE) IS
      SELECT rowid
        FROM item_loc_soh
       WHERE item = cv_item
         AND loc = cv_loc
         FOR UPDATE NOWAIT;

   cursor C_GET_SHIPSKU_COST is
      select unit_cost,
             NVL(inv_status, -1) inv_status
        from shipsku
       where shipment = I_shipment
         and item      = I_item
         and distro_no = I_tsf_alloc_no
         and distro_type = I_tsf_alloc_type
         and NVL(carton, '-*-') = NVL(I_carton, NVL(carton, '-*-'))
         and rownum = 1
       order by inv_status;

   cursor C_GET_TSF_COST is
      select tsf_cost
        from tsfdetail
       where tsf_no       = I_tsf_alloc_no
         and item         = I_item;
BEGIN
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                       L_item_record,
                                       I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                L_action_type,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_to_loc,
                                                I_to_loc_type) = FALSE then
       return FALSE;
   end if;

   open C_GET_SHIPSKU_COST;
   fetch C_GET_SHIPSKU_COST into L_shipment_cost, L_inv_status;
   if C_GET_SHIPSKU_COST%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_UNIT_COST',I_item, I_tsf_alloc_no,NULL);
      close C_GET_SHIPSKU_COST;
      return false;
   end if;
   close C_GET_SHIPSKU_COST;
   ---
   --Item_loc_soh should not be updated for non-stockholding franchise stores.
   --It is only possible for returns from non-stockholding franchise stores to
   --be reconciled. Hence, retrieve from-loc's store_type and stockholding indicator.
   L_frm_non_stkhldg_f_store := 'N';
   if I_from_loc_type = 'S' then
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exists,     --dummy
                                  L_store_row,
                                  I_from_loc) = FALSE then
         return FALSE;
      end if;
      if L_store_row.store_type = 'F' and L_store_row.stockholding_ind = 'N' then
         L_frm_non_stkhldg_f_store := 'Y';
      end if;
   end if;
   ---
   if I_tsf_alloc_type = 'T' then
      ---
      if TRANSFER_SQL.GET_TSF_TYPE(O_error_message,
                                   L_tsf_type,
                                   I_tsf_alloc_no)= FALSE then
         return FALSE;
      end if;
      ---
      if L_tsf_type = 'EG' or (L_tsf_type = 'CO' and LP_system_options_row.oms_ind = 'Y') then
         if I_to_loc_type = 'W' then
            if GET_TO_LOC_VWH(O_error_message,
                              L_to_loc_vwh,
                              I_tsf_alloc_no,
                              I_to_loc_type)= FALSE then
               return FALSE;
            end if;
         end if;
         if I_from_loc_type = 'W' THEN
            if GET_FROM_LOC_VWH(O_error_message,
                                L_from_loc_vwh,
                                I_tsf_alloc_no,
                                I_from_loc_type)= FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      if TRANSFER_SQL.GET_TSF_ATTRIB(O_error_message,
                                     L_from_finisher_ind,
                                     L_to_finisher_ind,
                                     L_intercompany,
                                     I_tsf_alloc_type,
                                     I_tsf_alloc_no,
                                     NVL(L_from_loc_vwh,I_from_loc),
                                     I_from_loc_type,
                                     NVL(L_to_loc_vwh,I_to_loc),
                                     I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
   elsif I_tsf_alloc_type = 'A' then
      if TRANSFER_SQL.IS_INTERCOMPANY(O_error_message,
                                      L_intercompany,
                                      I_tsf_alloc_type,
                                      NULL, -- L_tsf_type
                                      NVL(L_from_loc_vwh,I_from_loc),
                                      I_from_loc_type,
                                      NVL(L_to_loc_vwh, I_to_loc),
                                      I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   L_l10n_fin_rec.procedure_key  :='UPDATE_AV_COST';
   L_l10n_fin_rec.country_id     := NULL;
   L_l10n_fin_rec.source_entity  := 'LOC';
   L_l10n_fin_rec.source_id      := NVL(L_from_loc_vwh, I_from_loc);
   L_l10n_fin_rec.source_type    := I_from_loc_type;
   L_l10n_fin_rec.item           := I_item;
   L_l10n_fin_rec.dest_entity    := 'LOC';
   L_l10n_fin_rec.dest_id        := NVL(L_to_loc_vwh, I_to_loc);
   L_l10n_fin_rec.dest_type      := I_to_loc_type;
   ---
   if L_item_record.pack_ind = 'N' then   -- Single item
      ---
      -- Calculate total chrg in primary currency
      ---
      if I_tsf_alloc_type = 'A' then   -- shipment for allocation
         if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                        L_total_chrgs_prim,
                                                        L_profit_chrgs_to_loc,
                                                        L_exp_chrgs_to_loc,
                                                        'A',
                                                        I_tsf_alloc_no,
                                                        NULL,          -- tsf_seq_no
                                                        NULL,          -- shipment
                                                        NULL,          -- ship_seq_no
                                                        I_item,
                                                        NULL,          -- pack_item
                                                        I_from_loc,
                                                        I_from_loc_type,
                                                        I_to_loc,
                                                        I_to_loc_type) = FALSE then
            return FALSE;
         end if;
      else  -- shipment for transfer
         if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                        L_total_chrgs_prim,
                                                        L_profit_chrgs_to_loc,
                                                        L_exp_chrgs_to_loc,
                                                        'T',
                                                        I_tsf_alloc_no,
                                                        I_tsf_seq_no,
                                                        null,          -- shipment, for non 'EG' tsf_type
                                                        null,
                                                        I_item,
                                                        NULL,          -- pack_item
                                                        I_from_loc,
                                                        I_from_loc_type,
                                                        I_to_loc,
                                                        I_to_loc_type) = FALSE then
            return FALSE;
         end if;
      end if;

      ---WAC recalculation:
      -- Inventory Adjustment is always done at item-loc's current WAC. As such, WAC does NOT need to be
      -- recalculated for 'NL'/'RL' adjustments.
      --
      -- For 'BL'/'SL' adjustment, a reverse transaction of 30/32, 37/38, 20/82, 24/83 is written based
      -- on the shipment cost (on shipsku.unit_cost), which can be different from source location's current
      -- WAC. As such, WAC needs to be recalculated for both source location and destination location.
      --
      -- WAC is NOT recalculated at the destination location for franchise returns - RECALC_WAC handles that.
      -- WAC is not recalculated at the source location if the source location is a non-stockholding franchise store.
      --
      if I_adjust_type in ('BL', 'SL') then
         -- Convert total chrg from primary currency to to_loc's currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             L_total_chrgs_prim,
                                             L_charge_to_loc,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
         --Recalc destination location's wac based on the following (determined in RECALC_WAC):
         --1) transfer cost or from-loc's wac (intra-company)
         --2) pricing cost (intercompany)
         --3) customer sale price (franchise order)
         if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                         L_upd_to_av_cost,
                                         I_tsf_alloc_no,
                                         I_tsf_alloc_type,
                                         I_item,
                                         NULL,
                                         NULL,
                                         NVL(L_from_loc_vwh,I_from_loc),
                                         I_from_loc_type,
                                         NVL(L_to_loc_vwh,I_to_loc),
                                         I_to_loc_type,
                                         -I_shortage_qty,    --always a negative adjustment for the to-loc
                                         L_charge_to_loc,
                                         L_intercompany,     --intercompany
                                         'T') = FALSE then   --recalc to-loc's WAC
            return FALSE;
         end if;

         --Recalc source location's wac based on the shipment cost (shipsku.unit_cost, determined in RECALC_WAC).
         --Do NOT recalc wac if transferring from a non-stockholding franchise store.
         if L_frm_non_stkhldg_f_store = 'N' then
            -- Convert total chrg from primary currency to from_loc's currency
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                NULL,
                                                NULL,
                                                NULL,
                                                I_from_loc,
                                                I_from_loc_type,
                                                NULL,
                                                L_total_chrgs_prim,
                                                L_charge_from_loc,
                                                'C',
                                                NULL,
                                                NULL) = FALSE then
               return FALSE;
            end if;
            ---
            open C_GET_TSF_COST;
            fetch C_GET_TSF_COST into L_tsf_cost;
            close C_GET_TSF_COST; 
            --recalc source location's wac based on shipment cost (determined in RECALC_WAC):
            if L_tsf_cost is null then            
            if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                            L_upd_from_av_cost,
                                            I_tsf_alloc_no,
                                            I_tsf_alloc_type,
                                            I_item,
                                            NULL,
                                            NULL,
                                            NVL(L_from_loc_vwh,I_from_loc),
                                            I_from_loc_type,
                                            NVL(L_to_loc_vwh, I_to_loc),
                                            I_to_loc_type,
                                            I_shortage_qty,    --always positive adjustment at the from-loc for adjust_type of NL/BL
                                            L_charge_from_loc,
                                            L_intercompany,
                                            'F',
                                            I_shipment,
                                            I_carton) = FALSE then  --recalc from-loc's wac
               return FALSE;
               end if;
            end if;
         end if;
      end if;  --adjust_type 'BL'/'SL' for wac recalc

      ---
      -- Inventory and WAC updates:
      -- 1) Decrement to-loc's in-transit for all adjustment types (NL/BL/SL/RL)
      -- 2) Increment from-loc's stock on hand for adjustment types NL/BL
      -- 3) Update from-loc/to-loc's av_cost for adjustment types BL/SL
      ---
      -- To_loc updates:
      L_key1 := I_item;
      L_key2 := NVL(L_to_loc_vwh,I_to_loc);
      ---
      open C_lock_ils(I_item,
                      NVL(L_to_loc_vwh,I_to_loc));
      fetch C_lock_ils into L_rowid;
      close C_lock_ils;
      ---
      UPDATE item_loc_soh
         SET in_transit_qty = in_transit_qty - I_shortage_qty,
             av_cost = DECODE(I_adjust_type, 'BL', ROUND(L_upd_to_av_cost, 4),
                                             'SL', ROUND(L_upd_to_av_cost, 4), av_cost)
       WHERE rowid = L_rowid;
      ---
      if I_adjust_type in ('BL', 'SL') then
         L_l10n_fin_rec.av_cost := ROUND(L_upd_to_av_cost, 4);
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if UPDATE_STAKE_SKU_LOC(O_error_message,
                              L_key1,
                              I_to_loc_type,
                              NVL(L_to_loc_vwh,I_to_loc),
                              L_tran_date,
                              0,
                              I_shortage_qty*-1)= FALSE then
         return FALSE;
      end if;

      -- From-loc updates:
      if L_frm_non_stkhldg_f_store = 'N' then
         ---
         L_key1 := I_item;
         L_key2 := NVL(L_from_loc_vwh,I_from_loc);
         ---
         open C_lock_ils(I_item,
                         NVL(L_from_loc_vwh,I_from_loc));
         fetch C_lock_ils into L_rowid;
         close C_lock_ils;

         UPDATE item_loc_soh
            SET stock_on_hand = NVL(stock_on_hand,0) + DECODE(I_adjust_type, 'NL', I_shortage_qty,
                                                                             'BL', I_shortage_qty, 0),
                av_cost = DECODE(I_adjust_type, 'BL', DECODE(L_tsf_cost,NULL,ROUND(L_upd_from_av_cost, 4),av_cost),
                                                'SL', DECODE(L_tsf_cost,NULL,ROUND(L_upd_from_av_cost, 4),av_cost), av_cost)
          WHERE rowid = L_rowid;
         ---
         if I_adjust_type in ('NL', 'BL') then
            if UPDATE_STAKE_SKU_LOC(O_error_message,
                                    I_item,
                                    I_from_loc_type,
                                    NVL(L_from_loc_vwh,I_from_loc),
                                    L_tran_date,
                                    I_shortage_qty,
                                    0)= FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      -- write a reverse transation for 'SL','BL' adjustment
      if I_adjust_type in ('BL', 'SL') then
         ---
         if L_action_type IN ('O', 'R') then
            ---
            -- call STKLEDGR_SQL.WF_WRITE_FINANCIALS to write franchise order (20/82), franchise return (24/83) tran_codes
            ---
            if STKLEDGR_SQL.WF_WRITE_FINANCIALS(O_error_message,
                                                L_dummy_cost,
                                                L_dummy_retail,
                                                I_tsf_alloc_no,
                                                I_tsf_alloc_type,
                                                NVL(L_receipt_date,L_tran_date),
                                                I_item,
                                                NULL,         -- pack no
                                                NULL,         -- pct_in_pack
                                                L_item_record.dept,
                                                L_item_record.class,
                                                L_item_record.subclass,
                                                -I_shortage_qty,
                                                NVL(L_from_loc_vwh, I_from_loc),
                                                I_from_loc_type,
                                                NVL(L_to_loc_vwh, I_to_loc),
                                                I_to_loc_type,
                                                NULL,               -- Weight UOM
                                                NULL,               -- WF_order or RMA No
                                                I_shipment,
                                                I_carton) = FALSE then
               return FALSE;
            end if;
         else
            ---
            -- call STKLEDGR_SQL.WRITE_FINANCIALS to write profit up-charge(tran code 28), expense up-charge(tran code 29),
            -- transfer-in/out (tran code 30/32) or intercompany in/out (tran code 37/38)
            ---
            -- Reversals for Tran_data should post at (shipment cost - upcharges) as initial postings for 32/38 were at this cost
            L_shipment_cost := L_shipment_cost - NVL(L_charge_from_loc,0);
            if STKLEDGR_SQL.WRITE_FINANCIALS(O_error_message,
                                             L_dummy_cost,
                                             NULL,              -- call type
                                             I_shipment,
                                             I_tsf_alloc_no,
                                             NVL(L_receipt_date,L_tran_date),
                                             I_item,
                                             NULL,      -- pack no
                                             NULL,      -- pct_in_pack
                                             L_item_record.dept,
                                             L_item_record.class,
                                             L_item_record.subclass,
                                             -I_shortage_qty,
                                             NULL,                                -- weight_cuom
                                             NVL(L_from_loc_vwh,I_from_loc),
                                             I_from_loc_type,
                                             L_from_finisher_ind,
                                             NVL(L_to_loc_vwh,I_to_loc),
                                             I_to_loc_type,
                                             L_to_finisher_ind,
                                             L_shipment_cost,                     -- Unit Cost at which reversal are posted
                                             L_profit_chrgs_to_loc,
                                             L_exp_chrgs_to_loc,
                                             L_intercompany,
                                             NULL,
                                             NULL,
                                             L_ship_date) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;  -- I_adjust_type in ('BL', 'SL')
      ---
      -- write inventory adjustment (TRAN_CODE 22).
      -- Inventory Adjustment is always evaluated at the item-loc's current wac and current retail.
      if I_adjust_type in ('NL', 'RL', 'SL') then
         --write tran_data 22 for 'NL', 'RL', 'SL' adjustments
         --1) 'NL' adjustment: positive adjustment at from-loc, negative adjustment at to-loc
         --2) 'SL' adjustment: negative adjustment at from-loc
         --3) 'RL' adjustment: negative adjustment at to-loc

         --write tran_data 22 for To-Loc:
         if I_adjust_type in ('NL', 'RL') then
            if WRITE_INVENTORY_ADJUSTMENT(O_error_message,
                                          I_shipment,
                                          I_tsf_alloc_no,
                                          NVL(L_to_loc_vwh, I_to_loc),
                                          I_to_loc_type,
                                          I_item,
                                          -I_shortage_qty,    --always negative adjustment for the to-loc
                                          NVL(L_receipt_date,L_tran_date)) = FALSE then
               return FALSE;
            end if;
         end if;  --adjust_type NL/RL

         --write tran_data 22 from From-Loc:
         if L_frm_non_stkhldg_f_store = 'N' and I_adjust_type in ('NL', 'SL') then

            --positive adjustment for 'NL', negative adjustment for 'SL'
            if I_adjust_type = 'NL' then
               L_sign := 1;
            else --'SL'
               L_sign := -1;
            end if;

            if WRITE_INVENTORY_ADJUSTMENT(O_error_message,
                                          I_shipment,
                                          I_tsf_alloc_no,
                                          NVL(L_from_loc_vwh, I_from_loc),
                                          I_from_loc_type,
                                          I_item,
                                          L_sign*I_shortage_qty,
                                          NVL(L_receipt_date,L_tran_date)) = FALSE then
               return FALSE;
            end if;
            ---
         end if;
         ---
      end if;
      ---
   else   -- pack item
      ---
      L_pack_no := I_item;
      ---
      if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                L_receive_as_type,
                                                I_item,
                                                NVL(L_to_loc_vwh,I_to_loc)) = FALSE then
         return FALSE;
      end if;
      ---
      L_receive_as_type := NVL(L_receive_as_type, 'E');
      ---
      if L_item_record.pack_type != 'B' then  -- Vendor pack
         ---
         -- Calculate pack chrg in primary currency
         ---
         if I_tsf_alloc_type = 'A' then   -- shipment for allocation
            if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                           L_pack_total_chrgs_prim,
                                                           L_pack_profit_chrgs_to_loc,
                                                           L_pack_exp_chrgs_to_loc,
                                                           'A',
                                                           I_tsf_alloc_no,
                                                           NULL,          -- tsf_seq_no
                                                           NULL,          -- shipment
                                                           NULL,          -- ship_seq_no
                                                           I_item,
                                                           NULL,          -- pack_item
                                                           I_from_loc,
                                                           I_from_loc_type,
                                                           I_to_loc,
                                                           I_to_loc_type) = FALSE then
               return FALSE;
            end if;
         else  -- shipment for transfer
            if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                           L_pack_total_chrgs_prim,
                                                           L_pack_profit_chrgs_to_loc,
                                                           L_pack_exp_chrgs_to_loc,
                                                           'T',
                                                           I_tsf_alloc_no,
                                                           I_tsf_seq_no,
                                                           null,          -- shipment, for non 'EG' tsf_type
                                                           null,
                                                           I_item,
                                                           NULL,          -- pack_item
                                                           I_from_loc,
                                                           I_from_loc_type,
                                                           I_to_loc,
                                                           I_to_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      end if; -- end of Vendor pack
      ---
      FOR REC_comp_item IN C_comp_item LOOP
         ---
         --- In Case of frachise return use destination location WAC for prorating pack cost to
         --- component items
         if L_action_type = 'R' then
            if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                             L_pct_in_pack,
                                             L_pack_no,
                                             REC_comp_item.item,
                                             NVL(L_to_loc_vwh,I_to_loc)) = FALSE then
               return FALSE;
            end if;
         else
            if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                             L_pct_in_pack,
                                             L_pack_no,
                                             REC_comp_item.item,
                                             NVL(L_from_loc_vwh,I_from_loc)) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_item_record.pack_type != 'B' then  -- Vendor pack
            L_total_chrgs_prim := L_pack_total_chrgs_prim * L_pct_in_pack;
            L_profit_chrgs_to_loc := L_pack_profit_chrgs_to_loc * L_pct_in_pack;
            L_exp_chrgs_to_loc    := L_pack_exp_chrgs_to_loc    * L_pct_in_pack;
         else  -- Buyer pack
            --- look up chrgs at comp level
            if I_tsf_alloc_type = 'A' then   -- shipment for allocation
               if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                              L_total_chrgs_prim,
                                                              L_profit_chrgs_to_loc,
                                                              L_exp_chrgs_to_loc,
                                                              'A',
                                                              I_tsf_alloc_no,
                                                              NULL,          -- tsf_seq_no
                                                              NULL,          -- shipment
                                                              NULL,          -- ship_seq_no
                                                              REC_comp_item.item,
                                                              L_pack_no,          -- pack_item
                                                              I_from_loc,
                                                              I_from_loc_type,
                                                              I_to_loc,
                                                              I_to_loc_type) = FALSE then
                  return FALSE;
               end if;
            else  -- shipment for transfer
               if UP_CHARGE_SQL.CALC_TSF_ALLOC_ITEM_LOC_CHRGS(O_error_message,
                                                              L_total_chrgs_prim,
                                                              L_profit_chrgs_to_loc,
                                                              L_exp_chrgs_to_loc,
                                                              'T',
                                                              I_tsf_alloc_no,
                                                              I_tsf_seq_no,
                                                              null,          -- shipment, for non 'EG' tsf_type
                                                              null,
                                                              REC_comp_item.item,
                                                              L_pack_no,          -- pack_item
                                                              I_from_loc,
                                                              I_from_loc_type,
                                                              I_to_loc,
                                                              I_to_loc_type) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
         end if;
         ---

         -- WAC recalculation (same rule as non-pack):
         if I_adjust_type in ('BL', 'SL') then
            -- Convert total chrg from primary currency to to_loc's currency
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                NULL,
                                                NULL,
                                                NULL,
                                                I_to_loc,
                                                I_to_loc_type,
                                                NULL,
                                                L_total_chrgs_prim,
                                                L_charge_to_loc,
                                                'C',
                                                NULL,
                                                NULL) = FALSE then
               return FALSE;
            end if;
            ---
            if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                            L_upd_to_av_cost,
                                            I_tsf_alloc_no,
                                            I_tsf_alloc_type,
                                            REC_comp_item.item,
                                            L_pack_no,
                                            L_pct_in_pack,
                                            NVL(L_from_loc_vwh,I_from_loc),
                                            I_from_loc_type,
                                            NVL(L_to_loc_vwh,I_to_loc),
                                            I_to_loc_type,
                                            (-1) * REC_comp_item.qty * I_shortage_qty,
                                            L_charge_to_loc,
                                            L_intercompany,
                                            'T') = FALSE then  --recalc wac for to-loc
               return FALSE;
            end if;
            ---
            if L_frm_non_stkhldg_f_store = 'N' then
               -- Convert total chrg from primary currency to from_loc's currency
               if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   I_from_loc,
                                                   I_from_loc_type,
                                                   NULL,
                                                   L_total_chrgs_prim,
                                                   L_charge_from_loc,
                                                   'C',
                                                   NULL,
                                                   NULL) = FALSE then
                  return FALSE;
               end if;
               ---
               open C_GET_TSF_COST;
               fetch C_GET_TSF_COST into L_tsf_cost;
               close C_GET_TSF_COST;          
               if L_tsf_cost is null then                   
               if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                               L_upd_from_av_cost,
                                               I_tsf_alloc_no,
                                               I_tsf_alloc_type,
                                               REC_comp_item.item,
                                               L_pack_no,
                                               L_pct_in_pack,
                                               NVL(L_from_loc_vwh,I_from_loc),
                                               I_from_loc_type,
                                               NVL(L_to_loc_vwh, I_to_loc),
                                               I_to_loc_type,
                                               REC_comp_item.qty * I_shortage_qty,
                                               L_charge_from_loc,
                                               L_intercompany,
                                               'F',
                                               I_shipment,
                                               I_carton) = FALSE then
                  return FALSE;
               end if;
               end if;
            end if; --frm_non_stkhldg_f_store
         end if;  --adjust_type 'BL'/'SL' for wac recalc

         -- Inventory and WAC updates (same rule as non-pack):
         -- To-Loc updates:
         L_key1 := REC_comp_item.item;
         L_key2 := NVL(L_to_loc_vwh,I_to_loc);
         ---
         open C_lock_ils(REC_comp_item.item,
                         NVL(L_to_loc_vwh,I_to_loc));
         fetch C_lock_ils into L_rowid;
         close C_lock_ils;
         ---
         if I_to_loc_type = 'S' then

            UPDATE item_loc_soh
               SET in_transit_qty = (NVL(in_transit_qty,0)-(REC_comp_item.qty * I_shortage_qty)),
                   av_cost = DECODE(I_adjust_type, 'BL', ROUND(L_upd_to_av_cost, 4),
                                                   'SL', ROUND(L_upd_to_av_cost, 4), av_cost)
             WHERE rowid = L_rowid;
               ---
            if UPDATE_STAKE_SKU_LOC(O_error_message,
                                    L_key1,
                                    I_to_loc_type,
                                    NVL(L_to_loc_vwh,I_to_loc),
                                    L_tran_date,
                                    0,
                                    REC_comp_item.qty*I_shortage_qty*-1)= FALSE then
               return FALSE;
            end if;
            ---
            if I_adjust_type in ('BL', 'SL') then
               L_l10n_fin_rec.item := REC_comp_item.item;
               L_l10n_fin_rec.av_cost := ROUND(L_upd_to_av_cost, 4);
               if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                         L_l10n_fin_rec) = FALSE then
                  return FALSE;
               end if;
            end if;
         elsif I_to_loc_type = 'W' then

            UPDATE item_loc_soh
               SET in_transit_qty = DECODE(L_receive_as_type,
                                           'P', in_transit_qty,
                                           in_transit_qty - REC_comp_item.qty * I_shortage_qty),
                   pack_comp_intran = DECODE(L_receive_as_type,
                                             'P', pack_comp_intran - REC_comp_item.qty * I_shortage_qty,
                                             pack_comp_intran),
                   av_cost = DECODE(I_adjust_type, 'BL', ROUND(L_upd_to_av_cost, 4),
                                                   'SL', ROUND(L_upd_to_av_cost, 4), av_cost)
             WHERE rowid = L_rowid;
               ---
            if L_receive_as_type != 'P' then
               if UPDATE_STAKE_SKU_LOC(O_error_message,
                                       L_key1,
                                       I_to_loc_type,
                                       NVL(L_to_loc_vwh,I_to_loc),
                                       L_tran_date,
                                       0,
                                       REC_comp_item.qty*I_shortage_qty*-1)= FALSE then
                  return FALSE;
               end if;
            end if;
            ---
            if I_adjust_type in ('BL', 'SL') then
               L_l10n_fin_rec.item := REC_comp_item.item;
               L_l10n_fin_rec.av_cost := ROUND(L_upd_to_av_cost, 4);
               if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                         L_l10n_fin_rec) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
         ---
         -- From-loc updates:
         if L_frm_non_stkhldg_f_store = 'N' then
            ---
            L_key1 := REC_comp_item.item;
            L_key2 := NVL(L_from_loc_vwh,I_from_loc);
            ---
            open C_lock_ils(REC_comp_item.item,
                            NVL(L_from_loc_vwh,I_from_loc));
            fetch C_lock_ils into L_rowid;
            close C_lock_ils;

            if I_from_loc_type = 'S' then

               UPDATE item_loc_soh
                  SET stock_on_hand = stock_on_hand+ DECODE(I_adjust_type, 'NL', REC_comp_item.qty * I_shortage_qty,
                                                                           'BL', REC_comp_item.qty * I_shortage_qty, 0),
                      av_cost = DECODE(I_adjust_type, 'BL', DECODE(L_tsf_cost,NULL,ROUND(L_upd_from_av_cost, 4),av_cost),
                                                      'SL', DECODE(L_tsf_cost,NULL,ROUND(L_upd_from_av_cost, 4),av_cost), av_cost)
                WHERE rowid = L_rowid;
               ---
               if I_adjust_type in ('NL', 'BL') then
                  if UPDATE_STAKE_SKU_LOC(O_error_message,
                                          I_item,
                                          I_from_loc_type,
                                          NVL(L_from_loc_vwh,I_from_loc),
                                          L_tran_date,
                                          REC_comp_item.qty * I_shortage_qty,
                                          0)= FALSE then
                     return FALSE;
                  end if;
               end if;
               ---
            elsif I_from_loc_type = 'W' then
               UPDATE item_loc_soh
                  SET pack_comp_soh = pack_comp_soh + DECODE(I_adjust_type, 'NL', REC_comp_item.qty * I_shortage_qty,
                                                                            'BL', REC_comp_item.qty * I_shortage_qty, 0),
                      av_cost = DECODE(I_adjust_type, 'BL', ROUND(L_upd_from_av_cost,4),
                                                      'SL', ROUND(L_upd_from_av_cost,4), av_cost)
                WHERE rowid = L_rowid;
            end if;
         end if;
         ---
         -- write a reverse transaction for 'SL'/'BL' adjustment
         if I_adjust_type IN ('BL', 'SL') then

            if L_action_type IN ('O', 'R') then
               ---
               -- call STKLEDGR_SQL.WF_WRITE_FINANCIALS to write franchise order (20/82), franchise return (24/83) tran_codes
               ---
               if STKLEDGR_SQL.WF_WRITE_FINANCIALS(O_error_message,
                                                   L_dummy_cost,
                                                   L_dummy_retail,
                                                   I_tsf_alloc_no,
                                                   I_tsf_alloc_type,
                                                   L_tran_date,
                                                   REC_comp_item.item,
                                                   L_pack_no,                               -- pack no
                                                   L_pct_in_pack,                           -- pct_in_pack
                                                   REC_comp_item.dept,
                                                   REC_comp_item.class,
                                                   REC_comp_item.subclass,
                                                   -REC_comp_item.qty * I_shortage_qty,
                                                   NVL(L_from_loc_vwh, I_from_loc),
                                                   I_from_loc_type,
                                                   NVL(L_to_loc_vwh, I_to_loc),
                                                   I_to_loc_type,
                                                   NULL,                                    --- Weight UOM
                                                   NULL,
                                                   I_shipment,
                                                   I_carton) = FALSE then                       --- WF_order or RMA No
                  return FALSE;
               end if;
            else -- from location is not a franchise store or a warehouse
               ---
               -- call STKLEDGR_SQL.WRITE_FINANCIALS to write profit up-charge(tran code 28), expense up-charge(tran code 29),
               -- transfer-in/out (tran code 30/32) or intercompany in/out (tran code 37/38)
               ---
               -- Reversals for Tran_data should post at (shipment cost - upcharges) as initial postings for 32/38 were at this cost
               L_shipment_cost := (L_shipment_cost * L_pct_in_pack) - NVL(L_charge_from_loc,0);
               if STKLEDGR_SQL.WRITE_FINANCIALS(O_error_message,
                                                L_dummy_cost,
                                                NULL,             -- CALL TYPE
                                                I_shipment,
                                                I_tsf_alloc_no,
                                                NVL(L_receipt_date,L_tran_date),
                                                REC_comp_item.item,
                                                L_pack_no,
                                                L_pct_in_pack,
                                                REC_comp_item.dept,
                                                REC_comp_item.class,
                                                REC_comp_item.subclass,
                                                -REC_comp_item.qty * I_shortage_qty,
                                                NULL,                                   -- weight_cuom
                                                NVL(L_from_loc_vwh,I_from_loc),
                                                I_from_loc_type,
                                                L_from_finisher_ind,
                                                NVL(L_to_loc_vwh,I_to_loc),
                                                I_to_loc_type,
                                                L_to_finisher_ind,
                                                L_shipment_cost ,        -- Unit Cost at which reversal are posted
                                                L_profit_chrgs_to_loc,
                                                L_exp_chrgs_to_loc,
                                                L_intercompany) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;

         -- write inventory adjustment (TRAN_CODE 22)
         if I_adjust_type IN ('NL', 'RL', 'SL') then

            --write tran_data 22 for To-Loc
            if I_adjust_type in ('NL', 'RL') then
               if WRITE_INVENTORY_ADJUSTMENT(O_error_message,
                                             I_shipment,
                                             I_tsf_alloc_no,
                                             NVL(L_to_loc_vwh, I_to_loc),
                                             I_to_loc_type,
                                             REC_comp_item.item,
                                             -REC_comp_item.qty*I_shortage_qty,   --always negative adjustment for the to-loc
                                             NVL(L_receipt_date,L_tran_date)) = FALSE then   --from_to_loc
                  return FALSE;
               end if;
            end if;

            -- write tran_data 22 for From-Loc
            if L_frm_non_stkhldg_f_store = 'N' and I_adjust_type in ('NL', 'SL') then

               --positive adjustment for 'NL', negative adjustment for 'SL'
               if I_adjust_type = 'NL' then
                  L_sign := 1;
               else --'SL'
                  L_sign := -1;
               end if;
               ---
               if WRITE_INVENTORY_ADJUSTMENT(O_error_message,
                                             I_shipment,
                                             I_tsf_alloc_no,
                                             NVL(L_from_loc_vwh, I_from_loc),
                                             I_from_loc_type,
                                             REC_comp_item.item,
                                             L_sign*REC_comp_item.qty*I_shortage_qty,
                                             NVL(L_receipt_date,L_tran_date)) = FALSE then   --from_to_loc
                  return FALSE;
               end if;
            end if;
         end if;
         ---
      END LOOP;

      if I_from_loc_type = 'W' and I_adjust_type in ('NL', 'BL') then
         L_key1 := I_item;
         L_key2 := NVL(L_from_loc_vwh,I_from_loc);

         open C_lock_ils(I_item,
                         NVL(L_from_loc_vwh,I_from_loc));
         fetch C_lock_ils into L_rowid;
         close C_lock_ils;

         UPDATE item_loc_soh
            SET stock_on_hand = stock_on_hand + I_shortage_qty
          WHERE rowid = L_rowid;
         ---
         if UPDATE_STAKE_SKU_LOC(O_error_message,
                                 I_item,
                                 I_from_loc_type,
                                 NVL(L_from_loc_vwh,I_from_loc),
                                 L_tran_date,
                                 I_shortage_qty,
                                 0)= FALSE then
            return FALSE;
         end if;
         ---
      end if;

      if I_to_loc_type = 'W' and L_receive_as_type = 'P' then
         L_key1 := I_item;
         L_key2 := NVL(L_to_loc_vwh, I_to_loc);

         open C_lock_ils(I_item,
                         NVL(L_to_loc_vwh, I_to_loc));
         fetch C_lock_ils into L_rowid;
         close C_lock_ils;

         UPDATE item_loc_soh
            SET in_transit_qty = in_transit_qty - I_shortage_qty
          WHERE rowid = L_rowid;
         ---
         if UPDATE_STAKE_SKU_LOC(O_error_message,
                                 I_item,
                                 I_to_loc_type,
                                 NVL(L_to_loc_vwh,I_to_loc),
                                 L_tran_date,
                                 0,
                                 I_shortage_qty*-1)= FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
   end if; -- end of pack item
   ---
   --- Post WF billing for shortage quantity
   if L_action_type IN ('O', 'R') and I_adjust_type IN ('BL', 'SL') then
      if WF_TRANSFER_SQL.GET_WF_ORDER_RMA_NO(O_error_message,
                                             L_wf_order_no,
                                             L_rma_no,
                                             I_tsf_alloc_type,
                                             I_tsf_alloc_no,
                                             I_to_loc,
                                             I_to_loc_type) = FALSE then
         return FALSE;
      end if;
      ---
      if L_action_type = 'R' then
         if WF_BOL_SQL.WRITE_WF_BILLING_RETURNS(O_error_message,
                                                L_rma_no,
                                                I_tsf_alloc_no,
                                                I_item,
                                                I_to_loc,
                                                I_to_loc_type,
                                                -1 * I_shortage_qty,
                                                L_tran_date) = FALSE then
            return FALSE;
         end if;
       elsif L_action_type = 'O' then
         if WF_BOL_SQL.WRITE_WF_BILLING_SALES(O_error_message,
                                              L_wf_order_no,
                                              I_item,
                                              I_from_loc,
                                              I_from_loc_type,
                                              I_to_loc,
                                              NULL,
                                              NULL,
                                              -1 * I_shortage_qty,
                                              L_tran_date) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END ADJUST_SHORTAGE;
--------------------------------------------------------------------------------------------
FUNCTION TO_LOC_ADJUST_NL_BL(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_shipment         IN       SHIPMENT.SHIPMENT%TYPE,
                             I_carton           IN       SHIPSKU.CARTON%TYPE,
                             I_adjust_level     IN       VARCHAR2,
                             I_from_loc_type    IN       SHIPMENT.FROM_LOC_TYPE%TYPE,
                             I_from_loc         IN       SHIPMENT.FROM_LOC%TYPE,
                             I_to_loc_type      IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                             I_to_loc           IN       SHIPMENT.TO_LOC%TYPE,
                             I_adjust_type      IN       SHIPSKU.ADJUST_TYPE%TYPE,
                             I_distro_no        IN       TSFHEAD.TSF_NO%TYPE DEFAULT NULL,
                             I_document_type    IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)   := 'STOCK_ORDER_RECONCILE_SQL.TO_LOC_ADJUST_NL_BL';
   L_invalid_param   VARCHAR2(20);
   L_adj_qty         TSFDETAIL.RECONCILED_QTY%TYPE;
   L_adj_user        SHIPSKU.RECONCILE_USER_ID%TYPE := GET_USER;
   L_adj_date        SHIPSKU.RECONCILE_DATE%TYPE := get_vdate;
   L_rowid           ROWID;
   L_table           VARCHAR2(30) := 'SHIPSKU';
   L_key1            VARCHAR2(100) := to_char(I_shipment);
   L_key2            VARCHAR2(100);

   L_doc_exist       VARCHAR2(1) := 'N';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_ITEM IS
      SELECT td.item,
             td.tsf_no distro_no,
             td.tsf_seq_no,
             ss.distro_type,
             NVL(ss.qty_received,0) received_qty,
             ss.qty_expected ship_qty,
             t.from_loc,
             DECODE(I_to_loc_type, 'S', NVL(ss.actual_receiving_store,t.to_loc), t.to_loc) to_loc,
             ss.rowid
        FROM shipment s,
             shipsku ss,
             tsfhead t,
             tsfdetail td
       WHERE s.shipment = ss.shipment
         AND ss.distro_no = t.tsf_no
         AND t.tsf_no = td.tsf_no
         AND ss.distro_type = 'T'
         AND ss.item = td.item
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         AND (ss.adjust_type is null or ss.adjust_type not in ('SL','RL','FC','FR','NL','BL'))
         AND NVL(ss.qty_received,0) < ss.qty_expected
         AND s.shipment = I_shipment
         AND ss.distro_no = NVL(I_distro_no, ss.distro_no)
         AND ss.distro_type = NVL(I_document_type,'T')
      UNION ALL
      SELECT ah.item,
             ah.alloc_no distro_no,
             NULL alloc_seq_no,
             ss.distro_type,
             ss.qty_received received_qty,
             ss.qty_expected ship_qty,
             ah.wh from_loc,
             DECODE(I_to_loc_type, 'S', NVL(ss.actual_receiving_store,ad.to_loc), ad.to_loc) to_loc,
             ss.rowid
        FROM shipment s,
             shipsku ss,
             wh wh,
             alloc_header ah,
             alloc_detail ad
       WHERE s.shipment = ss.shipment
         AND ss.distro_no = ah.alloc_no
         AND ah.alloc_no = ad.alloc_no
         AND ss.distro_type = 'A'
         AND ss.item = ah.item
         AND s.status_code = 'R'
         AND NVL(ss.carton,'-*-') = NVL(I_carton, NVL(ss.carton,'-*-'))
         AND ad.to_loc = DECODE(ad.to_loc_type, 'W', wh.wh, I_to_loc)
         AND wh.wh(+) = ad.to_loc
         AND wh.physical_wh(+) = I_to_loc
         AND (ss.adjust_type is null or ss.adjust_type not in ('SL','RL','FC','FR','NL','BL'))
         AND ss.qty_received < ss.qty_expected
         AND s.shipment = I_shipment
         AND ss.distro_no = NVL(I_distro_no, ss.distro_no)
         AND ss.distro_type = NVL(I_document_type,'A');

   CURSOR C_shipsku_lock(cv_rowid   ROWID) is
      SELECT rowid
        FROM shipsku ss
       WHERE ss.shipment = I_shipment
         AND ss.rowid = cv_rowid
         FOR UPDATE NOWAIT;

BEGIN
   ---
   if I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   elsif I_adjust_level = 'C' and I_carton is NULL then
      L_invalid_param := 'I_carton';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                             L_program,
                                             L_invalid_param,
                                             'NULL');
      return FALSE;
   end if;
   ---
   FOR REC_ITEM IN C_ITEM LOOP
      ---
         L_adj_qty := REC_ITEM.ship_qty - REC_ITEM.received_qty;
      ---
      if ADJUST_SHORTAGE(O_error_message,
                         I_shipment,
                         I_carton,
                         REC_ITEM.distro_type,
                         REC_ITEM.distro_no,
                         REC_ITEM.tsf_seq_no,
                         I_from_loc_type,
                         REC_ITEM.from_loc,
                         I_to_loc_type,
                         REC_ITEM.to_loc,
                         REC_ITEM.item,
                         L_adj_qty,
                         I_adjust_type) = FALSE then
         return FALSE;
      end if;
      ---
      L_key2 := REC_ITEM.item;
      ---
      open C_shipsku_lock(REC_ITEM.rowid);
      close C_shipsku_lock;
      ---
      update shipsku ss
         set ss.adjust_type = I_adjust_type,
             ss.reconcile_user_id = L_adj_user,
             ss.reconcile_date = L_adj_date
       where rowid = REC_ITEM.rowid;
      ---
   END LOOP;
   ---
   INSERT INTO doc_close_queue
   SELECT DISTINCT distro_no, distro_type
     FROM shipsku s
    WHERE shipment = I_shipment
      AND adjust_type IS NOT NULL
      AND reconcile_user_id = L_adj_user
      AND reconcile_date = L_adj_date
      AND distro_no = NVL(I_distro_no, distro_no)
      AND distro_type = NVL(I_document_type,distro_type)
      AND NOT EXISTS (SELECT 'Y'
                        FROM doc_close_queue
                       WHERE doc = s.distro_no
                         AND doc_type = s.distro_type);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                        L_program,
                        to_char(SQLCODE));
      return FALSE;

END TO_LOC_ADJUST_NL_BL;
--------------------------------------------------------------------------------
FUNCTION APPLY_ADJUSTMENTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_document_type   IN       VARCHAR2,
                           I_asn_nbr         IN       SHIPMENT.BOL_NO%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(64) := 'STOCK_ORDER_RECONCILE_SQL.APPLY_ADJUSTMENTS';
   L_carton_ind                VARCHAR2(1);
   L_inventory_treatment_ind   SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND%TYPE;
   L_tran_date                 TRAN_DATA.TRAN_DATE%TYPE := GET_VDATE;

   cursor C_SHIPMENT_CARTON_DETAILS is
      select distinct sh.shipment,
             sk.carton,
             sh.from_loc,
             sh.from_loc_type,
             sh.to_loc,
             sh.to_loc_type,
             sh.receive_date,
             sh.ship_date
        from shipment sh,
             shipsku sk
       where sh.shipment=sk.shipment
         and sh.bol_no =I_asn_nbr;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;

   FOR rec in C_SHIPMENT_CARTON_DETAILS LOOP
   --
      L_receipt_date := NVL(rec.receive_date,L_tran_date);
   --
     L_ship_date:= NVL(rec.ship_date,L_tran_date);
	 
      if STOCK_ORDER_RCV_SQL.GET_INVENTORY_TREATMENT(O_error_message,
                                                     L_inventory_treatment_ind,
                                                     rec.from_loc_type,
                                                     rec.to_loc_type,
                                                     NULL, -- Distro No is Null as shipment is closed,
                                                     I_document_type,
                                                     rec.shipment,
                                                     NULL, -- shipment sequence number is
                                                     TRUE, -- Shipment close indicator
                                                     FALSE) = FALSE then
         return FALSE;
      end if;

      if (rec.to_loc_type ='S' and LP_system_options_row.tsf_auto_close_store='Y') or
         (rec.to_loc_type ='W' and LP_system_options_row.tsf_auto_close_wh='Y') then

         update shipment
            set status_code  = 'R',
                receive_date = L_receipt_date
          where bol_no       = I_asn_nbr;

         if L_inventory_treatment_ind ='SL' then

            if FROM_LOC_ADJUST(O_error_message,
                               rec.shipment,
                               rec.carton,
                               NULL,
                               rec.from_loc_type,
                               rec.from_loc,
                               rec.to_loc_type,
                               rec.to_loc) = FALSE then
               return FALSE;
            end if;
         elsif L_inventory_treatment_ind ='RL' then
            if TO_LOC_ADJUST(O_error_message,
                             rec.shipment,
                             rec.carton,
                             NULL,
                             rec.from_loc_type,
                             rec.from_loc,
                             rec.to_loc_type,
                             rec.to_loc) = FALSE then
               return FALSE;
            end if;
         elsif L_inventory_treatment_ind in ('NL','BL') then
            if TO_LOC_ADJUST_NL_BL(O_error_message,
                                   rec.shipment,
                                   rec.carton,
                                   NULL,
                                   rec.from_loc_type,
                                   rec.from_loc,
                                   rec.to_loc_type,
                                   rec.to_loc,
                                   L_inventory_treatment_ind) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END APPLY_ADJUSTMENTS;
-----------------------------------------------------------------------------------------
FUNCTION  APPLY_ADJUSTMENTS_DOC_CLOSE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_distro_no       IN       TSFHEAD.TSF_NO%TYPE,
                                       I_document_type   IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(64)   := 'STOCK_ORDER_RECONCILE_SQL.APPLY_ADJUSTMENTS_DOC_CLOSE';

   L_system_options_row        SYSTEM_OPTIONS%ROWTYPE;
   L_inventory_treatment_ind   SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND%TYPE;

   CURSOR C_SHIPMENT_DETAILS is
      select distinct sh.shipment,
                      sh.from_loc,
                      sh.from_loc_type,
                      sh.to_loc,
                      sh.to_loc_type,
                      sh.ship_date
        from shipment sh
       where exists (select 'x'
                       from shipsku ss
                      where ss.distro_no = I_distro_no
                        and ss.shipment = sh.shipment);

BEGIN
   if I_distro_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_distro_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   FOR rec in C_SHIPMENT_DETAILS LOOP
   L_ship_date:=NVL(rec.ship_date,L_tran_date);
      if STOCK_ORDER_RCV_SQL.GET_INVENTORY_TREATMENT(O_error_message,
                                                     L_inventory_treatment_ind,
                                                     rec.from_loc_type,
                                                     rec.to_loc_type,
                                                     NULL, -- Distro No is Null as shipment is closed,
                                                     I_document_type,
                                                     rec.shipment,
                                                     NULL, -- shipment sequence number is
                                                     TRUE, -- Shipment close indicator
                                                     FALSE) = FALSE then
         return FALSE;
      end if;
      ---
      if L_inventory_treatment_ind ='SL' then
         if FROM_LOC_ADJUST(O_error_message,
                            rec.shipment,
                            NULL,
                            NULL,
                            rec.from_loc_type,
                            rec.from_loc,
                            rec.to_loc_type,
                            rec.to_loc,
                            I_distro_no,
                            I_document_type) = FALSE then
            return FALSE;
         end if;
      elsif L_inventory_treatment_ind ='RL' then
         if TO_LOC_ADJUST(O_error_message,
                          rec.shipment,
                          NULL,
                          NULL,
                          rec.from_loc_type,
                          rec.from_loc,
                          rec.to_loc_type,
                          rec.to_loc,
                          I_distro_no,
                          I_document_type) = FALSE then
            return FALSE;
         end if;
      elsif L_inventory_treatment_ind in ('NL','BL') then
         if TO_LOC_ADJUST_NL_BL(O_error_message,
                                rec.shipment,
                                NULL,
                                NULL,
                                rec.from_loc_type,
                                rec.from_loc,
                                rec.to_loc_type,
                                rec.to_loc,
                                L_inventory_treatment_ind,
                                I_distro_no,
                                I_document_type) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END APPLY_ADJUSTMENTS_DOC_CLOSE;
-----------------------------------------------------------------------------------------
FUNCTION UPDATE_STAKE_SKU_LOC(O_error_message IN OUT VARCHAR2,
                              I_item          IN     item_loc.item%TYPE,
                              I_loc_type      IN     item_loc.loc_type%TYPE,
                              I_location      IN     item_loc.loc%TYPE,
                              I_tran_date     IN     DATE,
                              I_adj_soh       IN     item_loc_soh.stock_on_hand%TYPE,
                              I_adj_intran    IN     item_loc_soh.stock_on_hand%TYPE)
RETURN BOOLEAN IS

   TYPE rowid_TBL is table of ROWID;
   TYPE adj_qty_TBL is table of item_loc_soh.stock_on_hand%TYPE;

   L_rowid_tbl        rowid_TBL;
   L_adj_soh_tbl      adj_qty_TBL;
   L_adj_intran_tbl   adj_qty_TBL;
   L_table            VARCHAR2(30);

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

cursor C_LOCK_STAKE_SKU_LOC is
   select rowid,
          I_adj_soh,
          I_adj_intran
     from stake_sku_loc
    where stake_sku_loc.item  = I_item
      and stake_sku_loc.loc_type = I_loc_type
      and stake_sku_loc.location = I_location
      and stake_sku_loc.processed = 'N'
      and stake_sku_loc.snapshot_on_hand_qty is not NULL
      and cycle_count in (SELECT cycle_count
                            FROM stake_head
                           WHERE stake_head.stocktake_date >= I_tran_date)
      for update nowait;

BEGIN

   L_table := 'stake_sku_loc';
   SQL_LIB.SET_MARK('UPDATE', NULL, 'stake_sku_loc',
                    'ITEM: '||I_item);

   open C_LOCK_STAKE_SKU_LOC;
   fetch C_LOCK_STAKE_SKU_LOC BULK COLLECT INTO L_rowid_tbl,
                                                L_adj_soh_tbl,
                                                L_adj_intran_tbl;
   close C_LOCK_STAKE_SKU_LOC;

   if L_rowid_tbl.COUNT > 0 then
      FORALL i in 1..L_rowid_tbl.COUNT
      UPDATE stake_sku_loc s
         SET s.snapshot_on_hand_qty = NVL(s.snapshot_on_hand_qty,0) + L_adj_soh_tbl(i),
             s.snapshot_in_transit_qty = NVL(s.snapshot_in_transit_qty,0) + L_adj_intran_tbl(i)
       WHERE rowid = L_rowid_tbl(i);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_item,
                                             NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'STOCK_ORDER_RECONCILE_SQL.UPDATE_STAKE_SKU_LOC',
                                          to_char(SQLCODE));
END UPDATE_STAKE_SKU_LOC;
-----------------------------------------------------------------------------------------
END STOCK_ORDER_RECONCILE_SQL;
/
