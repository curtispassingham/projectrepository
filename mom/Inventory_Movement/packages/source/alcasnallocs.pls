CREATE OR REPLACE PACKAGE ALC_ASN_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name: ALLOC_ASN
--Purpose:       Allocates all Obligations for a given Purchase Order
--               and inserts them into the Actual Landed Cost tables.
--               This function is only used for Obligations at the
--               Purchase Order Header or Purchase Order/Item levels.
--               Obligations at the PO Header level must wait to be
--               allocated until the PO has been closed.  Obligations
--               at the PO/Item level must be re-allocated once the PO
--               is closed to pick up the final quantities that were
--               received.  This function will be called upon PO closure.
--               Obligations at all other levels will be allocated upon
--               Obligation creation by calling the ALLOC_ALLOC_ALL_OBL_COMPS
--               function.
-------------------------------------------------------------------------------
FUNCTION ALLOC_ASN (O_error_message      IN OUT   VARCHAR2,
                    O_handled            IN OUT   BOOLEAN,
                    I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                    I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                    I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                    I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                    I_key_value_3        IN       OBLIGATION.KEY_VALUE_3%TYPE,
                    I_key_value_4        IN       OBLIGATION.KEY_VALUE_4%TYPE,
                    I_key_value_5        IN       OBLIGATION.KEY_VALUE_5%TYPE,
                    I_key_value_6        IN       OBLIGATION.KEY_VALUE_6%TYPE)
   RETURN BOOLEAN ;
-------------------------------------------------------------------------------
--Function Name: ASN_NO_EXISTS
--Purpose:       Allocates all Obligations for a given Purchase Order
-------------------------------------------------------------------------------
FUNCTION ASN_NO_EXISTS(O_error_message   IN OUT   VARCHAR2,
                       O_valid              OUT   BOOLEAN ,
                       I_asn_no          IN       SHIPMENT.ASN%TYPE) 
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END ALC_ASN_SQL;
/
