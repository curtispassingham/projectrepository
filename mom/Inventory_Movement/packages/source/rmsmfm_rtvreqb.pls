CREATE OR REPLACE PACKAGE BODY RMSMFM_RTVREQ AS

TYPE ROWID_TBL    IS TABLE OF ROWID INDEX BY BINARY_INTEGER;

-- Depending upon the message type, there will come a point in the publication process
-- when error retry will no longer be possible. This variable will track whether or
-- not that point has been reached.
LP_error_status   VARCHAR2(1)   :=  NULL;

--------------------------------------------------------
       /*** Private Program Declarations ***/
--------------------------------------------------------

---
PROCEDURE HANDLE_ERRORS(O_status_code   IN OUT  VARCHAR2,
                        O_error_msg     IN OUT  VARCHAR2,
                        O_message       IN OUT  RIB_OBJECT,
                        O_message_type  IN OUT  VARCHAR2,
                        O_bus_obj_id    IN OUT  RIB_BUSOBJID_TBL,
                        O_routing_info  IN OUT  RIB_ROUTINGINFO_TBL,
                        I_rtv_order_no  IN      RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                        I_seq_no        IN      RTVREQ_MFQUEUE.SEQ_NO%TYPE,
                        I_rtv_seq_no    IN      RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE);
---
FUNCTION PROCESS_QUEUE_RECORD(O_error_msg      IN OUT         VARCHAR2,
                              O_break_loop     IN OUT         BOOLEAN,
                              O_message        IN OUT nocopy  RIB_OBJECT,
                              O_routing_info   IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id     IN OUT nocopy  RIB_BUSOBJID_TBL,
                              O_message_type   IN OUT         VARCHAR2,
                              I_rtv_order_no   IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                              I_hdr_published  IN             RTVREQ_PUB_INFO.PUBLISHED%TYPE,
                              I_rtv_seq_no     IN             RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE,
                              I_rtv_item       IN             RTVREQ_MFQUEUE.ITEM%TYPE, 
                              I_pub_status     IN             RTVREQ_MFQUEUE.PUB_STATUS%TYPE,
                              I_seq_no         IN             RTVREQ_MFQUEUE.SEQ_NO%TYPE,
                              I_rowid          IN             ROWID,
                              O_keep_queue     IN OUT         BOOLEAN)
RETURN BOOLEAN;
---
FUNCTION MAKE_CREATE(O_error_msg     IN OUT         VARCHAR2,
                     O_message       IN OUT nocopy  RIB_OBJECT,
                     O_routing_info  IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                     I_rtv_order_no  IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                     I_rtv_seq_no    IN             RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE,
                     I_max_details   IN             RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE,
                     I_rowid         IN             ROWID,
                     O_keep_queue    IN OUT         BOOLEAN)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg           IN OUT         VARCHAR2,
                                     O_rib_rtvreqdesc_rec  IN OUT nocopy  "RIB_RTVReqDesc_REC",
                                     I_message_type        IN             RTVREQ_MFQUEUE.MESSAGE_TYPE%TYPE,
                                     I_rtv_order_no        IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                                     I_rtv_seq_no          IN             RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE,
                                     I_rtv_item            IN             RTVREQ_MFQUEUE.ITEM%TYPE, 
                                     I_max_details         IN             RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_DELETE_OBJECTS(O_error_msg          IN OUT         VARCHAR2,
                                     O_rib_rtvreqref_rec  IN OUT nocopy  "RIB_RTVReqRef_REC",
                                     I_rtv_order_no       IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                                     I_max_details        IN             RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_HEADER_OBJECT(O_error_msg           IN OUT         VARCHAR2,
                             O_rib_rtvreqdesc_rec  IN OUT nocopy  "RIB_RTVReqDesc_REC",
                             O_rib_rtvreqref_rec   IN OUT nocopy  "RIB_RTVReqRef_REC",
                             O_routing_info        IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                             I_rtv_order_no        IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg             IN OUT         VARCHAR2,
                              O_message               IN OUT nocopy  "RIB_RTVReqDtl_TBL",
                              O_rtvreq_mfqueue_rowid  IN OUT nocopy  ROWID_TBL,
                              O_rtvreq_mfqueue_size   IN OUT         BINARY_INTEGER,
                              O_rtv_detail_rowid      IN OUT nocopy  ROWID_TBL,
                              O_rtv_detail_size       IN OUT         BINARY_INTEGER,
                              O_delete_rowid_ind      IN OUT         VARCHAR2,
                              I_message_type          IN             RTVREQ_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_rtv_order_no          IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                              I_rtv_item              IN             RTVREQ_MFQUEUE.ITEM%TYPE, 		
                              I_max_details           IN             RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_SINGLE_DETAIL(O_error_msg           IN OUT         VARCHAR2,
                             O_message             IN OUT nocopy  "RIB_RTVReqDtl_TBL",
                             IO_rib_rtvreqdtl_rec  IN OUT nocopy  "RIB_RTVReqDtl_REC",
                             I_rtv_order_no        IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                             I_rtv_seq_no          IN             RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE,
                             I_item                IN             RTV_DETAIL.ITEM%TYPE,
                             I_shipment            IN             RTV_DETAIL.SHIPMENT%TYPE,
                             I_inv_status          IN             RTV_DETAIL.INV_STATUS%TYPE,
                             I_qty_requested       IN             RTV_DETAIL.QTY_REQUESTED%TYPE,
                             I_unit_cost           IN             RTV_DETAIL.UNIT_COST%TYPE,
                             I_reason              IN             RTV_DETAIL.REASON%TYPE)
RETURN BOOLEAN;
---
FUNCTION DELETE_QUEUE_REC(O_error_msg  IN OUT  VARCHAR2,
                          I_seq_no     IN      RTVREQ_MFQUEUE.SEQ_NO%TYPE)
RETURN BOOLEAN;
---
FUNCTION LOCK_THE_BLOCK(O_error_msg     IN OUT  VARCHAR2,
                        O_queue_locked  IN OUT  BOOLEAN,
                        I_rtv_order_no  IN      RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE)
RETURN BOOLEAN;
---

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
           /*** Public Program Bodies ***/
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_msg     IN OUT  VARCHAR2,
                I_message_type  IN      VARCHAR2,
                I_rtv_order_no  IN      RTV_HEAD.RTV_ORDER_NO%TYPE,
                I_status        IN      RTV_HEAD.STATUS_IND%TYPE,
                I_ext_ref_no    IN      RTV_HEAD.EXT_REF_NO%TYPE,
                I_rtv_seq_no    IN      RTV_DETAIL.SEQ_NO%TYPE,
                I_item          IN      RTV_DETAIL.ITEM%TYPE,
                I_publish_ind   IN      RTV_DETAIL.PUBLISH_IND%TYPE)
RETURN BOOLEAN IS

   L_max_details        RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE   :=  NULL;
   L_thread_no          RIB_SETTINGS.NUM_THREADS%TYPE              :=  NULL;
   L_initial_approval   VARCHAR2(1)                                :=  'N';
   L_num_threads        RIB_SETTINGS.NUM_THREADS%TYPE              :=  NULL;
   L_min_time_lag       RIB_SETTINGS.MINUTES_TIME_LAG%TYPE         :=  NULL;
   L_status_code        VARCHAR2(1)                                :=  NULL;
   L_shipped            RTVREQ_PUB_INFO.SHIPPED_IND%TYPE           :=  NULL;
   
   cursor C_HEAD is
      select thread_no,
             initial_approval_ind,
             shipped_ind
        from rtvreq_pub_info
       where rtv_order_no = I_rtv_order_no;

BEGIN
   if I_message_type != RMSMFM_RTVREQ.HDR_ADD then
      open C_HEAD;
      fetch C_HEAD into L_thread_no,
                        L_initial_approval,
                        L_shipped;
      if C_HEAD%NOTFOUND then
         close C_HEAD;
         O_error_msg := SQL_LIB.CREATE_MSG('PUB_INFO_NOT_FOUND',
                                           'RTVREQ_PUB_INFO',
                                           I_rtv_order_no,
                                           NULL);
         return FALSE;
      end if;
      close C_HEAD;
   end if;

   if I_message_type != RMSMFM_RTVREQ.HDR_ADD then
      if I_message_type = RMSMFM_RTVREQ.HDR_UPD then
         if I_status = '10' and L_initial_approval = 'N' then
            update rtvreq_pub_info
               set initial_approval_ind = 'Y'
             where rtv_order_no = I_rtv_order_no;
             
            L_initial_approval := 'Y';
         end if;
      
         if I_status = '15' and L_shipped = 'N' then
            update rtvreq_pub_info
               set shipped_ind = 'Y'
             where rtv_order_no = I_rtv_order_no;
             
            L_shipped := 'Y';
         
            return TRUE;
         end if;
      end if;
      
      if ((I_message_type not in (RMSMFM_RTVREQ.HDR_DEL,RMSMFM_RTVREQ.DTL_DEL)) and (L_initial_approval = 'N' or L_shipped = 'Y' or I_status = 12)) then
         return TRUE;
      end if;
   end if;

   -- If the message is a detail message, all previous records on the queue
   -- relating to the detail record can be deleted.
   if I_message_type = RMSMFM_RTVREQ.DTL_DEL then
   
      delete from rtvreq_mfqueue
       where rtv_order_no = I_rtv_order_no
         and rtv_seq_no = I_rtv_seq_no;

   elsif I_message_type = RMSMFM_RTVREQ.DTL_UPD then

      delete from rtvreq_mfqueue
       where rtv_order_no = I_rtv_order_no
         and rtv_seq_no = I_rtv_seq_no
         and message_type = RMSMFM_RTVREQ.DTL_UPD;
         
   elsif I_message_type = RMSMFM_RTVREQ.HDR_DEL then

      delete from rtvreq_mfqueue
       where rtv_order_no = I_rtv_order_no;
       
      delete from rtvreq_pub_info 
       where rtv_order_no = I_rtv_order_no;
       
      return TRUE;

   end if;

   if I_message_type = RMSMFM_RTVREQ.HDR_ADD then
  
      API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                   O_error_msg,
                                   L_max_details,
                                   L_num_threads,
                                   L_min_time_lag,
                                   RMSMFM_RTVREQ.FAMILY);
      if L_status_code in (API_CODES.UNHANDLED_ERROR) then
         return FALSE;
      end if;
          
      insert into rtvreq_pub_info (rtv_order_no,
                                   initial_approval_ind,
                                   shipped_ind,
                                   thread_no,
                                   published)
                           values (I_rtv_order_no,
                                   decode(I_status, '10', 'Y','12','Y', 'N'),
                                   decode(I_status, '15', 'Y', 'N'),
                                   MOD(I_rtv_order_no, L_num_threads) + 1,
                                   decode(I_ext_ref_no, NULL, 'N', 'Y'));

   elsif (I_publish_ind = 'Y' or I_message_type != RMSMFM_RTVREQ.DTL_DEL) then

      insert into rtvreq_mfqueue (seq_no, 
                                  rtv_order_no,
                                  rtv_seq_no,
                                  item,
                                  message_type,
                                  thread_no,
                                  family,
                                  custom_message_type,
                                  pub_status,
                                  transaction_number,
                                  transaction_time_stamp)
                          values (rtvreq_mfsequence.nextval,
                                  I_rtv_order_no,
                                  I_rtv_seq_no,
                                  I_item,
                                  I_message_type,
                                  L_thread_no,
                                  RMSMFM_RTVREQ.FAMILY,
                                  NULL,
                                  'U',
                                  I_rtv_order_no,
                                  sysdate);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RTVREQ.ADDTOQ',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;

--------------------------------------------------------------------------------

PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1)
IS

   L_break_loop      BOOLEAN                            :=  FALSE;
   L_message_type    RTVREQ_MFQUEUE.MESSAGE_TYPE%TYPE   :=  NULL;

   L_rtv_order_no    RTV_HEAD.RTV_ORDER_NO%TYPE         :=  NULL;
   L_rtv_seq_no      RTV_DETAIL.SEQ_NO%TYPE;
   L_rtv_item        RTV_DETAIL.ITEM%TYPE                := NULL;
   L_seq_no          RTVREQ_MFQUEUE.SEQ_NO%TYPE         :=  NULL;
   L_seq_limit       RTVREQ_MFQUEUE.SEQ_NO%TYPE         :=  0;

   L_pub_status      RTVREQ_MFQUEUE.PUB_STATUS%TYPE     :=  NULL;
   L_rowid           ROWID                              :=  NULL;

   L_hdr_published   RTVREQ_PUB_INFO.PUBLISHED%TYPE     :=  NULL;

   L_hosp            VARCHAR2(1)                        :=  'N';
   L_queue_locked    BOOLEAN                            :=  FALSE;
   L_keep_queue      BOOLEAN                            :=  FALSE;

   cursor C_QUEUE is
      select q.rtv_order_no,
             q.rtv_seq_no,
             q.item,
             q.message_type,
             q.pub_status,
             q.seq_no,
             q.rowid
        from rtvreq_mfqueue q
       where q.seq_no = (select min(q2.seq_no)
                           from rtvreq_mfqueue q2
                          where q2.thread_no  = I_thread_val
                            and q2.pub_status = 'U'
                            and q2.seq_no     > L_seq_limit)
         and q.thread_no = I_thread_val;
  
   cursor C_HOSP is
      select 'x'
        from rtvreq_mfqueue
       where rtv_order_no = L_rtv_order_no
         and pub_status = API_CODES.HOSPITAL;

   cursor C_GET_PUBLISHED is
      select published
        from rtvreq_pub_info
       where rtv_order_no = L_rtv_order_no;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   LOOP
      O_message := NULL;

      open C_QUEUE;
      fetch C_QUEUE into L_rtv_order_no,
                         L_rtv_seq_no,
                         L_rtv_item,
                         L_message_type,
                         L_pub_status,
                         L_seq_no,
                         L_rowid;
      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
      close C_QUEUE;

      if LOCK_THE_BLOCK(O_error_msg,
                        L_queue_locked,
                        L_rtv_order_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked = FALSE then

         open  C_HOSP;
         fetch C_HOSP into L_hosp;
         if C_HOSP%FOUND then
            close C_HOSP;
            O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                              NULL, NULL, NULL);
            raise PROGRAM_ERROR;
         end if;
         close C_HOSP;
   
         open  C_GET_PUBLISHED;
         fetch C_GET_PUBLISHED into L_hdr_published;
         close C_GET_PUBLISHED;

         if PROCESS_QUEUE_RECORD(O_error_msg,
                                 L_break_loop,
                                 O_message,
                                 O_routing_info,
                                 O_bus_obj_id,
                                 L_message_type,
                                 L_rtv_order_no,
                                 L_hdr_published,
                                 L_rtv_seq_no,
                                 L_rtv_item,
                                 L_pub_status,
                                 L_seq_no,
                                 L_rowid,
                                 L_keep_queue) = FALSE then
            raise PROGRAM_ERROR;
         end if;

         if L_break_loop = TRUE then
            O_message_type := L_message_type;
            EXIT;
         end if;

      else 
         L_seq_limit := L_seq_no;
      end if;

   END LOOP;

   if O_message is NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id  := RIB_BUSOBJID_TBL(L_rtv_order_no);
   end if;
   
EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_rtv_order_no,
                    L_seq_no,
                    L_rtv_seq_no);
END GETNXT;

--------------------------------------------------------------------------------

PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT)
IS

   L_seq_no          RTVREQ_MFQUEUE.SEQ_NO%TYPE         :=  NULL;

   L_break_loop      BOOLEAN                            :=  FALSE;

   L_rtv_order_no    RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE   :=  NULL;
   L_hdr_published   RTVREQ_PUB_INFO.PUBLISHED%TYPE     :=  NULL;
   L_rtv_seq_no      RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE     :=  NULL;
   L_rtv_item        RTV_DETAIL.ITEM%TYPE                := NULL;
   L_message_type    RTVREQ_MFQUEUE.MESSAGE_TYPE%TYPE   :=  NULL;
   L_pub_status      RTVREQ_MFQUEUE.PUB_STATUS%TYPE     :=  NULL;
   L_rowid           ROWID                              :=  NULL;

   L_keep_queue      BOOLEAN                            :=  FALSE;
   L_queue_locked    BOOLEAN                            :=  FALSE;

   cursor C_RETRY_QUEUE is
      select q.rtv_order_no,
             rho.published hdr_published,
             q.rtv_seq_no,
             q.item,
             q.message_type,
             q.pub_status,
             q.rowid
        from rtvreq_mfqueue q,
             rtvreq_pub_info rho
       where q.seq_no = L_seq_no
         and q.rtv_order_no = rho.rtv_order_no;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;


   --get info from routing info
   ---assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;

   O_message := NULL;

   --get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_rtv_order_no,
                            L_hdr_published,
                            L_rtv_seq_no,
                            L_rtv_item,
                            O_message_type,
                            L_pub_status,
                            L_rowid;
   close C_RETRY_QUEUE;

   if L_rtv_order_no is NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   if LOCK_THE_BLOCK(O_error_msg,
                     L_queue_locked,
                     L_rtv_order_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then

      if PROCESS_QUEUE_RECORD( O_error_msg,
                               L_break_loop,
                               O_message,
                               O_routing_info,
                               O_bus_obj_id,
                               O_message_type,
                               L_rtv_order_no,
                               L_hdr_published,
                               L_rtv_seq_no,
                               L_rtv_item,
                               L_pub_status,
                               L_seq_no,
                               L_rowid,
                               L_keep_queue) = FALSE then
         raise PROGRAM_ERROR;
      end if;
  
      if O_message is NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         if L_keep_queue then
            O_status_code := API_CODES.INCOMPLETE_MSG;
         else
            O_status_code := API_CODES.NEW_MSG;
         end if;
         O_bus_obj_id := RIB_BUSOBJID_TBL(L_rtv_order_no);
      end if;
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_rtv_order_no,
                    L_seq_no,
                    L_rtv_seq_no);
END PUB_RETRY;

--------------------------------------------------------------------------------
           /*** Private Program Bodies ***/
--------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code   IN OUT  VARCHAR2,
                        O_error_msg     IN OUT  VARCHAR2,
                        O_message       IN OUT  RIB_OBJECT,
                        O_message_type  IN OUT  VARCHAR2,
                        O_bus_obj_id    IN OUT  RIB_BUSOBJID_TBL,
                        O_routing_info  IN OUT  RIB_ROUTINGINFO_TBL,
                        I_rtv_order_no  IN      RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                        I_seq_no        IN      RTVREQ_MFQUEUE.SEQ_NO%TYPE,
                        I_rtv_seq_no    IN      RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE)
IS

   L_rib_rtvreqref_rec      "RIB_RTVReqRef_REC"      :=  NULL;
   L_rib_rtvreqdtlref_rec   "RIB_RTVReqDtlRef_REC"   :=  NULL;
   L_rib_rtvreqdtlref_tbl   "RIB_RTVReqDtlRef_TBL"   :=  NULL;

   L_error_type             VARCHAR2(5)            :=  NULL;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then

      O_bus_obj_id    := RIB_BUSOBJID_TBL(I_rtv_order_no);
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no', I_seq_no, NULL, NULL, NULL, NULL));

      L_rib_rtvreqdtlref_rec := "RIB_RTVReqDtlRef_REC"(0, I_rtv_seq_no,NULL);
      L_rib_rtvreqdtlref_tbl := "RIB_RTVReqDtlRef_TBL"(L_rib_rtvreqdtlref_rec);
      L_rib_rtvreqref_rec    := "RIB_RTVReqRef_REC"(0,
                                                  I_rtv_order_no,
                                                  L_rib_rtvreqdtlref_tbl);

      O_message := L_rib_rtvreqref_rec;

      update rtvreq_mfqueue
         set pub_status = LP_error_status
       where seq_no     = I_seq_no;

   end if;

   /* Pass out parsed error message */ 
   SQL_LIB.API_MSG(L_error_type, 
                   O_error_msg); 

EXCEPTION 
   when OTHERS then 
      API_LIBRARY.HANDLE_ERRORS(O_status_code, 
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR, 
                                'RMSMFM_RTVREQ'); 
END HANDLE_ERRORS; 

--------------------------------------------------------------------------------

FUNCTION PROCESS_QUEUE_RECORD(O_error_msg      IN OUT         VARCHAR2,
                              O_break_loop     IN OUT         BOOLEAN,
                              O_message        IN OUT nocopy  RIB_OBJECT,
                              O_routing_info   IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id     IN OUT nocopy  RIB_BUSOBJID_TBL,
                              O_message_type   IN OUT         VARCHAR2,
                              I_rtv_order_no   IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                              I_hdr_published  IN             RTVREQ_PUB_INFO.PUBLISHED%TYPE,
                              I_rtv_seq_no     IN             RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE,
                              I_rtv_item       IN             RTVREQ_MFQUEUE.ITEM%TYPE,
                              I_pub_status     IN             RTVREQ_MFQUEUE.PUB_STATUS%TYPE,
                              I_seq_no         IN             RTVREQ_MFQUEUE.SEQ_NO%TYPE,
                              I_rowid          IN             ROWID,
                              O_keep_queue     IN OUT         BOOLEAN)
RETURN BOOLEAN IS

   L_max_details          RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE   :=  NULL;
   L_num_threads          RIB_SETTINGS.NUM_THREADS%TYPE              :=  NULL;
   L_min_time_lag         RIB_SETTINGS.MINUTES_TIME_LAG%TYPE         :=  NULL;

   L_rib_rtvreqdesc_rec   "RIB_RTVReqDesc_REC"                         :=  NULL;
   L_rib_rtvreqref_rec    "RIB_RTVReqRef_REC"                          :=  NULL;
   L_rib_routing_rec      RIB_ROUTINGINFO_REC                        :=  NULL;

   L_status_code          VARCHAR2(1)                                :=  NULL;

BEGIN

   O_break_loop := TRUE;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_RTVREQ.FAMILY);
   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   if I_hdr_published = 'N' or I_hdr_published = 'I' then

      if I_hdr_published = 'N' then
         O_message_type := RMSMFM_RTVREQ.HDR_ADD;
      else
         O_message_type := RMSMFM_RTVREQ.DTL_ADD;
      end if;

      -- publish the entire rtv                           -- RtvDesc (all details)
      if MAKE_CREATE(O_error_msg,
                     O_message,
                     O_routing_info,
                     I_rtv_order_no,
                     I_rtv_seq_no,
                     L_max_details,
                     I_rowid,
                     O_keep_queue) = FALSE then
         return FALSE;
      end if;

      -- write the current record (anything but HDR_ADD)
   elsif O_message_type = RMSMFM_RTVREQ.HDR_UPD then         -- RtvDesc (no details)

      if BUILD_HEADER_OBJECT(O_error_msg,
                             L_rib_rtvreqdesc_rec,
                             L_rib_rtvreqref_rec,
                             O_routing_info,
                             I_rtv_order_no) = FALSE then
         return FALSE;
      end if;

      L_rib_rtvreqdesc_rec.rtvreqdtl_tbl   := NULL;
      L_rib_rtvreqref_rec.rtvreqdtlref_tbl := NULL;

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      update rtvreq_pub_info
         set published = 'Y'
       where rtv_order_no = I_rtv_order_no;

      if DELETE_QUEUE_REC(O_error_msg,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;

      O_message := L_rib_rtvreqdesc_rec;

   elsif O_message_type in (RMSMFM_RTVREQ.DTL_ADD, RMSMFM_RTVREQ.DTL_UPD) then       --RtvDesc (one detail)

      if BUILD_HEADER_OBJECT(O_error_msg,
                             L_rib_rtvreqdesc_rec,
                             L_rib_rtvreqref_rec,
                             O_routing_info,
                             I_rtv_order_no) = FALSE then
         return FALSE;
      end if;

      if BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg,
                                     L_rib_rtvreqdesc_rec,
                                     O_message_type,
                                     I_rtv_order_no,
                                     I_rtv_seq_no,
                                     I_rtv_item,
                                     L_max_details) = FALSE then
         return FALSE;
      end if;

      O_message := L_rib_rtvreqdesc_rec;

   elsif O_message_type = RMSMFM_RTVREQ.DTL_DEL then       --RtvDtlRef

      if BUILD_HEADER_OBJECT(O_error_msg,
                             L_rib_rtvreqdesc_rec,
                             L_rib_rtvreqref_rec,
                             O_routing_info,
                             I_rtv_order_no) = FALSE then
         return FALSE;
      end if;

      if BUILD_DETAIL_DELETE_OBJECTS(O_error_msg,
                                     L_rib_rtvreqref_rec,
                                     I_rtv_order_no,
                                     L_max_details) = FALSE then
         return FALSE;
      end if;

      O_message := L_rib_rtvreqref_rec;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RTVREQ.PROCESS_QUEUE_RECORD',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_QUEUE_RECORD;

--------------------------------------------------------------------------------

FUNCTION MAKE_CREATE(O_error_msg     IN OUT         VARCHAR2,
                     O_message       IN OUT nocopy  RIB_OBJECT,
                     O_routing_info  IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                     I_rtv_order_no  IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                     I_rtv_seq_no    IN             RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE,
                     I_max_details   IN             RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE,
                     I_rowid         IN             ROWID,
                     O_keep_queue    IN OUT         BOOLEAN)

RETURN BOOLEAN IS

   L_rib_rtvreqdesc_rec     "RIB_RTVReqDesc_REC"    :=  NULL;
   L_rib_rtvreqdtl_tbl      "RIB_RTVReqDtl_TBL"     :=  NULL;

   L_rtvreq_mfqueue_rowid   ROWID_TBL;
   L_rtvreq_mfqueue_size    BINARY_INTEGER        :=  0;
   
   L_rtv_detail_rowid       ROWID_TBL;
   L_rtv_detail_size        BINARY_INTEGER        :=  0;

   L_rib_routing_rec        RIB_ROUTINGINFO_REC   :=  NULL;
   L_delete_rowid_ind       VARCHAR2(1)           :=  'Y';

   L_rib_rtvreqref_rec      "RIB_RTVReqRef_REC";

   PROGRAM_ERROR            EXCEPTION;

BEGIN

   if BUILD_HEADER_OBJECT( O_error_msg,
                           L_rib_rtvreqdesc_rec,
                           L_rib_rtvreqref_rec,
                           O_routing_info,
                           I_rtv_order_no) = FALSE then
      return FALSE;
   end if;

   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           L_rib_rtvreqdtl_tbl,
                           L_rtvreq_mfqueue_rowid,
                           L_rtvreq_mfqueue_size,
                           L_rtv_detail_rowid,
                           L_rtv_detail_size,
                           L_delete_rowid_ind,
                           NULL,                         --message_type
                           I_rtv_order_no,
                           NULL,
                           I_max_details) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   --- if flag from BUILD_DETAIL_OBJECTS is TRUE, add rowid to L_rtvreq_mfqueue_rowid table;
   if L_delete_rowid_ind = 'Y' then
      L_rtvreq_mfqueue_size := L_rtvreq_mfqueue_size + 1;
      L_rtvreq_mfqueue_rowid(L_rtvreq_mfqueue_size) := I_rowid;
   else
      O_keep_queue := TRUE;
   end if;

   L_rib_rtvreqdesc_rec.rtvreqdtl_tbl := NULL;
   L_rib_rtvreqref_rec.rtvreqdtlref_tbl := NULL;

   update rtvreq_pub_info
      set published = decode(L_delete_rowid_ind, 'Y', 'Y', 'I')
    where rtv_order_no = I_rtv_order_no;

   -- add the detail to the header
   L_rib_rtvreqdesc_rec.rtvreqdtl_tbl := L_rib_rtvreqdtl_tbl;

   if L_rtvreq_mfqueue_size > 0 then
      FORALL i IN 1..L_rtvreq_mfqueue_size
         delete from rtvreq_mfqueue 
          where rowid = L_rtvreq_mfqueue_rowid(i);
   end if;

   if L_rtv_detail_size > 0 then
      FORALL i IN 1..L_rtv_detail_size
         update rtv_detail 
            set publish_ind = 'Y' 
          where rowid = L_rtv_detail_rowid(i);
   end if;
   
   O_message := L_rib_rtvreqdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RTVREQ.MAKE_CREATE',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END MAKE_CREATE;

--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg           IN OUT         VARCHAR2,
                                     O_rib_rtvreqdesc_rec  IN OUT nocopy  "RIB_RTVReqDesc_REC",
                                     I_message_type        IN             RTVREQ_MFQUEUE.MESSAGE_TYPE%TYPE,
                                     I_rtv_order_no        IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                                     I_rtv_seq_no          IN             RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE,
                                     I_rtv_item            IN	          RTVREQ_MFQUEUE.ITEM%TYPE,  		
                                     I_max_details         IN             RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE)
RETURN BOOLEAN IS

   L_rtvreq_mfqueue_rowid   ROWID_TBL;
   L_rtvreq_mfqueue_size    BINARY_INTEGER   :=  0;
   
   L_rtv_detail_rowid       ROWID_TBL;
   L_rtv_detail_size        BINARY_INTEGER   :=  0;

   L_delete_rowid_ind       VARCHAR2(1);

   PROGRAM_ERROR            EXCEPTION;

BEGIN

   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           O_rib_rtvreqdesc_rec.rtvreqdtl_tbl,
                           L_rtvreq_mfqueue_rowid,
                           L_rtvreq_mfqueue_size,
                           L_rtv_detail_rowid,
                           L_rtv_detail_size,
                           L_delete_rowid_ind,
                           I_message_type,
                           I_rtv_order_no,
                           I_rtv_item,
                           I_max_details) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_rtv_detail_size > 0 then
      FORALL i IN 1..L_rtv_detail_size
         update rtv_detail 
            set publish_ind = 'Y' 
          where rowid = L_rtv_detail_rowid(i);
   end if;
   
   if L_rtvreq_mfqueue_size > 0 then
      FORALL i IN 1..L_rtvreq_mfqueue_size
         delete from rtvreq_mfqueue 
          where rowid = L_rtvreq_mfqueue_rowid(i);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RTVREQ.BUILD_DETAIL_CHANGE_OBJECTS',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_CHANGE_OBJECTS;

--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_DELETE_OBJECTS(O_error_msg          IN OUT         VARCHAR2,
                                     O_rib_rtvreqref_rec  IN OUT nocopy  "RIB_RTVReqRef_REC",
                                     I_rtv_order_no       IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                                     I_max_details        IN             RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE)
RETURN BOOLEAN IS

   L_rtvreq_mfqueue_rowid   ROWID_TBL;
   L_rtvreq_mfqueue_size    BINARY_INTEGER      :=  0;

   L_rib_rtvreqdtlref_tbl   "RIB_RTVReqDtlRef_TBL"   :=  NULL;
   L_rib_rtvreqdtlref_rec   "RIB_RTVReqDtlRef_REC"   :=  NULL;

   cursor C_DETAIL_DELETE is
      select rtv_seq_no,
             item,
             rowid
        from rtvreq_mfqueue
       where rtv_order_no = I_rtv_order_no
         and message_type = RMSMFM_RTVREQ.DTL_DEL
         and rownum <= I_max_details;
BEGIN

   L_rib_rtvreqdtlref_tbl := "RIB_RTVReqDtlRef_TBL"();
   L_rib_rtvreqdtlref_rec := "RIB_RTVReqDtlRef_REC"(0, NULL, NULL);

   FOR rec IN C_DETAIL_DELETE LOOP

      L_rib_rtvreqdtlref_rec.seq_no := rec.rtv_seq_no;
      L_rib_rtvreqdtlref_rec.item := rec.item;
      L_rib_rtvreqdtlref_tbl.EXTEND;
      L_rib_rtvreqdtlref_tbl(L_rib_rtvreqdtlref_tbl.COUNT) := L_rib_rtvreqdtlref_rec;

      L_rtvreq_mfqueue_size := L_rtvreq_mfqueue_size + 1;
      L_rtvreq_mfqueue_rowid(L_rtvreq_mfqueue_size) := rec.rowid;

   END LOOP;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_rtvreq_mfqueue_size > 0 then
      FORALL i IN 1..L_rtvreq_mfqueue_size
         delete from rtvreq_mfqueue 
          where rowid = L_rtvreq_mfqueue_rowid(i);
   end if;

   O_rib_rtvreqref_rec.rtvreqdtlref_tbl := L_rib_rtvreqdtlref_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RTVREQ.BUILD_DETAIL_DELETE_OBJECTS',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_DELETE_OBJECTS;

--------------------------------------------------------------------------------

FUNCTION BUILD_HEADER_OBJECT(O_error_msg           IN OUT         VARCHAR2,
                             O_rib_rtvreqdesc_rec  IN OUT nocopy  "RIB_RTVReqDesc_REC",
                             O_rib_rtvreqref_rec   IN OUT nocopy  "RIB_RTVReqRef_REC",
                             O_routing_info        IN OUT nocopy  RIB_ROUTINGINFO_TBL,
                             I_rtv_order_no        IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_supplier             RTV_HEAD.SUPPLIER%TYPE             :=  NULL;
   L_status_ind           RTV_HEAD.STATUS_IND%TYPE           :=  NULL;
   L_store                RTV_HEAD.STORE%TYPE                :=  NULL;
   L_wh                   RTV_HEAD.WH%TYPE                   :=  NULL;
   L_total_order_amt      RTV_HEAD.TOTAL_ORDER_AMT%TYPE      :=  NULL;
   L_ship_to_add_1        RTV_HEAD.SHIP_TO_ADD_1%TYPE        :=  NULL;
   L_ship_to_add_2        RTV_HEAD.SHIP_TO_ADD_2%TYPE        :=  NULL;
   L_ship_to_add_3        RTV_HEAD.SHIP_TO_ADD_3%TYPE        :=  NULL;
   L_ship_to_city         RTV_HEAD.SHIP_TO_CITY%TYPE         :=  NULL;
   L_state                RTV_HEAD.STATE%TYPE                :=  NULL;
   L_ship_to_country_id   RTV_HEAD.SHIP_TO_COUNTRY_ID%TYPE   :=  NULL;
   L_ship_to_pcode        RTV_HEAD.SHIP_TO_PCODE%TYPE        :=  NULL;
   L_ret_auth_num         RTV_HEAD.RET_AUTH_NUM%TYPE         :=  NULL;
   L_courier              RTV_HEAD.COURIER%TYPE              :=  NULL;
   L_freight              RTV_HEAD.FREIGHT%TYPE              :=  NULL;
   L_created_date         RTV_HEAD.CREATED_DATE%TYPE         :=  NULL;
   L_completed_date       RTV_HEAD.COMPLETED_DATE%TYPE       :=  NULL;
   L_restock_pct          RTV_HEAD.RESTOCK_PCT%TYPE          :=  NULL;
   L_restock_cost         RTV_HEAD.RESTOCK_COST%TYPE         :=  NULL;
   L_ext_ref_no           RTV_HEAD.EXT_REF_NO%TYPE           :=  NULL;
   L_comment_desc         RTV_HEAD.COMMENT_DESC%TYPE         :=  NULL;
   L_not_after_date       RTV_HEAD.NOT_AFTER_DATE%TYPE       :=  NULL;
   L_loc_type             VARCHAR2(1)                        :=  NULL;
   L_loc                  NUMBER(10)                         :=  NULL;
   L_physical_loc         NUMBER(10)                         :=  NULL;

   L_rib_routing_rec      RIB_ROUTINGINFO_REC                :=  NULL;

   PROGRAM_ERROR          EXCEPTION;

   cursor C_RTV_HEAD is
      select rh.supplier,
             rh.status_ind,
             rh.store,
             rh.wh,
             rh.total_order_amt,
             rh.ship_to_add_1,
             rh.ship_to_add_2,
             rh.ship_to_add_3,
             rh.ship_to_city,
             rh.state,
             rh.ship_to_country_id,
             rh.ship_to_pcode,
             rh.ret_auth_num,
             rh.courier,
             rh.freight,
             rh.created_date,
             rh.completed_date,
             rh.restock_pct,
             rh.restock_cost,
             rh.ext_ref_no,
             rh.comment_desc,
             rh.not_after_date
        from rtv_head rh
       where rh.rtv_order_no = I_rtv_order_no;

   cursor C_GET_PHY_WH is
      select physical_wh
        from wh
       where wh = L_wh;

BEGIN

   open C_RTV_HEAD;
   fetch C_RTV_HEAD into L_supplier,
                         L_status_ind,
                         L_store,
                         L_wh,
                         L_total_order_amt,
                         L_ship_to_add_1,
                         L_ship_to_add_2,
                         L_ship_to_add_3,
                         L_ship_to_city,
                         L_state,
                         L_ship_to_country_id,
                         L_ship_to_pcode,
                         L_ret_auth_num,
                         L_courier,
                         L_freight,
                         L_created_date,
                         L_completed_date,
                         L_restock_pct,
                         L_restock_cost,
                         L_ext_ref_no,
                         L_comment_desc,
                         L_not_after_date;
   if C_RTV_HEAD%NOTFOUND then
      close C_RTV_HEAD;
      O_error_msg := SQL_LIB.CREATE_MSG('NO_RTVHEAD_PUB',
                                        I_rtv_order_no, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;
   close C_RTV_HEAD;

   if L_wh = -1 then
      L_loc_type     := 'S';
      L_loc          := L_store;
      L_physical_loc := L_store;
   elsif L_store = -1 then
      L_loc_type := 'W';
      L_loc      := L_wh;

      open C_GET_PHY_WH;
      fetch C_GET_PHY_WH into L_physical_loc;
      close C_GET_PHY_WH;
   end if;

   O_rib_rtvreqdesc_rec := "RIB_RTVReqDesc_REC"(0,
                                              I_rtv_order_no,
                                              L_supplier,
                                              L_status_ind,
                                              L_loc,
                                              L_loc_type,
                                              L_physical_loc,
                                              L_total_order_amt,
                                              L_ship_to_add_1,
                                              L_ship_to_add_2,
                                              L_ship_to_add_3,
                                              L_ship_to_city,
                                              L_state,
                                              L_ship_to_country_id,
                                              L_ship_to_pcode,
                                              L_ret_auth_num,
                                              L_courier,
                                              L_freight,
                                              L_created_date,
                                              L_completed_date,
                                              L_restock_pct,
                                              L_restock_cost,
                                              L_ext_ref_no,
                                              L_comment_desc,
                                              NULL,
                                              L_not_after_date);

   O_rib_rtvreqref_rec := "RIB_RTVReqRef_REC"(0,
                                            I_rtv_order_no,
                                            NULL);

   -- create routing info --
   O_routing_info := RIB_ROUTINGINFO_TBL();
   if L_wh = -1 then
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', L_store, 'to_phys_loc_type', L_loc_type, NULL, NULL);
   else
      L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', L_physical_loc, 'to_phys_loc_type', L_loc_type, NULL, NULL);
   end if;
   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RTVREQ.BUILD_HEADER_OBJECT',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_HEADER_OBJECT;

--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg             IN OUT         VARCHAR2,
                              O_message               IN OUT nocopy  "RIB_RTVReqDtl_TBL",
                              O_rtvreq_mfqueue_rowid  IN OUT nocopy  ROWID_TBL,
                              O_rtvreq_mfqueue_size   IN OUT         BINARY_INTEGER,
                              O_rtv_detail_rowid      IN OUT nocopy  ROWID_TBL,
                              O_rtv_detail_size       IN OUT         BINARY_INTEGER,
                              O_delete_rowid_ind      IN OUT         VARCHAR2,
                              I_message_type          IN             RTVREQ_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_rtv_order_no          IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                              I_rtv_item              IN             RTVREQ_MFQUEUE.ITEM%TYPE,		 
                              I_max_details           IN             RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE)
RETURN BOOLEAN IS

   L_rib_rtvreqdtl_rec   "RIB_RTVReqDtl_REC"                          :=  NULL;
   L_rib_rtvreqdtl_tbl   "RIB_RTVReqDtl_TBL"                          :=  NULL;
  
   L_records_found       BOOLEAN                                    :=  FALSE;
   L_details_processed   RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE   :=  0;

   L_table               VARCHAR2(30)                               :=  'RTV_DETAIL';
   L_key1                VARCHAR2(100)                              :=  I_rtv_order_no;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   L_prev_record         RTV_DETAIL.SEQ_NO%TYPE                      :=0;
   L_curr_record         RTV_DETAIL.SEQ_NO%TYPE                      :=0; 
   
   cursor C_RTV_DETAIL_MC is
      select rd.seq_no,
             rd.item,
             rd.shipment,
             rd.inv_status,
             rd.qty_requested,
             rd.unit_cost,
             rd.reason,
             rd.publish_ind,
             rd.rowid rtv_detail_rowid,
             rq.rowid queue_rowid
        from rtv_detail rd,
             rtvreq_mfqueue rq
       where rd.rtv_order_no = I_rtv_order_no
         and rd.rtv_order_no = rq.rtv_order_no(+)
	     and rd.item=rq.item(+)
         and rd.publish_ind  = 'N'
         and NOT EXISTS (select 'x'
                           from item_master im
                          where im.item = rd.item
                            and im.deposit_item_type = 'A')
	     order by rd.seq_no
         for update of rd.publish_ind nowait;

   cursor C_RTV_DETAIL is
      select rd.seq_no,
             rd.item,
             rd.shipment,
             rd.inv_status,
             rd.qty_requested,
             rd.unit_cost,
             rd.reason,
             rd.publish_ind,
             rd.rowid rtv_detail_rowid,
             rq.rowid queue_rowid
        from rtv_detail rd,
             rtvreq_mfqueue rq
       where rd.rtv_order_no = I_rtv_order_no
         and rd.rtv_order_no = rq.rtv_order_no(+)
         and rd.item=I_rtv_item
         and rd.item=rq.item(+)
         and rq.message_type(+) = I_message_type
         and NOT EXISTS (select 'x'
                           from item_master im
                          where im.item = rd.item
                            and im.deposit_item_type = 'A')
         for update of rd.publish_ind nowait;

BEGIN

   L_rib_rtvreqdtl_rec := "RIB_RTVReqDtl_REC"(0,NULL,NULL,NULL,
                                            NULL,NULL,NULL,NULL);
  
   if O_message is NULL then
      L_rib_rtvreqdtl_tbl := "RIB_RTVReqDtl_TBL"();
   else
      L_rib_rtvreqdtl_tbl := O_message;
   end if;

   if I_message_type is NULL then

      FOR rec IN C_RTV_DETAIL_MC LOOP

         L_records_found := TRUE;
         L_curr_record   :=rec.seq_no;
         --only publish when the record exists on the rtv_detail and has not yet been published
         if rec.publish_ind = 'N' or I_message_type is NOT NULL then
            if L_details_processed >= I_max_details then
               O_delete_rowid_ind := 'N';
               EXIT;
            end if;
        if  L_prev_record!=L_curr_record then 
            if BUILD_SINGLE_DETAIL(O_error_msg,
                                   L_rib_rtvreqdtl_tbl,
                                   L_rib_rtvreqdtl_rec,
                                   I_rtv_order_no,
                                   rec.seq_no,
                                   rec.item,
                                   rec.shipment,
                                   rec.inv_status,
                                   rec.qty_requested,
                                   rec.unit_cost,
                                   rec.reason) = FALSE then
               return FALSE;
            end if;
            
            if I_message_type is NULL or I_message_type = RMSMFM_RTVREQ.DTL_ADD then
               O_rtv_detail_size := O_rtv_detail_size + 1;
               O_rtv_detail_rowid(O_rtv_detail_size) := rec.rtv_detail_rowid;
            end if;
            
            L_details_processed := L_details_processed + 1;
         end if;
		 end if;

         if rec.queue_rowid is not NULL then
            O_rtvreq_mfqueue_size := O_rtvreq_mfqueue_size + 1;
            O_rtvreq_mfqueue_rowid(O_rtvreq_mfqueue_size) := rec.queue_rowid;
         end if;
		
		L_prev_record:=L_curr_record;
	  
      END LOOP;

   else

      FOR rec IN C_RTV_DETAIL LOOP

         L_records_found := TRUE;

         if L_details_processed >= I_max_details then
            O_delete_rowid_ind := 'N';
            EXIT;
         end if;

         if BUILD_SINGLE_DETAIL(O_error_msg,
                                L_rib_rtvreqdtl_tbl,
                                L_rib_rtvreqdtl_rec,
                                I_rtv_order_no,
                                rec.seq_no,
                                rec.item,
                                rec.shipment,
                                rec.inv_status,
                                rec.qty_requested,
                                rec.unit_cost,
                                rec.reason) = FALSE then
            return FALSE;
         end if;

         if I_message_type = RMSMFM_RTVREQ.DTL_ADD then
            O_rtv_detail_size := O_rtv_detail_size + 1;                                                        
            O_rtv_detail_rowid(O_rtv_detail_size) := rec.rtv_detail_rowid;
         end if;
         
         if rec.queue_rowid is not NULL then
            O_rtvreq_mfqueue_size := O_rtvreq_mfqueue_size + 1;
            O_rtvreq_mfqueue_rowid(O_rtvreq_mfqueue_size) := rec.queue_rowid;
         end if;

         L_details_processed := L_details_processed + 1;

      END LOOP;

   end if;

   -- if no data found in cursor, raise error
   if not L_records_found then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_RTVDETAIL_PUB',
                                        I_rtv_order_no, NULL, NULL);
      return FALSE;
   end if;

   if L_rib_rtvreqdtl_tbl.COUNT > 0 then
      O_message := L_rib_rtvreqdtl_tbl;
   else
      O_message := NULL;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        NULL);
      return FALSE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RTVREQ.BUILD_DETAIL_OBJECTS',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_OBJECTS;

--------------------------------------------------------------------------------

FUNCTION BUILD_SINGLE_DETAIL(O_error_msg           IN OUT         VARCHAR2,
                             O_message             IN OUT nocopy  "RIB_RTVReqDtl_TBL",
                             IO_rib_rtvreqdtl_rec  IN OUT nocopy  "RIB_RTVReqDtl_REC",
                             I_rtv_order_no        IN             RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE,
                             I_rtv_seq_no          IN             RTVREQ_MFQUEUE.RTV_SEQ_NO%TYPE,
                             I_item                IN             RTV_DETAIL.ITEM%TYPE,
                             I_shipment            IN             RTV_DETAIL.SHIPMENT%TYPE,
                             I_inv_status          IN             RTV_DETAIL.INV_STATUS%TYPE,
                             I_qty_requested       IN             RTV_DETAIL.QTY_REQUESTED%TYPE,
                             I_unit_cost           IN             RTV_DETAIL.UNIT_COST%TYPE,
                             I_reason              IN             RTV_DETAIL.REASON%TYPE)
RETURN BOOLEAN IS

BEGIN

   IO_rib_rtvreqdtl_rec.seq_no       := I_rtv_seq_no;
   IO_rib_rtvreqdtl_rec.item         := I_item;
   IO_rib_rtvreqdtl_rec.shipment     := I_shipment;
   IO_rib_rtvreqdtl_rec.inv_status   := I_inv_status;
   IO_rib_rtvreqdtl_rec.rtv_qty      := I_qty_requested;
   IO_rib_rtvreqdtl_rec.unit_cost    := I_unit_cost;
   IO_rib_rtvreqdtl_rec.reason       := I_reason;

   O_message.EXTEND;
   O_message(O_message.COUNT) := IO_rib_rtvreqdtl_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RTVREQ.BUILD_SINGLE_DETAIL',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_SINGLE_DETAIL;

--------------------------------------------------------------------------------

FUNCTION DELETE_QUEUE_REC(O_error_msg  IN OUT  VARCHAR2,
                          I_seq_no     IN      RTVREQ_MFQUEUE.SEQ_NO%TYPE)
RETURN BOOLEAN IS

BEGIN

   delete from rtvreq_mfqueue
    where seq_no = I_seq_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RTVREQ.DELETE_QUEUE_REC',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_QUEUE_REC;

--------------------------------------------------------------------------------

FUNCTION LOCK_THE_BLOCK(O_error_msg     IN OUT  VARCHAR2,
                        O_queue_locked  IN OUT  BOOLEAN,
                        I_rtv_order_no  IN      RTVREQ_MFQUEUE.RTV_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_table         VARCHAR2(30)                         :=  'RTVREQ_MFQUEUE';
   L_key1          VARCHAR2(100)                        :=  I_rtv_order_no;
   L_key2          VARCHAR2(100)                        :=  NULL;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from rtvreq_mfqueue
       where rtv_order_no = I_rtv_order_no
         for update nowait;

BEGIN

   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      O_queue_locked := TRUE;
      return TRUE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_RTVREQ.LOCK_THE_BLOCK',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_THE_BLOCK;

--------------------------------------------------------------------------------

END RMSMFM_RTVREQ;
/

