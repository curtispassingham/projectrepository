CREATE OR REPLACE PACKAGE STGSVC_FULFILORD AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------------
-- Function Name: POP_CREATE_TABLES
-- Purpose      : This function stages customer order fulfillment create requests
--                in the RIB collection object to interface staging tables.
-----------------------------------------------------------------------------------------------
FUNCTION POP_CREATE_TABLES(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_process_id                 IN OUT   SVC_FULFILORD.PROCESS_ID%TYPE,
                           I_rib_fulfilordcoldesc_rec   IN       "RIB_FulfilOrdColDesc_REC",
                           I_action_type                IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: POP_CANCEL_TABLES
-- Purpose      : This function stages customer order fulfillment cancel requests
--                in the RIB collection object to interface staging tables.
-----------------------------------------------------------------------------------------------
FUNCTION POP_CANCEL_TABLES(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_process_id                IN OUT   SVC_FULFILORDREF.PROCESS_ID%TYPE,
                           I_rib_fulfilordcolref_rec   IN       "RIB_FulfilOrdColRef_REC",
                           I_action_type               IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: CLEANUP_TABLES
-- Purpose      : This is the public function that deletes staged customer order
--                fulfillment create or cancel requests in the interface staging tables.
-----------------------------------------------------------------------------------------------
FUNCTION CLEANUP_TABLES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                        I_action_type     IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END STGSVC_FULFILORD;
/
