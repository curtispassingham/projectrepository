
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ALLOC_CHARGE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name:  DELETE_CHRGS
--Purpose      :  Deletes all the Allocation Charges records for
--                the passed in Allocation or Allocation/To Location combination.
-------------------------------------------------------------------------------
FUNCTION DELETE_CHRGS(O_error_message IN OUT VARCHAR2,
                      I_alloc_no      IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                      I_to_loc        IN     ITEM_LOC.LOC%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_CHRGS
-- Purpose      : Attaches the appropriate Up Charges to an Allocation Detail
--                when the To Location is added to the Allocation.
------------------------------------------------------------------------------------------
FUNCTION DEFAULT_CHRGS(O_error_message IN OUT VARCHAR2,
                       I_alloc_no      IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                       I_from_loc      IN     STORE.STORE%TYPE,
                       I_to_loc        IN     STORE.STORE%TYPE,
                       I_to_loc_type   IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: CHARGES_EXIST
-- Purpose      : This function will check if up charges exist for the given 
--                allocation or allocation/to location combination (if I_to_loc is null). 
------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_alloc_no      IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                       I_to_loc        IN     STORE.STORE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
END ALLOC_CHARGE_SQL;
/
