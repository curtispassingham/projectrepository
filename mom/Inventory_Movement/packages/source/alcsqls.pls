CREATE OR REPLACE PACKAGE ALC_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: CALC_PERCENT_VARIANCE
-- Purpose      : Calculates the percent variance based on the formula:
--                100*((I_base_value - I_compare_value)/I_base_value).
---------------------------------------------------------------------------------------------
FUNCTION CALC_PERCENT_VARIANCE(O_error_message         IN OUT VARCHAR2,
                               O_percent_variance      IN OUT ALC_HEAD.ALC_QTY%TYPE,
                               I_base_value            IN     ALC_COMP_LOC.ACT_VALUE%TYPE,
                               I_compare_value         IN     ALC_COMP_LOC.ACT_VALUE%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: LOC_EXISTS
-- Purpose      : Determines if any Location records exist on the ALC_COMP_LOC table.
--                If a location type and location is passed in, this function will check for
--                its existence on the ALC_COMP_LOC table.
---------------------------------------------------------------------------------------------
FUNCTION LOC_EXISTS  (O_error_message         IN OUT VARCHAR2,
                      O_exists                IN OUT BOOLEAN,
                      I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                      I_seq_no                IN     ALC_HEAD.SEQ_NO%TYPE,
                      I_comp_id               IN     ELC_COMP.COMP_ID%TYPE,
                      I_location              IN     ALC_COMP_LOC.LOCATION%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_LOC_TOTALS
-- Purpose      : Determines the value of the Estimated Location Expense and Actual Location Expense
--                in primary currency based on the passed-in Order, Item, Component and Location.
---------------------------------------------------------------------------------------------
FUNCTION GET_LOC_TOTALS(O_error_message              IN OUT VARCHAR2,
                        O_unit_est_loc_value_prim    IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                        O_unit_act_loc_value_prim    IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                        O_percent_variance           IN OUT NUMBER,
                        I_order_no                   IN     ALC_COMP_LOC.ORDER_NO%TYPE,
                        I_item                       IN     ITEM_MASTER.ITEM%TYPE,
                        I_pack_item                  IN     ITEM_MASTER.ITEM%TYPE,
                        I_seq_no                     IN     ALC_HEAD.SEQ_NO%TYPE,
                        I_comp_id                    IN     ALC_COMP_LOC.COMP_ID%TYPE,
                        I_location                   IN     ALC_COMP_LOC.LOCATION%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_SHIP_LOC_TOTALS
-- Purpose      : Determines the value of the Estimated Location Expense and Actual Location Expense
--                in primary currency based on the passed-in Order, Item, Vessel/Voyage/ETD,Component and Location.
---------------------------------------------------------------------------------------------
FUNCTION GET_SHIP_LOC_TOTALS(O_error_message              IN OUT VARCHAR2,
                             O_unit_est_loc_value_prim    IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                             O_unit_act_loc_value_prim    IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                             O_percent_variance           IN OUT NUMBER,
                             I_order_no                   IN     ALC_COMP_LOC.ORDER_NO%TYPE,
                             I_item                       IN     ITEM_MASTER.ITEM%TYPE,
                             I_pack_item                  IN     ITEM_MASTER.ITEM%TYPE,
                             I_vessel_id                  IN     TRANSPORTATION.VESSEL_ID%TYPE,
                             I_voyage_flt_id              IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                             I_estimated_depart_date      IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                             I_comp_id                    IN     ALC_COMP_LOC.COMP_ID%TYPE,
                             I_location                   IN     ALC_COMP_LOC.LOCATION%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_COMP_TOTALS
-- Purpose      : Determines the Estimated Component Expense and Actual Component Expense in primary currency
--                based on the passed-in Order, Item, Sequence No. and Component.
---------------------------------------------------------------------------------------------
FUNCTION GET_COMP_TOTALS(O_error_message              IN OUT VARCHAR2,
                         O_unit_est_comp_value_prim   IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                         O_unit_act_comp_value_prim   IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                         O_percent_variance           IN OUT NUMBER,
                         I_order_no                   IN     ALC_COMP_LOC.ORDER_NO%TYPE,
                         I_item                       IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item                  IN     ITEM_MASTER.ITEM%TYPE,
                         I_seq_no                     IN     ALC_HEAD.SEQ_NO%TYPE,
                         I_comp_id                    IN     ALC_COMP_LOC.COMP_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_SHIP_COMP_TOTALS
-- Purpose      : Determines the Estimated Component Expense and Actual Component Expense in primary currency
--                based on the passed-in Order, Item, Vessel/Voyage/ETD, Component.
---------------------------------------------------------------------------------------------
FUNCTION GET_SHIP_COMP_TOTALS(O_error_message              IN OUT VARCHAR2,
                              O_unit_est_comp_value_prim   IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                              O_unit_act_comp_value_prim   IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                              O_percent_variance           IN OUT NUMBER,
                              I_order_no                   IN     ALC_COMP_LOC.ORDER_NO%TYPE,
                              I_item                       IN     ITEM_MASTER.ITEM%TYPE,
                              I_pack_item                  IN     ITEM_MASTER.ITEM%TYPE,
                              I_vessel_id                  IN     TRANSPORTATION.VESSEL_ID%TYPE,
                              I_voyage_flt_id              IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                              I_estimated_depart_date      IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                              I_comp_id                    IN     ALC_COMP_LOC.COMP_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_OBLIGATION_TOTALS
-- Purpose      : Determines the Estimated Landed Cost and Actual Landed Cost in primary currency
--                for an Obligation based on the Order No., Item, and Sequence No. combination.
---------------------------------------------------------------------------------------------
FUNCTION GET_OBLIGATION_TOTALS(O_error_message              IN OUT VARCHAR2,
                               O_unit_est_obl_value_prim    IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                               O_unit_act_obl_value_prim    IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                               O_total_est_obl_value_prim   IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                               O_total_act_obl_value_prim   IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                               O_percent_variance           IN OUT NUMBER,
                               I_order_no                   IN     ALC_COMP_LOC.ORDER_NO%TYPE,
                               I_item                       IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_item                  IN     ITEM_MASTER.ITEM%TYPE,
                               I_seq_no                     IN     ALC_HEAD.SEQ_NO%TYPE,
                               I_qty                        IN     ALC_HEAD.ALC_QTY%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_OBLIGATION_UNIT_VALUES
-- Purpose      : Determines the unit value of Estimated Landed Cost and Actual Landed Cost 
--                in primary currency for an Obligation based on the Order No., Item, and 
--                Sequence No. combination. It is used by the ADF ALC Header screen to display
--                the 'Estimates with No Actuals' and 'Values included in ALC by Direct Nomination'.
---------------------------------------------------------------------------------------------
FUNCTION GET_OBLIGATION_UNIT_VALUES(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_unit_est_obl_value_prim    IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                                    O_unit_act_obl_value_prim    IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                                    O_total_est_obl_value_prim   IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                                    O_total_act_obl_value_prim   IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                                    I_order_no                   IN     ALC_COMP_LOC.ORDER_NO%TYPE,
                                    I_item                       IN     ITEM_MASTER.ITEM%TYPE,
                                    I_pack_item                  IN     ITEM_MASTER.ITEM%TYPE,
                                    I_seq_no                     IN     ALC_HEAD.SEQ_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_ORDER_ITEM_TOTALS
-- Purpose      : Calculates the Unit ELC, Unit ALC, Total ELC, Total ALC in primary currency
--                based on the Order, Item, Obligation, Vessel/Voyage/ETD (if shipment records exist)
--                and Sequence No.
---------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_ITEM_TOTALS(O_error_message          IN OUT   VARCHAR2,
                               O_unit_elc_value_prim    IN OUT   NUMBER,
                               O_unit_alc_value_prim    IN OUT   ALC_COMP_LOC.ACT_VALUE%TYPE,
                               O_total_elc_value_prim   IN OUT   NUMBER,
                               O_total_alc_value_prim   IN OUT   ALC_COMP_LOC.ACT_VALUE%TYPE,
                               O_percent_variance       IN OUT   NUMBER,
                               I_order_no               IN       ORDHEAD.ORDER_NO%TYPE,
                               I_shipment               IN       SHIPMENT.SHIPMENT%TYPE,
                               I_item                   IN       ITEM_MASTER.ITEM%TYPE,
                               I_pack_item              IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_SHIP_TOTALS
-- Purpose      : Calculates the Unit ELC, Unit ALC, Total ELC, Total ALC in primary currency
--                based on the Vessel/Voyage/ETD, Order and Item.
---------------------------------------------------------------------------------------------
FUNCTION GET_SHIP_TOTALS(O_error_message              IN OUT VARCHAR2,
                         O_unit_elc_value_prim        IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                         O_unit_alc_value_prim        IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                         O_total_elc_value_prim       IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                         O_total_alc_value_prim       IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                         O_percent_variance           IN OUT NUMBER,
                         I_order_no                   IN     ALC_COMP_LOC.ORDER_NO%TYPE,
                         I_item                       IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item                  IN     ITEM_MASTER.ITEM%TYPE,
                         I_vessel_id                  IN     TRANSPORTATION.VESSEL_ID%TYPE,
                         I_voyage_flt_id              IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                         I_estimated_depart_date      IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CALC_ORD_ITEM_LOC_ALC
-- Purpose      : Calculates the Total ALC in primary currency
--                based on the Order, Item, Pack Item, and Location.  NOTE: The
--                qty received that is passed in must be the qty received for the
--                order/item/location.
---------------------------------------------------------------------------------------------
FUNCTION CALC_ORD_ITEM_LOC_ALC(O_error_message  IN OUT VARCHAR2,
                               O_alc_prim       IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                               I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                               I_item           IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_item      IN     ITEM_MASTER.ITEM%TYPE,
                               I_location       IN     ORDLOC.LOCATION%TYPE,
                               I_unit_cost_prim IN     ORDLOC.UNIT_COST%TYPE,
                               I_qty_received   IN     ORDLOC.QTY_RECEIVED%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_STKLEDGR
-- Purpose      : Inserts into tran data, updates average cost,
--                and updates OTB with the finalized ALC amounts in primary currency
--                based on the Order, Item, Pack Item, and Location.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_STKLEDGR(O_error_message     IN OUT VARCHAR2,
                         I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item         IN     ITEM_MASTER.ITEM%TYPE,
                         I_shipment          IN     SHIPMENT.SHIPMENT%TYPE,
                         I_alc_seq_no        IN     ALC_HEAD.SEQ_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:   GET_STATUS
--Purpose      :   This function retrieves the status of an ALC Header record based on the
--                 passed-in obligation key.
---------------------------------------------------------------------------------------------
FUNCTION GET_STATUS (O_error_message         IN OUT VARCHAR2,
                     O_exists                IN OUT BOOLEAN,
                     O_status                IN OUT OBLIGATION.STATUS%TYPE,
                     I_obligation_key        IN     OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:   GET_STATUS  (overloading with the above function)
--Purpose      :   This function retrieves the status of an ALC Header record
--                 based on the passed-in order/item.
--------------------------------------------------------------------------------
FUNCTION GET_STATUS (O_error_message         IN OUT VARCHAR2,
                     O_status                IN OUT ALC_HEAD.STATUS%TYPE,
                     I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                     I_item                  IN     ORDSKU.ITEM%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:   DELETE_ERRORS
--Purpose      :   This function deletes from if_errors table based a Unit of Measure composed
--                 of the passed-in Order No. and Obligation Key.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_ERRORS (O_error_message         IN OUT VARCHAR2,
                        I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                        I_obligation_key        IN     OBLIGATION.OBLIGATION_KEY%TYPE,
                        I_entry_no              IN     CE_HEAD.ENTRY_NO%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_ALC_ERRORS
-- Purpose      : Checks associated records of Order/Item/Pack records being finalized for
--                errors.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_ALC_ERRORS(O_error_message         IN OUT VARCHAR2,
                          O_exists                IN OUT BOOLEAN,
                          I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                          I_pack_item             IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:   UDPATE_STATUS
--Purpose      :   This function updates the status of an ALC Header record
--                 to the value passed in for the passed-in order/item/pack_item.
--------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message         IN OUT VARCHAR2,
                       I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                       I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                       I_pack_item             IN     ITEM_MASTER.ITEM%TYPE,
                       I_status                IN     ALC_HEAD.STATUS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:   GET_ORDER_TOTALS
--Purpose      : This function will return the total ALC value, the total ELC value, and the
--               percent variance between the ELC and ALC values for a passed order number.
--------------------------------------------------------------------------------
FUNCTION GET_ORDER_TOTALS (O_error_message        IN OUT VARCHAR2,
                           O_total_elc_value_prim IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                           O_total_alc_value_prim IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                           O_percent_variance     IN OUT NUMBER,
                           I_order_no             IN     ORDHEAD.ORDER_NO%TYPE,
                           I_shipment             IN     SHIPMENT.SHIPMENT%type)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name:   DEL_ALC_HEAD
--Purpose      : This function will delete from the ALC tables ALC_COMP_LOC and ALC_HEAD
--               based on the Order No passed as a parameter.
--------------------------------------------------------------------------------
FUNCTION DEL_ALC_HEAD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: GET_ALC_COMP_STATUS
--Purpose      : This function will return the ALC component status ('P'ending, 'F'inalized, 'E'stimated or 'M'ultiple).
--               For stock ledger updating, any status not in 'P'ending is considered 'F'inalized
--               and should not be processed.
--------------------------------------------------------------------------------
FUNCTION GET_ALC_COMP_STATUS(O_error_message    IN OUT VARCHAR2,
                             O_status           IN OUT ALC_HEAD.STATUS%TYPE,
                             I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                             I_item             IN     ITEM_MASTER.ITEM%TYPE,
                             I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                             I_loc              IN     ORDLOC.LOCATION%TYPE,
							 I_comp_id          IN     ELC_COMP.COMP_ID%TYPE,
                             I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                             I_alc_seq_no       IN     ALC_HEAD.SEQ_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name: GET_ALC_ITEM_STATUS
--Purpose      : This function will return the ALC item status ('P'ending or 'F'inalized).
--               For stock ledger updating, any status not in 'P'ending is considered 'F'inalized
--               and should not be processed.
--------------------------------------------------------------------------------
FUNCTION GET_ALC_ITEM_STATUS(O_error_message    IN OUT VARCHAR2,
                             O_status           IN OUT ALC_HEAD.STATUS%TYPE,
                             I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                             I_item             IN     ITEM_MASTER.ITEM%TYPE,
                             I_pack_item        IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name: UNIT_ELC_LOC_TOTALS
--Purpose      : This function calculates the total unit ELC of expense/assessment
--               components for the order/item/location depending on the ALC status.
--               O_unit_elc_estimated represents the total with no ALC entries.
--               O_unit_elc_pending represents the total with only pending (not
--                                  finalized) ALC entries
--               O_unit_elc_finalized represents the total with finalized (or partially
--                                  finalized) ALC entries
--------------------------------------------------------------------------------
FUNCTION UNIT_ELC_LOC_TOTALS(O_error_message         IN OUT VARCHAR2,
                             O_unit_elc_estimated    IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                             O_unit_elc_pending      IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                             O_unit_elc_finalized    IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                             I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                             I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                             I_pack_item             IN     ITEM_MASTER.ITEM%TYPE,
                             I_loc                   IN     ORDLOC.LOCATION%TYPE,
                             I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_shipment              IN     SHIPMENT.SHIPMENT%TYPE,
                             I_alc_seq_no            IN     ALC_HEAD.SEQ_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name: EXT_ALC_LOC_TOTALS
--Purpose      : This function returns the extended values of the 'In ALC',
--               Pending and Finalized ALC totals.
--------------------------------------------------------------------------------
FUNCTION EXT_ALC_LOC_TOTALS(O_error_message         IN OUT VARCHAR2,
                            O_received_qty          IN OUT ALC_COMP_LOC.QTY%TYPE,
                            O_ext_alc_estimated     IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                            O_ext_alc_pending       IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                            O_ext_alc_finalized     IN OUT ALC_COMP_LOC.ACT_VALUE%TYPE,
                            I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                            I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                            I_pack_item             IN     ITEM_MASTER.ITEM%TYPE,
                            I_loc                   IN     ORDLOC.LOCATION%TYPE,
                            I_loc_type              IN     ORDLOC.LOC_TYPE%TYPE,
                            I_shipment              IN     SHIPMENT.SHIPMENT%TYPE,
                            I_alc_seq_no            IN     ALC_HEAD.SEQ_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name: UPDATE_STKLEDGR_LOC
--Purpose      : This function performs the updates to stock ledger, WAC, and OTB.
--               This function does not iterate through pack components.  It is the
--               responsibility of the calling function to iterate through the pack
--               components as necessary.
--------------------------------------------------------------------------------
FUNCTION UPDATE_STKLEDGR_LOC(O_error_message       IN OUT VARCHAR2,
                             I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                             I_item                IN     ITEM_MASTER.ITEM%TYPE,
                             I_loc                 IN     ORDLOC.LOCATION%TYPE,
                             I_loc_type            IN     ORDLOC.LOC_TYPE%TYPE,
                             I_dept                IN     DEPS.DEPT%TYPE,
                             I_class               IN     CLASS.CLASS%TYPE,
                             I_subclass            IN     SUBCLASS.SUBCLASS%TYPE,
                             I_qty_received        IN     ORDLOC.QTY_RECEIVED%TYPE,
                             I_unit_lc_new         IN     ALC_COMP_LOC.ACT_VALUE%TYPE,
                             I_unit_lc_old         IN     ALC_COMP_LOC.ACT_VALUE%TYPE,
                             I_pack_item           IN     ITEM_MASTER.ITEM%TYPE,
                             I_shipment            IN     SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: DETERMINE_STATUS
--Purpose      : This function determines the ALC status of a PO based on the statuses
--               of the ALC_HEAD records for the PO.  This function is called from
--               the V_ALC_HEAD view.
---------------------------------------------------------------------------------------------
FUNCTION DETERMINE_STATUS(I_estimated_ind    IN     VARCHAR2,
                          I_no_finalize_ind  IN     VARCHAR2,
                          I_pending_ind      IN     VARCHAR2,
                          I_processed_ind    IN     VARCHAR2)
  RETURN ALC_HEAD.STATUS%TYPE;
PRAGMA RESTRICT_REFERENCES(DETERMINE_STATUS,WNDS,WNPS);
---------------------------------------------------------------------------------------------
--Function Name: GET_SHIPMENT_ALC_STATUS
--Purpose      : This function retrieves the status of an ALC Header record
--               based on the passed-in shipment and order number.
---------------------------------------------------------------------------------------------
FUNCTION GET_SHIPMENT_ALC_STATUS(O_error_message IN OUT VARCHAR2,                                 
                                 O_alc_status    OUT    VARCHAR2,
                                 I_shipment      IN     SHIPMENT.SHIPMENT%TYPE,
                                 I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
                                 
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: CALC_TOTALS_FINALIZED
--Purpose      : Claculates Estimated Landing cost for Finalized ALC
---------------------------------------------------------------------------------------------   
FUNCTION CALC_TOTALS_FINALIZED(O_error_message     IN OUT VARCHAR2,
                               O_total_elc         OUT    NUMBER,
                               I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                               I_item              IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_item         IN     ITEM_MASTER.ITEM%TYPE,
                               I_location          IN     ORDLOC.LOCATION%TYPE, 
                               I_supplier          IN     SUPS.SUPPLIER%TYPE,
                               I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                               I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                               I_shipment          IN     SHIPMENT.SHIPMENT%TYPE,
                               I_cost              IN     ORDLOC.UNIT_COST%TYPE)
                  
   RETURN BOOLEAN;
   

---------------------------------------------------------------------------------------------
-- Function Name: QTY_CHECK
-- Purpose      : Determines the qty for estimating the ELC/ALC.
---------------------------------------------------------------------------------------------
FUNCTION QTY_CHECK   (O_error_message         IN OUT VARCHAR2,
                      I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                      I_shipment              IN       SHIPMENT.SHIPMENT%TYPE,
                      I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                      I_pack_item             IN       ITEM_MASTER.ITEM%TYPE,
                      L_base_qty              OUT      ORDLOC.QTY_ORDERED%TYPE)
   RETURN BOOLEAN;
   
END;
/
