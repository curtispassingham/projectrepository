CREATE OR REPLACE PACKAGE BODY RMSSUB_STKORD_RECEIPT AS
-------------------------------------------------------------------------------------------------------

LP_system_options_row  SYSTEM_OPTIONS%ROWTYPE;

-------------------------------------------------------------------------------------------------------
-- PRIVATE DECLARATION
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_BOL_LEVEL_RECEIPT(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_valid                 IN OUT   BOOLEAN,
                                   IO_validation_code       IN OUT   VARCHAR2,
                                   IO_shipment              IN OUT   SHIPMENT.SHIPMENT%TYPE,
                                   IO_item_table            IN OUT   STOCK_ORDER_RCV_SQL.ITEM_TAB,
                                   IO_qty_expected_table    IN OUT   STOCK_ORDER_RCV_SQL.QTY_TAB,
                                   IO_inv_status_table      IN OUT   STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                                   IO_carton_table          IN OUT   STOCK_ORDER_RCV_SQL.CARTON_TAB,
                                   IO_distro_no_table       IN OUT   STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                                   IO_tampered_ind_table    IN OUT   STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB,
                                   I_appt                   IN       APPT_HEAD.APPT%TYPE,
                                   I_rib_receipt_rec        IN       "RIB_Receipt_REC")
RETURN BOOLEAN;


-------------------------------------------------------------------------------------------------------
-- PUBLIC
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_status_code       IN OUT   VARCHAR2,
                 O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_appt              IN       APPT_HEAD.APPT%TYPE,
                 I_rib_receipt_rec   IN       "RIB_Receipt_REC")

RETURN BOOLEAN IS

   L_program               VARCHAR2(61) := 'RMSSUB_STKORD_RECEIPT.CONSUME';
   L_valid                 BOOLEAN;
   L_validation_code       VARCHAR2(25);
   L_shipment              SHIPMENT.SHIPMENT%TYPE;
   L_item_table            STOCK_ORDER_RCV_SQL.ITEM_TAB;
   L_qty_expected_table    STOCK_ORDER_RCV_SQL.QTY_TAB;
   L_inv_status_table      STOCK_ORDER_RCV_SQL.INV_STATUS_TAB;
   L_carton_table          STOCK_ORDER_RCV_SQL.CARTON_TAB;
   L_distro_no_table       STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB;
   L_tampered_ind_table    STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB;
   L_ctn_shipment          SHIPMENT.SHIPMENT%TYPE;
   L_ctn_bol_no            SHIPMENT.BOL_NO%TYPE;
   L_ctn_to_loc            SHIPMENT.TO_LOC%TYPE;
   L_wrong_store_ind       VARCHAR2(1);
   L_wrong_store           SHIPMENT.TO_LOC%TYPE;
   L_receiptdtl_TBL        "RIB_ReceiptDtl_TBL" := "RIB_ReceiptDtl_TBL"();
   L_all_overage_cartons   VARCHAR2(1) := 'Y';
   L_index                 BINARY_INTEGER := 0;
   L_non_zero_receipt      BINARY_INTEGER := 0;
   L_receipt_date          DATE := NULL;
   L_document_type         SHIPSKU.DISTRO_TYPE%TYPE;
   L_carton                SHIPSKU.CARTON%TYPE;
   -- Need a local copy since the receipt_type needs to be defaulted if NULL
   L_rib_receipt_rec     "RIB_Receipt_REC" := I_rib_receipt_rec;
   L_exists               VARCHAR2(1) := 'N';
   L_exists_carton        VARCHAR2(1) := 'N';
   L_insert_dup_flag      VARCHAR2(1) := 'N';
   L_count_distro         BINARY_INTEGER := 0;
   L_processed_tsf        VARCHAR2(1) := 'N';

   cursor C_SHIPSKU_TSF(I_carton_id SHIPSKU.CARTON%TYPE) is
      select 'Y'
        from shipsku ss,
             shipment sh
       where ss.shipment = sh.shipment
         and sh.bol_no = I_rib_receipt_rec.asn_nbr
         and nvl(ss.carton,'X') = nvl(I_carton_id,'X')
         and ss.distro_type = 'T';

   cursor C_SHIPSKU_ALLOC(I_carton_id SHIPSKU.CARTON%TYPE) is
      select 'Y'
        from shipsku ss,
             shipment sh
       where ss.shipment = sh.shipment
         and sh.bol_no = I_rib_receipt_rec.asn_nbr
         and nvl(ss.carton,'X') = nvl(I_carton_id,'X')
         and ss.distro_type ='A';

   cursor C_CHECK_VALID_CARTON(I_carton_id SHIPSKU.CARTON%TYPE) is
      select 'Y'
        from shipsku ss,
             shipment sh
       where ss.shipment = sh.shipment
         and sh.bol_no = I_rib_receipt_rec.asn_nbr
         and nvl(ss.carton,'X') = nvl(I_carton_id,'X');

     cursor C_CHECK_CARTON_PROCESS_COUNT(I_carton_id SHIPSKU.CARTON%TYPE) is
      select count(distinct ss.distro_type)
        from shipsku ss,
             shipment sh
       where ss.shipment = sh.shipment
         and sh.bol_no = I_rib_receipt_rec.asn_nbr
         and nvl(ss.carton,'X') = nvl(I_carton_id,'X');
BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;

   -- When interfacing with RDM 10, receipt type will be null.  Only SKU level
   -- receiving is available in RDM 10.  Therefore default receipt type = 'SK'
   if ( L_rib_receipt_rec.receipt_type is NULL ) then
      L_rib_receipt_rec.receipt_type := 'SK';
   end if;

   -- Validate the receipt level info
   if RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_RECEIPT(O_error_message,
                                                   L_valid,
                                                   L_validation_code,
                                                   L_rib_receipt_rec) = FALSE
   or L_valid = FALSE then
      return FALSE;
   end if;

   -- receipt_type BL indicates BOL or carton level receiving,
   -- depending on the existence of carton details.
   if L_rib_receipt_rec.receipt_type = 'BL' then

      if I_rib_receipt_rec.receiptcartondtl_tbl is NOT NULL and
         I_rib_receipt_rec.receiptcartondtl_tbl.COUNT > 0 then
         L_index :=I_rib_receipt_rec.receiptcartondtl_tbl.FIRST;
         if I_rib_receipt_rec.receiptcartondtl_tbl(L_index).container_id is NULL then
            L_index := 0;
         end if;
      else
         L_index := 0;
      end if;
      --- if the carton_id is null then the receiving is being done at the BOL level
      if L_index = 0 then
         -----------------------------------------------------------------------
         -- BOL-level receiving (BL with no carton details)
         -----------------------------------------------------------------------
         if PROCESS_BOL_LEVEL_RECEIPT(O_error_message,
                                      L_valid,
                                      L_validation_code,
                                      L_shipment,
                                      L_item_table,
                                      L_qty_expected_table,
                                      L_inv_status_table,
                                      L_carton_table,
                                      L_distro_no_table,
                                      L_tampered_ind_table,
                                      I_appt,
                                      I_rib_receipt_rec) = FALSE then
            return FALSE;
         end if;
         -- End of BOL-level receiving

      else
         if L_rib_receipt_rec.document_type is NULL then
         -----------------------------------------------------------------------
         -- CARTON-level receiving (BL with carton details)
         -----------------------------------------------------------------------
         for i in L_rib_receipt_rec.receiptcartondtl_tbl.FIRST..L_rib_receipt_rec.receiptcartondtl_tbl.LAST loop
           L_document_type   :='T'; 
           L_insert_dup_flag :='N';
           L_exists          :='N';
            open  C_CHECK_VALID_CARTON(L_rib_receipt_rec.receiptcartondtl_tbl(i).container_id);
            fetch C_CHECK_VALID_CARTON into L_exists_carton;
            close C_CHECK_VALID_CARTON;
            if L_exists_carton ='N' then
               O_error_message := SQL_LIB.CREATE_MSG('INV_CTN',
                                                     L_rib_receipt_rec.receiptcartondtl_tbl(i).container_id,
                                                     NULL,
                                                     NULL);
              return FALSE;
            end if; 
            open  C_CHECK_CARTON_PROCESS_COUNT(L_rib_receipt_rec.receiptcartondtl_tbl(i).container_id);
            fetch C_CHECK_CARTON_PROCESS_COUNT into L_count_distro;
            close C_CHECK_CARTON_PROCESS_COUNT; 
               if L_count_distro = 1 then
                   L_insert_dup_flag :='Y';
               end if;
               FOR j in 1..2 loop
                  if L_processed_tsf = 'N' then
                     open  C_SHIPSKU_TSF(L_rib_receipt_rec.receiptcartondtl_tbl(i).container_id);
                     fetch C_SHIPSKU_TSF into L_exists;
                     close C_SHIPSKU_TSF;
                  end if;

                  if L_exists  ='Y' then
                     if RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_CARTON(O_error_message,
                                                                    L_valid,
                                                                    L_validation_code,
                                                                    L_ctn_shipment,
                                                                    L_ctn_to_loc,
                                                                    L_ctn_bol_no,
                                                                    L_item_table,
                                                                    L_qty_expected_table,
                                                                    L_inv_status_table,
                                                                    L_carton_table,
                                                                    L_distro_no_table,
                                                                    L_tampered_ind_table,
                                                                    L_wrong_store_ind,
                                                                    L_wrong_store,
                                                                    --------------------
                                                                    L_rib_receipt_rec.asn_nbr,
                                                                    L_rib_receipt_rec.dc_dest_id,
                                                                    L_rib_receipt_rec.from_loc,
                                                                    L_rib_receipt_rec.from_loc_type,
                                                                    L_document_type,
                                                                    L_rib_receipt_rec.receiptcartondtl_tbl(i),
                                                                    L_insert_dup_flag) = FALSE then
                        return FALSE;
                     end if;
                    -- Ignore invalid carton if it is a duplicate within the same receipt message.
                    -- If it is invalid for any other reason then return false.
                        if  L_valid = FALSE
                          and L_validation_code != 'DUP_CTN' then
                          return FALSE;
                        end if;
                          -- Only persist valid cartons (not duplicate carton).
                           if L_valid = TRUE then
                              if RMSSUB_STKORD_RECEIPT_SQL.PERSIST_CARTON(O_error_message,
                                                           I_appt,
                                                           L_document_type,
                                                           L_ctn_shipment,
                                                           L_ctn_to_loc,
                                                           L_ctn_bol_no,
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i).receipt_nbr,
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i).to_disposition,
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i).receipt_date,
                                                           L_item_table,
                                                           L_qty_expected_table,
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i).weight,     -- Catch Weight
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i).weight_uom, -- Catch Weight
                                                           L_inv_status_table,
                                                           L_carton_table,
                                                           L_distro_no_table,
                                                           L_tampered_ind_table,
                                                           L_wrong_store_ind,
                                                           L_wrong_store) = FALSE then
                                 return FALSE;
                              end if;
                             -- L_all_overage_cartons is Y when the carton detail contains only overage cartons.
                                 if I_rib_receipt_rec.receiptcartondtl_tbl(i).carton_status_ind in ('D', 'A') then
                                    L_all_overage_cartons := 'N';
                                 end if;
                           end if;
                  end if;
                  L_processed_tsf :='Y';
                  L_exists :='N';
                  open  C_SHIPSKU_ALLOC(L_rib_receipt_rec.receiptcartondtl_tbl(i).container_id);
                  fetch C_SHIPSKU_ALLOC into L_exists;
                  close C_SHIPSKU_ALLOC;
                  if L_exists ='N' then
                  exit;
                  end if;
                  L_document_type :='A';
                     if L_count_distro > 1 and L_exists ='Y' then
                        L_insert_dup_flag:='Y';
                     end if;
             end loop;
         end loop;
         -- All cartons have been processed.  If the carton detail contains only
         -- overage cartons, all cartons will be received in full for the
         -- specified BOL.
         if L_all_overage_cartons = 'Y' then
            if PROCESS_BOL_LEVEL_RECEIPT(O_error_message,
                                         L_valid,
                                         L_validation_code,
                                         L_shipment,
                                         L_item_table,
                                         L_qty_expected_table,
                                         L_inv_status_table,
                                         L_carton_table,
                                         L_distro_no_table,
                                         L_tampered_ind_table,
                                         I_appt,
                                         I_rib_receipt_rec) = FALSE then
               return FALSE;
            end if;
         end if;
      else
         -----------------------------------------------------------------------
         -- CARTON-level receiving (BL with carton details)
         -----------------------------------------------------------------------
         for i in L_rib_receipt_rec.receiptcartondtl_tbl.FIRST..L_rib_receipt_rec.receiptcartondtl_tbl.LAST loop

            if RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_CARTON(O_error_message,
                                                           L_valid,
                                                           L_validation_code,
                                                           L_ctn_shipment,
                                                           L_ctn_to_loc,
                                                           L_ctn_bol_no,
                                                           L_item_table,
                                                           L_qty_expected_table,
                                                           L_inv_status_table,
                                                           L_carton_table,
                                                           L_distro_no_table,
                                                           L_tampered_ind_table,
                                                           L_wrong_store_ind,
                                                           L_wrong_store,
                                                           --------------------
                                                           L_rib_receipt_rec.asn_nbr,
                                                           L_rib_receipt_rec.dc_dest_id,
                                                           L_rib_receipt_rec.from_loc,
                                                           L_rib_receipt_rec.from_loc_type,
                                                           L_rib_receipt_rec.document_type,
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i)) = FALSE then
               return FALSE;
            end if;

            -- Ignore invalid carton if it is a duplicate within the same receipt message.
            -- If it is invalid for any other reason then return false.
            if  L_valid = FALSE
            and L_validation_code != 'DUP_CTN' then
               return FALSE;
            end if;

            -- Only persist valid cartons (not duplicate carton).
            if L_valid = TRUE then
               if RMSSUB_STKORD_RECEIPT_SQL.PERSIST_CARTON(O_error_message,
                                                           I_appt,
                                                           L_rib_receipt_rec.document_type,
                                                           L_ctn_shipment,
                                                           L_ctn_to_loc,
                                                           L_ctn_bol_no,
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i).receipt_nbr,
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i).to_disposition,
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i).receipt_date,
                                                           L_item_table,
                                                           L_qty_expected_table,
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i).weight,     -- Catch Weight
                                                           L_rib_receipt_rec.receiptcartondtl_tbl(i).weight_uom, -- Catch Weight
                                                           L_inv_status_table,
                                                           L_carton_table,
                                                           L_distro_no_table,
                                                           L_tampered_ind_table,
                                                           L_wrong_store_ind,
                                                           L_wrong_store) = FALSE then
                  return FALSE;
               end if;

               -- L_all_overage_cartons is Y when the carton detail contains only overage cartons.
               if I_rib_receipt_rec.receiptcartondtl_tbl(i).carton_status_ind in ('D', 'A') then
                  L_all_overage_cartons := 'N';
               end if;

            end if;

         end loop;

         -- All cartons have been processed.  If the carton detail contains only
         -- overage cartons, all cartons will be received in full for the
         -- specified BOL.
         if L_all_overage_cartons = 'Y' then
            if PROCESS_BOL_LEVEL_RECEIPT(O_error_message,
                                         L_valid,
                                         L_validation_code,
                                         L_shipment,
                                         L_item_table,
                                         L_qty_expected_table,
                                         L_inv_status_table,
                                         L_carton_table,
                                         L_distro_no_table,
                                         L_tampered_ind_table,
                                         I_appt,
                                         I_rib_receipt_rec) = FALSE then
               return FALSE;
            end if;
         end if;

      end if;
      end if;
      -- End of CARTON-level receiving

   elsif L_rib_receipt_rec.receipt_type = 'SK' then

      --------------------------------------------------------------------------
      -- SKU-level receiving
      --------------------------------------------------------------------------
      if  L_rib_receipt_rec.receiptdtl_tbl is NOT NULL
      and L_rib_receipt_rec.receiptdtl_tbl.COUNT > 0 then

         -- 1) Separate items in the message into 2 groups: BTS sellable items
         --    and everything else (L_receiptdtl_TBL).
         -- 2) Find the orderables of the BTS sellables and add them to
         --    L_receiptdtl_TBL. This is the table of receipt details to process.
		 L_receipt_date  := NVL(I_rib_receipt_rec.receiptdtl_tbl(I_rib_receipt_rec.receiptdtl_tbl.FIRST).receipt_date, GET_VDATE);
		 
         if RMSSUB_STKORD_RECEIPT_VALIDATE.GET_ITEMS(O_error_message,
                                                     L_valid,
                                                     L_validation_code,
                                                     L_receiptdtl_tbl,
                                                     L_rib_receipt_rec.receiptdtl_tbl,
                                                     L_rib_receipt_rec.dc_dest_id,
                                                     L_rib_receipt_rec.document_type,
                                                     L_rib_receipt_rec.po_nbr) = FALSE
         or L_valid = FALSE then
            return FALSE;
         end if;

         if  L_receiptdtl_TBL is NOT NULL
         and L_receiptdtl_TBL.COUNT > 0 then

            for i in L_receiptdtl_TBL.FIRST..L_receiptdtl_TBL.LAST loop
				
				if L_receiptdtl_tbl(i).unit_qty != 0 then
					L_non_zero_receipt := 1;
				end if;
				

               if RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_ITEM(O_error_message,
                                                            L_valid,
                                                            L_validation_code,
                                                            I_rib_receipt_rec,
                                                            L_receiptdtl_tbl(i)) = FALSE
               or L_valid = FALSE then
                  return FALSE;
               end if;

               if RMSSUB_STKORD_RECEIPT_SQL.PERSIST_LINE_ITEM(O_error_message,
                                                              L_rib_receipt_rec.dc_dest_id,
                                                              L_rib_receipt_rec.asn_nbr,
                                                              L_rib_receipt_rec.po_nbr,
                                                              L_rib_receipt_rec.document_type,
                                                              I_appt,
                                                              L_receiptdtl_TBL(i)) = FALSE then
                  return FALSE;
               end if;

            end loop;

         end if;  -- L_receiptdtl_TBL is NOT NULL
	 end if;  -- L_rib_receipt_rec.receiptdtl_tbl is NOT NULL
	 
     if ((LP_system_options_row.tsf_auto_close_store = 'N' or LP_system_options_row.tsf_auto_close_wh = 'N') 
			and L_rib_receipt_rec.document_type IN ('T','A','D')
			and L_non_zero_receipt = 0)		then 
         
         if L_rib_receipt_rec.document_type  != 'D' then
            insert into doc_close_queue( doc,
                                         doc_type)
                                 values( L_rib_receipt_rec.po_nbr,
                                         L_rib_receipt_rec.document_type);
         end if;
         
         update shipment
            set status_code = 'R',
                receive_date = DECODE(L_receipt_date,NULL,GET_VDATE,L_receipt_date)

          where bol_no = L_rib_receipt_rec.asn_nbr
            and from_loc = L_rib_receipt_rec.from_loc
            and to_loc = L_rib_receipt_rec.dc_dest_id
            and status_code = 'I';
      end if;  -- L_rib_receipt_rec.receiptdtl_tbl is NULL

      -- End of SKU-level receiving


   end if; -- L_rib_receipt_rec.receipt_type

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONSUME;


--------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
--------------------------------------------------------------------------------
FUNCTION PROCESS_BOL_LEVEL_RECEIPT(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_valid                 IN OUT   BOOLEAN,
                                   IO_validation_code       IN OUT   VARCHAR2,
                                   IO_shipment              IN OUT   SHIPMENT.SHIPMENT%TYPE,
                                   IO_item_table            IN OUT   STOCK_ORDER_RCV_SQL.ITEM_TAB,
                                   IO_qty_expected_table    IN OUT   STOCK_ORDER_RCV_SQL.QTY_TAB,
                                   IO_inv_status_table      IN OUT   STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                                   IO_carton_table          IN OUT   STOCK_ORDER_RCV_SQL.CARTON_TAB,
                                   IO_distro_no_table       IN OUT   STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                                   IO_tampered_ind_table    IN OUT   STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB,
                                   I_appt                   IN       APPT_HEAD.APPT%TYPE,
                                   I_rib_receipt_rec        IN       "RIB_Receipt_REC")
RETURN BOOLEAN IS

   L_program                 VARCHAR2(61)   := 'RMSSUB_STKORD_RECEIPT.PROCESS_BOL_LEVEL_RECEIPT';
   L_receipt_date            DATE           := NULL;
   L_carton                  SHIPSKU.CARTON%TYPE;
   L_duplicate               BOOLEAN;
   L_received_ind            VARCHAR2(1)    := 'N';
   L_carton_fetch_table      STOCK_ORDER_RCV_SQL.CARTON_TAB;

   TYPE carton_duplicate_tbl is table of SHIPSKU.CARTON%TYPE;
   L_carton_dup_table        carton_duplicate_tbl;

   IO_tsf_item_table            STOCK_ORDER_RCV_SQL.ITEM_TAB;
   IO_tsf_qty_expected_table    STOCK_ORDER_RCV_SQL.QTY_TAB;
   IO_tsf_inv_status_table      STOCK_ORDER_RCV_SQL.INV_STATUS_TAB;
   L_tsf_carton_fetch_table     STOCK_ORDER_RCV_SQL.CARTON_TAB;
   IO_tsf_distro_no_table       STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB;
   IO_tsf_tampered_ind_table    STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB;

   IO_alloc_item_table          STOCK_ORDER_RCV_SQL.ITEM_TAB;
   IO_alloc_qty_expected_table  STOCK_ORDER_RCV_SQL.QTY_TAB;
   IO_alloc_inv_status_table    STOCK_ORDER_RCV_SQL.INV_STATUS_TAB;
   L_alloc_carton_fetch_table   STOCK_ORDER_RCV_SQL.CARTON_TAB;
   IO_alloc_distro_no_table     STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB;
   IO_alloc_tampered_ind_table  STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB;

   cursor C_SHIPSKU_TSF is
      select ss.item,
             ss.qty_expected,
             ss.inv_status,
             ss.carton,
             ss.distro_no,
             ss.tampered_ind
        from shipsku ss,
             shipment sh
       where ss.shipment = sh.shipment
         and sh.shipment = IO_shipment
         and sh.bol_no = I_rib_receipt_rec.asn_nbr
         and nvl(ss.carton,'X') = nvl(L_carton,'X')
         and ss.distro_type = 'T';

   cursor C_SHIPSKU_ALLOC is
      select ss.item,
             ss.qty_expected,
             ss.inv_status,
             ss.carton,
             ss.distro_no,
             ss.tampered_ind
        from shipsku ss,
             shipment sh
       where ss.shipment = sh.shipment
         and sh.shipment = IO_shipment
         and sh.bol_no = I_rib_receipt_rec.asn_nbr
         and nvl(ss.carton,'X') = nvl(L_carton,'X')
         and ss.distro_type ='A';

BEGIN

   -- If cartons are present then grab the receipt date from the first one.
   if  I_rib_receipt_rec.receiptcartondtl_tbl IS NOT NULL
   and I_rib_receipt_rec.receiptcartondtl_tbl.COUNT > 0 then
      L_receipt_date := I_rib_receipt_rec.receiptcartondtl_tbl(I_rib_receipt_rec.receiptcartondtl_tbl.FIRST).receipt_date;
   end if;
   ---
   if RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_BOL(O_error_message,
                                               IO_valid,
                                               IO_validation_code,
                                               IO_shipment,
                                               IO_item_table,
                                               IO_qty_expected_table,
                                               IO_inv_status_table,
                                               IO_carton_table,
                                               IO_distro_no_table,
                                               IO_tampered_ind_table,
                                               --------------------
                                               I_rib_receipt_rec.asn_nbr,              -- bol_no
                                               I_rib_receipt_rec.dc_dest_id) = FALSE   -- to_loc
   or IO_valid = FALSE then
      return FALSE;
   end if;
   ---

   -- Clean up the carton duplicate table for each receipt
   if L_carton_dup_table is NULL then
      L_carton_dup_table := carton_duplicate_tbl();
   else
      L_carton_dup_table.DELETE;
   end if;

   if  IO_carton_table is NOT NULL and IO_carton_table.COUNT > 0 then

      for i in IO_carton_table.FIRST..IO_carton_table.LAST loop
         -- Check for carton duplicates in this receipt
         L_duplicate := FALSE;
         ---
         if  L_carton_dup_table IS NOT NULL and L_carton_dup_table.COUNT > 0 then
            for j in L_carton_dup_table.FIRST..L_carton_dup_table.LAST loop
               if IO_carton_table(i) = L_carton_dup_table(j) then
                  L_duplicate := TRUE;
                  exit; -- break out of loop, duplicate has been found
               end if;
            end loop;
         end if;
         ---
         if RMSSUB_STKORD_RECEIPT_VALIDATE.CHECK_BOL_CARTON(O_error_message,
                                                            L_received_ind,
                                                            I_rib_receipt_rec.asn_nbr,
                                                            IO_carton_table(i)) = FALSE then
            return FALSE;
         end if;

         if L_received_ind = 'Y' and LP_system_options_row.duplicate_receiving_ind = 'N' then
            -- If this carton has been received and it is not a duplicate in
            -- this receipt then insert record in receiving_log table.
            if NOT(L_duplicate) then
               if RMSSUB_STKORD_RECEIPT_SQL.PERSIST_INSERT_DUP_RECEIPT(O_error_message,
                                                                       I_rib_receipt_rec.asn_nbr,
                                                                       IO_carton_table(i),
                                                                       NULL,
                                                                       I_rib_receipt_rec.dc_dest_id,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL) = FALSE then
                  return FALSE;
               end if;
            end if;
         elsif (L_received_ind = 'N' and LP_system_options_row.duplicate_receiving_ind = 'N') or
               (LP_system_options_row.duplicate_receiving_ind = 'Y' and NOT(L_duplicate)) then

            L_carton := IO_carton_table(i);
            SQL_LIB.SET_MARK('OPEN', 'C_SHIPSKU_TSF', 'SHIPSKU',
                             'Shipment: '||IO_shipment||', Bol_no: ' ||I_rib_receipt_rec.asn_nbr||', Carton: '||IO_carton_table(i));
            open  C_SHIPSKU_TSF;

            SQL_LIB.SET_MARK('FETCH', 'C_SHIPSKU_TSF', 'SHIPSKU',
                             'Shipment: '||IO_shipment||', Bol_no: ' ||I_rib_receipt_rec.asn_nbr||', Carton: '||IO_carton_table(i));
            fetch C_SHIPSKU_TSF BULK COLLECT into IO_tsf_item_table,
                                                  IO_tsf_qty_expected_table,
                                                  IO_tsf_inv_status_table,
                                                  L_tsf_carton_fetch_table,
                                                  IO_tsf_distro_no_table,
                                                  IO_tsf_tampered_ind_table;

            SQL_LIB.SET_MARK('CLOSE', 'C_SHIPSKU_TSF', 'SHIPSKU',
                             'Shipment: '||IO_shipment||', Bol_no: ' ||I_rib_receipt_rec.asn_nbr||', Carton: '||IO_carton_table(i));
            close C_SHIPSKU_TSF;

            if IO_tsf_item_table IS NOT NULL and IO_tsf_item_table.COUNT > 0 then
               if RMSSUB_STKORD_RECEIPT_SQL.PERSIST_BOL(O_error_message,
                                                        --------------------
                                                        I_appt,
                                                        'T',
                                                        IO_shipment,
                                                        I_rib_receipt_rec.dc_dest_id,  -- to_loc
                                                        I_rib_receipt_rec.asn_nbr,     -- bol_no
                                                        L_receipt_date,
                                                        IO_tsf_item_table,
                                                        IO_tsf_qty_expected_table,
                                                        IO_tsf_inv_status_table,
                                                        L_tsf_carton_fetch_table,
                                                        IO_tsf_distro_no_table,
                                                        IO_tsf_tampered_ind_table) = FALSE then
                  return FALSE;
               end if;
            end if;

            SQL_LIB.SET_MARK('OPEN', 'C_SHIPSKU_ALLOC', 'SHIPSKU',
                             'Shipment: '||IO_shipment||', Bol_no: ' ||I_rib_receipt_rec.asn_nbr||', Carton: '||IO_carton_table(i));
            open  C_SHIPSKU_ALLOC;

            SQL_LIB.SET_MARK('FETCH', 'C_SHIPSKU_ALLOC', 'SHIPSKU',
                             'Shipment: '||IO_shipment||', Bol_no: ' ||I_rib_receipt_rec.asn_nbr||', Carton: '||IO_carton_table(i));
            fetch C_SHIPSKU_ALLOC BULK COLLECT into IO_alloc_item_table,
                                                    IO_alloc_qty_expected_table,
                                                    IO_alloc_inv_status_table,
                                                    L_alloc_carton_fetch_table,
                                                    IO_alloc_distro_no_table,
                                                    IO_alloc_tampered_ind_table;

            SQL_LIB.SET_MARK('CLOSE', 'C_SHIPSKU_ALLOC', 'SHIPSKU',
                             'Shipment: '||IO_shipment||', Bol_no: ' ||I_rib_receipt_rec.asn_nbr||', Carton: '||IO_carton_table(i));
            close C_SHIPSKU_ALLOC;

            if IO_alloc_item_table IS NOT NULL and IO_alloc_item_table.COUNT > 0 then
               if RMSSUB_STKORD_RECEIPT_SQL.PERSIST_BOL(O_error_message,
                                                        --------------------
                                                        I_appt,
                                                        'A',
                                                        IO_shipment,
                                                        I_rib_receipt_rec.dc_dest_id,  -- to_loc
                                                        I_rib_receipt_rec.asn_nbr,     -- bol_no
                                                        L_receipt_date,
                                                        IO_alloc_item_table,
                                                        IO_alloc_qty_expected_table,
                                                        IO_alloc_inv_status_table,
                                                        L_alloc_carton_fetch_table,
                                                        IO_alloc_distro_no_table,
                                                        IO_alloc_tampered_ind_table) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;

         --add both received and unreceived cartons to dup table
         L_carton_dup_table.EXTEND;
         L_carton_dup_table(L_carton_dup_table.COUNT) := IO_carton_table(i);

      end loop;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_BOL_LEVEL_RECEIPT;

--------------------------------------------------------------------------------
END RMSSUB_STKORD_RECEIPT;
/