CREATE OR REPLACE PACKAGE BODY RECEIVE_SQL AS
-----------------------------------------------------------------------------------
-- Name   : VALIDATE_ITEM_LVL_RCPT
-- Purpose: This function will validate shipment records.
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_LVL_RCPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid           IN OUT   BOOLEAN,
                                I_asn_bol_no      IN       SHIPMENT.ASN%TYPE,
                                I_location        IN       SHIPMENT.TO_LOC%TYPE,
                                I_distro_type     IN       SHIPSKU.DISTRO_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(60) := 'RECEIVE_SQL.VALIDATE_ITEM_LVL_RCPT';
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_exists_ind           VARCHAR2(1)  := NULL;

   cursor C_BOL is
      select 'Y'
        from v_shipment
       where asn = I_asn_bol_no;

   cursor C_SHIPMENT is
      select 'Y'
        from v_shipment
       where bol_no = I_asn_bol_no
         and to_loc = I_location;

BEGIN

   O_valid := TRUE;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   open C_BOL;
   fetch C_BOL into L_exists_ind;
   close C_BOL;

   if L_exists_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('SHIP_NO_ASN',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   end if;

   L_exists_ind := NULL;
   open C_SHIPMENT;
   fetch C_SHIPMENT into L_exists_ind;
   close C_SHIPMENT;

   if L_exists_ind is NULL then
      if NOT(L_system_options_row.wrong_st_receipt_ind = 'Y' and I_distro_type in ('A', 'T')) then
         O_error_message := SQL_LIB.CREATE_MSG('BOL_NOT_EXIST',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_valid         := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ITEM_LVL_RCPT;
-----------------------------------------------------------------------------------
-- Name   : VALIDATE_CARTON_LVL_RCPT
-- Purpose: This function will validate shipment records.
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_CARTON_LVL_RCPT(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_valid                IN OUT   BOOLEAN,
                                  O_item_table           IN OUT   STOCK_ORDER_RCV_SQL.ITEM_TAB,
                                  O_qty_expected_table   IN OUT   STOCK_ORDER_RCV_SQL.QTY_TAB,
                                  O_inv_status_table     IN OUT   STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                                  O_carton_table         IN OUT   STOCK_ORDER_RCV_SQL.CARTON_TAB,
                                  O_distro_no_table      IN OUT   STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                                  O_tampered_ind_table   IN OUT   STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB,
                                  O_distro_type_table    IN OUT   RECEIVE_SQL.DISTRO_TYPE_TAB,
                                  O_doc_type             IN OUT   SHIPSKU.DISTRO_TYPE%TYPE,
                                  I_carton               IN       SHIPSKU.CARTON%TYPE,
                                  I_shipment             IN       SHIPMENT.SHIPMENT%TYPE,
                                  I_bol_asn_no           IN       SHIPMENT.BOL_NO%TYPE,
                                  I_to_loc               IN       SHIPMENT.TO_LOC%TYPE)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'RECEIVE_SQL.VALIDATE_CARTON_LVL_RCPT';
   L_invalid_param     VARCHAR2(30) := NULL;
   L_bol_asn_exists    VARCHAR2(1)  := 'N';
   L_tsf_exists        VARCHAR2(1)  := 'N';
   L_alloc_exists      VARCHAR2(1)  := 'N';
   L_bol_to_loc        SHIPMENT.TO_LOC%TYPE;
   L_ctn_bol_asn_no    SHIPMENT.BOL_NO%TYPE;
   L_validation_code   VARCHAR2(255);

   cursor C_CTN_SHIPSKU is
      select item,
             NVL(qty_expected, 0),
             inv_status,
             carton,
             distro_no,
             distro_type,
             tampered_ind
        from v_shipsku
       where shipment = I_shipment
         and carton   = I_carton
       order by distro_type;

   cursor C_GET_BOL_ASN_NO is
      select NVL(bol_no, asn) bol_asn_no
        from v_shipment
       where shipment = I_shipment;

   cursor C_BOL_ASN_EXISTS is
      select 'Y',
             to_loc
        from v_shipment
       where NVL(bol_no, asn) = I_bol_asn_no;

BEGIN
   -- Initialize output parameters.
   O_error_message := NULL;
   O_valid         := TRUE;
   O_doc_type      := NULL;
   --
   O_item_table.DELETE;
   O_qty_expected_table.DELETE;
   O_inv_status_table.DELETE;
   O_carton_table.DELETE;
   O_distro_no_table.DELETE;
   O_tampered_ind_table.DELETE;
   O_distro_type_table.DELETE;

   -- Check for required input
   if I_carton is NULL then
      L_invalid_param := 'I_carton';
   elsif I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   elsif I_bol_asn_no is NULL then
      L_invalid_param := 'I_bol_asn_no';
   elsif I_to_loc is NULL then
      L_invalid_param := 'I_to_loc';
   end if;

   if L_invalid_param is NOT NULL then
      raise PARAM_ERROR;
   end if;

   -- Get all line item info for this carton
   open  C_CTN_SHIPSKU;
   fetch C_CTN_SHIPSKU BULK COLLECT into O_item_table,
                                         O_qty_expected_table,
                                         O_inv_status_table,
                                         O_carton_table,
                                         O_distro_no_table,
                                         O_distro_type_table,
                                         O_tampered_ind_table;
   close C_CTN_SHIPSKU;

   -- check the shipment transaction type contents(pure PO, TSF, Alloc or Mixed Tsf/Alloc
   FOR i in O_distro_type_table.FIRST..O_distro_type_table.LAST LOOP

      if L_tsf_exists = 'N' then
         if O_distro_type_table(i) = 'T' then
            L_tsf_exists := 'Y';
         end if;
      end if;

      if L_alloc_exists = 'N' then
        if O_distro_type_table(i) = 'A' then
           L_alloc_exists := 'Y';
        end if;
      end if;

      if L_tsf_exists = 'Y' and L_alloc_exists = 'Y' then
         O_doc_type := 'M';
      elsif L_tsf_exists = 'Y' and L_alloc_exists = 'N' then
         O_doc_type := 'T';
      elsif L_tsf_exists = 'N' and L_alloc_exists = 'Y' then
         O_doc_type := 'A';
      elsif L_tsf_exists = 'N' and L_alloc_exists = 'N' then
         O_doc_type := 'P';
      end if;

      EXIT when O_doc_type = 'M';

   end LOOP;

   -- Get shipment info for carton
   open  C_GET_BOL_ASN_NO;
   fetch C_GET_BOL_ASN_NO into L_ctn_bol_asn_no;
   close C_GET_BOL_ASN_NO;

   -- Get shipment info for BOL
   open  C_BOL_ASN_EXISTS;
   fetch C_BOL_ASN_EXISTS into L_bol_asn_exists,
                               L_bol_to_loc;
   close C_BOL_ASN_EXISTS;

   if L_bol_asn_exists is NULL or
      L_bol_to_loc != I_to_loc then
      L_validation_code := 'CTN_BOL_LOC_NOT_EXIST';
      raise INVALID_ERROR;
   end if;
   ---
   if L_ctn_bol_asn_no != I_bol_asn_no then
      L_validation_code := 'CTN_BOL_NOT_EXIST';
      raise INVALID_ERROR;
   end if;

   return TRUE;

EXCEPTION
   when INVALID_ERROR then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG(L_validation_code,
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   when PARAM_ERROR then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            L_invalid_param,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_CARTON_LVL_RCPT;
-----------------------------------------------------------------------------------
-- Name   : RECEIVE_ITEMS
-- Purpose: This procedure will perform item level receiving executed by the user through
--          the item receiving form (recitem.fmb).
-----------------------------------------------------------------------------------
PROCEDURE RECEIVE_ITEMS(O_item_receiving_table   IN OUT   RECEIVE_SQL.ITEM_RECV_TABLE,
                        I_shipment               IN       SHIPMENT.SHIPMENT%TYPE,
                        I_distro_content         IN       VARCHAR2,
                        I_location               IN       SHIPMENT.TO_LOC%TYPE,
                        I_asn_bol_no             IN       SHIPMENT.ASN%TYPE,
                        I_order_no               IN       SHIPMENT.ORDER_NO%TYPE,
                        I_receipt_date           IN       SHIPMENT.RECEIVE_DATE%TYPE,
                        I_disposition            IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE)

IS
   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE;
   L_program               VARCHAR2(60)                 := 'RECEIVE_SQL.RECEIVE_ITEMS';
   L_invalid_param         VARCHAR2(30)                 := NULL;
   L_first_tsf             BOOLEAN                      := TRUE;
   L_distro_no             SHIPSKU.DISTRO_NO%TYPE;
   L_first_tsf_distro_no   SHIPSKU.DISTRO_NO%TYPE;
   L_distro_type           SHIPSKU.DISTRO_TYPE%TYPE;
   L_valid                 BOOLEAN;
   -- variables for the break to sell logic
   L_processed_item        BOL_SQL.BOL_SHIPSKU_TBL;
   L_unprocessed_item      BOL_SQL.BOL_SHIPSKU_TBL;
   L_carton                SHIPSKU.CARTON%TYPE;
   L_item                  SHIPSKU.ITEM%TYPE;
   L_ship_qty              SHIPSKU.QTY_EXPECTED%TYPE;
   L_weight                SHIPSKU.WEIGHT_EXPECTED%TYPE;
   L_weight_uom            SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE;
   L_from_loc              ITEM_LOC.LOC%TYPE := NULL;
   L_import_id             ORDHEAD.IMPORT_ID%TYPE;
   L_import_type           ORDHEAD.IMPORT_TYPE%TYPE;

   cursor C_GET_SHIP_FROM_LOC is
      select from_loc
        from v_shipment
       where shipment = I_shipment;

BEGIN
   ---Validate parameters
   if I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   elsif I_distro_content  is NULL then
      L_invalid_param := 'I_distro_content';
   elsif I_location is NULL then
      L_invalid_param := 'I_location';
   elsif I_receipt_date is NULL then
      L_invalid_param := 'I_receipt_date';
   elsif O_item_receiving_table is NULL then
      L_invalid_param := 'O_item_receiving_table';
   end if;

   if L_invalid_param is NOT NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      O_item_receiving_table(1).error_message := L_error_message;
      O_item_receiving_table(1).return_code   := 'FALSE';
      return;
   end if;

   if (I_order_no is NOT NULL AND I_asn_bol_no is NULL) then
      L_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                            'I_order_no'||'I_asn_bol_no',
                                            L_program,
                                            NULL);
      O_item_receiving_table(1).error_message := L_error_message;
      O_item_receiving_table(1).return_code   := 'FALSE';
      return;
   end if;

   if (I_order_no is NULL AND I_distro_content = 'P') then
      L_error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                            'I_order_no'||'I_distro_content',
                                            L_program,
                                            NULL);
      O_item_receiving_table(1).error_message := L_error_message;
      O_item_receiving_table(1).return_code   := 'FALSE';
      return;
   end if;

   -- Get the From Location of the shipment prior to the LOOP of the table type
   open C_GET_SHIP_FROM_LOC;
   fetch C_GET_SHIP_FROM_LOC into L_from_loc;
   close C_GET_SHIP_FROM_LOC;

   if GET_RECV_FIRST_TSF(L_error_message,
                         L_first_tsf_distro_no,
                         I_shipment) = FALSE then
      O_item_receiving_table(1).error_message := L_error_message;
      O_item_receiving_table(1).return_code   := 'FALSE';
      return;
   end if;

   if STOCK_ORDER_RCV_SQL.INIT_TSF_ALLOC_GROUP(L_error_message) = FALSE then
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
   end if;
   ---
   if ORDER_SQL.GET_DEFAULT_IMP_EXP(L_error_message,
                                    L_import_id,
                                    L_import_type,
                                    I_order_no) = FALSE then
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
   end if;
   ---
   if L_import_id is NULL then
      -- LOOP through the records of ITEM_RECV_TABLE
      FOR i in O_item_receiving_table.FIRST..O_item_receiving_table.LAST LOOP

         if I_distro_content = 'A' then
            L_distro_no    := O_item_receiving_table(i).distro_no;
            L_distro_type  := 'A';
         elsif I_distro_content = 'P' then
            L_distro_no    := I_order_no;
            L_distro_type  := 'P';
         elsif I_distro_content in ('M') then
            if O_item_receiving_table(i).distro_no is NULL then
               L_distro_type := 'T';
               L_distro_no   := L_first_tsf_distro_no;
            else -- regular TSF/ALLOC in the Mixed Doc Type
               L_distro_type := O_item_receiving_table(i).distro_type;
               L_distro_no   := O_item_receiving_table(i).distro_no;
            end if;
         elsif I_distro_content = 'T' then
            if O_item_receiving_table(i).distro_no is NULL then
               L_distro_no   := L_first_tsf_distro_no;
            else
               L_distro_no   := O_item_receiving_table(i).distro_no;
            end if;
            L_distro_type  := 'T';
         end if;

         if RECEIVE_SQL.VALIDATE_ITEM_LVL_RCPT(L_error_message,
                                               L_valid,
                                               I_asn_bol_no,
                                               I_location,
                                               L_distro_type) = FALSE then
            raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
         end if;

         if L_distro_type = 'P' then
            if ORDER_RCV_SQL.PO_LINE_ITEM_ONLINE(L_error_message,
                                                 I_location,
                                                 I_order_no,
                                                 O_item_receiving_table(i).item,
                                                 O_item_receiving_table(i).qty_received,
                                                 'R',
                                                 I_receipt_date,
                                                 I_order_no,
                                                 I_asn_bol_no,
                                                 NULL,
                                                 O_item_receiving_table(i).carton,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 O_item_receiving_table(i).inv_status,
                                                 NULL,
                                                 'Y',
                                                 I_shipment,
                                                 O_item_receiving_table(i).weight_received,
                                                 O_item_receiving_table(i).weight_received_uom) = FALSE then
               raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
            end if;

         elsif L_distro_type in ('T', 'A') then

            if L_distro_type = 'A' then
               if STOCK_ORDER_RCV_SQL.ALLOC_LINE_ITEM(L_error_message,                                   --O_error_message
                                                      I_location,                                        --I_loc
                                                      O_item_receiving_table(i).item,                    --I_item
                                                      O_item_receiving_table(i).qty_received,            --I_qty
                                                      O_item_receiving_table(i).weight_received,         --I_weight
                                                      O_item_receiving_table(i).weight_received_uom,     --I_weight_uom
                                                      'R',                                               --I_transaction_type
                                                      I_receipt_date,                                    --I_tran_date
                                                      NULL,                                              --I_receipt_number
                                                      I_asn_bol_no,                                      --I_bol_no
                                                      NULL,                                              --I_appt
                                                      O_item_receiving_table(i).carton,                  --I_carton
                                                      L_distro_type,                                     --I_distro_type
                                                      L_distro_no,                                       --I_distro_number
                                                      O_item_receiving_table(i).inv_status,              --I_disp
                                                      NULL,                                              --I_tampered_ind
                                                      NULL,                                              --I_dummy_carton_ind
                                                      NULL) = FALSE then                                 --I_function_call_ind
                  raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
               end if;
            elsif L_distro_type = 'T' then
               if STOCK_ORDER_RCV_SQL.TSF_LINE_ITEM(L_error_message,                                     --O_error_message
                                                    I_location,                                          --I_loc
                                                    O_item_receiving_table(i).item,                      --I_item
                                                    O_item_receiving_table(i).qty_received,              --I_qty
                                                    O_item_receiving_table(i).weight_received,           --I_weight (should be weight expected, pass here as null)
                                                    O_item_receiving_table(i).weight_received_uom,       --I_weight_uom
                                                    'R',                                                 --I_transaction_type
                                                    I_receipt_date,                                      --I_tran_date
                                                    NULL,                                                --I_receipt_number
                                                    I_asn_bol_no,                                        --I_bol_no
                                                    NULL,                                                --I_appt
                                                    O_item_receiving_table(i).carton,                    --I_carton
                                                    L_distro_type,                                       --I_distro_type
                                                    L_distro_no,                                         --I_distro_number
                                                    O_item_receiving_table(i).inv_status,                --I_disp
                                                    NULL,                                                --I_tampered_ind
                                                    NULL) = FALSE then                                   --I_dummy_carton_ind
                  raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
               end if;
            end if;
         end if;         -- end of transfer/allocation transactions
      end LOOP;          -- end of O_item_receiving_table loop
   else                  -- if import order
      FOR i in O_item_receiving_table.FIRST..O_item_receiving_table.LAST LOOP
         if I_distro_content = 'P' then
            L_distro_no    := I_order_no;
            L_distro_type  := 'P';
         end if;

         if RECEIVE_SQL.VALIDATE_ITEM_LVL_RCPT(L_error_message,
                                               L_valid,
                                               I_asn_bol_no,
                                               I_location,
                                               L_distro_type) = FALSE then
            raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
         end if;
      end LOOP;
      ---
      if L_distro_type = 'P' then
         if ORDER_RCV_SQL.PO_IMPORT_ITEM_ONLINE(L_error_message,
                                                I_location,
                                                I_order_no,
                                                'R',
                                                I_receipt_date,
                                                I_order_no,
                                                I_asn_bol_no,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                I_disposition,
                                                NULL,
                                                'Y',
                                                I_shipment,
                                                O_item_receiving_table) = FALSE then
            raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
         end if;
      end if;
   end if;

   if STOCK_ORDER_RCV_SQL.FINISH_TSF_ALLOC_GROUP(L_error_message) = FALSE then
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
   end if;

   if STOCK_ORDER_RECONCILE_SQL.APPLY_ADJUSTMENTS(L_error_message,
                                                  NULL,
                                                  I_asn_bol_no) = FALSE then
      raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
   end if;

EXCEPTION
   when OTHERS then
      O_item_receiving_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE));
      O_item_receiving_table(1).return_code   := 'FALSE';
   return;

END RECEIVE_ITEMS;
-----------------------------------------------------------------------------------
-- Name   : RECEIVE_CARTON
-- Purpose: This procedure will perform carton level receiving executed by the user
--          through the carton receiving form (reccarton.fmb)
------------------------------------------------------------------------------------
PROCEDURE RECEIVE_CARTON(O_carton_receiving_table   IN OUT   RECEIVE_SQL.CARTON_RECV_TABLE,
                         I_shipment                 IN       SHIPMENT.SHIPMENT%TYPE,
                         I_asn_bol_no               IN       SHIPMENT.ASN%TYPE,
                         I_order_no                 IN       SHIPMENT.ORDER_NO%TYPE,
                         I_to_loc                   IN       SHIPMENT.TO_LOC%TYPE,
                         I_receipt_date             IN       SHIPMENT.RECEIVE_DATE%TYPE)

IS

   L_program                    VARCHAR2(60) := 'RECEIVE_SQL.RECEIVE_CARTON';
   L_invalid_param              VARCHAR2(30) := NULL;
   L_valid                      BOOLEAN;
   L_validation_code            VARCHAR2(1);

   L_ctn_bol_no                 SHIPMENT.BOL_NO%TYPE;
   L_item_table                 STOCK_ORDER_RCV_SQL.ITEM_TAB;
   L_qty_expected_table         STOCK_ORDER_RCV_SQL.QTY_TAB;
   L_inv_status_table           STOCK_ORDER_RCV_SQL.INV_STATUS_TAB;
   L_carton_table               STOCK_ORDER_RCV_SQL.CARTON_TAB;
   L_distro_no_table            STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB;
   L_tampered_ind_table         STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB;
   L_distro_type_table          RECEIVE_SQL.DISTRO_TYPE_TAB;

   L_item_tsf_table             STOCK_ORDER_RCV_SQL.ITEM_TAB;
   L_qty_expected_tsf_table     STOCK_ORDER_RCV_SQL.QTY_TAB;
   L_inv_status_tsf_table       STOCK_ORDER_RCV_SQL.INV_STATUS_TAB;
   L_carton_tsf_table           STOCK_ORDER_RCV_SQL.CARTON_TAB;
   L_distro_no_tsf_table        STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB;
   L_tampered_ind_tsf_table     STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB;

   L_item_alloc_table           STOCK_ORDER_RCV_SQL.ITEM_TAB;
   L_qty_expected_alloc_table   STOCK_ORDER_RCV_SQL.QTY_TAB;
   L_inv_status_alloc_table     STOCK_ORDER_RCV_SQL.INV_STATUS_TAB;
   L_carton_alloc_table         STOCK_ORDER_RCV_SQL.CARTON_TAB;
   L_distro_no_alloc_table      STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB;
   L_tampered_ind_alloc_table   STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB;

   L_doc_type                   SHIPSKU.DISTRO_TYPE%TYPE;
   L_system_options_row         SYSTEM_OPTIONS%ROWTYPE;
   L_mixed_distro_alloc         BOOLEAN := FALSE;
   L_mixed_distro_tsf           BOOLEAN := FALSE;
   L_tsf_ctr                    NUMBER;
   L_alloc_ctr                  NUMBER;

BEGIN

   -- Validate input parameters
   if I_to_loc is NULL then
      L_invalid_param := 'I_to_loc';
   elsif I_asn_bol_no is NULL then
      L_invalid_param := 'I_asn_bol_no';
   elsif I_receipt_date is NULL then
      L_invalid_param := 'I_receipt_date';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_carton_receiving_table(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                       L_invalid_param,
                                                                       L_program,
                                                                       NULL);
      O_carton_receiving_table(1).return_code   := 'FALSE';
      return;
   end if;

   FOR i in O_carton_receiving_table.FIRST..O_carton_receiving_table.LAST LOOP
      if O_carton_receiving_table(i).received_carton_ind = 'Y' then

         if VALIDATE_CARTON_LVL_RCPT(O_carton_receiving_table(i).error_message,
                                     L_valid,
                                     L_item_table,
                                     L_qty_expected_table,
                                     L_inv_status_table,
                                     L_carton_table,
                                     L_distro_no_table,
                                     L_tampered_ind_table,
                                     L_distro_type_table,
                                     L_doc_type,
                                     O_carton_receiving_table(i).container_id,
                                     I_shipment,
                                     I_asn_bol_no,
                                     I_to_loc) = FALSE then
            O_carton_receiving_table(i).return_code   := 'FALSE';
            return;
         end if;

         if (I_order_no is NULL AND L_doc_type = 'P') then
            O_carton_receiving_table(i).error_message := SQL_LIB.CREATE_MSG('INV_NULL_INPUT_VAR',
                                                                            'I_order_no'||'L_doc_type',
                                                                            L_program,
                                                                            NULL);
            O_carton_receiving_table(i).return_code   := 'FALSE';
            return;
         end if;

         if L_valid = TRUE then
            -- if Mixed transations(TSF and ALLOC combos), segregate TSF and ALLOC items
            -- TSF items will be processed by TSF_BOL_CARTON function while ALLOC items
            -- will be sent to ALLOC_BOL_CARTON
            if L_doc_type = 'M' then

               L_item_tsf_table.DELETE; 
               L_item_alloc_table.DELETE; 

               FOR j in L_item_table.FIRST..L_item_table.LAST LOOP
                  if L_distro_type_table(j) = 'T' then

                     L_tsf_ctr := L_item_tsf_table.COUNT + 1;
                     L_item_tsf_table(L_tsf_ctr)         := L_item_table(j);
                     L_qty_expected_tsf_table(L_tsf_ctr) := L_qty_expected_table(j);
                     L_inv_status_tsf_table(L_tsf_ctr)   := L_inv_status_table(j);
                     L_carton_tsf_table(L_tsf_ctr)       := L_carton_table(j);
                     L_distro_no_tsf_table(L_tsf_ctr)    := L_distro_no_table(j);
                     L_tampered_ind_tsf_table(L_tsf_ctr) := L_tampered_ind_table(j);

                     L_mixed_distro_tsf          := TRUE;

                  elsif L_distro_type_table(j) = 'A' then

                     L_alloc_ctr := L_item_alloc_table.COUNT + 1;
                     L_item_alloc_table(L_alloc_ctr)         := L_item_table(j);
                     L_qty_expected_alloc_table(L_alloc_ctr) := L_qty_expected_table(j);
                     L_inv_status_alloc_table(L_alloc_ctr)   := L_inv_status_table(j);
                     L_carton_alloc_table(L_alloc_ctr)       := L_carton_table(j);
                     L_distro_no_alloc_table(L_alloc_ctr)    := L_distro_no_table(j);
                     L_tampered_ind_alloc_table(L_alloc_ctr) := L_tampered_ind_table(j);

                     L_mixed_distro_alloc          := TRUE;
                  end if;
               end LOOP;

               if L_mixed_distro_tsf then
                  if STOCK_ORDER_RCV_SQL.TSF_BOL_CARTON(O_carton_receiving_table(i).error_message,
                                                        NULL,
                                                        I_shipment,
                                                        I_to_loc,
                                                        I_asn_bol_no,
                                                        NULL,
                                                        'ATS',
                                                        I_receipt_date,
                                                        L_item_tsf_table,
                                                        L_qty_expected_tsf_table,
                                                        NULL,
                                                        NULL,
                                                        L_inv_status_tsf_table,
                                                        L_carton_tsf_table,
                                                        L_distro_no_tsf_table,
                                                        L_tampered_ind_tsf_table,
                                                        'N',
                                                        NULL) = FALSE then
                     O_carton_receiving_table(i).return_code   := 'FALSE';
                     return;
                  end if;
               end if;

               if L_mixed_distro_alloc then
                  if STOCK_ORDER_RCV_SQL.ALLOC_BOL_CARTON(O_carton_receiving_table(i).error_message,
                                                          NULL,
                                                          I_shipment,
                                                          I_to_loc,
                                                          I_asn_bol_no,
                                                          NULL,
                                                          'ATS',
                                                          I_receipt_date,
                                                          L_item_alloc_table,
                                                          L_qty_expected_alloc_table,
                                                          NULL,
                                                          NULL,
                                                          L_inv_status_alloc_table,
                                                          L_carton_alloc_table,
                                                          L_distro_no_alloc_table,
                                                          L_tampered_ind_alloc_table,
                                                          'N',
                                                          NULL) = FALSE then
                     O_carton_receiving_table(i).return_code   := 'FALSE';
                     return;
                  end if;
               end if;

            elsif L_doc_type = 'T' then
               if STOCK_ORDER_RCV_SQL.TSF_BOL_CARTON(O_carton_receiving_table(i).error_message,
                                                     NULL,
                                                     I_shipment,
                                                     I_to_loc,
                                                     I_asn_bol_no,
                                                     NULL,
                                                     'ATS',
                                                     I_receipt_date,
                                                     L_item_table,
                                                     L_qty_expected_table,
                                                     NULL,
                                                     NULL,
                                                     L_inv_status_table,
                                                     L_carton_table,
                                                     L_distro_no_table,
                                                     L_tampered_ind_table,
                                                     'N',
                                                     NULL) = FALSE then
                  O_carton_receiving_table(i).return_code   := 'FALSE';
                  return;
               end if;

            elsif L_doc_type = 'A' then
               if STOCK_ORDER_RCV_SQL.ALLOC_BOL_CARTON(O_carton_receiving_table(i).error_message,
                                                       NULL,
                                                       I_shipment,
                                                       I_to_loc,
                                                       I_asn_bol_no,
                                                       NULL,
                                                       'ATS',
                                                       I_receipt_date,
                                                       L_item_table,
                                                       L_qty_expected_table,
                                                       NULL,
                                                       NULL,
                                                       L_inv_status_table,
                                                       L_carton_table,
                                                       L_distro_no_table,
                                                       L_tampered_ind_table,
                                                       'N',
                                                       NULL) = FALSE then
                  O_carton_receiving_table(1).return_code := 'FALSE';
                  return;
               end if;

            elsif L_doc_type = 'P' then
               FOR j in L_item_table.FIRST..L_item_table.LAST LOOP
                  if ORDER_RCV_SQL.PO_LINE_ITEM_ONLINE(O_carton_receiving_table(i).error_message,
                                                       I_to_loc,
                                                       I_order_no,
                                                       L_item_table(j),
                                                       L_qty_expected_table(j),
                                                       'R',
                                                       I_receipt_date,
                                                       I_order_no,
                                                       I_asn_bol_no,
                                                       NULL,
                                                       L_carton_table(j),
                                                       NULL,
                                                       NULL,
                                                       NULL,
                                                       'ATS',
                                                       NULL,
                                                       'Y',
                                                       I_shipment) = FALSE then
                     O_carton_receiving_table(i).return_code   := 'FALSE';
                     return;
                  end if;
               end LOOP;
            end if;   -- L_doc_type
         end if;  -- L_valid
      end if;  -- receive_carton_ind
   end LOOP;

   if STOCK_ORDER_RECONCILE_SQL.APPLY_ADJUSTMENTS(O_carton_receiving_table(1).error_message,
                                                  NULL,
                                                  I_asn_bol_no) = FALSE then
      O_carton_receiving_table(1).return_code := 'FALSE';
      return;
   end if;

EXCEPTION
   when OTHERS then
      O_carton_receiving_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                      SQLERRM,
                                                                      L_program,
                                                                      TO_CHAR(SQLCODE));
      O_carton_receiving_table(1).return_code := 'FALSE';
      return;

END RECEIVE_CARTON;
-----------------------------------------------------------------------------------
-- Name   : FETCH_CARTON_BULK
-- Purpose: This procedure will retrieve all cartons to be received based on the search
--          criteria specified by the reccarton.fmb form.
------------------------------------------------------------------------------------
PROCEDURE FETCH_CARTON_BULK(O_carton_table   IN OUT   RECEIVE_SQL.CARTON_RECV_TABLE,
                            I_shipment       IN       SHIPMENT.SHIPMENT%TYPE)

IS

   L_program         VARCHAR2(60) := 'RECEIVE_SQL.FETCH_CARTON_BULK';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_invalid_param   VARCHAR2(30) := NULL;

   cursor C_GET_CARTONS is
      select distinct carton,
             'N' received_carton_ind,
             NULL,   -- error code
             'TRUE'  -- return code
        from v_shipsku
       where v_shipsku.shipment = I_shipment
         and v_shipsku.status_code != 'R'
         and v_shipsku.carton IS NOT NULL
       order by carton;

BEGIN
   --- Validate parameters
   if I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      O_carton_table(1).error_message := L_error_message;
      O_carton_table(1).return_code   := 'FALSE';
      return;
   end if;

   open C_GET_CARTONS;
   fetch C_GET_CARTONS BULK COLLECT into O_carton_table;
   close C_GET_CARTONS;

   return;

EXCEPTION
   when OTHERS then
      O_carton_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                            SQLERRM,
                                                            L_program,
                                                            TO_CHAR(SQLCODE));
      O_carton_table(1).return_code := 'FALSE';
      return;

END FETCH_CARTON_BULK;
-----------------------------------------------------------------------------------
-- Name   : FETCH_CARTON_ITEMS_BULK
-- Purpose: This procedure will return all item records for a carton and shipment number
--          This will be called by the reccarton.fmb form.
------------------------------------------------------------------------------------
PROCEDURE FETCH_CARTON_ITEMS_BULK(O_carton_details_table   IN OUT   RECEIVE_SQL.CARTON_RECV_ITEM_TABLE,
                                  I_shipment               IN       SHIPMENT.SHIPMENT%TYPE,
                                  I_carton                 IN       SHIPSKU.CARTON%TYPE)

IS

   L_program         VARCHAR2(60) := 'RECEIVE_SQL.FETCH_CARTON_ITEMS_BULK';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_invalid_param   VARCHAR2(30) := NULL;

   cursor C_GET_CARTON_DETAILS is
      select  v.item,
              i.item_desc,
              v.qty_expected,
              NULL,   -- error code
             'TRUE'  -- return code
         from v_shipsku v,
              v_item_master i
        where v.item        = i.item
          and v.shipment    = I_shipment
          and v.status_code != 'R'
          and v.carton      = I_carton;

BEGIN
   --- Validate parameters
   if I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   elsif I_carton is NULL then
      L_invalid_param := 'I_carton';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      O_carton_details_table(1).error_message := L_error_message;
      O_carton_details_table(1).return_code   := 'FALSE';
      return;
   end if;

   open C_GET_CARTON_DETAILS;
   fetch C_GET_CARTON_DETAILS BULK COLLECT into O_carton_details_table;
   close C_GET_CARTON_DETAILS;

   return;

EXCEPTION
   when OTHERS then
      O_carton_details_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE));
      O_carton_details_table(1).return_code := 'FALSE';
      return;

END FETCH_CARTON_ITEMS_BULK;

-----------------------------------------------------------------------------------
-- Name   : FETCH_ITEM_BULK
-- Purpose: This procedure will retrieve item level shipment records from shipsku and
--          item_master tables for the passed in shipment number.
--          The recitem.fmb form will call this function.
------------------------------------------------------------------------------------
PROCEDURE FETCH_ITEM_BULK(O_receive_item_table   IN OUT   RECEIVE_SQL.ITEM_RECV_TABLE,
                          O_distro_contents      IN OUT   VARCHAR2,
                          I_shipment             IN       SHIPMENT.SHIPMENT%TYPE)

IS

   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   L_program           VARCHAR2(60) := 'RECEIVE_SQL.FETCH_ITEM_BULK';
   L_invalid_param     VARCHAR2(30) := NULL;
   L_istrans           BOOLEAN      := FALSE;
   L_isalloc           BOOLEAN      := FALSE;
   L_distro_contents   VARCHAR2(1);


   cursor C_GET_ITEM_RECEIPT_DETAILS is
      select distinct vs.item,
             i.item_desc,
             vs.carton,
             vs.distro_no,
             vs.distro_type,
             vs.qty_expected,
             vs.qty_received,
             i.standard_uom,
             vs.weight_received,
             vs.weight_received_uom,
             NULL,   -- error code
             'TRUE',  -- return code
             NULL   -- inv_status
        from v_shipsku vs,
             item_master i
       where vs.item = i.item
         and vs.status_code != 'R'
         and nvl(i.deposit_item_type,'0') != 'A'
         and vs.shipment = I_shipment
    order by vs.distro_type,
             vs.distro_no;

BEGIN
   -- Validate input parameters
   if I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   end if;

   if L_invalid_param is NOT NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      O_receive_item_table(1).error_message := L_error_message;
      O_receive_item_table(1).return_code   := 'FALSE';
      return;
   end if;

   FOR i in C_GET_ITEM_RECEIPT_DETAILS LOOP
      if i.distro_type = ('T') then
         L_istrans := TRUE;
      elsif i.distro_type = ('A') then
         L_isalloc := TRUE;
      else
         L_distro_contents := 'P';
      end if;
   END LOOP;

   if L_istrans and L_isalloc then
      L_distro_contents     := 'M';
   elsif L_istrans and NOT L_isalloc then
      L_distro_contents     := 'T';
   elsif L_isalloc and NOT L_istrans then
      L_distro_contents     := 'A';
   end if;

   O_distro_contents := L_distro_contents;

   open C_GET_ITEM_RECEIPT_DETAILS;
   fetch C_GET_ITEM_RECEIPT_DETAILS BULK COLLECT into O_receive_item_table;
   close C_GET_ITEM_RECEIPT_DETAILS;

   return;

EXCEPTION
   when OTHERS then
      O_receive_item_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                  SQLERRM,
                                                                  L_program,
                                                                  TO_CHAR(SQLCODE));
      O_receive_item_table(1).return_code := 'FALSE';
      return;

END FETCH_ITEM_BULK;
-----------------------------------------------------------------------------------
FUNCTION LOCK_RECV_SHIPMENT (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_shipment      IN     SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_program       VARCHAR2(60) := 'RECEIVE_SQL.LOCK_RECV_SHIPMENT';

   cursor C_LOCK_SHIPMENT is
      select 'x'
        from shipment sh,
             shipsku sk
       where sh.shipment = sk.shipment
         and sh.shipment = I_shipment
         for update nowait;

BEGIN
   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_shipment,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_LOCK_SHIPMENT;
   close C_LOCK_SHIPMENT;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('SHIPMENT_REC_LOCK',
                                             I_shipment,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_RECV_SHIPMENT;
------------------------------------------------------------------------------------
FUNCTION GET_RECV_FIRST_TSF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_tsf           IN OUT SHIPSKU.DISTRO_NO%TYPE,
                            I_shipment      IN     SHIPSKU.SHIPMENT%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'RECEIVE_SQL.GET_RECV_FIRST_TSF';

   cursor C_GET_TSF is
      select min(s.distro_no)
        from shipsku s
       where s.shipment = I_shipment
         and s.status_code != 'R'
         and s.distro_type = 'T';

BEGIN
   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_shipment,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_GET_TSF;
   fetch C_GET_TSF into O_tsf;
   close C_GET_TSF;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_RECV_FIRST_TSF;
--------------------------------------------------------------------------------
FUNCTION GET_INV_CODES(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists                 IN OUT   BOOLEAN,
                       O_inv_status_code_desc   IN OUT   INV_STATUS_CODES_TL.INV_STATUS_CODE_DESC%TYPE,
                       I_inv_code               IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'INVADJ_SQL.GET_INV_CODES';

   cursor C_INV_STATUS_CODES is
      select inv_status_code_desc
        from v_inv_status_codes_tl
       where inv_status_code = I_inv_code;

BEGIN

   open C_INV_STATUS_CODES;
   fetch C_INV_STATUS_CODES into O_inv_status_code_desc;
   if O_inv_status_code_desc is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_INV_STATUS_CODES;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_INV_CODES;
------------------------------------------------------------------------------------
-- Name   : RETRIEVE_SHIPMENT
-- Purpose: This function will retrieve related shipment details based on the search criteria
--          in item receiving form (recitem.fmb).
-------------------------------------------------------------------------------------------
FUNCTION RETRIEVE_SHIPMENT (
                            I_LOCATION	      IN       SHIPMENT.TO_LOC%TYPE,
                            I_ASN             IN       SHIPMENT.ASN%TYPE,
                            I_BOL_NO          IN       SHIPMENT.BOL_NO%TYPE,
                            I_LOC_TYPE        IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                            I_LOCTYPE_S       IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                            I_LOCTYPE_W       IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                            I_DISTRO_NUMBER   IN       SHIPSKU.DISTRO_NO%TYPE,
                            I_DISTRO_TYPE     IN       SHIPSKU.DISTRO_TYPE%TYPE)
                            
RETURN SHIP_TYPE_TBL PIPELINED IS

   cursor C_SHIPMENT is 
   WITH main_q as
   (
     select /*+ ordered materialize */ distinct vsh1.shipment shipment,
            vsh1.ship_origin ship_origin,
            vsh1.ship_date ship_date,
            vsh1.to_loc_type to_loc_type,
            vsh1.order_no order_no,
            DECODE(I_DISTRO_TYPE,'P',vsh1.order_no,sk.distro_no) distro_no,
            sk.distro_type distro_type
       from v_shipment vsh1,
            shipsku sk
      where vsh1.shipment = sk.shipment
        and sk.status_code  != 'R'
        and vsh1.status_code not in('C','R')
       and vsh1.to_loc = NVL(I_LOCATION, vsh1.to_loc)
       and (vsh1.asn= I_ASN or I_ASN is null)
       and (vsh1.bol_no= I_BOL_NO or I_BOL_NO is null)
       and (vsh1.to_loc_type = I_LOC_TYPE
             or (I_LOC_TYPE IS NULL
                 and vsh1.to_loc_type in(I_LOCTYPE_S ,I_LOCTYPE_W)))   
       )
      select main_q.shipment,main_q.ship_origin,main_q.ship_date
        from main_q
       where I_DISTRO_NUMBER is NULL 
      union all     
      select  main_q.shipment,main_q.ship_origin,main_q.ship_date
        from main_q
      where ((main_q.distro_no  = I_DISTRO_NUMBER
        and main_q.distro_type = I_DISTRO_TYPE
        and I_DISTRO_TYPE in ('A', 'T'))
         or (main_q.order_no  = SUBSTR(I_DISTRO_NUMBER,1,12)
         and I_DISTRO_TYPE = 'P'))
         and I_DISTRO_NUMBER  is NOT NULL;
       
   TYPE SHIPMENT_REC IS RECORD(
        shipment    NUMBER(10),
        ship_origin	VARCHAR2(1),
        ship_date	  DATE);
    
   TYPE SHIP_TBL is TABLE OF SHIPMENT_REC index by binary_integer; 			
     
   L_shipment_rec SHIP_TBL;
  
   L_out SHIP_TYPE:=SHIP_TYPE(null,null,null);  
  
BEGIN
   open C_SHIPMENT;
   LOOP

   fetch C_SHIPMENT BULK COLLECT into L_shipment_rec limit 100; 
   EXIT when L_shipment_rec.count=0;

      FOR I in 1..L_shipment_rec.count LOOP
   
         L_out.shipment     := L_shipment_rec(i).shipment;
         L_out.ship_origin  := L_shipment_rec(i).ship_origin;
         L_out.ship_date    := L_shipment_rec(i).ship_date;
	     
	       PIPE ROW (L_out);
	 
      END LOOP;
   END LOOP;
   
   close C_SHIPMENT;
   return;
   
EXCEPTION
 when OTHERS then
    raise;
END RETRIEVE_SHIPMENT;
------------------------------------------------------------------------------------ 
FUNCTION RECEIVE_ITEMS_LOCK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_shipment        IN       SHIPMENT.SHIPMENT%TYPE,
                            I_distro_number   IN       TSFHEAD.TSF_NO%TYPE
)
RETURN BOOLEAN IS

   L_table_name            varchar2(100);
   L_program   VARCHAR2(50) := 'RECEIVE_SQL.RECEIVE_ITEMS_LOCK';

   RECORD_LOCKED           exception;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);

cursor C_GET_SHIP_ITEM is
      select item
        from shipsku
       where shipment = I_shipment;

cursor C_GET_LOCK (L_item varchar2) is
      select item 
              from item_loc_soh ils
             where ils.item = L_item
              AND  
                     ils.loc in(
                  (select distinct to_loc from shipitem_inv_flow where shipment = I_shipment) union all
                  (select distinct from_loc from shipitem_inv_flow where shipment = I_shipment) union all
                  (select distinct to_loc from shipsku_loc where shipment = I_shipment) union all
                  (select distinct to_loc from tsfhead where tsf_no = I_distro_number and tsf_type <>'EG') union all
                  (select distinct from_loc from tsfhead where tsf_no = I_distro_number and tsf_type <>'EG') union all
                  (select distinct location from ordloc where order_no = I_distro_number)
      
  ) for update nowait;
     
cursor C_GET_SHIP_LOCK is
      select 1 
        from shipment sh, shipsku sk
       where sh.shipment = sk.shipment
         and sh.shipment=I_shipment
       for update nowait;
BEGIN
   ---
   L_table_name := 'ITEM_LOC_SOH';
   for REC_GET_SHIP_ITEM in C_GET_SHIP_ITEM loop
      open C_GET_LOCK(REC_GET_SHIP_ITEM.ITEM);
      close C_GET_LOCK;
   end loop;

   L_table_name := 'SHIPMENT';
      open C_GET_SHIP_LOCK;
      close C_GET_SHIP_LOCK;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then 
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED',
                                            L_table_name,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RECEIVE_ITEMS_LOCK;
------------------------------------------------------------------------------------

FUNCTION ORDLOC_ITEMS_LOCK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE, 
                            I_item            IN       ORDLOC.ITEM%TYPE, 
                            I_loc             IN       ORDLOC.LOCATION%TYPE                            
 ) 
 RETURN BOOLEAN IS 
   
    L_program   VARCHAR2(50) := 'RECEIVE_SQL.ORDLOC_ITEMS_LOCK'; 
   
    RECORD_LOCKED           exception; 
    PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54); 
       
 cursor C_GET_ORDLOC_LOCK is 
       select 1  
         from ordloc ol 
        where ol.order_no   = I_order_no 
          and ol.item       = I_item 
          and ((ol.location = I_loc)  
           or (ol.loc_type  = 'W'  
            and ol.location in(select wh  
                               from wh  
                               where physical_wh = I_loc))) 
        for update nowait; 
 BEGIN 
       open C_GET_ORDLOC_LOCK; 
       close C_GET_ORDLOC_LOCK; 
   
    return TRUE; 
 EXCEPTION 
    when RECORD_LOCKED then 
       if C_GET_ORDLOC_LOCK%ISOPEN then 
          close C_GET_ORDLOC_LOCK; 
       end if; 
       O_error_message := O_error_message||SQL_LIB.CREATE_MSG('ORDLOC_REC_LOCK_3', 
                                             I_order_no, 
                                             I_item, 
                                             I_loc); 
       return FALSE; 
    when OTHERS then 
       if C_GET_ORDLOC_LOCK%ISOPEN then 
          close C_GET_ORDLOC_LOCK; 
       end if; 
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program, 
                                             TO_CHAR(SQLCODE)); 
       return FALSE; 
 END ORDLOC_ITEMS_LOCK; 
------------------------------------------------------------------------------------ 
FUNCTION RECEIVE_CARTON_WRP(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_carton_receiving_table   IN OUT   WRP_CARTON_RECV_TBL,
                            I_shipment                 IN       SHIPMENT.SHIPMENT%TYPE,
                            I_asn_bol_no               IN       SHIPMENT.ASN%TYPE,
                            I_order_no                 IN       SHIPMENT.ORDER_NO%TYPE,
                            I_to_loc                   IN       SHIPMENT.TO_LOC%TYPE,
                            I_receipt_date             IN       SHIPMENT.RECEIVE_DATE%TYPE)
   RETURN NUMBER IS

   L_program_name       VARCHAR2(64) := 'RECEIVE_SQL.RECEIVE_CARTON_WRP';
   L_carton_recv_tbl    RECEIVE_SQL.CARTON_RECV_TABLE;
   j NUMBER := 1;

BEGIN
    
    --copy the data in database type O_carton_receiving_table to PLSQL type L_carton_recv_tbl
    
    if O_carton_receiving_table is not null then
        for i in O_carton_receiving_table.first.. O_carton_receiving_table.last 
        Loop
            L_carton_recv_tbl(j).container_id := O_carton_receiving_table(i).container_id;
            L_carton_recv_tbl(j).received_carton_ind := O_carton_receiving_table(i).received_carton_ind;
            L_carton_recv_tbl(j).error_message := O_carton_receiving_table(i).error_message;
            L_carton_recv_tbl(j).return_code := O_carton_receiving_table(i).return_code;
            j := j+1;
        end loop;
    end if;
    
    if L_carton_recv_tbl is not null then
        RECEIVE_CARTON(L_carton_recv_tbl,
                       I_shipment,
                       I_asn_bol_no,
                       I_order_no,
                       I_to_loc,
                       I_receipt_date);

        for i in L_carton_recv_tbl.first..L_carton_recv_tbl.last loop

            O_carton_receiving_table(i) := WRP_CARTON_RECV_REC(L_carton_recv_tbl(i).container_id, 
                                                               L_carton_recv_tbl(i).received_carton_ind, 
                                                               L_carton_recv_tbl(i).error_message, 
                                                               L_carton_recv_tbl(i).return_code);
        end loop;      
    end if;
    return 1;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
   return 0;
END RECEIVE_CARTON_WRP;
------------------------------------------------------------------------------------
FUNCTION RECEIVE_ITEMS_WRP(
                        O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_item_receiving_table   IN OUT   WRP_ITEM_RECV_TBL,
                        I_shipment               IN       SHIPMENT.SHIPMENT%TYPE,
                        I_distro_content         IN       VARCHAR2,
                        I_location               IN       SHIPMENT.TO_LOC%TYPE,
                        I_asn_bol_no             IN       SHIPMENT.ASN%TYPE,
                        I_order_no               IN       SHIPMENT.ORDER_NO%TYPE,
                        I_receipt_date           IN       SHIPMENT.RECEIVE_DATE%TYPE,
                        I_disposition            IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE)
RETURN NUMBER IS

   L_program_name     VARCHAR2(64) := 'RECEIVE_SQL.RECEIVE_ITEMS_WRP';
   L_item_recv_tbl    RECEIVE_SQL.ITEM_RECV_TABLE;
   j NUMBER := 1;

BEGIN

   --copy the data in database type O_item_receiving_table to PLSQL type L_item_recv_tbl

   if O_item_receiving_table is not null then
      for i in O_item_receiving_table.first.. O_item_receiving_table.last
      Loop
         L_item_recv_tbl(j).item := O_item_receiving_table(i).item;
         L_item_recv_tbl(j).item_desc := O_item_receiving_table(i).item_desc;
         L_item_recv_tbl(j).carton := O_item_receiving_table(i).carton;
         L_item_recv_tbl(j).distro_no := O_item_receiving_table(i).distro_no;
         L_item_recv_tbl(j).distro_type := O_item_receiving_table(i).distro_type;
         L_item_recv_tbl(j).qty_expected := O_item_receiving_table(i).qty_expected;
         L_item_recv_tbl(j).qty_received := O_item_receiving_table(i).qty_received;
         L_item_recv_tbl(j).receipt_unit :=  O_item_receiving_table(i).receipt_unit;
         L_item_recv_tbl(j).weight_received := O_item_receiving_table(i).weight_received;
         L_item_recv_tbl(j).weight_received_uom := O_item_receiving_table(i).weight_received_uom;
         L_item_recv_tbl(j).error_message := O_item_receiving_table(i).error_message;
         L_item_recv_tbl(j).return_code := O_item_receiving_table(i).return_code;
         L_item_recv_tbl(j).inv_status := O_item_receiving_table(i).inv_status;
         j := j+1;
      end loop;
    
      RECEIVE_ITEMS(L_item_recv_tbl,
                    I_shipment,
                    I_distro_content,
                    I_location,
                    I_asn_bol_no,
                    I_order_no,
                    I_receipt_date,
                    I_disposition);

      for i in L_item_recv_tbl.first..L_item_recv_tbl.last loop

          O_item_receiving_table(i) := WRP_ITEM_RECV_REC(L_item_recv_tbl(i).item,
                                                         L_item_recv_tbl(i).item_desc,
                                                         L_item_recv_tbl(i).carton,
                                                         L_item_recv_tbl(i).distro_no,
                                                         L_item_recv_tbl(i).distro_type,
                                                         L_item_recv_tbl(i).qty_expected,
                                                         L_item_recv_tbl(i).qty_received,
                                                         L_item_recv_tbl(i).receipt_unit,
                                                         L_item_recv_tbl(i).weight_received,
                                                         L_item_recv_tbl(i).weight_received_uom,
                                                         L_item_recv_tbl(i).error_message,
                                                         L_item_recv_tbl(i).return_code,
                                                         L_item_recv_tbl(i).inv_status);
      end loop;
   end if;
   
   return 1;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
   return 0;
END RECEIVE_ITEMS_WRP;
------------------------------------------------------------------------------------
FUNCTION INSERT_RECEIVE_ITEMS_GTT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_distro_contents        IN OUT   VARCHAR2,
                          I_shipment               IN       SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN  
IS

   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   L_program           VARCHAR2(60) := 'RECEIVE_SQL.INSERT_RECEIVE_ITEMS_GTT';
   L_invalid_param     VARCHAR2(30) := NULL;
   L_istrans           BOOLEAN      := FALSE;
   L_isalloc           BOOLEAN      := FALSE;
   L_distro_contents   VARCHAR2(1);


   cursor C_GET_ITEM_RECEIPT_DETAILS is
select   vs.distro_type   
        from v_shipsku vs    
         where vs.shipment = I_shipment ;

BEGIN
   -- Validate input parameters
   if I_shipment is NULL then
      L_invalid_param := 'I_shipment';
   end if;

   if L_invalid_param is NOT NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
     
      return false ;
   end if;
  

   FOR i in C_GET_ITEM_RECEIPT_DETAILS LOOP
      if i.distro_type = ('T') then
         L_istrans := TRUE;
      elsif i.distro_type = ('A') then
         L_isalloc := TRUE;
      else
         L_distro_contents := 'P';
      end if;
   END LOOP;

   if L_istrans and L_isalloc then
      L_distro_contents     := 'M';
   elsif L_istrans and NOT L_isalloc then
      L_distro_contents     := 'T';
   elsif L_isalloc and NOT L_istrans then
      L_distro_contents     := 'A';
   end if;

   O_distro_contents := L_distro_contents;

insert into RECEIVE_ITEMS_GTT (
ITEM  
, SEQ_NO 
, ITEM_DESC 
, CARTON 
, DISTRO_NO 
, DISTRO_TYPE 
, QTY_EXPECTED  
, QTY_RECEIVED  
, STANDARD_UOM  
, WEIGHT_RECEIVED  
, WEIGHT_RECEIVED_UOM  
, ERROR_CODE  
, RETURN_CODE  
, INV_STATUS 
, QTY_EXPECTED_UOM  
, QTY_RECIEVED_UOM  
, QTY_RECIEVED_UOM_FOR_DISPLAY )
select  vs.item,     
                vs.seq_no,  
             i.item_desc,       
             vs.carton,       
             vs.distro_no,       
             vs.distro_type,       
             vs.qty_expected,       
             vs.qty_received,       
             i.standard_uom,       
             vs.weight_received,       
             vs.weight_received_uom,       
             NULL error_code,       
             'TRUE' return_code,       
             NULL   inv_status ,      
             i.standard_uom qty_expected_uom,    
             i.standard_uom qty_received_uom ,   
             i.standard_uom qty_received_uom_for_display       
        from v_shipsku vs,       
             v_item_master i       
       where vs.item = i.item       
         and vs.status_code != 'R'       
         and nvl(i.deposit_item_type,'0') != 'A'       
         and vs.shipment = I_shipment;

   return true;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                  SQLERRM,
                                                                  L_program,
                                                                  TO_CHAR(SQLCODE));
    
      return false;

END INSERT_RECEIVE_ITEMS_GTT;
------------------------------------------------------------------------------------
END RECEIVE_SQL;
/
