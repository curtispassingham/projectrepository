CREATE OR REPLACE PACKAGE TSF_CONSTANTS AUTHID CURRENT_USER AS
   SUCCESS                     CONSTANT NUMBER(1)      := 1;
   FAILURE                     CONSTANT NUMBER(1)      := 0;
   --
   YES_IND                     CONSTANT VARCHAR2(1)    := 'Y';
   NO_IND                      CONSTANT VARCHAR2(1)    := 'N';
   --
   LOC_TYPE_S                  CONSTANT VARCHAR2(1)    := 'S';
   LOC_TYPE_W                  CONSTANT VARCHAR2(1)    := 'W';
   STORE_TYPE_C                CONSTANT VARCHAR2(1)    := 'C';
   --
   TSF_TYPE_AD                 CONSTANT VARCHAR2(2)    := 'AD';  --administrative
   TSF_TYPE_CF                 CONSTANT VARCHAR2(2)    := 'CF';  --confirmation
   TSF_TYPE_MR                 CONSTANT VARCHAR2(2)    := 'MR';  --manual requisition
   TSF_TYPE_RAC                CONSTANT VARCHAR2(3)    := 'RAC'; --reallocation transfer
   TSF_TYPE_RV                 CONSTANT VARCHAR2(2)    := 'RV';  --return to vendor
   TSF_TYPE_IC                 CONSTANT VARCHAR2(2)    := 'IC';  --intercompany
   TSF_TYPE_BT                 CONSTANT VARCHAR2(2)    := 'BT';  --book transfer

   TSF_TYPE_CO                 CONSTANT VARCHAR2(2)    := 'CO';  --customer order transfer
   TSF_TYPE_FO                 CONSTANT VARCHAR2(2)    := 'FO';  --franchise order transfer
   TSF_TYPE_FR                 CONSTANT VARCHAR2(2)    := 'FR';  --franchise return transfer
   TSF_TYPE_NS                 CONSTANT VARCHAR2(2)    := 'NS';  --note: 'NS' tsf_type is NOT in the check constraint, therefore no longer valid
   TSF_TYPE_SR                 CONSTANT VARCHAR2(2)    := 'SR';  --store requisition
   TSF_TYPE_PL                 CONSTANT VARCHAR2(2)    := 'PL';  --po-linked transfer
   TSF_TYPE_EG                 CONSTANT VARCHAR2(2)    := 'EG';  --externally generated transfer
   TSF_TYPE_SG                 CONSTANT VARCHAR2(2)    := 'SG';  --system generated transfer
   TSF_TYPE_AIP                CONSTANT VARCHAR2(3)    := 'AIP'; --AIP generated transfer
   TSF_TYPE_SIM                CONSTANT VARCHAR2(3)    := 'SIM'; --SIM generated transfer
   --
   PARTNER_TYPE_E              CONSTANT VARCHAR2(1)    := 'E';
   --
   CODE_TYPE_TRST              CONSTANT VARCHAR2(4)    := 'TRST';
   CODE_TYPE_TR4E              CONSTANT VARCHAR2(4)    := 'TR4E';
   CODE_TYPE_TR2E              CONSTANT VARCHAR2(4)    := 'TR2E';
   CODE_TYPE_TRS1              CONSTANT VARCHAR2(4)    := 'TRS1';
   --
   FINISHER_TYPE_E             CONSTANT VARCHAR2(1)    := 'E';
   FINISHER_TYPE_I             CONSTANT VARCHAR2(1)    := 'I';
   --
   TSF_STATUS_A                CONSTANT VARCHAR2(1)    := 'A';   --approved
   TSF_STATUS_B                CONSTANT VARCHAR2(1)    := 'B';   --submitted
   TSF_STATUS_I                CONSTANT VARCHAR2(1)    := 'I';   --input
   TSF_STATUS_C                CONSTANT VARCHAR2(1)    := 'C';   --closed
   TSF_STATUS_D                CONSTANT VARCHAR2(1)    := 'D';   --deleted
   TSF_STATUS_S                CONSTANT VARCHAR2(1)    := 'S';   --shipped
   --
   ADD_IND                     CONSTANT VARCHAR2(1)    := 'A';
   --
   TSF_ENTITY_TYPE_A           CONSTANT VARCHAR2(1)    := 'A';   --intracompany
   TSF_ENTITY_TYPE_E           CONSTANT VARCHAR2(1)    := 'E';   --intercompany

END TSF_CONSTANTS;
/

