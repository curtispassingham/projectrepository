
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_DSRCPT AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PUBLIC VARIABLES
--------------------------------------------------------------------------------

DSRCPT_ADD   CONSTANT VARCHAR2(30) := 'dsrcptcre';


--------------------------------------------------------------------------------
-- PUBLIC RECORD TYPES
--------------------------------------------------------------------------------

TYPE DSRCPT_REC_TYPE IS RECORD
(
trans_date   DATE,
item         ITEM_MASTER.ITEM%TYPE,
store        NUMBER,
units        NUMBER,
unit_cost    NUMBER,
--
pack_ind     ITEM_MASTER.PACK_IND%TYPE,
dept         ITEM_MASTER.DEPT%TYPE,
class        ITEM_MASTER.CLASS%TYPE,
subclass     ITEM_MASTER.SUBCLASS%TYPE
);


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

PROCEDURE CONSUME(O_status_code   IN OUT VARCHAR2,
                  O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message       IN     RIB_OBJECT,
                  I_message_type  IN     VARCHAR2);


END RMSSUB_DSRCPT;
/
