CREATE OR REPLACE PACKAGE BODY TRANSFER_COST_SQL AS

TYPE tsf_item_cost_id_TBL  is table of TSF_ITEM_COST.TSF_ITEM_COST_ID%TYPE  INDEX BY BINARY_INTEGER;
TYPE tsf_no_TBL            is table of TSFHEAD.TSF_NO%TYPE                  INDEX BY BINARY_INTEGER;
TYPE item_TBL              is table of ITEM_MASTER.ITEM%TYPE                INDEX BY BINARY_INTEGER;
TYPE tsf_qty_TBL           is table of TSFDETAIL.TSF_QTY%TYPE               INDEX BY BINARY_INTEGER;
TYPE tsf_price_TBL         is table of TSFDETAIL.TSF_PRICE%TYPE             INDEX BY BINARY_INTEGER;

-- All indicator variables are VARCHAR2(1).
TYPE ind_TBL               is table of ITEM_MASTER.PACK_IND%TYPE            INDEX BY BINARY_INTEGER;

TYPE activity_id_TBL       is table of WO_ACTIVITY.ACTIVITY_ID%TYPE         INDEX BY BINARY_INTEGER;
TYPE unit_cost_TBL         is table of WO_ACTIVITY.UNIT_COST%TYPE           INDEX BY BINARY_INTEGER;

-- Global variables
LP_tsf_no                TSFHEAD.TSF_NO%TYPE;
LP_tsf_parent_no         TSFHEAD.TSF_PARENT_NO%TYPE;
LP_from_loc              TSFHEAD.FROM_LOC%TYPE;
LP_ict_leg               TSF_ITEM_COST.LEG_IND%TYPE;

-- Item transformation variables
P_from_items             item_TBL;
P_to_items               item_TBL;

-- Item transformation sum of to_item_qty variables
P_sum_to_items           item_TBL;
P_sum_to_items_qty       tsf_qty_TBL;

-- Tsfdetail fetch variables
P_fetch_item             item_TBL;
P_fetch_pack_ind         ind_TBL;
P_fetch_tsf_price        tsf_price_TBL;
P_fetch_tsf_qty          tsf_qty_TBL;

-- Work order fetch variables
P_wo_fetch_item          item_TBL;
P_wo_fetch_activity_id   activity_id_TBL;
P_wo_fetch_unit_cost     unit_cost_TBL;

-- Array to populate the tsf_item_cost table
P_tic_tsf_item_cost_id   tsf_item_cost_id_TBL;
P_tic_tsf_no             tsf_no_TBL;
P_tic_tsf_parent_no      tsf_no_TBL;
P_tic_item               item_TBL;
P_tic_tsf_qty            tsf_qty_TBL;
P_tic_tsf_avg_price      tsf_price_TBL;
P_tic_xform_to_item      item_TBL;
P_tic_leg_ind            ind_TBL;
P_tic_ict_leg_ind        ind_TBL;
P_tic_size               NUMBER;

-- Array to populate the tsf_item_wo_cost table
P_tiwc_tsf_item_cost_id  tsf_item_cost_id_TBL;
P_tiwc_activity_id       activity_id_TBL;
P_tiwc_avg_unit_cost     unit_cost_TBL;
P_tiwc_size              NUMBER;


---------------------------------------------------------------------------------------------
-- Function Name: GET_BULK_FETCH
-- Purpose      : Bulk collect all the items, transformations, and work orders that
--                apply to this transfer
---------------------------------------------------------------------------------------------
FUNCTION GET_BULK_FETCH(O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ITEM
-- Purpose      : Populate the table arrays based on the item entered.  If called from
--                PROCESS_PACK, pack_no and the percent in pack value will be passed in.
--                These values will be used to prorate workorder activity costs and tsf_price.
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM(O_error_message IN OUT VARCHAR2,
                      I_item          IN     ITEM_MASTER.ITEM%TYPE,
                      I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                      I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_tsf_price     IN     TSFDETAIL.TSF_PRICE%TYPE,
                      I_pct_in_pack   IN     NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PROCESS_PACK
-- Purpose      : For each component in the pack, determine the percent in pack value and
--                call PROCESS_ITEM.
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_PACK(O_error_message IN OUT VARCHAR2,
                      I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                      I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_tsf_price     IN     TSFDETAIL.TSF_PRICE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: ADD_TO_TIC_ARRAY
-- Purpose      : Add a new item to the array that will be inserted into tsf_item_cost.
---------------------------------------------------------------------------------------------
FUNCTION ADD_TO_TIC_ARRAY(O_error_message IN OUT VARCHAR2,
                          I_item          IN     ITEM_MASTER.ITEM%TYPE,
                          I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                          I_tsf_price     IN     TSFDETAIL.TSF_PRICE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: ADD_TO_TIC_ARRAY_LEG_1
-- Purpose      : Add a new item to the array that will be inserted into tsf_item_cost.
---------------------------------------------------------------------------------------------
FUNCTION ADD_TO_TIC_ARRAY_LEG_1(O_error_message IN OUT VARCHAR2,
                                I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                I_xform_item    IN     ITEM_MASTER.ITEM%TYPE,
                                I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                                I_tsf_price     IN     TSFDETAIL.TSF_PRICE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_TIC_ARRAY
-- Purpose      : Update the array that will be inserted into tsf_item_cost.  Average the
--                tsf_price across the total quantity.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_TIC_ARRAY(O_error_message IN OUT VARCHAR2,
                          I_index         IN     NUMBER,
                          I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                          I_tsf_price     IN     TSFDETAIL.TSF_PRICE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PROCESS_WO_ACTIVITIES
-- Purpose      : Process any workorder activities that affect the item
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_WO_ACTIVITIES(O_error_message IN OUT VARCHAR2,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE,
                               I_tic_index     IN     NUMBER,
                               I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                               I_old_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                               I_pct_in_pack   IN     NUMBER,
                               I_sum_xform_qty IN     TSFDETAIL.TSF_QTY%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: ADD_TO_TIWC_ARRAY
-- Purpose      : Add a new item to the array that will be inserted into tsf_item_wo_cost.
---------------------------------------------------------------------------------------------
FUNCTION ADD_TO_TIWC_ARRAY(O_error_message IN OUT VARCHAR2,
                           I_tic_index     IN     NUMBER,
                           I_activity_id   IN     WO_ACTIVITY.ACTIVITY_ID%TYPE,
                           I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                           I_unit_cost     IN     WO_ACTIVITY.UNIT_COST%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_TIWC_ARRAY
-- Purpose      : Update the array that will be inserted into tsf_item_wo_cost.  Average the
--                unit_cost across the total quantity.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_TIWC_ARRAY(O_error_message IN OUT VARCHAR2,
                           I_index         IN     NUMBER,
                           I_tic_index     IN     NUMBER,
                           I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                           I_old_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                           I_unit_cost     IN     WO_ACTIVITY.UNIT_COST%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CALC_NEW_AVG
-- Purpose      : The function calculates the unit cost based on the averages calculated thus
--                far.  For a new activity it calculates in one of two ways.  Either an average
--                based on either the tsf qty, or a weighted average based on the sum of the
--                transformation quantities and the item qty.
--                   For and existing item / activity id the average is re-calculated based on the
--                existing transfer qty and the additional qty.
---------------------------------------------------------------------------------------------
FUNCTION CALC_NEW_AVG(O_error_message      IN OUT VARCHAR2,
                      O_new_avg_unit_cost  IN OUT WO_ACTIVITY.UNIT_COST%TYPE,
                      I_tic_index          IN     NUMBER,
                      I_tiwc_index         IN     NUMBER,
                      I_tsf_qty            IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_old_qty            IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_activity_unit_cost IN     WO_ACTIVITY.UNIT_COST%TYPE,
                      I_sum_xform_qty      IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_pct_in_pack        IN     NUMBER)

   return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CONVERT_CURRENCY
-- Purpose      : Convert the tsf_item_cost.tsf_avg_price and tsf_item_wo_cost.avg_unit_cost
--                to the to_loc currency.
---------------------------------------------------------------------------------------------
FUNCTION CONVERT_CURRENCY(O_error_message IN OUT VARCHAR2,
                          I_from_loc_type IN     TSFHEAD.FROM_LOC_TYPE%TYPE,
                          I_to_loc        IN     TSFHEAD.TO_LOC%TYPE,
                          I_to_loc_type   IN     TSFHEAD.TO_LOC_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: FLUSH_ARRAYS
-- Purpose      : Insert the arrayed values into the tsf_item_cost and tsf_item_wo_cost tables
---------------------------------------------------------------------------------------------
FUNCTION FLUSH_ARRAYS(O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION PCT_IN_PACK(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_pct_in_pack   IN OUT NUMBER,
                     I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                     I_item          IN     ITEM_MASTER.ITEM%TYPE,
                     I_loc           IN     ITEM_LOC_SOH.LOC%TYPE)
   RETURN BOOLEAN IS

   L_total_av_pack_value     ITEM_LOC_SOH.AV_COST%TYPE;
   L_total_unit_pack_value   ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_total_pack_qty          NUMBER;
   L_pct_av_pack             NUMBER;
   L_pct_unit_pack           NUMBER;
   L_std_av_ind              SYSTEM_OPTIONS.STD_AV_IND%TYPE;


   cursor C_GET_PACK_VALUE is
      select sum(vpq.qty * ils.av_cost),
             sum(vpq.qty * ils.unit_cost),
             sum(vpq.qty)
        from v_packsku_qty vpq,
             item_loc_soh ils
       where vpq.item    = ils.item
         and vpq.pack_no = I_pack_no
         and ils.loc     = I_loc;

   
    cursor C_GET_PCT_IN_PACK is
      select DECODE(L_total_av_pack_value,
                    0,1/L_total_pack_qty,
                    (ils.av_cost / L_total_av_pack_value)),
             DECODE(L_total_unit_pack_value,
                    0,1/L_total_pack_qty,
                    (ils.unit_cost / L_total_unit_pack_value))
        from item_loc_soh ils
       where ils.item    = I_item
         and ils.loc     = I_loc;

BEGIN
   -- This function returns the percentage of component item's unit value with respect to
   -- the pack's unit value. Item and pack item are valued at average cost or unit cost
   -- depending on the std_av_ind in system_options.

   open C_GET_PACK_VALUE;
   
   fetch C_GET_PACK_VALUE into L_total_av_pack_value,
                               L_total_unit_pack_value,
                               L_total_pack_qty;
   close C_GET_PACK_VALUE;
   
   open C_GET_PCT_IN_PACK;
   fetch C_GET_PCT_IN_PACK into L_pct_av_pack, L_pct_unit_pack;
   close C_GET_PCT_IN_PACK;

   if not SYSTEM_OPTIONS_SQL.STD_AV_IND(O_error_message,
                                        L_std_av_ind) then
      return FALSE;
   end if;

   if L_std_av_ind = 'A' then
      O_pct_in_pack := L_pct_av_pack;
   else 
      O_pct_in_pack := L_pct_unit_pack;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'TRANSFER_COST_SQL.PCT_IN_PACK',NULL);
      RETURN FALSE;
END PCT_IN_PACK;
---------------------------------------------------------------------------------------------
FUNCTION PCT_IN_PACK_UNIT_COST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_pct_in_pack   IN OUT NUMBER,
                               I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE,
                               I_loc           IN     ITEM_LOC_SOH.LOC%TYPE)
   RETURN BOOLEAN IS

   L_total_pack_value    ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_total_pack_qty          NUMBER;

   cursor C_GET_PACK_VALUE is
      select sum(vpq.qty * ils.unit_cost),
             sum(vpq.qty)      
        from v_packsku_qty vpq,
             item_loc_soh ils
       where vpq.item    = ils.item
         and vpq.pack_no = I_pack_no
         and ils.loc     = I_loc;

   cursor C_GET_PCT_IN_PACK is
      select DECODE(L_total_pack_value, 
                    0,1/L_total_pack_qty,
                    (ils.unit_cost / L_total_pack_value))
        from item_loc_soh ils
       where ils.item    = I_item
         and ils.loc     = I_loc;

BEGIN
   -- This function returns the percentage of component item's unit value with respect to
   -- the pack's unit value. Item and pack item are valued at unit cost.

   open C_GET_PACK_VALUE;
   fetch C_GET_PACK_VALUE into L_total_pack_value,L_total_pack_qty;
   close C_GET_PACK_VALUE;

   open C_GET_PCT_IN_PACK;
   fetch C_GET_PCT_IN_PACK into O_pct_in_pack;
   close C_GET_PCT_IN_PACK;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                            'TRANSFER_COST_SQL.PCT_IN_PACK_UNIT_COST',NULL);
      RETURN FALSE;
END PCT_IN_PACK_UNIT_COST;
---------------------------------------------------------------------------------------------
FUNCTION CALC_ACTIVITY_COSTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE)
   RETURN BOOLEAN IS

   L_tsfhead     TRANSFER_SQL.TSFHEAD_INFO;
   L_sysopt_row  SYSTEM_OPTIONS%ROWTYPE;

BEGIN
   if I_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_tsf_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   -- Initialize variables
   P_tic_size        := 0;
   P_tiwc_size       := 0;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_sysopt_row) = FALSE then
      return FALSE;
   end if;

   if TRANSFER_SQL.GET_BASIC_TSFHEAD_INFO(O_error_message,
                                          L_tsfhead,
                                          I_tsf_no) = FALSE then
      return FALSE;
   end if;

   LP_tsf_parent_no := L_tsfhead.tsf_no;
   LP_tsf_no        := L_tsfhead.tsf_child_no;
   LP_from_loc      := L_tsfhead.from_loc;

   -- LP_tsf_no is the child transfer if one exists, otherwise it is the parent
   if LP_tsf_no is NULL then
      LP_tsf_no        := LP_tsf_parent_no;
      LP_tsf_parent_no := NULL;
   end if;

   -- determine which leg of the transfer (if any) is intercompany
   if (L_tsfhead.from_tsf_entity = L_tsfhead.to_tsf_entity
      and L_sysopt_row.intercompany_transfer_basis = 'T') or
      (L_tsfhead.from_set_of_books_id = L_tsfhead.to_set_of_books_id
      and L_sysopt_row.intercompany_transfer_basis = 'B') then
      -- not intercompany
      LP_ict_leg := NULL;
   elsif LP_tsf_parent_no is NULL then
      -- single leg intercompany transfer
      LP_ict_leg := 'S';
   elsif (L_tsfhead.from_tsf_entity = L_tsfhead.finisher_tsf_entity
         and L_sysopt_row.intercompany_transfer_basis = 'T') or
         (L_tsfhead.from_set_of_books_id = L_tsfhead.finisher_set_of_books_id
         and L_sysopt_row.intercompany_transfer_basis = 'B') then
      -- second leg is intercompany
      LP_ict_leg := '2';
   else
      -- first leg is intercompany
      LP_ict_leg := '1';
   end if;

   -- populate the fetch arrays
   if GET_BULK_FETCH(O_error_message) = FALSE then
      return FALSE;
   end if;

   for i IN 1..P_fetch_item.COUNT LOOP
      if P_fetch_pack_ind(i) = 'N' then
         if PROCESS_ITEM(O_error_message,
                         P_fetch_item(i),
                         NULL,
                         P_fetch_tsf_qty(i),
                         P_fetch_tsf_price(i),
                         NULL) = FALSE then
            return FALSE;
         end if;
      else
         if PROCESS_PACK(O_error_message,
                         P_fetch_item(i),
                         P_fetch_tsf_qty(i),
                         P_fetch_tsf_price(i)) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop;

   -- Convert values to to_loc currency.  Since from_loc is a global, no need
   -- for it to be an input variable.
   if CONVERT_CURRENCY(O_error_message,
                       L_tsfhead.from_loc_type,
                       L_tsfhead.to_loc,
                       L_tsfhead.to_loc_type) = FALSE then
      return FALSE;
   end if;

   -- populate the tables
   if FLUSH_ARRAYS(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                            'TRANSFER_COST_SQL.CALC_ACTIVITY_COSTS',NULL);
      RETURN FALSE;
END CALC_ACTIVITY_COSTS;
---------------------------------------------------------------------------------------------
FUNCTION GET_BULK_FETCH(O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   --tsf_qty will only be 0 when the item is added to the tsf at receipt.
   --This means that this is being called to rebuild the second leg of the transfer
   --(The 2nd leg has been set to input status and is now being re-approved).
   cursor C_GET_TSF_ITEMS is
      select t.item,
             im.pack_ind,
             t.tsf_price,
             decode(t.tsf_qty,0, nvl(received_qty,0),tsf_qty) tsf_qty
        from tsfdetail t,
             item_master im
       where t.item   = im.item
         and t.tsf_no = LP_tsf_parent_no;

   cursor C_GET_TSF_ITEMS_NO_FINISH is
      select t.item,
             im.pack_ind,
             t.tsf_price,
             t.tsf_qty
        from tsfdetail t,
             item_master im
       where t.item   = im.item
         and t.tsf_no = LP_tsf_no;

   cursor C_GET_XFORM_ITEMS is
      select td.from_item,
             td.to_item
        from tsf_xform_detail td,
             tsf_xform t
       where t.tsf_xform_id = td.tsf_xform_id
         and t.tsf_no       = LP_tsf_parent_no;

   --The sum qty is used to calculate a weighted average unit cost for
   --the wo activity costs.  This is required for many-to-one transformations
   cursor C_GET_SUM_XFORM_TO_ITEMS is
      select sum(to_qty), to_item
        from tsf_xform x,
             tsf_xform_detail xd
       where x.tsf_xform_id = xd.tsf_xform_id
         and x.tsf_no = LP_tsf_parent_no
    group by to_item ;

   cursor C_GET_WO_ACTIVITIES is
      select td.item,
             td.activity_id,
             td.unit_cost
        from tsf_wo_head   th,
             tsf_wo_detail td
       where th.tsf_wo_id = td.tsf_wo_id
         and th.tsf_no    = LP_tsf_parent_no;
BEGIN
   -- There will be no tsf_parent_no for intercompany transfers that have no
   -- finishing involved.
   if LP_tsf_parent_no is NOT NULL then
      open C_GET_TSF_ITEMS;
      fetch C_GET_TSF_ITEMS BULK COLLECT INTO P_fetch_item,
                                              P_fetch_pack_ind,
                                              P_fetch_tsf_price,
                                              P_fetch_tsf_qty;
      close C_GET_TSF_ITEMS;

      open C_GET_XFORM_ITEMS;
      fetch C_GET_XFORM_ITEMS BULK COLLECT INTO P_from_items,
                                                P_to_items;
      close C_GET_XFORM_ITEMS;

      open C_GET_SUM_XFORM_TO_ITEMS;
      fetch C_GET_SUM_XFORM_TO_ITEMS BULK COLLECT INTO P_sum_to_items_qty,
                                                       P_sum_to_items;
      close C_GET_SUM_XFORM_TO_ITEMS;

      open C_GET_WO_ACTIVITIES;
      fetch C_GET_WO_ACTIVITIES BULK COLLECT INTO P_wo_fetch_item,
                                                  P_wo_fetch_activity_id,
                                                  P_wo_fetch_unit_cost;
      close C_GET_WO_ACTIVITIES;
   else
      open C_GET_TSF_ITEMS_NO_FINISH;
      fetch C_GET_TSF_ITEMS_NO_FINISH BULK COLLECT INTO P_fetch_item,
                                                        P_fetch_pack_ind,
                                                        P_fetch_tsf_price,
                                                        P_fetch_tsf_qty;
      close C_GET_TSF_ITEMS_NO_FINISH;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GET_BULK_FETCH',
                                            to_char(SQLCODE));
      return FALSE;
END GET_BULK_FETCH;
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_PACK(O_error_message IN OUT VARCHAR2,
                      I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                      I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_tsf_price     IN     TSFDETAIL.TSF_PRICE%TYPE)
   RETURN BOOLEAN is

   L_pack_value   ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_pct_in_pack  NUMBER;

   cursor C_GET_PACK_VALUE is
      select sum(vpq.qty * ils.unit_cost)
        from v_packsku_qty vpq,
             item_loc_soh ils
       where vpq.item    = ils.item
         and vpq.pack_no = I_pack_no
         and ils.loc     = LP_from_loc;

   cursor C_GET_COMPONENTS is
      select vpq.item,
             (vpq.qty * I_tsf_qty) pack_qty,
             DECODE(L_pack_value,
                    0,0,
                    (ils.unit_cost / L_pack_value)) pct_in_pack
        from v_packsku_qty vpq,
             item_loc_soh ils
       where vpq.item    = ils.item
         and vpq.pack_no = I_pack_no
         and ils.loc     = LP_from_loc;

BEGIN
   -- Since we need to loop through v_packsku_qty and item_loc_soh anyway,
   -- it makes more sense to retrieve values here rather than call
   -- TRANSFER_COST_SQL.PCT_IN_PACK, which uses the same cursors.
   open C_GET_PACK_VALUE;
   fetch C_GET_PACK_VALUE into L_pack_value;
   close C_GET_PACK_VALUE;

   for rec in C_GET_COMPONENTS loop
      if PROCESS_ITEM(O_error_message,
                      rec.item,
                      I_pack_no,
                      rec.pack_qty,
                      I_tsf_price * rec.pct_in_pack,  -- prorated value
                      rec.pct_in_pack) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PROCESS_PACK',
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_PACK;
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM(O_error_message IN OUT VARCHAR2,
                      I_item          IN     ITEM_MASTER.ITEM%TYPE,
                      I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                      I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_tsf_price     IN     TSFDETAIL.TSF_PRICE%TYPE,
                      I_pct_in_pack   IN     NUMBER)
   return BOOLEAN is

   L_xform_item       ITEM_MASTER.ITEM%TYPE;
   L_tic_index        NUMBER;
   L_xform_index      NUMBER                  := 0;
   L_old_qty          TSFDETAIL.TSF_QTY%TYPE  := 0;
   L_sum_xform_to_qty TSFDETAIL.TSF_QTY%TYPE  := NULL;
   L_leg_1_status     TSFHEAD.STATUS%TYPE;

   cursor C_LEG_1_STATUS is
      select status
        from tsfhead
       where tsf_no = LP_tsf_parent_no;

BEGIN
   -- Check if the item will be transformed.  If not, it will be the same
   -- as I_item.  If tsf_parent_no is NULL, no items will be transformed.
   L_xform_item := I_item;
   if LP_tsf_parent_no is NOT NULL then
      for i IN 1..P_from_items.COUNT LOOP
         if P_from_items(i) = I_item then
            L_xform_item := P_to_items(i);
            exit;
         end if;
      end loop;

      --if item is transformed get the total number of resultant items for all transformations.
      if L_xform_item != I_item then
         for i IN 1..P_sum_to_items.COUNT LOOP
            if P_sum_to_items(i) = L_xform_item then
               L_sum_xform_to_qty := P_sum_to_items_qty(i);
               exit;
            end if;
         end loop;
      end if;
   end if;

   -- Check if the item exists in the tsf_item_cost table array
   L_tic_index := 0;
   if P_tic_size > 0 then
      for i in 1..P_tic_size LOOP
         if P_tic_item(i) = L_xform_item then
            L_tic_index := i;
            exit;
         end if;
      end loop;
   end if;

   if L_tic_index = 0 then
      if ADD_TO_TIC_ARRAY(O_error_message,
                          L_xform_item,
                          I_tsf_qty,
                          I_tsf_price) = FALSE then
         return FALSE;
      end if;

      -- Set the index equal to the new row for use in the workorder activity loop.
      -- Set the old qty equal to the current qty.  This value is used to prorate
      --   workorder activity costs
      L_tic_index := P_tic_size;
      L_old_qty   := I_tsf_qty;
   else
      -- Set the old qty equal to the existing qty.  This value is used to prorate
      --   workorder activity costs
      L_old_qty := P_tic_tsf_qty(L_tic_index);
      if UPDATE_TIC_ARRAY(O_error_message,
                          L_tic_index,
                          I_tsf_qty,
                          I_tsf_price) = FALSE then
         return FALSE;
      end if;
   end if;

   -- Check if there are any workorder activites for the item or pack
   -- If tsf_parent_no is NULL, no finishing is being done.
   if LP_tsf_parent_no is NOT NULL then
      if I_pack_no is NOT NULL then
         if PROCESS_WO_ACTIVITIES(O_error_message,
                                  I_pack_no,
                                  L_tic_index,
                                  I_tsf_qty,
                                  L_old_qty,
                                  I_pct_in_pack,
                                  L_sum_xform_to_qty) = FALSE then
            return FALSE;
         end if;
      else
         if PROCESS_WO_ACTIVITIES(O_error_message,
                                  I_item,
                                  L_tic_index,
                                  I_tsf_qty,
                                  L_old_qty,
                                  1,
                                  L_sum_xform_to_qty) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   -- Now, add the from_item to the tsf_item_cost table array.  This is only
   -- necessary for multi-legged intercompany transfers.
   if LP_tsf_parent_no is NOT NULL and LP_ict_leg is NOT NULL then
      -- Find the index for the from item
      for i IN 1..P_from_items.COUNT LOOP
         if P_from_items(i) = I_item then
            L_xform_index := i;
            exit;
         end if;
      end loop;

      -- Check if the item exists in the tsf_item_cost table array
      L_tic_index := 0;
      if P_tic_size > 0 then
         for i in 1..P_tic_size LOOP
            if P_tic_item(i) = I_item and
               P_tic_tsf_no(i) = LP_tsf_parent_no then
               L_tic_index := i;
               exit;
            end if;
         end loop;
      end if;

      if L_tic_index = 0 then

         -- only need to insert first leg transfer_item_cost records
         -- if the 1st leg is in Input or Submitted status. Otherwise,
         -- 1st leg is already in progress and transfer_item_cost
         -- records are already present.

         open C_LEG_1_STATUS;
         fetch C_LEG_1_STATUS into L_leg_1_status;
         close C_LEG_1_STATUS;

         if L_leg_1_status in ('I','B') then

            if L_xform_index = 0 then
               -- the item was not transformed so just write a record for the passed in item
               if ADD_TO_TIC_ARRAY_LEG_1(O_error_message,
                                         I_item,
                                         NULL,
                                         I_tsf_qty,
                                         I_tsf_price) = FALSE then
                  return FALSE;
               end if;
            else
               if ADD_TO_TIC_ARRAY_LEG_1(O_error_message,
                                         P_from_items(L_xform_index),
                                         P_to_items(L_xform_index),
                                         I_tsf_qty,
                                         I_tsf_price) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
      else
         if UPDATE_TIC_ARRAY(O_error_message,
                             L_tic_index,
                             I_tsf_qty,
                             I_tsf_price) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PROCESS_ITEM',
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_ITEM;
---------------------------------------------------------------------------------------------
FUNCTION ADD_TO_TIC_ARRAY(O_error_message IN OUT VARCHAR2,
                          I_item          IN     ITEM_MASTER.ITEM%TYPE,
                          I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                          I_tsf_price     IN     TSFDETAIL.TSF_PRICE%TYPE)
   return BOOLEAN is

   L_seq_no            TSF_ITEM_COST.TSF_ITEM_COST_ID%TYPE;
   L_leg_ind           TSF_ITEM_COST.LEG_IND%TYPE;
   L_ict_leg_ind       TSF_ITEM_COST.ICT_LEG_IND%TYPE;

   cursor C_TIC_SEQ is
      select tsf_item_cost_id_sequence.nextval
        from dual;

BEGIN
   P_tic_size := P_tic_size + 1;

   open C_TIC_SEQ;
   fetch C_TIC_SEQ into L_seq_no;
   close C_TIC_SEQ;

   -- This function is called either for the second leg of a transfer or
   -- if there is no item transformation.  Determine whether this is the
   -- intercompany leg of the transfer.
   if LP_ict_leg is NULL then
      -- not an intercompany transfer
      L_ict_leg_ind := NULL;

      -- since this function is not called for single leg intracompany transfers,
      -- this must be the second leg
      L_leg_ind := '2';
   elsif LP_ict_leg = 'S' then
      -- this is a single leg intercompany transfer
      L_ict_leg_ind := NULL;
      L_leg_ind     := 'S';
   elsif LP_ict_leg = '1' then
      -- the intercompany leg is the first leg
      L_ict_leg_ind := 'N';
      L_leg_ind     := '2';
   elsif LP_ict_leg = '2' then
      -- the intercompany leg is the second leg
      L_ict_leg_ind := 'Y';
      L_leg_ind     := '2';
   end if;

   P_tic_tsf_item_cost_id(P_tic_size) := L_seq_no;
   P_tic_tsf_no(P_tic_size)           := LP_tsf_no;
   P_tic_tsf_parent_no(P_tic_size)    := LP_tsf_parent_no;
   P_tic_item(P_tic_size)             := I_item;
   P_tic_xform_to_item(P_tic_size)    := NULL;
   P_tic_tsf_qty(P_tic_size)          := I_tsf_qty;
   P_tic_tsf_avg_price(P_tic_size)    := I_tsf_price;
   P_tic_ict_leg_ind(P_tic_size)      := L_ict_leg_ind;
   P_tic_leg_ind(P_tic_size)          := L_leg_ind;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ADD_TO_TIC_ARRAY',
                                            to_char(SQLCODE));
      return FALSE;
END ADD_TO_TIC_ARRAY;
---------------------------------------------------------------------------------------------
FUNCTION ADD_TO_TIC_ARRAY_LEG_1(O_error_message IN OUT VARCHAR2,
                                I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                I_xform_item    IN     ITEM_MASTER.ITEM%TYPE,
                                I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                                I_tsf_price     IN     TSFDETAIL.TSF_PRICE%TYPE)
   return BOOLEAN is

   L_seq_no            TSF_ITEM_COST.TSF_ITEM_COST_ID%TYPE;
   L_leg_ind           TSF_ITEM_COST.LEG_IND%TYPE;
   L_ict_leg_ind       TSF_ITEM_COST.ICT_LEG_IND%TYPE;

   cursor C_TIC_SEQ is
      select tsf_item_cost_id_sequence.nextval
        from dual;

BEGIN
   P_tic_size := P_tic_size + 1;

   open C_TIC_SEQ;
   fetch C_TIC_SEQ into L_seq_no;
   close C_TIC_SEQ;

   -- This function is only called for the first leg of a multi-legged transfer.
   -- Determine whether this is the intercompany leg of the transfer.
   if LP_ict_leg is NULL then
      -- not an intercompany transfer
      L_ict_leg_ind := NULL;

      -- since this function is not called for single leg intracompany transfers,
      -- this must be the second leg
      L_leg_ind := '1';
   elsif LP_ict_leg = 'S' then
      -- this is a single leg intercompany transfer
      L_ict_leg_ind := NULL;
      L_leg_ind     := 'S';
   elsif LP_ict_leg = '1' then
      -- the intercompany leg is the first leg
      L_ict_leg_ind := 'Y';
      L_leg_ind     := '1';
   elsif LP_ict_leg = '2' then
      -- the intercompany leg is the second leg
      L_ict_leg_ind := 'N';
      L_leg_ind     := '1';
   end if;

   -- for multi-legged transfers, the tsf_parent_no is the tsf_no for the first leg
   P_tic_tsf_item_cost_id(P_tic_size) := L_seq_no;
   P_tic_tsf_no(P_tic_size)           := LP_tsf_parent_no;
   P_tic_tsf_parent_no(P_tic_size)    := NULL;
   P_tic_item(P_tic_size)             := I_item;
   P_tic_xform_to_item(P_tic_size)    := I_xform_item;
   P_tic_tsf_qty(P_tic_size)          := I_tsf_qty;
   P_tic_tsf_avg_price(P_tic_size)    := I_tsf_price;
   P_tic_ict_leg_ind(P_tic_size)      := L_ict_leg_ind;
   P_tic_leg_ind(P_tic_size)          := L_leg_ind;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ADD_TO_TIC_ARRAY_LEG_1',
                                            to_char(SQLCODE));
      return FALSE;
END ADD_TO_TIC_ARRAY_LEG_1;
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_TIC_ARRAY(O_error_message IN OUT VARCHAR2,
                          I_index         IN     NUMBER,
                          I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                          I_tsf_price     IN     TSFDETAIL.TSF_PRICE%TYPE)
   return BOOLEAN is

   L_old_qty   TSFDETAIL.TSF_QTY%TYPE;

BEGIN
   L_old_qty                        := P_tic_tsf_qty(I_index);

   P_tic_tsf_qty(I_index)           := P_tic_tsf_qty(I_index) + I_tsf_qty;

   -- tsf_price will be null if this is not an intercompany transfer
   -- otherwise average the tsf_price over the total qty (which was updated above)
   if I_tsf_price is NOT NULL then
      P_tic_tsf_avg_price(I_index)   := ((P_tic_tsf_avg_price(I_index) * L_old_qty) +
                                         (I_tsf_price * I_tsf_qty)) / P_tic_tsf_qty(I_index);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'UPDATE_TIC_ARRAY',
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_TIC_ARRAY;
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_WO_ACTIVITIES(O_error_message IN OUT VARCHAR2,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE,
                               I_tic_index     IN     NUMBER,
                               I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                               I_old_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                               I_pct_in_pack   IN     NUMBER,
                               I_sum_xform_qty IN     TSFDETAIL.TSF_QTY%TYPE)
   return BOOLEAN is

   L_tiwc_index         NUMBER;
   L_avg_unit_cost      WO_ACTIVITY.UNIT_COST%TYPE := NULL;
   --default to zero for when a new wo activity is being added
   L_old_avg_unit_cost  WO_ACTIVITY.UNIT_COST%TYPE := 0;

BEGIN
   for i in 1..P_wo_fetch_item.COUNT LOOP
      if P_wo_fetch_item(i) = I_item then
         -- There are workorders against the item.  Determine if
         -- the activity has been added to the tsf_item_wo_cost array
         L_tiwc_index := 0;
         if P_tiwc_size > 0 then
            for j in 1..P_tiwc_size LOOP
               if P_tiwc_tsf_item_cost_id(j) = P_tic_tsf_item_cost_id(I_tic_index) and
                  P_tiwc_activity_id(j)      = P_wo_fetch_activity_id(i) then
                  L_tiwc_index := j;
                  exit;
               end if;
            end loop;
         end if;

         if CALC_NEW_AVG(O_error_message,
                         L_avg_unit_cost,
                         I_tic_index,
                         L_tiwc_index,
                         I_tsf_qty,
                         I_old_qty,
                         P_wo_fetch_unit_cost(i),
                         I_sum_xform_qty,
                         I_pct_in_pack) = FALSE then
            return FALSE;
         end if;

         if L_tiwc_index = 0 then
            if ADD_TO_TIWC_ARRAY(O_error_message,
                                 I_tic_index,
                                 P_wo_fetch_activity_id(i),
                                 I_tsf_qty,
                                 L_avg_unit_cost) = FALSE then
               return FALSE;
            end if;
         else
            if UPDATE_TIWC_ARRAY(O_error_message,
                                 L_tiwc_index,
                                 I_tic_index,
                                 I_tsf_qty,
                                 I_old_qty,
                                 L_avg_unit_cost) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PROCESS_WO_ACTIVITIES',
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_WO_ACTIVITIES;
---------------------------------------------------------------------------------------------
FUNCTION ADD_TO_TIWC_ARRAY(O_error_message IN OUT VARCHAR2,
                           I_tic_index     IN     NUMBER,
                           I_activity_id   IN     WO_ACTIVITY.ACTIVITY_ID%TYPE,
                           I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                           I_unit_cost     IN     WO_ACTIVITY.UNIT_COST%TYPE)
   return BOOLEAN is

BEGIN
   P_tiwc_size := P_tiwc_size + 1;
   P_tiwc_tsf_item_cost_id(P_tiwc_size) := P_tic_tsf_item_cost_id(I_tic_index);
   P_tiwc_activity_id(P_tiwc_size)      := I_activity_id;
   P_tiwc_avg_unit_cost(P_tiwc_size)    := I_unit_cost;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ADD_TO_TIWC_ARRAY',
                                            to_char(SQLCODE));
      return FALSE;
END ADD_TO_TIWC_ARRAY;
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_TIWC_ARRAY(O_error_message IN OUT VARCHAR2,
                           I_index         IN     NUMBER,
                           I_tic_index     IN     NUMBER,
                           I_tsf_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                           I_old_qty       IN     TSFDETAIL.TSF_QTY%TYPE,
                           I_unit_cost     IN     WO_ACTIVITY.UNIT_COST%TYPE)
   return BOOLEAN is

BEGIN

   P_tiwc_avg_unit_cost(I_index)    :=  I_unit_cost;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'UPDATE_TIWC_ARRAY',
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_TIWC_ARRAY;
---------------------------------------------------------------------------------------------
FUNCTION CALC_NEW_AVG(O_error_message      IN OUT VARCHAR2,
                      O_new_avg_unit_cost  IN OUT WO_ACTIVITY.UNIT_COST%TYPE,
                      I_tic_index          IN     NUMBER,
                      I_tiwc_index         IN     NUMBER,
                      I_tsf_qty            IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_old_qty            IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_activity_unit_cost IN     WO_ACTIVITY.UNIT_COST%TYPE,
                      I_sum_xform_qty      IN     TSFDETAIL.TSF_QTY%TYPE,
                      I_pct_in_pack        IN     NUMBER)

   return BOOLEAN is

   --default to zero for when a new wo activity is being added
   L_old_avg_unit_cost  wo_activity.unit_cost%TYPE := 0;

BEGIN

   if I_tiwc_index != 0 then
      L_old_avg_unit_cost := P_tiwc_avg_unit_cost(I_tiwc_index);
   end if;


   --To properly average costs for transformations use the sum xform qty
   --  For many-to-one transformations if the same activity is being done for multiple
   --  'from' items the old average cost is the average for the item qty processed thus far.
   --  It must be re-avarage to include the new qty.
   --  For instance
   --     10 Item A  --> transformed into item C
   --     10 Item B  --> transformed into item C
   --                                      = 20 item C
   --
   --      Item A work order activity 9  unit cost .10 = $1
   --      Item A work order activity 99 unit cost .25 = $2.5
   --
   --      Item B work order activity 9  unit cost .10 = $1
   --                                              ------------
   --                                  activity 9        $2
   --                                  activity 99       $2.5
   --
   --      When item A is processed:
   --         activity  9  old avg unit cost = 0
   --         activity  99 old avg unit cost = 0
   --
   --         The correct activty amounts are:
   --            activity 9  : ((0 * 20) + (.10 * 10)) / 20 = .05
   --            activity 99 : ((0 * 20) + (.25 * 10)) / 20 = .125
   --
   --      When item B is processed:
   --          activity 9  old avg unit cost = .05
   --          activity 99 old avg unit cost = .125
   --
   --          The correct activity amount for 9 is now:
   --               ((.05 * 20) + (.10 * 10)) / 20 = .10
   --
   --          Activity 99 is uneffected when B is processed because it is not an activity for B
   --
   --      When the activity costs are written the average price is multipled times the transfer qty
   --             .10  * 20  = $2
   --             .125 * 20 = $2.5
   --

   if I_sum_xform_qty is NOT NULL then
      --old cost is 0 when adding a new activity. Then L_old_avg_unit_cost = 0 * x = 0
      L_old_avg_unit_cost := L_old_avg_unit_cost * I_sum_xform_qty;

      -- If the item is a pack, prorate the unit_cost.  If it is not a pack,
      -- I_pct_in_pack = 1, so the full unit_cost will be used.
      O_new_avg_unit_cost :=  (L_old_avg_unit_cost +
                                 ((I_activity_unit_cost * I_pct_in_pack) * I_tsf_qty)) / I_sum_xform_qty;
   else
      --old cost is 0 if adding a new activity 0 * x = 0
      L_old_avg_unit_cost := L_old_avg_unit_cost * I_old_qty;


      -- If the item is a pack, prorate the unit_cost.  If it is not a pack,
      -- I_pct_in_pack = 1, so the full unit_cost will be used.
      O_new_avg_unit_cost := (L_old_avg_unit_cost +
                               ((I_activity_unit_cost * I_pct_in_pack) * I_tsf_qty)) / P_tic_tsf_qty(I_tic_index);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSFER_COST_SQL.CALC_NEW_AVG',
                                            to_char(SQLCODE));
      return FALSE;

END CALC_NEW_AVG;
---------------------------------------------------------------------------------------------
FUNCTION CONVERT_CURRENCY(O_error_message IN OUT VARCHAR2,
                          I_from_loc_type IN     TSFHEAD.FROM_LOC_TYPE%TYPE,
                          I_to_loc        IN     TSFHEAD.TO_LOC%TYPE,
                          I_to_loc_type   IN     TSFHEAD.TO_LOC_TYPE%TYPE)
   return BOOLEAN is

BEGIN
   if P_tic_size > 0 then
      for i in 1..P_tic_size LOOP
         -- For multi-legged intercompany transfers, the record for the
         -- first leg should remain in the from_loc currency.  The only
         -- time the tsf_no is not equal to the global tsf_no is in this
         -- situation.
         if P_tic_tsf_no(i) = LP_tsf_no then
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                LP_from_loc,
                                                I_from_loc_type,
                                                NULL,
                                                I_to_loc,
                                                I_to_loc_type,
                                                NULL,
                                                P_tic_tsf_avg_price(i),
                                                P_tic_tsf_avg_price(i),
                                                NULL,
                                                NULL,
                                                NULL) = FALSE then
               return FALSE;
            end if;
         end if;
      end loop;
   end if;

   -- All workorder activity costs should be converted to the to_loc currency
   if P_tiwc_size > 0 then
      for i in 1..P_tiwc_size LOOP
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             LP_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             P_tiwc_avg_unit_cost(i),
                                             P_tiwc_avg_unit_cost(i),
                                             NULL,
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'CONVERT_CURRENCY',
                                            to_char(SQLCODE));
      return FALSE;
END CONVERT_CURRENCY;
---------------------------------------------------------------------------------------------
FUNCTION FLUSH_ARRAYS(O_error_message  IN OUT  VARCHAR2)
   return BOOLEAN IS

BEGIN
   if P_tic_size > 0 then
      FORALL i IN 1..P_tic_size
         insert into tsf_item_cost(tsf_item_cost_id,
                                   tsf_no,
                                   tsf_parent_no,
                                   item,
                                   tsf_qty,
                                   tsf_avg_price,
                                   xform_to_item,
                                   leg_ind,
                                   ict_leg_ind)
                            values(P_tic_tsf_item_cost_id(i),
                                   P_tic_tsf_no(i),
                                   P_tic_tsf_parent_no(i),
                                   P_tic_item(i),
                                   P_tic_tsf_qty(i),
                                   P_tic_tsf_avg_price(i),
                                   P_tic_xform_to_item(i),
                                   P_tic_leg_ind(i),
                                   P_tic_ict_leg_ind(i));

      -- Since tsf_item_wo_cost is a child table, it won't be populated if
      -- tsf_item_cost has no records.
      if P_tiwc_size > 0 then
         FORALL i IN 1..P_tiwc_size
            insert into tsf_item_wo_cost(tsf_item_cost_id,
                                         activity_id,
                                         avg_unit_cost)
                                  values(P_tiwc_tsf_item_cost_id(i),
                                         P_tiwc_activity_id(i),
                                         P_tiwc_avg_unit_cost(i));
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FLUSH_ARRAYS',
                                            to_char(SQLCODE));
      return FALSE;
END FLUSH_ARRAYS;
---------------------------------------------------------------------------------------------
FUNCTION RECALC_WAC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_new_wac             IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                    I_distro_no           IN       TSFHEAD.TSF_NO%TYPE,
                    I_distro_type         IN       VARCHAR2,
                    I_item                IN       TSFDETAIL.ITEM%TYPE,
                    I_pack_no             IN       ITEM_MASTER.ITEM%TYPE,
                    I_percent_in_pack     IN       NUMBER,
                    I_from_loc            IN       ITEM_LOC.LOC%TYPE,
                    I_from_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_to_loc              IN       ITEM_LOC.LOC%TYPE,
                    I_to_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                    I_to_loc_charge       IN       ITEM_LOC_SOH.AV_COST%TYPE,
                    I_intercompany        IN       BOOLEAN,
                    I_recalc_from_to_loc  IN       VARCHAR2 DEFAULT 'T',
                    I_shipment            IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                    I_carton              IN       SHIPSKU.CARTON%TYPE DEFAULT NULL)
return BOOLEAN IS

   L_function          VARCHAR2(60) := 'TRANSFER_COST_SQL.RECALC_WAC';

   L_from_wac          ITEM_LOC_SOH.AV_COST%TYPE;

BEGIN

   if RECALC_WAC(O_error_message,
                 O_new_wac,
                 I_distro_no,
                 I_distro_type,
                 I_item,
                 I_pack_no,
                 I_percent_in_pack,
                 I_from_loc,
                 I_from_loc_type,
                 I_to_loc,
                 I_to_loc_type,
                 I_qty,
                 NULL,   -- weight_cuom
                 L_from_wac,
                 I_to_loc_charge,
                 I_intercompany,
                 I_recalc_from_to_loc,
                 I_shipment,
                 I_carton) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END RECALC_WAC;
---------------------------------------------------------------------------------------------
FUNCTION RECALC_WAC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_new_wac             IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                    I_distro_no           IN       TSFHEAD.TSF_NO%TYPE,
                    I_distro_type         IN       VARCHAR2,
                    I_item                IN       TSFDETAIL.ITEM%TYPE,
                    I_pack_no             IN       ITEM_MASTER.ITEM%TYPE,
                    I_percent_in_pack     IN       NUMBER,
                    I_from_loc            IN       ITEM_LOC.LOC%TYPE,
                    I_from_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_to_loc              IN       ITEM_LOC.LOC%TYPE,
                    I_to_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                    I_weight_cuom         IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                    I_from_wac            IN       ITEM_LOC_SOH.AV_COST%TYPE,
                    I_to_loc_charge       IN       ITEM_LOC_SOH.AV_COST%TYPE,
                    I_intercompany        IN       BOOLEAN,
                    I_recalc_from_to_loc  IN       VARCHAR2 DEFAULT 'T',
                    I_shipment            IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                    I_carton              IN       SHIPSKU.CARTON%TYPE DEFAULT NULL,
                    I_chrg_from_loc       IN       ITEM_LOC_SOH.AV_COST%TYPE DEFAULT NULL,
                    I_wrong_st_rcv_ind    IN       VARCHAR2 DEFAULT NULL,
                    I_tran_date           IN       TRAN_DATA.TRAN_DATE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   L_function              VARCHAR2(60) := 'TRANSFER_COST_SQL.RECALC_WAC';

   L_new_cost_to_loc       ITEM_LOC_SOH.AV_COST%TYPE := NULL; -- to loc currency
   L_total_wo_uicosts      ITEM_LOC_SOH.AV_COST%TYPE := 0;

   --This will hold the total cost of 1 item for a non-catch weight simple pack
   L_total_unit_cost       ITEM_LOC_SOH.AV_COST%TYPE;

   --This will hold the total cost of I_qty for a catch weight simple pack item
   L_extended_cost         ITEM_LOC_SOH.AV_COST%TYPE;

   L_current_soh           ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_cv_qty                ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_current_av_cost       ITEM_LOC_SOH.AV_COST%TYPE;
   L_neg_soh_wac_adj_amt   ITEM_LOC_SOH.AV_COST%TYPE;
   L_dept                  ITEM_MASTER.DEPT%TYPE;
   L_class                 ITEM_MASTER.CLASS%TYPE;
   L_subclass              ITEM_MASTER.SUBCLASS%TYPE;
   L_from_wac              ITEM_LOC_SOH.AV_COST%TYPE;

   L_item_master           ITEM_MASTER%ROWTYPE;
   L_tran_date             TRAN_DATA.TRAN_DATE%TYPE := get_vdate;
   L_tran_code             TRAN_DATA.TRAN_CODE%TYPE;

   L_l10n_fin_rec          L10N_FIN_REC          := L10N_FIN_REC();
   L_franchise_ordret_ind  VARCHAR2(1)           := NULL;
   L_from_to_loc           ITEM_LOC_SOH.LOC%TYPE;
   L_from_to_loc_type      ITEM_LOC_SOH.LOC_TYPE%TYPE;
   L_inv_status            SHIPSKU.INV_STATUS%TYPE;

   cursor C_GET_SHIPSKU_COST(cv_item ITEM_MASTER.ITEM%TYPE) is
      select unit_cost,
             NVL(inv_status, -1) inv_status
        from shipsku
       where shipment = NVL(I_shipment,shipment)
         and item      = cv_item
         and distro_no = I_distro_no
         and distro_type = I_distro_type
         and NVL(carton, '-*-') = NVL(I_carton, NVL(carton, '-*-'))
         and rownum = 1
       order by inv_status;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_item','NULL','NOT NULL');
      return FALSE;
   elsif I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_from_loc','NULL','NOT NULL');
      return FALSE;
   elsif I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_from_loc_type','NULL','NOT NULL');
      return FALSE;
   elsif I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_to_loc','NULL','NOT NULL');
      return FALSE;
   elsif I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_to_loc_type','NULL','NOT NULL');
      return FALSE;
   elsif I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_qty','NULL','NOT NULL');
      return FALSE;
   elsif I_to_loc_charge is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_to_loc_charge','NULL','NOT NULL');
      return FALSE;
   elsif I_recalc_from_to_loc NOT IN ('T', 'F') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_recalc_from_to_loc',I_recalc_from_to_loc,'T or F');
      return FALSE;
   end if;

   L_l10n_fin_rec.procedure_key := 'GET_CURRENT_SOH_WAC';
   L_l10n_fin_rec.source_entity := 'LOC';
   if I_recalc_from_to_loc = 'F' then
      L_l10n_fin_rec.source_id     := I_from_loc;
      L_l10n_fin_rec.source_type   := I_from_loc_type;
   else 
      L_l10n_fin_rec.source_id     := I_to_loc;
      L_l10n_fin_rec.source_type   := I_to_loc_type;
   end if;
   L_l10n_fin_rec.item          := I_item;
   L_l10n_fin_rec.calc_fld_type := 'TOTSTK';

   --For localized country Brazil, the L10N_SQL.EXEC_FUNCTION will call L10N_BR_FIN_SQL.GET_CURRENT_SOH_WAC to
   --get the SOH for all the virtual locations having the same CNPJ number.
   --For non-localized country, the L10N_SQL.EXEC_FUNCTION will call ITEMLOC_ATTRIB_SQL.GET_CURRENT_SOH_WAC to
   --get the SOH of a particular location.
   if L10N_SQL.EXEC_FUNCTION (O_error_message,
                              L_l10n_fin_rec) = FALSE then
      return FALSE;
   end if;

   L_current_soh     := L_l10n_fin_rec.stock_on_hand;
   L_current_av_cost := L_l10n_fin_rec.av_cost;
   
   --determine if the transfer is a Franchise returns
   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                L_franchise_ordret_ind,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_to_loc,
                                                I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   --if franchise return then WAC should not be recalculated,
   --return the current item/to_loc's av_cost as the new_wac.
   if L_franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
      O_new_wac := L_current_av_cost;
      return TRUE;
   end if;
   
   --From-Loc's wac is only recalculated in case of stock reconciliation ('SL'/'BL') where a reverse
   --tran_data of 30/32, 37/38, 20/82, 24/83 is written. Since the reverse tran_data is written based
   --on shipksku.unit_cost, from-loc's wac recalculation must be based on shipsku.unit_cost as well.
   --Shipsku.unit_cost is in from-loc's currency.
   if I_recalc_from_to_loc = 'F' then
      if I_pack_no is NOT NULL then
         open C_GET_SHIPSKU_COST(I_pack_no);
         fetch C_GET_SHIPSKU_COST into L_total_unit_cost, L_inv_status;
         close C_GET_SHIPSKU_COST;
         if L_total_unit_cost is NOT NULL then
            L_total_unit_cost := L_total_unit_cost * I_percent_in_pack;
         end if;
      else
         open C_GET_SHIPSKU_COST(I_item);
         fetch C_GET_SHIPSKU_COST into L_total_unit_cost, L_inv_status;
         close C_GET_SHIPSKU_COST;
      end if;
      if L_total_unit_cost is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_UNIT_COST',I_item, I_distro_no,NULL);
         return false;
      end if;
      -- Recalculation for src location should happen at (shipsku_cost - upcharge) as initial postings for 
      -- 32/38 are done on this cost.When called from STOCK_ORDER_RECONCILE_SQL.ADJUST_SHORTAGE , From_loc
      -- charges have been passed as I_to_loc_charge for I_recalc_from_to_loc = 'F'
      L_total_unit_cost := L_total_unit_cost - NVL(I_to_loc_charge,0);
      
   else

      if I_wrong_st_rcv_ind = 'Y' and (NVL(I_chrg_from_loc, -1) != -1) then
         if I_pack_no is NOT NULL then
            open C_GET_SHIPSKU_COST(I_pack_no);
            fetch C_GET_SHIPSKU_COST into L_total_unit_cost, L_inv_status;
            close C_GET_SHIPSKU_COST;
         
            L_total_unit_cost := (L_total_unit_cost * I_percent_in_pack) - I_chrg_from_loc;
         
            --convert L_total_unit_cost into to loc currency
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                I_from_loc,
                                                I_from_loc_type,
                                                NULL,
                                                I_to_loc,
                                                I_to_loc_type,
                                                NULL,
                                                L_total_unit_cost,
                                                L_total_unit_cost,
                                                'C',
                                                I_tran_date,
                                                NULL) = FALSE then
               return FALSE;
            end if;
            L_total_unit_cost := L_total_unit_cost + I_to_loc_charge;
         else
            open C_GET_SHIPSKU_COST(I_item);
            fetch C_GET_SHIPSKU_COST into L_total_unit_cost, L_inv_status;
            close C_GET_SHIPSKU_COST;
         
            L_total_unit_cost := L_total_unit_cost - I_chrg_from_loc;
         
            --convert L_total_unit_cost into to loc currency
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                I_from_loc,
                                                I_from_loc_type,
                                                NULL,
                                                I_to_loc,
                                                I_to_loc_type,
                                                NULL,
                                                L_total_unit_cost,
                                                L_total_unit_cost,
                                                'C',
                                                I_tran_date,
                                                NULL) = FALSE then
               return FALSE;
            end if;
            L_total_unit_cost := L_total_unit_cost + I_to_loc_charge;
         end if;
      --Recalculate WAC for the to-loc.
      --Get the cost/price depending on intra vs inter company transfer and MLE.
      --For a franchise transaction (transfer or allocation), get fixed_cost/customer_cost or return_unit_cost.
      --For I_distro_type of 'B' (book transfer), I_from_wac will be used.
      else
         if I_from_wac is NOT NULL  AND I_distro_type ='A' AND L_franchise_ordret_ind ='N' then
            L_from_wac := I_from_wac;
         elsif I_from_wac IS NULL OR I_intercompany OR L_franchise_ordret_ind ='Y' then
            if I_distro_type in ('T', 'A','B') then  
               if GET_TSF_ALLOC_COST_PRICE(O_error_message,
                                           L_new_cost_to_loc,
                                           L_total_wo_uicosts,
                                           I_distro_no,
                                           I_distro_type,
                                           I_item,
                                           I_pack_no,
                                           I_percent_in_pack,
                                           I_from_loc,
                                           I_from_loc_type,
                                           I_to_loc,
                                           I_to_loc_type,
                                           I_intercompany,
                                           L_franchise_ordret_ind) = FALSE then
                  return FALSE;
               end if;
            end if;
            
            -- Use the sending loc WAC if GET_TSF_ALLOC_COST_PRICE is not called
            ----
            if L_new_cost_to_loc is NULL then
               if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                                 I_item,
                                                 L_dept,
                                                 L_class,
                                                 L_subclass) = FALSE then
                  return FALSE;
               end if;
            
               if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                             L_from_wac,
                                             I_item,
                                             L_dept,
                                             L_class,
                                             L_subclass,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,  -- tran_date
                                             I_to_loc,
                                             I_to_loc_type) = FALSE then
                  return FALSE;
               end if;
            end if; --L_new_cost_to_loc is NULL
         else
            L_from_wac := I_from_wac;
         end if; -- I_from_wac is not NULL
      
         if L_from_wac IS NOT NULL then
            --convert L_from_wac into to loc currency(L_new_cost_to_loc)
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                I_from_loc,
                                                I_from_loc_type,
                                                NULL,
                                                I_to_loc,
                                                I_to_loc_type,
                                                NULL,
                                                L_from_wac,
                                                L_new_cost_to_loc,
                                                'C',
                                                I_tran_date,
                                                NULL) = FALSE then
               return FALSE;
            end if;
         end if;
      
         --Do the final calculations
         if I_weight_cuom is NOT NULL then
            -- catch weight simple pack, calculate total cost by weight
            -- weight should not apply to work order cost and upcharge
            if NOT CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                                    L_extended_cost,
                                                    I_pack_no,
                                                    L_new_cost_to_loc,  -- without work order and charge
                                                    I_weight_cuom,
                                                    I_qty) then
               return FALSE;
            end if;
            -- add in the total cost of work order and charge
            L_extended_cost := L_extended_cost + (L_total_wo_uicosts + I_to_loc_charge) * I_qty;
         else
            L_total_unit_cost := L_new_cost_to_loc + L_total_wo_uicosts + I_to_loc_charge;
         end if;
      end if; -- I_wrong_st_rcv_ind
   end if;  --I_recalc_from_to_loc
   if L_current_soh !=0 AND L_current_av_cost is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('AV_COST_NULL',I_item, I_to_loc,NULL);
      return FALSE;
   end if;
   if STKLEDGR_ACCTING_SQL.WAC_CALC_QTY_CHANGE(O_error_message,
                                               O_new_wac,
                                               L_neg_soh_wac_adj_amt,
                                               L_current_av_cost,
                                               L_current_soh,
                                               L_total_unit_cost,
                                               I_qty,
                                               L_extended_cost) = FALSE then
      return FALSE;
   end if;
   ---
   if L_neg_soh_wac_adj_amt != 0 then
      ---
      -- Insert tran_data with tran_code 70, the function STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT
      -- will build tran data array only, any functions which call RECALC_WAC should first
      -- initilize tran data array first, and finally flush this array to really insert it.

      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_master,
                                         I_item) = FALSE then
         return FALSE;
      end if;
      ---
      if I_qty <= 0 then
         L_cv_qty := I_qty;
      else
         L_cv_qty := L_current_soh;
      end if;
      ---
      if I_recalc_from_to_loc = 'F' then 
         L_from_to_loc := I_from_loc;
         L_from_to_loc_type := I_from_loc_type;
      else
         L_from_to_loc := I_to_loc;
         L_from_to_loc_type := I_to_loc_type;
      end if;
      ---
      L_tran_code := 70;
      ---
      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_item_master.dept,
                                             L_item_master.class,
                                             L_item_master.subclass,
                                             L_from_to_loc,
                                             L_from_to_loc_type,
                                             L_tran_date,
                                             L_tran_code,
                                             NULL,         -- I_adj_code,
                                             L_cv_qty,     -- I_units,
                                             L_neg_soh_wac_adj_amt,
                                             NULL,         -- I_total_retail,
                                             I_distro_no,  -- I_ref_no_1,
                                             NULL,         -- I_ref_no_2,
                                             NULL,         -- from_loc,
                                             NULL,         -- from_loc_type,
                                             NULL,         -- I_old_unit_retail,
                                             NULL,         -- I_new_unit_retail,
                                             NULL,         -- I_source_dept,
                                             NULL,         -- I_source_class,
                                             NULL,         -- I_source_subclass,
                                             L_function,   -- I_pgm_name,
                                             NULL) = FALSE then      -- I_gl_ref_no
         return FALSE;
      end if;
      ---
   end if;
   ---

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END RECALC_WAC;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_ALLOC_COST_PRICE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_tsf_cost_price        IN OUT TSFDETAIL.TSF_PRICE%TYPE,
                                  O_total_wo_cost         IN OUT TSF_ITEM_WO_COST.AVG_UNIT_COST%TYPE,
                                  I_distro_no             IN     SHIPSKU.DISTRO_NO%TYPE,
                                  I_distro_type           IN     SHIPSKU.DISTRO_TYPE%TYPE,
                                  I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                                  I_pack_no               IN     PACKITEM.PACK_NO%TYPE,
                                  I_percent_in_pack       IN     NUMBER,
                                  I_from_loc              IN     ITEM_LOC.LOC%TYPE,
                                  I_from_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                                  I_to_loc                IN     ITEM_LOC.LOC%TYPE,
                                  I_to_loc_type           IN     ITEM_LOC.LOC_TYPE%TYPE,
                                  I_intercompany          IN     BOOLEAN,
                                  I_franchise_ordret_ind  IN     VARCHAR2)
return BOOLEAN IS

   L_function               VARCHAR2(61) := 'TRANSFER_COST_SQL.GET_TSF_ALLOC_COST_PRICE';

   L_dept                   ITEM_MASTER.DEPT%TYPE;
   L_class                  ITEM_MASTER.CLASS%TYPE;
   L_subclass               ITEM_MASTER.SUBCLASS%TYPE;
   L_from_wac               ITEM_LOC_SOH.AV_COST%TYPE;
   L_franchise_cost         WF_ORDER_DETAIL.FIXED_COST%TYPE;
   L_franchise_currency     WF_ORDER_HEAD.CURRENCY_CODE%TYPE;
   L_to_loc_currency        STORE.CURRENCY_CODE%TYPE;
   L_restocking_cost        WF_RETURN_DETAIL.RETURN_UNIT_COST%TYPE;

BEGIN

   if I_distro_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_distro_no','NULL','NOT NULL');
      return FALSE;
   elsif I_distro_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_distro_type','NULL','NOT NULL');
      return FALSE;
   elsif I_franchise_ordret_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_franchise_ordret_ind','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_NONE then
      -- non-franchise transaction, get the transfer cost/price
      if I_distro_type in('T','B') then
         if GET_TSF_COST_PRICE(O_error_message,
                               O_tsf_cost_price,
                               O_total_wo_cost,
                               I_distro_no,
                               I_item,
                               I_pack_no,
                               I_percent_in_pack,
                               I_from_loc,
                               I_from_loc_type,
                               I_to_loc,
                               I_to_loc_type,
                               I_intercompany) = FALSE then
            return FALSE;
         end if;
      end if;

      -- Use the sending loc WAC converted to to-loc's currency for:
      -- 1) allocations
      -- 2) intercompany transfers if tsf_price was not found
      -- 3) intracompany transfers if tsf_cost was not found
      if O_tsf_cost_price is NULL then
         if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                           I_item,
                                           L_dept,
                                           L_class,
                                           L_subclass) = FALSE then
            return FALSE;
         end if;

         if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                       L_from_wac,
                                       I_item,
                                       L_dept,
                                       L_class,
                                       L_subclass,
                                       I_from_loc,
                                       I_from_loc_type,
                                       NULL,  -- tran_date
                                       I_to_loc,
                                       I_to_loc_type) = FALSE then
            return FALSE;
         end if;
         --convert L_from_wac into to-loc currency 
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_from_loc,
                                             I_from_loc_type,
                                             NULL,
                                             I_to_loc,
                                             I_to_loc_type,
                                             NULL,
                                             L_from_wac,
                                             O_tsf_cost_price,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      if I_franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
         -- Franchise returns transaction
         if I_pack_no is NOT NULL then
            if WF_RETURN_SQL.GET_RETURN_COST(O_error_message,
                                             L_franchise_cost,
                                             L_franchise_currency,
                                             L_restocking_cost,
                                             NULL,  --I_rma_no
                                             I_distro_no, 
                                             I_pack_no) = FALSE then
               return FALSE;
            end if;

            L_franchise_cost := L_franchise_cost * I_percent_in_pack;
         else
            if WF_RETURN_SQL.GET_RETURN_COST(O_error_message,
                                             L_franchise_cost,
                                             L_franchise_currency,
                                             L_restocking_cost,
                                             NULL,  --I_rma_no
                                             I_distro_no, 
                                             I_item) = FALSE then
               return FALSE;
            end if;
         end if;
      else  -- WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER
         -- Franchise order transaction
         if I_pack_no is NOT NULL then
            if WF_ORDER_SQL.GET_ORDER_ITEM_COST(O_error_message,
                                                L_franchise_cost,
                                                L_franchise_currency,
                                                I_distro_no,
                                                I_distro_type,
                                                I_to_loc,
                                                I_to_loc_type,
                                                I_pack_no) = FALSE then
               return FALSE;
            end if;

            L_franchise_cost := L_franchise_cost * I_percent_in_pack;
         else
            if WF_ORDER_SQL.GET_ORDER_ITEM_COST(O_error_message,
                                                L_franchise_cost,
                                                L_franchise_currency,
                                                I_distro_no,
                                                I_distro_type,
                                                I_to_loc,
                                                I_to_loc_type,
                                                I_item) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;

      --convert L_franchise_cost from franchise currency to to-loc's currency
      select currency_code into L_to_loc_currency
        from store
       where store = I_to_loc
         and I_to_loc_type = 'S'
       union all 
      select currency_code
        from wh
       where wh = I_to_loc
         and I_to_loc_type = 'W';

      if CURRENCY_SQL.CONVERT(O_error_message, 
                              L_franchise_cost,
                              L_franchise_currency,
                              L_to_loc_currency, 
                              O_tsf_cost_price,
                              'C',   --I_cost_retail_ind
                              NULL,  --I_effective_date
                              NULL,  --I_exchange_type
                              NULL,  --I_in_exchange_rate
                              NULL) = FALSE then  --I_out_exchange_rate
         return FALSE;
      end if;
   end if;  --I_franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_NONE

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TSF_ALLOC_COST_PRICE;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_COST_PRICE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_tsf_cost_price     IN OUT TSFDETAIL.TSF_PRICE%TYPE,
                            O_total_wo_cost      IN OUT TSF_ITEM_WO_COST.AVG_UNIT_COST%TYPE,
                            I_tsf_no             IN     TSFHEAD.TSF_NO%TYPE,
                            I_item               IN     ITEM_MASTER.ITEM%TYPE,
                            I_pack_no            IN     PACKITEM.PACK_NO%TYPE,
                            I_percent_in_pack    IN     NUMBER,
                            I_from_loc           IN     ITEM_LOC.LOC%TYPE,
                            I_from_loc_type      IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_to_loc             IN     ITEM_LOC.LOC%TYPE,
                            I_to_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_intercompany       IN     BOOLEAN)
return BOOLEAN IS

   L_function          VARCHAR2(60) := 'TRANSFER_COST_SQL.GET_TSF_COST_PRICE';

   --default to single leg
   L_tsf_leg           VARCHAR2(2) := 'N';
   L_child_tsf_no      TSFHEAD.TSF_NO%TYPE;
   L_child_to_loc      ITEM_LOC.LOC%TYPE;
   L_child_to_loc_type ITEM_LOC.LOC_TYPE%TYPE;

   --holds the transfer cost/price in from loc's currency
   L_new_cost_from     ITEM_LOC_SOH.AV_COST%TYPE := NULL;
   --holds the transfer cost/price in to loc's currency
   L_new_cost_to       ITEM_LOC_SOH.AV_COST%TYPE := NULL;

   cursor C_GET_TSF_PRICE(cv_item ITEM_MASTER.ITEM%TYPE) is
      select tsf_price
        from tsfdetail
       where tsf_no = I_tsf_no
         and item = cv_item;

   cursor C_GET_TSF_COST(cv_item ITEM_MASTER.ITEM%TYPE) is
      select tsf_cost
        from tsfdetail
       where tsf_no = I_tsf_no
         and item = cv_item;

BEGIN

   -- This function will return the transfer cost for an intra-company transfer
   -- or the transfer price for an inter-company transfer.
   -- It will also return the total work order cost for an item if it is a second leg transfer.
   -- Both O_tsf_cost_price and O_total_wo_cost will be returned in to loc's currency.

   --determine if tsf_no is for single leg-N, first leg-F, or second leg-S
   if TRANSFER_SQL.CHECK_FOR_CHILD_TSF(O_error_message,
                                       L_tsf_leg,
                                       L_child_tsf_no,
                                       L_child_to_loc,
                                       L_child_to_loc_type,
                                       I_tsf_no) = FALSE then
      return FALSE;
   end if;

   if I_intercompany then

      if L_tsf_leg = 'F' then
         --if transfer is a parent, transfer price for item is on tsf_detail, in from loc currency.
         if I_pack_no is NOT NULL then
            open C_GET_TSF_PRICE(I_pack_no);
            fetch C_GET_TSF_PRICE into L_new_cost_from;
            close C_GET_TSF_PRICE;

            if L_new_cost_from is NOT NULL then
               L_new_cost_from := L_new_cost_from * I_percent_in_pack;
            end if;
         else
            open C_GET_TSF_PRICE(I_item);
            fetch C_GET_TSF_PRICE into L_new_cost_from;
            close C_GET_TSF_PRICE;
         end if;
      else
         --I_tsf_no is for a child tsf or single leg tsf.
         --If found tsf_item_cost Avg price is in I_to_loc's currency
         if GET_TSF_AVG_PRICE(O_error_message,
                              L_new_cost_to,
                              I_tsf_no,
                              NULL,
                              I_item) = FALSE then
            return FALSE;
         end if;
      end if; --tsf_leg = F
      if L_new_cost_to is null then
         if I_pack_no is NOT NULL then
            open C_GET_TSF_PRICE(I_pack_no);
            fetch C_GET_TSF_PRICE into L_new_cost_from;
            close C_GET_TSF_PRICE;

            if L_new_cost_from is NOT NULL then
               L_new_cost_from := L_new_cost_from * I_percent_in_pack;
            end if;
         else
            open C_GET_TSF_PRICE(I_item);
            fetch C_GET_TSF_PRICE into L_new_cost_from;
            close C_GET_TSF_PRICE;
         end if;
      end if;    
   else
      -- intracompany, transfer cost for item is on tsf_detail, in from loc currency.
      if I_pack_no is NOT NULL then
         open C_GET_TSF_COST(I_pack_no);
         fetch C_GET_TSF_COST into L_new_cost_from;
         close C_GET_TSF_COST;

         if L_new_cost_from is NOT NULL then
            L_new_cost_from := L_new_cost_from * I_percent_in_pack;
         end if;
      else
         open C_GET_TSF_COST(I_item);
         fetch C_GET_TSF_COST into L_new_cost_from;
         close C_GET_TSF_COST;
      end if;
   end if;  --intercompany

   -- O_tsf_cost_price should be returned in to loc currency
   if L_new_cost_from is NOT NULL then
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          I_from_loc,
                                          I_from_loc_type,
                                          NULL,
                                          I_to_loc,
                                          I_to_loc_type,
                                          NULL,
                                          L_new_cost_from,
                                          O_tsf_cost_price,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
   elsif L_new_cost_to is NOT NULL then
      O_tsf_cost_price := L_new_cost_to;
   else
      O_tsf_cost_price := NULL;  -- default
   end if;

   --If the transfer is the second leg of a tsf with finishing, get the sum of the
   --activity costs that update inventory cost. They apply to the second leg only
   if L_tsf_leg = 'S' then
      if GET_TOTAL_WO_UPD_INV_COSTS(O_error_message,
                                    O_total_wo_cost,
                                    NULL,  --tsf_item_cost_id
                                    I_tsf_no,
                                    I_item) = FALSE then
         return FALSE;
      end if;
   else
      O_total_wo_cost := 0;  -- default
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GET_TSF_COST_PRICE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_TSF_COST_PRICE;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_AVG_PRICE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_avg_price     IN OUT TSF_ITEM_COST.TSF_AVG_PRICE%TYPE,
                           I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                           I_tsf_parent_no IN     TSFHEAD.TSF_NO%TYPE,
                           I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN IS

   cursor C_AVG_PRICE_CHILD is
      select tsf_avg_price
        from tsf_item_cost
       where item = I_item
         and tsf_no = I_tsf_no;

   cursor C_AVG_PRICE_PARENT is
      select tsf_avg_price
        from tsf_item_cost
       where item = I_item
         and tsf_parent_no = I_tsf_parent_no;

BEGIN

   if I_tsf_no IS NULL and I_tsf_parent_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_tsf_no','NULL','NOT NULL');
      return FALSE;
   elsif I_item IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_item','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_tsf_no is NOT NULL then
      open C_AVG_PRICE_CHILD;
      fetch C_AVG_PRICE_CHILD into O_avg_price;
      close C_AVG_PRICE_CHILD;
   else
      open C_AVG_PRICE_PARENT;
      fetch C_AVG_PRICE_PARENT into O_avg_price;
      close C_AVG_PRICE_PARENT;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GET_TSF_AVG_PRICE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_TSF_AVG_PRICE;
---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_WO_UPD_INV_COSTS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_sum_wo_uicosts   IN OUT TSF_ITEM_WO_COST.AVG_UNIT_COST%TYPE,
                                    I_tsf_item_cost_id IN     TSF_ITEM_WO_COST.TSF_ITEM_COST_ID%TYPE,
                                    I_tsf_no           IN     TSFHEAD.TSF_NO%TYPE,
                                    I_item             IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN is

   cursor C_SUM_UPD_INV_COSTS is
      select nvl(sum(avg_unit_cost), 0)
        from tsf_item_cost ic,
             tsf_item_wo_cost wc,
             wo_activity wa
       where ic.tsf_no = I_tsf_no
         and ic.item   = I_item
         and ic.tsf_item_cost_id = wc.tsf_item_cost_id
         and wc.activity_id = wa.activity_id
         and wa.cost_type = 'U';

   cursor C_SUM_UPD_INV_CSTS_ID is
      select nvl(sum(avg_unit_cost), 0)
        from tsf_item_wo_cost wc,
             wo_activity wa
       where wc.tsf_item_cost_id = I_tsf_item_cost_id
         and wc.activity_id = wa.activity_id
         and wa.cost_type = 'U';


BEGIN

   if I_tsf_item_cost_id is NOT NULL then
      open C_SUM_UPD_INV_CSTS_ID;
      fetch C_SUM_UPD_INV_CSTS_ID into O_sum_wo_uicosts;
      close C_SUM_UPD_INV_CSTS_ID;
  else
     open C_SUM_UPD_INV_COSTS;
     fetch C_SUM_UPD_INV_COSTS into O_sum_wo_uicosts;
     close C_SUM_UPD_INV_COSTS;
  end if;

return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GET_TOTAL_WO_UPD_INV_COSTS',
                                            to_char(SQLCODE));
      return FALSE;

END GET_TOTAL_WO_UPD_INV_COSTS;
---------------------------------------------------------------------------------------------
END TRANSFER_COST_SQL;
/