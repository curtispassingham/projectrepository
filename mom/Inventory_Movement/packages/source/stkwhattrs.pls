
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STKWHATTR_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name:   CHECK_REPLENISHMENT_WH_EXISTS
--Purpose      :   Returns True if WH is replenishment WH.
--------------------------------------------------------------------------------
FUNCTION CHECK_REPLENISHMENT_WH_EXISTS    (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                           O_exist                 IN OUT BOOLEAN,
                                           I_vwh                   IN WH.WH%TYPE,
                                           I_pwh                   IN WH.WH%TYPE)         
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   CHECK_ROUNDING_WH_EXISTS
--Purpose      :   Returns True if virtual wh associated with physical WH.
--------------------------------------------------------------------------------
FUNCTION CHECK_ROUNDING_WH_EXISTS   (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exist                 IN OUT BOOLEAN,
                                     I_vwh                   IN WH.WH%TYPE,
                                     I_pwh                   IN WH.WH%TYPE)         
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name:   CHECK_INVESTMENT_BUY_WH_EXISTS
--Purpose      :   Returns True if WH is investment buy WH.
--------------------------------------------------------------------------------
FUNCTION CHECK_INVESTMENT_BUY_WH_EXISTS   (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                           O_exist                 IN OUT BOOLEAN,
                                           I_vwh                   IN WH.WH%TYPE,
                                           I_pwh                   IN WH.WH%TYPE)         
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name:   CHECK_SOURCE_ORDER_EXISTS
--Purpose      :   Returns True if repl source order is found.
--------------------------------------------------------------------------------

FUNCTION CHECK_SOURCE_ORDER_EXISTS     (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_exist                 IN OUT BOOLEAN,
                                        I_vwh                   IN WH.WH%TYPE,
                                        I_pwh                   IN WH.WH%TYPE,
                                        I_repl_wh_link          IN WH.REPL_WH_LINK%TYPE,
                                        I_source_order          IN WH.REPL_SRC_ORD%TYPE)         
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
END STKWHATTR_SQL;
/
