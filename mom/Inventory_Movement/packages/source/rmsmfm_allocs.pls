
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_ALLOC AUTHID CURRENT_USER AS

FAMILY         CONSTANT  VARCHAR2(30) := 'alloc';

HDR_ADD        CONSTANT  VARCHAR2(30) := 'AllocCre';
HDR_UPD        CONSTANT  VARCHAR2(30) := 'AllocHdrMod';
HDR_DEL        CONSTANT  VARCHAR2(30) := 'AllocDel';
DTL_ADD        CONSTANT  VARCHAR2(30) := 'AllocDtlCre';
DTL_UPD        CONSTANT  VARCHAR2(30) := 'AllocDtlMod';
DTL_DEL        CONSTANT  VARCHAR2(30) := 'AllocDtlDel';
--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_msg                 OUT VARCHAR2,
                I_message_type          IN      ALLOC_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_alloc_no              IN      ALLOC_HEADER.ALLOC_NO%TYPE,
                I_alloc_header_status   IN      ALLOC_HEADER.STATUS%TYPE,
                I_to_loc                IN      ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1);
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT);
--------------------------------------------------------------------------------
END RMSMFM_ALLOC;
/

