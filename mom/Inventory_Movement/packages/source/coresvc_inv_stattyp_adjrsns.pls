CREATE OR REPLACE PACKAGE CORESVC_INV_STAT_TYP_ADJ_RSN AUTHID CURRENT_USER AS
   
   template_key           CONSTANT VARCHAR2(255)         := 'INV_STATUS_ADJ_REASON_DATA';
   template_category               CODE_DETAIL.CODE%TYPE := 'RMSINV';
   
   action_new                      VARCHAR2(25)          := 'NEW';
   action_mod                      VARCHAR2(25)          := 'MOD';
   action_del                      VARCHAR2(25)          := 'DEL';
   action_column                   VARCHAR2(255)         := 'ACTION';
   sheet_name_trans                S9T_PKG.TRANS_MAP_TYP;
   
   INV_ST_TYPS_TL_SHEET            VARCHAR2(255)         := 'INV_STATUS_TYPES';
   INV_ST_TYPS_TL$ACTION           NUMBER                := 1;
   INV_ST_TYPS_TL$INV_STATUS_DESC  NUMBER                := 4;
   INV_ST_TYPS_TL$INV_STATUS       NUMBER                := 2;
   
   IS_TYPES_LANG_sheet             VARCHAR2(255)         := 'INV_STATUS_TYPES_TL';
   IS_TYPES_LANG$ACTION            NUMBER                := 1;
   IS_TYPES_LANG$LANG              NUMBER                := 2;
   IS_TYPES_LANG$INV_STATUS        NUMBER                := 3;
   IS_TYPES_LANG$INV_STATUS_DESC   NUMBER                := 4;
                                                        
   INV_STATUS_CODES_TL_SHEET       VARCHAR2(255)         := 'INV_STATUS_CODES';
   INV_ST_CODES_TL$ACTION          NUMBER                :=1;
   INV_ST_CODES_TL$INV_ST_CD_DESC  NUMBER                :=4;
   INV_ST_CODES_TL$INV_ST_CODE     NUMBER                :=2;
   INV_ST_CODES_TL$INV_STATUS      NUMBER                :=3;
                                                        
   IS_CODES_LANG_sheet             VARCHAR2(255)         := 'INV_STATUS_CODES_TL';
   IS_CODES_LANG$ACTION            NUMBER                := 1;
   IS_CODES_LANG$LANG              NUMBER                := 2;
   IS_CODES_LANG$INV_STATUS        NUMBER                := 3;
   IS_CODES_LANG$INV_STATUS_CODE   NUMBER                := 4;
   IS_CODES_LANG$IS_CODE_DESC      NUMBER                := 5;
   
   INV_ADJ_REASON_sheet            VARCHAR2(25)          := 'INV_ADJ_REASON';
   INV_ADJ_REASON$Action           NUMBER                := 1;
   INV_ADJ_REASON$COGS_IND         NUMBER                := 4;
   INV_ADJ_REASON$REASON           NUMBER                := 2;
   INV_ADJ_REASON$REASON_DESC      NUMBER                := 3;
   
   INV_ADJ_RSN_TL_sheet            VARCHAR2(25)          := 'INV_ADJ_REASON_TL';
   INV_ADJ_RSN_TL$Action           NUMBER                := 1;
   INV_ADJ_RSN_TL$LANG             NUMBER                := 2;
   INV_ADJ_RSN_TL$REASON           NUMBER                := 3;
   INV_ADJ_RSN_TL$REASON_DESC      NUMBER                := 4;

   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)

   RETURN BOOLEAN;
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name      IN       VARCHAR2)
   RETURN VARCHAR2;
END CORESVC_INV_STAT_TYP_ADJ_RSN;
/