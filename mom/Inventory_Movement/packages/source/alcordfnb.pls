CREATE OR REPLACE PACKAGE BODY ALC_ORDER_FINALIZE_SQL AS
---------------------------------------------------------------------
FUNCTION WHERE_CLAUSE (O_error_message       IN OUT VARCHAR2,
                       O_out_where_clause    IN OUT VARCHAR2,
                       I_start_variance_pct  IN     NUMBER,
                       I_end_variance_pct    IN     NUMBER)
RETURN BOOLEAN IS

   L_order_no                  ALC_HEAD.ORDER_NO%TYPE;
   L_shipment                  ALC_HEAD.SHIPMENT%TYPE;
   L_seq_no                    ALC_HEAD.SEQ_NO%TYPE;
   L_item                      ALC_HEAD.ITEM%TYPE;
   L_pack_item                 ALC_HEAD.PACK_ITEM%TYPE;
   L_total_elc_item            ALC_COMP_LOC.ACT_VALUE%TYPE;
   L_total_alc_item            ALC_COMP_LOC.ACT_VALUE%TYPE;
   L_total_elc_order           ALC_COMP_LOC.ACT_VALUE%TYPE := 0;
   L_total_alc_order           ALC_COMP_LOC.ACT_VALUE%TYPE := 0;
   L_variance_percent          ALC_HEAD.ALC_QTY%TYPE;
   L_unit_elc                  ALC_COMP_LOC.ACT_VALUE%TYPE;
   L_unit_alc                  ALC_COMP_LOC.ACT_VALUE%TYPE;
   L_percent                   NUMBER;
   L_exists                    VARCHAR2(1);
   L_program                   VARCHAR2(64) := 'ALC_ORDER_FINALIZE_SQL.WHERE_CLAUSE';

   cursor C_GET_ALL_FIELDS is
      select order_no,
             shipment,
             seq_no,
             item,
             pack_item
        from alc_head_temp
       order by order_no, seq_no
         for update nowait;

   cursor C_ORDER_EXISTS is
      select 'x'
        from alc_head_temp
       where order_no = L_order_no
         and NVL(shipment,0) = NVL(L_shipment,NVL(shipment,0))
         and processed_ind = 'N';

BEGIN

   for C_all_fields in C_GET_ALL_FIELDS LOOP
      L_exists           := NULL;
      L_order_no         := C_all_fields.order_no;
      L_shipment         := C_all_fields.shipment;
      L_seq_no           := C_all_fields.seq_no;
      L_item             := C_all_fields.item;
      L_pack_item        := C_all_fields.pack_item;
      ---
      if ALC_SQL.GET_ORDER_ITEM_TOTALS (O_error_message,
                                        L_unit_elc,
                                        L_unit_alc,
                                        L_total_elc_item,
                                        L_total_alc_item,
                                        L_percent,
                                        L_order_no,
                                        L_shipment,
                                        L_item,
                                        L_pack_item) = FALSE then
         return FALSE;
      end if;
      ---
      L_total_elc_order := L_total_elc_order + L_total_elc_item;
      L_total_elc_item  := NULL;
      L_total_alc_order := L_total_alc_order + L_total_alc_item;
      L_total_alc_item  := NULL;
      ---
      update alc_head_temp
         set processed_ind = 'Y'
       where order_no = L_order_no
         and shipment = L_shipment
         and seq_no   = L_seq_no
         and item     = L_item
         and ((pack_item = L_pack_item
               and L_pack_item is NOT NULL
               and pack_item is NOT NULL)
          or (L_pack_item is NULL
              and pack_item is NULL));
      ---
      open  C_ORDER_EXISTS;
      fetch C_ORDER_EXISTS into L_exists;
      close C_ORDER_EXISTS;
      ---
      if L_exists is NULL then
         if ALC_SQL.CALC_PERCENT_VARIANCE (O_error_message,
                                           L_variance_percent,
                                           L_total_alc_order,
                                           L_total_elc_order) = FALSE then
             return FALSE;
         end if;
         ---
         update alc_head_temp
            set variance_pct = L_variance_percent
          where order_no = L_order_no
            and shipment = L_shipment
            and seq_no   = L_seq_no
            and item     = L_item
            and ((pack_item = L_pack_item
                  and L_pack_item is NOT NULL
                  and pack_item is NOT NULL)
             or (L_pack_item is NULL
                 and pack_item is NULL));
         ---
         L_total_elc_order  := 0;
         L_total_alc_order  := 0;
         L_variance_percent := NULL;
      end if;
   END LOOP;
   ---
   if I_start_variance_pct is not NULL or I_end_variance_pct is not NULL then
      O_out_where_clause := 'where variance_pct between ' || NVL(I_start_variance_pct,0) || ' and ' || NVL(I_end_variance_pct,100);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END WHERE_CLAUSE;
----------------------------------------------------------------------
FUNCTION ALC_FINALIZE (O_error_message      IN OUT VARCHAR2,
                       I_order_no           IN     ORDHEAD.ORDER_NO%TYPE,
                       I_shipment           IN     SHIPMENT.SHIPMENT%TYPE,
                       I_update_wac_ind     IN     VARCHAR2,
                       I_status             IN     ALC_HEAD.STATUS%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ALC_ORDER_FINALIZE_SQL.ALC_FINALIZE';

   cursor C_GET_ITEM_INFO is
      SELECT DISTINCT a.item,
                      a.pack_item
        FROM v_alc_head a
       WHERE a.order_no = I_order_no
         AND NVL(a.shipment,0)=NVL(I_shipment,NVL(a.shipment,0))
         AND a.status='P'
         AND (obligation_key IS NOT NULL OR ce_id IS NOT NULL);


BEGIN

   if I_order_no is NOT NULL and
      I_update_wac_ind is NOT NULL then
      ---
      FOR C_item IN C_GET_ITEM_INFO LOOP
      --
         if ALC_SQL.UPDATE_STKLEDGR (O_error_message,
                                     I_order_no,
                                     C_item.item,
                                     C_item.pack_item,
                                     I_shipment,
                                     NULL) = FALSE then
            return FALSE;
         end if;
         ---
      END LOOP;
      ---
      if CE_SQL.UPDATE_ORD_ITEM_ALC_STATUS (O_error_message,
                                           'R',
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            I_order_no,
                                            NULL) = FALSE then
         return FALSE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ALC_FINALIZE;

------------------------------------------------------------------
FUNCTION GET_ALC_STATUS (O_error_message    IN OUT VARCHAR2,
                         O_alc_status       IN OUT ALC_HEAD.STATUS%TYPE,
                         I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                         I_shipment         IN     SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'ALC_ORDER_FINALIZE_SQL.GET_ALC_STATUS';
   --
   cursor C_STATUS is
      select po_alc_status
        from v_alc_head
       where order_no = I_order_no
         and (shipment = I_shipment and I_shipment is not null 
              or shipment is null and I_shipment is null);

BEGIN

   O_alc_status := 'E';
   open  C_STATUS;
   fetch C_STATUS into O_alc_status;
   close C_STATUS;

   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ALC_STATUS;
----------------------------------------------------------------------
FUNCTION UPDATE_ALC_STATUS (O_error_message    IN OUT VARCHAR2,
                I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                I_status           IN     ALC_HEAD.STATUS%TYPE,
                I_alc_seq_no       IN   ALC_HEAD.SEQ_NO%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ALC_ORDER_FINALIZE_SQL.UPDATE_ALC_STATUS';
   L_table              VARCHAR2(64) := 'ALC_HEAD';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_ALC_HEAD is
      select 'x'
        from alc_head
       where order_no = I_order_no
         and status = 'P'
         and NVL(shipment,0) = NVL(I_shipment,NVL(shipment,0))
         and seq_no = I_alc_seq_no
         for update nowait;

BEGIN
   if I_order_no is NOT NULL and I_status is NOT NULL then
      open C_lock_alc_head;
      close C_lock_alc_head;
      ---
      update alc_head
         set status = I_status
       where order_no = I_order_no
         and status   = 'P'
         and NVL(shipment,0) = NVL(I_shipment,NVL(shipment,0))
         and seq_no          = I_alc_seq_no;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                               L_program,
                               NULL,
                               NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ALC_STATUS;
----------------------------------------------------------------------
FUNCTION INSERT_ALC_HEAD_TEMP (O_error_message        IN OUT  VARCHAR2,
                               I_from_var             IN      NUMBER,
                               I_to_var               IN      NUMBER,
                               I_order_no             IN      ORDHEAD.ORDEr_NO%TYPE,
                               I_asn                  IN      SHIPMENT.ASN%TYPE,
                               I_item                 IN      V_ALC_HEAD.ITEM%TYPE,
                               I_pack_item            IN      V_ALC_HEAD.PACK_ITEM%TYPE,
                               I_obligation_key       IN      V_ALC_HEAD.OBLIGATION_KEY%TYPE,
                               I_vessel_id            IN      V_ALC_HEAD.VESSEL_ID%TYPE,
                               I_voyage_flt_id        IN      V_ALC_HEAD.VOYAGE_FLT_ID%TYPE,
                               I_estimated_depart     IN      V_ALC_HEAD.ESTIMATED_DEPART_DATE%TYPE,
                               I_alc_status           IN      V_ALC_HEAD.PO_ALC_STATUS%TYPE,
                               I_entry_no             IN      CE_HEAD.ENTRY_NO%TYPE,
                               I_order_status         IN      ORDHEAD.STATUS%TYPE,
                               I_mode                 IN      VARCHAR2,
                               O_where_clause         IN OUT  VARCHAR2,
                               O_rec_inserted         IN OUT  BOOLEAN)

   RETURN BOOLEAN IS
   
   L_program            VARCHAR2(64) := 'ALC_ORDER_FINALIZE_SQL.INSERT_ALC_HEAD_TEMP';
  
BEGIN
   if I_from_var is NOT NULL or 
      I_to_var is NOT NULL then
         insert into alc_head_temp (order_no, 
                                    shipment,
                                    seq_no,
                                    item, 
                                    pack_item,
                                    processed_ind)
                                   (select ah.order_no,
                                           ah.shipment,
                                           ah.seq_no,
                                           ah.item,
                                           ah.pack_item,
                                           'N'
                                      from v_alc_head ah
                                     where ah.status != 'E'                -- Excluding ALC records which are in 'E'stimated status.
                                       and ((ah.order_no = I_order_no
                                             and I_order_no is NOT NULL)
                                        or I_order_no is NULL)
                                       and ((ah.shipment in(select shipment from shipment where asn= I_asn)
                                             and I_asn is NOT NULL)
                                        or I_asn is NULL)
                                       and ((ah.item = I_item
                                             and I_item is NOT NULL)
                                        or I_item is NULL)
                                       and ((ah.pack_item = I_pack_item
                                           and I_pack_item is NOT NULL)
                                        or I_pack_item is NULL)
                                       and ((ah.obligation_key = I_obligation_key
                                           and I_obligation_key is NOT NULL)
                                        or I_obligation_key is NULL)
                                       and ((ah.vessel_id = I_vessel_id
                                           and I_vessel_id is NOT NULL)
                                        or I_vessel_id is NULL)
                                       and ((ah.voyage_flt_id = I_voyage_flt_id
                                           and I_voyage_flt_id is NOT NULL)
                                        or I_voyage_flt_id is NULL)
                                       and ((ah.estimated_depart_date = I_estimated_depart
                                           and I_estimated_depart is NOT NULL)
                                        or I_estimated_depart is NULL)
                                       and ((exists (select 'x'
                                                       from ce_head ch,
                                                            ce_ord_item ci
                                                      where ch.ce_id = ci.ce_id
                                                        and ch.entry_no = I_entry_no
                                                        and ci.order_no = ah.order_no
                                                        and ci.vessel_id = ah.vessel_id
                                                        and ci.voyage_flt_id = ah.voyage_flt_id
                                                        and ci.estimated_depart_date = ah.estimated_depart_date)
                                       and I_entry_no is NOT NULL)
                                        or I_entry_no is NULL)
                                       and ((exists (select 'x'
                                                       from ordhead oh
                                                      where oh.status = I_order_status
                                                        and oh.order_no = ah.order_no)
                                       and I_order_status is NOT NULL)
                                        or I_order_status is NULL)
                                       and ((ah.po_alc_status = I_alc_status
                                             and I_alc_status IS NOT NULL)
                                            or I_alc_status IS NULL));

      if SQL%ROWCOUNT > 0 then
         O_rec_inserted := TRUE;
      end if;

      if ALC_ORDER_FINALIZE_SQL.WHERE_CLAUSE (O_error_message,
                                              O_where_clause,
                                              I_from_var,
                                              I_to_var) = FALSE then
           return FALSE;
      end if;
   else
      ---
       insert into alc_head_temp (order_no, shipment)
                                 (select distinct ah.order_no, ah.shipment
                                    from v_alc_head ah
                                   where ah.status != 'E'                     -- Excluding ALC records which are in 'E'stimated status.
                                     and ((ah.order_no = I_order_no
                                           and I_order_no is NOT NULL)
                                      or I_order_no is NULL)
                                     and ((ah.shipment in(select shipment from shipment where asn= I_asn)
                                           and I_asn is NOT NULL)
                                      or I_asn is NULL)
                                     and ((ah.item = I_item
                                           and I_item is NOT NULL)
                                      or I_item is NULL)
                                     and ((ah.pack_item = I_pack_item
                                         and I_pack_item is NOT NULL)
                                      or I_pack_item is NULL)
                                     and ((ah.obligation_key = I_obligation_key
                                         and I_obligation_key is NOT NULL)
                                      or I_obligation_key is NULL)
                                     and ((ah.vessel_id = I_vessel_id
                                         and I_vessel_id is NOT NULL)
                                      or I_vessel_id is NULL)
                                     and ((ah.voyage_flt_id = I_voyage_flt_id
                                         and I_voyage_flt_id is NOT NULL)
                                      or I_voyage_flt_id is NULL)
                                     and ((ah.estimated_depart_date = I_estimated_depart
                                         and I_estimated_depart is NOT NULL)
                                      or I_estimated_depart is NULL)
                                     and ((exists (select 'x'
                                                     from ce_head ch,
                                                          ce_ord_item ci
                                                    where ch.ce_id = ci.ce_id
                                                      and ch.entry_no = I_entry_no
                                                      and ci.order_no = ah.order_no
                                                      and ci.vessel_id = ah.vessel_id
                                                      and ci.voyage_flt_id = ah.voyage_flt_id
                                                      and ci.estimated_depart_date = ah.estimated_depart_date)
                                     and I_entry_no is NOT NULL)
                                      or I_entry_no is NULL)
                                     and ((exists (select 'x'
                                                        from ordhead oh
                                                       where oh.status = I_order_status
                                                         and oh.order_no = ah.order_no)
                                     and I_order_status is NOT NULL)
                                      or I_order_status is NULL)
                                     and ((ah.po_alc_status = I_alc_status
                                           and I_alc_status IS NOT NULL)
                                          or I_alc_status IS NULL));

      if SQL%ROWCOUNT > 0 then
         O_rec_inserted := TRUE;
      end if;
      O_where_clause   :=  'where order_no in ( select distinct ah.order_no  from v_alc_head ah '|| 
                                                ' where ah.order_no = nvl('''|| I_order_no ||''',ah.order_no) '|| 
                                                 
                                                '   and ah.item = nvl('''|| I_item ||''',ah.item) ' || 
                                                '   and nvl(ah.pack_item,-999) = nvl('''|| I_pack_item ||''',nvl(ah.pack_item,-999)) '|| 
											    '   and nvl(ah.obligation_key,-999) = nvl('''|| I_obligation_key ||''',nvl(ah.obligation_key,-999)) '|| 
												'   and nvl(ah.vessel_id,''-999'') = nvl('''|| I_vessel_id ||''',nvl(ah.vessel_id,''-999'')) '|| 
											    '   and nvl(ah.voyage_flt_id,''-999'') = nvl('''|| I_voyage_flt_id ||''',nvl(ah.voyage_flt_id,''-999'')) '|| 
											    '   and nvl(to_char(ah.estimated_depart_date,''YYMMDD''),''991231'') = nvl(to_char(to_timestamp(to_char(''' || to_char(I_estimated_depart) || ''')), ''YYMMDD''), nvl(to_char(ah.estimated_depart_date,''YYMMDD''),''991231'')) ' ; 
                                                
		if I_asn is NOT NULL then
			O_where_clause   :=  O_where_clause  || '   and exists (select 1 from shipment sh where sh.asn = '''||I_asn||''' )';
		end if;
												
		if I_alc_status is NOT NULL then
			O_where_clause   :=  O_where_clause  || '   and ah.po_alc_status = '''||I_alc_status||''' '; 
		end if;
												
		if I_order_status is NOT NULL then 
			O_where_clause   :=  O_where_clause  || '   and exists (select 1 from ordhead oh where oh.status = '''||I_order_status||''' and oh.order_no = ah.order_no) ';	
		end if; 
                                                 
		if I_entry_no is NOT NULL then 
		    O_where_clause   :=  O_where_clause  || '   and exists (select 1 from ce_head ch, ce_ord_item ci where ch.ce_id = ci.ce_id and ch.entry_no= '''||I_entry_no ||''' '||
		   '   and ci.order_no = ah.order_no and ci.vessel_id = ah.vessel_id and ci.voyage_flt_id = ah.voyage_flt_id and ci.estimated_depart_date=ah.estimated_depart_date) ';
		end if;
										
		if O_where_clause is NOT NULL then 
			O_where_clause   :=  O_where_clause  || ' ) ';
		end if;

   end if;
   ---
   update alc_head_temp aht
      set aht.asn= (select asn 
                      from shipment sh 
                     where sh.shipment = aht.shipment);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_ALC_HEAD_TEMP;
----------------------------------------------------------------------
FUNCTION DELETE_ALC_HEAD_TEMP (O_error_message        IN OUT    VARCHAR2)

   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ALC_ORDER_FINALIZE_SQL.DELETE_ALC_HEAD_TEMP';
   L_table              VARCHAR2(64) := 'ALC_HEAD_TEMP';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_ALC_HEAD_TEMP is
      select 'x'
        from alc_head_temp
         for update nowait;

BEGIN
   open  C_LOCK_ALC_HEAD_TEMP;
   close C_LOCK_ALC_HEAD_TEMP;
   ---
   delete from alc_head_temp;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_ALC_HEAD_TEMP;
----------------------------------------------------------------------
FUNCTION NO_FINALIZATION (O_error_message    IN OUT VARCHAR2,
                          I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                          I_shipment         IN     SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ALC_ORDER_FINALIZE_SQL.NO_FINALIZATION';
   L_table              VARCHAR2(64) := 'ALC_HEAD';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_ALC_HEAD is
      select 'x'
        from alc_head
       where order_no = I_order_no
         and NVL(shipment, -1) = NVL(I_shipment, NVL(shipment, -1))
         and status = 'P'
         and (obligation_key IS NOT NULL 
              OR ce_id IS NOT NULL)
         for update nowait;

BEGIN
   if I_order_no is NOT NULL then
      open C_lock_alc_head;
      close C_lock_alc_head;
      ---
      update alc_head
         set status = 'N'
       where order_no = I_order_no
         and NVL(shipment, -1) = NVL(I_shipment, NVL(shipment, -1))
         and status = 'P'
         and (obligation_key IS NOT NULL 
              OR ce_id IS NOT NULL);
      ---
      if sql%rowcount = 0 then
         if not ALC_ALLOC_SQL.INSERT_ELC_COMPS(O_error_message,
                                               I_order_no) then
            return FALSE;
         end if;
         ---
         update alc_head
            set status = 'N'
          where order_no = I_order_no
            and NVL(shipment, -1) = NVL(I_shipment, NVL(shipment, -1))
            and status = 'P'
            and (obligation_key IS NOT NULL 
                 OR ce_id IS NOT NULL);
      end if;
      ---
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END NO_FINALIZATION;
----------------------------------------------------------------------
END ALC_ORDER_FINALIZE_SQL;
/
