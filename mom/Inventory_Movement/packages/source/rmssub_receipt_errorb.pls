CREATE OR REPLACE PACKAGE BODY RMSSUB_RECEIPT_ERROR AS

-------------------------------------------------------------------------------
-- Usage:
--
-- In the receiving process, call INIT before processing any receipts.  This
-- will initialize all of the global variables.
--
-- For each receipt record, call BEGIN_RECEIPT.  This function will hold the
-- header level values in global variables which may be used to build an error
-- record when necessary.
--
-- When an error is encountered in the receiving process, call ADD_ERROR.  This
-- function save the error information in a temp PL/SQL table.
--
-- When an entire receipt has been processed, call END_RECEIPT.  This function
-- will go through the temp table and group all similar errors (error type).
-- It then creates a complete receipt message for each error type, adding the
-- similar errors as detail nodes.  Then it creates an error object for each
-- receipt message and adds the error object to the global error table.
--
-- At the end of the receiving process, call FINISH.  This will pass out a copy
-- of the global error table (if any errors exist) which is then sent to the
-- RIB for further processing.
--
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- GLOBAL TYPES
-------------------------------------------------------------------------------
TYPE temp_error_rec is RECORD
     (error_type    VARCHAR2(25),
      error_code    VARCHAR2(25),
      error_desc    VARCHAR2(4000),
      error_object  RIB_OBJECT,
      error_level   VARCHAR2(10));

TYPE temp_error_tbl is TABLE of temp_error_rec;


-------------------------------------------------------------------------------
-- GLOBAL VARIABLES
-------------------------------------------------------------------------------
LP_message_family        VARCHAR2(10) := 'receiving';
LP_error_application     VARCHAR2(10) := 'RMS';
LP_message_type          VARCHAR2(20);
LP_vdate                 DATE;

LP_error_tbl             RIB_ERROR_TBL;

LP_temp_error_tbl        TEMP_ERROR_TBL;

LP_receiptdesc_rec       "RIB_ReceiptDesc_REC";
LP_receipt_rec           "RIB_Receipt_REC";
LP_receipt_tbl           "RIB_Receipt_TBL";

LP_cached_bol_no         SHIPMENT.BOL_NO%TYPE      := NULL;
LP_from_loc              SHIPMENT.FROM_LOC%TYPE      := NULL;
LP_from_loc_type         SHIPMENT.FROM_LOC_TYPE%TYPE := NULL;


-------------------------------------------------------------------------------
-- PRIVATE FUNCTION PROTOTYPE: GET_MESSAGE_KEY
-- This function will get the message key from a SQL_LIB error message (those
-- errors which contain parameters in the form of @0, @1, etc.).  If the error
-- message is just text without any parameters, the entire message will be
-- passed back out as the key.  This key will be used to compare and group
-- errors when building receipt messages for error objects.
-------------------------------------------------------------------------------
FUNCTION GET_MESSAGE_KEY(I_message  IN  VARCHAR2)
RETURN VARCHAR2;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
FUNCTION INIT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
              I_message_type   IN      VARCHAR2)
RETURN BOOLEAN IS

   L_function  VARCHAR2(61) := 'RMSSUB_RECEIPT_ERROR.INIT';

BEGIN

   -- Check for required input
   if I_message_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_function,
                                            'I_message_type',
                                            'NULL');
      return FALSE;
   end if;

   -- Set globals
   LP_message_type       := I_message_type;
   LP_vdate              := GET_VDATE;
   LP_cached_bol_no      := NULL;
   LP_from_loc           := NULL;
   LP_from_loc_type      := NULL;

   -- Initialize the error table
   if LP_error_tbl is NULL then
      LP_error_tbl := RIB_ERROR_TBL();
   else
      LP_error_tbl.DELETE;
   end if;

   -- Initialize the temp error table
   if LP_temp_error_tbl is NULL then
      LP_temp_error_tbl := TEMP_ERROR_TBL();
   else
      LP_temp_error_tbl.DELETE;
   end if;

   -- Create a receipt record element to hold the receipt info
   if LP_receipt_rec is NULL then
      LP_receipt_rec := "RIB_Receipt_REC"(NULL,   -- rib OID
                                        NULL,   -- dc_dest_id
                                        NULL,   -- po_nbr
                                        NULL,   -- document_type
                                        NULL,   -- asn_nbr
                                        NULL,   -- receiptdtl_tbl
                                        NULL,   -- receiptcartondtl_tbl
                                        NULL,   -- receipt_type
                                        NULL,   -- from_loc
                                        NULL);  -- from_loc_type
   else
      -- Receipt record exists so clean it out
      LP_receipt_rec.rib_oid              := NULL;  -- rib OID
      LP_receipt_rec.dc_dest_id           := NULL;  -- dc_dest_id
      LP_receipt_rec.po_nbr               := NULL;  -- po_nbr
      LP_receipt_rec.document_type        := NULL;  -- document_type
      LP_receipt_rec.asn_nbr              := NULL;  -- asn_nbr
      LP_receipt_rec.receiptdtl_tbl       := NULL;  -- receiptdtl_tbl
      LP_receipt_rec.receiptcartondtl_tbl := NULL;  -- receiptcartondtl_tbl
      LP_receipt_rec.receipt_type         := NULL;  -- receipt_type
      LP_receipt_rec.from_loc             := NULL;  -- from_loc
      LP_receipt_rec.from_loc_type        := NULL;  -- from_loc_type
   end if;

   -- Create a receipt table
   if LP_receipt_tbl is NULL then
      LP_receipt_tbl := "RIB_Receipt_TBL"();
   else
      LP_receipt_tbl.DELETE;
   end if;

   -- Add receipt record to the receipt table.  Each receipt table will
   -- always contain only one receipt, so we can use the index 1.
   LP_receipt_tbl.EXTEND;
   LP_receipt_tbl(1) := LP_receipt_rec;

   -- Create a receiptdesc element with the receipt table
   if LP_receiptdesc_rec is NULL then
      LP_receiptdesc_rec := "RIB_ReceiptDesc_REC"(NULL,            -- rib OID
                                                NULL,               --Schedule Number 
                                                NULL,            -- appt_nbr
                                                LP_receipt_tbl); -- receipt_tbl
   else
      -- receiptdesc element exists so clean it out
      LP_receiptdesc_rec.rib_oid     := NULL;
      LP_receiptdesc_rec.appt_nbr    := NULL;
      LP_receiptdesc_rec.receipt_tbl := LP_receipt_tbl;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INIT;

-------------------------------------------------------------------------------
FUNCTION BEGIN_RECEIPT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_appt_nbr         IN      APPT_HEAD.APPT%TYPE,
                       I_rib_receipt_rec  IN      "RIB_Receipt_REC")
RETURN BOOLEAN IS

   L_function       VARCHAR2(61) := 'RMSSUB_RECEIPT_ERROR.BEGIN_RECEIPT';

BEGIN

   -- Check for required parameters
   if I_rib_receipt_rec is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_function,
                                            'I_rib_receipt_rec',
                                            'NULL');
      return FALSE;
   end if;

   -- Clean out the temp table
   LP_temp_error_tbl.DELETE;

   -- Save the header level info in global variables
   LP_receiptdesc_rec.appt_nbr                            := I_appt_nbr;
   LP_receiptdesc_rec.receipt_tbl(1).dc_dest_id           := I_rib_receipt_rec.dc_dest_id;
   LP_receiptdesc_rec.receipt_tbl(1).po_nbr               := I_rib_receipt_rec.po_nbr;
   LP_receiptdesc_rec.receipt_tbl(1).document_type        := I_rib_receipt_rec.document_type;
   LP_receiptdesc_rec.receipt_tbl(1).asn_nbr              := I_rib_receipt_rec.asn_nbr;
   LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl       := NULL;
   LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl := NULL;
   LP_receiptdesc_rec.receipt_tbl(1).receipt_type         := I_rib_receipt_rec.receipt_type;
   LP_receiptdesc_rec.receipt_tbl(1).from_loc             := I_rib_receipt_rec.from_loc;
   LP_receiptdesc_rec.receipt_tbl(1).from_loc_type        := I_rib_receipt_rec.from_loc_type;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BEGIN_RECEIPT;

-------------------------------------------------------------------------------
FUNCTION ADD_ERROR(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_error_type     IN      VARCHAR2,
                   I_error_code     IN      VARCHAR2,
                   I_error_desc     IN      VARCHAR2,
                   I_error_level    IN      VARCHAR2,
                   I_error_object   IN      RIB_OBJECT)
RETURN BOOLEAN IS

   L_function         VARCHAR2(61)   := 'RMSSUB_RECEIPT_ERROR.ADD_ERROR';
   L_invalid_param    VARCHAR2(20)   := NULL;
   L_invalid_value    VARCHAR2(500);
   L_error_text       RTK_ERRORS.RTK_TEXT%TYPE;
   L_temp_error_rec   TEMP_ERROR_REC;
   L_temp_error_type  rtk_errors.rtk_type%TYPE;
   L_temp_error       rtk_errors.rtk_text%type   := I_error_desc;


   cursor C_GET_ERROR is
      select rtk_text
        from rtk_errors
       where rtk_key = I_error_code;

BEGIN

   -- Check for required parameters
   if I_error_type is NULL then
      L_invalid_param := 'I_error_type';
      L_invalid_value := 'NULL';
   elsif I_error_code is NULL then
      L_invalid_param := 'I_error_code';
      L_invalid_value := 'NULL';
   elsif I_error_level is NULL then
      L_invalid_param := 'I_error_level';
      L_invalid_value := 'NULL';
   elsif I_error_object is NULL then
      L_invalid_param := 'I_error_object';
      L_invalid_value := 'NULL';
   -- Check for invalid values
   elsif I_error_type NOT in ('BL','SY') then
      L_invalid_param := 'I_error_type';
      L_invalid_value := I_error_type;
   elsif I_error_level NOT in ('RECEIPT','BOL','CARTON','ITEM') then
      L_invalid_param := 'I_error_level';
      L_invalid_value := I_error_level;
   end if;
   --
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_function,
                                            L_invalid_param,
                                            'NULL');
      return FALSE;
   end if;


   -- Create a temp error record
   L_temp_error_rec.error_type   := I_error_type;
   L_temp_error_rec.error_code   := I_error_code;
   L_temp_error_rec.error_object := I_error_object;
   L_temp_error_rec.error_level  := I_error_level;
   -- For system type errors, parse the text message from the function in error.
   -- For business errors encountered in the stock order receiving functions, get
   -- the text of the message from the error tables.
   if I_error_type = 'SY' then
      SQL_LIB.API_MSG(L_temp_error_type,
                      L_temp_error);
      L_temp_error_rec.error_desc := L_temp_error;
   else -- I_error_type = 'BL'
      L_temp_error_rec.error_desc := SQL_LIB.GET_MESSAGE_TEXT(I_error_code,
                                                              NULL,
                                                              NULL,
                                                              NULL);
   end if;

   -- Add temp error record to temp error table
   LP_temp_error_tbl.EXTEND;
   LP_temp_error_tbl(LP_temp_error_tbl.COUNT) := L_temp_error_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_ERROR;

-------------------------------------------------------------------------------
FUNCTION END_RECEIPT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_function                VARCHAR2(61) := 'RMSSUB_RECEIPT_ERROR.END_RECEIPT';
   --
   L_routinginfo_rec         RIB_ROUTINGINFO_REC;
   L_routinginfo_tbl         RIB_ROUTINGINFO_TBL;
   L_rib_busobjid_tbl        RIB_BUSOBJID_TBL;
   L_rib_error_rec           RIB_ERROR_REC;
   --
   L_current_error_rec       TEMP_ERROR_REC;
   L_current_index           BINARY_INTEGER;
   L_current_error_type      VARCHAR2(25);
   L_current_error_code      VARCHAR2(25);
   L_current_error_desc      VARCHAR2(4000);
   L_current_error_object    RIB_OBJECT;
   L_current_error_level     VARCHAR2(10);
   --
   L_next_error_rec          TEMP_ERROR_REC;
   L_next_index              BINARY_INTEGER;
   L_next_error_type         VARCHAR2(25);
   L_next_error_code         VARCHAR2(25);
   L_next_error_desc         VARCHAR2(4000);
   L_next_error_object       RIB_OBJECT;
   L_next_error_level        VARCHAR2(10);
   --
   L_bol_no_exists           VARCHAR2(1) := 'N';

   cursor C_GET_FROM_LOC is
      select 'Y',
             from_loc,
             from_loc_type
        from shipment sh
       where bol_no = LP_cached_bol_no;

BEGIN

   -- Get the index of the first element in the temp error table
   L_current_index := LP_temp_error_tbl.FIRST;

   -- If there are no records on the temp error table then we
   -- have nothing to process.
   if L_current_index is NULL then
      return TRUE;
   end if;

   -- Get the routing info (from_loc/from_loc_type) for the error object.
   -- If the values are in the message then use them.
   if LP_receiptdesc_rec.receipt_tbl(1).from_loc is NOT NULL
   or LP_receiptdesc_rec.receipt_tbl(1).from_loc_type is NOT NULL then
      --
      LP_from_loc      := LP_receiptdesc_rec.receipt_tbl(1).from_loc;
      LP_from_loc_type := LP_receiptdesc_rec.receipt_tbl(1).from_loc_type;
      --
   -- If the from_loc/from_loc_type are not in the message,
   -- then we must get them using the BOL number.
   elsif LP_cached_bol_no is NULL
   or    LP_cached_bol_no != LP_receiptdesc_rec.receipt_tbl(1).asn_nbr then
      --
      LP_cached_bol_no := LP_receiptdesc_rec.receipt_tbl(1).asn_nbr;
      --
      open C_GET_FROM_LOC;
      fetch C_GET_FROM_LOC into L_bol_no_exists,
                                LP_from_loc,
                                LP_from_loc_type;
      close C_GET_FROM_LOC;

      -- If this bol_no does not exist (may be a dummy bol)
      -- then clear out the from loc values.
      if L_bol_no_exists = 'N' then
         LP_from_loc      := NULL;
         LP_from_loc_type := NULL;
      end if;
      --
   end if;

   -- Create a routing info record and add it to a routing info table
   L_routinginfo_rec := RIB_ROUTINGINFO_REC('FROM_LOC',
                                            LP_from_loc,
                                            'FROM_LOC_TYPE',
                                            LP_from_loc_type,
                                            NULL,
                                            NULL);
   --
   L_routinginfo_tbl := RIB_ROUTINGINFO_TBL();
   L_routinginfo_tbl.EXTEND;
   L_routinginfo_tbl(1) := L_routinginfo_rec;

   <<outer_loop>>
   while L_current_index is NOT NULL loop

      -- Hold on to the first error record
      L_current_error_rec     := LP_temp_error_tbl(L_current_index);
      L_current_error_object  := L_current_error_rec.error_object;
      L_current_error_type    := L_current_error_rec.error_type;
      L_current_error_code    := L_current_error_rec.error_code;
      L_current_error_desc    := L_current_error_rec.error_desc;
      L_current_error_level   := L_current_error_rec.error_level;

      -- If the error is at the RECEIPT or BOL level then the error object is a
      -- rib_receipt_rec (complete receipt).  A RECEIPT may have item or carton
      -- details while a BOL will have no details.  If the error is at the carton
      -- level then the error object is a rib_receiptcartondtl_rec (carton detail).
      -- If the error is at the ITEM level then the error object is a rib_receiptdtl_rec
      -- (item detail).  Save the current error object to the global receipt record.
      if L_current_error_level in ('RECEIPT','BOL') then

         -- Save the entire receipt to the global variable
         LP_receiptdesc_rec.receipt_tbl(1) := treat(L_current_error_object as "RIB_Receipt_REC");

      elsif L_current_error_level = 'CARTON' then

         -- This is a carton error so delete any item details
         LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl := NULL;

         -- Initialize the carton table (create it or delete any existing elements)
         if LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl is NULL then
            LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl := "RIB_ReceiptCartonDtl_TBL"();
         else
            LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl.DELETE;
         end if;

         -- Add this carton to the carton table of the global receipt variable
         LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl.EXTEND;
         LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl(LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl.COUNT) := treat(L_current_error_object as "RIB_ReceiptCartonDtl_REC");

      elsif L_current_error_level = 'ITEM' then

         -- This is an item error so delete any carton details
         LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl := NULL;

         -- Initialize the item table (create it or delete any existing elements)
         if LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl is NULL then
            LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl := "RIB_ReceiptDtl_TBL"();
         else
            LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl.DELETE;
         end if;

         -- Add this item to the item table of the global receipt variable
         LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl.EXTEND;
         LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl(LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl.COUNT) := treat(L_current_error_object as "RIB_ReceiptDtl_REC");

      end if;


      -- If the error is at the RECEIPT or BOL level, or it is an unwanded carton at the
      -- CARTON level, then we will not be looping through the remaining errors to group
      -- similar errors.  Each of these errors are complete and no additional detail
      -- nodes will be added to them.
      if  L_current_error_level in ('RECEIPT','BOL')
      or  (L_current_error_type  = 'BL'    and
           L_current_error_level = 'CARTON' and
           L_current_error_code  = 'CTN_UNWANDED') then

         -- Do not loop through all of the other records or add additional nodes
         NULL;

      else

         -- Get the index of the next element in the temp error table
         L_next_index := LP_temp_error_tbl.NEXT(L_current_index);

         -- Loop through the rest of the error elements.  If the errors are the same
         -- type as the 'current' error then group by adding them to this curent
         -- receipt error as a detail node.
         << inner_loop >>
         while L_next_index is NOT NULL loop

            -- Hold the values of this record
            L_next_error_rec     := LP_temp_error_tbl(L_next_index);
            L_next_error_object  := L_next_error_rec.error_object;
            L_next_error_type    := L_next_error_rec.error_type;
            L_next_error_code    := L_next_error_rec.error_code;
            L_next_error_desc    := L_next_error_rec.error_desc;
            L_next_error_level   := L_next_error_rec.error_level;

            -- If this error is similar to the 'current' error then
            -- add this error as a detail node.  If this is a system
            -- error then we only want to group this error if it has
            -- the same message key (message parameters may differ).
            if  L_next_error_type  = L_current_error_type
            and L_next_error_code  = L_current_error_code
            and L_next_error_level = L_current_error_level
            and (  (L_next_error_type = 'SY' and GET_MESSAGE_KEY(L_next_error_desc) = GET_MESSAGE_KEY(L_current_error_desc))
                 or(L_next_error_type != 'SY')) then

               -- If this is an unwanded carton at the ITEM level and the container numbers
               -- are different then do not add this error to the group.  We only want to group
               -- other unwanded item records that have the same carton number.
               if  L_next_error_level = 'ITEM'
               and L_next_error_code = 'CTN_UNWANDED'
               and (treat(L_next_error_object as "RIB_ReceiptDtl_REC").container_id !=
                    LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl(1).container_id) then

                  -- Do not add this to group -- different unwanded carton
                  NULL;

               else

                  -- Add this to the error object as a detail node
                  if L_next_error_level = 'CARTON' then
                     LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl.EXTEND;
                     LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl(LP_receiptdesc_rec.receipt_tbl(1).receiptcartondtl_tbl.COUNT) := treat(L_next_error_object as "RIB_ReceiptCartonDtl_REC");
                  elsif L_next_error_level = 'ITEM' then
                     LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl.EXTEND;
                     LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl(LP_receiptdesc_rec.receipt_tbl(1).receiptdtl_tbl.COUNT) := treat(L_next_error_object as "RIB_ReceiptDtl_REC");
                  end if;

                  -- Delete this element from the temp error table
                  LP_temp_error_tbl.DELETE(L_next_index);

               end if;

            end if;

            -- Get the next element from the temp error table.
            L_next_index := LP_temp_error_tbl.NEXT(L_next_index);

         end loop inner_loop;

      end if;

      -- We have now grouped all similar errors and added them to the global
      -- receipt message as detail nodes.  Use this receipt message to create
      -- an error record

      -- Create business object table and add records.
      L_rib_busobjid_tbl := RIB_BUSOBJID_TBL();
      L_rib_busobjid_tbl.EXTEND;
      
      if L_current_error_code = 'CTN_UNWANDED' then
         -- Set the business object table to NULL so ID for RIB processing will be null.
         L_rib_busobjid_tbl(L_rib_busobjid_tbl.COUNT) := NULL;
      else
         --    Record 1. appointment number/asn
         --    Record 2: from location
         if LP_receiptdesc_rec.appt_nbr is NULL then
            L_rib_busobjid_tbl(L_rib_busobjid_tbl.COUNT) := LP_receiptdesc_rec.receipt_tbl(1).asn_nbr;
         else
            L_rib_busobjid_tbl(L_rib_busobjid_tbl.COUNT) := LP_receiptdesc_rec.appt_nbr;
         end if; 
         L_rib_busobjid_tbl.EXTEND;
         L_rib_busobjid_tbl(L_rib_busobjid_tbl.COUNT) := LP_receiptdesc_rec.receipt_tbl(1).from_loc;
      end if;

      L_rib_error_rec := RIB_ERROR_REC(L_current_error_type,                  -- error_type
                                       L_current_error_code,                  -- error_code
                                       GET_MESSAGE_KEY(L_current_error_desc), -- error_desc
                                       LP_receiptdesc_rec,                    -- error_rib_object
                                       LP_message_family,                     -- error_message_family
                                       LP_message_type,                       -- error_message_type
                                       L_rib_busobjid_tbl,                    -- error_bus_object_id
                                       L_routinginfo_tbl,                     -- error_routing_info
                                       LP_error_application);                 -- error_application


      -- Add the error record to the error table
      LP_error_tbl.EXTEND;
      LP_error_tbl(LP_error_tbl.COUNT) := L_rib_error_rec;

      -- Delete the current element from the temp error table
      LP_temp_error_tbl.DELETE(L_current_index);

      -- Get the next element from the temp error table and continue
      -- processing if it is not null.
      L_current_index := LP_temp_error_tbl.NEXT(L_current_index);

   end loop outer_loop;

   -- Clean out the temp table
   LP_temp_error_tbl.DELETE;

   --------------------------------------------------------------------------------
   -- All similar errors have been grouped and added to receipt messages.
   -- An error object has been created for each of these receipt messages.
   -- All error objects have been added to the global error table.
   --------------------------------------------------------------------------------

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END END_RECEIPT;

-------------------------------------------------------------------------------
FUNCTION FINISH(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                O_rib_error_tbl     OUT  RIB_ERROR_TBL)
RETURN BOOLEAN IS

   L_function  VARCHAR2(61) := 'RMSSUB_RECEIPT_ERROR.FINISH';

BEGIN

   O_rib_error_tbl := LP_error_tbl;
   LP_error_tbl    := NULL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FINISH;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- GET_MESSAGE_KEY
-- This function will get the message key from a SQL_LIB error message (those
-- errors which contain parameters in the form of @0, @1, etc.).  If the error
-- message is just text without any parameters, the entire message will be
-- passed back out as the key.  This key will be used to compare and group
-- errors when building receipt messages for error objects.
-------------------------------------------------------------------------------
FUNCTION GET_MESSAGE_KEY(I_message  IN  VARCHAR2)
RETURN VARCHAR2 IS

   L_function  VARCHAR2(61)  := 'RMSSUB_RECEIPT_ERROR.GET_MESSAGE_KEY(PRIVATE)';
   L_key       RTK_ERRORS.RTK_TEXT%TYPE;
   L_pos_1     NUMBER        := 0;
   L_pos_2     NUMBER        := 0;

BEGIN

   if  LENGTH(I_message) > 3
   and SUBSTR(I_message, 1, 2) = '@0' then
      -- Get the position of the key and parameters
      L_pos_1 := INSTR(I_message, '@0');
      L_pos_2 := INSTR(I_message, '@1');

      if L_pos_2 = 0 then
         -- No parameters in message
         L_key := SUBSTR(I_message, 3);
      else
         -- 1 or more parameters in message
         L_key := SUBSTR(I_message, 3, (L_pos_2 - L_pos_1 - 2));
      end if;
   else
      -- This is not a SQL_LIB error with parameters, so the
      -- key is equal to I_message.
      L_key := I_message;
   end if;

   return L_key;

END GET_MESSAGE_KEY;

-------------------------------------------------------------------------------
END RMSSUB_RECEIPT_ERROR;
/
