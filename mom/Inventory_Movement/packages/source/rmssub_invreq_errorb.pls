CREATE OR REPLACE PACKAGE BODY RMSSUB_INVREQ_ERROR AS

-------------------------------------------------------------------------------
-- Usage:
--
-- In the item/store order request process, call INIT before processing any inv   
-- reqs. This will initialize all of the global variables.
--
-- For each invreq record, call BEGIN_INVREQ.  This function will hold the
-- header level values in global variables which may be used to build an error
-- record when necessary.
--
-- When an error is encountered in the inv req process, call ADD_ERROR.  This
-- function adds the error type/description and error object on the global error 
-- table.
--
-- At the end of the inv req process, call FINISH.  This will pass out a copy
-- of the global error table (if any errors exist) which is then sent to the
-- RIB for further processing.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- GLOBAL VARIABLES
-------------------------------------------------------------------------------
LP_message_family        VARCHAR2(10) := 'invreq';
LP_error_application     VARCHAR2(10) := 'RMS';
LP_message_type          VARCHAR2(20);
LP_vdate                 DATE;

LP_error_tbl             RIB_ERROR_TBL;

LP_invreqdesc_rec        "RIB_InvReqDesc_REC";
LP_invreqitem_rec        "RIB_InvReqItem_REC";
LP_invreqitem_tbl        "RIB_InvReqItem_TBL";

-------------------------------------------------------------------------------
-- PRIVATE FUNCTION PROTOTYPE: GET_MESSAGE_KEY
-- This function will get the message key from a SQL_LIB error message (those
-- errors which contain parameters in the form of @0, @1, etc.).  If the error
-- message is just text without any parameters, the entire message will be
-- passed back out as the key.  This key will be used to compare and group
-- errors when building invreq messages for error objects. Grouping similar 
-- error messages are not being done for inv req at this time.
-------------------------------------------------------------------------------
FUNCTION GET_MESSAGE_KEY(I_message  IN  VARCHAR2)
RETURN VARCHAR2;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
FUNCTION INIT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
              I_message_type   IN      VARCHAR2)
RETURN BOOLEAN IS

   L_function  VARCHAR2(61) := 'RMSSUB_INVREQ_ERROR.INIT';

BEGIN

   -- Set globals. I_message_type is NULL for invreq
   LP_message_type       := I_message_type;
   LP_vdate              := GET_VDATE;

   -- Initialize the error table
   if LP_error_tbl is NULL then
      LP_error_tbl := RIB_ERROR_TBL();
   else
      LP_error_tbl.DELETE;
   end if;


   -- Create an invreqitem record element to hold the invreqitem info
   if LP_invreqitem_rec is NULL then
      LP_invreqitem_rec := "RIB_InvReqItem_REC"(NULL,   -- rib OID
                                        NULL,   -- item
                                        NULL,   -- qty_requested
                                        NULL,   -- UOP
                                        NULL,   -- need date
                                        NULL);   -- delivery_slot_id
   else
      -- Invereqitem record exists so clean it out
      LP_invreqitem_rec.rib_oid              := NULL;  -- rib OID
      LP_invreqitem_rec.item                 := NULL;  -- item
      LP_invreqitem_rec.qty_rqst             := NULL;  -- qty_rqst
      LP_invreqitem_rec.uop                  := NULL;  -- uop
      LP_invreqitem_rec.need_date            := NULL;  -- need_date
      LP_invreqitem_rec.delivery_slot_id     := NULL;  -- delivery slot
   end if;

   -- Create an invreqitem table
   if LP_invreqitem_tbl is NULL then
      LP_invreqitem_tbl := "RIB_InvReqItem_TBL"();
   else
      LP_invreqitem_tbl.DELETE;
   end if;

   -- Add invreqitem record to the invreqitem table.  Each invreqitem table will
   -- always contain only one request, so we can use the index 1.
   LP_invreqitem_tbl.EXTEND;
   LP_invreqitem_tbl(1) := LP_invreqitem_rec;

   -- Create an invreqdesc element with the invreqitem table
   if LP_invreqdesc_rec is NULL then
      LP_invreqdesc_rec := "RIB_InvReqDesc_REC"(NULL,            -- rib OID
                                              NULL,            -- request_id
                                              NULL,            -- store
                                              NULL,            -- request_type
                                              LP_invreqitem_tbl); -- invreqitem_tbl
   else
      -- invreqdesc element exists so clean it out
      LP_invreqdesc_rec.rib_oid        := NULL;
      LP_invreqdesc_rec.request_id     := NULL;
      LP_invreqdesc_rec.store          := NULL;
      LP_invreqdesc_rec.request_type   := NULL;    
      LP_invreqdesc_rec.invreqitem_tbl := LP_invreqitem_tbl;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INIT;

-------------------------------------------------------------------------------
FUNCTION BEGIN_INVREQ(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_request_id       IN      NUMBER,
                      I_store            IN      STORE_ORDERS.STORE%TYPE,
                      I_request_type     IN      VARCHAR2)
RETURN BOOLEAN IS

   L_function       VARCHAR2(61) := 'RMSSUB_INVREQ_ERROR.BEGIN_INVREQ';

BEGIN

   -- Save the header level info in global variables
   LP_invreqdesc_rec.request_id        := I_request_id;
   LP_invreqdesc_rec.store             := I_store;
   LP_invreqdesc_rec.request_type      := I_request_type;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BEGIN_INVREQ;

-------------------------------------------------------------------------------
FUNCTION ADD_ERROR(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_error_desc     IN      VARCHAR2,
                   I_error_object   IN      RIB_OBJECT)
RETURN BOOLEAN IS

   L_function         VARCHAR2(61)   := 'RMSSUB_INVREQ_ERROR.ADD_ERROR';
   L_invalid_param    VARCHAR2(20)   := NULL;
   L_invalid_value    VARCHAR2(500);
   L_rib_error_rec    RIB_ERROR_REC;
   L_temp_error_type  rtk_errors.rtk_type%TYPE;
   L_temp_error       rtk_errors.rtk_text%type   := I_error_desc;

BEGIN

   -- Check for required parameters
   if I_error_desc is NULL then
      L_invalid_param := 'I_error_desc';
      L_invalid_value := 'NULL';
   elsif I_error_object is NULL then
      L_invalid_param := 'I_error_object';
      L_invalid_value := 'NULL';
   end if;
   --
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_function,
                                            L_invalid_param,
                                            'NULL');
      return FALSE;
   end if;

   SQL_LIB.API_MSG(L_temp_error_type,
                   L_temp_error); 
   
   -- Initialize the invreqitem table (create it or delete any existing elements)
   if LP_invreqdesc_rec.invreqitem_tbl is NULL then
      LP_invreqdesc_rec.invreqitem_tbl := "RIB_InvReqItem_TBL"();
   else
      LP_invreqdesc_rec.invreqitem_tbl.DELETE;
   end if;

   -- Add this invreqitem table of the global invreqdesc variable
   LP_invreqdesc_rec.invreqitem_tbl.EXTEND;
   LP_invreqdesc_rec.invreqitem_tbl(LP_invreqdesc_rec.invreqitem_tbl.COUNT) := treat(I_error_object as "RIB_InvReqItem_REC");

   -- Use the invreq message to create and error record   
   L_rib_error_rec := RIB_ERROR_REC(NULL,                          -- error_type
                                    NULL,                          -- error_code
                                    GET_MESSAGE_KEY(L_temp_error), -- error_desc
                                    LP_invreqdesc_rec,             -- error_rib_object
                                    LP_message_family,             -- error_message_family
                                    LP_message_type,               -- error_message_type
                                    NULL,                          -- error_bus_object_id
                                    NULL,                          -- error_routing_info
                                    LP_error_application);         -- error_application
                                    
   -- Add temp error record to temp error table
   LP_error_tbl.EXTEND;
   LP_error_tbl(LP_error_tbl.COUNT) := L_rib_error_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_ERROR;
-------------------------------------------------------------------------------
FUNCTION FINISH(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                O_rib_error_tbl     OUT  RIB_ERROR_TBL)
RETURN BOOLEAN IS

   L_function  VARCHAR2(61) := 'RMSSUB_INVREQ_ERROR.FINISH';

BEGIN

   O_rib_error_tbl := LP_error_tbl;
   LP_error_tbl    := NULL;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FINISH;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- GET_MESSAGE_KEY
-- This function will get the message key from a SQL_LIB error message (those
-- errors which contain parameters in the form of @0, @1, etc.).  If the error
-- message is just text without any parameters, the entire message will be
-- passed back out as the key.  This key can be used to compare and group
-- errors when building invreq messages for error objects.  Grouping similar 
-- error messages are not being done for inv req at this time.
-------------------------------------------------------------------------------
FUNCTION GET_MESSAGE_KEY(I_message  IN  VARCHAR2)
RETURN VARCHAR2 IS

   L_function  VARCHAR2(61)  := 'RMSSUB_INVREQ_ERROR.GET_MESSAGE_KEY(PRIVATE)';
   L_key       RTK_ERRORS.RTK_TEXT%TYPE;
   L_pos_1     NUMBER        := 0;
   L_pos_2     NUMBER        := 0;

BEGIN

   if  LENGTH(I_message) > 3
   and SUBSTR(I_message, 1, 2) = '@0' then
      -- Get the position of the key and parameters
      L_pos_1 := INSTR(I_message, '@0');
      L_pos_2 := INSTR(I_message, '@1');

      if L_pos_2 = 0 then
         -- No parameters in message
         L_key := SUBSTR(I_message, 3);
      else
         -- 1 or more parameters in message
         L_key := SUBSTR(I_message, 3, (L_pos_2 - L_pos_1 - 2));
      end if;
   else
      -- This is not a SQL_LIB error with parameters, so the
      -- key is equal to I_message.
      L_key := I_message;
   end if;

   return L_key;

END GET_MESSAGE_KEY;

-------------------------------------------------------------------------------
END RMSSUB_INVREQ_ERROR;
/