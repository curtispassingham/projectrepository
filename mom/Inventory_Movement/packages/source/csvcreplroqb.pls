CREATE OR REPLACE PACKAGE BODY CORESVC_REPL_ROQ_SQL AS
--
   LP_vdate                         DATE                                               := GET_VDATE;
   LP_curr_weekday                  NUMBER(1)                                          := TO_NUMBER(TO_CHAR(GET_VDATE, 'D'));
   LP_so_rec                        SYSTEM_OPTIONS%ROWTYPE                             := NULL;
   LP_wh_crosslink_ind              SYSTEM_OPTIONS.WH_CROSS_LINK_IND%TYPE;
   LP_srvce_lvl_type_simple_sales   CONSTANT   REPL_ITEM_LOC.SERVICE_LEVEL_TYPE%TYPE   := 'SS';
   LP_wh_exists                     VARCHAR2(1)                                        := 'N';
--
FUNCTION REPL_METHOD(O_error_message         IN OUT   VARCHAR2,
                     I_last_run_of_the_day   IN       VARCHAR2 DEFAULT 'Y',
                     I_thread_id             IN       NUMBER DEFAULT NULL,
                     I_calling_program       IN       VARCHAR2 DEFAULT 'REPLROQ')
RETURN BOOLEAN;
--
FUNCTION GET_NET_INVENTORY (O_error_message    IN OUT VARCHAR2,
                            I_thread_id        IN NUMBER DEFAULT NULL,
                            I_lost_sales_ind   IN VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--
FUNCTION GET_LOC_FUTURE_AVAIL(O_error_message      IN OUT   VARCHAR2,
                              I_thread_id          IN       NUMBER DEFAULT NULL,
                              I_lost_sales_ind     IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--
FUNCTION GET_ON_ORDER(O_error_message    IN OUT   VARCHAR2,
                      I_thread_id        IN       NUMBER DEFAULT NULL,
                      I_lost_sales_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--
FUNCTION GET_ALLOC_IN(O_error_message    IN OUT   VARCHAR2,
                      I_thread_id        IN       NUMBER DEFAULT NULL,
                      I_lost_sales_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--
FUNCTION GET_ALLOC_OUT(O_error_message    IN OUT   VARCHAR2,
                       I_thread_id        IN       NUMBER DEFAULT NULL,
                       I_lost_sales_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--
FUNCTION GET_CO_TRANSFER_IN(O_error_message    IN OUT   VARCHAR2,
                            I_thread_id        IN       NUMBER DEFAULT NULL,
                            I_lost_sales_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--
FUNCTION GET_PL_TRANSFER(O_error_message      IN OUT   VARCHAR2,
                         I_thread_id          IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN;
--
FUNCTION GET_TRAN_ITEM_LOC_QTYS(O_error_message        IN OUT   VARCHAR2,
                                I_thread_id            IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN;
--
FUNCTION BULK_FORECAST(O_error_message   IN OUT   VARCHAR2,
                       I_thread_id       IN       NUMBER)
RETURN BOOLEAN;
--
FUNCTION GET_LOCN_NEED_DYNAMIC(O_error_message        IN OUT   VARCHAR2,
                               I_thread_id            IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN;
--
FUNCTION GET_CONSTANT_MINMAX_FLOAT_TS_D (O_error_message        IN OUT   VARCHAR2,
                                         I_thread_id            IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN;
--
FUNCTION GET_FORECAST_FOR_PERIOD_DYN (O_error_message  IN OUT   VARCHAR2,
                                      I_thread_id      IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN;
--
FUNCTION GET_STORE_ORDERS(O_error_message   IN OUT   VARCHAR2,
                          I_thread_id       IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN;
--
FUNCTION ADD_SUB_ITEMS(O_error_message   IN OUT   VARCHAR2,
                       I_thread_id       IN       NUMBER)
RETURN BOOLEAN;
--
FUNCTION ADD_WH_LINKS(O_error_message   IN OUT   VARCHAR2,
                      I_thread_id       IN       NUMBER)
RETURN BOOLEAN;
--
FUNCTION ADD_PACK_COMP(O_error_message   IN OUT   VARCHAR2,
                       I_thread_id       IN       NUMBER)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------
FUNCTION SETUP_DATA(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_chunk_size        IN       RMS_PLSQL_BATCH_CONFIG.MAX_CHUNK_SIZE%TYPE,
                    I_last_run_of_day   IN       VARCHAR2 DEFAULT 'Y')
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.SETUP_DATA';

BEGIN

   insert /*+ append */ into svc_repl_roq (i_next_delivery_date,
                                           i_supp_lead_time,
                                           i_pickup_lead_time,
                                           i_wh_lead_time,
                                           i_last_delivery_date,
                                           i_item,
                                           i_locn_type,
                                           i_locn,
                                           i_primary_repl_supplier,
                                           i_origin_country_id,
                                           i_review_cycle,
                                           i_sub_item_loc,
                                           i_store_need,
                                           i_pres_stock,
                                           i_demo_stock,
                                           i_repl_method,
                                           i_min_stock,
                                           i_max_stock,
                                           i_incr_pct,
                                           i_min_supply_days,
                                           i_max_supply_days,
                                           i_time_supply_horizon,
                                           i_inv_selling_days,
                                           i_service_level,
                                           i_lost_sales_factor,
                                           i_terminal_stock_qty,
                                           i_unit_cost,
                                           i_unit_retail,
                                           i_due_ord_process_ind,
                                           i_repl_results_all_ind,
                                           i_season_id,
                                           i_phase_id,
                                           i_domain_id,
                                           i_reject_store_ord_ind,
                                           i_date,
                                           i_last_run_of_the_day,
                                           i_stock_cat,
                                           i_source_wh,
                                           i_source_physical_wh,
                                           i_service_level_type,
                                           i_wh_delivery_policy,
                                           i_sup_delivery_policy,
                                           i_item_parent,
                                           i_item_grandparent,
                                           i_pack_ind,
                                           i_item_level,
                                           i_tran_level,
                                           i_sub_forecast_ind,
                                           i_sub_stock_ind,
                                           i_all_orders,
                                           i_order_point,
                                           i_repl_ind,
                                           i_store_type,
                                           thread_id,
                                           i_add_lead_time_ind)
                                           --
                                    select /*+ parallel(r) */ r.next_delivery_date                      i_next_delivery_date,
                                           DECODE(r.stock_cat, 'L', NVL(r.supp_lead_time,0),0)          i_supp_lead_time,
                                           NVL(r.pickup_lead_time,0)                                    i_pickup_lead_time,
                                           NVL(r.wh_lead_time,0)                                        i_wh_lead_time,
                                           r.last_delivery_date                                         i_last_delivery_date,
                                           r.item                                                       i_item,
                                           r.loc_type                                                   i_locn_type,
                                           r.location                                                   i_locn,
                                           r.primary_repl_supplier                                      i_primary_repl_supplier,
                                           r.origin_country_id                                          i_origin_country_id,
                                           r.review_cycle                                               i_review_cycle,
                                           r.source_wh                                                  i_sub_item_loc,
                                           0                                                            i_store_need,
                                           r.pres_stock                                                 i_pres_stock,
                                           r.demo_stock                                                 i_demo_stock,
                                           r.repl_method                                                i_repl_method,
                                           r.min_stock                                                  i_min_stock,
                                           r.max_stock                                                  i_max_stock,
                                           r.incr_pct                                                   i_incr_pct,
                                           r.min_supply_days                                            i_min_supply_days,
                                           r.max_supply_days                                            i_max_supply_days,
                                           r.time_supply_horizon                                        i_time_supply_horizon,
                                           r.inv_selling_days                                           i_inv_selling_days,
                                           r.service_level                                              i_service_level,
                                           r.lost_sales_factor                                          i_lost_sales_factor,
                                           r.terminal_stock_qty                                         i_terminal_stock_qty,
                                           1                                                            i_unit_cost,
                                           1                                                            i_unit_retail,
                                           'N'                                                          i_due_ord_process_ind,
                                           'N'                                                          i_repl_results_all_ind,
                                           r.season_id                                                  i_season_id,
                                           r.phase_id                                                   i_phase_id,
                                           case
                                              when so.domain_level = 'D' then
                                                 (select domain_id
                                                    from domain_dept
                                                   where im.dept = dept)
                                              when so.domain_level = 'C' then
                                                 (select domain_id
                                                    from domain_class
                                                   where im.dept = dept
                                                     and im.class = class)
                                              when so.domain_level = 'S' then
                                                 (select domain_id
                                                    from domain_subclass
                                                   where im.dept = dept
                                                     and im.class = class
                                                     and im.subclass = subclass)
                                              end                                                       i_domain_id,
                                           r.reject_store_ord_ind                                       i_reject_store_ord_ind,
                                           LP_vdate + 1                                                 i_date,

                                           I_last_run_of_day                                            i_last_run_of_the_day,
                                           r.stock_cat                                                  i_stock_cat,
                                           r.source_wh                                                  i_source_wh,
                                           wah.physical_wh                                              i_source_physical_wh,
                                           r.service_level_type                                         i_service_level_type,
                                           wah.delivery_policy                                          i_wh_delivery_policy,
                                           case
                                              when r.stock_cat = 'L' then
                                                 (select delivery_policy
                                                    from sups
                                                   where r.primary_repl_supplier = supplier(+))
                                              else
                                                 'NEXT'
                                              end                                                       i_sup_delivery_policy,
                                           r.item_parent                                                i_item_parent,
                                           r.item_grandparent                                           i_item_grandparent,
                                           im.pack_ind                                                  i_pack_ind,
                                           im.item_level                                                i_item_level,
                                           im.tran_level                                                i_tran_level,
                                           'Y'                                                          i_sub_forecast_ind,
                                           'N'                                                          i_sub_stock_ind,
                                           'N'                                                          i_all_orders,
                                           0                                                            i_order_point,
                                           'R'                                                          i_repl_ind,
                                           NVL(s.store_type, 'C')                                       i_store_type,
                                           ceil(ROWNUM/I_chunk_size)                                    thread_id,
                                           r.add_lead_time_ind                                          i_add_lead_time_ind
                                      from repl_item_loc r,
                                           store s,
                                           item_master im,
                                           system_options so,
                                           wh wah,
                                           repl_day rdy
                                     where r.loc_type = 'S'
                                       and r.stock_cat in ('W','L')
                                       and r.status = 'A'
                                       and (   LP_vdate != r.last_review_date
                                            or r.last_review_date is NULL)
                                       and LP_vdate >= NVL(r.last_review_date, TO_DATE('19010101', 'YYYYMMDD')) +
                                                            (DECODE(TO_NUMBER(r.review_cycle),
                                                                    1, 0,
                                                                    r.review_cycle) * 7)
                                       and r.activate_date <= LP_vdate
                                       and NVL(r.deactivate_date, LP_vdate + 1) > LP_vdate
                                       and (I_last_run_of_day = 'Y'
                                           or
                                           (I_last_run_of_day = 'N' and r.mult_runs_per_day_ind = 'Y' and r.repl_method = 'SO'))
                                       and rdy.item = r.item
                                       and rdy.location = r.location
                                       and rdy.weekday = LP_curr_weekday
                                       and NOT EXISTS (select 'x'
                                                         from rpl_net_inventory_tmp rnit
                                                        where rnit.item = r.item
                                                          and rnit.location = r.location
                                                          and ROWNUM = 1)
                                       and s.store = r.location
                                       and (LP_vdate  + r.wh_lead_time <=
                                             (NVL(s.store_close_date, LP_vdate) - NVL(s.stop_order_days, 0)) or
                                             s.store_close_date is NULL)
                                       and s.store_open_date - s.start_order_days <= LP_vdate + r.wh_lead_time
                                       and r.item = im.item
                                       and r.source_wh = wah.wh;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SETUP_DATA;
-------------------------------------------------------------------------------
FUNCTION SETUP_DATA_GTT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_thread_id        IN       NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.SETUP_DATA_GTT';

BEGIN

  insert into svc_repl_roq_gtt(i_next_delivery_date,
                               i_supp_lead_time,
                               i_pickup_lead_time,
                               i_wh_lead_time,
                               i_last_delivery_date,
                               i_item,
                               i_locn_type,
                               i_locn,
                               i_primary_repl_supplier,
                               i_origin_country_id,
                               i_review_cycle,
                               i_sub_item_loc,
                               i_store_need,
                               i_pres_stock,
                               i_demo_stock,
                               i_repl_method,
                               i_min_stock,
                               i_max_stock,
                               i_incr_pct,
                               i_min_supply_days,
                               i_max_supply_days,
                               i_time_supply_horizon,
                               i_inv_selling_days,
                               i_service_level,
                               i_lost_sales_factor,
                               i_terminal_stock_qty,
                               i_unit_cost,
                               i_unit_retail,
                               i_due_ord_process_ind,
                               i_repl_results_all_ind,
                               i_season_id,
                               i_phase_id,
                               i_domain_id,
                               i_reject_store_ord_ind,
                               i_date,
                               i_last_run_of_the_day,
                               i_stock_cat,
                               i_source_wh,
                               i_source_physical_wh,
                               i_service_level_type,
                               i_wh_delivery_policy,
                               i_sup_delivery_policy,
                               i_item_parent,
                               i_item_grandparent,
                               i_pack_ind,
                               i_item_level,
                               i_tran_level,
                               i_sub_forecast_ind,
                               i_sub_stock_ind,
                               i_all_orders,
                               i_order_point,
                               i_repl_ind,
                               i_store_type,
                               thread_id,
                               i_add_lead_time_ind)
                        select i_next_delivery_date,
                               i_supp_lead_time,
                               i_pickup_lead_time,
                               i_wh_lead_time,
                               i_last_delivery_date,
                               i_item,
                               i_locn_type,
                               i_locn,
                               i_primary_repl_supplier,
                               i_origin_country_id,
                               i_review_cycle,
                               i_sub_item_loc,
                               i_store_need,
                               i_pres_stock,
                               i_demo_stock,
                               i_repl_method,
                               i_min_stock,
                               i_max_stock,
                               i_incr_pct,
                               i_min_supply_days,
                               i_max_supply_days,
                               i_time_supply_horizon,
                               i_inv_selling_days,
                               i_service_level,
                               i_lost_sales_factor,
                               i_terminal_stock_qty,
                               i_unit_cost,
                               i_unit_retail,
                               i_due_ord_process_ind,
                               i_repl_results_all_ind,
                               i_season_id,
                               i_phase_id,
                               i_domain_id,
                               i_reject_store_ord_ind,
                               i_date,
                               i_last_run_of_the_day,
                               i_stock_cat,
                               i_source_wh,
                               i_source_physical_wh,
                               i_service_level_type,
                               i_wh_delivery_policy,
                               i_sup_delivery_policy,
                               i_item_parent,
                               i_item_grandparent,
                               i_pack_ind,
                               i_item_level,
                               i_tran_level,
                               i_sub_forecast_ind,
                               i_sub_stock_ind,
                               i_all_orders,
                               i_order_point,
                               i_repl_ind,
                               i_store_type,
                               thread_id,
                               i_add_lead_time_ind
                          from svc_repl_roq
                         where thread_id = I_thread_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SETUP_DATA_GTT;
-------------------------------------------------------------------------------
FUNCTION PROCESS_DATA(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_thread_id         IN       NUMBER,
                      I_last_run_of_day   IN       VARCHAR2 DEFAULT 'Y',
                      I_calling_program   IN       VARCHAR2 DEFAULT 'REPLROQ')
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.PROCESS_DATA';

BEGIN

   if REPL_METHOD(O_error_message,
                  I_last_run_of_day,
                  I_thread_id,
                  I_calling_program) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_DATA;
-------------------------------------------------------------------------------
FUNCTION CALC_DATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_thread_id       IN       NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.CALC_DATES';

BEGIN

   if REPL_OLT_SQL.GET_OLTS_AND_REVIEW_TIME(O_error_message,
                                            LP_dummy_row.i_curr_order_lead_time,
                                            LP_dummy_row.i_next_order_lead_time,
                                            LP_dummy_row.i_days_added_to_colt,
                                            LP_dummy_row.i_days_added_to_nolt,
                                            LP_dummy_row.i_review_lead_time,
                                            LP_dummy_row.i_next_delivery_date,
                                            LP_dummy_row.i_next_review_date,
                                            LP_dummy_row.i_supp_lead_time,
                                            LP_dummy_row.i_pickup_lead_time,
                                            LP_dummy_row.i_wh_lead_time,
                                            LP_dummy_row.i_last_delivery_date,
                                            LP_dummy_row.i_item,
                                            LP_dummy_row.i_locn,
                                            LP_dummy_row.i_locn_type,
                                            LP_dummy_row.i_date,
                                            LP_dummy_row.i_stock_cat,
                                            LP_dummy_row.i_repl_method,
                                            LP_dummy_row.i_review_cycle,
                                            LP_dummy_row.i_primary_repl_supplier,
                                            LP_dummy_row.I_sup_delivery_policy,
                                            LP_dummy_row.i_source_wh,
                                            LP_dummy_row.i_source_physical_wh,
                                            LP_dummy_row.i_wh_delivery_policy,
                                            LP_so_rec.loc_activity_ind,
                                            LP_so_rec.loc_dlvry_ind,
                                            I_thread_id ) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CALC_DATES;
-------------------------------------------------------------------------------
FUNCTION REPL_METHOD(O_error_message         IN OUT   VARCHAR2,
                     I_last_run_of_the_day   IN       VARCHAR2 DEFAULT 'Y',
                     I_thread_id             IN       NUMBER DEFAULT NULL,
                     I_calling_program       IN       VARCHAR2 DEFAULT 'REPLROQ')

RETURN BOOLEAN IS

   L_program   VARCHAR2(200) := 'CORESVC_REPL_ROQ_SQL.REPL_METHOD';
   L_plength   NUMBER(2)     := LENGTH('CORESVC_REPL_ROQ_SQL.REPL_METHOD');

   cursor C_WH_EXISTS is
      select 'Y'
        from svc_repl_roq_gtt
       where I_locn_type = 'W'
         and thread_id = I_thread_id
         and ROWNUM = 1;

BEGIN
   if I_thread_id is NOT NULL then

      if ADD_SUB_ITEMS(O_error_message,
                       I_thread_id) = FALSE then
         return FALSE;
      end if;

      open C_WH_EXISTS;
      fetch C_WH_EXISTS into LP_wh_exists;
      close C_WH_EXISTS;

      if I_calling_program != 'REPLROQ' and LP_wh_exists = 'Y' then
         if ADD_WH_LINKS(O_error_message,
                         I_thread_id) = FALSE then
            return FALSE;
         end if;
      end if;

      /* MERGE_1 - for Start and End Dates of Season ID and Phase ID*/
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';
      --
      merge into svc_repl_roq_gtt target
      using (select wrr.i_item,
                    wrr.i_locn,
                    phs.start_date start_dt,
                    phs.end_date end_dt,
                    wrr.thread_id
               from svc_repl_roq_gtt wrr,
                    phases phs
              where phs.phase_id = wrr.i_phase_id
                and phs.season_id = wrr.i_season_id
                and wrr.i_phase_id is NOT NULL
                and wrr.i_season_id is NOT NULL
                and wrr.i_sub_item_master is NULL
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_repl_method in ('D', 'DI', 'S', 'T', 'TI', 'E')
                and wrr.thread_id = I_thread_id ) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.I_phase_start = use_this.start_dt,
                    target.I_phase_end   = use_this.end_dt;
      --
      L_program := substr(L_program,1,L_plength);
      --
      if NOT GET_NET_INVENTORY(O_error_message,
                               I_thread_id) then
         return FALSE;
      end if;

      if I_last_run_of_the_day = 'Y' then

         if NOT BULK_FORECAST(O_error_message,
                              I_thread_id) then
            return FALSE;
         end if;

         if NOT GET_CONSTANT_MINMAX_FLOAT_TS_D(O_error_message,
                                               I_thread_id) then
            return FALSE;
         end if;

         if NOT GET_LOCN_NEED_DYNAMIC(O_error_message,
                                      I_thread_id) then
              return FALSE;
         end if;

      end if;

      if NOT GET_STORE_ORDERS(O_error_message,
                              I_thread_id) then
         return FALSE;
      end if;

      /* MERGE_2 - for ROQ, Quantities and Forecast of Season ID and Phase ID */
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';
      --
      merge into svc_repl_roq_gtt target
      using (select DISTINCT wrr.i_item,
                    wrr.i_locn,
                    wrr.thread_id
               from svc_repl_roq_gtt wrr,
                    phases phs
              where phs.phase_id = wrr.i_phase_id
                and phs.season_id = wrr.i_season_id
                and wrr.i_phase_id is NOT NULL
                and wrr.i_season_id is NOT NULL
                and wrr.i_repl_method in ('D', 'DI', 'S', 'T', 'TI', 'E')
                and wrr.i_sub_item_master is NULL
                and wrr.i_lead_linked_wh is NULL
                and (
                    (wrr.i_date + wrr.i_curr_order_lead_time < phs.start_date -1) or
                    (wrr.i_date + wrr.i_curr_order_lead_time >= phs.end_date)
                    )
                and wrr.thread_id = I_thread_id ) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_order_qty                = 0,
                    target.O_order_point              = 0,
                    target.O_order_up_to_point        = 0,
                    target.O_net_inventory            = 0,
                    target.O_due_ind                  = 'N',
                    ---
                    target.O_stock_on_hand            = NULL,
                    target.O_pack_comp_soh            = NULL,
                    target.O_on_order                 = NULL,
                    target.O_in_transit_qty           = NULL,
                    target.O_pack_comp_intran         = NULL,
                    target.O_tsf_resv_qty             = NULL,
                    target.O_pack_comp_resv           = NULL,
                    target.O_tsf_expected_qty         = NULL,
                    target.O_pack_comp_exp            = NULL,
                    target.O_rtv_qty                  = NULL,
                    target.O_alloc_in_qty             = NULL,
                    target.O_alloc_out_qty            = NULL,
                    target.O_non_sellable_qty         = NULL,
                    ---
                    target.O_safety_stock             = NULL,
                    target.O_lost_sales               = NULL,
                    target.O_aso                      = 0,
                    target.O_eso                      = 0,
                    target.O_min_supply_days_forecast = NULL,
                    target.O_max_supply_days_forecast = NULL,
                    target.O_tsh_forecast             = NULL,
                    target.O_curr_olt_forecast        = NULL,
                    target.O_next_olt_forecast        = NULL,
                    target.O_review_time_forecast     = NULL,
                    target.O_isd_forecast             = NULL;
      ---
      delete from svc_repl_roq_gtt w
       where (w.i_sub_item_master is NOT NULL
              or w.i_lead_linked_wh is NOT NULL)
         and w.thread_id = I_thread_id;

   end if;

   return TRUE;

EXCEPTION
when OTHERS then
   O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
   return FALSE;
END REPL_METHOD;
-------------------------------------------------------------------------------
FUNCTION GET_NET_INVENTORY (O_error_message    IN OUT VARCHAR2,
                            I_thread_id        IN NUMBER DEFAULT NULL,
                            I_lost_sales_ind   IN VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(200)  := 'CORESVC_REPL_ROQ_SQL.GET_NET_INVENTORY';


BEGIN
   if I_thread_id is NOT NULL then --do bulk processing
      if NOT GET_LOC_FUTURE_AVAIL(O_error_message,
                                  I_thread_id,
                                  I_lost_sales_ind) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    return FALSE;
END GET_NET_INVENTORY;
-------------------------------------------------------------------------------
FUNCTION GET_LOC_FUTURE_AVAIL
        (O_error_message      IN OUT   VARCHAR2,
         I_thread_id          IN       NUMBER DEFAULT NULL,
         I_lost_sales_ind     IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'CORESVC_REPL_ROQ_SQL.GET_LOC_FUTURE_AVAIL';
   L_plength   NUMBER(2)      := LENGTH('CORESVC_REPL_ROQ_SQL.GET_LOC_FUTURE_AVAIL');
BEGIN

    If I_thread_id is NOT NULL then --do bulk process
      if I_lost_sales_ind = 'N' then
         if NOT GET_TRAN_ITEM_LOC_QTYS(O_error_message,
                                       I_thread_id) then
            return FALSE;
         end if;
      end if;

      if LP_wh_exists = 'Y' then
         if NOT GET_ALLOC_OUT(O_error_message,
                              I_thread_id,
                              I_lost_sales_ind) then
            return FALSE;
         end if;
      end if;

      if NOT GET_CO_TRANSFER_IN (O_error_message,
                                 I_thread_id,
                                 I_lost_sales_ind) then
         return FALSE;
      end if;
      ----
      if NOT GET_ON_ORDER(O_error_message,
                          I_thread_id,
                          I_lost_sales_ind) then
         return FALSE;
      end if;
      ----
      if NOT GET_ALLOC_IN(O_error_message,
                          I_thread_id,
                          I_lost_sales_ind) then
         return FALSE;
      end if;
      ----
      if I_lost_sales_ind = 'N' then
         --merge_1
         L_program := substr(L_program,1,L_plength)||'::MERGE_1';

         merge into svc_repl_roq_gtt target
         using (select wrr.i_item item_id,
                       wrr.i_locn loc_id,
                       ( GREATEST ((
                        (
                        ( GREATEST(wrr.O_stock_on_hand, 0) + wrr.O_pack_comp_soh + wrr.O_in_transit_qty + wrr.O_pack_comp_intran
                          + wrr.O_tsf_expected_qty + wrr.O_pack_comp_exp + wrr.O_on_order + wrr.O_alloc_in_qty )
                        -
                        ( wrr.O_rtv_qty + wrr.O_alloc_out_qty + wrr.O_co_tsf_out_qty + wrr.O_tsf_resv_qty + wrr.O_pack_comp_resv + GREATEST(wrr.O_non_sellable_qty, 0)
                          + GREATEST(wrr.O_pack_comp_non_sellable, 0) + wrr.O_customer_resv + wrr.O_pack_comp_cust_resv )
                        ) - NVL(wrr.I_demo_stock, 0)), 0) - (GREATEST(NVL(wrr.O_customer_backorder, 0), 0) + GREATEST(NVL(wrr.O_pack_comp_cust_back, 0), 0))
                       ) avail_inventory,
                       I_thread_id thread_id
                  from svc_repl_roq_gtt wrr
                 where wrr.thread_id = I_thread_id
                   and wrr.i_sub_item_master is NULL
                   and wrr.i_lead_linked_wh is NULL) use_this
         on (    target.i_item    = use_this.item_id
             and target.i_locn    = use_this.loc_id
             and target.thread_id = use_this.thread_id
             and target.i_sub_item_master is NULL
             and target.i_lead_linked_wh is NULL
             )
         when matched then
            update set target.O_net_inventory = NVL(use_this.avail_inventory, 0);

      else --lost sales is for dynamic repl method only
         --merge_2
         L_program := substr(L_program,1,L_plength)||'::MERGE_2';

         merge into svc_repl_roq_gtt target
         using (select wrr.i_item item_id,
                       wrr.i_locn loc_id,
                       ( GREATEST ((
                        (
                        ( GREATEST(wrr.O_stock_on_hand, 0) + wrr.O_pack_comp_soh + wrr.O_in_transit_qty + wrr.O_pack_comp_intran
                          + wrr.O_tsf_expected_qty + wrr.O_pack_comp_exp + wrr.O_on_order + wrr.O_alloc_in_qty )
                        -
                        ( wrr.O_rtv_qty + wrr.O_alloc_out_qty + wrr.O_tsf_resv_qty + wrr.O_pack_comp_resv + GREATEST(wrr.O_non_sellable_qty, 0)
                        /* CR237 - do NOT subtract quantity reserved for CO transfers/POs from available inventory for the "lost sale" case.
                                   Forecasted sales will take customer sales into account at the store.*/
                          + GREATEST(wrr.O_pack_comp_non_sellable, 0) + wrr.O_customer_resv + wrr.O_pack_comp_cust_resv )
                        ) - NVL(wrr.I_demo_stock, 0) ), 0)
                       ) lost_sales,
                       I_thread_id thread_id
                  from svc_repl_roq_gtt wrr
                 where wrr.thread_id = I_thread_id
                   and wrr.i_sub_item_master is NULL
                   and wrr.i_lead_linked_wh is NULL
                   and ((i_locn_type = 'S' and i_repl_method = 'D')
                        or
                        i_repl_method = 'DI')) use_this
         on (    target.i_item    = use_this.item_id
             and target.i_locn    = use_this.loc_id
             and target.thread_id = use_this.thread_id
             and target.i_sub_item_master is NULL
             and target.i_lead_linked_wh is NULL
             )
         when matched then
            update set target.O_lost_sales = NVL(use_this.lost_sales, 0);
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_LOC_FUTURE_AVAIL;
--------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ON_ORDER(O_error_message    IN OUT   VARCHAR2,
                      I_thread_id        IN       NUMBER DEFAULT NULL,
                      I_lost_sales_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.GET_ON_ORDER';
   L_plength   NUMBER(2)    := LENGTH('CORESVC_REPL_ROQ_SQL.GET_ON_ORDER');
   L_vdate     DATE         := GET_VDATE;
 BEGIN

   If I_thread_id is NOT NULL then -- do bulk process
      -- MERGE_1. for Non-Pack Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(l.qty_ordered - NVL(l.qty_received,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'N'
                and l.item = wrr.i_item
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(use_this.item_qty, 0);

      -- Merge_2 for Non-Pack Items as component
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(p) INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (l.qty_ordered - NVL(l.qty_received,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'N'
                and l.item = p.pack_no
                and p.item = wrr.i_item
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(use_this.item_qty, 0);

      -- Merge_3 for Pack Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_3';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(l.qty_ordered - NVL(l.qty_received,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'Y'
                and l.item = wrr.i_item
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(use_this.item_qty, 0);

      -- Merge_4 for Pack Items as component
      L_program := substr(L_program,1,L_plength)||'::MERGE_4';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(p) INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (l.qty_ordered - NVL(l.qty_received,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'Y'
                and l.item = p.pack_no
                and p.item = wrr.i_item
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(use_this.item_qty, 0);

      -- Merge_5 for Parent Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_5';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(l.qty_ordered - NVL(l.qty_received,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'N'
                and im.item = l.item
                and (   im.item_parent = wrr.i_item
                     or im.item_grandparent = wrr.i_item)
                and wrr.i_sub_item_master is NULL
                and im.item_level = im.tran_level
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(use_this.item_qty, 0);

      -- Merge_6 for Parent Items as component
      L_program := substr(L_program,1,L_plength)||'::MERGE_6';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(p) INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (l.qty_ordered - NVL(l.qty_received,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    packitem p,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'N'
                and l.item = p.pack_no
                and (   im.item_parent = wrr.i_item
                     or im.item_grandparent = wrr.i_item)
                and wrr.i_sub_item_master is NULL
                and im.item_level = im.tran_level
                and im.item = p.item
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(use_this.item_qty, 0);

      -- Merge_7 Non-pack Items for substitutes
      L_program := substr(L_program,1,L_plength)||'::MERGE_7';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(l.qty_ordered - NVL(l.qty_received,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'N'
                and (    l.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(target.O_on_order, 0) + NVL(use_this.item_qty, 0);

      -- Merge_8 Non-pack Items for substitutes as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_8';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(p) INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (l.qty_ordered - NVL(l.qty_received,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'N'
                and l.item = p.pack_no
                and (    p.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(target.O_on_order, 0) + NVL(use_this.item_qty, 0);

      -- Merge_9 Pack Items for substitutes
      L_program := substr(L_program,1,L_plength)||'::MERGE_9';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(l.qty_ordered - NVL(l.qty_received,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'Y'
                and (    l.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(target.O_on_order, 0) + NVL(use_this.item_qty, 0);

      -- Merge_10 Pack Items for substitutes as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_10';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(p) INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (l.qty_ordered - NVL(l.qty_received,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'Y'
                and l.item = p.pack_no
                and (    p.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(target.O_on_order, 0) + NVL(use_this.item_qty, 0);

      -- Merge_11 Parent Items for substitutes
      L_program := substr(L_program,1,L_plength)||'::MERGE_11';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(l.qty_ordered - NVL(l.qty_received,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'N'
                and im.item = l.item
                and ((   im.item_parent = wrr.i_sub_item_master
                      or im.item_grandparent = wrr.i_sub_item_master)
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and im.item_level = im.tran_level
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(target.O_on_order, 0) + NVL(use_this.item_qty, 0);

      -- Merge_12 Parent Items for substitutes as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_12';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(p) INDEX(l)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (l.qty_ordered - NVL(l.qty_received,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from ordloc l,
                    ordhead h,
                    packitem p,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where l.qty_ordered > NVL(l.qty_received,0)
                and l.location = wrr.i_locn
                and l.loc_type = wrr.i_locn_type
                and l.order_no = h.order_no
                and h.status = 'A'
                and (h.not_before_date < case
                                            when wrr.i_repl_method in ('C','M','F','D','DI') then
                                               case
                                                  when I_lost_sales_ind = 'N' then
                                                     wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                  else
                                                     wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                  end
                                            when wrr.i_repl_method = 'SO' then
                                               wrr.i_date
                                            else
                                               wrr.i_date + wrr.i_min_supply_days
                                            end)
                and NOT (    wrr.i_all_orders = 'N'
                         and h.include_on_order_ind = 'N')
                and wrr.i_pack_ind = 'N'
                and l.item = p.pack_no
                and ((   im.item_parent = wrr.i_sub_item_master
                      or im.item_grandparent = wrr.i_sub_item_master)
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and im.item_level = im.tran_level
                and im.item = p.item
                and wrr.thread_id = I_thread_id
                and (   (I_lost_sales_ind = 'N' and h.order_type != 'CO')
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_on_order = NVL(target.O_on_order, 0) + NVL(use_this.item_qty, 0);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ON_ORDER;
--------------------------------------------------------------------------------------
/* New function created for replenishement */
FUNCTION GET_ALLOC_IN(O_error_message    IN OUT   VARCHAR2,
                      I_thread_id        IN       NUMBER DEFAULT NULL,
                      I_lost_sales_ind   IN       VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'CORESVC_REPL_ROQ_SQL.GET_ALLOC_IN';
   L_plength   NUMBER(2)      := LENGTH('CORESVC_REPL_ROQ_SQL.GET_ALLOC_IN');

BEGIN

   if I_thread_id is NOT NULL then -- do bulk process
      --Merge_1 For Non-Pack Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';

      merge into svc_repl_roq_gtt target
      using (select inner.item_id,
                    inner.loc_id ,
                    NVL(SUM(inner.item_qty),0) item_qty,
                    inner.i_thread_id
               from (select wrr.i_item   item_id,
                    wrr.i_locn     loc_id,
                    alloc_in_qty  item_qty,
                    wrr.thread_id i_thread_id
               from rpl_alloc_in_tmp r,
                    svc_repl_roq_gtt wrr
              where (   r.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   r.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and case
                       when wrr.i_repl_method in ('C','M','F','D','DI') then
                          case
                             when I_lost_sales_ind = 'N' then
                                wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                             else
                                wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                             end
                       when wrr.i_repl_method = 'SO' then
                          wrr.i_date
                       else
                          wrr.i_date + wrr.i_min_supply_days
                       end >= r.not_before_date
                and wrr.i_pack_ind = 'N'
                and r.item = wrr.i_item
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              UNION ALL
              select wrr.i_item   item_id,
                     wrr.i_locn     loc_id,
                     (d.qty_allocated - NVL(d.qty_transferred,0))  item_qty,
                     wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and d.alloc_no = h.alloc_no
                and h.status in ('A','R')
                and h.item = wrr.i_item
                and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and wrr.i_pack_ind = 'N'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and ( EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))) inner 
              group by inner.item_id,
                       inner.loc_id,
                       inner.i_thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(use_this.item_qty, 0);

      --Merge_2 For non-pack items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and d.alloc_no = h.alloc_no
                and h.status in ('A','R')
                and h.item = p.pack_no
                and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and wrr.i_pack_ind = 'N'
                and p.item = wrr.i_item
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and NOT (    wrr.i_all_orders = 'N'
                                            and o.include_on_order_ind = 'N')
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(use_this.item_qty, 0);

      --Merge_3 For Pack items
      L_program := substr(L_program,1,L_plength)||'::MERGE_3';

      merge into svc_repl_roq_gtt target
      using (select inner.item_id,
                    inner.loc_id,
                    NVL(SUM(inner.item_qty),0) item_qty,
                    inner.i_thread_id
               from(select wrr.i_item item_id,
                           wrr.i_locn loc_id,
                           alloc_in_qty  item_qty,
                            wrr.thread_id i_thread_id
               from rpl_alloc_in_tmp r,
                    svc_repl_roq_gtt wrr
              where (   r.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   r.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and case
                       when wrr.i_repl_method in ('C','M','F','D','DI') then
                          case
                             when I_lost_sales_ind = 'N' then
                                wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                             else
                                wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                             end
                       when wrr.i_repl_method = 'SO' then
                          wrr.i_date
                       else
                          wrr.i_date + wrr.i_min_supply_days
                       end >= r.not_before_date
                and wrr.i_pack_ind = 'Y'
                and r.item = wrr.i_item
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
               UNION ALL
               select wrr.i_item item_id,
                      wrr.i_locn loc_id,
                      (d.qty_allocated - NVL(d.qty_transferred,0)) item_qty,
                      wrr.thread_id i_thread_id
                from alloc_detail d,
                     alloc_header h,
                     svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and d.alloc_no = h.alloc_no
                and h.status in ('A','R')
                and h.item =  wrr.i_item
                and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and wrr.i_pack_ind = 'Y'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and ( EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1)))inner
              group by inner.item_id,
                       inner.loc_id,
                       inner.i_thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(use_this.item_qty, 0);

      --Merge_4 For pack items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_4';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and d.alloc_no = h.alloc_no
                and h.status in ('A','R')
                and h.item = p.pack_no
                and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and wrr.i_pack_ind = 'Y'
                and p.item = wrr.i_item
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and NOT (    wrr.i_all_orders = 'N'
                                            and o.include_on_order_ind = 'N')
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(use_this.item_qty, 0);

      --Merge_5 For Parent items
      L_program := substr(L_program,1,L_plength)||'::MERGE_5';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.qty_allocated - NVL(d.qty_transferred,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and d.alloc_no = h.alloc_no
                and h.status in ('A','R')
                and h.item = im.item
                and (   im.item_parent = wrr.i_item
                     or im.item_grandparent = wrr.i_item)
                and wrr.i_sub_item_master is NULL
                and im.item_level = im.tran_level
                and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and wrr.i_pack_ind = 'N'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and NOT (    wrr.i_all_orders = 'N'
                                            and o.include_on_order_ind = 'N')
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(use_this.item_qty, 0);

      --Merge_6 For parent items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_6';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    packitem p,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and d.alloc_no = h.alloc_no
                and h.status in ('A','R')
                and h.item = p.pack_no
                and (   im.item_parent = wrr.i_item
                     or im.item_grandparent = wrr.i_item)
                and wrr.i_sub_item_master is NULL
                and im.item_level = im.tran_level
                and p.item = im.item
                and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and wrr.i_pack_ind = 'N'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and NOT (    wrr.i_all_orders = 'N'
                                            and o.include_on_order_ind = 'N')
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(use_this.item_qty, 0);

      --Merge_7 For Non pack Substitute items
      L_program := substr(L_program,1,L_plength)||'::MERGE_7';

      merge into svc_repl_roq_gtt target
      using (select inner.item_id,
                    inner.loc_id,
                    NVL(SUM(inner.item_qty),0) item_qty,
                    inner.i_thread_id
               from( select wrr.i_item item_id,
                            wrr.i_locn loc_id,
                            alloc_in_qty item_qty,
                            wrr.thread_id i_thread_id
                      from rpl_alloc_in_tmp r,
                           svc_repl_roq_gtt wrr
                     where (   r.to_loc = wrr.i_locn
                             or wrr.i_locn is NULL)
                       and (   r.to_loc_type = wrr.i_locn_type
                             or wrr.i_locn_type is NULL)
                       and case
                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                               case
                                 when I_lost_sales_ind = 'N' then
                                 wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                               else
                                 wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                              end
                          when wrr.i_repl_method = 'SO' then
                               wrr.i_date
                          else
                              wrr.i_date + wrr.i_min_supply_days
                          end >= r.not_before_date
                     and wrr.i_pack_ind = 'N'
                     and (    r.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                     and wrr.i_sub_stock_ind = 'Y'
                     and wrr.thread_id = I_thread_id
                     and (   I_lost_sales_ind = 'N'
                          or (    I_lost_sales_ind = 'Y'
                              and wrr.i_repl_method in ('D','DI')))
              UNION ALL
                select wrr.i_item item_id,
                       wrr.i_locn loc_id,
                      (d.qty_allocated - NVL(d.qty_transferred,0)) item_qty,
                      wrr.thread_id i_thread_id
                 from alloc_detail d,
                      alloc_header h,
                      svc_repl_roq_gtt wrr
                where d.qty_allocated > NVL(d.qty_transferred,0)
                  and h.alloc_no = d.alloc_no
                  and h.status in ('A', 'R')
                  and wrr.i_sub_item_master is not NULL
                  and h.item = wrr.i_sub_item_master
                  and wrr.i_item <> wrr.i_sub_item_master
                  and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                  and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                  and wrr.i_pack_ind = 'N'
                  and wrr.i_sub_stock_ind = 'Y'
                  and wrr.thread_id = I_thread_id
                  and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                  and ( EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))) inner
              group by inner.item_id,
                       inner.loc_id,
                       inner.i_thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(target.O_alloc_in_qty, 0) + NVL(use_this.item_qty, 0);

      --Merge_8 For Non pack Substitute items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_8';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and h.alloc_no = d.alloc_no
                and h.status in ('A', 'R')
                and h.item = p.pack_no
                and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and wrr.i_pack_ind = 'N'
                and (    p.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and NOT (    wrr.i_all_orders = 'N'
                                            and o.include_on_order_ind = 'N')
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(target.O_alloc_in_qty, 0) + NVL(use_this.item_qty, 0);

      --Merge_9 Pack Substitute items
      L_program := substr(L_program,1,L_plength)||'::MERGE_9';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(alloc_in_qty),0) item_qty,
                    wrr.thread_id i_thread_id
               from rpl_alloc_in_tmp r,
                    svc_repl_roq_gtt wrr
              where (   r.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   r.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and case
                       when wrr.i_repl_method in ('C','M','F','D','DI') then
                          case
                             when I_lost_sales_ind = 'N' then
                                wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                             else
                                wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                             end
                       when wrr.i_repl_method = 'SO' then
                          wrr.i_date
                       else
                          wrr.i_date + wrr.i_min_supply_days
                       end >= r.not_before_date
                and wrr.i_pack_ind = 'Y'
                and (    r.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(target.O_alloc_in_qty, 0) + NVL(use_this.item_qty, 0);

      --Merge_10 Pack Substitute items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_10';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and h.alloc_no = d.alloc_no
                and h.status in ('A', 'R')
                and h.item = p.pack_no
                and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and wrr.i_pack_ind = 'Y'
                and (    p.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and NOT (    wrr.i_all_orders = 'N'
                                            and o.include_on_order_ind = 'N')
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(target.O_alloc_in_qty, 0) + NVL(use_this.item_qty, 0);

      --Merge_11 Parent Substitute items
      L_program := substr(L_program,1,L_plength)||'::MERGE_11';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.qty_allocated - NVL(d.qty_transferred,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and h.alloc_no = d.alloc_no
                and h.status in ('A', 'R')
                and h.item = im.item
                and ((   im.item_parent = wrr.i_sub_item_master
                      or im.item_grandparent = wrr.i_sub_item_master)
                     and wrr.i_item <> wrr.i_sub_item_master)
                and im.item_level = im.tran_level
                and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and wrr.i_pack_ind = 'N'
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and NOT (    wrr.i_all_orders = 'N'
                                            and o.include_on_order_ind = 'N')
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(target.O_alloc_in_qty, 0) + NVL(use_this.item_qty, 0);

      --Merge_12 Parent Substitute items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_12';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    packitem p,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and h.alloc_no = d.alloc_no
                and h.status in ('A', 'R')
                and h.item = p.pack_no
                and h.wh = wrr.i_locn
                and ((   im.item_parent = wrr.i_sub_item_master
                      or im.item_grandparent = wrr.i_sub_item_master)
                     and wrr.i_item <> wrr.i_sub_item_master)
                and im.item_level = im.tran_level
                and p.item = im.item
                and (   d.to_loc = wrr.i_locn
                     or wrr.i_locn is NULL)
                and (   d.to_loc_type = wrr.i_locn_type
                     or wrr.i_locn_type is NULL)
                and wrr.i_pack_ind = 'N'
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and NOT (    wrr.i_all_orders = 'N'
                                            and o.include_on_order_ind = 'N')
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_in_qty = NVL(target.O_alloc_in_qty, 0) + NVL(use_this.item_qty, 0);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ALLOC_IN;

--------------------------------------------------------------------------------------
FUNCTION GET_ALLOC_OUT(O_error_message    IN OUT   VARCHAR2,
                       I_thread_id        IN       NUMBER DEFAULT NULL,
                       I_lost_sales_ind   IN       VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.GET_ALLOC_OUT';
   L_plength   NUMBER(2)    := LENGTH('CORESVC_REPL_ROQ_SQL.GET_ALLOC_OUT');
   L_vdate     DATE         := GET_VDATE;

BEGIN

   if I_thread_id is NOT NULL then --do bulk process
      -- MERGE_1. Non-Pack Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.qty_allocated - NVL(d.qty_transferred,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and h.alloc_no = d.alloc_no
                and h.status in ('A', 'R')
                and h.item = wrr.i_item
                and h.wh = wrr.i_locn
                and wrr.i_locn_type = 'W'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                UNION
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_out_qty = NVL(use_this.item_qty, 0);

      -- MERGE_2. Non-Pack Items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(p) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and d.alloc_no = h.alloc_no
                and h.status in ('A','R')
                and h.item = p.pack_no
                and h.wh = wrr.i_locn
                and wrr.i_locn_type = 'W'
                and p.item = wrr.i_item
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_out_qty = NVL(use_this.item_qty, 0);

      -- MERGE_3. Parent Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_3';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.qty_allocated - NVL(d.qty_transferred,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and d.alloc_no = h.alloc_no
                and h.status in ('A','R')
                and h.item = im.item
                and h.wh = wrr.i_locn
                and (   im.item_parent = wrr.i_item
                     or im.item_grandparent = wrr.i_item)
                and im.item_level = im.tran_level
                and wrr.i_locn_type = 'W'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_out_qty = NVL(use_this.item_qty, 0);

      -- MERGE_4. Parent Items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_4';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(p) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    packitem p,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where d.qty_allocated > NVL(d.qty_transferred,0)
                and d.alloc_no = h.alloc_no
                and h.status in ('A','R')
                and h.item = p.pack_no
                and h.wh = wrr.i_locn
                and (   im.item_parent = wrr.i_item
                     or im.item_grandparent = wrr.i_item)
                and im.item_level = im.tran_level
                and p.item = im.item
                and wrr.i_locn_type = 'W'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_out_qty = NVL(use_this.item_qty, 0);

      -- MERGE_5. Non-Pack Items for substitute
      L_program := substr(L_program,1,L_plength)||'::MERGE_5';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.qty_allocated - NVL(d.qty_transferred,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    svc_repl_roq_gtt wrr
              where d.alloc_no = h.alloc_no
                and h.status in ('A', 'R')
                and d.qty_allocated > NVL(d.qty_transferred,0)
                and h.wh = NVL(wrr.i_locn, h.wh)
                and wrr.i_locn_type = 'W'
                and (    h.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                UNION
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_out_qty = NVL(target.O_alloc_out_qty,0) + NVL(use_this.item_qty, 0);

      -- MERGE_6. Non-Pack Items for substitute as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_6';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(p) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where h.item = p.pack_no
                and d.alloc_no = h.alloc_no
                and h.status in ('A', 'R')
                and d.qty_allocated > NVL(d.qty_transferred,0)
                and h.wh = NVL(wrr.i_locn, h.wh)
                and wrr.i_locn_type = 'W'
                and (    p.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_out_qty = NVL(target.O_alloc_out_qty,0) + NVL(use_this.item_qty, 0);

      -- MERGE_7. Parent Items for substitute
      L_program := substr(L_program,1,L_plength)||'::MERGE_7';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.qty_allocated - NVL(d.qty_transferred,0)),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where d.alloc_no = h.alloc_no
                and h.status in ('A', 'R')
                and d.qty_allocated > NVL(d.qty_transferred,0)
                and im.item = h.item
                and (    (   im.item_parent = wrr.i_sub_item_master
                          or im.item_grandparent = wrr.i_sub_item_master)
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and im.item_level = im.tran_level
                and h.wh = NVL(wrr.i_locn, h.wh)
                and wrr.i_locn_type = 'W'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_out_qty = NVL(target.O_alloc_out_qty,0) + NVL(use_this.item_qty, 0);

      -- MERGE_8. Parent Items for substitute as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_8';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(p) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(p.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) item_qty,
                    wrr.thread_id i_thread_id
               from alloc_detail d,
                    alloc_header h,
                    packitem p,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where d.alloc_no = h.alloc_no
                and h.status in ('A', 'R')
                and d.qty_allocated > NVL(d.qty_transferred,0)
                and (    (   im.item_parent = wrr.i_sub_item_master
                          or im.item_grandparent = wrr.i_sub_item_master)
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and im.item_level = im.tran_level
                and p.item = im.item
                and h.item = p.pack_no
                and h.wh = NVL(wrr.i_locn, h.wh)
                and wrr.i_locn_type = 'W'
                and wrr.thread_id = I_thread_id
                and (   I_lost_sales_ind = 'N'
                     or (    I_lost_sales_ind = 'Y'
                         and wrr.i_repl_method in ('D','DI')))
                and (   EXISTS (select 'x'
                                  from ordhead o
                                 where o.order_no = h.order_no
                                   and o.status in ('A', 'C')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end >= o.not_before_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from alloc_header h1
                                 where h1.alloc_no = h.order_no
                                   and h1.status in ('A', 'R')
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end = h1.release_date
                                   and ROWNUM = 1)
                     or EXISTS (select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NULL
                                   and ROWNUM = 1
                                union
                                select 'x'
                                  from tsfhead t
                                 where t.tsf_no = h.order_no
                                   and t.status in ('A','S','P','L','C')
                                   and t.not_after_date is NOT NULL
                                   and case
                                          when wrr.i_repl_method in ('C','M','F','D','DI') then
                                             case
                                                when I_lost_sales_ind = 'N' then
                                                   wrr.i_date + wrr.i_next_order_lead_time + wrr.i_review_lead_time
                                                else
                                                   wrr.i_date + NVL(wrr.i_curr_order_lead_time,0)
                                                end
                                          when wrr.i_repl_method = 'SO' then
                                             wrr.i_date
                                          else
                                             wrr.i_date + wrr.i_min_supply_days
                                          end <= t.not_after_date
                                   and ROWNUM = 1))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.i_thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_alloc_out_qty = NVL(target.O_alloc_out_qty,0) + NVL(use_this.item_qty, 0);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ALLOC_OUT;

--------------------------------------------------------------------------------------
FUNCTION GET_CO_TRANSFER_IN(O_error_message    IN OUT   VARCHAR2,
                            I_thread_id        IN       NUMBER DEFAULT NULL,
                            I_lost_sales_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.GET_CO_TRANSFER_IN';
   L_plength   NUMBER(2)    := LENGTH('CORESVC_REPL_ROQ_SQL.GET_CO_TRANSFER_IN');

BEGIN

   if I_thread_id is NOT NULL then --do bulk process
      -- MERGE_1. Non-Pack Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';

      merge into svc_repl_roq_gtt target
         using (select /*+ INDEX(h) INDEX(d)*/
                       wrr.i_item item_id,
                       wrr.i_locn loc_id,
                       NVL(SUM(d.tsf_qty - NVL(d.received_qty,0)),0) item_qty,
                       wrr.thread_id i_thread_id
                  from tsfdetail d,
                       tsfhead h,
                       svc_repl_roq_gtt wrr
                 where h.tsf_no              = d.tsf_no
                   and h.status              IN ('A','S','P','L', 'C')
                   and h.tsf_type            = 'CO'
                   and h.to_loc_type         = wrr.i_locn_type
                   and h.to_loc              = wrr.i_locn
                   and d.tsf_qty             > NVL(d.received_qty,0)
                   and d.item                = wrr.i_item
                   and wrr.i_locn_type       = 'S' --for store loc only
                   and wrr.i_lead_linked_wh  is NULL
                   and wrr.i_pack_ind        = 'N'
                   and wrr.i_sub_item_master is NULL
                   and wrr.thread_id         = I_thread_id
                 group by wrr.i_item,
                          wrr.i_locn,
                          wrr.thread_id) use_this
         on(     target.i_item            = use_this.item_id
             and target.i_locn            = use_this.loc_id
             and target.thread_id         = use_this.i_thread_id
             and target.i_sub_item_master is NULL
             and target.i_lead_linked_wh  is NULL)
         when matched then
            update
               set target.O_co_tsf_out_qty = NVL(target.O_co_tsf_out_qty, 0) + NVL(use_this.item_qty, 0);

      -- MERGE_2. Non-Pack Items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';

      merge into svc_repl_roq_gtt target
         using (select /*+ INDEX(p) INDEX(h) INDEX(d)*/
                       wrr.i_item item_id,
                       wrr.i_locn loc_id,
                       NVL(SUM(p.pack_qty * (d.tsf_qty - NVL(d.received_qty,0))),0) item_qty,
                       wrr.thread_id i_thread_id
                  from tsfdetail d,
                       tsfhead h,
                       packitem p,
                       svc_repl_roq_gtt wrr
                 where h.tsf_no              = d.tsf_no
                   and h.status              IN ('A','S','P','L', 'C')
                   and h.tsf_type            = 'CO'
                   and h.to_loc_type         = wrr.i_locn_type
                   and h.to_loc              = wrr.i_locn
                   and d.tsf_qty             > NVL(d.received_qty,0)
                   and d.item                = p.pack_no
                   and p.item                = wrr.i_item
                   and wrr.i_locn_type       = 'S' --for store loc only
                   and wrr.i_lead_linked_wh  is NULL
                   and wrr.i_pack_ind        = 'N'
                   and wrr.i_sub_item_master is NULL
                   and wrr.thread_id         = I_thread_id
                 group by wrr.i_item,
                          wrr.i_locn,
                          wrr.thread_id) use_this
         on(     target.i_item            = use_this.item_id
             and target.i_locn            = use_this.loc_id
             and target.thread_id         = use_this.i_thread_id
             and target.i_sub_item_master is NULL
             and target.i_lead_linked_wh  is NULL)
         when matched then
            update
               set target.O_co_tsf_out_qty = NVL(target.O_co_tsf_out_qty, 0) + NVL(use_this.item_qty, 0);

      -- MERGE_3. Parent Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_3';

      merge into svc_repl_roq_gtt target
         using (select /*+ INDEX(im) INDEX(h) INDEX(d)*/
                       wrr.i_item item_id,
                       wrr.i_locn loc_id,
                       NVL(SUM(d.tsf_qty - NVL(d.received_qty,0)),0) item_qty,
                       wrr.thread_id
                  from tsfhead h,
                       tsfdetail d,
                       item_master im,
                       svc_repl_roq_gtt wrr
                 where h.tsf_no              = d.tsf_no
                   and h.status              IN ('A','S','P','L', 'C')
                   and h.tsf_type            = 'CO'
                   and h.to_loc_type         = wrr.i_locn_type
                   and h.to_loc              = wrr.i_locn
                   and d.tsf_qty             > NVL(d.received_qty,0)
                   and im.item               = d.item
                   and (   im.item_parent      = wrr.i_item
                        or im.item_grandparent = wrr.i_item)
                   and im.item_level         = im.tran_level
                   and wrr.i_locn_type       = 'S' --for store loc only
                   and wrr.i_lead_linked_wh  is NULL
                   and wrr.i_pack_ind        = 'N'
                   and wrr.i_sub_item_master is NULL
                   and wrr.thread_id         = I_thread_id
                 group by wrr.i_item,
                          wrr.i_locn,
                          wrr.thread_id) use_this
         on (    target.i_item            = use_this.item_id
             and target.i_locn            = use_this.loc_id
             and target.thread_id         = use_this.thread_id
             and target.i_sub_item_master is NULL
             and target.i_lead_linked_wh  is NULL)
         when matched then
            update
               set target.O_co_tsf_out_qty = NVL(use_this.item_qty, 0);

      -- MERGE_4. Parent Items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_4';

      merge into svc_repl_roq_gtt target
         using (select /*+ INDEX(im) INDEX(p) INDEX(h) INDEX(d)*/
                       wrr.i_item item_id,
                       wrr.i_locn loc_id,
                       NVL(SUM(p.pack_qty * (d.tsf_qty - NVL(d.received_qty,0))),0) item_qty,
                       wrr.thread_id
                  from tsfhead h,
                       tsfdetail d,
                       packitem p,
                       item_master im,
                       svc_repl_roq_gtt wrr
                 where h.tsf_no              = d.tsf_no
                   and h.status              IN ('A','S','P','L', 'C')
                   and h.tsf_type            = 'CO'
                   and h.to_loc_type         = wrr.i_locn_type
                   and h.to_loc              = wrr.i_locn
                   and d.tsf_qty             > NVL(d.received_qty,0)
                   and d.item                = p.pack_no
                   and (   im.item_parent      = wrr.i_item
                        or im.item_grandparent = wrr.i_item)
                   and im.item_level         = im.tran_level
                   and im.item               = p.item
                   and wrr.i_locn_type       = 'S' --for store loc only
                   and wrr.i_lead_linked_wh  is NULL
                   and wrr.i_pack_ind        = 'N'
                   and wrr.i_sub_item_master is NULL
                   and wrr.thread_id         = I_thread_id
                 group by wrr.i_item,
                          wrr.i_locn,
                          wrr.thread_id) use_this
         on (    target.i_item            = use_this.item_id
             and target.i_locn            = use_this.loc_id
             and target.thread_id         = use_this.thread_id
             and target.i_sub_item_master is NULL
             and target.i_lead_linked_wh  is NULL)
         when matched then
            update
               set target.O_co_tsf_out_qty = NVL(use_this.item_qty, 0);

      -- MERGE_5. Non-Pack Items for substitute
      L_program := substr(L_program,1,L_plength)||'::MERGE_5';

      merge into svc_repl_roq_gtt target
         using (select /*+ INDEX(h) INDEX(d)*/
                       wrr.i_item item_id,
                       wrr.i_locn loc_id,
                       NVL(SUM(d.tsf_qty - NVL(d.received_qty,0)),0) item_qty,
                       wrr.thread_id
                  from tsfhead h,
                       tsfdetail d,
                       svc_repl_roq_gtt wrr
                 where h.tsf_no             = d.tsf_no
                   and h.status             IN ('A','S','P','L', 'C')
                   and h.tsf_type           = 'CO'
                   and h.to_loc_type        = wrr.i_locn_type
                   and h.to_loc             = wrr.i_locn
                   and d.tsf_qty            > NVL(d.received_qty,0)
                   and (    d.item     =  wrr.i_sub_item_master
                        and wrr.i_item <> wrr.i_sub_item_master)
                   and wrr.i_sub_stock_ind  = 'Y'
                   and wrr.i_locn_type      = 'S' --for store loc only
                   and wrr.i_lead_linked_wh is NULL
                   and wrr.i_pack_ind       = 'N'
                   and wrr.thread_id        = I_thread_id
                 group by wrr.i_item,
                          wrr.i_locn,
                          wrr.thread_id) use_this
         on (    target.i_item            = use_this.item_id
             and target.i_locn            = use_this.loc_id
             and target.thread_id         = use_this.thread_id
             and target.i_sub_item_master is NULL
             and target.i_lead_linked_wh  is NULL)
         when matched then
            update
               set target.O_co_tsf_out_qty = NVL(target.O_co_tsf_out_qty, 0) + NVL(use_this.item_qty, 0);

      -- MERGE_6. Non-Pack Items for substitute as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_6';

      merge into svc_repl_roq_gtt target
         using (select /*+ INDEX(p) INDEX(h) INDEX(d)*/
                       wrr.i_item item_id,
                       wrr.i_locn loc_id,
                       NVL(SUM(p.pack_qty * (d.tsf_qty - NVL(d.received_qty,0))),0) item_qty,
                       wrr.thread_id
                  from tsfhead h,
                       tsfdetail d,
                       packitem p,
                       svc_repl_roq_gtt wrr
                 where h.tsf_no             = d.tsf_no
                   and h.status             IN ('A','S','P','L', 'C')
                   and h.tsf_type           = 'CO'
                   and h.to_loc_type        = wrr.i_locn_type
                   and h.to_loc             = wrr.i_locn
                   and d.tsf_qty            > NVL(d.received_qty,0)
                   and d.item               = p.pack_no
                   and (    p.item     =  wrr.i_sub_item_master
                        and wrr.i_item <> wrr.i_sub_item_master)
                   and wrr.i_sub_stock_ind  = 'Y'
                   and wrr.i_locn_type      = 'S' --for store loc only
                   and wrr.i_lead_linked_wh is NULL
                   and wrr.i_pack_ind       = 'N'
                   and wrr.thread_id        = I_thread_id
                 group by wrr.i_item,
                          wrr.i_locn,
                          wrr.thread_id) use_this
         on (    target.i_item            = use_this.item_id
             and target.i_locn            = use_this.loc_id
             and target.thread_id         = use_this.thread_id
             and target.i_sub_item_master is NULL
             and target.i_lead_linked_wh  is NULL)
         when matched then
            update
               set target.O_co_tsf_out_qty = NVL(target.O_co_tsf_out_qty, 0) + NVL(use_this.item_qty, 0);

      -- MERGE_7. Parent Items for substitute
      L_program := substr(L_program,1,L_plength)||'::MERGE_7';

      merge into svc_repl_roq_gtt target
         using (select /*+ INDEX(im) INDEX(h) INDEX(d)*/
                       wrr.i_item item_id,
                       wrr.i_locn loc_id,
                       NVL(SUM(d.tsf_qty - NVL(d.received_qty,0)),0) item_qty,
                       wrr.thread_id
                  from tsfhead h,
                       tsfdetail d,
                       item_master im,
                       svc_repl_roq_gtt wrr
                 where h.tsf_no             = d.tsf_no
                   and h.status             IN ('A','S','P','L', 'C')
                   and h.tsf_type           = 'CO'
                   and h.to_loc_type        = wrr.i_locn_type
                   and h.to_loc             = wrr.i_locn
                   and d.tsf_qty            > NVL(d.received_qty,0)
                   and im.item              = d.item
                   and (   (    im.item_parent =  wrr.i_sub_item_master
                            and wrr.i_item     <> wrr.i_sub_item_master)
                        or (    im.item_grandparent =  wrr.i_sub_item_master
                            and wrr.i_item          <> wrr.i_sub_item_master))
                   and im.item_level        = im.tran_level
                   and wrr.i_sub_stock_ind  = 'Y'
                   and wrr.i_locn_type      = 'S' --for store loc only
                   and wrr.i_lead_linked_wh is NULL
                   and wrr.i_pack_ind       = 'N'
                   and wrr.thread_id        = I_thread_id
                 group by wrr.i_item,
                          wrr.i_locn,
                          wrr.thread_id) use_this
         on (    target.i_item            = use_this.item_id
             and target.i_locn            = use_this.loc_id
             and target.thread_id         = use_this.thread_id
             and target.i_sub_item_master is NULL
             and target.i_lead_linked_wh  is NULL)
         when matched then
            update
               set target.O_co_tsf_out_qty = NVL(target.O_co_tsf_out_qty, 0) + NVL(use_this.item_qty, 0);

      -- MERGE_8. Parent Items for substitute as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_8';

      merge into svc_repl_roq_gtt target
         using (select /*+ INDEX(im) INDEX(p) INDEX(h) INDEX(d)*/
                       wrr.i_item item_id,
                       wrr.i_locn loc_id,
                       NVL(SUM(p.pack_qty * (d.tsf_qty - NVL(d.received_qty,0))),0) item_qty,
                       wrr.thread_id thread_id
                  from tsfhead h,
                       tsfdetail d,
                       packitem p,
                       item_master im,
                       svc_repl_roq_gtt wrr
                 where h.tsf_no             = d.tsf_no
                   and h.status             IN ('A','S','P','L', 'C')
                   and h.tsf_type           = 'CO'
                   and h.to_loc_type        = wrr.i_locn_type
                   and h.to_loc             = wrr.i_locn
                   and d.tsf_qty            > NVL(d.received_qty,0)
                   and d.item               = p.pack_no
                   and (   (    im.item_parent =  wrr.i_sub_item_master
                            and wrr.i_item     <> wrr.i_sub_item_master)
                        or (    im.item_grandparent =  wrr.i_sub_item_master
                            and wrr.i_item          <> wrr.i_sub_item_master))
                   and im.item_level        = im.tran_level
                   and im.item              = p.item
                   and wrr.i_pack_ind       = 'N'
                   and wrr.i_sub_stock_ind  = 'Y'
                   and wrr.i_locn_type      = 'S' --for store loc only
                   and wrr.i_lead_linked_wh is NULL
                   and wrr.thread_id        = I_thread_id
                 group by wrr.i_item,
                          wrr.i_locn,
                          wrr.thread_id) use_this
         on (    target.i_item            = use_this.item_id
             and target.i_locn            = use_this.loc_id
             and target.thread_id         = use_this.thread_id
             and target.i_sub_item_master is NULL
             and target.i_lead_linked_wh  is NULL)
         when matched then
            update
               set target.O_co_tsf_out_qty = NVL(target.O_co_tsf_out_qty, 0) + NVL(use_this.item_qty, 0);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_CO_TRANSFER_IN;

--------------------------------------------------------------------------------------
FUNCTION GET_PL_TRANSFER(O_error_message      IN OUT   VARCHAR2,
                         I_thread_id          IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.GET_PL_TRANSFER';
   L_plength   NUMBER(2)    := LENGTH('CORESVC_REPL_ROQ_SQL.GET_PL_TRANSFER');
   L_vdate     DATE         := GET_VDATE;

BEGIN

   if I_thread_id is NOT NULL then -- do bulk process

      -- MERGE_1. Non-Pack Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.tsf_qty - NVL(d.ship_qty,0)),0) item_qty,
                    0 pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and d.item = wrr.i_item
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'N'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(use_this.pack_qty, 0);

      -- MERGE_2. Non-pack Items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(p) INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    0 item_qty,
                    NVL(SUM(p.pack_qty * (d.tsf_qty - NVL(d.ship_qty, 0))),0) pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and d.item = p.pack_no
                and p.item = wrr.i_item
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'N'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(use_this.pack_qty, 0);

      -- MERGE_3. For Pack Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_3';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.tsf_qty - NVL(d.ship_qty,0)),0) item_qty,
                    0 pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and d.item = wrr.i_item
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'Y'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(use_this.pack_qty, 0);

      -- MERGE_4. For Pack Items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_4';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(p) INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    0 item_qty,
                    NVL(SUM(p.pack_qty * (d.tsf_qty - NVL(d.ship_qty,0))),0) pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and d.item = p.pack_no
                and p.item = wrr.i_item
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'Y'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(use_this.pack_qty, 0);

      -- MERGE_5. For Parent Items
      L_program := substr(L_program,1,L_plength)||'::MERGE_5';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.tsf_qty - NVL(d.ship_qty,0)),0) item_qty,
                    0 pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and im.item = d.item
                and (   im.item_parent = wrr.i_item
                     or im.item_grandparent = wrr.i_item)
                and im.item_level = im.tran_level
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'N'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(use_this.pack_qty, 0);

      -- MERGE_6. For Parent Items as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_6';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(p) INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    0 item_qty,
                    NVL(SUM(p.pack_qty * (d.tsf_qty - NVL(d.ship_qty,0))),0) pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    packitem p,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and d.item = p.pack_no
                and (   im.item_parent = wrr.i_item
                     or im.item_grandparent = wrr.i_item)
                and im.item_level = im.tran_level
                and im.item = p.item
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'N'
                and wrr.i_sub_item_master is NULL
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(use_this.pack_qty, 0);

      -- MERGE_7. For Non-pack Items for substitute
      L_program := substr(L_program,1,L_plength)||'::MERGE_7';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.tsf_qty - NVL(d.ship_qty,0)),0) item_qty,
                    0 pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and (    d.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'N'
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(target.O_tsf_expected_qty,0) + NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(target.O_pack_comp_exp,0) + NVL(use_this.pack_qty, 0);

      -- MERGE_8. For Non-pack Items for substitute as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_8';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(p) INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    0 item_qty,
                    NVL(SUM(p.pack_qty * (d.tsf_qty - NVL(d.ship_qty, 0))),0) pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and d.item = p.pack_no
                and (    p.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'N'
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(target.O_tsf_expected_qty,0) + NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(target.O_pack_comp_exp,0) + NVL(use_this.pack_qty, 0);

      -- MERGE_9. For Pack Items for substitute
      L_program := substr(L_program,1,L_plength)||'::MERGE_9';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.tsf_qty - NVL(d.ship_qty,0)),0) item_qty,
                    0 pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and (    d.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'Y'
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(target.O_tsf_expected_qty,0) + NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(target.O_pack_comp_exp,0) + NVL(use_this.pack_qty, 0);

      -- MERGE_10. For pack Items for substitute as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_10';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(p) INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    0 item_qty,
                    NVL(SUM(p.pack_qty * (d.tsf_qty - NVL(d.ship_qty,0))),0) pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    packitem p,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and d.item = p.pack_no
                and (    p.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'Y'
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(target.O_tsf_expected_qty,0) + NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(target.O_pack_comp_exp,0) + NVL(use_this.pack_qty, 0);

      -- MERGE_11. For Parent Items for substitute
      L_program := substr(L_program,1,L_plength)||'::MERGE_11';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(d.tsf_qty - NVL(d.ship_qty,0)),0) item_qty,
                    0 pack_qty,
                    wrr.thread_id
               from tsfhead h,
                    tsfdetail d,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and im.item = d.item
                and (   (    im.item_parent = wrr.i_sub_item_master
                         and wrr.i_item <> wrr.i_sub_item_master)
                     or (    im.item_grandparent = wrr.i_sub_item_master
                         and wrr.i_item <> wrr.i_sub_item_master))
                and im.item_level = im.tran_level
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.i_pack_ind = 'N'
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(target.O_tsf_expected_qty,0) + NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(target.O_pack_comp_exp,0) + NVL(use_this.pack_qty, 0);

      -- MERGE_12. For Parent Items for substitute as components
      L_program := substr(L_program,1,L_plength)||'::MERGE_12';

      merge into svc_repl_roq_gtt target
      using (select /*+ INDEX(im) INDEX(p) INDEX(h) INDEX(d)*/
                    wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    0 item_qty,
                    NVL(SUM(p.pack_qty * (d.tsf_qty - NVL(d.ship_qty,0))),0) pack_qty,
                    wrr.thread_id thread_id
               from tsfhead h,
                    tsfdetail d,
                    packitem p,
                    item_master im,
                    svc_repl_roq_gtt wrr
              where h.tsf_no = d.tsf_no
                and h.status in ('A','E','S','B')
                and h.tsf_type = 'PL'
                and h.to_loc_type = wrr.i_locn_type
                and h.to_loc = wrr.i_locn
                and d.tsf_qty > NVL(d.ship_qty,0)
                and d.item = p.pack_no
                and (   (    im.item_parent = wrr.i_sub_item_master
                         and wrr.i_item <> wrr.i_sub_item_master)
                     or (    im.item_grandparent = wrr.i_sub_item_master
                         and wrr.i_item <> wrr.i_sub_item_master))
                and im.item_level = im.tran_level
                and im.item = p.item
                and wrr.i_pack_ind = 'N'
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.i_locn_type = 'S' --for store loc only
                and wrr.i_lead_linked_wh is NULL
                and wrr.thread_id = I_thread_id
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_tsf_expected_qty = NVL(target.O_tsf_expected_qty,0) + NVL(use_this.item_qty, 0),
                    target.O_pack_comp_exp    = NVL(target.O_pack_comp_exp,0) + NVL(use_this.pack_qty, 0);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PL_TRANSFER;

--------------------------------------------------------------------------------------
FUNCTION GET_TRAN_ITEM_LOC_QTYS
        (O_error_message        IN OUT   VARCHAR2,
         I_thread_id            IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)        := 'CORESVC_REPL_ROQ_SQL.GET_TRAN_ITEM_LOC_QTYS';
   L_plength   NUMBER(2)           := LENGTH('CORESVC_REPL_ROQ_SQL.GET_TRAN_ITEM_LOC_QTYS');
   L_vdate     PERIOD.VDATE%TYPE   := GET_VDATE;

BEGIN

   if (LP_wh_crosslink_ind is NULL) then
     if NOT SYSTEM_OPTIONS_SQL.GET_WH_CROSS_LINK_IND(O_error_message,
                                                     LP_wh_crosslink_ind) then
        return FALSE;
     end if;
   end if;

   if I_thread_id is NOT NULL then --do bulk process

      --Merge_1. Update inventory fields of main items for store or wh loc
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';

      merge into svc_repl_roq_gtt target
      using (select /*+ index(ils) */ ils.item item_id,
                    ils.loc loc_id,
                    w.i_repl_pack repl_pack_id,
                    NVL(ils.stock_on_hand,0) stock_on_hand,
                    NVL(ils.pack_comp_soh,0) pack_comp_soh,
                    NVL(ils.in_transit_qty,0) in_transit_qty,
                    NVL(ils.pack_comp_intran,0) pack_comp_intran,
                    NVL(ils.tsf_reserved_qty,0) tsf_reserved_qty,
                    NVL(ils.pack_comp_resv,0) pack_comp_resv,
                    NVL(ils.tsf_expected_qty,0) tsf_expected_qty,
                    NVL(ils.pack_comp_exp,0) pack_comp_exp,
                    NVL(ils.rtv_qty,0) rtv_qty,
                    NVL(ils.non_sellable_qty,0) non_sellable_qty,
                    NVL(ils.pack_comp_non_sellable,0) pack_comp_non_sellable,
                    NVL(ils.customer_resv,0) customer_resv,
                    NVL(ils.customer_backorder,0) customer_backorder,
                    NVL(ils.pack_comp_cust_resv,0) pack_comp_cust_resv,
                    NVL(ils.pack_comp_cust_back,0) pack_comp_cust_back,
                    (w.I_next_order_lead_time + w.I_review_lead_time + GET_VDATE) i_so_to_date,
                    case
                       when w.I_reject_store_ord_ind = 'N' then
                          TO_DATE('19010101','YYYYMMDD')
                       else
                          (w.I_curr_order_lead_time + TO_DATE(GET_VDATE))
                       end i_so_from_date,
                    TO_DATE('19010101','YYYYMMDD') i_so_no_from_date,
                    0 alloc_out_qty,
                    0 ss_alloc_out_qty,
                    0 on_order,
                    0 ss_on_order,
                    0 alloc_in_qty,
                    0 ss_alloc_in_qty,
                    0 co_tsf_out_qty,
                    w.thread_id
               from svc_repl_roq_gtt w,
                    item_loc_soh ils
              where ils.item = w.i_item
                and ils.loc = DECODE(w.i_locn_type,
                                     'S', w.i_locn, --store loc
                                     'W', NVL(w.i_lead_linked_wh,w.i_locn), --wh under the wh.repl_wh_link
                                     w.i_locn)
                and ils.loc_type = w.i_locn_type
                and w.i_sub_item_master is NULL
                and w.thread_id = I_thread_id ) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and NVL(target.i_repl_pack, target.i_item) = NVL(use_this.repl_pack_id, use_this.item_id)
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_stock_on_hand          = GREATEST(use_this.stock_on_hand, 0),
                    target.O_pack_comp_soh          = GREATEST(use_this.pack_comp_soh, 0),
                    target.O_in_transit_qty         = GREATEST(use_this.in_transit_qty, 0),
                    target.O_pack_comp_intran       = GREATEST(use_this.pack_comp_intran, 0),
                    target.O_tsf_expected_qty       = GREATEST(use_this.tsf_expected_qty, 0),
                    target.O_pack_comp_exp          = GREATEST(use_this.pack_comp_exp, 0),
                    target.O_rtv_qty                = GREATEST(use_this.rtv_qty, 0),
                    target.O_tsf_resv_qty           = GREATEST(use_this.tsf_reserved_qty, 0),
                    target.O_pack_comp_resv         = GREATEST(use_this.pack_comp_resv, 0),
                    target.O_non_sellable_qty       = GREATEST(use_this.non_sellable_qty, 0),
                    target.O_customer_resv          = GREATEST(use_this.customer_resv, 0),
                    target.O_customer_backorder     = GREATEST(use_this.customer_backorder, 0),
                    target.O_pack_comp_cust_resv    = GREATEST(use_this.pack_comp_cust_resv, 0),
                    target.O_pack_comp_cust_back    = GREATEST(use_this.pack_comp_cust_back, 0),
                    target.I_so_to_date             = use_this.i_so_to_date,
                    target.I_so_from_date           = use_this.i_so_from_date,
                    target.I_so_no_from_date        = use_this.i_so_no_from_date,
                    target.O_ss_tsf_expected_qty    = GREATEST(use_this.tsf_expected_qty, 0),
                    target.O_ss_pack_comp_exp       = GREATEST(use_this.pack_comp_exp, 0),
                    target.O_alloc_out_qty          = use_this.alloc_out_qty,
                    target.O_ss_alloc_out_qty       = use_this.ss_alloc_out_qty,
                    target.O_on_order               = use_this.on_order,
                    target.O_ss_on_order            = use_this.ss_on_order,
                    target.O_alloc_in_qty           = use_this.alloc_in_qty,
                    target.O_ss_alloc_in_qty        = use_this.ss_alloc_in_qty,
                    target.O_co_tsf_out_qty         = use_this.co_tsf_out_qty,
                    target.O_pack_comp_non_sellable = use_this.pack_comp_non_sellable;

      --Merge_2. Update inventory fields of sub-items for store or wh loc
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';

      merge into svc_repl_roq_gtt target
      using (select /*+ index(ils) */ wrr.i_item item_id,
                    ils.loc loc_id,
                    wrr.i_repl_pack repl_pack_id,
                    SUM(NVL(GREATEST(ils.stock_on_hand,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                     ils.loc,
                                                                     wrr.i_repl_pack,
                                                                     wrr.thread_id) stock_on_hand,
                    SUM(NVL(GREATEST(ils.pack_comp_soh,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                     ils.loc,
                                                                     wrr.i_repl_pack,
                                                                     wrr.thread_id) pack_comp_soh,
                    SUM(NVL(GREATEST(ils.in_transit_qty,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                      ils.loc,
                                                                      wrr.i_repl_pack,
                                                                      wrr.thread_id) in_transit_qty,
                    SUM(NVL(GREATEST(ils.pack_comp_intran,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                        ils.loc,
                                                                        wrr.i_repl_pack,
                                                                        wrr.thread_id) pack_comp_intran,
                    SUM(NVL(GREATEST(ils.tsf_reserved_qty,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                        ils.loc,
                                                                        wrr.i_repl_pack,
                                                                        wrr.thread_id) tsf_reserved_qty,
                    SUM(NVL(GREATEST(ils.pack_comp_resv,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                      ils.loc,
                                                                      wrr.i_repl_pack,
                                                                      wrr.thread_id) pack_comp_resv,
                    SUM(NVL(GREATEST(ils.tsf_expected_qty,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                        ils.loc,
                                                                        wrr.i_repl_pack,
                                                                        wrr.thread_id) tsf_expected_qty,
                    SUM(NVL(GREATEST(ils.pack_comp_exp,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                     ils.loc,
                                                                     wrr.i_repl_pack,
                                                                     wrr.thread_id) pack_comp_exp,
                    SUM(NVL(GREATEST(ils.rtv_qty,0),0)) OVER (PARTITION BY wrr.i_item,
                                                               ils.loc,
                                                               wrr.i_repl_pack,
                                                               wrr.thread_id) rtv_qty,
                    SUM(NVL(GREATEST(ils.non_sellable_qty,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                        ils.loc,
                                                                        wrr.i_repl_pack,
                                                                        wrr.thread_id) non_sellable_qty,
                    SUM(NVL(ils.pack_comp_non_sellable,0)) OVER (PARTITION BY wrr.i_item,
                                                                              ils.loc,
                                                                              wrr.i_repl_pack,
                                                                              wrr.thread_id) pack_comp_non_sellable,
                    SUM(NVL(GREATEST(ils.customer_resv,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                     ils.loc,
                                                                     wrr.i_repl_pack,
                                                                     wrr.thread_id) customer_resv,
                    SUM(NVL(GREATEST(ils.customer_backorder,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                          ils.loc,
                                                                          wrr.i_repl_pack,
                                                                          wrr.thread_id) customer_backorder,
                    SUM(NVL(GREATEST(ils.pack_comp_cust_resv,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                           ils.loc,
                                                                           wrr.i_repl_pack,
                                                                           wrr.thread_id) pack_comp_cust_resv,
                    SUM(NVL(GREATEST(ils.pack_comp_cust_back,0),0)) OVER (PARTITION BY wrr.i_item,
                                                                           ils.loc,
                                                                           wrr.i_repl_pack,
                                                                           wrr.thread_id) pack_comp_cust_back,
                    (wrr.I_next_order_lead_time + wrr.I_review_lead_time + GET_VDATE) i_so_to_date,
                    case
                       when wrr.I_reject_store_ord_ind = 'N' then
                          TO_DATE('19010101','YYYYMMDD')
                       else
                          (wrr.I_curr_order_lead_time + TO_DATE(GET_VDATE))
                       end i_so_from_date,
                    TO_DATE('19010101','YYYYMMDD') i_so_no_from_date,
                    0 alloc_out_qty,
                    0 ss_alloc_out_qty,
                    0 on_order,
                    0 ss_on_order,
                    0 alloc_in_qty,
                    0 ss_alloc_in_qty,
                    0 co_tsf_out_qty,
                    wrr.thread_id,
        RANK() OVER (PARTITION BY wrr.i_item,
                          ils.loc,
                          wrr.i_repl_pack,
                          wrr.thread_id
                          ORDER BY wrr.i_item,
                                   ils.loc,
                                   wrr.i_repl_pack,
                                   wrr.thread_id,
                                   wrr.i_sub_item_master) rankorder
               from svc_repl_roq_gtt wrr,
                    item_loc_soh ils
              where (    ils.item = wrr.i_sub_item_master
                     and wrr.i_item <> wrr.i_sub_item_master)
                and ils.loc = DECODE(wrr.i_locn_type,
                                     'S', wrr.i_locn, --store loc
                                     'W', NVL(wrr.i_lead_linked_wh,wrr.i_locn), --wh under the wh.repl_wh_link
                                     wrr.i_locn)
                and ils.loc_type = wrr.i_locn_type
                and wrr.i_sub_stock_ind = 'Y'
                and wrr.thread_id = I_thread_id ) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and NVL(target.i_repl_pack, target.i_item) = NVL(use_this.repl_pack_id, use_this.item_id)
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL
          and use_this.rankorder = 1)

      when matched then
         update set target.O_stock_on_hand       = target.O_stock_on_hand        + NVL(use_this.stock_on_hand ,0),
                    target.O_pack_comp_soh       = target.O_pack_comp_soh        + NVL(use_this.pack_comp_soh ,0),
                    target.O_in_transit_qty      = target.O_in_transit_qty       + NVL(use_this.in_transit_qty ,0),
                    target.O_pack_comp_intran    = target.O_pack_comp_intran     + NVL(use_this.pack_comp_intran ,0),
                    target.O_tsf_expected_qty    = target.O_tsf_expected_qty     + NVL(use_this.tsf_expected_qty ,0),
                    target.O_pack_comp_exp       = target.O_pack_comp_exp        + NVL(use_this.pack_comp_exp ,0),
                    target.O_rtv_qty             = target.O_rtv_qty              + NVL(use_this.rtv_qty ,0),
                    target.O_tsf_resv_qty        = target.O_tsf_resv_qty         + NVL(use_this.tsf_reserved_qty ,0),
                    target.O_pack_comp_resv      = target.O_pack_comp_resv       + NVL(use_this.pack_comp_resv ,0),
                    target.O_non_sellable_qty    = target.O_non_sellable_qty     + NVL(use_this.non_sellable_qty ,0),
                    target.O_customer_resv       = target.O_customer_resv        + NVL(use_this.customer_resv ,0),
                    target.O_customer_backorder  = target.O_customer_backorder   + NVL(use_this.customer_backorder ,0),
                    target.O_pack_comp_cust_resv = target.O_pack_comp_cust_resv  + NVL(use_this.pack_comp_cust_resv ,0),
                    target.O_pack_comp_cust_back = target.O_pack_comp_cust_back  + NVL(use_this.pack_comp_cust_back ,0),
                    target.I_so_to_date          = use_this.i_so_to_date,
                    target.I_so_from_date        = use_this.i_so_from_date,
                    target.I_so_no_from_date     = use_this.i_so_no_from_date,
                    target.O_ss_tsf_expected_qty = target.O_ss_tsf_expected_qty  + NVL(use_this.tsf_expected_qty ,0),
                    target.O_ss_pack_comp_exp    = target.O_ss_pack_comp_exp     + NVL(use_this.pack_comp_exp ,0),
                    target.O_alloc_out_qty       = target.O_alloc_out_qty        + NVL(use_this.alloc_out_qty ,0),
                    target.O_ss_alloc_out_qty    = target.O_ss_alloc_out_qty     + NVL(use_this.ss_alloc_out_qty ,0),
                    target.O_on_order            = target.O_on_order             + NVL(use_this.on_order ,0),
                    target.O_ss_on_order         = target.O_ss_on_order          + NVL(use_this.ss_on_order ,0),
                    target.O_alloc_in_qty        = target.O_alloc_in_qty         + NVL(use_this.alloc_in_qty ,0),
                    target.O_ss_alloc_in_qty     = target.O_ss_alloc_in_qty      + NVL(use_this.ss_alloc_in_qty ,0),
                    target.O_co_tsf_out_qty      = target.O_co_tsf_out_qty       + NVL(use_this.co_tsf_out_qty,0),
                    target.O_pack_comp_non_sellable = target.O_pack_comp_non_sellable + NVL(use_this.pack_comp_non_sellable,0);

      L_program := substr(L_program,1,L_plength);

      if LP_wh_crosslink_ind = 'Y' then
         if NOT GET_PL_TRANSFER(O_error_message,
                                I_thread_id) then
            return FALSE;
         end if;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TRAN_ITEM_LOC_QTYS;
-------------------------------------------------------------------------------

FUNCTION ADD_SUB_ITEMS(O_error_message    IN OUT VARCHAR2,
                       I_thread_id        IN     NUMBER)
RETURN BOOLEAN IS

   L_program                VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.ADD_SUB_ITEMS';

BEGIN

   insert into svc_repl_roq_gtt(i_item,
                                i_locn,
                                i_locn_type,
                                i_date,
                                i_all_orders,
                                i_order_point,
                                i_repl_ind,
                                i_item_parent,
                                i_item_grandparent,
                                i_pack_ind,
                                i_item_level,
                                i_tran_level,
                                i_sub_item_master,
                                i_sub_forecast_ind,
                                i_sub_stock_ind,
                                ---
                                O_net_inventory,
                                I_curr_order_lead_time,
                                I_reject_store_ord_ind,
                                I_next_order_lead_time,
                                I_review_lead_time,
                                ---
                                i_time_supply_horizon,
                                i_phase_end,
                                i_min_supply_days,
                                i_max_supply_days,
                                i_domain_id,
                                ---
                                I_last_delivery_date,
                                I_next_delivery_date,
                                i_repl_method,
                                ---
                                thread_id,
                                i_add_lead_time_ind)
                         select wrr.i_item,                 --main item
                                wrr.I_locn,
                                wrr.I_locn_type,
                                wrr.i_date,
                                wrr.i_all_orders,
                                wrr.i_order_point,
                                wrr.i_repl_ind,
                                im.item_parent,
                                im.item_grandparent,
                                im.pack_ind,
                                im.item_level,
                                im.tran_level,
                                sid.sub_item,               --i_sub_item_master --> substitute item
                                sih.use_forecast_sales_ind, --I_sub_forecast_ind
                                sih.use_stock_ind,          --I_sub_stock_ind
                                ---
                                wrr.O_net_inventory,
                                wrr.I_curr_order_lead_time,
                                wrr.I_reject_store_ord_ind,
                                wrr.I_next_order_lead_time,
                                wrr.I_review_lead_time,
                                ---
                                wrr.i_time_supply_horizon,
                                wrr.i_phase_end,
                                wrr.i_min_supply_days,
                                wrr.i_max_supply_days,
                                wrr.i_domain_id,
                                ---
                                wrr.I_last_delivery_date,
                                wrr.I_next_delivery_date,
                                wrr.i_repl_method,
                                ---
                                I_thread_id,
                                wrr.i_add_lead_time_ind
                           from svc_repl_roq_gtt wrr,
                                sub_items_head sih,
                                sub_items_detail sid,
                                item_master im
                          where wrr.i_item      = sih.item
                            and sih.item        = sid.item
                            and sid.sub_item    = im.item
                            and sih.location    = sid.location
                            and (wrr.I_locn_type='S' and  wrr.I_STOCK_CAT in ('W','L') )
                            and wrr.i_source_wh = sid.location
                            and wrr.thread_id   = I_thread_id
                            union all
                        select wrr.i_item,                 --main item
                                wrr.I_locn,
                                wrr.I_locn_type,
                                wrr.i_date,
                                wrr.i_all_orders,
                                wrr.i_order_point,
                                wrr.i_repl_ind,
                                im.item_parent,
                                im.item_grandparent,
                                im.pack_ind,
                                im.item_level,
                                im.tran_level,
                                sid.sub_item,               --i_sub_item_master --> substitute item
                                sih.use_forecast_sales_ind, --I_sub_forecast_ind
                                sih.use_stock_ind,          --I_sub_stock_ind
                                ---
                                wrr.O_net_inventory,
                                wrr.I_curr_order_lead_time,
                                wrr.I_reject_store_ord_ind,
                                wrr.I_next_order_lead_time,
                                wrr.I_review_lead_time,
                                ---
                                wrr.i_time_supply_horizon,
                                wrr.i_phase_end,
                                wrr.i_min_supply_days,
                                wrr.i_max_supply_days,
                                wrr.i_domain_id,
                                ---
                                wrr.I_last_delivery_date,
                                wrr.I_next_delivery_date,
                                wrr.i_repl_method,
                                ---
                                I_thread_id,
                                wrr.i_add_lead_time_ind
                           from svc_repl_roq_gtt wrr,
                                sub_items_head sih,
                                sub_items_detail sid,
                                item_master im
                          where wrr.i_item      = sih.item
                            and sih.item        = sid.item
                            and sid.sub_item    = im.item
                            and sih.location    = sid.location
                            and ((wrr.I_locn_type='S' and  wrr.I_STOCK_CAT not in ('W','L'))
                                 or wrr.I_locn_type = 'W' )
                            and wrr.I_locn = sid.location
                            and wrr.thread_id   = I_thread_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_SUB_ITEMS;
-------------------------------------------------------------------------------
FUNCTION ADD_WH_LINKS(O_error_message    IN OUT VARCHAR2,
                      I_thread_id        IN     NUMBER)
RETURN BOOLEAN IS

   L_program                VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.ADD_WH_LINKS';

BEGIN

   insert into svc_repl_roq_gtt (i_item,
                                 i_locn,
                                 i_locn_type,
                                 i_date,
                                 i_all_orders,
                                 i_order_point,
                                 i_repl_ind,
                                 i_item_parent,
                                 i_item_grandparent,
                                 i_pack_ind,
                                 i_item_level,
                                 i_tran_level,
                                 i_sub_item_master,
                                 i_lead_linked_wh,
                                 i_repl_pack,
                                 i_repl_pack_qty,
                                 O_net_inventory,
                                 o_stock_on_hand,
                                 o_pack_comp_soh,
                                 o_in_transit_qty,
                                 o_pack_comp_intran,
                                 o_tsf_expected_qty,
                                 o_pack_comp_exp,
                                 o_on_order,
                                 O_alloc_in_qty,
                                 o_rtv_qty,
                                 O_alloc_out_qty,
                                 O_tsf_resv_qty,
                                 o_pack_comp_resv,
                                 o_non_sellable_qty,
                                 thread_id,
                                 i_add_lead_time_ind)
                          select w.i_item,
                                 w.I_locn,
                                 w.I_locn_type,
                                 w.i_date,
                                 w.i_all_orders,
                                 w.i_order_point,
                                 w.i_repl_ind,
                                 w.i_item_parent,
                                 w.i_item_grandparent,
                                 w.i_pack_ind,
                                 w.i_item_level,
                                 w.i_tran_level,
                                 w.i_sub_item_master,
                                 wh.wh,                     --i_lead_linked_wh,
                                 w.i_repl_pack,
                                 w.i_repl_pack_qty,
                                 NULL,                      --O_net_inventory,
                                 NULL,                      --o_stock_on_hand,
                                 NULL,                      --o_pack_comp_soh,
                                 NULL,                      --o_in_transit_qty,
                                 NULL,                      --o_pack_comp_intran,
                                 NULL,                      --o_tsf_expected_qty,
                                 NULL,                      --o_pack_comp_exp,
                                 NULL,                      --o_on_order,
                                 NULL,                      --O_alloc_in_qty,
                                 NULL,                      --o_rtv_qty,
                                 NULL,                      --O_alloc_out_qty,
                                 NULL,                      --O_tsf_resv_qty,
                                 NULL,                      --o_pack_comp_resv,
                                 NULL,                      --bo_non_sellable_qty
                                 I_thread_id,
                                 w.i_add_lead_time_ind
                            from svc_repl_roq_gtt w,
                                 wh wh
                           where w.I_locn    = wh.repl_wh_link
                             and w.I_locn   != wh.wh
                             and w.thread_id = I_thread_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_WH_LINKS;
-------------------------------------------------------------------------------
FUNCTION ADD_PACK_COMP(O_error_message    IN OUT VARCHAR2,
                       I_thread_id        IN     NUMBER)
RETURN BOOLEAN IS

   L_program                VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.ADD_PACK_COMP';

BEGIN

   insert into svc_repl_roq_gtt (i_item,
                                 I_locn,
                                 I_locn_type,
                                 i_date,
                                 i_all_orders,
                                 i_order_point,
                                 i_repl_ind,
                                 i_item_parent,
                                 i_item_grandparent,
                                 i_pack_ind,
                                 i_item_level,
                                 i_tran_level,
                                 i_sub_item_master,
                                 i_lead_linked_wh,
                                 i_repl_pack,
                                 i_repl_pack_qty,
                                 O_net_inventory,
                                 o_stock_on_hand,
                                 o_pack_comp_soh,
                                 o_in_transit_qty,
                                 o_pack_comp_intran,
                                 o_tsf_expected_qty,
                                 o_pack_comp_exp,
                                 o_on_order,
                                 O_alloc_in_qty,
                                 o_rtv_qty,
                                 O_alloc_out_qty,
                                 O_tsf_resv_qty,
                                 o_pack_comp_resv,
                                 o_non_sellable_qty,
                                 thread_id,
                                 i_add_lead_time_ind)
                          select p.item,
                                 w.I_locn,
                                 w.I_locn_type,
                                 w.i_date,
                                 w.i_all_orders,
                                 w.i_order_point,
                                 w.i_repl_ind,
                                 im.item_parent,
                                 im.item_grandparent,
                                 im.pack_ind,
                                 im.item_level,
                                 im.tran_level,
                                 w.i_sub_item_master,
                                 w.i_lead_linked_wh,
                                 w.i_item,                  --i_repl_pack
                                 p.pack_qty,                --i_repl_pack_qty
                                 NULL,                      --O_net_inventory,
                                 NULL,                      --o_stock_on_hand,
                                 NULL,                      --o_pack_comp_soh,
                                 NULL,                      --o_in_transit_qty,
                                 NULL,                      --o_pack_comp_intran,
                                 NULL,                      --o_tsf_expected_qty,
                                 NULL,                      --o_pack_comp_exp,
                                 NULL,                      --o_on_order,
                                 NULL,                      --O_alloc_in_qty,
                                 NULL,                      --o_rtv_qty,
                                 NULL,                      --O_alloc_out_qty,
                                 NULL,                      --O_tsf_resv_qty,
                                 NULL,                      --o_pack_comp_resv,
                                 NULL,                      --bo_non_sellable_qty
                                 I_thread_id,
                                 w.i_add_lead_time_ind
                            from svc_repl_roq_gtt w,
                                 packitem p,
                                 item_master im
                           where w.i_item    = p.pack_no
                             and p.item      = im.item
                             and w.thread_id = I_thread_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_PACK_COMP;
-------------------------------------------------------------------------------
FUNCTION BULK_FORECAST(O_error_message   IN OUT   VARCHAR2,
                       I_thread_id       IN       NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.BULK_FORECAST';

BEGIN

   --Bulk Processing of Forecast information for Floating Point, Time Supply and Dynamic methods

   -- Legend:
   -- v_svc_repl_roq_gtt   - driving cursor
   -- v_svc_repl_roq_gtt_2 - creates new rows for the ff repl methods: Time supply issues,Time supply,Floating point and Min/Max.
   -- v_svc_repl_roq_gtt_3 - removes uncessary rows from v_svc_repl_roq_gtt_2
   -- dif1              - daily_item_forecast calcuation
   -- dif               - dif1 table with eow calcuation.
   -- qry_get_forecast  - item_forecast calculation.
   -- tbl               - sums up the dif and qry_get_forecast calculation.
   merge into svc_repl_roq_gtt target
   using (WITH
             v_svc_repl_roq_gtt AS
                (select i_item,
                        i_locn,
                        i_domain_id,
                        thread_id,
                        vdate,
                        curr_454_day,
                        i_sub_item_master,
                        i_lead_linked_wh,
                        --FP forecast
                        case
                           when i_repl_method = 'F' and
                                i_sub_item_master is NULL then --this will be for the main item only
                              i_last_delivery_date
                           end first_date_fp_forecast,
                        case
                           when i_repl_method = 'F' and
                                i_sub_item_master is NULL then --this will be for the main item only
                              i_next_delivery_date
                           end last_date_fp_forecast,
                        --TSH forecast
                        case
                           when i_repl_method in ('T', 'TI') and
                                NVL(i_time_supply_horizon,0) > 0 then
                              case
                                 when i_add_lead_time_ind = 'Y' then
                                    LP_vdate + NVL(i_supp_lead_time, 0) + NVL(i_pickup_lead_time,0) + NVL(i_wh_lead_time,0)
                                 else
                                    LP_vdate
                                 end
                           end first_date_tsh_forecast,
                        case
                           when i_repl_method in ('T', 'TI') and
                                NVL(i_time_supply_horizon,0) > 0 then
                              case
                                 when i_add_lead_time_ind = 'Y' then
                                    LP_vdate + i_time_supply_horizon + NVL(i_supp_lead_time, 0) + NVL(i_pickup_lead_time,0) + NVL(i_wh_lead_time,0)
                                 else
                                    LP_vdate + i_time_supply_horizon
                                 end
                           end last_date_tsh_forecast,
                        --Min forecast
                        case
                           when i_repl_method in ('T', 'TI') and
                                NVL(i_time_supply_horizon,0) <= 0 then
                              LP_vdate
                           end first_date_min_forecast,
                        case
                           when i_repl_method in ('T', 'TI') and
                                NVL(i_time_supply_horizon,0) <= 0 then
                              case
                                 when (NVL(i_phase_end, i_min_supply_days + LP_vdate + 1) - LP_vdate <= i_min_supply_days) then
                                    i_phase_end
                                 else
                                    LP_vdate + i_min_supply_days
                                 end
                           end last_date_min_forecast,
                        --Max forecast
                        case
                           when i_repl_method in ('T', 'TI') and
                                NVL(i_time_supply_horizon,0) <= 0 then
                              LP_vdate
                           end first_date_max_forecast,
                        case
                           when i_repl_method in ('T', 'TI') and
                                NVL(i_time_supply_horizon,0) <= 0 then
                              case
                                 when (NVL(i_phase_end, i_max_supply_days + LP_vdate + 1) - LP_vdate <= i_max_supply_days) then
                                    i_phase_end
                                 when (NVL(i_phase_end, i_max_supply_days + LP_vdate + 1) - LP_vdate > i_max_supply_days) then
                                    LP_vdate + i_max_supply_days
                                 end
                           end last_date_max_forecast
                   from svc_repl_roq_gtt,
                        period
                  where (   (    i_locn_type = 'S'
                             and i_repl_method = 'T')
                         or i_repl_method in ('F','TI'))
                    and thread_id = I_thread_id
                    and i_sub_forecast_ind = 'Y'), --this includes sub-items
             --
             v_svc_repl_roq_gtt_2 AS
                (select i_item,
                        i_locn,
                        i_domain_id,
                        thread_id,
                        vdate,
                        curr_454_day,
                        i_sub_item_master,
                        i_lead_linked_wh,
                        forecast_type,
                        first_date,
                        last_date
                  from v_svc_repl_roq_gtt
                 model
                    return updated rows
                    partition by (i_item,
                                  i_locn,
                                  i_domain_id,
                                  thread_id,
                                  vdate,
                                  curr_454_day,
                                  i_sub_item_master,
                                  i_lead_linked_wh)
                    dimension by (0 as i)
                    measures     (cast(NULL as varchar2(12)) as forecast_type,
                                  cast(NULL as date) as first_date,
                                  cast(NULL as date) as last_date,
                                  first_date_fp_forecast,
                                  last_date_fp_forecast,
                                  first_date_tsh_forecast,
                                  last_date_tsh_forecast,
                                  first_date_min_forecast,
                                  last_date_min_forecast,
                                  first_date_max_forecast,
                                  last_date_max_forecast)
                    rules upsert all
                       (forecast_type[1] = 'FP_FORECAST',
                        forecast_type[2] = 'TSH_FORECAST',
                        forecast_type[3] = 'MIN_FORECAST',
                        forecast_type[4] = 'MAX_FORECAST',
                        --
                        first_date   [1] = first_date_fp_forecast[0],
                        first_date   [2] = first_date_tsh_forecast[0],
                        first_date   [3] = first_date_min_forecast[0],
                        first_date   [4] = first_date_max_forecast[0],
                        --
                        last_date    [1] = last_date_fp_forecast[0],
                        last_date    [2] = last_date_tsh_forecast[0],
                        last_date    [3] = last_date_min_forecast[0],
                        last_date    [4] = last_date_max_forecast[0])),
             --
             v_svc_repl_roq_gtt_3 AS
                (select i_item,
                        i_locn,
                        i_domain_id,
                        thread_id,
                        vdate,
                        curr_454_day,
                        i_sub_item_master,
                        i_lead_linked_wh,
                        forecast_type,
                        first_date,
                        last_date
                  from v_svc_repl_roq_gtt_2
                 where first_date is NOT NULL),
             --
             dif1 AS
                (select wrr.i_item,
                        wrr.i_locn,
                        wrr.thread_id,
                        wrr.i_domain_id,
                        wrr.vdate,
                        wrr.curr_454_day,
                        wrr.i_sub_item_master,
                        wrr.i_lead_linked_wh,
                        wrr.forecast_type,
                        SUM(NVL(dif.forecast_sales,0)) forecast_sales,
                        case
                           when MAX(dif.data_date) is NULL then
                              MIN(wrr.first_date)
                           else
                              MAX(dif.data_date)
                           end max_daily_date,
                        MIN(wrr.first_date) first_date,
                        MAX(wrr.last_date) last_date
                   from daily_item_forecast dif,
                        v_svc_repl_roq_gtt_3 wrr
                  where NVL(wrr.i_sub_item_master, wrr.i_item) = dif.item(+)
                    and NVL(wrr.i_lead_linked_wh, wrr.i_locn)  = dif.loc(+)
                    and wrr.i_domain_id = dif.domain_id(+)
                    and wrr.first_date  < dif.data_date(+)
                    and wrr.last_date   >= dif.data_date(+)
                  GROUP BY wrr.i_item,
                           wrr.i_locn,
                           wrr.thread_id,
                           wrr.i_domain_id,
                           wrr.vdate,
                           wrr.curr_454_day,
                           wrr.i_sub_item_master,
                           wrr.i_lead_linked_wh,
                           wrr.forecast_type),
             --
             dif AS
                (select dif1.i_item,
                        dif1.i_locn,
                        dif1.thread_id,
                        dif1.i_domain_id,
                        dif1.forecast_type,
                        dif1.vdate,
                        dif1.curr_454_day,
                        dif1.i_sub_item_master,
                        dif1.i_lead_linked_wh,
                        dif1.forecast_sales,
                        dif1.max_daily_date,
                        dif1.first_date,
                        dif1.last_date,
                        case
                           when MOD(ABS((vdate + (7-curr_454_day)) - last_date),7) = 0 then
                              dif1.last_date
                           else
                              dif1.last_date + (7 - MOD(ABS((vdate + (7-curr_454_day)) - last_date),7))
                           end eow_date
                   from dif1),
             --
             qry_get_forecast AS
                (select /*+ INDEX(ifc pk_item_forecast)*/
                        dif.i_item,
                        dif.i_locn,
                        dif.thread_id,
                        dif.i_domain_id,
                        dif.forecast_type,
                        case
                           when ROW_NUMBER() OVER (PARTITION BY ifc.item,
                                                                ifc.loc,
                                                                dif.forecast_type
                                                          ORDER BY ifc.eow_date) = 1 then
                               (ifc.forecast_sales / 7) * to_number((least(ifc.eow_date,dif.last_date)) - (greatest(dif.max_daily_date,ifc.eow_date - 7)))
                           else
                               (ifc.forecast_sales / 7) * to_number((7 - (greatest((ifc.eow_date - (dif.last_date)),0))))
                           end item_forecast_sales
                   from dif,
                        item_forecast ifc
                  where ifc.item      = NVL(dif.i_sub_item_master, dif.i_item)
                    and ifc.loc       = NVL(dif.i_lead_linked_wh, dif.i_locn)
                    and ifc.domain_id = dif.i_domain_id
                    and ifc.eow_date between dif.max_daily_date and dif.eow_date)
               -- end of WITH clause
            select tbl.i_item,
                   tbl.i_locn,
                   tbl.i_domain_id,
                   thread_id,
                   SUM(tbl.fp_forecast)  fp_forecast,
                   SUM(tbl.tsh_forecast) tsh_forecast,
                   SUM(tbl.min_forecast) min_forecast,
                   SUM(tbl.max_forecast) max_forecast
             from (select i_item,
                          i_locn,
                          i_domain_id,
                          thread_id,
                          DECODE(forecast_type, 'FP_FORECAST',  NVL(round(dif.forecast_sales,4),0), 0) fp_forecast,
                          DECODE(forecast_type, 'TSH_FORECAST', NVL(round(dif.forecast_sales,4),0), 0) tsh_forecast,
                          DECODE(forecast_type, 'MIN_FORECAST', NVL(round(dif.forecast_sales,4),0), 0) min_forecast,
                          DECODE(forecast_type, 'MAX_FORECAST', NVL(round(dif.forecast_sales,4),0), 0) max_forecast
                     from dif
                    union all
                   select i_item,
                          i_locn,
                          i_domain_id,
                          thread_id,
                          DECODE(forecast_type, 'FP_FORECAST',  NVL(round(item_forecast_sales,4),0), 0) fp_forecast,
                          DECODE(forecast_type, 'TSH_FORECAST', NVL(round(item_forecast_sales,4),0), 0) tsh_forecast,
                          DECODE(forecast_type, 'MIN_FORECAST', NVL(round(item_forecast_sales,4),0), 0) min_forecast,
                          DECODE(forecast_type, 'MAX_FORECAST', NVL(round(item_forecast_sales,4),0), 0) max_forecast
                    from qry_get_forecast) tbl
            group by tbl.i_item,
                     tbl.i_locn,
                     tbl.i_domain_id,
                     tbl.thread_id) use_this
   on (    target.i_item      = use_this.i_item
       and target.i_locn      = use_this.i_locn
       and NVL(target.i_domain_id, 0) = NVL(use_this.i_domain_id, 0)
       and target.thread_id   = use_this.thread_id
       and target.i_sub_item_master is NULL
       and target.i_lead_linked_wh is NULL)
   when matched then
      update set target.O_safety_stock             = case
                                                        when target.i_repl_method        = 'F' and
                                                             target.i_service_level_type = LP_srvce_lvl_type_simple_sales then
                                                           use_this.fp_forecast * ( NVL(target.i_service_level, 0) /100)
                                                        end,
                 target.O_tsh_forecast             = case
                                                        when target.i_repl_method in ('T','TI') and
                                                             target.i_time_supply_horizon > 0 then
                                                           use_this.tsh_forecast
                                                        end,
                 target.O_min_supply_days_forecast = case
                                                        when target.i_repl_method in ('T','TI') and
                                                             NVL(target.i_time_supply_horizon, 0) <= 0 then
                                                           use_this.min_forecast
                                                        end,
                 target.O_max_supply_days_forecast = case
                                                        when target.i_repl_method in ('T','TI') and
                                                             NVL(target.i_time_supply_horizon, 0) <= 0 then
                                                           use_this.max_forecast
                                                        end;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BULK_FORECAST;
-------------------------------------------------------------------------------
FUNCTION GET_CONSTANT_MINMAX_FLOAT_TS_D (O_error_message        IN OUT   VARCHAR2,
                                         I_thread_id            IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'CORESVC_REPL_ROQ_SQL.GET_CONSTANT_MINMAX_FLOAT_TS_D';
   L_plength   NUMBER(2)      := LENGTH('CORESVC_REPL_ROQ_SQL.GET_CONSTANT_MINMAX_FLOAT_TS_D');

BEGIN
   if I_thread_id is NOT NULL then -- do bulk process
      --merge_1
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';

      merge into svc_repl_roq_gtt target
      using (select w.i_item,
                    w.i_locn,
                    w.thread_id,
                    --
                    case
                       when w.i_repl_method in ('C','F') then
                          GREATEST(LEAST(w.i_max_stock * w.i_incr_pct/100, 99999999.9999),w.i_pres_stock)
                       when w.i_repl_method = 'M' then
                          GREATEST(LEAST(w.i_min_stock * w.i_incr_pct/100, 99999999.9999),w.i_pres_stock)
                       when w.i_repl_method in ('T','D') and
                            w.i_locn_type = 'W' then
                          LEAST(GREATEST(w.i_store_need, w.i_pres_stock),99999999.9999)
                       when w.i_repl_method in ('T', 'TI') and
                            NVL(w.i_time_supply_horizon,0) > 0 then
                         case
                            when NVL(w.i_phase_end, w.i_min_supply_days + LP_vdate + 1) - LP_vdate <= w.i_min_supply_days then
                               LEAST(NVL(w.o_tsh_forecast, 0) / w.i_time_supply_horizon * (w.i_phase_end-LP_vdate) +
                                     w.i_terminal_stock_qty,99999999.9999)
                            else
                               LEAST(GREATEST(NVL(w.o_tsh_forecast, 0) / w.i_time_supply_horizon * w.i_min_supply_days,
                                     w.i_pres_stock), 99999999.9999)
                            end
                       when w.i_repl_method in ('T', 'TI') and
                            NVL(w.i_time_supply_horizon,0) <= 0 then
                         case
                            when NVL(w.i_phase_end, w.i_min_supply_days + LP_vdate + 1) - LP_vdate <= w.i_min_supply_days then
                               LEAST(NVL(w.o_min_supply_days_forecast,0) + w.i_terminal_stock_qty, 99999999.9999)
                            else
                               LEAST(GREATEST(NVL(w.o_min_supply_days_forecast,0), w.i_pres_stock), 99999999.9999)
                            end
                       else
                          NVL(w.o_order_point,0)
                       end o_order_point,
                    --
                    case
                       when w.i_repl_method in ('C','M','F') then
                          GREATEST(LEAST(w.i_max_stock * w.i_incr_pct/100, 99999999.9999),w.i_pres_stock)
                       when w.i_repl_method in('T','D') and w.i_locn_type = 'W' then
                          LEAST(GREATEST(w.i_store_need, w.i_pres_stock),99999999.9999)
                       when w.i_repl_method in ('T', 'TI') and NVL(w.i_time_supply_horizon,0) > 0 then
                          case
                             when NVL(w.i_phase_end, w.i_min_supply_days + LP_vdate + 1) - LP_vdate <= w.i_min_supply_days then
                                LEAST(NVL(w.o_tsh_forecast,0) / w.i_time_supply_horizon * (w.i_phase_end-LP_vdate) +
                                      w.i_terminal_stock_qty,99999999.9999)
                             else
                                case
                                   when NVL(w.i_phase_end, w.i_max_supply_days + LP_vdate + 1) - LP_vdate <= w.i_max_supply_days then
                                      LEAST(NVL(w.o_tsh_forecast,0) / w.i_time_supply_horizon * (w.i_phase_end-LP_vdate) +
                                            w.i_terminal_stock_qty,99999999.9999)
                                   else
                                      LEAST(GREATEST(NVL(w.o_tsh_forecast,0) / w.i_time_supply_horizon * w.i_max_supply_days,
                                            w.i_pres_stock), 99999999.9999)
                                   end
                             end
                       when w.i_repl_method in ('T', 'TI') and
                            NVL(w.i_time_supply_horizon,0) <= 0 then
                          case
                             when NVL(w.i_phase_end, w.i_min_supply_days + LP_vdate + 1) - LP_vdate <= w.i_min_supply_days then
                                LEAST(NVL(w.o_min_supply_days_forecast,0) + w.i_terminal_stock_qty, 99999999.9999)
                             when NVL(w.i_phase_end, + w.i_max_supply_days + LP_vdate + 1) - LP_vdate <= w.i_max_supply_days then
                                LEAST(NVL(w.o_max_supply_days_forecast,0) + w.i_terminal_stock_qty, 99999999.9999)
                             else
                                LEAST(GREATEST(NVL(w.o_max_supply_days_forecast,0), w.i_pres_stock), 99999999.9999)
                             end
                       else
                          NVL(w.o_order_up_to_point,0)
                       end o_order_up_to_point,
                    --
                    case
                       when w.i_repl_method in ('C','F') then
                          decode(sign(GREATEST(LEAST(w.i_max_stock * w.i_incr_pct/100, 99999999.9999),w.i_pres_stock) - w.o_net_inventory),
                                 -1, 'N',
                                  0, 'N',
                                  1, 'Y')
                       when w.i_repl_method in ('T','D') and
                            w.i_locn_type = 'W' and
                            NVL(w.i_due_ord_process_ind,'N') = 'N' then
                          decode(sign(LEAST(GREATEST(w.i_store_need, w.i_pres_stock),99999999.9999) - w.o_net_inventory),
                                 -1, 'N',
                                  0, 'N',
                                  1, 'Y')
                       when w.i_repl_method = 'M' then
                          decode(sign(GREATEST(LEAST(w.i_min_stock * w.i_incr_pct/100, 99999999.9999),w.i_pres_stock) - w.o_net_inventory),
                                 -1, 'N',
                                  0, 'N',
                                  1, 'Y')
                       else
                          w.o_due_ind
                       end o_due_ind,
                    --
                    case
                       when w.i_repl_method in ('C','M','F') then
                          case
                             when (GREATEST(LEAST(w.i_max_stock * w.i_incr_pct/100, 99999999.9999),w.i_pres_stock) - w.o_net_inventory) < 0 then
                                0
                             else
                                GREATEST(LEAST(w.i_max_stock * w.i_incr_pct/100, 99999999.9999),w.i_pres_stock) - w.o_net_inventory
                             end
                       when w.i_repl_method in ('T','D') and
                            w.i_locn_type = 'W' then
                          case when (LEAST(GREATEST(w.i_store_need, w.i_pres_stock),99999999.9999) - w.o_net_inventory) < 0 then
                             0
                          else
                             LEAST(GREATEST(w.i_store_need, w.i_pres_stock),99999999.9999) - w.o_net_inventory
                          end
                       else
                          NVL(w.o_order_qty,0)
                       end o_order_qty
               from svc_repl_roq_gtt w
              where w.i_repl_method in ('C','M','F','T','TI','D','SO')
                and w.i_sub_item_master is NULL
                and w.i_lead_linked_wh is NULL
                and w.thread_id = I_thread_id) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_order_qty         = NVL(use_this.o_order_qty,0),
                    target.O_order_point       = NVL(use_this.o_order_point,0),
                    target.O_order_up_to_point = NVL(use_this.o_order_up_to_point,0),
                    target.O_due_ind           = use_this.o_due_ind;

      --merge_2
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';

      merge into svc_repl_roq_gtt target
      using (select w.i_item,
                    w.i_locn,
                    w.thread_id,
                    NVL(w.O_order_up_to_point,0) - w.o_net_inventory O_order_qty,
                    NVL(w.O_order_up_to_point,0) O_order_up_to_point,
                    case
                       when NVL(w.o_order_point,0) <= w.o_net_inventory then
                          'N'
                       else
                          'Y'
                       end O_due_ind
               from svc_repl_roq_gtt w
              where i_repl_method in ('T','TI')
                and w.i_sub_item_master is NULL
                and w.i_lead_linked_wh is NULL
                and w.thread_id = I_thread_id) use_this /*is: exclude T at WH locs */
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_order_qty         = NVL(use_this.o_order_qty,0),
                    target.O_order_up_to_point = NVL(use_this.o_order_up_to_point,0),
                    target.O_due_ind           = use_this.o_due_ind;

   end if;

   return TRUE;

EXCEPTION
when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
   return FALSE;
END GET_CONSTANT_MINMAX_FLOAT_TS_D;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_LOCN_NEED_DYNAMIC(O_error_message        IN OUT   VARCHAR2,
                               I_thread_id            IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN IS
   L_program   VARCHAR2(200) := 'CORESVC_REPL_ROQ_SQL.GET_LOCN_NEED_DYNAMIC';
   L_plength   NUMBER(2)     := LENGTH('CORESVC_REPL_ROQ_SQL.GET_LOCN_NEED_DYNAMIC');

BEGIN
   if I_thread_id is NOT NULL then -- do bulk process
      if NOT GET_FORECAST_FOR_PERIOD_DYN (O_error_message,
                                          I_thread_id) then
         return FALSE;
      end if;
      --
      --Merge_1 for safety stock
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';
      --
      --Legends:
      --v_svc_repl_roq_gtt  : working table, set-up the colt, nolt, review, and isd dates
      --dif              : get the forecast_std_dev from the daily_item_forecast table, records fethed here will NOT be processed using the item_forecast table.
      --set_ifc          : get the remaining records from the working tables that should be setup for item_forecast computation of forecast_std_dev.
      --forecast_std_dev : compute for the average and sum on forecast_std_dev
      --ss_helper        : compute for the exp_units_short used for safety stock calculation
      --std_dev_lookup   : get the in-between sets of high and low data (used for safety stock calculation) for the exp_units_short and safet_stock_std_dev.
      --sdl              : computation of safety stock.
      merge into svc_repl_roq_gtt target
      using (with
                v_svc_repl_roq_gtt AS
                   (select i_item,
                           i_locn,
                           i_domain_id,
                           i_sub_item_master,
                           i_lead_linked_wh,
                           o_next_olt_forecast,
                           o_review_time_forecast,
                           o_ss_forecast,
                           i_service_level,
                           i_terminal_stock_qty,
                           i_pres_stock,
                           i_phase_end,
                           thread_id,
                           LP_vdate init_date,
                           LP_vdate + NVL(i_curr_order_lead_time,0) colt_date,
                           LP_vdate + NVL(i_next_order_lead_time,0) nolt_date,
                           LP_vdate + NVL(i_next_order_lead_time,0) + NVL(i_review_lead_time,0) review_lead_date,
                           LP_vdate + NVL(i_curr_order_lead_time,0) + NVL(i_inv_selling_days,0) isd_date
                     from svc_repl_roq_gtt
                    where (   (    i_locn_type = 'S'
                               and i_repl_method = 'D')
                           or i_repl_method = 'DI')
                      and thread_id = I_thread_id
                      and i_sub_forecast_ind = 'Y' --this will include the sub_item
                      and NVL(i_phase_end,(LP_vdate + NVL(i_next_order_lead_time,0) + NVL(i_review_lead_time,0)) + 1)
                          > LP_vdate + NVL(i_next_order_lead_time,0) + NVL(i_review_lead_time,0)), --phase end > review lead date
                --
                dif as
                   (select vwrr.i_item,
                           vwrr.i_locn,
                           vwrr.thread_id,
                           vwrr.i_domain_id,
                           vwrr.i_sub_item_master,
                           vwrr.i_lead_linked_wh,
                           vwrr.o_next_olt_forecast,
                           vwrr.o_review_time_forecast,
                           vwrr.o_ss_forecast,
                           vwrr.i_service_level,
                           vwrr.i_terminal_stock_qty,
                           vwrr.i_pres_stock,
                           vwrr.i_phase_end,
                           vwrr.init_date,
                           vwrr.colt_date,
                           vwrr.nolt_date,
                           vwrr.review_lead_date,
                           vwrr.isd_date,
                           NVL(dif.forecast_std_dev,0) forecast_std_dev
                      from daily_item_forecast dif,
                           v_svc_repl_roq_gtt vwrr
                     where NVL(vwrr.i_sub_item_master, vwrr.i_item) = dif.item
                       and NVL(vwrr.i_lead_linked_wh, vwrr.i_locn)  = dif.loc
                       and vwrr.i_domain_id      = dif.domain_id
                       and vwrr.review_lead_date = dif.data_date
                       and vwrr.thread_id        = I_thread_id),
                --
                set_ifc as
                   (select vwrr.i_item,
                           vwrr.i_locn,
                           vwrr.thread_id,
                           vwrr.i_domain_id,
                           vwrr.i_sub_item_master,
                           vwrr.i_lead_linked_wh,
                           vwrr.o_next_olt_forecast,
                           vwrr.o_review_time_forecast,
                           vwrr.o_ss_forecast,
                           vwrr.i_service_level,
                           vwrr.i_terminal_stock_qty,
                           vwrr.i_pres_stock,
                           vwrr.i_phase_end,
                           vwrr.init_date,
                           vwrr.colt_date,
                           vwrr.nolt_date,
                           vwrr.review_lead_date,
                           vwrr.isd_date
                      from v_svc_repl_roq_gtt vwrr
                     minus
                    select dif.i_item,
                           dif.i_locn,
                           dif.thread_id,
                           dif.i_domain_id,
                           dif.i_sub_item_master,
                           dif.i_lead_linked_wh,
                           dif.o_next_olt_forecast,
                           dif.o_review_time_forecast,
                           dif.o_ss_forecast,
                           dif.i_service_level,
                           dif.i_terminal_stock_qty,
                           dif.i_pres_stock,
                           dif.i_phase_end,
                           dif.init_date,
                           dif.colt_date,
                           dif.nolt_date,
                           dif.review_lead_date,
                           dif.isd_date
                      from dif),
                --
                forecast_std_dev as
                   (select i_item,
                           i_locn,
                           i_domain_id,
                           thread_id,
                           sum(forecast_std_dev) forecast_std_dev,
                           avg(forecast_std_dev) forecast_std_dev_avg
                      from (select i_item,
                                   i_locn,
                                   i_domain_id,
                                   thread_id,
                                   forecast_std_dev
                              from dif
                             union all
                            select /*+ INDEX(ifc pk_item_forecast)*/
                                   vwrr2.i_item,
                                   vwrr2.i_locn,
                                   vwrr2.i_domain_id,
                                   vwrr2.thread_id,
                                   round(NVL(ifc.forecast_std_dev *
                                      sqrt(1 - ((to_number(to_char(to_date(LP_vdate + 1), 'D')) - to_number(to_char(to_date(vwrr2.review_lead_date), 'D')) + 6) /
                                          (vwrr2.review_lead_date - LP_vdate + to_number(to_char(to_date(LP_vdate + 1), 'D')) - to_number(to_char(to_date(vwrr2.review_lead_date), 'D'))
                                           + 6))),0)
                                      , 4) forecast_std_dev
                              from item_forecast ifc,
                                   set_ifc vwrr2
                             where NVL(vwrr2.i_sub_item_master, vwrr2.i_item) = ifc.item(+)
                               and NVL(vwrr2.i_lead_linked_wh, vwrr2.i_locn)  = ifc.loc(+)
                               and vwrr2.i_domain_id = ifc.domain_id(+)
                               and ifc.eow_date(+) >= vwrr2.review_lead_date
                               and ifc.eow_date(+) <  vwrr2.review_lead_date + 7
                               and vwrr2.thread_id   = I_thread_id)
                     group by i_item,
                              i_locn,
                              i_domain_id,
                              thread_id),
                --
                ss_helper as
                   (select wrr.i_item,
                           wrr.i_locn,
                           wrr.i_domain_id,
                           wrr.thread_id,
                           wrr.i_repl_method,
                           wrr.i_service_level,
                           wrr.i_service_level_type,
                           wrr.o_review_time_forecast,
                           wrr.o_ss_forecast,
                           fsd.forecast_std_dev_avg,
                           (((1 - (NVL(wrr.i_service_level,0)/100)) * wrr.o_review_time_forecast)
                                      /  greatest(fsd.forecast_std_dev_avg,1)) exp_unit_short
                      from svc_repl_roq_gtt wrr,
                           forecast_std_dev fsd
                     where wrr.i_item = fsd.i_item
                       and wrr.i_locn = fsd.i_locn
                       and wrr.i_domain_id = fsd.i_domain_id
                       and wrr.thread_id = I_thread_id
                       and wrr.i_sub_item_master is NULL --for main item only
                       and wrr.i_lead_linked_wh is NULL
                       and (   (wrr.i_locn_type = 'S' and wrr.i_repl_method = 'D')
                            or wrr.i_repl_method = 'DI')
                       and NVL(i_phase_end,(LP_vdate + NVL(i_next_order_lead_time,0) + NVL(i_review_lead_time,0)) + 1)
                           > LP_vdate + NVL(i_next_order_lead_time,0) + NVL(i_review_lead_time,0)), --phase end > review lead date
                --
                std_dev_lookup as
                   (select tbl2.i_item,
                           tbl2.i_locn,
                           tbl2.i_domain_id,
                           tbl2.thread_id,
                           tbl2.i_repl_method,
                           tbl2.i_service_level,
                           tbl2.i_service_level_type,
                           tbl2.o_review_time_forecast,
                           tbl2.o_ss_forecast,
                           round(tbl2.forecast_std_dev_avg, 4) forecast_std_dev_avg,
                           tbl2.exp_unit_short,
                           to_number(substr(eus_low, instr(eus_low,'~',1,1)+1)) exp_units_short_low,
                           to_number((substr(eus_low, 1, instr(eus_low,'~',1,1)-1))) ss_std_dev_low,
                           to_number(substr(eus_high, instr(eus_high,'~',1,1)+1)) exp_units_short_high,
                           to_number((substr(eus_high, 1, instr(eus_high,'~',1,1)-1))) ss_std_dev_high
                    from (select tbl.i_item,
                                 tbl.i_locn,
                                 tbl.i_domain_id,
                                 tbl.thread_id,
                                 tbl.i_repl_method,
                                 tbl.i_service_level,
                                 tbl.i_service_level_type,
                                 tbl.o_review_time_forecast,
                                 tbl.o_ss_forecast,
                                 tbl.forecast_std_dev_avg,
                                 tbl.exp_unit_short,
                                 (select to_char(min(safety_stock_std_dev))||'~'|| to_char(max(exp_units_short))
                                    from safety_stock_lookup
                                   where exp_units_short <= tbl.exp_unit_short) eus_low,
                                 (select to_char(max(safety_stock_std_dev))||'~'|| to_char(min(exp_units_short))
                                    from safety_stock_lookup
                                   where exp_units_short >= tbl.exp_unit_short) eus_high
                            from ss_helper tbl) tbl2)
             --end of WITH clauses
             --get safety_stock
             select sdl.i_item,
                    sdl.i_locn,
                    sdl.i_domain_id,
                    sdl.thread_id,
                    sdl.forecast_std_dev_avg,
                   --safety stock
                    round(case
                             when sdl.i_repl_method = 'D' and
                                  sdl.i_service_level_type = LP_srvce_lvl_type_simple_sales then
                                sdl.o_ss_forecast * (NVL(sdl.i_service_level,0)/100)
                             when NVL(sdl.forecast_std_dev_avg,0) = 0 then
                                0
                             else
                                (case
                                    when exp_unit_short >= 4.5 then
                                       -4.5
                                    when exp_unit_short < 0.000 then
                                       4.5
                                    else
                                       round((  (  ( (exp_unit_short - exp_units_short_low)
                                                    /((case
                                                          when (exp_units_short_high - exp_units_short_low) = 0 then
                                                            exp_units_short_high + 1
                                                          else
                                                            exp_units_short_high
                                                          end) - exp_units_short_low))
                                                 * (ss_std_dev_high - ss_std_dev_low))
                                              + ss_std_dev_low), 4)
                                    end) * forecast_std_dev_avg
                             end, 4) o_safety_stock
               from std_dev_lookup sdl
              where thread_id = I_thread_id) use_this
      on (    target.i_item      = use_this.i_item
          and target.i_locn      = use_this.i_locn
          and target.i_domain_id = use_this.i_domain_id
          and target.thread_id   = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.o_safety_stock   = use_this.o_safety_stock,
                    target.o_review_std_dev = use_this.forecast_std_dev_avg;

      --merge_2, get order_point for main item only
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';

      merge into svc_repl_roq_gtt target
      using (with
                v_svc_repl_roq_gtt AS
                   (select i_item,
                           i_locn,
                           i_domain_id,
                           o_next_olt_forecast,
                           o_review_time_forecast,
                           o_safety_stock,
                           i_terminal_stock_qty,
                           i_pres_stock,
                           i_phase_end,
                           thread_id,
                           LP_vdate + NVL(i_next_order_lead_time,0) nolt_date,
                           LP_vdate + NVL(i_next_order_lead_time,0) + NVL(i_review_lead_time,0) review_lead_date
                     from svc_repl_roq_gtt
                    where (   (    i_locn_type = 'S'
                               and i_repl_method = 'D')
                           or i_repl_method = 'DI')
                      and thread_id = I_thread_id
                      and i_sub_item_master is NULL --this will fetch main item only
                      and i_lead_linked_wh is NULL)
             --
             --get order_point
             select vwrr.i_item,
                    vwrr.i_locn,
                    vwrr.i_domain_id,
                    vwrr.thread_id,
                    --order_point
                    case
                       when NVL(vwrr.i_phase_end, vwrr.nolt_date + 1) <= vwrr.nolt_date then
                          LEAST(vwrr.o_next_olt_forecast + vwrr.i_terminal_stock_qty, 99999999.9999)
                       when NVL(vwrr.i_phase_end, vwrr.review_lead_date + 1) <= vwrr.review_lead_date then
                          LEAST(vwrr.o_next_olt_forecast + vwrr.o_review_time_forecast
                                + vwrr.i_terminal_stock_qty, 99999999.9999)
                       else
                          LEAST(vwrr.o_next_olt_forecast + vwrr.o_review_time_forecast
                                + GREATEST(vwrr.o_safety_stock, vwrr.i_pres_stock), 99999999.9999)
                    end o_order_point
               from v_svc_repl_roq_gtt vwrr
              where thread_id = I_thread_id) use_this
      on (    target.i_item      = use_this.i_item
          and target.i_locn      = use_this.i_locn
          and target.i_domain_id = use_this.i_domain_id
          and target.thread_id   = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.o_order_point = NVL(use_this.o_order_point,0);

      L_program := substr(L_program,1,L_plength);

      -- Retrive the LOST SALES,
      -- this has the assumption that net inventory have been calculated prior to lost sales calculation.
      if NOT GET_NET_INVENTORY(O_error_message,
                               I_thread_id,
                               'Y') then
         return FALSE;
      end if;

      /* merge_3, calculate lost sales. */
      L_program := substr(L_program,1,L_plength)||'::MERGE_3';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item,
                    wrr.i_locn,
                    case
                       when wrr.O_curr_olt_forecast <= wrr.O_lost_sales then
                          0
                       else
                          (1 - (NVL(wrr.I_lost_sales_factor,0)/100)) * (wrr.O_curr_olt_forecast - wrr.O_lost_sales)
                       end O_lost_sales,
                    wrr.thread_id
               from svc_repl_roq_gtt wrr
              where wrr.thread_id = I_thread_id
                and (   (    wrr.i_locn_type = 'S'
                         and wrr.i_repl_method = 'D')
                     or wrr.i_repl_method = 'DI')
                and wrr.i_sub_item_master is NULL
                and wrr.i_lead_linked_wh is NULL) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_lost_sales = NVL(use_this.O_lost_sales, 0);


      /* merge_4, calculate order up to point, isd forecast, aso, eso, and order quantity*/
      L_program := substr(L_program,1,L_plength)||'::MERGE_4';

      merge into svc_repl_roq_gtt target
      using (with
                v_svc_repl_roq_gtt AS
                   (select i_item,
                           i_locn,
                           i_domain_id,
                           o_curr_olt_forecast,
                           o_review_time_forecast,
                           o_isd_forecast,
                           o_order_point,
                           o_safety_stock,
                           o_net_inventory,
                           o_lost_sales,
                           i_terminal_stock_qty,
                           i_pres_stock,
                           i_phase_end,
                           thread_id,
                           LP_vdate + NVL(i_next_order_lead_time,0) + NVL(i_review_lead_time,0) review_lead_date,
                           LP_vdate + NVL(i_curr_order_lead_time,0) + NVL(i_inv_selling_days,0) isd_date
                     from svc_repl_roq_gtt
                    where (   (    i_locn_type = 'S'
                               and i_repl_method = 'D')
                           or i_repl_method = 'DI')
                      and thread_id = I_thread_id
                      and i_sub_item_master is NULL --main item only
                      and i_lead_linked_wh is NULL)
             --
             select wrr.i_item,
                    wrr.i_locn,
                    wrr.i_domain_id,
                    wrr.thread_id,
                    ---------------------
                    --order up to point--
                    ---------------------
                    case
                       when (wrr.review_lead_date >= wrr.isd_date) then
                          NVL(wrr.O_order_point,0)
                       else
                          case
                             when NVL(wrr.i_phase_end, wrr.isd_date + 1) <= wrr.isd_date then
                                case
                                   when NVL(wrr.i_phase_end, wrr.review_lead_date + 1) <= wrr.review_lead_date then
                                      NVL(wrr.O_order_point,0)
                                   else
                                      LEAST(wrr.o_curr_olt_forecast + wrr.o_isd_forecast
                                            + wrr.i_terminal_stock_qty, 99999999.9999)
                                   end
                             else
                                LEAST(wrr.o_curr_olt_forecast + wrr.o_isd_forecast
                                      + GREATEST(wrr.o_safety_stock, wrr.i_pres_stock), 99999999.9999)
                             end
                       end order_up_to_point,
                    ----------------
                    --isd_forecast--
                    ----------------
                    case
                       when (wrr.review_lead_date >= wrr.isd_date) then
                          wrr.o_review_time_forecast
                       else
                          case
                             when NVL(wrr.i_phase_end, wrr.isd_date + 1) <= wrr.isd_date then
                                case
                                   when NVL(wrr.i_phase_end, wrr.review_lead_date + 1) <= wrr.review_lead_date then
                                      wrr.o_review_time_forecast
                                   else
                                      wrr.o_isd_forecast
                                   end
                             else
                                wrr.o_isd_forecast
                             end
                       end o_isd_forecast,
                    -----------
                    --due_ind--
                    -----------
                    case
                       when (wrr.review_lead_date >= wrr.isd_date) then
                          case
                             when NVL(wrr.i_phase_end, wrr.review_lead_date + 1) <= wrr.review_lead_date then
                                case
                                   when NVL(wrr.O_order_point,0) <= wrr.O_net_inventory then
                                      'N'
                                   else
                                      'Y'
                                   end
                             end
                       else
                          case
                             when NVL(wrr.i_phase_end, wrr.isd_date + 1) <= wrr.isd_date then
                                case
                                   when NVL(wrr.i_phase_end, wrr.review_lead_date + 1) <= wrr.review_lead_date then
                                      case
                                         when NVL(wrr.O_order_point,0) <= wrr.O_net_inventory then
                                            'N'
                                         else
                                            'Y'
                                      end
                                   end
                             end
                       end o_due_ind,
                    -------
                    --aso--
                    -------
                    case
                       when (wrr.review_lead_date >= wrr.isd_date) then
                          case
                             when NVL(wrr.i_phase_end, wrr.review_lead_date + 1) <= wrr.review_lead_date then
                                0
                             end
                       else
                          case
                             when NVL(wrr.i_phase_end, wrr.isd_date + 1) <= wrr.isd_date then
                                case
                                   when NVL(wrr.i_phase_end, wrr.review_lead_date + 1) <= wrr.review_lead_date then
                                      0
                                   end
                             end
                       end o_aso,
                    -------
                    --eso--
                    -------
                    case
                       when (wrr.review_lead_date >= wrr.isd_date) then
                          case
                             when NVL(wrr.i_phase_end, wrr.review_lead_date + 1) <= wrr.review_lead_date then
                                0
                          end
                       else
                          case
                             when NVL(wrr.i_phase_end, wrr.isd_date + 1) <= wrr.isd_date then
                                case
                                   when NVL(wrr.i_phase_end, wrr.review_lead_date + 1) <= wrr.review_lead_date then
                                      0
                                   end
                             end
                       end o_eso,
                    -------------
                    --order_qty--
                    -------------
                    case
                       when (wrr.review_lead_date >= wrr.isd_date) then
                          case
                             when NVL(wrr.i_phase_end, wrr.review_lead_date + 1) <= wrr.review_lead_date then
                                NVL(wrr.o_order_point,0) - wrr.o_net_inventory - wrr.o_lost_sales
                             end
                       else
                          case
                             when NVL(wrr.i_phase_end, wrr.isd_date + 1) <= wrr.isd_date then
                                case
                                   when NVL(wrr.i_phase_end, wrr.review_lead_date + 1) <= wrr.review_lead_date then
                                      NVL(wrr.o_order_point,0) - wrr.o_net_inventory - wrr.o_lost_sales
                                   end
                             end
                       end o_order_qty
              from v_svc_repl_roq_gtt wrr
             where wrr.thread_id = I_thread_id) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_order_up_to_point = NVL(use_this.order_up_to_point, 0),
                    target.O_isd_forecast      = use_this.O_isd_forecast,
                    target.O_due_ind           = use_this.O_due_ind,
                    target.O_aso               = use_this.O_aso,
                    target.O_eso               = use_this.O_eso,
                    target.O_order_qty         = NVL(use_this.O_order_qty,0);

      /* merge_5, calculate eso and aso*/
      L_program := substr(L_program,1,L_plength)||'::MERGE_5';

      merge into svc_repl_roq_gtt target
      using (with
                v_svc_repl_roq_gtt AS
                   (select wrr.i_item,
                           i_locn,
                           i_due_ord_serv_basis,
                           i_domain_id,
                           o_curr_olt_forecast,
                           o_review_time_forecast,
                           o_isd_forecast,
                           o_order_point,
                           o_safety_stock,
                           o_net_inventory,
                           o_lost_sales,
                           o_review_std_dev,
                           o_next_olt_forecast,
                           ((o_net_inventory - (o_next_olt_forecast + o_review_time_forecast))/ o_review_std_dev) L_K,
                           (select to_char(max(exp_units_short))||'~'||to_char(min(safety_stock_std_dev))
                              from safety_stock_lookup
                             where safety_stock_std_dev >= ((o_net_inventory - (o_next_olt_forecast + o_review_time_forecast))/ o_review_std_dev)) LK_high,
                           (select to_char(min(exp_units_short))||'~'||to_char(max(safety_stock_std_dev))
                              from safety_stock_lookup
                             where safety_stock_std_dev <= ((o_net_inventory - (o_next_olt_forecast + o_review_time_forecast))/ o_review_std_dev)) LK_low,
                           i_terminal_stock_qty,
                           i_pres_stock,
                           i_phase_end,
                           i_due_ord_process_ind,
                           i_service_level,
                           i_unit_cost,
                           i_unit_retail,
                           im.uom_conv_factor,
                           thread_id,
                           LP_vdate + NVL(i_curr_order_lead_time,0) colt_date,
                           LP_vdate + NVL(i_next_order_lead_time,0) + NVL(i_review_lead_time,0) review_lead_date,
                           LP_vdate + NVL(i_curr_order_lead_time,0) + NVL(i_inv_selling_days,0) isd_date
                     from svc_repl_roq_gtt wrr,
                          item_master im
                    where wrr.i_item = im.item
                      and (   (    wrr.i_locn_type = 'S'
                               and wrr.i_repl_method = 'D')
                           or wrr.i_repl_method = 'DI')
                      and thread_id = I_thread_id
                      and wrr.i_sub_item_master is NULL --main item only
                      and wrr.i_lead_linked_wh is NULL),
                v_svc_repl_roq_gtt2 AS
                   (select i_item,
                           i_locn,
                           i_due_ord_serv_basis,
                           i_domain_id,
                           o_curr_olt_forecast,
                           o_review_time_forecast,
                           o_isd_forecast,
                           o_order_point,
                           o_safety_stock,
                           o_net_inventory,
                           o_lost_sales,
                           o_review_std_dev,
                           o_next_olt_forecast,
                           L_K,
                           to_number((substr(LK_high, 1, instr(LK_high,'~',1,1)-1))) LK_units_short_high,
                           to_number(substr(LK_high, instr(LK_high,'~',1,1)+1)) LK_std_dev_high,
                           to_number((substr(LK_low, 1, instr(LK_low,'~',1,1)-1))) LK_units_short_low,
                           to_number(substr(LK_low, instr(LK_low,'~',1,1)+1)) LK_std_dev_low,
                           i_terminal_stock_qty,
                           i_pres_stock,
                           i_phase_end,
                           i_due_ord_process_ind,
                           i_service_level,
                           i_unit_cost,
                           i_unit_retail,
                           uom_conv_factor,
                           thread_id,
                           colt_date,
                           review_lead_date,
                           isd_date
                     from v_svc_repl_roq_gtt),
                v_svc_repl_roq_gtt3 AS
                   (select i_item,
                           i_locn,
                           i_due_ord_serv_basis,
                           i_domain_id,
                           o_curr_olt_forecast,
                           o_review_time_forecast,
                           o_isd_forecast,
                           o_order_point,
                           o_safety_stock,
                           o_net_inventory,
                           o_lost_sales,
                           o_review_std_dev,
                           case
                              when NVL(I_due_ord_process_ind,'N') = 'Y' then
                                 (O_next_olt_forecast + O_review_time_forecast) * (1-(I_service_level/100))
                              else
                                 0
                              end aso,
                           case
                              when NVL(I_due_ord_process_ind,'N') = 'Y' then
                                 case
                                    when O_review_std_dev = 0 then
                                       0
                                    else
                                       (case
                                           when L_K <= -10 then
                                              10
                                           when L_K >= 4.5 then
                                               0
                                           else
                                               (  (  (  (L_K - LK_std_dev_low)
                                                      / ((case when (LK_std_dev_high - LK_std_dev_low) = 0 then
                                                                 LK_std_dev_high + 1
                                                               else
                                                                 LK_std_dev_high
                                                               end) - LK_std_dev_low))
                                                   * (LK_units_short_high - LK_units_short_low))
                                                + LK_units_short_low)
                                           end) * o_review_std_dev
                                    end
                              else
                                 0
                              end eso,
                           i_terminal_stock_qty,
                           i_pres_stock,
                           i_phase_end,
                           i_due_ord_process_ind,
                           i_unit_cost,
                           i_unit_retail,
                           uom_conv_factor,
                           thread_id,
                           colt_date,
                           review_lead_date,
                           isd_date
                      from v_svc_repl_roq_gtt2)
                   -- end of WITH clause
             select i_item,
                    i_locn,
                    i_domain_id,
                    thread_id,
                    -------
                    --aso--
                    -------
                    case
                       when NVL(I_due_ord_process_ind,'N') = 'Y' then
                          case
                             when i_due_ord_serv_basis = 'C' then
                                aso * i_unit_cost
                             when i_due_ord_serv_basis = 'P' then
                                aso * (i_unit_retail - i_unit_cost)
                             when i_due_ord_serv_basis = 'U' then
                                aso * NVL(uom_conv_factor,1)
                             end
                       else
                          0
                       end o_aso,
                    -------
                    --eso--
                    -------
                    case
                       when NVL(I_due_ord_process_ind,'N') = 'Y' then
                          case
                             when i_due_ord_serv_basis = 'C' then
                                eso * i_unit_cost
                             when i_due_ord_serv_basis = 'P' then
                                eso * (i_unit_retail - i_unit_cost)
                             when i_due_ord_serv_basis = 'U' then
                                eso * NVL(uom_conv_factor,1)
                             end
                       else
                          0
                       end o_eso
               from v_svc_repl_roq_gtt3) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.o_aso = NVL(use_this.o_aso,0),
                    target.o_eso = NVL(use_this.o_eso,0);


      /*merge_6, setup due ind */
      L_program := substr(L_program,1,L_plength)||'::MERGE_6';

      merge into svc_repl_roq_gtt target
      using (select i_item,
                    i_locn,
                    i_domain_id,
                    thread_id,
                    -----------
                    --due_ind--
                    -----------
                    case
                       when NVL(I_due_ord_process_ind,'N') = 'Y' then
                          case
                             when o_eso <= o_aso then
                                'N'
                             else
                                'Y'
                             end
                       else
                          case
                             when NVL(o_order_point,0) <= o_net_inventory then
                                'N'
                             else
                                'Y'
                          end
                       end o_due_ind
               from svc_repl_roq_gtt wrr
              where (   (    i_locn_type = 'S'
                         and i_repl_method = 'D')
                     or i_repl_method = 'DI')
                and thread_id = I_thread_id
                and I_sub_item_master is NULL --main item only
                and i_lead_linked_wh is NULL) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.o_due_ind = use_this.o_due_ind;

      /*merge_7 set final roq */
      L_program := substr(L_program,1,L_plength)||'::MERGE_7';

      merge into svc_repl_roq_gtt target
      using (select i_item,
                    i_locn,
                    i_domain_id,
                    thread_id,
                    -------
                    --roq--
                    -------
                    (GREATEST(NVL(o_order_point,0), NVL(o_order_up_to_point,0)) - NVL(o_net_inventory,0) - NVL(o_lost_sales,0)) o_order_qty
               from svc_repl_roq_gtt wrr
              where (   (    i_locn_type = 'S'
                         and i_repl_method = 'D')
                     or i_repl_method = 'DI')
                and thread_id = I_thread_id
                and I_sub_item_master is NULL --main item only
                and i_lead_linked_wh is NULL) use_this
      on (    target.i_item      = use_this.i_item
          and target.i_locn      = use_this.i_locn
          and target.thread_id   = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.o_order_qty = NVL(use_this.o_order_qty,0);
   end if;

   return TRUE;

EXCEPTION
when OTHERS then
   O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
   return FALSE;
END GET_LOCN_NEED_DYNAMIC;
----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_STORE_ORDERS(O_error_message   IN OUT   VARCHAR2,
                          I_thread_id       IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN IS
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   L_program   VARCHAR2(200) := 'CORESVC_REPL_ROQ_SQL.GET_STORE_ORDERS';
   L_plength   NUMBER(2)     := LENGTH('CORESVC_REPL_ROQ_SQL.GET_STORE_ORDERS');

   cursor C_LOCK_STORE_ORDERS is
      select 'x'
        from svc_repl_roq_gtt rrw,
             store_orders so
       where rrw.thread_id = I_thread_id
         and rrw.i_sub_item_master is NULL
         and rrw.i_locn_type = 'S'
         and rrw.i_so_due_ind = 'Y'
         and rrw.i_so_wf_need_date is NULL
         and so.item = rrw.i_item
         and so.store = rrw.i_locn
         and so.need_date BETWEEN rrw.i_so_from_date and rrw.i_so_to_date
         and so.processed_date is NULL
         and so.need_qty > 0
         and (   (    rrw.i_last_run_of_the_day = 'N'
                  and so.delivery_slot_id is NOT NULL)
              or rrw.i_last_run_of_the_day = 'Y')
         for update nowait;

   cursor C_WF_LOCK_STORE_ORDERS is
      select 'x'
        from svc_repl_roq_gtt rrw,
             store_orders so
       where rrw.thread_id = I_thread_id
         and rrw.i_sub_item_master is NULL
         and rrw.i_locn_type = 'S'
         and rrw.i_so_due_ind = 'Y'
         and rrw.i_so_wf_need_date is NOT NULL
         and so.item = rrw.i_item
         and so.store = rrw.i_locn
         and so.need_date = rrw.i_so_wf_need_date
         and so.processed_date is NULL
         and so.need_qty > 0
         for update nowait;
BEGIN
   if I_thread_id is NOT NULL then --do bulk processing
    /* merge_1: get total store order need for all stores at source wh*/
      L_program := substr(L_program,1,L_plength)||'::MERGE_1';

      merge into svc_repl_roq_gtt target
      using (select wrr.i_item item_id,
                    wrr.i_locn loc_id,
                    NVL(SUM(so.need_qty),0) so_roq,
                    case
                       when NVL(SUM(so.need_qty),0) = 0 then
                          'N'
                       else
                          'Y'
                       end so_due_ind,
                    wrr.thread_id
               from store_orders so,
                    repl_item_loc ril,
                    svc_repl_roq_gtt wrr
              where wrr.i_repl_method = 'SO'
                and wrr.i_locn_type = 'W'
                and wrr.i_sub_item_master is NULL
                and wrr.i_lead_linked_wh is NULL
                and wrr.thread_id = I_thread_id
                and so.item = wrr.i_item
                and so.item = ril.item
                and so.store = ril.location
                and ril.source_wh = wrr.i_locn
                and so.processed_date is NULL
                and so.need_qty > 0
                and (   (    ril.reject_store_ord_ind = 'Y'
                         and so.need_date BETWEEN (ril.wh_lead_time + wrr.i_so_from_date) and (ril.wh_lead_time + wrr.i_so_to_date))
                     or (    ril.reject_store_ord_ind = 'N'
                         and so.need_date BETWEEN (to_date('19010101','YYYYMMDD')) and (ril.wh_lead_time + wrr.i_so_to_date)))
              group by wrr.i_item,
                       wrr.i_locn,
                       wrr.thread_id) use_this
      on (    target.I_item    = use_this.item_id
          and target.I_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.I_so_due_ind   = NVL(use_this.so_due_ind, 'N'),
                    target.I_so_order_roq = NVL(use_this.so_roq, 0);

      /* merge_2: Franchise store records at last_run_of_day, get total need_qty */
      /*          for a specific item/store/need_date */
      L_program := substr(L_program,1,L_plength)||'::MERGE_2';

      merge into svc_repl_roq_gtt target
      using (select DISTINCT rrw.i_item item_id,
                    rrw.i_locn loc_id,
                    NVL(SUM(so.need_qty),0) so_roq,
                    case
                       when NVL(SUM(so.need_qty),0) = 0 then
                          'N'
                       else
                          'Y'
                       end so_due_ind,
                    wod.need_date wf_need_dt,
                    rrw.thread_id
               from wf_order_head wfh,
                    wf_order_detail wod,
                    store_orders so,
                    svc_repl_roq_gtt rrw
              where rrw.i_store_type = 'F'
                and rrw.i_last_run_of_the_day = 'Y'
                and rrw.i_locn_type = 'S'
                and rrw.i_sub_item_master is NULL
                and rrw.i_lead_linked_wh is NULL
                and rrw.thread_id = I_thread_id
                and so.item = rrw.i_item
                and so.store = rrw.i_locn
                and so.processed_date is NULL
                and so.need_qty > 0
                and wod.item = so.item
                and wod.need_date = so.need_date
                and wfh.wf_order_no = wod.wf_order_no
                and so.wf_order_no = wod.wf_order_no
                and so.wf_order_line_no = wod.wf_order_line_no
                and wfh.status in ('A','P')
                and rrw.i_so_from_date > wod.need_date
                and wod.not_after_date >= rrw.i_so_from_date
                and wod.not_after_date <= rrw.i_so_to_date
              group by rrw.i_item,
                       rrw.i_locn,
                       wod.need_date,
                       rrw.thread_id ) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.I_so_due_ind   = NVL(use_this.so_due_ind, 'N'),
                    target.I_so_order_roq = NVL(use_this.so_roq, 0),
                    target.I_so_wf_need_date = use_this.wf_need_dt;

      -- check for locks
      L_program := substr(L_program,1,L_plength);

      open C_WF_LOCK_STORE_ORDERS;
      close C_WF_LOCK_STORE_ORDERS;

      /*merge_3: Franchise store update store_orders.processed_date if need_date(i_so_wf_need_date) is present*/
      L_program := substr(L_program,1,L_plength)||'::MERGE_3';

      merge into store_orders target
      using (select rrw.i_item,
                    rrw.i_locn,
                    rrw.I_so_wf_need_date
               from svc_repl_roq_gtt rrw
              where rrw.thread_id = I_thread_id
                and rrw.i_store_type = 'F'
                and rrw.i_last_run_of_the_day = 'Y'
                and rrw.i_sub_item_master is NULL
                and rrw.i_lead_linked_wh is NULL
                and rrw.i_locn_type = 'S'
                and rrw.i_so_due_ind = 'Y'
                and rrw.i_so_wf_need_date is NOT NULL ) use_this
      on (    target.item = use_this.i_item
          and target.store = use_this.i_locn
          and target.need_date = use_this.i_so_wf_need_date
          and target.need_qty > 0)
      when matched then
         update set target.processed_date = LP_vdate;

      /*merge_4: Company store and Franchise store records. Get total need_qty if need_date(i_so_wf_need_date) is NULL*/
      L_program := substr(L_program,1,L_plength)||'::MERGE_4';

      merge into svc_repl_roq_gtt target
      using (select rrw.i_item item_id,
                    rrw.i_locn loc_id,
                    NVL(SUM(so.need_qty),0) so_roq,
                    case
                       when NVL(SUM(so.need_qty),0) = 0 then
                          'N'
                       else
                          'Y'
                       end so_due_ind,
                    rrw.thread_id
               from store_orders so,
                    svc_repl_roq_gtt rrw
              where rrw.i_locn_type = 'S'
                and rrw.i_sub_item_master is NULL
                and rrw.i_lead_linked_wh is NULL
                and rrw.thread_id = I_thread_id
                and rrw.I_so_order_roq is NULL
                and rrw.i_so_wf_need_date is NULL -- this is to prevent franchise stores that where processed in merge_2
                and so.item = rrw.i_item
                and so.store = rrw.i_locn
                and so.processed_date is NULL
                and so.need_qty > 0
                and so.need_date BETWEEN rrw.i_so_from_date and rrw.i_so_to_date
                and (   (    rrw.i_last_run_of_the_day = 'N'
                         and so.delivery_slot_id is NOT NULL)
                     or rrw.i_last_run_of_the_day = 'Y')
              group by rrw.i_item,
                       rrw.i_locn,
                       rrw.thread_id) use_this
      on (    target.i_item    = use_this.item_id
          and target.i_locn    = use_this.loc_id
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.I_so_due_ind   = NVL(use_this.so_due_ind, NVL(target.I_so_due_ind, 'N') ),
                    target.I_so_order_roq = NVL(target.I_so_order_roq, 0) + NVL(use_this.so_roq, 0);

      -- check for locks
      L_program := substr(L_program,1,L_plength);

      open C_LOCK_STORE_ORDERS;
      close C_LOCK_STORE_ORDERS;

      /*merge_5: update store_orders.processed_date for company stores and franchise store where need_date is NULL. */
      L_program := substr(L_program,1,L_plength)||'::MERGE_5';

      merge into store_orders target
      using (select so.item,
                    so.store,
                    so.need_date,
                    so.delivery_slot_id
               from svc_repl_roq_gtt rrw,
                    store_orders so
              where rrw.thread_id = I_thread_id
                and rrw.i_sub_item_master is NULL
                and rrw.i_lead_linked_wh is NULL
                and rrw.i_locn_type = 'S'
                and rrw.i_so_due_ind = 'Y'
                and rrw.i_so_wf_need_date is NULL
                and so.item = rrw.i_item
                and so.store = rrw.i_locn
                and so.need_date BETWEEN rrw.i_so_from_date and rrw.i_so_to_date
                and so.processed_date is NULL
                and so.need_qty > 0) use_this
      on (    target.item = use_this.item
          and target.store = use_this.store
          and target.need_date = use_this.need_date
          and NVL(target.delivery_slot_id,'-999') = NVL(use_this.delivery_slot_id,'-999')
          and target.need_qty > 0)
      when matched then
         update set target.processed_date = LP_vdate;

      /* update_1 update for ROQ having negative values */
      L_program := substr(L_program,1,L_plength)||'::UPDATE_1';

      update svc_repl_roq_gtt
         set O_order_qty = 0
       where i_repl_method != 'SO'
         and O_order_qty < 0
         and thread_id = I_thread_id;

      /* merge_6: For Store/Wh loc. Update for ROQ combining Store Orders total need_qty to the previously computed ROQ from other functions*/
      L_program := substr(L_program,1,L_plength)||'::MERGE_6';

      merge into svc_repl_roq_gtt target
      using (select wrr.I_item,
                    wrr.I_locn,
                    wrr.thread_id,
                    --
                    case
                       when wrr.i_repl_method <> 'SO' then
                          case
                             when wrr.o_due_ind = 'Y' and
                                  NVL(wrr.i_so_due_ind, 'N') = 'Y' then
                                NVL(wrr.o_order_qty,0) + NVL(wrr.i_so_order_roq, 0)
                             when wrr.o_due_ind = 'N' and
                                  NVL(wrr.i_so_due_ind, 'N') = 'Y' then
                                NVL(wrr.i_so_order_roq, 0)
                             else
                                NVL(wrr.o_order_qty,0)
                             end
                       else
                          NVL(wrr.i_so_order_roq, 0)
                       end order_qty,
                    --
                    case
                       when wrr.i_repl_method <> 'SO' then
                          case
                             when NVL(wrr.i_so_order_roq, 0) > 0 then
                                NVL(wrr.i_so_due_ind, 'N')
                             else
                                wrr.o_due_ind
                             end
                       else
                          NVL(wrr.i_so_due_ind, 'N')
                       end due_ind
               from svc_repl_roq_gtt wrr
              where wrr.i_sub_item_master is NULL
                and wrr.i_lead_linked_wh is NULL
                and wrr.thread_id = I_thread_id) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_order_qty = NVL(use_this.order_qty,0),
                    target.O_due_ind   = use_this.due_ind;

      --merge_7: If due order processing is off, the due indicator will be set to No if no order is needed for the location.
      --         Therefore, we set the roq to zero and pass it out of the function.
      L_program := substr(L_program,1,L_plength)||'::MERGE_7';

      merge into svc_repl_roq_gtt target
      using (select wrr.I_item,
                    wrr.I_locn,
                    wrr.thread_id,
                    --
                    case
                       when NVL(wrr.i_due_ord_process_ind,'N') = 'N' and
                            wrr.o_due_ind = 'N' then
                          0
                       else
                          NVL(wrr.o_order_qty,0)
                       end order_qty
               from svc_repl_roq_gtt wrr
              where wrr.i_sub_item_master is NULL
                and wrr.i_lead_linked_wh is NULL
                and wrr.thread_id = I_thread_id ) use_this
      on (    target.i_item    = use_this.i_item
          and target.i_locn    = use_this.i_locn
          and target.thread_id = use_this.thread_id
          and target.i_sub_item_master is NULL
          and target.i_lead_linked_wh is NULL)
      when matched then
         update set target.O_order_qty = use_this.order_qty;

   end if;
   ---
   return TRUE;

EXCEPTION
when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'STORE_ORDERS',
                                             NULL,
                                             NULL);
      return FALSE;

when OTHERS then
   O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END GET_STORE_ORDERS;
----------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_FORECAST_FOR_PERIOD_DYN (O_error_message  IN OUT   VARCHAR2,
                                      I_thread_id      IN       NUMBER DEFAULT NULL)
RETURN BOOLEAN IS
BEGIN

   --
   -- this will get the COLT, NOLT, REVIEW, and ISD total forecast for an item and its sub-items
   --
   MERGE INTO svc_repl_roq_gtt target
   USING (WITH
             v_comp_date AS
                (select i_item,
                        i_locn,
                        i_domain_id,
                        thread_id,
                        vdate,
                        curr_454_day,
                        i_sub_item_master,
                        i_lead_linked_wh,
                        i_last_delivery_date,
                        i_next_delivery_date,
                        LP_vdate init_date,
                        LP_vdate + NVL(i_curr_order_lead_time,0) colt_date,
                        LP_vdate + NVL(i_next_order_lead_time,0) nolt_date,
                        LP_vdate + NVL(i_next_order_lead_time,0) + NVL(i_review_lead_time,0) review_lead_date,
                        LP_vdate + NVL(i_curr_order_lead_time,0) + NVL(i_inv_selling_days,0) isd_date
                  from svc_repl_roq_gtt,
                       period
                 where (    (    i_locn_type = 'S'
                             and i_repl_method = 'D')
                         or i_repl_method = 'DI')
                   and thread_id = I_thread_id
                   and i_sub_forecast_ind = 'Y'), --this will include sub_items
             --
             v_svc_repl_roq_gtt AS
                (select wrr.i_item,
                        wrr.i_locn,
                        wrr.i_domain_id,
                        wrr.thread_id,
                        cdate.vdate,
                        cdate.curr_454_day,
                        cdate.i_sub_item_master,
                        cdate.i_lead_linked_wh,
                        --set dates for colt forecast
                        cdate.init_date first_date_dyn_colt,
                        cdate.colt_date last_date_dyn_colt,
                        --
                        --set dates for nolt forecast
                        cdate.init_date first_date_dyn_nolt,
                        case
                           when wrr.i_curr_order_lead_time = wrr.i_next_order_lead_time then
                              cdate.colt_date
                           else
                              case
                                 when NVL(wrr.i_phase_end, cdate.nolt_date + 1) <= cdate.nolt_date then
                                    wrr.i_phase_end
                                 else
                                    cdate.nolt_date
                                 end
                           end last_date_dyn_nolt,
                        --
                        --review date forecast
                        case
                           --phase_end <= nolt
                           when NVL(wrr.i_phase_end, cdate.nolt_date + 1) <= cdate.nolt_date then
                              NULL --this will make the forecast be set to 0
                           --phase_end <= review_lead_time
                           when NVL(wrr.i_phase_end, cdate.review_lead_date + 1) <= cdate.review_lead_date then
                              cdate.nolt_date
                           else
                              cdate.nolt_date
                           end first_date_dyn_review,
                        case
                           --phase_end <= nolt
                           when NVL(wrr.i_phase_end, cdate.nolt_date + 1) <= cdate.nolt_date then
                              NULL --this will make the forecast be set to 0
                           --phase_end <= review_lead_time
                           when NVL(wrr.i_phase_end, cdate.review_lead_date + 1) <= cdate.review_lead_date then
                              wrr.i_phase_end
                           else
                              cdate.review_lead_date
                           end last_date_dyn_review,
                        --
                        --inventory selling days forecast
                        cdate.colt_date first_date_dyn_isd,
                        case
                           when cdate.review_lead_date < cdate.isd_date then
                              case
                                 when NVL(wrr.i_phase_end, cdate.isd_date + 1) <= cdate.isd_date then
                                    case
                                       when NVL(wrr.i_phase_end, cdate.review_lead_date + 1) > cdate.review_lead_date then
                                          wrr.i_phase_end
                                    end
                              else
                                 cdate.isd_date
                              end
                           end last_date_dyn_isd,
                        --
                        --safety stock forecast
                        case
                           when wrr.i_service_level_type = LP_srvce_lvl_type_simple_sales and
                                NVL(wrr.i_phase_end, cdate.review_lead_date + 1) > cdate.review_lead_date and
                                wrr.i_sub_item_master is NULL and
                                wrr.i_lead_linked_wh is NULL then --this will be for the main item only
                              wrr.i_last_delivery_date
                           end first_date_dyn_ss,
                        case
                           when wrr.i_service_level_type = LP_srvce_lvl_type_simple_sales and
                                NVL(wrr.i_phase_end, cdate.review_lead_date + 1) > cdate.review_lead_date and
                                wrr.i_sub_item_master is NULL and
                                wrr.i_lead_linked_wh is NULL then --this will be for the main item only
                              wrr.i_next_delivery_date
                           end last_date_dyn_ss
                   from svc_repl_roq_gtt wrr,
                        v_comp_date cdate
                  where (    (    wrr.i_locn_type = 'S'
                              and wrr.i_repl_method = 'D')
                          or wrr.i_repl_method = 'DI')
                    and wrr.thread_id = I_thread_id
                    and wrr.i_item = cdate.i_item
                    and wrr.i_locn = cdate.i_locn
                    and wrr.i_domain_id = cdate.i_domain_id
                    and NVL(wrr.i_sub_item_master, '-1') = NVL(cdate.i_sub_item_master, '-1')
                    and NVL(wrr.i_lead_linked_wh, 0) = NVL(cdate.i_lead_linked_wh, 0)
                    and wrr.thread_id = cdate.thread_id),
             --
             v_svc_repl_roq_gtt_2 AS
                (select i_item,
                        i_locn,
                        i_domain_id,
                        thread_id,
                        vdate,
                        curr_454_day,
                        i_sub_item_master,
                        i_lead_linked_wh,
                        forecast_type,
                        first_date,
                        last_date
                   from v_svc_repl_roq_gtt
                 model
                    return updated rows
                    partition by (i_item,
                                  i_locn,
                                  i_domain_id,
                                  thread_id,
                                  vdate,
                                  curr_454_day,
                                  i_sub_item_master,
                                  i_lead_linked_wh)
                    dimension by (0 as i)
                    measures     (cast(NULL as varchar2(12)) as forecast_type,
                                  cast(NULL as date) as first_date,
                                  cast(NULL as date) as last_date,
                                  first_date_dyn_colt,
                                  last_date_dyn_colt,
                                  first_date_dyn_nolt,
                                  last_date_dyn_nolt,
                                  first_date_dyn_review,
                                  last_date_dyn_review,
                                  first_date_dyn_isd,
                                  last_date_dyn_isd,
                                  first_date_dyn_ss,
                                  last_date_dyn_ss)
                    rules upsert all
                       (forecast_type[1] = 'CLT_FORECAST',
                        forecast_type[2] = 'NLT_FORECAST',
                        forecast_type[3] = 'REV_FORECAST',
                        forecast_type[4] = 'ISD_FORECAST',
                        forecast_type[5] = 'SS_FORECAST',
                        --
                        first_date   [1] = first_date_dyn_colt[0],
                        first_date   [2] = first_date_dyn_nolt[0],
                        first_date   [3] = first_date_dyn_review[0],
                        first_date   [4] = first_date_dyn_isd[0],
                        first_date   [5] = first_date_dyn_ss[0],
                        --
                        last_date    [1] = last_date_dyn_colt[0],
                        last_date    [2] = last_date_dyn_nolt[0],
                        last_date    [3] = last_date_dyn_review[0],
                        last_date    [4] = last_date_dyn_isd[0],
                        last_date    [5] = last_date_dyn_ss[0])),
             --
             v_svc_repl_roq_gtt_3 AS
                (select i_item,
                        i_locn,
                        i_domain_id,
                        thread_id,
                        vdate,
                        curr_454_day,
                        i_sub_item_master,
                        i_lead_linked_wh,
                        forecast_type,
                        first_date,
                        last_date
                  from v_svc_repl_roq_gtt_2
                 where first_date is NOT NULL),
             --
             dif1 AS
                (select wrr.i_item,
                        wrr.i_locn,
                        wrr.thread_id,
                        wrr.i_domain_id,
                        wrr.vdate,
                        wrr.curr_454_day,
                        wrr.i_sub_item_master,
                        wrr.i_lead_linked_wh,
                        wrr.forecast_type,
                        SUM(NVL(dif.forecast_sales,0)) forecast_sales,
                        CASE
                           WHEN MAX(dif.data_date) IS NULL then
                              MIN(wrr.first_date)
                           ELSE
                              MAX(dif.data_date)
                           END max_daily_date,
                        MIN(wrr.first_date) first_date,
                        MAX(wrr.last_date) last_date
                   from daily_item_forecast dif,
                        v_svc_repl_roq_gtt_3 wrr
                  where NVL(wrr.i_sub_item_master, wrr.i_item) = dif.item(+)
                    and NVL(wrr.i_lead_linked_wh, wrr.i_locn)  = dif.loc(+)
                    and wrr.i_domain_id = dif.domain_id(+)
                    and wrr.first_date  < dif.data_date(+)
                    and wrr.last_date   >= dif.data_date(+)
                  GROUP BY wrr.i_item,
                           wrr.i_locn,
                           wrr.thread_id,
                           wrr.i_domain_id,
                           wrr.vdate,
                           wrr.curr_454_day,
                           wrr.i_sub_item_master,
                           wrr.i_lead_linked_wh,
                           wrr.forecast_type),
             --
             dif AS
                (select dif1.i_item,
                        dif1.i_locn,
                        dif1.thread_id,
                        dif1.i_domain_id,
                        dif1.forecast_type,
                        dif1.vdate,
                        dif1.curr_454_day,
                        dif1.i_sub_item_master,
                        dif1.i_lead_linked_wh,
                        dif1.forecast_sales,
                        dif1.max_daily_date,
                        dif1.first_date,
                        dif1.last_date,
                        case
                           when MOD(ABS((vdate + (7-curr_454_day)) - last_date),7) = 0 then
                              dif1.last_date
                           else
                              dif1.last_date + (7 - MOD(ABS((vdate + (7-curr_454_day)) - last_date),7))
                           end eow_date
                   from dif1),
             --
             qry_get_forecast AS
                (select /*+ INDEX(ifc pk_item_forecast)*/
                        dif.i_item,
                        dif.i_locn,
                        dif.thread_id,
                        dif.i_domain_id,
                        dif.forecast_type,
                        case
                           when ROW_NUMBER() OVER (PARTITION BY ifc.item,
                                                                ifc.loc,
                                                                dif.forecast_type
                                                          ORDER BY ifc.eow_date) = 1 then
                              (ifc.forecast_sales / 7) * to_number((least(ifc.eow_date,dif.last_date)) - (greatest(dif.max_daily_date,ifc.eow_date - 7)))
                           else
                              (ifc.forecast_sales / 7) * to_number((7 - (greatest((ifc.eow_date - (dif.last_date)),0))))
                           end item_forecast_sales
                   from dif,
                        item_forecast ifc
                  where ifc.item      = NVL(dif.i_sub_item_master, dif.i_item)
                    and ifc.loc       = NVL(dif.i_lead_linked_wh, dif.i_locn)
                    and ifc.domain_id = dif.i_domain_id
                    and ifc.eow_date between dif.max_daily_date and dif.eow_date)
             -- end of WITH clause
          select tbl.i_item,
                 tbl.i_locn,
                 tbl.i_domain_id,
                 thread_id,
                 SUM(tbl.colt_forecast)  colt_forecast,
                 SUM(tbl.nolt_forecast) nolt_forecast,
                 SUM(tbl.review_forecast) review_forecast,
                 SUM(tbl.isd_forecast) isd_forecast,
                 SUM(tbl.ss_forecast) ss_forecast
            from (select i_item,
                         i_locn,
                         i_domain_id,
                         thread_id,
                         DECODE(forecast_type,
                                'CLT_FORECAST', NVL(round(dif.forecast_sales,4),0),
                                0) colt_forecast,
                         DECODE(forecast_type,
                                'NLT_FORECAST', NVL(round(dif.forecast_sales,4),0),
                                0) nolt_forecast,
                         DECODE(forecast_type,
                                'REV_FORECAST', NVL(round(dif.forecast_sales,4),0),
                                0) review_forecast,
                         DECODE(forecast_type,
                                'ISD_FORECAST', NVL(round(dif.forecast_sales,4),0),
                                0) isd_forecast,
                         DECODE(forecast_type,
                                'SS_FORECAST',  NVL(round(dif.forecast_sales,4),0),
                                0) ss_forecast
                    from dif
                  union all
                  select i_item,
                         i_locn,
                         i_domain_id,
                         thread_id,
                         DECODE(forecast_type,
                                'CLT_FORECAST', NVL(round(item_forecast_sales,4),0),
                                0) colt_forecast,
                         DECODE(forecast_type,
                                'NLT_FORECAST', NVL(round(item_forecast_sales,4),0),
                                0) nolt_forecast,
                         DECODE(forecast_type,
                                'REV_FORECAST', NVL(round(item_forecast_sales,4),0),
                                0) review_forecast,
                         DECODE(forecast_type,
                                'ISD_FORECAST', NVL(round(item_forecast_sales,4),0),
                                0) isd_forecast,
                         DECODE(forecast_type,
                               'SS_FORECAST',  NVL(round(item_forecast_sales,4),0),
                               0) ss_forecast
                    from qry_get_forecast) tbl
           group by tbl.i_item,
                    tbl.i_locn,
                    tbl.i_domain_id,
                    tbl.thread_id) use_this
   ON (    target.i_item      = use_this.i_item
       and target.i_locn      = use_this.i_locn
       and NVL(target.i_domain_id, 0) = NVL(use_this.i_domain_id, 0)
       and target.thread_id   = use_this.thread_id
       and target.i_sub_item_master is NULL
       and target.i_lead_linked_wh is NULL)
   WHEN MATCHED THEN
      UPDATE SET target.o_curr_olt_forecast    = use_this.colt_forecast,
                 target.o_next_olt_forecast    = use_this.nolt_forecast,
                 target.o_review_time_forecast = use_this.review_forecast,
                 target.o_isd_forecast         = use_this.isd_forecast,
                 target.o_ss_forecast          = use_this.ss_forecast;


   return TRUE;

EXCEPTION
when OTHERS then
   O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                         SQLERRM,
                                         'CORESVC_REPL_ROQ_SQL.GET_FORECAST_FOR_PERIOD_DYN',
                                         to_char(SQLCODE));
   return FALSE;
END GET_FORECAST_FOR_PERIOD_DYN;
--------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PERSIST_DATA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_thread_id       IN       NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.PERSIST_DATA';

BEGIN

   insert into rpl_net_inventory_tmp (item,
                                      location,
                                      loc_type,
                                      curr_order_lead_time,
                                      next_order_lead_time,
                                      days_added_to_colt,
                                      days_added_to_nolt,
                                      review_time,
                                      next_delivery_date,
                                      next_review_date,
                                      roq,
                                      order_point,
                                      order_up_to_point,
                                      net_inventory,
                                      stock_on_hand,
                                      pack_comp_soh,
                                      on_order,
                                      in_transit_qty,
                                      pack_comp_intran,
                                      tsf_resv_qty,
                                      pack_comp_resv,
                                      tsf_expected_qty,
                                      pack_comp_exp,
                                      rtv_qty,
                                      alloc_in_qty,
                                      alloc_out_qty,
                                      non_sellable_qty,
                                      safety_stock,
                                      lost_sales,
                                      due_ind,
                                      aso,
                                      eso,
                                      min_sup_days_forecast,
                                      max_sup_days_forecast,
                                      time_sup_horizon_forecast,
                                      colt_forecast,
                                      nolt_forecast,
                                      review_time_forecast,
                                      inv_selling_days_forecast,
                                      supp_lead_time,
                                      pickup_lead_time,
                                      wh_lead_time,
                                      last_delivery_date)
                               select wrr.i_item,
                                      wrr.i_locn,
                                      wrr.i_locn_type,
                                      wrr.i_curr_order_lead_time,
                                      wrr.i_next_order_lead_time,
                                      wrr.i_days_added_to_colt,
                                      wrr.i_days_added_to_nolt,
                                      wrr.i_review_lead_time,
                                      wrr.i_next_delivery_date,
                                      wrr.i_next_review_date,
                                      NVL(wrr.o_order_qty,0),
                                      NVL(wrr.o_order_point,0),
                                      NVL(wrr.o_order_up_to_point,0),
                                      wrr.o_net_inventory,
                                      wrr.o_stock_on_hand,
                                      wrr.o_pack_comp_soh,
                                      wrr.o_on_order,
                                      wrr.o_in_transit_qty,
                                      wrr.o_pack_comp_intran,
                                      wrr.o_tsf_resv_qty,
                                      wrr.o_pack_comp_resv,
                                      wrr.o_tsf_expected_qty,
                                      wrr.o_pack_comp_exp,
                                      wrr.o_rtv_qty,
                                      wrr.o_alloc_in_qty,
                                      wrr.o_alloc_out_qty,
                                      wrr.o_non_sellable_qty,
                                      wrr.o_safety_stock,
                                      wrr.o_lost_sales,
                                      wrr.o_due_ind,
                                      wrr.o_aso,
                                      wrr.o_eso,
                                      wrr.o_min_supply_days_forecast,
                                      wrr.o_max_supply_days_forecast,
                                      wrr.o_tsh_forecast,
                                      wrr.o_curr_olt_forecast,
                                      wrr.o_next_olt_forecast,
                                      wrr.o_review_time_forecast,
                                      wrr.o_isd_forecast,
                                      wrr.i_supp_lead_time,
                                      wrr.i_pickup_lead_time,
                                      wrr.i_wh_lead_time,
                                      wrr.i_last_delivery_date
                                 from svc_repl_roq_gtt wrr
                                where wrr.thread_id = I_thread_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST_DATA;
-----------------------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_thread_id         IN       NUMBER,
                 I_last_run_of_day   IN       VARCHAR2 DEFAULT 'Y',
                 I_calling_program   IN       VARCHAR2 DEFAULT 'REPLROQ')
RETURN BOOLEAN IS

   L_program     VARCHAR2(64) := 'CORESVC_REPL_ROQ_SQL.CONSUME';

   cursor C_LOCK_REPL_ROQ is
      select 1
        from svc_repl_roq
       where thread_id = I_thread_id
         for update nowait;
BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_so_rec) = FALSE then
       return FALSE;
   end if;

   open C_DUMMY_ROW;
   fetch C_DUMMY_ROW into LP_dummy_row;
   close C_DUMMY_ROW;

   if I_calling_program = 'REPLROQ' then
      if SETUP_DATA_GTT(O_error_message,
                        I_thread_id) = FALSE then
         return FALSE;
      end if;

      open C_LOCK_REPL_ROQ;
      close C_LOCK_REPL_ROQ;
   end if;

   if CALC_DATES(O_error_message,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_DATA(O_error_message,
                   I_thread_id,
                   I_last_run_of_day,
                   I_calling_program) = FALSE then
      return FALSE;
   end if;

   if I_calling_program = 'REPLROQ' then
      if PERSIST_DATA(O_error_message,
                      I_thread_id) = FALSE then
         return FALSE;
      end if;

      -- delete records in the working table that were successfully persisted
      delete from svc_repl_roq
            where thread_id = I_thread_id;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CONSUME;
-----------------------------------------------------------------------------------------------------------
END CORESVC_REPL_ROQ_SQL;
/
