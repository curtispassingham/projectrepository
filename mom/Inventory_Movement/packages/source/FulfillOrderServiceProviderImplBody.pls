/******************************************************************************
* Service Name     : FulfillOrderService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/FulfillOrderService/v1
* Description      : 
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE BODY FulfillOrderServiceProviderImp AS

/******************************************************************************
 *
 * Operation       : createFulfilOrdColDesc
 * Description     : Create new Customer Orders or Transfers in RMS based on customer order 
                        fulfillment requests from an external Order Management System (OMS).
			           
 * 
 * Input           : "RIB_FulfilOrdColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FulfilOrdColDesc/v1
 * Description     : 
				      FulfilOrdColDesc object holds multiple customer order fulfillment requests from OMS.
                        
 * 
 * Output          : "RIB_FulfilOrdCfmCol_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FulfilOrdCfmCol/v1
 * Description     : 
				      FulfilOrdCfmCol object holds the confirmation messages as a result of 
                              processing the customer order fulfillment requests in RMS. It indicates if
                              a customer order is Fully created or Partially created or Not created in RMS. 
                              In case of a full or partial creation of a customer order, the confirmation
                              message also includes created PO or transfer number in RMS, as well as item's
                              confirmed quantity that can be sourced or fulfilled on the orders.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createFulfilOrdColDesc(I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                                 I_businessObject          IN     "RIB_FulfilOrdColDesc_REC",
                                 O_serviceOperationStatus     OUT "RIB_ServiceOpStatus_REC",
                                 O_businessObject             OUT "RIB_FulfilOrdCfmCol_REC")
IS

   L_program             VARCHAR2(60)            := 'fulfillOrderServiceProviderImp.createFulfilOrdColDesc';	
   L_status              "RIB_SuccessStatus_REC" := NULL;
   L_successStatus_TBL   "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();

BEGIN

   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;

   SVCPROV_FULFILORD.CREATE_FULFILLMENT(O_serviceOperationStatus,
                                        O_businessObject,
                                        I_serviceOperationContext,
                                        I_businessObject);

   if O_serviceOperationStatus.failStatus_TBL is NULL or O_serviceOperationStatus.failStatus_TBL.COUNT <= 0 then
      L_status := "RIB_SuccessStatus_REC"(0, 'createFulfilOrdColDesc service call was successful.');
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
   end if;

END createFulfilOrdColDesc;
/******************************************************************************/

/******************************************************************************
 *
 * Operation       : cancelFulfilOrdColRef
 * Description     : 
			      Cancel an existing Customer Order or Transfer in RMS based on customer order fulfillment
                        cancellation requests from an external Order Management System.
			           
 * 
 * Input           : "RIB_FulfilOrdColRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/FulfilOrdColRef/v1
 * Description     : 
				      FulfilOrdColRef object contains the customer order and fulfillment order numbers
                              to cancel.
                        
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
				      InvocationSuccess object contains the sucess or failure status of processing the 
                              confirmation requests.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE cancelFulfilOrdColRef(I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                                I_businessObject          IN     "RIB_FulfilOrdColRef_REC",
                                O_serviceOperationStatus     OUT "RIB_ServiceOpStatus_REC",
                                O_businessObject             OUT "RIB_InvocationSuccess_REC")
IS
   L_program             VARCHAR2(60)            := 'fulfillOrderServiceProviderImp.cancelFulfilOrdColRef';
   L_status              "RIB_SuccessStatus_REC" := NULL;
   L_successStatus_TBL   "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
	
BEGIN

   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;

   SVCPROV_FULFILORD.CANCEL_FULFILLMENT(O_serviceOperationStatus,
                                        O_businessObject,
                                        I_serviceOperationContext,
                                        I_businessObject);

   if O_serviceOperationStatus.failStatus_TBL is NULL or O_serviceOperationStatus.failStatus_TBL.COUNT <= 0 then
      L_status := "RIB_SuccessStatus_REC"(0, 'cancelFulfilOrdColRef service call was successful.');
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(L_successStatus_TBL.COUNT) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
   end if;

END cancelFulfilOrdColRef;
/******************************************************************************/

END FulfillOrderServiceProviderImp;
/



