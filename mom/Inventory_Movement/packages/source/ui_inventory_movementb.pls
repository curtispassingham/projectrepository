CREATE OR REPLACE PACKAGE Body INVENTORY_MOVEMENT_SQL IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : rtv (Form Module)
  -- Source Object            : FUNCTION   -> F_CHECK_DUP
  -- NEW ARGUMENTS (3)
  -- ITEM          :B_RTV_APPLY.LI_INV_STATUS.............. -> I_LI_INV_STATUS
  -- ITEM          :B_RTV_APPLY.LI_REASON.................. -> I_LI_REASON
  -- ITEM          :B_RTV_HEAD.RTV_ORDER_NO................ -> I_RTV_ORDER_NO
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_DUP(O_error_message   IN OUT   VARCHAR2,
                   I_LI_INV_STATUS   IN       VARCHAR2,
                   I_LI_REASON       IN       VARCHAR2,
                   I_RTV_ORDER_NO    IN       NUMBER,
                   I_item                     ITEM_MASTER.ITEM%TYPE)
  return BOOLEAN
is
  L_dup_exists      VARCHAR2(1) := 'N';
  L_status_desc     inv_status_types_tl.inv_status_desc%TYPE;
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  L_program         VARCHAR2(64) := 'INVENTORY_MOVEMENT_SQL.CHECK_DUP';
  ---
  cursor C_CHECK_DUP is
     select 'Y'
       from rtv_detail
      where rtv_order_no = I_RTV_ORDER_NO
        /*and (:B_rtv_apply.TI_shipment is NULL or
        shipment = :B_rtv_apply.TI_shipment)*/
        and item = I_item
        and reason = I_LI_REASON
        and (I_LI_INV_STATUS is NULL
         or (reason = 'U'
        and inv_status = I_LI_INV_STATUS));
BEGIN
  open C_CHECK_DUP;
  fetch C_CHECK_DUP into L_dup_exists;
  close C_CHECK_DUP;
  ---
  if L_dup_exists = 'Y' then
     --01/22/02 All shipment logic is being removed from the form for this release.
     --The logic will remain in the form in case that functionality returns.
     /*if :B_rtv_apply.TI_shipment is not NULL then
     emessage('DUP_RTV_DETAIL_SHIP', to_char(:B_rtv_apply.TI_shipment), I_item);
     elsif I_LI_INV_STATUS is not NULL then*/

     O_error_message := SQL_LIB.CREATE_MSG('DUP_RTV_DETAIL_ITEM',
                                           I_item,
                                           I_LI_REASON,
                                           NULL);
    return FALSE;
  else
    return TRUE;
  end if;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : rtv (Form Module)
-- Source Object            : PROCEDURE  -> P_RECALC_TOTALS
-- NEW ARGUMENTS (9)
-- ITEM          :B_RTV_HEAD.RESTOCK_COST................ -> I_RESTOCK_COST
-- ITEM          :B_RTV_HEAD.RESTOCK_COST_PRIM........... -> I_RESTOCK_COST_PRIM
-- ITEM          :B_RTV_HEAD.RTV_ORDER_NO................ -> I_RTV_ORDER_NO
-- ITEM          :B_RTV_HEAD.TOTAL_FINAL_RTV_COST........ -> I_TOTAL_FINAL_RTV_COST
-- ITEM          :B_RTV_HEAD.TOTAL_FINAL_RTV_COST_PRIM... -> I_TOTAL_FINAL_RTV_COST_PR
-- ITEM          :B_RTV_HEAD.TOTAL_ORDER_AMT............. -> I_TOTAL_ORDER_AMT
-- ITEM          :B_RTV_HEAD.TOTAL_ORDER_AMT_PRIM........ -> I_TOTAL_ORDER_AMT_PRIM
-- PARAMETER     :PARAMETER.PM_CURRENCY_PRIM............. -> P_PM_CURRENCY_PRIM
-- PARAMETER     :PARAMETER.PM_CURRENCY_SUP.............. -> P_PM_CURRENCY_SUP
--------------------------------------------------------------------------------------------
FUNCTION RECALC_TOTALS(O_error_message             IN OUT   VARCHAR2,
                       I_RESTOCK_COST              IN OUT   NUMBER,
                       I_RESTOCK_COST_PRIM         IN OUT   NUMBER,
                       I_RTV_ORDER_NO              IN       NUMBER,
                       I_TOTAL_FINAL_RTV_COST      IN OUT   NUMBER,
                       I_TOTAL_FINAL_RTV_COST_PR   IN OUT   NUMBER,
                       I_TOTAL_ORDER_AMT           IN OUT   NUMBER,
                       I_TOTAL_ORDER_AMT_PRIM      IN OUT   NUMBER,
                       P_PM_CURRENCY_PRIM          IN       VARCHAR2,
                       P_PM_CURRENCY_SUP           IN       VARCHAR2)
  return BOOLEAN
is
  L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
  L_total_order_amt   RTV_HEAD.total_order_amt%type;
  L_restock_cost      RTV_HEAD.restock_cost%type;
  L_program           VARCHAR2(64) := 'INVENTORY_MOVEMENT_SQL.RECALC_TOTALS';
  ---
  cursor C_GET_TOTAL
  is
    select NVL(SUM( QTY_REQUESTED * UNIT_COST ),0) ,
           NVL(SUM( QTY_REQUESTED * ( NVL(RESTOCK_PCT,0) /100 ) * UNIT_COST ),0)
      from rtv_detail
     where rtv_order_no = I_RTV_ORDER_NO;
BEGIN
  open C_GET_TOTAL;
  fetch C_GET_TOTAL into L_total_order_amt , L_restock_cost;
  close C_GET_TOTAL;
  I_TOTAL_FINAL_RTV_COST := L_total_order_amt - L_restock_cost;
  I_TOTAL_ORDER_AMT := L_total_order_amt;
  I_RESTOCK_COST := L_restock_cost;
  ---
  --- Convert the total order currency based on the new total.
  ---
  if CURRENCY_SQL.CONVERT(L_error_message,
                          I_TOTAL_ORDER_AMT,
                          P_PM_CURRENCY_SUP,
                          P_PM_CURRENCY_PRIM,
                          I_TOTAL_ORDER_AMT_PRIM,
                          'C',
                          NULL,
                          NULL) = FALSE then
     O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                           NULL,
                                           NULL,
                                           NULL);
    return FALSE;
  end if;
  ---
  --- Convert the restocking fee currency based on the new total.
  ---
  if CURRENCY_SQL.CONVERT(L_error_message,
                          I_RESTOCK_COST,
                          P_PM_CURRENCY_SUP,
                          P_PM_CURRENCY_PRIM,
                          I_RESTOCK_COST_PRIM,
                          'C',
                          NULL,
                          NULL) = FALSE then
     O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                           NULL,
                                           NULL,
                                           NULL);
    return FALSE;
  end if;
  ---
  I_TOTAL_FINAL_RTV_COST_PR := I_TOTAL_ORDER_AMT_PRIM - I_RESTOCK_COST_PRIM;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : rtv (Form Module)
-- Source Object            : FUNCTION   -> FORM_VALIDATION.ITEM_LIST
-- NEW ARGUMENTS (1)
-- ITEM          :B_RTV_APPLY.TI_ITEM.................... -> I_TI_ITEM
---------------------------------------------------------------------------------------------
FUNCTION ITEM_LIST(O_error_message   IN OUT   VARCHAR2,
                   O_exist           IN OUT   VARCHAR2,
                   I_TI_ITEM         IN       VARCHAR2)
  return BOOLEAN
is
  L_dummy     VARCHAR2(1);
  L_program   VARCHAR2(64) := 'INVENTORY_MOVEMENT_SQL.ITEM_LIST';
  cursor C_NON_ORDER_PACK_IN_LIST is
    select 'x'
      from item_master im,
           skulist_detail s
     where s.skulist = I_TI_ITEM
       and im.pack_ind = 'Y'
       and im.item = s.item
       and im.orderable_ind = 'N';
BEGIN
  ---
  open C_NON_ORDER_PACK_IN_LIST;
  fetch C_NON_ORDER_PACK_IN_LIST into L_dummy;
  if C_NON_ORDER_PACK_IN_LIST%FOUND then
     O_error_message := SQL_LIB.CREATE_MSG('NON_ORDER_PACK_IN_LIST',
                                           I_TI_ITEM,
                                           NULL,
                                           NULL);
     O_exist := 'Y';
  end if;
  close C_NON_ORDER_PACK_IN_LIST;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end ITEM_LIST;
---------------------------------------------------------------------------------------------
-- Module                   : tsfdetail (Form Module)
-- Source Object            : PROCEDURE  -> P_GET_BREAK_PACK_IND
-- NEW ARGUMENTS (0)
---------------------------------------------------------------------------------------------
FUNCTION GET_BREAK_PACK_IND(O_error_message    IN OUT   VARCHAR2,
                            O_break_pack_ind   IN OUT   WH.BREAK_PACK_IND%TYPE,
                            I_wh               IN       WH.WH%TYPE)
  return BOOLEAN
is
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  L_program         VARCHAR2(64) := 'INVENTORY_MOVEMENT_SQL.GET_BREAK_PACK_IND';
  cursor C_GET_BREAK_PACK_IND IS
     select break_pack_ind
       from wh
      where wh = I_wh ;
BEGIN
  open C_GET_BREAK_PACK_IND;
  fetch C_GET_BREAK_PACK_IND into O_break_pack_ind;
  close C_GET_BREAK_PACK_IND;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : tsfdetail (Form Module)
-- Source Object            : PROCEDURE  -> P_POPULATE_UOT_APPLY
-- NEW ARGUMENTS (20)
-- ITEM          :B_APPLY.LI_ITEM_TYPE................... -> I_LI_ITEM_TYPE
-- ITEM          :B_APPLY.TI_BREAK_PACK_IND.............. -> I_TI_BREAK_PACK_IND
-- ITEM          :B_APPLY.TI_CASE_DESC................... -> I_TI_CASE_DESC
-- ITEM          :B_APPLY.TI_CASE_NAME................... -> I_TI_CASE_NAME
-- ITEM          :B_APPLY.TI_INNER_DESC.................. -> I_TI_INNER_DESC
-- ITEM          :B_APPLY.TI_INNER_NAME.................. -> I_TI_INNER_NAME
-- ITEM          :B_APPLY.TI_ITEM........................ -> I_TI_ITEM
-- ITEM          :B_APPLY.TI_ORDERABLE_IND............... -> I_TI_ORDERABLE_IND
-- ITEM          :B_APPLY.TI_PACK_IND.................... -> I_TI_PACK_IND
-- ITEM          :B_APPLY.TI_PALLET_DESC................. -> I_TI_PALLET_DESC
-- ITEM          :B_APPLY.TI_PALLET_NAME................. -> I_TI_PALLET_NAME
-- ITEM          :B_APPLY.TI_PALLET_SIZE................. -> I_TI_PALLET_SIZE
-- ITEM          :B_APPLY.TI_STANDARD_UOM................ -> I_TI_STANDARD_UOM
-- ITEM          :B_APPLY.TI_STORE_ORD_MULT.............. -> I_TI_STORE_ORD_MULT
-- ITEM          :B_APPLY.TI_TSF_INNER_PACK_SIZE......... -> I_TI_TSF_INNER_PACK_SIZE
-- ITEM          :B_APPLY.TI_TSF_SUPP_PACK_SIZE.......... -> I_TI_TSF_SUPP_PACK_SIZE
-- ITEM          :B_APPLY.TI_UOT......................... -> I_TI_UOT
-- ITEM          :B_APPLY.TI_UOT_SIZE.................... -> I_TI_UOT_SIZE
-- ITEM          :B_HEAD.TI_TSF_NO....................... -> I_TI_TSF_NO
-- PARAMETER     :PARAMETER.PM_FROM_LOC.................. -> P_PM_FROM_LOC
-- CHANGED CALLS (1)
-- PROCEDURE     P_GET_BREAK_PACK_IND.................... -> INVENTORY_MOVEMENT_SQL.GET_BREAK_PACK_IND
---------------------------------------------------------------------------------------------
FUNCTION POPULATE_UOT_APPLY(O_error_message            IN OUT   VARCHAR2,
                            I_LI_ITEM_TYPE             IN       VARCHAR2,
                            I_TI_BREAK_PACK_IND        IN OUT   VARCHAR2,
                            I_TI_CASE_DESC             IN OUT   VARCHAR2,
                            I_TI_CASE_NAME             IN OUT   VARCHAR2,
                            I_TI_INNER_DESC            IN OUT   VARCHAR2,
                            I_TI_INNER_NAME            IN OUT   VARCHAR2,
                            I_TI_ITEM                  IN       VARCHAR2,
                            I_TI_ORDERABLE_IND         IN OUT   VARCHAR2,
                            I_TI_PACK_IND              IN       VARCHAR2,
                            I_TI_PALLET_DESC           IN OUT   VARCHAR2,
                            I_TI_PALLET_NAME           IN OUT   VARCHAR2,
                            I_TI_PALLET_SIZE           IN OUT   NUMBER,
                            I_TI_STANDARD_UOM          IN OUT   VARCHAR2,
                            I_TI_STORE_ORD_MULT        IN OUT   VARCHAR2,
                            I_TI_TSF_INNER_PACK_SIZE   IN OUT   NUMBER,
                            I_TI_TSF_SUPP_PACK_SIZE    IN OUT   NUMBER,
                            I_TI_UOT                   IN OUT   VARCHAR2,
                            I_TI_UOT_SIZE              IN OUT   NUMBER,
                            I_TI_TSF_NO                IN       NUMBER,
                            P_PM_FROM_LOC              IN       NUMBER)
  return BOOLEAN
is
  L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
  L_inner_pack_size          ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE;
  L_inner_desc               CODE_DETAIL.CODE_DESC%TYPE;
  L_inner_name               ITEM_SUPPLIER.INNER_NAME%TYPE;
  L_itspcnt_supp_pack_size   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
  L_ti                       ITEM_SUPP_COUNTRY.TI%TYPE;
  L_hi                       ITEM_SUPP_COUNTRY.HI%TYPE;
  L_pack_ind                 ITEM_MASTER.PACK_IND%TYPE;
  L_sellable_ind             ITEM_MASTER.SELLABLE_IND%TYPE;
  L_pack_type                ITEM_MASTER.PACK_TYPE%TYPE;
  L_item                     ITEM_MASTER%ROWTYPE;
  L_program                  VARCHAR2(64) := 'INVENTORY_MOVEMENT_SQL.POPULATE_UOT_APPLY';
  cursor C_TSFDTL_SIZE is
     select supp_pack_size
       from tsfdetail
      where item = I_TI_ITEM
        and tsf_no = I_TI_TSF_NO;
BEGIN
  I_TI_TSF_SUPP_PACK_SIZE := NULL;
  ---
  -- If the item type selected is item or style or style/color then the default uot will be
  -- standard uom if the store ord mult is in ('E' or 'I') or the supp_pack_size is 1.
  -- Otherwise the default uot will be case.
  ---
  if I_LI_ITEM_TYPE != 'IL' then
    if I_TI_PACK_IND  = 'Y' then
      if ITEM_ATTRIB_SQL.GET_PACK_INDS(L_error_message,
                                       L_pack_ind,
                                       L_sellable_ind,
                                       I_TI_ORDERABLE_IND,
                                       L_pack_type,
                                       I_TI_ITEM) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
    else
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_error_message,
                                         L_item,
                                         I_TI_ITEM) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
      I_TI_ORDERABLE_IND := L_item.orderable_ind;
    end if;
    ---
    -- populate standard uom holding field on detail block (used for validation and LOV)
    if ITEM_ATTRIB_SQL.GET_STORE_ORD_MULT_AND_UOM (L_error_message,
                                                   I_TI_STORE_ORD_MULT,
                                                   I_TI_STANDARD_UOM,
                                                   I_TI_ITEM) = FALSE then
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
    ---
    --- populate pallet and case size, desc, and name fields (used for validation and LOV)
    if SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES (L_error_message,
                                            L_itspcnt_supp_pack_size,
                                            I_TI_TSF_INNER_PACK_SIZE,
                                            I_TI_PALLET_DESC,
                                            I_TI_CASE_DESC,
                                            I_TI_INNER_DESC,
                                            I_TI_PALLET_NAME,
                                            I_TI_CASE_NAME,
                                            I_TI_INNER_NAME,
                                            I_TI_ITEM,
                                            NULL,
                                            NULL) = FALSE then
      if I_TI_ORDERABLE_IND                                                                                                                                                                                                                   = 'N' then
        --- Supplier information is optional for non-orderable items
        I_TI_UOT                  := I_TI_STANDARD_UOM;
        I_TI_UOT_SIZE             := 1;
        I_TI_TSF_SUPP_PACK_SIZE   := 1;
      else
         O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
    else
      ---
      if ITEM_SUPP_COUNTRY_SQL.GET_TI_HI(L_error_message,
                                         L_ti,
                                         L_hi,
                                         I_TI_ITEM) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
      ---
      I_TI_PALLET_SIZE := L_itspcnt_supp_pack_size * L_ti * L_hi;
      ---
      open C_TSFDTL_SIZE;
      fetch C_TSFDTL_SIZE into I_TI_TSF_SUPP_PACK_SIZE;
      close C_TSFDTL_SIZE;
      ---
      if I_TI_TSF_SUPP_PACK_SIZE is NULL then
        I_TI_TSF_SUPP_PACK_SIZE  := L_itspcnt_supp_pack_size;
      end if;
      ---
      if NOT INVENTORY_MOVEMENT_SQL.GET_BREAK_PACK_IND(L_error_message,
                                                       I_TI_BREAK_PACK_IND,
                                                       P_PM_FROM_LOC) then
         O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
      ---
      if I_TI_BREAK_PACK_IND       = 'N' then
        if I_TI_TSF_SUPP_PACK_SIZE = 1 or I_TI_STORE_ORD_MULT = 'E' or I_TI_STORE_ORD_MULT = 'I' then
          I_TI_UOT := I_TI_STANDARD_UOM;
          I_TI_UOT_SIZE := 1;
        else
          I_TI_UOT      := I_TI_CASE_DESC;
          I_TI_UOT_SIZE := I_TI_TSF_SUPP_PACK_SIZE;
        end if;
      else
        if I_TI_TSF_SUPP_PACK_SIZE = 1 or I_TI_STORE_ORD_MULT = 'E' then
          I_TI_UOT := I_TI_STANDARD_UOM;
          I_TI_UOT_SIZE := 1;
        elsif I_TI_STORE_ORD_MULT = 'I' then
          I_TI_UOT := I_TI_INNER_DESC;
          I_TI_UOT_SIZE := I_TI_TSF_INNER_PACK_SIZE;
        else
          I_TI_UOT := I_TI_CASE_DESC;
          I_TI_UOT_SIZE := I_TI_TSF_SUPP_PACK_SIZE;
        end if;
      end if;
    end if;
  else -- the item type selected is item list
    if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                  'LABL',
                                  'UOM',
                                  I_TI_UOT) = FALSE then
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
    ---
    I_TI_UOT_SIZE := 1;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
end INVENTORY_MOVEMENT_SQL;
/
