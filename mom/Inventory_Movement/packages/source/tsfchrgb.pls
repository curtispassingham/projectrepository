CREATE OR REPLACE PACKAGE BODY TRANSFER_CHARGE_SQL AS
-------------------------------------------------------------------------------
FUNCTION IS_FRANCHISE_TSF(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_franchise_tsf_ind        IN OUT VARCHAR2,
                          I_from_loc                 IN     STORE.STORE%TYPE,
                          I_from_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                          I_to_loc                   IN     STORE.STORE%TYPE,
                          I_to_loc_type              IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'TRANSFER_CHARGE_SQL.IS_FRANCHISE_TSF';
   L_f_action_type      VARCHAR2(1);

BEGIN
---
   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                L_f_action_type,
                                                I_from_loc,
                                                I_from_loc_type,
                                                I_to_loc,
                                                I_to_loc_type) = FALSE then
      return FALSE;
   end if;

   if L_f_action_type in ('O','R') then
      O_franchise_tsf_ind := 'Y';
   else
      O_franchise_tsf_ind := 'N';
   end if;

   return TRUE;
---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END IS_FRANCHISE_TSF;
---------------------------------------------------------------------------------------------------------
FUNCTION DELETE_CHRGS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                      I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'TRANSFER_CHARGE_SQL.DELETE_CHRGS';
   L_table       VARCHAR2(30) := 'TSFDETAIL_CHRG';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_TSFDETAIL_CHRG is
      select 'x'
        from tsfdetail_chrg
       where tsf_no = I_tsf_no
         and (item        = NVL(I_item, item)
             or pack_item = NVL(I_item, pack_item))
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LOCK_TSFDETAIL_CHRG','TSFDETAIL_CHRG','Tsf: '||to_char(I_tsf_no));
   open C_LOCK_TSFDETAIL_CHRG;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_TSFDETAIL_CHRG','TSFDETAIL_CHRG','Tsf: '||to_char(I_tsf_no));
   close C_LOCK_TSFDETAIL_CHRG;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'TSFDETAIL_CHRG','Tsf: '||to_char(I_tsf_no));
   delete from tsfdetail_chrg
         where tsf_no = I_tsf_no
           and (item        = NVL(I_item, item)
               or pack_item = NVL(I_item, pack_item));
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_tsf_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_CHRGS;
--------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_CHRGS_IND(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_default_chrgs_ind IN OUT TSF_TYPE.DEFAULT_CHRGS_IND%TYPE,
                               I_tsf_type          IN     TSF_TYPE.TSF_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'TRANSFER_CHARGE_SQL.GET_DEFAULT_CHRGS_IND';

   cursor C_GET_DEFAULT_CHRGS_IND is
      select default_chrgs_ind
        from tsf_type
       where tsf_type = I_tsf_type;

BEGIN
   O_default_chrgs_ind := 'N';
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_DEFAULT_CHRGS_IND','TSF_TYPE','Tsf Type: '|| I_tsf_type);
   open C_GET_DEFAULT_CHRGS_IND;
   SQL_LIB.SET_MARK('FETCH','C_GET_DEFAULT_CHRGS_IND','TSF_TYPE','Tsf Type: '|| I_tsf_type);
   fetch C_GET_DEFAULT_CHRGS_IND into O_default_chrgs_ind;
   SQL_LIB.SET_MARK('CLOSE','C_GET_DEFAULT_CHRGS_IND','TSF_TYPE','Tsf Type: '|| I_tsf_type);
   close C_GET_DEFAULT_CHRGS_IND;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_CHRGS_IND;
--------------------------------------------------------------------------------
FUNCTION DEFAULT_CHRGS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                       I_tsf_type      IN     TSFHEAD.TSF_TYPE%TYPE,
                       I_tsf_seq_no    IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                       I_shipment      IN     SHIPMENT.SHIPMENT%TYPE,
                       I_ship_seq_no   IN     SHIPSKU.SEQ_NO%TYPE,
                       I_from_loc      IN     STORE.STORE%TYPE,
                       I_from_loc_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_to_loc        IN     STORE.STORE%TYPE,
                       I_to_loc_type   IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(65) := 'TRANSFER_CHARGE_SQL.DEFAULT_CHRGS';
   L_item               ITEM_MASTER.ITEM%TYPE;
   L_comp_item          ITEM_MASTER.ITEM%TYPE;
   L_default_chrgs_ind  TSF_TYPE.DEFAULT_CHRGS_IND%TYPE := 'N';
   L_buyer_pack         VARCHAR2(1)                     := 'N';
   L_f_ord_ret_ind      VARCHAR2(1)                     := 'N';

   cursor C_BUYER_PACK is
      select 'Y'
        from item_master
       where item      = L_item
         and pack_ind  = 'Y'
         and pack_type = 'B';

   cursor C_PACKITEM is
      select item
        from v_packsku_qty
       where pack_no = L_item;

   -----------------------------------------------------------------------------------------
   -- Internal function called to perform the actual inserts into tsfdetail_chrg
   ---
   FUNCTION INSERT_CHRGS(IF_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                         IF_tsf_seq_no    IN     TSFDETAIL.TSF_SEQ_NO%TYPE,
                         IF_shipment      IN     SHIPMENT.SHIPMENT%TYPE,
                         IF_ship_seq_no   IN     SHIPSKU.SEQ_NO%TYPE,
                         IF_from_loc      IN     STORE.STORE%TYPE,
                         IF_from_loc_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                         IF_to_loc        IN     STORE.STORE%TYPE,
                         IF_to_loc_type   IN     ITEM_LOC.LOC_TYPE%TYPE,
                         IF_item          IN     ITEM_MASTER.ITEM%TYPE,
                         IF_pack_item     IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS
      LF_program   VARCHAR2(40) := 'TRANSFER_CHARGE_SQL.INSERT_CHRGS';

   BEGIN
      SQL_LIB.SET_MARK('INSERT', NULL, 'TSFDETAIL_CHRG', NULL);
      insert into tsfdetail_chrg(tsf_no,
                                 tsf_seq_no,
                                 shipment,
                                 ship_seq_no,
                                 item,
                                 from_loc,
                                 to_loc,
                                 comp_id,
                                 pack_item,
                                 from_loc_type,
                                 to_loc_type,
                                 comp_rate,
                                 per_count,
                                 per_count_uom,
                                 up_chrg_group,
                                 comp_currency,
                                 display_order)
                          select IF_tsf_no,
                                 IF_tsf_seq_no,
                                 IF_shipment,
                                 IF_ship_seq_no,
                                 IF_item,
                                 IF_from_loc,
                                 IF_to_loc,
                                 comp_id,
                                 IF_pack_item,
                                 IF_from_loc_type,
                                 IF_to_loc_type,
                                 comp_rate,
                                 per_count,
                                 per_count_uom,
                                 up_chrg_group,
                                 comp_currency,
                                 display_order
                            from item_chrg_detail i
                           where item          = IF_item
                             and from_loc      = IF_from_loc
                             and to_loc        = IF_to_loc
                             and not exists(select 'Y'
                                              from tsfdetail_chrg t
                                             where t.tsf_no              = IF_tsf_no
                                               and t.tsf_seq_no          = IF_tsf_seq_no
                                               and ((t.shipment          = IF_shipment
                                                     and IF_shipment    is not NULL
                                                     and t.ship_seq_no   = IF_ship_seq_no
                                                     and IF_ship_seq_no is not NULL)
                                                 or (IF_shipment        is NULL
                                                     and t.shipment     is NULL))
                                               and t.item                = IF_item
                                               and ((t.pack_item         = IF_pack_item
                                                     and IF_pack_item   is not NULL)
                                                 or (IF_pack_item       is NULL
                                                     and t.pack_item    is NULL))
                                               and t.from_loc            = IF_from_loc
                                               and t.to_loc              = IF_to_loc
                                               and t.comp_id             = i.comp_id);
       ---
       return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               LF_program,
                                               to_char(SQLCODE));
         return FALSE;
   END INSERT_CHRGS;
-----------------------------------------------------------------------------------------
BEGIN
   ---
   -- If transfer is related to F Order/Return, return TRUE without defaulting any charges.
   ---
   if IS_FRANCHISE_TSF(O_error_message,
                       L_f_ord_ret_ind,
                       I_from_loc,
                       I_from_loc_type,
                       I_to_loc,
                       I_to_loc_type) = FALSE then
      return FALSE;
   end if;
   
   if L_f_ord_ret_ind = 'Y' then
      return TRUE;
   end if;

   ---
   -- If Default Up Charges Indicator is set to 'N' for the Transfer Type
   -- return TRUE without defaulting any transfers.
   ---
   if GET_DEFAULT_CHRGS_IND(O_error_message,
                            L_default_chrgs_ind,
                            I_tsf_type) = FALSE then
      return FALSE;
   end if;
   ---
   if L_default_chrgs_ind = 'N' then
      return TRUE;
   end if;
   ---
   L_item := I_item;
   ---
   SQL_LIB.SET_MARK('OPEN','C_BUYER_PACK','ITEM_MASTER','Pack no: '||L_item);
   open C_BUYER_PACK;
   SQL_LIB.SET_MARK('FETCH','C_BUYER_PACK','ITEM_MASTER','Pack no: '||L_item);
   fetch C_BUYER_PACK into L_buyer_pack;
   SQL_LIB.SET_MARK('CLOSE','C_BUYER_PACK','ITEM_MASTER','Pack no: '||L_item);
   close C_BUYER_PACK;
   ---
   if L_buyer_pack = 'N' then
      if INSERT_CHRGS(I_tsf_no,
                      I_tsf_seq_no,
                      I_shipment,
                      I_ship_seq_no,
                      I_from_loc,
                      I_from_loc_type,
                      I_to_loc,
                      I_to_loc_type,
                      L_item,
                      NULL) = FALSE then
         return FALSE;
      end if;
   else  -- L_buyer_pack = 'Y'
      for C_rec in C_PACKITEM loop
         L_comp_item := C_rec.item;
         ---
         if INSERT_CHRGS(I_tsf_no,
                         I_tsf_seq_no,
                         I_shipment,
                         I_ship_seq_no,
                         I_from_loc,
                         I_from_loc_type,
                         I_to_loc,
                         I_to_loc_type,
                         L_comp_item,
                         L_item) = FALSE then
            return FALSE;
         end if;
         ---
      end loop;
   end if; -- L_buyer_pack = 'N'
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_CHRGS;
---------------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists        IN OUT BOOLEAN,
                       I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60)  := 'TRANSFER_CHARGE_SQL.CHARGES_EXIST';
   L_exists       VARCHAR2(1)   := 'N';

   cursor C_CHARGES_EXIST is
      select 'Y'
        from tsfdetail_chrg
       where tsf_no        = I_tsf_no
         and (item         = NVL(I_item, item)
              or pack_item = NVL(I_item, pack_item));

BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHARGES_EXIST','TSFDETAIL_CHRG','Transfer: '||to_char(I_tsf_no));
   open C_CHARGES_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_CHARGES_EXIST','TSFDETAIL_CHRG','Transfer: '||to_char(I_tsf_no));
   fetch C_CHARGES_EXIST into L_exists;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHARGES_EXIST','TSFDETAIL_CHRG','Transfer: '||to_char(I_tsf_no));
   close C_CHARGES_EXIST;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHARGES_EXIST;
----------------------------------------------------------------------------------------------------
FUNCTION SET_DEFAULT_CHRGS_2_LEG_IND(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_tsf_no            IN     TSFHEAD.TSF_NO%TYPE,
                                     I_item              IN     ITEM_MASTER.ITEM%TYPE,
                                     I_chrgs_2_leg_ind   IN     TSFDETAIL.DEFAULT_CHRGS_2_LEG_IND%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60)  := 'TRANSFER_CHARGE_SQL.SET_DEFAULT_CHRGS_2_LEG_IND';
   L_rowid              ROWID;

   L_table              VARCHAR2(30) := 'TSFDETAIL';
   L_key1               VARCHAR2(100) := I_tsf_no;
   L_key2               VARCHAR2(100) := I_item;
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_td_lock is
      select rowid
        from tsfdetail
       where tsf_no = I_tsf_no
         and item = I_item
         for update nowait;
BEGIN
   ---
   if I_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_item IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_td_lock;
   fetch C_td_lock into L_rowid;
   close C_td_lock;
   ---
   update tsfdetail
      set default_chrgs_2_leg_ind = I_chrgs_2_leg_ind,
          updated_by_rms_ind = 'Y'
    where rowid = L_rowid;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SET_DEFAULT_CHRGS_2_LEG_IND;
-------------------------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST_2ND_LEG(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_chrg_exists         IN OUT   BOOLEAN,
                               I_tsf_no              IN       TSFHEAD.TSF_NO%TYPE,
                               I_child_tsf_no        IN       TSFHEAD.TSF_NO%TYPE, 
                               I_finisher            IN       STORE.STORE%TYPE,
                               I_finisher_loc_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                               I_to_loc              IN       STORE.STORE%TYPE,
                               I_to_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(60) := 'TRANSFER_CHARGE_SQL.CHARGES_EXIST_2ND_LEG';
   L_chrg_items   CHRG_ITEM_TBL; --dummy

BEGIN

   if CHARGES_EXIST_2ND_LEG(O_error_message,
                            O_chrg_exists,
                            L_chrg_items,
                            I_tsf_no,
                            I_child_tsf_no,
                            I_finisher,
                            I_finisher_loc_type,
                            I_to_loc,
                            I_to_loc_type) = FALSE then
      return FALSE;
   end if;              

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHARGES_EXIST_2ND_LEG;
-------------------------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST_2ND_LEG(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_chrg_exists              IN OUT BOOLEAN,
                               O_chrg_items               IN OUT CHRG_ITEM_TBL,
                               I_tsf_no                   IN     TSFHEAD.TSF_NO%TYPE,
                               I_child_tsf_no             IN     TSFHEAD.TSF_NO%TYPE,
                               I_finisher                 IN     STORE.STORE%TYPE,
                               I_finisher_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_to_loc                   IN     STORE.STORE%TYPE,
                               I_to_loc_type              IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'TRANSFER_CHARGE_SQL.CHARGES_EXIST_2ND_LEG';

   -- The cursor below will retrive all items on the 2nd-leg which have up-charge at finisher/to_loc
   -- and the connected items on 1st-leg haven't set the default charge indicator

   cursor C_GET_2ND_LEG_ITEMS_LEFT is
      -- below: 2nd-leg item is bulk item or "non-buyer pack"
      --        and the default_chrgs_2_leg_ind of its connected item
      --        in the 1st-leg hasn't been set. (is null)
      select distinct leg2.item,
             leg2.tsf_seq_no
        from tsfdetail leg1,
             tsfdetail leg2,
             item_chrg_detail chrg
       where leg2.tsf_no = I_child_tsf_no
         and leg1.tsf_no = I_tsf_no
         and leg1.default_chrgs_2_leg_ind is null
         and chrg.item = leg2.item
         and chrg.from_loc = I_finisher
         and chrg.from_loc_type = I_finisher_loc_type
         and chrg.to_loc = I_to_loc
         and chrg.to_loc_type = I_to_loc_type
         and (leg1.item = leg2.item
              or leg1.item in (select txd.from_item
                                 from tsf_xform tx,
                                      tsf_xform_detail txd
                                where tx.tsf_xform_id = txd.tsf_xform_id
                                  and tx.tsf_no = I_tsf_no
                                  and txd.to_item = leg2.item
                                union all
                               select txd.from_item
                                 from tsf_xform tx,
                                      tsf_xform_detail txd,
                                      v_packsku_qty v
                                where tx.tsf_xform_id = txd.tsf_xform_id
                                  and tx.tsf_no = I_tsf_no
                                  and txd.from_item = v.item
                                  and leg1.item = v.pack_no
                                  and txd.to_item = leg2.item
                                union all
                               select tpd1.item
                                 from tsf_packing tp,
                                      tsf_packing_detail tpd,
                                      tsf_packing_detail tpd1
                                where tp.tsf_packing_id = tpd.tsf_packing_id
                                  and tpd1.tsf_packing_id = tpd.tsf_packing_id
                                  and tp.tsf_no = I_tsf_no
                                  and tpd.item = leg2.item
                                  and tpd.record_type = 'R'
                                  and tpd1.record_type = 'F'))
       union all
      -- below: 2nd-leg item is a Buyer Pack
      --        and the default_chrgs_2_leg_ind of its connected item
      --        in the 1st-leg hasn't been set (is null).
      select distinct leg2.item,
             leg2.tsf_seq_no
        from tsfdetail leg1,
             tsfdetail leg2,
             item_chrg_detail chrg,
             v_packsku_qty v,
             item_master im
       where leg2.tsf_no = I_child_tsf_no
         and leg1.tsf_no = I_tsf_no
         and leg1.default_chrgs_2_leg_ind is null
         and chrg.item = v.item
         and leg2.item = v.pack_no
         and im.item = leg2.item
         and im.pack_type = 'B'
         and chrg.from_loc = I_finisher
         and chrg.from_loc_type = I_finisher_loc_type
         and chrg.to_loc = I_to_loc
         and chrg.to_loc_type = I_to_loc_type
         and (leg1.item = leg2.item
              or leg1.item in (select tpd1.item
                                 from tsf_packing tp,
                                      tsf_packing_detail tpd,
                                      tsf_packing_detail tpd1
                                where tp.tsf_packing_id = tpd.tsf_packing_id
                                  and tpd1.tsf_packing_id = tpd.tsf_packing_id
                                  and tp.tsf_no = I_tsf_no
                                  and tpd.item = leg2.item
                                  and tpd.record_type = 'R'
                                  and tpd1.record_type = 'F'));

BEGIN
   --
   if I_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_child_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_child_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_finisher IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_finisher',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_finisher_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_finisher_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_to_loc IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_to_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   O_chrg_items.DELETE;
   ---
   open C_GET_2ND_LEG_ITEMS_LEFT;
   fetch C_GET_2ND_LEG_ITEMS_LEFT BULK COLLECT INTO O_chrg_items;
   close C_GET_2ND_LEG_ITEMS_LEFT;
   ---
   if O_chrg_items.count = 0 then
      O_chrg_exists := FALSE;
   else
      O_chrg_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHARGES_EXIST_2ND_LEG;
--------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_CHRGS_2ND_LEG(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no                    IN       TSFHEAD.TSF_NO%TYPE,
                               I_tsf_type                  IN       TSFHEAD.TSF_TYPE%TYPE,
                               I_child_tsf_no              IN       TSFHEAD.TSF_NO%TYPE, 
                               I_finisher                  IN       STORE.STORE%TYPE,
                               I_finisher_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                               I_to_loc                    IN       STORE.STORE%TYPE,
                               I_to_loc_type               IN       ITEM_LOC.LOC_TYPE%TYPE,
                               I_default_chrgs_2_leg_ind   IN       TSFDETAIL.DEFAULT_CHRGS_2_LEG_IND%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(60) := 'TRANSFER_CHARGE_SQL.DEFAULT_CHRGS_2ND_LEG';
   L_chrg_exists  BOOLEAN; --dummy
   L_chrg_items   CHRG_ITEM_TBL;

BEGIN
   
   if CHARGES_EXIST_2ND_LEG(O_error_message,
                            L_chrg_exists,
                            L_chrg_items,  --output
                            I_tsf_no,
                            I_child_tsf_no,
                            I_finisher,
                            I_finisher_loc_type,
                            I_to_loc,
                            I_to_loc_type) = FALSE then
      return FALSE;
   end if;              
   
   if DEFAULT_CHRGS_2ND_LEG(O_error_message,
                            I_tsf_no,
                            I_tsf_type,
                            I_child_tsf_no,
                            I_finisher,
                            I_finisher_loc_type,
                            I_to_loc,
                            I_to_loc_type,
                            I_default_chrgs_2_leg_ind,
                            L_chrg_items) = FALSE then  --input
      return FALSE;
   end if; 

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_CHRGS_2ND_LEG;
------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_CHRGS_2ND_LEG(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_tsf_no                   IN     TSFHEAD.TSF_NO%TYPE,
                               I_tsf_type                 IN     TSFHEAD.TSF_TYPE%TYPE,
                               I_child_tsf_no             IN     TSFHEAD.TSF_NO%TYPE,
                               I_finisher                 IN     STORE.STORE%TYPE,
                               I_finisher_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_to_loc                   IN     STORE.STORE%TYPE,
                               I_to_loc_type              IN     ITEM_LOC.LOC_TYPE%TYPE,
                               I_default_chrgs_2_leg_ind  IN     TSFDETAIL.DEFAULT_CHRGS_2_LEG_IND%TYPE,
                               I_chrg_items               IN     CHRG_ITEM_TBL)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'TRANSFER_CHARGE_SQL.DEFAULT_CHRGS_2ND_LEG';
   L_chrg_exists        BOOLEAN;
   L_chrg_idx           BINARY_INTEGER;

   -- 2nd-leg's item whose connected item at 1st-leg
   -- has been set to default up-charges to 2nd-leg

   cursor C_GET_2ND_LEG_CHRGS_ITEMS is
   -- below: 2nd-leg item is bulk item or "non-Buyer Pack"
      select distinct leg2.item,
             leg2.tsf_seq_no
        from tsfdetail leg1,
             tsfdetail leg2,
             item_chrg_detail chrg
       where leg2.tsf_no = I_child_tsf_no
         and leg1.tsf_no = I_tsf_no
         and leg1.default_chrgs_2_leg_ind = 'Y'
         and chrg.item = leg2.item
         and chrg.from_loc = I_finisher
         and chrg.from_loc_type = I_finisher_loc_type
         and chrg.to_loc = I_to_loc
         and chrg.to_loc_type = I_to_loc_type
         and (leg1.item = leg2.item
              or leg1.item in (select txd.from_item
                                 from tsf_xform tx,
                                      tsf_xform_detail txd
                                where tx.tsf_xform_id = txd.tsf_xform_id
                                  and tx.tsf_no = I_tsf_no
                                  and txd.to_item = leg2.item
                                union all
                               select txd.from_item
                                 from tsf_xform tx,
                                      tsf_xform_detail txd,
                                      v_packsku_qty v
                                where tx.tsf_xform_id = txd.tsf_xform_id
                                  and tx.tsf_no = I_tsf_no
                                  and txd.from_item = v.item
                                  and leg1.item = v.pack_no
                                  and txd.to_item = leg2.item
                                union all
                               select tpd1.item
                                 from tsf_packing tp,
                                      tsf_packing_detail tpd,
                                      tsf_packing_detail tpd1
                                where tp.tsf_packing_id = tpd.tsf_packing_id
                                  and tpd1.tsf_packing_id = tpd.tsf_packing_id
                                  and tp.tsf_no = I_tsf_no
                                  and tpd.item = leg2.item
                                  and tpd.record_type = 'R'
                                  and tpd1.record_type = 'F'))
       union all
       -- below: 2nd-leg item is a buyer pack
      select distinct leg2.item,
             leg2.tsf_seq_no
        from tsfdetail leg1,
             tsfdetail leg2,
             item_chrg_detail chrg,
             v_packsku_qty v,
             item_master im
       where leg2.tsf_no = I_child_tsf_no
         and leg1.tsf_no = I_tsf_no
         and leg1.default_chrgs_2_leg_ind = 'Y'
         and chrg.item = v.item
         and leg2.item = v.pack_no
         and im.item = leg2.item
         and im.pack_type = 'B'
         and chrg.from_loc = I_finisher
         and chrg.from_loc_type = I_finisher_loc_type
         and chrg.to_loc = I_to_loc
         and chrg.to_loc_type = I_to_loc_type
         and (leg1.item = leg2.item
              or leg1.item in (select tpd1.item
                                 from tsf_packing tp,
                                      tsf_packing_detail tpd,
                                      tsf_packing_detail tpd1
                                where tp.tsf_packing_id = tpd.tsf_packing_id
                                  and tpd1.tsf_packing_id = tpd.tsf_packing_id
                                  and tp.tsf_no = I_tsf_no
                                  and tpd.item = leg2.item
                                  and tpd.record_type = 'R'
                                  and tpd1.record_type = 'F'));

BEGIN
   --
   if I_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_child_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_child_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_finisher IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_finisher',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_finisher_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_finisher_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_to_loc IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_to_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if CHARGES_EXIST(O_error_message,
                    L_chrg_exists,
                    I_child_tsf_no,
                    NULL) = FALSE then
      return FALSE;
   end if;
   ---
   if L_chrg_exists = TRUE then
      if DELETE_CHRGS(O_error_message,
                      I_child_tsf_no,
                      NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   FOR REC_item IN C_GET_2ND_LEG_CHRGS_ITEMS LOOP
      ---
      if DEFAULT_CHRGS(O_error_message,
                       I_child_tsf_no,
                       I_tsf_type,
                       REC_item.tsf_seq_no,
                       NULL,     -- I_shipment
                       NULL,     -- I_ship_seq_no,
                       I_finisher,
                       I_finisher_loc_type,
                       I_to_loc,
                       I_to_loc_type,
                       REC_item.item) = FALSE then
         return FALSE;
      end if;
      ---
   END LOOP;
   ---
   if I_default_chrgs_2_leg_ind = 'Y' then
      ---
      FOR L_chrg_idx IN 1..I_chrg_items.count LOOP
         ---
         if DEFAULT_CHRGS(O_error_message,
                          I_child_tsf_no,
                          I_tsf_type,
                          I_chrg_items(L_chrg_idx).tsf_seq_no,
                          NULL,     -- I_shipment
                          NULL,     -- I_ship_seq_no,
                          I_finisher,
                          I_finisher_loc_type,
                          I_to_loc,
                          I_to_loc_type,
                          I_chrg_items(L_chrg_idx).item) = FALSE then
            return FALSE;
         end if;
         ---
      END LOOP;
      ---
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_CHRGS_2ND_LEG;
---------------------------------------------------------------------------------------------------------
FUNCTION ITEM_EXISTS_ON_LEG_2(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists                   IN OUT BOOLEAN,
                              I_tsf_no                   IN     TSFHEAD.TSF_NO%TYPE,
                              I_item                     IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'TRANSFER_CHARGE_SQL.ITEM_EXISTS_ON_LEG_2';
   L_exists             VARCHAR2(1) := 'N';

   cursor C_item_exists is
      select 'Y'
        from v_tsfdetail_2_leg
       where tsf_no = I_tsf_no
         and item = I_item;
BEGIN
   ---
   if I_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_item_exists;
   fetch C_item_exists into L_exists;
   close C_item_exists;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_ON_2_LEG',
                                            I_item,
                                            I_tsf_no,
                                            NULL);
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ITEM_EXISTS_ON_LEG_2;
---------------------------------------------------------------------------------------------------------
END TRANSFER_CHARGE_SQL;
/
