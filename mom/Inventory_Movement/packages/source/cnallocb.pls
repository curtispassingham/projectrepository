CREATE OR REPLACE PACKAGE BODY CANCEL_ALLOC_SQL AS
-----------------------------------------------------------------------------------------------------
FUNCTION CANCEL_SINGLE_ALLOC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN       ALLOC_HEADER.ORDER_NO%TYPE,
                             I_cancel_id         IN       ORDLOC.CANCEL_ID%TYPE,
                             I_alloc_close_ind   IN       VARCHAR2)
RETURN BOOLEAN is

   L_program               VARCHAR2(50) := 'CANCEL_ALLOC_SQL.CANCEL_SINGLE_ALLOC';

   L_closed                BOOLEAN;
   L_pick_not_after_days   NUMBER;
   L_vdate                 PERIOD.VDATE%TYPE := GET_VDATE;
   L_alloc_num             ALLOC_HEADER.ALLOC_NO%TYPE;
   L_shipment              SHIPMENT.SHIPMENT%TYPE;

   cursor C_CLOSE_ALH is
      select alh.alloc_no
        from alloc_header alh
       where alh.order_no = I_order_no
         and status <> 'C'
         and NVL(alh.doc_type, 'X') <> 'ASN'
         and (L_pick_not_after_days is NULL or (L_pick_not_after_days is NOT NULL and (release_date + L_pick_not_after_days < L_vdate)))
      UNION ALL
      select alh.alloc_no
        from alloc_header alh
       where alh.doc_type = 'ALLOC'
         and alh.order_no in (select alloc_no
                                from alloc_header
                               where order_no = I_order_no
                                 and status <> 'C')
       UNION 
       select alh.alloc_no 
         from alloc_header alh 
       where I_cancel_id = 'ordautcl' 
         and alh.order_no = I_order_no 
         and status <> 'C' 
         and not exists ( select 'Y'  
                          from shipment sh, shipsku ss 
                          where sh.order_no = I_order_no 
                            and sh.shipment = ss.shipment 
                            and alh.item    = ss.item 
                            and NVL(ss.qty_received, 0) <> 0 
                            and rownum = 1) 

         order by alloc_no;

   cursor C_GET_PICK_NOT_AFTER_DAYS is
      select cd.code_desc
        from code_detail cd
       where cd.code_type = 'DEFT'
         and code = 'DATE';

   cursor C_ORD_WITH_NO_SHIPMENT is
      select shipment
        from shipment sh1
       where sh1.order_no = I_order_no;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            'I_order_no',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_cancel_id = 'ordautcl' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_ORD_WITH_NO_SHIPMENT',
                       'SHIPMENT',
                       'Order_No = '||to_char(I_order_no));
      open C_ORD_WITH_NO_SHIPMENT;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ORD_WITH_NO_SHIPMENT',
                       'SHIPMENT',
                       'Order_No = '||to_char(I_order_no));
      fetch C_ORD_WITH_NO_SHIPMENT into L_shipment;
      if C_ORD_WITH_NO_SHIPMENT%NOTFOUND then
         L_pick_not_after_days := NULL;
      else
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_PICK_NOT_AFTER_DAYS',
                          'PERIOD, CODE_DETAIL',
                          'Order_No = '||to_char(I_order_no));
         open C_GET_PICK_NOT_AFTER_DAYS;
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_PICK_NOT_AFTER_DAYS',
                          'PERIOD, CODE_DETAIL',
                          'Order_No = '||to_char(I_order_no));
         fetch C_GET_PICK_NOT_AFTER_DAYS into L_pick_not_after_days;
         if C_GET_PICK_NOT_AFTER_DAYS%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_PICK_NOT_AFTER_DAYS',
                             'PERIOD, CODE_DETAIL',
                             'Order_No = '||to_char(I_order_no));
            close C_GET_PICK_NOT_AFTER_DAYS;
         return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_PICK_NOT_AFTER_DAYS',
                          'PERIOD, CODE_DETAIL',
                          'Order_No = '||to_char(I_order_no));
         close C_GET_PICK_NOT_AFTER_DAYS;
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ORD_WITH_NO_SHIPMENT',
                       'SHIPMENT',
                       'Order_No = '||to_char(I_order_no));
      close C_ORD_WITH_NO_SHIPMENT;
   else
      L_pick_not_after_days := NULL;
   end if;

   SQL_LIB.SET_MARK('LOOP',
                    'C_CLOSE_ALH',
                    'ALLOC_HEADER',
                    'Order_No : '||to_char(I_order_no));
   FOR c_close_alloc_rec in C_CLOSE_ALH LOOP
      L_alloc_num := c_close_alloc_rec.alloc_no;
      ---
      if CANCEL_ALLOC_SQL.UPDATE_ALLOC(O_error_message,
                                       L_alloc_num) = FALSE then
         return FALSE;
      end if;
      if NVL(I_alloc_close_ind,'N') = 'Y' then
         if APPT_DOC_CLOSE_SQL.CLOSE_ALLOC(O_error_message,
                                           L_closed,
                                           L_alloc_num) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CANCEL_SINGLE_ALLOC;
-----------------------------------------------------------------------------------------------------
FUNCTION CANCEL_ASN_ALLOC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no          IN       ALLOC_HEADER.ORDER_NO%TYPE,
                          I_alloc_close_ind   IN       VARCHAR2)
RETURN BOOLEAN is

   L_program        VARCHAR2(50) := 'CANCEL_ALLOC_SQL.CANCEL_ASN_ALLOC';

   L_closed         BOOLEAN;
   L_alloc_no       ALLOC_HEADER.ALLOC_NO%TYPE;

   cursor C_CLOSE_ALH is
      select alloc_no
        from alloc_header
       where order_no = I_order_no
         and doc_type = 'ASN'
         and status <> 'C'
      UNION ALL
      select alloc_no
        from alloc_header
       where doc_type = 'ALLOC'
         and order_no in (select alloc_no
                            from alloc_header
                           where order_no = I_order_no
                             and doc_type = 'ASN'
                             and status <> 'C');

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('LOOP',
                    'C_CLOSE_ALH',
                    'ALLOC_HEADER',
                    'Order_No: '||to_char(I_order_no));

   FOR c_close_alh_rec in C_CLOSE_ALH LOOP
      L_alloc_no := c_close_alh_rec.alloc_no;
      ---
      if CANCEL_ALLOC_SQL.UPDATE_ALLOC(O_error_message,
                                       L_alloc_no) = FALSE then
         return FALSE;
      end if;
      ---
      if NVL(I_alloc_close_ind, 'N') = 'Y' then
         if APPT_DOC_CLOSE_SQL.CLOSE_ALLOC(O_error_message,
                                           L_closed,
                                           L_alloc_no) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CANCEL_ASN_ALLOC;
-----------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN is

   L_program         VARCHAR2(50) := 'CANCEL_ALLOC_SQL.UPDATE_ALLOC';

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ALD is
      select 1
        from alloc_detail ald
       where ald.alloc_no = I_alloc_no
         and qty_allocated > GREATEST(NVL(po_rcvd_qty,0),(GREATEST(NVL(qty_selected,0),0) + GREATEST(NVL(qty_distro,0),0) + NVL(qty_transferred,0)))
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ALD',
                    'ALLOC_DETAIL',
                    'Alloc No: '||to_char(I_alloc_no));
   open C_LOCK_ALD;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ALD',
                    'ALLOC_DETAIL',
                    'Alloc No: '||to_char(I_alloc_no));
   close C_LOCK_ALD;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'ALLOC_DETAIL',
                    'Alloc No: '||to_char(I_alloc_no));
   ---
   update alloc_detail
       set qty_allocated = GREATEST(NVL(po_rcvd_qty,0),(GREATEST(NVL(qty_selected,0),0) + GREATEST(NVL(qty_distro,0),0) + NVL(qty_transferred,0))), 
           qty_cancelled = qty_allocated - GREATEST(NVL(po_rcvd_qty,0),(GREATEST(NVL(qty_selected,0),0) + GREATEST(NVL(qty_distro,0),0) + NVL(qty_transferred,0))) 
     where alloc_no      = I_alloc_no 
       and qty_allocated > GREATEST(NVL(po_rcvd_qty,0),(GREATEST(NVL(qty_selected,0),0) + GREATEST(NVL(qty_distro,0),0) + NVL(qty_transferred,0)));
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ALLOC_DETAIL_REC_LOCK_2',
                                            to_char(I_alloc_no),
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ALLOC;
-----------------------------------------------------------------------------------------------------
FUNCTION CANCEL_ALLOC_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_location        IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN is

   L_program    VARCHAR2(50) := 'CANCEL_ALLOC_SQL.CANCEL_ALLOC_ITEM';

   L_closed     BOOLEAN;
   L_alloc_no   ALLOC_HEADER.ALLOC_NO%TYPE;

   cursor C_CLOSE_ALH is
      select alloc_no
        from alloc_header
       where order_no = I_order_no
         and item = I_item
         and wh = I_location
         and status <> 'C'
      UNION ALL
      select alloc_no
        from alloc_header
       where doc_type = 'ALLOC'
         and order_no in (select alloc_no
                            from alloc_header
                           where order_no = I_order_no
                             and item = I_item
                             and wh = I_location
                             and status <> 'C');

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_order_no',
                                            NULL);
      return FALSE;
   elsif I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_item',
                                            NULL);
      return FALSE;
   elsif I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_location',
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('LOOP',
                    'C_CLOSE_ALH',
                    'ALLOC_HEADER',
                    'Order no: '||TO_CHAR(I_order_no)||
                    ' Item : '||TO_CHAR(I_item)||
                    ' Location : '||TO_CHAR(I_location));

   FOR c_close_alh_rec in C_CLOSE_ALH LOOP
      L_alloc_no := c_close_alh_rec.alloc_no;
      ---
      if CANCEL_ALLOC_SQL.UPDATE_ALLOC(O_error_message,
                                       L_alloc_no) = FALSE then
         return FALSE;
      end if;
      if APPT_DOC_CLOSE_SQL.CLOSE_ALLOC(O_error_message,
                                        L_closed,
                                        L_alloc_no) = FALSE then
          return FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CANCEL_ALLOC_ITEM;
-----------------------------------------------------------------------------------------------------
END CANCEL_ALLOC_SQL;
/