CREATE OR REPLACE PACKAGE RMSSUB_RECEIVING AUTHID CURRENT_USER AS

APPOINT_HDR_ADD        CONSTANT  VARCHAR2(30) := 'appointcre';
APPOINT_HDR_UPD        CONSTANT  VARCHAR2(30) := 'appointhdrmod';
APPOINT_HDR_DEL        CONSTANT  VARCHAR2(30) := 'appointdel';
APPOINT_DTL_ADD        CONSTANT  VARCHAR2(30) := 'appointdtlcre';
APPOINT_DTL_UPD        CONSTANT  VARCHAR2(30) := 'appointdtlmod';
APPOINT_DTL_DEL        CONSTANT  VARCHAR2(30) := 'appointdtldel';

RECEIPT_ADD            CONSTANT  VARCHAR2(30) := 'receiptcre';
RECEIPT_UPD            CONSTANT  VARCHAR2(30) := 'receiptmod';
RECEIPT_ORDADD         CONSTANT  VARCHAR2(30) := 'receiptordcre';

--------------------------------------------------------------------------------
-- Procedure Name: CONSUME
-- Purpose: This public procedure is to be called from the RIB. 
--------------------------------------------------------------------------------
PROCEDURE CONSUME (O_status_code          IN OUT  VARCHAR2,
                   O_error_message        IN OUT  VARCHAR2,
                   I_message              IN      RIB_OBJECT,
                   I_message_type         IN      VARCHAR2,
                   O_rib_otbdesc_rec         OUT  RIB_OBJECT,
                   O_rib_error_tbl           OUT  RIB_ERROR_TBL);
--------------------------------------------------------------------------------
-- Procedure Name: CONSUME_RECEIPT
-- Purpose: This public procedure is to be called from the dequeue process 
--          (notify_receiving_message), the error retry process (ribapi_aq_reprocess_sql)
--          and from RFM's RECEIVING consume function (fm_receiving_consume_sql.consume). 
--------------------------------------------------------------------------------
PROCEDURE CONSUME_RECEIPT(O_status_code          IN OUT  VARCHAR2,
                          O_error_message        IN OUT  VARCHAR2,
                          I_message              IN      RIB_OBJECT,
                          I_message_type         IN      VARCHAR2,
                          I_check_l10n_ind       IN      VARCHAR2,
                          O_rib_otbdesc_rec         OUT  RIB_OBJECT,
                          O_rib_error_tbl           OUT  RIB_ERROR_TBL);
--------------------------------------------------------------------------------
-- Function Name: CONSUME_RECEIPT
-- Purpose: This public function is the RMS base function called through the
--          L10N_SQL layer. 
--------------------------------------------------------------------------------
FUNCTION CONSUME_RECEIPT(O_error_message        IN OUT  VARCHAR2,
                         IO_L10N_RIB_REC        IN OUT  L10N_OBJ)
RETURN BOOLEAN;
--------------------------------------------------------------------------------                           
END RMSSUB_RECEIVING;
/