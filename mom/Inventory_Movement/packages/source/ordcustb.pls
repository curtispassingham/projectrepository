CREATE OR REPLACE PACKAGE BODY ORDCUST_ATTRIB_SQL AS
----------------------------------------------------------------------------------------------
FUNCTION PERSIST_ORDCUST_L10N_EXT(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_l10n_ordcust_ext_tbl   IN OUT   L10N_ORDCUST_EXT_TBL)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ORDCUST_ATTRIB_SQL.PERSIST_ORDCUST_L10N_EXT';

BEGIN

   if IO_l10n_ordcust_ext_tbl is NULL or IO_l10n_ordcust_ext_tbl.COUNT <= 0 then
      return TRUE;
   end if;

   FOR i in IO_l10n_ordcust_ext_tbl.FIRST..IO_l10n_ordcust_ext_tbl.LAST LOOP
      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                IO_l10n_ordcust_ext_tbl(i)) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDCUST_ATTRIB_SQL.PERSIST_ORDCUST_L10N_EXT',
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST_ORDCUST_L10N_EXT;
----------------------------------------------------------------------------------------------
END ORDCUST_ATTRIB_SQL;
/
