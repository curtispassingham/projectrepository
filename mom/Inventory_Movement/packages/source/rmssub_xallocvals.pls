
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XALLOC_VALIDATE AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   -- PACKAGE VARIABLES
----------------------------------------------------------------------------
   LP_dtl_mod_type   VARCHAR2(15) := 'xallocdtlmod';
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_alloc_rec       OUT    NOCOPY   ALLOCATION_SQL.ALLOC_REC,
                       I_message         IN              "RIB_XAlloc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_alloc_rec       OUT    NOCOPY   ALLOCATION_SQL.ALLOC_REC,
                       I_message         IN              "RIB_XAllocRef_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XALLOC_VALIDATE;
/



