CREATE OR REPLACE PACKAGE INVENTORY_MOVEMENT_SQL AUTHID CURRENT_USER IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : rtv (Form Module) 
  -- Source Object            : FUNCTION   -> F_CHECK_DUP
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_DUP(O_error_message   IN OUT   VARCHAR2,
				I_LI_INV_STATUS      IN       VARCHAR2,
				I_LI_REASON          IN       VARCHAR2,
				I_RTV_ORDER_NO       IN       NUMBER,
				I_item                        ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : rtv (Form Module) 
  -- Source Object            : PROCEDURE  -> P_RECALC_TOTALS
  ---------------------------------------------------------------------------------------------
FUNCTION RECALC_TOTALS(O_error_message             IN OUT   VARCHAR2,
					   I_RESTOCK_COST              IN OUT   NUMBER,
					   I_RESTOCK_COST_PRIM         IN OUT   NUMBER,
					   I_RTV_ORDER_NO              IN       NUMBER,
					   I_TOTAL_FINAL_RTV_COST      IN OUT   NUMBER,
					   I_TOTAL_FINAL_RTV_COST_PR   IN OUT   NUMBER,
					   I_TOTAL_ORDER_AMT           IN OUT   NUMBER,
					   I_TOTAL_ORDER_AMT_PRIM      IN OUT   NUMBER,
					   P_PM_CURRENCY_PRIM          IN       VARCHAR2,
					   P_PM_CURRENCY_SUP           IN       VARCHAR2)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : rtv (Form Module) 
  -- Source Object            : FUNCTION   -> FORM_VALIDATION.ITEM_LIST
  ---------------------------------------------------------------------------------------------
FUNCTION ITEM_LIST(O_error_message   IN OUT   VARCHAR2,
				   O_exist           IN OUT   VARCHAR2,
				   I_TI_ITEM         IN       VARCHAR2)
   return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : tsfdetail (Form Module) 
  -- Source Object            : PROCEDURE  -> P_GET_BREAK_PACK_IND
  ---------------------------------------------------------------------------------------------
FUNCTION GET_BREAK_PACK_IND(O_error_message    IN OUT   VARCHAR2,
							O_break_pack_ind   IN OUT   WH.BREAK_PACK_IND%TYPE,
							I_wh               IN       WH.WH%TYPE)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : tsfdetail (Form Module) 
  -- Source Object            : PROCEDURE  -> P_POPULATE_UOT_APPLY
  ---------------------------------------------------------------------------------------------
FUNCTION POPULATE_UOT_APPLY(O_error_message            IN OUT   VARCHAR2,
							I_LI_ITEM_TYPE             IN       VARCHAR2,
							I_TI_BREAK_PACK_IND        IN OUT   VARCHAR2,
							I_TI_CASE_DESC             IN OUT   VARCHAR2,
							I_TI_CASE_NAME             IN OUT   VARCHAR2,
							I_TI_INNER_DESC            IN OUT   VARCHAR2,
							I_TI_INNER_NAME            IN OUT   VARCHAR2,
							I_TI_ITEM                  IN       VARCHAR2,
							I_TI_ORDERABLE_IND         IN OUT   VARCHAR2,
							I_TI_PACK_IND              IN       VARCHAR2,
							I_TI_PALLET_DESC           IN OUT   VARCHAR2,
							I_TI_PALLET_NAME           IN OUT   VARCHAR2,
							I_TI_PALLET_SIZE           IN OUT   NUMBER,
							I_TI_STANDARD_UOM          IN OUT   VARCHAR2,
							I_TI_STORE_ORD_MULT        IN OUT   VARCHAR2,
							I_TI_TSF_INNER_PACK_SIZE   IN OUT   NUMBER,
							I_TI_TSF_SUPP_PACK_SIZE    IN OUT   NUMBER,
							I_TI_UOT                   IN OUT   VARCHAR2,
							I_TI_UOT_SIZE              IN OUT   NUMBER,
							I_TI_TSF_NO                IN       NUMBER,
							P_PM_FROM_LOC              IN       NUMBER)
   return BOOLEAN;
END INVENTORY_MOVEMENT_SQL;
/