CREATE OR REPLACE PACKAGE BODY TSF_BT_SQL AS

LP_user                  VARCHAR2(50)  := GET_USER;
LP_finisher_loc_ind      VARCHAR2(1) := NULL;
LP_finisher_entity_ind   VARCHAR2(1) := NULL;

---------------------------------------------------------------------------------

FUNCTION UPD_FROM_ITEM_LOC(O_error_message IN OUT VARCHAR2,
                           I_item          IN     item_master.item%TYPE,
                           I_comp_item     IN     VARCHAR2,
                           I_from_loc      IN     item_loc.loc%TYPE,
                           I_qty           IN     item_loc_soh.stock_on_hand%TYPE,
                           I_tsf_parent_no IN     tsfhead.tsf_parent_no%TYPE)
RETURN BOOLEAN IS

   L_rowid     ROWID;

   cursor C_LOCK_ILS is
      select ils.rowid
        from item_loc_soh ils
       where ils.item = I_item
         and ils.loc  = I_from_loc
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_LOC_SOH',
                    'Item:'||I_item||
                    ' From_Loc:'||to_char(I_from_loc));
   open C_LOCK_ILS;
   fetch C_LOCK_ILS into L_rowid;
   close C_LOCK_ILS;

   /* if I_comp_item is Y, assumes receive_as_type is P */
   update item_loc_soh ils
      set ils.stock_on_hand = DECODE(I_comp_item,
                                     'Y', ils.stock_on_hand,
                                     ils.stock_on_hand - I_qty),
          ils.pack_comp_soh = DECODE(I_comp_item,
                                     'Y', ils.pack_comp_soh - I_qty,
                                     ils.pack_comp_soh),
          ils.soh_update_datetime = DECODE(I_comp_item,
                                           'Y', ils.soh_update_datetime,
                                           DECODE(I_qty,
                                                  0, ils.soh_update_datetime,
                                                  SYSDATE)),
          ils.last_update_datetime = SYSDATE,
          ils.last_update_id       = LP_user
    where ils.rowid = L_rowid;

   if I_tsf_parent_no IS NOT NULL then
      update item_loc_soh ils
         set ils.tsf_reserved_qty = DECODE(I_comp_item,
                                           'Y', ils.tsf_reserved_qty,
                                           ils.tsf_reserved_qty - I_qty),
             ils.pack_comp_resv = DECODE(I_comp_item,
                                         'Y', ils.pack_comp_resv - I_qty,
                                         ils.pack_comp_resv),
             ils.last_update_datetime = SYSDATE,
             ils.last_update_id       = LP_user
       where ils.rowid = L_rowid;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TSF_BT_SQL.UPD_FROM_ITEM_LOC',
                                            to_char(SQLCODE));
   return FALSE;
END UPD_FROM_ITEM_LOC;

---------------------------------------------------------------------------------

FUNCTION UPD_TO_ITEM_LOC(O_error_message    IN OUT VARCHAR2,
                         I_item             IN     item_master.item%TYPE,
                         I_comp_item        IN     VARCHAR2,
                         I_to_loc           IN     item_loc.loc%TYPE,
                         I_qty              IN     item_loc_soh.stock_on_hand%TYPE,
                         I_av_cost          IN     item_loc_soh.av_cost%TYPE,
                         I_tsf_parent_no    IN     tsfhead.tsf_parent_no%TYPE)

RETURN BOOLEAN IS

   L_rowid     ROWID;
   --variable for Wrapper Function
   L_l10n_fin_rec          L10N_FIN_REC := L10N_FIN_REC();

   cursor C_LOCK_TO_ILS is
      select ils.rowid
        from item_loc_soh ils
       where ils.item = I_item
         and ils.loc  = I_to_loc
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_LOC_SOH',
                    'Item:'||I_item||
                    ' To_Loc:'||to_char(I_to_loc));
   open C_LOCK_TO_ILS;
   fetch C_LOCK_TO_ILS into L_rowid;
   close C_LOCK_TO_ILS;

   ---objects for L10N_FIN_REC
   L_l10n_fin_rec.procedure_key         := 'UPDATE_AV_COST';
   L_l10n_fin_rec.item                  := I_item;
   L_l10n_fin_rec.source_entity         := 'LOC';
   L_l10n_fin_rec.source_id             := I_to_loc;
   L_l10n_fin_rec.av_cost               := ROUND(I_av_cost,4);
   L_l10n_fin_rec.fin_rowid             := CHARTOROWID(L_rowid);

   /* if I_comp_item is Y, assumes receive_as_type is P */
   update item_loc_soh ils
      set ils.stock_on_hand = DECODE(I_comp_item,
                                     'Y', ils.stock_on_hand,
                                     ils.stock_on_hand + I_qty),
          ils.pack_comp_soh = DECODE(I_comp_item,
                                     'Y', ils.pack_comp_soh + I_qty,
                                     ils.pack_comp_soh),
          ils.av_cost       = ROUND(I_av_cost, 4),
          ils.soh_update_datetime = DECODE(I_comp_item,
                                           'Y', ils.soh_update_datetime,
                                           DECODE(I_qty,
                                                  0, ils.soh_update_datetime,
                                                  SYSDATE)),
          ils.last_update_datetime = SYSDATE,
          ils.last_update_id       = LP_user
    where ils.rowid = L_rowid;

   if I_tsf_parent_no IS NOT NULL then
      --if all locations on the multi-legged tsf are in the same physical wh the expected qty was
      --updated at approval for the item or pack_comp_item appropriately.
      if LP_finisher_loc_ind = 'B' then
         update item_loc_soh ils
            set ils.tsf_expected_qty = DECODE(I_comp_item,
                                              'Y', ils.tsf_expected_qty,
                                              ils.tsf_expected_qty - I_qty),
                ils.pack_comp_exp = DECODE(I_comp_item,
                                            'Y', ils.pack_comp_exp - I_qty,
                                            ils.pack_comp_exp),
                ils.last_update_datetime = SYSDATE,
                ils.last_update_id       = LP_user
          where ils.rowid = L_rowid;

      --if the finisher is located at the receiving wh the expected qty was updated
      --at the component level by stock_order_rcv
      elsif LP_finisher_loc_ind in ('R','E') then
         update item_loc_soh ils
            set ils.tsf_expected_qty = ils.tsf_expected_qty - I_qty,
                ils.last_update_datetime = SYSDATE,
                ils.last_update_id       = LP_user
          where ils.rowid = L_rowid;
      end if;
   end if; -- parent is not null

   if L10N_SQL.EXEC_FUNCTION(O_error_message,
                             L_l10n_fin_rec)= FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TSF_BT_SQL.UPD_TO_ITEM_LOC',
                                            to_char(SQLCODE));
   return FALSE;
END UPD_TO_ITEM_LOC;

---------------------------------------------------------------------------------

FUNCTION UPD_PACK_LOC(O_error_message IN OUT VARCHAR2,
                      I_item          IN     item_master.item%TYPE,
                      I_to_loc        IN     item_loc.loc%TYPE,
                      I_qty           IN     item_loc_soh.stock_on_hand%TYPE,
                      I_upd_exp_ind   IN     VARCHAR2)
RETURN BOOLEAN IS

   L_rowid     ROWID;

   cursor C_LOCK_TO_PACK_ILS is
      select ils.rowid
        from item_loc_soh ils
       where ils.item = I_item
         and ils.loc  = I_to_loc
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_LOC_SOH',
                    'Item:'||I_item||
                    ' To_Loc:'||to_char(I_to_loc));
   open C_LOCK_TO_PACK_ILS;
   fetch C_LOCK_TO_PACK_ILS into L_rowid;
   close C_LOCK_TO_PACK_ILS;

   update item_loc_soh ils
      set ils.stock_on_hand = ils.stock_on_hand + I_qty,
          ils.soh_update_datetime = DECODE(I_qty,
                                           0, ils.soh_update_datetime,
                                           SYSDATE),
          ils.last_update_datetime = SYSDATE,
          ils.last_update_id       = LP_user
    where ils.rowid = L_rowid;

   if LP_finisher_loc_ind = 'B' and I_upd_exp_ind = 'Y' then
      update item_loc_soh ils
         set ils.tsf_expected_qty = ils.tsf_expected_qty - I_qty,
             ils.last_update_datetime = SYSDATE,
             ils.last_update_id       = LP_user
       where ils.rowid = L_rowid;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TSF_BT_SQL.UPD_PACK_LOC',
                                            to_char(SQLCODE));
   return FALSE;
END UPD_PACK_LOC;

---------------------------------------------------------------------------------

FUNCTION INV_STATUS_UPD(O_error_message IN OUT VARCHAR2,
                        I_item          IN     ITEM_MASTER.ITEM%TYPE,
                        I_inv_status    IN     TSFDETAIL.INV_STATUS%TYPE,
                        I_loc           IN     ITEM_LOC.LOC%TYPE,
                        I_loc_type      IN     ITEM_LOC.LOC_TYPE%TYPE,
                        I_qty           IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        I_tran_date     IN     TSFHEAD.CREATE_DATE%TYPE)
RETURN BOOLEAN IS

   L_found     BOOLEAN;
   L_pgm_name  VARCHAR2(255) := 'TSF_BT_SQL.INV_STATUS_UPD';
   L_vdate     DATE := GET_VDATE;
   L_tran_code tran_data.tran_code%TYPE := 25;

BEGIN

   if INVADJ_SQL.ADJ_UNAVAILABLE(I_item,
                                 I_inv_status,
                                 I_loc_type,
                                 I_loc,
                                 I_qty,
                                 O_error_message,
                                 L_found) = FALSE then
      return FALSE;
   end if;

   if INVADJ_SQL.ADJ_TRAN_DATA(I_item,
                               I_loc_type,
                               I_loc,
                               I_qty,
                               L_pgm_name,
                               NVL(I_tran_date, L_vdate),
                               L_tran_code,
                               NULL,
                               I_inv_status,
                               NULL, -- wac
                               NULL, -- unit retail
                               O_error_message,
                               L_found) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_pgm_name,
                                            to_char(SQLCODE));
   return FALSE;
END INV_STATUS_UPD;
---------------------------------------------------------------------------------
FUNCTION UPD_TSFDETAIL(O_error_message IN OUT VARCHAR2,
                      I_tsf_no         IN     tsfhead.tsf_no%TYPE,
                      I_item           IN     item_master.item%TYPE,
                      I_qty            IN     item_loc_soh.stock_on_hand%TYPE)

RETURN BOOLEAN IS

   L_rowid     ROWID;

   cursor C_LOCK_TSFDETAIL is
      select rowid
        from tsfdetail
       where tsf_no = I_tsf_no
         and item  = I_item
         for update nowait;

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_tsf_no',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_item',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;

   if I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_qty',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE',NULL,'TSFDETAIL',
                    'tsf_no:'||to_char(I_tsf_no)||
                    'item:'||I_item);
   open C_LOCK_TSFDETAIL;
   fetch C_LOCK_TSFDETAIL into L_rowid;
   close C_LOCK_TSFDETAIL;

   update tsfdetail
      set ship_qty = nvl(ship_qty, 0) + I_qty,
          received_qty = nvl(received_qty,0) + I_qty,
          updated_by_rms_ind = 'Y'
    where rowid = L_rowid;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TSF_BT_SQL.UPD_TSFDETAIL',
                                            to_char(SQLCODE));
   return FALSE;
END UPD_TSFDETAIL;
---------------------------------------------------------------------------------
FUNCTION BT_EXECUTE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item            IN       ITEM_MASTER.ITEM%TYPE,
                    I_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                    I_dept            IN       ITEM_MASTER.DEPT%TYPE,
                    I_class           IN       ITEM_MASTER.CLASS%TYPE,
                    I_subclass        IN       ITEM_MASTER.SUBCLASS%TYPE,
                    I_from_loc        IN       ITEM_LOC.LOC%TYPE,
                    I_to_loc          IN       ITEM_LOC.LOC%TYPE,
                    I_tsf_qty         IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                    I_inv_status      IN       TSFDETAIL.INV_STATUS%TYPE,
                    I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE,
                    I_tran_date       IN       TSFHEAD.CREATE_DATE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   -- This holds the from location's wac based on the accounting method.
   L_from_wac                   item_loc_soh.av_cost%TYPE;
   L_tsf_no                     tsfhead.tsf_no%TYPE;
   L_tsf_parent_no              tsfhead.tsf_parent_no%TYPE;
   L_tsf_type                   tsfhead.tsf_type%TYPE;
   L_call_type                  VARCHAR2(1) := 'T';

   L_from_loc_comp_item         VARCHAR2(1) := 'Y';
   L_to_loc_comp_item           VARCHAR2(1) := 'Y';
   L_intercompany               BOOLEAN := FALSE;
   L_upd_exp_ind                VARCHAR2(1) := 'N';

   L_pct_in_pack                NUMBER := 0;
   L_new_wac                    item_loc_soh.unit_cost%TYPE;

   L_from_loc_type              item_loc.loc_type%TYPE;
   L_from_finisher_ind          wh.finisher_ind%TYPE := 'N';
   L_to_loc_type                item_loc.loc_type%TYPE;
   L_to_finisher_ind            wh.finisher_ind%TYPE := 'N';
   L_inventory_ind              item_master.inventory_ind%TYPE := 'Y';
   L_finisher                   BOOLEAN := FALSE;
   L_dummy_name                 wh.wh_name%TYPE;

   -- dummy
   L_tsf_unit_cost              item_loc_soh.av_cost%TYPE;

  -- cursors
   cursor C_ITEMS_IN_PACK is
      select vpq.item item,
             vpq.qty qty,
             im.dept dept,
             im.class class,
             im.subclass subclass
        from v_packsku_qty vpq,
             item_master im
       where vpq.pack_no      = I_item
         and im.item          = vpq.item
         and im.inventory_ind = L_inventory_ind;

   cursor C_GET_TSFHEAD_INFO is
      select tsf_parent_no,
             tsf_type,
             tsf_no
        from tsfhead
       where tsf_no = I_tsf_no;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_item',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;
   if I_pack_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_pack_ind',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_dept',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;
   if I_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_class',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;
   if I_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_subclass',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;
   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_from_loc',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;
   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_to_loc',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;
   if I_tsf_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_tsf_qty',
                                            'NULL','NOT NULL');
      return FALSE;
   end if;
   
   --get to_loc type
   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_to_loc_type,
                                   I_to_loc) = FALSE then
      return FALSE;
   end if;
   -- ensure item exist at to loc
   if NEW_ITEM_LOC(O_error_message,
                   I_item,
                   I_to_loc,
                   NULL, NULL, L_to_loc_type, NULL,
                   I_dept, I_class, I_subclass,
                   NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL) = FALSE then
      return FALSE;
   end if;

   if I_tsf_no is not NULL then
      open C_GET_TSFHEAD_INFO;
      fetch C_GET_TSFHEAD_INFO into L_tsf_parent_no,
                                    L_tsf_type,
                                    L_tsf_no;
      close C_GET_TSFHEAD_INFO;
   end if;

   --get from_loc type
   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_from_loc_type,
                                   I_from_loc) = FALSE then
      return FALSE;
   end if;

   --get from_loc finisher
   if L_from_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                      L_finisher,
                                      L_dummy_name,
                                      I_from_loc) = FALSE then
         return FALSE;
      end if;
   end if;

   --determine if from loc is a finisher, internal or 'E'xternal
   if L_finisher or L_from_loc_type = 'E' then
      L_from_finisher_ind := 'Y';
   end if;

   --get to_loc finisher
   --determine if wh is internal finisher
   if L_to_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                      L_finisher,
                                      L_dummy_name,
                                      I_to_loc) = FALSE then
         return FALSE;
      end if;
   end if;

   --determine if to loc is a finisher, internal or 'E'xternal
   if L_finisher or L_to_loc_type = 'E' then
      L_to_finisher_ind := 'Y';
   end if;

   if L_tsf_parent_no is NULL and I_tsf_no is not NULL then  --1st leg tsf
      if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                        LP_finisher_loc_ind,
                                        LP_finisher_entity_ind,
                                        I_tsf_no) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_tsf_parent_no is not NULL then --2nd leg tsf
      if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                        LP_finisher_loc_ind,
                                        LP_finisher_entity_ind,
                                        L_tsf_parent_no) = FALSE then
         return FALSE;
      end if;
      L_upd_exp_ind := 'Y';
   end if;

   if L_to_finisher_ind = 'Y' then  --1st leg
      --for 1st leg tsf, 'to' loc is a finisher, I_comp_item is set to 'N' because packs
      --are exploded at finishers (pack comp items soh and exp_qty are updated)
      L_to_loc_comp_item := 'N';
   elsif L_from_finisher_ind = 'Y' then  --2nd leg
      --for 2nd leg tsf, 'from' loc is finisher, I_comp_item is set to 'N' because packs
      --are exploded at finishers (pack comp items soh and resv_qty are updated)
      L_from_loc_comp_item := 'N';
   elsif L_tsf_type = 'BT' or I_tsf_no is NULL then
      L_call_type := 'B';
   end if;

   -- to handle externally generated book transfers
   -- for customer order return to the warehouse
   if L_from_loc_type = 'S' and L_to_loc_type = 'W' then
      L_from_loc_comp_item := 'N';
   end if;

   --determine inter vs intra company transfer
   if TRANSFER_SQL.IS_INTERCOMPANY(O_error_message,
                                   L_intercompany,
                                   L_call_type,  -- distro type
                                   L_tsf_type, -- transfer type
                                   I_from_loc,
                                   L_from_loc_type,
                                   I_to_loc,
                                   L_to_loc_type) = FALSE then
      return FALSE;
   end if;

   if I_pack_ind = 'Y' then
      if L_from_loc_comp_item = 'Y' then
         if UPD_PACK_LOC(O_error_message,
                         I_item,
                         I_from_loc,
                         I_tsf_qty * -1,
                         'N') = FALSE then  --No need to update 'from' loc's exp_qty
            return FALSE;
         end if;
      end if;
      if L_to_loc_comp_item = 'Y' then
         if UPD_PACK_LOC(O_error_message,
                         I_item,
                         I_to_loc,
                         I_tsf_qty,
                         L_upd_exp_ind) = FALSE then  --update exp_qty only if loc is final loc
            return FALSE;
         end if;
      end if;

      /* if pack, assume receive_as_type is P */
      if I_inv_status IS NOT NULL and I_inv_status != -1 then
         if INV_STATUS_UPD(O_error_message,
                           I_item,
                           I_inv_status,
                           I_from_loc,
                           L_from_loc_type,
                           I_tsf_qty * -1,
                           I_tran_date) = FALSE then
            return FALSE;
         end if;
         -- Do not update non_sellable_qty/inventory_status_qty if transferred to a finisher
         if L_to_finisher_ind = 'N' then
            if INV_STATUS_UPD(O_error_message,
                              I_item,
                              I_inv_status,
                              I_to_loc,
                              L_to_loc_type,
                              I_tsf_qty,
                              I_tran_date) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;

      FOR rec IN C_ITEMS_IN_PACK LOOP
         if UPD_FROM_ITEM_LOC(O_error_message,
                              rec.item,
                              L_from_loc_comp_item,
                              I_from_loc,
                              rec.qty * I_tsf_qty,
                              L_tsf_parent_no) = FALSE then
            return FALSE;
         end if;

         if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                       L_from_wac,
                                       rec.item,
                                       rec.dept,
                                       rec.class,
                                       rec.subclass,
                                       I_from_loc,
                                       L_from_loc_type,
                                       I_tran_date,
                                       I_to_loc,
                                       L_to_loc_type) = FALSE then
            return FALSE;
         end if;

         if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                          L_pct_in_pack,
                                          I_item,
                                          rec.item,
                                          I_from_loc) = FALSE then
            return FALSE;
         end if;

         if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                         L_new_wac,
                                         I_tsf_no,
                                         L_call_type,
                                         rec.item,
                                         I_item,
                                         L_pct_in_pack,
                                         I_from_loc,
                                         L_from_loc_type,
                                         I_to_loc,
                                         L_to_loc_type,
                                         rec.qty * I_tsf_qty,
                                         NULL,        -- weight
                                         L_from_wac,
                                         0,
                                         L_intercompany) = FALSE then
            return FALSE;
         end if;

         if UPD_TO_ITEM_LOC(O_error_message,
                            rec.item,
                            L_to_loc_comp_item,
                            I_to_loc,
                            rec.qty * I_tsf_qty,
                            L_new_wac,
                            L_tsf_parent_no) = FALSE then
            return FALSE;
         end if;

         --increment tsf_item_cost.shipped_qty if it is intercompany tsf
         --and going to the final loc
         if L_tsf_type = 'IC' and L_to_finisher_ind = 'N' then
            update tsf_item_cost
               set shipped_qty = nvl(shipped_qty,0) + rec.qty * I_tsf_qty
             where tsf_no = I_tsf_no
               and item = rec.item;
         end if;

         if STKLEDGR_SQL.WRITE_FINANCIALS(O_error_message,
                                          L_tsf_unit_cost,  -- dummy
                                          L_call_type,
                                          NULL, --shipment
                                          I_tsf_no,
                                          I_tran_date,
                                          rec.item,
                                          I_item,  --pack_no
                                          L_pct_in_pack,  --pct_in_pack
                                          I_dept,
                                          I_class,
                                          I_subclass,
                                          I_tsf_qty * rec.qty,
                                          NULL,   -- weight
                                          I_from_loc,
                                          L_from_loc_type,
                                          L_from_finisher_ind, --'Y' or 'N'
                                          I_to_loc,
                                          L_to_loc_type,
                                          L_to_finisher_ind,
                                          L_from_wac,
                                          NULL,
                                          NULL,
                                          L_intercompany) = FALSE then
            return FALSE;
         end if;
      END LOOP;

   else -- item is not a pack

      if UPD_FROM_ITEM_LOC(O_error_message,
                           I_item,
                           'N',        -- comp item
                           I_from_loc,
                           I_tsf_qty,
                           L_tsf_parent_no) = FALSE then
         return FALSE;
      end if;
      if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                    L_from_wac,
                                    I_item,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    I_from_loc,
                                    L_from_loc_type,
                                    I_tran_date,
                                    I_to_loc,
                                    L_to_loc_type) = FALSE then
         return FALSE;
      end if;
      if TRANSFER_COST_SQL.RECALC_WAC(O_error_message,
                                      L_new_wac,
                                      I_tsf_no,
                                      L_call_type,
                                      I_item,
                                      NULL,  --pack_no
                                      NULL,  --percent in pack
                                      I_from_loc,
                                      L_from_loc_type,
                                      I_to_loc,
                                      L_to_loc_type,
                                      I_tsf_qty,
                                      NULL,  -- weight
                                      L_from_wac,
                                      0,
                                      L_intercompany) = FALSE then
         return FALSE;
      end if;

      if UPD_TO_ITEM_LOC(O_error_message,
                         I_item,
                         'N',        -- comp item
                         I_to_loc,
                         I_tsf_qty,
                         L_new_wac,
                         L_tsf_parent_no) = FALSE then
         return FALSE;
      end if;

      if I_inv_status IS NOT NULL and I_inv_status != -1 then
         if INV_STATUS_UPD(O_error_message,
                           I_item,
                           I_inv_status,
                           I_from_loc,
                           L_from_loc_type,
                           I_tsf_qty * -1,
                           I_tran_date) = FALSE then
            return FALSE;
         end if;
         -- Do not update non_sellable_qty/inventory_status_qty if transferred to a finisher
         if L_to_finisher_ind = 'N' then
            if INV_STATUS_UPD(O_error_message,
                              I_item,
                              I_inv_status,
                              I_to_loc,
                              L_to_loc_type,
                              I_tsf_qty,
                              I_tran_date) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;

      --increment tsf_item_cost.shipped_qty if it is intercompany tsf
      --and going to the final loc
      if L_tsf_type = 'IC' and L_to_finisher_ind = 'N' then
         update tsf_item_cost
            set shipped_qty = nvl(shipped_qty,0) + I_tsf_qty
          where tsf_no = I_tsf_no
            and item = I_item;
      end if;

      if STKLEDGR_SQL.WRITE_FINANCIALS(O_error_message,
                                       L_tsf_unit_cost,  -- dummy
                                       L_call_type,
                                       NULL, --shipment
                                       I_tsf_no,
                                       I_tran_date,
                                       I_item,
                                       NULL,  --pack_no
                                       NULL,  --pct_in_pack
                                       I_dept,
                                       I_class,
                                       I_subclass,
                                       I_tsf_qty,
                                       NULL,  -- weight
                                       I_from_loc,
                                       L_from_loc_type,
                                       L_from_finisher_ind,
                                       I_to_loc,
                                       L_to_loc_type,
                                       L_to_finisher_ind,
                                       L_from_wac,
                                       NULL,
                                       NULL,
                                       L_intercompany) = FALSE then
         return FALSE;
      end if;
   end if;  --I_item is pack or not

   --update ship_qty and received_qty
   if I_tsf_no is not NULL then
      if UPD_TSFDETAIL(O_error_message,
                       I_tsf_no,
                       I_item,
                       I_tsf_qty) = FALSE then
         return FALSE;
      end if;

      if LP_finisher_loc_ind is NOT NULL then
         --update finisher_av_retail and finisher_units in item_loc_soh and tsfdetail
         if BOL_SQL.PUT_ILS_AV_RETAIL(O_error_message,
                                      I_to_loc,
                                      L_to_loc_type,
                                      I_item,
                                      NULL,
                                      I_tsf_no,
                                      L_tsf_type,
                                      I_tsf_qty)  = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TSF_BT_SQL.BT_EXECUTE',
                                            to_char(SQLCODE));
   return FALSE;
END BT_EXECUTE;
---------------------------------------------------------------------------------
FUNCTION CREATE_BOOK_TSF_CO_RTN(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_book_tsf_co_rtn_tbl   IN OUT   OBJ_BOOK_TSF_CO_RTN_TBL)
RETURN BOOLEAN IS

   L_program               VARCHAR2(64)             := 'TSF_BT_SQL.CREATE_BOOK_TSF_CO_RTN';
   L_book_tsf_co_rtn_tbl   OBJ_BOOK_TSF_CO_RTN_TBL;
   L_tsf_no                TSFHEAD.TSF_NO%TYPE;
   L_return_code           VARCHAR2(5);
   L_vwh                   WH.WH%TYPE;
   L_user                  VARCHAR2(30)             := GET_USER;
   L_vdate                 DATE                     := GET_VDATE;
   L_itemloc_exists        BOOLEAN;
   L_exists    VARCHAR2(1) ;  

   cursor C_VALIDATE_RECORDS is
      select *
        from (select case
                        when input.I_item is NULL then
                           SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_book_tsf_co_rtn_tbl.I_item', L_program, NULL)
                        -- validate if item is transaction level
                        when im.item is NULL or im.item_level <> im.tran_level then
                           SQL_LIB.CREATE_MSG('MUST_BE_TRAN_LEVEL', input.I_item, NULL, NULL)
                        when input.I_from_loc_type <> 'S' or input.I_from_loc_type is NULL then
                           SQL_LIB.CREATE_MSG('INV_BT_FROM_LOC_TYPE', input.I_item, NULL, NULL)
                        when input.I_to_loc_type <> 'W' or input.I_to_loc_type is NULL then
                           SQL_LIB.CREATE_MSG('INV_BT_TO_LOC_TYPE', input.I_item, NULL, NULL)
                        -- validate if from loc is a virtual store
                        when st.store is NULL or st.store_type <> 'C' or st.stockholding_ind <> 'N' then
                           SQL_LIB.CREATE_MSG('FROM_LOC_VIRTUAL_STORE', input.I_item, NULL, NULL)
                        -- validate if to loc is NULL
                        when wh.wh is NULL then
                           SQL_LIB.CREATE_MSG('INV_WH', input.I_item, L_program, NULL)
                        when input.I_return_without_inventory_ind not in ('Y','N') or input.I_return_without_inventory_ind is NULL then
                           SQL_LIB.CREATE_MSG('INV_RETURN_WO_IND', input.I_item, NULL, NULL)
                        -- validate that return disposition is NULL if inventory does not exist
                        when input.I_return_without_inventory_ind = 'Y' and input.I_return_disposition is NOT NULL then
                           SQL_LIB.CREATE_MSG('INV_RETURN_DISP_COMB_Y', input.I_item, NULL, NULL)
                        -- validate that return disposition is defined if inventory exists
                        when input.I_return_without_inventory_ind = 'N' and (input.I_return_disposition is NULL or isc.inv_status_code is NULL) then
                           SQL_LIB.CREATE_MSG('INV_RETURN_DISP_COMB_N', input.I_item, NULL, NULL)
                     end as error_msg
                 from TABLE(CAST(I_book_tsf_co_rtn_tbl AS OBJ_BOOK_TSF_CO_RTN_TBL)) input,
                      item_master im,
                      store st,
                      wh,
                      inv_status_codes isc
                where input.I_item = im.item(+)
                  and input.I_from_loc = st.store(+)
                  and input.I_to_loc = wh.wh(+)
                  and input.I_return_disposition = isc.inv_status_code(+))
       where error_msg is NOT NULL
         and rownum = 1;

   cursor C_BOOK_TSF_RECS is
      select OBJ_BOOK_TSF_CO_RTN_REC(input.I_from_loc,
                                     input.I_from_loc_type,
                                     input.I_to_loc,
                                     input.I_to_loc_type,
                                     input.I_item,
                                     input.I_return_qty,
                                     input.I_return_disposition,
                                     input.I_return_without_inventory_ind,
                                     input.I_tran_date,
                                     input.tsf_no,
                                     im.dept,
                                     im.class,
                                     im.subclass,
                                     im.pack_ind,
                                     isc.supp_pack_size,
                                     case
                                        when input.I_return_disposition is NOT NULL then
                                           (select inv_status
                                              from inv_status_codes
                                             where inv_status_code = input.I_return_disposition)
                                     end, --inv_status for the return disposition
                                     input.virtual_wh)
        from TABLE(CAST(I_book_tsf_co_rtn_tbl AS OBJ_BOOK_TSF_CO_RTN_TBL)) input,
             item_master im,
             item_supp_country isc
       where im.item = input.I_item
         and im.item = isc.item
         and isc.primary_supp_ind = 'Y'
         and isc.primary_country_ind = 'Y';
		 
   cursor C_CHECK_PHY_WH(p_wh WH.WH%TYPE) is
      select 'x' 
        from wh 
       where wh = p_wh 
         and wh = physical_wh;

BEGIN

   if I_book_tsf_co_rtn_tbl is NULL or I_book_tsf_co_rtn_tbl.count <= 0 then
      return TRUE;
   end if;

   open C_VALIDATE_RECORDS;
   fetch C_VALIDATE_RECORDS into O_error_message;
   close C_VALIDATE_RECORDS;

   if O_error_message is NOT NULL then
      return FALSE;
   end if;
   ---
   -- process the records after successful validation
   open C_BOOK_TSF_RECS;
   fetch C_BOOK_TSF_RECS BULK COLLECT into L_book_tsf_co_rtn_tbl;
   close C_BOOK_TSF_RECS;

   FOR i in 1..L_book_tsf_co_rtn_tbl.count LOOP
      NEXT_TRANSFER_NUMBER(L_tsf_no,
                           L_return_code,
                           O_error_message);

      if L_return_code = 'FALSE' then
         return FALSE;
      else
         L_book_tsf_co_rtn_tbl(i).tsf_no := L_tsf_no;
      end if;

      -- check if item exists at from loc
      if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                           L_book_tsf_co_rtn_tbl(i).I_item,
                                           L_book_tsf_co_rtn_tbl(i).I_from_loc,
                                           L_itemloc_exists) = FALSE then
         return FALSE;
      end if;

      if L_itemloc_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_LOC_REL_ITEM_LOC',
                                               L_book_tsf_co_rtn_tbl(i).I_item,
                                               L_book_tsf_co_rtn_tbl(i).I_from_loc,
                                               NULL);
         return FALSE;
      end if;

	  open C_CHECK_PHY_WH(L_book_tsf_co_rtn_tbl(i).I_to_loc);
      fetch C_CHECK_PHY_WH into L_exists;      
	  
	  if C_CHECK_PHY_WH%FOUND then
         if DISTRIBUTION_SQL.FIND_MAPPING_VWH(O_error_message,
                                              L_vwh,
                                              L_book_tsf_co_rtn_tbl(i).I_to_loc,
                                              'T',   --to_from_ind
                                              L_book_tsf_co_rtn_tbl(i).I_from_loc,
                                              L_book_tsf_co_rtn_tbl(i).I_from_loc_type,
                                              L_book_tsf_co_rtn_tbl(i).I_item,
                                              'Y') = FALSE then   --customer_order_loc_ind
            return FALSE;
         else
            L_book_tsf_co_rtn_tbl(i).virtual_wh := L_vwh;
         end if;
	  else
	     L_book_tsf_co_rtn_tbl(i).virtual_wh := L_book_tsf_co_rtn_tbl(i).I_to_loc;
	  end if;
	  close C_CHECK_PHY_WH;
   END LOOP;


   FORALL i in L_book_tsf_co_rtn_tbl.FIRST..L_book_tsf_co_rtn_tbl.LAST
      insert into tsfhead (tsf_no,
                           from_loc_type,
                           from_loc,
                           to_loc_type,
                           to_loc,
                           inventory_type,
                           tsf_type,
                           status,
                           freight_code,
                           create_date,
                           create_id,
                           close_date,
                           repl_tsf_approve_ind)
                   values (L_book_tsf_co_rtn_tbl(i).tsf_no,
                           L_book_tsf_co_rtn_tbl(i).I_from_loc_type,
                           L_book_tsf_co_rtn_tbl(i).I_from_loc,
                           L_book_tsf_co_rtn_tbl(i).I_to_loc_type,
                           L_book_tsf_co_rtn_tbl(i).virtual_wh,
                           DECODE(L_book_tsf_co_rtn_tbl(i).inv_status, 1, 'U', 'A'),
                           'BT',
                           'C',
                           'N',
                           NVL(L_book_tsf_co_rtn_tbl(i).I_tran_date, L_vdate),
                           L_user,
                           NVL(L_book_tsf_co_rtn_tbl(i).I_tran_date, L_vdate),
                           'N');

   FORALL i in L_book_tsf_co_rtn_tbl.FIRST..L_book_tsf_co_rtn_tbl.LAST
      insert into tsfdetail (tsf_no,
                             tsf_seq_no,
                             item,
                             inv_status,
                             tsf_qty,
                             supp_pack_size,
                             publish_ind,
                             updated_by_rms_ind)
                     values (L_book_tsf_co_rtn_tbl(i).tsf_no,
                             1,
                             L_book_tsf_co_rtn_tbl(i).I_item,
                             L_book_tsf_co_rtn_tbl(i).inv_status,
                             L_book_tsf_co_rtn_tbl(i).I_return_qty,
                             L_book_tsf_co_rtn_tbl(i).supp_pack_size,
                             'N',
                             'N');

   FOR i in 1..L_book_tsf_co_rtn_tbl.count LOOP
      if TSF_BT_SQL.BT_EXECUTE(O_error_message,
                               L_book_tsf_co_rtn_tbl(i).I_item,
                               L_book_tsf_co_rtn_tbl(i).pack_ind,
                               L_book_tsf_co_rtn_tbl(i).dept,
                               L_book_tsf_co_rtn_tbl(i).class,
                               L_book_tsf_co_rtn_tbl(i).subclass,
                               L_book_tsf_co_rtn_tbl(i).I_from_loc,
                               L_book_tsf_co_rtn_tbl(i).virtual_wh,
                               L_book_tsf_co_rtn_tbl(i).I_return_qty,
                               L_book_tsf_co_rtn_tbl(i).inv_status,
                               L_book_tsf_co_rtn_tbl(i).tsf_no,
                               L_book_tsf_co_rtn_tbl(i).I_tran_date) = FALSE then
         return FALSE;
      end if;

      if L_book_tsf_co_rtn_tbl(i).I_return_without_inventory_ind = 'Y' then
         if INVADJ_SQL.ADJ_STOCK(O_error_message,
                                 L_book_tsf_co_rtn_tbl(i).I_item,
                                 L_book_tsf_co_rtn_tbl(i).virtual_wh,
                                 'W',
                                 -1 * L_book_tsf_co_rtn_tbl(i).I_return_qty,
                                 191,   --inv adj reason code
                                 NULL,
                                 NULL,
                                 L_user,
                                 NVL(L_book_tsf_co_rtn_tbl(i).I_tran_date, L_vdate)) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CREATE_BOOK_TSF_CO_RTN;
-------------------------------------------------------------------------------
END;
/
