CREATE OR REPLACE PACKAGE MRT_ATTRIB_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Filename: mrtattrs.pls
--
-- Purpose: This package generatese all attributes of the Transfer of mass 
--          return to vendor transactions based from tables MRT, MRT_ITEM and 
--          MRT_ITEM_LOC. Package is used by the main Mrt form.
--          Author:  Raymond A. Sumbeling, (Accenture - ERC)
-------------------------------------------------------------------------------
-- Function Name: MRT_ITEM_LIST
-- Purpose:      This function will be used to determine whether a user can 
--               see all the mrt items available based on their security 
--               settings. 
-------------------------------------------------------------------------------
FUNCTION MRT_ITEM_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_diff          IN OUT VARCHAR2,
                       L_mrt_no        IN     MRT_ITEM.MRT_NO%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: GET_MRT_INFO
-- Purpose:       This function shall return all the attributes of the new 
--                table MRT by matching the MRT_NO column with the 
--                I_mrt_no parameter.
-------------------------------------------------------------------------------
FUNCTION GET_MRT_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_mrt_rec       IN OUT MRT%ROWTYPE,
                      O_exists        IN OUT BOOLEAN,
                      L_mrt_no        IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_MRT_ITEM_ROW
-- Purpose      : Returns a row from MRT_ITEM for a given MRT_NO and ITEM.
-------------------------------------------------------------------------------
FUNCTION GET_MRT_ITEM_ROW (O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           O_mrt_item      IN OUT MRT_ITEM%ROWTYPE,
                           I_mrt_no        IN     MRT_ITEM.MRT_NO%TYPE,
                           I_item          IN     MRT_ITEM.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_MRT_ITEM_LOC_INFO
-- Purpose:       This function shall return all the attributes of the table
--                MRT_ITEM_LOC by matching the MRT_NO, ITEM and LOCATION 
--                columns against the values of I_mrt_no, I_mrt_item and 
--                I_location  parameters, respectively.
-------------------------------------------------------------------------------
FUNCTION GET_MRT_ITEM_LOC_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_mrt_rec       IN OUT MRT_ITEM_LOC%ROWTYPE,
                               O_exists        IN OUT BOOLEAN,
                               I_mrt_no        IN     MRT_ITEM_LOC.MRT_NO%TYPE,
                               I_item          IN     MRT_ITEM_LOC.ITEM%TYPE,
                               I_location      IN     MRT_ITEM_LOC.LOCATION%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_MRT_ITEM_TOTALS
-- Purpose:       This new function will sum up and return the attributes 
--                across all locations for the given Mass return transfer and 
--                item from the MRT_ITEM_LOC table.
-------------------------------------------------------------------------------
FUNCTION GET_MRT_ITEM_TOTALS
   (O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_total_return_avail_qty IN OUT MRT_ITEM_LOC.RETURN_AVAIL_QTY%TYPE,
    O_total_tsf_qty          IN OUT MRT_ITEM_LOC.TSF_QTY%TYPE,
    O_total_retail           IN OUT MRT_ITEM_LOC.UNIT_RETAIL%TYPE,
    O_total_received_qty     IN OUT MRT_ITEM_LOC.RECEIVED_QTY%TYPE,
    O_total_cost             IN OUT MRT_ITEM_LOC.UNIT_COST%TYPE,
    I_mrt_no                 IN     MRT_ITEM_LOC.MRT_NO%TYPE,
    I_item                   IN     MRT_ITEM_LOC.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: MRT_ITEM_TOTALS_FILTER_LIST
-- Purpose  checks to see if the Number of records in V_MRT_ITEM_TOTAL 
-- is different then what's in MRT_ITEM for a specific transfer.
-- If it is, it'll return Y, else it'll return N.
-------------------------------------------------------------------------------
FUNCTION MRT_ITEM_TOTALS_FILTER_LIST
   (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_diff          IN OUT VARCHAR2,
    I_mrt_no        IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: MRT_ITEM_LOC_EXISTS
-- Purpose:       This new function will check to see if record exists in 
--                MRT_ITEM_LOC table with the input MRT number
-------------------------------------------------------------------------------
FUNCTION MRT_ITEM_LOC_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_rec_exists     IN OUT  BOOLEAN,
                             I_mrt_no         IN      MRT_ITEM_LOC.MRT_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_RTV_INFO_FOR_MRT
-- Purpose:       This function shall return all the attributes of the table 
--                RTV_HEAD by matching for an MRT_NO 
-------------------------------------------------------------------------------
FUNCTION GET_RTV_INFO_FOR_MRT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_rtv_rec       IN OUT RTV_HEAD%ROWTYPE,
                              O_exists        IN OUT BOOLEAN,
                              I_mrt_no        IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_TSF_INFO_FOR_MRT
-- Purpose:       This function shall return all the attributes of the table 
--                TSFHEAD by matching for an MRT_NO 
-------------------------------------------------------------------------------
FUNCTION GET_TSF_INFO_FOR_MRT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_tsf_rec       IN OUT TSFHEAD%ROWTYPE,
                              O_exists        IN OUT BOOLEAN,
                              I_mrt_no        IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_MRT_ITEM_INFO_FOR_MRT
-- Purpose:       This function shall return all the attributes of the table 
--                MRT_ITEM for a given MRT_NO 
-------------------------------------------------------------------------------
FUNCTION GET_MRT_ITEM_INFO_FOR_MRT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_mrt_item_rec  IN OUT MRT_ITEM%ROWTYPE,
                                   O_exists        IN OUT BOOLEAN,
                                   I_mrt_no        IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_MRT_TSF_SHIPPED_STATUS
-- Purpose:       This function shall check if at least one transfer for an MRT 
--                is in shipped status. A check used in mrt.fmb. For any single 
--                mrt, no transfers can be in shipped status when the user 
--                tries to to close the mrt. 
-------------------------------------------------------------------------------
FUNCTION CHECK_MRT_TSF_SHIPPED_STATUS (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_shipped_status IN OUT BOOLEAN,
                                       I_mrt_no         IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_MRT_ITEM_LOCATIONS
-- Purpose      : Checks locations already exist in MRT_ITEM_LOC table
----------------------------------------------------------------------------------
FUNCTION CHECK_MRT_ITEM_LOCATIONS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_check             IN OUT   VARCHAR2,
                                   I_item_apply_type   IN       VARCHAR2,
                                   I_group_loc_type    IN       MRT_ITEM_LOC.LOC_TYPE%TYPE,
                                   I_group_loc_value   IN       VARCHAR2,
                                   I_mrt_no            IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                                   I_item              IN       MRT_ITEM_LOC.ITEM%TYPE,
                                   I_inter_intra_ind   IN       VARCHAR2,
                                   I_tsf_entity_id     IN       V_TRANSFER_FROM_LOC.TSF_ENTITY_ID%TYPE,
                                   I_set_of_books_id   IN       TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE)

   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CHECK_MRT_ITEM_LOCATIONS
-- Purpose      : Checks if any of the locations are localized
----------------------------------------------------------------------------------
FUNCTION CHECK_MRT_LOC_LOCALIZED (O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,    
                                  O_localized     IN OUT  VARCHAR2,
                                  O_country_id       OUT  COUNTRY_ATTRIB.COUNTRY_ID%TYPE, 
                                  I_mrt_no        IN      MRT_ITEM_LOC.MRT_NO%TYPE)

   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_WH_TOTALS
-- Purpose      : Calculate the total cost/total price and retail for the warehouse
----------------------------------------------------------------------------------
FUNCTION GET_WH_TOTALS(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_error_flag                    IN OUT   VARCHAR2,
                       O_item                          IN OUT   MRT_ITEM.ITEM%TYPE,
                       IO_budget_return_qty            IN OUT   NUMBER,
                       IO_received_qty                 IN OUT   NUMBER,
                       IO_tsf_qty                      IN OUT   MRT_ITEM_LOC.TSF_QTY%TYPE,
                       IO_total_retail                 IN OUT   NUMBER,
                       IO_supplier_total_unit_retail   IN OUT   NUMBER,
                       IO_primary_total_unit_retail    IN OUT   NUMBER,
                       IO_total_cost                   IN OUT   NUMBER,
                       IO_total_price                  IN OUT   NUMBER,
                       IO_supplier_total_cost          IN OUT   NUMBER,
                       IO_primary_total_cost           IN OUT   NUMBER,
                       IO_supplier_total_price         IN OUT   NUMBER,
                       IO_primary_total_price          IN OUT   NUMBER,
                       IO_tot_tsf_qty                  IN OUT   NUMBER,
                       IO_tot_total_retail             IN OUT   NUMBER,
                       IO_tot_supp_total_unit_retail   IN OUT   NUMBER, 
                       IO_tot_pri_total_unit_retail    IN OUT   NUMBER,
                       IO_tot_total_cost               IN OUT   NUMBER,
                       IO_tot_supplier_total_cost      IN OUT   NUMBER,
                       IO_tot_primary_total_cost       IN OUT   NUMBER,
                       IO_tot_total_price              IN OUT   NUMBER,
                       IO_tot_supplier_total_price     IN OUT   NUMBER,
                       IO_tot_primary_total_price      IN OUT   NUMBER,
                       I_item                          IN       MRT_ITEM.ITEM%TYPE,
                       I_include_wh_inv                IN       MRT.INCLUDE_WH_INV%TYPE,
                       I_inventory_type                IN       MRT.INVENTORY_TYPE%TYPE,
                       I_wh                            IN       MRT.WH%TYPE,
                       I_supplier                      IN       MRT.SUPPLIER%TYPE,
                       I_rtv_cost                      IN       MRT_ITEM.RTV_COST%TYPE,
                       I_currency_code                 IN       MRT.CURRENCY_CODE%TYPE,
                       I_sups_currency_code            IN       MRT.CURRENCY_CODE%TYPE,
                       I_prim_currency_code            IN       MRT.CURRENCY_CODE%TYPE,
                       I_restock_pct                   IN       MRT_ITEM.RESTOCK_PCT%TYPE,
                       I_selected_ind                  IN       MRT_ITEM.SELECTED_IND%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALIDATE_GROUP_VALUE
-- Purpose      : Validate the group value for the passed group type
----------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP_VALUE(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_group_value_desc         IN OUT   VARCHAR2,
                              O_tsf_qty                  IN OUT   MRT_ITEM_LOC.TSF_QTY%TYPE,
                              O_tsf_price                IN OUT   MRT_ITEM_LOC.TSF_PRICE%TYPE,
                              O_tsf_cost                 IN OUT   MRT_ITEM_LOC.TSF_COST%TYPE,
                              O_franchise_store_exists   IN OUT   VARCHAR2,
                              I_group_type               IN       CODE_DETAIL.CODE%TYPE,
                              I_group_value              IN       VARCHAR2,
                              I_v_or_p_wh_ind            IN       VARCHAR2,
                              I_currency_code            IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                              I_intercompany_tsf_basis   IN       SYSTEM_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE,
                              I_mrt_no                   IN       MRT.MRT_NO%TYPE,
                              I_mrt_type                 IN       MRT.MRT_TYPE%TYPE,
                              I_inventory_type           IN       MRT.INVENTORY_TYPE%TYPE,
                              I_item                     IN       MRT_ITEM.ITEM%TYPE,
                              I_wh_tsf_entity_id         IN       WH.TSF_ENTITY_ID%TYPE,
                              I_wh_set_of_books_id       IN       ORG_UNIT.SET_OF_BOOKS_ID%TYPE,
                              I_supplier                 IN       MRT.SUPPLIER%TYPE,
                              I_wh                       IN       MRT.WH%TYPE,
                              I_rtv_cost                 IN       MRT_ITEM.RTV_COST%TYPE)
RETURN BOOLEAN ;
----------------------------------------------------------------------------------
END MRT_ATTRIB_SQL;
/