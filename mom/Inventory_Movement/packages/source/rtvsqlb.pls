create or replace PACKAGE BODY RTV_SQL AS

LP_vdate                    PERIOD.VDATE%TYPE;
LP_sdate                    DATE;
LP_user                     VARCHAR2(50);

LP_distribute               VARCHAR2(1);
LP_valid_rtv                VARCHAR2(1) := 'N';
LP_mrt_no                   RTV_HEAD.MRT_NO%TYPE;
LP_upd_rtv_qty_ind          VARCHAR2(1);

LP_stock_count_processed    BOOLEAN;
LP_snapshot_unit_cost       STAKE_SKU_LOC.SNAPSHOT_UNIT_COST%TYPE;
LP_snapshot_unit_retail     STAKE_SKU_LOC.SNAPSHOT_UNIT_RETAIL%TYPE;

LP_system_options           SYSTEM_OPTIONS%ROWTYPE;
LP_rtv_record               RTV_RECORD;
LP_index                    NUMBER;
LP_item_total_cost          NUMBER;
LP_dist_tab_1               DISTRIBUTION_SQL.DIST_TABLE_TYPE;

--- Code for a deposit item type of contents item is 'E' in
--- CODE_DETAIL table, code_type 'ITMT'
LP_contents_code  CONSTANT  ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE := 'E';
---------------------------------------------------------------------------------------
-- FUNCTION : BUILD_RTV_RECORD
-- Purpose  : This private function is called from RTV_SQL.SHIP_RTV. It builds an
-- RTV_SQL.RTV_RECORD (O_rtv_record) based on the rtv_head (I_rtv_head_row) and
-- rtv_detail (I_rtv_details) information. Since break-to-sell sellable items
-- will NOT be allowed on RTVs created in RMS, we do not need to handle BTS sellable
-- items. The RTV_RECORD will be passed on to RTV_SQL.APPLY_PROCESS to ship the RTV.
---------------------------------------------------------------------------------------
FUNCTION BUILD_RTV_RECORD(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                          O_rtv_record      IN OUT NOCOPY RTV_SQL.RTV_RECORD,
                          I_rtv_head_row    IN            RTV_HEAD%ROWTYPE,
                          I_rtv_details     IN            RTV_SQL.RTV_DETAIL_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- FUNCTION : POPULATE_RTV_DETAIL
-- Purpose  : This private function is called from BUILD_RTV_RECORD. It builds
-- RTV_SQL.RTV_DETAIL_REC and a collection of item_master rows based on the details
-- on RTV_DETAIL.
---------------------------------------------------------------------------------------
FUNCTION POPULATE_RTV_DETAIL(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                             O_details_rec     IN OUT NOCOPY  RTV_SQL.RTV_DETAIL_REC,
                             O_im_row_tbl      IN OUT NOCOPY  RTV_SQL.ITEM_MASTER_TBL,
                             I_details_tbl     IN             RTV_SQL.RTV_DETAIL_TBL)
RETURN BOOLEAN;
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
---------------------------------------------------------------------------------------
-- FUNCTION : INSERT_RTVITEM_INV_FLOW
-- Purpose  : This function inserts distribution details into rtvitem_inv_flow table.
---------------------------------------------------------------------------------------
FUNCTION INSERT_RTVITEM_INV_FLOW (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_rtv_order_no   IN     RTV_DETAIL.RTV_ORDER_NO%TYPE,
                                  I_seq_no         IN     RTV_DETAIL.SEQ_NO%TYPE,
                                  I_item           IN     RTV_DETAIL.ITEM%TYPE,
                                  I_dist_tab       IN     DISTRIBUTION_SQL.DIST_TABLE_TYPE)
   RETURN BOOLEAN;
$end
---------------------------------------------------------------------------------------
FUNCTION INITIALIZE(O_error_message   IN OUT   VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   LP_distribute               := 'N';

   LP_vdate                    := GET_VDATE;
   LP_user                     := GET_USER;
   LP_sdate                    := SYSDATE;

   LP_valid_rtv                := 'N';
   LP_mrt_no                   := NULL;
   LP_upd_rtv_qty_ind          := NULL;
   LP_stock_count_processed    := FALSE;
   LP_snapshot_unit_cost       := NULL;
   LP_snapshot_unit_retail     := NULL;

   LP_system_options           := NULL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RTV_SQL.INITIALIZE',
                                            to_char(SQLCODE));
      return FALSE;
END INITIALIZE;
-------------------------------------------------------------------------------
FUNCTION UPDATE_INV_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_qty_returned    IN       RTV_DETAIL.QTY_RETURNED%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'RTV_SQL.UPDATE_INV_STATUS';

   L_found     BOOLEAN        := NULL;

BEGIN
   -- This function is called for packs at wh, which can only be received as pack.
   -- For packs at store, it is called for component items.

   if LP_rtv_record.detail_TBL.inv_statuses(LP_index) is NOT NULL then
      if INVADJ_SQL.ADJ_TRAN_DATA(I_item,
                                  LP_rtv_record.loc_type,
                                  LP_rtv_record.loc,
                                  I_qty_returned * (-1),
                                  L_program,
                                  LP_vdate,
                                  25,   -- tran code
                                  NULL,
                                  LP_rtv_record.detail_TBL.inv_statuses(LP_index),
                                  NULL,
                                  NULL,
                                  O_error_message,
                                  L_found) = FALSE then
         return FALSE;
      end if;
      ---
      if INVADJ_SQL.ADJ_UNAVAILABLE(I_item,
                                    LP_rtv_record.detail_TBL.inv_statuses(LP_index),
                                    LP_rtv_record.loc_type,
                                    LP_rtv_record.loc,
                                    I_qty_returned * (-1),
                                    O_error_message,
                                    L_found) = FALSE then
         return FALSE;
      end if;
      ---
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_INV_STATUS;
---------------------------------------------------------------------
FUNCTION CHECK_COUNT_PROCESSED(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item                   IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)  := 'RTV_SQL.CHECK_COUNT_PROCESSED';

   L_cycle_count    STAKE_HEAD.CYCLE_COUNT%TYPE;

   cursor C_get_snapshot is
   select snapshot_unit_cost,
          snapshot_unit_retail
     from stake_sku_loc
    where cycle_count = L_cycle_count
      and item = I_item
      and location = LP_rtv_record.loc
      and loc_type = LP_rtv_record.loc_type;

BEGIN
   if LP_rtv_record.tran_date < LP_vdate then
      if STKCNT_ATTRIB_SQL.STOCK_COUNT_PROCESSED(O_error_message,
                                                 LP_stock_count_processed,
                                                 L_cycle_count,
                                                 LP_rtv_record.tran_date,
                                                 I_item,
                                                 LP_rtv_record.loc_type,
                                                 LP_rtv_record.loc) = FALSE then
         RETURN FALSE;
      end if;
   end if;

   if LP_stock_count_processed then
      open C_GET_SNAPSHOT;
      fetch C_GET_SNAPSHOT into LP_snapshot_unit_cost,
                                LP_snapshot_unit_retail;
      close C_GET_SNAPSHOT;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_COUNT_PROCESSED;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_STOCK(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                           I_qty_returned          IN       RTV_DETAIL.QTY_RETURNED%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'RTV_SQL.UPDATE_ITEM_STOCK';

   L_stock_count_processed_str   VARCHAR2(5);
   L_rowid                       ROWID;
   L_qty_returned                ITEM_LOC_SOH.STOCK_ON_HAND%TYPE; -- holds unit qty or weight
   L_upd_rtv_qty                 ITEM_LOC_SOH.RTV_QTY%TYPE; -- holds original RTV qty when RTV was created in RMS
   L_rtv_qty                     ITEM_LOC_SOH.RTV_QTY%TYPE;
   L_comp_qty                    V_PACKSKU_QTY.QTY%TYPE := NULL;

   L_table                       VARCHAR2(15);
   RECORD_LOCKED                 EXCEPTION;
   PRAGMA                        EXCEPTION_INIT(Record_Locked, -54);

   cursor C_ITEM_LOC_SOH is
   select rowid
     from item_loc_soh
    where item = I_item
      and loc = LP_rtv_record.loc
      and loc_type = LP_rtv_record.loc_type
      for update nowait;

BEGIN

   -- This function is called to update stock for non-packs or pack component items at store or wh.
   -- RTV_QTY should NOT be updated here. It will be updated in UPD_ITEM_RTV_QTY through UPD_RTV_QTY.

   -- For packs at wh, stock count happens for pack. Do not need to check stock count for component.
   if NOT (LP_rtv_record.loc_type = 'W' and LP_rtv_record.im_row_TBL(LP_index).pack_ind = 'Y') then
      if CHECK_COUNT_PROCESSED(O_error_message,
                               I_item) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_stock_count_processed then
      L_stock_count_processed_str := 'TRUE';
   else
      L_stock_count_processed_str := 'FALSE';
   end if;

   L_table := 'ITEM_LOC_SOH';

   open C_ITEM_LOC_SOH;
   fetch C_ITEM_LOC_SOH into L_rowid;
   close C_ITEM_LOC_SOH;

   -- for a simple pack catch weight component item, stock buckets may need to be updated by weight
   if LP_rtv_record.im_row_TBL(LP_index).simple_pack_ind = 'Y' and
      LP_rtv_record.im_row_TBL(LP_index).catch_weight_ind = 'Y' then
      if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                               L_qty_returned,
                                               I_item,
                                               I_qty_returned,
                                               LP_rtv_record.detail_TBL.weight_cuoms(LP_index),
                                               LP_rtv_record.detail_TBL.cuoms(LP_index)) = FALSE then
         return FALSE;
      end if;
   else
      L_qty_returned := I_qty_returned;
   end if;
   ---
   if LP_rtv_record.loc_type = 'S' then
      -- for a pack component item, update stock_on_hand at store
      update item_loc_soh
         set stock_on_hand = decode(L_stock_count_processed_str, 'FALSE',
                                    stock_on_hand - L_qty_returned, stock_on_hand),
             soh_update_datetime = decode(L_stock_count_processed_str, 'FALSE',
                                          decode(I_qty_returned, 0,
                                                 soh_update_datetime,LP_sdate),soh_update_datetime),
             last_update_datetime = LP_sdate,
             last_update_id = LP_user
       where rowid = L_rowid;
   else
      -- for a pack component item, update pack_comp_soh at wh
      update item_loc_soh
         set pack_comp_soh = decode(L_stock_count_processed_str, 'FALSE', decode(LP_rtv_record.im_row_TBL(LP_index).pack_ind, 'Y',
                                    pack_comp_soh - L_qty_returned, pack_comp_soh), pack_comp_soh),
             stock_on_hand = decode(L_stock_count_processed_str, 'FALSE', decode(LP_rtv_record.im_row_TBL(LP_index).pack_ind, 'Y',
                                    stock_on_hand, stock_on_hand - L_qty_returned), stock_on_hand),
             soh_update_datetime = decode(L_stock_count_processed_str, 'FALSE',
                                          decode(LP_rtv_record.im_row_TBL(LP_index).pack_ind, 'Y',
                                                 soh_update_datetime,
                                                 decode(I_qty_returned, 0,
                                                        soh_update_datetime,
                                                        LP_sdate)),
                                          soh_update_datetime),
             last_update_datetime = LP_sdate,
             last_update_id = LP_user
       where rowid = L_rowid;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_item,
                                             to_char(LP_rtv_record.loc));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ITEM_STOCK;
---------------------------------------------------------------------
FUNCTION UPDATE_PACK_STOCK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'RTV_SQL.UPDATE_PACK_STOCK';

   L_stock_count_processed_str   VARCHAR(5);
   L_avg_weight_new              ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_avg_weight_curr             ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_soh_curr                    ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_rowid                       ROWID;
   L_upd_rtv_qty                 ITEM_LOC_SOH.RTV_QTY%TYPE;
   L_rtv_qty                     ITEM_LOC_SOH.RTV_QTY%TYPE;
   L_rtv_qty_check               ITEM_LOC_SOH.RTV_QTY%TYPE;

   L_table          VARCHAR2(15);
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_LOC_SOH is
   select rowid
     from item_loc_soh
    where item = LP_rtv_record.detail_TBL.items(LP_index)
      and loc = LP_rtv_record.loc
      and loc_type = LP_rtv_record.loc_type
      for update nowait;

   cursor C_GET_RTV_QTY is
   select qty_requested
     from rtv_detail
    where rtv_order_no = LP_rtv_record.rtv_order_no
      and item = LP_rtv_record.detail_TBL.items(LP_index)
      and reason = LP_rtv_record.detail_TBL.reasons(LP_index)
      and ((inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)) or
           (inv_status is NULL and LP_rtv_record.detail_TBL.inv_statuses(LP_index) is NULL));

   cursor C_GET_RTVITEM_INV_FLOW_QTY is
   select qty_requested
     from rtvitem_inv_flow
    where rtv_order_no = LP_rtv_record.rtv_order_no
      and seq_no = LP_rtv_record.detail_TBL.seq_nos(LP_index)
      and item = LP_rtv_record.detail_TBL.items(LP_index)
      and loc = LP_rtv_record.loc
      and loc_type = LP_rtv_record.loc_type;

BEGIN
   -- This function is called to update stock for packs at wh,
   -- which can only be received as pack for rtv.
   -- RTV_QTY should NOT be updated here. It will be updated in UPD_ITEM_RTV_QTY through UPD_RTV_QTY.

   if CHECK_COUNT_PROCESSED(O_error_message,
                            LP_rtv_record.detail_TBL.items(LP_index)) = FALSE then
      return FALSE;
   end if;

   if LP_stock_count_processed then
      L_stock_count_processed_str := 'TRUE';
   else
      L_stock_count_processed_str := 'FALSE';
   end if;
   ---
   L_table := 'ITEM_LOC_SOH';

   open C_LOCK_ITEM_LOC_SOH;
   fetch C_LOCK_ITEM_LOC_SOH into L_rowid;
   close C_LOCK_ITEM_LOC_SOH;
   ---
   if LP_upd_rtv_qty_ind = 'Y' then
      -- RTVs created in RMS, ITEM_LOC_SOH.rtv_qty needs to be backed out.
      if LP_distribute = 'N' then
         -- RTVs from store and Virtual warehouse,RTV_DETAIL.qty_requested holds the virtual quantity.
         open C_GET_RTV_QTY;
         fetch C_GET_RTV_QTY into L_rtv_qty;
         close C_GET_RTV_QTY;
      else
         -- RTVs from Physical warehouse, RTVITEM_INV_FLOW.qty_requested holds the distributed quantities
         -- to the virtual warehouses. ITEM_LOC_SOH.rtv_qty is backed out for these quantities.
         open C_GET_RTVITEM_INV_FLOW_QTY;
         fetch C_GET_RTVITEM_INV_FLOW_QTY into L_rtv_qty;
         close C_GET_RTVITEM_INV_FLOW_QTY;
      end if;
      L_upd_rtv_qty := NVL(L_rtv_qty, 0);
   else
      -- RTVs created outside of RMS, no need to adjust ITEM_LOC_SOH.rtv_qty.
      L_upd_rtv_qty := 0;
   end if;

   update item_loc_soh
      set stock_on_hand = DECODE(L_stock_count_processed_str, 'FALSE',
                                 stock_on_hand - LP_rtv_record.detail_TBL.returned_qtys(LP_index), stock_on_hand),
          last_update_datetime = LP_sdate,
          soh_update_datetime = DECODE(L_stock_count_processed_str, 'FALSE',
                                       DECODE(LP_rtv_record.detail_TBL.returned_qtys(LP_index), 0,
                                              soh_update_datetime, LP_sdate),soh_update_datetime),
          last_update_id = LP_user
    where rowid = L_rowid;
   ---

   L_rtv_qty_check := LP_rtv_record.detail_TBL.returned_qtys(LP_index)- L_upd_rtv_qty;

   if L_rtv_qty_check > 0 then
      if UPDATE_INV_STATUS(O_error_message,
                           LP_rtv_record.detail_TBL.items(LP_index),
                           L_rtv_qty_check) = FALSE then
         return FALSE;
      end if;
   end if;

   ---
   if UPDATE_SNAPSHOT_SQL.EXECUTE(O_error_message,
                                  'RTV',
                                  LP_rtv_record.detail_TBL.items(LP_index),
                                  LP_rtv_record.store,
                                  LP_rtv_record.wh,
                                  LP_rtv_record.tran_date,
                                  LP_vdate,
                                  LP_rtv_record.detail_TBL.returned_qtys(LP_index)) = FALSE then
      RETURN FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                L_table,
                                                LP_rtv_record.detail_TBL.items(LP_index),
                                                to_char(LP_rtv_record.loc));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_PACK_STOCK;
---------------------------------------------------------------------
FUNCTION WRITE_TRAN_DATA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item            IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_no         IN       ITEM_MASTER.ITEM%TYPE,
                         I_dept            IN       ITEM_MASTER.DEPT%TYPE,
                         I_class           IN       ITEM_MASTER.CLASS%TYPE,
                         I_subclass        IN       ITEM_MASTER.SUBCLASS%TYPE,
                         I_qty_returned    IN       RTV_DETAIL.QTY_RETURNED%TYPE,
                         I_weight_cuom     IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                         I_cuom            IN       ITEM_SUPP_COUNTRY.COST_UOM%TYPE,
                         I_rtv_cost_loc    IN       RTV_DETAIL.UNIT_COST%TYPE,
                         I_item_xform_ind  IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                         I_sellable_ind    IN       ITEM_MASTER.SELLABLE_IND%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'RTV_SQL.WRITE_TRAN_DATA';

   L_tran_type      TRAN_DATA.TRAN_CODE%TYPE;
   L_wac            ITEM_LOC_SOH.AV_COST%TYPE;
   L_item_retail    ITEM_LOC.UNIT_RETAIL%TYPE;
   L_total_cost     ITEM_LOC_SOH.AV_COST%TYPE := 0;
   L_total_retail    ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_restock_cost    RTV_DETAIL.UNIT_COST%TYPE;
   L_rtv_qty         RTV_DETAIL.QTY_RETURNED%TYPE;
   L_rtv_qty_check   RTV_DETAIL.QTY_RETURNED%TYPE;
   L_qty_returned    RTV_DETAIL.QTY_RETURNED%TYPE; -- holds unit qty or weight
   L_comp_item_qty   V_PACKSKU_QTY.QTY%TYPE;

   cursor C_UNIT_RETAIL is
      select unit_retail
        from item_loc
       where item = I_item
         and loc = LP_rtv_record.loc
         and loc_type = LP_rtv_record.loc_type;

   cursor C_GET_RTV_QTY is
      select qty_requested
        from rtv_detail
       where rtv_order_no = LP_rtv_record.rtv_order_no
         and item = LP_rtv_record.detail_TBL.items(LP_index)
         and reason = LP_rtv_record.detail_TBL.reasons(LP_index)
         and ((inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)) or
              (inv_status is NULL and LP_rtv_record.detail_TBL.inv_statuses(LP_index) is NULL));

   cursor C_GET_COMP_ITEM_QTY is
      select qty
        from v_packsku_qty v
       where v.pack_no = LP_rtv_record.detail_TBL.items(LP_index)
         and v.item   = I_item;

BEGIN

   if I_sellable_ind = 'N' and I_item_xform_ind = 'Y' then
      if ITEM_XFORM_SQL.CALCULATE_RETAIL(O_error_message,
                                         I_item,
                                         LP_rtv_record.loc,
                                         L_item_retail) = FALSE then
         return FALSE;
      end if;
   else
      open C_UNIT_RETAIL;
      fetch C_UNIT_RETAIL into L_item_retail;
      close C_UNIT_RETAIL;
   end if;

   open C_GET_RTV_QTY;
   fetch C_GET_RTV_QTY into L_rtv_qty;
   if C_GET_RTV_QTY%NOTFOUND then
      L_rtv_qty := 0;
      close C_GET_RTV_QTY;
   else
      close C_GET_RTV_QTY;
   end if;

   -- if the item in the rtv is a pack, we need to get the rtv qty of its component item
   if LP_rtv_record.im_row_TBL(LP_index).pack_ind = 'Y' then
      open C_GET_COMP_ITEM_QTY;
      fetch C_GET_COMP_ITEM_QTY into L_comp_item_qty;
      close C_GET_COMP_ITEM_QTY;

      if L_comp_item_qty is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PAK',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      L_rtv_qty := L_rtv_qty * L_comp_item_qty;
   end if;
   ---
   -- calculate total cost based on weight. Use location currency for stock ledger.
   if LP_rtv_record.im_row_TBL(LP_index).simple_pack_ind = 'Y' and
      LP_rtv_record.im_row_TBL(LP_index).catch_weight_ind = 'Y' then

      if CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                          L_total_cost,
                                          I_pack_no,
                                          I_rtv_cost_loc, -- component unit cost
                                          I_weight_cuom,
                                          I_qty_returned) = FALSE then  -- uom, default to cost uom
         return FALSE;
      end if;

      ---
      -- For a simple pack catch weight item, there are two scenarioes:
      -- 1) the component item's standard UOM is weight, then TRAN_DATA.UNITS should be updated with
      -- the actual weight converted to component item's SUOM.
      -- 2) the component item's standard UOM is Eaches, then TRAN_DATA.UNITS should be updated with
      -- I_qty_returned, which is based on v_packsku_qty.
      ---
      if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                               L_qty_returned,   -- output qty
                                               I_item,
                                               I_qty_returned,   -- input qty
                                               I_weight_cuom,
                                               I_cuom) = FALSE then
         return FALSE;
      end if;

      if CATCH_WEIGHT_SQL.CALC_TOTAL_RETAIL (O_error_message ,
                                             I_item,
                                             L_qty_returned,
                                             L_item_retail,
                                             LP_rtv_record.loc,
                                             LP_rtv_record.loc_type,
                                             I_weight_cuom,
                                             I_cuom,
                                             L_total_retail) = FALSE then
          return FALSE;
       end if;
   else
      L_total_cost   := I_rtv_cost_loc * I_qty_returned;
      L_qty_returned := I_qty_returned;
      L_total_retail := L_item_retail * L_qty_returned;
   end if;
    if LP_rtv_record.detail_TBL.returned_qtys(LP_index) > 0 then
      L_tran_type := 24;
      if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                       I_item,
                                       I_dept,
                                       I_class,
                                       I_subclass,
                                       LP_rtv_record.loc,
                                       LP_rtv_record.loc_type,
                                       LP_rtv_record.tran_date,
                                       L_tran_type,
                                       NULL,
                                       L_qty_returned,
                                       L_total_cost,
                                       L_total_retail,
                                       LP_rtv_record.rtv_order_no,
                                       NULL,--I_ref_no_2
                                       NULL,--I_tsf_source_st
                                       NULL,--I_tsf_source_wh
                                       NULL,--I_old_unit_retail
                                       NULL,--I_new_unit_retail
                                       NULL,--I_source_dept
                                       NULL,--I_source_class
                                       NULL,--I_source_subclass
                                      'RTV_SQL.INVENTORY',
                                       NULL,-- gl_ref_no
                                       I_pack_no) = FALSE then
         return FALSE;
      end if;
   end if;

   -- write restocking fee for non-MRT RTV
   if LP_rtv_record.detail_TBL.returned_qtys(LP_index) > 0 then
      if LP_mrt_no is NULL and LP_rtv_record.detail_TBL.restock_pcts(LP_index) > 0 then
         L_tran_type := 65;
         L_restock_cost := L_total_cost * LP_rtv_record.detail_TBL.restock_pcts(LP_index)/100;
         if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                          I_item,
                                          I_dept,
                                          I_class,
                                          I_subclass,
                                          LP_rtv_record.loc,
                                          LP_rtv_record.loc_type,
                                          LP_rtv_record.tran_date,
                                          L_tran_type,
                                          NULL,
                                          L_qty_returned,
                                          L_restock_cost,
                                          NULL,  -- I_total_retail
                                          LP_rtv_record.rtv_order_no,
                                          NULL,--I_ref_no_2
                                          NULL,--I_tsf_source_st
                                          NULL,--I_tsf_source_wh
                                          NULL,--I_old_unit_retail
                                          NULL,--I_new_unit_retail
                                          NULL,--I_source_dept
                                          NULL,--I_source_class
                                          NULL,--I_source_subclass
                                          'RTV_SQL.INVENTORY') = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   -- get wac for writing cost variance
   if ITEMLOC_ATTRIB_SQL.GET_WAC(O_error_message,
                                 L_wac,
                                 I_item,
                                 I_dept,
                                 I_class,
                                 I_subclass,
                                 LP_rtv_record.loc,
                                 LP_rtv_record.loc_type,
                                 LP_rtv_record.tran_date) = FALSE then
         return FALSE;
   end if;

   if STKLEDGR_SQL.INIT_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if LP_rtv_record.detail_TBL.returned_qtys(LP_index) > 0 then
      if STKLEDGR_SQL.POST_COST_VARIANCE(O_error_message,
                                         I_item,
                                         I_pack_no,
                                         I_dept,
                                         I_class,
                                         I_subclass,
                                         LP_rtv_record.loc,
                                         LP_rtv_record.loc_type,
                                         I_rtv_cost_loc, -- rtv unit cost
                                         L_wac,
                                         L_qty_returned,
                                         I_weight_cuom,
                                         LP_rtv_record.rtv_order_no, -- ref_no_1,
                                         NULL,
                                         LP_rtv_record.tran_date) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   -- Since RTV only handles packs that are received as Pack at warehouses,
   -- no inventory adjustement, stock counting adjustment for packs at
   -- warehouse at the component level.
   if LP_rtv_record.loc_type = 'W' and LP_rtv_record.im_row_TBL(LP_index).pack_ind = 'Y' then
      return TRUE;
   end if;
   ---
   if LP_stock_count_processed then
      L_tran_type := 22;

      --TODO: STAKE_SKU_LOC.SNAPSHOT_UNIT_COST is nullable. What to do if NULL?
      L_total_cost := LP_snapshot_unit_cost * L_qty_returned;
      if LP_snapshot_unit_retail is NOT NULL then
         L_total_retail := LP_snapshot_unit_retail;
      else
         L_total_retail := L_item_retail;
      end if;
      ---
      if LP_rtv_record.im_row_TBL(LP_index).simple_pack_ind = 'Y' and
         LP_rtv_record.im_row_TBL(LP_index).catch_weight_ind = 'Y' then
         if CATCH_WEIGHT_SQL.CALC_TOTAL_RETAIL (O_error_message,
                                                I_item,
                                                L_qty_returned,
                                                L_total_retail,
                                                LP_rtv_record.loc,
                                                LP_rtv_record.loc_type,
                                                I_weight_cuom,
                                                I_cuom,
                                                L_total_retail) = FALSE then
            return FALSE;
         end if;
      else
         L_total_retail := L_total_retail * L_qty_returned;
      end if;
      ---
      if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                       I_item,
                                       I_dept,
                                       I_class,
                                       I_subclass,
                                       LP_rtv_record.loc,
                                       LP_rtv_record.loc_type,
                                       LP_rtv_record.tran_date,
                                       L_tran_type,
                                       NULL,
                                       L_qty_returned,
                                       L_total_cost,
                                       L_total_retail,
                                       LP_rtv_record.rtv_order_no,
                                       NULL,--I_ref_no_2
                                       NULL,--I_tsf_source_st
                                       NULL,--I_tsf_source_wh
                                       NULL,--I_old_unit_retail
                                       NULL,--I_new_unit_retail
                                       NULL,--I_source_dept
                                       NULL,--I_source_class
                                       NULL,--I_source_subclass
                                       'RTV_SQL.INVENTORY',
                                       2) = FALSE then
         return FALSE;
      end if;
   end if;
   ---

   L_rtv_qty_check := L_qty_returned-L_rtv_qty;

   if L_rtv_qty_check != 0 then --Handling both over/under ship scenario for non sellable qty
      if UPDATE_INV_STATUS(O_error_message,
                           I_item,
                           L_rtv_qty_check) = FALSE then
         return FALSE;
      end if;
   end if;

   ---
   if UPDATE_SNAPSHOT_SQL.EXECUTE(O_error_message,
                                  'RTV',
                                  I_item,
                                  LP_rtv_record.store,
                                  LP_rtv_record.wh,
                                  LP_rtv_record.tran_date,
                                  LP_vdate,
                                  L_qty_returned) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_TRAN_DATA;
---------------------------------------------------------------------
FUNCTION INVENTORY(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_upd_rtv_qty_ind   IN       VARCHAR2,
                   I_item_xform_ind    IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                   I_sellable_ind      IN       ITEM_MASTER.SELLABLE_IND%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(64) := 'RTV_SQL.INVENTORY';
   L_rtv_unit_cost_ind      INV_MOVE_UNIT_OPTIONS.RTV_UNIT_COST_IND%TYPE;
   L_pct_in_pack            NUMBER;
   L_count                  NUMBER(3) := 0;

   cursor C_ITEMS_IN_PACK is
      select v.item,
             v.qty,
             im.dept,
             im.class,
             im.subclass,
             im.inventory_ind
        from item_master im,
             v_packsku_qty v
       where v.pack_no = LP_rtv_record.detail_TBL.items(LP_index)
         and im.item   = v.item;

   cursor C_GET_ITEM_COUNT is
      select count(item)
        from rtv_detail
       where rtv_order_no = LP_rtv_record.rtv_order_no
         and item = LP_rtv_record.detail_TBL.items(LP_index);

   cursor C_LOCK_DETAIL is
      select 'x'
        from rtv_detail
       where rtv_order_no = LP_rtv_record.rtv_order_no
         and item = LP_rtv_record.detail_TBL.items(LP_index)
         for update nowait;

   cursor C_LOCK_RTV_DTL is
      select 'x'
        from rtv_detail
       where rtv_order_no = LP_rtv_record.rtv_order_no
         and item = LP_rtv_record.detail_TBL.items(LP_index)
         and reason = LP_rtv_record.detail_TBL.reasons(LP_index)
         and ((inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)) or
              (inv_status is NULL and LP_rtv_record.detail_TBL.inv_statuses(LP_index) is NULL))
         for update nowait;

BEGIN

   LP_upd_rtv_qty_ind := I_upd_rtv_qty_ind;
   ---
   if LP_rtv_record.wh = -1 then
      LP_rtv_record.loc_type := 'S';
      LP_rtv_record.loc := LP_rtv_record.store;
   else
      LP_rtv_record.loc_type := 'W';
      LP_rtv_record.loc := LP_rtv_record.wh;
   end if;
   ---
   if LP_valid_rtv = 'Y' then
      open C_GET_ITEM_COUNT;
      fetch C_GET_ITEM_COUNT into L_count;
      close C_GET_ITEM_COUNT;

      if L_count > 1 then
         open C_LOCK_RTV_DTL;
         close C_LOCK_RTV_DTL;

        update rtv_detail
           set reason = LP_rtv_record.detail_TBL.reasons(LP_index),
               inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)
         where rtv_order_no = LP_rtv_record.rtv_order_no
           and item = LP_rtv_record.detail_TBL.items(LP_index)
           and reason = LP_rtv_record.detail_TBL.reasons(LP_index)
           and ((inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)) or
                (inv_status is NULL and LP_rtv_record.detail_TBL.inv_statuses(LP_index) is NULL));
      else
         open C_LOCK_DETAIL;
         close C_LOCK_DETAIL;

         update rtv_detail
            set reason = LP_rtv_record.detail_TBL.reasons(LP_index),
                inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)
          where rtv_order_no = LP_rtv_record.rtv_order_no
            and item = LP_rtv_record.detail_TBL.items(LP_index);
      end if;
   end if;
   ---
   if LP_rtv_record.im_row_TBL(LP_index).pack_ind != 'Y' then
      ---
      if UPDATE_ITEM_STOCK(O_error_message,
                           LP_rtv_record.detail_TBL.items(LP_index),
                           LP_rtv_record.detail_TBL.returned_qtys(LP_index)) = FALSE then
         return FALSE;
      end if;
      ---
      if WRITE_TRAN_DATA(O_error_message,
                         LP_rtv_record.detail_TBL.items(LP_index),
                         NULL,  -- pack_no
                         LP_rtv_record.im_row_TBL(LP_index).dept,
                         LP_rtv_record.im_row_TBL(LP_index).class,
                         LP_rtv_record.im_row_TBL(LP_index).subclass,
                         LP_rtv_record.detail_TBL.returned_qtys(LP_index),
                         NULL,  -- weight
                         NULL,  -- weight_uom
                         NVL(LP_rtv_record.detail_TBL.extended_base_cost(LP_index),LP_rtv_record.detail_TBL.unit_cost_locs(LP_index)),
                         I_item_xform_ind,
                         I_sellable_ind) = FALSE then
         return FALSE;
      end if;
   else
      -- For RTVs, packs can be only received as packs at warehouse.
      -- Update stock on hand for the pack
      if LP_rtv_record.loc_type = 'W' then
         if UPDATE_PACK_STOCK(O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;
      --
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                              LP_system_options) = FALSE then
         return FALSE;
      end if;
      L_rtv_unit_cost_ind := LP_system_options.rtv_unit_cost_ind;
      -- handle packs at component level for both stores and warehouses
      FOR rec in C_ITEMS_IN_PACK LOOP
         ---
         if rec.inventory_ind = 'Y' then
            if UPDATE_ITEM_STOCK(O_error_message,
                                 rec.item,
                                 LP_rtv_record.detail_TBL.returned_qtys(LP_index) * rec.qty) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
          --The pack component calculation should use unit_cost when rtv_unit_cost_ind is R (last receipt cost).
         if L_rtv_unit_cost_ind = 'R' then
            if TRANSFER_COST_SQL.PCT_IN_PACK_UNIT_COST(O_error_message,
                                                       L_pct_in_pack,
                                                       LP_rtv_record.detail_TBL.items(LP_index),  -- pack_no
                                                       rec.item,
                                                       LP_rtv_record.loc) = FALSE then
               return FALSE;
            end if;
         else
            if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                             L_pct_in_pack,
                                             LP_rtv_record.detail_TBL.items(LP_index),  -- pack_no
                                             rec.item,
                                             LP_rtv_record.loc) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if WRITE_TRAN_DATA(O_error_message,
                            rec.item,
                            LP_rtv_record.detail_TBL.items(LP_index),  -- pack_no,
                            rec.dept,
                            rec.class,
                            rec.subclass,
                            LP_rtv_record.detail_TBL.returned_qtys(LP_index) * rec.qty,  -- comp qty
                            LP_rtv_record.detail_TBL.weight_cuoms(LP_index),
                            LP_rtv_record.detail_TBL.cuoms(LP_index),
                            NVL(LP_rtv_record.detail_TBL.extended_base_cost(LP_index),LP_rtv_record.detail_TBL.unit_cost_locs(LP_index))*L_pct_in_pack,
                            I_item_xform_ind,
                            I_sellable_ind) = FALSE then -- comp unit cost
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END INVENTORY;
---------------------------------------------------------------------
FUNCTION UPD_ITEM_RTV_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE,
                          I_location        IN       INV_ADJ.LOCATION%TYPE,
                          I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                          I_qty             IN       RTV_DETAIL.QTY_RETURNED%TYPE,
                          I_inv_status      IN       RTV_DETAIL.INV_STATUS%TYPE,
                          I_action_type     IN       VARCHAR2,
                          I_rtv_order_no    IN       RTV_DETAIL.RTV_ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(64) := 'RTV_SQL.UPD_ITEM_RTV_QTY';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(15);
   L_sysdate       DATE;
   L_user          VARCHAR2(50);
   L_found         BOOLEAN   := NULL;
   L_exists        VARCHAR2(1):='N';

   cursor C_LOCK_ITEM_LOC is
   select 'x'
     from item_loc_soh
    where item = I_item
      and loc = I_location
      and loc_type = I_loc_type
      for update nowait;

BEGIN
   L_sysdate := sysdate;
   if LP_user is NULL then
      L_user := get_user;
   else
      L_user := LP_user;
   end if;

   L_table := 'ITEM_LOC_SOH';

   open C_LOCK_ITEM_LOC;
   close C_LOCK_ITEM_LOC;

  update item_loc_soh
     set rtv_qty = rtv_qty + I_qty,
         last_update_datetime = L_sysdate,
         last_update_id       = L_user
   where item = I_item
     and loc = I_location
     and loc_type = I_loc_type;
   ---

   if I_action_type = 'S' then
      update rtv_detail
         set qty_returned = NVL(qty_returned,0),
             qty_cancelled = decode(sign(qty_requested - nvl(qty_returned, 0)), -1, 0, (qty_requested - nvl(qty_returned, 0))),
             updated_by_rms_ind = 'N'
       where item =I_item
         and rtv_order_no = I_rtv_order_no;
   end if;

   if (I_action_type = 'A' or I_action_type = 'C' or I_action_type = 'U') and I_inv_status is not null then
      if INVADJ_SQL.ADJ_TRAN_DATA(I_item,
                                  I_loc_type,
                                  I_location,
                                  I_qty * (-1),
                                  L_program,
                                  GET_VDATE,
                                  25,   -- tran code
                                  NULL,
                                  I_inv_status,
                                  NULL,
                                  NULL,
                                  O_error_message,
                                  L_found) = FALSE then
         return FALSE;
      end if;
      ---
      if INVADJ_SQL.ADJ_UNAVAILABLE(I_item,
                                    I_inv_status,
                                    I_loc_type,
                                    I_location,
                                    I_qty * (-1),
                                    O_error_message,
                                    L_found) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             to_char(I_location),
                                             I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_ITEM_RTV_QTY;
--------------------------------------------------------------------------------------------------
FUNCTION UPD_RTV_QTY( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_rtv_order_no    IN       RTV_DETAIL.RTV_ORDER_NO%TYPE,
                      I_item            IN       ITEM_MASTER.ITEM %TYPE,
                      I_action_type     IN       VARCHAR2 )
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'RTV_SQL.UPD_RTV_QTY';
   L_item            ITEM_MASTER.ITEM%TYPE;
   L_location        INV_ADJ.LOCATION%TYPE;
   L_loc_type        INV_ADJ.LOC_TYPE%TYPE;
   L_qty_requested   RTV_DETAIL.QTY_REQUESTED%TYPE := 0;
   L_pack_no         V_PACKSKU_QTY.PACK_NO%TYPE;
   L_wh              RTV_HEAD.WH%TYPE;
   L_store           RTV_HEAD.STORE%TYPE;
   L_pwh_flag        BOOLEAN := FALSE;
   L_dist_tab        DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   L_table           VARCHAR2(30);
   L_inv_status      RTV_DETAIL.INV_STATUS%TYPE;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_RTV_HEAD is
   select wh,
          store
     from rtv_head
    where rtv_order_no = I_rtv_order_no;

   cursor C_RTVITEM_INV_FLOW is
      select rif.item,
             rif.loc,
             rif.loc_type,
             rif.qty_requested - NVL(rif.qty_returned, 0) qty_requested,
             im.pack_ind,
             rd.inv_status
        from rtvitem_inv_flow rif,
             item_master im,
             rtv_detail rd
       where rif.rtv_order_no = I_rtv_order_no
         and rif.rtv_order_no = rd.rtv_order_no
         and rif.item = rd.item
         and im.item = rif.item
         and im.item = NVL(I_item,im.item)
         and NVL(im.deposit_item_type, 'X') != 'A';

   cursor C_RTV_DETAIL is
   select rd.item,
          rd.qty_requested - NVL(rd.qty_cancelled, 0) qty_requested,
          im.pack_ind,
          rd.seq_no,
          rd.inv_status
     from rtv_detail rd,
          item_master im
    where rd.rtv_order_no = I_rtv_order_no
      and im.item         = rd.item
      and im.item         = NVL(I_item,im.item)
      and NVL(im.deposit_item_type, 'X') != 'A';

   cursor C_PACKITEM_RTV_DETAIL is
   select v.item,
          v.qty
     from v_packsku_qty v
    where v.pack_no = L_pack_no;

   cursor C_RTVITEM_INV_FLOW_LOCK is
   select 'x'
     from RTVITEM_INV_FLOW
    where rtv_order_no = I_rtv_order_no
      and item = NVL(I_item,item)
      for update nowait;

BEGIN
   L_table := 'RTVITEM_INV_FLOW';

   open C_RTV_HEAD;
   fetch C_RTV_HEAD into L_wh,L_store;
   close C_RTV_HEAD;

   if L_wh = -1 then
      L_loc_type := 'S';
      L_location := L_store;
   else
      L_loc_type := 'W';
      L_location := L_wh;
   end if;

   if L_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh_flag,
                                 L_location) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_pwh_flag = TRUE and I_action_type != 'A' then
      -- If L_pwh_flag is TRUE and if I_action_type != 'A', cancelling qty for an RTV that
      -- involves a pwh. ITEM_LOC_SOH.rtv_qty needs to be backed out based on RTVITEM_INV_FLOW
      -- instead of DISTRIBUTION_SQL because DISTRIBUTION_SQL may be based on distorted qty buckets on
      -- ITEM_LOC_SOH.
      FOR rec IN C_RTVITEM_INV_FLOW LOOP
         L_item := rec.item;
         L_location := rec.loc;
         L_loc_type := rec.loc_type;
         L_qty_requested := rec.qty_requested * (-1); -- cancelling, decrement rtv_qty
         L_inv_status := rec.inv_status;

         if NOT UPD_ITEM_RTV_QTY(O_error_message,
                                 L_item,
                                 L_location,
                                 L_loc_type,
                                 L_qty_requested,
                                 L_inv_status,
                                 I_action_type,
                                 I_rtv_order_no) then
            return FALSE;
         end if;

         -- handle packs
         if rec.pack_ind = 'Y' then
            L_pack_no := rec.item;

            FOR packitem in C_PACKITEM_RTV_DETAIL LOOP
               if NOT UPD_ITEM_RTV_QTY(O_error_message,
                                       packitem.item,
                                       L_location,
                                       L_loc_type,
                                       (L_qty_requested * packitem.qty),
                                       NULL,
                                       I_action_type,
                                       I_rtv_order_no) then
                  return FALSE;
               end if;
            END LOOP;
         end if;
      END LOOP;

      return TRUE;
   end if;   -- End for L_pwh_flag 'Y'
   ---
   if L_pwh_flag then
      -- lock and delete cancelled RTVITEM_INV_FLOW records
      open C_RTVITEM_INV_FLOW_LOCK;
      close C_RTVITEM_INV_FLOW_LOCK;

      delete from RTVITEM_INV_FLOW
       where rtv_order_no = I_rtv_order_no
         and item = NVL(I_item,item);
   end if;

   -- loop through for each item for the rtv_order_no
   FOR rec IN C_RTV_DETAIL LOOP
      -- Clear L_dist_tab
      L_dist_tab.DELETE;

      L_item := rec.item;
      L_qty_requested := rec.qty_requested;

      -- RTV is from a physical warehouse.
      if L_pwh_flag then
         if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                        L_dist_tab,
                                        L_item,
                                        L_location,
                                        L_qty_requested,
                                        'RTV',
                                        rec.inv_status,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL,               -- Cycle Count
                                        I_rtv_order_no,               
                                        rec.seq_no) = FALSE then
            return FALSE;
         end if;
      else
         --when L_loc_type = 'S' or L_pwh_flag is false
         -- set L_dist_tab to loc/qty on table
         L_dist_tab(1).wh := L_location;
         L_dist_tab(1).dist_qty := L_qty_requested;
      end if;

      FOR i in 1 .. L_dist_tab.COUNT LOOP
         -- Approving 'A' an RTV must increment rtv_qty whereas
         -- Unapproving 'U' or Cancelling 'C' or Shipping 'S' RTV
         -- must decrement rtv_qty
         if (I_action_type = 'A') then
            L_qty_requested := L_dist_tab(i).dist_qty;
         else
            L_qty_requested := (-1) * L_dist_tab(i).dist_qty;
         end if;
         ---

         -- no need to apply updates to the pack record if the RTV loc is a store
         -- since pack quantities are not tracked at stores
         if (rec.pack_ind = 'Y' and L_loc_type <> 'S') or rec.pack_ind = 'N' then
            if UPD_ITEM_RTV_QTY(O_error_message,
                                rec.item,
                                L_dist_tab(i).wh,
                                L_loc_type,
                                L_qty_requested,
                                rec.inv_status,
                                I_action_type,
                                I_rtv_order_no) = FALSE then
               return FALSE;
            end if;
         end if;

         -- if the RTV item is a pack item, then also apply updates to the
         -- corresponding pack items
         if rec.pack_ind = 'Y' then
            L_pack_no := L_item;

            FOR packitem in C_PACKITEM_RTV_DETAIL LOOP
               if UPD_ITEM_RTV_QTY(O_error_message,
                                   packitem.item,
                                   L_dist_tab(i).wh,
                                   L_loc_type,
                                   (L_qty_requested * packitem.qty),
                                   NULL,
                                   I_action_type,
                                   I_rtv_order_no) = FALSE then
                  return FALSE;
               end if;
            END LOOP;
         end if;
      END LOOP; -- End of loop for distribution results

      -- RTV is from a physical warehouse.
      if I_action_type = 'A' and L_pwh_flag then
         if INSERT_RTVITEM_INV_FLOW(O_error_message,
                                    I_rtv_order_no,
                                    rec.seq_no,
                                    rec.item,
                                    L_dist_tab) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
   END LOOP; -- End of C_RTV_DETAIL LOOP.

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_rtv_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPD_RTV_QTY;
------------------------------------------------------------------------------------------------------
--- This function will decide whether the rtv is created in RMS or is externally generated.
--- Also based on multichannel indicator, location and loc_type,this function checks whether or not
--- to distribute the rtv quantity.
------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RTV_HEAD(O_error_message     IN OUT     VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'RTV_SQL.VALIDATE_RTV_HEAD';

   L_exist            VARCHAR2(1) := NULL;
   L_wh               RTV_HEAD.WH%TYPE := NULL;
   L_physical_wh      WH.PHYSICAL_WH%TYPE := NULL;
   L_store            RTV_HEAD.STORE%TYPE := NULL;
   L_rtv_order_no     RTV_HEAD.RTV_ORDER_NO%TYPE := NULL;
   L_return_code      VARCHAR2(5);
   L_supplier         RTV_HEAD.SUPPLIER%TYPE;
   L_vwh              RTV_HEAD.WH%TYPE := NULL;
   L_ret_courier      SUPS.RET_COURIER%TYPE;

   cursor C_RTV_EXISTS is
   select wh,
          store,
          mrt_no,
          supplier
     from rtv_head
    where rtv_order_no = LP_rtv_record.rtv_order_no
      and status_ind in ('10','12','15');

   cursor C_EXT_REF_RTV_EXISTS is
   select rtv_order_no,
          mrt_no,
          supplier
     from rtv_head
    where ext_ref_no = LP_rtv_record.ext_ref_no
      and status_ind in ('10','12','15')
      and ((store = LP_rtv_record.loc)
       or (wh = LP_rtv_record.loc));

   cursor C_WH_EXIST is
      select 'x',
             wh,
             physical_wh
        from wh
       where wh = LP_rtv_record.loc;

   cursor C_STORE_EXIST is
      select 'x'
        from store
       where store = LP_rtv_record.loc;

   cursor C_VWH_SUPP_ORG is
      select wh
        from wh
       where physical_wh = LP_rtv_record.loc
         and org_unit_id in (select org_unit_id
                               from partner_org_unit
                              where partner = LP_rtv_record.supplier);
							  
   cursor C_GET_PVWH is
      select primary_vwh 
        from wh 
       where wh = LP_rtv_record.loc;
                                                            
   cursor C_GET_SUP_INFO is
	  select s.ret_courier,
			 a.add_1,
			 a.add_2,
			 a.add_3,
			 a.city,
			 a.state,
			 a.country_id,
			 a.jurisdiction_code,
			 a.post
		from sups s,
			 addr a
	   where s.supplier = LP_rtv_record.supplier
		 and a.key_value_1 = to_char(LP_rtv_record.supplier)
		 and module = 'SUPP'
		 and s.ret_allow_ind = 'Y'
		 and a.addr_type = '03';
		 
   cursor C_GET_COURIER is
	  select ret_courier
		from sups
	   where supplier = LP_rtv_record.supplier;                              
							  
BEGIN
   -- check if RTV exists, if not it is an externally generated RTV
   open C_RTV_EXISTS;
   fetch C_RTV_EXISTS into L_wh, L_store, LP_mrt_no,L_supplier;
   close C_RTV_EXISTS;

   if L_wh is NOT NULL    or
      L_store is NOT NULL then
      LP_valid_rtv := 'Y';
      LP_upd_rtv_qty_ind := 'Y';  -- If RTV created by RMS, update item_loc_soh
      if L_wh != -1 then
         LP_rtv_record.loc := L_wh;
      end if;
   else

      open C_EXT_REF_RTV_EXISTS;
      fetch C_EXT_REF_RTV_EXISTS into L_rtv_order_no, LP_mrt_no,L_supplier;
      close C_EXT_REF_RTV_EXISTS;

      if L_rtv_order_no is NOT NULL then
         LP_valid_rtv := 'Y';
         LP_upd_rtv_qty_ind := 'N';  -- If RTV NOT created by RMS, DO NOT update item_loc_soh
         LP_rtv_record.rtv_order_no := L_rtv_order_no;

      else

         LP_valid_rtv := 'N';
         LP_upd_rtv_qty_ind := 'N';  -- If RTV NOT created by RMS, DO NOT update item_loc_soh

         -- Retrieve the next rtv_order_no so TRAN_DATA will write REF_NO_1 with RTV_ORDER_NO instead of NULL
         NEXT_RTV_ORDER_NO(LP_rtv_record.rtv_order_no,
                           L_return_code,
                           O_error_message);
         if L_return_code = 'FALSE' then
            return false;
         end if;
         ---
      end if;
   end if;
   ---
   if LP_valid_rtv = 'Y' then
      if LP_rtv_record.supplier <> L_supplier then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUP', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;
   ---
   L_wh := NULL;

   open C_WH_EXIST;
   fetch C_WH_EXIST into L_exist, L_wh, L_physical_wh;
   close C_WH_EXIST;
   ---
   if L_exist is NOT NULL then
      LP_rtv_record.loc_type := 'W';
      LP_rtv_record.wh := LP_rtv_record.loc;
      LP_rtv_record.physical_wh := L_physical_wh;
      LP_rtv_record.store := -1;
   else
      L_exist := NULL;
      open C_STORE_EXIST;
      fetch C_STORE_EXIST into L_exist;
      close C_STORE_EXIST;
      ---
      if L_exist is NOT NULL then
         LP_rtv_record.loc_type := 'S';
         LP_rtv_record.store := LP_rtv_record.loc;
         LP_rtv_record.wh := -1;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                               'LP_rtv_record.loc',
                                               LP_rtv_record.loc,
                                               L_program);
         return FALSE;
      end if;
   end if;
   ---
   --- if multichannel is on and the location is a store, the same processing can
   --- occur as though multichannel is off.
   --- If multichannel is on and the location is a physical wh, distribution should happen.
   --- to be removed CMP
   if LP_rtv_record.loc_type = 'W' and
      L_wh = L_physical_wh then
      LP_distribute := 'Y';   -- Indicates a Physical warehouse.
   else
      LP_distribute := 'N';   -- Indicates a Virtual warehouse or store.
   end if;

   if LP_distribute = 'Y' then
      open C_VWH_SUPP_ORG;
      LOOP
         fetch C_VWH_SUPP_ORG into L_vwh;
         exit when C_VWH_SUPP_ORG%NOTFOUND;
      END LOOP;
      if C_VWH_SUPP_ORG%ROWCOUNT = 1 then
         LP_distribute := 'N';
         LP_rtv_record.wh := L_vwh;
         LP_rtv_record.loc := L_vwh;
      end if;

      if C_VWH_SUPP_ORG%ROWCOUNT = 0 then
         open C_GET_PVWH;
         fetch C_GET_PVWH into L_vwh;      
         close C_GET_PVWH;
         LP_distribute := 'N';
         LP_rtv_record.wh := L_vwh;
         LP_rtv_record.loc := L_vwh;
      end if;         
      close C_VWH_SUPP_ORG;
   end if;

   if LP_rtv_record.ship_addr1 is NULL or LP_rtv_record.city is NULL or LP_rtv_record.country is NULL then
   -- address info on message is in complete, use supplier address
	  open C_GET_SUP_INFO;
		 fetch C_GET_SUP_INFO into L_ret_courier,
		   LP_rtv_record.ship_addr1,
		   LP_rtv_record.ship_addr2,
		   LP_rtv_record.ship_addr3,
		   LP_rtv_record.city,
		   LP_rtv_record.state,
		   LP_rtv_record.country,
		   LP_rtv_record.jurisdiction_code,
		   LP_rtv_record.pcode;
	  close C_GET_SUP_INFO;
   else
	  open C_GET_COURIER;
    fetch C_GET_COURIER into L_ret_courier;
	  close C_GET_COURIER;
   end if;
   if LP_valid_rtv = 'N' then
		insert into rtv_head (rtv_order_no,
                              supplier,
                              status_ind,
                              store,
                              wh,
                              total_order_amt,
                              ship_to_add_1,
                              ship_to_add_2,
                              ship_to_add_3,
                              ship_to_city,
                              state,
                              ship_to_country_id,
                              ship_to_jurisdiction_code,
                              ship_to_pcode,
                              ret_auth_num,
                              courier,
                              freight,
                              created_date,
                              completed_date,
                              restock_pct,
                              restock_cost,
                              ext_ref_no,
                              comment_desc)
					  values (LP_rtv_record.rtv_order_no,
                              LP_rtv_record.supplier,
                              15,   -- shipped
                              LP_rtv_record.store,
                              decode(LP_distribute, 'Y', LP_rtv_record.physical_wh, LP_rtv_record.wh),
                              LP_rtv_record.total_order_amt_unit_based,
                              LP_rtv_record.ship_addr1,
                              LP_rtv_record.ship_addr2,
                              LP_rtv_record.ship_addr3,
                              LP_rtv_record.city,
                              LP_rtv_record.state,
                              LP_rtv_record.country,
                              LP_rtv_record.jurisdiction_code,
                              LP_rtv_record.pcode,
                              LP_rtv_record.ret_auth_num,
                              L_ret_courier,
                              NULL,
                              LP_rtv_record.tran_date,
                              LP_rtv_record.tran_date,
                              LP_rtv_record.restock_pct,
                              LP_rtv_record.total_order_amt_unit_based * (LP_rtv_record.restock_pct / 100), -- restock cost
                              LP_rtv_record.ext_ref_no,
                              LP_rtv_record.comments);
   end if;
   LP_valid_rtv := 'Y';
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END VALIDATE_RTV_HEAD;
--------------------------------------------------------------------------------------------------------
FUNCTION DETERMINE_RTV_COST(O_error_message     IN OUT     VARCHAR2)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'RTV_SQL.DETERMINE_RTV_COST';

   L_avg_weight    ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_total_cost    ITEM_LOC_SOH.AV_COST%TYPE;

   cursor C_RTV_UNIT_COST is
   select unit_cost
     from rtv_detail
    where rtv_order_no = LP_rtv_record.rtv_order_no
      and item = LP_rtv_record.detail_TBL.items(LP_index);

BEGIN
   -- LP_rtv_record.detail_TBL.unit_cost_exts(LP_index) is from the message and in location currency.
   -- RTV goods will be valued at LP_rtv_record.detail_TBL.unit_cost_exts(LP_index) if it is defined.
   -- If not, it will be valued at RTV_DETAIL.unit_cost, which is in supplier currency.
   -- If RTV_DETAIL.unit_cost is not found, depending on the system level setting on
   -- INV_MOVE_UNIT_OPTIONS.RTV_UNIT_COST_IND, either last receipt cost, or from loc's standard unit cost
   -- or from loc's WAC will be used. Last receipt cost, standard unit cost and WAC are all in location currency.

   -- re-initialize to NULL for MC distributed WH processing
   LP_rtv_record.detail_TBL.unit_cost_supps(LP_index) := NULL;
   LP_rtv_record.detail_TBL.unit_cost_locs(LP_index) := NULL;
   LP_rtv_record.detail_TBL.extended_base_cost(LP_index) := NULL;

   if LP_rtv_record.detail_TBL.unit_cost_exts(LP_index) is NOT NULL then
      -- use unit cost in the message
      LP_rtv_record.detail_TBL.unit_cost_locs(LP_index) := LP_rtv_record.detail_TBL.unit_cost_exts(LP_index); -- in location currency
   else
      -- unit_cost not in the message, use RTV_DETAIL.UNIT_COST
      open C_RTV_UNIT_COST;
      fetch C_RTV_UNIT_COST into LP_rtv_record.detail_TBL.unit_cost_supps(LP_index);  -- in supplier currency
      close C_RTV_UNIT_COST;
      ---
   end if;

   if LP_rtv_record.detail_TBL.unit_cost_supps(LP_index) is NULL and LP_rtv_record.detail_TBL.unit_cost_locs(LP_index) is NULL then
      -- Get RTV Cost
      if RTV_SQL.DETERMINE_RTV_COST(O_error_message,
                                    LP_rtv_record.detail_TBL.unit_cost_locs(LP_index),
                                    LP_rtv_record.detail_TBL.unit_cost_supps(LP_index),
                                    LP_rtv_record.detail_TBL.items(LP_index),
                                    LP_rtv_record.loc,  -- virtual loc from distribution
                                    LP_rtv_record.loc_type,
                                    LP_rtv_record.supplier) = FALSE then
         return FALSE;
      end if;
   end if;

   -- Convert values into both supplier currency and location currency.
   -- If Unit Cost is from message, Convert value to Supplier Currency.
   -- If the cost is from RTV Detail, Convert value to Location Currency.

   if LP_rtv_record.detail_TBL.unit_cost_supps(LP_index) is NULL then
      -- convert to supplier currency
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          LP_rtv_record.loc,
                                          LP_rtv_record.loc_type,
                                          NULL,
                                          LP_rtv_record.supplier,
                                          'V',
                                          NULL,
                                          LP_rtv_record.detail_TBL.unit_cost_locs(LP_index),
                                          LP_rtv_record.detail_TBL.unit_cost_supps(LP_index),
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return false;
      end if;
   end if;

   if LP_rtv_record.detail_TBL.unit_cost_locs(LP_index) is NULL then
      -- convert to location currency
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          LP_rtv_record.supplier,
                                          'V',
                                          NULL,
                                          LP_rtv_record.loc,
                                          LP_rtv_record.loc_type,
                                          NULL,
                                          LP_rtv_record.detail_TBL.unit_cost_supps(LP_index),
                                          LP_rtv_record.detail_TBL.unit_cost_locs(LP_index),
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return false;
      end if;
   end if;
   LP_rtv_record.detail_TBL.extended_base_cost(LP_index) := LP_rtv_record.detail_TBL.unit_cost_locs(LP_index);

   -- LP_rtv_record.detail_TBL.unit_cost_supps(LP_index) can be based off the WACs from different virtual locations.
   -- The WACs can be different for different virtual locations.
   -- Total order amt should be the sum of total costs across different virtual locations.
   -- Total order amt should be in supplier currency and should take into account of weight.

   if LP_rtv_record.im_row_TBL(LP_index).simple_pack_ind = 'Y' and LP_rtv_record.im_row_TBL(LP_index).catch_weight_ind = 'Y' then
      if LP_rtv_record.detail_TBL.weight_cuoms(LP_index) is NULL then
         if ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                                  L_avg_weight,
                                                  LP_rtv_record.detail_TBL.items(LP_index),
                                                  LP_rtv_record.loc,
                                                  LP_rtv_record.loc_type) = FALSE then

            return FALSE;
         end if;
         LP_rtv_record.detail_TBL.weight_cuoms(LP_index) := L_avg_weight * LP_rtv_record.detail_TBL.returned_qtys(LP_index);

         if NOT ITEM_ATTRIB_SQL.GET_CATCH_WEIGHT_UOM(O_error_message,
                                                     LP_rtv_record.detail_TBL.cuoms(LP_index),
                                                     LP_rtv_record.detail_TBL.items(LP_index)) then
            return FALSE;
         end if;
      end if;

      if CATCH_WEIGHT_SQL.CALC_TOTAL_COST(O_error_message,
                                          L_total_cost,
                                          LP_rtv_record.detail_TBL.items(LP_index),
                                          LP_rtv_record.loc,
                                          LP_rtv_record.loc_type,
                                          LP_rtv_record.detail_TBL.unit_cost_supps(LP_index),  -- supplier currency
                                          LP_rtv_record.detail_TBL.returned_qtys(LP_index),    -- to virtual loc
                                          LP_rtv_record.detail_TBL.weight_cuoms(LP_index),
                                          NULL) = FALSE then  -- uom, default to cost uom
         return FALSE;
      end if;

      LP_rtv_record.total_order_amt_wgt_based := NVL(LP_rtv_record.total_order_amt_wgt_based, 0) + L_total_cost;
      LP_rtv_record.total_order_amt_unit_based := NVL(LP_rtv_record.total_order_amt_unit_based, 0) + LP_rtv_record.detail_TBL.returned_qtys(LP_index) * LP_rtv_record.detail_TBL.unit_cost_supps(LP_index);
   else
      L_total_cost := LP_rtv_record.detail_TBL.returned_qtys(LP_index) * LP_rtv_record.detail_TBL.unit_cost_supps(LP_index);

      LP_rtv_record.total_order_amt_wgt_based := NVL(LP_rtv_record.total_order_amt_wgt_based, 0) + L_total_cost;
      LP_rtv_record.total_order_amt_unit_based := NVL(LP_rtv_record.total_order_amt_unit_based, 0) + L_total_cost;
   end if;
   LP_item_total_cost := LP_item_total_cost + L_total_cost;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END DETERMINE_RTV_COST;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_RTV_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)

   RETURN BOOLEAN IS

   L_program               VARCHAR2(60) := 'RTV_SQL.VALIDATE_RTV_DETAIL';

   L_exist                 VARCHAR2(1) := NULL;
   L_receive_as_type       ITEM_LOC.RECEIVE_AS_TYPE%TYPE;

   cursor C_INV_STATUS is
      select inv_status
        from inv_status_codes
       where inv_status_code = LP_rtv_record.detail_TBL.from_disps(LP_index);

   cursor C_ITEM_SUPP_EXIST is
      select 'x'
        from item_supplier
       where item = LP_rtv_record.detail_TBL.items(LP_index)
         and supplier = LP_rtv_record.supplier;

   cursor C_ITEM_LOC_EXISTS is
   select 'x'
     from item_loc_soh
    where loc = LP_rtv_record.loc
      and item = LP_rtv_record.detail_TBL.items(LP_index);

   cursor C_RECEIVE_AS_TYPE is
   select NVL(receive_as_type, 'E')
     from item_loc
    where item = LP_rtv_record.detail_TBL.items(LP_index)
      and loc = LP_rtv_record.loc;

   cursor C_REASON_EXISTS is
   select 'x'
     from code_detail
    where code_type = 'RTVR'
      and code = LP_rtv_record.detail_TBL.reasons(LP_index);

BEGIN
   ---
   if LP_rtv_record.detail_TBL.inv_statuses(LP_index) is NULL then
      open C_INV_STATUS;
      fetch C_INV_STATUS into LP_rtv_record.detail_TBL.inv_statuses(LP_index);
      if C_INV_STATUS%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DISPOSITION', NULL, NULL, NULL);
         close C_INV_STATUS;
         return FALSE;
      end if;
      close C_INV_STATUS;
   end if;
   ---
   L_exist := NULL;
   open C_ITEM_SUPP_EXIST;
   fetch C_ITEM_SUPP_EXIST into L_exist;
   if C_ITEM_SUPP_EXIST%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_SUP', NULL, NULL, NULL);
      close C_ITEM_SUPP_EXIST;
      return FALSE;
   end if;
   close C_ITEM_SUPP_EXIST;
   ---
   L_exist := NULL;
   open C_ITEM_LOC_EXISTS;
   fetch C_ITEM_LOC_EXISTS into L_exist;
   close C_ITEM_LOC_EXISTS;
   ---
   if L_exist is NULL then
      if NEW_ITEM_LOC(O_error_message,
                      LP_rtv_record.detail_TBL.items(LP_index),
                      LP_rtv_record.loc,
                      NULL, NULL, LP_rtv_record.loc_type, NULL,
                      NULL, NULL, NULL,
                      NULL, NULL,
                      NULL, NULL, NULL, NULL,
                      NULL,
                      NULL, NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL, NULL,
                      NULL, NULL, NULL, NULL, NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   open C_RECEIVE_AS_TYPE;
   fetch C_RECEIVE_AS_TYPE into L_receive_as_type;
   close C_RECEIVE_AS_TYPE;
   ---
   -- At warehouses, packs can only be received as packs not eaches.
   if LP_rtv_record.loc_type = 'W' and
      LP_rtv_record.im_row_TBL(LP_index).pack_ind = 'Y' and
      L_receive_as_type = 'E' then

      O_error_message := SQL_LIB.CREATE_MSG('NO_PACK_EACH',
                                            LP_rtv_record.detail_TBL.items(LP_index) ,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if LP_rtv_record.detail_TBL.reasons(LP_index) is NOT NULL then
      L_exist := NULL;
      open C_REASON_EXISTS;
      fetch C_REASON_EXISTS into L_exist;
      close C_REASON_EXISTS;
      ---
      if L_exist is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               L_program,
                                               'reason',
                                               LP_rtv_record.detail_TBL.reasons(LP_index));
         return FALSE;
      end if;
   else
      LP_rtv_record.detail_TBL.reasons(LP_index) := 'W';
   end if;

   -- determine RTV cost
   if RTV_SQL.DETERMINE_RTV_COST(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END VALIDATE_RTV_DETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION BUILD_RTV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)

   RETURN BOOLEAN IS

   L_program       VARCHAR2(30) := 'RTV_SQL.BUILD_RTV';

   L_total_order_amt     RTV_HEAD.TOTAL_ORDER_AMT%TYPE;
   L_total_order_amt1    RTV_HEAD.TOTAL_ORDER_AMT%TYPE;
   L_order_amt           RTV_HEAD.TOTAL_ORDER_AMT%TYPE;
   L_total_restock_cost  RTV_HEAD.RESTOCK_COST%TYPE;
   L_total_qty_returned  RTV_DETAIL.QTY_RETURNED%TYPE;

   L_rowid               ROWID := NULL;
   L_seq_no              RTV_DETAIL.SEQ_NO%TYPE;
   L_ret_courier         SUPS.RET_COURIER%TYPE;

   db_ext_ref   rtv_head.ext_ref_no%TYPE;
   db_wh        wh.wh%TYPE;
   db_status    rtv_head.status_ind%TYPE;
   L_vdate      PERIOD.VDATE%TYPE;
   L_count      NUMBER(3) := 0;

   RECORD_LOCKED EXCEPTION;
   PRAGMA EXCEPTION_INIT(Record_Locked,-54);

   L_original_unit_cost  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_completed_date      RTV_HEAD.COMPLETED_DATE%TYPE;

   cursor C_DETAIL_EXISTS is
   select rowid
     from rtv_detail
    where rtv_order_no = LP_rtv_record.rtv_order_no
      and item = LP_rtv_record.detail_TBL.items(LP_index)
      and reason = LP_rtv_record.detail_TBL.reasons(LP_index)
      and ((inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)) or
           (inv_status is NULL and LP_rtv_record.detail_TBL.inv_statuses(LP_index) is NULL));

   cursor C_GET_ITEM_COUNT is
   select count(item)
     from rtv_detail
    where rtv_order_no = LP_rtv_record.rtv_order_no
      and item = LP_rtv_record.detail_TBL.items(LP_index);

   cursor C_RTV_DETAIL_EXISTS is
   select rowid
     from rtv_detail
    where rtv_order_no = LP_rtv_record.rtv_order_no
      and item = LP_rtv_record.detail_TBL.items(LP_index);

   cursor C_LOCK_RTV_DETAIL is
   select 'x'
     from rtv_detail
    where rowid = L_rowid
      for update nowait;

   cursor C_MAX_SEQ_NO is
   select max(seq_no) + 1
     from rtv_detail
    where rtv_order_no = LP_rtv_record.rtv_order_no;

   cursor C_LOCK_RTV_HEAD is
   select 'x'
     from rtv_head
    where rtv_order_no = LP_rtv_record.rtv_order_no
      for update nowait;

   cursor C_TOTAL_COST is
   select sum(unit_cost*NVL(qty_returned, 0)),
          sum(unit_cost*NVL(qty_returned, 0)*NVL(restock_pct, 0)/100)
     from rtv_detail
    where rtv_order_no = LP_rtv_record.rtv_order_no;

   cursor C_GET_COURIER is
      select ret_courier
        from sups
       where supplier = LP_rtv_record.supplier;

   cursor C_GET_SUP_INFO is
      select s.ret_courier,
             a.add_1,
             a.add_2,
             a.add_3,
             a.city,
             a.state,
             a.country_id,
             a.jurisdiction_code,
             a.post
        from sups s,
             addr a
       where s.supplier = LP_rtv_record.supplier
         and a.key_value_1 = to_char(LP_rtv_record.supplier)
         and module = 'SUPP'
         and s.ret_allow_ind = 'Y'
         and a.addr_type = '03';

   cursor C_ORDER_AMOUNT is
      select total_order_amt,
             completed_date
          from rtv_head
          where rtv_order_no = LP_rtv_record.rtv_order_no;

   cursor C_VDATE is
      select vdate
        from period;

BEGIN
   open C_MAX_SEQ_NO;
   fetch C_MAX_SEQ_NO into L_seq_no;
   close C_MAX_SEQ_NO;

   --- if multichannel is turned on, then the physical warehouse is stored on rtv_head and the qty_returned
   --- field on rtv_detail should reflect this by updating that field with the full amount returned to the
   --- physical warehouse (which is stored globally in LP_rtv_record.detail_TBL.mc_returned_qtys(LP_index)).
   --- LP_rtv_record.detail_TBL.returned_qtys(LP_index) will contain the
   --- value returned to the last virtual warehouse returned from distribution_sql.

   if LP_distribute = 'Y' then
      L_total_qty_returned := LP_rtv_record.detail_TBL.mc_returned_qtys(LP_index);
   else
      L_total_qty_returned := LP_rtv_record.detail_TBL.returned_qtys(LP_index);
   end if;

   if LP_valid_rtv = 'Y' then
      ---
      open C_GET_ITEM_COUNT;
      fetch C_GET_ITEM_COUNT into L_count;
      close C_GET_ITEM_COUNT;

      if L_count > 1 then
         open C_DETAIL_EXISTS;
         fetch C_DETAIL_EXISTS into L_rowid;
         close C_DETAIL_EXISTS;
      else
         open C_RTV_DETAIL_EXISTS;
         fetch C_RTV_DETAIL_EXISTS into L_rowid;
         close C_RTV_DETAIL_EXISTS;
      end if;
      
      open  C_order_amount;
      fetch C_order_amount into L_total_order_amt1,
                                L_completed_date;
      close C_order_amount;
      ---
      if L_rowid is NOT NULL then
         ---
         open C_LOCK_RTV_DETAIL;
         close C_LOCK_RTV_DETAIL;
         ---
         -- if unit_cost is in the message, overwrite rtv_detail.unit_cost with it (in supplier currency)
         update rtv_detail
            set qty_returned = NVL(qty_returned, 0) + L_total_qty_returned,
                unit_cost = decode(LP_rtv_record.detail_TBL.unit_cost_exts(LP_index), NULL, unit_cost, LP_rtv_record.detail_TBL.unit_cost_supps(LP_index)),
                updated_by_rms_ind = 'N'
          where rowid = L_rowid;

         update rtv_detail
            set qty_cancelled = decode(sign(qty_requested - nvl(qty_returned, 0)), -1, 0, (qty_requested - nvl(qty_returned, 0))),
                updated_by_rms_ind = 'N',
                reason = LP_rtv_record.detail_TBL.reasons(LP_index),
                inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)
          where rowid = L_rowid;
         ---
      else
         --- Retrieve original unit cost
         if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                           LP_rtv_record.detail_TBL.items(LP_index),
                                           LP_rtv_record.loc,
                                           LP_rtv_record.loc_type,
                                           L_original_unit_cost) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             LP_rtv_record.loc,
                                             LP_rtv_record.loc_type,
                                             NULL,
                                             LP_rtv_record.supplier,
                                             'V',
                                             NULL,
                                             L_original_unit_cost,
                                             L_original_unit_cost,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
             return FALSE;
         end if;
         
         L_order_amt := (LP_rtv_record.total_order_amt_unit_based)-NVL(L_total_order_amt1,0);
         LP_rtv_record.detail_TBL.seq_nos(LP_index) := nvl(L_seq_no,1);
         insert into rtv_detail (rtv_order_no,
                                 seq_no,
                                 item,
                                 shipment,
                                 inv_status,
                                 qty_requested,
                                 qty_returned,
                                 qty_cancelled,
                                 unit_cost,
                                 reason,
                                 restock_pct,
                                 publish_ind,
                                 original_unit_cost,
                                 updated_by_rms_ind)
                         values (LP_rtv_record.rtv_order_no,
                                 NVL(L_seq_no, 1),
                                 LP_rtv_record.detail_TBL.items(LP_index),
                                 NULL,
                                 LP_rtv_record.detail_TBL.inv_statuses(LP_index),
                                 L_total_qty_returned,
                                 L_total_qty_returned,
                                 0,
                                 -- if multichannel is turned on, LP_rtv_record.detail_TBL.unit_cost_supps(LP_index) holds the unit cost
                                 -- for the last virtual location. Use total order cost divided by
                                 -- total qty at the physical level to get the rtv unit cost.
                                 decode(LP_distribute, 'Y', L_order_amt/LP_rtv_record.detail_TBL.mc_returned_qtys(LP_index),
                                        LP_rtv_record.detail_TBL.unit_cost_supps(LP_index)),
                                 LP_rtv_record.detail_TBL.reasons(LP_index),
                                 LP_rtv_record.detail_TBL.restock_pcts(LP_index),
                                 'Y',
                                 L_original_unit_cost,
                                 'N');
      end if;
      ---
      open C_LOCK_RTV_HEAD;
      close C_LOCK_RTV_HEAD;

      open C_VDATE;
      fetch C_VDATE into L_vdate;
      close C_VDATE;

      -- recalculate total order amount and restock cost based on the qty returned
      -- Note: total_order_amt will be unit based, not weight based, even for a catch weight item.
      open C_TOTAL_COST;
      fetch C_TOTAL_COST into L_total_order_amt,
                              L_total_restock_cost;
      close C_TOTAL_COST;

      -- set header level cost info and mark rtv as shipped.
      update rtv_head
         set status_ind = 15,  -- shipped
             completed_date = case when L_completed_date > LP_rtv_record.tran_date 
                                      then L_completed_date
                                   else LP_rtv_record.tran_date
                               end,
             total_order_amt = L_total_order_amt,
             restock_cost = L_total_restock_cost,
             ret_auth_num = LP_rtv_record.ret_auth_num,
             comment_desc = LP_rtv_record.comments
       where rtv_order_no = LP_rtv_record.rtv_order_no;
      ---
   else
      ---
      if LP_rtv_record.ship_addr1 is NULL or LP_rtv_record.city is NULL or LP_rtv_record.country is NULL then
         -- address info on message is in complete, use supplier address
         open C_GET_SUP_INFO;
         fetch C_GET_SUP_INFO into L_ret_courier,
                                   LP_rtv_record.ship_addr1,
                                   LP_rtv_record.ship_addr2,
                                   LP_rtv_record.ship_addr3,
                                   LP_rtv_record.city,
                                   LP_rtv_record.state,
                                   LP_rtv_record.country,
                                   LP_rtv_record.jurisdiction_code,
                                   LP_rtv_record.pcode;
         close C_GET_SUP_INFO;
      else
         open C_GET_COURIER;
         fetch C_GET_COURIER into L_ret_courier;
         close C_GET_COURIER;
      end if;
      ---
      if LP_index = 1 then
         -- we only want to insert the header information the first time...

         insert into rtv_head (rtv_order_no,
                               supplier,
                               status_ind,
                               store,
                               wh,
                               total_order_amt,
                               ship_to_add_1,
                               ship_to_add_2,
                               ship_to_add_3,
                               ship_to_city,
                               state,
                               ship_to_country_id,
                               ship_to_jurisdiction_code,
                               ship_to_pcode,
                               ret_auth_num,
                               courier,
                               freight,
                               created_date,
                               completed_date,
                               restock_pct,
                               restock_cost,
                               ext_ref_no,
                               comment_desc)
                        values (LP_rtv_record.rtv_order_no,
                               LP_rtv_record.supplier,
                               15,   -- shipped
                               LP_rtv_record.store,
                               decode(LP_distribute, 'Y', LP_rtv_record.physical_wh, LP_rtv_record.wh),
                               LP_rtv_record.total_order_amt_unit_based,
                               LP_rtv_record.ship_addr1,
                               LP_rtv_record.ship_addr2,
                               LP_rtv_record.ship_addr3,
                               LP_rtv_record.city,
                               LP_rtv_record.state,
                               LP_rtv_record.country,
                               LP_rtv_record.jurisdiction_code,
                               LP_rtv_record.pcode,
                               LP_rtv_record.ret_auth_num,
                               L_ret_courier,
                               NULL,
                               LP_rtv_record.tran_date,
                               LP_rtv_record.tran_date,
                               LP_rtv_record.restock_pct,
                               LP_rtv_record.total_order_amt_unit_based * (LP_rtv_record.restock_pct / 100), -- restock cost
                               LP_rtv_record.ext_ref_no,
                               LP_rtv_record.comments);

         /*  Ensure that additional detail records will be checked for existence and updated
             rather than inserted in the case of a duplicate orderable item   */
         if sql%FOUND then
            LP_valid_rtv := 'Y';
         end if;

      end if;
      --- Retrieve original unit cost
      if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                        LP_rtv_record.detail_TBL.items(LP_index),
                                        LP_rtv_record.loc,
                                        LP_rtv_record.loc_type,
                                        L_original_unit_cost) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          LP_rtv_record.loc,
                                          LP_rtv_record.loc_type,
                                          NULL,
                                          LP_rtv_record.supplier,
                                          'V',
                                          NULL,
                                          L_original_unit_cost,
                                          L_original_unit_cost,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      LP_rtv_record.detail_TBL.seq_nos(LP_index) := nvl(L_seq_no,1);	  
      insert into rtv_detail (rtv_order_no,
                              seq_no,
                              item,
                              shipment,
                              inv_status,
                              qty_requested,
                              qty_returned,
                              qty_cancelled,
                              unit_cost,
                              reason,
                              restock_pct,
                              publish_ind,
                              original_unit_cost,
                              updated_by_rms_ind)
                       values (LP_rtv_record.rtv_order_no,
                              NVL(L_seq_no, 1),
                              LP_rtv_record.detail_TBL.items(LP_index),
                              NULL,
                              LP_rtv_record.detail_TBL.inv_statuses(LP_index),
                              L_total_qty_returned,
                              L_total_qty_returned,
                              0,
                              -- if multichannel is turned on, LP_rtv_record.detail_TBL.unit_cost_supps(LP_index) holds the unit cost
                              -- for the last virtual location. Use total order cost divided by
                              -- total qty at the physical level to get the rtv unit cost.
                              decode(LP_distribute, 'Y', LP_rtv_record.total_order_amt_unit_based/LP_rtv_record.detail_TBL.mc_returned_qtys(LP_index), LP_rtv_record.detail_TBL.unit_cost_supps(LP_index)),
                              LP_rtv_record.detail_TBL.reasons(LP_index),
                              LP_rtv_record.detail_TBL.restock_pcts(LP_index),
                              'Y',
                              L_original_unit_cost,
                              'N');
   end if;
   -- insert or update for a container item associated with the deposit item
   if LP_rtv_record.im_row_TBL(LP_index).deposit_item_type = LP_contents_code and
      LP_rtv_record.im_row_TBL(LP_index).container_item is NOT NULL then
      if RTV_SQL.PROCESS_DEPOSIT_ITEM(O_error_message,
                                      NULL,  -- seq_no
                                      LP_rtv_record.rtv_order_no,            --I_rtv_order_no
                                      LP_rtv_record.supplier,                --I_supplier
                                      LP_rtv_record.im_row_TBL(LP_index).container_item,          --I_container_item
                                      LP_rtv_record.detail_TBL.items(LP_index),                    --I_contents_item
                                      LP_rtv_record.loc,                                           --I_loc
                                      LP_rtv_record.loc_type,                                      -- I_loc_type
                                      L_total_qty_returned,       --I_qty
                                      LP_rtv_record.detail_TBL.reasons(LP_index),                  --I_reason
                                      LP_rtv_record.detail_TBL.inv_statuses(LP_index),              --I_inv_status
                                      'N',                         --I_online_ind
                                      'N') = FALSE then           --I_approve_ind
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'RTV_HEAD',
                                             to_char(LP_rtv_record.rtv_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END BUILD_RTV;
--------------------------------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_RTV(O_error_message     IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item_xform_ind    IN         ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                        I_sellable_ind      IN         ITEM_MASTER.SELLABLE_IND%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(30) := 'RTV_SQL.DISTRIBUTE_RTV';

   L_dist_tab       DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   L_dist_qty       RTV_DETAIL.QTY_RETURNED%TYPE;
   L_rtv_order_no   RTV_DETAIL.RTV_ORDER_NO%TYPE;
   L_exist          VARCHAR2(1);

   cursor C_ITEM_LOC_WH is
   select 'x'
     from item_loc
    where item = LP_rtv_record.detail_TBL.items(LP_index)
      and loc in (select wh
                    from wh
                   where physical_wh != wh
                     and physical_wh = LP_rtv_record.physical_wh);

   cursor C_VWH is
   select wh
     from wh
    where physical_wh != wh
      and physical_wh = LP_rtv_record.physical_wh;

   cursor C_RTV_DETAIL is
   select rtv_order_no,
          seq_no
     from rtv_detail
    where rtv_order_no = LP_rtv_record.rtv_order_no
      and item = LP_rtv_record.detail_TBL.items(LP_index)
      and reason = LP_rtv_record.detail_TBL.reasons(LP_index)
      and ((inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)) or
           (inv_status is NULL and LP_rtv_record.detail_TBL.inv_statuses(LP_index) is NULL));

   cursor C_INV_STATUS_CODES is
   select inv_status
     from inv_status_codes
    where inv_status_code = LP_rtv_record.detail_TBL.from_disps(LP_index);

BEGIN
   --- These values are used when building the rtv when multichannel is on.
   LP_rtv_record.physical_wh := LP_rtv_record.loc;
   LP_rtv_record.detail_TBL.mc_returned_qtys(LP_index) := LP_rtv_record.detail_TBL.returned_qtys(LP_index);
   LP_rtv_record.detail_TBL.mc_weight_cuoms(LP_index) := LP_rtv_record.detail_TBL.weight_cuoms(LP_index);  -- total weight

   --- Fetching inv status for the from disposition.
   open C_INV_STATUS_CODES;
   fetch C_INV_STATUS_CODES into LP_rtv_record.detail_TBL.inv_statuses(LP_index);
   if C_INV_STATUS_CODES%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DISPOSITION', NULL, NULL, NULL);
      close C_INV_STATUS_CODES;
      return FALSE;
   end if;
   close C_INV_STATUS_CODES;

   ---
   if LP_rtv_record.loc_type = 'W' then
      L_exist := NULL;

      open C_ITEM_LOC_WH;
      fetch C_ITEM_LOC_WH into L_exist;
      close C_ITEM_LOC_WH;
      ---
      if L_exist is NULL then
         for rec in C_VWH LOOP
            if NEW_ITEM_LOC(O_error_message,
                            LP_rtv_record.detail_TBL.items(LP_index),
                            rec.wh,
                            NULL, NULL, LP_rtv_record.loc_type, NULL,
                            NULL, NULL, NULL,
                            NULL, NULL,
                            NULL, NULL, NULL, NULL,
                            NULL,
                            NULL, NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL, NULL,
                            NULL, NULL, NULL, NULL, NULL) = FALSE then
               return FALSE;
            end if;
         END LOOP;
      end if;
   end if;
   --- RTV is created in RMS
   if LP_valid_rtv = 'Y' then
      -- Seq number for the order number, item, reason and inv status combination.
      open C_RTV_DETAIL;
      fetch C_RTV_DETAIL into L_rtv_order_no,LP_rtv_record.detail_TBL.seq_nos(LP_index);
      close C_RTV_DETAIL;
   end if;

   if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                  L_dist_tab,
                                  LP_rtv_record.detail_TBL.items(LP_index),
                                  LP_rtv_record.physical_wh,
                                  LP_rtv_record.detail_TBL.mc_returned_qtys(LP_index),
                                  'RTV',
                                  LP_rtv_record.detail_TBL.inv_statuses(LP_index),
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NVL(L_rtv_order_no,LP_rtv_record.rtv_order_no),   
                                  NVL(LP_rtv_record.detail_TBL.seq_nos(LP_index),NULL)) = FALSE then
      return false;
   end if;
   ---
   if L_dist_tab.count = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DIST_WH', NULL, NULL, NULL);
      return FALSE;
   end if;
   ---
   FOR i in L_dist_tab.first .. L_dist_tab.last LOOP
      ---
      LP_rtv_record.wh := L_dist_tab(i).wh;
      LP_rtv_record.loc := LP_rtv_record.wh;
      LP_rtv_record.detail_TBL.returned_qtys(LP_index) := L_dist_tab(i).dist_qty;
      LP_rtv_record.detail_TBL.weight_cuoms(LP_index) := LP_rtv_record.detail_TBL.mc_weight_cuoms(LP_index) * (LP_rtv_record.detail_TBL.returned_qtys(LP_index)/LP_rtv_record.detail_TBL.mc_returned_qtys(LP_index));
      ---
      if RTV_SQL.VALIDATE_RTV_DETAIL(O_error_message) = FALSE then
         return false;
      end if;
      ---
      if RTV_SQL.INVENTORY(O_error_message,
                           LP_upd_rtv_qty_ind,  -- Set in VALIDATE_RTV_HEAD
                           I_item_xform_ind,
                           I_sellable_ind) = FALSE then
         return false;
      end if;
   end LOOP;

   LP_dist_tab_1 := L_dist_tab;   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END DISTRIBUTE_RTV;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_DEPOSIT_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_seq_no          IN       RTV_DETAIL.SEQ_NO%TYPE,
                              I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE,
                              I_supplier        IN       RTV_HEAD.SUPPLIER%TYPE,
                              I_container_item  IN       RTV_DETAIL.ITEM%TYPE,
                              I_contents_item   IN       RTV_DETAIL.ITEM%TYPE,
                              I_loc             IN       ITEM_LOC.LOC%TYPE,
                              I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                              I_qty             IN       RTV_DETAIL.QTY_REQUESTED%TYPE,
                              I_reason          IN       RTV_DETAIL.REASON%TYPE,
                              I_inv_status      IN       RTV_DETAIL.INV_STATUS%TYPE,
                              I_online_ind      IN       VARCHAR2,
                              I_approve_ind     IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program                  VARCHAR2(60) := 'RTV_SQL.PROCESS_DEPOSIT_ITEM';

   L_rowid                    ROWID;
   L_seq_no                   RTV_DETAIL.SEQ_NO%TYPE;

   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_Locked, -54);

   L_original_unit_cost  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := 0;
   L_updated_by_rms_ind  VARCHAR2 (1);

   cursor C_LOCK_RTV_DETAIL is
   select rowid
     from rtv_detail
    where rtv_order_no = I_rtv_order_no
      and item = I_container_item
      and reason = I_reason
      and ((inv_status = I_inv_status) or
           (inv_status is NULL and I_inv_status is NULL))
      for update nowait;

   cursor C_MAX_SEQ_NO is
   select max(seq_no) + 1
     from rtv_detail
    where rtv_order_no = I_rtv_order_no;

BEGIN

   -- This function serves the purpose of adding/updating the container item for a contents deposit item
   -- in RTV_DETAIL table for online processing as well as RTV subscription processing.
   -- The online processing creates and updates an RTV in approved status; the RTV subscription processing
   -- creates and updates an RTV in approved or shipped status. When updating an RTV in approved status,
   -- qty_requested column needs to be updated; when updating an RTV in shipped status, qty_returned
   -- and qty_cancelled columns needs to be updated. For this reason 2 sets of update statements are used,
   -- 1 for approved RTVs and 1 for shipped RTVs. The I_qty_returned parameter should be the full amount
   -- returned to the physical warehouse if multichannel is turned on.

   open C_LOCK_RTV_DETAIL;
   fetch C_LOCK_RTV_DETAIL into L_rowid;
   close C_LOCK_RTV_DETAIL;

   if I_online_ind = 'Y' then
      L_updated_by_rms_ind := 'Y';
   else
      L_updated_by_rms_ind := 'N';
   end if;

   if L_rowid is NOT NULL then
      if I_approve_ind = 'N' then  --- API processing for shipped RTVs
         update rtv_detail
            set qty_returned = (nvl(qty_returned, 0) + I_qty),
                updated_by_rms_ind = L_updated_by_rms_ind
          where rowid = L_rowid;

         update rtv_detail
            set qty_cancelled = decode(sign(qty_requested - nvl(qty_returned, 0)), -1, 0, (qty_requested - nvl(qty_returned, 0))),
                updated_by_rms_ind = L_updated_by_rms_ind
          where rowid = L_rowid;

      else --- Online processing or API processing for approved RTVs, only qty_requested is updated
         update rtv_detail
            set qty_requested = (nvl(qty_requested, 0) + I_qty),
                updated_by_rms_ind = L_updated_by_rms_ind
          where rowid = L_rowid;
      end if;
   else --- Add the container item
      if I_seq_no is NOT NULL then
         L_seq_no := I_seq_no;
      else
         open C_MAX_SEQ_NO;
         fetch C_MAX_SEQ_NO into L_seq_no;
         close C_MAX_SEQ_NO;
      end if;
      --- Retrieve original unit cost
      if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                        I_container_item,
                                        I_loc,
                                        I_loc_type,
                                        L_original_unit_cost) = FALSE then
         return FALSE;
      end if;
      ---Convert the Cost in Location Currency to Supplier Currency
      if L_original_unit_cost is NOT NULL then
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_loc,
                                             I_loc_type,
                                             NULL,
                                             I_supplier,
                                             'V',
                                             NULL,
                                             L_original_unit_cost,
                                             L_original_unit_cost,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return false;
         end if;
      end if;

      insert into rtv_detail (rtv_order_no,
                              seq_no,
                              item,
                              shipment,
                              inv_status,
                              qty_requested,
                              qty_returned,
                              qty_cancelled,
                              unit_cost,
                              reason,
                              publish_ind,
                              restock_pct,
                              original_unit_cost,
                              updated_by_rms_ind)
                      select  rtv_order_no,
                              L_seq_no,
                              I_container_item,
                              NULL,
                              inv_status,
                              qty_requested,
                              qty_returned,
                              0,
                              L_original_unit_cost,
                              reason,
                              publish_ind,
                              restock_pct,
                              L_original_unit_cost,
                              L_updated_by_rms_ind
                        from  rtv_detail
                       where  item = I_contents_item
                         and  rtv_order_no = I_rtv_order_no
                         and  reason = I_reason
                         and  ((inv_status = I_inv_status) or
                              (inv_status is NULL and I_inv_status is NULL));
      end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'RTV_DETAIL',
                                             to_char(I_rtv_order_no),
                                             I_container_item);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END PROCESS_DEPOSIT_ITEM;
--------------------------------------------------------------------------------------------------------
FUNCTION GET_RESTOCK_PCT(O_error_message        IN OUT     VARCHAR2)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(30) := 'RTV_SQL.GET_RESTOCK_PCT';

   cursor C_restock_pct is
      select restock_pct
        from rtv_detail
       where rtv_order_no = LP_rtv_record.rtv_order_no
         and item = LP_rtv_record.detail_TBL.items(LP_index);

   cursor C_restock_pct_hd is
      select restock_pct
        from rtv_head
       where rtv_order_no = LP_rtv_record.rtv_order_no;

   cursor C_handling_pct is
      select NVL(handling_pct, 0)
        from sups
       where supplier = LP_rtv_record.supplier;

BEGIN
   -- This function will get the restocking pct from RTV_DETAIL or RTV_HEAD if exists.
   -- If not, it will get the handling pct from sups table.

   if LP_valid_rtv = 'Y' then
      open C_restock_pct;
      fetch C_restock_pct into LP_rtv_record.detail_TBL.restock_pcts(LP_index);
      close C_restock_pct;

      -- only need to get the restocking pct from RTV_HEAD once
      if LP_rtv_record.restock_pct is NULL then
         open C_restock_pct_hd;
         fetch C_restock_pct_hd into LP_rtv_record.restock_pct;
         close C_restock_pct_hd;
      end if;
   end if;

   -- only need to get the restocking pct from the sups table once
   if LP_rtv_record.restock_pct is NULL then
      open C_handling_pct;
      fetch C_handling_pct into LP_rtv_record.restock_pct;
      close C_handling_pct;
   end if;

   -- if detail level restock pct is not defined, use the header or supplier restock pct as default
   if LP_rtv_record.detail_TBL.restock_pcts(LP_index) is NULL then
      LP_rtv_record.detail_TBL.restock_pcts(LP_index) := LP_rtv_record.restock_pct;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_RESTOCK_PCT;
--------------------------------------------------------------------------------------------------------
FUNCTION WRITE_RESTOCKING_FEE(O_error_message      IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                              I_location           IN         ITEM_LOC.LOC%TYPE,
                              I_loc_type           IN         ITEM_LOC.LOC_TYPE%TYPE,
                              I_restock_qty        IN         RTV_DETAIL.QTY_RETURNED%TYPE,
                              I_restock_cost_supp  IN         RTV_DETAIL.UNIT_COST%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(60) := 'RTV_SQL.WRITE_RESTOCKING_FEE';

   L_tran_type         TRAN_DATA.TRAN_CODE%TYPE := 65;
   L_restock_cost_loc  RTV_DETAIL.UNIT_COST%TYPE;
   L_pct_in_pack       NUMBER;
   L_comp_restock_qty  RTV_DETAIL.QTY_RETURNED%TYPE;
   L_comp_restock_cost RTV_DETAIL.UNIT_COST%TYPE;

   cursor C_ITEMS_IN_PACK is
      select v.item,
             v.qty,
             im.dept,
             im.class,
             im.subclass
        from item_master im,
             v_packsku_qty v
       where v.pack_no = LP_rtv_record.detail_TBL.items(LP_index)
         and im.item   = v.item;

BEGIN
   -- This function will write tran data 65 for LP_rtv_record.detail_TBL.items(LP_index) and I_location.

   -- convert I_restock_cost_supp to location currency for writing to stock ledger
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       LP_rtv_record.supplier,
                                       'V',
                                       NULL,
                                       I_location,
                                       I_loc_type,
                                       NULL,
                                       I_restock_cost_supp,
                                       L_restock_cost_loc,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
      return false;
   end if;

   if LP_rtv_record.im_row_TBL(LP_index).pack_ind = 'N' then
      if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                       LP_rtv_record.detail_TBL.items(LP_index),
                                       LP_rtv_record.im_row_TBL(LP_index).dept,
                                       LP_rtv_record.im_row_TBL(LP_index).class,
                                       LP_rtv_record.im_row_TBL(LP_index).subclass,
                                       I_location,
                                       I_loc_type,
                                       LP_rtv_record.tran_date,
                                       L_tran_type,
                                       NULL,
                                       I_restock_qty,
                                       L_restock_cost_loc,  -- restocking cost
                                       NULL,  -- I_total_retail
                                       LP_rtv_record.rtv_order_no,
                                       NULL,--I_ref_no_2
                                       NULL,--I_tsf_source_st
                                       NULL,--I_tsf_source_wh
                                       NULL,--I_old_unit_retail
                                       NULL,--I_new_unit_retail
                                       NULL,--I_source_dept
                                       NULL,--I_source_class
                                       NULL,--I_source_subclass
                                       L_program) = FALSE then
         return FALSE;
      end if;
   else
      FOR rec in C_ITEMS_IN_PACK LOOP
         if TRANSFER_COST_SQL.PCT_IN_PACK(O_error_message,
                                          L_pct_in_pack,
                                          LP_rtv_record.detail_TBL.items(LP_index),  -- pack_no
                                          rec.item,
                                          I_location) = FALSE then
            return FALSE;
         end if;

         L_comp_restock_qty := I_restock_qty * rec.qty;
         L_comp_restock_cost := L_restock_cost_loc * L_pct_in_pack * rec.qty;

         if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                          rec.item,
                                          rec.dept,
                                          rec.class,
                                          rec.subclass,
                                          I_location,
                                          I_loc_type,
                                          LP_rtv_record.tran_date,
                                          L_tran_type,
                                          NULL,
                                          L_comp_restock_qty,
                                          L_comp_restock_cost,
                                          NULL,-- I_total_retail
                                          LP_rtv_record.rtv_order_no,
                                          NULL,--I_ref_no_2
                                          NULL,--I_tsf_source_st
                                          NULL,--I_tsf_source_wh
                                          NULL,--I_old_unit_retail
                                          NULL,--I_new_unit_retail
                                          NULL,--I_source_dept
                                          NULL,--I_source_class
                                          NULL,--I_source_subclass
                                          L_program) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END WRITE_RESTOCKING_FEE;
--------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_MRT_RESTOCKING_FEE(O_error_message        IN OUT     VARCHAR2)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(60) := 'RTV_SQL.PROCESS_MRT_RESTOCKING_FEE';

   L_total_restock_cost        RTV_HEAD.RESTOCK_COST%TYPE;
   L_total_mrt_restock_cost    RTV_HEAD.RESTOCK_COST%TYPE := 0;
   L_total_mrt_received_qty    MRT_ITEM_LOC.RECEIVED_QTY%TYPE := 0;
   L_total_mrt_prorated_qty    MRT_ITEM_LOC.RECEIVED_QTY%TYPE := 0;
   L_restock_cost              RTV_HEAD.RESTOCK_COST%TYPE;
   L_restock_qty               MRT_ITEM_LOC.RECEIVED_QTY%TYPE;

   L_mrt_locs                  LOC_TBL := LOC_TBL();
   L_mrt_loc_types             LOC_TYPE_TBL := LOC_TYPE_TBL();
   L_mrt_received_qtys         QTY_TBL := QTY_TBL();
   L_mrt_prorated_qtys         QTY_TBL := QTY_TBL();

   cursor C_total_mrt_qty is
      select sum(received_qty)
        from mrt_item_loc
       where mrt_no = LP_mrt_no
         and item = LP_rtv_record.detail_TBL.items(LP_index);

   cursor C_mrt_qty is
      select location,
             loc_type,
             received_qty
        from mrt_item_loc
       where mrt_no = LP_mrt_no
         and item = LP_rtv_record.detail_TBL.items(LP_index);

BEGIN

   -- This function will write stock ledger (tran_code 65) for restocking fees for an MRT RTV.
   -- For MRT RTVs, the restocking fee is prorated among all MRT locations, based on
   -- received_qty on MRT_ITEM_LOC. If returned qty is more than the MRT received quantity
   -- for all locations, the excess qty will be assessed against the RTV sending location.

   -- MRT RTVs will only be created in RMS, which can only have virtual locations.
   -- As a result, physical location distribution logic does not need to be applied here.
   -- Restocking fees will be written in location currency.
   -- For a pack item, restocking fess will be written for component items.
   -- no restocking pct, no need to write tran_data 65

   if LP_rtv_record.detail_TBL.restock_pcts(LP_index) = 0 or LP_item_total_cost <= 0 then
      return TRUE;
   end if;

   -- calculate the total restocking cost in supplier currency
     L_total_restock_cost := LP_item_total_cost * LP_rtv_record.detail_TBL.restock_pcts(LP_index)/100;

   -- get the total restocking qty across all mrt locations
   open C_total_mrt_qty;
   fetch C_total_mrt_qty into L_total_mrt_received_qty;
   close C_total_mrt_qty;

   -- prorate the qty among mrt locations
   open C_mrt_qty;
   fetch C_mrt_qty bulk collect into L_mrt_locs,
                                     L_mrt_loc_types,
                                     L_mrt_received_qtys;
   close C_mrt_qty;

   if LP_rtv_record.detail_TBL.returned_qtys(LP_index) < L_total_mrt_received_qty then
      -- returned qty is less than total MRT qty, prorate qty
      L_total_mrt_prorated_qty := 0;
      for i in L_mrt_locs.first .. L_mrt_locs.last loop
         -- always round down
         L_mrt_prorated_qtys.EXTEND();
         L_mrt_prorated_qtys(i) := round(LP_rtv_record.detail_TBL.returned_qtys(LP_index)/L_total_mrt_received_qty*L_mrt_received_qtys(i)-0.5, 0);
         L_total_mrt_prorated_qty := L_total_mrt_prorated_qty + L_mrt_prorated_qtys(i);
      end loop;

      -- add the left over to the last MRT location
      L_mrt_prorated_qtys(L_mrt_prorated_qtys.COUNT) :=
         L_mrt_prorated_qtys(L_mrt_prorated_qtys.COUNT) + (LP_rtv_record.detail_TBL.returned_qtys(LP_index) - L_total_mrt_prorated_qty);
   else
      -- returned qty is greater or equal to total MRT qty,
      -- assign prorated_qty as received qty
      for i in L_mrt_locs.first .. L_mrt_locs.last loop
         L_mrt_prorated_qtys.EXTEND();
         L_mrt_prorated_qtys(i) := L_mrt_received_qtys(i);
      end loop;
   end if;

   -- write restocking fee for MRT locations
   for i in L_mrt_locs.first .. L_mrt_locs.last loop
      L_restock_qty := L_mrt_prorated_qtys(i);
      L_restock_cost := L_total_restock_cost * L_restock_qty/LP_rtv_record.detail_TBL.returned_qtys(LP_index);

      if WRITE_RESTOCKING_FEE(O_error_message,
                              L_mrt_locs(i),
                              L_mrt_loc_types(i),
                              L_restock_qty,
                              L_restock_cost) = FALSE then
         return FALSE;
      end if;
      L_total_mrt_restock_cost := L_total_mrt_restock_cost + L_restock_cost;
   end loop;

   -- write restocking fee for the sending location for any excess qty
   if LP_rtv_record.detail_TBL.returned_qtys(LP_index) > L_total_mrt_received_qty then
      L_restock_qty := LP_rtv_record.detail_TBL.returned_qtys(LP_index) - L_total_mrt_received_qty;
      L_restock_cost := L_total_restock_cost - L_total_mrt_restock_cost;

      if WRITE_RESTOCKING_FEE(O_error_message,
                              LP_rtv_record.loc,  -- sending location
                              LP_rtv_record.loc_type,
                              L_restock_qty,
                              L_restock_cost) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END PROCESS_MRT_RESTOCKING_FEE;
---------------------------------------------------------------------------------------
FUNCTION APPLY_PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_rtv_record      IN       RTV_RECORD)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(30) := 'RTV_SQL.APPLY_PROCESS';

   L_uom_class           UOM_CLASS.UOM_CLASS%TYPE;
   L_rtv_external        BOOLEAN := FALSE;
   L_physical_loc        RTV_HEAD.STORE%TYPE;
   ---
   L_country_desc        COUNTRY.COUNTRY_DESC%TYPE;
   L_state_desc          STATE.DESCRIPTION%TYPE;
   L_jurisdiction_desc   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE;
   ---
   L_dup_exists          VARCHAR2(1)    := NULL;
   L_calling_module      VARCHAR2(12)   := NULL;
   L_calling_context     VARCHAR2(12)   := NULL;
   L_status_ind          RTV_HEAD.STATUS_IND%TYPE;
   L_already_shipped     BOOLEAN := FALSE;
   L_valid_rtv           VARCHAR2(1)    := 'N';
   L_exists           VARCHAR2(1);

   cursor C_RTV_EXISTS is
      select status_ind
        from rtv_head
       where rtv_order_no = LP_rtv_record.rtv_order_no
         and status_ind in ('10', '15');

   cursor C_UNSHIPPED_DETAIL is
   select item
     from rtv_detail d
    where rtv_order_no = LP_rtv_record.rtv_order_no
      and not exists (select 'x'
                        from TABLE(CAST(LP_rtv_record.detail_TBL.items AS ITEM_TBL)) rit
                       where rit.column_value=d.item
                         and rownum = 1);
						 
   cursor C_rtvitem_exists is
      select 'x'
        from rtvitem_inv_flow
       where rtv_order_no = LP_rtv_record.rtv_order_no
         and seq_no       = LP_rtv_record.detail_TBL.seq_nos(LP_index)
         and item         = LP_rtv_record.detail_TBL.items(LP_index);						 

BEGIN
  ---
   if INITIALIZE(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   LP_rtv_record  := I_rtv_record;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;
   ---
   open C_RTV_EXISTS;
   fetch C_RTV_EXISTS into L_status_ind;
   close C_RTV_EXISTS;
   ---
   if L_status_ind = '15' then
      L_already_shipped := TRUE;
   else
      L_already_shipped := FALSE;
   end if;
   ---
   if LP_rtv_record.jurisdiction_code is NOT NULL then
      L_calling_module  := ADDRESS_SQL.JURIS;
      L_calling_context := ADDRESS_SQL.JURIS;
   elsif LP_rtv_record.state is NOT NULL then
      L_calling_module  := ADDRESS_SQL.ST;
      L_calling_context := ADDRESS_SQL.ST;
   end if;
   ---
   if ADDRESS_SQL.CHECK_ADDR (O_error_message,
                              LP_rtv_record.country,
                              L_country_desc,
                              LP_rtv_record.state,
                              L_state_desc,
                              LP_rtv_record.jurisdiction_code,
                              L_jurisdiction_desc,
                              L_dup_exists,
                              L_calling_module,
                              L_calling_context) = FALSE then
      return FALSE;
   end if;
   ---
   if RTV_SQL.VALIDATE_RTV_HEAD(O_error_message) = FALSE then
      return false;
   end if;
   ---
   if LP_valid_rtv = 'N' and LP_upd_rtv_qty_ind = 'N' then
      L_rtv_external := TRUE;
   else
      L_rtv_external := FALSE;
   end if;
   ---
   L_physical_loc := LP_rtv_record.loc;
   ---
   for i in LP_rtv_record.detail_TBL.items.FIRST..LP_rtv_record.detail_TBL.items.LAST LOOP
      LP_index := i;
      LP_item_total_cost :=0;
      ---
      if (LP_rtv_record.detail_tbl.returned_qtys(i) >= 0 and LP_valid_rtv = 'Y' ) or
         (LP_rtv_record.detail_tbl.returned_qtys(i) > 0 and LP_valid_rtv = 'N' )then
         if LP_rtv_record.im_row_TBL(i).orderable_ind = 'Y' and
            LP_rtv_record.im_row_TBL(i).sellable_ind  = 'N' and
            LP_rtv_record.im_row_TBL(i).inventory_ind = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_NONINVENT_ITEM', NULL, NULL, NULL);
            return FALSE;
         end if;
         ---
         if LP_rtv_record.im_row_TBL(i).item_xform_ind = 'Y' and
            LP_rtv_record.im_row_TBL(i).orderable_ind  = 'Y' and
            LP_rtv_record.im_row_TBL(i).sellable_ind   = 'N' then
            -- get item_master info for orderable items
            if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                               LP_rtv_record.im_row_TBL(i),
                                               LP_rtv_record.detail_TBL.items(i)) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if LP_rtv_record.im_row_TBL(i).item_level > LP_rtv_record.im_row_TBL(i).tran_level then
            -- ref item on message
            LP_rtv_record.detail_TBL.items(i) := LP_rtv_record.im_row_TBL(i).item_parent;
         elsif LP_rtv_record.im_row_TBL(i).item_level < LP_rtv_record.im_row_TBL(i).tran_level then
            O_error_message := SQL_LIB.CREATE_MSG('NO_PARENT_ITEM',
                                                   LP_rtv_record.detail_TBL.items(i),
                                                   NULL, NULL);
            return FALSE;
         end if;
         ---
         if LP_rtv_record.im_row_TBL(i).simple_pack_ind = 'Y' and
            LP_rtv_record.im_row_TBL(i).catch_weight_ind = 'Y' then

           if LP_rtv_record.detail_TBL.weights(i) is NOT NULL and
              LP_rtv_record.detail_TBL.weight_uoms(i) is NULL or
              LP_rtv_record.detail_TBL.weights(i) is NULL and
              LP_rtv_record.detail_TBL.weight_uoms(i) is NOT NULL then

              O_error_message := SQL_LIB.CREATE_MSG('WGT_WGTUOM_REQUIRED', NULL, NULL, NULL);
              return FALSE;
            end if;

            if LP_rtv_record.detail_TBL.weights(i) is NOT NULL and
               LP_rtv_record.detail_TBL.weight_uoms(i) is NOT NULL then

               if NOT UOM_SQL.GET_CLASS(O_error_message,
                                        L_uom_class,
                                        UPPER(LP_rtv_record.detail_TBL.weight_uoms(i))) then
                  return FALSE;
               end if;

               if L_uom_class != 'MASS' then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_WGTUOM_CLASS',
                                                        LP_rtv_record.detail_TBL.weight_uoms(i),
                                                        L_uom_class, NULL);
                  return FALSE;
               end if;

               if NOT CATCH_WEIGHT_SQL.CONVERT_WEIGHT(O_error_message,
                                                      LP_rtv_record.detail_TBL.weight_cuoms(i),
                                                      LP_rtv_record.detail_TBL.cuoms(i),
                                                      LP_rtv_record.detail_TBL.items(i),
                                                      LP_rtv_record.detail_TBL.weights(i),
                                                      UPPER(LP_rtv_record.detail_TBL.weight_uoms(i))) then
                  return FALSE;
               end if;
            end if;
         end if;
         ---
         if RTV_SQL.GET_RESTOCK_PCT(O_error_message) = FALSE then
            return false;
         end if;
         ---
         if LP_distribute = 'N' then
            ---
            if RTV_SQL.VALIDATE_RTV_DETAIL(O_error_message) = FALSE then
               return false;
            end if;
            ---
            if RTV_SQL.INVENTORY(O_error_message,
                                 LP_upd_rtv_qty_ind,  -- Set in VALIDATE_RTV_HEAD
                                 LP_rtv_record.im_row_TBL(i).item_xform_ind,
                                 LP_rtv_record.im_row_TBL(i).sellable_ind) = FALSE then
               return false;
            end if;
            ---
         elsif LP_distribute = 'Y' then
            ---
            -- reset the header record location since distribution processing will
            -- change it for each virtual wh in the passed in physical wh.
            --
            LP_rtv_record.loc := L_physical_loc;
            ---
            if RTV_SQL.DISTRIBUTE_RTV(O_error_message,
                                      LP_rtv_record.im_row_TBL(i).item_xform_ind,
                                      LP_rtv_record.im_row_TBL(i).sellable_ind) = FALSE then
               return false;
            end if;
         ---
         end if;
         L_valid_rtv := LP_valid_rtv;

         -- Decrement the RTV_DETAIL.REQUESTED_QTY from ITEM_LOC_SOH.RTV_QTY
         -- for valid, internally created RTV's.
         -- This needs to be called before RTV_SQL.BUILD_RTV to avoid decrementing
         -- ITEM_LOC_SOH.RTV_QTY if the item in the RTV ship message is NOT present
         -- on the original RTV_DETAIL. RTV_SQL.BUILD_RTV will insert the item to RTV_DETAIL.
         if L_valid_rtv = 'Y' and
            L_rtv_external = FALSE and
            L_already_shipped = FALSE then

            if RTV_SQL.UPD_RTV_QTY(O_error_message,
                                   LP_rtv_record.rtv_order_no,
                                   LP_rtv_record.detail_TBL.items(i),
                                   'S') = FALSE then
               return false;
            end if;
            ---
         end if;

         -- update or insert RTV header/detail
         -- if MC is on, RTV holds info at the physical level
         if RTV_SQL.BUILD_RTV(O_error_message) = FALSE then
            return false;
         end if;
		 
         if LP_distribute = 'Y' then
            open C_rtvitem_exists;
            fetch C_rtvitem_exists into L_exists;
            close C_rtvitem_exists;
            if L_exists is NULL then
               if INSERT_RTVITEM_INV_FLOW(O_error_message,
                                          LP_rtv_record.rtv_order_no,
                                          LP_rtv_record.detail_TBL.seq_nos(LP_index),
                                          LP_rtv_record.detail_TBL.items(LP_index),
                                          LP_dist_tab_1) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
		 
         -- process restocking fee for an MRT RTV
         if LP_mrt_no is NOT NULL then
            if RTV_SQL.PROCESS_MRT_RESTOCKING_FEE(O_error_message) = FALSE then
               return FALSE;
            end if;
         end if;
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_ZERO_QTY',NULL, NULL, NULL);
         return FALSE;
      end if;

   end LOOP; --loop through detail table

   -- For items in RTV_DETAIL but not present in the RTV ship message,
   -- cancel out unshipped items by updating RTV_DETAIL.qty_cancelled and
   -- backing out ITEM_LOC_SOH.RTV_QTY
   for rec in C_UNSHIPPED_DETAIL loop
      if L_valid_rtv = 'Y' and
         L_rtv_external = FALSE and
         L_already_shipped = FALSE then

         if RTV_SQL.UPD_RTV_QTY(O_error_message,
                                LP_rtv_record.rtv_order_no,
                                rec.item,
                                'S') = FALSE then
            return false;
         end if;
         ---
      end if;
   end loop;
   ---
   if INVC_WRITE_SQL.WRITE_RTV_DBT_CRDT(O_error_message,
                                        LP_rtv_record.rtv_order_no,     -- rtv order number
                                        NULL,                -- invoice type
                                        LP_user,             -- user id
                                        NULL,                -- ref rsn code
                                        LP_rtv_record.supplier,         -- supplier
                                        NULL) = FALSE then   -- dbt crdt ind
      return FALSE;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END APPLY_PROCESS;
--------------------------------------------------------------------------------------------
FUNCTION REMOVE_RTV_DETAIL (  O_error_message OUT rtk_errors.rtk_text%TYPE
                             ,O_container_ind OUT VARCHAR2
                             ,I_rtv_order_no  IN  rtv_detail.rtv_order_no%TYPE
                             ,I_item          IN  item_master.item%TYPE ) RETURN BOOLEAN AS

   -----------------------------------
   -- Variable declarations
   -----------------------------------
   L_container item_master.container_item%TYPE;
   L_item      item_master.item%TYPE;
   L_count     NUMBER(8) := 0;
   L_program   VARCHAR2(30) := 'RTV_SQL.REMOVE_RTV_DETAIL';

   -----------------------------------
   -- Cursor declarations
   -----------------------------------

   -----------------------------------
   -- Identify container item
   -----------------------------------
   cursor c_item (c_item IN item_master.item%TYPE) IS
   select im.container_item
   from item_master im
   where im.item = c_item;

   ------------------------------------------------------------
   -- Lock rtv_Detail contents items related to container item
   -- and also lock the container item.
   ------------------------------------------------------------
   cursor c_lock_rtv ( c_container_item IN item_master.item%TYPE
                      ,c_rtv_order_no   IN rtv_detail.rtv_order_no%TYPE ) IS
   select rd.item
   from rtv_detail rd
   where exists (select 1
                 from item_master im
                 where ( im.container_item = c_container_item
                      OR ( im.container_item is null and im.item = c_container_item ))
                 and   rd.item             IN (  im.item , c_container_item )
                 )
   and rd.rtv_order_no = c_rtv_order_no
   for update of rd.item nowait;

   ------------------------------------------------
   -- Count number of contents items in container
   ------------------------------------------------
   cursor c_count ( c_container_item IN item_master.item%TYPE
                   ,c_rtv_order_no   IN rtv_detail.rtv_order_no%TYPE) IS
   select count(1)
   from rtv_detail rd
   where exists ( select 1
                    from item_master im
                   where  im.container_item = c_container_item
                     and  rd.item     = im.item)
     and rd.rtv_order_no = c_rtv_order_no;

   ---------------------------------------------
   -- Exception declarations
   ---------------------------------------------
   RECORD_LOCKED                 EXCEPTION;
   PRAGMA                        EXCEPTION_INIT(Record_Locked, -54);

BEGIN
   -------------------------------------------------------------------
   -- Get the container item
   -- Return to the form is the item does not have a container item
   -------------------------------------------------------------------
   open c_item(I_item);
   fetch c_item into L_container;
   if c_item%NOTFOUND OR L_container IS NULL then
      close c_item;
      O_container_ind := 'N';
      RETURN TRUE;
   end if;
   close c_item;
   ---
   if L_container IS NOT NULL then
      ---
      open c_count (L_container,I_rtv_order_no);
      fetch c_count into L_count;
      if L_count > 1 then
         ------------------------------------------------------------
         -- More than one contents item, so cannot remove container
         ------------------------------------------------------------
         close c_count;
         O_container_ind := 'Y';
         RETURN TRUE;
      elsif L_count = 1 then
         --------------------------------------------
         -- remove the container and its content item
         --------------------------------------------
         open c_lock_rtv ( L_container, I_rtv_order_no);
         fetch c_lock_rtv into L_item;
         if c_lock_rtv%FOUND then
            delete rtv_detail rd
             where  exists (select 1
                            from item_master im
                            where ( im.container_item = L_container
                                  OR ( im.container_item is null and im.item = L_container))
                                  and   rd.item  IN (  im.item , L_container))
               and rd.rtv_order_no = I_rtv_order_no;
         end if;
         close c_lock_rtv;
      end if;
      close c_count;
   end if;
   ---
   RETURN TRUE;

EXCEPTION
  when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'RTV_DETAIL',
                                             I_item,
                                             L_container);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);

      return FALSE;
END REMOVE_RTV_DETAIL;
-------------------------------------------------------------------------------------------------
FUNCTION UPDATE_CONTAINER_ITEM (  O_error_message OUT rtk_errors.rtk_text%TYPE
                                 ,I_rtv_order_no  IN  rtv_detail.rtv_order_no%TYPE
                                 ,I_item          IN  item_master.item%TYPE ) RETURN BOOLEAN AS

   -----------------------------------
   -- Variable declarations
   -----------------------------------
   L_container           item_master.container_item%TYPE;
   L_item                item_master.item%TYPE;
   L_program             VARCHAR2(30) := 'RTV_SQL.UPDATE_CONTAINER_ITEM';
   L_cont_total_qty_req  rtv_detail.qty_requested%TYPE;

   -----------------------------------
   -- Cursor declarations
   -----------------------------------

   ------------------------------------------------------------
   -- Identify contents records related to the container
   ------------------------------------------------------------
   cursor c_contents ( c_container_item IN item_master.item%TYPE
                      ,c_rtv_order_no   IN rtv_detail.rtv_order_no%TYPE ) IS
   select sum(rd.qty_requested)
     from rtv_detail rd
    where rd.rtv_order_no = c_rtv_order_no
      and exists ( select 1
                     from item_master im
                    where im.container_item = c_container_item
                      and im.item           = rd.item);

   ------------------------------------------------------------
   -- Lock rtv_Detail container item record
   ------------------------------------------------------------
   cursor c_lock_container ( c_item           IN item_master.item%TYPE
                            ,c_rtv_order_no   IN rtv_detail.rtv_order_no%TYPE ) IS
   select rd.item
   from rtv_detail rd
   where exists ( select 1
                  from item_master im
                  where im.item   = c_item
                    and rd.item   = im.container_item
                 )
   and rd.rtv_order_no = c_rtv_order_no
   for update of rd.item nowait;

   ---------------------------------------------
   -- Exception declarations
   ---------------------------------------------
   RECORD_LOCKED                 EXCEPTION;
   PRAGMA                        EXCEPTION_INIT(Record_Locked, -54);

BEGIN
   ---------------------------------------------------
   -- Attempt to Lock the items respective container
   ---------------------------------------------------
   open c_lock_container ( I_item , I_rtv_order_no);
   fetch c_lock_container into L_container;
   if c_lock_container%FOUND then
      ----------------------------------------------------------------------
      -- Get total qty requested for all contents relating to the container
      ----------------------------------------------------------------------
      open c_contents ( L_container, I_rtv_order_no);
      fetch c_contents into L_cont_total_qty_req;
      if c_contents%FOUND then
         ----------------------------------------
         -- Update the containers qty requested
         ----------------------------------------
         update rtv_detail rd
            set qty_requested = NVL(L_cont_total_qty_req,0),
                updated_by_rms_ind = 'Y'
         where current of c_lock_container;
      end if;
      close c_contents;
    end if;
    close c_lock_container;
    ---
    RETURN TRUE;

EXCEPTION
  when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'RTV_DETAIL',
                                             I_item,
                                             I_rtv_order_no);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);

      return FALSE;
END UPDATE_CONTAINER_ITEM;
--------------------------------------------------------------------------------------------------------
FUNCTION DETERMINE_RTV_COST (O_error_message  IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                             O_unit_cost_loc  IN OUT    SHIPSKU.UNIT_COST%TYPE,
                             O_unit_cost_supp IN OUT    SHIPSKU.UNIT_COST%TYPE,
                             I_item           IN    ITEM_LOC.ITEM%TYPE,
                             I_location       IN    ITEM_LOC.LOC%TYPE,
                             I_loc_type       IN    ITEM_LOC.LOC_TYPE%TYPE,
                             I_supplier       IN    SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'RTV_SQL.DETERMINE_RTV_COST';

   L_rtv_unit_cost_ind   INV_MOVE_UNIT_OPTIONS.RTV_UNIT_COST_IND%TYPE;
   L_std_av_ind          SYSTEM_OPTIONS.STD_AV_IND%TYPE;

   L_loc            ITEM_LOC_SOH.LOC%TYPE;
   L_pwh_flag       BOOLEAN := FALSE;
   L_exists         BOOLEAN;

   L_origin_country_id ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_unit_cost_loc     SHIPSKU.UNIT_COST%TYPE;
   L_unit_cost_supp    SHIPSKU.UNIT_COST%TYPE;
   L_vwh               WH.WH%TYPE;
   L_primary_vwh       WH.PRIMARY_VWH%TYPE;
   L_loc_curr          CURRENCIES.CURRENCY_CODE%TYPE;
   L_sups_curr         CURRENCIES.CURRENCY_CODE%TYPE;

   cursor C_get_primary_vwh is
      select w.primary_vwh
        from wh w
       where physical_wh = I_location
         and exists (select 'x'
                       from item_loc_soh
                      where loc = w.primary_vwh
                        and item = I_item);

   cursor C_get_virtual_wh is
      select wh
        from wh
       where physical_wh = I_location
         and wh in (select loc
                      from item_loc_soh
                     where item = I_item)
         and rownum = 1;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_loc_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;
   L_rtv_unit_cost_ind := LP_system_options.rtv_unit_cost_ind;
   L_std_av_ind := LP_system_options.std_av_ind;

   if I_loc_type = 'W' then
      --To get the last receipt cost we need to use the physical warehouse linked
      --to the displayed warehouse. This warehouse could be a physical or virtual wh.
      --If this is a virtual warehouse. A receipt will never come in as the value
      --of a virtual warehouse so get the physical wh for the virtual wh.
      --Check the wh is a physical wh
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_pwh_flag,
                                 I_location) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_rtv_unit_cost_ind = 'R' then
      if I_loc_type = 'W' and NOT L_pwh_flag then
         --Retrieve the Physical WH as the last receipt cost on SHIPSKU table is
         --for Physical WHs.
         if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                          L_loc,
                                          I_location) = FALSE then
            return FALSE;
         end if;
      else
         L_loc := I_location;
      end if;

      if ITEM_SUPP_COUNTRY_LOC_SQL.GET_LAST_RECEIPT_COST(O_error_message,
                                                         L_unit_cost_supp,
                                                         I_item,
                                                         I_supplier,
                                                         L_loc) = FALSE then
         return FALSE;
      end if;
   end if;
   if L_rtv_unit_cost_ind = 'S' or L_rtv_unit_cost_ind = 'A' or
      (L_rtv_unit_cost_ind = 'R' and L_unit_cost_supp is NULL) then
      if I_loc_type = 'W' and L_pwh_flag then
         open C_get_primary_vwh;
         fetch C_get_primary_vwh INTO L_primary_vwh;
         close C_get_primary_vwh;

         if L_primary_vwh is NULL then
            open C_get_virtual_wh;
            fetch C_get_virtual_wh INTO L_loc;
            close C_get_virtual_wh;
         else
            L_loc := L_primary_vwh;
         end if;

      else
         L_loc := I_location;
      end if;
   end if;

   if L_rtv_unit_cost_ind = 'S' or
      (L_rtv_unit_cost_ind = 'R' and L_unit_cost_supp is NULL) then
      if ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                   L_exists,
                                                   L_origin_country_id,
                                                   I_item,
                                                   I_supplier) = FALSE then
         return FALSE;
      end if;
      if SUPP_ITEM_SQL.GET_COST(O_error_message,
                                L_unit_cost_supp,
                                I_item,
                                I_supplier,
                                L_origin_country_id,
                                L_loc) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_rtv_unit_cost_ind = 'A' then
      if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                        I_item,
                                        L_loc,
                                        I_loc_type,
                                        L_unit_cost_loc) = FALSE then
         return FALSE;
      end if;
   end if;


  --Get Location Currency
   if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                L_loc,
                                I_loc_type,
                                NULL,
                                L_loc_curr) = FALSE then
      return FALSE;
   end if;

   --Get Supplier Currency
   if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                I_supplier,
                                'V',
                                NULL,
                                L_sups_curr) = FALSE then
      return FALSE;
   end if;

   if L_unit_cost_supp is NOT NULL then
      if L_sups_curr<>L_loc_curr then
         --Convert the Cost in Supplier Currency to Location Currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_supplier,
                                             'V',
                                             NULL,
                                             L_loc,
                                             I_loc_type,
                                             NULL,
                                             L_unit_cost_supp,
                                             L_unit_cost_loc,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return false;
         end if;
      else
         L_unit_cost_loc := L_unit_cost_supp;
      end if;
   elsif L_unit_cost_loc is NOT NULL then
      if L_sups_curr<>L_loc_curr then
         --Convert the Cost in Location Currency to Supplier Currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             L_loc,
                                             I_loc_type,
                                             NULL,
                                             I_supplier,
                                             'V',
                                             NULL,
                                             L_unit_cost_loc,
                                             L_unit_cost_supp,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then











            return false;
         end if;
      else
         L_unit_cost_supp := L_unit_cost_loc;
      end if;
   end if;

   O_unit_cost_supp := L_unit_cost_supp;
   O_unit_cost_loc  := L_unit_cost_loc;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END DETERMINE_RTV_COST;
---------------------------------------------------------------------------------------
-- FUNCTION : SHIP_RTV
-- Purpose  : This public function is called from T-ship trigger on RTV form to ship
-- the RTV created on-line. RTV created on-line will NOT contain break-to-sell
-- sellable items since they are non-inventory items.
---------------------------------------------------------------------------------------
FUNCTION SHIP_RTV (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_rtv_order_no   IN      RTV_HEAD.RTV_ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'RTV_SQL.SHIP_RTV';

   L_rtv_order_no    RTV_HEAD.RTV_ORDER_NO%TYPE := I_rtv_order_no;

   -- holds query results from RTV_HEAD/RTV_DETAIL
   L_rtv_head_row    RTV_HEAD%ROWTYPE;
   L_rtv_detail_tbl  RTV_SQL.RTV_DETAIL_TBL;

   -- final rtv record based on RTV_HEAD/RTV_DETAIL
   -- contains the orderables if a break-to-sell sellable is on RTV_DETAIL
   L_rtv_record      RTV_SQL.RTV_RECORD;

   cursor C_rtv_head is
   select *
     from rtv_head
    where rtv_order_no = L_rtv_order_no;

   cursor C_rtv_detail is
   select rtd.rtv_order_no,
          rtd.seq_no,
          rtd.item,
          rtd.shipment,
          rtd.inv_status,
          rtd.qty_requested,
          rtd.qty_returned,
          rtd.qty_cancelled,
          rtd.unit_cost,
          rtd.reason,
          rtd.publish_ind,
          rtd.restock_pct,
          rtd.original_unit_cost,
          rtd.updated_by_rms_ind
     from rtv_detail rtd,
          item_master im
    where rtd.rtv_order_no = L_rtv_order_no
      and rtd.item = im.item
      and (NVL(im.deposit_item_type,'E') != 'A');

BEGIN
   if L_rtv_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'L_rtv_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_rtv_head;
   fetch C_rtv_head into L_rtv_head_row;
   close C_rtv_head;

   if L_rtv_head_row.rtv_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RTV_ORD_NO_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_rtv_detail;
   fetch C_rtv_detail bulk collect into L_rtv_detail_tbl;
   close C_rtv_detail;

   if L_rtv_detail_tbl is NULL or L_rtv_detail_tbl.COUNT <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('RTV_ORD_NO_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- parse and build the RTV_RECORD
   if RTV_SQL.BUILD_RTV_RECORD(O_error_message,
                               L_rtv_record,  --- output record
                               L_rtv_head_row,
                               L_rtv_detail_tbl) = FALSE then
      return FALSE;
   end if;

   -- ship the RTV
   if RTV_SQL.APPLY_PROCESS(O_error_message,
                            L_rtv_record) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SHIP_RTV;
---------------------------------------------------------------------------------------
FUNCTION BUILD_RTV_RECORD(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                          O_rtv_record      IN OUT NOCOPY RTV_SQL.RTV_RECORD,
                          I_rtv_head_row    IN            RTV_HEAD%ROWTYPE,
                          I_rtv_details     IN            RTV_SQL.RTV_DETAIL_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(62) := 'RTV_SQL.BUILD_RTV_RECORD';

   L_rtv_record   RTV_SQL.RTV_RECORD;
   L_vdate        PERIOD.VDATE%TYPE := GET_VDATE;

BEGIN
   -- populate the RTV_SQL header record
   L_rtv_record.rtv_order_no  := I_rtv_head_row.rtv_order_no;

   L_rtv_record.store := I_rtv_head_row.store;
   L_rtv_record.wh := I_rtv_head_row.wh;
   if I_rtv_head_row.store = -1 then
      L_rtv_record.loc := I_rtv_head_row.wh;
      L_rtv_record.loc_type := 'W';
   else
      L_rtv_record.loc := I_rtv_head_row.store;
      L_rtv_record.loc_type := 'S';
   end if;

   L_rtv_record.ext_ref_no                 := I_rtv_head_row.ext_ref_no;
   L_rtv_record.ret_auth_num               := I_rtv_head_row.ret_auth_num;
   L_rtv_record.supplier                   := I_rtv_head_row.supplier;
   L_rtv_record.ship_addr1                 := I_rtv_head_row.ship_to_add_1;
   L_rtv_record.ship_addr2                 := I_rtv_head_row.ship_to_add_2;
   L_rtv_record.ship_addr3                 := I_rtv_head_row.ship_to_add_3;
   L_rtv_record.state                      := I_rtv_head_row.state;
   L_rtv_record.city                       := I_rtv_head_row.ship_to_city;
   L_rtv_record.pcode                      := I_rtv_head_row.ship_to_pcode;
   L_rtv_record.country                    := I_rtv_head_row.ship_to_country_id;
   L_rtv_record.tran_date                  := L_vdate;
   L_rtv_record.comments                   := I_rtv_head_row.comment_desc;
   L_rtv_record.total_order_amt_unit_based := I_rtv_head_row.total_order_amt;
   L_rtv_record.total_order_amt_wgt_based  := I_rtv_head_row.total_order_amt;
   L_rtv_record.ret_courier                := I_rtv_head_row.courier;
   L_rtv_record.restock_pct                := I_rtv_head_row.restock_pct;
   L_rtv_record.restock_cost               := I_rtv_head_row.restock_cost;
   L_rtv_record.jurisdiction_code          := I_rtv_head_row.ship_to_jurisdiction_code;

   -- populate the RTV_SQL detail record (detail_tbl and im_row_tbl)
   if POPULATE_RTV_DETAIL(O_error_message,
                          L_rtv_record.detail_tbl,
                          L_rtv_record.im_row_tbl,
                          I_rtv_details) = FALSE then
      return FALSE;
   end if;

   O_rtv_record := L_rtv_record;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BUILD_RTV_RECORD;
---------------------------------------------------------------------------------------
FUNCTION POPULATE_RTV_DETAIL(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                             O_details_rec     IN OUT NOCOPY  RTV_SQL.RTV_DETAIL_REC,
                             O_im_row_tbl      IN OUT NOCOPY  RTV_SQL.ITEM_MASTER_TBL,
                             I_details_tbl     IN             RTV_SQL.RTV_DETAIL_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'RTV_SQL.POPULATE_RTV_DETAIL';

   L_details_rec  RTV_SQL.RTV_DETAIL_REC;
   L_im_row_tbl   RTV_SQL.ITEM_MASTER_TBL;

   L_item_rec     ITEM_MASTER%ROWTYPE;
   L_dtl_count    NUMBER := 0;

   L_code        INV_STATUS_CODES.INV_STATUS_CODE%TYPE;

   cursor C_GET_INV_STATUS_CODE (status_in inv_status_codes.inv_status%TYPE) IS
      select inv_status_code
        from inv_status_codes
       where inv_status = status_in;

BEGIN

   if I_details_tbl is NULL or I_details_tbl.COUNT <= 0 then
      return TRUE;
   end if;

   -- initialize detail collection
   L_details_rec.seq_nos := RTV_SQL.SEQ_NO_TBL();
   L_details_rec.items := ITEM_TBL();
   L_details_rec.returned_qtys := QTY_TBL();
   L_details_rec.from_disps := RTV_SQL.INV_STATUS_CODES_TBL();
   L_details_rec.unit_cost_exts := UNIT_COST_TBL();
   L_details_rec.unit_cost_supps := UNIT_COST_TBL();
   L_details_rec.unit_cost_locs := UNIT_COST_TBL();
   L_details_rec.reasons := RTV_SQL.REASON_TBL();
   L_details_rec.restock_pcts := RTV_SQL.RESTOCK_PCT_TBL();
   L_details_rec.inv_statuses := INV_STATUS_TBL();
   L_details_rec.mc_returned_qtys := QTY_TBL();
   L_details_rec.weights := RTV_SQL.WEIGHT_TBL();
   L_details_rec.weight_uoms := RTV_SQL.UOM_CLASS_TBL();
   L_details_rec.weight_cuoms := RTV_SQL.WEIGHT_TBL();
   L_details_rec.mc_weight_cuoms := RTV_SQL.WEIGHT_TBL();
   L_details_rec.cuoms := RTV_SQL.COST_UOM_TBL();
   L_details_rec.extended_base_cost := UNIT_COST_TBL();

   L_im_row_tbl := RTV_SQL.ITEM_MASTER_TBL();

   for i in I_details_tbl.FIRST .. I_details_tbl.LAST loop

      -- allocate memory
      L_details_rec.seq_nos.EXTEND;
      L_details_rec.items.EXTEND;
      L_details_rec.returned_qtys.EXTEND;
      L_details_rec.from_disps.EXTEND;
      L_details_rec.unit_cost_exts.EXTEND;
      L_details_rec.unit_cost_supps.EXTEND;
      L_details_rec.unit_cost_locs.EXTEND;
      L_details_rec.reasons.EXTEND;
      L_details_rec.restock_pcts.EXTEND;
      L_details_rec.inv_statuses.EXTEND;
      L_details_rec.mc_returned_qtys.EXTEND;
      L_details_rec.weights.EXTEND;
      L_details_rec.weight_uoms.EXTEND;
      L_details_rec.weight_cuoms.EXTEND;
      L_details_rec.mc_weight_cuoms.EXTEND;
      L_details_rec.cuoms.EXTEND;
      L_details_rec.extended_base_cost.EXTEND;

      L_dtl_count := L_details_rec.items.COUNT;

      -- assign values
      L_details_rec.seq_nos(L_dtl_count)      := I_details_tbl(i).seq_no;
      L_details_rec.items(L_dtl_count)        := I_details_tbl(i).item;
      L_details_rec.inv_statuses(L_dtl_count) := I_details_tbl(i).inv_status;

      L_details_rec.returned_qtys(L_dtl_count)  := I_details_tbl(i).qty_requested
                                                     - NVL(I_details_tbl(i).qty_cancelled, 0)
                                                     - NVL(I_details_tbl(i).qty_returned, 0);
      if L_details_rec.returned_qtys(L_dtl_count) < 0 then
         L_details_rec.returned_qtys(L_dtl_count) := 0;
      end if;

      if I_details_tbl(i).inv_status is NULL then
         L_details_rec.from_disps(L_dtl_count) := 'ATS';
      else
         open C_GET_INV_STATUS_CODE (I_details_tbl(i).inv_status);
         fetch C_GET_INV_STATUS_CODE into L_code;
         close C_GET_INV_STATUS_CODE;
         L_details_rec.from_disps(L_dtl_count) := L_code;
      end if;

      L_details_rec.unit_cost_supps(L_dtl_count) := I_details_tbl(i).unit_cost; -- unit cost on RTV is in supplier currency
      L_details_rec.reasons(L_dtl_count)        := I_details_tbl(i).reason;

      L_details_rec.unit_cost_exts(L_dtl_count) := NULL;  -- not defined for on-line rtv
      L_details_rec.unit_cost_locs(L_dtl_count) := NULL;  -- not defined for on-line rtv
      L_details_rec.weights(L_dtl_count)        := NULL;  -- not defined for on-line rtv
      L_details_rec.weight_uoms(L_dtl_count)    := NULL;  -- not defined for on-line rtv

      -- get the item_master row
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_rec,
                                         I_details_tbl(i).item) = FALSE then
         return FALSE;
      end if;

      L_im_row_tbl.EXTEND;
      L_im_row_tbl(L_dtl_count) := L_item_rec;
   end loop;

   O_details_rec := L_details_rec;
   O_im_row_tbl := L_im_row_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_RTV_DETAIL;
---------------------------------------------------------------------------------------
-- FUNCTION : INSERT_RTVITEM_INV_FLOW
-- Purpose  : This function inserts distribution details into rtvitem_inv_flow table.
---------------------------------------------------------------------------------------
FUNCTION INSERT_RTVITEM_INV_FLOW (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_rtv_order_no   IN     RTV_DETAIL.RTV_ORDER_NO%TYPE,
                                  I_seq_no         IN     RTV_DETAIL.SEQ_NO%TYPE,
                                  I_item           IN     RTV_DETAIL.ITEM%TYPE,
                                  I_dist_tab       IN     DISTRIBUTION_SQL.DIST_TABLE_TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'RTV_SQL.INSERT_RTVITEM_INV_FLOW';

   TYPE wh_TBL     is TABLE of RTVITEM_INV_FLOW.LOC%TYPE INDEX BY BINARY_INTEGER;

   L_wh_tbl        wh_TBL;
   L_dist_qty_tbl  QTY_TBL := QTY_TBL();
   L_rtvitem_size  BINARY_INTEGER := 1;

BEGIN
   if I_rtv_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rtv_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_seq_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_dist_tab.COUNT > 0 then
      --- Building local table type variables to be used in bulk insert to rtvitem_inv_flow.
      FOR L_rtvitem_size in I_dist_tab.FIRST..I_dist_tab.LAST LOOP
            if I_dist_tab(L_rtvitem_size).wh is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                     'I_dist_tab.wh',
                                                     L_program,
                                                     NULL);
               return FALSE;
            end if;
            L_wh_tbl(L_rtvitem_size) := I_dist_tab(L_rtvitem_size).wh;
            ---
            if I_dist_tab(L_rtvitem_size).dist_qty is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                     'I_dist_tab.dist_qty',
                                                     L_program,
                                                     NULL);
               return FALSE;
            end if;
            L_dist_qty_tbl.EXTEND();
            L_dist_qty_tbl(L_rtvitem_size) := I_dist_tab(L_rtvitem_size).dist_qty;
      END LOOP;

      --- Bulk insert into rtvitem_inv_flow.
      FORALL i in I_dist_tab.FIRST..I_dist_tab.LAST
         insert into rtvitem_inv_flow (rtv_order_no,
                                       seq_no,
                                       item,
                                       loc,
                                       loc_type,
                                       qty_requested,
                                       qty_returned)
                               values (I_rtv_order_no,
                                       I_seq_no,
                                       I_item,
                                       L_wh_tbl(i),
                                       'W',
                                       L_dist_qty_tbl(i),
                                       NULL);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_RTVITEM_INV_FLOW;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_RTV_HEAD_APPRV (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'RTV_SQL.VALIDATE_RTV_HEAD_APPROV';
   L_exist            VARCHAR2(1) := NULL;

   L_wh               RTV_HEAD.WH%TYPE := NULL;
   L_store            RTV_HEAD.STORE%TYPE := NULL;
   L_status_ind       RTV_HEAD.STATUS_IND%TYPE := NULL;

cursor C_WH_EXIST is
   select 'x'
     from wh
    where wh = LP_rtv_record.loc;

cursor C_RTV_EXISTS is
   select wh,
          store,
          status_ind
     from rtv_head
    where rtv_order_no = LP_rtv_record.rtv_order_no
      and status_ind in ('10','12','15');

cursor C_EXT_REF_RTV_EXISTS is
   select wh,
          store,
          status_ind
     from rtv_head
    where ext_ref_no = LP_rtv_record.ext_ref_no
      and status_ind in ('10','12','15')
      and ((store = LP_rtv_record.loc)
       or (wh = LP_rtv_record.loc));

cursor C_STORE_EXIST is
   select 'x'
     from store
    where store = LP_rtv_record.loc;

BEGIN
   --- Only the store system (SIM) will send RTV messages in approved status, but not
   --- the warehouse system (WMS). WMS will only send an RTV message when it's shipped.
   --- Once shipped, an RTV cannot be changed back to approved status.

   open C_WH_EXIST;
   fetch C_WH_EXIST into L_exist;
   close C_WH_EXIST;

   if L_exist = 'x' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_APPRV_WH_RTV',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   --- check if RTV exists, if not it is an externally generated RTV
   open C_RTV_EXISTS;
   fetch C_RTV_EXISTS into L_wh, L_store, L_status_ind;
   close C_RTV_EXISTS;

   if L_wh is NULL or L_store is NULL then
      open C_EXT_REF_RTV_EXISTS;
      fetch C_EXT_REF_RTV_EXISTS into L_wh, L_store, L_status_ind;
      close C_EXT_REF_RTV_EXISTS;
   end if;

   if L_wh is NULL then
      LP_valid_rtv := 'N';
   else
      if L_wh != -1 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_APPRV_WH_RTV',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      elsif L_status_ind = 15 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_APPV_SHIP_RTV',
                                               LP_rtv_record.rtv_order_no,
                                               NULL,
                                               NULL);
         return FALSE;
      elsif LP_rtv_record.loc != L_store then
         O_error_message := SQL_LIB.CREATE_MSG('INV_RTV_LOC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      else
         LP_valid_rtv := 'Y';
      end if;
   end if;
   ---
   L_exist := NULL;
   open C_STORE_EXIST;
   fetch C_STORE_EXIST into L_exist;
   close C_STORE_EXIST;
   ---
   if L_exist is NOT NULL then
      LP_rtv_record.loc_type := 'S';
      LP_rtv_record.store := LP_rtv_record.loc;
      LP_rtv_record.wh := -1;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_STORE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_RTV_HEAD_APPRV;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_RTV_DETAIL_APPRV (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'RTV_SQL.VALIDATE_RTV_DETAIL_APPROV';

    cursor C_INV_STATUS_CODES is
      select inv_status
        from inv_status_codes
       where inv_status_code = LP_rtv_record.detail_TBL.from_disps(LP_index);

   cursor C_PACKITEM(I_pack_no ITEM_MASTER.ITEM%TYPE) is
      select v.item,
             v.qty
        from v_packsku_qty v
       where v.pack_no = I_pack_no;

BEGIN

   if LP_rtv_record.detail_TBL.items is not NULL and LP_rtv_record.detail_TBL.items.count > 0 then
      for i in LP_rtv_record.detail_TBL.items.FIRST..LP_rtv_record.detail_TBL.items.LAST LOOP
         LP_index := i;
         ---
         if LP_rtv_record.im_row_TBL(i).orderable_ind = 'Y' and
            LP_rtv_record.im_row_TBL(i).sellable_ind  = 'N' and
            LP_rtv_record.im_row_TBL(i).inventory_ind = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_NONINVENT_ITEM', NULL, NULL, NULL);
            return FALSE;
         end if;
         ---
         if LP_rtv_record.im_row_TBL(i).item_level > LP_rtv_record.im_row_TBL(i).tran_level then
            -- ref item on message
            LP_rtv_record.detail_TBL.items(i) := LP_rtv_record.im_row_TBL(i).item_parent;
         elsif LP_rtv_record.im_row_TBL(i).item_level < LP_rtv_record.im_row_TBL(i).tran_level then
            O_error_message := SQL_LIB.CREATE_MSG('NO_PARENT_ITEM',
                                                  LP_rtv_record.detail_TBL.items(i),
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
         ---
         if LP_rtv_record.detail_TBL.weights(i) is NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                                  'LP_rtv_record.detail_TBL.weights',
                                                  'NOT NULL',
                                                  L_program);
            return FALSE;
         end if;
         ---
         if LP_rtv_record.detail_TBL.weight_uoms(i) is NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                                  'LP_rtv_record.detail_TBL.weight_uoms',
                                                  'NOT NULL',
                                                  L_program);
            return FALSE;
         end if;
         --- Fetching inv status for the from disposition.
         open C_INV_STATUS_CODES;
         fetch C_INV_STATUS_CODES into LP_rtv_record.detail_TBL.inv_statuses(LP_index);

         if C_INV_STATUS_CODES%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_DISPOSITION', NULL, NULL, NULL);
            close C_INV_STATUS_CODES;
            return FALSE;
         end if;
         close C_INV_STATUS_CODES;
         ---
         if RTV_SQL.VALIDATE_RTV_DETAIL(O_error_message) = FALSE then
            return FALSE;
         end if;
         ---
         -- if the location is a store, and the item is a pack, update the component record instead
         if LP_rtv_record.loc_type = 'S' and LP_rtv_record.im_row_TBL(i).pack_ind = 'Y' then
            FOR packitem in C_PACKITEM(LP_rtv_record.detail_TBL.items(i)) LOOP
               if UPD_ITEM_RTV_QTY(O_error_message,
                                   packitem.item,
                                   LP_rtv_record.loc,
                                   LP_rtv_record.loc_type,
                                   (LP_rtv_record.detail_TBL.returned_qtys(i) * packitem.qty),
                                   LP_rtv_record.detail_TBL.inv_statuses(i),
                                   'A',
                                   LP_rtv_record.rtv_order_no) = FALSE then
                  return FALSE;
              end if;
           END LOOP;
         else
            if UPD_ITEM_RTV_QTY(O_error_message,
                                LP_rtv_record.detail_TBL.items(i),
                                LP_rtv_record.loc,
                                LP_rtv_record.loc_type,
                                LP_rtv_record.detail_TBL.returned_qtys(i),
                                LP_rtv_record.detail_TBL.inv_statuses(i),
                                'A',
                                LP_rtv_record.rtv_order_no) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_RTV_DETAIL_APPRV;
--------------------------------------------------------------------------------------
FUNCTION BUILD_RTV_APPRV(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'RTV_SQL.BUILD_RTV_APPRV';

   L_rowid               ROWID := NULL;
   L_seq_no              RTV_DETAIL.SEQ_NO%TYPE;
   L_ret_courier         SUPS.RET_COURIER%TYPE;
   L_qty_requested       RTV_DETAIL.QTY_REQUESTED%TYPE;
   L_exists              VARCHAR2(1);

   db_ext_ref   rtv_head.ext_ref_no%TYPE;
   db_wh        wh.wh%TYPE;
   db_status    rtv_head.status_ind%TYPE;

   RECORD_LOCKED EXCEPTION;
   PRAGMA EXCEPTION_INIT(Record_Locked,-54);

   L_original_unit_cost  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;

   cursor C_DETAIL_EXISTS is
      select qty_requested, rowid
        from rtv_detail
       where rtv_order_no = LP_rtv_record.rtv_order_no
         and item = LP_rtv_record.detail_TBL.items(LP_index)
         and reason = LP_rtv_record.detail_TBL.reasons(LP_index)
         and ((inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)) or
              (inv_status is NULL and LP_rtv_record.detail_TBL.inv_statuses(LP_index) is NULL));

   cursor C_DETAIL_REC_EXISTS is
      select 'x'
        from rtv_detail
       where rtv_order_no = LP_rtv_record.rtv_order_no
         and NVL(qty_cancelled,-2) != NVL(qty_requested,-1)
         and rownum=1;

   cursor C_LOCK_RTV_DETAIL is
      select 'x'
        from rtv_detail
       where rowid = L_rowid
         for update nowait;

   cursor C_LOCK_RTV_HEAD is
      select 'x'
        from rtv_head
       where RTV_ORDER_NO = LP_rtv_record.rtv_order_no
         for update nowait;

   cursor C_MAX_SEQ_NO is
      select max(seq_no) + 1
        from rtv_detail
       where rtv_order_no = LP_rtv_record.rtv_order_no;

   cursor C_GET_COURIER is
      select ret_courier
        from sups
       where supplier = LP_rtv_record.supplier;

   cursor C_GET_SUP_INFO is
      select s.ret_courier,
             a.add_1,
             a.add_2,
             a.add_3,
             a.city,
             a.state,
             a.country_id,
             a.jurisdiction_code,
             a.post
        from sups s,
             addr a
       where s.supplier = LP_rtv_record.supplier
         and a.key_value_1 = to_char(LP_rtv_record.supplier)
         and module = 'SUPP'
         and s.ret_allow_ind = 'Y'
         and a.addr_type = '03';

BEGIN

   if LP_valid_rtv = 'N' then
      --- RTV not present in RMS. New RTV record to be created.
      if LP_rtv_record.ship_addr1 is NULL or LP_rtv_record.city is NULL or LP_rtv_record.country is NULL then
         -- address info on message is incomplete, use supplier address
         open C_GET_SUP_INFO;
         fetch C_GET_SUP_INFO into L_ret_courier,
                                   LP_rtv_record.ship_addr1,
                                   LP_rtv_record.ship_addr2,
                                   LP_rtv_record.ship_addr3,
                                   LP_rtv_record.city,
                                   LP_rtv_record.state,
                                   LP_rtv_record.country,
                                   LP_rtv_record.jurisdiction_code,
                                   LP_rtv_record.pcode;
         close C_GET_SUP_INFO;
      else
         open C_GET_COURIER;
         fetch C_GET_COURIER into L_ret_courier;
         close C_GET_COURIER;
      end if;
      ---

      insert into rtv_head (rtv_order_no,
                            supplier,
                            status_ind,
                            store,
                            wh,
                            total_order_amt,
                            ship_to_add_1,
                            ship_to_add_2,
                            ship_to_add_3,
                            ship_to_city,
                            state,
                            ship_to_country_id,
                            ship_to_jurisdiction_code,
                            ship_to_pcode,
                            ret_auth_num,
                            courier,
                            freight,
                            created_date,
                            completed_date,
                            restock_pct,
                            restock_cost,
                            ext_ref_no,
                            comment_desc)
                     values (LP_rtv_record.rtv_order_no,
                            LP_rtv_record.supplier,
                            12,   -- For RTVs created by SIM, always set it to In progress status to prevent modification in RMS.
                            LP_rtv_record.store,
                            LP_rtv_record.wh,
                            LP_rtv_record.total_order_amt_unit_based * (1-LP_rtv_record.restock_pct/100),
                            LP_rtv_record.ship_addr1,
                            LP_rtv_record.ship_addr2,
                            LP_rtv_record.ship_addr3,
                            LP_rtv_record.city,
                            LP_rtv_record.state,
                            LP_rtv_record.country,
                            LP_rtv_record.jurisdiction_code,
                            LP_rtv_record.pcode,
                            LP_rtv_record.ret_auth_num,
                            L_ret_courier,
                            NULL,
                            LP_rtv_record.tran_date,
                            NULL,
                            LP_rtv_record.restock_pct,
                            LP_rtv_record.total_order_amt_unit_based * (LP_rtv_record.restock_pct / 100), -- restock cost
                            LP_rtv_record.ext_ref_no,
                            LP_rtv_record.comments);

   end if;

   if LP_rtv_record.detail_TBL.items is not NULL and LP_rtv_record.detail_TBL.items.count > 0 then
      for i in LP_rtv_record.detail_TBL.items.FIRST..LP_rtv_record.detail_TBL.items.LAST
      LOOP
         LP_index := i;

         open C_MAX_SEQ_NO;
         fetch C_MAX_SEQ_NO into L_seq_no;
         close C_MAX_SEQ_NO;

         --- Initializing to NULL before checking for next detail record.
         L_rowid := NULL;
         L_original_unit_cost := NULL;

         open C_DETAIL_EXISTS;
         fetch C_DETAIL_EXISTS into L_qty_requested, L_rowid;
         close C_DETAIL_EXISTS;
         ---
         if L_rowid is NOT NULL then
            ---If RTV exists, Update the status to 12 (i.e I'n progress)
            open C_LOCK_RTV_HEAD;
            close C_LOCK_RTV_HEAD;
               ---If rtv exists, overwrite the status_ind to 12 (I'n progress)
               update rtv_head
                  set status_ind=12,
                      comment_desc = LP_rtv_record.comments
                where rtv_order_no = LP_rtv_record.rtv_order_no;

            --- RTV Details exists. Delete the detail if qty_requested result in 0; otherwise, update the qty.
            open C_LOCK_RTV_DETAIL;
            close C_LOCK_RTV_DETAIL;
            ---

            if LP_rtv_record.detail_TBL.returned_qtys(LP_index) <= 0 then
               --- RTV_detail.updated_by_rms_ind has been updated before deleting the rtv_detail record,
               --- inorder to make sure that the rtv detail deletes are not published, when the deletion
               --- is published from the external source. The logic for not publishing the record is based
               --- on the update_by_rms_ind column.
               /*update rtv_detail
                  set updated_by_rms_ind = 'N'
                where rowid = L_rowid;

               delete rtv_detail
                where rowid = L_rowid;*/

                -- if the returned qty is less than zero then update the cancelled qty

                update rtv_detail
                   set qty_cancelled = NVL(qty_cancelled, 0) + (-1*LP_rtv_record.detail_TBL.returned_qtys(LP_index)),
                       unit_cost = decode(LP_rtv_record.detail_TBL.unit_cost_exts(LP_index), NULL, unit_cost, LP_rtv_record.detail_TBL.unit_cost_supps(LP_index)),
                       updated_by_rms_ind = 'N',
                       reason = LP_rtv_record.detail_TBL.reasons(LP_index),
                       inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)
                 where rowid = L_rowid;

            else
               -- if unit_cost is in the message, overwrite rtv_detail.unit_cost with it (in supplier currency)
               update rtv_detail
                  set qty_requested = NVL(qty_requested, 0) + LP_rtv_record.detail_TBL.returned_qtys(LP_index),
                      unit_cost = decode(LP_rtv_record.detail_TBL.unit_cost_exts(LP_index), NULL, unit_cost, LP_rtv_record.detail_TBL.unit_cost_supps(LP_index)),
                      updated_by_rms_ind = 'N',
                      reason = LP_rtv_record.detail_TBL.reasons(LP_index),
                      inv_status = LP_rtv_record.detail_TBL.inv_statuses(LP_index)
                where rowid = L_rowid;
            end if;
         else
            --- Retrieve original unit cost
            if ITEMLOC_ATTRIB_SQL.GET_AV_COST(O_error_message,
                                              LP_rtv_record.detail_TBL.items(LP_index),
                                              LP_rtv_record.loc,
                                              LP_rtv_record.loc_type,
                                              L_original_unit_cost) = FALSE then
               return FALSE;
            end if;

            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                LP_rtv_record.loc,
                                                LP_rtv_record.loc_type,
                                                NULL,
                                                LP_rtv_record.supplier,
                                                'V',
                                                NULL,
                                                L_original_unit_cost,
                                                L_original_unit_cost,
                                                'C',
                                                NULL,
                                                NULL) = FALSE then
               return FALSE;
            end if;

            -- RTV Details does not exists. Inserting a new row.
            insert into rtv_detail (rtv_order_no,
                                    seq_no,
                                    item,
                                    shipment,
                                    inv_status,
                                    qty_requested,
                                    qty_returned,
                                    qty_cancelled,
                                    unit_cost,
                                    reason,
                                    restock_pct,
                                    publish_ind,
                                    original_unit_cost,
                                    updated_by_rms_ind)
                            values (LP_rtv_record.rtv_order_no,
                                    NVL(L_seq_no, 1),
                                    LP_rtv_record.detail_TBL.items(LP_index),
                                    NULL,
                                    LP_rtv_record.detail_TBL.inv_statuses(LP_index),
                                    LP_rtv_record.detail_TBL.returned_qtys(LP_index),
                                    0,
                                    0,
                                    LP_rtv_record.detail_TBL.unit_cost_supps(LP_index),
                                    LP_rtv_record.detail_TBL.reasons(LP_index),
                                    LP_rtv_record.detail_TBL.restock_pcts(LP_index),
                                    'Y',
                                    L_original_unit_cost,
                                    'N');
         end if;

         -- insert or update for a container item associated with the deposit item
         if LP_rtv_record.im_row_TBL(LP_index).deposit_item_type = LP_contents_code and
            LP_rtv_record.im_row_TBL(LP_index).container_item is NOT NULL then
            if RTV_SQL.PROCESS_DEPOSIT_ITEM(O_error_message,
                                            NULL,                                                 --seq_no
                                            LP_rtv_record.rtv_order_no,                           --I_rtv_order_no
                                            LP_rtv_record.supplier,                               --I_supplier
                                            LP_rtv_record.im_row_TBL(LP_index).container_item,    --I_container_item
                                            LP_rtv_record.detail_TBL.items(LP_index),             --I_contents_item
                                            LP_rtv_record.loc,                                    --I_loc
                                            LP_rtv_record.loc_type,                               --I_loc_type
                                            LP_rtv_record.detail_TBL.returned_qtys(LP_index),     --I_qty
                                            LP_rtv_record.detail_TBL.reasons(LP_index),           --I_reason
                                            LP_rtv_record.detail_TBL.inv_statuses(LP_index),      --I_inv_status
                                            'N',                                                  --I_online_ind
                                            'Y') = FALSE then                                     --I_approve_ind
               return FALSE;
            end if;
         end if;

      END LOOP;
   end if;

   open C_DETAIL_REC_EXISTS;
   fetch C_DETAIL_REC_EXISTS into L_exists;
   if C_DETAIL_REC_EXISTS%NOTFOUND then

      open C_LOCK_RTV_HEAD;
      close C_LOCK_RTV_HEAD;

      /*delete rtv_head
       where rtv_order_no = LP_rtv_record.rtv_order_no;*/

       update rtv_head
          set status_ind = 20
        where rtv_order_no = LP_rtv_record.rtv_order_no;
   end if;
   close C_DETAIL_REC_EXISTS;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'RTV_DETAIL',
                                             to_char(LP_rtv_record.rtv_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
   return FALSE;
END BUILD_RTV_APPRV;
--------------------------------------------------------------------------------------
FUNCTION APPROVE_RTV (O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_rtv_record     IN       RTV_SQL.RTV_RECORD)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(30) := 'RTV_SQL.APPROVE_RTV';
   ---
   L_country_desc           COUNTRY.COUNTRY_DESC%TYPE;
   L_state_desc             STATE.DESCRIPTION%TYPE;
   L_jurisdiction_desc      COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE;
   ---
   L_dup_exists             VARCHAR2(1)    := NULL;
   L_calling_module         VARCHAR2(12)   := NULL;
   L_calling_context        VARCHAR2(12)   := NULL;

BEGIN

   if INITIALIZE(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   LP_rtv_record  := I_rtv_record;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;
   ---
   if LP_rtv_record.jurisdiction_code is NOT NULL then
      L_calling_module  := ADDRESS_SQL.JURIS;
      L_calling_context := ADDRESS_SQL.JURIS;
   elsif LP_rtv_record.state is NOT NULL then
      L_calling_module  := ADDRESS_SQL.ST;
      L_calling_context := ADDRESS_SQL.ST;
   end if;
   ---
   if ADDRESS_SQL.CHECK_ADDR (O_error_message,
                              LP_rtv_record.country,
                              L_country_desc,
                              LP_rtv_record.state,
                              L_state_desc,
                              LP_rtv_record.jurisdiction_code,
                              L_jurisdiction_desc,
                              L_dup_exists,
                              L_calling_module,
                              L_calling_context) = FALSE then
      return FALSE;
   end if;
   ---
   if RTV_SQL.VALIDATE_RTV_HEAD_APPRV(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if RTV_SQL.VALIDATE_RTV_DETAIL_APPRV(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if RTV_SQL.BUILD_RTV_APPRV(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END APPROVE_RTV;
-----------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_rtv_order_no      IN OUT   RTV_HEAD.RTV_ORDER_NO%TYPE,
                    I_function_key      IN OUT   VARCHAR2,
                    I_seq_no            IN OUT   NUMBER)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(75)   := 'RTV_SQL.CUSTOM_VAL';
   L_custom_obj_rec      CUSTOM_OBJ_REC :=  CUSTOM_OBJ_REC();

BEGIN

   L_custom_obj_rec.function_key:= I_function_key;
   L_custom_obj_rec.call_seq_no:= I_seq_no;
   L_custom_obj_rec.rtv_order_no:= I_rtv_order_no;

   --call the custom code for client specific order approval
   if CALL_CUSTOM_SQL.EXEC_FUNCTION(O_error_message,
                                    L_custom_obj_rec) = FALSE then
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CUSTOM_VAL;

--------------------------------------------------------------------------------------
END;
/