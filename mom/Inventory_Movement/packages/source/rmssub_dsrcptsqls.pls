
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_DSRCPT_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PUBLIC PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION PERSIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message_type  IN     VARCHAR2,
                 I_dsrcpt_rec    IN     RMSSUB_DSRCPT.DSRCPT_REC_TYPE)
RETURN BOOLEAN;


END RMSSUB_DSRCPT_SQL;
/
