
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE WO_VALIDATE_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------
-- Function Name: EXIST
-- Purpose:       This function will check for the existence of the
--                passed criteria on the wo_head table.
-- Created:       28-Jul-98
------------------------------------------------------------------------
FUNCTION EXIST(O_error_message  IN OUT  VARCHAR2,
               O_exist	        IN OUT  VARCHAR2,
               I_wo_id	        IN	    WO_HEAD.WO_ID%TYPE,
               I_order_no       IN      WO_HEAD.ORDER_NO%TYPE,
               I_tsfalloc_no    IN      WO_HEAD.TSFALLOC_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
END WO_VALIDATE_SQL;
/
