
CREATE OR REPLACE PACKAGE BODY DEAL_PASSTHRU_SQL AS

TYPE DP_DEPT_TBL     is table of DEAL_PASSTHRU.DEPT%TYPE;
TYPE DP_SUPP_TBL     is table of DEAL_PASSTHRU.SUPPLIER%TYPE;
TYPE COSTING_LOC_TBL is table of DEAL_PASSTHRU.COSTING_LOC%TYPE;
TYPE LOCATION_TBL    is table of DEAL_PASSTHRU.LOCATION%TYPE;
TYPE PASSTHRU_PCT_TBL    is table of DEAL_PASSTHRU.PASSTHRU_PCT%TYPE;

LP_dept              DP_DEPT_TBL       := DP_DEPT_TBL();
LP_supplier          DP_SUPP_TBL       := DP_SUPP_TBL();
LP_costing_loc       COSTING_LOC_TBL   := COSTING_LOC_TBL();
LP_location          LOCATION_TBL      := LOCATION_TBL();
LP_passthru_pct      PASSTHRU_PCT_TBL  := PASSTHRU_PCT_TBL();

--------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_PASSTHRU
-- Purpose: This function will retrieve passthru records which meet the input criteria passed in.
--------------------------------------------------------------------------------------------------------------
PROCEDURE GET_PASSTHRU(O_DEAL_PASSTHRU_TBL     IN OUT     DEAL_PASSTHRU_TBL,
                       I_DEPT                  IN         DEAL_PASSTHRU.DEPT%TYPE,
                       I_SUPPLIER              IN         DEAL_PASSTHRU.SUPPLIER%TYPE,
                       I_COSTING_LOC           IN         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                       I_ORG_LEVEL             IN         CODE_DETAIL.CODE%TYPE,
                       I_ORG_VALUE             IN         DEAL_PASSTHRU.LOCATION%TYPE) IS

   L_program   VARCHAR2(40) := 'DEAL_PASSTHRU_SQL.GET_PASSTHRU';

   cursor C_GET_STORE_CUSTOMER_GROUP_REC is
      select distinct dp.dept,
             d.dept_name,
             dp.supplier,
             s.sup_name,
             dp.costing_loc,
             vcl.costing_loc_name,
             dp.location,
             v.store_name LOCATION_NAME,
             dp.loc_type,
             dp.passthru_pct,
             null,
             null
        from deal_passthru dp,
             wf_customer_group wcg,
             wf_customer wc,
             v_store v,
             period p,
             deps d,
             sups s,
             v_costing_loc vcl
       where dp.location = v.store
         and exists (select location
                       from wf_customer 
                      where wf_customer_group_id = I_org_value
                        and wf_customer_id = v.wf_customer_id)
         and wcg.wf_customer_group_id = I_org_value
         and NVL(v.store_close_date, p.vdate + 1) > p.vdate
         and dp.dept = NVL(I_dept, dp.dept)
         and dp.dept = d.dept
         and dp.supplier = NVL(I_supplier, dp.supplier)
         and dp.supplier = s.supplier
         and dp.costing_loc = NVL(I_costing_loc, dp.costing_loc)
         and dp.costing_loc = vcl.costing_loc;

   cursor C_GET_STORE_CUSTOMER_REC is
      select dp.dept,
             d.dept_name,
             dp.supplier,
             s.sup_name,
             dp.costing_loc,
             vcl.costing_loc_name,
             dp.location,
             v.store_name LOCATION_NAME,
             dp.loc_type,
             dp.passthru_pct,
             null,
             null
        from deal_passthru dp,
             v_store v,
             period p,
             deps d,
             sups s,
             v_costing_loc vcl
       where dp.location = v.store
         and v.wf_customer_id = I_org_value
         and NVL(v.store_close_date, p.vdate + 1) > p.vdate
         and dp.dept = NVL(I_dept, dp.dept)
         and dp.dept = d.dept
         and dp.supplier = NVL(I_supplier, dp.supplier)
         and dp.supplier = s.supplier
         and dp.costing_loc = NVL(I_costing_loc, dp.costing_loc)
         and dp.costing_loc = vcl.costing_loc;

   cursor C_GET_STORE_REC is
      select distinct dp.dept,
             d.dept_name,
             dp.supplier,
             s.sup_name,
             dp.costing_loc,
             vcl.costing_loc_name,
             dp.location,
             v.store_name LOCATION_NAME,
             dp.loc_type,
             dp.passthru_pct,
             null ERROR_MESSAGE,
             null RETURN_CODE
        from deal_passthru dp,
             v_store v,
             period p,
             deps d,
             sups s,
             v_costing_loc vcl
       where dp.location = v.store
         and dp.location = I_org_value
         and v.store_type in ('W', 'F')
         and NVL(v.store_close_date, p.vdate + 1) > p.vdate
         and dp.dept = NVL(I_dept, dp.dept)
         and dp.dept = d.dept
         and dp.supplier = NVL(I_supplier, dp.supplier)
         and dp.supplier = s.supplier
         and dp.costing_loc = NVL(I_costing_loc, dp.costing_loc)
         and dp.costing_loc = vcl.costing_loc;

   cursor C_GET_AREA_REC is
         select distinct dp.dept,
                d.dept_name,
                dp.supplier,
                s.sup_name,
                dp.costing_loc,
                vcl.costing_loc_name,
                dp.location,
                v.store_name LOCATION_NAME,
                dp.loc_type,
                dp.passthru_pct,
                null,
                null
           from deal_passthru dp,
                v_store v,
                period p,
                deps d,
                sups s,
                v_costing_loc vcl
          where dp.location = v.store
            and v.area = I_org_value
            and v.store_type in ('W', 'F')
            and NVL(v.store_close_date, p.vdate + 1) > p.vdate
            and dp.dept = NVL(I_dept, dp.dept)
            and dp.dept = d.dept
            and dp.supplier = NVL(I_supplier, dp.supplier)
            and dp.supplier = s.supplier
            and dp.costing_loc = NVL(I_costing_loc, dp.costing_loc)
            and dp.costing_loc = vcl.costing_loc;
   
   cursor C_GET_DISTRICT_REC is
         select distinct dp.dept,
                d.dept_name,
                dp.supplier,
                s.sup_name,
                dp.costing_loc,
                vcl.costing_loc_name,
                dp.location,
                v.store_name LOCATION_NAME,
                dp.loc_type,
                dp.passthru_pct,
                null,
                null
           from deal_passthru dp,
                v_store v,
                period p,
                deps d,
                sups s,
                v_costing_loc vcl
          where dp.location = v.store
            and v.district = I_org_value
            and v.store_type in ('W', 'F')
            and NVL(v.store_close_date, p.vdate + 1) > p.vdate
            and dp.dept = NVL(I_dept, dp.dept)
            and dp.dept = d.dept
            and dp.supplier = NVL(I_supplier, dp.supplier)
            and dp.supplier = s.supplier
            and dp.costing_loc = NVL(I_costing_loc, dp.costing_loc)
            and dp.costing_loc = vcl.costing_loc;
   
   cursor C_GET_REGION_REC is
         select distinct dp.dept,
                d.dept_name,
                dp.supplier,
                s.sup_name,
                dp.costing_loc,
                vcl.costing_loc_name,
                dp.location,
                v.store_name LOCATION_NAME,
                dp.loc_type,
                dp.passthru_pct,
                null,
                null
           from deal_passthru dp,
                v_store v,
                period p,
                deps d,
                sups s,
                v_costing_loc vcl
          where dp.location = v.store
            and v.region = I_org_value
            and v.store_type in ('W', 'F')
            and NVL(v.store_close_date, p.vdate + 1) > p.vdate
            and dp.dept = NVL(I_dept, dp.dept)
            and dp.dept = d.dept
            and dp.supplier = NVL(I_supplier, dp.supplier)
            and dp.supplier = s.supplier
            and dp.costing_loc = NVL(I_costing_loc, dp.costing_loc)
            and dp.costing_loc = vcl.costing_loc;
   
   cursor C_GET_LOCLIST_REC is
         select distinct dp.dept,
                d.dept_name,
                dp.supplier,
                s.sup_name,
                dp.costing_loc,
                vcl.costing_loc_name,
                dp.location,
                v.store_name LOCATION_NAME,
                dp.loc_type,
                dp.passthru_pct,
                NULL ERROR_MESSAGE,
                NULL RETURN_CODE
           from deal_passthru dp,
                v_store v,
                period p,
                deps d,
                sups s,
                v_costing_loc vcl,
                v_loc_list_head vll,
                loc_list_detail lld
          where vll.loc_list = I_org_value
            and vll.loc_list = lld.loc_list
            and dp.location = lld.location
            and dp.location = v.store
            and NVL(v.store_close_date, p.vdate + 1) > p.vdate
            and dp.dept = NVL(I_dept, dp.dept)
            and dp.dept = d.dept
            and dp.supplier = NVL(I_supplier, dp.supplier)
            and dp.supplier = s.supplier
            and dp.costing_loc = NVL(I_costing_loc, dp.costing_loc)
            and dp.costing_loc = vcl.costing_loc;

   cursor C_GET_ALL_REC is
      select dp.dept,
             d.dept_name,
             dp.supplier,
             s.sup_name,
             dp.costing_loc,
             vcl.costing_loc_name,
             dp.location,
             v.store_name LOCATION_NAME,
             dp.loc_type,
             dp.passthru_pct,
             NULL,
             NULL
        from deal_passthru dp,
             v_store v,
             period p,
             deps d,
             sups s,
             v_costing_loc vcl
       where dp.location = v.store
         and v.store_type in ('W', 'F')
         and NVL(v.store_close_date, p.vdate + 1) > p.vdate
         and dp.dept = NVL(I_dept, dp.dept)
         and dp.dept = d.dept
         and dp.supplier = NVL(I_supplier, dp.supplier)
         and dp.supplier = s.supplier
         and dp.costing_loc = NVL(I_costing_loc, dp.costing_loc)
         and dp.costing_loc = vcl.costing_loc;

BEGIN

   if (I_dept is NULL and I_supplier is NULL and I_costing_loc is NULL) and (I_org_level is NULL and I_org_value is NULL) then
      O_deal_passthru_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                 NULL,
                                                                 L_program,
                                                                 NULL);
      O_deal_passthru_tbl(1).return_code := 'FALSE';
      return;
   end if;

   if I_org_level = 'G' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_STORE_CUSTOMER_GROUP_REC',
                       'DEAL_PASSTHRU, V_STORE, WF_CUSTOMER_GROUP, WF_CUSTOMER, PERIOD',
                       NULL);
      open C_GET_STORE_CUSTOMER_GROUP_REC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_STORE_CUSTOMER_GROUP_REC',
                       'DEAL_PASSTHRU, V_STORE, WF_CUSTOMER_GROUP, WF_CUSTOMER, PERIOD',
                       NULL);
      fetch C_GET_STORE_CUSTOMER_GROUP_REC BULK COLLECT into O_deal_passthru_tbl;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_STORE_CUSTOMER_GROUP_REC',
                       'DEAL_PASSTHRU, V_STORE, WF_CUSTOMER_GROUP, WF_CUSTOMER, PERIOD',
                       NULL);
      close C_GET_STORE_CUSTOMER_GROUP_REC;
   elsif I_org_level = 'C' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_STORE_CUSTOMER_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      open C_GET_STORE_CUSTOMER_REC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_STORE_CUSTOMER_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      fetch C_GET_STORE_CUSTOMER_REC BULK COLLECT into O_deal_passthru_tbl;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_STORE_CUSTOMER_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      close C_GET_STORE_CUSTOMER_REC;
   elsif I_org_level = 'S' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_STORE_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      open C_GET_STORE_REC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_STORE_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      fetch C_GET_STORE_REC BULK COLLECT into O_deal_passthru_tbl;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_STORE_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      close C_GET_STORE_REC;
   elsif I_org_level = 'A' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_AREA_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      open C_GET_AREA_REC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_AREA_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      fetch C_GET_AREA_REC BULK COLLECT into O_deal_passthru_tbl;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_AREA_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      close C_GET_AREA_REC;
   elsif I_org_level = 'D' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_DISTRICT_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      open C_GET_DISTRICT_REC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_DISTRICT_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      fetch C_GET_DISTRICT_REC BULK COLLECT into O_deal_passthru_tbl;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_DISTRICT_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      close C_GET_DISTRICT_REC;
   elsif I_org_level = 'R' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_REGION_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      open C_GET_REGION_REC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_REGION_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      fetch C_GET_REGION_REC BULK COLLECT into O_deal_passthru_tbl;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_REGION_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      close C_GET_REGION_REC;
   elsif I_org_level = 'LL' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_LOCLIST_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      open C_GET_LOCLIST_REC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_LOCLIST_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      fetch C_GET_LOCLIST_REC BULK COLLECT into O_deal_passthru_tbl;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_LOCLIST_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      close C_GET_LOCLIST_REC;
   elsif I_org_level is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_ALL_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      open C_GET_ALL_REC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_ALL_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      fetch C_GET_ALL_REC BULK COLLECT into O_deal_passthru_tbl;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_ALL_REC',
                       'DEAL_PASSTHRU, V_STORE, PERIOD',
                       NULL);
      close C_GET_ALL_REC;
   end if;

   return;

EXCEPTION
   when OTHERS then
           
      O_deal_passthru_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
     O_deal_passthru_tbl(1).return_code := 'FALSE';

     return;

END GET_PASSTHRU;

--------------------------------------------------------------------------------------------------------------
-- Function Name: LOCATION_EXPLODE
-- Purpose: This function will take the org level type and org level value inputs and explode them down to the 
--          store level, and then insert records into the DEAL_PASSTHRU table for WF stores only.
--------------------------------------------------------------------------------------------------------------
FUNCTION LOCATION_EXPLODE(O_ERROR_MESSAGE     IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                          I_DEPT              IN         DEAL_PASSTHRU.DEPT%TYPE,
                          I_SUPPLIER          IN         DEAL_PASSTHRU.SUPPLIER%TYPE,
                          I_COSTING_LOC       IN         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                          I_ORG_LEVEL         IN         CODE_DETAIL.CODE%TYPE,
                          I_ORG_VALUE         IN         DEAL_PASSTHRU.LOCATION%TYPE,
                          I_PASSTHRU_PCT      IN         DEAL_PASSTHRU.PASSTHRU_PCT%TYPE,
                          I_OVERWRITE_IND     IN         VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(40) := 'DEAL_PASSTHRU_SQL.LOCATION_EXPLODE';
   L_store            V_STORE.STORE%TYPE := NULL;
   
   cursor C_CUSTOMER is
     select v.store
       from v_store v,
            period p
      where v.wf_customer_id = I_org_value
        and not exists (select location
                          from deal_passthru
                         where dept = I_dept
                           and supplier = I_supplier
                           and costing_loc = I_costing_loc
                           and location = v.store)
        and NVL(v.store_close_date, p.vdate + 1) > p.vdate;

   cursor C_CUSTOMER_GROUP is
      select v.store
        from v_store v,
             period p
       where exists (select wf_customer_id
                       from wf_customer
                      where wf_customer_group_id = I_org_value
                        and wf_customer_id = v.wf_customer_id)
         and not exists (select location
                           from deal_passthru
                          where dept = I_dept
                            and supplier = I_supplier
                            and costing_loc = I_costing_loc
                            and location = v.store)
         and NVL(v.store_close_date, p.vdate + 1) > p.vdate;

   cursor C_STORE is
      select v.store
        from v_store v,
             period p
       where v.store = I_org_value
         and not exists (select location
                           from deal_passthru
                          where dept = I_dept
                            and supplier = I_supplier
                            and costing_loc = I_costing_loc
                            and location = v.store)
         and NVL(v.store_close_date, p.vdate + 1) > p.vdate;

   cursor C_EXPLODE_STORES is
      select distinct vs.store
        from v_store vs,
             district dis,
             period p
       where ((TO_CHAR(vs.area) = I_org_value
                    and I_org_level = 'A')
          or  (TO_CHAR(vs.district) = I_org_value
                  and I_org_level = 'D')
          or  (TO_CHAR(vs.region) = I_org_value
                  and I_org_level = 'R')
          or  (I_org_level = 'LL'
                  and exists (select location
                                from loc_list_detail lld
                               where lld.loc_type = 'S'
                                 and lld.location = vs.store
                                 and to_char(lld.loc_list) = I_org_value)))
         and not exists (select location
                           from deal_passthru
                          where dept = I_dept
                            and supplier = I_supplier
                            and costing_loc = I_costing_loc
                            and location = vs.store)
         and NVL(vs.store_close_date, p.vdate + 1) > p.vdate
         and vs.district = dis.district
         and vs.store_type in ('W', 'F');

BEGIN

   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_costing_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_costing_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_org_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_org_level',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_org_value is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_org_value',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_passthru_pct is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_passthru_pct',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_overwrite_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_overwrite_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
  
   if I_overwrite_ind = 'Y' then
      if I_org_level = 'C' then  --Customer
         MERGE into deal_passthru dp USING
            (select v.store,
                    I_dept dept,
                    I_supplier supplier,
                    I_costing_loc costing_loc
               from v_store v
              where v.wf_customer_id = I_org_value) a
                 on (dp.location = a.store
                and dp.dept = a.dept
                and dp.supplier = a.supplier
                and dp.costing_loc = a.costing_loc)
         when matched then update set dp.passthru_pct = I_passthru_pct
         when not matched then insert (dp.dept,
                                       dp.supplier,
                                       dp.costing_loc,
                                       dp.location,
                                       dp.loc_type,
                                       dp.passthru_pct)
                               values (a.dept,
                                       a.supplier,
                                       a.costing_loc,
                                       a.store,
                                       'S',
                                       I_passthru_pct);
      elsif I_org_level = 'G' then  --Customer Group
         MERGE into deal_passthru dp USING
            (select v.store,
                    I_dept dept,
                    I_supplier supplier,
                    I_costing_loc costing_loc
               from v_store v
              where exists (select wf_customer_id
                              from wf_customer
                             where wf_customer_group_id = I_org_value
                               and wf_customer_id = v.wf_customer_id)) b
                 on (dp.location = b.store
                and dp.dept = b.dept
                and dp.supplier = b.supplier
                and dp.costing_loc = b.costing_loc)
         when matched then update set dp.passthru_pct = I_passthru_pct
         when not matched then insert (dp.dept,
                                       dp.supplier,
                                       dp.costing_loc,
                                       dp.location,
                                       dp.loc_type,
                                       dp.passthru_pct)
                               values (b.dept,
                                       b.supplier,
                                       b.costing_loc,
                                       b.store,
                                       'S',
                                       I_passthru_pct);
      elsif I_org_level = 'S' then --Store
         MERGE into deal_passthru dp USING
            (select v.store,
                    I_dept dept,
                    I_supplier supplier,
                    I_costing_loc costing_loc
               from v_store v
              where v.store = I_org_value) c
                 on (dp.location = c.store
                and dp.dept = c.dept
                and dp.supplier = c.supplier
                and dp.costing_loc = c.costing_loc)
         when matched then update set dp.passthru_pct = I_passthru_pct
         when not matched then insert (dp.dept,
                                       dp.supplier,
                                       dp.costing_loc,
                                       dp.location,
                                       dp.loc_type,
                                       dp.passthru_pct)
                               values (c.dept,
                                       c.supplier,
                                       c.costing_loc,
                                       c.store,
                                       'S',
                                       I_passthru_pct);
      elsif I_org_level = 'A' then --Area
         MERGE into deal_passthru dp USING
            (select v.store,
                    I_dept dept,
                    I_supplier supplier,
                    I_costing_loc costing_loc
               from v_store v
              where v.area = I_org_value
                and v.store_type in ('W','F')) b
                 on (dp.location = b.store
                and dp.dept = b.dept
                and dp.supplier = b.supplier
                and dp.costing_loc = b.costing_loc)
         when matched then update set dp.passthru_pct = I_passthru_pct
     when not matched then insert (dp.dept,
                                   dp.supplier,
                                   dp.costing_loc,
                                   dp.location,
                                   dp.loc_type,
                                   dp.passthru_pct)
                           values (b.dept,
                                   b.supplier,
                                   b.costing_loc,
                                   b.store,
                                   'S',
                                   I_passthru_pct);
      elsif I_org_level = 'D' then --District 
         MERGE into deal_passthru dp USING
            (select v.store,
                    I_dept dept,
                    I_supplier supplier,
                    I_costing_loc costing_loc
               from v_store v
              where v.district = I_org_value
                and v.store_type in ('W','F')) b
                 on (dp.location = b.store
                and dp.dept = b.dept
                and dp.supplier = b.supplier
                and dp.costing_loc = b.costing_loc)
         when matched then update set dp.passthru_pct = I_passthru_pct
     when not matched then insert (dp.dept,
                                   dp.supplier,
                                   dp.costing_loc,
                                   dp.location,
                                   dp.loc_type,
                                   dp.passthru_pct)
                           values (b.dept,
                                   b.supplier,
                                   b.costing_loc,
                                   b.store,
                                   'S',
                                   I_passthru_pct);
      elsif I_org_level = 'R' then --Region 
         MERGE into deal_passthru dp USING
            (select v.store,
                    I_dept dept,
                    I_supplier supplier,
                    I_costing_loc costing_loc
               from v_store v
              where v.region = I_org_value
                and v.store_type in ('W','F')) b
                 on (dp.location = b.store
                and dp.dept = b.dept
                and dp.supplier = b.supplier
                and dp.costing_loc = b.costing_loc)
         when matched then update set dp.passthru_pct = I_passthru_pct
     when not matched then insert (dp.dept,
                                   dp.supplier,
                                   dp.costing_loc,
                                   dp.location,
                                   dp.loc_type,
                                   dp.passthru_pct)
                           values (b.dept,
                                   b.supplier,
                                   b.costing_loc,
                                   b.store,
                                   'S',
                                   I_passthru_pct);
      elsif I_org_level = 'LL' then --Location List
         MERGE into deal_passthru dp USING
            (select v.store,
                    I_dept dept,
                    I_supplier supplier,
                    I_costing_loc costing_loc
               from v_store v
              where exists (select location
                              from loc_list_detail lld
                             where lld.location = v.store
                               and TO_CHAR(lld.loc_list) = I_org_value)) b
                 on (dp.location = b.store
                and dp.dept = b.dept
                and dp.supplier = b.supplier
                and dp.costing_loc = b.costing_loc)
         when matched then update set dp.passthru_pct = I_passthru_pct
     when not matched then insert (dp.dept,
                                   dp.supplier,
                                   dp.costing_loc,
                                   dp.location,
                                   dp.loc_type,
                                   dp.passthru_pct)
                           values (b.dept,
                                   b.supplier,
                                   b.costing_loc,
                                   b.store,
                                   'S',
                                   I_passthru_pct);
      end if; --Checking for org level
   else -- I_overwrite_ind = 'N'
      if I_org_level = 'C' then  --Customer

         FOR C_REC in C_CUSTOMER LOOP
         
            L_store := C_REC.store;

            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'DEAL_PASSTHRU',
                             NULL);

            insert into deal_passthru (dept,
                                       supplier,
                                       costing_loc,
                                       location,
                                       loc_type,
                                       passthru_pct)
                               values (I_dept,
                                       I_supplier,
                                       I_costing_loc,
                                       L_store,
                                       'S',
                                       I_passthru_pct);
         END LOOP;

      elsif I_org_level = 'G' then  --Customer Group

         FOR C_REC in C_CUSTOMER_GROUP LOOP
     
            L_store := C_REC.store;

            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'DEAL_PASSTHRU',
                             NULL);

            insert into deal_passthru (dept,
                                       supplier,
                                       costing_loc,
                                       location,
                                       loc_type,
                                       passthru_pct)
                               values (I_dept,
                                       I_supplier,
                                       I_costing_loc,
                                       L_store,
                                       'S',
                                       I_passthru_pct);
         END LOOP;

      elsif I_org_level = 'S' then  --Store

         FOR C_REC in C_STORE LOOP

            L_store := C_REC.store;

            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'DEAL_PASSTHRU',
                             NULL);

            insert into deal_passthru (dept,
                                       supplier,
                                       costing_loc,
                                       location,
                                       loc_type,
                                       passthru_pct)
                               values (I_dept,
                                       I_supplier,
                                       I_costing_loc,
                                       L_store,
                                       'S',
                                       I_passthru_pct);
         END LOOP;
      
      elsif I_org_level in ('A', 'D', 'R', 'LL') then

         FOR C_REC in C_EXPLODE_STORES LOOP

            L_store := C_REC.store;

            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'DEAL_PASSTHRU',
                             NULL);

            insert into deal_passthru (dept,
                                       supplier,
                                       costing_loc,
                                       location,
                                       loc_type,
                                       passthru_pct)
                               values (I_dept,
                                       I_supplier,
                                       I_costing_loc,
                                       L_store,
                                       'S',
                                       I_passthru_pct);
         END LOOP;

      end if; --check of org level
   end if; -- overwrite ind check

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCATION_EXPLODE;

--------------------------------------------------------------------------------------------------------------
-- Function Name: OVERLAP_CHECK
-- Purpose: This function will check and determine if records already exist for the dept, supplier, wh, 
--          group type, value combination passed in on the DEAL_PASSTHRU table.
--------------------------------------------------------------------------------------------------------------
FUNCTION OVERLAP_CHECK(O_ERROR_MESSAGE        IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                       O_OVERLAP_EXISTS       IN OUT     VARCHAR2,
                       I_DEPT                 IN         DEAL_PASSTHRU.DEPT%TYPE,
                       I_SUPPLIER             IN         DEAL_PASSTHRU.SUPPLIER%TYPE,
                       I_COSTING_LOC          IN         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                       I_ORG_LEVEL            IN         CODE_DETAIL.CODE%TYPE,
                       I_ORG_VALUE            IN         DEAL_PASSTHRU.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(40) := 'DEAL_PASSTHRU_SQL.OVERLAP_CHECK';
   L_exists           VARCHAR2(1) := NULL;


   cursor C_CHECK_STORE_REC_EXISTS is
      select 'Y'
        from (select dp.dept,
                     dp.supplier,
                     dp.costing_loc,
                     dp.location
                from deal_passthru dp,
                     v_store v,
                     period p
               where dp.dept = I_dept
                 and dp.supplier = I_supplier
                 and dp.costing_loc = I_costing_loc
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate
              intersect
              select I_dept,
                     I_supplier,
                     I_costing_loc,
                     v.store
                from v_store v,
                     period p
               where store = I_org_value
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate);

   cursor C_CHECK_GROUP_REC_EXISTS is
      select 'Y'
        from (select dp.dept,
                     dp.supplier,
                     dp.costing_loc,
                     dp.location
                from deal_passthru dp,
                     v_store v,
                     period p
               where dp.dept = I_dept
                 and dp.supplier = I_supplier
                 and dp.costing_loc = I_costing_loc
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate
              intersect
              select I_dept,
                     I_supplier,
                     I_costing_loc,
                     v.store
                from v_store v,
                     period p
               where exists (select wf_customer_id
                               from wf_customer
                              where wf_customer_group_id = I_org_value
                                and wf_customer_id = v.wf_customer_id)
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate);

   cursor C_CHECK_CUST_REC_EXISTS is
      select 'Y'
        from (select dp.dept,
                     dp.supplier,
                     dp.costing_loc,
                     dp.location
                from deal_passthru dp,
                     v_store v,
                     period p
               where dp.dept = I_dept
                 and dp.supplier = I_supplier
                 and dp.costing_loc = I_costing_loc
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate
              intersect
              select I_dept,
                     I_supplier,
                     I_costing_loc,
                     v.store
                from v_store v,
                     period p
               where wf_customer_id = I_org_value
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate);

   cursor C_CHECK_AREA_REC_EXISTS is
      select 'Y'
        from (select dp.dept,
                     dp.supplier,
                     dp.costing_loc,
                     dp.location
                from deal_passthru dp,
                     v_store v,
                     period p
               where dp.dept = I_dept
                 and dp.supplier = I_supplier
                 and dp.costing_loc = I_costing_loc
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate
              intersect
              select I_dept,
                     I_supplier,
                     I_costing_loc,
                     v.store
                from v_store v,
                     period p
               where v.area = I_org_value
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate);

   cursor C_CHECK_DISTRICT_REC_EXISTS is
      select 'Y'
        from (select dp.dept,
                     dp.supplier,
                     dp.costing_loc,
                     dp.location
                from deal_passthru dp,
                     v_store v,
                     period p
               where dp.dept = I_dept
                 and dp.supplier = I_supplier
                 and dp.costing_loc = I_costing_loc
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate
              intersect
              select I_dept,
                     I_supplier,
                     I_costing_loc,
                     v.store
                from v_store v,
                     period p
               where v.district = I_org_value
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate);

   cursor C_CHECK_REGION_REC_EXISTS is
      select 'Y'
        from (select dp.dept,
                     dp.supplier,
                     dp.costing_loc,
                     dp.location
                from deal_passthru dp,
                     v_store v,
                     period p
               where dp.dept = I_dept
                 and dp.supplier = I_supplier
                 and dp.costing_loc = I_costing_loc
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate
              intersect
              select I_dept,
                     I_supplier,
                     I_costing_loc,
                     v.store
                from v_store v,
                     period p
               where v.region = I_org_value
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate);

   cursor C_CHECK_LOCLIST_REC_EXISTS is
      select 'Y'
        from (select dp.dept,
                     dp.supplier,
                     dp.costing_loc,
                     dp.location
                from deal_passthru dp,
                     v_store v,
                     period p
               where dp.dept = I_dept
                 and dp.supplier = I_supplier
                 and dp.costing_loc = I_costing_loc
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate
              intersect
              select I_dept,
                     I_supplier,
                     I_costing_loc,
                     v.store
                from v_store v,
                     period p
               where exists (select v.store
                                from loc_list_detail lld
                               where lld.loc_type = 'S'
                                 and lld.location = v.store
                                 and to_char(lld.loc_list) = I_org_value)
                 and NVL(v.store_close_date, p.vdate + 1) > p.vdate);

BEGIN

   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_costing_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_costing_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_org_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_org_level',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_org_value is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_org_value',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_org_level = 'S' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_STORE_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      open C_CHECK_STORE_REC_EXISTS;
   
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_STORE_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      fetch C_CHECK_STORE_REC_EXISTS into L_exists;
   
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_STORE_REC_EXISTS',
                       'DEAL_PASSTHRU, VSTORE',
                       NULL);
      close C_CHECK_STORE_REC_EXISTS;
   elsif I_org_level = 'G' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_GROUP_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      open C_CHECK_GROUP_REC_EXISTS;
   
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_GROUP_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      fetch C_CHECK_GROUP_REC_EXISTS into L_exists;
   
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_GROUP_REC_EXISTS',
                       'DEAL_PASSTHRU, VSTORE',
                       NULL);
      close C_CHECK_GROUP_REC_EXISTS;
   elsif I_org_level = 'C' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_CUST_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      open C_CHECK_CUST_REC_EXISTS;
   
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_CUST_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      fetch C_CHECK_CUST_REC_EXISTS into L_exists;
   
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_CUST_REC_EXISTS',
                       'DEAL_PASSTHRU, VSTORE',
                       NULL);
      close C_CHECK_CUST_REC_EXISTS;
   elsif I_org_level = 'A' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_AREA_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      open C_CHECK_AREA_REC_EXISTS;
   
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_AREA_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      fetch C_CHECK_AREA_REC_EXISTS into L_exists;
   
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_AREA_REC_EXISTS',
                       'DEAL_PASSTHRU, VSTORE',
                       NULL);
      close C_CHECK_AREA_REC_EXISTS;
   elsif I_org_level = 'D' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_DISTRICT_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      open C_CHECK_DISTRICT_REC_EXISTS;
   
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_DISTRICT_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      fetch C_CHECK_DISTRICT_REC_EXISTS into L_exists;
   
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_DISTRICT_REC_EXISTS',
                       'DEAL_PASSTHRU, VSTORE',
                       NULL);
      close C_CHECK_DISTRICT_REC_EXISTS;
   elsif I_org_level = 'R' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_REGION_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      open C_CHECK_REGION_REC_EXISTS;
   
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_REGION_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      fetch C_CHECK_REGION_REC_EXISTS into L_exists;
   
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_REGION_REC_EXISTS',
                       'DEAL_PASSTHRU, VSTORE',
                       NULL);
      close C_CHECK_REGION_REC_EXISTS;
   elsif I_org_level = 'LL' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_LOCLIST_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      open C_CHECK_LOCLIST_REC_EXISTS;
   
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_LOCLIST_REC_EXISTS',
                       'DEAL_PASSTHRU, V_STORE',
                       NULL);
      fetch C_CHECK_LOCLIST_REC_EXISTS into L_exists;
   
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_LOCLIST_REC_EXISTS',
                       'DEAL_PASSTHRU, VSTORE',
                       NULL);
      close C_CHECK_LOCLIST_REC_EXISTS;
   end if;

   if L_exists is not NULL then
      O_overlap_exists := 'Y';
   else
      O_overlap_exists := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END OVERLAP_CHECK;

--------------------------------------------------------------------------------------------------------------
-- Function Name: PASSTHRU_MASS_UPDATE
-- Purpose: This function will, based on input parameters, either update existing records for dept/supplier/wh 
--          combinations passed in, or leave the existing values as is. New records will be inserted for any 
--          non-existing locations.
--------------------------------------------------------------------------------------------------------------
FUNCTION PASSTHRU_MASS_UPDATE(O_ERROR_MESSAGE      IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                              I_DEAL_PASSTHRU_TBL  IN         DEAL_PASSTHRU_TBL,
                              I_PASSTHRU_PCT       IN         DEAL_PASSTHRU.PASSTHRU_PCT%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(40) := 'DEAL_PASSTHRU_SQL.PASSTHRU_MASS_UPDATE';
   L_counter          NUMBER(10)   := 0;
   L_tbl_count        INTEGER;

BEGIN

   if I_passthru_pct is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_passthru_pct',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_deal_passthru_tbl(1).dept is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_passthru_tbl',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   L_tbl_count := I_deal_passthru_tbl.COUNT;

   LP_dept.extend(L_tbl_count);
   LP_supplier.extend(L_tbl_count);
   lp_costing_loc.extend(L_tbl_count);
   LP_location.extend(L_tbl_count);

   FOR i in I_deal_passthru_tbl.FIRST..I_deal_passthru_tbl.LAST LOOP
      L_counter := L_counter + 1;
      LP_dept(L_counter)     := I_deal_passthru_tbl(i).dept;
      LP_supplier(L_counter) := I_deal_passthru_tbl(i).supplier;
      lp_costing_loc(L_counter):= I_deal_passthru_tbl(i).costing_loc;
      LP_location(L_counter) := I_deal_passthru_tbl(i).location;
   END LOOP;

   FORALL i in 1..L_counter

      update deal_passthru
         set passthru_pct = I_passthru_pct
       where dept = LP_dept(i)
         and supplier = LP_supplier(i)
         and costing_loc = lp_costing_loc(i)
         and location = LP_location(i);

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PASSTHRU_MASS_UPDATE;

--------------------------------------------------------------------------------------------------------------
-- Function Name: UPD_PASSTHRU_VALUE
-- Purpose: This function will be used as the update procedure for the calling form's multi-record block.
--------------------------------------------------------------------------------------------------------------
FUNCTION UPD_PASSTHRU_VALUE(O_ERROR_MESSAGE        IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                            I_DEPT                 IN         DEAL_PASSTHRU.DEPT%TYPE,
                            I_SUPPLIER             IN         DEAL_PASSTHRU.SUPPLIER%TYPE,
                            I_COSTING_LOC          IN         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                            I_ORG_LEVEL            IN         CODE_DETAIL.CODE%TYPE,
                            I_ORG_VALUE            IN         DEAL_PASSTHRU.LOCATION%TYPE,
                            I_PASSTHRU_PCT         IN         DEAL_PASSTHRU.PASSTHRU_PCT%TYPE)
   RETURN BOOLEAN is

   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   L_program          VARCHAR2(40) := 'DEAL_PASSTHRU_SQL.UPD_PASSTHRU_VALUE';

   cursor C_DEAL_PASSTHRU_LOCK_TBL is
      select 'x'
        from deal_passthru
       where dept = I_dept
         and supplier = I_supplier
         and costing_loc = I_costing_loc
         and location = I_org_value
         for update of passthru_pct nowait;

BEGIN

   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_costing_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_costing_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_org_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_org_level',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_org_value is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_org_value',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_passthru_pct is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_passthru_pct',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_DEAL_PASSTHRU_LOCK_TBL',
                    'DEAL_PASSTHRU',
                    NULL);
   open C_DEAL_PASSTHRU_LOCK_TBL;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_DEAL_PASSTHRU_LOCK_TBL',
                    'DEAL_PASSTHRU',
                    NULL);
   close C_DEAL_PASSTHRU_LOCK_TBL;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'DEAL_PASSTHRU',
                    NULL);

   update deal_passthru
      set passthru_pct = I_passthru_pct
    where dept = I_dept
      and supplier = I_supplier
      and costing_loc = I_costing_loc
      and location = I_org_value;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'DEAL_PASSTHRU',
                                                               'I_dept:' || I_dept || 'I_supplier:' || I_supplier,
                                                               'I_costing_loc:' || I_costing_loc || 'I_org_value:' || I_org_value);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPD_PASSTHRU_VALUE;

--------------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_ALL_PASSTHRU_RECS
-- Purpose: This function will be used as the delete procedure for the calling form's multi-record block to 
--          delete all records from the DEAL_PASSTHRU table where location is equal to records from the input 
--          table type.
--------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ALL_PASSTHRU_RECS(O_ERROR_MESSAGE      IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_DEAL_PASSTHRU_TBL  IN         DEAL_PASSTHRU_TBL)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(45) := 'DEAL_PASSTHRU_SQL.DELETE_ALL_PASSTHRU_RECS';
   L_counter          NUMBER(10) := 0;
   L_tbl_count        INTEGER;

BEGIN

   if I_deal_passthru_tbl(1).dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            NULL,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   L_tbl_count := I_deal_passthru_tbl.COUNT;

   LP_dept.extend(L_tbl_count);
   LP_supplier.extend(L_tbl_count);
   lp_costing_loc.extend(L_tbl_count);
   LP_location.extend(L_tbl_count);

   FOR i in I_deal_passthru_tbl.FIRST..I_deal_passthru_tbl.LAST LOOP
      L_counter := L_counter + 1;
      LP_dept(L_counter)     := I_deal_passthru_tbl(i).dept;
      LP_supplier(L_counter) := I_deal_passthru_tbl(i).supplier;
      lp_costing_loc(L_counter)       := I_deal_passthru_tbl(i).costing_loc;
      LP_location(L_counter) := I_deal_passthru_tbl(i).location;
   END LOOP;

   FORALL i in 1..L_counter

      delete from deal_passthru
       where dept = LP_dept(i)
         and supplier = LP_supplier(i)
         and costing_loc = lp_costing_loc(i)
         and location = LP_location(i);

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ALL_PASSTHRU_RECS;

--------------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_PASSTHRU_RECS
-- Purpose: This function will be used as the delete procedure for the calling form's multi-record block to
--          delete records from the DEAL_PASSTHRU table.
--------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_PASSTHRU_RECS(O_ERROR_MESSAGE      IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                              I_DEPT               IN         DEAL_PASSTHRU.DEPT%TYPE,
                              I_SUPPLIER           IN         DEAL_PASSTHRU.SUPPLIER%TYPE,
                              I_COSTING_LOC        IN         DEAL_PASSTHRU.COSTING_LOC%TYPE,
                              I_LOCATION           IN         DEAL_PASSTHRU.LOCATION%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   L_program          VARCHAR2(40) := 'DEAL_PASSTHRU_SQL.DELETE_PASSTHRU_RECS';

   cursor C_DEAL_PASSTHRU_LOCK is
      select 'x'
        from deal_passthru
       where dept = I_dept
         and supplier = I_supplier
         and costing_loc = I_costing_loc
         and location = I_location
         for update nowait;

BEGIN

   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_costing_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_costing_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_location,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_DEAL_PASSTHRU_LOCK',
                    'DEAL_PASSTHRU',
                    NULL);
   open C_DEAL_PASSTHRU_LOCK;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_DEAL_PASSTHRU_LOCK',
                    'DEAL_PASSTHRU',
                    NULL);
   close C_DEAL_PASSTHRU_LOCK;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'DEAL_PASSTHRU',
                    NULL);

   delete from deal_passthru
    where dept = I_dept
      and supplier = I_supplier
      and costing_loc = I_costing_loc
      and location = I_location;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'DEAL_PASSTHRU',
                                                               NULL,
                                                               NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_PASSTHRU_RECS;
--------------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_GTT
-- Purpose: This function will be used to populate the table GTT_DEAL_PASSTHRU with the
--          affected records along with type of modification 
--------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_ERROR_MESSAGE          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_DEAL_PASSTHRU_NEW_TBL  IN OUT OBJ_DEAL_PASSTHRU_EVENT_TBL, 
                      O_DEAL_PASSTHRU_MOD_TBL  IN OUT OBJ_DEAL_PASSTHRU_EVENT_TBL,
                      O_DEAL_PASSTHRU_REM_TBL  IN OUT OBJ_DEAL_PASSTHRU_EVENT_TBL,
                      I_DEAL_PASSTHRU_TBL      IN     DEAL_PASSTHRU_TBL,
                      I_NEW_PASSTHRU_PCT       IN     GTT_DEAL_PASSTHRU.NEW_PASSTHRU_PCT%TYPE,
                      I_ACTION                 IN     COST_EVENT.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40) := 'DEAL_PASSTHRU_SQL.POPULATE_GTT';
   L_counter            NUMBER(10)   := 0;
   L_tbl_count          INTEGER;
   L_deal_passthru_rec  OBJ_DEAL_PASSTHRU_EVENT_REC;
   L_action             COST_EVENT.ACTION%TYPE;
         
   cursor C_ACTION is
      select distinct action 
        from gtt_deal_passthru;
      
   cursor C_DATA is
      select dept,           
             supplier,
             costing_loc,
             location,
             loc_type,       
             new_passthru_pct
        from gtt_deal_passthru
        where action = L_action;

BEGIN

   if I_new_passthru_pct is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_passthru_pct',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_action is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_deal_passthru_tbl(1).dept is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_passthru_tbl',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   L_tbl_count := I_deal_passthru_tbl.COUNT;

   LP_dept.extend(L_tbl_count);
   LP_supplier.extend(L_tbl_count);
   lp_costing_loc.extend(L_tbl_count);
   LP_location.extend(L_tbl_count);
   LP_passthru_pct.extend(L_tbl_count);

   FOR i in I_deal_passthru_tbl.FIRST..I_deal_passthru_tbl.LAST LOOP
      L_counter := L_counter + 1;
      LP_dept(L_counter)     := I_deal_passthru_tbl(i).dept;
      LP_supplier(L_counter) := I_deal_passthru_tbl(i).supplier;
      lp_costing_loc(L_counter)       := I_deal_passthru_tbl(i).costing_loc;
      LP_location(L_counter) := I_deal_passthru_tbl(i).location;
      LP_passthru_pct(L_counter) := I_deal_passthru_tbl(i).passthru_pct;
   END LOOP;

   FORALL i in 1..L_counter
      merge into gtt_deal_passthru
           using dual
              on (dept     = LP_dept(i) and
                  supplier = LP_supplier(i) and
                  costing_loc = lp_costing_loc (i) and
                  location = LP_location (i))
            when matched then
          update
             set old_passthru_pct = decode(action,'ADD',I_new_passthru_pct,old_passthru_pct),
                 new_passthru_pct =I_new_passthru_pct,
                 action = decode(I_action,'REM',decode(action,'ADD','ADD','MOD','REM',action),'ADD',decode(action,'REM','MOD',action),action)
                 
          delete 
           where action ='ADD'  and 
                 I_action = 'REM'
            when not matched then
          insert(dept,
                 supplier,
                 costing_loc,
                 location,
                 loc_type,
                 new_passthru_pct,
                 old_passthru_pct,
                 action)
          values(LP_dept(i),
                 LP_supplier(i),
                 lp_costing_loc(i),
                 LP_location(i),
                 'S',
                 decode(I_action,'REM',LP_passthru_pct(i),I_new_passthru_pct),
                 LP_passthru_pct(i),
                 I_action);
   
   delete from gtt_deal_passthru
    where action = 'MOD' 
      and new_passthru_pct = old_passthru_pct;
      
   if O_deal_passthru_new_tbl is not NULL then
      O_deal_passthru_new_tbl.DELETE;
   end if;
   if O_deal_passthru_mod_tbl is not NULL then
      O_deal_passthru_mod_tbl.DELETE;
   end if;
   if O_deal_passthru_rem_tbl is not NULL then
      O_deal_passthru_rem_tbl.DELETE;
   end if;
         
   FOR i IN C_ACTION LOOP
      L_action := i.action;
      if L_action = 'ADD' then
         O_deal_passthru_new_tbl := new OBJ_DEAL_PASSTHRU_EVENT_TBL();
      elsif L_action = 'MOD' then
         O_deal_passthru_mod_tbl := new OBJ_DEAL_PASSTHRU_EVENT_TBL();
      elsif L_action = 'REM' then
         O_deal_passthru_rem_tbl := new OBJ_DEAL_PASSTHRU_EVENT_TBL();
      end if;
      FOR j IN C_DATA LOOP
         L_deal_passthru_rec := new OBJ_DEAL_PASSTHRU_EVENT_REC(j.dept,           
                                                                j.supplier,          
                                                                j.location,
                                                                j.loc_type,
                                                                j.costing_loc,
                                                                j.new_passthru_pct); 
         if L_action = 'ADD' then
            O_deal_passthru_new_tbl.EXTEND;
            O_deal_passthru_new_tbl(O_deal_passthru_new_tbl.COUNT) := L_deal_passthru_rec;
         elsif L_action = 'MOD' then
            O_deal_passthru_mod_tbl.EXTEND;
            O_deal_passthru_mod_tbl(O_deal_passthru_mod_tbl.COUNT) := L_deal_passthru_rec;
         elsif L_action = 'REM' then
            O_deal_passthru_rem_tbl.EXTEND;
            O_deal_passthru_rem_tbl(O_deal_passthru_rem_tbl.COUNT) := L_deal_passthru_rec;
         end if;
      END LOOP;
   END LOOP;
             
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_GTT;
--------------------------------------------------------------------------------------------------------------
-- Function Name: FUTURE_COST_UPD
-- Purpose: This function will be used to call future cost engine with passed in object types
--------------------------------------------------------------------------------------------------------------
FUNCTION FUTURE_COST_UPD(O_ERROR_MESSAGE         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_NEW_DEAL_PASSTHRU_TBL IN     OBJ_DEAL_PASSTHRU_EVENT_TBL,
                         I_MOD_DEAL_PASSTHRU_TBL IN     OBJ_DEAL_PASSTHRU_EVENT_TBL,
                         I_REM_DEAL_PASSTHRU_TBL IN     OBJ_DEAL_PASSTHRU_EVENT_TBL)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(40) := 'DEAL_PASSTHRU_SQL.FUTURE_COST_UPD';
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_dummy      VARCHAR2(1);
   
   cursor C_new_deal_passthru_tbl_exists is
      select 'x'
        from table(cast(I_new_deal_passthru_tbl as OBJ_DEAL_PASSTHRU_EVENT_TBL))
       where rownum <2;
       
   cursor C_mod_deal_passthru_tbl_exists is
      select 'x'
        from table(cast(I_mod_deal_passthru_tbl as OBJ_DEAL_PASSTHRU_EVENT_TBL))
       where rownum <2;
       
       
   cursor C_rem_deal_passthru_tbl_exists is
      select 'x'
        from table(cast(I_rem_deal_passthru_tbl as OBJ_DEAL_PASSTHRU_EVENT_TBL))
       where rownum <2;
BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_NEW_DEAL_PASSTHRU_TBL_EXISTS',
                    'I_NEW_DEAL_PASSTHRU_TBL',
                    NULL);
   open C_NEW_DEAL_PASSTHRU_TBL_EXISTS;
      
   SQL_LIB.SET_MARK('FETCH',
                    'C_NEW_DEAL_PASSTHRU_TBL_EXISTS',
                    'I_NEW_DEAL_PASSTHRU_TBL',
                     NULL);
   fetch C_NEW_DEAL_PASSTHRU_TBL_EXISTS into L_dummy;
    
   SQL_LIB.SET_MARK('CLOSE',
                    'C_NEW_DEAL_PASSTHRU_TBL_EXISTS',
                    'I_NEW_DEAL_PASSTHRU_TBL',
                    NULL);
                    
   close C_NEW_DEAL_PASSTHRU_TBL_EXISTS;
   
   if L_dummy is not NULL then
      if FUTURE_COST_EVENT_SQL.ADD_DEAL_PASSTHRU_CHG(O_error_message,
                                                     L_cost_event_process_id,
                                                     I_new_deal_passthru_tbl,
                                                     'ADD',
                                                     USER) = FALSE then
         return FALSE;
      end if;
   end if;
   
   L_dummy := NULL;
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_MOD_DEAL_PASSTHRU_TBL_EXISTS',
                    'I_MOD_DEAL_PASSTHRU_TBL',
                    NULL);
   open C_MOD_DEAL_PASSTHRU_TBL_EXISTS;
         
   SQL_LIB.SET_MARK('FETCH',
                    'C_MOD_DEAL_PASSTHRU_TBL_EXISTS',
                    'I_MOD_DEAL_PASSTHRU_TBL',
                     NULL);
   fetch C_MOD_DEAL_PASSTHRU_TBL_EXISTS into L_dummy;
       
   SQL_LIB.SET_MARK('CLOSE',
                    'C_MOD_DEAL_PASSTHRU_TBL_EXISTS',
                    'I_MOD_DEAL_PASSTHRU_TBL',
                    NULL);
                    
   close C_MOD_DEAL_PASSTHRU_TBL_EXISTS;
                    
   if L_dummy is not NULL then
      if FUTURE_COST_EVENT_SQL.ADD_DEAL_PASSTHRU_CHG(O_error_message,
                                                     L_cost_event_process_id,
                                                     I_mod_deal_passthru_tbl,
                                                     'MOD',
                                                     USER) = FALSE then
          return FALSE;
      end if;
   end if;
   
   L_dummy := NULL;
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_REM_DEAL_PASSTHRU_TBL_EXISTS',
                    'I_REM_DEAL_PASSTHRU_TBL',
                    NULL);
   open C_REM_DEAL_PASSTHRU_TBL_EXISTS;
         
   SQL_LIB.SET_MARK('FETCH',
                    'C_REM_DEAL_PASSTHRU_TBL_EXISTS',
                    'I_REM_DEAL_PASSTHRU_TBL',
                     NULL);
   fetch C_REM_DEAL_PASSTHRU_TBL_EXISTS into L_dummy;
       
   SQL_LIB.SET_MARK('CLOSE',
                    'C_REM_DEAL_PASSTHRU_TBL_EXISTS',
                    'I_REM_DEAL_PASSTHRU_TBL',
                    NULL);
                    
   
   close C_REM_DEAL_PASSTHRU_TBL_EXISTS;
   
   if L_dummy is not NULL then
      if FUTURE_COST_EVENT_SQL.ADD_DEAL_PASSTHRU_CHG(O_error_message,
                                                     L_cost_event_process_id,
                                                     I_rem_deal_passthru_tbl,
                                                     'REM',
                                                     USER) = FALSE then
         return FALSE;
      end if;
   end if;
   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END FUTURE_COST_UPD;
--------------------------------------------------------------------------------------------------------------
END DEAL_PASSTHRU_SQL;
/
