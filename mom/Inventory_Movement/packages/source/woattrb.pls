
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY WO_ATTRIB_SQL AS

------------------------------------------------------------------------
FUNCTION NEXT_WO_ID(O_error_message  IN OUT  VARCHAR2,
                    O_wo_id	       IN OUT  WO_HEAD.WO_ID%TYPE) 
RETURN BOOLEAN is
---
L_program                VARCHAR2(64)   := 'WO_ATTRIB_SQL.NEXT_WO_ID';
L_exists                 VARCHAR2(1)    := NULL;
L_first_time             VARCHAR2(3)    := 'Yes';
L_wrap_sequence_number   WO_HEAD.WO_ID%TYPE;
---
cursor C_NEXTVAL is
   select workord_sequence.NEXTVAL
     from sys.dual;
---
cursor C_SEQ_EXISTS is
   select 'x'
     from wo_head
    where wo_id = O_wo_id;
---
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_NEXTVAL', 'WO_HEAD', NULL);
   open C_NEXTVAL;
   ---
   LOOP
   ---
      SQL_LIB.SET_MARK('FETCH', 'C_NEXTVAL', 'WO_HEAD', NULL);
      fetch C_NEXTVAL into O_wo_id;
      --- handling if sequence runs out of numbers, notify user to contact DBA
      if (L_first_time = 'Yes') then
          L_wrap_sequence_number := O_wo_id;
          L_first_time := 'No';
      elsif (O_wo_id = L_wrap_sequence_number) THEN
          O_error_message := 'NO_MORE_WO_ID_SEQUENCE';
          return FALSE;
      end if;
      ---
      /* does newly fetched sequence number exist on wo_head table for a work order? */
      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_EXISTS', 'WO_HEAD', NULL);
      open C_SEQ_EXISTS;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_EXISTS', 'WO_HEAD', NULL);
      fetch C_SEQ_EXISTS into L_exists;
      ---
      if (C_SEQ_EXISTS%NOTFOUND) then
          SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_EXISTS', 'WO_HEAD', NULL);
          close C_SEQ_EXISTS;
          EXIT;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_EXISTS', 'WO_HEAD', NULL);
      close C_SEQ_EXISTS;
      ---
   end LOOP;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_NEXTVAL', 'WO_HEAD', NULL);
   close C_NEXTVAL; 
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      RETURN FALSE;

END NEXT_WO_ID;
------------------------------------------------------------------------
FUNCTION GET_MAX_SEQ_NO(O_error_message IN OUT VARCHAR2,
                        O_max_seq_no    IN OUT WO_WIP.SEQ_NO%TYPE,
                        I_wo_id         IN     WO_WIP.WO_ID%TYPE)
RETURN BOOLEAN is
---
L_program  VARCHAR2(64)  := 'WO_ATTRIB_SQL.GET_MAX_SEQ_NO';
L_seq_no   VARCHAR2(1)   := NULL;
---
cursor C_MAX_SEQ_NO is
   select MAX(seq_no)
     from wo_wip
    where wo_id = I_wo_id;
---
BEGIN
   --- check input parameter
   if I_wo_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_MAX_SEQ_NO', 'WO_WIP', NULL);
   open C_MAX_SEQ_NO;
   SQL_LIB.SET_MARK('FETCH', 'C_MAX_SEQ_NO', 'WO_WIP', NULL);
   fetch C_MAX_SEQ_NO into L_seq_no;
   SQL_LIB.SET_MARK('CLOSE', 'C_MAX_SEQ_NO', 'WO_WIP', NULL);
   close C_MAX_SEQ_NO;
   ---
   if L_seq_no is NULL then
      O_max_seq_no := 0;
   else
      O_max_seq_no := L_seq_no;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      RETURN FALSE;

END GET_MAX_SEQ_NO;
------------------------------------------------------------------------
FUNCTION GET_NEXT_WO(O_error_message  IN OUT  VARCHAR2,
                     O_next_wo_id     IN OUT  WO_HEAD.WO_ID%TYPE,
                     O_last_wo_ind    IN OUT  BOOLEAN,
                     I_wo_id          IN      WO_HEAD.WO_ID%TYPE,
                     I_order_no       IN      WO_HEAD.ORDER_NO%TYPE,
                     I_tsfalloc_no    IN      WO_HEAD.TSFALLOC_NO%TYPE)
RETURN BOOLEAN is
---
L_program     VARCHAR2(64)         := 'WO_ATTRIB_SQL.GET_NEXT_WO';
L_wo_id       WO_HEAD.WO_ID%TYPE   := NULL;
L_small_wo_id WO_HEAD.WO_ID%TYPE   := NULL;
---
cursor C_WORK_ORDER_NUMBER is
   select wo_id
     from wo_head
    where ((order_no = I_order_no and order_no is not NULL)
       or (order_no is NULL and I_order_no is NULL))
      and ((tsfalloc_no = I_tsfalloc_no and tsfalloc_no is not NULL)
       or (tsfalloc_no is NULL and I_tsfalloc_no is NULL))
      and wo_id > I_wo_id
 order by wo_id asc;
---
cursor C_NULL_WORK_ORDER_NUMBER is
   select MIN(wo_id) 
     from wo_head
    where ((order_no = I_order_no and order_no is not NULL)
       or (order_no is NULL and I_order_no is NULL))
      and ((tsfalloc_no = I_tsfalloc_no and tsfalloc_no is not NULL)
       or (tsfalloc_no is NULL and I_tsfalloc_no is NULL));
---
BEGIN
   --- check input parameters
   if (I_order_no is NULL and I_tsfalloc_no is NULL) or
      (I_order_no is not NULL and I_tsfalloc_no is not NULL) then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      return FALSE;
   end if;
   ---
   if I_wo_id is not NULL then
   ---
      SQL_LIB.SET_MARK('OPEN', 'C_WORK_ORDER_NUMBER', 'WO_WIP', NULL);
      open C_WORK_ORDER_NUMBER;
      SQL_LIB.SET_MARK('FETCH', 'C_WORK_ORDER_NUMBER', 'WO_WIP', NULL);
      fetch C_WORK_ORDER_NUMBER into L_wo_id;
      SQL_LIB.SET_MARK('CLOSE', 'C_WORK_ORDER_NUMBER', 'WO_WIP', NULL);
      close C_WORK_ORDER_NUMBER;
      ---
      O_next_wo_id := L_wo_id;
      if L_wo_id is NULL then
         O_last_wo_ind := TRUE;
         return TRUE;
      else
         O_last_wo_ind := FALSE;
         return TRUE;  
      end if;
      --- 
   else 
   ---
      SQL_LIB.SET_MARK('OPEN', 'C_NULL_WORK_ORDER_NUMBER', 'WO_WIP', NULL);
      open C_NULL_WORK_ORDER_NUMBER;
      SQL_LIB.SET_MARK('FETCH', 'C_NULL_WORK_ORDER_NUMBER', 'WO_WIP', NULL);
      fetch C_NULL_WORK_ORDER_NUMBER into L_small_wo_id;
      SQL_LIB.SET_MARK('CLOSE', 'C_NULL_WORK_ORDER_NUMBER', 'WO_WIP', NULL);
      close C_NULL_WORK_ORDER_NUMBER;
      ---
      O_next_wo_id := L_small_wo_id;
      if L_small_wo_id is NULL then
         O_error_message := 'NO_WORKORDERS_EXIST';
         return FALSE;
      end if;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      RETURN FALSE;

END GET_NEXT_WO;
------------------------------------------------------------------------
FUNCTION GET_PREV_WO(O_error_message  IN OUT  VARCHAR2,
                     O_prev_wo_id     IN OUT  WO_HEAD.WO_ID%TYPE,
                     O_first_wo_ind   IN OUT  BOOLEAN,
                     I_wo_id          IN      WO_HEAD.WO_ID%TYPE,
                     I_order_no       IN      WO_HEAD.ORDER_NO%TYPE,
                     I_tsfalloc_no    IN      WO_HEAD.TSFALLOC_NO%TYPE)
RETURN BOOLEAN is
---
L_program      VARCHAR2(64)         := 'WO_ATTRIB_SQL.GET_PREV_WO';
L_wo_id        WO_HEAD.WO_ID%TYPE   := NULL;
L_small_wo_id  WO_HEAD.WO_ID%TYPE   := NULL;
---
cursor C_WORK_ORDER_NUMBER is
   select wo_id 
     from wo_head
    where ((order_no = I_order_no and order_no is not NULL)
       or (order_no is NULL and I_order_no is NULL))
      and ((tsfalloc_no = I_tsfalloc_no and tsfalloc_no is not NULL)
       or (tsfalloc_no is NULL and I_tsfalloc_no is NULL))
      and (wo_id < I_wo_id)
 order by wo_id desc;
---
BEGIN
   --- check input parameters
   if (I_wo_id is NULL) or
      (I_order_no is NULL and I_tsfalloc_no is NULL) or
      (I_order_no is not NULL and I_tsfalloc_no is not NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_WORK_ORDER_NUMBER', 'WO_WIP', NULL);
   open C_WORK_ORDER_NUMBER;
   SQL_LIB.SET_MARK('FETCH', 'C_WORK_ORDER_NUMBER', 'WO_WIP', NULL);
   fetch C_WORK_ORDER_NUMBER into L_wo_id;
   SQL_LIB.SET_MARK('CLOSE', 'C_WORK_ORDER_NUMBER', 'WO_WIP', NULL);
   close C_WORK_ORDER_NUMBER;
   ---
   O_prev_wo_id := L_wo_id;
   --- 
   if L_wo_id is NULL then
      O_first_wo_ind := TRUE;
      return TRUE;
   else
      O_first_wo_ind := FALSE;
      return TRUE;  
   end if;
   --- 
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      RETURN FALSE;

END GET_PREV_WO;
------------------------------------------------------------------------
FUNCTION GET_MAX_QTY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_max_qty       IN OUT ORDLOC.QTY_ORDERED%TYPE,
	             I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
	             I_item          IN     ORDLOC.ITEM%TYPE,
	             I_location      IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(50)  := 'WO_ATTRIB_SQL.GET_MAX_NAME';
   ---       
   cursor C_GET_MAX_QTY is
      select qty_ordered
        from ordloc
       where order_no = I_order_no
         and item = I_item
         and location = I_location;
       
BEGIN
   open C_GET_MAX_QTY;
   fetch C_GET_MAX_QTY into O_max_qty;
   ---
   if C_GET_MAX_QTY%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORD_ITEM_LOC', NULL, NULL, NULL);
      close C_GET_MAX_QTY;
      return FALSE;
   end if;
   ---
   close C_GET_MAX_QTY;
   ---
   return TRUE;
       
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_MAX_QTY;
------------------------------------------------------------------------
END WO_ATTRIB_SQL;
/



