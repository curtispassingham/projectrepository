CREATE OR REPLACE PACKAGE CORESVC_FULFILORD AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
-- Constant Variables

ACTION_TYPE_CREATE         CONSTANT VARCHAR2(6)  := 'create';
ACTION_TYPE_CANCEL         CONSTANT VARCHAR2(6)  := 'cancel';

PROCESS_STATUS_ERROR       CONSTANT VARCHAR2(1)  := 'E';
PROCESS_STATUS_VALIDATED   CONSTANT VARCHAR2(1)  := 'V';
PROCESS_STATUS_NEW         CONSTANT VARCHAR2(1)  := 'N';
PROCESS_STATUS_INACTIVE    CONSTANT VARCHAR2(1)  := 'I';
PROCESS_STATUS_COMPLETED   CONSTANT VARCHAR2(1)  := 'C';

ORDCUST_STATUS_PARTIAL     CONSTANT VARCHAR2(1)  := 'P';
ORDCUST_STATUS_NOT_CRE     CONSTANT VARCHAR2(1)  := 'X';
ORDCUST_STATUS_COMPLETE    CONSTANT VARCHAR2(1)  := 'C';

-------------------------------------------------------------------------------------
-- Procedure Name: CREATE_FULFILLMENT
-- Purpose       : This function contains the core logic to process customer order
--                 fulfillment create requests in the interface staging tables
--                 associated to a given a process and chunk ID.
-------------------------------------------------------------------------------------
FUNCTION CREATE_FULFILLMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_ordcust_ids     IN OUT   ID_TBL,
                            I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Procedure Name: CANCEL_FULFILLMENT
-- Purpose       : This function contains the core logic to process customer order
--                 fulfillment cancellation requests in the interface staging tables
--                 associated to a given a process and chunk ID.
-------------------------------------------------------------------------------------
FUNCTION CANCEL_FULFILLMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
END CORESVC_FULFILORD;
/
