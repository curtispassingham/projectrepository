CREATE OR REPLACE PACKAGE CORESVC_REPL_EXT_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name:  CALL_REPLROQ
---------------------------------------------------------------------------------------------
FUNCTION CALL_REPLROQ(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_thread_id           IN       NUMBER,
                      I_last_run_of_day     IN       VARCHAR2 DEFAULT 'Y')
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  SETUP_DATA
---------------------------------------------------------------------------------------------
FUNCTION SETUP_DATA(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_chunk_size        IN       RMS_PLSQL_BATCH_CONFIG.MAX_CHUNK_SIZE%TYPE,
                    I_last_run_of_day   IN       VARCHAR2 DEFAULT 'Y')
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  CONSUME
---------------------------------------------------------------------------------------------
FUNCTION CONSUME(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_thread_id           IN       NUMBER,
                 I_last_run_of_day     IN       VARCHAR2 DEFAULT 'Y')
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------

END CORESVC_REPL_EXT_SQL;
/
