CREATE OR REPLACE PACKAGE RMSSUB_XTSF_DEFAULT AUTHID CURRENT_USER AS

DEFAULT_FREIGHT_CODE   TSFHEAD.FREIGHT_CODE%TYPE  := 'N';
DEFAULT_TSF_TYPE       TSFHEAD.TSF_TYPE%TYPE      := 'MR';
DEFAULT_STATUS         TSFHEAD.STATUS%TYPE        := 'A';

-------------------------------------------------------------------------------------------------------
   -- Function Name: DEFAULT_SUPP_PACK_SIZE
   -- Purpose      : This function will get the default supp_pack_size for an item and its
   --                primary country and primary supplier.
-------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_SUPP_PACK_SIZE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_supp_pack_size  IN OUT   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XTSF_DEFAULT;
/
