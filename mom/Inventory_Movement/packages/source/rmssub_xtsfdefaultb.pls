CREATE OR REPLACE PACKAGE BODY RMSSUB_XTSF_DEFAULT AS

FUNCTION DEFAULT_SUPP_PACK_SIZE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_supp_pack_size  IN OUT   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XTSF_DEFAULT.DEFAULT_SUPP_PACK_SIZE';

BEGIN
   O_supp_pack_size := NULL;

   if SUPP_ITEM_ATTRIB_SQL.GET_SUPP_PACK_SIZE(O_error_message,
                                              O_supp_pack_size,
                                              I_item,
                                              NULL,
                                              NULL) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DEFAULT_SUPP_PACK_SIZE;
---------------------------------------------------------------------------------------------------------
END RMSSUB_XTSF_DEFAULT;
/
