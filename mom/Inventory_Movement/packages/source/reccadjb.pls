CREATE OR REPLACE PACKAGE BODY REC_COST_ADJ_SQL AS

   LP_use_elc              VARCHAR2(1) := 'N';
   LP_item                 item_master.item%TYPE;
   LP_sum_comp_cost        ordloc.unit_cost%TYPE;
   LP_tran_date            DATE;
   LP_order_no             ordhead.order_no%TYPE;
   LP_loc_type             ordloc.loc_type%TYPE;
   LP_location             ordloc.location%TYPE;
   LP_ordered_qty          ordloc.qty_received%TYPE;
   LP_receipt_qty          ordloc.qty_received%TYPE;
   LP_supplier             sups.supplier%TYPE;
   LP_local_currency       currencies.currency_code%TYPE;
   LP_order_currency       currencies.currency_code%TYPE;
   LP_prim_currency        currencies.currency_code%TYPE;
   LP_table                VARCHAR2(30);
   LP_elc_old_prim         ordloc.unit_cost%TYPE;
   LP_elc_new_prim         ordloc.unit_cost%TYPE;
   LP_tran_type            tran_data.tran_code%TYPE;
   LP_origin_country_id    ordsku.origin_country_id%TYPE;
   LP_import_country_id    ordhead.import_country_id%TYPE;
   LP_exchange_rate_ord    ordhead.exchange_rate%TYPE;
   LP_import_order_ind     ordhead.import_order_ind%TYPE;
   LP_expenses             ordloc.unit_cost%TYPE;
   LP_currency_exp         currencies.currency_code%TYPE;
   LP_exchange_rate_exp    currency_rates.exchange_rate%TYPE;
   LP_duty                 ordloc.unit_cost%TYPE;
   LP_currency_dty         currencies.currency_code%TYPE;
   LP_comp_item            v_packsku_qty.pack_no%TYPE;
   LP_comp_qty             ordloc.qty_ordered%TYPE;
   LP_comp_ord_qty         ordloc.qty_ordered%TYPE;
   LP_comp_rcv_qty         ordloc.qty_ordered%TYPE;
   LP_comp_cost            item_supp_country_loc.unit_cost%TYPE;
   LP_dept                 deps.dept%TYPE;
   LP_class                class.class%TYPE;
   LP_subclass             subclass.subclass%TYPE;
   LP_cost_diff_prim       ordloc.unit_cost%TYPE;
   LP_cost_diff_loc        ordloc.unit_cost%TYPE;
   LP_prev_zone_id         cost_zone.zone_id%TYPE;
   LP_zone_group_id        cost_zone.zone_group_id%TYPE;
   LP_pack_type            item_master.pack_type%TYPE;
   LP_adjust_matched_ind   VARCHAR2(1) := 'N';
   LP_cost_ratio           NUMBER;
   LP_default_tax_type     SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

   LP_new_comp_item_elc_prim     NUMBER(20,8) := 0;
   LP_new_comp_item_elc_loc      NUMBER(20,8) := 0;
   LP_old_comp_item_elc_loc      NUMBER(20,8) := 0;
   LP_new_vendor_pack_elc_prim   NUMBER(20,8) := 0;
   LP_new_vendor_pack_elc_loc    NUMBER(20,8) := 0;
   LP_old_vendor_pack_elc_loc    NUMBER(20,8) := 0;
   LP_alc_status                 ALC_HEAD.STATUS%TYPE := NULL;

   LP_pc_old_ord_cost       ordloc.unit_cost%TYPE;
   LP_pc_new_ord_cost       ordloc.unit_cost%TYPE;

   cursor CP_PACK_ITEMS is
      select vpq.item,
             vpq.qty,
             DECODE(LP_default_tax_type, 'GTAX', its.extended_base_cost, its.unit_cost)  unit_cost,
             im.dept,
             im.class,
             im.subclass
        from item_supp_country_loc its,
             item_master im,
             v_packsku_qty vpq
       where vpq.pack_no           = LP_item
         and vpq.item              = im.item
         and vpq.item              = its.item
         and its.supplier          = LP_supplier
         and its.origin_country_id = LP_origin_country_id
         and its.loc               = LP_location
         and its.loc_type          = LP_loc_type;

      cursor CP_ORDLOC is
      select location,
             loc_type,
             qty_ordered,
             nvl(qty_received,0) qty_received
        from ordloc
       where order_no            = LP_order_no
         and item                = LP_item
         and nvl(qty_received,0) > 0
         and location            = LP_location
         and loc_type            = LP_loc_type;
---------------------------------------------------------------------
FUNCTION UPDATE_ORDER_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_unit_cost            IN       ORDLOC.UNIT_COST%TYPE,
                           I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                           I_nwp_item             IN       ORDLOC.ITEM%TYPE,
                           I_order_item           IN       ORDLOC.ITEM%TYPE,
                           I_location             IN       ORDLOC.LOCATION%TYPE,
                           I_loc_type             IN       ORDLOC.LOC_TYPE%TYPE,
                           I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   L_program             VARCHAR2(60) := 'REC_COST_ADJ_SQL.UPDATE_ORDER_COST';
   L_physical_wh         WH.PHYSICAL_WH%TYPE;
   L_tmp                 VARCHAR2(1);
   L_pack                VARCHAR2(1);

   L_seq_no              SHIPSKU.SEQ_NO%TYPE;
   L_rowid               rowid;
   L_shipment            SHIPMENT.SHIPMENT%TYPE;
   L_new_shipment        SHIPMENT.SHIPMENT%TYPE;

   L_new_seq_no          SHIPSKU.SEQ_NO%TYPE;
   L_qty_expected        SHIPSKU.QTY_EXPECTED%TYPE;
   L_qty_received        SHIPSKU.QTY_RECEIVED%TYPE;
   L_qty_matched         SHIPSKU.QTY_MATCHED%TYPE;
   L_qty_unmatched       SHIPSKU.QTY_RECEIVED%TYPE;
   L_loc                 SHIPMENT.TO_LOC%TYPE;
   L_qty                 SHIPSKU.QTY_RECEIVED%TYPE;
   L_unit_cost           SHIPSKU.UNIT_COST%TYPE;

   L_dist_table          DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   --> Table returned from distribution call with locations and quantities

   cursor C_LOCK_ORDSKU is
      select rowid
        from ordloc
       where order_no = I_order_no
         and item     = I_order_item
         and location = I_location
         and loc_type = I_loc_type
         for update nowait;

   cursor C_LOCK_SHIPSKU is
      select ss.shipment,
             sk.qty_expected,
             sk.qty_received,
             NVL(sk.qty_matched,0) qty_matched,
             sk.qty_received - NVL(sk.qty_matched,0) qty_unmatched,
             sk.seq_no,
             sk.rowid,
             sk.unit_cost
        from shipsku  sk,
             shipment ss
       where sk.item     = I_order_item
         and sk.shipment = ss.shipment
         and ss.order_no = I_order_no
         and ( (  ss.to_loc = L_physical_wh and I_loc_type = 'W')
              or ss.to_loc = I_location )
         and ss.to_loc_type = I_loc_type
         for update nowait;

   cursor C_INVC_SHIP is
      select NVL(ss.parent_shipment,ss.shipment) shipment,
             SUM(NVL(sk.qty_matched,0)) qty_matched,
             unit_cost
        from shipsku  sk,
             shipment ss
       where sk.item     = I_order_item
         and sk.shipment = ss.shipment
         and ss.order_no = I_order_no
         and ( (  ss.to_loc = L_physical_wh and I_loc_type = 'W')
              or ss.to_loc = I_location )
         and ss.to_loc_type = I_loc_type
         group by NVL(ss.parent_shipment,ss.shipment),unit_cost;

   cursor C_GET_PHYSICAL is
      select physical_wh
        from wh
       where wh = I_location;

   -- The I_adjust_matched_ind passed as 'N' means that
   -- there is no need to write adjustment for the matched receipt and if it is set to
   -- 'Y'  there is a need to write the adjustment for the matched receipt too.

BEGIN
   LP_table := 'ordloc';
   ---
   if I_loc_type = 'W' then
      open C_GET_PHYSICAL;
      fetch C_GET_PHYSICAL into L_physical_wh;
      close C_GET_PHYSICAL;
   end if;

   open C_LOCK_ORDSKU;
   fetch C_LOCK_ORDSKU into L_rowid;
   close C_LOCK_ORDSKU;

   update ordloc
     set  unit_cost   = I_unit_cost,
          cost_source = 'MANL'
    where rowid = L_rowid;

   LP_table := 'shipsku';

   for c_rec in C_INVC_SHIP LOOP
      if (I_adjust_matched_ind  = 'Y') and  (c_rec.qty_matched != 0)
         and  (I_unit_cost - c_rec.unit_cost != 0) then
         if INVC_WRITE_SQL.WRITE_RCA_DBT_CRDT(O_error_message,
                                              I_order_no,
                                              NULL,     --I_invc_type
                                              USER,     --I_user_id
                                              NULL,     --I_ref_rsn_code
                                              NULL,     --I_supplier
                                              NULL,     --I_dbt_crdt_id
                                              c_rec.shipment,
                                              I_order_item,
                                              I_unit_cost,
                                              c_rec.unit_cost,
                                              c_rec.qty_matched) = FALSE then
            return FALSE;
         end if;
     end if; -- I_adjust_matched_ind  = 'Y'
   end loop;

   for c_rec IN C_LOCK_SHIPSKU LOOP
      --
      L_shipment          := c_rec.shipment;
      L_seq_no            := c_rec.seq_no;
      L_qty_expected      := c_rec.qty_expected;
      L_qty_received      := c_rec.qty_received;
      L_qty_matched       := c_rec.qty_matched;
      L_qty_unmatched     := c_rec.qty_unmatched;
      L_rowid             := c_rec.rowid;
      L_unit_cost         := c_rec.unit_cost;
      --
      if L_qty_matched = 0 then
         update shipsku ss
            set ss.unit_cost = I_unit_cost
          where rowid = L_rowid;
      elsif L_qty_received > L_qty_matched and L_qty_matched > 0 then

         -- Update the current receipt with the matched qty
         -- then there will be another line item created for unmatched qty for the item with
         -- the new cost I_unit_cost.

         update shipsku
            set qty_received = qty_matched,
                qty_expected = least(qty_expected, qty_matched)
         where rowid = L_rowid;

         if I_loc_type = 'W' then
            -- Call distribution package to get the distribution for the unmatched qty. The distribution for the unmatched
            -- qty will be used to subtract qty from the existing SHIPSKU_LOC record and also will be used to create
            -- the distribution for the unmatched qty
            if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                           L_dist_table,           -- O_dist_tab: results table returned by distribution
                                           I_order_item,           -- L_item
                                           L_physical_wh,          -- I_loc: phy_loc
                                           L_qty_unmatched,        -- I_qty
                                           'SHIPMENT',             -- I_CMI: calling module indicator
                                           NULL,                   -- I_inv_status
                                           NULL,                   -- I_to_loc_type,
                                           NULL,                   -- I_to_loc,
                                           I_order_no,             -- I_order_no
                                           L_shipment,             -- I_shipment
                                           L_seq_no) = FALSE then  -- I_seq_no
               return FALSE;
            end if;

            --- If no records returned on distribution table then error out
            if L_dist_table.count = 0 then
               O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRIBUTION', NULL, NULL, NULL);
               return FALSE;
            end if;
            --
            for i in 1 .. L_dist_table.count LOOP
               --
               L_loc := L_dist_table(i).wh;
               L_qty := L_dist_table(i).dist_qty;
               --

               update shipsku_loc
                  set qty_received = qty_received - L_qty
                where item     = I_order_item
                  and shipment = L_shipment
                  and seq_no   = L_seq_no
                  and to_loc   = L_loc;
            end loop;
            --
         end if; --  (I_loc_type = 'W')
         --
         if L_qty_unmatched > 0 then
            -- Create a new SHIPSKU record for the item in the shipment for the
            -- unmatched qty
            --- Fetch the next sequence number for insert into shipsku
            if SHIPSKU_ATTRIB_SQL.GET_NEXT_SEQ_NO(O_error_message,
                                                 L_new_seq_no,
                                                 L_shipment,
                                                 I_order_item) = FALSE then
                return FALSE;
            end if;
            -- Insert into shipsku table
            insert into shipsku(shipment,
                                seq_no,
                                item,
                                distro_no,
                                ref_item,
                                carton,
                                inv_status,
                                status_code,
                                qty_received,
                                unit_cost,
                                unit_retail,
                                qty_expected,
                                match_invc_id)
                         select shipment,                      -- shipment
                                L_new_seq_no,                  -- seq_no
                                item,                          -- item
                                distro_no,                     -- distro_no
                                ref_item,                      -- ref_item
                                carton,                        -- carton
                                inv_status,                    -- inv_status
                                'A',                           -- status_code
                                L_qty_unmatched,               -- qty_received
                                I_unit_cost,                   -- unit_cost, order currency
                                unit_retail,                   -- unit_retail, local currency
                                L_qty_expected - qty_expected, -- qty_expected
                                NULL                           -- match_invc_id
                          from shipsku
                         where item     = I_order_item
                           and shipment = L_shipment
                           and seq_no   = L_seq_no;
            --
            if I_loc_type = 'W' then
               if L_dist_table.count = 0  then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRIBUTION', NULL, NULL, NULL);
                  return FALSE;
               end if;
               for i in 1 .. L_dist_table.count LOOP
               --
                  L_loc := L_dist_table(i).wh;
                  L_qty := L_dist_table(i).dist_qty;
                  --
                  insert into shipsku_loc(shipment,
                                          seq_no,
                                          item,
                                          to_loc,
                                          qty_received)
                          values (L_shipment,
                                  L_new_seq_no,
                                  I_order_item,
                                  L_loc,
                                  L_qty);
               end loop;
            end if; -- ( I_loc_type = 'W')
         end if; -- L_qty_unmatched > 0
         --
      end if;  -- L_qty_received > L_qty_matched and L_qty_matched > 0
      --
      if (I_adjust_matched_ind  = 'Y') and  (L_qty_matched != 0)
         and  (I_unit_cost - L_unit_cost != 0) then

         update shipsku
            set orig_matched_cost = NVL(orig_matched_cost,L_unit_cost),
                unit_cost = I_unit_cost
          where rowid = L_rowid;
        --
     end if; -- I_adjust_matched_ind  = 'Y'
     --
   end loop;

   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack,
                                    L_tmp,
                                    L_tmp,
                                    L_tmp,
                                    I_nwp_item) = FALSE then
      return FALSE;
   end if;

   if (L_pack = 'N' or L_pack is null) then
      if nwp_update_sql.UPDATE_NWP_RECORD ( O_error_message
                                          , I_nwp_item          -- NWP item - component
                                          , I_order_item        -- order_item
                                          , I_location          -- location
                                          , I_loc_type          -- location type
                                          , I_order_no          -- order no
                                          , null                -- shipment
                                          , null                -- I_receipt_date
                                          , null                -- I_receipt_quantity
                                          , null                -- I_receipt_cost
                                          , I_unit_cost         -- I_unit_cost
                                          , null                -- unit adjustment
                                          , null                -- I_ord_currency (not required for unit adjust)
                                          , null                -- I_loc_currency (not required for unit adjust)
                                          , null                -- I_ord_exchange_rate (not required for unit adjust)
                                          ,'PO'
                                          ) =  FALSE then
         return FALSE;
      end if;
   end if;

   RETURN TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',LP_table,
                                            TO_CHAR(I_order_no),(I_order_item));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END UPDATE_ORDER_COST;
--------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_unit_cost            IN       ORDLOC.UNIT_COST%TYPE,
                           I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                           I_item                 IN       ORDLOC.ITEM%TYPE,
                           I_location             IN       ORDLOC.LOCATION%TYPE,
                           I_loc_type             IN       ORDLOC.LOC_TYPE%TYPE,
                           I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(60) := 'REC_COST_ADJ_SQL.UPDATE_ORDER_COST';

BEGIN
   if (UPDATE_ORDER_COST(O_error_message,
                         I_unit_cost,
                         I_order_no,
                         I_item,
                         I_item,
                         I_location,
                         I_loc_type,
                         I_adjust_matched_ind) = FALSE) then
      RETURN FALSE;
   end if;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END UPDATE_ORDER_COST;
--------------------------------------------------------------------------------
FUNCTION UPDATE_SUP_DATA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_dept            IN       sup_data.dept%TYPE,
                         I_supplier        IN       sup_data.supplier%TYPE,
                         I_tran_date       IN       DATE,
                         I_tran_type       IN       sup_data.tran_type%TYPE,
                         I_amount          IN       sup_data.amount%TYPE)
RETURN BOOLEAN IS
   L_program             VARCHAR2(60) := 'REC_COST_ADJ_SQL.UPDATE_SUP_DATA';

BEGIN
   SQL_LIB.SET_MARK('INSERT', NULL, 'SUP_DATA', NULL);
   insert into sup_data(dept,
                        supplier,
                        day_date,
                        tran_type,
                        amount)
                 values(I_dept,
                        I_supplier,
                        I_tran_date,
                        I_tran_type,
                        I_amount);
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END UPDATE_SUP_DATA;
---------------------------------------------------------------------------
FUNCTION UPDATE_STOCK_LEDGER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'REC_COST_ADJ_SQL.UPDATE_STOCK_LEDGER';
   L_cost_diff_prim    NUMBER(20,8);
   L_cost_diff_temp    NUMBER(20,8);

   L_new_cost_loc      NUMBER(20,8);
   L_old_cost_loc      NUMBER(20,8);
   L_new_pc_loc        NUMBER(20,8);
   L_old_pc_loc        NUMBER(20,8);
   L_new_cost_loc_excl_elc NUMBER(20,8);
   L_old_cost_loc_excl_elc NUMBER(20,8);

BEGIN
   -- convert new cost (excluding ELC) to location currency
   if CURRENCY_SQL.CONVERT(O_error_message,
                           LP_pc_new_ord_cost,  
                           LP_order_currency, -- from curr
                           LP_local_currency, -- to curr
                           L_new_pc_loc,      -- to amount
                           'C',
                           NULL,
                           NULL,
                           NULL,
                           NULL) = FALSE then
      return FALSE;
   end if;

   -- convert old cost (excluding ELC) to location currency
   if CURRENCY_SQL.CONVERT(O_error_message,
                           LP_pc_old_ord_cost,  
                           LP_order_currency, -- from curr
                           LP_local_currency, -- to curr
                           L_old_pc_loc,      -- to amount
                           'C',
                           NULL,
                           NULL,
                           NULL,
                           NULL) = FALSE then
      return FALSE;
   end if;

   if LP_pack_type = 'B' then
      L_new_cost_loc   := LP_new_comp_item_elc_loc;
      L_old_cost_loc   := LP_old_comp_item_elc_loc;

      L_cost_diff_temp := (LP_new_comp_item_elc_loc - LP_old_comp_item_elc_loc);

      if LP_sum_comp_cost = 0 then
         L_new_cost_loc_excl_elc := L_new_pc_loc * LP_cost_ratio;
         L_old_cost_loc_excl_elc := L_old_pc_loc * LP_cost_ratio;
      else
         L_new_cost_loc_excl_elc := L_new_pc_loc * LP_comp_cost / LP_sum_comp_cost;
         L_old_cost_loc_excl_elc := L_old_pc_loc * LP_comp_cost / LP_sum_comp_cost;
      end if;

      if LP_prim_currency = LP_local_currency then
         L_cost_diff_prim := L_cost_diff_temp;
      else

         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_cost_diff_temp,
                                 LP_local_currency,
                                 LP_prim_currency,
                                 L_cost_diff_prim,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      if LP_sum_comp_cost = 0 then
         L_new_cost_loc := LP_new_vendor_pack_elc_loc * LP_cost_ratio;
         L_old_cost_loc := LP_old_vendor_pack_elc_loc * LP_cost_ratio;
         L_new_cost_loc_excl_elc := L_new_pc_loc * LP_cost_ratio;
         L_old_cost_loc_excl_elc := L_old_pc_loc * LP_cost_ratio;
      else
         L_new_cost_loc := LP_new_vendor_pack_elc_loc * LP_comp_cost / LP_sum_comp_cost;
         L_old_cost_loc := LP_old_vendor_pack_elc_loc * LP_comp_cost / LP_sum_comp_cost;
         L_new_cost_loc_excl_elc := L_new_pc_loc * LP_comp_cost / LP_sum_comp_cost;
         L_old_cost_loc_excl_elc := L_old_pc_loc * LP_comp_cost / LP_sum_comp_cost;
      end if;

      L_cost_diff_temp := LP_new_vendor_pack_elc_loc - LP_old_vendor_pack_elc_loc;
      ---
      if LP_prim_currency = LP_local_currency then
         L_cost_diff_prim := L_cost_diff_temp;
      else

         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_cost_diff_temp,
                                 LP_local_currency,
                                 LP_prim_currency,
                                 L_cost_diff_prim,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if LP_sum_comp_cost = 0 then
         L_cost_diff_prim := L_cost_diff_prim * LP_cost_ratio;
      else
         L_cost_diff_prim := L_cost_diff_prim * LP_comp_cost / LP_sum_comp_cost;
      end if;
   end if;

   if ITEMLOC_UPDATE_SQL.UPD_AV_COST_CHANGE_COST(O_error_message ,
                                                 LP_comp_item,
                                                 LP_location,
                                                 LP_loc_type,
                                                 L_new_cost_loc,
                                                 L_old_cost_loc,
                                                 LP_comp_rcv_qty,
                                                 NULL,
                                                 NULL,
                                                 'Y',
                                                 LP_order_no,
                                                 NULL,
                                                 LP_item,
                                                 L_program,
                                                 'C',
                                                 NULL,
                                                 L_new_cost_loc_excl_elc,
                                                 L_old_cost_loc_excl_elc,
                                                 LP_adjust_matched_ind) = FALSE then

      return FALSE;
   end if;

   if OTB_SQL.ORD_RECEIVE( O_error_message  ,
                           0                ,
                           L_cost_diff_prim ,
                           LP_order_no      ,
                           LP_dept          ,
                           LP_class         ,
                           LP_subclass      ,
                           LP_comp_rcv_qty  ,
                           LP_comp_ord_qty ) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
   return FALSE;

END UPDATE_STOCK_LEDGER;
---------------------------------------------------------------------
FUNCTION BUYER_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(60) := 'REC_COST_ADJ_SQL.BUYER_PACK';

   cursor C_GET_OLD_COMP_ELC is
      select landed_cost
        from comp_item_elc_temp
       where order_no  = LP_order_no
         and pack_item = LP_item
         and item      = LP_comp_item
         and location  = LP_location;

BEGIN

   FOR packsku_rec IN CP_PACK_ITEMS LOOP
      LP_comp_item  := packsku_rec.item;
      LP_comp_qty   := packsku_rec.qty;
      LP_comp_cost  := packsku_rec.unit_cost;
      LP_dept       := packsku_rec.dept;
      LP_class      := packsku_rec.class;
      LP_subclass   := packsku_rec.subclass;
      ---
      FOR c_rec IN cp_ordloc LOOP
         LP_location    := c_rec.location;
         LP_loc_type    := c_rec.loc_type;
         LP_ordered_qty := c_rec.qty_ordered;
         -- This function will return the shipment qty_received when LP_adjust_matched_ind
         -- is set to 'Y' and otherwise will return the unmatched qty.
         if REC_COST_ADJ_SQL.GET_PO_UNMATCHED_QTY(O_error_message,
                                                  LP_receipt_qty,
                                                  LP_item,
                                                  LP_order_no,
                                                  LP_location,
                                                  LP_loc_type,
                                                  LP_adjust_matched_ind) = FALSE then
            return FALSE;
         end if;
         ---
         LP_comp_ord_qty := LP_comp_qty * LP_ordered_qty;
         LP_comp_rcv_qty := LP_comp_qty * LP_receipt_qty;
         ---
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                      LP_location,
                                      LP_loc_type,
                                      NULL,
                                      LP_local_currency) = FALSE then
            return FALSE;
         end if;
         ---
         -- Get new ELC for component item
         ---
         if LP_alc_status = 'PW' then
            --- get elc total for finalized order
            if REC_COST_ADJ_SQL.CALC_ELC(O_error_message,
                                         LP_new_comp_item_elc_prim,
                                         LP_order_no,
                                         LP_item,
                                         LP_comp_item,
                                         LP_location,
                                         LP_supplier,
                                         LP_origin_country_id,
                                         LP_import_country_id,
                                         NULL) = FALSE then
               return FALSE;
            end if;
         else
            if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                        LP_new_comp_item_elc_prim,
                                        LP_expenses,
                                        LP_currency_exp,
                                        LP_exchange_rate_exp,
                                        LP_duty,
                                        LP_currency_dty,
                                        LP_order_no,
                                        LP_item,
                                        LP_comp_item,
                                        NULL,
                                        LP_location,
                                        LP_supplier,
                                        LP_origin_country_id,
                                        LP_import_country_id,
                                        NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         -- Convert to local curr
         ---
         if LP_prim_currency = LP_local_currency then
            LP_new_comp_item_elc_loc := LP_new_comp_item_elc_prim;
         else
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    LP_new_comp_item_elc_prim,
                                    LP_prim_currency,
                                    LP_local_currency,
                                    LP_new_comp_item_elc_loc,
                                    'C',
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         open C_GET_OLD_COMP_ELC;
         fetch C_GET_OLD_COMP_ELC into LP_old_comp_item_elc_loc;
         close C_GET_OLD_COMP_ELC;
         ---
         if UPDATE_STOCK_LEDGER(O_error_message) = FALSE then
            return FALSE;
         end if;
      END LOOP;  -- loop ordloc
   END LOOP;  -- loop v_packsku_qty;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END BUYER_PACK;
---------------------------------------------------------------------
FUNCTION VENDOR_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'REC_COST_ADJ_SQL.VENDOR_PACK';

   cursor C_GET_OLD_PACK_ELC is
      select landed_cost
        from comp_item_elc_temp
       where order_no  = LP_order_no
         and pack_item = LP_item
         and item is NULL
         and location  = LP_location;

BEGIN

   FOR c_rec IN CP_ORDLOC LOOP
   
      LP_location    := c_rec.location;
      LP_loc_type    := c_rec.loc_type;
      LP_ordered_qty := c_rec.qty_ordered;
      -- This function will return the shipment qty_received when LP_adjust_matched_ind
      -- is set to 'Y' and otherwise will return the unmatched qty.
      if REC_COST_ADJ_SQL.GET_PO_UNMATCHED_QTY(O_error_message,
                                               LP_receipt_qty,
                                               LP_item,
                                               LP_order_no,
                                               LP_location,
                                               LP_loc_type,
                                               LP_adjust_matched_ind) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                   LP_location,
                                   LP_loc_type,
                                   NULL,
                                   LP_local_currency) = FALSE then
         return FALSE;
      end if;
      ---
      -- Get new ELC for vendor pack
      ---
      if LP_alc_status = 'PW' then
         ---- get elc total for finalized order
         if REC_COST_ADJ_SQL.CALC_ELC(O_error_message,
                                      LP_new_vendor_pack_elc_prim,
                                      LP_order_no,
                                      LP_item,
                                      NULL,
                                      LP_location,
                                      LP_supplier,
                                      LP_origin_country_id,
                                      LP_import_country_id,
                                      NULL) = FALSE then
            return FALSE;
         end if;
      else
         if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                     LP_new_vendor_pack_elc_prim,
                                     LP_expenses,
                                     LP_currency_exp,
                                     LP_exchange_rate_exp,
                                     LP_duty,
                                     LP_currency_dty,
                                     LP_order_no,
                                     LP_item,
                                     NULL,
                                     NULL,
                                     LP_location,
                                     LP_supplier,
                                     LP_origin_country_id,
                                     LP_import_country_id,
                                     NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- Convert to local curr
      ---
      if LP_prim_currency = LP_local_currency then
         LP_new_vendor_pack_elc_loc := LP_new_vendor_pack_elc_prim;
      else
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 LP_new_vendor_pack_elc_prim,
                                 LP_prim_currency,
                                 LP_local_currency,
                                 LP_new_vendor_pack_elc_loc,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;
         ---
         open C_GET_OLD_PACK_ELC;
         fetch C_GET_OLD_PACK_ELC into LP_old_vendor_pack_elc_loc;
         close C_GET_OLD_PACK_ELC;
         ---
      FOR packsku_rec IN cp_pack_items LOOP
         LP_comp_item      := packsku_rec.item;
         LP_comp_ord_qty   := packsku_rec.qty * LP_ordered_qty;
         LP_comp_rcv_qty   := packsku_rec.qty * LP_receipt_qty;
         LP_comp_cost      := packsku_rec.unit_cost;
         LP_dept           := packsku_rec.dept;
         LP_class          := packsku_rec.class;
         LP_subclass       := packsku_rec.subclass;
         ---
         if UPDATE_STOCK_LEDGER(O_error_message) = FALSE then
            return FALSE;
         end if;
      END LOOP;  -- packsku loop
   END LOOP;  -- ordloc loop
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END VENDOR_PACK;
---------------------------------------------------------------------
FUNCTION UPD_PACK_ITEM(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_pack_item            IN       ordsku.item%TYPE,
                       I_tran_date            IN       DATE,
                       I_order_no             IN       ordhead.order_no%TYPE,
                       I_pc_old_ord           IN       ordloc.unit_cost%TYPE,
                       I_pc_new_ord           IN       ordloc.unit_cost%TYPE,
                       I_supplier             IN       sups.supplier%TYPE,
                       I_origin_country_id    IN       ordsku.origin_country_id%TYPE,
                       I_import_country_id    IN       ordhead.import_country_id%TYPE,
                       I_exchange_rate_ord    IN       ordhead.exchange_rate%TYPE,
                       I_prim_currency        IN       currencies.currency_code%TYPE,
                       I_order_currency       IN       currencies.currency_code%TYPE,
                       I_alc_status           IN       alc_head.status%TYPE,
                       I_import_order_ind     IN       ordhead.import_order_ind%TYPE,
                       I_import_ind           IN       system_options.import_ind%TYPE,
                       I_elc_ind              IN       system_options.elc_ind%TYPE,
                       I_location             IN       ordloc.location%TYPE,
                       I_loc_type             IN       ordloc.loc_type%TYPE,
                       I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'REC_COST_ADJ_SQL.UPD_PACK_ITEM';
   L_sum_comp_qty      V_PACKSKU_QTY.QTY%TYPE;

   cursor C_PACK_TYPE is
      select pack_type
        from item_master
       where item = LP_item;

   cursor C_SUM_COMP is
      select SUM(v.qty * i.unit_cost),
             SUM(v.qty)
        from v_packsku_qty v,
             item_supp_country_loc i
       where v.pack_no           = LP_item
         and i.item              = v.item
         and i.supplier          = LP_supplier
         and i.origin_country_id = LP_origin_country_id
         and i.loc               = LP_location
         and i.loc_type          = LP_loc_type;

BEGIN

   LP_item               := I_pack_item;
   LP_tran_date          := I_tran_date;
   LP_order_no           := I_order_no;
   LP_supplier           := I_supplier;
   LP_tran_type          := 20;
   LP_origin_country_id  := I_origin_country_id;
   LP_import_country_id  := I_import_country_id;
   LP_exchange_rate_ord  := I_exchange_rate_ord;
   LP_prim_currency      := I_prim_currency;
   LP_order_currency     := I_order_currency;
   LP_import_order_ind   := I_import_order_ind;
   LP_location           := I_location;
   LP_loc_type           := I_loc_type;
   LP_pc_old_ord_cost    := I_pc_old_ord;
   LP_pc_new_ord_cost    := I_pc_new_ord;
   LP_alc_status         := I_alc_status;
   LP_adjust_matched_ind := I_adjust_matched_ind;

   open C_PACK_TYPE;
   fetch C_PACK_TYPE into LP_pack_type;
   if C_PACK_TYPE%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PAK',LP_item,NULL,NULL);
      close C_PACK_TYPE;
      return FALSE;
   end if;
   close C_PACK_TYPE;

   open C_SUM_COMP;
   fetch C_SUM_COMP into LP_sum_comp_cost,
                         L_sum_comp_qty;
   if C_SUM_COMP%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PAK',LP_item,NULL,NULL);
      close C_SUM_COMP;
      return FALSE;    end if;
   close C_SUM_COMP;

   if LP_sum_comp_cost = 0 then
      LP_cost_ratio := 1 / L_sum_comp_qty;
   end if;

   if LP_pack_type = 'B' then
      if BUYER_PACK(O_error_message) = FALSE then
         return FALSE;
      end if;
   else
      if VENDOR_PACK(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END UPD_PACK_ITEM;
---------------------------------------------------------------------
FUNCTION ITEM(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
              I_item                  IN       ORDSKU.ITEM%TYPE,
              I_dept                  IN       DEPS.DEPT%TYPE,
              I_class                 IN       CLASS.CLASS%TYPE,
              I_subclass              IN       SUBCLASS.SUBCLASS%TYPE,
              I_tran_date             IN       DATE,
              I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
              I_pc_diff_loc           IN       ORDLOC.UNIT_COST%TYPE,
              I_lc_diff_loc           IN       ORDLOC.UNIT_COST%TYPE,
              I_qty_ordered           IN       ORDLOC.QTY_RECEIVED%TYPE,
              I_qty_received          IN       ORDLOC.QTY_RECEIVED%TYPE,
              I_loc_currency          IN       ORDHEAD.CURRENCY_CODE%TYPE,
              I_prim_currency         IN       CURRENCIES.CURRENCY_CODE%TYPE,
              I_avg_cost_new          IN       ITEM_LOC_SOH.AV_COST%TYPE,
              I_neg_soh_wac_adj_amt   IN       ITEM_LOC_SOH.AV_COST%TYPE,
              I_loc_type              IN       INV_STATUS_QTY.LOC_TYPE%TYPE,
              I_location              IN       INV_STATUS_QTY.LOCATION%TYPE,
              I_supplier              IN       SUPS.SUPPLIER%TYPE,
              I_elc_ind               IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
              I_import_ind            IN       SYSTEM_OPTIONS.IMPORT_IND%TYPE,
              I_alc_status            IN       ALC_HEAD.STATUS%TYPE,
              I_adj_qty               IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE DEFAULT NULL,
              I_new_po_cost           IN       ORDLOC.UNIT_COST%TYPE,
              I_old_po_cost           IN       ORDLOC.UNIT_COST%TYPE,
              I_new_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE,
              I_old_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE,
              I_adjust_matched_ind    IN       VARCHAR2 DEFAULT 'N')

RETURN BOOLEAN IS

   L_total_cost       ORDLOC.UNIT_COST%TYPE        := 0;
   L_tran_code        TRAN_DATA.TRAN_CODE%TYPE     := 20;
   L_otb              VARCHAR2(1)                  := NULL;
   L_cost_diff_loc    ORDLOC.UNIT_COST%TYPE        := 0;
   L_cost_diff_prim   ORDLOC.UNIT_COST%TYPE        := 0;
   L_pc_diff_prim     ORDLOC.UNIT_COST%TYPE        := 0;
   L_qty_ordered      ORDLOC.QTY_ORDERED%TYPE      := NULL;
   L_system_options   SYSTEM_OPTIONS%ROWTYPE;
   L_avg_cost_new     ITEM_LOC_SOH.AV_COST%TYPE;

   L_program          VARCHAR2(60) := 'REC_COST_ADJ_SQL.ITEM';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor C_OTB is
      select otb_calc_type
        from deps
       where dept = I_dept;

BEGIN

   if (I_elc_ind = 'Y' and I_import_ind = 'N') or
      (I_import_ind = 'Y' and I_alc_status != 'PW') then
      L_cost_diff_loc := I_lc_diff_loc;
   else
      L_cost_diff_loc := I_pc_diff_loc;
   end if;

   L_total_cost := I_qty_received * L_cost_diff_loc;

   if I_qty_received > 0 then
      if ITEMLOC_UPDATE_SQL.UPD_AV_COST_CHANGE_COST(O_error_message,
                                                    I_item,
                                                    I_location,
                                                    I_loc_type,
                                                    I_new_po_cost,
                                                    I_old_po_cost,
                                                    I_qty_received,
                                                    I_avg_cost_new,
                                                    I_neg_soh_wac_adj_amt,
                                                    'Y',
                                                    I_order_no,
                                                    NULL,
                                                    NULL,
                                                    'REC_COST_ADJ_SQL.ITEM',
                                                    'C',
                                                    NULL,
                                                    I_new_cost_excl_elc,
                                                    I_old_cost_excl_elc,
                                                    I_adjust_matched_ind) = FALSE then
         return FALSE;
      end if;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_OTB',
                    'DEPS',
                    'DEPT: '|| TO_CHAR(I_dept));
   open C_OTB;

   SQL_LIB.SET_MARK('FETCH',
                    'C_OTB',
                    'DEPS',
                    'DEPT: '|| TO_CHAR(I_dept));
   fetch C_OTB into L_otb;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_OTB',
                    'DEPS',
                    'DEPT: '|| TO_CHAR(I_dept));
   close C_OTB;

   if L_otb = 'C' then
      --Converts the cost difference from local currency to primary currency
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_cost_diff_loc,
                              I_loc_currency,
                              I_prim_currency,
                              L_cost_diff_prim,
                              'C',
                              NULL,
                              NULL) = FALSE then
         RETURN FALSE;
      end if;

      if OTB_SQL.ORD_RECEIVE(O_error_message,
                             0,
                             L_cost_diff_prim,
                             I_order_no,
                             I_dept,
                             I_class,
                             I_subclass,
                             I_qty_received,
                             I_qty_ordered) = FALSE then
         RETURN FALSE;
      end if;
   end if; --- L_otb = 'C'

   if I_order_no is not NULL then
      if CREATE_ORD_TSF_SQL.UPDATE_SG_TSF_DETAIL(O_error_message,
                                                  I_order_no,
                                                  I_location,
                                                  I_loc_type,
                                                  I_item,
                                                  I_new_po_cost) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            (I_item),
                                            TO_CHAR(I_location));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ITEM;
---------------------------------------------------------------------
FUNCTION CHECK_DEALS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_min_unit_cost   IN OUT   ordloc_invc_cost.unit_cost%TYPE,
                     O_max_unit_cost   IN OUT   ordloc_invc_cost.unit_cost%TYPE,
                     I_order_no        IN       ordhead.order_no%TYPE,
                     I_item            IN       ordsku.item%TYPE,
                     I_location        IN       ordloc_invc_cost.location%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(60) := 'REC_COST_ADJ_SQL.CHECK_DEALS';

   cursor C_GET_COSTS is
      select min(unit_cost),
             max(unit_cost)
        from ordloc_invc_cost
       where order_no = I_order_no
         and item     = I_item
         and location = I_location;

BEGIN

   open C_GET_COSTS;
   fetch C_GET_COSTS into O_min_unit_cost,
                          O_max_unit_cost;
   close C_GET_COSTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_DEALS;
---------------------------------------------------------------------
FUNCTION CALC_DEAL_AVG_COST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_avg_cost        IN OUT ordloc.unit_cost%TYPE,
                            I_order_no        IN     ordhead.order_no%TYPE,
                            I_item            IN     ordsku.item%TYPE,
                            I_location        IN     ordloc_invc_cost.location%TYPE,
                            I_total_order_qty IN     ordloc.qty_ordered%TYPE,
                            I_buy_cost        IN     ordloc_invc_cost.unit_cost%TYPE,
                            I_get_cost        IN     ordloc_invc_cost.unit_cost%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(60)                          := 'REC_COST_ADJ_SQL.CALC_DEAL_AVG_COST';
   L_thresh_buy_qty        deal_detail.qty_thresh_buy_qty%TYPE   := 0;
   L_thresh_get_qty        deal_detail.qty_thresh_get_qty%TYPE   := 0;
   L_recur_ind             deal_detail.qty_thresh_recur_ind%TYPE := 'N';
   L_remain_ord_qty        ordloc.qty_ordered%TYPE               := 0;
   L_buy_qty               ordloc.qty_ordered%TYPE               := 0;
   L_get_qty               ordloc.qty_ordered%TYPE               := 0;

   cursor C_GET_THRESHOLD_INFO is
      select NVL(dd.qty_thresh_buy_qty,0),
             NVL(dd.qty_thresh_recur_ind,'N'),
             NVL(dd.qty_thresh_get_qty,0)
        from deal_detail dd,
             ordloc_discount od
       where od.order_no             = I_order_no
         and od.item                 = I_item
         and od.location             = I_location
         and od.deal_id              = dd.deal_id
         and od.deal_detail_id       = dd.deal_detail_id
         and dd.threshold_value_type = 'Q'
         and dd.qty_thresh_buy_item  = I_item
         and dd.qty_thresh_get_item  = I_item;

BEGIN

   open C_GET_THRESHOLD_INFO;
   fetch C_GET_THRESHOLD_INFO into L_thresh_buy_qty,
                                   L_recur_ind,
                                   L_thresh_get_qty;
   close C_GET_THRESHOLD_INFO;
   ---
   if L_thresh_buy_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NOT_BUY_GET_DEAL',
                                            NULL,
                                            NULL,
                                            NULL);
      O_avg_cost := (I_buy_cost + I_get_cost) / 2;
      return TRUE;
   end if;

   -- add any "unused" buy qty to the current receipt
   L_remain_ord_qty := I_total_order_qty;
   LOOP
      if (L_remain_ord_qty - L_thresh_buy_qty) > 0 then
         L_buy_qty := L_buy_qty + L_thresh_buy_qty;
         L_remain_ord_qty := L_remain_ord_qty - L_thresh_buy_qty;
      else
         L_buy_qty := L_buy_qty + L_remain_ord_qty;
         L_remain_ord_qty := 0;
      end if;
      if (L_remain_ord_qty - L_thresh_get_qty) > 0 then
         L_get_qty := L_get_qty + L_thresh_get_qty;
         L_remain_ord_qty := L_remain_ord_qty - L_thresh_get_qty;
      else
         L_get_qty := L_get_qty + L_remain_ord_qty;
         L_remain_ord_qty := 0;
      end if;
      if L_recur_ind = 'N' or L_remain_ord_qty = 0 then
         -- if no recursion but still some receipt qty
         L_buy_qty := L_buy_qty + L_remain_ord_qty;
         EXIT;
      end if;
   end LOOP;

   O_avg_cost := ((L_buy_qty * I_buy_cost) +
                  (L_get_qty * I_get_cost)) /
                 I_total_order_qty;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_DEAL_AVG_COST;
---------------------------------------------------------------------
FUNCTION UPDATE_DEALS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN     ordhead.order_no%TYPE,
                      I_item            IN     ordsku.item%TYPE,
                      I_location        IN     ordloc.location%TYPE,
                      I_old_cost_1      IN     ordloc.unit_cost%TYPE,
                      I_new_cost_1      IN     ordloc.unit_cost%TYPE,
                      I_old_cost_2      IN     ordloc.unit_cost%TYPE,
                      I_new_cost_2      IN     ordloc.unit_cost%TYPE)
RETURN BOOLEAN IS
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   L_program        VARCHAR2(60) := 'REC_COST_ADJ_SQL.UPDATE_DEALS';

   cursor C_LOCK_ORDLOC_INVC_COST_1 is
      select 'x'
        from ordloc_invc_cost
       where order_no  = I_order_no
         and item      = I_item
         and location  = I_location
         and unit_cost = I_old_cost_1
         for update nowait;

   cursor C_LOCK_ORDLOC_INVC_COST_2 is
      select 'x'
        from ordloc_invc_cost
       where order_no  = I_order_no
         and item      = I_item
         and location  = I_location
         and unit_cost = I_old_cost_2
         for update nowait;

BEGIN

   LP_table := 'ORDLOC_INVC_COST';
   ---
   open C_LOCK_ORDLOC_INVC_COST_1;
   close C_LOCK_ORDLOC_INVC_COST_1;
   ---
   update ordloc_invc_cost
      set unit_cost = I_new_cost_1
    where order_no  = I_order_no
      and item      = I_item
      and location  = I_location
      and unit_cost = I_old_cost_1;

   if I_old_cost_2 is not NULL then
      open C_LOCK_ORDLOC_INVC_COST_2;
      close C_LOCK_ORDLOC_INVC_COST_2;
      ---
      update ordloc_invc_cost
         set unit_cost = I_new_cost_2
       where order_no  = I_order_no
         and item      = I_item
         and location  = I_location
         and unit_cost = I_old_cost_2;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             LP_table,
                                             TO_CHAR(I_order_no),
                                             (I_item));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_DEALS;
-------------------------------------------------------------------------------
FUNCTION APPLY_TO_ALL_LOCS( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                            I_dept                 IN       DEPS.DEPT%TYPE,
                            I_class                IN       CLASS.CLASS%TYPE,
                            I_subclass             IN       SUBCLASS.SUBCLASS%TYPE,
                            I_chg_amt_cost         IN       ORDLOC.UNIT_COST%TYPE,
                            I_chg_amt_ord          IN       ORDLOC.UNIT_COST%TYPE,
                            I_sum_qty_received     IN       ORDLOC.QTY_RECEIVED%TYPE,
                            I_tran_date            IN       TRAN_DATA.TRAN_DATE%TYPE,
                            I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                            I_local_currency       IN       ORDHEAD.CURRENCY_CODE%TYPE,
                            I_primary_currency     IN       CURRENCIES.CURRENCY_CODE%TYPE,
                            I_supplier             IN       SUPS.SUPPLIER%TYPE,
                            I_elc_ind              IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                            I_import_ind           IN       SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                            I_alc_status           IN       ALC_HEAD.STATUS%TYPE,
                            I_tran_level           IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                            I_item_level           IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                            I_ord_currency         IN       ORDHEAD.CURRENCY_CODE%TYPE,
                            I_exchange_rate_ord    IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                            I_origin_country_id    IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                            I_import_country_id    IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                            I_pack_ind             IN       ITEM_MASTER.PACK_IND%TYPE,
                            I_import_order_ind     IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                            I_import_id            IN       ORDHEAD.IMPORT_ID%TYPE DEFAULT NULL,
                            I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'REC_COST_ADJ_SQL.APPLY_TO_ALL_LOCS';

   L_new_wac               ITEM_LOC_SOH.AV_COST%TYPE;
   L_adj_qty               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_neg_soh_wac_adj_amt   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_system_options_row    SYSTEM_OPTIONS%ROWTYPE;

   L_exp_currency          CURRENCIES.CURRENCY_CODE%TYPE;
   L_dty_currency          CURRENCIES.CURRENCY_CODE%TYPE;
   L_currency_code         CURRENCIES.CURRENCY_CODE%TYPE;

   L_min_unit_cost         ORDLOC_INVC_COST.UNIT_COST%TYPE;
   L_max_unit_cost         ORDLOC_INVC_COST.UNIT_COST%TYPE;

   L_exchange_rate_exp     CURRENCY_RATES.EXCHANGE_RATE%TYPE;

   L_total_elc             NUMBER := NULL;
   L_total_exp             NUMBER := NULL;
   L_total_dty             NUMBER := NULL;
   L_old_cost          NUMBER := NULL;
   L_new_land_cost         NUMBER := NULL;
   L_land_cost_diff        NUMBER := NULL;
   L_old_purchase_cost     NUMBER := NULL;
   L_new_purchase_cost     NUMBER := NULL;
   L_purchase_cost_diff    NUMBER := NULL;
   L_new_po_cost           NUMBER := NULL;
   L_tran_code             TRAN_DATA.TRAN_CODE%TYPE;
   L_tran_date             DATE := get_vdate;
   L_cost_diff_prim        ordloc.unit_cost%TYPE := 0;
   L_cost_diff_loc         ordloc.unit_cost%TYPE := 0;
   L_otb                   VARCHAR2(1);
   L_buy_get_ind           VARCHAR2( 1 );
   L_total_cost            TRAN_DATA.TOTAL_COST%TYPE;
   L_item_rec              ITEM_MASTER%ROWTYPE;
   L_tsf_no                TSFHEAD.TSF_NO%TYPE := NULL;
   L_pack_no                    PACKITEM.PACK_NO%TYPE;
   L_item                       ORDSKU.ITEM%TYPE;

   cursor C_GET_LOCS is
      select location,
             loc_type,
             unit_cost,
             qty_ordered,
             qty_received
        from ordloc
       where item     = I_item
         and order_no = I_order_no;

   cursor C_OTB is
       select otb_calc_type
         from deps
        where dept = I_dept;

   cursor C_GET_PACK_ITEMS is
      select item,
             qty
        from v_packsku_qty
       where pack_no = I_item;

   cursor C_TSFHEAD(I_to_loc tsfhead.to_loc%type) is
    select tsf_no
      from tsfhead
     where order_no = I_order_no
       and tsf_type = 'SG'
       and from_loc = I_import_id
       and to_loc = I_to_loc;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_rec,
                                      I_item) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_tran_level != I_item_level then
      return TRUE;
   end if;

   FOR C_rec in C_GET_LOCS LOOP

      if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                   C_rec.location,
                                   C_rec.loc_type,
                                   NULL,
                                   L_currency_code) = FALSE then
         return FALSE;
      end if;
      L_currency_code := NVL(I_local_currency, L_currency_code);

--- Old cost
      if I_pack_ind = 'N' then
         if ( I_import_ind = 'N' and I_elc_ind = 'Y' ) or
            ( I_import_ind = 'Y' and I_alc_status != 'PW' ) then

            if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                        L_total_elc,
                                        L_total_exp,
                                        L_exp_currency,
                                        L_exchange_rate_exp,
                                        L_total_dty,
                                        L_dty_currency,
                                        I_order_no,
                                        I_item,
                                        NULL,
                                        NULL,
                                        C_rec.location,
                                        I_supplier,
                                        I_origin_country_id,
                                        I_import_country_id,
                                        C_rec.unit_cost) = FALSE then
               raise PROGRAM_ERROR;
            end if;

            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_total_elc,
                                    I_primary_currency,
                                    L_currency_code,
                                    L_old_purchase_cost,
                                    'C',
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
               raise PROGRAM_ERROR;
            end if;
         else
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    C_rec.unit_cost,
                                    I_ord_currency,
                                    L_currency_code,
                                    L_old_purchase_cost,
                                    'C',
                                    NULL,
                                    NULL,
                                    I_exchange_rate_ord,
                                    NULL) = FALSE then
               raise PROGRAM_ERROR;
            end if;
         end if;
      else  -- pack ind = Y
         if L_item_rec.pack_type = 'B' then
         -- Populate comp item_elc_temp table with 'old landed cost' fpr
         -- each component itemin the buyer pack.
            FOR c_rec_pack IN c_get_pack_items LOOP
               if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                           L_total_elc,
                                           L_total_exp,
                                           L_exp_currency,
                                           L_exchange_rate_exp,
                                           L_total_dty,
                                           L_dty_currency,
                                           I_order_no,
                                           I_item,
                                           c_rec_pack.item,  -- component item in pack
                                           NULL,
                                           C_rec.location,
                                           I_supplier,
                                           I_origin_country_id,
                                           I_import_country_id,
                                           NULL) = FALSE then
                  raise PROGRAM_ERROR;
               end if;

               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_total_elc,
                                       I_primary_currency,
                                       L_currency_code,
                                       L_total_elc,
                                       'C',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL) = FALSE then
                  raise PROGRAM_ERROR;
               end if;

               insert into comp_item_elc_temp (order_no,
                                               pack_item,
                                               item,
                                               location,
                                               landed_cost)
                                       values (I_order_no,
                                               I_item,
                                               c_rec_pack.item,
                                               C_rec.location,
                                               L_total_elc);
            END LOOP;
         else
            -- Pack is a Vendor Pack
            -- Populate comp_item_elc_temp table with 'old landed cost' for Vendor Pack
            if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                        L_total_elc,
                                        L_total_exp,
                                        L_exp_currency,
                                        L_exchange_rate_exp,
                                        L_total_dty,
                                        L_dty_currency,
                                        I_order_no,
                                        I_item,
                                        NULL, -- Component item in Pack not needed in Vendor Packs
                                        NULL,
                                        C_rec.location,
                                        I_supplier,
                                        I_origin_country_id,
                                        I_import_country_id,
                                        NULL) = FALSE then
               raise PROGRAM_ERROR;
            end if;

            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_total_elc,
                                    I_primary_currency,
                                    L_currency_code,
                                    L_total_elc,
                                    'C',
                                    NULL,
                                    NULL) = FALSE then
               raise PROGRAM_ERROR;
            end if;

            insert into comp_item_elc_temp (order_no,
                                            pack_item,
                                            item,
                                            location,
                                            landed_cost)
                                    values (I_order_no,
                                            I_item,
                                            NULL,
                                            C_rec.location,
                                            L_total_elc);
         end if;
      end if; -- pack_ind = y
---
      if I_chg_amt_cost is null then
         L_new_po_cost := I_chg_amt_ord;
      else
        L_new_po_cost := I_chg_amt_cost;
      end if;

      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_new_po_cost,
                              I_ord_currency,
                              L_currency_code,
                              L_new_purchase_cost,
                              'C',
                               NULL,
                               NULL,
                               I_exchange_rate_ord,
                               NULL) = FALSE then
         return FALSE;
      end if;

      if L_new_purchase_cost is null then
         L_new_purchase_cost := L_new_po_cost;
      end if;

      if REC_COST_ADJ_SQL.UPDATE_ORDER_COST(O_error_message,
                                            L_new_purchase_cost,
                                            I_order_no,
                                            I_item,
                                            C_rec.location,
                                            C_rec.loc_type,
                                            I_adjust_matched_ind) = FALSE then
         return FALSE;
      end if;
---
      if ( I_import_ind = 'N' and I_elc_ind = 'Y' ) or
         ( I_import_ind = 'Y' and I_alc_status != 'PW' ) then

         if L_item_rec.pack_type = 'B' then
            L_item      := NULL;
            L_pack_no   := I_item;
         else
            L_item      := I_item;
            L_pack_no   := NULL;
         end if;

         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PE',
                                   L_item,
                                   I_supplier,
                                   NULL,
                                   NULL,
                                   I_order_no,
                                   NULL,
                                   L_pack_no,
                                   NULL,
                                   NULL,
                                   I_import_country_id,
                                   I_origin_country_id,
                                   NULL,
                                   NULL) = FALSE then
            return FALSE;
         end if;

         if I_import_order_ind = 'Y' then
            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PA',
                                      L_item,
                                      I_supplier,
                                      NULL,
                                      NULL,
                                      I_order_no,
                                      NULL,
                                      L_pack_no,
                                      NULL,
                                      NULL,
                                      I_import_country_id,
                                      I_origin_country_id,
                                      NULL,
                                      NULL) = FALSE then
               return FALSE;
            end if;

            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PE',
                                      L_item,
                                      I_supplier,
                                      NULL,
                                      NULL,
                                      I_order_no,
                                      NULL,
                                      L_pack_no,
                                      NULL,
                                      NULL,
                                      I_import_country_id,
                                      I_origin_country_id,
                                      NULL,
                                      NULL) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
---
      if C_rec.qty_received > 0 then

         if I_pack_ind = 'N' then

            if ( I_import_ind = 'N' and I_elc_ind = 'Y' ) or
               ( I_import_ind = 'Y' and I_alc_status != 'PW' ) then

               if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                           L_total_elc,
                                           L_total_exp,
                                           L_exp_currency,
                                           L_exchange_rate_exp,
                                           L_total_dty,
                                           L_dty_currency,
                                           I_order_no,
                                           I_item,
                                           NULL,
                                           NULL,
                                           C_rec.location,
                                           I_supplier,
                                           I_origin_country_id,
                                           I_import_country_id,
                                           L_new_purchase_cost) = FALSE then
                  raise PROGRAM_ERROR;
               end if;

               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_total_elc,
                                       I_primary_currency,
                                       L_currency_code,
                                       L_new_purchase_cost,
                                       'C',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL) = FALSE then
                  raise PROGRAM_ERROR;
               end if;
            else
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_new_po_cost,
                                       I_ord_currency,
                                       L_currency_code,
                                       L_new_purchase_cost,
                                       'C',
                                       NULL,
                                       NULL,
                                       I_exchange_rate_ord,
                                       NULL) = FALSE then
                  raise PROGRAM_ERROR;
               end if;
            end if;

            if ITEMLOC_UPDATE_SQL.UPD_AV_COST_CHANGE_COST(O_error_message,
                                                          I_item,
                                                          C_rec.location,
                                                          C_rec.loc_type,
                                                          L_new_purchase_cost,
                                                          L_old_purchase_cost,
                                                          C_rec.qty_received,
                                                          L_new_wac,
                                                          L_neg_soh_wac_adj_amt,
                                                          'Y',
                                                          I_order_no,
                                                          NULL,
                                                          NULL,
                                                          L_program,
                                                          I_adjust_matched_ind) = FALSE then
               return FALSE;
            end if;

            SQL_LIB.SET_MARK('OPEN','C_OTB','DEPS','DEPT: '|| TO_CHAR(I_dept));
            open C_OTB;
            SQL_LIB.SET_MARK('FETCH','C_OTB','DEPS','DEPT: '|| TO_CHAR(I_dept));
            fetch C_OTB into L_otb;
            SQL_LIB.SET_MARK('CLOSE','C_OTB','DEPS','DEPT: '|| TO_CHAR(I_dept));
            close C_OTB;
--- Update OTB
            if L_otb = 'C' then
            --Converts the cost difference from local currency to primary currency
               L_cost_diff_loc := L_new_purchase_cost - L_old_purchase_cost;
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_cost_diff_loc,
                                       I_local_currency,
                                       I_primary_currency,
                                       L_cost_diff_prim,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
                  RETURN FALSE;
               end if;
               if OTB_SQL.ORD_RECEIVE(O_error_message,
                                      0,
                                      L_cost_diff_prim,
                                      I_order_no,
                                      I_dept,
                                      I_class,
                                      I_subclass,
                                      C_rec.qty_received,
                                      C_rec.qty_ordered) = FALSE then
                  RETURN FALSE;
               end if;
            end if; --- L_otb = 'C'
         else -- if pack_ind ='Y'
            if REC_COST_ADJ_SQL.UPD_PACK_ITEM(O_error_message,
                                              I_item,
                                              I_tran_date,
                                              I_order_no,
                                              C_rec.unit_cost,
                                              L_new_purchase_cost,
                                              I_supplier,
                                              I_origin_country_id,
                                              I_import_country_id,
                                              I_exchange_rate_ord,
                                              I_primary_currency,
                                              I_ord_currency,
                                              I_alc_status,
                                              I_import_order_ind,
                                              I_import_ind,
                                              I_elc_ind,
                                              C_rec.location,
                                              C_rec.loc_type,
                                              I_adjust_matched_ind) = FALSE then
               return FALSE;
            end if;

            if CURRENCY_SQL.CONVERT(O_error_message,
                                    NULL,
                                    I_ord_currency,
                                    I_primary_currency,
                                    L_old_cost,
                                    'C',
                                    NULL,
                                    NULL,
                                    I_exchange_rate_ord,
                                    NULL) = FALSE then
               return FALSE;
            end if;

            L_old_cost := L_old_cost * NVL(I_sum_qty_received, C_rec.qty_received);

            if REC_COST_ADJ_SQL.UPDATE_SUP_DATA(O_error_message,
                                                I_dept,
                                                I_supplier,
                                                I_tran_date,
                                                1,
                                                L_old_cost) = FALSE then
               return FALSE;
            end if;
         end if; -- if I_pack_ind = 'N' then
--- Update deals
         if REC_COST_ADJ_SQL.CHECK_DEALS( O_error_message,
                                         L_min_unit_cost,
                                         L_max_unit_cost,
                                         I_order_no,
                                         I_item,
                                         C_rec.location) = FALSE then
            return FALSE;
         end if;

         if ( L_min_unit_cost is NULL ) or
            ( L_min_unit_cost = L_max_unit_cost ) then
            L_buy_get_ind := 'N';
         else
            L_buy_get_ind := 'Y';
         end if;

         if L_buy_get_ind = 'Y' then
            if REC_COST_ADJ_SQL.UPDATE_DEALS(O_error_message,
                                             I_order_no,
                                             I_item,
                                             C_rec.location,
                                             L_max_unit_cost,
                                             I_chg_amt_ord,
                                             L_min_unit_cost,
                                             I_chg_amt_cost) = FALSE then
               return FALSE;
            end if;
         else
            if REC_COST_ADJ_SQL.UPDATE_DEALS( O_error_message,
                                             I_order_no,
                                             I_item,
                                             C_rec.location,
                                             C_rec.unit_cost,
                                             I_chg_amt_cost,
                                             NULL,
                                             NULL) = FALSE then
               return FALSE;
            end if;
         end if;
---- Adjust SG transfer cost and stock ledger
         if I_import_id is NOT NULL then
            SQL_LIB.SET_MARK('OPEN','C_TSFHEAD','ORDER_NO','ORDER_NO: '|| TO_CHAR(I_order_no));
            open C_TSFHEAD(C_rec.location);
            SQL_LIB.SET_MARK('FETCH','C_TSFHEAD','ORDER_NO','ORDER_NO: '|| TO_CHAR(I_order_no));
            fetch C_TSFHEAD into L_tsf_no;
            SQL_LIB.SET_MARK('CLOSE','C_TSFHEAD','ORDER_NO','ORDER_NO: '|| TO_CHAR(I_order_no));
            close C_TSFHEAD;

            if L_tsf_no is NOT NULL then
               if TRANSFER_SQL.UPDATE_TSF_COST(O_error_message,
                                               L_tsf_no,
                                               I_item,
                                               L_new_purchase_cost) = FALSE then
                  return FALSE;
               end if;

               if STKLEDGR_SQL.INIT_TRAN_DATA_INSERT(O_error_message) = FALSE then
                  return FALSE;
               end if;
               if CREATE_ORD_TSF_SQL.BUILD_ADJ_TRAN_DATA_SG(O_error_message,
                                                             I_order_no,
                                                             I_item,
                                                             L_new_purchase_cost - L_old_purchase_cost,
                                                             L_new_purchase_cost - L_old_purchase_cost,
                                                             C_rec.qty_received,
                                                             NULL) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
---
      end if; -- if C_rec.qty_received > 0
---
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR( SQLCODE ));
      return FALSE;

END APPLY_TO_ALL_LOCS;
----------------------------------------------------------------------
FUNCTION ADJUST_LINEITEM_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item                 IN       ORDSKU.ITEM%TYPE,
                              I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                              I_adj_unit_cost        IN       ORDLOC.UNIT_COST%TYPE ,
                              I_adj_curr_code        IN       ORDHEAD.CURRENCY_CODE%TYPE,
                              I_location             IN       INV_STATUS_QTY.LOCATION%TYPE,
                              I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_otb              VARCHAR2(1)                          := NULL;
   L_cost_diff_loc    ORDLOC.UNIT_COST%TYPE                := 0;
   L_cost_diff_prim   ORDLOC.UNIT_COST%TYPE                := 0;
   L_program          VARCHAR2(60) := 'REC_COST_ADJ_SQL.ADJUST_LINEITEM_COST';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   L_sysopt_rec                 SYSTEM_OPTIONS%ROWTYPE;
   L_item_rec                   ITEM_MASTER%ROWTYPE;
   L_itemloc_rec                ITEM_LOC%ROWTYPE;
   L_ordhead_rec                ORDHEAD%ROWTYPE;
   L_ordsku_rec                 ORDSKU%ROWTYPE;
   L_ordloc_rec                 ORDLOC%ROWTYPE;
   L_error_message              RTK_ERRORS.RTK_TEXT%TYPE :=   NULL;
   L_status                     ALC_HEAD.STATUS%TYPE := 'E';
   L_cost_new_loc               NUMBER;
   L_cost_old_loc               NUMBER;
   L_total_elc                  NUMBER;
   L_exists                     BOOLEAN;
   L_total_exp                  NUMBER;
   L_exp_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate_exp          CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_total_dty                  NUMBER;
   L_dty_currency               CURRENCIES.CURRENCY_CODE%TYPE;

   L_order_no                   ORDHEAD.ORDER_NO%TYPE;
   L_pack_no                    PACKITEM.PACK_NO%TYPE;
   L_item                       ORDSKU.ITEM%TYPE;
   L_location                   ITEM_LOC.LOC%TYPE;
   L_adjusted_unit_cost         ORDLOC.UNIT_COST%TYPE;
   L_location_curr_code         CURRENCIES.CURRENCY_CODE%TYPE;
   L_adj_curr_code              ORDHEAD.CURRENCY_CODE%TYPE;
   L_adj_exchange_rate          ORDHEAD.EXCHANGE_RATE%TYPE := NULL;
   L_new_ord_cost               ORDLOC.UNIT_COST%TYPE;
   L_tran_date                  DATE := get_vdate;
   L_import_id                  ORDHEAD.IMPORT_ID%TYPE;
   L_import_type                ORDHEAD.IMPORT_TYPE%TYPE;

   L_new_cost_excl_elc          ORDLOC.UNIT_COST%TYPE;
   L_old_cost_excl_elc          ORDLOC.UNIT_COST%TYPE;


   cursor C_OTB is
     select otb_calc_type
       from deps
      where dept = L_item_rec.dept;

   cursor C_GET_PACK_ITEMS is
      select item,
             qty
        from v_packsku_qty
       where pack_no = L_item;

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;
   if I_order_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_order_no',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;
   if I_location is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_location',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;
   if I_adj_unit_cost is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_adj_unit_cost',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;
   L_adjusted_unit_cost  := I_adj_unit_cost;
   L_order_no            := I_order_no;
   L_item                := I_item;
   L_location            := I_location;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_sysopt_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if ALC_SQL.GET_STATUS(L_error_message,
                         L_status,
                         L_order_no,
                         L_item) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if L_status is null then 
      L_status :='P';
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_error_message,
                                      L_item_rec,
                                      L_item) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if L_item_rec.item is null then
      O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                             L_program,
                                             'ITEM_MASTER',
                                             'ITEM:'||L_item);
      return FALSE;
   end if;
   ---
   if L_item_rec.item_level != L_item_rec.tran_level then
      return TRUE;
   end if;
   ---
   if ORDER_ATTRIB_SQL.GET_ORDHEAD_ROW(L_error_message,
                                       L_ordhead_rec,
                                       L_order_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if L_ordhead_rec.order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                             L_program,
                                             'ORDHEAD',
                                             'ORDER_NO:'||L_order_no);
      return FALSE;
   end if;
   ---
   L_adj_curr_code := NVL(I_adj_curr_code, L_ordhead_rec.currency_code);
   if L_adj_curr_code = L_ordhead_rec.currency_code then
      L_adj_exchange_rate := L_ordhead_rec.exchange_rate;
   end if;
   ---
   if ORDER_ATTRIB_SQL.GET_ORDSKU_ROW(L_error_message,
                                      L_exists,
                                      L_ordsku_rec,
                                      L_order_no,
                                      L_item) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if L_ordsku_rec.order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                             L_program,
                                             'ORDSKU',
                                             'ORDER_NO:'||L_order_no||', ITEM:'||L_item);
      return FALSE;
   end if;
   ---
   --- For orders which have an importer attached, REIM will do cost adjustment at importer (bill_to_loc).
   --- In this case a single cost assumed at all locations for an item. so call APPLY_TO_ALL_LOCS and exit.
   if ORDER_SQL.GET_DEFAULT_IMP_EXP(O_error_message,
                                    L_import_id,
                                    L_import_type,
                                    L_order_no) = FALSE then
      return FALSE;
   end if;

   if L_import_id is NOT NULL and L_import_type <> 'F' then
      if REC_COST_ADJ_SQL.APPLY_TO_ALL_LOCS(O_error_message,
                                            L_item,
                                            L_item_rec.dept,
                                            L_item_rec.class,
                                            L_item_rec.subclass,
                                            I_adj_unit_cost,
                                            I_adj_unit_cost,
                                            NULL,
                                            L_tran_date,
                                            L_order_no,
                                            NULL,
                                            L_sysopt_rec.currency_code,
                                            L_ordhead_rec.supplier,
                                            L_sysopt_rec.elc_ind,
                                            L_sysopt_rec.import_ind,
                                            L_status,
                                            L_item_rec.tran_level,
                                            L_item_rec.item_level,
                                            L_adj_curr_code,
                                            L_ordhead_rec.exchange_rate,
                                            L_ordsku_rec.origin_country_id,
                                            L_ordhead_rec.import_country_id,
                                            L_item_rec.pack_ind,
                                            L_ordhead_rec.import_order_ind,
                                            L_import_id,
                                            I_adjust_matched_ind) = FALSE then
         return FALSE;
      end if;

      return TRUE;
   end if;
   ---
   if ORDER_ATTRIB_SQL.GET_ORDLOC_ROW(L_error_message,
                                      L_exists,
                                      L_ordloc_rec,
                                      L_order_no,
                                      L_item,
                                      L_location) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if L_ordloc_rec.order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                             L_program,
                                             'ORDLOC',
                                             'ORDER_NO:'||L_order_no||', ITEM:'||L_item
                                             ||', LOCATION:'||L_location);
      return FALSE;
   end if;
   if CURRENCY_SQL.GET_CURR_LOC (L_error_message,
                                 L_location,
                                 L_ordloc_rec.loc_type,
                                 NULL,
                                 L_location_curr_code) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   L_ordloc_rec.qty_received := NVL(L_ordloc_rec.qty_received,0);

   ---
   if L_item_rec.pack_ind = 'N' then

      if (L_sysopt_rec.import_ind = 'N' and L_sysopt_rec.elc_ind = 'Y') or
         (L_sysopt_rec.import_ind = 'Y' and L_status != 'PW') then
         ---
         if ELC_CALC_SQL.CALC_TOTALS(L_error_message,
                                     L_total_elc,
                                     L_total_exp,
                                     L_exp_currency,
                                     L_exchange_rate_exp,
                                     L_total_dty,
                                     L_dty_currency,
                                     L_order_no,
                                     L_item,
                                     NULL,
                                     NULL,
                                     L_location,
                                     L_ordhead_rec.supplier ,
                                     L_ordsku_rec.origin_country_id,
                                     L_ordhead_rec.import_country_id,
                                     L_ordloc_rec.unit_cost) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
         if CURRENCY_SQL.CONVERT (L_error_message,
                                  L_total_elc,
                                  L_sysopt_rec.currency_code,
                                  L_location_curr_code,
                                  L_cost_old_loc,
                                  'C',
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  'N') = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
      elsif (L_sysopt_rec.import_ind = 'Y' and L_status = 'PW') then
         ---
         if REC_COST_ADJ_SQL.CALC_ELC(L_error_message,
                                      L_total_elc,
                                      L_order_no,
                                      L_item,
                                      NULL,
                                      L_location,
                                      L_ordhead_rec.supplier,
                                      L_ordsku_rec.origin_country_id,
                                      L_ordhead_rec.import_country_id,
                                      L_ordloc_rec.unit_cost) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
         if CURRENCY_SQL.CONVERT (L_error_message,
                                  L_total_elc,
                                  L_sysopt_rec.currency_code,
                                  L_location_curr_code,
                                  L_cost_old_loc,
                                  'C',
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
      else
         ---
         if CURRENCY_SQL.CONVERT (L_error_message,
                                  L_ordloc_rec.unit_cost,
                                  L_ordhead_rec.currency_code,
                                  L_location_curr_code,
                                  L_cost_old_loc,
                                  'C',
                                  NULL,
                                  NULL,
                                  L_ordhead_rec.exchange_rate,
                                  NULL,
                                  'N') = FALSE then
            raise PROGRAM_ERROR;
         end if;
      ---
      end if;
      ---
   else  -- pack ind = Y

          if L_item_rec.pack_type = 'B' then
               -- Populate comp item_elc_temp table with 'old landed cost' fpr
               -- each component itemin the buyer pack.
               FOR c_rec IN c_get_pack_items LOOP
                  if L_status = 'PW' then
                     ---
                     if REC_COST_ADJ_SQL.CALC_ELC(L_error_message,
                                                  L_total_elc,
                                                  L_order_no,
                                                  L_item,
                                                  c_rec.item,  -- component item in pack
                                                  L_location,
                                                  L_ordhead_rec.supplier,
                                                  L_ordsku_rec.origin_country_id,
                                                  L_ordhead_rec.import_country_id,
                                                  NULL) = FALSE then
                        raise PROGRAM_ERROR;
                     end if;
                     ---
                  else
                     if ELC_CALC_SQL.CALC_TOTALS(L_error_message,
                                                 L_total_elc,
                                                 L_total_exp,
                                                 L_exp_currency,
                                                 L_exchange_rate_exp,
                                                 L_total_dty,
                                                 L_dty_currency,
                                                 L_order_no,
                                                 L_item,
                                                 c_rec.item,  -- component item in pack
                                                 NULL,
                                                 L_location,
                                                 L_ordhead_rec.supplier ,
                                                 L_ordsku_rec.origin_country_id,
                                                 L_ordhead_rec.import_country_id,
                                                 NULL) = FALSE then
                          raise PROGRAM_ERROR;
                     end if;
                  ---
                  end if;
                  if CURRENCY_SQL.CONVERT (L_error_message,
                                           L_total_elc,
                                           L_sysopt_rec.currency_code,
                                           L_location_curr_code,
                                           L_cost_old_loc,
                                           'C',
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           'N') = FALSE then
                      raise PROGRAM_ERROR;
                  end if;
                  ---
                  insert into comp_item_elc_temp (order_no,
                                                  pack_item,
                                                  item,
                                                  location,
                                                  landed_cost)
                                          values (L_order_no,
                                                  L_item,
                                                  c_rec.item,
                                                  L_location,
                                                  L_total_elc);
               END LOOP;
          else  -- Pack is a Vendor Pack
               if L_status = 'PW' then
                  ---
                  if REC_COST_ADJ_SQL.CALC_ELC(L_error_message,
                                               L_total_elc,
                                               L_order_no,
                                               L_item,
                                               NULL,-- Component item in Pack not needed in Vendor Packs
                                               L_location,
                                               L_ordhead_rec.supplier,
                                               L_ordsku_rec.origin_country_id,
                                               L_ordhead_rec.import_country_id,
                                               NULL) = FALSE then
                     raise PROGRAM_ERROR;
                  end if;
                  ---
               else
                  -- Populate comp_item_elc_temp table with 'old landed cost' for Vendor Pack
                  if ELC_CALC_SQL.CALC_TOTALS(L_error_message,
                                            L_total_elc,
                                            L_total_exp,
                                            L_exp_currency,
                                            L_exchange_rate_exp,
                                            L_total_dty,
                                            L_dty_currency,
                                            L_order_no,
                                            L_item,
                                            NULL, -- Component item in Pack not needed in Vendor Packs
                                              NULL,
                                              L_location,
                                              L_ordhead_rec.supplier ,
                                              L_ordsku_rec.origin_country_id,
                                              L_ordhead_rec.import_country_id,
                                              NULL) = FALSE then
                     raise PROGRAM_ERROR;
                  end if;
               ---
               end if;
               if CURRENCY_SQL.CONVERT(L_error_message,
                                       L_total_elc,
                                       L_sysopt_rec.currency_code,
                                       L_location_curr_code,
                                       L_total_elc,
                                       'C',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       'N') = FALSE then
                  raise PROGRAM_ERROR;
               end if;
               ---
               insert into comp_item_elc_temp (order_no,
                                               pack_item,
                                               item,
                                               location,
                                               landed_cost)
                                       values (L_order_no,
                                               L_item,
                                               NULL,
                                               L_location,
                                               L_total_elc);

         end if;

         ---
   end if; -- pack_ind = y
   ---

   if CURRENCY_SQL.CONVERT (L_error_message,
                            L_adjusted_unit_cost,
                            L_adj_curr_code,
                            L_ordhead_rec.currency_code,
                            L_new_ord_cost,
                            'C',
                            NULL,
                            NULL,
                            L_adj_exchange_rate,
                            L_ordhead_rec.exchange_rate,
                            'N') = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if REC_COST_ADJ_SQL.UPDATE_ORDER_COST(L_error_message,
                                         L_new_ord_cost,
                                         L_order_no,
                                         L_item,
                                         L_location,
                                         L_ordloc_rec.loc_type,
                                         I_adjust_matched_ind) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if ( L_sysopt_rec.import_ind = 'N' and L_sysopt_rec.elc_ind = 'Y' ) or
      ( L_sysopt_rec.import_ind = 'Y' and L_status != 'PW' ) then

      if L_item_rec.pack_type = 'B' then
         L_item      := NULL;
         L_pack_no   := I_item;
      else
         L_item      := I_item;
         L_pack_no   := NULL;
      end if;

      if ELC_CALC_SQL.CALC_COMP( O_error_message     ,
                                 'PE'                ,
                                 L_item              ,
                                 L_ordhead_rec.supplier,
                                 NULL                ,
                                 NULL                ,
                                 I_order_no          ,
                                 NULL                ,
                                 L_pack_no           ,
                                 NULL                ,
                                 NULL                ,
                                 L_ordhead_rec.import_country_id,
                                 L_ordsku_rec.origin_country_id,
                                 NULL                ,
                                 NULL ) = FALSE then
         return FALSE;
      end if;

      if L_ordhead_rec.import_order_ind = 'Y' then

         if ELC_CALC_SQL.CALC_COMP( O_error_message     ,
                                    'PA'                ,
                                    L_item              ,
                                    L_ordhead_rec.supplier,
                                    NULL                ,
                                    NULL                ,
                                    I_order_no          ,
                                    NULL                ,
                                    L_pack_no           ,
                                    NULL                ,
                                    NULL                ,
                                    L_ordhead_rec.import_country_id,
                                    L_ordsku_rec.origin_country_id,
                                    NULL                ,
                                    NULL ) = FALSE then
            return FALSE;
         end if;

         if ELC_CALC_SQL.CALC_COMP( O_error_message     ,
                                    'PE'                ,
                                    L_item              ,
                                    L_ordhead_rec.supplier,
                                    NULL                ,
                                    NULL                ,
                                    I_order_no          ,
                                    NULL                ,
                                    L_pack_no           ,
                                    NULL                ,
                                    NULL                ,
                                    L_ordhead_rec.import_country_id,
                                    L_ordsku_rec.origin_country_id,
                                    NULL                ,
                                    NULL ) = FALSE then
            return FALSE;
         end if;

      end if;
   end if;


   if L_item_rec.pack_ind = 'N' then
      if (L_sysopt_rec.import_ind = 'N' and L_sysopt_rec.elc_ind = 'Y') or
         (L_sysopt_rec.import_ind = 'Y' and L_status != 'PW') then
         ---
         if ELC_CALC_SQL.CALC_TOTALS(L_error_message,
                                     L_total_elc,
                                     L_total_exp,
                                     L_exp_currency,
                                     L_exchange_rate_exp,
                                     L_total_dty,
                                     L_dty_currency,
                                     L_order_no,
                                     L_item,
                                     NULL,
                                     NULL,
                                     L_location,
                                     L_ordhead_rec.supplier ,
                                     L_ordsku_rec.origin_country_id,
                                     L_ordhead_rec.import_country_id,
                                     L_new_ord_cost) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
         if CURRENCY_SQL.CONVERT(L_error_message,
                                 L_total_elc,
                                 L_sysopt_rec.currency_code,
                                 L_location_curr_code,
                                 L_cost_new_loc ,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 'N') = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
      elsif (L_sysopt_rec.import_ind = 'Y' and L_status = 'PW') then
         ---
         if REC_COST_ADJ_SQL.CALC_ELC(L_error_message,
                                      L_total_elc,
                                      L_order_no,
                                      L_item,
                                      NULL,
                                      L_location,
                                      L_ordhead_rec.supplier,
                                      L_ordsku_rec.origin_country_id,
                                      L_ordhead_rec.import_country_id,
                                      L_new_ord_cost) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
         ---
         if CURRENCY_SQL.CONVERT(L_error_message,
                                 L_total_elc,
                                 L_sysopt_rec.currency_code,
                                 L_location_curr_code,
                                 L_cost_new_loc ,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
      else
         if CURRENCY_SQL.CONVERT(L_error_message,
                           L_adjusted_unit_cost,
                           L_adj_curr_code,
                           L_location_curr_code,
                           L_cost_new_loc ,
                           'C',
                           NULL,
                           NULL,
                           L_adj_exchange_rate,
                           NULL,
                                 'N') = FALSE then
            raise PROGRAM_ERROR;
          end if;
      end if;
      ---
      --The new order cost (exclusive of elc) has to go through to the respective currency conversion
      if CURRENCY_SQL.CONVERT(L_error_message,
                              L_adjusted_unit_cost,
                              L_adj_curr_code,
                              L_location_curr_code,
                              L_new_cost_excl_elc ,
                              'C',
                              NULL,
                              NULL,
                              L_adj_exchange_rate,
                              NULL,
                              'N') = FALSE then
         raise PROGRAM_ERROR;
      end if;
      ---
      --The old order cost (exclusive of elc) has to go through to the respective currency conversion
      if CURRENCY_SQL.CONVERT(L_error_message,
                              L_ordloc_rec.unit_cost,
                              L_ordhead_rec.currency_code,
                              L_location_curr_code,
                              L_old_cost_excl_elc ,
                              'C',
                              NULL,
                              NULL,
                              L_adj_exchange_rate,
                              NULL,
                              'N') = FALSE then
         raise PROGRAM_ERROR;
      end if;
      ---
      if ITEMLOC_UPDATE_SQL.UPD_AV_COST_CHANGE_COST(   O_error_message,
                                                       L_item,
                                                       L_location,
                                                       L_ordloc_rec.loc_type,
                                                       L_cost_new_loc,
                                                       L_cost_old_loc,
                                                       L_ordloc_rec.qty_received,
                                                       NULL,     -- new wac
                                                       NULL,     -- neg_soh_wac_adj_amt
                                                       'Y',
                                                       L_order_no,
                                                       NULL,
                                                       NULL,
                                                       L_program,
                                                       'C',
                                                       NULL,
                                                       L_new_cost_excl_elc,
                                                       L_old_cost_excl_elc,
                                                       I_adjust_matched_ind) = FALSE then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN','C_OTB','DEPS','DEPT: '|| TO_CHAR(L_item_rec.dept));
      open C_OTB;
      SQL_LIB.SET_MARK('FETCH','C_OTB','DEPS','DEPT: '|| TO_CHAR(L_item_rec.dept));
      fetch C_OTB into L_otb;
      SQL_LIB.SET_MARK('CLOSE','C_OTB','DEPS','DEPT: '|| TO_CHAR(L_item_rec.dept));
      close C_OTB;

      if L_otb = 'C' then
         L_cost_diff_loc := L_cost_new_loc - L_cost_old_loc;
         --Converts the cost difference from local currency to primary currency
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_cost_diff_loc,
                                 L_location_curr_code,
                                 NULL,
                                 L_cost_diff_prim,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 'N') = FALSE then
            RETURN FALSE;
         end if;

         if OTB_SQL.ORD_RECEIVE(O_error_message,
                                0,
                                L_cost_diff_prim,
                                L_order_no,
                                L_item_rec.dept,
                                L_item_rec.class,
                                L_item_rec.subclass,
                                L_ordloc_rec.qty_received,
                                L_ordloc_rec.qty_ordered) = FALSE then
            RETURN FALSE;
         end if;
      end if; --- L_otb = 'C'

   else  -- pack ind = n
      if REC_COST_ADJ_SQL.UPD_PACK_ITEM( O_error_message     ,
                                         I_item              ,
                                         L_tran_date         ,
                                         L_order_no          ,
                                         L_ordloc_rec.unit_cost,
                                         L_new_ord_cost     ,
                                         L_ordhead_rec.supplier  ,
                                         L_ordsku_rec.origin_country_id ,
                                         L_ordhead_rec.import_country_id,
                                         L_ordhead_rec.exchange_rate ,
                                         L_sysopt_rec.currency_code ,
                                         L_ordhead_rec.currency_code,
                                         L_status            ,
                                         L_ordhead_rec.import_order_ind  ,
                                         L_sysopt_rec.import_ind,
                                         L_sysopt_rec.elc_ind,
                                         L_location      ,
                                         L_ordloc_rec.loc_type,
                                         I_adjust_matched_ind) = FALSE then

         return FALSE;
      end if;
   end if; -- pack ind = y

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',LP_table,
                                           (I_item),TO_CHAR(I_location));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;

END ADJUST_LINEITEM_COST;
----------------------------------------------------------------------
FUNCTION CALC_ELC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_total_elc           IN OUT   NUMBER,
                  I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                  I_item                IN       ITEM_MASTER.ITEM%TYPE,
                  I_comp_item           IN       ITEM_MASTER.ITEM%TYPE,
                  I_location            IN       ORDLOC.LOCATION%TYPE,
                  I_supplier            IN       SUPS.SUPPLIER%TYPE,
                  I_origin_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                  I_import_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                  I_cost                IN       ORDLOC.UNIT_COST%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)                  := 'REC_COST_ADJ_SQL.CALC_ELC';
   L_qty_received         ORDLOC.QTY_RECEIVED%TYPE      := 0;
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_pack_no              ITEM_MASTER.ITEM%TYPE;
   L_packsku_qty          ORDLOC.QTY_RECEIVED%TYPE      := 0;
   L_unit_elc_prim        ORDLOC_EXP.EST_EXP_VALUE%TYPE := 0;
   L_elc_estimated        ORDLOC_EXP.EST_EXP_VALUE%TYPE := 0;
   L_elc_finalized        ORDLOC_EXP.EST_EXP_VALUE%TYPE := 0;
   L_tot_elc_loc          ORDLOC_EXP.EST_EXP_VALUE%TYPE := 0;
   L_tot_qty_received     shipsku.qty_received%TYPE     := 0;
   L_avg_unit_elc         ORDLOC_EXP.EST_EXP_VALUE%TYPE := 0;
   L_cost                 NUMBER                        := 0;
   L_comp_item_cost       NUMBER                        := 0;
   L_tot_comp_items_cost  NUMBER                        := 0;
   L_supplier             SUPS.SUPPLIER%TYPE            := I_supplier;
   L_origin_country_id    COUNTRY.COUNTRY_ID%TYPE       := I_origin_country_id;
   L_import_country_id    COUNTRY.COUNTRY_ID%TYPE       := I_import_country_id;
   L_buyer_pack           VARCHAR2(1)                   := 'N';
   L_pack_type            ITEM_MASTER.PACK_TYPE%TYPE    := 'N';
   L_pack_ind             ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind         ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind        ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_currency_prim        CURRENCIES.CURRENCY_CODE%TYPE;
   L_currency_sup         CURRENCIES.CURRENCY_CODE%TYPE;
   L_currency_ord         CURRENCIES.CURRENCY_CODE%TYPE;
   L_ord_exchange_rate    CURRENCY_RATES.EXCHANGE_RATE%TYPE;

   cursor C_GET_LOCS is
      select shp.shipment shipment,
             shp.to_loc loc,
             shp.to_loc_type loc_type,
             SUM(shs.qty_received) qty_received
      from shipment shp,
           shipsku shs
     where shp.order_no = I_order_no
       and shp.shipment = shs.shipment
       and shp.to_loc   = I_location
       and shs.qty_received is not NULL
       and shp.to_loc_type <> 'W'
       and shs.item     = I_item
      group by shp.shipment ,
               shp.to_loc ,
               shp.to_loc_type
     union all
      select shp.shipment shipment,
             shl.to_loc loc,
             shp.to_loc_type loc_type,
             SUM(shl.qty_received) qty_received
      from shipment shp,
           shipsku shs,
           shipsku_loc shl
     where shp.order_no = I_order_no
       and shp.shipment = shs.shipment
       and shl.to_loc   = I_location
       and shs.shipment = shl.shipment
       and shs.seq_no   = shl.seq_no
       and shs.qty_received is not NULL
       and shp.to_loc_type = 'W'
       and shs.item     = I_item
       and shs.item     = shl.item
       group by shp.shipment ,
                shl.to_loc ,
                shp.to_loc_type
       order by 1,2;

   cursor C_PACKSKU_QTY is
      select qty
        from v_packsku_qty
       where pack_no           = I_item
         and item              = I_comp_item;

   cursor C_GET_ORD_SUPP_ORIG is
      select oh.supplier,
             os.origin_country_id,
             oh.import_country_id
        from ordhead oh,
             ordsku os
       where oh.order_no = I_order_no
         and oh.order_no = os.order_no
         and os.item     = I_comp_item;

   cursor C_GET_ORDER_COST is
      select ol.unit_cost,
             oh.currency_code,
             oh.exchange_rate
        from ordloc ol,
             ordhead oh
       where oh.order_no = I_order_no
         and oh.order_no = ol.order_no
         and ol.item     = I_item
         and ol.location = I_location;

   cursor C_GET_UNIT_COST(L_item ITEM_MASTER.ITEM%TYPE) is
      select i.unit_cost,
             s.currency_code
        from item_supp_country i,
             sups s
       where i.item              = L_item
         and i.supplier          = s.supplier
         and s.supplier          = L_supplier
         and i.origin_country_id = L_origin_country_id;

   cursor C_GET_SUM_COMP_COST is
      select NVL(SUM(i.unit_cost * v.qty), 0)
        from item_supp_country i,
             v_packsku_qty v
       where i.supplier          = L_supplier
         and i.origin_country_id = L_origin_country_id
         and i.item              = v.item
         and v.pack_no           = I_item;

BEGIN

   FOR rec_get_locs in C_GET_LOCS LOOP
      ---
      if I_comp_item is not NULL then
         L_item      := I_comp_item;
         L_pack_no   := I_item;
         ---
         open C_PACKSKU_QTY;
         fetch C_PACKSKU_QTY into L_packsku_qty;
         close C_PACKSKU_QTY;
      else
         L_item := I_item;
      end if;
      ---
      if I_comp_item is not NULL then
         L_qty_received := rec_get_locs.qty_received * L_packsku_qty;
      else
         L_qty_received := rec_get_locs.qty_received;
      end if;
      ---
      if L_qty_received > 0 then
         ---
         L_tot_qty_received := NVL(L_tot_qty_received,0) + L_qty_received ;
         ---
         if ALC_SQL.UNIT_ELC_LOC_TOTALS(O_error_message,
                                        L_elc_estimated,
                                        L_unit_elc_prim, -- pending ELC
                                        L_elc_finalized,
                                        I_order_no,
                                        L_item,
                                        L_pack_no,
                                        rec_get_locs.loc,
                                        I_import_country_id,
                                        rec_get_locs.shipment,
                                        NULL) = FALSE then
            return FALSE;
         end if;
         ---
         L_tot_elc_loc := NVL(L_tot_elc_loc,0)
                               + ( L_qty_received
                                    * ( NVL(L_elc_estimated,0) + NVL(L_unit_elc_prim,0)));
         ---
      end if;

   END LOOP;--or rec_get_locs in C_GET_LOCS
   ---
   --- Calculate Average ELC Loc
   if NVL(L_tot_qty_received,0) > 0 then
      L_avg_unit_elc := L_tot_elc_loc / L_tot_qty_received ;
   end if;
   ---
   --- Calculate order cost
   -- For pack item get the pack details
   if L_pack_no is not NULL then
      if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                       L_pack_ind,
                                       L_sellable_ind,
                                       L_orderable_ind,
                                       L_pack_type,
                                       L_pack_no) = FALSE then
         return FALSE;
      end if;
   end if;

   if(L_orderable_ind = 'N') then
       return TRUE ;
   end if;

   ---
   if L_pack_type = 'B' then
      L_buyer_pack := 'Y';
   end if;
   ---
   if (L_orderable_ind = 'Y' or L_pack_ind = 'N') then
      ---
      if (I_supplier is NULL or I_origin_country_id is NULL) then
         open C_GET_ORD_SUPP_ORIG;
         fetch C_GET_ORD_SUPP_ORIG into L_supplier,
                                        L_origin_country_id,
                                        L_import_country_id;
         close C_GET_ORD_SUPP_ORIG;
       end if;
   end if;
   ---
   if I_import_country_id is NULL then
      if SYSTEM_OPTIONS_SQL.GET_BASE_COUNTRY(O_error_message,
                                             L_import_country_id) = FALSE then
         return FALSE;
      end if;
   else
      L_import_country_id := I_import_country_id;
   end if;
   ---
   ---
   -- if buyer pack then need to get location for each component item
   -- loop through the comps of the pack get the texp and tdty
   -- sum results from fetch in loop.
   ---
   if L_buyer_pack = 'Y' then
      if I_cost is NULL then
         ---
         open C_GET_ORDER_COST;
         fetch C_GET_ORDER_COST into L_cost,
                                     L_currency_ord,
                                     L_ord_exchange_rate;
         if C_GET_ORDER_COST%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('NO_ORD_COST_FOUND',
                                                  I_order_no,
                                                  I_comp_item,
                                                  L_supplier);
            close C_GET_ORDER_COST;
            return FALSE;
         end if;
         ---
         close C_GET_ORDER_COST;
      else
         L_cost := I_cost;
         ---
         if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE(O_error_message,
                                               L_currency_ord,
                                               L_ord_exchange_rate,
                                               I_order_no) = FALSE then
            return FALSE;
         end if;
      end if;
      --
      -- Convert the Order Cost from the order's currency
      -- to primary currency.
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_cost,
                              L_currency_ord,
                              L_currency_prim,
                              L_cost,
                              NULL,
                              NULL,
                              NULL,
                              L_ord_exchange_rate,
                              NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if I_comp_item is NOT NULL then
         ---
         open C_GET_UNIT_COST(I_comp_item);
         fetch C_GET_UNIT_COST into L_comp_item_cost,
                                    L_currency_sup;
         if C_GET_UNIT_COST%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('NO_SUP_CTRY_UNIT_COST',
                                                  I_comp_item,
                                                  L_supplier,
                                                  L_origin_country_id);
            close C_GET_UNIT_COST;
            return FALSE;
         end if;
         ---
         close C_GET_UNIT_COST;
         ---
         open C_GET_SUM_COMP_COST;
         fetch C_GET_SUM_COMP_COST into L_tot_comp_items_cost;
         close C_GET_SUM_COMP_COST;
         ---
         if L_tot_comp_items_cost = 0 then
            L_cost := 0;
         else
            L_cost := (NVL(L_comp_item_cost, 0)
                         * NVL(L_cost, 0))/NVL(L_tot_comp_items_cost, 1);
         end if;
      end if; -- if I_comp_sku is not NULL
   else-- L_buyer_pack = 'N'
      if I_cost is NULL then
         open C_GET_ORDER_COST;
         fetch C_GET_ORDER_COST into L_cost,
                                     L_currency_ord,
                                     L_ord_exchange_rate;
         if C_GET_ORDER_COST%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('NO_ORD_COST_FOUND',
                                                  I_order_no,
                                                  I_comp_item,
                                                  L_supplier);
            close C_GET_ORDER_COST;
            return FALSE;
         end if;
         ---
         close C_GET_ORDER_COST;
      else
         L_cost := I_cost;
         ---
         if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE(O_error_message,
                                               L_currency_ord,
                                               L_ord_exchange_rate,
                                               I_order_no) = FALSE then
            return FALSE;
         end if;
      end if;
      --
      -- Convert the Order Cost from the order's currency
      -- to primary currency.
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_cost,
                              L_currency_ord,
                              L_currency_prim,
                              L_cost,
                              NULL,
                              NULL,
                              NULL,
                              L_ord_exchange_rate,
                              NULL) = FALSE then
         return FALSE;
      end if;
   end if; -- L_buyer_pack = 'Y'/'N'
   ---
   -- Add the Total Expense value to the Cost to
   -- get Total Estimated Landed Cost (Total ELC).  The value
   -- is in the Primary currency.
   ---
   O_total_elc := NVL(L_avg_unit_elc, 0) + NVL(L_cost, 0);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CALC_ELC;
--------------------------------------------------------------------------------------
FUNCTION FM_COST_ADJUSTMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                           I_ref_doc_no       IN       SHIPMENT.REF_DOC_NO%TYPE,
                           I_item             IN       SHIPSKU.ITEM%TYPE,
                           I_old_ebc          IN       SHIPSKU.UNIT_COST%TYPE,
                           I_new_ebc          IN       SHIPSKU.UNIT_COST%TYPE,
                           I_new_nic          IN       SHIPSKU.UNIT_COST%TYPE)

return BOOLEAN is

   L_program           VARCHAR2(60) := 'REC_COST_ADJ_SQL.FM_COST_ADJUSTMENT';
   L_vdate             PERIOD.VDATE%TYPE := GET_VDATE();
   L_dept              ITEM_MASTER.DEPT%TYPE;
   L_currency_order    CURRENCIES.CURRENCY_CODE%TYPE;
   L_currency_loc      CURRENCIES.CURRENCY_CODE%TYPE;
   L_old_ebc_loc       SHIPSKU.UNIT_COST%TYPE;
   L_new_ebc_loc       SHIPSKU.UNIT_COST%TYPE;
   L_sysopt_rec        SYSTEM_OPTIONS%ROWTYPE;
   L_item_rec          ITEM_MASTER%ROWTYPE;
   L_itemloc_rec       ITEM_LOC%ROWTYPE;
   L_ordhead_rec       ORDHEAD%ROWTYPE;
   L_ordsku_rec        ORDSKU%ROWTYPE;
   L_ordloc_rec        ORDLOC%ROWTYPE;
   L_exists            BOOLEAN;
   
   cursor C_OTB is
      select otb_calc_type
        from deps
       where dept = L_dept;

   cursor C_GET_LOCS is
      select sm.shipment,
             sm.to_loc,
             sm.to_loc_type,
             im.dept,
             im.class,
             im.subclass,
             oh.currency_code,
             ol.location,
             ol.loc_type,
             ss.qty_received,
             ol.qty_ordered
        from ordhead oh,
             ordloc ol,
             shipment sm,
             shipsku ss,
             item_master im
       where oh.order_no = I_order_no
         and ol.item     = I_item
         and ol.order_no = oh.order_no
         and sm.order_no = ol.order_no
         and sm.ref_doc_no = I_ref_doc_no
         and sm.shipment = ss.shipment
         and ss.item = ol.item
         and ol.item = im.item
         and ((ol.location in (select wh
                                 from wh
                                where physical_wh = sm.to_loc)
               and sm.to_loc_type = 'W') OR
              ( ol.location = sm.to_loc and sm.to_loc_type = 'S'));

   cursor C_GET_SHIP_INFO is
      select sm.shipment
        from shipment sm
       where sm.order_no = I_order_no
         and sm.ref_doc_no = NVL(I_ref_doc_no, sm.ref_doc_no);
    --
   TYPE SHIPMENT_TBL_TYPE IS TABLE OF C_GET_SHIP_INFO%ROWTYPE INDEX BY BINARY_INTEGER;
   shipment_tbl SHIPMENT_TBL_TYPE;

   ---------------------------------------------------------------------------
   FUNCTION UPD_AV_COST_CHANGE_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                    I_location              IN       ORDLOC.LOCATION%TYPE,
                                    I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE,
                                    I_old_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                    I_new_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                    I_receipt_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                    I_new_wac_loc           IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                    I_neg_soh_wac_adj_amt   IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                    I_recalc_ind            IN       VARCHAR2,
                                    I_order_number          IN       ORDHEAD.ORDER_NO%TYPE,
                                    I_ref_no_2              IN       TRAN_DATA.REF_NO_2%TYPE,
                                    I_pack_item             IN       ITEM_MASTER.ITEM%TYPE,
                                    I_pgm_name              IN       TRAN_DATA.PGM_NAME%TYPE,
                                    I_adj_code              IN       TRAN_DATA.ADJ_CODE%TYPE,
                                    I_new_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE,
                                    I_old_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE,
                                    I_ref_doc_no            IN       SHIPMENT.REF_DOC_NO%TYPE,
                                    I_adjust_matched_ind    IN       VARCHAR2 DEFAULT 'N')

   RETURN BOOLEAN IS

      L_stock_on_hand         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE   := 0;
      L_old_av_cost_loc       ITEM_LOC_SOH.AV_COST%TYPE         := 0;
      L_new_av_cost_loc       ITEM_LOC_SOH.AV_COST%TYPE;
      L_neg_soh_wac_adj_amt   ITEM_LOC_SOH.AV_COST%TYPE;
      L_neg_soh_wac_adj_qty   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;

      L_recalc_ind            VARCHAR2(1)    := NVL(I_recalc_ind, 'Y');
      L_item_master           ITEM_MASTER%ROWTYPE;
      L_tran_code             TRAN_DATA.TRAN_CODE%TYPE;
      L_tran_date             DATE           := get_vdate;

      RECORD_LOCKED           EXCEPTION;
      PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

      L_loc_string            VARCHAR2(25);
      L_table                 VARCHAR2(30);
      L_key1                  VARCHAR2(100);
      L_key2                  VARCHAR2(100);
      L_systems_options_row   SYSTEM_OPTIONS%ROWTYPE;
      L_adj_qty               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
      L_program               VARCHAR2(45)   := 'ITEMLOC_UPDATE_SQL.UPD_AV_COST_CHANGE_COST';
      L_new_cst               TRAN_DATA.TOTAL_COST%TYPE;
      L_tran_data_units       TRAN_DATA.UNITS%TYPE;
      L_tot_cost_excl_elc     TRAN_DATA.TOTAL_COST_EXCL_ELC%TYPE;

      L_loc                   SHIPMENT.TO_LOC%TYPE;
      L_process_item          ITEM_MASTER.ITEM%TYPE;
      L_pack_qty              ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
      L_receive_as_type       ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
      L_po_pwh_rec_qty        SHIPSKU.QTY_RECEIVED%TYPE;
      L_po_vwh_rec_qty        SHIPSKU.QTY_RECEIVED%TYPE;
      L_shs_qty_received      SHIPSKU.QTY_RECEIVED%TYPE;
      L_backpost_rca_rua_ind  PROCUREMENT_UNIT_OPTIONS.BACKPOST_RCA_RUA_IND%TYPE;
      L_ref_pack_no           TRAN_DATA.REF_PACK_NO%TYPE := I_pack_item;

      L_l10n_fin_rec          L10N_FIN_REC := L10N_FIN_REC();

      cursor C_PHY_SHIPMENT is
         select physical_wh
           from wh
          where wh = I_location;

      cursor C_GET_PACK_COMP_QTY is
         select qty
           from v_packsku_qty
          where pack_no = I_pack_item
            and item = I_item;

      -- For 'S'tandard calculate based on receipt qty whether matched or unmatched
      -- For 'F'IFO calculate based on unmatched reteipt qty

      cursor C_SHIPMENT is
         select *
           from (   -- the first subquery handles the receipt for L_process_item
                 select shp.shipment,
                        shp.receive_date,
                        shs.qty_received qty_received, -- total qty received for the shipment
                        shs.item,
                        L_pack_qty comp_qty
                   from shipment shp,
                        shipsku shs
                  where shp.shipment = shs.shipment
                    and shp.to_loc = L_loc
                    and shp.to_loc_type = I_loc_type
                    and shs.item = L_process_item
                    and shs.qty_received != 0
                    and ((L_systems_options_row.rcv_cost_adj_type = 'F'
                          and shp.status_code != 'C'
                          and shp.invc_match_status = 'U'
                          and nvl(shs.qty_matched, 0) = 0)
                     or L_systems_options_row.rcv_cost_adj_type = 'S')
                    and shp.order_no is NOT NULL
                    and shp.order_no = I_order_number
                    and shp.ref_doc_no = I_ref_doc_no
              UNION ALL   -- the second subquery handles pack items broken on receipt
                 select shp.shipment,
                        shp.receive_date,
                        shs.qty_received qty_received, -- total qty received for the shipment
                        shs.item,
                        v.qty comp_qty -- pack component quantity -- Validate this for pack
                   from shipment shp,
                        shipsku shs,
                        v_packsku_qty v,
                        item_loc il
                  where L_receive_as_type = 'E'
                    and shp.shipment = shs.shipment
                    and shp.to_loc = L_loc
                    and shp.to_loc_type = I_loc_type
                    and shs.qty_received != 0
                    and shs.item = v.pack_no
                    and il.item = v.pack_no
                    and il.loc = I_location
                    and nvl(il.receive_as_type,'E') = 'E'
                    and v.item = I_item
                    and v.pack_no = I_pack_item
                    and ((L_systems_options_row.rcv_cost_adj_type = 'F'
                          and shp.status_code != 'C'
                          and shp.invc_match_status = 'U'
                          and nvl(shs.qty_matched, 0) = 0)
                     or L_systems_options_row.rcv_cost_adj_type = 'S')
                    and shp.order_no is NOT NULL
                    and shp.order_no = I_order_number
                    and shp.ref_doc_no = I_ref_doc_no)
          order by receive_date desc;

       -- This cursor gives the distribution for requirement
       -- even if the pack was broken on receipt, we have to distribute
       -- based on the item that was shipped.
       cursor C_DISTRIBUTE_PO is
          select sum(nvl(DECODE(l.location, I_location, l.qty_received, 0), 0)),
                 sum(nvl(l.qty_received, 0))
            from ordloc l,
                 wh
           where physical_wh = L_loc
             and l.order_no  = I_order_number
             and l.item      = NVL(I_pack_item, I_item)
             and l.location  = wh;

       cursor C_PROCUREMENT_UNIT_OPTIONS is
          select backpost_rca_rua_ind
            from procurement_unit_options;
      ---
   BEGIN
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_master,
                                         I_item) = FALSE then
         return FALSE;
      end if;
      ---
      L_table := 'ITEM_LOC';
      L_key1  := I_item;
      L_key2  := TO_CHAR(I_location);
      ---
      if L_item_master.item_level = L_item_master.tran_level then
         L_loc_string := ' Location: '||TO_CHAR(I_location);
         ---
         L_l10n_fin_rec.procedure_key := 'GET_CURRENT_SOH_WAC';
         L_l10n_fin_rec.source_entity := 'LOC';
         L_l10n_fin_rec.source_id     := I_location;
         L_l10n_fin_rec.source_type   := I_loc_type;
         L_l10n_fin_rec.item          := I_item;
         L_l10n_fin_rec.calc_fld_type := 'TOTSTK';

         --For localized country Brazil, the l10n_sql.exec_function will call L10N_BR_FIN_SQL.GET_CURRENT_SOH_WAC to
         --get the SOH for all the virtual locations having the same CNPJ number.
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec) = FALSE then
            return FALSE;
         end if;

         L_stock_on_hand   := L_l10n_fin_rec.stock_on_hand;
         L_old_av_cost_loc := L_l10n_fin_rec.av_cost;
         ---
         if L_recalc_ind = 'Y' then
            ---
            if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                     L_systems_options_row) = FALSE then
               return FALSE;
            end if;
            ---
            open C_PROCUREMENT_UNIT_OPTIONS;
            SQL_LIB.SET_MARK('OPEN',
                              'C_PROCUREMENT_UNIT_OPTIONS',
                              NULL,
                              NULL);
            fetch C_PROCUREMENT_UNIT_OPTIONS into L_backpost_rca_rua_ind;
            SQL_LIB.SET_MARK('FETCH',
                              'C_PROCUREMENT_UNIT_OPTIONS',
                              NULL,
                              NULL);
            close C_PROCUREMENT_UNIT_OPTIONS;
            SQL_LIB.SET_MARK('CLOSE',
                              'C_PROCUREMENT_UNIT_OPTIONS',
                              NULL,
                              NULL);

            if L_backpost_rca_rua_ind = 'Y' then
               if I_loc_type = 'W' then
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_PHY_SHIPMENT',
                                   NULL,
                                   'Location: '||TO_CHAR(I_location));
                  open C_PHY_SHIPMENT;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_PHY_SHIPMENT',
                                   NULL,
                                   'Location: '||TO_CHAR(I_location));
                  fetch C_PHY_SHIPMENT into L_loc;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_PHY_SHIPMENT',
                                   NULL,
                                   'Location: '||TO_CHAR(I_location));
                  close C_PHY_SHIPMENT;
                  if L_loc is null then
                     O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                                            L_program,
                                                            'WH',
                                                            'WH:'||I_location);
                     return FALSE;
                  end if;

               else
                  L_loc := I_location;
               end if;

               if I_pack_item is NULL then
                  L_process_item := I_item;
                  L_receive_as_type := 'E';
                  L_pack_qty := 1;
               else
                  if I_loc_type = 'W' then
                     if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                               L_receive_as_type,
                                                               I_pack_item,
                                                               I_location) = FALSE then
                        return FALSE;
                     end if;
                  else
                     L_receive_as_type := 'E';
                  end if;
                  if L_receive_as_type = 'E' then
                     L_process_item := I_item;
                  else
                     L_process_item := I_pack_item;
                  end if;

                  SQL_LIB.SET_MARK('OPEN',
                                   'C_GET_PACK_COMP_QTY',
                                   NULL,
                                   'Pack Item: '||TO_CHAR(I_pack_item)|| ', Component Item: '||TO_CHAR(I_item));
                  open C_GET_PACK_COMP_QTY;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_GET_PACK_COMP_QTY',
                                   NULL,
                                   'Pack Item: '||TO_CHAR(I_pack_item)|| ', Component Item: '||TO_CHAR(I_item));
                  fetch C_GET_PACK_COMP_QTY into L_pack_qty;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_GET_PACK_COMP_QTY',
                                   NULL,
                                   'Pack Item: '||TO_CHAR(I_pack_item)|| ', Component Item: '||TO_CHAR(I_item));
                  close C_GET_PACK_COMP_QTY;
                  if L_pack_qty is null then
                     O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                                            L_program,
                                                            'V_PACKSKU_QTY',
                                                            'PACK_NO:'||I_pack_item||', ITEM:'||I_item);
                     return FALSE;
                  end if;
               end if;
            end if;
            ---
            if L_systems_options_row.rcv_cost_adj_type = 'S' then
               if STKLEDGR_ACCTING_SQL.WAC_CALC_COST_CHANGE(O_error_message,
                                                            L_new_av_cost_loc,
                                                            L_neg_soh_wac_adj_amt,
                                                            L_old_av_cost_loc,
                                                            L_stock_on_hand,
                                                            I_new_cost,
                                                            I_old_cost,
                                                            I_receipt_qty) = FALSE then
                  return FALSE;
               end if;
               ---
               --- note: currently only alc_sql is expecting this record to be written
               --- ultimately the other callers should be changed so that the 20 record
               --- is only written from here. The adj code is inserted as A only for
               --- alc sql
               if I_pgm_name is not null and I_receipt_qty > 0 then
                  L_tran_code := 20;
                  if L_backpost_rca_rua_ind = 'N' then
                     L_new_cst := (I_receipt_qty * (I_new_cost - I_old_cost));
                     L_tot_cost_excl_elc := (I_receipt_qty * (I_new_cost_excl_elc - I_old_cost_excl_elc));

                     if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                                      I_item,
                                                      L_item_master.dept,
                                                      L_item_master.class,
                                                      L_item_master.subclass,
                                                      I_location,
                                                      I_loc_type,
                                                      L_tran_date,
                                                      L_tran_code,
                                                      I_adj_code,
                                                      I_receipt_qty,
                                                      L_new_cst,
                                                      0,
                                                      I_order_number,   -- I_ref_no_1,
                                                      I_ref_no_2,   -- I_ref_no_2,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      nvl(I_pgm_name, L_program),
                                                      NULL,
                                                      L_ref_pack_no,
                                                      L_tot_cost_excl_elc) = FALSE then
                        return FALSE;
                     end if;
                  else -- If unit option is set to 'Y'
                     FOR rec in C_SHIPMENT LOOP
                        L_po_vwh_rec_qty := NULL;
                        L_po_pwh_rec_qty := NULL;

                        if I_loc_type = 'W' then
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_DISTRIBUTE_PO',
                                            NULL,
                                            'VWH: ' || I_location || ', PWH: '|| L_loc ||
                                            ', PO: ' || I_order_number || ', Item: '||nvl(I_pack_item, I_item));
                           open C_DISTRIBUTE_PO;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_DISTRIBUTE_PO',
                                            NULL,
                                            'VWH: ' || I_location || ', PWH: '|| L_loc ||
                                            ', PO: ' || I_order_number || ', Item: '||nvl(I_pack_item, I_item));
                           fetch C_DISTRIBUTE_PO into L_po_vwh_rec_qty, L_po_pwh_rec_qty;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_DISTRIBUTE_PO',
                                            NULL,
                                            'VWH: ' || I_location || ', PWH: '|| L_loc ||
                                            ', PO: ' || I_order_number || ', Item: '||nvl(I_pack_item, I_item));
                          close C_DISTRIBUTE_PO;
                           -- this validation would fail if the function were ever called
                           -- for a virtual warehouse that wasn't on the PO
                           if L_po_vwh_rec_qty is null then
                              O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                                                     L_program,
                                                                     'ORDLOC',
                                                                     'ORDER_NO:'||I_order_number
                                                                     ||', ITEM:'||nvl(I_pack_item, I_item)
                                                                     ||', LOCATION:'||I_location);
                              return FALSE;
                           end if;
                        end if;

                        -- make sure that PO received qty is positive
                        if L_po_pwh_rec_qty > L_po_vwh_rec_qty
                        and L_po_vwh_rec_qty >= 0 then
                           L_shs_qty_received := rec.qty_received * rec.comp_qty
                                                    * (L_po_vwh_rec_qty / L_po_pwh_rec_qty);
                        else
                           L_shs_qty_received := rec.qty_received * rec.comp_qty;
                        end if;

                        L_new_cst := (L_shs_qty_received * (I_new_cost - I_old_cost));
                        L_tot_cost_excl_elc := (L_shs_qty_received * (I_new_cost_excl_elc - I_old_cost_excl_elc));

                        if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                                         I_item,
                                                         L_item_master.dept,
                                                         L_item_master.class,
                                                         L_item_master.subclass,
                                                         I_location,
                                                         I_loc_type,
                                                         rec.receive_date, --Post tran_data on receipt date
                                                         L_tran_code,
                                                         I_adj_code,
                                                         L_shs_qty_received,
                                                         L_new_cst,
                                                         0,
                                                         I_order_number,   -- I_ref_no_1,
                                                         rec.shipment,   -- I_ref_no_2,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         nvl(I_pgm_name, L_program),
                                                         NULL,
                                                         L_ref_pack_no,
                                                         L_tot_cost_excl_elc) = FALSE then
                           return FALSE;
                        end if;
                     END LOOP;
                  end if;
                  ---
                  if CREATE_ORD_TSF_SQL.BUILD_ADJ_TRAN_DATA_SG  (O_error_message,
                                                                 I_order_number,
                                                                 I_item,
                                                                 L_new_cst,
                                                                 0,
                                                                 I_receipt_qty,
                                                                 I_pack_item) = FALSE then
                     return FALSE;
                  end if;
                  ---
               end if;
               ---
               if L_neg_soh_wac_adj_amt != 0 then
                  ---
                  L_tran_code := 70;
                  ---
                  if L_stock_on_hand > 0 and L_stock_on_hand < I_receipt_qty then
                     L_tran_data_units := I_receipt_qty - L_stock_on_hand;
                  else
                     L_tran_data_units := I_receipt_qty;
                  end if;
                  ---
                  if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                                   I_item,
                                                   L_item_master.dept,
                                                   L_item_master.class,
                                                   L_item_master.subclass,
                                                   I_location,
                                                   I_loc_type,
                                                   L_tran_date,
                                                   L_tran_code,
                                                   NULL,
                                                   L_tran_data_units,     -- unit
                                                   L_neg_soh_wac_adj_amt,
                                                   NULL,         -- Total Retail
                                                   I_order_number,   -- I_ref_no_1,
                                                   I_ref_no_2,   -- I_ref_no_2,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   nvl(I_pgm_name, L_program),
                                                   NULL) = FALSE then
                     RETURN FALSE;
                  end if;
               end if;
            elsif  L_systems_options_row.rcv_cost_adj_type = 'F' then
               ---
               if L_backpost_rca_rua_ind = 'N' then
                  if STKLEDGR_ACCTING_SQL.ALT_WAC_CALC_COST_CHANGE(O_error_message,
                                                                   L_new_av_cost_loc,
                                                                   L_adj_qty,
                                                                   L_neg_soh_wac_adj_qty,
                                                                   L_old_av_cost_loc,
                                                                   L_stock_on_hand,
                                                                   I_old_cost,
                                                                   I_order_number,
                                                                   I_new_cost,
                                                                   I_location,
                                                                   I_item,
                                                                   I_receipt_qty,
                                                                   I_pack_item) = FALSE then

                     return FALSE;
                  end if;
                  if L_adj_qty > 0 then
                     L_tran_code := 20;
                     L_new_cst := (L_adj_qty * (I_new_cost - I_old_cost));
                     L_tot_cost_excl_elc := (L_adj_qty * (I_new_cost_excl_elc - I_old_cost_excl_elc));
                     if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                                      I_item,
                                                      L_item_master.dept,
                                                      L_item_master.class,
                                                      L_item_master.subclass,
                                                      I_location,
                                                      I_loc_type,
                                                      L_tran_date,
                                                      L_tran_code,
                                                      I_adj_code,
                                                      L_adj_qty,
                                                      L_new_cst,
                                                      0,
                                                      I_order_number,   -- I_ref_no_1,
                                                      I_ref_no_2,   -- I_ref_no_2,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      nvl(I_pgm_name, L_program),
                                                      NULL,
                                                      L_ref_pack_no,
                                                      L_tot_cost_excl_elc) = FALSE then

                        return FALSE;
                     end if;
                     ---
                     if CREATE_ORD_TSF_SQL.BUILD_ADJ_TRAN_DATA_SG  (O_error_message,
                                                                    I_order_number,
                                                                    I_item,
                                                                    L_new_cst,
                                                                    0,
                                                                    L_adj_qty,
                                                                    I_pack_item) = FALSE then
                        return FALSE;
                     end if;
                     ---
                  end if;

                  if L_neg_soh_wac_adj_qty > 0 then
                     L_tran_code := 73;
                     L_new_cst   := (L_neg_soh_wac_adj_qty * (I_new_cost - I_old_cost));
                     L_tot_cost_excl_elc := (L_neg_soh_wac_adj_qty * (I_new_cost_excl_elc - I_old_cost_excl_elc));

                     if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                                      I_item,
                                                      L_item_master.dept,
                                                      L_item_master.class,
                                                      L_item_master.subclass,
                                                      I_location,
                                                      I_loc_type,
                                                      L_tran_date,
                                                      L_tran_code,
                                                      NULL,
                                                      L_neg_soh_wac_adj_qty,
                                                      L_new_cst,
                                                      NULL,
                                                      I_order_number,   -- I_ref_no_1,
                                                      I_ref_no_2,   -- I_ref_no_2,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      nvl(I_pgm_name, L_program),
                                                      NULL,
                                                      L_ref_pack_no,
                                                      L_tot_cost_excl_elc) = FALSE then
                        return FALSE;
                     end if;
                  end if;
               else --- L_backpost_rca_rua_ind = Y
                  ---
                  FOR rec in C_SHIPMENT LOOP
                     ---
                     if STKLEDGR_ACCTING_SQL.ALT_WAC_CALC_COST_CHANGE(O_error_message,
                                                                      L_new_av_cost_loc,
                                                                      L_adj_qty,
                                                                      L_neg_soh_wac_adj_qty,
                                                                      L_old_av_cost_loc,
                                                                      L_stock_on_hand,
                                                                      I_old_cost,
                                                                      I_order_number,
                                                                      I_new_cost,
                                                                      I_location,
                                                                      I_item,
                                                                      I_receipt_qty,
                                                                      I_pack_item,
                                                                      rec.shipment) = FALSE then
                        return FALSE;
                     end if;
                     ---
                     L_old_av_cost_loc := L_new_av_cost_loc ;
                     ---

                     if L_adj_qty > 0 then
                        L_tran_code := 20;
                        L_new_cst := (L_adj_qty  * (I_new_cost - I_old_cost));
                        L_tot_cost_excl_elc := (L_adj_qty * (I_new_cost_excl_elc - I_old_cost_excl_elc));

                        if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                                         I_item,
                                                         L_item_master.dept,
                                                         L_item_master.class,
                                                         L_item_master.subclass,
                                                         I_location,
                                                         I_loc_type,
                                                         rec.receive_date,-- Post tran_data on receipt date
                                                         L_tran_code,
                                                         I_adj_code,
                                                         L_adj_qty,
                                                         L_new_cst,
                                                         0,
                                                         I_order_number,-- I_ref_no_1,
                                                         rec.shipment,-- I_ref_no_2,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         nvl(I_pgm_name, L_program),
                                                         NULL,
                                                         L_ref_pack_no,
                                                         L_tot_cost_excl_elc) = FALSE then
                           return FALSE;
                        end if;
                        ---
                        if CREATE_ORD_TSF_SQL.BUILD_ADJ_TRAN_DATA_SG (O_error_message,
                                                                      I_order_number,
                                                                      I_item,
                                                                      L_new_cst,
                                                                      0,
                                                                      L_adj_qty,
                                                                      I_pack_item) = FALSE then
                           return FALSE;
                        end if;
                        ---
                     end if;

                     if L_neg_soh_wac_adj_qty > 0 then

                        L_tran_code := 73;
                        L_new_cst := (L_neg_soh_wac_adj_qty  * (I_new_cost - I_old_cost));
                        L_tot_cost_excl_elc := (L_neg_soh_wac_adj_qty * (I_new_cost_excl_elc - I_old_cost_excl_elc));

                        if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                                         I_item,
                                                         L_item_master.dept,
                                                         L_item_master.class,
                                                         L_item_master.subclass,
                                                         I_location,
                                                         I_loc_type,
                                                         rec.receive_date,--Post tran_data on receipt date
                                                         L_tran_code,
                                                         NULL,
                                                         L_neg_soh_wac_adj_qty,
                                                         L_new_cst,
                                                         0,
                                                         I_order_number,-- I_ref_no_1,
                                                         rec.shipment,-- I_ref_no_2,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         NULL,
                                                         nvl(I_pgm_name, L_program),
                                                         NULL,
                                                         L_ref_pack_no,
                                                         L_tot_cost_excl_elc) = FALSE then
                           return FALSE;
                        end if;
                     end if;
                  END LOOP;
               end if;
            end if;   ----END!
         else
            L_new_av_cost_loc := I_new_wac_loc;
            L_neg_soh_wac_adj_amt := I_neg_soh_wac_adj_amt;
         end if;
         ---
         L_l10n_fin_rec.procedure_key := 'UPDATE_AV_COST';
         L_l10n_fin_rec.av_cost       := L_new_av_cost_loc;

         --For localized country Brazil, the l10n_sql.exec_function will call L10N_BR_FIN_SQL.UPDATE_AV_COST to
         --get the SOH for all the virtual locations having the same CNPJ number.
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      return TRUE;

   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                L_table,
                                                L_key1,
                                                L_key2);
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END UPD_AV_COST_CHANGE_COST;
   ---------------------------------------------------------------------------
   FUNCTION UPD_PACK_ITEM_COST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_pack_item          IN       ordsku.item%TYPE,
                              I_tran_date          IN       DATE,
                              I_order_no           IN       ordhead.order_no%TYPE,
                              I_pc_old_loc         IN       ordloc.unit_cost%TYPE,
                              I_pc_new_loc         IN       ordloc.unit_cost%TYPE,
                              I_supplier           IN       sups.supplier%TYPE,
                              I_origin_country_id  IN       ordsku.origin_country_id%TYPE,
                              I_import_country_id  IN       ordhead.import_country_id%TYPE,
                              I_exchange_rate_ord  IN       ordhead.exchange_rate%TYPE,
                              I_prim_currency      IN       currencies.currency_code%TYPE,
                              I_order_currency     IN       currencies.currency_code%TYPE,
                              I_alc_status         IN       alc_head.status%TYPE,
                              I_import_order_ind   IN       ordhead.import_order_ind%TYPE,
                              I_import_ind         IN       system_options.import_ind%TYPE,
                              I_elc_ind            IN       system_options.elc_ind%TYPE,
                              I_location           IN       ordloc.location%TYPE,
                              I_loc_type           IN       ordloc.loc_type%TYPE)
   RETURN BOOLEAN IS

      L_program               VARCHAR2(45)                      := 'ITEMLOC_UPDATE_SQL.UPD_PACK_ITEM_COST';
      L_stock_on_hand         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE   := 0;
      L_old_av_cost_loc       ITEM_LOC_SOH.AV_COST%TYPE         := 0;
      RECORD_LOCKED           EXCEPTION;
      PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);
      L_cost_diff_prim        NUMBER(20,8);
      L_cost_diff_temp        NUMBER(20,8);
	  L_default_tax_type      SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;
   --
   cursor C_PACK_TYPE is
      select pack_type
        from item_master
       where item = I_pack_item;

   cursor C_SUM_COMP is
      select DECODE(LP_default_tax_type, 'GTAX', SUM(v.qty * i.extended_base_cost), SUM(v.qty * i.unit_cost))
        from v_packsku_qty v, item_supp_country_loc i
       where v.pack_no           = I_pack_item
         and i.item              = v.item
         and i.supplier          = I_supplier
         and i.origin_country_id = I_origin_country_id
         and i.loc               = I_location
         and i.loc_type          = I_loc_type;
		 
      cursor C_ORDLOC is
      select ol.location,
             ol.loc_type,
             ol.qty_ordered,
             nvl(ss.qty_received,0) qty_received
        from ordloc ol, shipment sh, shipsku ss
       where ol.order_no            = LP_order_no
         and ol.item                = LP_item
         and nvl(ol.qty_received,0) > 0
         and ol.location            = LP_location
         and ol.loc_type            = LP_loc_type
         and sh.order_no            = ol.order_no
         and sh.ref_doc_no          = I_ref_doc_no
         and sh.shipment            = ss.shipment
         and ss.item                = ol.item
         and ((ol.location in (select wh
                                 from wh
                                where physical_wh = sh.to_loc)
               and sh.to_loc_type = 'W') OR
              ( ol.location = sh.to_loc and sh.to_loc_type = 'S'));		 
		 
   BEGIN
      LP_item              := I_pack_item;
      LP_tran_date         := I_tran_date;
      LP_order_no          := I_order_no;
      LP_supplier          := I_supplier;
      LP_tran_type         := 20;
      LP_origin_country_id := I_origin_country_id;
      LP_import_country_id := I_import_country_id;
      LP_exchange_rate_ord := I_exchange_rate_ord;
      LP_prim_currency     := I_prim_currency;
      LP_order_currency    := I_order_currency;
      LP_import_order_ind  := I_import_order_ind;
      LP_location          := I_location;
      LP_loc_type          := I_loc_type;
      --
	  
	  if NOT SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_message,
                                                     LP_default_tax_type) then
         return FALSE;
      end if;
	  
      open C_PACK_TYPE;
      fetch C_PACK_TYPE into LP_pack_type;
      if C_PACK_TYPE%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PAK',LP_item,NULL,NULL);
         close C_PACK_TYPE;
         return FALSE;
      end if;
      close C_PACK_TYPE;
      --
      open C_SUM_COMP;
      fetch C_SUM_COMP into LP_sum_comp_cost;
      if C_SUM_COMP%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PAK',LP_item,NULL,NULL);
         close C_SUM_COMP;
         return FALSE;
      end if;
      close C_SUM_COMP;
      --
      if LP_pack_type = 'B' then
         FOR packsku_rec IN CP_PACK_ITEMS LOOP
             LP_comp_item  := packsku_rec.item;
             LP_comp_qty   := packsku_rec.qty;
             LP_comp_cost  := packsku_rec.unit_cost;
             LP_dept       := packsku_rec.dept;
             LP_class      := packsku_rec.class;
             LP_subclass   := packsku_rec.subclass;
             ---
             FOR c_rec IN c_ordloc LOOP
                LP_location    := c_rec.location;
                LP_loc_type    := c_rec.loc_type;
                LP_ordered_qty := c_rec.qty_ordered;
                LP_receipt_qty := c_rec.qty_received;
                ---
                LP_comp_ord_qty := LP_comp_qty * LP_ordered_qty;
                LP_comp_rcv_qty := LP_comp_qty * LP_receipt_qty;
                if UPD_AV_COST_CHANGE_COST(O_error_message,
                                       LP_comp_item,  -- Component Item
                                       LP_location,
                                       LP_loc_type,
                                       (I_pc_old_loc * LP_comp_cost/LP_sum_comp_cost),
                                       (I_pc_new_loc * LP_comp_cost/LP_sum_comp_cost),
                                       LP_comp_rcv_qty,
                                       NULL,
                                       NULL,
                                       'Y',
                                       I_order_no,
                                       NULL,
                                       I_item,   -- Pack Item
                                       'RFM',
                                       'C',
                                       (I_pc_new_loc * LP_comp_cost/LP_sum_comp_cost),
                                       (I_pc_old_loc * LP_comp_cost/LP_sum_comp_cost),
                                       I_ref_doc_no) = FALSE then
                  return FALSE;
               end if;
               L_cost_diff_temp := (I_pc_new_loc - I_pc_old_loc) * LP_comp_cost/LP_sum_comp_cost;
               --
               if LP_prim_currency = LP_local_currency then
                  L_cost_diff_prim := L_cost_diff_temp;
               else
                  if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_cost_diff_prim,
                                    LP_prim_currency,
                                    LP_local_currency,
                                    L_cost_diff_temp,
                                    'C',
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
               --
               if OTB_SQL.ORD_RECEIVE( O_error_message  ,
                           0                ,
                           L_cost_diff_prim ,
                           LP_order_no      ,
                           LP_dept          ,
                           LP_class         ,
                           LP_subclass      ,
                           LP_comp_rcv_qty  ,
                           LP_comp_ord_qty ) = FALSE then
                  return FALSE;
               end if;
            END LOOP;  -- loop ordloc
         END LOOP;  -- loop v_packsku_qty;
      else
         FOR c_rec IN CP_ORDLOC LOOP
            LP_location    := c_rec.location;
            LP_loc_type    := c_rec.loc_type;
            LP_ordered_qty := c_rec.qty_ordered;
            LP_receipt_qty := c_rec.qty_received;
            --
            FOR packsku_rec IN cp_pack_items LOOP
               LP_comp_item      := packsku_rec.item;
               LP_comp_ord_qty   := packsku_rec.qty * LP_ordered_qty;
               LP_comp_rcv_qty   := packsku_rec.qty * LP_receipt_qty;
               LP_comp_cost      := packsku_rec.unit_cost;
               LP_dept           := packsku_rec.dept;
               LP_class          := packsku_rec.class;
               LP_subclass       := packsku_rec.subclass;
               --
               if UPD_AV_COST_CHANGE_COST(O_error_message,
                                       LP_comp_item,  -- Component Item
                                       LP_location,
                                       LP_loc_type,
                                       (I_pc_old_loc * LP_comp_cost/LP_sum_comp_cost),
                                       (I_pc_new_loc * LP_comp_cost/LP_sum_comp_cost),
                                       LP_comp_rcv_qty,
                                       NULL,
                                       NULL,
                                       'Y',
                                       I_order_no,
                                       NULL,
                                       I_item,   -- Pack Item
                                       'RFM',
                                       'C',
                                       (I_pc_new_loc * LP_comp_cost/LP_sum_comp_cost),
                                       (I_pc_old_loc * LP_comp_cost/LP_sum_comp_cost),
                                       I_ref_doc_no) = FALSE then
                  return FALSE;
               end if;
               L_cost_diff_temp := (I_pc_new_loc - I_pc_old_loc) * LP_comp_cost/LP_sum_comp_cost;
               --
               if LP_prim_currency = LP_local_currency then
                  L_cost_diff_prim := L_cost_diff_temp;
               else
                  if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_cost_diff_prim,
                                    LP_prim_currency,
                                    LP_local_currency,
                                    L_cost_diff_temp,
                                    'C',
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
               --
               if OTB_SQL.ORD_RECEIVE( O_error_message  ,
                           0                ,
                           L_cost_diff_prim ,
                           LP_order_no      ,
                           LP_dept          ,
                           LP_class         ,
                           LP_subclass      ,
                           LP_comp_rcv_qty  ,
                           LP_comp_ord_qty ) = FALSE then
                  return FALSE;
               end if;
            END LOOP;  -- loop v_packsku_qty;
         END LOOP;  -- loop ordloc
      end if;

      return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
   END UPD_PACK_ITEM_COST;
   -------------------------------------------------------------------------------------------
BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_sysopt_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if ORDER_ATTRIB_SQL.GET_ORDHEAD_ROW(O_error_message,
                                       L_ordhead_rec,
                                       I_order_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if L_ordhead_rec.order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                             L_program,
                                             'ORDHEAD',
                                             'ORDER_NO:'||I_order_no);
      return FALSE;
   end if;
   ---
   if ORDER_ATTRIB_SQL.GET_ORDSKU_ROW(O_error_message,
                                      L_exists,
                                      L_ordsku_rec,
                                      I_order_no,
                                      I_item) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if L_ordsku_rec.order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                             L_program,
                                             'ORDSKU',
                                             'ORDER_NO:'||I_order_no||', ITEM:'||I_item);
      return FALSE;
   end if;
   --
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_rec,
                                      I_item) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   if L_item_rec.item is null then
      O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                             L_program,
                                             'ITEM_MASTER',
                                             'ITEM:'||I_item);
      return FALSE;
   end if;
   ---
   if L_item_rec.item_level != L_item_rec.tran_level then
      return TRUE;
   end if;
   --
   -- Loop through the virtual locations of the shipments attached
   -- to the Nota Fiscal Number.
   FOR ordloc_rec in C_GET_LOCS LOOP
      -- Retrieve location currency.
      if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                   ordloc_rec.location,
                                   ordloc_rec.loc_type,
                                   NULL,
                                   LP_local_currency) = FALSE then
         return FALSE;
      end if;

      -- Convert Old and New EBC from order to location currency.
      if CURRENCY_SQL.CONVERT(O_error_message,
                              I_old_ebc,
                              ordloc_rec.currency_code,
                              LP_local_currency,
                              L_old_ebc_loc,
                              'C',
                              L_vdate,
                              NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              I_new_ebc,
                              ordloc_rec.currency_code,
                              LP_local_currency,
                              L_new_ebc_loc,
                              'C',
                              L_vdate,
                              NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if ordloc_rec.qty_received > 0 then
         ---
         if L_item_rec.pack_ind = 'N' then
            if UPD_AV_COST_CHANGE_COST(O_error_message,
                                       I_item,
                                       ordloc_rec.location,
                                       ordloc_rec.loc_type,
                                       L_old_ebc_loc,
                                       L_new_ebc_loc,
                                       ordloc_rec.qty_received,
                                       NULL,
                                       NULL,
                                       'Y',
                                       I_order_no,
                                       NULL,
                                       NULL,
                                       'RFM',
                                       'C',
                                       L_new_ebc_loc,
                                       L_old_ebc_loc,
                                       I_ref_doc_no) = FALSE then
               return FALSE;
            end if;
            if OTB_SQL.ORD_RECEIVE( O_error_message  ,
                           0                ,
                           (I_new_ebc - I_old_ebc),
                           I_order_no      ,
                           L_item_rec.dept          ,
                           L_item_rec.class         ,
                           L_item_rec.subclass      ,
                           ordloc_rec.qty_received  ,
                           ordloc_rec.qty_ordered ) = FALSE then
               return FALSE;
            end if;
         else
            if UPD_PACK_ITEM_COST(O_error_message,
                                  I_item,
                                  L_vdate,
                                  I_order_no,
                                  L_old_ebc_loc,
                                  L_new_ebc_loc,
                                  L_ordhead_rec.supplier,
                                  L_ordsku_rec.origin_country_id ,
                                  L_ordhead_rec.import_country_id, --I_import_country_id
                                  L_ordhead_rec.exchange_rate, --I_exchange_rate_ord
                                  L_sysopt_rec.currency_code, --I_prim_currency,
                                  L_ordhead_rec.currency_code, -- I_order_currency
                                  null, -- I_alc_status,
                                  L_ordhead_rec.import_order_ind, -- I_import_order_ind
                                  L_sysopt_rec.import_ind, -- I_import_ind
                                  L_sysopt_rec.elc_ind,
                                  ordloc_rec.location,
                                  ordloc_rec.loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
         --
      end if;

      update ordloc
         set unit_cost = I_new_nic
       where order_no  = I_order_no
         and item      = I_item
         and location  = ordloc_rec.location;
   END LOOP;

   open C_GET_SHIP_INFO;
   fetch C_GET_SHIP_INFO bulk collect into shipment_tbl;
   close C_GET_SHIP_INFO;

   FOR i in shipment_tbl.FIRST..shipment_tbl.LAST LOOP
      update shipsku
         set unit_cost = I_new_nic
       where item = I_item
         and shipment = shipment_tbl(i).shipment;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FM_COST_ADJUSTMENT;
----------------------------------------------------------------------
FUNCTION FM_RECEIPT_MATCHED(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_ref_doc_no      IN       SHIPMENT.REF_DOC_NO%TYPE)
RETURN BOOLEAN IS
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);
   L_program             VARCHAR2(60) := 'REC_COST_ADJ_SQL.FM_RECEIPT_MATCHED';
   --
   cursor C_GET_SHIP_INFO is
      select sm.shipment
        from shipment sm
       where sm.order_no = I_order_no
         and sm.ref_doc_no = NVL(I_ref_doc_no, sm.ref_doc_no);
    --
   TYPE SHIPMENT_TBL_TYPE IS TABLE OF C_GET_SHIP_INFO%ROWTYPE INDEX BY BINARY_INTEGER;
   shipment_tbl SHIPMENT_TBL_TYPE;

BEGIN

   open C_GET_SHIP_INFO;
   fetch C_GET_SHIP_INFO bulk collect into shipment_tbl;
   close C_GET_SHIP_INFO;
   --
   FORALL i in shipment_tbl.first..shipment_tbl.last
       update shipment sm
          set invc_match_status = 'M'
        where sm.shipment = shipment_tbl(i).shipment;
   --
   FORALL i in shipment_tbl.first..shipment_tbl.last
       update shipsku ss
          set qty_matched = qty_received
        where ss.shipment = shipment_tbl(i).shipment;
   --
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',LP_table,
                                            TO_CHAR(I_order_no),(I_ref_doc_no));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END FM_RECEIPT_MATCHED;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_VIR_UNMATCHED_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_vir_qty         IN OUT   SHIPSKU.QTY_MATCHED%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE,
                               I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                               I_shipment        IN       SHIPSKU.SHIPMENT%TYPE,
                               I_seq_no          IN       SHIPSKU.SEQ_NO%TYPE,
                               I_phy_loc         IN       ORDLOC.LOCATION%TYPE,
                               I_vir_loc         IN       ORDLOC.LOCATION%TYPE,
                               I_phy_qty         IN       SHIPSKU.QTY_MATCHED%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'REC_COST_ADJ_SQL.GET_VIR_UNMATCHED_QTY';
   L_dist_table   DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   --> Table returned from distribution call with locations and quantities
   L_loc          SHIPMENT.TO_LOC%TYPE;

BEGIN
   ---

   O_vir_qty :=0;
   -- Call distribution package to get the distribution for the unmatched qty
   if DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                  L_dist_table,           -- O_dist_tab: results table returned by distribution
                                  I_item,                 -- L_item
                                  I_phy_loc,              -- I_loc: phy_loc
                                  I_phy_qty,              -- I_qty
                                  'SHIPMENT',             -- I_CMI: calling module indicator
                                  NULL,                  -- I_inv_status
                                  NULL,                  -- I_to_loc_type,
                                  NULL,                  -- I_to_loc,
                                  I_order_no,            -- I_order_no
                                  I_shipment,            -- I_shipment
                                  I_seq_no) = FALSE then -- I_seq_no
      return FALSE;
   end if;

   --- If no records returned on distribution table then error out
   if L_dist_table.count = 0  then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRIBUTION', NULL, NULL, NULL);
      return FALSE;
   end if;
   --
   for i in 1 .. L_dist_table.count LOOP
      --
      L_loc := L_dist_table(i).wh;
      if L_loc = I_vir_loc then
         O_vir_qty := O_vir_qty + L_dist_table(i).dist_qty;
      end if;
      --
   end loop;
   return TRUE;
   --
EXCEPTION
   when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
   return FALSE;
END GET_VIR_UNMATCHED_QTY;
-------------------------------------------------------------------------------------------
FUNCTION GET_PO_UNMATCHED_QTY (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_unmatched_qty        IN OUT   SHIPSKU.QTY_MATCHED%TYPE,
                               I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                               I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                               I_location             IN       ORDLOC.LOCATION%TYPE,
                               I_loc_type             IN       ORDLOC.LOC_TYPE%TYPE,
                               I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_program             VARCHAR2(64)   :=   'REC_COST_ADJ_SQL.GET_PO_UNMATCHED_QTY';
   L_physical_wh         WH.PHYSICAL_WH%TYPE;
   L_seq_no              SHIPSKU.SEQ_NO%TYPE;
   L_rowid               rowid;
   L_shipment            SHIPMENT.SHIPMENT%TYPE;
   --
   L_qty_unmatched       SHIPSKU.QTY_RECEIVED%TYPE;
   L_loc                 SHIPMENT.TO_LOC%TYPE;
   L_qty                 SHIPSKU.QTY_RECEIVED%TYPE;
   L_vir_qty             SHIPSKU.QTY_RECEIVED%TYPE;


   -- The I_adjust_matched_ind passed as 'N' means that
   -- there is no need to write adjustment for the matched receipt and if it is set to
   -- 'Y'  there is a need to write the adjustment for the matched receipt too.

   -- This cursor will fetch the unmatched receipt quantity when I_adjust_matched_ind is
   -- passed in as 'N' and will fetch the entire receipt quantity when it is passed in as 'Y' as
   -- the entire receipt qty including the matched qty needs to be considered.
   -- Since the qty_matched is only there in SHIPSKU, we need to get the unmatched qty from SHIPSKU
   -- and then use the DISTRIBUTION_SQL.DISTRIBUTE package to distribute the unmatched qty based on
   -- SHIPSKU_LOC receipt quantity for the virtual warehouses in multichannel environment.

   cursor C_GET_TOTAL_UNMATCHED_QTY is
      select sk.shipment,
             sk.seq_no,
             sk.qty_received - decode(I_adjust_matched_ind,'Y',0,NVL(sk.qty_matched,0)) qty_unmatched
        from shipsku  sk,
             shipment ss
       where sk.item     = I_item
         and sk.shipment = ss.shipment
         and ss.order_no = I_order_no
         and ((ss.to_loc_type = 'W' and ss.to_loc = L_physical_wh)
              or ss.to_loc = I_location )
         and ss.to_loc_type = I_loc_type
		                  and sk.qty_received is not null;

   cursor C_GET_PHYSICAL is
      select physical_wh
        from wh
       where wh = I_location;

BEGIN

   if I_loc_type = 'W' then
      SQL_LIB.SET_MARK('OPEN','C_GET_PHYSICAL','wh',NULL);
      open C_GET_PHYSICAL;
      SQL_LIB.SET_MARK('FETCH','C_GET_PHYSICAL','wh',NULL);
      fetch C_GET_PHYSICAL into L_physical_wh;
      SQL_LIB.SET_MARK('CLOSE','C_GET_PHYSICAL','wh',NULL);
      close C_GET_PHYSICAL;
   end if;

   L_qty := 0;
   for c_rec in c_get_total_unmatched_qty loop
      L_qty_unmatched := c_rec.qty_unmatched;
      L_shipment := c_rec.shipment;
      L_seq_no   := c_rec.seq_no;
      if (I_loc_type = 'W' and L_qty_unmatched > 0) then
         if GET_VIR_UNMATCHED_QTY(O_error_message,
                                  L_vir_qty,
                                  I_item,
                                  I_order_no,
                                  L_shipment,
                                  L_seq_no,
                                  L_physical_wh,
                                  I_location,
                                  L_qty_unmatched) = FALSE then
            return FALSE;
         end if;
         L_qty := L_qty + L_vir_qty;
      else
         L_qty := L_qty + L_qty_unmatched;
      end if; --  (I_loc_type = 'W')
   end loop;
   --
   O_unmatched_qty := L_qty;
   return TRUE;
   --
EXCEPTION
   when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
   return FALSE;
END GET_PO_UNMATCHED_QTY;
--------------------------------------------------------------------------------------
FUNCTION CALC_NEW_AV_COST (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_new_wac_loc           IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                           O_neg_soh_wac_adj_amt   IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                           O_adj_qty               IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_avg_cost_old          IN       ITEM_LOC_SOH.AV_COST%TYPE,
                           I_total_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_new_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                           I_old_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                           I_qty_matched           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_order_number          IN       ORDHEAD.ORDER_NO%TYPE   DEFAULT NULL,
                           I_location              IN       ORDLOC.LOCATION%TYPE,
                           I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE,
                           I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                           I_adjust_matched_ind    IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   L_program     varchar2(64) := 'REC_COST_ADJ_SQL.CALC_NEW_AV_COST';
BEGIN
   if ITEMLOC_UPDATE_SQL.REC_COST_ADJ_INVENTORY(O_error_message,
                                                O_new_wac_loc,
                                                O_neg_soh_wac_adj_amt,
                                                O_adj_qty,
                                                I_item,
                                                I_location,
                                                I_loc_type,
                                                I_new_cost,
                                                I_old_cost,
                                                I_qty_matched,
                                                'Y',
                                                I_order_number,
                                                NULL,
                                                NULL,
                                                L_program,
                                                'C',
                                                NULL,
                                                NULL,
                                                NULL,
                                                I_adjust_matched_ind,
                                                FALSE) = FALSE then
      return FALSE;
   end if;
   return TRUE;
   --
EXCEPTION
   when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
   return FALSE;
END CALC_NEW_AV_COST;
------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC_QTY_COST (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_avg_cost_old          IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                                O_on_hand               IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                O_comp_on_hand          IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                O_in_transit            IN OUT   ITEM_LOC_SOH.IN_TRANSIT_QTY%TYPE,
                                O_qty_unmatched         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                O_qty_expected          IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                O_local_currency        IN OUT   ORDHEAD.CURRENCY_CODE%TYPE,
                                O_pur_cost_old_loc      IN OUT   ORDLOC.UNIT_COST%TYPE,
                                O_landed_cost_old_loc   IN OUT   ORDLOC.UNIT_COST%TYPE,
                                O_physical_wh           IN OUT   INV_STATUS_QTY.LOCATION%TYPE,       
                                I_item                  IN       ORDSKU.ITEM%TYPE,
                                I_loc_type              IN       INV_STATUS_QTY.LOC_TYPE%TYPE,
                                I_location              IN       INV_STATUS_QTY.LOCATION%TYPE,
                                I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                                I_supplier              IN       SUPS.SUPPLIER%TYPE,
                                I_unit_cost             IN       ORDLOC.UNIT_COST%TYPE, 
                                I_pack_ind              IN       ITEM_MASTER.PACK_IND%TYPE,
                                I_prim_currency         IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                I_ord_currency          IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                I_exchange_rate         IN       ORDHEAD.EXCHANGE_RATE%TYPE, 
                                I_alc_status            IN       ALC_HEAD.STATUS%TYPE,
                                I_origin_country_id     IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                I_import_country_id     IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                                I_import_ind            IN       SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                                I_elc_ind               IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                                I_adj_matched_rcpt      IN       VARCHAR2 DEFAULT 'N')
return BOOLEAN IS
   L_program               VARCHAR2(64) := 'REC_COST_ADJ_SQL.GET_ITEM_LOC_QTY_COST';
   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_wh                    WH.WH%TYPE;
   L_expenses              ORDLOC.UNIT_COST%TYPE;
   L_total_elc             ORDLOC.UNIT_COST%TYPE;
   L_exp_currency          ORDLOC_EXP.COMP_CURRENCY%TYPE;
   L_exchange_rate_exp     ORDLOC_EXP.EXCHANGE_RATE%TYPE;
   L_duty                  ORDLOC.UNIT_COST%TYPE;
   L_duty_currency         ORDLOC_EXP.COMP_CURRENCY%TYPE;
   L_location              WH.WH%TYPE;
   L_stock_on_hand         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_pack_comp_soh         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_pack_comp_intran      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_tsf_reserved_qty      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_pack_comp_resv        ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_tsf_expected_qty      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_pack_comp_exp         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_rtv_qty               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_non_sellable_qty      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_customer_resv         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_customer_backorder    ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_pack_comp_cust_resv   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_pack_comp_cust_back   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_unmatched_qty         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
BEGIN
   
   --populate the current stock on hand
   if ITEMLOC_ATTRIB_SQL.GET_AV_COST_SOH(L_error_message,
                                         I_item,
                                         I_location,
                                         I_loc_type,
                                         O_avg_cost_old,
                                         O_on_hand,
                                         O_comp_on_hand) = FALSE then
      return FALSE;
   end if;
   
   -- Stock on hand field displays the sum of both stock on hand and comp on hand
   O_on_hand := (NVL(O_on_hand,0) + NVL(O_comp_on_hand,0));
   ---
   --populate the current stock in transit
   if ITEMLOC_QUANTITY_SQL.GET_ITEM_LOC_QTYS(L_error_message,
                                             L_stock_on_hand,
                                             L_pack_comp_soh,
                                             O_in_transit,
                                             L_pack_comp_intran,
                                             L_tsf_reserved_qty,
                                             L_pack_comp_resv,
                                             L_tsf_expected_qty,
                                             L_pack_comp_exp,
                                             L_rtv_qty,
                                             L_non_sellable_qty,
                                             L_customer_resv,
                                             L_customer_backorder,
                                             L_pack_comp_cust_resv,
                                             L_pack_comp_cust_back,
                                             I_item,
                                             I_location,
                                             I_loc_type) = FALSE then
      return FALSE;
   end if;

   if REC_COST_ADJ_SQL.GET_PO_UNMATCHED_QTY(L_error_message,
                                            L_unmatched_qty,
                                            I_item,
                                            I_order_no,
                                            I_location,
                                            I_loc_type,
                                            I_adj_matched_rcpt) = FALSE then
      return FALSE;
   end if;   

   if L_unmatched_qty < 0 then
      O_qty_unmatched := 0;
   else
      O_qty_unmatched := L_unmatched_qty;
   end if;

   O_qty_expected := O_in_transit + L_pack_comp_intran;

   --Get the local currency
   if CURRENCY_SQL.GET_CURR_LOC(L_error_message,
                                I_location,
                                I_loc_type,
                                NULL,
                                O_local_currency) = FALSE then
     return FALSE;
   end if;

   --Get the sku's old purchase cost in local currency
   if CURRENCY_SQL.CONVERT(L_error_message,
                           I_unit_cost, --:B_head.TI_cost_old_ord,
                           I_ord_currency,
                           O_local_currency,
                           O_pur_cost_old_loc,
                           'C',
                           NULL,
                           NULL,
                           I_exchange_rate,
                           NULL) = FALSE then
      return FALSE;
   end if;

   -- if (ELC is on and ALC is off) or
   --    (ALC is on and ALC has not been finalized)
   if (I_import_ind = 'N' and
       I_elc_ind = 'Y') or
      (I_import_ind = 'Y' and
       I_alc_status != 'PW') then
       if ELC_CALC_SQL.CALC_TOTALS(L_error_message,
                                   L_total_elc,
                                   L_expenses,
                                   L_exp_currency,
                                   L_exchange_rate_exp,
                                   L_duty,
                                   L_duty_currency,
                                   I_order_no,
                                   I_item,
                                   NULL,
                                   NULL,
                                   I_location,
                                   I_supplier,
                                   I_origin_country_id,
                                   I_import_country_id,
                                   I_unit_cost)= FALSE then
          return FALSE;
       end if;
       ---
       if CURRENCY_SQL.CONVERT(L_error_message,
                               L_total_elc,
                               I_prim_currency,
                               O_local_currency,
                               O_landed_cost_old_loc,
                               'C',
                               NULL,
                               NULL,
                               NULL,
                               NULL) = FALSE then
         return FALSE;
      end if; 
   elsif (I_import_ind = 'Y' and I_alc_status = 'PW') then
       ---- get elc total for finalized order  
      if REC_COST_ADJ_SQL.CALC_ELC(L_error_message,
                                   L_total_elc,
                                   I_order_no,
                                   I_item,
                                   NULL, 
                                   I_location, 
                                   I_supplier,
                                   I_origin_country_id,
                                   I_import_country_id,
                                   I_unit_cost) = FALSE then
         return FALSE;
      end if; 
      if CURRENCY_SQL.CONVERT(L_error_message,
                              L_total_elc,
                              I_prim_currency,
                              O_local_currency,
                              O_landed_cost_old_loc,
                              'C',
                              NULL,
                              NULL,
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   --- if displaying a pack, null out fields that don't apply
   if I_pack_ind = 'Y' then
      O_avg_cost_old          := NULL;
      O_on_hand               := NULL;
      O_in_transit            := NULL;
      O_landed_cost_old_loc   := NULL;
   end if;
   ---
   if I_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(L_error_message,
                                       O_physical_wh,
                                       I_location) = FALSE then
         return FALSE;
      end if;
      ---
   else
      O_physical_wh := -99;
   end if;
   ---
   if I_loc_type = 'W' then
      L_location := O_physical_wh;
   else
      L_location := I_location;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
   return FALSE;
END GET_ITEM_LOC_QTY_COST;
------------------------------------------------------------------------------------------------------------------
FUNCTION UPD_NEW_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_pc_new_loc          IN OUT   ORDLOC.UNIT_COST%TYPE,  
                      O_lc_new_loc          IN OUT   ORDLOC.UNIT_COST%TYPE,
                      I_item                IN       ORDSKU.ITEM%TYPE,
                      I_loc_type            IN       INV_STATUS_QTY.LOC_TYPE%TYPE,
                      I_location            IN       INV_STATUS_QTY.LOCATION%TYPE,
                      I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                      I_supplier            IN       SUPS.SUPPLIER%TYPE,
                      I_dept                IN       DEPS.DEPT%TYPE,
                      I_local_currency      IN       ORDHEAD.CURRENCY_CODE%TYPE,
                      I_order_currency      IN       ORDHEAD.CURRENCY_CODE%TYPE,
                      I_prim_currency       IN       ORDHEAD.CURRENCY_CODE%TYPE,
                      I_order_as_type       IN       ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                      I_new_cost_ord        IN       ORDLOC.UNIT_COST%TYPE, 
                      I_old_cost_ord        IN       ORDLOC.UNIT_COST%TYPE, 
                      I_pack_ind            IN       ITEM_MASTER.PACK_IND%TYPE,
                      I_pack_type           IN       ITEM_MASTER.PACK_TYPE%TYPE,
                      I_item_level          IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                      I_tran_level          IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                      I_exchange_rate       IN       ORDHEAD.EXCHANGE_RATE%TYPE, 
                      I_alc_status          IN       ALC_HEAD.STATUS%TYPE,
                      I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                      I_import_country_id   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                      I_import_ord_ind      IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                      I_import_ind          IN       SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                      I_elc_ind             IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                      I_qty_received        IN       V_ORDLOC.QTY_RECEIVED%TYPE,
                      I_physical_wh         IN       WH.WH%TYPE,
                      I_adj_matched_rcpt    IN       VARCHAR2 DEFAULT 'N')
return BOOLEAN IS
   L_program             VARCHAR2(64) := 'REC_COST_ADJ_SQL.UPD_NEW_COST';
   L_item                ITEM_MASTER.ITEM%TYPE;
   L_pack_item           ITEM_MASTER.ITEM%TYPE;
   L_total_elc           NUMBER;
   L_expenses            ORDLOC.UNIT_COST%TYPE;
   L_exp_currency        VARCHAR2(255);
   L_exchange_rate_exp   VARCHAR2(255);
   L_duty                VARCHAR2(255);
   L_duty_currency       VARCHAR2(255);
   L_amount              SUP_DATA.AMOUNT%TYPE;
   L_location            WH.WH%TYPE;

   cursor C_GET_PACK_ITEMS is
      select item,
             qty
        from v_packsku_qty
       where pack_no = I_item;

BEGIN
   if I_new_cost_ord is not NULL then
      -- Update Order Deals with new cost
      if I_loc_type = 'W' then
         L_location := I_physical_wh;
      else
         L_location := I_location;
      end if;
   
      if REC_COST_ADJ_SQL.UPDATE_DEALS(O_error_message,
                                       I_order_no,
                                       I_item,
                                       I_location,
                                       I_old_cost_ord,
                                       I_new_cost_ord,
                                       NULL,
                                       NULL) = FALSE then
         return FALSE;                           
      end if;
      
      if I_item_level = I_tran_level then
         if I_pack_ind = 'Y' then
            if I_pack_type = 'B' then
               -- Populate comp item_elc_temp table with 'old landed cost' for
               -- each component item in the buyer pack.
               FOR c_rec IN c_get_pack_items LOOP
                  if I_alc_status = 'PW' then
                     ---- get elc total for finalized order
                     if REC_COST_ADJ_SQL.CALC_ELC(O_error_message,
                                                  L_total_elc,
                                                  I_order_no,
                                                  I_item,
                                                  c_rec.item,   -- component item in pack
                                                  I_location,
                                                  I_supplier,
                                                  I_origin_country_id,
                                                  I_import_country_id,
                                                  NULL) = FALSE then
                      return FALSE;
                     end if;
                  else
                     if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                                 L_total_elc,
                                                 L_expenses,
                                                 L_exp_currency,
                                                 L_exchange_rate_exp,
                                                 L_duty,
                                                 L_duty_currency,
                                                 I_order_no,
                                                 I_item,
                                                 c_rec.item,   -- component item in pack
                                                 NULL,
                                                 I_location,
                                                 I_supplier,
                                                 I_origin_country_id,
                                                 I_import_country_id,
                                                 NULL) = FALSE then
                        return FALSE;
                     end if;
                  end if;
                  ---
                  if CURRENCY_SQL.CONVERT(O_error_message,
                                          L_total_elc,
                                          I_prim_currency,
                                          I_local_currency,
                                          L_total_elc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  insert into comp_item_elc_temp (order_no,
                                                  pack_item,
                                                  item,
                                                  location,
                                                  landed_cost)
                                          values (I_order_no,
                                                  I_item,
                                                  c_rec.item,
                                                  I_location,
                                                  L_total_elc);
               END LOOP;
            else  -- Pack is a Vendor Pack
               --- if order is finalized 
               if I_alc_status = 'PW' then
                  ---- get elc total for finalized order  
                  if REC_COST_ADJ_SQL.CALC_ELC(O_error_message,
                                               L_total_elc,
                                               I_order_no,
                                               I_item,
                                               NULL,   -- Component item in Pack not needed in Vendor Packs
                                               I_location, 
                                               I_supplier,
                                               I_origin_country_id,
                                               I_import_country_id,
                                               NULL) = FALSE then
                     return FALSE;
                   end if;
               else
                  -- Populate comp_item_elc_temp table with 'old landed cost' for Vendor Pack
                  if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                              L_total_elc,
                                              L_expenses,
                                              L_exp_currency,
                                              L_exchange_rate_exp,
                                              L_duty,
                                              L_duty_currency,
                                              I_order_no,
                                              I_item,
                                              NULL,   -- Component item in Pack not needed in Vendor Packs
                                              NULL,
                                              I_location,
                                              I_supplier,
                                              I_origin_country_id,
                                              I_import_country_id,
                                              NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
               ---
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_total_elc,
                                       I_prim_currency,
                                       I_local_currency,
                                       L_total_elc,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
                  return FALSE;
               end if;
               ---
               insert into comp_item_elc_temp (order_no,
                                               pack_item,
                                               item,
                                               location,
                                               landed_cost)
                                       values (I_order_no,
                                               I_item,
                                               NULL,
                                               I_location,
                                               L_total_elc);
            end if;
         end if;
         ---
         if REC_COST_ADJ_SQL.UPDATE_ORDER_COST(O_error_message,
                                               I_new_cost_ord,
                                               I_order_no,
                                               I_item,
                                               I_location,
                                               I_loc_type,
                                               I_adj_matched_rcpt) = FALSE then
           return FALSE;
         end if;
         ---
         -- if (ELC is on and ALC is off) or
         --    (ALC is on)
         if (I_import_ind = 'N' and
            I_elc_ind = 'Y') or
            (I_import_ind = 'Y') then
            -- If the item is a Buyer Pack with an Order As Type of Pack
            -- The Pack Number must be passed in the Pack Item Parameter
            if I_pack_type = 'B' and I_order_as_type = 'P' then
               L_item      := NULL;
               L_pack_item := I_item;
            else
               L_item      := I_item;
               L_pack_item := NULL;
            end if;
            ---
            -- re-calculate ELC based on new cost
            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PE',   -- 'Purchase Order Expenses'
                                      L_item,   -- comp sku if item is a buyer pack
                                      NULL,
                                      NULL,
                                      NULL,
                                      I_order_no,
                                      NULL,
                                      L_pack_item,   -- pack number if item is a buyer pack otherwise null
                                      NULL,
                                      NULL,
                                      I_import_country_id,
                                      I_origin_country_id,
                                      NULL,
                                      NULL) = FALSE then
               return FALSE;
            end if;
            if I_import_ord_ind = 'Y' then
               if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                         'PA',   -- 'Purchase Order Assessments'
                                         L_item,
                                         NULL,
                                         NULL,
                                         NULL,
                                         I_order_no,
                                         NULL,
                                         L_pack_item,
                                         NULL,
                                         NULL,
                                         I_import_country_id,
                                         I_origin_country_id,
                                         NULL,
                                         NULL) = FALSE then
                  return FALSE;
               end if;
               if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                         'PE',   -- 'Purchase Order Expenses'
                                         L_item,   -- comp sku if item is a buyer pack
                                         NULL,
                                         NULL,
                                         NULL,
                                         I_order_no,
                                         NULL,
                                         L_pack_item,   -- pack number if item is a buyer pack otherwise null
                                         NULL,
                                         NULL,
                                         I_import_country_id,
                                         I_origin_country_id,
                                         NULL,
                                         NULL) = FALSE then
                  return FALSE;
               end if;
            end if; /* import ind is Y */
         end if; /* multiple inds */
         
         --get the item's new purchase cost in local currency
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 I_new_cost_ord,
                                 I_order_currency,
                                 I_local_currency,
                                 O_pc_new_loc,
                                 'C',
                                 NULL,
                                 NULL,
                                 I_exchange_rate,
                                 NULL) = FALSE then
               return FALSE;
         end if;
            
         -- if (ELC is on and ALC is off) or
         --    (ALC is on and ALC has not been finalized)
         if I_pack_ind = 'N' then
            if (I_import_ind = 'N' and
               I_elc_ind = 'Y') or
               (I_import_ind = 'Y' and
               I_alc_status != 'PW') then
               -- get elc total for new cost
                  if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                              L_total_elc,
                                              L_expenses,
                                              L_exp_currency,
                                              L_exchange_rate_exp,
                                              L_duty,
                                              L_duty_currency,
                                              I_order_no,
                                              I_item,
                                              NULL,
                                              NULL,
                                              I_location,
                                              I_supplier,
                                              I_origin_country_id,
                                              I_import_country_id,
                                              I_new_cost_ord) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  if CURRENCY_SQL.CONVERT(O_error_message,
                                          L_total_elc,
                                          I_prim_currency,
                                          I_local_currency,
                                          O_lc_new_loc,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
                     return FALSE;
                  end if;
                  ---
            elsif (I_import_ind = 'Y' and I_alc_status = 'PW') then
                  ---- get elc total for finalized order
               if REC_COST_ADJ_SQL.CALC_ELC(O_error_message,
                                            L_total_elc,
                                            I_order_no,
                                            I_item,
                                            NULL,
                                            I_location,
                                            I_supplier,
                                            I_origin_country_id,
                                            I_import_country_id,
                                            I_new_cost_ord) = FALSE then
                  return FALSE;
               end if;
               
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_total_elc,
                                       I_prim_currency,
                                       I_local_currency,
                                       O_lc_new_loc,
                                       'C',
                                        NULL,
                                        NULL) = FALSE then
                  return FALSE;
               end if;
               ---
            end if;
         end if;
      end if; /* item_level = Tran_level */
   else
      O_pc_new_loc := NULL;
      O_lc_new_loc:= NULL;
   end if; /* if cost is not NULL */
   
   if I_pack_ind = 'Y' then
      if REC_COST_ADJ_SQL.UPD_PACK_ITEM(O_error_message,
                                        I_item,
                                        get_vdate,
                                        I_order_no,
                                        I_old_cost_ord,
                                        I_new_cost_ord,
                                        I_supplier,
                                        I_origin_country_id,
                                        I_import_country_id,
                                        I_exchange_rate,
                                        I_prim_currency,
                                        I_order_currency,
                                        I_alc_status,
                                        I_import_ord_ind,
                                        I_import_ind,
                                        I_elc_ind,
                                        I_location,
                                        I_loc_type,
                                        I_adj_matched_rcpt) = FALSE then
         return FALSE;
      end if;
   end if;
   -- update sup_data
   if CURRENCY_SQL.CONVERT(O_error_message,
                           I_new_cost_ord - I_old_cost_ord,
                           I_order_currency,
                           I_prim_currency,
                           L_amount,
                           'C',
                           NULL,
                           NULL,
                           I_exchange_rate,
                           NULL) = FALSE then
      return FALSE;
   end if;
   
   L_amount := I_qty_received * L_amount;
   
   if REC_COST_ADJ_SQL.UPDATE_SUP_DATA(O_error_message,
                                       I_dept,
                                       I_supplier,
                                       get_vdate,
                                       1,  -- tran_type,
                                       L_amount) = FALSE then
      return FALSE;
   end if;
      
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END UPD_NEW_COST;
--------------------------------------------------------------------------------------
FUNCTION CREATE_SUPP_COST_CHANGE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item               IN       ORDSKU.ITEM%TYPE,
                                 I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                                 I_supplier           IN       SUPS.SUPPLIER%TYPE,
                                 I_new_cost_ord       IN       ORDLOC.UNIT_COST%TYPE,
                                 I_order_currency     IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                 I_local_currency     IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                 I_exchange_rate      IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                                 I_loc_type           IN       INV_STATUS_QTY.LOC_TYPE%TYPE,
                                 I_location           IN       INV_STATUS_QTY.LOCATION%TYPE,
                                 I_physical_wh        IN       INV_STATUS_QTY.LOCATION%TYPE,
                                 I_apply_to_all_loc   IN       VARCHAR2 DEFAULT 'N')
return BOOLEAN IS
   --  Function that will either perform a cost change for one location
   --  or perform a cost change for all locations on the order. 
   L_location                WH.WH%TYPE;
   L_exists                  BOOLEAN;
   L_conflicts               BOOLEAN := FALSE;
   L_cost_change             COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE;
   L_chg_amt_loc             COST_CHANGE_LOC_TEMP.UNIT_COST_NEW%TYPE;
   L_chg_amt_sup             COST_CHANGE_LOC_TEMP.UNIT_COST_NEW%TYPE;
   L_ordsku                  ORDSKU%ROWTYPE;
   L_vdate                   TRAN_DATA.TRAN_DATE%TYPE;
   L_return_code             VARCHAR2(10);
   L_sups                    SUPS%ROWTYPE;
   L_conflict_apply_all      VARCHAR2(1) := 'N';
   L_program                 VARCHAR2(64) := 'REC_COST_ADJ_SQL.CREATE_SUPP_COST_CHANGE';
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_cost_changes_rec        OBJ_CC_COST_EVENT_REC;
   L_cost_changes_tbl        OBJ_CC_COST_EVENT_TBL;
   
BEGIN
   L_vdate := get_vdate+1;
   ---
   if ORDER_ATTRIB_SQL.GET_ORDSKU_ROW(O_error_message, 
                                      L_exists,
                                      L_ordsku,
                                      I_order_no,
                                      I_item) = FALSE then
      return FALSE;
   end if;   
   ---
   if SUPP_ATTRIB_SQL.GET_SUPS(O_error_message, 
                               L_sups,
                               I_supplier) = FALSE then
      return FALSE;
   end if;   

   if CURRENCY_SQL.CONVERT(O_error_message,
                           I_new_cost_ord,
                           I_order_currency,
                           L_sups.currency_code,
                           L_chg_amt_sup,
                           'C',
                           NULL,
                           NULL,
                           NULL,
                           NULL) = FALSE then 
       return FALSE;
   end if;  
   ---
   if CURRENCY_SQL.CONVERT(O_error_message,
                           I_new_cost_ord,
                           I_order_currency,
                           I_local_currency,
                           L_chg_amt_loc,
                           'C',
                           NULL,
                           NULL,
                           I_exchange_rate,
                           NULL) = FALSE then 
       return FALSE;
   end if;  
   ---
   if I_apply_to_all_loc = 'N' then
      --Verify that there are no existing cost change scheduled to come into effect on the same day.
      if COST_CHANGE_SQL.COST_CHANGE_EXISTS(O_error_message,
                                            L_conflicts,
                                            I_supplier,
                                            I_item,
                                            I_location,
                                            L_vdate) = FALSE then
         return FALSE;
      end if;      
   end if;

   if L_conflicts = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('COST_CHANGE_EXISTS', NULL, NULL, NULL);
      return FALSE;
   else
      ---
      NEXT_COSTCHG_NUMBER(L_cost_change,
                          L_return_code,
                          O_error_message);
                        
      if L_return_code = 'FALSE' then
         return FALSE;
      end if;
      ---
      savepoint apply_all_locs;
      if COST_CHANGE_SQL.CREATE_RCA_COST_CHG(O_error_message,
                                             L_cost_change,
                                             8, --reason
                                             L_vdate,
                                             'A',
                                             'SKU',
                                             get_vdate,
                                             user,
                                             get_vdate,
                                             user) = FALSE then

         return FALSE;
      end if;
      if I_apply_to_all_loc = 'N' then     	  
         --Insert values into the temporary tables
         if COST_CHANGE_SQL.POP_TEMP_DETAIL_LOC(O_error_message,
                                                L_exists,
                                                'NEW',
                                                L_cost_change,
                                                I_supplier,
                                                L_ordsku.origin_country_id,
                                                I_Item,
                                                99, --Reason code should not be 1, 2 or 3
                                                NULL) = FALSE then
            return FALSE;
         end if;
     
         if I_loc_type = 'W' then
            L_location := I_physical_wh;
         else
            L_location := I_location;
         end if;              
         if COST_CHANGE_SQL.APPLY_CHANGE_LOC(O_error_message,  
                                             I_supplier,
                                             L_ordsku.origin_country_id,
                                             I_Item,
                                             I_loc_type,
                                             L_location,
                                             NULL,
                                             'F',
                                             L_chg_amt_sup) = FALSE then
            return FALSE;
         end if;      

         if COST_CHANGE_SQL.INSERT_UPDATE_COST_CHANGE(O_error_message,  
                                                      L_cost_change) = FALSE then
            return FALSE;
         end if;
         if COST_CHANGE_SQL.DELETE_COST_CHANGE_LOC_TEMP(O_error_message,  
                                                        L_cost_change) = FALSE then
            return FALSE;
         end if;
         ---
      else
         if COST_CHANGE_SQL.APPLY_TO_ALL_LOCS(O_error_message,
                                              L_conflict_apply_all,
                                              I_order_no,
                                              I_item,
                                              NULL,
                                              L_chg_amt_sup,
                                              L_cost_change) = FALSE then
            rollback to savepoint apply_all_locs;
            return FALSE;
         end if;
         
         if L_conflict_apply_all = 'Y' then
            rollback to savepoint apply_all_locs;
            O_error_message := SQL_LIB.CREATE_MSG('COST_CHANGE_EXISTS', NULL, NULL, NULL);
         end if;
         
      end if;
      ---
      if L_conflict_apply_all = 'N' then
         L_cost_changes_tbl := new OBJ_CC_COST_EVENT_TBL();
         L_cost_changes_rec := new OBJ_CC_COST_EVENT_REC(L_cost_change, 'N'); --Cost Change records will not be sourced from the temp tables.
         L_cost_changes_tbl.EXTEND;
         L_cost_changes_tbl(L_cost_changes_tbl.COUNT) := L_cost_changes_rec;
         if FUTURE_COST_EVENT_SQL.ADD_COST_CHANGE_EVENT(O_error_message,
                                                        L_cost_event_process_id,
                                                        'ADD',
                                                        L_cost_changes_tbl,
                                                        'Y',
                                                        user) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_SUPP_COST_CHANGE;
--------------------------------------------------------------------------------------
FUNCTION PURGE_COMP_ITEM_ELC_TEMP( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
return BOOLEAN IS

   L_program       VARCHAR2(64) := 'REC_COST_ADJ_SQL.PURGE_COMP_ITEM_ELC_TEMP';

BEGIN
   ---
   delete from comp_item_elc_temp;
   ---
   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PURGE_COMP_ITEM_ELC_TEMP;
--------------------------------------------------------------------------------------
END REC_COST_ADJ_SQL;
/