CREATE OR REPLACE PACKAGE BODY RTV_VALIDATE_SQL AS
-------------------------------------------------------------------
FUNCTION EXIST(O_error_message  IN OUT  VARCHAR2,
               I_rtv            IN      RTV_HEAD.RTV_ORDER_NO%TYPE,
               O_exist          IN OUT  BOOLEAN)
   RETURN BOOLEAN IS

   L_dummy   VARCHAR2(1);

   cursor C_EXIST is
      select 'x'
        from rtv_head
       where rtv_order_no = I_rtv;
BEGIN
   open C_EXIST;
   fetch C_EXIST into L_dummy;
   if C_EXIST%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_RTV',
                                            NULL,
                                            NULL,   
                                            NULL);
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   close C_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RTV_VALIDATE_SQL.EXIST',
                                             to_char(SQLCODE));

   return FALSE;

END EXIST;
---------------------------------------------------------------------------
FUNCTION RTV_EXISTS_FOR_SUPP(O_error_message IN OUT VARCHAR2,
                             O_rtv_exists    IN OUT BOOLEAN,
                             I_supplier      IN     RTV_HEAD.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64)   := 'RTV_VALIDATE_SQL.RTV_EXISTS_FOR_SUPP';
   L_dummy    VARCHAR2(1)    := 'N';

   cursor C_SUPP_RTV_EXIST is
      select 'Y'
        from rtv_head
       where supplier = I_supplier
         and status_ind in ('05','10','12');

BEGIN
   if  I_supplier IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   open  C_SUPP_RTV_EXIST;
   fetch C_SUPP_RTV_EXIST into L_dummy;
   close C_SUPP_RTV_EXIST;

   -- If the cursor finds a record then L_dummy will be equal to 'Y'. 
   -- That causes the statement (L_dummy = 'Y') to evaluate to 
   -- the BOOLEAN value TRUE ('Y' = 'Y'). Thus, O_rtv_exists = TRUE.

   O_rtv_exists := (L_dummy = 'Y');
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RTV_EXISTS_FOR_SUPP;
-------------------------------------------------------------------
FUNCTION SHIPMENT_VALID(O_error_message  IN OUT   VARCHAR2,
                        I_shipment       IN       SHIPMENT.SHIPMENT%TYPE,
                        I_supplier       IN       ORDHEAD.SUPPLIER%TYPE,
                        I_location       IN       SHIPMENT.TO_LOC%TYPE,
                        O_valid          IN OUT   BOOLEAN)
                        RETURN BOOLEAN IS

   L_program  VARCHAR2(64)   := 'RTV_VALIDATE_SQL.SHIPMENT_VALID';
   L_dummy    VARCHAR2(1);

   cursor C_VALID is
      select 'x'
        from shipment s, 
             ordhead  o
       where s.shipment  = I_shipment
         and s.to_loc    = I_location
         and s.order_no  = o.order_no 
         and o.supplier  = I_supplier;    

BEGIN

   open C_VALID;
   fetch C_VALID into L_dummy;
   if C_VALID%NOTFOUND then
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SHIP_SUP_LOC',
                                            NULL,
                                            NULL,
                                            NULL);
   else
      O_valid := TRUE;
   end if;
   close C_VALID;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SHIPMENT_VALID;

-------------------------------------------------------------------
FUNCTION INVENTORY_ITEM(O_error_message  IN OUT  VARCHAR2,
                        I_item           IN      ITEM_MASTER.ITEM%TYPE,
                        I_supplier       IN      ITEM_SUPPLIER.SUPPLIER%TYPE,
                        I_store          IN      STORE.STORE%TYPE,
                        I_wh             IN      WH.WH%TYPE,
                        O_valid          IN OUT  BOOLEAN)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'RTV_VALIDATE_SQL.INVENTORY_ITEM';
   L_dummy    VARCHAR2(1);

   cursor C_WH is
      select 'x'
        from item_loc      il, 
             item_supplier sk 
       where il.item       = sk.item
         and sk.supplier   = I_supplier 
         and il.item       = I_item
         and il.loc        = I_wh;
   cursor C_STORE is
      select 'x'
        from item_loc      il, 
             item_supplier sk 
       where il.item       = sk.item
         and sk.supplier   = I_supplier 
         and il.item       = I_item
         and il.loc        = I_store;

BEGIN
   if I_wh = -1 then
      open C_STORE;
      fetch C_STORE into L_dummy;
      if C_STORE%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('CANT_RETURN_SKU',
                                               I_item,
                                               NULL,
                                               NULL);
         O_valid := FALSE;
      else
         O_valid := TRUE;
      end if;
      close C_STORE;
   else
      open C_WH;
      fetch C_WH into L_dummy;
      if C_WH%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('CANT_RETURN_SKU',
                                               I_item,
                                               NULL,
                                               NULL);
         O_valid := FALSE;
      else
         O_valid := TRUE;
      end if;
      close C_WH;   
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));

   return FALSE;

END INVENTORY_ITEM;
-------------------------------------------------------------------
FUNCTION DETAILS_EXIST(O_error_message     IN OUT     VARCHAR2,
                       O_details_exist     IN OUT     BOOLEAN,
                       I_rtv_order_no      IN         RTV_HEAD.RTV_ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_exists    VARCHAR2(1);
   L_program   VARCHAR2(50) := 'RTV_VALIDATE_SQL.DETAILS_EXIST';

cursor C_DETAILS_EXIST is
   select 'x' 
     from rtv_detail
    where rtv_order_no = I_rtv_order_no;

BEGIN
   if  I_rtv_order_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_rtv_order_no',
                                            L_program, NULL);
      return FALSE;
   end if;

   open C_DETAILS_EXIST;
   fetch C_DETAILS_EXIST into L_exists;
   if C_DETAILS_EXIST%NOTFOUND then
      O_details_exist := FALSE;
   else
      O_details_exist := TRUE;
   end if;

   close C_DETAILS_EXIST;

   return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DETAILS_EXIST;
-------------------------------------------------------------------------------
FUNCTION VALID_RTV_QTY(O_error_message     IN OUT     VARCHAR2,
                       I_rtv_order_no      IN         RTV_HEAD.RTV_ORDER_NO%TYPE,
                       I_loc               IN         ITEM_LOC.LOC%TYPE,
                       I_loc_type          IN         ITEM_LOC.LOC_TYPE%TYPE,
                       I_uot_size          IN         ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE)
   RETURN BOOLEAN IS

L_program                   VARCHAR2(50) := 'RTV_VALIDATE_SQL.VALID_RTV_QTY';
L_item                      RTV_DETAIL.ITEM%TYPE;
L_prev_item                 RTV_DETAIL.ITEM%TYPE                := NULL;
L_qty_requested             RTV_DETAIL.QTY_REQUESTED%TYPE;
L_reason                    RTV_DETAIL.REASON%TYPE;
L_prev_reason               RTV_DETAIL.REASON%TYPE              := NULL;
L_physical_wh_ind           VARCHAR2(1)                         := 'N';
L_check_ind                 BOOLEAN                             := FALSE;

cursor C_GET_RTV_DETAILS is
   select rd.item,
          rd.qty_requested,
          rd.reason
     from rtv_detail rd,
          item_master im
    where rtv_order_no = I_rtv_order_no
      and rd.item = im.item
      and NVL(im.deposit_item_type, 'N') != 'A';

cursor C_GET_TOTAL_REQUESTED is
   select SUM(qty_requested)
     from rtv_detail
    where rtv_order_no = I_rtv_order_no
      and item = L_item
      and reason = 'O';

cursor C_GET_TOTAL_REQUESTED_UNAVAIL is
   select SUM(qty_requested)
     from rtv_detail
    where rtv_order_no = I_rtv_order_no
      and item = L_item
      and reason = 'U';

BEGIN
   if I_rtv_order_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_rtv_order_no',
                                            L_program, NULL); 
      return FALSE;
   end if;

   if I_loc IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_loc',
                                            L_program, NULL);
      return FALSE;
   end if;

   if  I_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_loc_type',
                                            L_program, NULL); 
      return FALSE;
   end if;

   if I_loc_type = 'W' then
      if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                 L_check_ind,
                                 I_loc) = FALSE then
         return FALSE;
      end if;
      if L_check_ind = TRUE then
         L_physical_wh_ind := 'Y';
      end if;
   end if;
   
   FOR rec in C_GET_RTV_DETAILS LOOP
      L_item := rec.item;
      L_qty_requested := rec.qty_requested;
      L_reason := rec.reason;

      if L_prev_item is NULL or
         L_prev_reason is NULL or
         L_prev_item != L_item or
         L_prev_reason != L_reason then
         
         if L_reason = 'U' then
            -- Validation needs to be done against the total unavailable quantity for the item.
            open C_GET_TOTAL_REQUESTED_UNAVAIL;
            fetch C_GET_TOTAL_REQUESTED_UNAVAIL into L_qty_requested;
            close C_GET_TOTAL_REQUESTED_UNAVAIL;
         else
            -- Validation needs to be done against the total available quantity for the item.
            open C_GET_TOTAL_REQUESTED;
            fetch C_GET_TOTAL_REQUESTED into L_qty_requested;
            close C_GET_TOTAL_REQUESTED;
         end if;
         
         if RTV_VALIDATE_SQL.VALID_RTV_ITEM_QTY(O_error_message,
                                                L_qty_requested,
                                                L_item,
                                                I_loc,
                                                I_loc_type,
                                                L_physical_wh_ind,
                                                L_reason,
                                                'N',
                                                1) = FALSE then
            return FALSE;
         end if;
         L_prev_item := L_item;
         L_prev_reason := L_reason;
      end if;
   END LOOP;

return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      return FALSE;

END VALID_RTV_QTY;
-------------------------------------------------------------------------------
FUNCTION GET_TOTAL_RTV_QTYS(O_error_message           IN OUT     VARCHAR2,
                            O_total_available_rtv     IN OUT     RTV_DETAIL.QTY_RETURNED%TYPE,
                            O_total_unavailable_rtv   IN OUT     RTV_DETAIL.QTY_RETURNED%TYPE,
                            I_item                    IN         RTV_DETAIL.ITEM%TYPE,
                            I_loc                     IN         ITEM_LOC.LOC%TYPE)

   RETURN BOOLEAN IS


cursor C_GET_TOTAL_RTV_QTY is
   select sum (decode(rd.reason, 'U', rd.qty_requested, 0)),
          sum (decode(rd.reason, 'U', 0, rd.qty_requested))
     from rtv_detail rd,
          rtv_head rh
    where rd.rtv_order_no = rh.rtv_order_no
      and rd.item       = I_item
      and rh.status_ind in ('10','12')
      and (rh.wh        = I_loc
       or  rh.store     = I_loc);

BEGIN

      open  C_GET_TOTAL_RTV_QTY;
      fetch C_GET_TOTAL_RTV_QTY into O_total_unavailable_rtv, O_total_available_rtv;
      close C_GET_TOTAL_RTV_QTY;

      if O_total_available_rtv is NULL then
         O_total_available_rtv := 0;
      end if;

      if O_total_unavailable_rtv is NULL then
         O_total_unavailable_rtv := 0;
      end if;

return TRUE;
EXCEPTION 
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            'RTV_VALIDATE_SQL.GET_TOTAL_RTV_QTYS', 
                                            to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_RTV_QTYS;
--------------------------------------------------------------------------------------
FUNCTION VALID_RTV_ITEM_QTY(O_error_message           IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_qty_requested          IN OUT     RTV_DETAIL.QTY_REQUESTED%TYPE,
                            I_item                    IN         RTV_DETAIL.ITEM%TYPE,
                            I_location                IN         ITEM_LOC.LOC%TYPE,
                            I_loc_type                IN         ITEM_LOC.LOC_TYPE%TYPE,
                            I_physical_wh_ind         IN         VARCHAR2,
                            I_reason                  IN         RTV_DETAIL.REASON%TYPE,
                            I_default_soh_ind         IN         VARCHAR2,
                            I_uot_size                IN         ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE)
RETURN BOOLEAN IS

   L_available_qty               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE     := 0;
   L_unavailable_qty             ITEM_LOC_SOH.STOCK_ON_HAND%TYPE     := 0;
   L_total_available_qty         RTV_DETAIL.QTY_REQUESTED%TYPE       := 0;
   L_total_unavailable_qty       RTV_DETAIL.QTY_REQUESTED%TYPE       := 0;
   L_tsf_reserved_qty            ITEM_LOC_SOH.NON_SELLABLE_QTY%TYPE;
   L_customer_resv               ITEM_LOC_SOH.NON_SELLABLE_QTY%TYPE; 
   L_non_sellable_qty            ITEM_LOC_SOH.NON_SELLABLE_QTY%TYPE;
   L_rtv_qty                     ITEM_LOC_SOH.RTV_QTY%TYPE;
   L_dummy                       VARCHAR2(255);
   L_group_type                  CODE_DETAIL.CODE%TYPE;
   L_cust_ord_resv_qty           ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_program                     VARCHAR2(50) := 'RTV_VALIDATE_SQL.VALID_RTV_ITEM_QTY';

   CURSOR C_INV_ST_QTY IS
      SELECT NVL(qty,0) cust_ord_resv_qty
        FROM inv_status_qty
       WHERE loc_type = I_loc_type
         AND location = I_location
         AND item = I_item
         AND inv_status = 2;

BEGIN
   if I_item IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_item',
                                             L_program, NULL);
      return FALSE;
   end if;

   if I_location IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_location',
                                             L_program, NULL);
      return FALSE;
   end if;

   if I_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_loc_type',
                                             L_program, NULL);
      return FALSE;
   end if;

   if I_physical_wh_ind IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_physical_wh_ind',
                                             L_program, NULL);
      return FALSE;
   end if;

   if I_reason IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_reason',
                                             L_program, NULL);
      return FALSE;
   end if;

   if I_default_soh_ind IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_default_soh_ind',
                                             L_program, NULL);
      return FALSE;
   end if;

   if RTV_VALIDATE_SQL.GET_TOTAL_RTV_QTYS(O_error_message,
                                          L_total_available_qty,
                                          L_total_unavailable_qty,
                                          I_item,
                                          I_location) = FALSE then
      return FALSE;
   end if;

   if I_physical_wh_ind = 'Y' then
      L_group_type := 'PW';
      if ITEMLOC_QUANTITY_SQL.GET_ITEM_GROUP_QTYS(O_error_message,
                                                  L_available_qty, --O_stock_on_hand,
                                                  L_dummy, --O_pack_comp_soh,
                                                  L_dummy, --O_in_transit_qty,
                                                  L_dummy, --O_pack_comp_intran,
                                                  L_tsf_reserved_qty, --O_tsf_reserved_qty,
                                                  L_dummy, --O_pack_comp_resv,
                                                  L_dummy, --O_tsf_expected_qty,
                                                  L_dummy, --O_pack_comp_exp,
                                                  L_rtv_qty, --O_rtv_qty,
                                                  L_non_sellable_qty, --O_non_sellable_qty
                                                  L_customer_resv, --O_customer_resv,
                                                  L_dummy, --O_customer_backorder,
                                                  L_dummy, --O_pack_comp_cust_resv,
                                                  L_dummy, --O_pack_comp_cust_back,
                                                  I_item,
                                                  I_location, --I_group_id
                                                  L_group_type) = FALSE then
         return FALSE;
      end if;
   else
      if ITEMLOC_QUANTITY_SQL.GET_ITEM_LOC_QTYS(O_error_message,
                                                L_available_qty, --O_stock_on_hand,
                                                L_dummy, --O_pack_comp_soh,
                                                L_dummy, --O_in_transit_qty,
                                                L_dummy, --O_pack_comp_intran,
                                                L_tsf_reserved_qty, --O_tsf_reserved_qty,
                                                L_dummy, --O_pack_comp_resv,
                                                L_dummy, --O_tsf_expected_qty,
                                                L_dummy, --O_pack_comp_exp,
                                                L_rtv_qty, --O_rtv_qty,
                                                L_non_sellable_qty,
                                                L_customer_resv, --O_customer_resv,
                                                L_dummy, --O_customer_backorder,
                                                L_dummy, --O_pack_comp_cust_resv,
                                                L_dummy, --O_pack_comp_cust_back,
                                                I_item,
                                                I_location,
                                                I_loc_type) = FALSE then
         return FALSE;
      end if;         
   end if;

   if I_loc_type = 'S' then

      SQL_LIB.SET_MARK('OPEN', 'C_INV_ST_QTY', 'INV_STATUS_QTY','Item: ' ||I_item);
      open C_INV_ST_QTY;

      SQL_LIB.SET_MARK('FETCH', 'C_INV_ST_QTY', 'INV_STATUS_QTY','Item: ' ||I_item);
      fetch C_INV_ST_QTY into L_cust_ord_resv_qty;

      L_non_sellable_qty := GREATEST(L_non_sellable_qty, 0) - GREATEST(L_cust_ord_resv_qty, 0);
      SQL_LIB.SET_MARK('CLOSE', 'C_INV_ST_QTY', 'INV_STATUS_QTY','Item: ' ||I_item);
      close C_INV_ST_QTY;
   end if;
   
   L_available_qty := GREATEST(L_available_qty,0) - (GREATEST(L_tsf_reserved_qty, 0) + GREATEST(L_total_available_qty, 0) + GREATEST(L_non_sellable_qty, 0) + GREATEST(L_customer_resv, 0));
   L_unavailable_qty := (GREATEST(L_non_sellable_qty, 0) + GREATEST(L_rtv_qty,0) - GREATEST(L_total_unavailable_qty, 0));
      
   if I_default_soh_ind = 'Y' then
      if I_reason = 'U' then
         if L_unavailable_qty <= 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_UNAVAIL_ITEM', I_item, NULL, NULL);
            return FALSE;
         else
            IO_qty_requested := round((L_unavailable_qty/I_uot_size),4);
         end if;
      elsif I_reason != 'U' then
         if L_available_qty <= 0 then
            O_error_message := SQL_LIB.CREATE_MSG('RTV_NOT_APPR_ZERO', I_item, NULL, NULL);
            return FALSE;
         else
            IO_qty_requested := round((L_available_qty/I_uot_size),4);
         end if;
      end if;

   elsif I_default_soh_ind = 'N' then

      if I_reason = 'U' then
         if L_unavailable_qty < round((IO_qty_requested * I_uot_size),4) then
            O_error_message := SQL_LIB.CREATE_MSG('GREATER_THAN_UNAVAILABLE',
                                                  round((L_unavailable_qty/I_uot_size),4),
                                                  I_item, NULL);
            return FALSE;
         else
            return TRUE;
         end if;
      elsif I_reason != 'U' then
         if L_available_qty < round((IO_qty_requested * I_uot_size),4) then
            O_error_message := SQL_LIB.CREATE_MSG('RTV_GREATER_STOCK_ONHAND', round((L_available_qty/I_uot_size),4), NULL, NULL);
            return FALSE;
         else
            return TRUE;
         end if;
      end if;
   end if;

return TRUE;

EXCEPTION 
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      return FALSE;

END VALID_RTV_ITEM_QTY;
-------------------------------------------------------------------------------
FUNCTION VALID_INV_STATUS_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_qty_requested   IN       RTV_DETAIL.QTY_REQUESTED%TYPE,
                              I_item            IN       RTV_DETAIL.ITEM%TYPE,
                              I_location        IN       ITEM_LOC.LOC%TYPE,
                              I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                              I_inv_status      IN       INV_STATUS_TYPES.INV_STATUS%TYPE)
RETURN BOOLEAN IS

   L_inv_status_qty  INV_STATUS_QTY.QTY%TYPE;
   L_program         VARCHAR2(50)  := 'RTV_VALIDATE_SQL.VALID_INV_STATUS_QTY';

   cursor C_GET_INV_STATUS_QTY is
      select isq.qty
        from inv_status_qty isq,
             inv_status_types ist
       where isq.inv_status = ist.inv_status
         and isq.item = I_item
         and isq.location = I_location
         and isq.loc_type = I_loc_type
         and isq.inv_status = I_inv_status;

BEGIN
   if I_item IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_item',
                                             L_program, NULL); 
      return FALSE;
   end if;

   if I_location IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_location',
                                             L_program, NULL);
      return FALSE;
   end if;

   if I_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_loc_type',
                                             L_program, NULL);
      return FALSE;
   end if;

   if I_inv_status IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_inv_status',
                                             L_program, NULL);
      return FALSE;
   end if;

   open C_GET_INV_STATUS_QTY;

   fetch C_GET_INV_STATUS_QTY into L_inv_status_qty;
   
   close C_GET_INV_STATUS_QTY;

   if L_inv_status_qty is NULL  then
      L_inv_status_qty := 0;
      O_error_message := SQL_LIB.CREATE_MSG('GREATER_THAN_UNAVAILABLE',
                                            (L_inv_status_qty),
                                            I_item, 
                                            NULL);
      return FALSE;
   end if;

   if L_inv_status_qty < I_qty_requested or
      L_inv_status_qty <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('GREATER_THAN_UNAVAILABLE',
                                            (L_inv_status_qty),
                                            I_item, 
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALID_INV_STATUS_QTY;
-------------------------------------------------------------------------------
END RTV_VALIDATE_SQL;
/