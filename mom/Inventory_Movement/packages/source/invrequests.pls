
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE INV_REQUEST_SQL AUTHID CURRENT_USER AS

TYPE item_rec is RECORD (item       ITEM_MASTER.ITEM%TYPE,
                         need_qty   STORE_ORDERS.NEED_QTY%TYPE,
                         need_date  STORE_ORDERS.NEED_DATE%TYPE);
                         
TYPE item_tbl is TABLE of item_rec INDEX BY BINARY_INTEGER;

/* Function and Procedure Bodies */
-------------------------------------------------------------------------
FUNCTION PROCESS (O_error_message  IN OUT  VARCHAR2,
                  I_store          IN      STORE_ORDERS.STORE%TYPE,
                  I_request_type   IN      VARCHAR2,
                  I_invreqitem_rec IN      "RIB_InvReqItem_REC")
RETURN BOOLEAN;
-------------------------------------------------------------------------
FUNCTION INIT(O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------
FUNCTION FLUSH(O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------
END INV_REQUEST_SQL;
/
