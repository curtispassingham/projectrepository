create or replace PACKAGE BODY CORESVC_INV_STAT_TYP_ADJ_RSN as
   cursor C_SVC_INV_ST_TYPS_TL(I_process_id NUMBER,
                               I_chunk_id   NUMBER) is
      select pk_inv_status_tl.rowid            as pk_inv_status_tl_rid,
             st.rowid                          as st_rid,
             inv_status_types_tl_fk.rowid      as inv_status_types_tl_fk_rid,
             pk_inv_status_tl.inv_status_desc  as old_inv_status_desc,
             st.inv_status,
             st.inv_status_desc,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)                  as action,
             st.process$status
        from svc_inv_st_typs_tl st,
             inv_status_types_tl pk_inv_status_tl,
             inv_status_types inv_status_types_tl_fk
       where st.process_id             = I_process_id
         and st.chunk_id               = I_chunk_id
         and st.inv_status             = pk_inv_status_tl.inv_status (+)
         and st.inv_status             = inv_status_types_tl_fk.inv_status (+);

    cursor C_SVC_INV_STATUS_CODES_TL(I_process_id NUMBER,
                                    I_chunk_id NUMBER) is
      select pk_inv_status_code_tl.rowid      as pk_inv_status_code_tl_rid,
             st.rowid                         as st_rid,
             inv_status_types.rowid           as inv_status_types_rid,
             st.inv_status_code_desc,
             UPPER(st.inv_status_code) as inv_status_code,
             st.inv_status,
             inv_status_code_tl_fk.inv_status as old_inv_status,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) as action,
             st.process$status
        from svc_inv_status_codes_tl st,
             inv_status_codes_tl     pk_inv_status_code_tl,
             inv_status_codes        inv_status_code_tl_fk,
             inv_status_types        inv_status_types
       where st.process_id                  = I_process_id
         and st.chunk_id                    = I_chunk_id
         and UPPER(st.inv_status_code)      = pk_inv_status_code_tl.inv_status_code (+)
         and UPPER(st.inv_status_code)      = inv_status_code_tl_fk.inv_status_code (+)
         and st.inv_status                  = inv_status_types.inv_status (+);

   cursor C_SVC_INV_ADJ_REASON(I_process_id NUMBER,
                               I_chunk_id NUMBER) is
      select pk_inv_adj_reason.rowid  as pk_inv_adj_reason_rid,
             st.rowid                 as st_rid,
             st.cogs_ind,
             st.reason,
             st.reason_desc,
             pk_inv_adj_reason.cogs_ind as old_cogs_ind,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)         as action,
             st.process$status
        from svc_inv_adj_reason st,
             inv_adj_reason pk_inv_adj_reason
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.reason     = pk_inv_adj_reason.reason (+);
   
   TYPE LP_errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab         LP_errors_tab_typ;
   TYPE LP_s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab     LP_s9t_errors_tab_typ;

   Type IS_TYPE_LANG_TAB IS TABLE OF INV_STATUS_TYPES_TL%ROWTYPE;
   Type IS_CODE_LANG_TAB IS TABLE OF INV_STATUS_CODES_TL%ROWTYPE;
   Type INVADJ_TL_TAB IS TABLE OF INV_ADJ_REASON_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   LP_primary_lang       LANG.LANG%TYPE;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name      IN       VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := S9T_ERRORS_SEQ.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                       I_error_type   IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets                   s9t_pkg.names_map_typ;
   inv_st_typs_tl_cols        s9t_pkg.names_map_typ;
   inv_status_codes_tl_cols   s9t_pkg.names_map_typ;
   is_types_lang_cols         s9t_pkg.names_map_typ;
   is_codes_lang_cols         s9t_pkg.names_map_typ;
   inv_adj_reason_cols        s9t_pkg.names_map_typ;
   inv_adj_rsn_tl_cols        s9t_pkg.names_map_typ;

BEGIN
   L_sheets                         := s9t_pkg.get_sheet_names(I_file_id);
   inv_st_typs_tl_cols              := s9t_pkg.get_col_names(I_file_id,
                                                             INV_ST_TYPS_TL_SHEET);
   INV_ST_TYPS_TL$ACTION            := inv_st_typs_tl_cols('ACTION');
   INV_ST_TYPS_TL$INV_STATUS        := inv_st_typs_tl_cols('INV_STATUS');
   INV_ST_TYPS_TL$INV_STATUS_DESC   := inv_st_typs_tl_cols('INV_STATUS_DESC');

   inv_status_codes_tl_cols         := s9t_pkg.get_col_names(I_file_id,
                                                             INV_STATUS_CODES_TL_sheet);
   INV_ST_CODES_TL$ACTION           := inv_status_codes_tl_cols('ACTION');
   INV_ST_CODES_TL$INV_ST_CD_DESC   := inv_status_codes_tl_cols('INV_STATUS_CODE_DESC');
   INV_ST_CODES_TL$INV_ST_CODE      := inv_status_codes_tl_cols('INV_STATUS_CODE');
   INV_ST_CODES_TL$INV_STATUS       := inv_status_codes_tl_cols('INV_STATUS');

   is_types_lang_cols               := s9t_pkg.get_col_names(I_file_id,
                                                             IS_TYPES_LANG_sheet);
   IS_TYPES_LANG$ACTION             := is_types_lang_cols('ACTION');
   IS_TYPES_LANG$LANG               := is_types_lang_cols('LANG');
   IS_TYPES_LANG$INV_STATUS         := is_types_lang_cols('INV_STATUS');
   IS_TYPES_LANG$INV_STATUS_DESC    := is_types_lang_cols('INV_STATUS_DESC');

   is_codes_lang_cols               := s9t_pkg.get_col_names(I_file_id,
                                                             IS_CODES_LANG_sheet);
   IS_CODES_LANG$ACTION             := is_codes_lang_cols('ACTION');
   IS_CODES_LANG$LANG               := is_codes_lang_cols('LANG');
   IS_CODES_LANG$INV_STATUS         := is_codes_lang_cols('INV_STATUS');
   IS_CODES_LANG$INV_STATUS_CODE    := is_codes_lang_cols('INV_STATUS_CODE');
   IS_CODES_LANG$IS_CODE_DESC       := is_codes_lang_cols('INV_STATUS_CODE_DESC');
   
   inv_adj_reason_cols              := s9t_pkg.get_col_names(I_file_id,
                                                             INV_ADJ_REASON_sheet);
   INV_ADJ_REASON$ACTION            := inv_adj_reason_cols('ACTION');
   INV_ADJ_REASON$COGS_IND          := inv_adj_reason_cols('COGS_IND');
   INV_ADJ_REASON$REASON            := inv_adj_reason_cols('REASON');

   inv_adj_rsn_tl_cols              := s9t_pkg.get_col_names(I_file_id,
                                                             INV_ADJ_RSN_TL_sheet);
   INV_ADJ_RSN_TL$Action            := inv_adj_rsn_tl_cols('ACTION');
   INV_ADJ_RSN_TL$LANG              := inv_adj_rsn_tl_cols('LANG');
   INV_ADJ_RSN_TL$REASON            := inv_adj_rsn_tl_cols('REASON');
   INV_ADJ_RSN_TL$REASON_DESC       := inv_adj_rsn_tl_cols('REASON_DESC');

END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_INV_ST_TYPS_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = INV_ST_TYPS_TL_SHEET)
   select s9t_row(s9t_cells( CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod,
                           inv_status,
                           inv_status_desc))
     from inv_status_types_tl
    where lang = LP_primary_lang;
END POPULATE_INV_ST_TYPS_TL;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_INV_STATUS_CODES_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = INV_STATUS_CODES_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod ,
                               tl.INV_STATUS_CODE,
                             INV_STATUS,
                             tl.INV_STATUS_CODE_DESC))
     from inv_status_codes_tl tl,inv_status_codes
     where lang = LP_primary_lang
     and tl.inv_status_code=inv_status_codes.inv_status_code;
END POPULATE_INV_STATUS_CODES_TL;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_IS_TYPES_LANG(I_file_id   IN   NUMBER) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = IS_TYPES_LANG_sheet)
   select s9t_row(s9t_cells(CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod,
                            lang,
                            inv_status,
                            inv_status_desc))
     from inv_status_types_tl
    where lang <> LP_primary_lang;

END POPULATE_IS_TYPES_LANG;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_IS_CODES_LANG(I_file_id   IN   NUMBER) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = IS_CODES_LANG_sheet)
   select s9t_row(s9t_cells(CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod,
                            isc_tl.lang,
                            is_cd.inv_status,
                            isc_tl.inv_status_code,
                            isc_tl.inv_status_code_desc))
     from inv_status_codes_tl isc_tl,
          inv_status_codes is_cd
    where lang <> LP_primary_lang
      and isc_tl.inv_status_code = is_cd.inv_status_code;

END POPULATE_IS_CODES_LANG;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_INV_ADJ_REASON( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = INV_ADJ_REASON_sheet )
   select s9t_row(s9t_cells(CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod ,
                            tl.reason,
                            tl.reason_desc,
                            iar.cogs_ind))
     from inv_adj_reason_tl tl,
          inv_adj_reason iar
    where iar.reason = tl.reason
      and lang = LP_primary_lang;
END POPULATE_INV_ADJ_REASON;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_INV_ADJ_RSN_TL(I_file_id   IN   NUMBER) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = INV_ADJ_RSN_TL_sheet)
   select s9t_row(s9t_cells(CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod ,
                            lang,
                            reason,
                            reason_desc))
     from inv_adj_reason_tl
    where lang <> LP_primary_lang;

END POPULATE_INV_ADJ_RSN_TL;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := S9T_FOLDER_SEQ.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(INV_ST_TYPS_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(INV_ST_TYPS_TL_SHEET)).column_headers := s9t_cells( 'ACTION',
                                                                                            'INV_STATUS',
                                                                                            'INV_STATUS_DESC');

   L_file.add_sheet(INV_STATUS_CODES_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(INV_STATUS_CODES_TL_SHEET)).column_headers := s9t_cells( 'ACTION',
                                                                                                'INV_STATUS_CODE',
                                                                                                'INV_STATUS',
                                                                                                'INV_STATUS_CODE_DESC');

   L_file.add_sheet(IS_TYPES_LANG_sheet);
   L_file.sheets(L_file.get_sheet_index(IS_TYPES_LANG_sheet)).column_headers := s9t_cells('ACTION',
                                                                                          'LANG',
                                                                                          'INV_STATUS',
                                                                                          'INV_STATUS_DESC');

   L_file.add_sheet(IS_CODES_LANG_sheet);
   L_file.sheets(L_file.get_sheet_index(IS_CODES_LANG_sheet)).column_headers := s9t_cells('ACTION',
                                                                                          'LANG',
                                                                                          'INV_STATUS',
                                                                                          'INV_STATUS_CODE',
                                                                                          'INV_STATUS_CODE_DESC');

   L_file.add_sheet(INV_ADJ_REASON_sheet);
   L_file.sheets(l_file.get_sheet_index(INV_ADJ_REASON_sheet)).column_headers := s9t_cells('ACTION',
                                                                                           'REASON',
                                                                                           'REASON_DESC',
                                                                                           'COGS_IND');

   L_file.add_sheet(INV_ADJ_RSN_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(INV_ADJ_RSN_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                           'LANG',
                                                                                           'REASON',
                                                                                           'REASON_DESC');
   
   S9T_PKG.SAVE_OBJ(L_file);

END INIT_S9T;
-----------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_INV_STAT_TYP_ADJ_RSN.CREATE_S9T';
   L_file    s9t_file;
BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                                    template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_INV_ST_TYPS_TL(O_file_id);
      POPULATE_INV_STATUS_CODES_TL(O_file_id);
      POPULATE_IS_TYPES_LANG(O_file_id);
      POPULATE_IS_CODES_LANG(O_file_id);
      POPULATE_INV_ADJ_REASON(O_file_id);
      POPULATE_INV_ADJ_RSN_TL(O_file_id);
      Commit;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
---------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_INV_ST_TYPS_TL( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                      I_process_id IN   SVC_INV_ST_TYPS_TL.PROCESS_ID%TYPE) IS
   TYPE svc_inv_st_typs_tl_col_typ is TABLE OF SVC_INV_ST_TYPS_TL%ROWTYPE;
   L_temp_rec             SVC_INV_ST_TYPS_TL%ROWTYPE;
   svc_inv_st_typs_tl_col svc_inv_st_typs_tl_col_typ          := NEW svc_inv_st_typs_tl_col_typ();
   L_process_id           SVC_INV_ST_TYPS_TL.process_id%TYPE;
   L_error                BOOLEAN                             := FALSE;
   L_default_rec          SVC_INV_ST_TYPS_TL%ROWTYPE;
   cursor C_MANDATORY_IND is
      select inv_status_mi,
             inv_status_desc_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key          = CORESVC_INV_STAT_TYP_ADJ_RSN.template_key
                 and wksht_key             = 'INV_STATUS_TYPES'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('INV_STATUS'      as inv_status,
                                            'INV_STATUS_DESC' as inv_status_desc,
                                            null              as dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
    L_table         VARCHAR2(30)   := 'SVC_INV_ST_TYPS_TL';
   L_pk_columns    VARCHAR2(255)  := 'Status';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  inv_status_dv,
                       inv_status_desc_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key    = CORESVC_INV_STAT_TYP_ADJ_RSN.template_key
                          and wksht_key       = 'INV_STATUS_TYPES'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('INV_STATUS'      as inv_status,
                                                     'INV_STATUS_DESC' as inv_status_desc,
                                                      NULL             as dummy)))
   LOOP
      BEGIN
         L_default_rec.inv_status := rec.inv_status_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'INV_STATUS_TYPES',
                            NULL,
                            'INV_STATUS',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.inv_status_desc := rec.inv_status_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'INV_STATUS_TYPES',
                            NULL,
                            'INV_STATUS_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(INV_ST_TYPS_TL$ACTION)           as action,
                     r.get_cell(INV_ST_TYPS_TL$INV_STATUS)       as inv_status,
                     r.get_cell(INV_ST_TYPS_TL$INV_STATUS_DESC)  as inv_status_desc,
                     r.get_row_seq()                             as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = I_file_id
                 and ss.sheet_name = sheet_name_trans(INV_ST_TYPS_TL_SHEET))
   LOOP

        L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ST_TYPS_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.inv_status := rec.inv_status;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ST_TYPS_TL_SHEET,
                            rec.row_seq,
                            'INV_STATUS',
                            SQLCODE,
                            SQLERRM);
            L_error := FALSE;
      END;
      BEGIN
         L_temp_rec.inv_status_desc := rec.inv_status_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ST_TYPS_TL_SHEET,
                            rec.row_seq,
                            'INV_STATUS_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_new then
         L_temp_rec.inv_status      := NVL( L_temp_rec.inv_status,l_default_rec.inv_status);
         L_temp_rec.inv_status_desc := NVL( L_temp_rec.inv_status_desc,l_default_rec.inv_status_desc);
      end if;
      if not (L_temp_rec.inv_status is not null
              and 1 = 1)then
         WRITE_S9T_ERROR( I_file_id,
                       INV_ST_TYPS_TL_SHEET,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_inv_st_typs_tl_col.extend();
         svc_inv_st_typs_tl_col(svc_inv_st_typs_tl_col.COUNT()):= L_temp_rec;
      end if;
   END LOOP;
   BEGIN
   forall i IN 1..svc_inv_st_typs_tl_col.COUNT SAVE EXCEPTIONS
   merge into SVC_INV_ST_TYPS_TL st
   using(select (case
                 when L_mi_rec.inv_status_mi    = 'N'
                  and svc_inv_st_typs_tl_col(i).action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                  and s1.inv_status             IS NULL
                 then mt.inv_status
                 else s1.inv_status
                  end) as inv_status,
                (case
                 when L_mi_rec.inv_status_desc_mi    = 'N'
                  and svc_inv_st_typs_tl_col(i).action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                  and s1.inv_status_desc             IS NULL
                 then mt.inv_status_desc
                 else s1.inv_status_desc
                 end) as inv_status_desc,
                null as dummy
           from (select svc_inv_st_typs_tl_col(i).inv_status      as inv_status,
                        svc_inv_st_typs_tl_col(i).inv_status_desc as inv_status_desc,
                        null                                      as dummy
                   from dual) s1,
          INV_STATUS_TYPES_TL mt
          where mt.inv_status (+)     = s1.inv_status
            and mt.LANG (+) = LP_primary_lang
            and 1 = 1) sq
             on (st.inv_status      = sq.inv_status and
                 svc_inv_st_typs_tl_col(i).action in( CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod,
                                                      CORESVC_INV_STAT_TYP_ADJ_RSN.action_del))
   when matched then
      update
         set process_id        = svc_inv_st_typs_tl_col(i).process_id ,
             chunk_id          = svc_inv_st_typs_tl_col(i).chunk_id ,
             action            = svc_inv_st_typs_tl_col(i).action,
             row_seq           = svc_inv_st_typs_tl_col(i).row_seq ,
             process$status    = svc_inv_st_typs_tl_col(i).process$status ,
             inv_status_desc   = sq.inv_status_desc ,
             create_id         = svc_inv_st_typs_tl_col(i).create_id ,
             create_datetime   = svc_inv_st_typs_tl_col(i).create_datetime ,
             last_upd_id       = svc_inv_st_typs_tl_col(i).last_upd_id ,
             last_upd_datetime = svc_inv_st_typs_tl_col(i).last_upd_datetime
   when NOT matched then
      insert(process_id ,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             inv_status ,
             inv_status_desc ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_inv_st_typs_tl_col(i).process_id ,
             svc_inv_st_typs_tl_col(i).chunk_id ,
             svc_inv_st_typs_tl_col(i).row_seq ,
             svc_inv_st_typs_tl_col(i).action ,
             svc_inv_st_typs_tl_col(i).process$status ,
             sq.inv_status ,
             sq.inv_status_desc ,
             svc_inv_st_typs_tl_col(i).create_id ,
             svc_inv_st_typs_tl_col(i).create_datetime ,
             svc_inv_st_typs_tl_col(i).last_upd_id ,
             svc_inv_st_typs_tl_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
                if L_error_code=1 then
                   L_error_code:=NULL;
                   L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
                end if;
            WRITE_S9T_ERROR( I_file_id,
                             INV_ST_TYPS_TL_SHEET,
                             svc_inv_st_typs_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_INV_ST_TYPS_TL;
-----------------------------------------------------------------
PROCEDURE PROCESS_S9T_INV_ST_CD_TL( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id IN   SVC_INV_STATUS_CODES_TL.PROCESS_ID%TYPE) IS
   TYPE svc_inv_codes_tl_col_typ IS TABLE OF SVC_INV_STATUS_CODES_TL%ROWTYPE;
   L_temp_rec                  SVC_INV_STATUS_CODES_TL%ROWTYPE;
   svc_inv_codes_tl_col svc_inv_codes_tl_col_typ :=NEW svc_inv_codes_tl_col_typ();
   L_process_id                SVC_INV_STATUS_CODES_TL.PROCESS_ID%TYPE;
   L_error                     BOOLEAN:=FALSE;
   L_default_rec SVC_INV_STATUS_CODES_TL%ROWTYPE;
   cursor C_MANDATORY_IND is
      select INV_STATUS_CODE_DESC_mi,
             INV_STATUS_CODE_mi,
                 INV_STATUS_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'INV_STATUS_CODES'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN('INV_STATUS_CODE_DESC' as INV_STATUS_CODE_DESC,
                                           'INV_STATUS_CODE'      as INV_STATUS_CODE,
                                                         'INV_STATUS'           as INV_STATUS,
                                            null as dummy));
      L_mi_rec C_MANDATORY_IND%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
        L_table         VARCHAR2(30)   := 'SVC_INV_STATUS_CODES_TL';
      L_pk_columns    VARCHAR2(255)  := 'Inventory Status Code';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select INV_STATUS_CODE_DESC_dv,
                      INV_STATUS_CODE_dv,
                             INV_STATUS_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'INV_STATUS_CODES'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('INV_STATUS_CODE_DESC' as INV_STATUS_CODE_DESC,
                                                     'INV_STATUS_CODE' as INV_STATUS_CODE,
                                                     'INV_STATUS' as INV_STATUS,
                                                                      NULL as dummy)))
   LOOP
      BEGIN
         L_default_rec.INV_STATUS_CODE_DESC := rec.INV_STATUS_CODE_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'INV_STATUS_CODES' ,
                            NULL,
                           'INV_STATUS_CODE_DESC ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.INV_STATUS_CODE := rec.INV_STATUS_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'INV_STATUS_CODES' ,
                            NULL,
                           'INV_STATUS_CODE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
        BEGIN
         L_default_rec.INV_STATUS := rec.INV_STATUS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'INV_STATUS_CODES' ,
                            NULL,
                           'INV_STATUS' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(INV_ST_CODES_TL$ACTION)                      as Action,
          r.get_cell(INV_ST_CODES_TL$INV_ST_CD_DESC)              as INV_STATUS_CODE_DESC,
          UPPER(r.get_cell(INV_ST_CODES_TL$INV_ST_CODE))                 as INV_STATUS_CODE,
             r.get_cell(INV_ST_CODES_TL$INV_STATUS)                  as INV_STATUS,
          r.get_row_seq()                                         as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(INV_STATUS_CODES_TL_sheet))
   LOOP

        L_temp_rec:=NULL;
        L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
           L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_STATUS_CODES_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.INV_STATUS_CODE_DESC := rec.INV_STATUS_CODE_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_STATUS_CODES_TL_sheet,
                            rec.row_seq,
                            'INV_STATUS_CODE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.INV_STATUS_CODE := rec.INV_STATUS_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_STATUS_CODES_TL_sheet,
                            rec.row_seq,
                            'INV_STATUS_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
        BEGIN
         L_temp_rec.INV_STATUS    := rec.INV_STATUS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_STATUS_CODES_TL_sheet,
                            rec.row_seq,
                            'INV_STATUS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_new then
         L_temp_rec.INV_STATUS_CODE_DESC := NVL( L_temp_rec.INV_STATUS_CODE_DESC,L_default_rec.INV_STATUS_CODE_DESC);
         L_temp_rec.INV_STATUS_CODE      := NVL( L_temp_rec.INV_STATUS_CODE,L_default_rec.INV_STATUS_CODE);
         L_temp_rec.INV_STATUS           := NVL( L_temp_rec.INV_STATUS,L_default_rec.INV_STATUS);
        end if;
      if not (L_temp_rec.INV_STATUS_CODE is NOT NULL
              and 1 = 1)then
         WRITE_S9T_ERROR( I_file_id,
                       INV_STATUS_CODES_TL_sheet,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_inv_codes_tl_col.extend();
         svc_inv_codes_tl_col(svc_inv_codes_tl_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_inv_codes_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_INV_STATUS_CODES_TL st
      using(select
                  (case
                   when L_mi_rec.inv_status_code_desc_mi    = 'N'
                    and svc_inv_codes_tl_col(i).action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                    and s1.inv_status_code_desc is null
                   then mtl.inv_status_code_desc
                   else s1.inv_status_code_desc
                   end) as inv_status_code_desc,
                   (case
                    when L_mi_rec.inv_status_code_mi    = 'N'
                     and svc_inv_codes_tl_col(i).action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                     and s1.inv_status_code is null
                    then mt.inv_status_code
                    else s1.inv_status_code
                    end) as inv_status_code,
                   (case
                    when L_mi_rec.inv_status_mi    = 'N'
                     and svc_inv_codes_tl_col(i).action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                     and s1.inv_status is null
                    then mt.inv_status
                    else s1.inv_status
                    end) as inv_status,
                   null as dummy
              from (select svc_inv_codes_tl_col(i).inv_status_code_desc as inv_status_code_desc,
                           svc_inv_codes_tl_col(i).inv_status_code as inv_status_code,
                           svc_inv_codes_tl_col(i).inv_status as inv_status
                      from dual ) s1,
                   inv_status_codes mt,
                   inv_status_codes_tl mtl
             where mt.inv_status_code (+)     = s1.inv_status_code
               and mtl.inv_status_code (+)    = s1.inv_status_code
               and mtl.lang (+)               = LP_primary_lang)sq
                on (st.inv_status_code      = sq.inv_status_code and
                    svc_inv_codes_tl_col(i).action in (CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod,CORESVC_INV_STAT_TYP_ADJ_RSN.action_del))
      when matched then
      update
         set process_id           = svc_inv_codes_tl_col(i).process_id ,
             chunk_id             = svc_inv_codes_tl_col(i).chunk_id ,
             row_seq              = svc_inv_codes_tl_col(i).row_seq ,
             action               = svc_inv_codes_tl_col(i).action ,
             process$status       = svc_inv_codes_tl_col(i).process$status ,
             inv_status_code_desc = sq.inv_status_code_desc ,
             inv_status           = sq.inv_status,
             create_id            = svc_inv_codes_tl_col(i).create_id ,
             create_datetime      = svc_inv_codes_tl_col(i).create_datetime ,
             last_upd_id          = svc_inv_codes_tl_col(i).last_upd_id ,
             last_upd_datetime    = svc_inv_codes_tl_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             inv_status_code_desc ,
             inv_status_code ,
             inv_status,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_inv_codes_tl_col(i).process_id ,
             svc_inv_codes_tl_col(i).chunk_id ,
             svc_inv_codes_tl_col(i).row_seq ,
             svc_inv_codes_tl_col(i).action ,
             svc_inv_codes_tl_col(i).process$status ,
             sq.inv_status_code_desc ,
             sq.inv_status_code ,
             sq.inv_status,
             svc_inv_codes_tl_col(i).create_id ,
             svc_inv_codes_tl_col(i).create_datetime ,
             svc_inv_codes_tl_col(i).last_upd_id ,
             svc_inv_codes_tl_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
                if L_error_code=1 then
                   L_error_code:=NULL;
                   L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
                end if;
            WRITE_S9T_ERROR( I_file_id,
                             INV_STATUS_CODES_TL_sheet,
                             svc_inv_codes_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_INV_ST_CD_TL;
-----------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_IS_TYPE_LANG(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_INV_STATUS_TYPES_TL.PROCESS_ID%TYPE) IS

   TYPE svc_is_type_lang_col_typ is TABLE OF SVC_INV_STATUS_TYPES_TL%ROWTYPE;
   L_temp_rec             SVC_INV_STATUS_TYPES_TL%ROWTYPE;
   svc_is_type_lang_col   svc_is_type_lang_col_typ := NEW svc_is_type_lang_col_typ();
   L_process_id           SVC_INV_STATUS_TYPES_TL.PROCESS_ID%TYPE;
   L_error                BOOLEAN                  := FALSE;
   L_default_rec          SVC_INV_STATUS_TYPES_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             inv_status_mi,
             inv_status_desc_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key =  template_key
                 and wksht_key    = 'INV_STATUS_TYPES_TL'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('LANG'            as lang,
                                            'INV_STATUS'      as inv_status,
                                            'INV_STATUS_DESC' as inv_status_desc));

   L_mi_rec               C_MANDATORY_IND%ROWTYPE;
   dml_errors             EXCEPTION;
   PRAGMA                 exception_init(dml_errors, -24381);
   L_table                VARCHAR2(30)             := 'SVC_INV_STATUS_TYPES_TL';
   L_pk_columns           VARCHAR2(255)            := 'Inventory Status, Lang';
   L_error_code           NUMBER;
   L_error_msg            RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FOR rec IN (select lang_dv,
                      inv_status_dv,
                      inv_status_desc_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  =  template_key
                          and wksht_key     = 'INV_STATUS_TYPES_TL'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('LANG'            as lang,
                                                     'INV_STATUS'      as inv_status,
                                                     'INV_STATUS_DESC' as inv_status_desc)))
   LOOP
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_TYPES_LANG_sheet,
                            NULL,
                            'LANG',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.inv_status_desc := rec.inv_status_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_TYPES_LANG_sheet,
                            NULL,
                            'INV_STATUS_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.inv_status := rec.inv_status_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_TYPES_LANG_sheet,
                            NULL,
                            'INV_STATUS',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(is_types_lang$action)            as action,
                     r.get_cell(is_types_lang$lang)              as lang,
                     r.get_cell(is_types_lang$inv_status)        as inv_status,
                     r.get_cell(is_types_lang$inv_status_desc)   as inv_status_desc,
                     r.get_row_seq()                             as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(IS_TYPES_LANG_sheet)
             )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := false;

      BEGIN
        L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_TYPES_LANG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_TYPES_LANG_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.inv_status_desc := rec.inv_status_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_TYPES_LANG_sheet,
                            rec.row_seq,
                            'INV_STATUS_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.inv_status := rec.inv_status;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_TYPES_LANG_sheet,
                            rec.row_seq,
                            'INV_STATUS',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      if rec.action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_new then
        L_temp_rec.inv_status_desc := NVL(L_temp_rec.inv_status_desc, L_default_rec.inv_status_desc);
        L_temp_rec.inv_status      := NVL(L_temp_rec.inv_status, L_default_rec.inv_status);
        L_temp_rec.lang            := NVL(L_temp_rec.lang, L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.inv_status is NOT NULL and L_temp_rec.lang is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         IS_TYPES_LANG_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_is_type_lang_col.extend();
         svc_is_type_lang_col(svc_is_type_lang_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_is_type_lang_col.COUNT SAVE EXCEPTIONS
         merge into SVC_INV_STATUS_TYPES_TL st
         USING(select (case
                       when L_mi_rec.inv_status_mi    = 'N'
                       and svc_is_type_lang_col(i).action   = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                          and s1.inv_status             IS NULL
                       then mt.inv_status
                       else s1.inv_status
                       end) as inv_status,
                      (case
                       when L_mi_rec.lang_mi    = 'N'
                       and svc_is_type_lang_col(i).action   = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                          and s1.lang             IS NULL
                       then mt.lang
                       else s1.lang
                       end) as lang,
                      (case
                       when L_mi_rec.inv_status_desc_mi = 'N'
                       and svc_is_type_lang_col(i).action     = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                       and s1.inv_status_desc             IS NULL
                       then mt.inv_status_desc
                       else s1.inv_status_desc
                        end) as inv_status_desc,
                       NULL as dummy
                 from (select svc_is_type_lang_col(i).lang as lang,
                              svc_is_type_lang_col(i).inv_status as inv_status,
                              svc_is_type_lang_col(i).inv_status_desc as inv_status_desc,
                              null as dummy
                         from dual) s1,
                       inv_status_types_tl mt
                where mt.lang (+)       = s1.lang
                  and mt.inv_status (+) = s1.inv_status) sq
                  ON (st.inv_status = sq.inv_status
                      and st.lang   = sq.lang
                      and svc_is_type_lang_col(i).action IN (CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod,
                                                             CORESVC_INV_STAT_TYP_ADJ_RSN.action_del))
         when matched then
            update
               set process_id      = svc_is_type_lang_col(i).process_id ,
                   chunk_id        = svc_is_type_lang_col(i).chunk_id ,
                   row_seq         = svc_is_type_lang_col(i).row_seq ,
                   action          = svc_is_type_lang_col(i).action,
                   process$status  = svc_is_type_lang_col(i).process$status ,
                   inv_status_desc = sq.inv_status_desc
         when NOT matched then
            insert(process_id,
                   chunk_id,
                   row_seq,
                   action,
                   process$status,
                   lang,
                   inv_status,
                   inv_status_desc)
            values(svc_is_type_lang_col(i).process_id,
                   svc_is_type_lang_col(i).chunk_id,
                   svc_is_type_lang_col(i).row_seq,
                   svc_is_type_lang_col(i).action,
                   svc_is_type_lang_col(i).process$status,
                   sq.lang,
                   sq.inv_status,
                   sq.inv_status_desc);

   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            IS_TYPES_LANG_sheet,
                            svc_is_type_lang_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;

END PROCESS_S9T_IS_TYPE_LANG;
--------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_IS_CODE_LANG(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_INV_STATUS_CODES_LANG_TL.PROCESS_ID%TYPE) IS

   TYPE svc_is_code_lang_col_typ is TABLE OF SVC_INV_STATUS_CODES_LANG_TL%ROWTYPE;
   L_temp_rec             SVC_INV_STATUS_CODES_LANG_TL%ROWTYPE;
   svc_is_code_lang_col   svc_is_code_lang_col_typ := NEW svc_is_code_lang_col_typ();
   L_process_id           SVC_INV_STATUS_CODES_LANG_TL.PROCESS_ID%TYPE;
   L_error                BOOLEAN                  := FALSE;
   L_default_rec          SVC_INV_STATUS_CODES_LANG_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             inv_status_code_mi,
             inv_status_code_desc_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key =  template_key
                 and wksht_key    = 'INV_STATUS_CODES_TL'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('LANG'                 as lang,
                                            'INV_STATUS_CODE'      as inv_status_code,
                                            'INV_STATUS_CODE_DESC' as inv_status_code_desc));

   L_mi_rec               C_MANDATORY_IND%ROWTYPE;
   dml_errors             EXCEPTION;
   PRAGMA                 exception_init(dml_errors, -24381);
   L_table                VARCHAR2(30)             := 'SVC_INV_STATUS_CODES_LANG_TL';
   L_pk_columns           VARCHAR2(255)            := 'Inventory Status Code, Lang';
   L_error_code           NUMBER;
   L_error_msg            RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FOR rec IN (select lang_dv,
                      inv_status_code_dv,
                      inv_status_code_desc_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  =  template_key
                          and wksht_key     = 'INV_STATUS_CODES_TL'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('LANG'                 as lang,
                                                     'INV_STATUS_CODE'      as inv_status_code,
                                                     'INV_STATUS_CODE_DESC' as inv_status_code_desc)))
   LOOP
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_CODES_LANG_sheet,
                            NULL,
                            'LANG',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.inv_status_code_desc := rec.inv_status_code_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_CODES_LANG_sheet,
                            NULL,
                            'INV_STATUS_CODE_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.inv_status_code := rec.inv_status_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_CODES_LANG_sheet,
                            NULL,
                            'INV_STATUS_CODE',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(is_codes_lang$action)                 as action,
                     r.get_cell(is_codes_lang$lang)                   as lang,
                     r.get_cell(is_codes_lang$inv_status)             as inv_status,
                     UPPER(r.get_cell(is_codes_lang$inv_status_code)) as inv_status_code,
                     r.get_cell(is_codes_lang$is_code_desc)           as inv_status_code_desc,
                     r.get_row_seq()                                  as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(IS_CODES_LANG_sheet)
             )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := false;

      BEGIN
        L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_CODES_LANG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_CODES_LANG_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.inv_status_code_desc := rec.inv_status_code_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_CODES_LANG_sheet,
                            rec.row_seq,
                            'INV_STATUS_CODE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.inv_status_code := rec.inv_status_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            IS_CODES_LANG_sheet,
                            rec.row_seq,
                            'INV_STATUS_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      if rec.action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_new then
        L_temp_rec.inv_status_code_desc := NVL(L_temp_rec.inv_status_code_desc, L_default_rec.inv_status_code_desc);
        L_temp_rec.inv_status_code      := NVL(L_temp_rec.inv_status_code, L_default_rec.inv_status_code);
        L_temp_rec.lang                 := NVL(L_temp_rec.lang, L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.inv_status_code is NOT NULL and L_temp_rec.lang is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         IS_CODES_LANG_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_is_code_lang_col.extend();
         svc_is_code_lang_col(svc_is_code_lang_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_is_code_lang_col.COUNT SAVE EXCEPTIONS
         merge into SVC_INV_STATUS_CODES_LANG_TL st
         USING(select (case
                       when L_mi_rec.inv_status_code_mi    = 'N'
                       and svc_is_code_lang_col(i).action   = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                          and s1.inv_status_code             IS NULL
                       then mt.inv_status_code
                       else s1.inv_status_code
                       end) as inv_status_code,
                      (case
                       when L_mi_rec.lang_mi    = 'N'
                       and svc_is_code_lang_col(i).action   = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                          and s1.lang             IS NULL
                       then mt.lang
                       else s1.lang
                       end) as lang,
                      (case
                       when L_mi_rec.inv_status_code_desc_mi = 'N'
                       and svc_is_code_lang_col(i).action     = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                       and s1.inv_status_code_desc             IS NULL
                       then mt.inv_status_code_desc
                       else s1.inv_status_code_desc
                        end) as inv_status_code_desc,
                       NULL as dummy
                 from (select svc_is_code_lang_col(i).lang as lang,
                              svc_is_code_lang_col(i).inv_status_code as inv_status_code,
                              svc_is_code_lang_col(i).inv_status_code_desc as inv_status_code_desc,
                              null as dummy
                         from dual) s1,
                       inv_status_codes_tl mt
                where mt.lang (+)       = s1.lang
                  and mt.inv_status_code (+) = s1.inv_status_code) sq
                  ON (st.inv_status_code = sq.inv_status_code
                      and st.lang        = sq.lang
                      and svc_is_code_lang_col(i).action IN (CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod,
                                                             CORESVC_INV_STAT_TYP_ADJ_RSN.action_del))
         when matched then
            update
               set process_id           = svc_is_code_lang_col(i).process_id ,
                   chunk_id             = svc_is_code_lang_col(i).chunk_id ,
                   row_seq              = svc_is_code_lang_col(i).row_seq ,
                   action               = svc_is_code_lang_col(i).action,
                   process$status       = svc_is_code_lang_col(i).process$status ,
                   inv_status_code_desc = sq.inv_status_code_desc
         when NOT matched then
            insert(process_id,
                   chunk_id,
                   row_seq,
                   action,
                   process$status,
                   lang,
                   inv_status_code,
                   inv_status_code_desc)
            values(svc_is_code_lang_col(i).process_id,
                   svc_is_code_lang_col(i).chunk_id,
                   svc_is_code_lang_col(i).row_seq,
                   svc_is_code_lang_col(i).action,
                   svc_is_code_lang_col(i).process$status,
                   sq.lang,
                   sq.inv_status_code,
                   sq.inv_status_code_desc);

   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            IS_CODES_LANG_sheet,
                            svc_is_code_lang_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;

END PROCESS_S9T_IS_CODE_LANG;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_INV_ADJ_REASON( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                      I_process_id IN   SVC_INV_ADJ_REASON.process_id%TYPE) IS
   TYPE svc_inv_adj_reason_col_typ IS TABLE OF SVC_INV_ADJ_REASON%ROWTYPE;
   L_temp_rec             SVC_INV_ADJ_REASON%ROWTYPE;
   svc_inv_adj_reason_col svc_inv_adj_reason_col_typ :=NEW svc_inv_adj_reason_col_typ();
   L_process_id           SVC_INV_ADJ_REASON.process_id%TYPE;
   L_error                BOOLEAN:=FALSE;
   L_default_rec          SVC_INV_ADJ_REASON%ROWTYPE;
   cursor C_MANDATORY_IND is
      select cogs_ind_mi,
             reason_mi,
             reason_desc_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key      = template_key
                 and wksht_key         = 'INV_ADJ_REASON'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('COGS_IND'    as cogs_ind,
                                            'REASON'      as reason,
                                            'REASON_DESC' as reason_desc,
                                             null         as dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
    L_table         VARCHAR2(30)   := 'SVC_INV_ADJ_REASON';
   L_pk_columns    VARCHAR2(255)  := 'Reason';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  cogs_ind_dv,
                       reason_dv,
                       reason_desc_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key    = template_key
                          and wksht_key       = 'INV_ADJ_REASON'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('COGS_IND'    as cogs_ind,
                                                     'REASON'      as reason,
                                                     'REASON_DESC' as reason_desc,
                                                      NULL         as dummy)))
   LOOP
      BEGIN
         L_default_rec.cogs_ind := rec.cogs_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'INV_ADJ_REASON' ,
                            NULL,
                           'COGS_IND' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.reason := rec.reason_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'INV_ADJ_REASON' ,
                            NULL,
                           'REASON' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.reason_desc := rec.reason_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'INV_ADJ_REASON' ,
                            NULL,
                           'REASON_DESC' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
   (select r.get_cell(INV_ADJ_REASON$Action)      as Action,
           r.get_cell(INV_ADJ_REASON$COGS_IND)    as cogs_ind,
           r.get_cell(INV_ADJ_REASON$REASON)      as reason,
           r.get_cell(INV_ADJ_REASON$REASON_DESC) as reason_desc,
           r.get_row_seq()                        as row_seq
      from s9t_folder sf,
           TABLE(sf.s9t_file_obj.sheets) ss,
           TABLE(ss.s9t_rows) r
      where sf.file_id  = I_file_id
        and ss.sheet_name = GET_SHEET_NAME_TRANS(INV_ADJ_REASON_sheet))
   LOOP
      L_temp_rec                   := NULL;
        L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               INV_ADJ_REASON_sheet,
                               rec.row_seq,
                               action_column,
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cogs_ind := rec.cogs_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ADJ_REASON_sheet,
                            rec.row_seq,
                            'COGS_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.reason := rec.reason;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ADJ_REASON_sheet,
                            rec.row_seq,
                            'REASON',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.reason_desc := rec.reason_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ADJ_REASON_sheet,
                            rec.row_seq,
                            'REASON_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_new then
         L_temp_rec.cogs_ind      := NVL( L_temp_rec.cogs_ind,L_default_rec.cogs_ind);
         L_temp_rec.reason        := NVL( L_temp_rec.reason,L_default_rec.reason);
         L_temp_rec.reason_desc   := NVL( L_temp_rec.reason_desc,L_default_rec.reason_desc);
      end if;
      if not (L_temp_rec.reason is NOT NULL
              and 1 = 1 )then
         WRITE_S9T_ERROR( I_file_id,
                       INV_ADJ_REASON_sheet,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_inv_adj_reason_col.extend();
         svc_inv_adj_reason_col(svc_inv_adj_reason_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_inv_adj_reason_col.COUNT SAVE EXCEPTIONS
      merge into SVC_INV_ADJ_REASON st
      using(select
                   (case
                    when L_mi_rec.cogs_ind_mi    = 'N'
                     and svc_inv_adj_reason_col(i).action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                     and s1.cogs_ind IS NULL
                    then mt.cogs_ind
                    else s1.cogs_ind
                    end) as cogs_ind,
                   (case
                    when L_mi_rec.reason_mi    = 'N'
                     and svc_inv_adj_reason_col(i).action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                     and s1.reason IS NULL
                    then mt.reason
                    else s1.reason
                    end) as reason,
                   (case
                    when L_mi_rec.reason_desc_mi    = 'N'
                     and svc_inv_adj_reason_col(i).action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                     and s1.reason_desc IS NULL
                    then mtl.reason_desc
                    else s1.reason_desc
                    end) as reason_desc,
                   NULL as dummy
             from  (select svc_inv_adj_reason_col(i).cogs_ind as cogs_ind,
                           svc_inv_adj_reason_col(i).reason as reason,
                           svc_inv_adj_reason_col(i).reason_desc as reason_desc,
                           NULL as dummy
                      from dual ) s1,
                   inv_adj_reason mt,
                   inv_adj_reason_tl mtl
             where mt.reason (+) = s1.reason
               and mtl.reason (+) = s1.reason
               and mtl.lang (+) = LP_primary_lang
               and 1 = 1 )sq
                on (st.reason      = sq.reason
                    and svc_inv_adj_reason_col(i).ACTION IN (CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod,CORESVC_INV_STAT_TYP_ADJ_RSN.action_del))
      when matched then
      update
         set process_id      = svc_inv_adj_reason_col(i).process_id ,
             chunk_id        = svc_inv_adj_reason_col(i).chunk_id ,
             row_seq         = svc_inv_adj_reason_col(i).row_seq ,
             action          = svc_inv_adj_reason_col(i).action ,
             process$status  = svc_inv_adj_reason_col(i).process$status ,
             cogs_ind        = sq.cogs_ind ,
             reason_desc     = sq.reason_desc,
             create_id       = svc_inv_adj_reason_col(i).create_id ,
             create_datetime = svc_inv_adj_reason_col(i).create_datetime ,
             last_upd_id     = svc_inv_adj_reason_col(i).last_upd_id ,
             last_upd_datetime = svc_inv_adj_reason_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             cogs_ind ,
             reason ,
             reason_desc,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_inv_adj_reason_col(i).process_id ,
             svc_inv_adj_reason_col(i).chunk_id ,
             svc_inv_adj_reason_col(i).row_seq ,
             svc_inv_adj_reason_col(i).action ,
             svc_inv_adj_reason_col(i).process$status ,
             sq.cogs_ind ,
             sq.reason ,
             sq.reason_desc,
             svc_inv_adj_reason_col(i).create_id ,
             svc_inv_adj_reason_col(i).create_datetime ,
             svc_inv_adj_reason_col(i).last_upd_id ,
             svc_inv_adj_reason_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
                if L_error_code=1 then
                   L_error_code:=NULL;
                   L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
                end if;
            WRITE_S9T_ERROR( I_file_id,
                             INV_ADJ_REASON_sheet,
                             svc_inv_adj_reason_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
      when OTHERS then
         if C_MANDATORY_IND%ISOPEN then
            close C_MANDATORY_IND;
         end if;
         rollback;
   END;
END PROCESS_S9T_INV_ADJ_REASON;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_INVADJ_RSN_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id   IN   SVC_INV_ADJ_REASON_TL.PROCESS_ID%TYPE) IS

   TYPE svc_invadj_rsn_tl_col_typ is TABLE OF SVC_INV_ADJ_REASON_TL%ROWTYPE;
   L_temp_rec              SVC_INV_ADJ_REASON_TL%ROWTYPE;
   svc_invadj_rsn_tl_col   svc_invadj_rsn_tl_col_typ :=NEW svc_invadj_rsn_tl_col_typ();
   L_process_id            SVC_INV_ADJ_REASON_TL.PROCESS_ID%TYPE;
   L_error                 BOOLEAN                   :=FALSE;
   L_default_rec           SVC_INV_ADJ_REASON_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             reason_mi,
             reason_desc_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key =  template_key
                 and wksht_key    = 'INV_ADJ_REASON_TL'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('LANG'        as lang,
                                            'REASON'      as reason,
                                            'REASON_DESC' as reason_desc));

   L_mi_rec                C_MANDATORY_IND%ROWTYPE;
   dml_errors              EXCEPTION;
   PRAGMA                  exception_init(dml_errors, -24381);
   L_table                 VARCHAR2(30)   := 'SVC_INV_ADJ_REASON_TL';
   L_pk_columns            VARCHAR2(255)  := 'Reason, Lang';
   L_error_code            NUMBER;
   L_error_msg             RTK_ERRORS.RTK_TEXT%type;

BEGIN
   FOR rec IN (select lang_dv,
                      reason_dv,
                      reason_desc_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key =  template_key
                          and wksht_key    = 'INV_ADJ_REASON_TL'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'LANG'        as lang,
                                                      'REASON'      as reason,
                                                      'REASON_DESC' as reason_desc)))
   LOOP
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ADJ_RSN_TL_sheet,
                            NULL,
                            'LANG',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.reason_desc := rec.reason_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ADJ_RSN_TL_sheet,
                            NULL,
                            'REASON_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.reason := rec.reason_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ADJ_RSN_TL_sheet,
                            NULL,
                            'REASON',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(inv_adj_rsn_tl$action)      as action,
                     r.get_cell(inv_adj_rsn_tl$lang)        as lang,
                     r.get_cell(inv_adj_rsn_tl$reason)      as reason,
                     r.get_cell(inv_adj_rsn_tl$reason_desc) as reason_desc,
                     r.get_row_seq()                        as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(INV_ADJ_RSN_TL_sheet)
             )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := false;

      BEGIN
        L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ADJ_RSN_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ADJ_RSN_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.reason_desc := rec.reason_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ADJ_RSN_TL_sheet,
                            rec.row_seq,
                            'REASON_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.reason := rec.reason;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            INV_ADJ_RSN_TL_sheet,
                            rec.row_seq,
                            'REASON',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      if rec.action = CORESVC_INV_STAT_TYP_ADJ_RSN.action_new then
        L_temp_rec.reason_desc := NVL(L_temp_rec.reason_desc, L_default_rec.reason_desc);
        L_temp_rec.reason      := NVL(L_temp_rec.reason,L_default_rec.reason);
        L_temp_rec.lang        := NVL(L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.reason is NOT NULL and L_temp_rec.lang is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         INV_ADJ_RSN_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_invadj_rsn_tl_col.extend();
         svc_invadj_rsn_tl_col(svc_invadj_rsn_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_invadj_rsn_tl_col.COUNT SAVE EXCEPTIONS
         Merge into SVC_INV_ADJ_REASON_TL st
         USING(select (case
                       when L_mi_rec.reason_mi    = 'N'
                       and svc_invadj_rsn_tl_col(i).action   = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                          and s1.reason             IS NULL
                       then mt.reason
                       else s1.reason
                       end) as reason,
                      (case
                       when L_mi_rec.lang_mi    = 'N'
                       and svc_invadj_rsn_tl_col(i).action   = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                          and s1.lang             IS NULL
                       then mt.lang
                       else s1.lang
                       end) as lang,
                      (case
                       when L_mi_rec.reason_desc_mi = 'N'
                       and svc_invadj_rsn_tl_col(i).action     = CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod
                       and s1.reason_desc             IS NULL
                       then mt.reason_desc
                       else s1.reason_desc
                        end) as reason_desc,
                        null as dummy
                 from (select svc_invadj_rsn_tl_col(i).lang as lang,
                              svc_invadj_rsn_tl_col(i).reason as reason,
                              svc_invadj_rsn_tl_col(i).reason_desc as reason_desc,
                              null as dummy
                         from dual) s1,
                       inv_adj_reason_tl mt
                where mt.lang (+)   = s1.lang
                  and mt.reason (+) = s1.reason) sq
                  ON (st.reason   = sq.reason
                      and st.lang = sq.lang
                      and svc_invadj_rsn_tl_col(i).action IN (CORESVC_INV_STAT_TYP_ADJ_RSN.action_mod,
                                                              CORESVC_INV_STAT_TYP_ADJ_RSN.action_del))
         when matched then
            update
               set process_id     = svc_invadj_rsn_tl_col(i).process_id,
                   chunk_id       = svc_invadj_rsn_tl_col(i).chunk_id,
                   row_seq        = svc_invadj_rsn_tl_col(i).row_seq,
                   action         = svc_invadj_rsn_tl_col(i).action,
                   process$status = svc_invadj_rsn_tl_col(i).process$status ,
                   reason_desc    = sq.reason_desc
         when NOT matched then
            insert(process_id,
                   chunk_id,
                   row_seq,
                   action,
                   process$status,
                   lang,
                   reason,
                   reason_desc)
            values(svc_invadj_rsn_tl_col(i).process_id,
                   svc_invadj_rsn_tl_col(i).chunk_id,
                   svc_invadj_rsn_tl_col(i).row_seq,
                   svc_invadj_rsn_tl_col(i).action,
                   svc_invadj_rsn_tl_col(i).process$status,
                   sq.lang,
                   sq.reason,
                   sq.reason_desc);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT
            LOOP
               L_error_code:=sql%bulk_exceptions(i).error_code;
               if L_error_code=1 then
                  L_error_code := NULL;
                  L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
               end if;
               WRITE_S9T_ERROR(I_file_id,
                               INV_ADJ_RSN_TL_sheet,
                               svc_invadj_rsn_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                               NULL,
                               L_error_code,
                               L_error_msg);
            END LOOP;
   END;
END PROCESS_S9T_INVADJ_RSN_TL;
--------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_INV_STAT_TYP_ADJ_RSN.PROCESS_S9T';
   L_file             s9t_file;
   L_sheets           S9T_PKG.NAMES_MAP_TYP;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   COMMIT;--to ensure that the record in s9t_folder is commited
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_INV_ST_TYPS_TL(I_file_id,
                                 I_process_id);
      PROCESS_S9T_INV_ST_CD_TL(I_file_id,
                               I_process_id);
      PROCESS_S9T_IS_TYPE_LANG(I_file_id,
                               I_process_id);
      PROCESS_S9T_IS_CODE_LANG(I_file_id,
                               I_process_id);
      PROCESS_S9T_INV_ADJ_REASON(I_file_id,I_process_id);
      PROCESS_S9T_INVADJ_RSN_TL(I_file_id,I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
              file_id    = I_file_id
    where process_id = I_process_id;
   commit;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
         values LP_s9t_errors_tab(i);
         update svc_process_tracker
            set status       = 'PE',
                file_id      = i_file_id
          where process_id   = i_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
             values Lp_s9t_errors_tab(i);

         update svc_process_tracker
            set status = 'PE',
                file_id = I_file_id
          where process_id = I_process_id;
         COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_INV_ST_TYPS_TL_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error           IN OUT   BOOLEAN,
                                    I_rec             IN       C_SVC_INV_ST_TYPS_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                        := 'CORESVC_INV_STAT_TYP_ADJ_RSN.PROCESS_INV_ST_TYPS_TL_VAL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'SVC_INV_ST_TYPS_TL';
   L_found   BOOLEAN;
BEGIN
   if I_rec.action=action_new then
      if INVADJ_VALIDATE_SQL.CHECK_INV_STATUS_EXIST(I_rec.inv_status,
                                                    O_error_message,
                                                    L_found) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'INV_STATUS',
                     O_error_message);
         O_error:= TRUE;
      end if;
      if L_found = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'INV_STATUS',
                     'NO_DUPLICATE_INV_STATUS');
         O_error:= TRUE;
      end if;
      if I_rec.inv_status=0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'INV_STATUS',
                     'NO_INV_STATUS_ZERO');
         O_error:= TRUE;
      end if;
   end if;
   L_found:=FALSE;
   if I_rec.action=action_del
       and I_rec.pk_inv_status_tl_rid is NOT NULL then
      if INVADJ_VALIDATE_SQL.INV_STATUS_CONSTRAINTS_EXIST(I_rec.inv_status,
                                                          O_error_message,
                                                          L_found) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'INV_STATUS',
                     O_error_message);
         O_error:= TRUE;
      end if;
      if L_found = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'INV_STATUS',
                     'INV_STAT_FRGN_CONSTRAINT');
         O_error:= TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_INV_ST_TYPS_TL_VAL;
-----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_INV_ST_CD_TL_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error           IN OUT   BOOLEAN,
                                  I_rec             IN       C_SVC_INV_STATUS_CODES_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_INV_STATUS_CODES.PROCESS_INV_ST_CD_TL_VAL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_STATUS_CODES_TL';
BEGIN
   if I_rec.action=action_mod
       and nvl(I_rec.inv_status,-1) <> nvl(I_rec.old_inv_status,-1) then
        WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'INV_STATUS',
                  'CANNOT_MOD_INV_STATUS');
      O_error:= TRUE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_INV_ST_CD_TL_VAL;
--------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_ST_TYPS_TL_PRE_INS( O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_inv_st_typs_tl_temp_rec   IN       INV_STATUS_TYPES_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INV_ST_TYPS_TL_PRE_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_ST_TYPS_TL';
BEGIN
   insert
     into inv_status_types
   values (I_inv_st_typs_tl_temp_rec.inv_status);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_ST_TYPS_TL_PRE_INS;
-------------------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_ST_TYPS_TL_INS( O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_inv_st_typs_tl_temp_rec   IN       INV_STATUS_TYPES_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INV_ST_TYPS_TL_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_ST_TYPS_TL';
BEGIN
   if EXEC_INV_ST_TYPS_TL_PRE_INS(O_error_message,
                                  I_inv_st_typs_tl_temp_rec)=FALSE then
      return FALSE;
   end if;
   if INVADJ_SQL.MERGE_INV_STATUS_TYPES_TL(O_error_message,
                                           I_inv_st_typs_tl_temp_rec.inv_status,
                                           I_inv_st_typs_tl_temp_rec.inv_status_desc,
                                           LP_primary_lang) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_ST_TYPS_TL_INS;
---------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_ST_TYPS_TL_UPD( O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_inv_st_typs_tl_temp_rec   IN       INV_STATUS_TYPES_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INV_ST_TYPS_TL_UPD';
BEGIN
   if INVADJ_SQL.MERGE_INV_STATUS_TYPES_TL(O_error_message,
                                           I_inv_st_typs_tl_temp_rec.inv_status,
                                           I_inv_st_typs_tl_temp_rec.inv_status_desc,
                                           LP_primary_lang) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_ST_TYPS_TL_UPD;
----------------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_ST_TYPS_TL_POST_DEL( O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_inv_st_typs_tl_temp_rec   IN       INV_STATUS_TYPES_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INV_ST_TYPS_TL_POST_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_ST_TYPS_TL';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_INV_LOCK is
      select 'X'
        from inv_status_types
       where inv_status = I_inv_st_typs_tl_temp_rec.inv_status
         for update nowait;
BEGIN
   open C_INV_LOCK;
   close C_INV_LOCK;
   delete
     from inv_status_types
    where inv_status = I_inv_st_typs_tl_temp_rec.inv_status;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_inv_st_typs_tl_temp_rec.inv_status,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_INV_LOCK%ISOPEN then
         close C_INV_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_ST_TYPS_TL_POST_DEL;
--------------------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_ST_TYPS_TL_DEL( O_error_message                OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_inv_st_typs_tl_temp_rec   IN       INV_STATUS_TYPES_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INV_ST_TYPS_TL_DEL';
BEGIN
   if INVADJ_SQL.DEL_INV_STATUS_TYPES_TL(O_error_message,
                                         I_inv_st_typs_tl_temp_rec.inv_status) = FALSE then

      return FALSE;
   end if;
   if EXEC_INV_ST_TYPS_TL_POST_DEL(O_error_message,
                                   I_inv_st_typs_tl_temp_rec)=FALSE then

         return FALSE;
   end if;
   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_ST_TYPS_TL_DEL;
------------------------------------------------------------------------------------------
FUNCTION MERGE_INV_STATUS_CODES_TL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_inv_status_code        IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                                   I_inv_status_code_desc   IN       INV_STATUS_CODES_TL.INV_STATUS_CODE_DESC%TYPE,
                                   I_lang                   IN       LANG.LANG%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)                      := 'CORESVC_INV_STATUS_CODES.MERGE_INV_STATUS_CODES_TL';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_STATUS_CODES_TL';
    L_orig_lang_ind   INV_STATUS_CODES_TL.ORIG_LANG_IND%TYPE;
   L_reviewed_ind    INV_STATUS_CODES_TL.REVIEWED_IND%TYPE;
   L_status_found    VARCHAR2(1);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_INV_STATUS_CODES is
      select 'X'
        from inv_status_codes_tl
       where inv_status_code = I_inv_status_code
         and lang   = I_lang
         for update nowait;

BEGIN
   open C_INV_STATUS_CODES;
   fetch C_INV_STATUS_CODES into L_status_found;
   if C_INV_STATUS_CODES%NOTFOUND then
      L_orig_lang_ind := 'Y';
      L_reviewed_ind  := 'N';
   else
      L_orig_lang_ind := 'N';
      L_reviewed_ind  := 'Y';
   end if;
   close C_INV_STATUS_CODES;

   merge into inv_status_codes_tl iscl
      using (select I_inv_status_code      inv_status_code,
                    I_lang                 lang,
                    I_inv_status_code_desc inv_status_code_desc,
                    L_orig_lang_ind        orig_lang_ind,
                    L_reviewed_ind         reviewed_ind,
                    user                   create_id,
                    sysdate                create_datetime,
                    user                   last_update_id,
                    sysdate                last_update_datetime
               from dual) use_this
         on (iscl.inv_status_code = use_this.inv_status_code and
             iscl.lang            = use_this.lang)
      when matched then
         update
            set iscl.inv_status_code_desc = use_this.inv_status_code_desc,
                iscl.reviewed_ind = decode(iscl.orig_lang_ind, 'Y', 'N', reviewed_ind),
                     --when description is changefor the original language, set the entry for translation review
                iscl.last_update_id = use_this.last_update_id,
                iscl.last_update_datetime = use_this.last_update_datetime
      when NOT matched then
         insert (inv_status_code,
                 lang,
                 inv_status_code_desc,
                 orig_lang_ind,
                 reviewed_ind,
                 create_id,
                 create_datetime,
                 last_update_id,
                 last_update_datetime)
         values (use_this.inv_status_code,
                 use_this.lang,
                 use_this.inv_status_code_desc,
                 use_this.orig_lang_ind,
                 use_this.reviewed_ind,
                 use_this.create_id,
                 use_this.create_datetime,
                 use_this.last_update_id,
                 use_this.last_update_datetime);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'I_inv_status_code: '||I_inv_status_code,
                                            'I_lang: '||I_lang);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END MERGE_INV_STATUS_CODES_TL;
--------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_ST_CD_TL_PRE_INS(O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                                          I_inv_status_codes_tl_temp_rec   IN       INV_STATUS_CODES_TL%ROWTYPE,
                                                        I_inv_status                     IN       INV_STATUS_CODES.INV_STATUS%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_INV_STATUS_CODES.EXEC_INV_ST_CD_TL_PRE_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_STATUS_CODES_TL';
BEGIN
   insert
     into inv_status_codes
   values (I_inv_status_codes_tl_temp_rec.inv_status_code,
            I_inv_status);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_ST_CD_TL_PRE_INS;
--------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_STATUS_CODES_TL_INS(O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                                      I_inv_status_codes_tl_temp_rec   IN       INV_STATUS_CODES_TL%ROWTYPE,
                                                  I_inv_status                     IN       INV_STATUS_CODES.INV_STATUS%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_INV_STATUS_CODES.EXEC_INV_STATUS_CODES_TL_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_STATUS_CODES_TL';
BEGIN
   if EXEC_INV_ST_CD_TL_PRE_INS(O_error_message,
                                  I_inv_status_codes_tl_temp_rec,
                                             I_inv_status)=FALSE then
      return FALSE;
   end if;
    if MERGE_INV_STATUS_CODES_TL(O_error_message,
                                 I_inv_status_codes_tl_temp_rec.inv_status_code,
                                 I_inv_status_codes_tl_temp_rec.inv_status_code_desc,
                                 LP_primary_lang) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_STATUS_CODES_TL_INS;
------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_STATUS_CODES_TL_UPD(O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                                      I_inv_status_codes_tl_temp_rec   IN   INV_STATUS_CODES_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_INV_STATUS_CODES.EXEC_INV_STATUS_CODES_TL_UPD';
BEGIN
   if MERGE_INV_STATUS_CODES_TL(O_error_message,
                                I_inv_status_codes_tl_temp_rec.inv_status_code,
                                I_inv_status_codes_tl_temp_rec.inv_status_code_desc,
                                LP_primary_lang) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_STATUS_CODES_TL_UPD;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_ST_CD_TL_POST_DEL(O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                                     I_inv_status_codes_tl_temp_rec   IN       INV_STATUS_CODES_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                             := 'CORESVC_INV_STATUS_CODES.EXEC_INV_ST_CD_TL_POST_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_STATUS_CODES_TL';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_INV_CODES_LOCK is
      select 'X'
        from inv_status_codes
       where inv_status_code = I_inv_status_codes_tl_temp_rec.inv_status_code
         for update nowait;
BEGIN
   open C_INV_CODES_LOCK;
   close C_INV_CODES_LOCK;
   delete
     from inv_status_codes
    where inv_status_code = I_inv_status_codes_tl_temp_rec.inv_status_code;
    return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_inv_status_codes_tl_temp_rec.inv_status_code,
                                             NULL);
      return FALSE;
   when OTHERS then
       if C_INV_CODES_LOCK%ISOPEN then
          close C_INV_CODES_LOCK;
       end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_ST_CD_TL_POST_DEL;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_STATUS_CODES_TL_DEL(O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                                      I_inv_status_codes_tl_temp_rec   IN   INV_STATUS_CODES_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_INV_STATUS_CODES.EXEC_INV_STATUS_CODES_TL_DEL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_STATUS_CODES_TL';
    RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_INV_CODES_TL_LOCK is
      select 'X'
        from inv_status_codes_tl
       where inv_status_code = I_inv_status_codes_tl_temp_rec.inv_status_code
          for update nowait;
BEGIN
   open C_INV_CODES_TL_LOCK;
    close C_INV_CODES_TL_LOCK;
   delete
     from inv_status_codes_tl
    where inv_status_code = I_inv_status_codes_tl_temp_rec.inv_status_code;
   if EXEC_INV_ST_CD_TL_POST_DEL(O_error_message,
                                  I_inv_status_codes_tl_temp_rec) = FALSE then
       return FALSE;
    end if;
    return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_inv_status_codes_tl_temp_rec.inv_status_code,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_INV_CODES_TL_LOCK%ISOPEN then
         close C_INV_CODES_TL_LOCK;
      end if;
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_STATUS_CODES_TL_DEL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_INV_ST_TYPS_TL( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN       SVC_INV_ST_TYPS_TL.PROCESS_ID%TYPE,
                                 I_chunk_id        IN       SVC_INV_ST_TYPS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.PROCESS_INV_ST_TYPS_TL';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_ST_TYPS_TL';
   L_error                   BOOLEAN;
   L_process_error           BOOLEAN                           := FALSE;
   L_inv_st_typs_tl_temp_rec INV_STATUS_TYPES_TL%ROWTYPE;
   L_count                   NUMBER;
BEGIN
   FOR rec IN C_SVC_INV_ST_TYPS_TL(I_process_id,
                                   I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.inv_status is NOT NULL
         and rec.pk_inv_status_tl_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'INV_STATUS',
                    'PK_INV_STATUS_TL_MISSING');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new,action_mod)
           and rec.inv_status_desc  IS NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'INV_STATUS_DESC',
                     'ENTER_INV_STATUS_DESC');
         L_error := TRUE;
      end if;
      if PROCESS_INV_ST_TYPS_TL_VAL(O_error_message,
                                    L_error,
                                    rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_inv_st_typs_tl_temp_rec.inv_status              := rec.inv_status;
         L_inv_st_typs_tl_temp_rec.inv_status_desc         := rec.inv_status_desc;
         L_inv_st_typs_tl_temp_rec.create_id               := GET_USER;
         L_inv_st_typs_tl_temp_rec.create_datetime         := SYSDATE;
         L_inv_st_typs_tl_temp_rec.last_update_datetime    := SYSDATE;
         SAVEPOINT successful_inv_status;
         if rec.action = action_new then
            if EXEC_INV_ST_TYPS_TL_INS( O_error_message,
                                        L_inv_st_typs_tl_temp_rec)= FALSE then
               WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_chunk_id,
                          L_table,
                          rec.row_seq,
                          NULL,
                          O_error_message);
               ROLLBACK TO successful_inv_status;
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_INV_ST_TYPS_TL_UPD( O_error_message,
                                        L_inv_st_typs_tl_temp_rec)= FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_INV_ST_TYPS_TL_DEL( O_error_message,
                                        L_inv_st_typs_tl_temp_rec)= FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               ROLLBACK TO successful_inv_status;
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_INV_ST_TYPS_TL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_INV_STATUS_CODES_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_process_id      IN       SVC_INV_STATUS_CODES_TL.PROCESS_ID%TYPE,
                                     I_chunk_id        IN       SVC_INV_STATUS_CODES_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program       VARCHAR2(64):='CORESVC_INV_STATUS_CODES.PROCESS_INV_STATUS_CODES_TL';
    L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   :='SVC_INV_STATUS_CODES_TL';
    L_error         BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_inv_status   INV_STATUS_CODES.INV_STATUS%TYPE;
   L_INV_STATUS_CODES_TL_temp_rec INV_STATUS_CODES_TL%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_INV_STATUS_CODES_TL(I_process_id,
                                        I_chunk_id)
   LOOP
       L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      if rec.action = action_new
         and rec.pk_inv_status_code_tl_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'INV_STATUS_CODE',
                     'PK_INV_STATUS_CODE_TL');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.pk_inv_status_code_tl_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'INV_STATUS_CODE',
                     'INV_INV_STATUS_CODE');
         L_error :=TRUE;
      end if;
        if rec.action=action_new
           and rec.inv_status is NOT NULL
          and rec.inv_status_types_rid is NULL then
           WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'INV_STATUS',
                     'INV_STATUS');
         L_error :=TRUE;
        end if;
        if rec.action IN (action_new,action_mod)
          and rec.inv_status_code_desc  IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'INV_STATUS_CODE_DESC',
                        'ENTER_DESC');
            L_error :=TRUE;
      end if;
      if PROCESS_INV_ST_CD_TL_VAL( O_error_message,
                                   L_error,
                                   rec ) = FALSE then

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;
        if NOT L_error then
           SAVEPOINT successful_inv_status_code;
         L_inv_status_codes_tl_temp_rec.inv_status_code_desc          := rec.inv_status_code_desc;
         L_inv_status_codes_tl_temp_rec.inv_status_code               := rec.inv_status_code;
         L_inv_status_codes_tl_temp_rec.create_id                     := GET_USER;
           L_inv_status_codes_tl_temp_rec.create_datetime               := SYSDATE;
         L_inv_status_codes_tl_temp_rec.last_update_id                := GET_USER;
            L_inv_status_codes_tl_temp_rec.last_update_datetime          := SYSDATE;
         L_inv_status                                                 := rec.inv_status;
            if rec.action = action_new then
            if EXEC_INV_STATUS_CODES_TL_INS( O_error_message,
                                             L_inv_status_codes_tl_temp_rec,
                                             L_inv_status)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
                    ROLLBACK TO successful_inv_status_code;
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_INV_STATUS_CODES_TL_UPD( O_error_message,
                                             L_inv_status_codes_tl_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_INV_STATUS_CODES_TL_DEL( O_error_message,
                                             L_inv_status_codes_tl_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
                    ROLLBACK TO successful_inv_status_code;
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_INV_STATUS_CODES_TL;
----------------------------------------------------------------------------------------------------------
FUNCTION EXEC_IS_TYPE_LANG_INS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_is_type_lang_ins_tab   IN       IS_TYPE_LANG_TAB)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_IS_TYPE_LANG_INS';

BEGIN
   if I_is_type_lang_ins_tab is NOT NULL and I_is_type_lang_ins_tab.COUNT > 0 then
      FORALL i IN 1..I_is_type_lang_ins_tab.COUNT()
         insert into inv_status_types_tl
              values I_is_type_lang_ins_tab(i);


      FORALL i IN 1..I_is_type_lang_ins_tab.COUNT()
         update inv_status_types_tl
            set reviewed_ind = 'Y'
          where reviewed_ind = 'N'
            and inv_status = I_is_type_lang_ins_tab(i).inv_status
            and lang = LP_primary_lang;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_IS_TYPE_LANG_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_IS_TYPE_LANG_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_is_type_lang_upd_tab   IN       IS_TYPE_LANG_TAB,
                               I_is_type_tl_upd_rst     IN       ROW_SEQ_TAB,
                               I_process_id             IN       SVC_INV_STATUS_TYPES_TL.PROCESS_ID%TYPE,
                               I_chunk_id               IN       SVC_INV_STATUS_TYPES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_IS_TYPE_LANG_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_STATUS_TYPES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30)                      := NULL;
   L_key_val2      VARCHAR2(30)                      := NULL;

    --Cursor to lock the record
   cursor C_LOCK_IS_TYPE_LANG_UPD(I_inv_status   INV_STATUS_TYPES_TL.INV_STATUS%TYPE,
                                  I_lang         INV_STATUS_TYPES_TL.LANG%TYPE) is
      select 'x'
        from inv_status_types_tl
       where inv_status = I_inv_status
         and lang = I_lang
         for update nowait;

BEGIN
   if I_is_type_lang_upd_tab is NOT NULL and I_is_type_lang_upd_tab.count > 0 then
      for i in I_is_type_lang_upd_tab.FIRST..I_is_type_lang_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_is_type_lang_upd_tab(i).lang);
            L_key_val2 := 'INV_STATUS: '||to_char(I_is_type_lang_upd_tab(i).inv_status);
            open C_LOCK_IS_TYPE_LANG_UPD(I_is_type_lang_upd_tab(i).inv_status,
                                         I_is_type_lang_upd_tab(i).lang);
            close C_LOCK_IS_TYPE_LANG_UPD;
         
            update inv_status_types_tl
               set inv_status_desc = I_is_type_lang_upd_tab(i).inv_status_desc,
                   last_update_id = I_is_type_lang_upd_tab(i).last_update_id,
                   last_update_datetime = I_is_type_lang_upd_tab(i).last_update_datetime
             where lang = I_is_type_lang_upd_tab(i).lang
               and inv_status = I_is_type_lang_upd_tab(i).inv_status;
            
            update inv_status_types_tl
               set reviewed_ind = 'Y'
             where reviewed_ind = 'N'
               and inv_status = I_is_type_lang_upd_tab(i).inv_status
               and lang = LP_primary_lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_INV_STATUS_TYPES_TL',
                           I_is_type_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_IS_TYPE_LANG_UPD%ISOPEN then
         close C_LOCK_IS_TYPE_LANG_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_IS_TYPE_LANG_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_IS_TYPE_LANG_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_is_type_lang_del_tab   IN       IS_TYPE_LANG_TAB,
                               I_is_type_tl_del_rst     IN       ROW_SEQ_TAB,
                               I_process_id             IN       SVC_INV_STATUS_TYPES_TL.PROCESS_ID%TYPE,
                               I_chunk_id               IN       SVC_INV_STATUS_TYPES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_IS_TYPE_LANG_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_STATUS_TYPES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30)                      := NULL;
   L_key_val2      VARCHAR2(30)                      := NULL;

    --Cursor to lock the record
   cursor C_LOCK_IS_TYPE_LANG_DEL(I_inv_status   INV_STATUS_TYPES_TL.INV_STATUS%TYPE,
                                  I_lang         INV_STATUS_TYPES_TL.LANG%TYPE) is
      select 'x'
        from inv_status_types_tl
       where inv_status = I_inv_status
         and lang = I_lang
         for update nowait;

BEGIN
   if I_is_type_lang_del_tab is NOT NULL and I_is_type_lang_del_tab.COUNT > 0 then
      for i in I_is_type_lang_del_tab.FIRST..I_is_type_lang_del_tab.LAST loop
         BEGIN
         L_key_val1 := 'Lang: '||to_char(I_is_type_lang_del_tab(i).lang);
         L_key_val2 := 'INV_STATUS: '||to_char(I_is_type_lang_del_tab(i).inv_status);
         open C_LOCK_IS_TYPE_LANG_DEL(I_is_type_lang_del_tab(i).inv_status,
                                      I_is_type_lang_del_tab(i).lang);
         close C_LOCK_IS_TYPE_LANG_DEL;

         delete inv_status_types_tl
          where lang = I_is_type_lang_del_tab(i).lang
            and inv_status = I_is_type_lang_del_tab(i).inv_status;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_INV_STATUS_TYPES_TL',
                           I_is_type_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_IS_TYPE_LANG_DEL%ISOPEN then
         close C_LOCK_IS_TYPE_LANG_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_IS_TYPE_LANG_DEL;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_IS_TYPES_LANG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_INV_STATUS_TYPES_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_INV_STATUS_TYPES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.PROCESS_IS_TYPES_LANG';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_STATUS_TYPES_TL';
   L_base_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_STATUS_TYPES';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_STATUS_TYPES_TL';
   L_error                   BOOLEAN                           := FALSE;
   L_process_error           BOOLEAN                           := FALSE;
   L_is_type_lang_temp_rec   INV_STATUS_TYPES_TL%ROWTYPE;
   L_is_type_tl_upd_rst      ROW_SEQ_TAB;
   L_is_type_tl_del_rst      ROW_SEQ_TAB;

   cursor C_SVC_IST_TL(I_process_id NUMBER,
                       I_chunk_id NUMBER) is
      select pk_is_type_tl.rowid        as pk_is_type_tl_rid,
             fk_inv_stat_typ.rowid      as fk_inv_stat_typ_rid,
             fk_lang.rowid              as fk_lang_rid,
             pk_is_type_prim.inv_status as pk_is_type_prim_rec,
             st.lang,
             st.inv_status              as inv_status,
             st.inv_status_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)           as action,
             st.process$status
        from svc_inv_status_types_tl st,
             inv_status_types        fk_inv_stat_typ,
             inv_status_types_tl     pk_is_type_tl,
             (select inv_status
                from inv_status_types_tl
               where lang = LP_primary_lang) pk_is_type_prim,
             lang                       fk_lang
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.inv_status = fk_inv_stat_typ.inv_status (+)
         and st.lang       = pk_is_type_tl.lang (+)
         and st.inv_status = pk_is_type_tl.inv_status (+)
         and st.inv_status = pk_is_type_prim.inv_status (+)
         and st.lang       = fk_lang.lang (+);

   TYPE SVC_IST_TL is TABLE OF C_SVC_IST_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_is_type_lang_tab    SVC_IST_TL;

   L_is_type_lang_ins_tab    is_type_lang_tab                  := NEW is_type_lang_tab();
   L_is_type_lang_upd_tab    is_type_lang_tab                  := NEW is_type_lang_tab();
   L_is_type_lang_del_tab    is_type_lang_tab                  := NEW is_type_lang_tab();

BEGIN
   if C_SVC_IST_TL%ISOPEN then
      close C_SVC_IST_TL;
   end if;

   open C_SVC_IST_TL(I_process_id,
                     I_chunk_id);
   LOOP
      fetch C_SVC_IST_TL bulk collect into L_svc_is_type_lang_tab limit LP_bulk_fetch_limit;
      if L_svc_is_type_lang_tab.COUNT > 0 then
         FOR i in L_svc_is_type_lang_tab.FIRST..L_svc_is_type_lang_tab.LAST LOOP
         L_error := FALSE;

            --check for primary_lang
            if L_svc_is_type_lang_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_type_lang_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;
            if L_svc_is_type_lang_tab(i).action = action_new and L_svc_is_type_lang_tab(i).lang <> LP_primary_lang and L_svc_is_type_lang_tab(i).pk_is_type_tl_rid is NULL then
               if L_svc_is_type_lang_tab(i).pk_is_type_prim_rec is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_is_type_lang_tab(i).row_seq,
                              'LANG',
                              'PRIMARY_LANG_REQ');
                  L_error := TRUE;
               end if;
            end if;

            -- check if action is valid
            if L_svc_is_type_lang_tab(i).action is NULL
               or L_svc_is_type_lang_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_type_lang_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_is_type_lang_tab(i).action = action_new
               and L_svc_is_type_lang_tab(i).pk_is_type_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_type_lang_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_is_type_lang_tab(i).action IN (action_mod, action_del)
               and L_svc_is_type_lang_tab(i).lang is NOT NULL
               and L_svc_is_type_lang_tab(i).inv_status is NOT NULL
               and L_svc_is_type_lang_tab(i).pk_is_type_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_is_type_lang_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            -- check for FK
            if L_svc_is_type_lang_tab(i).action = action_new
               and L_svc_is_type_lang_tab(i).inv_status is NOT NULL
               and L_svc_is_type_lang_tab(i).fk_inv_stat_typ_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_is_type_lang_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_is_type_lang_tab(i).action = action_new
               and L_svc_is_type_lang_tab(i).lang is NOT NULL
               and L_svc_is_type_lang_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_type_lang_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_is_type_lang_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_type_lang_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error := TRUE;
            end if;

            if L_svc_is_type_lang_tab(i).inv_status is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_type_lang_tab(i).row_seq,
                           'INV_STATUS',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_is_type_lang_tab(i).action in (action_new, action_mod) then
               if L_svc_is_type_lang_tab(i).inv_status_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_is_type_lang_tab(i).row_seq,
                              'INV_STATUS_DESC',
                              'MUST_ENTER_FIELD');
                  L_error := TRUE;
               end if;
            end if;

            if NOT L_error then
               L_is_type_lang_temp_rec.lang := L_svc_is_type_lang_tab(i).lang;
               L_is_type_lang_temp_rec.inv_status := L_svc_is_type_lang_tab(i).inv_status;
               L_is_type_lang_temp_rec.inv_status_desc := L_svc_is_type_lang_tab(i).inv_status_desc;
               L_is_type_lang_temp_rec.orig_lang_ind := 'N';
               L_is_type_lang_temp_rec.reviewed_ind := 'Y';
               L_is_type_lang_temp_rec.create_datetime := SYSDATE;
               L_is_type_lang_temp_rec.create_id := GET_USER;
               L_is_type_lang_temp_rec.last_update_datetime := SYSDATE;
               L_is_type_lang_temp_rec.last_update_id := GET_USER;

               if L_svc_is_type_lang_tab(i).action = action_new then
                  L_is_type_lang_ins_tab.extend;
                  L_is_type_lang_ins_tab(L_is_type_lang_ins_tab.count()) := L_is_type_lang_temp_rec;
               end if;

               if L_svc_is_type_lang_tab(i).action = action_mod then
                  L_is_type_lang_upd_tab.extend;
                  L_is_type_lang_upd_tab(L_is_type_lang_upd_tab.count()) := L_is_type_lang_temp_rec;
                  L_is_type_tl_upd_rst(L_is_type_lang_upd_tab.count()) := L_svc_is_type_lang_tab(i).row_seq;
               end if;

               if L_svc_is_type_lang_tab(i).action = action_del then
                  L_is_type_lang_del_tab.extend;
                  L_is_type_lang_del_tab(L_is_type_lang_del_tab.count()) := L_is_type_lang_temp_rec;
                  L_is_type_tl_del_rst(L_is_type_lang_del_tab.count()) := L_svc_is_type_lang_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_IST_TL%NOTFOUND;
   END LOOP;
   close C_SVC_IST_TL;

   if EXEC_IS_TYPE_LANG_INS(O_error_message,
                            L_is_type_lang_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_IS_TYPE_LANG_UPD(O_error_message,
                            L_is_type_lang_upd_tab,
                            L_is_type_tl_upd_rst,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
      return FALSE;
   end if;

   if EXEC_IS_TYPE_LANG_DEL(O_error_message,
                            L_is_type_lang_del_tab,
                            L_is_type_tl_del_rst,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_IS_TYPES_LANG;
--------------------------------------------------------------------------------------------------
FUNCTION EXEC_IS_CODE_LANG_INS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_is_code_lang_ins_tab   IN       IS_CODE_LANG_TAB)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_IS_CODE_LANG_INS';

BEGIN
   if I_is_code_lang_ins_tab is NOT NULL and I_is_code_lang_ins_tab.COUNT > 0 then
      FORALL i IN 1..I_is_code_lang_ins_tab.COUNT()
         insert into inv_status_codes_tl
              values I_is_code_lang_ins_tab(i);


      FORALL i IN 1..I_is_code_lang_ins_tab.COUNT()
         update inv_status_codes_tl
            set reviewed_ind = 'Y'
          where reviewed_ind = 'N'
            and inv_status_code = I_is_code_lang_ins_tab(i).inv_status_code
            and lang = LP_primary_lang;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_IS_CODE_LANG_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_IS_CODE_LANG_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_is_code_lang_upd_tab   IN       IS_CODE_LANG_TAB,
                               I_is_codes_lang_upd_rst  IN       ROW_SEQ_TAB,
                               I_process_id             IN       SVC_INV_STATUS_CODES_TL.PROCESS_ID%TYPE,
                               I_chunk_id               IN       SVC_INV_STATUS_CODES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_IS_CODE_LANG_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_STATUS_CODES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30)                      := NULL;
   L_key_val2      VARCHAR2(30)                      := NULL;

    --Cursor to lock the record
   cursor C_LOCK_IS_CODE_LANG_UPD(I_inv_status_code   INV_STATUS_CODES_TL.INV_STATUS_CODE%TYPE,
                                  I_lang              INV_STATUS_CODES_TL.LANG%TYPE) is
      select 'x'
        from inv_status_codes_tl
       where inv_status_code = I_inv_status_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_is_code_lang_upd_tab is NOT NULL and I_is_code_lang_upd_tab.count > 0 then
      for i in I_is_code_lang_upd_tab.FIRST..I_is_code_lang_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_is_code_lang_upd_tab(i).lang);
            L_key_val2 := 'INV_STATUS_CODE: '||to_char(I_is_code_lang_upd_tab(i).inv_status_code);
            open C_LOCK_IS_CODE_LANG_UPD(I_is_code_lang_upd_tab(i).inv_status_code,
                                         I_is_code_lang_upd_tab(i).lang);
            close C_LOCK_IS_CODE_LANG_UPD;
            
            update inv_status_codes_tl
               set inv_status_code_desc = I_is_code_lang_upd_tab(i).inv_status_code_desc,
                   last_update_id = I_is_code_lang_upd_tab(i).last_update_id,
                   last_update_datetime = I_is_code_lang_upd_tab(i).last_update_datetime
             where lang = I_is_code_lang_upd_tab(i).lang
               and inv_status_code = I_is_code_lang_upd_tab(i).inv_status_code;
           
            update inv_status_codes_tl
               set reviewed_ind = 'Y'
             where reviewed_ind = 'N'
               and inv_status_code = I_is_code_lang_upd_tab(i).inv_status_code
               and lang = LP_primary_lang;
               
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_INV_STATUS_CODES_TL',
                           I_is_codes_lang_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_IS_CODE_LANG_UPD%ISOPEN then
         close C_LOCK_IS_CODE_LANG_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_IS_CODE_LANG_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_IS_CODE_LANG_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_is_code_lang_del_tab   IN       IS_CODE_LANG_TAB,
                               I_is_codes_lang_del_rst  IN       ROW_SEQ_TAB,
                               I_process_id             IN       SVC_INV_STATUS_CODES_TL.PROCESS_ID%TYPE,
                               I_chunk_id               IN       SVC_INV_STATUS_CODES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_IS_CODE_LANG_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_STATUS_CODES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30)                      := NULL;
   L_key_val2      VARCHAR2(30)                      := NULL;

    --Cursor to lock the record
   cursor C_LOCK_IS_CODE_LANG_DEL(I_inv_status_code   INV_STATUS_CODES_TL.INV_STATUS_CODE%TYPE,
                                  I_lang              INV_STATUS_CODES_TL.LANG%TYPE) is
      select 'x'
        from inv_status_codes_tl
       where inv_status_code = I_inv_status_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_is_code_lang_del_tab is NOT NULL and I_is_code_lang_del_tab.COUNT > 0 then
      for i in I_is_code_lang_del_tab.FIRST..I_is_code_lang_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_is_code_lang_del_tab(i).lang);
            L_key_val2 := 'INV_STATUS_CODE: '||to_char(I_is_code_lang_del_tab(i).inv_status_code);
            open C_LOCK_IS_CODE_LANG_DEL(I_is_code_lang_del_tab(i).inv_status_code,
                                         I_is_code_lang_del_tab(i).lang);
            close C_LOCK_IS_CODE_LANG_DEL;
            
            delete inv_status_codes_tl
             where lang = I_is_code_lang_del_tab(i).lang
               and inv_status_code = I_is_code_lang_del_tab(i).inv_status_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_INV_STATUS_CODES_TL',
                           I_is_codes_lang_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_IS_CODE_LANG_DEL%ISOPEN then
         close C_LOCK_IS_CODE_LANG_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_IS_CODE_LANG_DEL;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_IS_CODES_LANG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_INV_STATUS_CODES_LANG_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_INV_STATUS_CODES_LANG_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.PROCESS_IS_CODES_LANG';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_STATUS_CODES_TL';
   L_base_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_STATUS_CODES';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_STATUS_CODES_LANG_TL';
   L_error                   BOOLEAN                           := FALSE;
   L_process_error           BOOLEAN                           := FALSE;
   L_is_code_lang_temp_rec   INV_STATUS_CODES_TL%ROWTYPE;
   L_is_codes_lang_upd_rst   ROW_SEQ_TAB;
   L_is_codes_lang_del_rst   ROW_SEQ_TAB;

   cursor C_SVC_ISC_TL(I_process_id NUMBER,
                       I_chunk_id NUMBER) is
      select pk_is_code_tl.rowid             as pk_is_code_tl_rid,
             fk_inv_stat_cd.rowid            as fk_inv_stat_cd_rid,
             fk_lang.rowid                   as fk_lang_rid,
             pk_is_code_prim.inv_status_code as pk_is_code_prim_rec,
             st.lang,
             UPPER(st.inv_status_code)       as inv_status_code,
             st.inv_status_code_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)                as action,
             st.process$status
        from svc_inv_status_codes_lang_tl st,
             inv_status_codes        fk_inv_stat_cd,
             inv_status_codes_tl     pk_is_code_tl,
             (select inv_status_code
                from inv_status_codes_tl
               where lang = LP_primary_lang) pk_is_code_prim,
             lang                       fk_lang
       where st.process_id      = I_process_id
         and st.chunk_id        = I_chunk_id
         and st.inv_status_code = fk_inv_stat_cd.inv_status_code (+)
         and st.lang            = pk_is_code_tl.lang (+)
         and st.inv_status_code = pk_is_code_tl.inv_status_code (+)
         and st.inv_status_code = pk_is_code_prim.inv_status_code (+)
         and st.lang            = fk_lang.lang (+);

   TYPE SVC_ISC_TL is TABLE OF C_SVC_ISC_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_is_code_lang_tab    SVC_ISC_TL;

   L_is_code_lang_ins_tab    is_code_lang_tab                  := NEW is_code_lang_tab();
   L_is_code_lang_upd_tab    is_code_lang_tab                  := NEW is_code_lang_tab();
   L_is_code_lang_del_tab    is_code_lang_tab                  := NEW is_code_lang_tab();

BEGIN
   if C_SVC_ISC_TL%ISOPEN then
      close C_SVC_ISC_TL;
   end if;

   open C_SVC_ISC_TL(I_process_id,
                     I_chunk_id);
   LOOP
      fetch C_SVC_ISC_TL bulk collect into L_svc_is_code_lang_tab limit LP_bulk_fetch_limit;
      if L_svc_is_code_lang_tab.COUNT > 0 then
         FOR i in L_svc_is_code_lang_tab.FIRST..L_svc_is_code_lang_tab.LAST LOOP
         L_error := FALSE;

            --check for primary_lang
            if L_svc_is_code_lang_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_code_lang_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG');
               continue;
            end if;
            if L_svc_is_code_lang_tab(i).action = action_new and L_svc_is_code_lang_tab(i).lang <> LP_primary_lang and L_svc_is_code_lang_tab(i).pk_is_code_tl_rid is NULL then
               if L_svc_is_code_lang_tab(i).pk_is_code_prim_rec is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_is_code_lang_tab(i).row_seq,
                              'LANG',
                              'PRIMARY_LANG_REQ');
                  L_error := TRUE;
               end if;
            end if;

            -- check if action is valid
            if L_svc_is_code_lang_tab(i).action is NULL
               or L_svc_is_code_lang_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_code_lang_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_is_code_lang_tab(i).action = action_new
               and L_svc_is_code_lang_tab(i).pk_is_code_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_code_lang_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_is_code_lang_tab(i).action IN (action_mod, action_del)
               and L_svc_is_code_lang_tab(i).lang is NOT NULL
               and L_svc_is_code_lang_tab(i).inv_status_code is NOT NULL
               and L_svc_is_code_lang_tab(i).pk_is_code_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_is_code_lang_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            -- check for FK
            if L_svc_is_code_lang_tab(i).action = action_new
               and L_svc_is_code_lang_tab(i).inv_status_code is NOT NULL
               and L_svc_is_code_lang_tab(i).fk_inv_stat_cd_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_is_code_lang_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_is_code_lang_tab(i).action = action_new
               and L_svc_is_code_lang_tab(i).lang is NOT NULL
               and L_svc_is_code_lang_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_code_lang_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_is_code_lang_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_code_lang_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error := TRUE;
            end if;

            if L_svc_is_code_lang_tab(i).inv_status_code is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_is_code_lang_tab(i).row_seq,
                           'INV_STATUS',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_is_code_lang_tab(i).action in (action_new, action_mod) then
               if L_svc_is_code_lang_tab(i).inv_status_code_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_is_code_lang_tab(i).row_seq,
                              'INV_STATUS_DESC',
                              'MUST_ENTER_FIELD');
                  L_error := TRUE;
               end if;
            end if;

            if NOT L_error then
               L_is_code_lang_temp_rec.lang := L_svc_is_code_lang_tab(i).lang;
               L_is_code_lang_temp_rec.inv_status_code := L_svc_is_code_lang_tab(i).inv_status_code;
               L_is_code_lang_temp_rec.inv_status_code_desc := L_svc_is_code_lang_tab(i).inv_status_code_desc;
               L_is_code_lang_temp_rec.orig_lang_ind := 'N';
               L_is_code_lang_temp_rec.reviewed_ind := 'Y';
               L_is_code_lang_temp_rec.create_datetime := SYSDATE;
               L_is_code_lang_temp_rec.create_id := GET_USER;
               L_is_code_lang_temp_rec.last_update_datetime := SYSDATE;
               L_is_code_lang_temp_rec.last_update_id := GET_USER;

               if L_svc_is_code_lang_tab(i).action = action_new then
                  L_is_code_lang_ins_tab.extend;
                  L_is_code_lang_ins_tab(L_is_code_lang_ins_tab.count()) := L_is_code_lang_temp_rec;
               end if;

               if L_svc_is_code_lang_tab(i).action = action_mod then
                  L_is_code_lang_upd_tab.extend;
                  L_is_code_lang_upd_tab(L_is_code_lang_upd_tab.count()) := L_is_code_lang_temp_rec;
                  L_is_codes_lang_upd_rst(L_is_code_lang_upd_tab.count()) := L_svc_is_code_lang_tab(i).row_seq;
               end if;

               if L_svc_is_code_lang_tab(i).action = action_del then
                  L_is_code_lang_del_tab.extend;
                  L_is_code_lang_del_tab(L_is_code_lang_del_tab.count()) := L_is_code_lang_temp_rec;
                  L_is_codes_lang_del_rst(L_is_code_lang_del_tab.count()) := L_svc_is_code_lang_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_ISC_TL%NOTFOUND;
   END LOOP;
   close C_SVC_ISC_TL;

   if EXEC_IS_CODE_LANG_INS(O_error_message,
                            L_is_code_lang_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_IS_CODE_LANG_UPD(O_error_message,
                            L_is_code_lang_upd_tab,
                            L_is_codes_lang_upd_rst,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_IS_CODE_LANG_DEL(O_error_message,
                            L_is_code_lang_del_tab,
                            L_is_codes_lang_del_rst,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_IS_CODES_LANG;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_INV_ADJ_REASON_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error           IN OUT   BOOLEAN,
                                    I_rec             IN       C_SVC_INV_ADJ_REASON%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                     := 'CORESVC_INV_STAT_TYP_ADJ_RSN.PROCESS_INV_ADJ_REASON_VAL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:= 'SVC_INV_ADJ_REASON';
BEGIN
   if I_rec.action IN (action_new,action_mod)
       and I_rec.reason_desc  IS NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'REASON_DESC',
                  'NO_INV_REASON_DESC');
      O_error :=TRUE;
   end if;

   if I_rec.action IN (action_new,action_mod)
       and ( I_rec.reason < 1 ) then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'REASON',
                  'INVADJ_REASON_ATLEAST_1');
      O_error :=TRUE;
   end if;

   if I_rec.action IN (action_new,action_mod)
       and ( I_rec.reason = 98) then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'REASON',
                  'NO_REASON_98');
      O_error :=TRUE;
   end if;
   if I_rec.action = action_mod
       and I_rec.cogs_ind is NOT NULL
        and I_rec.cogs_ind <> I_rec.old_cogs_ind then
        WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'COGS_IND',
                  'CANNOT_MOD_COGS_IND');
      O_error :=TRUE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_INV_ADJ_REASON_VAL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_ADJ_REASON_POST_INS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_rec           IN     C_SVC_INV_ADJ_REASON%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                       := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INV_ADJ_REASON_POST_INS';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_INV_ADJ_REASON';
   L_exists         BOOLEAN;
BEGIN
   if INVADJ_SQL.MERGE_INV_ADJ_REASON_TL(O_error_message,
                                         I_rec.reason,
                                         I_rec.reason_desc,
                                         LP_primary_lang) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.CHUNK_ID,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      ROLLBACK TO successful_inv_status;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_INV_ADJ_REASON_POST_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_INV_ADJ_REASON_INS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_inv_adj_reason_temp_rec   IN       INV_ADJ_REASON%ROWTYPE,
                                 I_rec                       IN       C_SVC_INV_ADJ_REASON%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INV_ADJ_REASON_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_ADJ_REASON';
BEGIN
   insert into inv_adj_reason
        values I_inv_adj_reason_temp_rec;

   if EXEC_INV_ADJ_REASON_POST_INS(O_error_message,
                                   I_rec) = FALSE then
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_ADJ_REASON_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_INV_ADJ_REASON_PRE_DEL(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_error         IN OUT   BOOLEAN,
                                     I_rec           IN       C_SVC_INV_ADJ_REASON%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                       := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INV_ADJ_REASON_PRE_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_INV_ADJ_REASON';
   L_exists         BOOLEAN;
   L_found          BOOLEAN                            := FALSE;

BEGIN
   if INVADJ_VALIDATE_SQL.INV_REASON_CONSTRAINTS_EXIST( I_rec.reason,
                                                        O_error_message,
                                                        L_found) = FALSE then
      O_error := TRUE;
   end if;
   if L_found then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'REASON',
                  'INV_REAS_FRGN_CONSTRAINT');
      O_error := TRUE;
   else
      if INVADJ_SQL.DEL_INV_ADJ_REASON_TL(O_error_message,
                                          I_rec.reason) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'REASON',
                     O_error_message);
         O_error := TRUE;
         ROLLBACK TO successful_inv_status;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END EXEC_INV_ADJ_REASON_PRE_DEL;

--------------------------------------------------------------------------------
FUNCTION EXEC_INV_ADJ_REASON_DEL( O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error                   IN OUT   BOOLEAN,
                                  I_rec                     IN       C_SVC_INV_ADJ_REASON%ROWTYPE )
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INV_ADJ_REASON_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'INV_ADJ_REASON';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the FREIGHT_TYPE record
   cursor C_LOCK_INV_ADJ_REASON_DEL is
      select 'x'
        from inv_adj_reason
       where reason = I_rec.reason
         for update nowait;
BEGIN

   if EXEC_INV_ADJ_REASON_PRE_DEL(O_error_message,
                                  O_error,
                                  I_rec) = FALSE then
      return FALSE;
   else
      if NOT O_error then
         open  C_LOCK_INV_ADJ_REASON_DEL;
         close C_LOCK_INV_ADJ_REASON_DEL;

         delete from inv_adj_reason
            where reason = I_rec.reason;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_rec.reason,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_INV_ADJ_REASON_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_INV_ADJ_REASON( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id   IN   SVC_INV_ADJ_REASON.PROCESS_ID%TYPE,
                                 I_chunk_id     IN   SVC_INV_ADJ_REASON.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program               VARCHAR2(64)                         :='CORESVC_INV_STAT_TYP_ADJ_RSN.PROCESS_INV_ADJ_REASON';
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    :='SVC_INV_ADJ_REASON';
   L_error                 BOOLEAN;
   L_process_error         BOOLEAN                              := FALSE;
   inv_adj_reason_temp_rec INV_ADJ_REASON%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_INV_ADJ_REASON(I_process_id,
                                   I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.pk_inv_adj_reason_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'REASON',
                     'NO_DUPLICATE_REASON');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.pk_inv_adj_reason_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'REASON',
                     'INVADJ_REASON_MISSING');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new,action_mod)
           and rec.cogs_ind NOT IN  ( 'Y','N' )   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COGS_IND',
                     'INV_Y_N_IND');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_new,action_mod)
           and rec.cogs_ind  IS NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COGS_IND',
                     'INVADJ_COGS_IND_NULL');
         L_error :=TRUE;
      end if;

      --Customised Validations
      SAVEPOINT successful_inv_status;
      if PROCESS_INV_ADJ_REASON_VAL(O_error_message,
                                    L_error,
                                    rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then

         L_process_error := FALSE;
         inv_adj_reason_temp_rec.reason              := rec.reason;
         inv_adj_reason_temp_rec.cogs_ind            := rec.cogs_ind;
         if rec.action = action_new then
            if EXEC_INV_ADJ_REASON_INS(O_error_message,
                                       inv_adj_reason_temp_rec,
                                       rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if INVADJ_SQL.MERGE_INV_ADJ_REASON_TL(O_error_message,
                                                  rec.reason,
                                                  rec.reason_desc,
                                                  LP_primary_lang) = FALSE then
               WRITE_ERROR(rec.PROCESS_ID,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           rec.CHUNK_ID,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               ROLLBACK TO successful_inv_status;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_INV_ADJ_REASON_DEL(O_error_message,
                                       L_process_error,
                                       rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_INV_ADJ_REASON;
------------------------------------------------------------------------------------------------
FUNCTION EXEC_INVADJ_RSN_TL_INS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_invadj_rsn_tl_ins_tab   IN       INVADJ_TL_TAB)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INVADJ_RSN_TL_INS';

BEGIN
   if I_invadj_rsn_tl_ins_tab is NOT NULL and I_invadj_rsn_tl_ins_tab.COUNT > 0 then
      FORALL i IN 1..I_invadj_rsn_tl_ins_tab.COUNT()
         insert into inv_adj_reason_tl
              values I_invadj_rsn_tl_ins_tab(i);

      FORALL i IN 1..I_invadj_rsn_tl_ins_tab.COUNT()
         update inv_adj_reason_tl
            set reviewed_ind = 'Y'
          where reviewed_ind = 'N'
            and reason = I_invadj_rsn_tl_ins_tab(i).reason
            and lang = LP_primary_lang;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_INVADJ_RSN_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_INVADJ_RSN_TL_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_invadj_rsn_tl_upd_tab   IN       INVADJ_TL_TAB,
                                I_invadj_rsn_tl_upd_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_INV_ADJ_REASON_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_INV_ADJ_REASON_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INVADJ_RSN_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_ADJ_REASON_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30)                      := NULL;
   L_key_val2      VARCHAR2(30)                      := NULL;

    --Cursor to lock the record
   cursor C_LOCK_INVADJ_RSN_TL_UPD(I_reason   INV_ADJ_REASON_TL.REASON%TYPE,
                                   I_lang     INV_ADJ_REASON_TL.LANG%TYPE) is
      select 'x'
        from inv_adj_reason_tl
       where reason = I_reason
         and lang = I_lang
         for update nowait;

BEGIN
   if I_invadj_rsn_tl_upd_tab is NOT NULL and I_invadj_rsn_tl_upd_tab.count > 0 then
      for i in I_invadj_rsn_tl_upd_tab.FIRST..I_invadj_rsn_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_invadj_rsn_tl_upd_tab(i).lang);
            L_key_val2 := 'Reason: '||to_char(I_invadj_rsn_tl_upd_tab(i).reason);
            open C_LOCK_INVADJ_RSN_TL_UPD(I_invadj_rsn_tl_upd_tab(i).reason,
                                         I_invadj_rsn_tl_upd_tab(i).lang);
            close C_LOCK_INVADJ_RSN_TL_UPD;
            
            update inv_adj_reason_tl
               set reason_desc = I_invadj_rsn_tl_upd_tab(i).reason_desc,
                   last_update_id = I_invadj_rsn_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_invadj_rsn_tl_upd_tab(i).last_update_datetime
             where lang = I_invadj_rsn_tl_upd_tab(i).lang
               and reason = I_invadj_rsn_tl_upd_tab(i).reason;
            
            update inv_adj_reason_tl
               set reviewed_ind = 'Y'
             where reviewed_ind = 'N'
               and reason = I_invadj_rsn_tl_upd_tab(i).reason
               and lang = LP_primary_lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_INV_ADJ_REASON_TL',
                           I_invadj_rsn_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_INVADJ_RSN_TL_UPD%ISOPEN then
         close C_LOCK_INVADJ_RSN_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_INVADJ_RSN_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_INVADJ_RSN_TL_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_invadj_rsn_tl_del_tab   IN       INVADJ_TL_TAB,
                                I_invadj_rsn_tl_del_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_INV_ADJ_REASON_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_INV_ADJ_REASON_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.EXEC_INVADJ_RSN_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_ADJ_REASON_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30)                      := NULL;
   L_key_val2      VARCHAR2(30)                      := NULL;

    --Cursor to lock the record
   cursor C_LOCK_INVADJ_RSN_TL_DEL(I_reason   INV_ADJ_REASON_TL.REASON%TYPE,
                                   I_lang     INV_ADJ_REASON_TL.LANG%TYPE) is
      select 'x'
        from inv_adj_reason_tl
       where reason = I_reason
         and lang = I_lang
         for update nowait;

BEGIN
   if I_invadj_rsn_tl_del_tab is NOT NULL and I_invadj_rsn_tl_del_tab.COUNT > 0 then
      for i in I_invadj_rsn_tl_del_tab.FIRST..I_invadj_rsn_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_invadj_rsn_tl_del_tab(i).lang);
            L_key_val2 := 'Reason: '||to_char(I_invadj_rsn_tl_del_tab(i).reason);
            open C_LOCK_INVADJ_RSN_TL_DEL(I_invadj_rsn_tl_del_tab(i).reason,
                                         I_invadj_rsn_tl_del_tab(i).lang);
            close C_LOCK_INVADJ_RSN_TL_DEL;
            
            delete inv_adj_reason_tl
             where lang = I_invadj_rsn_tl_del_tab(i).lang
               and reason = I_invadj_rsn_tl_del_tab(i).reason;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_INV_ADJ_REASON_TL',
                           I_invadj_rsn_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_INVADJ_RSN_TL_DEL%ISOPEN then
         close C_LOCK_INVADJ_RSN_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_INVADJ_RSN_TL_DEL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_INV_ADJ_RSN_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_INV_ADJ_REASON_TL.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_INV_ADJ_REASON_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)                      := 'CORESVC_INV_STAT_TYP_ADJ_RSN.PROCESS_INV_ADJ_RSN_TL';
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_ADJ_REASON_TL';
   L_base_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'INV_ADJ_REASON';
   L_table                    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_INV_ADJ_REASON_TL';
   L_error                    BOOLEAN                           := FALSE;
   L_process_error            BOOLEAN                           := FALSE;
   L_invadj_rsn_tl_temp_rec   INV_ADJ_REASON_TL%ROWTYPE;
   L_invadj_rsn_tl_upd_rst    ROW_SEQ_TAB;
   L_invadj_rsn_tl_del_rst    ROW_SEQ_TAB;
   
   cursor C_SVC_IAR_TL(I_process_id   NUMBER,
                       I_chunk_id     NUMBER) is
      select pk_invadj_rsn_tl.rowid       as pk_invadj_rsn_tl_rid,
             fk_invadj_rsn.rowid          as fk_invadj_rsn_rid,
             fk_lang.rowid                as fk_lang_rid,
             pk_invadj_rsn_tl_prim.reason as pk_invadj_rsn_tl_prim_rec,
             st.lang,
             st.reason                    as reason,
             st.reason_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)             as action,
             st.process$status
        from svc_inv_adj_reason_tl st,
             inv_adj_reason        fk_invadj_rsn,
             inv_adj_reason_tl     pk_invadj_rsn_tl,
             (select reason
                from inv_adj_reason_tl
               where lang = LP_primary_lang) pk_invadj_rsn_tl_prim,
             lang                       fk_lang
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.reason     = fk_invadj_rsn.reason (+)
         and st.lang       = pk_invadj_rsn_tl.lang (+)
         and st.reason     = pk_invadj_rsn_tl.reason (+)
         and st.reason     = pk_invadj_rsn_tl_prim.reason (+)
         and st.lang       = fk_lang.lang (+);

   TYPE SVC_IAR_TL is TABLE OF C_SVC_IAR_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_invadj_rsn_tl_tab    SVC_IAR_TL;

   L_invadj_rsn_tl_ins_tab    invadj_tl_tab                  := NEW invadj_tl_tab();
   L_invadj_rsn_tl_upd_tab    invadj_tl_tab                  := NEW invadj_tl_tab();
   L_invadj_rsn_tl_del_tab    invadj_tl_tab                  := NEW invadj_tl_tab();

BEGIN
   if C_SVC_IAR_TL%ISOPEN then
      close C_SVC_IAR_TL;
   end if;

   open C_SVC_IAR_TL(I_process_id,
                     I_chunk_id);
   LOOP
      fetch C_SVC_IAR_TL bulk collect into L_svc_invadj_rsn_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_invadj_rsn_tl_tab.COUNT > 0 then
         FOR i in L_svc_invadj_rsn_tl_tab.FIRST..L_svc_invadj_rsn_tl_tab.LAST LOOP
         L_error := FALSE;

            --check for primary_lang
            if L_svc_invadj_rsn_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_invadj_rsn_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;
            if L_svc_invadj_rsn_tl_tab(i).action = action_new and L_svc_invadj_rsn_tl_tab(i).lang <> LP_primary_lang and L_svc_invadj_rsn_tl_tab(i).pk_invadj_rsn_tl_rid is NULL then
               if L_svc_invadj_rsn_tl_tab(i).pk_invadj_rsn_tl_prim_rec is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_invadj_rsn_tl_tab(i).row_seq,
                              'LANG',
                              'PRIMARY_LANG_REQ');
                  L_error := TRUE;
               end if;
            end if;

            -- check if action is valid
            if L_svc_invadj_rsn_tl_tab(i).action is NULL
               or L_svc_invadj_rsn_tl_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_invadj_rsn_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_invadj_rsn_tl_tab(i).action = action_new
               and L_svc_invadj_rsn_tl_tab(i).pk_invadj_rsn_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_invadj_rsn_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_invadj_rsn_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_invadj_rsn_tl_tab(i).lang is NOT NULL
               and L_svc_invadj_rsn_tl_tab(i).reason is NOT NULL
               and L_svc_invadj_rsn_tl_tab(i).pk_invadj_rsn_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_invadj_rsn_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            -- check for FK
            if L_svc_invadj_rsn_tl_tab(i).action = action_new
               and L_svc_invadj_rsn_tl_tab(i).reason is NOT NULL
               and L_svc_invadj_rsn_tl_tab(i).fk_invadj_rsn_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_invadj_rsn_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_invadj_rsn_tl_tab(i).action = action_new
               and L_svc_invadj_rsn_tl_tab(i).lang is NOT NULL
               and L_svc_invadj_rsn_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_invadj_rsn_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_invadj_rsn_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_invadj_rsn_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error := TRUE;
            end if;

            if L_svc_invadj_rsn_tl_tab(i).reason is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_invadj_rsn_tl_tab(i).row_seq,
                           'REASON',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_invadj_rsn_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_invadj_rsn_tl_tab(i).reason_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_invadj_rsn_tl_tab(i).row_seq,
                              'REASON_DESC',
                              'MUST_ENTER_FIELD');
                  L_error := TRUE;
               end if;
            end if;

            if NOT L_error then
               L_invadj_rsn_tl_temp_rec.lang := L_svc_invadj_rsn_tl_tab(i).lang;
               L_invadj_rsn_tl_temp_rec.reason := L_svc_invadj_rsn_tl_tab(i).reason;
               L_invadj_rsn_tl_temp_rec.reason_desc := L_svc_invadj_rsn_tl_tab(i).reason_desc;
               L_invadj_rsn_tl_temp_rec.orig_lang_ind := 'N';
               L_invadj_rsn_tl_temp_rec.reviewed_ind := 'Y';
               L_invadj_rsn_tl_temp_rec.create_datetime := SYSDATE;
               L_invadj_rsn_tl_temp_rec.create_id := GET_USER;
               L_invadj_rsn_tl_temp_rec.last_update_datetime := SYSDATE;
               L_invadj_rsn_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_invadj_rsn_tl_tab(i).action = action_new then
                  L_invadj_rsn_tl_ins_tab.extend;
                  L_invadj_rsn_tl_ins_tab(L_invadj_rsn_tl_ins_tab.count()) := L_invadj_rsn_tl_temp_rec;
               end if;

               if L_svc_invadj_rsn_tl_tab(i).action = action_mod then
                  L_invadj_rsn_tl_upd_tab.extend;
                  L_invadj_rsn_tl_upd_tab(L_invadj_rsn_tl_upd_tab.count()) := L_invadj_rsn_tl_temp_rec;
                  L_invadj_rsn_tl_upd_rst(L_invadj_rsn_tl_upd_tab.count()) := L_svc_invadj_rsn_tl_tab(i).row_seq;
               end if;

               if L_svc_invadj_rsn_tl_tab(i).action = action_del then
                  L_invadj_rsn_tl_del_tab.extend;
                  L_invadj_rsn_tl_del_tab(L_invadj_rsn_tl_del_tab.count()) := L_invadj_rsn_tl_temp_rec;
                  L_invadj_rsn_tl_del_rst(L_invadj_rsn_tl_del_tab.count()) := L_svc_invadj_rsn_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_IAR_TL%NOTFOUND;
   END LOOP;
   close C_SVC_IAR_TL;

   if EXEC_INVADJ_RSN_TL_INS(O_error_message,
                             L_invadj_rsn_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_INVADJ_RSN_TL_UPD(O_error_message,
                             L_invadj_rsn_tl_upd_tab,
                             L_invadj_rsn_tl_upd_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_INVADJ_RSN_TL_DEL(O_error_message,
                             L_invadj_rsn_tl_del_tab,
                             L_invadj_rsn_tl_del_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_INV_ADJ_RSN_TL;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete from svc_inv_status_codes_lang_tl
    where process_id = I_process_id;

   delete from svc_inv_status_types_tl
    where process_id = I_process_id;

   delete
     from svc_inv_status_codes_tl
    where process_id = I_process_id;

    delete
     from svc_inv_st_typs_tl
    where process_id = I_process_id;
    
   delete
     from svc_inv_adj_reason_tl
    where process_id = I_process_id;

   delete
     from svc_inv_adj_reason
    where process_id = I_process_id;

END;
--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)                    := 'CORESVC_INV_STAT_TYP_ADJ_RSN.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW LP_errors_tab_typ();
   if PROCESS_INV_ST_TYPS_TL(O_error_message,
                             I_process_id,
                             I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_INV_STATUS_CODES_TL(O_error_message,
                                  I_process_id,
                                  I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_IS_TYPES_LANG(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_IS_CODES_LANG(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   if PROCESS_INV_ADJ_REASON(O_error_message,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;
   if PROCESS_INV_ADJ_RSN_TL(O_error_message,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW LP_errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;
   
   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                    when status = 'PE'
                    then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-------------------------------------------------------------------------------------------------
END CORESVC_INV_STAT_TYP_ADJ_RSN;
/
