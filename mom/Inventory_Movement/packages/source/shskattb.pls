
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SHIPSKU_ATTRIB_SQL AS
--------------------------------------------------------------------
FUNCTION GET_QTY_RECEIVED (O_error_message  IN OUT  VARCHAR2,
                           I_shipment       IN      SHIPSKU.SHIPMENT%TYPE,
                           I_item           IN      SHIPSKU.ITEM%TYPE,
                           I_carton         IN      SHIPSKU.CARTON%TYPE,
                           O_qty            IN OUT  SHIPSKU.QTY_RECEIVED%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64) := 'SHIPSKU_ATTRIB_SQL.GET_QTY_RECEIVED';

   cursor C_GET_QTY is
      select qty_received
        from shipsku
       where shipment = I_shipment
         and item = I_item
         and nvl(carton,'0') = nvl(I_carton,'0');

BEGIN
   open C_GET_QTY;
   fetch C_GET_QTY into O_qty;
   if C_GET_QTY%NOTFOUND then
      close C_GET_QTY;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SHIP_ITEM',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   close C_GET_QTY;
   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));

END GET_QTY_RECEIVED;
--------------------------------------------------------------------
FUNCTION GET_UNIT_COST(O_error_message  IN OUT  VARCHAR2,
                       I_shipment       IN      SHIPSKU.SHIPMENT%TYPE,
                       I_item           IN      SHIPSKU.ITEM%TYPE,
                       I_carton         IN      SHIPSKU.CARTON%TYPE,
                       O_unit_cost      IN OUT  SHIPSKU.UNIT_COST%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64) := 'SHIPSKU_ATTRIB_SQL.GET_UNIT_COST';

   cursor C_GET_COST is
      select unit_cost
        from shipsku
       where shipment = I_shipment
         and item = I_item
         and carton = nvl(I_carton,'0');

BEGIN
   open C_GET_COST;
   fetch C_GET_COST into O_unit_cost;
   if C_GET_COST%NOTFOUND then
      close C_GET_COST;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SHIP_ITEM',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   close C_GET_COST;
   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));

END GET_UNIT_COST;
--------------------------------------------------------------------
FUNCTION SHIPSKU_ITEM_EXIST (O_error_message   IN OUT  VARCHAR2,
                             I_shipment        IN      SHIPSKU.SHIPMENT%TYPE,
                             I_item            IN      SHIPSKU.ITEM%TYPE,
                             I_tran_level_ind  IN      VARCHAR2,
                             O_exist           IN OUT  BOOLEAN)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'SHIPSKU_ATTRIB_SQL. SHIPSKU_ITEM_EXIST ';
   L_record_found     VARCHAR2(1);

   cursor C_VAL_ITEM is
      select 'X'
        from shipsku
       where shipment = I_shipment
         and item = I_item;

   cursor C_VAL_REF_ITEM is
      select 'X'
        from shipsku
       where shipment = I_shipment
         and ref_item = I_item;

BEGIN
   O_exist := FALSE;

   if I_tran_level_ind = 'Y' then
      open C_VAL_ITEM;
      fetch C_VAL_ITEM into L_record_found;
      if C_VAL_ITEM %NOTFOUND then
         close C_VAL_ITEM;
         O_error_message := SQL_LIB.CREATE_MSG('INV_SHIP_ITEM',
                                                NULL,
                                                NULL,
                                                NULL);
     /*START_RMS11_DIS_SIR29497
         return TRUE;
     END_RMS11_DIS_SIR29497*/

     --START_RMS11_DIS_SIR29497--
        return FALSE;
     --END_RMS11_DIS_SIR29497--

      end if;
      close C_VAL_ITEM;

      O_exist := TRUE;

   else
      open C_VAL_REF_ITEM;
      fetch C_VAL_REF_ITEM into L_record_found;
      if C_VAL_REF_ITEM %NOTFOUND then
         close C_VAL_REF_ITEM;
         O_error_message := SQL_LIB.CREATE_MSG('INV_SHIP_ITEM',
                                                    NULL,
                                                    NULL,
                                                    NULL);
     /*START_RMS11_DIS_SIR29497
         return TRUE;
     END_RMS11_DIS_SIR29497*/

     --START_RMS11_DIS_SIR29497--
        return FALSE;
     --END_RMS11_DIS_SIR29497--

      end if;
      close C_VAL_REF_ITEM;
      O_exist := TRUE;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END SHIPSKU_ITEM_EXIST;
--------------------------------------------------------------------
FUNCTION SHIPMENT_CARTON_ITEM_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists         IN OUT BOOLEAN,
                                     O_seq_no         IN OUT SHIPSKU.SEQ_NO%TYPE,
                                     IO_carton        IN OUT SHIPSKU.CARTON%TYPE,
                                     I_shipment       IN     SHIPMENT.SHIPMENT%TYPE,
                                     I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)         := 'SHIPSKU_ATTRIB_SQL.SHIPMENT_CARTON_ITEM_EXISTS';
   L_carton  SHIPSKU.CARTON%TYPE;

   cursor C_ITEM_EXISTS is
      select carton,
             seq_no
        from shipsku
       where shipment = NVL(I_shipment, shipment)
         and (carton  = NVL(IO_carton, carton)
              or (carton is NULL
                  and IO_carton is NULL))
         and item     = I_item
         and match_invc_id is NULL;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   O_exists := FALSE;
   ---
   --- Validate that the passed in item exists on the shipsku table for the passed in
   --- shipment and carton.
   open C_ITEM_EXISTS;
   fetch C_ITEM_EXISTS into L_carton,
                            O_seq_no;
   ---
   if C_ITEM_EXISTS%FOUND then
      O_exists := TRUE;
      ---
      if IO_carton is NULL then
         IO_carton := L_carton;
      end if;
   end if;
   ---
   close C_ITEM_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END SHIPMENT_CARTON_ITEM_EXISTS;
--------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_seq_no         IN OUT SHIPSKU.SEQ_NO%TYPE,
                         I_shipment       IN     SHIPMENT.SHIPMENT%TYPE,
                         I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   cursor C_SEQ is
      select nvl(max(seq_no + 1), 1)
        from shipsku
       where shipment = I_shipment
         and item = I_item;

BEGIN
   open C_SEQ;
   fetch C_SEQ into O_seq_no;
   close C_SEQ;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SHIPSKU_ATTRIB_SQL.GET_NEXT_SEQ_NO',
                                             to_char(SQLCODE));
      return FALSE;
END GET_NEXT_SEQ_NO;
--------------------------------------------------------------------------------
FUNCTION CARTON_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT  BOOLEAN,
                       I_carton         IN      SHIPSKU.CARTON%TYPE)
RETURN BOOLEAN IS

   L_exists  VARCHAR2(1) := 'N';

   cursor C_CARTON_EXISTS is
      select 'Y'
        from shipsku
       where carton = I_carton;

BEGIN

   open C_CARTON_EXISTS;
   fetch C_CARTON_EXISTS into L_exists;
   close C_CARTON_EXISTS;

   O_exists := (L_exists = 'Y');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SHIPSKU_ATTRIB_SQL.CARTON_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END CARTON_EXISTS;
--------------------------------------------------------------------------------
FUNCTION SHIPSKU_RECORD_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               I_shipment        IN       SHIPSKU.SHIPMENT%TYPE,
                               I_distro_no       IN       SHIPSKU.DISTRO_NO%TYPE,
                               I_distro_type     IN       SHIPSKU.DISTRO_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'SHIPSKU_ATTRIB_SQL.SHIPSKU_RECORD_EXISTS';
   L_ship_exists   VARCHAR2(1)  := NULL;

   cursor C_CHECK_SHIPSKU is
      select 'X'
        from shipsku sk,
             shipment st
       where sk.shipment = st.shipment
         and st.shipment = I_shipment
         and ((NVL(sk.distro_no, -999) = NVL(I_distro_no, NVL(sk.distro_no, -999))
                  and sk.distro_type = I_distro_type
                  and I_distro_type in ('T','A'))
               or (NVL(st.order_no, -999) = NVL(substr(I_distro_no, 1,12), NVL(st.order_no, -999))
                  and I_distro_type = 'P'))
         and rownum = 1;

BEGIN
   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_shipment',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_SHIPSKU;
   fetch C_CHECK_SHIPSKU into L_ship_exists;
   ---
   if C_CHECK_SHIPSKU%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   close C_CHECK_SHIPSKU;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SHIPSKU_RECORD_EXISTS;
--------------------------------------------------------------------------------
END SHIPSKU_ATTRIB_SQL;
/
