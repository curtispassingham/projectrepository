
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SHIPSKU_ATTRIB_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------
-- Function Name: GET_QTY_RECEIVED
-- Purpose      : takes a shipment number, ITEM and gets
--                the quantity recieved for the item from the
--                shipsku table
-- Calls        : none
-- Created      : 07-OCT-96 by Matt Sniffen
--------------------------------------------------------------------
FUNCTION GET_QTY_RECEIVED (O_error_message  IN OUT  VARCHAR2,
                           I_shipment       IN      SHIPSKU.SHIPMENT%TYPE,
                           I_item           IN      SHIPSKU.ITEM%TYPE,
                           I_carton         IN      SHIPSKU.CARTON%TYPE,
                           O_qty            IN OUT  SHIPSKU.QTY_RECEIVED%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function Name: GET_UNIT_COST
-- Purpose      : takes a shipment number, SKU and gets
--                the av_cost for the item from the shipsku table
-- Calls        : none
-- Created      : 07-OCT-96 by Matt Sniffen
--------------------------------------------------------------------
FUNCTION GET_UNIT_COST(O_error_message  IN OUT  VARCHAR2,
                       I_shipment       IN      SHIPSKU.SHIPMENT%TYPE,
                       I_item           IN      SHIPSKU.ITEM%TYPE,
                       I_carton         IN      SHIPSKU.CARTON%TYPE,
                       O_unit_cost      IN OUT  SHIPSKU.UNIT_COST%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function Name: SHIPSKU_ITEM_EXIST
-- Purpose      : validate the existence of transaction level or reference
--                item in the shipsku table
-- Calls        : none
-- Created      : 03-MAR-01 by Pete Ricksecker
--------------------------------------------------------------------
FUNCTION SHIPSKU_ITEM_EXIST (O_error_message   IN OUT  VARCHAR2,
                             I_shipment        IN      SHIPSKU.SHIPMENT%TYPE,
                             I_item            IN      SHIPSKU.ITEM%TYPE,
                             I_tran_level_ind  IN      VARCHAR2,
                             O_exist           IN OUT  BOOLEAN)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function Name: SHIPMENT_CARTON_ITEM_EXISTS
-- Purpose      : This function does one of 2 things
--                1. Given a shipment and carton, it validates that the
--                   passed item exists for the shipment and carton.
--                   If neither the shipment or carton are passed, it just
--                   validates that the item exists on shipsku.
--                2. Given a shipment and item, it returns the carton.
--------------------------------------------------------------------
FUNCTION SHIPMENT_CARTON_ITEM_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists         IN OUT BOOLEAN,
                                     O_seq_no         IN OUT SHIPSKU.SEQ_NO%TYPE,
                                     IO_carton        IN OUT SHIPSKU.CARTON%TYPE,
                                     I_shipment       IN     SHIPMENT.SHIPMENT%TYPE,
                                     I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function Name: GET_NEXT_SEQ_NO
-- Purpose      : This function will retrieve the next seq_no for a shipment/item.
--------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ_NO(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_seq_no         IN OUT SHIPSKU.SEQ_NO%TYPE,
                         I_shipment       IN     SHIPMENT.SHIPMENT%TYPE,
                         I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- CARTON_EXISTS
-- Determines if I_carton exists (is valid for any shipsku record).
--------------------------------------------------------------------------------
FUNCTION CARTON_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT  BOOLEAN,
                       I_carton         IN      SHIPSKU.CARTON%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function Name: SHIPSKU_RECORD_EXISTS
-- Purpose      : This function validates the shipsku records for
--                order, transfer and allocation.
-- Created      : 12-DEC-06 by Dennis Diamzon
--------------------------------------------------------------------
FUNCTION SHIPSKU_RECORD_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               I_shipment        IN       SHIPSKU.SHIPMENT%TYPE,
                               I_distro_no       IN       SHIPSKU.DISTRO_NO%TYPE,
                               I_distro_type     IN       SHIPSKU.DISTRO_TYPE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
END SHIPSKU_ATTRIB_SQL;
/

