CREATE OR REPLACE PACKAGE RMSSUB_XTSF_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_tsf_tbl         IN       RMSSUB_XTSF.TSF_TBL,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then
   -------------------------------------------------------------------------------
   -- Function Name: CREATE_HEADER
   -- Purpose      : Insert a record into the TSFHEAD table from I_tsf_rec object
   -------------------------------------------------------------------------------
   FUNCTION CREATE_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------
   -- Function Name: MODIFY_HEADER
   -- Purpose      : Modify an existing record on TSFHEAD with a I_tsf_rec objecdt
   -------------------------------------------------------------------------------
   FUNCTION MODIFY_HEADER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_rec         IN       RMSSUB_XTSF.TSF_REC)
   RETURN BOOLEAN;
$end
----------------------------------------------------------------------------------
END RMSSUB_XTSF_SQL;
/
