CREATE OR REPLACE PACKAGE RTV_ATTRIB_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------
-- Function Name: DECODE_STATUS
-- Purpose      : to translate the rtv status ind into the
--                full rtv status description.
-- Calls        : none
-- Created      : 12-SEP-96 by Matt Sniffen
-------------------------------------------------------------------
FUNCTION DECODE_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_status_ind      IN       NUMBER,
                       O_status_decode   IN OUT   VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: GET_SUPP_INFO
-- Purpose      : takes a supplier number and get the information
--                needed for the rtv form from sups and sups_add.
-- Calls        : none
-- Created      : 12-SEP-96 by Matt Sniffen
-------------------------------------------------------------------
FUNCTION GET_SUPP_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_supplier        IN       NUMBER,
                       O_sup_name        IN OUT   VARCHAR2,
                       O_contact_name    IN OUT   VARCHAR2,
                       O_contact_phone   IN OUT   VARCHAR2,
                       O_contact_fax     IN OUT   VARCHAR2,
                       O_address_1       IN OUT   VARCHAR2,
                       O_address_2       IN OUT   VARCHAR2,
                       O_address_3       IN OUT   VARCHAR2,
                       O_city            IN OUT   VARCHAR2,
                       O_state           IN OUT   VARCHAR2,
                       O_post_code       IN OUT   VARCHAR2,
                       O_country         IN OUT   VARCHAR2,
                       O_jurisdiction    IN OUT   ADDR.JURISDICTION_CODE%TYPE,
                       O_min_dol_amt     IN OUT   NUMBER,
                       O_ret_courier     IN OUT   VARCHAR2,
                       O_handling_pct    IN OUT   NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: GET_HEADER_INFO
-- Purpose      : takes an rtv order number and gets the information
--                needed for the rtvfind from sups and sups_add.
-- Calls        : none
-- Created      : 12-SEP-96 by Matt Sniffen
-------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rtv_order_no    IN       NUMBER,
                         O_supplier        IN OUT   NUMBER,
                         O_store           IN OUT   NUMBER,
                         O_wh              IN OUT   NUMBER,
                         O_status          IN OUT   NUMBER,
                         O_create_date     IN OUT   DATE)
   RETURN BOOLEAN;
-------------------------------------------------------------------
--- Function : GET_HEADER_INFO
--- Purpose  : Returns header level information about an RTV
---            for display in the RSS task list module. The information
---            brought back is the supplier, return authorization number,
---            created date, carrier and handling percent. 
--- Calls    : none
--- Created  : 30-MAR-98 by Rebecca Dobosh
-------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_supplier        IN OUT   RTV_HEAD.SUPPLIER%TYPE,
                         O_ret_auth_num    IN OUT   RTV_HEAD.RET_AUTH_NUM%TYPE,
                         O_created_date    IN OUT   RTV_HEAD.CREATED_DATE%TYPE,
                         O_courier         IN OUT   RTV_HEAD.COURIER%TYPE,
                         O_restock_pct     IN OUT   RTV_HEAD.RESTOCK_PCT%TYPE,
                         I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------
FUNCTION RTV_FILTER_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diff            IN OUT   VARCHAR2,
                         I_rtv_order_no    IN       RTV_HEAD.RTV_ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------
--- Function : GET_HANDLING_PCT
--- Purpose  : Retrieves the SUPS.HANDLING_PCT of the primary supplier for the item.
-------------------------------------------------------------------
FUNCTION GET_HANDLING_PCT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_handling_pct    IN OUT   SUPS.HANDLING_PCT%TYPE,
                          I_item            IN       ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------
END RTV_ATTRIB_SQL;
/
