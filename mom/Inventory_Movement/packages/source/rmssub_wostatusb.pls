
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_WOSTATUS AS

TYPE item_TBL            is table of item_master.item%TYPE         INDEX BY BINARY_INTEGER;
TYPE tsf_qty_TBL         is table of tsfdetail.tsf_qty%TYPE        INDEX BY BINARY_INTEGER;
 
/* Declare private procedure and functions.*/
---------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  VARCHAR2,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2);
--------------------------------------------------------------------

/* Function and Procedure Bodies */
-------------------------------------------------------------------------
PROCEDURE CONSUME (O_status_code          IN OUT  VARCHAR2,
                   O_error_message        IN OUT  VARCHAR2,
                   I_message              IN      RIB_OBJECT,
                   I_message_type         IN      VARCHAR2)
IS

   L_rib_wostatusdesc_rec   "RIB_WOStatusDesc_REC";
   L_check                  VARCHAR2(1);
   L_tsf_no                 tsfhead.tsf_no%TYPE;
   L_tsf_parent_no          tsfhead.tsf_no%TYPE;
   L_tsf_finisher           tsfhead.to_loc%TYPE;
   L_tsf_finisher_loc_type  tsfhead.to_loc_type%TYPE;
   L_tsf_final_loc          tsfhead.to_loc%TYPE;

   -- array items to process outstanding transfer quantities
   L_wos_item               item_TBL;
   L_wos_tsf_qty            tsf_qty_TBL;

   L_doc_close              VARCHAR2(1) := 'Y';

   L_module                 VARCHAR2(100) := 'RMSSUB_WORKORDER_STATUS.CONSUME';

   cursor C_CHECK_PHYS_WH(CV_wh1 IN wh.wh%TYPE, CV_wh2 IN wh.wh%TYPE) is
      select 'x'
        from wh wh1,
             wh wh2
       where wh1.wh          = CV_wh1
         and wh2.wh          = CV_wh2
         and wh1.physical_wh = wh2.physical_wh;

   cursor C_GET_TSF_INFO is
      select from_loc,
             from_loc_type,
             to_loc,
             tsf_parent_no
        from tsfhead
       where tsf_no = L_tsf_no;

   cursor C_GET_TSFDETAIL is
      select td.item,
             (nvl(td.tsf_qty,0) - nvl(td.received_qty,0) - nvl(td.cancelled_qty,0)) qty
        from tsfdetail td
       where td.tsf_no = L_tsf_no;

   cursor C_CHECK_QTYS is
      select decode(sum(nvl(tsf_qty, 0)), (sum(nvl(received_qty, 0)) + sum(nvl(cancelled_qty, 0))), 'Y', 'N')
        from tsfdetail
       where tsf_no = L_tsf_no;
       
   cursor C_LOCK_TSFHEAD is
      select 'x'
        from tsfhead
       where tsf_no = L_tsf_no
         for update nowait;

BEGIN
   if lower(I_message_type) != WOSTATUS_ADD then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_message_type',
                                            I_message_type,L_module);
      raise PROGRAM_ERROR;
   end if;

   L_rib_wostatusdesc_rec := treat (I_message as "RIB_WOStatusDesc_REC");

   if L_rib_wostatusdesc_rec.distro_nbr is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','distro_nbr',
                                            'NULL','NOT NULL');
      raise PROGRAM_ERROR;
   elsif L_rib_wostatusdesc_rec.distro_doc_type != 'T' then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','distro_doc_type',
                                            L_rib_wostatusdesc_rec.distro_doc_type,'T');
      raise PROGRAM_ERROR;
   elsif (L_rib_wostatusdesc_rec.item is NULL and
          L_rib_wostatusdesc_rec.completed_qty is not NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','item',
                                            'NULL','NOT NULL');
      raise PROGRAM_ERROR;
   elsif (L_rib_wostatusdesc_rec.item is not NULL and
          L_rib_wostatusdesc_rec.completed_qty is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','completed_qty',
                                            'NULL','NOT NULL');
      raise PROGRAM_ERROR;
   end if;

   L_tsf_no := L_rib_wostatusdesc_rec.distro_nbr;

   open C_GET_TSF_INFO;
   fetch C_GET_TSF_INFO into L_tsf_finisher,
                             L_tsf_finisher_loc_type,
                             L_tsf_final_loc,
                             L_tsf_parent_no;
   close C_GET_TSF_INFO;

   if L_rib_wostatusdesc_rec.distro_parent_nbr is not NULL and
      L_rib_wostatusdesc_rec.distro_parent_nbr != L_tsf_parent_no then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','distro_parent_nbr',
                                            to_char(L_rib_wostatusdesc_rec.distro_parent_nbr),
                                            to_char(L_tsf_parent_no));
      raise PROGRAM_ERROR;
   end if;

   -- confirm the finisher and the final receiving location are in the same
   -- physical warehouse.  This exception processing will take place when a
   -- workorder complete message is sent in conjunction with a BOL message.
   L_check := NULL;

   open C_CHECK_PHYS_WH(L_rib_wostatusdesc_rec.location, L_tsf_final_loc);
   fetch C_CHECK_PHYS_WH into L_check;
   close C_CHECK_PHYS_WH;

   if L_check is NULL then
      O_status_code := API_CODES.SUCCESS;
      return;
   end if;

   -- now that validation is complete, process the record.
   -- the function expects the parent transfer number, so pass in the local variable.
   -- pass in TRUE so the function will book transfer the goods once processing
   -- is complete.
   if L_rib_wostatusdesc_rec.item is not NULL then
      if TSF_WO_COMP_SQL.WO_ITEM_COMP(O_error_message,
                                      L_tsf_no,
                                      L_tsf_parent_no,
                                      L_rib_wostatusdesc_rec.item,
                                      L_tsf_finisher,
                                      L_tsf_finisher_loc_type,
                                      L_tsf_final_loc,
                                      'W',
                                      L_rib_wostatusdesc_rec.completed_qty,
                                      L_rib_wostatusdesc_rec.complete_date,
                                      TRUE) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- Book transfer has now been at least partially completed so update 2nd leg to 'S'hipped
      -- status to prevent online modification.
      open C_LOCK_TSFHEAD;
      fetch C_LOCK_TSFHEAD into L_check; 
      close C_LOCK_TSFHEAD;     

      update tsfhead
         set status = 'S'
        where tsf_no = L_tsf_no; 

      --Check if item is fully received
      open C_CHECK_QTYS;  
      fetch C_CHECK_QTYS into L_doc_close; 
      close C_CHECK_QTYS;

      if L_doc_close = 'Y' then
         --Processing only occurs for 2nd leg transfers.  Insert into doc_close_queue to determine if the
         --overall tsf can be closed.
         if TSF_WO_COMP_SQL.DOC_CLOSE_QUEUE_INSERT(O_error_message,
                                                   L_tsf_no) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;
   else
      -- no item indicates that the entire transfer should be completed
      -- loop through all items and their outstanding quantities and process
      open C_GET_TSFDETAIL;
      fetch C_GET_TSFDETAIL BULK COLLECT INTO L_wos_item,
                                              L_wos_tsf_qty;
      close C_GET_TSFDETAIL;

      for i in 1..L_wos_item.COUNT LOOP
         if TSF_WO_COMP_SQL.WO_ITEM_COMP(O_error_message,
                                         L_tsf_no,
                                         L_tsf_parent_no,
                                         L_wos_item(i),
                                         L_tsf_finisher,
                                         L_tsf_finisher_loc_type,
                                         L_tsf_final_loc,
                                         'W',
                                         L_wos_tsf_qty(i),
                                         L_rib_wostatusdesc_rec.complete_date,
                                         TRUE) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
         if L_doc_close = 'Y' and
            L_wos_tsf_qty(i) != (L_rib_wostatusdesc_rec.completed_qty) then
            -- if the tsf has not been fully received (tsf_qty != received_qty+cancelled_qty+new completed qty)
            -- the tsf should not be added to the doc close queue
            L_doc_close := 'N';
         end if;
      end LOOP;

      -- Book transfer has now been completed so update 2nd leg to 'S'hipped
      -- status to prevent online modification.
      open C_LOCK_TSFHEAD;
      fetch C_LOCK_TSFHEAD into L_check; 
      close C_LOCK_TSFHEAD;     

      update tsfhead
         set status = 'S'
        where tsf_no = L_tsf_no; 
      
      if L_doc_close = 'Y' then 
         --Processing only occurs for 2nd leg transfers.  Insert into doc_close_queue to determine if the
         --overall tsf can be closed.
         if TSF_WO_COMP_SQL.DOC_CLOSE_QUEUE_INSERT(O_error_message,
                                                   L_tsf_no) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;
   end if;
   ---
   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_module);

END CONSUME;
-----------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  VARCHAR2,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2)
IS
   L_module  VARCHAR2(100)  := 'RMSSUB_WORKORDER_STATUS.HANDLE_ERRORS';

BEGIN
   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_module,
                                             To_Char(SQLCODE));
      ---
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_module);

END HANDLE_ERRORS;
----------------------------------------------------------------------------------------
END RMSSUB_WOSTATUS;
/
