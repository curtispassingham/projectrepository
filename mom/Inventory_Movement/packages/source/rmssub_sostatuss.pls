CREATE OR REPLACE PACKAGE RMSSUB_SOSTATUS AUTHID CURRENT_USER AS
------------------------------------------------------------------------
/*Accepts stock order status record for validation and insertion into*/
/* RMS                                                               */
------------------------------------------------------------------------
PROCEDURE CONSUME(O_STATUS_CODE   IN OUT VARCHAR2,
                  O_ERROR_MESSAGE IN OUT VARCHAR2,
                  I_MESSAGE       IN     RIB_OBJECT,
                  I_MESSAGE_TYPE  IN     VARCHAR2);
------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then
   ---------------------------------------------------------------------
   -- Function Name: BUILD_XTSFDESC
   -- Purpose      : Builds a RIB_XTsfDesc_REC object to be passed
   --                in the RMSSUB_XTSF.CONSUME function
   ---------------------------------------------------------------------
   FUNCTION BUILD_XTSFDESC(O_error_message IN OUT        VARCHAR2,
                           O_xtsfdesc_rec     OUT NOCOPY "RIB_XTsfDesc_REC",
                           I_sos_rec       IN            "RIB_SOStatusDesc_REC")
   RETURN BOOLEAN;
   
   FUNCTION PROCESS_SOS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_distro_type      IN       VARCHAR2,
                        I_distro_number    IN       TSFHEAD.TSF_NO%TYPE,
                        I_to_location      IN       STORE.STORE%TYPE,
                        I_from_location    IN       ITEM_LOC_SOH.LOC%TYPE,
                        I_item             IN       ITEM_LOC_SOH.ITEM%TYPE,
                        I_qty              IN       NUMBER,
                        I_status           IN       VARCHAR2,
                        I_inventory_type   IN       TSFHEAD.INVENTORY_TYPE%TYPE)
   RETURN BOOLEAN;

   FUNCTION DO_BULK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;

   FUNCTION RESET(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
$end
------------------------------------------------------------------------
END RMSSUB_SOSTATUS;
/
