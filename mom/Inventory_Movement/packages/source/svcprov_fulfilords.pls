CREATE OR REPLACE PACKAGE SVCPROV_FULFILORD AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-- compile private functions as public for unit testing
--alter session set plsql_ccflags = 'UTPLSQL:TRUE';
$if $$UTPLSQL=TRUE $then
-------------------------------------------------------------------------------------------------------------
-- Function Name  : BUILD_CONFIRM_MSG
-- Purpose        : This function builds a collection of customer order fulfillment confirmations 
-- based on ORDCUST and ORDCUST_DETAILs created in RMS. It includes ORDCUST records created in 'C'
-- (order fully created), 'P'artial (order partially created) or 'X' (order not created) status.
-------------------------------------------------------------------------------------------------------------
FUNCTION BUILD_CONFIRM_MSG(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_confirm_obj             IN OUT   "RIB_FulfilOrdCfmCol_REC", 
                           I_ordcust_ids             IN       ID_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name  : PARSE_ERR_MSG
-- Purpose        : This function parses ERROR_MSG on fulfillment staging tables and add parsed
-- errors to FailStatus_TBL in ServiceOpStatus object. This will pass the errors to the web service client.
-------------------------------------------------------------------------------------------------------------
FUNCTION PARSE_ERR_MSG(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_serviceOperationStatus  IN OUT   "RIB_ServiceOpStatus_REC", 
                       I_process_id              IN       SVC_FULFILORD.PROCESS_ID%TYPE, 
                       I_action_type             IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
$end
-------------------------------------------------------------------------------------------------------------
-- Function Name  : CREATE_FULFILLMENT
-- Purpose        : This is the public procedure to be called from the service provider
--                  for a customer order fulfillment create request.
-- Input: I_serviceOperationContext: standard input of operational context
--        I_businessObject: collection of create requests
-- Output: O_serviceOperationStatus: standard output of success or failure    
--         O_businessObject: collection of confirmations
-------------------------------------------------------------------------------------------------------------
PROCEDURE CREATE_FULFILLMENT(O_serviceOperationStatus    IN OUT   "RIB_ServiceOpStatus_REC",
                             O_businessObject            IN OUT   "RIB_FulfilOrdCfmCol_REC",
                             I_serviceOperationContext   IN       "RIB_ServiceOpContext_REC",
                             I_businessObject            IN       "RIB_FulfilOrdColDesc_REC");
-------------------------------------------------------------------------------------------------------------
-- Function Name  : CANCEL_FULFILLMENT
-- Purpose        : This is the public procedure to be called from the service provider 
--                  for a customer order fulfillment cancel request.
-- Input: I_serviceOperationContext: standard input of operational context
--        I_businessObject: collection of cancellation requests
-- Output: O_serviceOperationStatus: standard output of success or failure    
--         O_businessObject: operation success
-------------------------------------------------------------------------------------------------------------
PROCEDURE CANCEL_FULFILLMENT(O_serviceOperationStatus    IN OUT   "RIB_ServiceOpStatus_REC",
                             O_businessObject            IN OUT   "RIB_InvocationSuccess_REC",
                             I_serviceOperationContext   IN       "RIB_ServiceOpContext_REC",
                             I_businessObject            IN       "RIB_FulfilOrdColRef_REC");
------------------------------------------------------------------------------------------------------------- 
END SVCPROV_FULFILORD;
/
