
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_INVREQ AS

/* Function and Procedure Bodies */
-------------------------------------------------------------------------
PROCEDURE CONSUME (O_status_code          IN OUT  VARCHAR2,
                   O_error_message        IN OUT  VARCHAR2,
                   I_message              IN      RIB_OBJECT,
                   I_message_type         IN      VARCHAR2,
                   O_rib_error_tbl           OUT  RIB_ERROR_TBL)
IS

   L_rib_invreqdesc_rec    "RIB_InvReqDesc_REC";

   L_module VARCHAR2(64) := 'RMSSUB_INVREQ.CONSUME';

BEGIN

   L_rib_invreqdesc_rec := treat (I_message as "RIB_InvReqDesc_REC");

   --empty out cache of inserts and updates to store_orders table
   if INV_REQUEST_SQL.INIT (O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if RMSSUB_INVREQ_ERROR.INIT (O_error_message,
                                I_message_type) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Check inputs at header level. If NULL, reject the entire request. No need
   -- to write to error table.
   if L_rib_invreqdesc_rec.store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','L_rib_invreqdesc_rec.store',
                                            'NULL',L_module);
      raise PROGRAM_ERROR;
   else
      -- initialize invreq errors package
      if RMSSUB_INVREQ_ERROR.BEGIN_INVREQ(O_error_message,
                                          L_rib_invreqdesc_rec.request_id,
                                          L_rib_invreqdesc_rec.store,
                                          L_rib_invreqdesc_rec.request_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      -- loop through the request and process each item
      FOR i in 1..L_rib_invreqdesc_rec.InvReqItem_tbl.COUNT LOOP

         if INV_REQUEST_SQL.PROCESS(O_error_message,
                                    L_rib_invreqdesc_rec.store,
                                    L_rib_invreqdesc_rec.request_type,
                                    L_rib_invreqdesc_rec.InvReqItem_tbl(i)) = FALSE then

            raise PROGRAM_ERROR;
         end if;
      END LOOP;
   end if; -- store is null

   --use cache to insert and update the store_orders table
   if INV_REQUEST_SQL.FLUSH (O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if RMSSUB_INVREQ_ERROR.FINISH(O_error_message,
                                 O_rib_error_tbl) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_module);
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_module);
END CONSUME;
----------------------------------------------------------------------------------------
END RMSSUB_INVREQ;
/
