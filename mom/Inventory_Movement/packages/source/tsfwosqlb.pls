CREATE OR REPLACE PACKAGE BODY TSF_WO_SQL AS
--------------------------------------------------------------------
FUNCTION GET_TMPL_ROW(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists          IN OUT  BOOLEAN,
                      O_wo_tmpl_row     IN OUT  WO_TMPL_HEAD%ROWTYPE,
                      I_wo_tmpl_id      IN      WO_TMPL_HEAD.WO_TMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'TSF_WO_SQL.GET_TMPL_ROW';

   cursor C_GET_TMPL_ROW is
      select *
        from wo_tmpl_head
       where wo_tmpl_id = I_wo_tmpl_id;

BEGIN

   --- Check required input parameters
   if I_wo_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wo_tmpl_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --- Initialize the output variable
   O_wo_tmpl_row := NULL;

   --- Get the row from tsf_entity
   open  C_GET_TMPL_ROW;
   fetch C_GET_TMPL_ROW into O_wo_tmpl_row;
   close C_GET_TMPL_ROW;

   O_exists := (O_wo_tmpl_row.wo_tmpl_id is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TMPL_ROW;
--------------------------------------------------------------------
FUNCTION DELETE_WO_TMPL(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_wo_tmpl_id      IN      WO_TMPL_HEAD.WO_TMPL_ID%TYPE)

RETURN BOOLEAN IS

   L_program      VARCHAR2(61)  := 'TSF_WO_SQL.GET_TMPL_ROW';
   L_table        VARCHAR2(20)  := 'WO_TMPL_DETAIL';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_RECORD_DETAIL_LOCK is
      select 'x'
        from wo_tmpl_detail
       where wo_tmpl_id = I_wo_tmpl_id
         for update nowait;

   cursor C_RECORD_LOCK_HEAD_TL is
      select 'x'
        from wo_tmpl_head_tl
       where wo_tmpl_id = I_wo_tmpl_id
         for update nowait;

   cursor C_RECORD_LOCK_HEAD is
      select 'x'
        from wo_tmpl_head
       where wo_tmpl_id = I_wo_tmpl_id
         for update nowait;

BEGIN
   --- Check required input parameters
   if I_wo_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wo_tmpl_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_RECORD_DETAIL_LOCK;
   close C_RECORD_DETAIL_LOCK;

   delete from wo_tmpl_detail
    where wo_tmpl_id = I_wo_tmpl_id;

   open C_RECORD_LOCK_HEAD_TL;
   close C_RECORD_LOCK_HEAD_TL;

   delete from wo_tmpl_head_tl
    where wo_tmpl_id = I_wo_tmpl_id;

   open C_RECORD_LOCK_HEAD;
   close C_RECORD_LOCK_HEAD;

   delete from wo_tmpl_head
    where wo_tmpl_id = I_wo_tmpl_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            L_table,
                                            'WO_TMPL_ID '||I_wo_tmpl_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_WO_TMPL;
--------------------------------------------------------------------
FUNCTION CHECK_WO_TMPL_ACTIVITY_EXISTS(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_wo_activity_exists   IN OUT  BOOLEAN,
                                       I_wo_tmpl_id           IN      WO_TMPL_DETAIL.WO_TMPL_ID%TYPE,
                                       I_activity_id          IN      WO_TMPL_DETAIL.ACTIVITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'TSF_WO_SQL.CHECK_WO_TMPL_ACTIVITY_EXISTS';
   L_exists   VARCHAR2(1);

   cursor C_ACTIVITY is
      select 'x'
        from wo_tmpl_detail
       where activity_id = I_activity_id
         and wo_tmpl_id = I_wo_tmpl_id;

BEGIN
   --- Check required input parameters
   if I_wo_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wo_tmpl_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_activity_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_activity_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ACTIVITY;
   fetch C_ACTIVITY into L_exists;
   close C_ACTIVITY;

   O_wo_activity_exists := (L_exists is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_WO_TMPL_ACTIVITY_EXISTS;
--------------------------------------------------------------------------------
FUNCTION CHECK_WO_TMPL_DETAIL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists            IN OUT  BOOLEAN,
                              I_wo_tmpl_id        IN      WO_TMPL_DETAIL.WO_TMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(61)  := 'TSF_WO_SQL.CHECK_WO_TMPL_DETAIL';
   L_detail_exists   VARCHAR2(1);

   cursor C_CHECK_DETAIL is
      select 'x'
        from wo_tmpl_detail
       where wo_tmpl_id = I_wo_tmpl_id
         and rownum     = 1;

BEGIN
   --- Check required input parameters
   if I_wo_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wo_tmpl_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CHECK_DETAIL;
   fetch C_CHECK_DETAIL into L_detail_exists;
   close C_CHECK_DETAIL;

   O_exists := (L_detail_exists is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_WO_TMPL_DETAIL;
--------------------------------------------------------------------------------
FUNCTION NEXT_WO_TMPL_ID_SEQ_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_wo_tmpl_id     IN OUT  WO_TMPL_HEAD.WO_TMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'TSF_WO_SQL.NEXT_WO_TMPL_ID_SEQ_NO';

   cursor C_NEXT_SEQ_NO is
      select wo_tmpl_id_seq.nextval
        from dual;

BEGIN

   open  C_NEXT_SEQ_NO;
   fetch C_NEXT_SEQ_NO into O_wo_tmpl_id;
   close C_NEXT_SEQ_NO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END NEXT_WO_TMPL_ID_SEQ_NO;
--------------------------------------------------------------------------------
FUNCTION NEXT_WO_TMPLDTAIL_SEQ_NO(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_wo_tmpldtail_id   IN OUT  WO_TMPL_DETAIL.WO_TMPL_DETAIL_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'TSF_WO_SQL.NEXT_WO_TMPLDTAIL_SEQ_NO';

   cursor C_NEXT_SEQ_NO is
      select wo_tmpl_detail_id_seq.nextval
        from dual;

BEGIN

   open  C_NEXT_SEQ_NO;
   fetch C_NEXT_SEQ_NO into O_wo_tmpldtail_id;
   close C_NEXT_SEQ_NO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END NEXT_WO_TMPLDTAIL_SEQ_NO;
--------------------------------------------------------------------------------
FUNCTION ITEM_ACTIVITY_EXIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists            IN OUT   BOOLEAN,
                             I_wo_id             IN       TSF_WO_DETAIL.TSF_WO_ID%TYPE,
                             I_item_type         IN       VARCHAR2,
                             I_item              IN       TSF_WO_DETAIL.ITEM%TYPE,
                             I_diff_id           IN       DIFF_IDS.DIFF_ID%TYPE,
                             I_activity_type     IN       VARCHAR2,
                             I_activity_id       IN       WO_ACTIVITY.ACTIVITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)  := 'TSF_WO_SQL.ITEM_ACTIVITY_EXIST';
   L_exist    VARCHAR2(1);
   L_table    VARCHAR2(20)  := 'WO_TMPL_DETAIL';

   cursor C_ITEM_ACTIVITY_EXIST is
      select 'x'
        from tsf_wo_detail td
       where tsf_wo_id = I_wo_id
         and item = NVL(I_item, item)
         and activity_id = NVL(I_activity_id, activity_id);

   cursor C_ITEM_PARENT_ACT_EXIST is
      select 'x'
        from tsf_wo_detail td,
             item_master im
       where td.tsf_wo_id = I_wo_id
         and td.activity_id = NVL(I_activity_id, activity_id)
         and im.item_parent = I_item
         and im.item = td.item
         and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
              NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
              NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
              NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999')));

   cursor C_ITEM_TMPL_EXIST is
      select 'x'
        from tsf_wo_detail td,
             wo_tmpl_detail wd
       where tsf_wo_id = I_wo_id
         and td.item = NVL(I_item, item)
         and wd.activity_id = td.activity_id
         and wd.wo_tmpl_id = I_activity_id;

   cursor C_ITEM_PARENT_TMPL_EXIST is
      select 'x'
        from tsf_wo_detail td,
             wo_tmpl_detail wd,
             item_master im
       where tsf_wo_id = I_wo_id
         and wd.wo_tmpl_id = I_activity_id
         and wd.activity_id = td.activity_id
         and im.item_parent = I_item
         and im.item = td.item
         and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
              NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
              NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
              NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999')));
BEGIN
   -- If work order detail is deleted, activity type is D.
   if I_activity_type in ('A','D') then
      if I_item_type in ('ST', 'STC') then
         open C_ITEM_PARENT_ACT_EXIST;
         fetch C_ITEM_PARENT_ACT_EXIST into L_exist;
         close C_ITEM_PARENT_ACT_EXIST;
      else
         open  C_ITEM_ACTIVITY_EXIST;
         fetch C_ITEM_ACTIVITY_EXIST into L_exist;
         close C_ITEM_ACTIVITY_EXIST;
      end if;
   elsif I_activity_type = 'T' then
      if I_item_type in ('ST', 'STC') then
         open C_ITEM_PARENT_TMPL_EXIST;
         fetch C_ITEM_PARENT_TMPL_EXIST into L_exist;
         close C_ITEM_PARENT_TMPL_EXIST;
      else
         open C_ITEM_TMPL_EXIST;
         fetch C_ITEM_TMPL_EXIST into L_exist;
         close C_ITEM_TMPL_EXIST;
      end if;
   end if;

   O_exists := (L_exist is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_ACTIVITY_EXIST;
--------------------------------------------------------------------
FUNCTION PARENT_DIFF_APPLY(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_overwrite     IN      BOOLEAN,
                           I_item          IN      TSF_WO_DETAIL.ITEM%TYPE,
                           I_diff_id       IN      DIFF_IDS.DIFF_ID%TYPE,
                           I_transfer      IN      TSF_WO_HEAD.TSF_NO%TYPE,
                           I_wo_id         IN      TSF_WO_HEAD.TSF_WO_ID%TYPE,
                           I_activity_type IN      VARCHAR2,
                           I_activity_id   IN      WO_ACTIVITY.ACTIVITY_ID%TYPE,
                           I_unit_cost     IN      TSF_WO_DETAIL.UNIT_COST%TYPE,
                           I_comments      IN      TSF_WO_DETAIL.COMMENTS%TYPE,
                           I_finisher_curr IN      CURRENCIES.CURRENCY_CODE%TYPE,
                           I_primary_curr  IN      CURRENCIES.CURRENCY_CODE%TYPE,
                           I_inv_status    IN      TSF_WO_DETAIL.INV_STATUS%TYPE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61)  := 'TSF_WO_SQL.PARENT_DIFF_APPLY';
   L_exist             VARCHAR2(1)   := NULL;
   L_table             VARCHAR2(20)  := 'TSF_WO_DETAIL';
   L_cost_retail_ind   VARCHAR2(1)   := 'C';
   L_unit_cost         TSF_WO_DETAIL.UNIT_COST%TYPE;

   L_rowid             ROWID;
   L_activity_id       TSF_WO_DETAIL.ACTIVITY_ID%TYPE;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_DETAIL is
      select 'x'
        from tsf_wo_detail ts
       where ts.tsf_wo_id = I_wo_id
         and NVL(ts.activity_id, -999) = DECODE(ts.activity_id, NULL, -999, I_activity_id)
         and exists (select 1
                       from item_master im
                      where im.item_parent = I_item
                        and im.item = ts.item
                        and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
                             NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
                             NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
                             NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999'))))
         for update nowait;

   cursor C_LOCK_DETAIL_TMPL is
      select rowid,
             ts.activity_id
        from tsf_wo_detail ts
       where ts.tsf_wo_id = I_wo_id
         and (exists (select 1
                       from wo_tmpl_detail wt
                      where wt.activity_id = ts.activity_id)
             OR ts.activity_id is NULL)
         and exists (select 1
                       from item_master im
                      where im.item_parent = I_item
                        and im.item = ts.item
                        and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
                             NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
                             NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
                             NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999'))))
         for update nowait;

   cursor C_UPDATE_PARENT_TMPL is
      select ts.rowid,
             ts.comments,
             wt.unit_cost
        from tsf_wo_detail ts,
             wo_tmpl_detail wt,
             item_master im
       where ts.tsf_wo_id = I_wo_id
         and wt.wo_tmpl_id = I_activity_id
         and wt.activity_id = ts.activity_id
         and im.item_parent = I_item
         and im.item = ts.item
         and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
              NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
              NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
              NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999')));

BEGIN

   if I_activity_type = 'A' then
      open C_LOCK_DETAIL;
      close C_LOCK_DETAIL;
      ---
      if I_overwrite then
         --This will update records in tsf_wo_detail under the Parent or Parent/Diff item type.

         update tsf_wo_detail ts
            set ts.unit_cost = NVL(I_unit_cost, ts.unit_cost),
                ts.comments = I_comments
          where ts.tsf_wo_id = I_wo_id
            and ts.activity_id = I_activity_id
            and exists (select 1
                          from item_master im
                         where im.item_parent = I_item
                           and im.item = ts.item
                           and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
                                NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
                                NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
                                NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999'))));
      end if;
      ---

      -- Update all item/parent recs with no activity
      update tsf_wo_detail ts
            set ts.activity_id = NVL(I_activity_id, ts.activity_id),
                ts.unit_cost = NVL(I_unit_cost, ts.unit_cost),
                ts.comments = I_comments,
                ts.inv_status = I_inv_status
          where ts.tsf_wo_id = I_wo_id
            and ts.activity_id is NULL
            and exists (select 1
                          from item_master im
                         where im.item_parent = I_item
                           and im.item = ts.item
                           and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
                                NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
                                NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
                                NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999'))));

      --This will insert records in tsf_wo_details under the Parent or Parent/Diff item type.
      insert into tsf_wo_detail(tsf_wo_detail_id,
                                tsf_wo_id,
                                item,
                                activity_id,
                                unit_cost,
                                comments,
                                inv_status)
                         select TSF_WO_DETAIL_ID_SEQ.nextval,
                                I_wo_id,
                                im.item,
                                I_activity_id,
                                I_unit_cost,
                                I_comments,
                                I_inv_status
                           from item_master im,
                                tsfdetail   td
                          where im.item_parent = I_item
                            and im.item = td.item
                            and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
                                 NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
                                 NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
                                 NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999')))
                            and td.tsf_no = I_transfer
                            and NOT exists (select 1
                                              from tsf_wo_detail tw
                                             where tw.tsf_wo_id = I_wo_id
                                               and tw.item = im.item
                                               and tw.activity_id = I_activity_id
                                               and NVL(tw.inv_status, -999) = DECODE(tw.inv_status, NULL, -999,I_inv_status));
   elsif I_activity_type = 'T' then

      --This will update records in tsf_wo_detail under the Parent or Parent/Diff item type.
      open C_LOCK_DETAIL_TMPL;
      fetch C_LOCK_DETAIL_TMPL into L_rowid,
                                    L_activity_id;
      close C_LOCK_DETAIL_TMPL;
      ---
      if I_overwrite then
         FOR rec IN C_UPDATE_PARENT_TMPL LOOP
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    rec.unit_cost,
                                    I_primary_curr,
                                    I_finisher_curr,
                                    L_unit_cost,
                                    L_cost_retail_ind,
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
            --- Use the converted value to update the activity's unit cost in the tsf_wo_detail table
            update tsf_wo_detail
               set unit_cost = L_unit_cost,
                   comments  = I_comments
             where rowid = rec.rowid;
         END LOOP;
      end if;
      ---
      delete from tsf_wo_detail ts
         where ts.tsf_wo_id = I_wo_id
           and ts.activity_id is NULL
           and exists(select 1
                       from item_master im,
                            tsfdetail td
                      where im.item_parent = I_item
                        and im.item = ts.item
                        and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
                             NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
                             NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
                             NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999')))
                        and td.tsf_no = I_transfer);

      --This will insert records into tsf_wo_detail under the Parent or Parent/Diff item type.
      insert into tsf_wo_detail(tsf_wo_detail_id,
                                tsf_wo_id,
                                item,
                                activity_id,
                                unit_cost,
                                comments,
                                inv_status)
                         select TSF_WO_DETAIL_ID_SEQ.nextval,
                                I_wo_id,
                                im.item,
                                wt.activity_id,
                                CURRENCY_SQL.CONVERT_VALUE(L_cost_retail_ind,
                                                           I_finisher_curr,
                                                           I_primary_curr,
                                                           wt.unit_cost),
                                I_comments,
                                I_inv_status
                           from item_master im,
                                tsfdetail td,
                                wo_tmpl_detail wt
                          where im.item_parent = I_item
                            and im.item = td.item
                            and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
                                 NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
                                 NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
                                 NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999')))
                            and td.tsf_no = I_transfer
                            and wt.wo_tmpl_id = I_activity_id
                            and not exists (select 1
                                              from tsf_wo_detail ts
                                             where ts.tsf_wo_id = I_wo_id
                                               and ts.item = im.item
                                               and ts.activity_id = wt.activity_id
                                               and NVL(ts.inv_status, -999) = DECODE(ts.inv_status, NULL, -999,I_inv_status));

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'Item '||I_item,
                                            'Work Order ID ' || I_wo_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PARENT_DIFF_APPLY;
---------------------------------------------------------------------
FUNCTION ITEM_APPLY(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_overwrite     IN      BOOLEAN,
                    I_item          IN      TSF_WO_DETAIL.ITEM%TYPE,
                    I_transfer      IN      TSF_WO_HEAD.TSF_NO%TYPE,
                    I_wo_id         IN      TSF_WO_HEAD.TSF_WO_ID%TYPE,
                    I_activity_type IN      VARCHAR2,
                    I_activity_id   IN      WO_ACTIVITY.ACTIVITY_ID%TYPE,
                    I_unit_cost     IN      TSF_WO_DETAIL.UNIT_COST%TYPE,
                    I_comments      IN      TSF_WO_DETAIL.COMMENTS%TYPE,
                    I_finisher_curr IN      CURRENCIES.CURRENCY_CODE%TYPE,
                    I_primary_curr  IN      CURRENCIES.CURRENCY_CODE%TYPE,
                    I_inv_status    IN      TSF_WO_DETAIL.INV_STATUS%TYPE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61)  := 'TSF_WO_SQL.ITEM_APPLY';
   L_exist             VARCHAR2(1)   := NULL;
   L_table             VARCHAR2(20)  := 'TSF_WO_DETAIL';
   L_cost_retail_ind   VARCHAR2(1)   := 'C';
   L_unit_cost         TSF_WO_DETAIL.UNIT_COST%TYPE;

   L_rowid             ROWID;
   L_activity_id       TSF_WO_DETAIL.ACTIVITY_ID%TYPE;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_DETAIL is
      select rowid,
             ts.activity_id
        from tsf_wo_detail ts
       where ts.item = I_item
         and ts.tsf_wo_id = I_wo_id
         and NVL(ts.activity_id, -999) = DECODE(ts.activity_id, NULL, -999,I_activity_id)
         for update nowait;

   cursor C_LOCK_DETAIL_TMPL is
      select ts.rowid,
             ts.activity_id
        from tsf_wo_detail ts
       where ts.item = I_item
         and ts.tsf_wo_id = I_wo_id
         and (exists (select 1
                       from wo_tmpl_detail wt
                      where wt.activity_id = ts.activity_id)
             OR ts.activity_id is NULL)
         for update nowait;

   cursor C_UPDATE_ITEM_TMPL is
      select ts.rowid,
             ts.comments,
             wt.unit_cost
        from tsf_wo_detail ts,
             wo_tmpl_detail wt
       where ts.item = I_item
         and ts.tsf_wo_id = I_wo_id
         and wt.wo_tmpl_id = I_activity_id
         and wt.activity_id = ts.activity_id;

BEGIN

   --- Check required input parameters
   if I_wo_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wo_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_transfer is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_transfer',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_activity_type = 'A' then
      open C_LOCK_DETAIL;
      fetch C_LOCK_DETAIL into L_rowid,
                               L_activity_id;
      close C_LOCK_DETAIL;
      ---

      if L_rowid is NOT NULL AND I_overwrite = TRUE then
         update tsf_wo_detail ts
            set ts.activity_id = NVL(I_activity_id,ts.activity_id),
                ts.unit_cost   = NVL(I_unit_cost, ts.unit_cost),
                ts.comments    = I_comments
          where rowid = L_rowid;
      else
         insert into tsf_wo_detail(tsf_wo_detail_id,
                                   tsf_wo_id,
                                   item,
                                   activity_id,
                                   unit_cost,
                                   comments,
                                   inv_status)
                            select TSF_WO_DETAIL_ID_SEQ.nextval,
                                   I_wo_id,
                                   item,
                                   I_activity_id,
                                   I_unit_cost,
                                   I_comments,
                                   I_inv_status
                              from tsfdetail td
                             where td.tsf_no = I_transfer
                               and td.item = I_item
                               and not exists (select 1
                                                 from tsf_wo_detail tw
                                                where tw.tsf_wo_id = I_wo_id
                                                  and td.item = tw.item
                                                  and tw.item = I_item
                                                  and tw.activity_id = I_activity_id
                                                  and NVL(tw.inv_status, -999) = DECODE(tw.inv_status, NULL, -999,I_inv_status));
         ---
      end if;
   elsif I_activity_type = 'T' then
      if I_overwrite then
         open C_LOCK_DETAIL_TMPL;
         fetch C_LOCK_DETAIL_TMPL into L_rowid,
                                       L_activity_id;
         close C_LOCK_DETAIL_TMPL;
         FOR rec IN C_UPDATE_ITEM_TMPL LOOP
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    rec.unit_cost,
                                    I_primary_curr,
                                    I_finisher_curr,
                                    L_unit_cost,
                                    L_cost_retail_ind,
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
            --- Use the converted value to update the activity's unit cost in the tsf_wo_detail table
            update tsf_wo_detail
               set unit_cost = L_unit_cost,
                   comments  = I_comments
             where rowid = rec.rowid;
         END LOOP;
      end if;
      ---
      delete from tsf_wo_detail
         where tsf_wo_id = I_wo_id
           and item = I_item
           and activity_id is NULL;
      ---
      insert into tsf_wo_detail(tsf_wo_detail_id,
                                tsf_wo_id,
                                item,
                                activity_id,
                                unit_cost,
                                comments,
                                inv_status)
                         select TSF_WO_DETAIL_ID_SEQ.nextval,
                                I_wo_id,
                                td.item,
                                wt.activity_id,
                                CURRENCY_SQL.CONVERT_VALUE(L_cost_retail_ind,
                                                           I_finisher_curr,
                                                           I_primary_curr,
                                                           wt.unit_cost),
                                I_comments,
                                I_inv_status
                           from tsfdetail td,
                                wo_tmpl_detail wt
                          where td.tsf_no = I_transfer
                            and td.item = I_item
                            and wt.wo_tmpl_id = I_activity_id
                            and not exists (select 1
                                              from tsf_wo_detail tw
                                             where tw.tsf_wo_id = I_wo_id
                                               and tw.item = td.item
                                               and tw.activity_id = wt.activity_id
                                               and NVL(tw.inv_status, -999) = DECODE(tw.inv_status, NULL, -999,I_inv_status));

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'Item '||I_item,
                                            'Work Order ID ' || I_wo_id );
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_APPLY;
---------------------------------------------------------------------
FUNCTION ALL_ITEMS_APPLY(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_overwrite     IN      BOOLEAN,
                         I_transfer      IN      TSF_WO_HEAD.TSF_NO%TYPE,
                         I_wo_id         IN      TSF_WO_HEAD.TSF_WO_ID%TYPE,
                         I_activity_type IN      VARCHAR2,
                         I_activity_id   IN      WO_ACTIVITY.ACTIVITY_ID%TYPE,
                         I_unit_cost     IN      TSF_WO_DETAIL.UNIT_COST%TYPE,
                         I_comments      IN      TSF_WO_DETAIL.COMMENTS%TYPE,
                         I_finisher_curr IN      CURRENCIES.CURRENCY_CODE%TYPE,
                         I_primary_curr  IN      CURRENCIES.CURRENCY_CODE%TYPE,
                         I_inv_status    IN      TSF_WO_DETAIL.INV_STATUS%TYPE DEFAULT NULL)

RETURN BOOLEAN IS

   L_program           VARCHAR2(61)                  := 'TSF_WO_SQL.ALL_ITEMS_APPLY';
   L_table             VARCHAR2(20)                  := 'TSF_WO_DETAIL';
   L_cost_retail_ind   VARCHAR2(1)                   := 'C';
   L_unit_cost         TSF_WO_DETAIL.UNIT_COST%TYPE;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);


   cursor C_LOCK_RECORD is
      select 'x'
        from tsf_wo_detail
       where tsf_wo_id = I_wo_id
         for update nowait;

   cursor C_UPDATE_ALL_TMPL is
      select ts.rowid,
             ts.comments,
             wt.unit_cost
        from tsf_wo_detail ts,
             wo_tmpl_detail wt
       where ts.tsf_wo_id = I_wo_id
         and wt.wo_tmpl_id = I_activity_id
         and wt.activity_id = ts.activity_id;

BEGIN

   --- Check required input parameters
   if I_wo_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wo_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_transfer is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_transfer',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_LOCK_RECORD;
   close C_LOCK_RECORD;
   ---
   if I_activity_type = 'A' OR I_activity_type is NULL then

      --This will update records for all items in tsf_wo_detail.
      if I_overwrite then
         update tsf_wo_detail ts
            set ts.activity_id = NVL(I_activity_id, ts.activity_id),
                ts.unit_cost = NVL(I_unit_cost, ts.unit_cost),
                ts.comments = I_comments
          where ts.tsf_wo_id = I_wo_id
            and ts.activity_id = I_activity_id;
      end if;

      -- Update all items without activities
      update tsf_wo_detail ts
         set ts.activity_id = NVL(I_activity_id, ts.activity_id),
             ts.unit_cost = NVL(I_unit_cost, ts.unit_cost),
             ts.comments = I_comments,
             ts.inv_status = I_inv_status
       where ts.tsf_wo_id = I_wo_id
         and ts.activity_id is NULL;

      --This will insert records for items in tsf_wo_detail for all items.
      insert into tsf_wo_detail(tsf_wo_detail_id,
                                tsf_wo_id,
                                item,
                                activity_id,
                                unit_cost,
                                comments,
                                inv_status)
                         select TSF_WO_DETAIL_ID_SEQ.nextval,
                                I_wo_id,
                                item,
                                I_activity_id,
                                I_unit_cost,
                                I_comments,
                                I_inv_status
                           from tsfdetail td
                          where td.tsf_no = I_transfer
                            and not exists (select 1
                                              from tsf_wo_detail ts
                                             where ts.tsf_wo_id = I_wo_id
                                               and ts.item = td.item
                                               and NVL(ts.activity_id, -999) = DECODE(ts.activity_id, NULL, -999, I_activity_id)
                                               and NVL(ts.inv_status, -999) = DECODE(ts.inv_status, NULL, -999, I_inv_status));
   elsif I_activity_type = 'T' then
      if I_overwrite then
         FOR rec IN C_UPDATE_ALL_TMPL LOOP
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    rec.unit_cost,
                                    I_primary_curr,
                                    I_finisher_curr,
                                    L_unit_cost,
                                    L_cost_retail_ind,
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
            --- Use the converted value to update the activity's unit cost in the tsf_wo_detail table
            update tsf_wo_detail
               set unit_cost = L_unit_cost,
                   comments  = I_comments
             where rowid = rec.rowid;
         END LOOP;
      end if;
      ---
      delete from tsf_wo_detail ts
       where ts.tsf_wo_id = I_wo_id
         and activity_id is NULL;
      ---
      insert into tsf_wo_detail(tsf_wo_detail_id,
                                tsf_wo_id,
                                item,
                                activity_id,
                                unit_cost,
                                comments,
                                inv_status)
                         select TSF_WO_DETAIL_ID_SEQ.nextval,
                                I_wo_id,
                                td.item,
                                wt.activity_id,
                                CURRENCY_SQL.CONVERT_VALUE(L_cost_retail_ind,
                                                           I_finisher_curr,
                                                           I_primary_curr,
                                                           wt.unit_cost),
                                I_comments,
                                I_inv_status
                           from tsfdetail td,
                                wo_tmpl_detail wt
                          where td.tsf_no = I_transfer
                            and wt.wo_tmpl_id = I_activity_id
                            and not exists (select 1
                                              from tsf_wo_detail tw
                                             where tw.tsf_wo_id = I_wo_id
                                               and tw.item = td.item
                                               and tw.activity_id = wt.activity_id
                                               and NVL(tw.inv_status, -999) = DECODE(tw.inv_status, NULL, -999, I_inv_status));
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'Work Order ID '||I_wo_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ALL_ITEMS_APPLY;
--------------------------------------------------------------------
FUNCTION DELETE_ITEMS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_item          IN      TSF_WO_DETAIL.ITEM%TYPE,
                      I_tsf_no        IN      TSFDETAIL.TSF_NO%TYPE,
                      I_wo_id         IN      TSF_WO_HEAD.TSF_WO_ID%TYPE,
                      I_activity_id   IN      WO_ACTIVITY.ACTIVITY_ID%TYPE,
                      I_delete_header IN      VARCHAR2 DEFAULT 'N')

RETURN BOOLEAN IS

   L_program      VARCHAR2(61)  := 'TSF_WO_SQL.DELETE_ITEMS';
   L_table        VARCHAR2(20)  := 'TSF_WO_DETAIL';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_RECORD is
      select 'x'
        from tsf_wo_detail ts,
             tsf_wo_head td
       where ts.tsf_wo_id = I_wo_id
         and td.tsf_wo_id = ts.tsf_wo_id
         and ts.item = NVL(I_item, ts.item)
         and NVL(ts.activity_id, -999) = DECODE(ts.activity_id, NULL, -999, I_activity_id)
         for update nowait;

BEGIN
   --- Check required input parameters
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_wo_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wo_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_LOCK_RECORD;
   close C_LOCK_RECORD;

   if I_item is NULL then
      -- This will clear out all items not in tsfdetail in startup
      delete from tsf_wo_detail tw
       where tsf_wo_id = I_wo_id
         and not exists (select 1
                           from tsfdetail td
                          where td.tsf_no = I_tsf_no
                            and td.item = tw.item);
   else
      -- This will clear out the item specified by the user
      delete from tsf_wo_detail td
       where tsf_wo_id = I_wo_id
         and item = I_item
         and activity_id = I_activity_id;
   end if;

   if I_delete_header = 'Y' then
     delete from tsf_wo_head
      where tsf_wo_id = I_wo_id
        and tsf_no = I_tsf_no
        and not exists (select 1
                          from tsf_wo_detail td
                         where td.tsf_wo_id = I_wo_id);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            L_table,
                                            'Work Order ID '||I_wo_id,
                                            'Item ' || I_item);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ITEMS;
---------------------------------------------------------------------
FUNCTION GET_NEXT_WRKORDER_HEAD_NO(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists            IN OUT  BOOLEAN,
                                   O_wrkorder_head_id  IN OUT  TSF_WO_HEAD.TSF_WO_ID%TYPE,
                                   I_tsf_no            IN      TSF_WO_HEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'TSF_WO_SQL.GET_NEXT_WRKORDER_HEAD_NO';
   L_exists   VARCHAR2(1);

   cursor C_CHECK_WO is
      select 'x'
        from tsf_wo_head
       where tsf_no = I_tsf_no;

   cursor C_NEXT_HEAD_SEQ_NO is
      select tsf_wo_id_seq.nextval
        from dual;

BEGIN

   open C_CHECK_WO;
   fetch C_CHECK_WO into L_exists;
   close C_CHECK_WO;

   if L_exists is NULL then
      open C_NEXT_HEAD_SEQ_NO;
      fetch C_NEXT_HEAD_SEQ_NO into O_wrkorder_head_id;
      close C_NEXT_HEAD_SEQ_NO;
      ---
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_NEXT_WRKORDER_HEAD_NO;
---------------------------------------------------------------------
FUNCTION GET_NEXT_WRKORDER_DETAIL_NO(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_wrkorder_detail_id  IN OUT  TSF_WO_DETAIL.TSF_WO_DETAIL_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'TSF_WO_SQL.GET_NEXT_WRKORDER_DETAIL_NO';

   cursor C_NEXT_DETAIL_SEQ_NO is
      select TSF_WO_DETAIL_ID_SEQ.nextval
        from dual;

BEGIN

   open C_NEXT_DETAIL_SEQ_NO;
   fetch C_NEXT_DETAIL_SEQ_NO into O_wrkorder_detail_id;
   close C_NEXT_DETAIL_SEQ_NO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_NEXT_WRKORDER_DETAIL_NO;
---------------------------------------------------------------------
FUNCTION CALC_ACTIVITY_TOTAL(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_upd_inv_total    IN OUT WO_ACTIVITY.UNIT_COST%TYPE,
                             O_post_fin_total   IN OUT WO_ACTIVITY.UNIT_COST%TYPE,
                             I_tsf_no           IN     TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(61)  := 'TSF_WO_SQL.CALC_ACTIVITY_TOTAL';
   L_cost_type  WO_ACTIVITY.COST_TYPE%TYPE := 'U';

   cursor CALC_TOTAL is
      select NVL(sum(twd.unit_cost * td.tsf_qty), 0)
        from tsf_wo_head twh,
             tsf_wo_detail twd,
             wo_activity wo,
             tsfdetail td
       where twh.tsf_no = I_tsf_no
         and twh.tsf_wo_id = twd.tsf_wo_id
         and twd.activity_id = wo.activity_id
         and td.tsf_no = twh.tsf_no
         and td.item = twd.item
         and wo.cost_type = L_cost_type;

BEGIN

   open CALC_TOTAL;
   fetch CALC_TOTAL into O_upd_inv_total;
   close CALC_TOTAL;
   ---
   L_cost_type := 'P';
   ---
   open CALC_TOTAL;
   fetch CALC_TOTAL into O_post_fin_total;
   close CALC_TOTAL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_ACTIVITY_TOTAL;
---------------------------------------------------------------------
FUNCTION CHECK_NO_ACTIVITY(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exist            IN OUT BOOLEAN,
                           I_wo_id            IN     TSF_WO_HEAD.TSF_WO_ID%TYPE,
                           I_tsf_no           IN     TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(61)  := 'TSF_WO_SQL.CHECK_NO_ACTIVITY';
   L_exist      VARCHAR2(1)   := 'N';

   cursor C_CHECK_ACTIVITY is
      select 'Y'
        from tsf_wo_detail
       where tsf_wo_id = I_wo_id
         and activity_id is NULL;

BEGIN

   open C_CHECK_ACTIVITY;
   fetch C_CHECK_ACTIVITY into L_exist;
   close C_CHECK_ACTIVITY;

   O_exist := (L_exist = 'Y');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_NO_ACTIVITY;
---------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ITEM_MASTER V_TSFDETAIL
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TRANSFER_ITEMS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid          IN OUT  BOOLEAN,
                                 O_item_desc      IN OUT  ITEM_MASTER.ITEM_DESC%TYPE,
                                 I_tsf_no         IN      TSFDETAIL.TSF_NO%TYPE,
                                 I_item           IN      TSF_WO_DETAIL.ITEM%TYPE,
                                 I_item_type      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program          VARCHAR2(61)  := 'TSF_WO_SQL.VALIDATE_TRANSFER_ITEMS';
   L_invalid_param    VARCHAR2(20);
   L_valid            BOOLEAN;
   L_item_master_row  V_ITEM_MASTER%ROWTYPE;
   L_exists           VARCHAR2(1)   := 'N';
   INVALID_ERROR      EXCEPTION;

   cursor C_TSF_ITEM_PARENT is
      select 'Y'
        from v_item_master im,
             v_tsfdetail td
       where td.tsf_no      = I_tsf_no
         and td.item        = im.item
         and im.item_parent = I_item;

   cursor C_TSF_ITEM is
      select 'Y'
        from v_tsfdetail td
       where td.tsf_no = I_tsf_no
         and td.item   = I_item;

BEGIN

   --- Validate input
   if I_item is NULL then
      L_invalid_param := 'I_item';
   elsif I_tsf_no is NULL then
      L_invalid_param := 'I_tsf_no';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                            L_invalid_param,
                                            'NULL',
                                            L_program);
      return FALSE;
   end if;

   --- Verify if the user has visibility to the item entered
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_ITEM_MASTER(O_error_message,
                                                   L_valid,
                                                   L_item_master_row,
                                                   I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if L_valid = FALSE THEN
      raise INVALID_ERROR;
   end if;

   --- Check that item_type matches the entered item
   if  (I_item_type is NULL or I_item_type = 'I')
   and L_item_master_row.item_level != L_item_master_row.tran_level then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_ENT_STYLES',NULL,NULL,NULL);
      raise INVALID_ERROR;
   elsif I_item_type in ('ST', 'STC')
   and   L_item_master_row.item_level >= L_item_master_row.tran_level then
      O_error_message := SQL_LIB.CREATE_MSG('ABOVE_TRAN_LEVEL',NULL,NULL,NULL);
      raise INVALID_ERROR;
   end if;

   --- Check if item/item_parent is on the transfer
   if I_item_type in ('ST', 'STC') then
      open  C_TSF_ITEM_PARENT;
      fetch C_TSF_ITEM_PARENT into L_exists;
      close C_TSF_ITEM_PARENT;
   else
      open  C_TSF_ITEM;
      fetch C_TSF_ITEM into L_exists;
      close C_TSF_ITEM;
   end if;
   ---
   if L_exists = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_ITEM',NULL,NULL,NULL);
      raise INVALID_ERROR;
   end if;

   O_valid := TRUE;
   O_item_desc := L_item_master_row.item_desc;

   return TRUE;

EXCEPTION
   when INVALID_ERROR then
      O_valid     := FALSE;
      O_item_desc := NULL;
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_TRANSFER_ITEMS;
---------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ITEM_MASTER V_TSFDETAIL
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_DIFF_ID(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid          IN OUT  BOOLEAN,
                            O_diff_desc      IN OUT  V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                            I_tsf_no         IN      TSFDETAIL.TSF_NO%TYPE,
                            I_item           IN      TSF_WO_DETAIL.ITEM%TYPE,
                            I_diff_id        IN      DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61)  := 'TSF_WO_SQL.CHECK_ITEM_DIFF_ID';
   L_exists       VARCHAR2(1)  := 'N';
   L_diff_desc    V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   INVALID_ERROR  EXCEPTION;

   cursor C_DIFF_TSF is
      select 'Y'
       from v_item_master im,
            v_tsfdetail td
      where td.tsf_no      = I_tsf_no
        and td.item        = im.item
        and im.item_parent = NVL(I_item, im.item_parent)
        and (NVL(im.diff_1, '-999') = NVL(I_diff_id, NVL(im.diff_1, '-999')) or
             NVL(im.diff_2, '-999') = NVL(I_diff_id, NVL(im.diff_2, '-999')) or
             NVL(im.diff_3, '-999') = NVL(I_diff_id, NVL(im.diff_3, '-999')) or
             NVL(im.diff_4, '-999') = NVL(I_diff_id, NVL(im.diff_4, '-999')));

   cursor C_DIFF is
      select diff_desc
        from diff_ids
       where diff_id = I_diff_id;

BEGIN

   --- Validate input
   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',
                                            'I_tsf_no',
                                            'NULL',
                                            L_program);
      return FALSE;
   end if;

   --- Get diff description
   open  C_DIFF;
   fetch C_DIFF into L_diff_desc;
   close C_DIFF;
   ---
   if L_diff_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID',NULL,NULL,NULL);
      raise INVALID_ERROR;
   end if;

   --- Diff exists so see if it is on the transfer
   open  C_DIFF_TSF;
   fetch C_DIFF_TSF into L_exists;
   close C_DIFF_TSF;
   ---
   if L_exists = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_DIFF',NULL,NULL,NULL);
      raise INVALID_ERROR;
   end if;

   O_valid     := TRUE;
   O_diff_desc := L_diff_desc;

   return TRUE;

EXCEPTION
   when INVALID_ERROR then
      O_valid     := FALSE;
      O_diff_desc := NULL;
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_ITEM_DIFF_ID;
---------------------------------------------------------------------
FUNCTION TOTAL_WO_COST(O_error_message              IN OUT  VARCHAR2,
                       O_total_inventory_from       IN OUT  ORDLOC.UNIT_COST%TYPE,
                       O_total_inventory_to         IN OUT  ORDLOC.UNIT_COST%TYPE,
                       O_total_inventory_prim       IN OUT  ORDLOC.UNIT_COST%TYPE,
                       O_total_financial_from       IN OUT  ORDLOC.UNIT_COST%TYPE,
                       O_total_financial_to         IN OUT  ORDLOC.UNIT_COST%TYPE,
                       O_total_financial_prim       IN OUT  ORDLOC.UNIT_COST%TYPE,
                       I_tsf_no                     IN      TSFHEAD.TSF_NO%TYPE,
                       I_tsf_type                   IN      TSFHEAD.TSF_TYPE%TYPE,
                       I_from_loc                   IN      TSFHEAD.FROM_LOC%TYPE,
                       I_from_loc_type              IN      TSFHEAD.FROM_LOC_TYPE%TYPE,
                       I_to_loc                     IN      TSFHEAD.TO_LOC%TYPE,
                       I_to_loc_type                IN      TSFHEAD.TO_LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_from_loc_type    TSFHEAD.FROM_LOC_TYPE%TYPE := I_from_loc_type;
   L_program          VARCHAR2(64) := 'TSF_WO_SQL.TOTAL_WO_COST';

   cursor C_UPDATE_INVENTORY is
      select sum(twd.unit_cost * td.tsf_qty)
        from tsf_wo_detail twd,
             tsf_wo_head twh,
             wo_activity wa,
             tsfdetail td
       where twh.tsf_no = I_tsf_no
         and twd.tsf_wo_id = twh.tsf_wo_id
         and twd.unit_cost is not NULL
         and wa.activity_id = twd.activity_id
         and wa.cost_type = 'U'
         and td.tsf_no = twh.tsf_no
         and td.item = twd.item
         and td.tsf_qty is not NULL;

   cursor C_POST_FINANCIALS is
      select sum(twd.unit_cost * td.tsf_qty)
        from tsf_wo_detail twd,
             tsf_wo_head twh,
             wo_activity wa,
             tsfdetail td
       where twh.tsf_no = I_tsf_no
         and twd.tsf_wo_id = twh.tsf_wo_id
         and twd.unit_cost is not NULL
         and wa.activity_id = twd.activity_id
         and wa.cost_type = 'P'
         and td.tsf_no = twh.tsf_no
         and td.item = twd.item
         and td.tsf_qty is not NULL;

BEGIN
   O_total_inventory_from := 0;
   O_total_inventory_to   := 0;
   O_total_inventory_prim := 0;
   O_total_financial_from := 0;
   O_total_financial_to   := 0;
   O_total_financial_prim := 0;

   if I_tsf_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_from_loc IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_from_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_from_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_to_loc IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_to_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if L_from_loc_type = 'I' then
      L_from_loc_type := 'W';
   end if;
   -- -------------------------------------------------------------------------
   -- Fetch Update Inventory Cost total
   -- -------------------------------------------------------------------------
   open  C_UPDATE_INVENTORY;
   fetch C_UPDATE_INVENTORY into O_total_inventory_from;
   close C_UPDATE_INVENTORY;
   --
   -- convert total inventory cost from 'from' local currency to primary currency
   --
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       I_from_loc,
                                       L_from_loc_type,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       O_total_inventory_from,
                                       O_total_inventory_prim,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
         return FALSE;
   end if;
   ---
   -- convert total inventory cost from 'from' local currency to 'to' local currency
   ---
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       I_from_loc,
                                       L_from_loc_type,
                                       NULL,
                                       I_to_loc,
                                       I_to_loc_type,
                                       NULL,
                                       O_total_inventory_from,
                                       O_total_inventory_to,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
         return FALSE;
   end if;
   -- -------------------------------------------------------------------------
   -- Fetch Post to Financials Cost total
   -- -------------------------------------------------------------------------
   open  C_POST_FINANCIALS;
   fetch C_POST_FINANCIALS into O_total_financial_from;
   close C_POST_FINANCIALS;
   --
   -- convert total inventory cost from 'from' local currency to primary currency
   --
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       I_from_loc,
                                       L_from_loc_type,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       O_total_financial_from,
                                       O_total_financial_prim,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
         return FALSE;
   end if;
   --
   -- convert total inventory cost from 'from' local currency to 'to' local currency
   --
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       I_from_loc,
                                       L_from_loc_type,
                                       NULL,
                                       I_to_loc,
                                       I_to_loc_type,
                                       NULL,
                                       O_total_financial_from,
                                       O_total_financial_to,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
         return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END TOTAL_WO_COST;
---------------------------------------------------------------------
END TSF_WO_SQL;
/
