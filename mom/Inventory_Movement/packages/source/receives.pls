CREATE OR REPLACE PACKAGE RECEIVE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
   -- Define errors so procedures can raise_application_error
   ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;
   INVALID_ERROR         EXCEPTION;
   PARAM_ERROR           EXCEPTION;
-------------------------------------------------------------------------------------------
   TYPE CARTON_RECV_REC IS RECORD(CONTAINER_ID          SHIPSKU.CARTON%TYPE,
                                  RECEIVED_CARTON_IND   VARCHAR2(1),
                                  ERROR_MESSAGE         RTK_ERRORS.RTK_TEXT%TYPE,
                                  RETURN_CODE           VARCHAR2(5));

   TYPE CARTON_RECV_TABLE IS TABLE OF CARTON_RECV_REC
      INDEX BY BINARY_INTEGER;

   TYPE CARTON_RECV_ITEM_REC IS RECORD(ITEM            ITEM_MASTER.ITEM%TYPE,
                                       ITEM_DESC       ITEM_MASTER.ITEM_DESC%TYPE,
                                       QTY_EXPECTED    SHIPSKU.QTY_EXPECTED%TYPE,
                                       ERROR_MESSAGE   RTK_ERRORS.RTK_TEXT%TYPE,
                                       RETURN_CODE     VARCHAR2(5));

   TYPE CARTON_RECV_ITEM_TABLE IS TABLE OF CARTON_RECV_ITEM_REC
      INDEX BY BINARY_INTEGER;

   TYPE ITEM_RECV_REC IS RECORD(ITEM                  ITEM_MASTER.ITEM%TYPE,
                                ITEM_DESC             ITEM_MASTER.ITEM_DESC%TYPE,
                                CARTON                SHIPSKU.CARTON%TYPE,
                                DISTRO_NO             SHIPSKU.DISTRO_NO%TYPE,
                                DISTRO_TYPE           SHIPSKU.DISTRO_TYPE%TYPE,
                                QTY_EXPECTED          SHIPSKU.QTY_EXPECTED%TYPE,
                                QTY_RECEIVED          SHIPSKU.QTY_RECEIVED%TYPE,
                                RECEIPT_UNIT          UOM_CLASS.UOM%TYPE,
                                WEIGHT_RECEIVED       SHIPSKU.WEIGHT_RECEIVED%TYPE,
                                WEIGHT_RECEIVED_UOM   SHIPSKU.WEIGHT_RECEIVED_UOM%TYPE,
                                ERROR_MESSAGE         RTK_ERRORS.RTK_TEXT%TYPE,
                                RETURN_CODE           VARCHAR2(5),
                                INV_STATUS            INV_STATUS_CODES.INV_STATUS_CODE%TYPE);

   TYPE ITEM_RECV_TABLE IS TABLE OF ITEM_RECV_REC
      INDEX BY BINARY_INTEGER;

   TYPE DISTRO_TYPE_TAB IS TABLE OF SHIPSKU.DISTRO_TYPE%TYPE
      INDEX BY BINARY_INTEGER;
-----------------------------------------------------------------------------------
-- Name   : VALIDATE_ITEM_LVL_RCPT
-- Purpose: This function will validate shipment records.
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_LVL_RCPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid           IN OUT   BOOLEAN,
                                I_asn_bol_no      IN       SHIPMENT.ASN%TYPE,
                                I_location        IN       SHIPMENT.TO_LOC%TYPE,
                                I_distro_type     IN       SHIPSKU.DISTRO_TYPE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name   : VALIDATE_CARTON_LVL_RCPT
-- Purpose: This function will validate shipment records.
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_CARTON_LVL_RCPT(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_valid                IN OUT   BOOLEAN,
                                  O_item_table           IN OUT   STOCK_ORDER_RCV_SQL.ITEM_TAB,
                                  O_qty_expected_table   IN OUT   STOCK_ORDER_RCV_SQL.QTY_TAB,
                                  O_inv_status_table     IN OUT   STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                                  O_carton_table         IN OUT   STOCK_ORDER_RCV_SQL.CARTON_TAB,
                                  O_distro_no_table      IN OUT   STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                                  O_tampered_ind_table   IN OUT   STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB,
                                  O_distro_type_table    IN OUT   RECEIVE_SQL.DISTRO_TYPE_TAB,
                                  O_doc_type             IN OUT   SHIPSKU.DISTRO_TYPE%TYPE,
                                  I_carton               IN       SHIPSKU.CARTON%TYPE,
                                  I_shipment             IN       SHIPMENT.SHIPMENT%TYPE,
                                  I_bol_asn_no           IN       SHIPMENT.BOL_NO%TYPE,
                                  I_to_loc               IN       SHIPMENT.TO_LOC%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name   : RECEIVE_ITEMS
-- Purpose: This procedure will perform item level receiving executed by the user through
--          the item receiving form (recitem.fmb).
-------------------------------------------------------------------------------------------
PROCEDURE RECEIVE_ITEMS(O_item_receiving_table   IN OUT   RECEIVE_SQL.ITEM_RECV_TABLE,
                        I_shipment               IN       SHIPMENT.SHIPMENT%TYPE,
                        I_distro_content         IN       VARCHAR2,
                        I_location               IN       SHIPMENT.TO_LOC%TYPE,
                        I_asn_bol_no             IN       SHIPMENT.ASN%TYPE,
                        I_order_no               IN       SHIPMENT.ORDER_NO%TYPE,
                        I_receipt_date           IN       SHIPMENT.RECEIVE_DATE%TYPE,
                        I_disposition            IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE);
-------------------------------------------------------------------------------------------
-- Name   : RECEIVE_CARTON
-- Purpose: This procedure will perform carton level receiving executed by the user
--          through the carton receiving form (reccarton.fmb).
-------------------------------------------------------------------------------------------
PROCEDURE RECEIVE_CARTON(O_carton_receiving_table   IN OUT   RECEIVE_SQL.CARTON_RECV_TABLE,
                         I_shipment                 IN       SHIPMENT.SHIPMENT%TYPE,
                         I_asn_bol_no               IN       SHIPMENT.ASN%TYPE,
                         I_order_no                 IN       SHIPMENT.ORDER_NO%TYPE,
                         I_to_loc                   IN       SHIPMENT.TO_LOC%TYPE,
                         I_receipt_date             IN       SHIPMENT.RECEIVE_DATE%TYPE);
-------------------------------------------------------------------------------------------
-- Name   : FETCH_CARTON_BULK
-- Purpose: This procedure will retrieve all cartons to be received based on the search
--          criteria specified by the reccarton.fmb form.
-------------------------------------------------------------------------------------------
PROCEDURE FETCH_CARTON_BULK(O_carton_table   IN OUT   RECEIVE_SQL.CARTON_RECV_TABLE,
                            I_shipment       IN       SHIPMENT.SHIPMENT%TYPE);
-------------------------------------------------------------------------------------------
-- Name   : FETCH_CARTON_ITEMS_BULK
-- Purpose: This procedure will return all item records for a carton and shipment number.
--          This will be called by the reccarton.fmb form.
-------------------------------------------------------------------------------------------
PROCEDURE FETCH_CARTON_ITEMS_BULK(O_carton_details_table   IN OUT   RECEIVE_SQL.CARTON_RECV_ITEM_TABLE,
                                  I_shipment               IN       SHIPMENT.SHIPMENT%TYPE,
                                  I_carton                 IN       SHIPSKU.CARTON%TYPE);
-------------------------------------------------------------------------------------------
-- Name   : FETCH_ITEM_BULK
-- Purpose: This procedure will retrieve item level shipment records from shipsku and
--          item_master tables for the passed in shipment number.
--          The recitem.fmb form will call this function.
-------------------------------------------------------------------------------------------
PROCEDURE FETCH_ITEM_BULK(O_receive_item_table   IN OUT   RECEIVE_SQL.ITEM_RECV_TABLE,
                          O_distro_contents      IN OUT   VARCHAR2,
                          I_shipment             IN       SHIPMENT.SHIPMENT%TYPE);
-------------------------------------------------------------------------------------------
-- Name   : LOCK_RECV_SHIPMENT
-- Purpose: This function will lock the shipment records for the shipment selected in the
--          receiving forms.
-------------------------------------------------------------------------------------------
FUNCTION LOCK_RECV_SHIPMENT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_shipment      IN     SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name   : GET_RECV_FIRST_TSF
-- Purpose: This function will get the first transfer in the shipment.
-------------------------------------------------------------------------------------------
FUNCTION GET_RECV_FIRST_TSF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_tsf           IN OUT SHIPSKU.DISTRO_NO%TYPE,
                            I_shipment      IN     SHIPSKU.SHIPMENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:      GET_INV_CODES
-- Purpose:   Called from recitem.fmb to validate if inv_status_codes exists for
--            the selected criteria.
-------------------------------------------------------------------------------------------
FUNCTION GET_INV_CODES(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists                 IN OUT   BOOLEAN,
                       O_inv_status_code_desc   IN OUT   INV_STATUS_CODES_TL.INV_STATUS_CODE_DESC%TYPE,
                       I_inv_code               IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name   : RETRIEVE_SHIPMENT
-- Purpose: This function will retrieve related shipment details based on the search criteria
--          in item receiving form (recitem.fmb).
-------------------------------------------------------------------------------------------
FUNCTION RETRIEVE_SHIPMENT (
                            I_LOCATION	      IN       SHIPMENT.TO_LOC%TYPE,
                            I_ASN             IN       SHIPMENT.ASN%TYPE,
                            I_BOL_NO          IN       SHIPMENT.BOL_NO%TYPE,
                            I_LOC_TYPE        IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                            I_LOCTYPE_S       IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                            I_LOCTYPE_W       IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                            I_DISTRO_NUMBER   IN       SHIPSKU.DISTRO_NO%TYPE,
                            I_DISTRO_TYPE     IN       SHIPSKU.DISTRO_TYPE%TYPE)
                            
RETURN SHIP_TYPE_TBL  PIPELINED;
-------------------------------------------------------------------------------------------
-- Name   : RECEIVE_ITEMS_LOCK
-- Purpose: This function will perform locking while receiving is executed by the user through
--          the item receiving form (recitem.fmb).
-------------------------------------------------------------------------------------------
FUNCTION RECEIVE_ITEMS_LOCK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_shipment        IN       SHIPMENT.SHIPMENT%TYPE,
                            I_distro_number   IN       TSFHEAD.TSF_NO%TYPE
)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name   : ORDLOC_ITEMS_LOCK 
-- Purpose: This function will perform locking on ordloc table while receiving is executed by the 
--          user through the item receiving form (recitem.fmb). 
------------------------------------------------------------------------------------------- 
FUNCTION ORDLOC_ITEMS_LOCK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE, 
                           I_item            IN       ORDLOC.ITEM%TYPE, 
                           I_loc             IN       ORDLOC.LOCATION%TYPE                            
) 
   RETURN BOOLEAN; 
-------------------------------------------------------------------------------------------
-- Name   : RECEIVE_CARTON_WRP
-- Purpose: This is a wrapper function for RECEIVE_CARTON that passes a database
-- type object instead of a PL/SQL type as an input out parameter to be called from Java wrappers.
-------------------------------------------------------------------------------------------
FUNCTION RECEIVE_CARTON_WRP(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_carton_receiving_table   IN OUT   WRP_CARTON_RECV_TBL,
                            I_shipment                 IN       SHIPMENT.SHIPMENT%TYPE,
                            I_asn_bol_no               IN       SHIPMENT.ASN%TYPE,
                            I_order_no                 IN       SHIPMENT.ORDER_NO%TYPE,
                            I_to_loc                   IN       SHIPMENT.TO_LOC%TYPE,
                            I_receipt_date             IN       SHIPMENT.RECEIVE_DATE%TYPE)
   RETURN NUMBER;

-------------------------------------------------------------------------------------------
-- Name   : RECEIVE_ITEMS_WRP
-- Purpose: This is a wrapper function for RECEIVE_ITEMS that passes a database
-- type object instead of a PL/SQL type as an input out parameter to be called from Java wrappers.

FUNCTION RECEIVE_ITEMS_WRP(
                        O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_item_receiving_table     IN OUT   WRP_ITEM_RECV_TBL,
                        I_shipment                 IN       SHIPMENT.SHIPMENT%TYPE,
                        I_distro_content           IN       VARCHAR2,
                        I_location                 IN       SHIPMENT.TO_LOC%TYPE,
                        I_asn_bol_no               IN       SHIPMENT.ASN%TYPE,
                        I_order_no                 IN       SHIPMENT.ORDER_NO%TYPE,
                        I_receipt_date             IN       SHIPMENT.RECEIVE_DATE%TYPE,
                        I_disposition              IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------
-- Name   : INSERT_RECEIVE_ITEMS_GTT
-- Purpose: This procedure will retrieve item level shipment records from shipsku and
--          item_master tables for the passed in shipment number and inserts into temporary table.
-------------------------------------------------------------------------------------------
FUNCTION INSERT_RECEIVE_ITEMS_GTT(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_distro_contents      IN OUT   VARCHAR2,
                          I_shipment             IN       SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN;                          
-------------------------------------------------------------------------------------------
END RECEIVE_SQL;
/