-- File Name : CORESVC_WO_ACTIVITY_body.pls
------------------------------------------------------------------
CREATE OR REPLACE PACKAGE BODY CORESVC_WORK_ORDER AS
LP_error BOOLEAN;
   cursor C_SVC_WO_ACTIVITY(I_process_id NUMBER,I_chunk_id NUMBER) is
      select uk_wo_activity.rowid          as uk_wo_activity_rid,
             uk_wo_activity.activity_id,
             uk_wo_activity.cost_type      as old_cost_type,
             st.cost_type,
             st.unit_cost,
             st.activity_desc,
             UPPER(st.activity_code)       as activity_code,
             st.process_id,
             st.row_seq,
             st.rowid as st_rid,
             st.chunk_id,
             upper(st.action) as action,
             st.process$status
        from svc_wo_activity st,
             wo_activity uk_wo_activity
       where st.process_id             = I_process_id
         and st.chunk_id               = I_chunk_id
         and UPPER(st.activity_code)   = uk_wo_activity.activity_code (+);


cursor C_SVC_WO_TMPL(I_process_id NUMBER,I_chunk_id NUMBER) is
   select t.uk_wo_tmpl_detail_rid,
          t.wo_tmpl_detail_id,
          t.wtd_woc_fk_rid,
          t.wtd_wth_fk_rid,
          UPPER(t.stwd_activity_code) as stwd_activity_code,
          t.stwd_activity_id,
          t.stwd_comments,
          t.stwd_unit_cost,
          t.stwd_wo_tmpl_id,
          t.stwd_process_id,
          t.stwd_row_seq,
          t.stwd_chunk_id,
          t.stwd_action,
          t.stwd_prcsts,
          LEAD(t.stwh_wo_tmpl_id, 1, 0) over (order by t.stwh_wo_tmpl_id) as next_stwh_wo_tmpl_id,
          ROW_NUMBER() over (partition by t.stwh_wo_tmpl_id
                                 order by t.stwh_wo_tmpl_id) as ch_rank,
          t.stwd_rid,
          t.stwh_rid,
          t.stwh_process_id,
          t.stwh_chunk_id,
          t.stwh_row_seq,
          t.stwh_action,
          t.stwh_prcsts,
          t.stwh_wo_tmpl_id,
          t.stwh_wo_tmpl_desc
     from (select uk_wo_tmpl_detail.rowid                as uk_wo_tmpl_detail_rid,
                  uk_wo_tmpl_detail.wo_tmpl_detail_id    as wo_tmpl_detail_id,
                  wtd_woc_fk.rowid                       as wtd_woc_fk_rid,
                  wtd_wth_fk.rowid                       as wtd_wth_fk_rid,
                  UPPER(stwd.activity_code)              as stwd_activity_code,
                  wtd_woc_fk.activity_id                 as stwd_activity_id,
                  stwd.comments                          as stwd_comments,
                  stwd.unit_cost                         as stwd_unit_cost,
                  stwd.wo_tmpl_id                        as stwd_wo_tmpl_id,
                  stwd.process_id                        as stwd_process_id,
                  stwd.row_seq                           as stwd_row_seq,
                  stwd.rowid                             as stwd_rid,
                  stwh.rowid                             as stwh_rid,
                  stwd.chunk_id                          as stwd_chunk_id,
                  UPPER(stwd.action)                     as stwd_action,
                  stwd.process$status                    as stwd_prcsts,
                  stwh.process_id                        as stwh_process_id,
                  stwh.chunk_id                          as stwh_chunk_id,
                  stwh.row_seq                           as stwh_row_seq,
                  UPPER(stwh.action)                     as stwh_action,
                  stwh.process$status                    as stwh_prcsts,
                  stwh.wo_tmpl_id                        as stwh_wo_tmpl_id,
                  stwh.wo_tmpl_desc                      as stwh_wo_tmpl_desc
             from svc_wo_tmpl_head stwh,
                  wo_tmpl_detail uk_wo_tmpl_detail,
                  wo_activity wtd_woc_fk,
                  wo_tmpl_head wtd_wth_fk,
                  svc_wo_tmpl_detail stwd
            where stwh.process_id                = I_process_id
              and stwh.chunk_id                  = I_chunk_id
              and stwh.process_id                = stwd.process_id(+)
              and stwh.chunk_id                  = stwd.chunk_id(+)
              and stwd.wo_tmpl_id                = uk_wo_tmpl_detail.wo_tmpl_id (+)
              and stwh.wo_tmpl_id                = wtd_wth_fk.wo_tmpl_id(+)
              and UPPER(stwd.activity_code)      = wtd_woc_fk.activity_code(+)
              and wtd_woc_fk.activity_id         = uk_wo_tmpl_detail.activity_id(+)
              and NVL(stwh.wo_tmpl_id,-1)        = NVL(stwd.wo_tmpl_id(+),-1)
        union all
           select uk_wo_tmpl_detail.rowid                as uk_wo_tmpl_detail_rid,
                  uk_wo_tmpl_detail.wo_tmpl_detail_id    as wo_tmpl_detail_id,
                  wtd_woc_fk.rowid                       as wtd_woc_fk_rid,
                  wtd_wth_fk.rowid                       as wtd_wth_fk_rid,
                  UPPER(stwd.activity_code)              as stwd_activity_code,
                  wtd_woc_fk.activity_id                 as stwd_activity_id,
                  stwd.comments                          as stwd_comments,
                  stwd.unit_cost                         as stwd_unit_cost,
                  stwd.wo_tmpl_id                        as stwd_wo_tmpl_id,
                  stwd.process_id                        as stwd_process_id,
                  stwd.row_seq                           as stwd_row_seq,
                  stwd.rowid                             as stwd_rid,
                  stwh.rowid                             as stwh_rid,
                  stwd.chunk_id                          as stwd_chunk_id,
                  UPPER(stwd.action)                     as stwd_action,
                  stwd.process$status                    as stwd_prcsts,
                  stwh.process_id                        as stwh_process_id,
                  stwh.chunk_id                          as stwh_chunk_id,
                  stwh.row_seq                           as stwh_row_seq,
                  UPPER(stwh.action)                     as stwh_action,
                  stwh.process$status                    as stwh_prcsts,
                  nvl(stwh.wo_tmpl_id,wtd_wth_fk.wo_tmpl_id)  as stwh_wo_tmpl_id,
                  stwh.wo_tmpl_desc                      as stwh_wo_tmpl_desc
             from svc_wo_tmpl_detail stwd,
                  wo_tmpl_detail uk_wo_tmpl_detail,
                  wo_activity wtd_woc_fk,
                  wo_tmpl_head wtd_wth_fk,
                  svc_wo_tmpl_head stwh
            where stwd.process_id                = I_process_id
              and stwd.chunk_id                  = I_chunk_id
              and stwd.process_id                = stwh.process_id(+)
              and stwd.chunk_id                  = stwh.chunk_id(+)
              and stwd.wo_tmpl_id                = uk_wo_tmpl_detail.wo_tmpl_id (+)
              and stwd.wo_tmpl_id                = wtd_wth_fk.wo_tmpl_id(+)
              and UPPER(stwd.activity_code)      = wtd_woc_fk.activity_code(+)
              and wtd_woc_fk.activity_id         = uk_wo_tmpl_detail.activity_id(+)
              and NVL(stwd.wo_tmpl_id,-1)        = NVL(stwh.wo_tmpl_id(+),-1)
              and stwh.wo_tmpl_id is NULL) t;

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type WO_ACTIVITY_REC is RECORD(Lang                 WO_ACTIVITY_TL.LANG%TYPE,
                                  Activity_id          WO_ACTIVITY_TL.ACTIVITY_ID%TYPE,
                                  Activity_code        WO_ACTIVITY.ACTIVITY_CODE%TYPE,
                                  Activity_desc        WO_ACTIVITY_TL.ACTIVITY_DESC%TYPE,
                                  Create_datetime      WO_ACTIVITY_TL.CREATE_DATETIME%TYPE,
                                  Create_id            WO_ACTIVITY_TL.CREATE_ID%TYPE,
                                  Last_update_datetime WO_ACTIVITY_TL.LAST_UPDATE_DATETIME%TYPE,
                                  Last_update_id       WO_ACTIVITY_TL.LAST_UPDATE_ID%TYPE);
   Type WO_ACTIVITY_TAB IS TABLE OF WO_ACTIVITY_REC;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
   Type WO_TMPL_HEAD_TAB IS TABLE OF WO_TMPL_HEAD_TL%ROWTYPE;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
----------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).file_id              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_seq_no         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).template_key         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).wksht_key            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).column_key           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).row_seq              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );

   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_id            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_datetime      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_id       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_datetime := SYSDATE;
END WRITE_S9T_ERROR;
------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
---------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets            S9T_PKG.names_map_typ;
   WO_ACTIVITY_cols    S9T_PKG.names_map_typ;
   WO_ACTIVITY_TL_cols S9T_PKG.names_map_typ;
   WO_TMPL_HEAD_cols     s9t_pkg.names_map_typ;
   WO_TMPL_HEAD_TL_cols  s9t_pkg.names_map_typ;
   WO_TMPL_DETAIL_cols   s9t_pkg.names_map_typ;
BEGIN
   L_sheets                      := S9T_PKG.get_sheet_names(I_file_id);
   WO_ACTIVITY_cols              := S9T_PKG.get_coL_names(I_file_id,WO_ACTIVITY_sheet);
   WO_ACTIVITY$Action            := WO_ACTIVITY_cols('ACTION');
   WO_ACTIVITY$COST_TYPE         := WO_ACTIVITY_cols('COST_TYPE');
   WO_ACTIVITY$UNIT_COST         := WO_ACTIVITY_cols('UNIT_COST');
   WO_ACTIVITY$ACTIVITY_DESC     := WO_ACTIVITY_cols('ACTIVITY_DESC');
   WO_ACTIVITY$ACTIVITY_CODE     := WO_ACTIVITY_cols('ACTIVITY_CODE');

   WO_ACTIVITY_TL_cols           := S9T_PKG.get_coL_names(I_file_id,WO_ACTIVITY_TL_SHEET);
   WO_ACTIVITY_TL$Action         := WO_ACTIVITY_TL_cols('ACTION');
   WO_ACTIVITY_TL$LANG           := WO_ACTIVITY_TL_cols('LANG');
   WO_ACTIVITY_TL$ACTIVITY_CODE  := WO_ACTIVITY_TL_cols('ACTIVITY_CODE');
   WO_ACTIVITY_TL$ACTIVITY_DESC  := WO_ACTIVITY_TL_cols('ACTIVITY_DESC');
   
   WO_TMPL_HEAD_TL_cols                    :=s9t_pkg.get_coL_names(I_file_id,WO_TMPL_HEAD_TL_SHEET);
   WO_TMPL_HEAD_TL$Action                  := WO_TMPL_HEAD_TL_cols('ACTION');
   WO_TMPL_HEAD_TL$LANG                    := WO_TMPL_HEAD_TL_cols('LANG');
   WO_TMPL_HEAD_TL$WO_TMPL_ID              := WO_TMPL_HEAD_TL_cols('WO_TMPL_ID');
   WO_TMPL_HEAD_TL$WO_TMPL_DESC            := WO_TMPL_HEAD_TL_cols('WO_TMPL_DESC');   
   
   WO_TMPL_DETAIL_cols                  := s9t_pkg.get_col_names(I_file_id,WO_TMPL_DTL_sheet );
   WO_TMPL_DTL$Action                   := WO_TMPL_DETAIL_cols('ACTION');
   WO_TMPL_DTL$WO_TMPL_ID               := WO_TMPL_DETAIL_cols('WO_TMPL_ID');
   WO_TMPL_DTL$ACTIVITY_CODE            := WO_TMPL_DETAIL_cols('ACTIVITY_CODE');
   WO_TMPL_DTL$UNIT_COST                := WO_TMPL_DETAIL_cols('UNIT_COST');
   WO_TMPL_DTL$COMMENTS                 := WO_TMPL_DETAIL_cols('COMMENTS');
   WO_TMPL_HEAD_cols                    :=s9t_pkg.get_coL_names(I_file_id,WO_TMPL_HEAD_sheet);
   WO_TMPL_HEAD$Action                  := WO_TMPL_HEAD_cols('ACTION');
   WO_TMPL_HEAD$WO_TMPL_ID              := WO_TMPL_HEAD_cols('WO_TMPL_ID');
   WO_TMPL_HEAD$WO_TMPL_DESC            := WO_TMPL_HEAD_cols('WO_TMPL_DESC');
   

END POPULATE_NAMES;
---------------------------------------------------------------------------
PROCEDURE POPULATE_WO_ACTIVITY( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE
        (select ss.s9t_rows
           from s9t_folder sf,
                TABLE(sf.s9t_file_obj.sheets) ss
          where sf.file_id  = I_file_id
            and ss.sheet_name = WO_ACTIVITY_sheet )
         select s9t_row(s9t_cells(CORESVC_WORK_ORDER.action_mod,
                                  activity_code,
                                  activity_desc,
                                  unit_cost,
                                  cost_type))
           from wo_activity ;
END POPULATE_WO_ACTIVITY;
---------------------------------------------------------------------------
PROCEDURE POPULATE_WO_ACTIVITY_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE
        (select ss.s9t_rows
           from s9t_folder sf,
                TABLE(sf.s9t_file_obj.sheets) ss
          where sf.file_id  = I_file_id
            and ss.sheet_name = WO_ACTIVITY_TL_SHEET )
         select s9t_row(s9t_cells(CORESVC_WORK_ORDER.action_mod,
                                  wt.lang,
                                  wa.activity_code,
                                  wt.activity_desc))
           from wo_activity_tl wt,
                wo_activity wa
          where wt.activity_id = wa.activity_id;
END POPULATE_WO_ACTIVITY_TL;
----------------------------------------------------------------------------
PROCEDURE POPULATE_WO_TMPL_HEAD( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = WO_TMPL_HEAD_sheet )
                 select s9t_row(s9t_cells( CORESVC_WORK_ORDER.action_mod,
                                           wo_tmpl_id,
                                           wo_tmpl_desc))
                   from wo_tmpl_head ;
END POPULATE_WO_TMPl_HEAD;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_WO_TMPL_HEAD_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = WO_TMPL_HEAD_TL_SHEET )
                 select s9t_row(s9t_cells( CORESVC_WORK_ORDER.action_mod,
                                           lang,
                                           wo_tmpl_id,
                                           wo_tmpl_desc))
                   from wo_tmpl_head_tl ;
END POPULATE_WO_TMPL_HEAD_TL;
------------------------------------------------------------------------------
PROCEDURE POPULATE_WO_TMPL_DETAIL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = WO_TMPL_DTL_sheet )
                 select s9t_row(s9t_cells( CORESVC_WORK_ORDER.action_mod,
                                           wt.wo_tmpl_id,
                                           wa.activity_code,
                                           wt.unit_cost,
                                           wt.comments))
                   from wo_tmpl_detail wt,wo_activity wa
                   where wt.activity_id=wa.activity_id(+);
END POPULATE_WO_TMPL_DETAIL;
----------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file S9T_FILE;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW S9T_FILE();
   O_file_id           := S9T_FOLDER_SEQ.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(WO_ACTIVITY_sheet);
   L_file.sheets(L_file.get_sheet_index(WO_ACTIVITY_sheet)).column_headers := s9t_cells('ACTION',
                                                                                        'ACTIVITY_CODE',
                                                                                        'ACTIVITY_DESC',
                                                                                        'UNIT_COST',
                                                                                        'COST_TYPE');

   L_file.add_sheet(WO_ACTIVITY_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(WO_ACTIVITY_TL_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                           'LANG',
                                                                                           'ACTIVITY_CODE',
                                                                                           'ACTIVITY_DESC');
   L_file.add_sheet(WO_TMPL_HEAD_sheet);
   L_file.sheets(L_file.get_sheet_index(WO_TMPL_HEAD_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                          'WO_TMPL_ID',
                                                                                          'WO_TMPL_DESC');

   L_file.add_sheet(WO_TMPL_HEAD_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(WO_TMPL_HEAD_TL_SHEET)).column_headers := s9t_cells( 'ACTION',
                                                                                             'LANG',
                                                                                             'WO_TMPL_ID',
                                                                                             'WO_TMPL_DESC');                                                                                              
   L_file.add_sheet(WO_TMPL_DTL_sheet);
   L_file.sheets(L_file.get_sheet_index(WO_TMPL_DTL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                         'WO_TMPL_ID',
                                                                                         'ACTIVITY_CODE',
                                                                                         'UNIT_COST',
                                                                                         'COMMENTS');
                                                                                             
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
---------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_WORK_ORDER.CREATE_S9T';
   L_file S9T_FILE;
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                                           template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_WO_ACTIVITY(O_file_id);
      POPULATE_WO_ACTIVITY_TL(O_file_id);
      POPULATE_WO_TMPL_HEAD(O_file_id);
      POPULATE_WO_TMPL_HEAD_TL(O_file_id);
      POPULATE_WO_TMPL_DETAIL(O_file_id);
      Commit;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;

----------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_WO_ACTIVITY( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id IN   SVC_WO_ACTIVITY.PROCESS_ID%TYPE) IS

   TYPE svc_WO_ACTIVITY_coL_typ IS TABLE OF SVC_WO_ACTIVITY%ROWTYPE;
   L_temp_rec                               SVC_WO_ACTIVITY%ROWTYPE;
   svc_WO_ACTIVITY_col                      svc_WO_ACTIVITY_coL_typ :=NEW svc_WO_ACTIVITY_coL_typ();
   L_process_id                             SVC_WO_ACTIVITY.PROCESS_ID%TYPE;
   L_error                                  BOOLEAN                 :=FALSE;
   L_default_rec                            SVC_WO_ACTIVITY%ROWTYPE;

   cursor C_MANDATORY_IND is
      select COST_TYPE_mi,
             UNIT_COST_mi,
             ACTIVITY_DESC_mi,
             ACTIVITY_CODE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key    = CORESVC_WORK_ORDER.template_key
                 AND wksht_key       = 'WO_ACTIVITY'
              )PIVOT (MAX(mandatory) AS mi
                      FOR (column_key) IN ('cost_type'     as cost_type,
                                           'unit_cost'     as unit_cost,
                                           'activity_desc' as activity_desc,
                                           'activity_code' as activity_code,
                                           null            as dummy));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dmL_errors, -24381);
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_WO_ACTIVITY';
   L_pk_columns    VARCHAR2(255)  := 'Activity Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select COST_TYPE_dv,
                      UNIT_COST_dv,
                      ACTIVITY_DESC_dv,
                      ACTIVITY_CODE_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key     = CORESVC_WORK_ORDER.template_key
                          AND wksht_key        = 'WO_ACTIVITY'
                       )PIVOT (MAX(default_value) AS dv
                               FOR (column_key) IN ('cost_type'     as cost_type,
                                                    'unit_cost'     as unit_cost,
                                                    'activity_desc' as activity_desc,
                                                    'activity_code' as activity_code,
                                                     null           as dummy)))LOOP

      BEGIN
         L_default_rec.COST_TYPE := rec.COST_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'WO_ACTIVITY',
                            NULL,
                            'COST_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.UNIT_COST := rec.UNIT_COST_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'WO_ACTIVITY',
                            NULL,
                            'UNIT_COST',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.ACTIVITY_DESC := rec.ACTIVITY_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'WO_ACTIVITY',
                            NULL,
                            'ACTIVITY_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.ACTIVITY_CODE := rec.ACTIVITY_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'WO_ACTIVITY',
                            NULL,
                            'ACTIVITY_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_MI_REC;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(WO_ACTIVITY$Action)               as action,
                      r.get_cell(WO_ACTIVITY$COST_TYPE)            as cost_type,
                      r.get_cell(WO_ACTIVITY$UNIT_COST)            as unit_cost,
                      r.get_cell(WO_ACTIVITY$ACTIVITY_DESC)        as activity_desc,
                      UPPER(r.get_cell(WO_ACTIVITY$ACTIVITY_CODE)) as activity_code,
                      r.get_row_seq()                       as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(WO_ACTIVITY_sheet)) LOOP

     L_temp_rec                   := NULL;
     L_temp_rec.process_id        := I_process_id;
     L_temp_rec.chunk_id          := 1;
     L_temp_rec.row_seq           := rec.row_seq;
     L_temp_rec.process$status    := 'N';
     L_temp_rec.create_id         := GET_USER;
     L_temp_rec.last_upd_id       := GET_USER;
     L_temp_rec.create_datetime   := SYSDATE;
     L_temp_rec.last_upd_datetime := SYSDATE;
     L_error := FALSE;
     BEGIN
       L_temp_rec.Action := rec.Action;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR(I_file_id,
                           WO_ACTIVITY_sheet,
                           rec.row_seq,
                           action_column,
                           SQLCODE,
                           SQLERRM);
           L_error := TRUE;
    END;

    BEGIN
       L_temp_rec.COST_TYPE := rec.COST_TYPE;
    EXCEPTION
       when OTHERS then
          WRITE_S9T_ERROR(I_file_id,
                          WO_ACTIVITY_sheet,
                          rec.row_seq,
                          'COST_TYPE',
                          SQLCODE,
                          SQLERRM);
          L_error := TRUE;
    END;

    BEGIN
       L_temp_rec.UNIT_COST := rec.UNIT_COST;
    EXCEPTION
       when OTHERS then
          WRITE_S9T_ERROR(I_file_id,
                          WO_ACTIVITY_sheet,
                          rec.row_seq,
                          'UNIT_COST',
                          SQLCODE,
                          SQLERRM);
          L_error := TRUE;
    END;

    BEGIN
       L_temp_rec.ACTIVITY_DESC := rec.ACTIVITY_DESC;
    EXCEPTION
    when OTHERS then
       WRITE_S9T_ERROR(I_file_id,
                       WO_ACTIVITY_sheet,
                       rec.row_seq,
                       'ACTIVITY_DESC',
                       SQLCODE,
                       SQLERRM);
       L_error := TRUE;
    END;

    BEGIN
       L_temp_rec.ACTIVITY_CODE := rec.ACTIVITY_CODE;
    EXCEPTION
       when OTHERS then
          WRITE_S9T_ERROR(I_file_id,
                          WO_ACTIVITY_sheet,
                          rec.row_seq,
                          'ACTIVITY_CODE',
                          SQLCODE,
                          SQLERRM);
          L_error := TRUE;
    END;

    if rec.action = CORESVC_WORK_ORDER.action_new then
       L_temp_rec.COST_TYPE     := NVL( L_temp_rec.COST_TYPE,L_default_rec.COST_TYPE);
       L_temp_rec.UNIT_COST     := NVL( L_temp_rec.UNIT_COST,L_default_rec.UNIT_COST);
       L_temp_rec.ACTIVITY_DESC := NVL( L_temp_rec.ACTIVITY_DESC,L_default_rec.ACTIVITY_DESC);
       L_temp_rec.ACTIVITY_CODE := NVL( L_temp_rec.ACTIVITY_CODE,L_default_rec.ACTIVITY_CODE);
    end if;

    if NOT(L_temp_rec.activity_code is NOT NULL
               and 1 = 1)then
         WRITE_S9T_ERROR( I_file_id,
                          WO_ACTIVITY_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
       L_error := TRUE;
    end if;

    if NOT L_error then
       svc_WO_ACTIVITY_col.EXTEND();
       svc_WO_ACTIVITY_col(svc_WO_ACTIVITY_col.COUNT()):=L_temp_rec;
     end if;

   END LOOP;

   BEGIN
      forall i IN 1..svc_WO_ACTIVITY_col.COUNT SAVE EXCEPTIONS
         merge into SVC_WO_ACTIVITY st
         using (select (case
                           when L_mi_rec.COST_TYPE_mi    = 'N'
                            and svc_WO_ACTIVITY_col(i).action = CORESVC_WORK_ORDER.action_mod
                            and s1.cost_type             is NULL
                           then mt.cost_type
                           else s1.cost_type
                            end) as cost_type,

                       (case
                           when L_mi_rec.UNIT_COST_mi    = 'N'
                            and svc_WO_ACTIVITY_col(i).action = CORESVC_WORK_ORDER.action_mod
                            and s1.unit_cost             is NULL
                           then mt.unit_cost
                           else s1.unit_cost
                           end) as unit_cost,

                       (case
                           when L_mi_rec.ACTIVITY_DESC_mi    = 'N'
                            and svc_WO_ACTIVITY_col(i).action = CORESVC_WORK_ORDER.action_mod
                            and s1.activity_desc         is NULL
                           then mt.activity_desc
                           else s1.activity_desc
                            end) as activity_desc,

                       (case
                           when L_mi_rec.ACTIVITY_CODE_mi    = 'N'
                            and svc_WO_ACTIVITY_col(i).action = CORESVC_WORK_ORDER.action_mod
                            and s1.activity_code         is NULL
                           then mt.activity_code
                           else s1.activity_code
                           end) as activity_code,
                       NULL as dummy
                  from (select svc_WO_ACTIVITY_col(i).cost_type     as cost_type,
                               svc_WO_ACTIVITY_col(i).unit_cost     as unit_cost,
                               svc_WO_ACTIVITY_col(i).activity_desc as activity_desc,
                               svc_WO_ACTIVITY_col(i).activity_code as activity_code,
                               NULL as dummy
                          from dual) s1,
                               WO_ACTIVITY mt
                         where mt.ACTIVITY_CODE (+)     = s1.activity_code
                           and 1 = 1) sq
            on (st.activity_code      = sq.activity_code
                and svc_WO_ACTIVITY_col(i).action in (CORESVC_WORK_ORDER.action_mod,
                                                      CORESVC_WORK_ORDER.action_del))
          when matched then
             update
                SET process_id        = svc_WO_ACTIVITY_col(i).process_id ,
                    chunk_id          = svc_WO_ACTIVITY_col(i).chunk_id ,
                    row_seq           = svc_WO_ACTIVITY_col(i).row_seq ,
                    action            = svc_WO_ACTIVITY_col(i).action,
                    process$status    = svc_WO_ACTIVITY_col(i).process$status ,
                    cost_type         = sq.cost_type ,
                    activity_desc     = sq.activity_desc ,
                    unit_cost         = sq.unit_cost ,
                    create_id         = svc_WO_ACTIVITY_col(i).create_id ,
                    create_datetime   = svc_WO_ACTIVITY_col(i).create_datetime ,
                    last_upd_id       = svc_WO_ACTIVITY_col(i).last_upd_id ,
                    last_upd_datetime = svc_WO_ACTIVITY_col(i).last_upd_datetime
          when NOT matched then
             insert (process_id ,
                     chunk_id ,
                     row_seq ,
                     action ,
                     process$status ,
                     cost_type ,
                     unit_cost ,
                     activity_desc ,
                     activity_code ,
                     create_id ,
                     create_datetime ,
                     last_upd_id ,
                     last_upd_datetime) values(svc_WO_ACTIVITY_col(i).process_id ,
                                               svc_WO_ACTIVITY_col(i).chunk_id ,
                                               svc_WO_ACTIVITY_col(i).row_seq ,
                                               svc_WO_ACTIVITY_col(i).action ,
                                               svc_WO_ACTIVITY_col(i).process$status ,
                                               sq.cost_type ,
                                               sq.unit_cost ,
                                               sq.activity_desc ,
                                               sq.activity_code ,
                                               svc_WO_ACTIVITY_col(i).create_id ,
                                               svc_WO_ACTIVITY_col(i).create_datetime ,
                                               svc_WO_ACTIVITY_col(i).last_upd_id ,
                                               svc_WO_ACTIVITY_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            WO_ACTIVITY_sheet,
                            svc_WO_ACTIVITY_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_WO_ACTIVITY;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_WO_ACT_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                I_process_id   IN   SVC_WO_ACTIVITY_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_WO_ACTIVITY_TL_COL_TYP IS TABLE OF SVC_WO_ACTIVITY_TL%ROWTYPE;
   L_temp_rec               SVC_WO_ACTIVITY_TL%ROWTYPE;
   svc_wo_activity_tl_col   SVC_WO_ACTIVITY_TL_COL_TYP := NEW SVC_WO_ACTIVITY_TL_COL_TYP();
   L_process_id             SVC_WO_ACTIVITY_TL.PROCESS_ID%TYPE;
   L_error                  BOOLEAN := FALSE;
   L_default_rec            SVC_WO_ACTIVITY_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select activity_desc_mi,
             activity_code_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'WO_ACTIVITY_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('ACTIVITY_DESC' as activity_desc,
                                       'ACTIVITY_CODE' as activity_code,
                                       'LANG'          as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_WO_ACTIVITY_TL';
   L_pk_columns    VARCHAR2(255)  := 'Activity Code, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select activity_desc_dv,
                      activity_code_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'WO_ACTIVITY_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('ACTIVITY_DESC' as activity_desc,
                                                'ACTIVITY_CODE' as activity_code,
                                                'LANG'          as lang)))
   LOOP
      BEGIN
         L_default_rec.activity_desc := rec.activity_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_ACTIVITY_TL_SHEET ,
                            NULL,
                            'ACTIVITY_DESC' ,
                            NULL,
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.activity_code := rec.activity_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           WO_ACTIVITY_TL_SHEET ,
                            NULL,
                           'ACTIVITY_CODE' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_ACTIVITY_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(wo_activity_tl$action))        as action,
                      r.get_cell(wo_activity_tl$activity_desc)        as activity_desc,
                      UPPER(r.get_cell(wo_activity_tl$activity_code)) as activity_code,
                      r.get_cell(wo_activity_tl$lang)                 as lang,
                      r.get_row_seq()                                 as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(WO_ACTIVITY_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_ACTIVITY_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.activity_desc := rec.activity_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_ACTIVITY_TL_SHEET,
                            rec.row_seq,
                            'ACTIVITY_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.activity_code := rec.activity_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_ACTIVITY_TL_SHEET,
                            rec.row_seq,
                            'ACTIVITY_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_ACTIVITY_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_WORK_ORDER.action_new then
         L_temp_rec.activity_desc := NVL( L_temp_rec.activity_desc,L_default_rec.activity_desc);
         L_temp_rec.activity_code := NVL( L_temp_rec.activity_code,L_default_rec.activity_code);
         L_temp_rec.lang          := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.activity_code is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         WO_ACTIVITY_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_wo_activity_tl_col.extend();
         svc_wo_activity_tl_col(svc_wo_activity_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_wo_activity_tl_col.COUNT SAVE EXCEPTIONS
      merge into svc_wo_activity_tl st
      using(select
                  (case
                   when l_mi_rec.activity_desc_mi = 'N'
                    and svc_wo_activity_tl_col(i).action = CORESVC_WORK_ORDER.action_mod
                    and s1.activity_desc IS NULL then
                        mt.activity_desc
                   else s1.activity_desc
                   end) as activity_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_wo_activity_tl_col(i).action = CORESVC_WORK_ORDER.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.activity_code_mi = 'N'
                    and svc_wo_activity_tl_col(i).action = CORESVC_WORK_ORDER.action_mod
                    and s1.activity_code IS NULL then
                        mt.activity_code
                   else s1.activity_code
                   end) as activity_code
              from (select svc_wo_activity_tl_col(i).activity_desc  as activity_desc,
                           svc_wo_activity_tl_col(i).activity_code  as activity_code,
                           svc_wo_activity_tl_col(i).lang           as lang
                      from dual) s1,
                   (select wt.lang,
                           wa.activity_code,
                           wt.activity_desc
                      from wo_activity_tl wt,
                           wo_activity wa
                     where wt.activity_id = wa.activity_id) mt
             where mt.activity_code (+) = s1.activity_code
               and mt.lang (+) = s1.lang) sq
                on (st.activity_code = sq.activity_code and
                    st.lang = sq.lang and
                    svc_wo_activity_tl_col(i).ACTION IN (CORESVC_WORK_ORDER.action_mod,CORESVC_WORK_ORDER.action_del))
      when matched then
      update
         set process_id        = svc_wo_activity_tl_col(i).process_id ,
             chunk_id          = svc_wo_activity_tl_col(i).chunk_id ,
             row_seq           = svc_wo_activity_tl_col(i).row_seq ,
             action            = svc_wo_activity_tl_col(i).action ,
             process$status    = svc_wo_activity_tl_col(i).process$status ,
             activity_desc = sq.activity_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             activity_desc ,
             activity_code ,
             lang)
      values(svc_wo_activity_tl_col(i).process_id ,
             svc_wo_activity_tl_col(i).chunk_id ,
             svc_wo_activity_tl_col(i).row_seq ,
             svc_wo_activity_tl_col(i).action ,
             svc_wo_activity_tl_col(i).process$status ,
             sq.activity_desc ,
             sq.activity_code ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            WO_ACTIVITY_TL_SHEET,
                            svc_wo_activity_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_WO_ACT_TL;
---------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_WO_TMPL_HEAD( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id IN   SVC_WO_TMPL_HEAD.PROCESS_ID%TYPE) IS
   TYPE svc_WO_TMPL_HEAD_col_typ IS TABLE OF SVC_WO_TMPL_HEAD%ROWTYPE;
   L_temp_rec                                SVC_WO_TMPL_HEAD%ROWTYPE;
   svc_WO_TMPL_HEAD_col                      svc_WO_TMPL_HEAD_coL_typ :=NEW svc_WO_TMPL_HEAD_col_typ();
   L_process_id                              SVC_WO_TMPL_HEAD.PROCESS_ID%TYPE;
   L_default_rec                             SVC_WO_TMPL_HEAD%ROWTYPE;

   cursor C_MANDATORY_IND is
      select WO_TMPL_DESC_mi,
             WO_TMPL_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                        = CORESVC_WORK_ORDER.template_key
                 and wksht_key                           = 'WO_TMPL_HEAD'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('wo_tmpl_desc' as wo_tmpl_desc,
                                            'wo_tmpl_id' as wo_tmpl_id,
                                            NULL as dummy));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_WO_TMPL_HEAD';
   L_pk_columns    VARCHAR2(255)  := 'Work Order Template';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select WO_TMPL_DESC_dv,
                      WO_TMPL_ID_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                 = CORESVC_WORK_ORDER.template_key
                          and wksht_key                    = 'WO_TMPL_HEAD'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ('wo_tmpl_desc' as wo_tmpl_desc,
                                                     'wo_tmpl_id' as wo_tmpl_id,
                                                     NULL as dummy)))
   LOOP
   BEGIN
      L_default_rec.WO_TMPL_DESC := rec.WO_TMPL_DESC_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'WO_TMPL_HEAD',
                         NULL,
                         'WO_TMPL_DESC',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.WO_TMPL_ID := rec.WO_TMPL_ID_dv;

   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'WO_TMPL_HEAD',
                         NULL,
                         'WO_TMPL_ID',
                         NULL,
                         'INV_DEFAULT');
   END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(WO_TMPL_HEAD$Action)       as action,
                      r.get_cell(WO_TMPL_HEAD$WO_TMPL_DESC) as wo_tmpl_desc,
                      r.get_cell(WO_TMPL_HEAD$WO_TMPL_ID)   as wo_tmpl_id,
                      r.get_row_seq()                       as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id    = I_file_id
                  and ss.sheet_name = sheet_name_trans(WO_TMPL_HEAD_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      LP_error := FALSE;

    BEGIN
      L_temp_rec.action := rec.action;
    EXCEPTION
    when OTHERS then
       WRITE_S9T_ERROR(I_file_id,
                       WO_TMPL_HEAD_sheet,
                       rec.row_seq,
                       action_column,
                       SQLCODE,
                       SQLERRM);
       LP_error := TRUE;
    END;

    BEGIN
      L_temp_rec.wo_tmpl_desc := rec.wo_tmpl_desc;
    EXCEPTION
       when OTHERS then
          WRITE_S9T_ERROR(I_file_id,
                          WO_TMPL_HEAD_sheet,
                          rec.row_seq,
                          'WO_TMPL_DESC',
                          SQLCODE,
                          SQLERRM);
          LP_error := TRUE;
    END;

    BEGIN
      L_temp_rec.WO_TMPL_ID := rec.WO_TMPL_ID;
    EXCEPTION
       when OTHERS then
          WRITE_S9T_ERROR(I_file_id,
                          WO_TMPL_HEAD_sheet,
                          rec.row_seq,
                          'WO_TMPL_ID',
                          SQLCODE,
                          SQLERRM);
          LP_error := TRUE;
    END;

    if rec.action = CORESVC_WORK_ORDER.action_new then
       L_temp_rec.WO_TMPL_DESC := NVL( L_temp_rec.WO_TMPL_DESC,
                                       L_default_rec.WO_TMPL_DESC);
       L_temp_rec.WO_TMPL_ID := NVL( L_temp_rec.WO_TMPL_ID,
                                     L_default_rec.WO_TMPL_ID);
    end if;
    if NOT (L_temp_rec.wo_tmpl_id is NOT NULL
            and 1 = 1) then
         WRITE_S9T_ERROR( I_file_id,
                          WO_TMPL_HEAD_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
       LP_error := TRUE;
    end if;
    if NOT LP_error then
       svc_WO_TMPL_HEAD_col.EXTEND();
       svc_WO_TMPL_HEAD_col(svc_WO_TMPL_HEAD_col.COUNT()):=L_temp_rec;
    end if;
    L_temp_rec := NULL;
   END LOOP;

   BEGIN
      forall i IN 1..svc_WO_TMPL_HEAD_col.COUNT SAVE EXCEPTIONS
       merge into SVC_WO_TMPL_HEAD st
       using (select (case
                         when L_mi_rec.WO_TMPL_DESC_mi    = 'N'
                          and svc_WO_TMPL_HEAD_col(i).action = CORESVC_WORK_ORDER.action_mod
                          and s1.wo_tmpl_desc             is NULL
                         then mt.wo_tmpl_desc
                         else s1.wo_tmpl_desc
                         end) as wo_tmpl_desc,

                     (case
                         when L_mi_rec.WO_TMPL_ID_mi    = 'N'
                          and svc_WO_TMPL_HEAD_col(i).action = CORESVC_WORK_ORDER.action_mod
                          and s1.wo_tmpl_id             is NULL
                         then mt.wo_tmpl_id
                         else s1.wo_tmpl_id
                         end) as wo_tmpl_id,
                     NULL as dummy
                from (select svc_WO_TMPL_HEAD_col(i).wo_tmpl_desc as wo_tmpl_desc,
                             svc_WO_TMPL_HEAD_col(i).wo_tmpl_id as wo_tmpl_id,
                             NULL as dummy
                        from dual) s1,
                             WO_TMPL_HEAD mt
                       where mt.WO_TMPL_ID (+)     = s1.WO_TMPL_ID
                        and 1 = 1) sq
          on (st.wo_tmpl_id      = sq.wo_tmpl_id
              and svc_WO_TMPL_HEAD_col(i).action in (CORESVC_WORK_ORDER.action_mod,
                                                     CORESVC_WORK_ORDER.action_del))
        when matched then
           update
              set process_id        = svc_WO_TMPL_HEAD_col(i).process_id ,
                  chunk_id          = svc_WO_TMPL_HEAD_col(i).chunk_id ,
                  row_seq           = svc_WO_TMPL_HEAD_col(i).row_seq ,
                  action            = svc_WO_TMPL_HEAD_col(i).action,
                  process$status    = svc_WO_TMPL_HEAD_col(i).process$status ,
                  wo_tmpl_desc      = sq.wo_tmpl_desc ,
                  create_id         = svc_WO_TMPL_HEAD_col(i).create_id ,
                  create_datetime   = svc_WO_TMPL_HEAD_col(i).create_datetime ,
                  last_upd_id       = svc_WO_TMPL_HEAD_col(i).last_upd_id ,
                  last_upd_datetime = svc_WO_TMPL_HEAD_col(i).last_upd_datetime
        when NOT matched then
           insert(process_id ,
                  chunk_id ,
                  row_seq ,
                  action ,
                  process$status ,
                  wo_tmpl_desc ,
                  wo_tmpl_id ,
                  create_id ,
                  create_datetime ,
                  last_upd_id ,
                  last_upd_datetime) values(svc_WO_TMPL_HEAD_col(i).PROCESS_ID ,
                                            svc_WO_TMPL_HEAD_col(i).CHUNK_ID ,
                                            svc_WO_TMPL_HEAD_col(i).ROW_SEQ ,
                                            svc_WO_TMPL_HEAD_col(i).ACTION ,
                                            svc_WO_TMPL_HEAD_col(i).PROCESS$STATUS ,
                                            sq.WO_TMPL_DESC ,
                                            sq.WO_TMPL_ID ,
                                            svc_WO_TMPL_HEAD_col(i).CREATE_ID ,
                                            svc_WO_TMPL_HEAD_col(i).CREATE_DATETIME ,
                                            svc_WO_TMPL_HEAD_col(i).LAST_UPD_ID ,
                                            svc_WO_TMPL_HEAD_col(i).LAST_UPD_DATETIME);
   EXCEPTION
      when DML_ERRORS then
       FOR i IN 1..sql%bulk_exceptions.COUNT
       LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            WO_TMPL_HEAD_sheet,
                            svc_WO_TMPL_HEAD_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
       END LOOP;
   END;
END PROCESS_S9T_WO_TMPL_HEAD;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_WOTHEAD_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id   IN   SVC_WO_TMPL_HEAD_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_WO_TMPL_HEAD_TL_COL_TYP IS TABLE OF SVC_WO_TMPL_HEAD_TL%ROWTYPE;
   L_temp_rec                SVC_WO_TMPL_HEAD_TL%ROWTYPE;
   SVC_WO_TMPL_HEAD_TL_COL   SVC_WO_TMPL_HEAD_TL_COL_TYP := NEW SVC_WO_TMPL_HEAD_TL_COL_TYP();
   L_process_id              SVC_WO_TMPL_HEAD_TL.PROCESS_ID%TYPE;
   L_error                   BOOLEAN := FALSE;
   L_default_rec             SVC_WO_TMPL_HEAD_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select wo_tmpl_desc_mi,
             wo_tmpl_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'WO_TMPL_HEAD_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('WO_TMPL_DESC' as wo_tmpl_desc,
                                       'WO_TMPL_ID'   as wo_tmpl_id,
                                       'LANG'         as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_WO_TMPL_HEAD_TL';
   L_pk_columns    VARCHAR2(255)  := 'Work Order Template ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select wo_tmpl_desc_dv,
                      wo_tmpl_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'WO_TMPL_HEAD_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('WO_TMPL_DESC' as wo_tmpl_desc,
                                                'WO_TMPL_ID'   as wo_tmpl_id,
                                                'LANG'         as lang)))
   LOOP
      BEGIN
         L_default_rec.wo_tmpl_desc := rec.wo_tmpl_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_HEAD_TL_SHEET ,
                            NULL,
                            'WO_TMPL_DESC' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.wo_tmpl_id := rec.wo_tmpl_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_HEAD_TL_SHEET ,
                            NULL,
                            'WO_TMPL_ID' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_HEAD_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   for rec in (select UPPER(r.get_cell(wo_tmpl_head_tl$action))     as action,
                      r.get_cell(wo_tmpl_head_tl$wo_tmpl_desc)      as wo_tmpl_desc,
                      r.get_cell(wo_tmpl_head_tl$wo_tmpl_id)        as wo_tmpl_id,
                      r.get_cell(wo_tmpl_head_tl$lang)              as lang,
                      r.get_row_seq()                               as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(WO_TMPL_HEAD_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            wo_tmpl_head_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wo_tmpl_desc := rec.wo_tmpl_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_HEAD_TL_SHEET,
                            rec.row_seq,
                            'WO_TMPL_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.wo_tmpl_id := rec.wo_tmpl_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_HEAD_TL_SHEET,
                            rec.row_seq,
                            'WO_TMPL_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_HEAD_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_WORK_ORDER.action_new then
         L_temp_rec.wo_tmpl_desc := NVL( L_temp_rec.wo_tmpl_desc,L_default_rec.wo_tmpl_desc);
         L_temp_rec.wo_tmpl_id        := NVL( L_temp_rec.wo_tmpl_id,L_default_rec.wo_tmpl_id);
         L_temp_rec.lang              := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.wo_tmpl_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         WO_TMPL_HEAD_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_WO_TMPL_HEAD_TL_col.extend();
         SVC_WO_TMPL_HEAD_TL_col(SVC_WO_TMPL_HEAD_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_WO_TMPL_HEAD_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_WO_TMPL_HEAD_TL st
      using(select
                  (case
                   when l_mi_rec.wo_tmpl_desc_mi = 'N'
                    and SVC_WO_TMPL_HEAD_TL_col(i).action = CORESVC_WORK_ORDER.action_mod
                    and s1.wo_tmpl_desc IS NULL then
                        mt.wo_tmpl_desc
                   else s1.wo_tmpl_desc
                   end) as wo_tmpl_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and SVC_WO_TMPL_HEAD_TL_col(i).action = CORESVC_WORK_ORDER.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.wo_tmpl_id_mi = 'N'
                    and SVC_WO_TMPL_HEAD_TL_col(i).action = CORESVC_WORK_ORDER.action_mod
                    and s1.wo_tmpl_id IS NULL then
                        mt.wo_tmpl_id
                   else s1.wo_tmpl_id
                   end) as wo_tmpl_id
              from (select SVC_WO_TMPL_HEAD_TL_col(i).wo_tmpl_desc as wo_tmpl_desc,
                           SVC_WO_TMPL_HEAD_TL_col(i).wo_tmpl_id        as wo_tmpl_id,
                           SVC_WO_TMPL_HEAD_TL_col(i).lang              as lang
                      from dual) s1,
                   wo_tmpl_head_TL mt
             where mt.wo_tmpl_id (+) = s1.wo_tmpl_id
               and mt.lang (+)       = s1.lang) sq
                on (st.wo_tmpl_id = sq.wo_tmpl_id and
                    st.lang = sq.lang and
                    SVC_WO_TMPL_HEAD_TL_col(i).ACTION IN (CORESVC_WORK_ORDER.action_mod,CORESVC_WORK_ORDER.action_del))
      when matched then
      update
         set process_id        = svc_wo_tmpl_head_tl_col(i).process_id ,
             chunk_id          = svc_wo_tmpl_head_tl_col(i).chunk_id ,
             row_seq           = svc_wo_tmpl_head_tl_col(i).row_seq ,
             action            = svc_wo_tmpl_head_tl_col(i).action ,
             process$status    = svc_wo_tmpl_head_tl_col(i).process$status ,
             wo_tmpl_desc = sq.wo_tmpl_desc
      when not matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             wo_tmpl_desc ,
             wo_tmpl_id ,
             lang)
      values(svc_wo_tmpl_head_tl_col(i).process_id ,
             svc_wo_tmpl_head_tl_col(i).chunk_id ,
             svc_wo_tmpl_head_tl_col(i).row_seq ,
             svc_wo_tmpl_head_tl_col(i).action ,
             svc_wo_tmpl_head_tl_col(i).process$status ,
             sq.wo_tmpl_desc ,
             sq.wo_tmpl_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            WO_TMPL_HEAD_TL_SHEET,
                            SVC_WO_TMPL_HEAD_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_WOTHEAD_TL;
--------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_WO_TMPL_DETAIL( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                      I_process_id IN   SVC_WO_TMPL_DETAIL.PROCESS_ID%TYPE) IS

   TYPE svc_WO_TMPL_DETAIL_col_typ IS TABLE OF SVC_WO_TMPL_DETAIL%ROWTYPE;
   L_temp_rec                                  SVC_WO_TMPL_DETAIL%ROWTYPE;
   svc_WO_TMPL_DETAIL_col                      svc_WO_TMPL_DETAIL_col_typ :=NEW svc_WO_TMPL_DETAIL_col_typ();
   L_process_id                                SVC_WO_TMPL_DETAIL.PROCESS_ID%TYPE;
   L_default_rec                               SVC_WO_TMPL_DETAIL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select COMMENTS_mi,
             UNIT_COST_mi,
             WO_TMPL_ID_mi,
             WO_TMPL_DETAIL_ID_mi,
             ACTIVITY_CODE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = CORESVC_WORK_ORDER.template_key
                 and wksht_key                                 = 'WO_TMPL_DETAIL'
              )PIVOT (MAX(mandatory) AS mi
                      FOR (column_key) IN ('comments' as comments,
                                           'unit_cost' as unit_cost,
                                           'wo_tmpL_id' as wo_tmpl_id,
                                           'wo_tmpl_detail_id' as wo_tmpl_detail_id,
                                           'activity_code' as activity_code,
                                            NULL as dummy));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_WO_TMPL_DETAIL';
   L_pk_columns    VARCHAR2(255)  := 'Work Order Template,Activity';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select COMMENTS_dv,
                      UNIT_COST_dv,
                      WO_TMPL_ID_dv,
                      WO_TMPL_DETAIL_ID_dv,
                      ACTIVITY_CODE_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = CORESVC_WORK_ORDER.template_key
                          and wksht_key                                     = 'WO_TMPL_DETAIL'
                       )PIVOT (MAX(default_value) AS dv
                               FOR (column_key) IN ( 'comments' as comments,
                                                     'unit_cost' as unit_cost,
                                                     'wo_tmpl_id' as wo_tmpl_id,
                                                     'wo_tmpl_detail_id' as wo_tmpl_detail_id,
                                                     'activity_code' as activity_code,
                                                     NULL as dummy)))
   LOOP
   BEGIN
      l_default_rec.COMMENTS := rec.COMMENTS_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'WO_TMPL_DETAIL',
                         NULL,
                         'COMMENTS',
                         NULL,
                        'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.UNIT_COST := rec.UNIT_COST_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'WO_TMPL_DETAIL',
                         NULL,
                         'UNIT_COST',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.WO_TMPL_ID := rec.WO_TMPL_ID_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'WO_TMPL_DETAIL',
                         NULL,
                         'WO_TMPL_ID',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.ACTIVITY_CODE := rec.ACTIVITY_CODE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'WO_TMPL_DETAIL',
                         NULL,
                         'ACTIVITY_CODE',
                         NULL,
                        'INV_DEFAULT');
   END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(WO_TMPL_DTL$Action)               as action,
                      r.get_cell(WO_TMPL_DTL$COMMENTS)             as comments,
                      r.get_cell(WO_TMPL_DTL$UNIT_COST)            as unit_cost,
                      r.get_cell(WO_TMPL_DTL$WO_TMPL_ID)           as wo_tmpl_id,
                      UPPER(r.get_cell(WO_TMPL_DTL$ACTIVITY_CODE)) as activity_code,
                      r.get_row_seq()                        as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(WO_TMPL_DTL_sheet))
    LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      LP_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_DTL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            LP_error := TRUE;
      END;

      BEGIN
         L_temp_rec.comments := rec.comments;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_DTL_sheet,
                            rec.row_seq,
                            'COMMENTS',
                            SQLCODE,
                            SQLERRM);
            LP_error := TRUE;
      END;

      BEGIN
         L_temp_rec.unit_cost := rec.unit_cost;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_DTL_sheet,
                            rec.row_seq,
                            'UNIT_COST',
                            SQLCODE,
                            SQLERRM);
            LP_error := TRUE;
      END;

      BEGIN
         L_temp_rec.wo_tmpl_id := rec.wo_tmpl_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_DTL_sheet,
                            rec.row_seq,
                            'WO_TMPL_ID',
                            SQLCODE,
                            SQLERRM);
            LP_error := TRUE;
      END;

      BEGIN
         L_temp_rec.activity_code := rec.activity_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            WO_TMPL_DTL_sheet,
                            rec.row_seq,
                            'activity_code',
                            SQLCODE,
                            SQLERRM);
            LP_error := TRUE;
      END;

      if rec.action = CORESVC_WORK_ORDER.action_new then
         L_temp_rec.COMMENTS          := NVL( L_temp_rec.COMMENTS,
                                              L_default_rec.COMMENTS);
         L_temp_rec.UNIT_COST         := NVL( L_temp_rec.UNIT_COST,
                                              L_default_rec.UNIT_COST);
         L_temp_rec.WO_TMPL_ID        := NVL( L_temp_rec.WO_TMPL_ID,
                                              L_default_rec.WO_TMPL_ID);
         L_temp_rec.activity_code     := NVL( L_temp_rec.activity_code,
                                              L_default_rec.activity_code);
      end if;

      if (L_temp_rec.activity_code is NULL
          or L_temp_rec.wo_tmpl_id is NULL
          and 1 = 1)then
         WRITE_S9T_ERROR( I_file_id,
                          WO_TMPL_DTL_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         LP_error := TRUE;
      end if;

      if NOT LP_error then
         svc_WO_TMPL_DETAIL_col.EXTEND();
         svc_WO_TMPL_DETAIL_col(svc_WO_TMPL_DETAIL_col.COUNT()):=L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      forall i IN 1..svc_WO_TMPL_DETAIL_col.COUNT SAVE EXCEPTIONS
       merge into SVC_WO_TMPL_DETAIL st
       using (select (case
                         when L_mi_rec.COMMENTS_mi    = 'N'
                          and svc_WO_TMPL_DETAIL_col(i).action = CORESVC_WORK_ORDER.action_mod
                          and s1.comments             is NULL
                         then mt.comments
                         else s1.comments
                         end) as comments,

                     (case
                         when L_mi_rec.UNIT_COST_mi    = 'N'
                          and svc_WO_TMPL_DETAIL_col(i).action = CORESVC_WORK_ORDER.action_mod
                          and s1.unit_cost             is NULL
                         then mt.unit_cost
                         else s1.unit_cost
                         end) as unit_cost,

                     (case
                         when L_mi_rec.WO_TMPL_ID_mi    = 'N'
                          and svc_WO_TMPL_DETAIL_col(i).action = CORESVC_WORK_ORDER.action_mod
                          and s1.wo_tmpl_id             is NULL
                         then mt.wo_tmpl_id
                         else s1.wo_tmpl_id
                         end) as wo_tmpl_id,

                     (case
                         when L_mi_rec.ACTIVITY_CODE_mi    = 'N'
                          AND svc_WO_TMPL_DETAIL_col(i).action = CORESVC_WORK_ORDER.action_mod
                          and s1.activity_code             is NULL
                         then wt.activity_code
                         else s1.activity_code
                         end) as activity_code,
                     NULL as dummy
               from (select svc_WO_TMPL_DETAIL_col(i).comments as comments,
                            svc_WO_TMPL_DETAIL_col(i).unit_cost as unit_cost,
                            svc_WO_TMPL_DETAIL_col(i).wo_tmpl_id as wo_tmpl_id,
                            svc_WO_TMPL_DETAIL_col(i).activity_code as activity_code,
                            NULL as dummy
                      from dual) s1,
                           WO_TMPL_DETAIL mt,
                           WO_ACTIVITY wt
                     where wt.activity_code (+)     = s1.activity_code
                       and mt.wo_tmpl_id (+)     = s1.wo_tmpl_id
                       and mt.activity_id(+)     = wt.activity_id
                       and 1 = 1 ) sq
          on (st.activity_code      = sq.activity_code
              and st.wo_tmpl_id      = sq.wo_tmpl_id
              and svc_WO_TMPL_DETAIL_col(i).action in (CORESVC_WORK_ORDER.action_mod,
                                                       CORESVC_WORK_ORDER.action_del))
        when matched then
           update
              set process_id        = svc_WO_TMPL_DETAIL_col(i).process_id ,
                  chunk_id          = svc_WO_TMPL_DETAIL_col(i).chunk_id ,
                  row_seq           = svc_WO_TMPL_DETAIL_col(i).row_seq ,
                  action            = svc_WO_TMPL_DETAIL_col(i).action,
                  process$status    = svc_WO_TMPL_DETAIL_col(i).process$status ,
                  comments          = sq.comments ,
                  unit_cost         = sq.unit_cost ,
                  create_id         = svc_WO_TMPL_DETAIL_col(i).create_id ,
                  create_datetime   = svc_WO_TMPL_DETAIL_col(i).create_datetime ,
                  last_upd_id       = svc_WO_TMPL_DETAIL_col(i).last_upd_id ,
                  last_upd_datetime = svc_WO_TMPL_DETAIL_col(i).last_upd_datetime
        when NOT matched then
          insert(process_id ,
                 chunk_id ,
                 row_seq ,
                 action ,
                 process$status ,
                 comments ,
                 unit_cost ,
                 wo_tmpl_id ,
                 activity_code ,
                 create_id ,
                 create_datetime ,
                 last_upd_id ,
                 last_upd_datetime) values (svc_WO_TMPL_DETAIL_col(i).process_id ,
                                            svc_WO_TMPL_DETAIL_col(i).chunk_id ,
                                            svc_WO_TMPL_DETAIL_col(i).row_seq ,
                                            svc_WO_TMPL_DETAIL_col(i).action ,
                                            svc_WO_TMPL_DETAIL_col(i).process$status ,
                                            sq.comments ,
                                            sq.unit_cost ,
                                            sq.wo_tmpl_id ,
                                            sq.activity_code ,
                                            svc_WO_TMPL_DETAIL_col(i).create_id ,
                                            svc_WO_TMPL_DETAIL_col(i).create_datetime ,
                                            svc_WO_TMPL_DETAIL_col(i).last_upd_id ,
                                            svc_WO_TMPL_DETAIL_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            WO_TMPL_DTL_sheet,
                            svc_WO_TMPL_DETAIL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_WO_TMPL_DETAIL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count    IN OUT NUMBER,
                               I_file_id        IN      S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id     IN      NUMBER)
   RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_WORK_ORDER.PROCESS_S9T';
   L_file           S9T_FILE;
   L_sheets         S9T_PKG.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR     EXCEPTION;
   PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   commit;
   s9t_pkg.ODS2OBJ(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(l_file);
   if s9t_pkg.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.sheet_trans(l_file.template_key,l_file.user_lang);
      PROCESS_S9T_WO_ACTIVITY(I_file_id,I_process_id);
      PROCESS_S9T_WO_ACT_TL(I_file_id,I_process_id);
      PROCESS_S9T_WO_TMPL_DETAIL(I_file_id,I_process_id);
      PROCESS_S9T_WO_TMPL_HEAD(I_file_id,I_process_id);
      PROCESS_S9T_WOTHEAD_TL(I_file_id,I_process_id);
      
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   commit;
   return TRUE;
EXCEPTION

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION EXEC_WO_ACTIVITY_INS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_wo_activity_temp_rec IN     WO_ACTIVITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_WORK_ORDER.EXEC_WO_ACTIVITY_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WO_ACTIVITY';
BEGIN
   insert into wo_activity
        values I_wo_activity_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WO_ACTIVITY_INS;
-------------------------------------------------------------------------------------
FUNCTION EXEC_WO_ACTIVITY_UPD(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_wo_activity_temp_rec IN     WO_ACTIVITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_WORK_ORDER.EXEC_WO_ACTIVITY_UPD';

   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_WO_ACTIVITY_LOCK is
      select 'X'
        from wo_activity
       where activity_code = I_wo_activity_temp_rec.activity_code
         for update nowait;
BEGIN
   open C_WO_ACTIVITY_LOCK;

   close C_WO_ACTIVITY_LOCK;

   update wo_activity
      set activity_desc = I_wo_activity_temp_rec.activity_desc,
          unit_cost     = I_wo_activity_temp_rec.unit_cost
    where activity_code = I_wo_activity_temp_rec.activity_code;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_WO_ACTIVITY',
                                                                I_wo_activity_temp_rec.activity_code,
                                                                NULL);
      return FALSE;

   when OTHERS then
      if C_WO_ACTIVITY_LOCK%ISOPEN then
         close C_WO_ACTIVITY_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WO_ACTIVITY_UPD;
-----------------------------------------------------------------------------------------
FUNCTION EXEC_WO_ACTIVITY_DEL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_wo_activity_temp_rec IN     WO_ACTIVITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_WORK_ORDER.EXEC_WO_ACTIVITY_DEL';
   L_activity_id           WO_ACTIVITY.ACTIVITY_ID%TYPE;

   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_WO_ACTIVITY_TL_LOCK is
      select 'X'
        from wo_activity_tl
       where activity_id in (select activity_id
                               from wo_activity
                              where activity_code = I_wo_activity_temp_rec.activity_code)
         for update nowait;

   cursor C_WO_ACTIVITY_LOCK is
      select 'X'
        from wo_activity
       where activity_code = I_wo_activity_temp_rec.activity_code
         for update nowait;
   
      
BEGIN
  
   open C_WO_ACTIVITY_TL_LOCK;
   close C_WO_ACTIVITY_TL_LOCK;
   delete
     from wo_activity_tl
    where activity_id in (select activity_id
                            from wo_activity
                           where activity_code = I_wo_activity_temp_rec.activity_code);

   open C_WO_ACTIVITY_LOCK;
   close C_WO_ACTIVITY_LOCK;
   delete
     from wo_activity
    where activity_code = I_wo_activity_temp_rec.activity_code;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_WO_ACTIVITY',
                                                                I_wo_activity_temp_rec.activity_code,
                                                                NULL);
      return FALSE;

   when OTHERS then
      if C_WO_ACTIVITY_TL_LOCK%ISOPEN then
         close C_WO_ACTIVITY_TL_LOCK;
      end if;
      if C_WO_ACTIVITY_LOCK%ISOPEN then
         close C_WO_ACTIVITY_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WO_ACTIVITY_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WO_ACT_TL_INS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_wo_activity_tl_ins_tab    IN       WO_ACTIVITY_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_WORK_ORDER.EXEC_WO_ACT_TL_INS';

BEGIN
   if I_wo_activity_tl_ins_tab is NOT NULL and I_wo_activity_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_wo_activity_tl_ins_tab.COUNT()
         insert into wo_activity_tl(lang,
                                    activity_id,
                                    activity_desc,
                                    create_datetime,
                                    create_id,
                                    last_update_datetime,
                                    last_update_id)
                             values(I_wo_activity_tl_ins_tab(i).lang,
                                    I_wo_activity_tl_ins_tab(i).activity_id,
                                    I_wo_activity_tl_ins_tab(i).activity_desc,
                                    I_wo_activity_tl_ins_tab(i).create_datetime,
                                    I_wo_activity_tl_ins_tab(i).create_id,
                                    I_wo_activity_tl_ins_tab(i).last_update_datetime,
                                    I_wo_activity_tl_ins_tab(i).last_update_id);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_WO_ACT_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WO_ACT_TL_UPD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_wo_act_tl_upd_tab   IN       WO_ACTIVITY_TAB,
                            I_wo_act_tl_upd_rst   IN       ROW_SEQ_TAB,
                            I_process_id          IN       SVC_WO_ACTIVITY_TL.PROCESS_ID%TYPE,
                            I_chunk_id            IN       SVC_WO_ACTIVITY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_WORK_ORDER.EXEC_WO_ACT_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WO_ACTIVITY_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_WO_ACT_TL_UPD(I_wo_activity  WO_ACTIVITY.ACTIVITY_CODE%TYPE,
                               I_lang         WO_ACTIVITY_TL.LANG%TYPE) is
      select 'x'
        from wo_activity_tl
       where activity_id in (select activity_id
                               from wo_activity
                              where activity_code = I_wo_activity)
         and lang = I_lang
         for update nowait;

BEGIN
   if I_wo_act_tl_upd_tab is NOT NULL and I_wo_act_tl_upd_tab.count > 0 then
      for i in I_wo_act_tl_upd_tab.FIRST..I_wo_act_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_wo_act_tl_upd_tab(i).lang);
            L_key_val2 := 'Activity Code: '||to_char(I_wo_act_tl_upd_tab(i).activity_code);
            open C_LOCK_WO_ACT_TL_UPD(I_wo_act_tl_upd_tab(i).activity_code,
                                      I_wo_act_tl_upd_tab(i).lang);
            close C_LOCK_WO_ACT_TL_UPD;
            
            update wo_activity_tl
               set activity_desc = I_wo_act_tl_upd_tab(i).activity_desc,
                   last_update_id = I_wo_act_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_wo_act_tl_upd_tab(i).last_update_datetime
             where lang = I_wo_act_tl_upd_tab(i).lang
               and activity_id in (select activity_id
                                     from wo_activity
                                    where activity_code = I_wo_act_tl_upd_tab(i).activity_code);
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_WO_ACTIVITY_TL',
                           I_wo_act_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_WO_ACT_TL_UPD%ISOPEN then
         close C_LOCK_WO_ACT_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_WO_ACT_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WO_ACT_TL_DEL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_wo_act_tl_del_tab   IN       WO_ACTIVITY_TAB,
                            I_wo_act_tl_del_rst   IN       ROW_SEQ_TAB,
                            I_process_id          IN       SVC_WO_ACTIVITY_TL.PROCESS_ID%TYPE,
                            I_chunk_id            IN       SVC_WO_ACTIVITY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_WORK_ORDER.EXEC_WO_ACT_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WO_ACTIVITY_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_WO_ACT_TL_DEL(I_activity_code  WO_ACTIVITY.ACTIVITY_CODE%TYPE,
                               I_lang           WO_ACTIVITY_TL.LANG%TYPE) is
      select 'x'
        from wo_activity_tl
       where activity_id in (select activity_id
                               from wo_activity
                              where activity_code = I_activity_code)
         and lang = I_lang
         for update nowait;

BEGIN
   if I_wo_act_tl_del_tab is NOT NULL and I_wo_act_tl_del_tab.count > 0 then
      for i in I_wo_act_tl_del_tab.FIRST..I_wo_act_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_wo_act_tl_del_tab(i).lang);
            L_key_val2 := 'Activity Code: '||to_char(I_wo_act_tl_del_tab(i).activity_code);
            open C_LOCK_WO_ACT_TL_DEL(I_wo_act_tl_del_tab(i).activity_code,
                                      I_wo_act_tl_del_tab(i).lang);
            close C_LOCK_WO_ACT_TL_DEL;
            
            delete wo_activity_tl
             where lang = I_wo_act_tl_del_tab(i).lang
               and activity_id in (select activity_id
                                     from wo_activity
                                    where activity_code = I_wo_act_tl_del_tab(i).activity_code);
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_WO_ACTIVITY_TL',
                           I_wo_act_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_WO_ACT_TL_DEL%ISOPEN then
         close C_LOCK_WO_ACT_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_WO_ACT_TL_DEL;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ON_DELETE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error          IN OUT BOOLEAN,
                         I_rec            IN OUT C_SVC_WO_ACTIVITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program               VARCHAR2(64)                      :='CORESVC_WORK_ORDER.CHECK_ON_DELETE';
   L_delete_ok             BOOLEAN;
   L_activity_id           WO_ACTIVITY.ACTIVITY_ID%TYPE      := I_rec.activity_id;
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WO_ACTIVITY';
   L_exists   VARCHAR2(1)   := 'N';
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);

   CURSOR C_LOCK_WO_ACTIVITY is
      select 'x'
        from wo_activity
       where activity_id = L_activity_id
         for update of activity_id nowait;

   CURSOR C_wo_tmpl_detail_del is 
      select wo_tmpl_id,
            action
      from svc_wo_tmpl_detail
    where activity_code = I_rec.activity_code;
    
   CURSOR C_wo_tmpl_head(l_activity_id NUMBER) is 
     select 'Y'
      from svc_wo_tmpl_head
    where wo_tmpl_id = l_activity_id
     and action =action_del;  
     
   CURSOR C_WO_TMPL_DETAIL_LOCK( L_tmpl_id NUMBER) is
      select 'X'
        from wo_tmpl_detail
       where activity_id = I_rec.activity_id
         and wo_tmpl_id = L_tmpl_id
         for update nowait;  
     
   CURSOR C_LOCK_WTHEAD_TL_DEL( L_tmpl_id NUMBER) is 
      select 'x'
        from wo_tmpl_head_tl
       where wo_tmpl_id = L_tmpl_id
         for update nowait;
         
   CURSOR C_WO_TMPL_HEAD_LOCK( L_tmpl_id NUMBER) is 
      select 'X'
        from wo_tmpl_head
       where wo_tmpl_id = L_tmpl_id
         for update nowait;
         
BEGIN
   if I_rec.activity_code is NOT NULL and L_activity_id is NOT NULL then
      savepoint BEGIN_EXIT;
      FOR rec in  C_wo_tmpl_detail_del loop
        if(I_rec.action = action_del) then
           open C_WO_TMPL_DETAIL_LOCK(rec.wo_tmpl_id);
           close C_WO_TMPL_DETAIL_LOCK;
           
           delete from wo_tmpl_detail where 1 = 1 and wo_tmpl_id = rec.wo_tmpl_id;
           
           update svc_wo_tmpl_detail 
              set process$status='P' 
	    where process_id = I_rec.process_id
	      and chunk_id = I_rec.chunk_id 
	      and wo_tmpl_id = rec.wo_tmpl_id;	                
        end if;
        open C_wo_tmpl_head(rec.wo_tmpl_id);
        fetch C_wo_tmpl_head into L_exists;
        close C_wo_tmpl_head;
        
        if(L_exists = 'Y') then
           open C_WO_TMPL_DETAIL_LOCK(rec.wo_tmpl_id);
           close C_WO_TMPL_DETAIL_LOCK;
           delete from wo_tmpl_detail where 1 = 1 and wo_tmpl_id = rec.wo_tmpl_id;
            
            update svc_wo_tmpl_detail
               set process$status='P' 
	     where process_id = I_rec.process_id
	       and chunk_id = I_rec.chunk_id 
	       and wo_tmpl_id = rec.wo_tmpl_id;
           open C_LOCK_WTHEAD_TL_DEL(rec.wo_tmpl_id);
           close C_LOCK_WTHEAD_TL_DEL;
          
            delete from wo_tmpl_head_tl where 1 = 1 and wo_tmpl_id =  rec.wo_tmpl_id;
           
           update svc_wo_tmpl_head_tl 
              set process$status='P' 
	    where process_id = I_rec.process_id
	      and chunk_id = I_rec.chunk_id 
	      and wo_tmpl_id = rec.wo_tmpl_id;
           
           open C_WO_TMPL_HEAD_LOCK(rec.wo_tmpl_id);
           close C_WO_TMPL_HEAD_LOCK;	                 
           delete from wo_tmpl_head   where 1 = 1  and wo_tmpl_id =  rec.wo_tmpl_id;
         
            update svc_wo_tmpl_head
               set process$status='P' 
             where process_id = I_rec.process_id
               and chunk_id = I_rec.chunk_id 
               and wo_tmpl_id = rec.wo_tmpl_id;
         end if;
      end loop;         
      if WO_ACTIVITY_SQL.CHECK_DELETE(O_error_message,
                                      L_delete_ok,
                                      L_activity_id)= FALSE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error :=TRUE;
      end if;
      if L_delete_ok = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'ACTIVITY_CODE',
                     'ACTIVITY_ASSOC_WO');
         O_error :=TRUE;
      end if;
      if WO_ACTIVITY_SQL.DELETE_GL_XREF(O_error_message,
                                        L_activity_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error :=TRUE;
      end if;
      open C_LOCK_WO_ACTIVITY;
      close C_LOCK_WO_ACTIVITY;
   end if;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
        rollback to Begin_Exit;
        WRITE_ERROR(I_rec.process_id,
                    svc_admin_upld_er_seq.NEXTVAL,
                    I_rec.chunk_id,
                    L_table,
                    I_rec.row_seq,
                    'ACTIVITY_CODE',
                    'TABLE_IS_LOCKED');
         O_error :=TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CHECK_ON_DELETE;
------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_WO_ACTIVITY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error IN OUT BOOLEAN,
                                 I_rec IN OUT C_SVC_WO_ACTIVITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      :='CORESVC_WORK_ORDER.PROCESS_VAL_WO_ACTIVITY';
   L_alL_activities   VARCHAR2(2)                       := '-1';
   L_exists           BOOLEAN;
   L_wo_activity_row  WO_ACTIVITY%ROWTYPE;
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WO_ACTIVITY';
   L_check_decimal    WO_ACTIVITY.UNIT_COST%TYPE;
BEGIN
   if I_rec.activity_code is NOT NULL
      and I_rec.activity_code=L_alL_activities then
      WRITE_ERROR(I_rec.process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'ACTIVITY_CODE',
                  'NOT_ALL_ACTIVITIES');
      O_error :=TRUE;
   end if;

   if I_rec.action IN (action_del,action_mod)
      and I_rec.activity_code is NOT NULL
      and WO_ACTIVITY_SQL.VALIDATE_ACTIVITY_CODE(O_error_message,
                                                 L_exists,
                                                 L_wo_activity_row,
                                                 I_rec.activity_code) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'ACTIVITY_CODE',
                  O_error_message);
      O_error :=TRUE;
   end if;

   if I_rec.action IN (action_new,action_mod)
      and I_rec.unit_cost is NOT NULL
      and I_rec.unit_cost < 0 then
      WRITE_ERROR(I_rec.process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'UNIT_COST',
                  'UNIT_COST_NOT_NEG');
      O_error :=TRUE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_VAL_WO_ACTIVITY;
------------------------------------------------------------------------------------------
FUNCTION PROCESS_WO_ACTIVITY( O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id    IN       SVC_WO_ACTIVITY.PROCESS_ID%TYPE,
                              I_chunk_id      IN       SVC_WO_ACTIVITY.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program              VARCHAR2(64)                      :='CORESVC_WORK_ORDER.PROCESS_WO_ACTIVITY';
   L_error                BOOLEAN;
   L_process_error        BOOLEAN                           := FALSE;
   L_wo_activity_temp_rec WO_ACTIVITY%ROWTYPE;
   L_next_wo_activity_seq WO_ACTIVITY.ACTIVITY_ID%TYPE      :=NULL;
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WO_ACTIVITY';
BEGIN
   FOR rec IN c_svc_WO_ACTIVITY(I_process_id,
                                I_chunk_id)
   LOOP
      L_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action = action_new
         and rec.UK_WO_ACTIVITY_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTIVITY_CODE',
                     'DUP_ACTIVITY');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.UK_WO_ACTIVITY_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTIVITY_CODE',
                     'INV_ACTIVITY');
         L_error :=TRUE;
      end if;

      if rec.action IN(action_new,action_mod)
         and rec.activity_desc is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTIVITY_DESC',
                     'ACTIVITY_DESC_REQ');
         L_error :=TRUE;
      end if;

      if rec.action IN(action_new)
         and rec.cost_type is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COST_TYPE',
                     'COST_TYPE_REQ');
         L_error :=TRUE;
      end if;
      if rec.action IN(action_mod)
         and NVL(rec.cost_type,'-1') <> NVL(rec.old_cost_type,'-1') then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COST_TYPE',
                     'CANNOT_UPDATE_REC');
         L_error :=TRUE;
      end if;
      if rec.action IN(action_new)
         and rec.cost_type is NOT NULL
         and NOT (rec.cost_type IN('U','P')) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COST_TYPE',
                     'INV_COST_TYPE');
         L_error :=TRUE;
      end if;
      if PROCESS_VAL_WO_ACTIVITY(O_error_message,
                                 L_error,
                                 rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;
      if rec.action=action_del
         and CHECK_ON_DELETE(O_error_message,
                             L_error,
                             rec)=FALSE then
         L_table:='SVC_WO_ACTIVITY';
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;


      if NOT L_error
         and rec.action=action_new
         and L_next_wo_activity_seq is NULL then
             if WO_ACTIVITY_SQL.NEXT_SEQ_NO(O_error_message,
                                            L_next_wo_activity_seq)=FALSE then
                WRITE_ERROR(I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
                L_error :=TRUE;
             else
                L_wo_activity_temp_rec.activity_id         := L_next_wo_activity_seq;
                L_next_wo_activity_seq:=NULL;
             end if;
      end if;

      if NOT L_error then
         if rec.action IN (action_mod,action_del)then
            L_wo_activity_temp_rec.activity_id             :=rec.activity_id;
         end if;

         L_wo_activity_temp_rec.activity_code              := rec.activity_code;
         L_wo_activity_temp_rec.activity_desc              := rec.activity_desc;
         L_wo_activity_temp_rec.unit_cost                  := rec.unit_cost;
         L_wo_activity_temp_rec.cost_type                  := rec.cost_type;
      end if;

      if rec.action = action_new
         and NOT L_error then
         if EXEC_WO_ACTIVITY_INS( O_error_message,
                                  L_wo_activity_temp_rec)=FALSE then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_process_error :=TRUE;
            L_next_wo_activity_seq:=NULL;
         end if;
      end if;

      if rec.action = action_mod
         and NOT L_error then
         if EXEC_WO_ACTIVITY_UPD( O_error_message,
                                  L_wo_activity_temp_rec)=FALSE then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_process_error :=TRUE;
         end if;
      end if;

      if rec.action = action_del
         and NOT L_error then
         if EXEC_WO_ACTIVITY_DEL( O_error_message,
                                  L_wo_activity_temp_rec)=FALSE then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_process_error :=TRUE;
         end if;
      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_WO_ACTIVITY;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_WO_ACT_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_WO_ACTIVITY_TL.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_WO_ACTIVITY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64) := 'CORESVC_WORK_ORDER.PROCESS_WO_ACT_TL';
   L_error_message              RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WO_ACTIVITY_TL';
   L_base_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WO_ACTIVITY';
   L_table                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WO_ACTIVITY_TL';
   L_error                      BOOLEAN := FALSE;
   L_process_error              BOOLEAN := FALSE;
   L_wo_act_tl_upd_rst          ROW_SEQ_TAB;
   L_wo_act_tl_del_rst          ROW_SEQ_TAB;

   cursor C_SVC_WO_ACTIVITY_TL(I_process_id NUMBER,
                               I_chunk_id NUMBER) is
      select pk_wo_activity_tl.rowid  as pk_wo_activity_tl_rid,
             fk_wo_activity.rowid     as fk_wo_activity_rid,
             fk_lang.rowid            as fk_lang_rid,
             st.lang,
             fk_wo_activity.activity_id,
             st.activity_code,
             st.activity_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_wo_activity_tl  st,
             wo_activity         fk_wo_activity,
             (select wa.activity_id,
                     wa.activity_code,
                     wt.lang
                from wo_activity wa,
                     wo_activity_tl wt
               where wa.activity_id = wt.activity_id)    pk_wo_activity_tl,
             lang                fk_lang
       where st.process_id     =  I_process_id
         and st.chunk_id       =  I_chunk_id
         and st.activity_code  =  fk_wo_activity.activity_code (+)
         and st.lang           =  pk_wo_activity_tl.lang (+)
         and st.activity_code  =  pk_wo_activity_tl.activity_code (+)
         and st.lang           =  fk_lang.lang (+);

   TYPE svc_wo_activity_tl is TABLE OF C_SVC_WO_ACTIVITY_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_wo_activity_tab        svc_wo_activity_tl;

   L_wo_activity_tl_ins_tab         WO_ACTIVITY_TAB         := NEW WO_ACTIVITY_TAB();
   L_wo_activity_tl_upd_tab         WO_ACTIVITY_TAB         := NEW WO_ACTIVITY_TAB();
   L_wo_activity_tl_del_tab         WO_ACTIVITY_TAB         := NEW WO_ACTIVITY_TAB();
   L_wo_activity_tl_temp_rec        WO_ACTIVITY_REC;

BEGIN
   if C_SVC_WO_ACTIVITY_TL%ISOPEN then
      close C_SVC_WO_ACTIVITY_TL;
   end if;

   open C_SVC_WO_ACTIVITY_TL(I_process_id,
                             I_chunk_id);
   LOOP
      fetch C_SVC_WO_ACTIVITY_TL bulk collect into L_svc_wo_activity_tab limit LP_bulk_fetch_limit;
      if L_svc_wo_activity_tab.COUNT > 0 then
         FOR i in L_svc_wo_activity_tab.FIRST..L_svc_wo_activity_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_wo_activity_tab(i).action is NULL
               or L_svc_wo_activity_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_activity_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_wo_activity_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_activity_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_wo_activity_tab(i).action = action_new
               and L_svc_wo_activity_tab(i).pk_wo_activity_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_activity_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_wo_activity_tab(i).action IN (action_mod, action_del)
               and L_svc_wo_activity_tab(i).lang is NOT NULL
               and L_svc_wo_activity_tab(i).activity_code is NOT NULL
               and L_svc_wo_activity_tab(i).pk_wo_activity_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_wo_activity_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_wo_activity_tab(i).action = action_new
               and L_svc_wo_activity_tab(i).activity_code is NOT NULL
               and L_svc_wo_activity_tab(i).fk_wo_activity_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_wo_activity_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_wo_activity_tab(i).action = action_new
               and L_svc_wo_activity_tab(i).lang is NOT NULL
               and L_svc_wo_activity_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_activity_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_wo_activity_tab(i).action in (action_new, action_mod) then
               if L_svc_wo_activity_tab(i).activity_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_wo_activity_tab(i).row_seq,
                              'ACTIVITY_DESC_REQ',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_wo_activity_tab(i).activity_code is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_activity_tab(i).row_seq,
                           'ACTIVITY_CODE',
                           'ACTIVITY_CODE_REQ');
               L_error :=TRUE;
            end if;

            if L_svc_wo_activity_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_activity_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_wo_activity_tl_temp_rec.lang := L_svc_wo_activity_tab(i).lang;
               L_wo_activity_tl_temp_rec.activity_id := L_svc_wo_activity_tab(i).activity_id;
               L_wo_activity_tl_temp_rec.activity_code := L_svc_wo_activity_tab(i).activity_code;
               L_wo_activity_tl_temp_rec.activity_desc := L_svc_wo_activity_tab(i).activity_desc;
               L_wo_activity_tl_temp_rec.create_datetime := SYSDATE;
               L_wo_activity_tl_temp_rec.create_id := GET_USER;
               L_wo_activity_tl_temp_rec.last_update_datetime := SYSDATE;
               L_wo_activity_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_wo_activity_tab(i).action = action_new then
                  L_wo_activity_TL_ins_tab.extend;
                  L_wo_activity_TL_ins_tab(L_wo_activity_TL_ins_tab.count()) := L_wo_activity_tl_temp_rec;
               end if;

               if L_svc_wo_activity_tab(i).action = action_mod then
                  L_wo_activity_TL_upd_tab.extend;
                  L_wo_activity_TL_upd_tab(L_wo_activity_TL_upd_tab.count()) := L_wo_activity_tl_temp_rec;
                  L_wo_act_tl_upd_rst(L_wo_activity_TL_upd_tab.count()) := L_svc_wo_activity_tab(i).row_seq;
               end if;

               if L_svc_wo_activity_tab(i).action = action_del then
                  L_wo_activity_TL_del_tab.extend;
                  L_wo_activity_TL_del_tab(L_wo_activity_TL_del_tab.count()) := L_wo_activity_tl_temp_rec;
                  L_wo_act_tl_del_rst(L_wo_activity_TL_del_tab.count()) := L_svc_wo_activity_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_WO_ACTIVITY_TL%NOTFOUND;
   END LOOP;
   close C_SVC_WO_ACTIVITY_TL;

   if EXEC_WO_ACT_TL_INS(O_error_message,
                         L_wo_activity_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_WO_ACT_TL_UPD(O_error_message,
                         L_wo_activity_tl_upd_tab,
                         L_wo_act_tl_upd_rst,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_WO_ACT_TL_DEL(O_error_message,
                         L_wo_activity_tl_del_tab,
                         L_wo_act_tl_del_rst,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_WO_ACT_TL;
------------------------------------------------------------------------------
FUNCTION EXEC_WO_TMPL_DETAIL_INS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_wo_tmpl_detail_temp_rec   IN       WO_TMPL_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_WORK_ORDER.EXEC_WO_TMPL_DETAIL_INS';
BEGIN

   insert into wo_tmpl_detail
        values I_wo_tmpl_detail_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WO_TMPL_DETAIL_INS;
----------------------------------------------------------------------------------------
FUNCTION EXEC_WO_TMPL_DETAIL_UPD(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_wo_tmpl_detail_temp_rec   IN       WO_TMPL_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64):= 'CORESVC_WORK_ORDER.EXEC_WO_TMPL_DETAIL_UPD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_WO_TMPL_DETAIL_LOCK is
      select 'X'
        from wo_tmpl_detail
       where activity_id = I_wo_tmpl_detail_temp_rec.activity_id
         and wo_tmpl_id = I_wo_tmpl_detail_temp_rec.wo_tmpl_id
         for update nowait;

BEGIN
   open  C_WO_TMPL_DETAIL_LOCK;
   close C_WO_TMPL_DETAIL_LOCK;

   update wo_tmpl_detail
      set row = I_wo_tmpl_detail_temp_rec
    where 1 = 1
      and activity_id = I_wo_tmpl_detail_temp_rec.activity_id
      and wo_tmpl_id = I_wo_tmpl_detail_temp_rec.wo_tmpl_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'WO_TMPL_DETAIL',
                                                                I_wo_tmpl_detail_temp_rec.activity_id,
                                                                I_wo_tmpl_detail_temp_rec.wo_tmpl_id);
      return FALSE;

   when OTHERS then
      if C_WO_TMPL_DETAIL_LOCK%ISOPEN then
         close C_WO_TMPL_DETAIL_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WO_TMPL_DETAIL_UPD;
-----------------------------------------------------------------------------------------
FUNCTION EXEC_WO_TMPL_DETAIL_DEL(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_wo_tmpl_detail_temp_rec   IN       WO_TMPL_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64):= 'CORESVC_WORK_ORDER.EXEC_WO_TMPL_DETAIL_DEL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_WO_TMPL_DETAIL_LOCK is
      select 'X'
        from wo_tmpl_detail
       where activity_id = I_wo_tmpl_detail_temp_rec.activity_id
         and wo_tmpl_id = I_wo_tmpl_detail_temp_rec.wo_tmpl_id
         for update nowait;

BEGIN
   open  C_WO_TMPL_DETAIL_LOCK;
   close C_WO_TMPL_DETAIL_LOCK;
   delete
     from wo_tmpl_detail
    where 1 = 1
      and wo_tmpl_detail_id = I_wo_tmpl_detail_temp_rec.wo_tmpl_detail_id
      and wo_tmpl_id = I_wo_tmpl_detail_temp_rec.wo_tmpl_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'WO_TMPL_DETAIL',
                                                                I_wo_tmpl_detail_temp_rec.activity_id,
                                                                I_wo_tmpl_detail_temp_rec.wo_tmpl_id);
      return FALSE;

   when OTHERS then
      if C_WO_TMPL_DETAIL_LOCK%ISOPEN then
         close C_WO_TMPL_DETAIL_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WO_TMPL_DETAIL_DEL;
------------------------------------------------------------------------------------------
FUNCTION EXEC_WO_TMPL_HEAD_INS( O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_wo_tmpl_head_temp_rec   IN       WO_TMPL_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_WORK_ORDER.EXEC_WO_TMPL_HEAD_INS';
BEGIN
   insert into wo_tmpl_head
        values I_wo_tmpl_head_temp_rec;

   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WO_TMPL_HEAD_INS;
------------------------------------------------------------------------------------
FUNCTION EXEC_WO_TMPL_HEAD_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_wo_tmpl_head_temp_rec   IN       WO_TMPL_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64):= 'CORESVC_WORK_ORDER.EXEC_WO_TMPL_HEAD_UPD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_WO_TMPL_HEAD_LOCK is
      select 'X'
        from wo_tmpl_head
       where wo_tmpl_id = I_wo_tmpl_head_temp_rec.wo_tmpl_id
         for update nowait;

BEGIN
   open  C_WO_TMPL_HEAD_LOCK;
   close C_WO_TMPL_HEAD_LOCK;

   update wo_tmpl_head
      set row = I_wo_tmpl_head_temp_rec
    where 1 = 1
      and wo_tmpl_id = I_wo_tmpl_head_temp_rec.wo_tmpl_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'WO_TMPL_HEAD',
                                                                I_wo_tmpl_head_temp_rec.wo_tmpl_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
      if C_WO_TMPL_HEAD_LOCK%ISOPEN then
         close C_WO_TMPL_HEAD_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_WO_TMPL_HEAD_UPD;
--------------------------------------------------------------------------------------------------
FUNCTION EXEC_WO_TMPL_HEAD_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_wo_tmpl_head_temp_rec   IN       WO_TMPL_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_WORK_ORDER.EXEC_WO_TMPL_HEAD_DEL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_WO_TMPL_DETAIL_LOCK is
      select 'X'
        from wo_tmpl_detail
       where wo_tmpl_id = I_wo_tmpl_head_temp_rec.wo_tmpl_id
         for update nowait;

   cursor C_LOCK_WTHEAD_TL_DEL is
      select 'x'
        from wo_tmpl_head_tl
       where wo_tmpl_id = I_wo_tmpl_head_temp_rec.wo_tmpl_id
         for update nowait;

   cursor C_WO_TMPL_HEAD_LOCK is
      select 'X'
        from wo_tmpl_head
       where wo_tmpl_id = I_wo_tmpl_head_temp_rec.wo_tmpl_id
         for update nowait;

BEGIN
   open  C_WO_TMPL_DETAIL_LOCK;
   close C_WO_TMPL_DETAIL_LOCK;

   delete
      from wo_tmpl_detail
    where 1 = 1
      and wo_tmpl_id = I_wo_tmpl_head_temp_rec.wo_tmpl_id;

   open  C_LOCK_WTHEAD_TL_DEL;
   close C_LOCK_WTHEAD_TL_DEL;

   delete
      from wo_tmpl_head_tl
    where 1 = 1
      and wo_tmpl_id = I_wo_tmpl_head_temp_rec.wo_tmpl_id;

   open  C_WO_TMPL_HEAD_LOCK;
   close C_WO_TMPL_HEAD_LOCK;

   delete
     from wo_tmpl_head
    where 1 = 1
      and wo_tmpl_id = I_wo_tmpl_head_temp_rec.wo_tmpl_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'WO_TMPL_HEAD',
                                                                I_wo_tmpl_head_temp_rec.wo_tmpl_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
      if C_WO_TMPL_DETAIL_LOCK%ISOPEN then
         close C_WO_TMPL_DETAIL_LOCK;
      end if;

      if C_LOCK_WTHEAD_TL_DEL%ISOPEN then
         close C_LOCK_WTHEAD_TL_DEL;
      end if;

      if C_WO_TMPL_HEAD_LOCK%ISOPEN then
         close C_WO_TMPL_HEAD_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      return FALSE;

END EXEC_WO_TMPL_HEAD_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WOTHEAD_TL_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wothead_tl_ins_tab  IN       WO_TMPL_HEAD_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_WORK_ORDER.EXEC_WOTHEAD_TL_INS';

BEGIN
   if I_wothead_tl_ins_tab is NOT NULL and I_wothead_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_wothead_tl_ins_tab.COUNT()
         insert into wo_tmpl_head_tl
              values I_wothead_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_WOTHEAD_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WOTHEAD_TL_UPD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wothead_tl_upd_tab  IN       WO_TMPL_HEAD_TAB,
                             I_wothead_tl_upd_rst  IN       ROW_SEQ_TAB,
                             I_process_id          IN       SVC_WO_TMPL_HEAD_TL.PROCESS_ID%TYPE,
                             I_chunk_id            IN       SVC_WO_TMPL_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_WORK_ORDER.EXEC_WOTHEAD_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WO_TMPL_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_WOTHEAD_TL_UPD(I_wo_tmpl_head  WO_TMPL_HEAD_TL.WO_TMPL_ID%TYPE,
                                I_lang          WO_TMPL_HEAD_TL.LANG%TYPE) is
      select 'x'
        from wo_tmpl_head_tl
       where wo_tmpl_id = I_wo_tmpl_head
         and lang = I_lang
         for update nowait;

BEGIN
   if I_wothead_tl_upd_tab is NOT NULL and I_wothead_tl_upd_tab.count > 0 then
      for i in I_wothead_tl_upd_tab.FIRST..I_wothead_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_wothead_tl_upd_tab(i).lang);
            L_key_val2 := 'Work Order Template ID: '||to_char(I_wothead_tl_upd_tab(i).wo_tmpl_id);
            open C_LOCK_WOTHEAD_TL_UPD(I_wothead_tl_upd_tab(i).wo_tmpl_id,
                                     I_wothead_tl_upd_tab(i).lang);
            close C_LOCK_WOTHEAD_TL_UPD;
            
            update wo_tmpl_head_tl
               set wo_tmpl_desc = I_wothead_tl_upd_tab(i).wo_tmpl_desc,
                   last_update_id = I_wothead_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_wothead_tl_upd_tab(i).last_update_datetime
             where lang = I_wothead_tl_upd_tab(i).lang
               and wo_tmpl_id = I_wothead_tl_upd_tab(i).wo_tmpl_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_WO_TMPL_HEAD_TL',
                           I_wothead_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_WOTHEAD_TL_UPD%ISOPEN then
         close C_LOCK_WOTHEAD_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_WOTHEAD_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_WOTHEAD_TL_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_wothead_tl_del_tab   IN       WO_TMPL_HEAD_TAB,
                             I_wothead_tl_del_rst   IN       ROW_SEQ_TAB,
                             I_process_id           IN       SVC_WO_TMPL_HEAD_TL.PROCESS_ID%TYPE,
                             I_chunk_id             IN       SVC_WO_TMPL_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_WORK_ORDER.EXEC_WOTHEAD_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WO_TMPL_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_WTHEAD_TL_DEL(I_wo_tmpl_id  WO_TMPL_HEAD_TL.wo_tmpl_id%TYPE,
                               I_lang        WO_TMPL_HEAD_TL.LANG%TYPE) is
      select 'x'
        from wo_tmpl_head_tl
       where wo_tmpl_id = I_wo_tmpl_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_wothead_tl_del_tab is NOT NULL and I_wothead_tl_del_tab.count > 0 then
      for i in I_wothead_tl_del_tab.FIRST..I_wothead_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_wothead_tl_del_tab(i).lang);
            L_key_val2 := 'Work Order Template ID: '||to_char(I_wothead_tl_del_tab(i).wo_tmpl_id);
            open C_LOCK_WTHEAD_TL_DEL(I_wothead_tl_del_tab(i).wo_tmpl_id,
                                      I_wothead_tl_del_tab(i).lang);
            close C_LOCK_WTHEAD_TL_DEL;
            
            delete wo_tmpl_head_tl
             where lang = I_wothead_tl_del_tab(i).lang
               and wo_tmpl_id = I_wothead_tl_del_tab(i).wo_tmpl_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_WO_TMPL_HEAD_TL',
                           I_wothead_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_WTHEAD_TL_DEL%ISOPEN then
         close C_LOCK_WTHEAD_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_WOTHEAD_TL_DEL;
--------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_WO_TMPL(O_error_message IN OUT VARCHAR2,
                                    O_error         IN OUT BOOLEAN,
                                    I_rec           IN     C_SVC_WO_TMPL%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)     :='CORESVC_WORK_ORDER.PROCESS_VAL_WO_TMPL_DETAIL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_WO_TMPL_DETAIL';
BEGIN

   if I_rec.stwd_unit_cost is NOT NULL
      and I_rec.stwd_unit_cost < 0 then
      WRITE_ERROR(I_rec.stwd_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.stwd_chunk_id,
                  L_table,
                  I_rec.stwd_row_seq,
                  'UNIT_COST',
                  'UNIT_COST_NOT_NEG');
      O_error :=TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_VAL_WO_TMPL;
-------------------------------------------------------------------------------------------
FUNCTION PROCESS_WO_TMPL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id    IN     SVC_WO_TMPL_DETAIL.PROCESS_ID%TYPE,
                                I_chunk_id      IN     SVC_WO_TMPL_DETAIL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                     VARCHAR2(64)       :='CORESVC_WORK_ORDER.PROCESS_WO_TMPL_DETAIL';
   L_process_error               BOOLEAN            := FALSE;
   L_wo_tmpl_detail_temp_rec     WO_TMPL_DETAIL%ROWTYPE;
   L_table                       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_WO_TMPL_DETAIL';
   L_table1                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_WO_TMPL_HEAD';
   L_next_wo_tmpl_dtl_seq        WO_TMPL_DETAIL.WO_TMPL_DETAIL_ID%TYPE;
   L_wo_tmpl_head_temp_rec       WO_TMPL_HEAD%ROWTYPE;
   L_next_wo_tmpl_hd_seq         WO_TMPL_HEAD.WO_TMPL_ID%TYPE;
   L_count                       NUMBER(10)         :=1;
   L_processed_head              BOOLEAN;
   L_validate_head               BOOLEAN;
   L_temp                        WO_TMPL_DETAIL.WO_TMPL_ID%TYPE;

   L_rowseq_count               NUMBER;
   c                            NUMBER      := 1;
    TYPE L_row_seq_tab_type IS TABLE OF SVC_WO_TMPL_DETAIL.ROW_SEQ%TYPE;
   L_row_seq_tab                       L_row_seq_tab_type;

    cursor C_CHK_DTL_ACT(wo_hd_id SVC_WO_TMPL_HEAD.WO_TMPL_ID%TYPE) is
      select COUNT('X')
        from wo_tmpl_detail
       where wo_tmpl_id=wo_hd_id;

BEGIN
   L_row_seq_tab := L_row_seq_tab_type();
   FOR rec IN C_SVC_WO_TMPL(I_process_id,
                            I_chunk_id)
   LOOP
      LP_error       := FALSE;
      L_process_error := FALSE;

      if rec.ch_rank=1 then
         L_next_wo_tmpl_hd_seq:=NULL;
         L_processed_head:=FALSE;
            L_row_seq_tab.DELETE;
         c:=1;
         L_validate_head:=FALSE;
         SAVEPOINT successful_item;
      end if;

      if rec.stwd_action NOT IN (action_new,action_mod,action_del)
         or (NVL(rec.stwd_action,'-1') = NVL(NULL,'-1') and rec.stwd_rid is NOT NULL) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.stwd_row_seq,
                     'ACTION',
                     'INV_ACT');
         LP_error :=TRUE;
      end if;

      if rec.stwh_action NOT IN (action_new,action_mod,action_del)
         or (NVL(rec.stwh_action,'-1') = NVL(NULL,'-1') and rec.stwh_rid is NOT NULL) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table1,
                     rec.stwh_row_seq,
                     'ACTION',
                     'INV_ACT');
         L_validate_head:=TRUE;
         LP_error :=TRUE;
      end if;

      if rec.ch_rank=1 then
         if rec.stwh_action IN (action_new,action_mod)
            and rec.stwh_wo_tmpl_desc is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table1,
                        rec.stwh_row_seq,
                        'WO_TMPL_DESC',
                        'WO_TMPL_DESC_REQ');
            LP_error :=TRUE;
            L_validate_head:=TRUE;
         end if;

         if rec.stwh_action IN (action_mod,action_del)
            and rec.stwh_wo_tmpl_id is NOT NULL
            and rec.wtd_wth_fk_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table1,
                        rec.stwh_row_seq,
                        'WO_TMPL_ID',
                        'INV_WO_TMPL');
            LP_error :=TRUE;
            L_validate_head:=TRUE;
         end if;
      end if;

      if rec.stwd_action IN (action_new) then
         if rec.stwh_action is NULL 
            and rec.uk_wo_tmpl_detail_rid is NOT NULL then
            WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.stwd_row_seq,
                     'Work Order Template,Activity',
                     'WO_TMPL_ACTIVITY_EXISTS');
            LP_error :=TRUE;
         end if;
      end if;
      if rec.stwd_action is not null and rec.wtd_woc_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
     	             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
	             I_chunk_id,
	             L_table,
	             rec.stwd_row_seq,
	             'ACTIVITY_CODE',
	             'INV_ACTIVITY');
          LP_error :=TRUE;
      end if;

      if rec.stwd_action IN (action_mod,action_del)
         and rec.stwd_wo_tmpl_id is NOT NULL
         and rec.stwd_activity_code is NOT NULL
         and rec.uk_wo_tmpl_detail_rid is NULL then

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.stwd_row_seq,
                     'Work Order Template,Activity',
                     'INV_COMB_TMPL_ID_ACTIVITY');
         LP_error :=TRUE;
      end if;

      if rec.stwd_action IN (action_new,
                             action_mod)
         and rec.stwd_unit_cost is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.stwd_row_seq,
                     'UNIT_COST',
                     'ACT_COST_REQ');
         LP_error :=TRUE;
      end if;

      if rec.stwd_action IN (action_new,action_mod,action_del)
         and rec.stwd_wo_tmpl_id is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.stwd_row_seq,
                     'WO_TMPL_ID',
                     'WO_TMPL_ID_REQ');
         LP_error :=TRUE;
      end if;
      if rec.stwd_action IN (action_new,action_mod,action_del)
         and NVL(rec.stwh_action,'-1') NOT IN (action_new)
         and rec.wtd_wth_fk_rid is NULL
         and rec.wtd_woc_fk_rid is NOT NULL then

            WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.stwd_row_seq,
                     'WO_TMPL_ID',
                     'INV_WO_TMPL');
         LP_error :=TRUE;
      end if;

      if PROCESS_VAL_WO_TMPL(O_error_message,
                             LP_error,
                             rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.stwd_row_seq,
                     NULL,
                     O_error_message);
         LP_error :=TRUE;
      end if;

      if L_validate_head
         and rec.stwd_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.stwd_row_seq,
                     'WO_TMPL_ID',
                     'ERR_IN_CORRESPONDING_TMPL');
         LP_error :=TRUE;
      end if;

      if LP_error
         and rec.stwd_action=action_new
         and rec.stwh_action=action_new
         and L_next_wo_tmpl_hd_seq is NOT NULL then
             ROLLBACK TO successfuL_item;
             WRITE_ERROR(I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table1,
                         rec.stwh_row_seq,
                         'WO_TMPL_ID',
                         'INV_WO_TMPL');
            L_process_error:=TRUE;
      end if;

      if NOT LP_error then
         if rec.stwh_action=action_new
            and rec.ch_rank=1
            and L_next_wo_tmpl_hd_seq is NULL then
            if TSF_WO_SQL.NEXT_WO_TMPL_ID_SEQ_NO(O_error_message,
                                                 L_next_wo_tmpl_hd_seq)=FALSE then
               WRITE_ERROR(I_process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_chunk_id,
                         L_table1,
                         rec.stwh_row_seq,
                         NULL,
                         O_error_message);
               LP_error :=TRUE;
            else
               L_wo_tmpl_head_temp_rec.wo_tmpl_id           := L_next_wo_tmpl_hd_seq;
               L_wo_tmpl_detail_temp_rec.wo_tmpl_id         := L_next_wo_tmpl_hd_seq;
            end if;
         end if;

         L_next_wo_tmpl_dtl_seq:=NULL;

         if rec.stwd_action=action_new
            and L_next_wo_tmpl_dtl_seq is NULL then
            if TSF_WO_SQL.NEXT_WO_TMPLDTAIL_SEQ_NO(O_error_message,
                                                   L_next_wo_tmpl_dtl_seq)=FALSE then
                WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                          I_chunk_id,
                          L_table,
                          rec.stwd_row_seq,
                          NULL,
                          O_error_message);
              LP_error :=TRUE;
            else
              update svc_wo_tmpl_head_tl
                 set wo_tmpl_id = L_next_wo_tmpl_dtl_seq 
              where  wo_tmpl_id = rec.stwh_wo_tmpl_id
                 and process_id =I_process_id
                 and chunk_id = I_chunk_id
                 and action   = action_new;
              L_wo_tmpl_detail_temp_rec.wo_tmpl_detail_id              := L_next_wo_tmpl_dtl_seq;
            end if;
         end if;

         if rec.stwd_action=action_new
            and rec.ch_rank>1
            and NVL(rec.stwh_wo_tmpl_id,-1)= NVL(rec.stwd_wo_tmpl_id,-1)
            and L_next_wo_tmpl_hd_seq is NOT NULL then
            L_wo_tmpl_detail_temp_rec.wo_tmpl_id         := L_next_wo_tmpl_hd_seq;

         else
            if NVL(rec.stwh_wo_tmpl_id,-1)= NVL(rec.stwd_wo_tmpl_id,-1)
               and rec.stwd_action=action_new
               and rec.stwh_action=action_new
               and L_next_wo_tmpl_hd_seq is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.stwd_row_seq,
                           'WO_TMPL_ID',
                           'WO_TMPL_ID_NOT_FOUND');
                LP_error :=TRUE;
            end if;
         end if;

         if rec.stwd_action=action_new
            and NVL(rec.stwh_action,'-1') NOT IN (action_new) then
            
            L_wo_tmpl_detail_temp_rec.wo_tmpl_id        :=rec.stwd_wo_tmpl_id;
         end if;

         if rec.stwh_action IN (action_mod,action_del) then
            L_wo_tmpl_head_temp_rec.wo_tmpl_id          :=rec.stwh_wo_tmpl_id;
         end if;

         if rec.stwd_action IN (action_mod,action_del) then
            L_wo_tmpl_detail_temp_rec.wo_tmpl_detail_id          :=rec.wo_tmpl_detail_id;
            L_wo_tmpl_detail_temp_rec.wo_tmpl_id                 :=rec.stwd_wo_tmpl_id;
         end if;


          L_wo_tmpl_head_temp_rec.wo_tmpl_desc                    := rec.stwh_wo_tmpl_desc;

          L_wo_tmpl_detail_temp_rec.activity_id                   := rec.stwd_activity_id;
          L_wo_tmpl_detail_temp_rec.unit_cost                     := rec.stwd_unit_cost;
          L_wo_tmpl_detail_temp_rec.comments                      := rec.stwd_comments;

          if rec.ch_rank=1 then
             if rec.stwh_action = action_new then
                if EXEC_WO_TMPL_HEAD_INS( O_error_message,
                                          L_wo_tmpl_head_temp_rec)=FALSE then
                   WRITE_ERROR(I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_table1,
                               rec.stwh_row_seq,
                               NULL,
                               O_error_message);
                   L_process_error :=TRUE;
                   L_processed_head:=TRUE;
                end if;
             end if;

             if rec.stwh_action = action_mod then
                if EXEC_WO_TMPL_HEAD_UPD( O_error_message,
                                          L_wo_tmpl_head_temp_rec)=FALSE then
                   WRITE_ERROR(I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_table1,
                               rec.stwh_row_seq,
                               NULL,
                               O_error_message);
                   L_process_error :=TRUE;
                   L_processed_head:=TRUE;
                end if;
             end if;

             if rec.stwh_action = action_del then

                if EXEC_WO_TMPL_HEAD_DEL( O_error_message,
                                          L_wo_tmpl_head_temp_rec)=FALSE then
                   WRITE_ERROR(I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_table1,
                               rec.stwh_row_seq,
                               NULL,
                               O_error_message);
                   L_process_error :=TRUE;
                   L_processed_head:=TRUE;
                end if;
             end if;
         end if;

         if NOT L_processed_head
            and rec.stwd_action in (action_new,action_mod,action_del) then
                if rec.stwh_action is NULL
                   or rec.stwh_action <> action_del then
                   if rec.stwd_action = action_new then
         
                      if EXEC_WO_TMPL_DETAIL_INS(O_error_message,
                                                 L_wo_tmpl_detail_temp_rec)=FALSE then
                         WRITE_ERROR(I_process_id,
                                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                     I_chunk_id,
                                     L_table,
                                     rec.stwd_row_seq,
                                     NULL,
                                     O_error_message);
                          L_process_error :=TRUE;
                      end if;
                   end if;

                  if rec.stwd_action = action_mod then
                     if EXEC_WO_TMPL_DETAIL_UPD(O_error_message,
                                                L_wo_tmpl_detail_temp_rec)=FALSE then
                        WRITE_ERROR(I_process_id,
                                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                    I_chunk_id,
                                    L_table,
                                    rec.stwd_row_seq,
                                    NULL,
                                    O_error_message);
                        L_process_error :=TRUE;
                     end if;
                  end if;

                  if rec.stwd_action = action_del then
                     if EXEC_WO_TMPL_DETAIL_DEL(O_error_message,
                                                L_wo_tmpl_detail_temp_rec)=FALSE then
 
                         WRITE_ERROR(I_process_id,
                                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                     I_chunk_id,
                                     L_table,
                                     rec.stwd_row_seq,
                                     NULL,
                                     O_error_message);
                         L_process_error :=TRUE;
                     end if;
                  end if;
                  if NOT L_process_error then
                     L_row_seq_tab.extend();
                     L_row_seq_tab(c)  := rec.stwd_row_seq;
                     c:=c+1;
                  end if;
          else

                 WRITE_ERROR(I_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_chunk_id,
                             L_table,
                             rec.stwd_row_seq,
                             'WO_TMPL_ID',
                             'WO_TMPL_REC_DEL');
                 L_process_error :=TRUE;
           end if;
        end if;
     end if;
     if NVL(rec.stwh_wo_tmpl_id,-1) <> NVL(rec.next_stwh_wo_tmpl_id,-1) then
        if rec.stwh_action=action_new then
           open C_CHK_DTL_ACT(L_wo_tmpl_head_temp_rec.wo_tmpl_id);
           fetch C_CHK_DTL_ACT into L_count;
           close C_CHK_DTL_ACT;
        end if;

        if rec.stwd_action=action_del then
           open C_CHK_DTL_ACT(L_wo_tmpl_detail_temp_rec.wo_tmpl_id);
           fetch C_CHK_DTL_ACT into L_count;
           close C_CHK_DTL_ACT;
        end if;

        if L_count < 1 then
           if rec.stwh_action=action_new then
              ROLLBACK TO successful_item;
              WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_chunk_id,
                          L_table1,
                          rec.stwh_row_seq,
                          'WO_TMPL_ID',
                          'SHD_HAVE_ONE_ACTIVITY');
                           L_process_error:=TRUE;
           end if;
           L_rowseq_count := L_row_seq_tab.count();
           if L_rowseq_count > 0 then
               for i in 1..L_rowseq_count
               LOOP
                  if rec.stwd_action=action_del
                     and nvl(rec.stwh_action,'-1') <> action_del then
                     ROLLBACK TO successful_item;
                        WRITE_ERROR(I_process_id,
                                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                    I_chunk_id,
                                    L_table,
                                    L_row_seq_tab(i),
                                    'WO_TMPL_ID',
                                    'SHD_HAVE_ONE_ACTIVITY');
                         L_process_error:=TRUE;
                  end if;
                  L_count:=0;
               END LOOP;
          end if;
        end if;
     end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

        return FALSE;
END PROCESS_WO_TMPL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_WOTHEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_WO_TMPL_HEAD_TL.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_WO_TMPL_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                     VARCHAR2(64) := 'CORESVC_WORK_ORDER.PROCESS_WOTHEAD_TL';
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WO_TMPL_HEAD_TL';
   L_base_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'WO_TMPL_HEAD';
   L_table                       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_WO_TMPL_HEAD_TL';
   L_error                       BOOLEAN := FALSE;
   L_process_error               BOOLEAN := FALSE;
   L_wo_tmpl_head_TL_temp_rec    WO_TMPL_HEAD_TL%ROWTYPE;
   L_wothead_tl_upd_rst          ROW_SEQ_TAB;
   L_wothead_tl_del_rst          ROW_SEQ_TAB;

   cursor C_SVC_WO_TMPL_HEAD_TL(I_process_id NUMBER,
                                I_chunk_id NUMBER) is
      select pk_wo_tmpl_head_tl.rowid  as pk_wo_tmpl_head_tl_rid,
             fk_wo_tmpl_head.rowid     as fk_wo_tmpl_head_rid,
             fk_lang.rowid             as fk_lang_rid,
             st.lang,
             st.wo_tmpl_id,
             st.wo_tmpl_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_wo_tmpl_head_tl  st,
             wo_tmpl_head         fk_wo_tmpl_head,
             wo_tmpl_head_tl      pk_wo_tmpl_head_tl,
             lang                 fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.wo_tmpl_id  =  fk_wo_tmpl_head.wo_tmpl_id (+)
         and st.lang        =  pk_wo_tmpl_head_tl.lang (+)
         and st.wo_tmpl_id  =  pk_wo_tmpl_head_tl.wo_tmpl_id (+)
         and st.lang        =  fk_lang.lang (+)
         and st.process$status = 'N';

   TYPE SVC_WO_TMPL_HEAD_TL is TABLE OF C_SVC_WO_TMPL_HEAD_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_wo_tmpl_head_tab        SVC_WO_TMPL_HEAD_TL;

   L_wo_tmpl_head_TL_ins_tab         wo_tmpl_head_tab         := NEW wo_tmpl_head_tab();
   L_wo_tmpl_head_TL_upd_tab         wo_tmpl_head_tab         := NEW wo_tmpl_head_tab();
   L_wo_tmpl_head_TL_del_tab         wo_tmpl_head_tab         := NEW wo_tmpl_head_tab();

BEGIN
   if C_SVC_WO_TMPL_HEAD_TL%ISOPEN then
      close C_SVC_WO_TMPL_HEAD_TL;
   end if;

   open C_SVC_WO_TMPL_HEAD_TL(I_process_id,
                              I_chunk_id);
   LOOP
      fetch C_SVC_WO_TMPL_HEAD_TL bulk collect into L_svc_wo_tmpl_head_tab limit LP_bulk_fetch_limit;
      if L_svc_wo_tmpl_head_tab.COUNT > 0 then
         FOR i in L_svc_wo_tmpl_head_tab.FIRST..L_svc_wo_tmpl_head_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_wo_tmpl_head_tab(i).action is NULL
               or L_svc_wo_tmpl_head_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_tmpl_head_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_wo_tmpl_head_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_tmpl_head_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_wo_tmpl_head_tab(i).action = action_new
               and L_svc_wo_tmpl_head_tab(i).pk_wo_tmpl_head_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_tmpl_head_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_wo_tmpl_head_tab(i).action IN (action_mod, action_del)
               and L_svc_wo_tmpl_head_tab(i).lang is NOT NULL
               and L_svc_wo_tmpl_head_tab(i).wo_tmpl_id is NOT NULL
               and L_svc_wo_tmpl_head_tab(i).pk_wo_tmpl_head_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_wo_tmpl_head_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_wo_tmpl_head_tab(i).action = action_new
               and L_svc_wo_tmpl_head_tab(i).wo_tmpl_id is NOT NULL
               and L_svc_wo_tmpl_head_tab(i).fk_wo_tmpl_head_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_wo_tmpl_head_tab(i).row_seq,
                            'WO_TMPL_ID',
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_wo_tmpl_head_tab(i).action = action_new
               and L_svc_wo_tmpl_head_tab(i).lang is NOT NULL
               and L_svc_wo_tmpl_head_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_tmpl_head_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_wo_tmpl_head_tab(i).action in (action_new, action_mod) then
               if L_svc_wo_tmpl_head_tab(i).wo_tmpl_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_wo_tmpl_head_tab(i).row_seq,
                              'WO_TMPL_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_wo_tmpl_head_tab(i).wo_tmpl_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_tmpl_head_tab(i).row_seq,
                           'WO_TMPL_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_wo_tmpl_head_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_wo_tmpl_head_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_wo_tmpl_head_TL_temp_rec.lang := L_svc_wo_tmpl_head_tab(i).lang;
               L_wo_tmpl_head_TL_temp_rec.wo_tmpl_id := L_svc_wo_tmpl_head_tab(i).wo_tmpl_id;
               L_wo_tmpl_head_TL_temp_rec.wo_tmpl_desc := L_svc_wo_tmpl_head_tab(i).wo_tmpl_desc;
               L_wo_tmpl_head_TL_temp_rec.create_datetime := SYSDATE;
               L_wo_tmpl_head_TL_temp_rec.create_id := GET_USER;
               L_wo_tmpl_head_TL_temp_rec.last_update_datetime := SYSDATE;
               L_wo_tmpl_head_TL_temp_rec.last_update_id := GET_USER;

               if L_svc_wo_tmpl_head_tab(i).action = action_new then
                  L_wo_tmpl_head_TL_ins_tab.extend;
                  L_wo_tmpl_head_TL_ins_tab(L_wo_tmpl_head_TL_ins_tab.count()) := L_wo_tmpl_head_TL_temp_rec;
               end if;

               if L_svc_wo_tmpl_head_tab(i).action = action_mod then
                  L_wo_tmpl_head_TL_upd_tab.extend;
                  L_wo_tmpl_head_TL_upd_tab(L_wo_tmpl_head_TL_upd_tab.count()) := L_wo_tmpl_head_TL_temp_rec;
                  L_wothead_tl_upd_rst(L_wo_tmpl_head_TL_upd_tab.count()) := L_svc_wo_tmpl_head_tab(i).row_seq;
               end if;

               if L_svc_wo_tmpl_head_tab(i).action = action_del then
                  L_wo_tmpl_head_TL_del_tab.extend;
                  L_wo_tmpl_head_TL_del_tab(L_wo_tmpl_head_TL_del_tab.count()) := L_wo_tmpl_head_TL_temp_rec;
                  L_wothead_tl_del_rst(L_wo_tmpl_head_TL_del_tab.count()) := L_svc_wo_tmpl_head_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_WO_TMPL_HEAD_TL%NOTFOUND;
   END LOOP;
   close C_SVC_WO_TMPL_HEAD_TL;

   if EXEC_WOTHEAD_TL_INS(O_error_message,
                          L_wo_tmpl_head_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_WOTHEAD_TL_UPD(O_error_message,
                          L_wo_tmpl_head_TL_upd_tab,
                          L_wothead_tl_upd_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_WOTHEAD_TL_DEL(O_error_message,
                          L_wo_tmpl_head_TL_del_tab,
                          L_wothead_tl_del_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_WOTHEAD_TL;
-------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
     from svc_wo_activity_tl
    where process_id = I_process_id;

   delete
     from svc_wo_activity
    where process_id = I_process_id;
    
   Delete
     from svc_wo_tmpl_head_tl
    where process_id= I_process_id;

   Delete
     from svc_wo_tmpl_head
    where process_id= I_process_id;

   Delete
     from svc_wo_tmpl_detail
    where process_id= I_process_id;

    
END;
-------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      :='CORESVC_WORK_ORDER.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE   := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_WO_ACTIVITY(O_error_message,
                          I_process_id,
                          I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_WO_ACT_TL(O_error_message,
                        I_process_id,
                        I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_WO_TMPL(O_error_message,
                                I_process_id,
                                I_chunk_id)=FALSE then
         return FALSE;
   end if; 
   if PROCESS_WOTHEAD_TL(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE then
         return FALSE;
      end if;
    
   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
        set status = (case
                        when status = 'PE'
                        then 'PE'
                    else L_process_status
                    end),
              action_date = sysdate
    where process_id = I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
---------------------------------------------------------------------------------
END CORESVC_WORK_ORDER;
/