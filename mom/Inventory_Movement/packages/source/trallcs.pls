
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TRAN_ALLOC_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------
FUNCTION INSERT_ALC_COMP_LOCS(O_error_message     IN OUT VARCHAR2,
                              I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                              I_item              IN     ITEM_MASTER.ITEM%TYPE,
                              I_pack_item         IN     ITEM_MASTER.ITEM%TYPE,
                              I_obligation_key    IN     OBLIGATION.OBLIGATION_KEY%TYPE,
                              I_vessel_id         IN     TRANSPORTATION.VESSEL_ID%TYPE,
                              I_voyage_flt_id     IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                              I_etd               IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                              I_comp_id           IN     ELC_COMP.COMP_ID%TYPE,
                              I_location          IN     ORDLOC.LOCATION%TYPE,
                              I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                              I_act_value         IN     ALC_COMP_LOC.ACT_VALUE%TYPE,
                              I_qty               IN     ALC_COMP_LOC.QTY%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION ALLOC_PO_ITEM(O_error_message         IN OUT VARCHAR2,
                       I_obligation_key        IN     OBLIGATION.OBLIGATION_KEY%TYPE,
                       I_obligation_level      IN     OBLIGATION.OBLIGATION_LEVEL%TYPE,
                       I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                       I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                       I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                       I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                       I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                       I_comp_id               IN     ELC_COMP.COMP_ID%TYPE,
                       I_alloc_basis_uom       IN     UOM_CLASS.UOM%TYPE,
                       I_qty                   IN     OBLIGATION_COMP.QTY%TYPE,
                       I_amt_prim              IN     OBLIGATION_COMP.AMT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
FUNCTION INSERT_ALC_HEAD(O_error_message     IN OUT VARCHAR2,
                         I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item         IN     ITEM_MASTER.ITEM%TYPE,
                         I_obligation_key    IN     OBLIGATION.OBLIGATION_KEY%TYPE,
                         I_ce_id             IN     CE_HEAD.CE_ID%TYPE,
                         I_vessel_id         IN     TRANSPORTATION.VESSEL_ID%TYPE,
                         I_voyage_flt_id     IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                         I_etd               IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                         I_item_qty          IN     ORDLOC.QTY_RECEIVED%TYPE,
                         I_error_ind         IN     ALC_HEAD.ERROR_IND%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
END TRAN_ALLOC_SQL;
/
