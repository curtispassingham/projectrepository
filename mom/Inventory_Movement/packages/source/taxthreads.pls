CREATE OR REPLACE PACKAGE TAX_THREAD_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------------
-- LOAD_REPL_ORDER_TAX_OBJECT:
-- Function:   This function insert/updates the ORD_TAX_BREAKUP table for approved replenishment
--             purchase orders.  It accepts a thread number and number of allowed parallel threads
--             value and reads from the REPL_APPRV_GTAX_QUEUE table.  Records are locked then
--             L10N_SQL.EXEC_FUNCTION is called per PO for tax processing.  Records that were
--             processed are then deleted from the REPL_APPRV_GTAX_QUEUE table.
--
-- Scheduling: This function is called by the batch_rplapprvgtax.ksh batch.
--             The script batch_rplapprvgtax.ksh is scheduled to run run after rplapprv.pc
--------------------------------------------------------------------------------------------------
FUNCTION LOAD_REPL_ORDER_TAX_BREAKUP(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_thread_id         IN     NUMBER,
                                     I_number_of_threads IN     NUMBER)
return BOOLEAN;
--------------------------------------------------------------------------------------------------
END;
/

