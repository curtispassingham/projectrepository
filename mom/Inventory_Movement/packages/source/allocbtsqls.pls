CREATE OR REPLACE PACKAGE ALLOC_BOOK_TSF_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------
-- FUNCTION: PROCESS_BOOK_TSF
-- Purpose:  Creates book transfers for allocations between
--           virtual warehouses in the same physical warehouse.
------------------------------------------------------------------
FUNCTION PROCESS_BOOK_TSF (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_thread_no       IN       NUMBER,
                           I_num_threads     IN       NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END ALLOC_BOOK_TSF_SQL;
/
