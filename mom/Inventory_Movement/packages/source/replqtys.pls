CREATE OR REPLACE PACKAGE GET_REPL_ORDER_QTY_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- Function Name        : REPL_METHOD
-- Purpose              : This package will be used to calculate the
--                        SKU-location quantity for replenishment based on the
--                        given replenishment method passed into the program. 
-- Called by            : Package (ordrclcb.pls)
-- Calls                : itemloc_quantity_sql.get_store_future_avail
--                        itemloc_quantity_sql.get_wh_future_avail
--------------------------------------------------------------------------------

FUNCTION REPL_METHOD(
                     O_error_message            IN OUT   VARCHAR2,
                     O_order_qty                IN OUT   ORDLOC.QTY_ORDERED%TYPE,                  
                     O_order_point              IN OUT   REPL_RESULTS.ORDER_POINT%TYPE,            
                     O_order_up_to_point        IN OUT   REPL_RESULTS.ORDER_UP_TO_POINT%TYPE,      
                     O_net_inventory            IN OUT   REPL_RESULTS.NET_INVENTORY%TYPE,          
                     O_stock_on_hand            IN OUT   REPL_RESULTS.STOCK_ON_HAND%TYPE,          
                     O_pack_comp_soh            IN OUT   REPL_RESULTS.PACK_COMP_SOH%TYPE,          
                     O_on_order                 IN OUT   REPL_RESULTS.ON_ORDER%TYPE,               
                     O_in_transit_qty           IN OUT   REPL_RESULTS.IN_TRANSIT_QTY%TYPE,         
                     O_pack_comp_intran         IN OUT   REPL_RESULTS.PACK_COMP_INTRAN%TYPE,       
                     O_tsf_resv_qty             IN OUT   REPL_RESULTS.TSF_RESV_QTY%TYPE,           
                     O_pack_comp_resv           IN OUT   REPL_RESULTS.PACK_COMP_RESV%TYPE,         
                     O_tsf_expected_qty         IN OUT   REPL_RESULTS.TSF_EXPECTED_QTY%TYPE,       
                     O_pack_comp_exp            IN OUT   REPL_RESULTS.PACK_COMP_EXP%TYPE,          
                     O_rtv_qty                  IN OUT   REPL_RESULTS.RTV_QTY%TYPE,                
                     O_alloc_in_qty             IN OUT   REPL_RESULTS.ALLOC_IN_QTY%TYPE,           
                     O_alloc_out_qty            IN OUT   REPL_RESULTS.ALLOC_OUT_QTY%TYPE,          
                     O_non_sellable_qty         IN OUT   REPL_RESULTS.NON_SELLABLE_QTY%TYPE,       
                     O_safety_stock             IN OUT   REPL_RESULTS.SAFETY_STOCK%TYPE,           
                     O_lost_sales               IN OUT   REPL_RESULTS.LOST_SALES%TYPE,             
                     O_due_ind                  IN OUT   REPL_RESULTS.DUE_IND%TYPE,                
                     O_aso                      IN OUT   REPL_RESULTS.ACCEPTED_STOCK_OUT%TYPE,     
                     O_eso                      IN OUT   REPL_RESULTS.ESTIMATED_STOCK_OUT%TYPE,    
                     O_min_supply_days_forecast IN OUT   REPL_RESULTS.MIN_SUPPLY_DAYS_FORECAST%TYPE,
                     O_max_supply_days_forecast IN OUT   REPL_RESULTS.MAX_SUPPLY_DAYS_FORECAST%TYPE,
                     O_tsh_forecast             IN OUT   REPL_RESULTS.TIME_SUPPLY_HORIZON_FORECAST%TYPE,
                     O_curr_olt_forecast        IN OUT   REPL_RESULTS.ORDER_LEAD_TIME_FORECAST%TYPE,
                     O_next_olt_forecast        IN OUT   REPL_RESULTS.NEXT_LEAD_TIME_FORECAST%TYPE,
                     O_review_time_forecast     IN OUT   REPL_RESULTS.REVIEW_TIME_FORECAST%TYPE,   
                     O_isd_forecast             IN OUT   REPL_RESULTS.INV_SELL_DAYS_FORECAST%TYPE, 
                     I_item                     IN       REPL_ITEM_LOC.ITEM%TYPE,                  
                     I_locn_type                IN       REPL_ITEM_LOC.LOC_TYPE%TYPE,              
                     I_locn                     IN       REPL_ITEM_LOC.LOCATION%TYPE,              
                     I_sub_item_loc             IN       SUB_ITEMS_HEAD.LOCATION%TYPE, --JMA       
                     I_store_need               IN       ORDLOC.QTY_ORDERED%TYPE,                  
                     I_pres_stock               IN       REPL_ITEM_LOC.PRES_STOCK%TYPE,            
                     I_demo_stock               IN       REPL_ITEM_LOC.DEMO_STOCK%TYPE,            
                     I_repl_method              IN       REPL_ITEM_LOC.REPL_METHOD%TYPE,           
                     I_min_stock                IN       REPL_ITEM_LOC.MIN_STOCK%TYPE,             
                     I_max_stock                IN       REPL_ITEM_LOC.MAX_STOCK%TYPE,             
                     I_incr_pct                 IN       REPL_ITEM_LOC.INCR_PCT%TYPE,              
                     I_min_supply_days          IN       REPL_ITEM_LOC.MIN_SUPPLY_DAYS%TYPE,       
                     I_max_supply_days          IN       REPL_ITEM_LOC.MAX_SUPPLY_DAYS%TYPE,       
                     I_time_supply_horizon      IN       REPL_ITEM_LOC.TIME_SUPPLY_HORIZON%TYPE,   
                     I_inv_selling_days         IN       REPL_ITEM_LOC.INV_SELLING_DAYS%TYPE,      
                     I_service_level            IN       REPL_ITEM_LOC.SERVICE_LEVEL%TYPE,         
                     I_lost_sales_factor        IN       REPL_ITEM_LOC.LOST_SALES_FACTOR%TYPE,     
                     I_curr_order_lead_time     IN       RPL_NET_INVENTORY_TMP.CURR_ORDER_LEAD_TIME%TYPE,
                     I_next_order_lead_time     IN       RPL_NET_INVENTORY_TMP.NEXT_ORDER_LEAD_TIME%TYPE,
                     I_review_lead_time         IN       REPL_ITEM_LOC.PICKUP_LEAD_TIME%TYPE,      
                     I_terminal_stock_qty       IN       REPL_ITEM_LOC.TERMINAL_STOCK_QTY%TYPE,    
                     I_due_ord_serv_basis       IN       SUP_INV_MGMT.DUE_ORD_SERV_BASIS%TYPE,     
                     I_unit_cost                IN       TRAN_DATA.TOTAL_COST%TYPE,                
                     I_unit_retail              IN       TRAN_DATA.TOTAL_RETAIL%TYPE,              
                     I_due_ord_process_ind      IN       SUP_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,    
                     I_repl_results_all_ind     IN       VARCHAR2, 
                     I_season_id                IN       REPL_ITEM_LOC.SEASON_ID%TYPE,             
                     I_phase_id                 IN       REPL_ITEM_LOC.PHASE_ID%TYPE,              
                     I_domain_id                IN       DOMAIN.DOMAIN_ID%TYPE,                    
                     I_reject_store_ord_ind     IN       REPL_ITEM_LOC.REJECT_STORE_ORD_IND%TYPE,  
                     I_date                     IN       DATE,
                     I_last_run_of_the_day      IN       VARCHAR2 DEFAULT 'Y',                                                                     
                     I_last_delivery_date       IN       REPL_ITEM_LOC.LAST_DELIVERY_DATE%TYPE DEFAULT NULL,
                     I_next_delivery_date       IN       REPL_ITEM_LOC.NEXT_DELIVERY_DATE%TYPE DEFAULT NULL) 

RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name        : GET_FORECAST_FOR_PERIOD
-- Purpose              : This function will retrieve the number of units that are 
--                        forecasted for a location within a specified time period.
-- Called by            : Batch (???)
-- Calls                : CAL_TO_454
--------------------------------------------------------------------------------
FUNCTION GET_FORECAST_FOR_PERIOD (O_error_message    IN OUT VARCHAR2,
                                  O_forecast         IN OUT item_forecast.forecast_sales%TYPE,
                                  I_loc              IN     item_forecast.loc%TYPE,
                                  I_first_date       IN     period.vdate%TYPE,
                                  I_item             IN     item_master.item%TYPE,
                                  I_domain_id        IN     domain.domain_id%TYPE,
                                  I_last_date        IN     period.vdate%TYPE,
                                  I_days_of_period   IN     repl_item_loc.wh_lead_time%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name        : GET_ITEM_SUB_FORECAST
-- Purpose              : This function will retrieve all substitute items associated  
--                      : with the passed in item and retrieve and sum the unit forecasts 
--                      : for these items at the passed in location within a specified time period.
-- Called by            : Batch (???)
-- Calls                : GET_REPL_ORDER_QTY_SQL.GET_FORECAST_FOR_PERIOD
--------------------------------------------------------------------------------
FUNCTION GET_ITEM_SUB_FORECAST (O_error_message      IN OUT VARCHAR2,
                                O_total_forecast     IN OUT item_forecast.forecast_sales%TYPE,
                                I_item               IN     item_master.item%TYPE,
                                I_loc                IN     item_forecast.loc%TYPE,
                                I_sub_item_loc       IN     sub_items_head.location%TYPE, --JMA
                                I_sub_forecast_ind   IN     sub_items_head.use_forecast_sales_ind%TYPE,
                                I_first_date         IN     period.vdate%TYPE,
                                I_domain_id          IN     domain.domain_id%TYPE,
                                I_last_date          IN     period.vdate%TYPE,
                                I_days_of_period     IN     repl_item_loc.wh_lead_time%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END GET_REPL_ORDER_QTY_SQL;
/