CREATE OR REPLACE PACKAGE BODY RMSSUB_FULFILORD AS
----------------------------------------------------------------------------
-- Private Procedure
----------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT   VARCHAR2,
                        O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause           IN       VARCHAR2,
                        I_program         IN       VARCHAR2);
----------------------------------------------------------------------------
-- Public Procedure
----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2)
IS

   L_program       VARCHAR2(255) := 'RMSSUB_FULFILORD.CONSUME';
   L_invalid_param VARCHAR2(30)  := NULL;

   L_create_msg "RIB_FulfilOrdDesc_REC" := NULL;
   L_cancel_msg "RIB_FulfilOrdRef_REC"  := NULL;

   L_rib_fulfilordcoldesc_rec "RIB_FulfilOrdColDesc_REC" := NULL;
   L_rib_fulfilorddesc_tbl    "RIB_FulfilOrdDesc_TBL"    := NULL;
   L_rib_fulfilordcolref_rec  "RIB_FulfilOrdColRef_REC"  := NULL;
   L_rib_fulfilordref_tbl     "RIB_FulfilOrdRef_TBL"     := NULL;

   L_process_id      SVC_FULFILORD.PROCESS_ID%TYPE   := NULL;
   L_chunk_id        SVC_FULFILORD.CHUNK_ID%TYPE     := 1;
   L_ordcust_id_TBL  ID_TBL;

   PROGRAM_ERROR     EXCEPTION;

BEGIN

   O_status_code := API_CODES.SUCCESS;

   -- Perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message_type is NULL then
      L_invalid_param := 'I_message_type';
   elsif I_message is NULL then
      L_invalid_param := 'I_message';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      raise PROGRAM_ERROR;
   end if;

   -- Check message type
   if lower(I_message_type) NOT IN (RMSSUB_FULFILORD.MSG_TYPE_CREATE_PO,
                                    RMSSUB_FULFILORD.MSG_TYPE_CREATE_TSF,
                                    RMSSUB_FULFILORD.MSG_TYPE_CREATE_STORE_DELIVERY,
                                    RMSSUB_FULFILORD.MSG_TYPE_CANCEL_APPROVE,
                                    RMSSUB_FULFILORD.MSG_TYPE_CANCEL_REQUEST)then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE',
                                             NVL(lower(I_message_type), 'NULL'));
      raise PROGRAM_ERROR;
   end if;

   if lower(I_message_type) in (RMSSUB_FULFILORD.MSG_TYPE_CREATE_PO,
                                RMSSUB_FULFILORD.MSG_TYPE_CREATE_TSF,
                                RMSSUB_FULFILORD.MSG_TYPE_CREATE_STORE_DELIVERY) then
      ---
      L_create_msg := TREAT(I_message AS "RIB_FulfilOrdDesc_REC");

      if L_create_msg is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      L_rib_fulfilorddesc_tbl := new "RIB_FulfilOrdDesc_TBL" ();
      L_rib_fulfilorddesc_tbl.extend();
      L_rib_fulfilorddesc_tbl(L_rib_fulfilorddesc_tbl.COUNT) := L_create_msg;

      L_rib_fulfilordcoldesc_rec := new "RIB_FulfilOrdColDesc_REC"(0,                        -- rib_oid number
                                                                   1,                        -- collection_size number
                                                                   L_rib_fulfilorddesc_tbl); -- "RIB_FulfilOrdDesc_TBL"

      if STGSVC_FULFILORD.POP_CREATE_TABLES(O_error_message,
                                            L_process_id,
                                            L_rib_fulfilordcoldesc_rec,
                                            CORESVC_FULFILORD.ACTION_TYPE_CREATE) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if CORESVC_FULFILORD.CREATE_FULFILLMENT(O_error_message,
                                              L_ordcust_id_TBL,
                                              L_process_id,
                                              L_chunk_id) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_ordcust_id_TBL is NOT NULL and L_ordcust_id_TBL.count > 0 then
         -- Bulk Insert into ORDCUST_PUB_TEMP
         FORALL i IN 1..L_ordcust_id_TBL.COUNT
            insert into ordcust_pub_temp
            values (L_ordcust_id_TBL(i));

      end if;

      -- Perform Cleanup on Successfully Processed Records
      if STGSVC_FULFILORD.CLEANUP_TABLES (O_error_message,
                                          L_process_id,
                                          CORESVC_FULFILORD.ACTION_TYPE_CREATE) = FALSE then
         raise PROGRAM_ERROR;
      end if;

   -- RMS only needs to process CANCEL_APPROVE message type;
   -- CANCEL_REQUEST message type will be processed by SIM, ignored by RMS
   elsif lower(I_message_type) = RMSSUB_FULFILORD.MSG_TYPE_CANCEL_APPROVE then
      ---
      L_cancel_msg := TREAT(I_message AS "RIB_FulfilOrdRef_REC");

      if L_cancel_msg is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      L_rib_fulfilordref_tbl := new "RIB_FulfilOrdRef_TBL" ();
      L_rib_fulfilordref_tbl.extend();
      L_rib_fulfilordref_tbl(L_rib_fulfilordref_tbl.COUNT) := L_cancel_msg;

      L_rib_fulfilordcolref_rec := new "RIB_FulfilOrdColRef_REC"(0,                       -- rib_oid number
                                                                 1,                       -- collection_size number
                                                                 L_rib_fulfilordref_tbl); -- "RIB_FulfilOrdRef_TBL"

      if STGSVC_FULFILORD.POP_CANCEL_TABLES(O_error_message,
                                            L_process_id,
                                            L_rib_fulfilordcolref_rec,
                                            CORESVC_FULFILORD.ACTION_TYPE_CANCEL) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if CORESVC_FULFILORD.CANCEL_FULFILLMENT(O_error_message,
                                              L_process_id,
                                              L_chunk_id) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- Perform Cleanup on Successfully Processed Records
      if STGSVC_FULFILORD.CLEANUP_TABLES (O_error_message,
                                          L_process_id,
                                          CORESVC_FULFILORD.ACTION_TYPE_CANCEL) = FALSE then
         raise PROGRAM_ERROR;
      end if;

   end if;

   if O_status_code != API_CODES.SUCCESS then
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END CONSUME;
----------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT   VARCHAR2,
                        O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause           IN       VARCHAR2,
                        I_program         IN       VARCHAR2)
IS
   L_program  VARCHAR2(100)  := 'RMSSUB_FULFILORD.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             O_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
END HANDLE_ERRORS;
----------------------------------------------------------------------------
END RMSSUB_FULFILORD;
/
