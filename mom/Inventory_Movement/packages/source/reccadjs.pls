CREATE OR REPLACE PACKAGE REC_COST_ADJ_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------
FUNCTION UPDATE_ORDER_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_unit_cost            IN       ORDLOC.UNIT_COST%TYPE,
                           I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                           I_nwp_item             IN       ORDLOC.ITEM%TYPE,
                           I_order_item           IN       ORDLOC.ITEM%TYPE,
                           I_location             IN       ORDLOC.LOCATION%TYPE,
                           I_loc_type             IN       ORDLOC.LOC_TYPE%TYPE,
                           I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION UPDATE_ORDER_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_unit_cost            IN       ORDLOC.UNIT_COST%TYPE,
                           I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                           I_item                 IN       ORDLOC.ITEM%TYPE,
                           I_location             IN       ORDLOC.LOCATION%TYPE,
                           I_loc_type             IN       ORDLOC.LOC_TYPE%TYPE,
                           I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION UPDATE_SUP_DATA(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_dept          IN       sup_data.dept%TYPE,
                         I_supplier      IN       sup_data.supplier%TYPE,
                         I_tran_date     IN       DATE,
                         I_tran_type     IN       sup_data.tran_type%TYPE,
                         I_amount        IN       sup_data.amount%TYPE)
 RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function: UPD_PACK_ITEM
-- Purpose: Called for a received pack item
--          for which purchase cost has been adjusted.  Does major
--          portions of receiver cost adjustment processing.
---------------------------------------------------------------------
FUNCTION UPD_PACK_ITEM(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_pack_item            IN       ordsku.item%TYPE,
                       I_tran_date            IN       DATE,
                       I_order_no             IN       ordhead.order_no%TYPE,
                       I_pc_old_ord           IN       ordloc.unit_cost%TYPE,
                       I_pc_new_ord           IN       ordloc.unit_cost%TYPE,
                       I_supplier             IN       sups.supplier%TYPE,
                       I_origin_country_id    IN       ordsku.origin_country_id%TYPE,
                       I_import_country_id    IN       ordhead.import_country_id%TYPE,
                       I_exchange_rate_ord    IN       ordhead.exchange_rate%TYPE,
                       I_prim_currency        IN       currencies.currency_code%TYPE,
                       I_order_currency       IN       currencies.currency_code%TYPE,
                       I_alc_status           IN       alc_head.status%TYPE,
                       I_import_order_ind     IN       ordhead.import_order_ind%TYPE,
                       I_import_ind           IN       system_options.import_ind%TYPE,
                       I_elc_ind              IN       system_options.elc_ind%TYPE,
                       I_location             IN       ordloc.location%TYPE,
                       I_loc_type             IN       ordloc.loc_type%TYPE,
                       I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
         RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function:ITEM
-- Purpose:Called for a received item/location
--         for which purchase cost has been adjusted.  Does major
--         portions of receiver cost adjustment processing.
---------------------------------------------------------------------
FUNCTION ITEM(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
              I_item                  IN       ordsku.item%TYPE,
              I_dept                  IN       deps.dept%TYPE,
              I_class                 IN       class.class%TYPE,
              I_subclass              IN       subclass.subclass%TYPE,
              I_tran_date             IN       DATE,
              I_order_no              IN       ordhead.order_no%TYPE,
              I_pc_diff_loc           IN       ordloc.unit_cost%TYPE,
              I_lc_diff_loc           IN       ordloc.unit_cost%TYPE,
              I_qty_ordered           IN       ordloc.qty_received%TYPE,
              I_qty_received          IN       ordloc.qty_received%TYPE,
              I_loc_currency          IN       ordhead.currency_code%TYPE,
              I_prim_currency         IN       currencies.currency_code%TYPE,
              I_avg_cost_new          IN       item_loc_soh.av_cost%TYPE,
              I_neg_soh_wac_adj_amt   IN       item_loc_soh.av_cost%TYPE,
              I_loc_type              IN       inv_status_qty.loc_type%TYPE,
              I_location              IN       inv_status_qty.location%TYPE,
              I_supplier              IN       sups.supplier%TYPE,
              I_elc_ind               IN       system_options.elc_ind%TYPE,
              I_import_ind            IN       system_options.import_ind%TYPE,
              I_alc_status            IN       alc_head.status%TYPE,
              I_adj_qty               IN       item_loc_soh.stock_on_hand%TYPE DEFAULT NULL,
              I_new_po_cost           IN       ordloc.unit_cost%TYPE,
              I_old_po_cost           IN       ordloc.unit_cost%TYPE,
              I_new_cost_excl_elc     IN       ordloc.unit_cost%TYPE,
              I_old_cost_excl_elc     IN       ordloc.unit_cost%TYPE,
              I_adjust_matched_ind    IN       VARCHAR2 DEFAULT 'N')

RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function: CHECK_DEALS
-- Purpose : Checks for the existence of deals for the passed in item/
--           location/order.  It returns the min and max cost.
---------------------------------------------------------------------
FUNCTION CHECK_DEALS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_min_unit_cost   IN OUT   ordloc_invc_cost.unit_cost%TYPE,
                     O_max_unit_cost   IN OUT   ordloc_invc_cost.unit_cost%TYPE,
                     I_order_no        IN       ordhead.order_no%TYPE,
                     I_item            IN       ordsku.item%TYPE,
                     I_location        IN       ordloc_invc_cost.location%TYPE)
         RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function: CALC_DEAL_AVG_COST
-- Purpose : Calculates the average deal cost for a buy/get deal.
--------------------------------------------------------------------------------
FUNCTION CALC_DEAL_AVG_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_avg_cost        IN OUT   ordloc.unit_cost%TYPE,
                            I_order_no        IN       ordhead.order_no%TYPE,
                            I_item            IN       ordsku.item%TYPE,
                            I_location        IN       ordloc_invc_cost.location%TYPE,
                            I_total_order_qty IN       ordloc.qty_ordered%TYPE,
                            I_buy_cost        IN       ordloc_invc_cost.unit_cost%TYPE,
                            I_get_cost        IN       ordloc_invc_cost.unit_cost%TYPE)
         RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function: UPDATE_DEALS
-- Purpose : Updates deals based on a cost adjustment.
----------------------------------------------------------------------------------
FUNCTION UPDATE_DEALS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ordhead.order_no%TYPE,
                      I_item            IN       ordsku.item%TYPE,
                      I_location        IN       ordloc.location%TYPE,
                      I_old_cost_1      IN       ordloc.unit_cost%TYPE,
                      I_new_cost_1      IN       ordloc.unit_cost%TYPE,
                      I_old_cost_2      IN       ordloc.unit_cost%TYPE,
                      I_new_cost_2      IN       ordloc.unit_cost%TYPE)
         RETURN BOOLEAN;

---------------------------------------------------------------------------------
-- Function: APPLY_TO_ALL_LOCS
-- Purpose : Updates all locations based on the item and order number.
----------------------------------------------------------------------------------
FUNCTION APPLY_TO_ALL_LOCS( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                            I_dept                 IN       DEPS.DEPT%TYPE,
                            I_class                IN       CLASS.CLASS%TYPE,
                            I_subclass             IN       SUBCLASS.SUBCLASS%TYPE,
                            I_chg_amt_cost         IN       ORDLOC.UNIT_COST%TYPE,
                            I_chg_amt_ord          IN       ORDLOC.UNIT_COST%TYPE,
                            I_sum_qty_received     IN       ORDLOC.QTY_RECEIVED%TYPE,
                            I_tran_date            IN       TRAN_DATA.TRAN_DATE%TYPE,
                            I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                            I_local_currency       IN       ORDHEAD.CURRENCY_CODE%TYPE,
                            I_primary_currency     IN       CURRENCIES.CURRENCY_CODE%TYPE,
                            I_supplier             IN       SUPS.SUPPLIER%TYPE,
                            I_elc_ind              IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                            I_import_ind           IN       SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                            I_alc_status           IN       ALC_HEAD.STATUS%TYPE,
                            I_tran_level           IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                            I_item_level           IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                            I_ord_currency         IN       ORDHEAD.CURRENCY_CODE%TYPE,
                            I_exchange_rate_ord    IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                            I_origin_country_id    IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                            I_import_country_id    IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                            I_pack_ind             IN       ITEM_MASTER.PACK_IND%TYPE,
                            I_import_order_ind     IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                            I_import_id            IN       ORDHEAD.IMPORT_ID%TYPE DEFAULT NULL,
                            I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
          RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function:ADJUST_LINE_ITEM_COST
-- Purpose:Called for a received item/location
--         This function does the receiver cost adjustment processing used by REIM.
--------------------------------------------------------------------------------------
FUNCTION ADJUST_LINEITEM_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item                 IN       ordsku.item%TYPE,
                              I_order_no             IN       ordhead.order_no%TYPE,
                              I_adj_unit_cost        IN       ordloc.unit_cost%TYPE ,
                              I_adj_curr_code        IN       ordhead.currency_code%TYPE,
                              I_location             IN       inv_status_qty.location%TYPE,
                              I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
         RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function:CALC_ELC
-- Purpose:Called to calculate total elc if any alc record has been finalized for
--         the order/shipment.
--------------------------------------------------------------------------------------
FUNCTION CALC_ELC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_total_elc           IN OUT   NUMBER,
                  I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                  I_item                IN       ITEM_MASTER.ITEM%TYPE,
                  I_comp_item           IN       ITEM_MASTER.ITEM%TYPE,
                  I_location            IN       ORDLOC.LOCATION%TYPE, 
                  I_supplier            IN       SUPS.SUPPLIER%TYPE,
                  I_origin_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                  I_import_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                  I_cost                IN       ORDLOC.UNIT_COST%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION FM_COST_ADJUSTMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                           I_ref_doc_no       IN       SHIPMENT.REF_DOC_NO%TYPE,
                           I_item             IN       SHIPSKU.ITEM%TYPE,
                           I_old_ebc          IN       SHIPSKU.UNIT_COST%TYPE,
                           I_new_ebc          IN       SHIPSKU.UNIT_COST%TYPE,
                           I_new_nic          IN       SHIPSKU.UNIT_COST%TYPE)
               RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION FM_RECEIPT_MATCHED(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_ref_doc_no      IN       SHIPMENT.REF_DOC_NO%TYPE)
               RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION GET_VIR_UNMATCHED_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_vir_qty         IN OUT   SHIPSKU.QTY_MATCHED%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE,
                               I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                               I_shipment        IN       SHIPSKU.SHIPMENT%TYPE,
                               I_seq_no          IN       SHIPSKU.SEQ_NO%TYPE,
                               I_phy_loc         IN       ORDLOC.LOCATION%TYPE,
                               I_vir_loc         IN       ORDLOC.LOCATION%TYPE,
                               I_phy_qty         IN       SHIPSKU.QTY_MATCHED%TYPE)
               RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function:GET_PO_UNMATCHED_QTY
-- Purpose: This function will return the shipment qty_received when the 
--          adjust matched receipts indicator is passed in as Y . Otherwise it
--          will return the unmatched qty.
--------------------------------------------------------------------------------------
FUNCTION GET_PO_UNMATCHED_QTY(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_unmatched_qty        IN OUT   SHIPSKU.QTY_MATCHED%TYPE,
                              I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                              I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                              I_location             IN       ORDLOC.LOCATION%TYPE,
                              I_loc_type             IN       ORDLOC.LOC_TYPE%TYPE,
                              I_adjust_matched_ind   IN       VARCHAR2 DEFAULT 'N')
               RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION CALC_NEW_AV_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_new_wac_loc           IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                          O_neg_soh_wac_adj_amt   IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                          O_adj_qty               IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_avg_cost_old          IN       ITEM_LOC_SOH.AV_COST%TYPE,
                          I_total_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_new_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                          I_old_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                          I_qty_matched           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                          I_order_number          IN       ORDHEAD.ORDER_NO%TYPE DEFAULT NULL,
                          I_location              IN       ORDLOC.LOCATION%TYPE,
                          I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE,
                          I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                          I_adjust_matched_ind    IN       VARCHAR2 DEFAULT 'N')
               RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function: GET_ITEM_LOC_QTY_COST
-- Purpose: Called for each row to get Quantity and Cost information for a given Item & Location.
---------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC_QTY_COST (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_avg_cost_old          IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                                O_on_hand               IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                O_comp_on_hand          IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                O_in_transit            IN OUT   ITEM_LOC_SOH.IN_TRANSIT_QTY%TYPE,
                                O_qty_unmatched         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                O_qty_expected          IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                O_local_currency        IN OUT   ORDHEAD.CURRENCY_CODE%TYPE,
                                O_pur_cost_old_loc      IN OUT   ORDLOC.UNIT_COST%TYPE,
                                O_landed_cost_old_loc   IN OUT   ORDLOC.UNIT_COST%TYPE,
                                O_physical_wh           IN OUT   INV_STATUS_QTY.LOCATION%TYPE,       
                                I_item                  IN       ORDSKU.ITEM%TYPE,
                                I_loc_type              IN       INV_STATUS_QTY.LOC_TYPE%TYPE,
                                I_location              IN       INV_STATUS_QTY.LOCATION%TYPE,
                                I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                                I_supplier              IN       SUPS.SUPPLIER%TYPE,
                                I_unit_cost             IN       ORDLOC.UNIT_COST%TYPE, 
                                I_pack_ind              IN       ITEM_MASTER.PACK_IND%TYPE,
                                I_prim_currency         IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                I_ord_currency          IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                I_exchange_rate         IN       ORDHEAD.EXCHANGE_RATE%TYPE, 
                                I_alc_status            IN       ALC_HEAD.STATUS%TYPE,
                                I_origin_country_id     IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                I_import_country_id     IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                                I_import_ind            IN       SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                                I_elc_ind               IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                                I_adj_matched_rcpt      IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function:UPD_NEW_COST
-- Purpose:Called when the purchase cost is adjusted for an item that has already been received.
-- This updates the deals and order cost based on a cost adjustment 
-- and returns the cost in the location's currency.
---------------------------------------------------------------------  
FUNCTION UPD_NEW_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_pc_new_loc          IN OUT   ORDLOC.UNIT_COST%TYPE,  
                      O_lc_new_loc          IN OUT   ORDLOC.UNIT_COST%TYPE,
                      I_item                IN       ORDSKU.ITEM%TYPE,
                      I_loc_type            IN       INV_STATUS_QTY.LOC_TYPE%TYPE,
                      I_location            IN       INV_STATUS_QTY.LOCATION%TYPE,
                      I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                      I_supplier            IN       SUPS.SUPPLIER%TYPE,
                      I_dept                IN       DEPS.DEPT%TYPE,
                      I_local_currency      IN       ORDHEAD.CURRENCY_CODE%TYPE,
                      I_order_currency      IN       ORDHEAD.CURRENCY_CODE%TYPE,
                      I_prim_currency       IN       ORDHEAD.CURRENCY_CODE%TYPE,
                      I_order_as_type       IN       ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                      I_new_cost_ord        IN       ORDLOC.UNIT_COST%TYPE, 
                      I_old_cost_ord        IN       ORDLOC.UNIT_COST%TYPE, 
                      I_pack_ind            IN       ITEM_MASTER.PACK_IND%TYPE,
                      I_pack_type           IN       ITEM_MASTER.PACK_TYPE%TYPE,
                      I_item_level          IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                      I_tran_level          IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                      I_exchange_rate       IN       ORDHEAD.EXCHANGE_RATE%TYPE, 
                      I_alc_status          IN       ALC_HEAD.STATUS%TYPE,
                      I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                      I_import_country_id   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                      I_import_ord_ind      IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                      I_import_ind          IN       SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                      I_elc_ind             IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                      I_qty_received        IN       V_ORDLOC.QTY_RECEIVED%TYPE,
                      I_physical_wh         IN       WH.WH%TYPE,
                      I_adj_matched_rcpt    IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function:CREATE_SUPP_COST_CHANGE
-- Purpose: Creates a supplier cost change. 
---------------------------------------------------------------------
FUNCTION CREATE_SUPP_COST_CHANGE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item               IN       ORDSKU.ITEM%TYPE,
                                 I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                                 I_supplier           IN       SUPS.SUPPLIER%TYPE,
                                 I_new_cost_ord       IN       ORDLOC.UNIT_COST%TYPE,
                                 I_order_currency     IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                 I_local_currency     IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                 I_exchange_rate      IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                                 I_loc_type           IN       INV_STATUS_QTY.LOC_TYPE%TYPE,
                                 I_location           IN       INV_STATUS_QTY.LOCATION%TYPE,
                                 I_physical_wh        IN       INV_STATUS_QTY.LOCATION%TYPE,
                                 I_apply_to_all_loc   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function:PURGE_COMP_ITEM_ELC_TEMP
-- Purpose: Purges the temp table. 
--------------------------------------------------------------------------------------
FUNCTION PURGE_COMP_ITEM_ELC_TEMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
END REC_COST_ADJ_SQL;
/
