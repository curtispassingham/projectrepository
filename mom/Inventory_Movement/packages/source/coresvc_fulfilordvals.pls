CREATE OR REPLACE PACKAGE CORESVC_FULFILORD_VALIDATE AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------
-- Name   : CHECK_MESSAGE_CREATE
-- Purpose: This is a public function that performs basic validation of staged customer order
--          fulfillment creation requests on SVC_FULFILORD. It should process all messages
--          on the staging table for a given I_process_id and I_chunk_id in 'N'ot processed status.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE_CREATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : CHECK_MESSAGE_CANCEL
-- Purpose: This is a public function that performs basic validation of staged customer order
--          fulfillment cancellation requests on SVC_FULFULORDREF. It should process all messages
--          on the staging table for a given I_process_id and I_chunk_id in 'N'ot processed status.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE_CANCEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END CORESVC_FULFILORD_VALIDATE;
/