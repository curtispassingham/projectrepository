
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ALLOC_VALIDATE_SQL AUTHID CURRENT_USER AS
  
-------------------------------------------------------------------
-- Name:      EXIST
-- Purpose:   To check if a given allocation number exists
--		  on the alloc_header table
-- Created By: 27-Aug-96 by Matt Sniffen
-------------------------------------------------------------------
FUNCTION EXIST(	O_error_message	IN OUT	VARCHAR2,
		I_alloc_no	IN	NUMBER,
		O_exist		IN OUT	BOOLEAN)
	RETURN BOOLEAN;

END ALLOC_VALIDATE_SQL;
/
