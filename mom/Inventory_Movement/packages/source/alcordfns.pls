CREATE OR REPLACE PACKAGE ALC_ORDER_FINALIZE_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: WHERE_CLAUSE
-- Purpose      : This function will be used to format a where clause for use within the
--                ALC Order Find form (alcofind.fmb) and the ALC Order Finalization form (alcordfn.fmb).
---------------------------------------------------------------------------------------------
FUNCTION WHERE_CLAUSE (O_error_message       IN OUT VARCHAR2,
                       O_out_where_clause    IN OUT VARCHAR2,
                       I_start_variance_pct  IN     NUMBER,
                       I_end_variance_pct    IN     NUMBER)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: ALC_FINALIZE
-- Purpose      : Update the stock ledger and finalize the ALC for an order.
---------------------------------------------------------------------------------------------
FUNCTION ALC_FINALIZE (O_error_message      IN OUT VARCHAR2,
                       I_order_no           IN     ORDHEAD.ORDER_NO%TYPE,
                       I_shipment           IN     SHIPMENT.SHIPMENT%TYPE,
                       I_update_wac_ind     IN     VARCHAR2,
                       I_status             IN     ALC_HEAD.STATUS%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_ALC_STATUS
-- Purpose      : Return the ALC status for an order.
---------------------------------------------------------------------------------------------
FUNCTION GET_ALC_STATUS (O_error_message    IN OUT VARCHAR2,
                         O_alc_status       IN OUT ALC_HEAD.STATUS%TYPE,
                         I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                         I_shipment         IN     SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ALC_STATUS
-- Purpose      : Update the ALC status on the alc_head table to match that status on the
--                ce_ord_item table.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ALC_STATUS (O_error_message    IN OUT VARCHAR2,
                            I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                            I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                            I_status           IN     ALC_HEAD.STATUS%TYPE,
                            I_alc_seq_no       IN     ALC_HEAD.SEQ_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INSERT_ALC_HEAD_TEMP
-- Purpose:       Insert records into the alc_head_temp table.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ALC_HEAD_TEMP (O_error_message        IN OUT  VARCHAR2,
                               I_from_var             IN      NUMBER,
                               I_to_var               IN      NUMBER,
                               I_order_no             IN      ORDHEAD.ORDEr_NO%TYPE,
                               I_asn                  IN      SHIPMENT.ASN%TYPE,
                               I_item                 IN      V_ALC_HEAD.ITEM%TYPE,
                               I_pack_item            IN      V_ALC_HEAD.PACK_ITEM%TYPE,
                               I_obligation_key       IN      V_ALC_HEAD.OBLIGATION_KEY%TYPE,
                               I_vessel_id            IN      V_ALC_HEAD.VESSEL_ID%TYPE,
                               I_voyage_flt_id        IN      V_ALC_HEAD.VOYAGE_FLT_ID%TYPE,
                               I_estimated_depart     IN      V_ALC_HEAD.ESTIMATED_DEPART_DATE%TYPE,
                               I_alc_status           IN      V_ALC_HEAD.PO_ALC_STATUS%TYPE,
                               I_entry_no             IN      CE_HEAD.ENTRY_NO%TYPE,
                               I_order_status         IN      ORDHEAD.STATUS%TYPE,
                               I_mode                 IN      VARCHAR2,
                               O_where_clause         IN OUT  VARCHAR2,
                               O_rec_inserted         IN OUT  BOOLEAN)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_ALC_HEAD_TEMP
-- Purpose:       Deletes records from the alc_head_temp table.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_ALC_HEAD_TEMP(O_error_message        IN OUT    VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: NO_FINALIZATION
-- Purpose:       Update the ALC status of the PO from 'P'ending to 'N'o Finalization
---------------------------------------------------------------------------------------------
FUNCTION NO_FINALIZATION (O_error_message    IN OUT VARCHAR2,
                          I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                          I_shipment         IN     SHIPMENT.SHIPMENT%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END ALC_ORDER_FINALIZE_SQL;
/
