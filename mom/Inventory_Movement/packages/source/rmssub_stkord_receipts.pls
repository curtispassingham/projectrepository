
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_STKORD_RECEIPT AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
FUNCTION CONSUME (O_status_code          IN OUT  VARCHAR2,
                  O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  I_appt                 IN      APPT_HEAD.APPT%TYPE,
                  I_rib_receipt_rec      IN      "RIB_Receipt_REC")
return BOOLEAN;
--------------------------------------------------------------------------------
END RMSSUB_STKORD_RECEIPT;
/
