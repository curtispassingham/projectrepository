
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE REPL_ATTRIBUTE_UPDATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
-- Function  : CREATE_NEW_MRA
-- Purpose   : This package will be called to create a new MRA record.
--             The function will simply do a select statement from repl_item_loc
--             and repl_day  where the item and the location has been passed in
--             and insert the same values into master_repl_attr. The function will need to
--             'translate' the values in repl_day, if for instance, there is a Monday record
--             in repl_day then Monday_ind on master_repl_attr will be set to 'Y'.
-- Called by : Replenishment Attribute Maintenance form (rplattr.fmb)
-- Author    : Margarita B. Carino (Accenture - ERC)
-------------------------------------------------------------------------------------------
FUNCTION CREATE_NEW_MRA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN       REPL_ITEM_LOC.ITEM%TYPE,
                        I_location        IN       REPL_ITEM_LOC.LOCATION%TYPE,
                        I_loc_type        IN       REPL_ITEM_LOC.LOC_TYPE%TYPE
                        )
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function  : UPDATE_MRA
-- Purpose   : This package will be called to update MRA records.
--             This is a new function that takes in the item and location and updates the 
--             corresponding record in the master_repl_attr table. This will be called by the 
--             form if the Update Master checkbox is checked. If the parent item is updated, 
--             it will update the children as well.
-------------------------------------------------------------------------------------------
FUNCTION UPDATE_MRA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item            IN       REPL_ITEM_LOC.ITEM%TYPE,
                    I_location        IN       REPL_ITEM_LOC.LOCATION%TYPE,
                    I_loc_type        IN       REPL_ITEM_LOC.LOC_TYPE%TYPE
                    )
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
END REPL_ATTRIBUTE_UPDATE_SQL;
/
