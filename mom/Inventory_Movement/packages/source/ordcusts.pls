CREATE OR REPLACE PACKAGE ORDCUST_ATTRIB_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------
-- Function Name  : PERSIST_ORDCUST_L10N_EXT
-- Purpose        : This is a public function that persists data to ORDCUST_L10N_EXT table
--                  for the input collection through the decoupling layer.
----------------------------------------------------------------------------------------------
FUNCTION PERSIST_ORDCUST_L10N_EXT(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_l10n_ordcust_ext_tbl   IN OUT   L10N_ORDCUST_EXT_TBL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
END ORDCUST_ATTRIB_SQL;
/
