
SET FEEDBACK ON
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY TSFSOBS_SQL AS
--------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (IO_tsf_entity_org_unit_sob_tbl IN OUT  TSFSOBS_SQL.TSF_ENTITY_ORG_UNIT_SOB_TBL,
                           I_tsf_entity                   IN      TSF_ENTITY_ORG_UNIT_SOB.TSF_ENTITY_ID%TYPE) 
IS

   cursor C_TSF_EOU_SOB is
      select tous.org_unit_id,
             ou.description,
             tous.set_of_books_id,
             fgs.set_of_books_desc,
             'TRUE' return_code,
             NULL error_message
        from tsf_entity_org_unit_sob tous,
             v_org_unit_tl ou,
             fif_gl_setup fgs
       where tous.tsf_entity_id = I_tsf_entity
         and tous.org_unit_id = ou.org_unit_id
         and tous.set_of_books_id = fgs.set_of_books_id;
BEGIN

   if I_tsf_entity IS NOT NULL then
      open C_TSF_EOU_SOB;
      fetch C_TSF_EOU_SOB BULK COLLECT into IO_tsf_entity_org_unit_sob_tbl;
      close C_TSF_EOU_SOB;

   else
      IO_tsf_entity_org_unit_sob_tbl(1).return_code   := 'FALSE';
      IO_tsf_entity_org_unit_sob_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                            'I_tsf_entity',
                                                                            'TSFSOBS_SQL.QUERY_PROCEDURE',
                                                                            'NULL');
   end if;
EXCEPTION
   when OTHERS then
      IO_tsf_entity_org_unit_sob_tbl(1).return_code   := 'FALSE';
      IO_tsf_entity_org_unit_sob_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                             SQLERRM,
                                                                             'TSFSOBS_SQL.QUERY_PROCEDURE',
                                                                             to_char(SQLCODE));

END QUERY_PROCEDURE;
--------------------------------------------------------------------------------------------------------------
PROCEDURE DELETE_PROCEDURE (IO_tsf_entity_org_unit_sob_tbl IN OUT TSFSOBS_SQL.TSF_ENTITY_ORG_UNIT_SOB_TBL,
                            I_tsf_entity                   IN     TSF_ENTITY_ORG_UNIT_SOB.TSF_ENTITY_ID%TYPE) 
IS
BEGIN

   for i in 1..IO_tsf_entity_org_unit_sob_tbl.COUNT
   loop
      delete from tsf_entity_org_unit_sob
            where org_unit_id     = IO_tsf_entity_org_unit_sob_tbl(i).org_unit_id
              and set_of_books_id = IO_tsf_entity_org_unit_sob_tbl(i).set_of_books_id
              and tsf_entity_id   = I_tsf_entity;
   end loop;

END DELETE_PROCEDURE;
--------------------------------------------------------------------------------------------------------------
PROCEDURE INSERT_PROCEDURE (IO_tsf_entity_org_unit_sob_tbl IN OUT TSFSOBS_SQL.TSF_ENTITY_ORG_UNIT_SOB_TBL,
                            I_tsf_entity                   IN     TSF_ENTITY_ORG_UNIT_SOB.TSF_ENTITY_ID%TYPE)
IS
BEGIN

   for i in 1..IO_tsf_entity_org_unit_sob_tbl.COUNT
   loop
      insert into tsf_entity_org_unit_sob (tsf_entity_id,
                                           org_unit_id,
                                           set_of_books_id)
                                   values (I_tsf_entity,
                                           IO_tsf_entity_org_unit_sob_tbl(i).org_unit_id,
                                           IO_tsf_entity_org_unit_sob_tbl(i).set_of_books_id);
   end loop;

END INSERT_PROCEDURE;
--------------------------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE (IO_tsf_entity_org_unit_sob_tbl IN OUT TSFSOBS_SQL.TSF_ENTITY_ORG_UNIT_SOB_TBL,
                          I_tsf_entity                   IN     TSF_ENTITY_ORG_UNIT_SOB.TSF_ENTITY_ID%TYPE)
IS
   L_exists   VARCHAR2(1);
BEGIN

   for i in 1..IO_tsf_entity_org_unit_sob_tbl.COUNT
   loop
      select 'x'
        into L_exists
        from tsf_entity_org_unit_sob
       where org_unit_id     = IO_tsf_entity_org_unit_sob_tbl(i).org_unit_id
         and set_of_books_id = IO_tsf_entity_org_unit_sob_tbl(i).set_of_books_id
         and tsf_entity_id   = I_tsf_entity
         for update nowait;
   end loop;

END LOCK_PROCEDURE;
------------------------------------------------------------------------------
END TSFSOBS_SQL;
/
