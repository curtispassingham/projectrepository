CREATE OR REPLACE PACKAGE DISTRIBUTION_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
TYPE DIST_REC_TYPE IS RECORD
(
wh         NUMBER,
from_loc   NUMBER,
to_loc     NUMBER,
dist_qty   NUMBER
);

TYPE DIST_TABLE_TYPE IS TABLE OF DIST_REC_TYPE
INDEX BY BINARY_INTEGER;

TYPE INV_FLOW_REC IS RECORD(
   phy_loc             ITEM_LOC.LOC%TYPE,
   loc                 ITEM_LOC.LOC%TYPE,
   loc_type            ITEM_LOC.LOC_TYPE%TYPE,

   channel_id          CHANNELS.CHANNEL_ID%TYPE,
   channel_type        CHANNELS.CHANNEL_TYPE%TYPE,

   primary_vwh         WH.PRIMARY_VWH%TYPE,
   protected_ind       WH.PROTECTED_IND%TYPE,
   restricted_ind      WH.RESTRICTED_IND%TYPE,

   org_unit_id         WH.ORG_UNIT_ID%TYPE,
   tsf_entity_id       WH.TSF_ENTITY_ID%TYPE,
   
   status              ITEM_LOC.STATUS%TYPE,
   receive_as_type     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,

   qty                 ITEM_LOC_SOH.STOCK_ON_HAND%TYPE
);
TYPE INV_FLOW_ARRAY IS TABLE OF INV_FLOW_REC
 INDEX BY BINARY_INTEGER;

--------------------------------------------------------------------------------
-- Function Name: DISTRIBUTE
-- Purpose      : This function calls other private functions that perform the
--                inventory distribution logic. For transfers, it supports
--                customer order locations when distributing physical warehouse
--                quantities.
--------------------------------------------------------------------------------
FUNCTION DISTRIBUTE(O_error_message        IN OUT   VARCHAR2,
                    O_dist_tab             IN OUT   DIST_TABLE_TYPE,
                    I_item                 IN       ITEM_LOC.ITEM%TYPE,
                    I_loc                  IN       ITEM_LOC.LOC%TYPE,
                    I_qty                  IN       NUMBER,
                    I_CMI                  IN       VARCHAR2,
                    I_inv_status           IN       INV_STATUS_TYPES.INV_STATUS%TYPE,
                    I_to_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                    I_to_loc               IN       ITEM_LOC.LOC%TYPE,
                    I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                    I_shipment             IN       SHIPITEM_INV_FLOW.SHIPMENT%TYPE,
                    I_seq_no               IN       SHIPITEM_INV_FLOW.SEQ_NO%TYPE,
                    I_cycle_count          IN       STAKE_SKU_LOC.CYCLE_COUNT%TYPE DEFAULT NULL,
                    I_rtv_order_no         IN       RTV_HEAD.RTV_ORDER_NO%TYPE DEFAULT NULL,
                    I_rtv_seq_no           IN       RTV_DETAIL.SEQ_NO%TYPE DEFAULT NULL,
                    I_tsf_no               IN       TSFHEAD.TSF_NO%TYPE DEFAULT NULL,
                    I_tsf_create_ind       IN       VARCHAR2 DEFAULT 'N',
                    I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE DEFAULT 'N',
                    I_alloc_no             IN       ALLOC_HEADER.ALLOC_NO%TYPE DEFAULT NULL)

RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_EG_TSF_DIST_VWH
-- Purpose      : Retrieves the distribution virtual warehouse associated with 
--                an externally generated transfer from a store                 
--                to a (physical) warehouse. EG transfers will always be associated
--                with physical locations. For store to warehouse EG tsfs, merchandise
--                is only distributed to the vwh that shares a channel_id with the sending
--                store. For any physical warehouse, there can only be one non-finisher vwh 
--                associated a particular channel. So this function assumes the presence of
--                one and only one distribution vwh. 
--                This vwh will be used by tsfdetail.fmb to retrieve the 'to loc' cost
--                and retail for an item. The form needs to use the transfer's distribution
--                vwh because it will always have an item_loc record. 
--                (An item_loc record may not exist for the physical warehouse's primary vwh.)
---------------------------------------------------------------------------------------------
FUNCTION GET_EG_TSF_DIST_VWH(O_error_message   IN OUT   VARCHAR2,
                             O_dist_vwh        IN OUT   WH.WH%TYPE,
                             I_tsf_no          IN       TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: FIND_MAPPING_VWH
-- Purpose      : This overloaded function calls the public function without the 
--                ITEM parameter to find the mapping virtual warehouse within the
--                physical warehouse.   
--------------------------------------------------------------------------------
FUNCTION FIND_MAPPING_VWH(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_virtual_wh           IN OUT   WH.WH%TYPE,
                          I_phy_wh               IN       WH.WH%TYPE,
                          I_to_from_ind          IN       VARCHAR2,
                          I_other_loc            IN       ITEM_LOC.LOC%TYPE,
                          I_other_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_item                 IN       ITEM_LOC.ITEM%TYPE,
                          I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: FIND_MAPPING_VWH
-- Purpose      : This function identifies one virtual warehouse within the physical
--                warehouse that will be used to create a transfer or adjust inventory
--                when the physical warehouse is a destination location.
--------------------------------------------------------------------------------
FUNCTION FIND_MAPPING_VWH(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_virtual_wh           IN OUT   WH.WH%TYPE,
                          I_phy_wh               IN       WH.WH%TYPE,
                          I_to_from_ind          IN       VARCHAR2,
                          I_other_loc            IN       ITEM_LOC.LOC%TYPE,
                          I_other_loc_type       IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_cust_order_loc_ind   IN       WH.CUSTOMER_ORDER_LOC_IND%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END DISTRIBUTION_SQL;
/
