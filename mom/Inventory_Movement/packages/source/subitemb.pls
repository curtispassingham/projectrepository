CREATE OR REPLACE PACKAGE BODY SUB_ITEM_SQL AS
   LP_rejected_records  BOOLEAN;
   TYPE locs_table_type IS
      TABLE OF ITEM_LOC.LOC%TYPE INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------
FUNCTION GET_REPL_ITEM(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_repl_item_exists IN OUT  BOOLEAN,
                       O_repl_item        IN OUT  REPL_ITEM_LOC.ITEM%TYPE,
                       I_repl_pack        IN      REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE)
RETURN BOOLEAN IS
   cursor C_GET_REPL_ITEM is
      select item
        from repl_item_loc
       where primary_pack_no = I_repl_pack;

BEGIN
   O_repl_item_exists := TRUE; -- initialize to default value
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_REPL_ITEM', 'REPL_ITEM_LOC', NULL);
    open C_GET_REPL_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_PRIM_REPL_PACK', 'REPL_ITEM_LOC', NULL);
   fetch C_GET_REPL_ITEM into O_repl_item;
   ---
   if C_GET_REPL_ITEM%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_PRIM_REPL_PACK', 'REPL_ITEM_LOC', NULL);
      O_repl_item_exists := FALSE;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_PRIM_REPL_PACK', 'REPL_ITEM_LOC', NULL);
      close C_GET_REPL_ITEM;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_PRIM_REPL_PACK', 'REPL_ITEM_LOC', NULL);
   close C_GET_REPL_ITEM;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.GET_MAIN_ITEM',
                                            to_char(SQLCODE));
      return FALSE;
END GET_REPL_ITEM;
---------------------------------------------------------------------------------------------------
FUNCTION GET_PRIM_REPL_PACK(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_prim_repl_pack  IN OUT  REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE,
                            I_item            IN      REPL_ITEM_LOC.ITEM%TYPE,
                            I_location        IN      REPL_ITEM_LOC.LOCATION%TYPE,
                            I_loc_type        IN      REPL_ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS
   cursor C_GET_PRIM_REPL_PACK is
      select primary_pack_no
        from repl_item_loc
       where item     = I_item
         and location = I_location
         and loc_type = I_loc_type;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_loc_type',
                                            'NULL',
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_PRIM_REPL_PACK', 'REPL_ITEM_LOC', NULL);
   open C_GET_PRIM_REPL_PACK;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_PRIM_REPL_PACK', 'REPL_ITEM_LOC', NULL);
   fetch C_GET_PRIM_REPL_PACK into O_prim_repl_pack;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_PRIM_REPL_PACK', 'REPL_ITEM_LOC', NULL);
   close C_GET_PRIM_REPL_PACK;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.GET_PRIM_REPL_PACK',
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIM_REPL_PACK;
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_REPL_IND(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_replenishable   IN OUT  BOOLEAN,
                        I_wh_group_type   IN      CODE_DETAIL.CODE%TYPE,
                        I_wh_group_value  IN      WH.WH%TYPE)
RETURN BOOLEAN IS
   L_dummy   VARCHAR2(1);
   ---
   cursor C_WH is
      select 'x'
        from wh
       where wh       = I_wh_group_value
         and repl_ind = 'Y';

   cursor C_LOC_LIST_WH is
      select 'x'
        from loc_list_detail l, wh w
       where l.loc_list = to_number(I_wh_group_value)
         and l.loc_type = 'W'
         and l.location = w.wh
         and w.repl_ind = 'Y';

BEGIN
   O_replenishable := TRUE;   -- initialize default output
   ---
   if I_wh_group_type = 'W' then  -- check if passed-in Warehouse is replenishable
      SQL_LIB.SET_MARK('OPEN', 'C_WH', 'WH', NULL);
       open C_WH;
      SQL_LIB.SET_MARK('FETCH', 'C_WH', 'WH', NULL);
      fetch C_WH into L_dummy;
      ---
      if C_WH%NOTFOUND then  -- Warehouse isn't replenishable
         O_replenishable := FALSE;
         SQL_LIB.SET_MARK('CLOSE', 'C_WH', 'WH', NULL);
         close C_WH;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_WH', 'WH', NULL);
      close C_WH;
   elsif I_wh_group_type = 'LLW' then  -- check if any Warehouse on passed-in Location List is replenishable
      SQL_LIB.SET_MARK('OPEN', 'C_LOC_LIST_WH', 'WH', NULL);
       open C_LOC_LIST_WH;
      SQL_LIB.SET_MARK('FETCH', 'C_LOC_LIST_WH', 'WH', NULL);
      fetch C_LOC_LIST_WH into L_dummy;
      ---
      if C_LOC_LIST_WH%NOTFOUND then -- none of the listed Warehouses are replenishable
         O_replenishable := FALSE;
         SQL_LIB.SET_MARK('CLOSE', 'C_LOC_LIST_WH', 'WH', NULL);
         close C_LOC_LIST_WH;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_LOC_LIST_WH', 'WH', NULL);
      close C_LOC_LIST_WH;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.CHECK_REPL_IND',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_REPL_IND;
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_MAIN_CONFLICT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists         IN OUT  BOOLEAN,
                                 I_item           IN      SUB_ITEMS_HEAD.ITEM%TYPE,
                                 I_item_ind       IN      VARCHAR2,
                                 I_location       IN      SUB_ITEMS_HEAD.LOCATION%TYPE)
RETURN BOOLEAN IS
   L_dummy     VARCHAR2(1);
   L_function  VARCHAR2(60)                      := 'SUB_ITEMS_SQL.CHECK_MAIN_CONFLICT';
   ---
   cursor C_CHECK_MAIN_CONFLICT is
      select 'x'
        from sub_items_detail sid
       where I_item_ind = 'M'
              and sid.sub_item = I_item
              and sid.location = I_location;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            L_function,
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_item_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item_ind',
                                            L_function,
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_location',
                                            L_function,
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   O_exists := FALSE; -- default: the Item does not exist as a Main Item at the Location
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_MAIN_CONFLICT','SUB_ITEMS_HEAD',NULL);
    open C_CHECK_MAIN_CONFLICT;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_MAIN_CONFLICT','SUB_ITEMS_HEAD',NULL);
   fetch C_CHECK_MAIN_CONFLICT into L_dummy;
   ---
   if C_CHECK_MAIN_CONFLICT%FOUND then
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_MAIN_CONFLICT','SUB_ITEMS_HEAD',NULL);
   close C_CHECK_MAIN_CONFLICT;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.CHECK_MAIN_CONFLICT',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_MAIN_CONFLICT;
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_SUB_CONFLICT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists         IN OUT  BOOLEAN,
                            I_item           IN      SUB_ITEMS_HEAD.ITEM%TYPE,
                            I_item_ind       IN      VARCHAR2,
                            I_location       IN      SUB_ITEMS_HEAD.LOCATION%TYPE)
RETURN BOOLEAN IS
   L_dummy     VARCHAR2(1);
   L_function  VARCHAR2(60)                      := 'SUB_ITEMS_SQL.CHECK_SUB_CONFLICT';
   ---
   cursor C_CHECK_SUB_CONFLICT is
      select 'x'
        from sub_items_detail sid
       where I_item_ind = 'S'
         and sid.item     = I_item
		 and sid.location = I_location
      union all
      select 'x'
        from repl_item_loc ril
       where  I_item_ind = 'S'
         and ril.item     = I_item
         and ril.location = I_location;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            L_function,
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_item_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item_ind',
                                            L_function,
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_location',
                                            L_function,
                                            'NOT NULL');
      RETURN FALSE;
   end if;
   ---
   O_exists := FALSE; -- default: the Item does not exist as a Main Item at the Location
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_SUB_CONFLICT','SUB_ITEMS_HEAD',NULL);
    open C_CHECK_SUB_CONFLICT;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_SUB_CONFLICT','SUB_ITEMS_HEAD',NULL);
   fetch C_CHECK_SUB_CONFLICT into L_dummy;
   ---
   if C_CHECK_SUB_CONFLICT%FOUND then
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_SUB_CONFLICT','SUB_ITEMS_HEAD',NULL);
   close C_CHECK_SUB_CONFLICT;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.CHECK_SUB_CONFLICT',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_SUB_CONFLICT;
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_EMPTY_SUB_HEAD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists         IN OUT  BOOLEAN,
                              I_item           IN      SUB_ITEMS_HEAD.ITEM%TYPE)
RETURN BOOLEAN IS
   L_dummy   VARCHAR2(1);

   cursor C_check_head is
      select 'x'
        from sub_items_head sih
       where sih.item = I_item
         and not exists(select 'x'
                          from sub_items_detail sid
                         where sid.item     = sih.item
                           and sid.location = sih.location
                           and sid.loc_type = sih.loc_type);

BEGIN
   O_exists := FALSE;
   ---
    open C_check_head;
   fetch C_check_head into L_dummy;
   ---
   if C_check_head%FOUND then
      O_exists := TRUE;
   end if;
   ---
   close C_check_head;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.CHECK_EMPTY_SUB_HEAD',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_EMPTY_SUB_HEAD;
--------------------------------------------------------------------------------------------
FUNCTION DELETE_EMPTY_SUB_HEAD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item           IN      SUB_ITEMS_HEAD.ITEM%TYPE)
RETURN BOOLEAN IS
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_HEAD is
      select 'x'
        from sub_items_head sih
       where sih.item = I_item
         and not exists(select 'x'
                          from sub_items_detail sid
                         where sid.item     = sih.item
                           and sid.location = sih.location
                           and sid.loc_type = sih.loc_type)
      for update nowait;

BEGIN
   --- lock Sub. Items Head table
    open C_LOCK_HEAD;
   close C_LOCK_HEAD;
   --- remove any Detail-less Header records
   delete from sub_items_head sih
    where sih.item = I_item
      and not exists(select 'x'
                       from sub_items_detail sid
                      where sid.item     = sih.item
                        and sid.location = sih.location
                        and sid.loc_type = sih.loc_type);
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('DELRECSB_REC_LOC',
                                            'SUB_ITEMS_HEAD',
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.DELETE_EMPTY_SUB_HEAD',
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_EMPTY_SUB_HEAD;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_MC_REJECTIONS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_change_type    IN      CODE_DETAIL.CODE%TYPE)
RETURN BOOLEAN IS
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_MC_REJECTIONS is
      select 'x'
        from mc_rejections
       where change_type = I_change_type
         and user_id     = get_user
      for update nowait;

BEGIN
   --- check for record-locking
    open C_LOCK_MC_REJECTIONS;
   close C_LOCK_MC_REJECTIONS;
   ---
   delete
     from mc_rejections
    where change_type = I_change_type
      and user_id     = get_user;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('DELRECSB_REC_LOC',
                                            'MC_REJECTIONS',
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.DELETE_MC_REJECTIONS',
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_MC_REJECTIONS;
---------------------------------------------------------------------------------------------------
FUNCTION ACCEPT_REJECT_ITEM_LOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_reject            IN OUT  BOOLEAN,
                                I_main_item         IN      ITEM_LOC.ITEM%TYPE,
                                I_sub_item          IN      ITEM_LOC.ITEM%TYPE,
                                I_prim_repl_pack    IN      ITEM_LOC.ITEM%TYPE,
                                I_location          IN      ITEM_LOC.LOC%TYPE,
                                I_loc_type          IN      ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS
   L_exists  BOOLEAN     := FALSE;

BEGIN
   O_reject := FALSE; -- default:  Items are valid
   ---
   --- 1) Item must exist at Location
   if I_main_item is not NULL then -- verify that Main Item exists at Location
      if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                           I_main_item,
                                           I_location,
                                           L_exists) = FALSE then
         return FALSE;
      end if;
      ---
      if not L_exists then  -- Main Item is Invalid: increment rejection counter and write MC Rejections record
         O_reject := TRUE;
         ---
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_main_item,
                                                   I_loc_type,
                                                   I_location,
                                                   'S',
                                                   'MAIN_ITEM_LOC_MISMATCH',
                                                   get_user,
                                                   I_main_item,
                                                   I_location,
                                                   NULL) = FALSE then
            return FALSE;
         end if;
         ---
         LP_rejected_records := TRUE;
         return TRUE;
      end if;
   end if;
   ---
   if I_sub_item is not NULL then -- verify that Substitute Item exists at Location
      if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                           I_sub_item,
                                           I_location,
                                           L_exists) = FALSE then
         return FALSE;
      end if;
      ---
      if not L_exists then  -- Sub. Item is Invalid: increment rejection counter and write MC Rejections record
         O_reject := TRUE;
         ---
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_sub_item,
                                                   I_loc_type,
                                                   I_location,
                                                   'S',
                                                   'SUB_ITEM_LOC_MISMATCH',
                                                   get_user,
                                                   I_sub_item,
                                                   I_location,
                                                   NULL) = FALSE then
            return FALSE;
         end if;
         ---
         LP_rejected_records := TRUE;
         return TRUE;
      end if;
   end if;
   ---
   if I_prim_repl_pack is not NULL then -- verify that Primary Replenishment Pack exists at Location
      if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                           I_prim_repl_pack,
                                           I_location,
                                           L_exists) = FALSE then
         return FALSE;
      end if;
      ---
      if not L_exists then  -- Prim. Repl. Pack is Invalid: increment rejection counter and write MC Rejections record
         O_reject := TRUE;
         ---
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_prim_repl_pack,
                                                   I_loc_type,
                                                   I_location,
                                                   'S',
                                                   'PACK_ITEM_LOC_MISMATCH',
                                                   get_user,
                                                   I_prim_repl_pack,
                                                   I_location,
                                                   NULL) = FALSE then
            return FALSE;
         end if;
         ---
         LP_rejected_records := TRUE;
         return TRUE;
      end if;
   end if;
   ---
   --- 2) Item cannot be both Main and Sub. at Location
   if I_main_item is not NULL then  -- verify that Main Item doesn't already exist as Sub. Item at Location
      if CHECK_MAIN_CONFLICT(O_error_message,
                                 L_exists,
                                 I_main_item,
                                 'M', -- Main Item being passed in
                                 I_location) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists then
         O_reject := TRUE;
         ---
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_main_item,
                                                   I_loc_type,
                                                   I_location,
                                                   'S',
                                                   'MAIN_EXISTS_AS_SUB',
                                                   get_user,
                                                   I_main_item,
                                                   I_location,
                                                   NULL) = FALSE then
            return FALSE;
         end if;
         ---
         LP_rejected_records := TRUE;
         return TRUE;
      end if;
   end if;
   ---
   if I_sub_item is not NULL then  -- verify that Sub. Item doesn't already exist as Main Item at Location
      if CHECK_SUB_CONFLICT(O_error_message,
                                 L_exists,
                                 I_sub_item,
                                 'S', -- Substitute Item being passed in
                                 I_location) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists then
         O_reject := TRUE;
         ---
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_sub_item,
                                                   I_loc_type,
                                                   I_location,
                                                   'S',
                                                   'SUB_EXISTS_AS_MAIN',
                                                   get_user,
                                                   I_sub_item,
                                                   I_location,
                                                   NULL) = FALSE then
            return FALSE;
         end if;
         ---
         LP_rejected_records := TRUE;
         return TRUE;
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.ACCEPT_REJECT_ITEM_LOC',
                                            to_char(SQLCODE));
      return FALSE;
END ACCEPT_REJECT_ITEM_LOC;
---------------------------------------------------------------------------------------------------
FUNCTION APPLY_SUB_ITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_main_item          IN       SUB_ITEMS_HEAD.ITEM%TYPE,
                        I_location           IN       ITEM_LOC.LOC%TYPE,
                        I_loc_type           IN       SUB_ITEMS_HEAD.LOC_TYPE%TYPE,
                        I_sub_item           IN       SUB_ITEMS_DETAIL.SUB_ITEM%TYPE,
                        I_prim_repl_pack     IN       SUB_ITEMS_DETAIL.PRIMARY_REPL_PACK%TYPE,
                        I_fill_priority      IN       SUB_ITEMS_HEAD.FILL_PRIORITY%TYPE,
                        I_use_sales_ind      IN       SUB_ITEMS_HEAD.USE_SALES_IND%TYPE,
                        I_use_stock_ind      IN       SUB_ITEMS_HEAD.USE_STOCK_IND%TYPE,
                        I_use_forecast_ind   IN       SUB_ITEMS_HEAD.USE_FORECAST_SALES_IND%TYPE,
                        I_pick_priority      IN       SUB_ITEMS_DETAIL.PICK_PRIORITY%TYPE,
                        I_action_type        IN       VARCHAR2,
                        I_AIP_ind            IN       SYSTEM_OPTIONS.AIP_IND%TYPE,
                        I_start_date         IN       SUB_ITEMS_DETAIL.START_DATE%TYPE,
                        I_end_date           IN       SUB_ITEMS_DETAIL.END_DATE%TYPE,
                        I_substitute_reason  IN       SUB_ITEMS_DETAIL.SUBSTITUTE_REASON%TYPE)
RETURN BOOLEAN IS
   L_rowid         ROWID;
   L_reject        BOOLEAN;
   L_vdate         DATE := GET_VDATE;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_HEAD_EXISTS is
      select rowid
        from sub_items_head
       where item     = I_main_item
         and location = I_location
         and loc_type = I_loc_type
         for update nowait;

   cursor C_DETAIL_EXISTS is
      select rowid
        from sub_items_detail
       where item              = I_main_item
         and location          = I_location
         and loc_type          = I_loc_type
         and (   (    sub_item          = I_sub_item
                  and primary_repl_pack = I_prim_repl_pack
                  and I_prim_repl_pack is not NULL)
              or (    sub_item = I_sub_item
                  and primary_repl_pack is NULL
                  and I_prim_repl_pack is NULL))
         and start_date        = I_start_date 
         and end_date          = I_end_date
         for update nowait;

   cursor C_LOCK_PICK_PRIORITY is
      select 'x'
        from sub_items_detail
       where item           = I_main_item
         and location       = I_location
         and loc_type       = I_loc_type
         and pick_priority >= I_pick_priority
         for update nowait;

BEGIN
   --- check whether Header record already exists
   open C_HEAD_EXISTS;
   fetch C_HEAD_EXISTS into L_rowid;
   ---
   if C_HEAD_EXISTS%NOTFOUND then -- Header record doesn't exist
      close C_HEAD_EXISTS;
      ---
      if I_action_type = 'AM' then -- create SI structure if Action Type is 'Add Main Item'
         --- verify that all passed-in Items exist at current Location
         if ACCEPT_REJECT_ITEM_LOC(O_error_message,
                                   L_reject,
                                   I_main_item,
                                   I_sub_item,
                                   I_prim_repl_pack,
                                   I_location,
                                   I_loc_type) = FALSE then
            return FALSE;
         end if;
         ---
         if NOT L_reject then -- all passed-in Items are valid: create Header and Detail records
            insert into sub_items_head(item,
                                       location,
                                       loc_type,
                                       fill_priority,
                                       use_sales_ind,
                                       use_stock_ind,
                                       use_forecast_sales_ind,
                                       create_datetime,
                                       last_update_datetime,
                                       last_update_id)
                                values(I_main_item,
                                       I_location,
                                       I_loc_type,
                                       I_fill_priority,
                                       I_use_sales_ind,
                                       I_use_stock_ind,
                                       I_use_forecast_ind,
                                       SYSDATE,
                                       SYSDATE,
                                       GET_USER);
            ---
            insert into sub_items_detail(item,
                                         location,
                                         loc_type,
                                         sub_item,
                                         primary_repl_pack,
                                         pick_priority,
                                         create_datetime,
                                         last_update_datetime,
                                         last_update_id,
                                         start_date,
                                         end_date,
                                         substitute_reason)
                                  values(I_main_item,
                                         I_location,
                                         I_loc_type,
                                         I_sub_item,
                                         I_prim_repl_pack,
                                         I_pick_priority,
                                         SYSDATE,
                                         SYSDATE,
                                         GET_USER,
                                         DECODE(I_start_date, NULL, L_vdate,
                                                                    I_start_date),
                                         DECODE(I_end_date, NULL, TO_DATE('01-01-9999','DD-MM-YYYY'),
                                                                  I_end_date),
                                         I_substitute_reason);
         end if; -- all passed-in Items are valid
      end if; -- Action Type is Add Main Item
   else -- Header record does exist
      close C_HEAD_EXISTS;
      ---
      if I_action_type in ('U', 'UH') then -- update Header if Action Type is 'Update All' or 'Update Head'
         update sub_items_head --- update indicators on Header record
            set fill_priority          = I_fill_priority,
                use_sales_ind          = I_use_sales_ind,
                use_stock_ind          = I_use_stock_ind,
                use_forecast_sales_ind = I_use_forecast_ind
          where rowid = L_rowid;
      end if; -- Action Type is 'Update All' or 'Update Head'
      --- check whether Detail record already exists
      open C_DETAIL_EXISTS;
      fetch C_DETAIL_EXISTS into L_rowid;
      ---
      if C_DETAIL_EXISTS%NOTFOUND then -- Detail record doesn't exist
         if I_action_Type in ('AM','AS') then -- Add SI to existing structure if Action Type is Add ('AM', 'AS')
            close C_DETAIL_EXISTS;
            --- verify that both Sub. Item and Prim. Repl. Pack exist at current Location
            if ACCEPT_REJECT_ITEM_LOC(O_error_message,
                                      L_reject,
                                      NULL,
                                      I_sub_item,
                                      I_prim_repl_pack,
                                      I_location,
                                      I_loc_type) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT L_reject then -- both Sub. Item and Prim. Repl. Pack are valid: create Detail record
               --- update conflicting Pick Priorities (if any)
               open C_LOCK_PICK_PRIORITY;
               close C_LOCK_PICK_PRIORITY;
               ---
               update sub_items_detail
                  set pick_priority        = pick_priority + 1,
                      last_update_datetime = sysdate,
                      last_update_id       = get_user
                where item           = I_main_item
                  and location       = I_location
                  and loc_type       = I_loc_type
                  and pick_priority >= I_pick_priority
                  and exists (select 'x'
                                from sub_items_detail sid
                               where sid.item          = I_main_item
                                 and sid.location      = I_location
                                 and sid.loc_type      = I_loc_type
                                 and sid.pick_priority = I_pick_priority);
               --- create Detail record
               insert into sub_items_detail(item,
                                            location,
                                            loc_type,
                                            sub_item,
                                            primary_repl_pack,
                                            pick_priority,
                                            create_datetime,
                                            last_update_datetime,
                                            last_update_id,
                                            start_date,
                                            end_date,
                                            substitute_reason)
                                     values(I_main_item,
                                            I_location,
                                            I_loc_type,
                                            I_sub_item,
                                            I_prim_repl_pack,
                                            I_pick_priority,
                                            SYSDATE,
                                            SYSDATE,
                                            GET_USER,
                                            DECODE(I_start_date, NULL, L_vdate,
                                                                       I_start_date),
                                            DECODE(I_end_date, NULL, TO_DATE('01-01-9999','DD-MM-YYYY'),
                                                                     I_end_date),
                                            I_substitute_reason);
            end if; -- both Sub. Item and Prim. Repl. Pack are valid
         end if;  -- Action Type is Add ('AM', 'AS')
      else -- Detail record does exist
         close C_DETAIL_EXISTS;
         ---
         if I_action_type in ('U','UD') and I_pick_priority is not NULL then -- update Detail if Pick Priority is passed in
            -- verify that Prim. Repl. Pack exists at current Location
            if ACCEPT_REJECT_ITEM_LOC(O_error_message,
                                      L_reject,
                                      NULL,
                                      NULL,
                                      I_prim_repl_pack,
                                      I_location,
                                      I_loc_type) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT L_reject then -- Prim. Repl. Pack is valid: update Detail record
               --- update conflicting Pick Priorities (if any)
               open C_LOCK_PICK_PRIORITY;
               close C_LOCK_PICK_PRIORITY;
               ---
               update sub_items_detail
                  set pick_priority        = pick_priority + 1,
                      last_update_datetime = SYSDATE,
                      last_update_id       = GET_USER
                where item           = I_main_item
                  and location       = I_location
                  and loc_type       = I_loc_type
                  and pick_priority >= I_pick_priority
                  and exists (select 'x'
                                from sub_items_detail sid
                               where sid.item          = I_main_item
                                 and sid.location      = I_location
                                 and sid.loc_type      = I_loc_type
                                 and sid.pick_priority = I_pick_priority);
               --- update Detail record
               update sub_items_detail
                  set primary_repl_pack    = I_prim_repl_pack,
                      pick_priority        = I_pick_priority,
                      last_update_datetime = SYSDATE,
                      last_update_id       = GET_USER,
                      start_date           = DECODE(I_start_date, NULL, L_vdate,
                                                                        I_start_date),
                      end_date             = DECODE(I_end_date, NULL, TO_DATE('01-01-9999','DD-MM-YYYY'),
                                                                      I_end_date)
                where rowid = L_rowid;
            end if; -- Prim. Repl. Pack is valid
         end if; -- Action Type is 'Update All' or 'Update Detail'
      end if; -- Detail record doesn't exist
   end if; -- Header record doesn't exist
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECSB_REC_LOC',
                                            'SUB_ITEMS_DETAIL',
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.APPLY_SUB_ITEM',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END APPLY_SUB_ITEM;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAILS(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_main_item         IN      SUB_ITEMS_DETAIL.ITEM%TYPE,
                        I_sub_item          IN      SUB_ITEMS_DETAIL.SUB_ITEM%TYPE,
                        I_prim_repl_pack    IN      SUB_ITEMS_DETAIL.PRIMARY_REPL_PACK%TYPE,
                        I_location          IN      ITEM_LOC.LOC%TYPE,
                        I_loc_type          IN      SUB_ITEMS_DETAIL.LOC_TYPE%TYPE)
RETURN BOOLEAN IS
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_DETAIL is
      select 'x'
        from sub_items_detail
       where item     = I_main_item
         and location = I_location
         and loc_type = I_loc_type
         and (   (    I_sub_item is not NULL
                  and sub_item = I_sub_item
                  and (   (    I_prim_repl_pack is not NULL
                           and primary_repl_pack = I_prim_repl_pack)
                       or (    I_prim_repl_pack is NULL
                           and primary_repl_pack is NULL)))
              or I_sub_item is NULL);

BEGIN
    open C_LOCK_DETAIL;
   close C_LOCK_DETAIL;
   ---
   delete
     from sub_items_detail
    where item              = I_main_item
      and location          = I_location
      and loc_type          = I_loc_type
      and (   (    I_sub_item is not NULL
               and sub_item = I_sub_item
               and (   (    I_prim_repl_pack is not NULL
                        and primary_repl_pack = I_prim_repl_pack)
                    or (    I_prim_repl_pack is NULL
                        and primary_repl_pack is NULL)))
           or I_sub_item is NULL);
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('DELRECSB_REC_LOC',
                                            'SUB_ITEMS_DETAIL',
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.DELETE_DETAILS',
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_DETAILS;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_HEADER(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_main_item         IN      SUB_ITEMS_DETAIL.ITEM%TYPE,
                       I_location          IN      ITEM_LOC.LOC%TYPE,
                       I_loc_type          IN      SUB_ITEMS_DETAIL.LOC_TYPE%TYPE)
RETURN BOOLEAN IS
   L_rowid           ROWID;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_HEAD is
      select rowid
        from sub_items_head
       where item     = I_main_item
         and location = I_location
         and loc_type = I_loc_type
      for update nowait;

BEGIN
    open C_LOCK_HEAD;
   fetch C_LOCK_HEAD into L_rowid;
   close C_LOCK_HEAD;
   ---
   delete
     from sub_items_head
    where rowid = L_rowid;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECSB_REC_LOC',
                                            'SUB_ITEMS_HEAD',
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.DELETE_HEADER',
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_HEADER;
---------------------------------------------------------------------------------------------------
FUNCTION EDIT_SUB_ITEM_LOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_rejected_records  IN OUT  BOOLEAN,
                           I_main_item         IN      SUB_ITEMS_HEAD.ITEM%TYPE,
                           I_loc_group_type    IN      CODE_DETAIL.CODE%TYPE,
                           I_loc_group_value   IN      VARCHAR2,
                           I_fill_priority     IN      SUB_ITEMS_HEAD.FILL_PRIORITY%TYPE,
                           I_use_sales_ind     IN      SUB_ITEMS_HEAD.USE_SALES_IND%TYPE,
                           I_use_stock_ind     IN      SUB_ITEMS_HEAD.USE_STOCK_IND%TYPE,
                           I_use_forecast_ind  IN      SUB_ITEMS_HEAD.USE_FORECAST_SALES_IND%TYPE,
                           I_sub_item          IN      SUB_ITEMS_DETAIL.SUB_ITEM%TYPE,
                           I_prim_repl_pack    IN      SUB_ITEMS_DETAIL.PRIMARY_REPL_PACK%TYPE,
                           I_pick_priority     IN      SUB_ITEMS_DETAIL.PICK_PRIORITY%TYPE,
                           I_action_type       IN      VARCHAR2,
                           I_AIP_ind           IN      SYSTEM_OPTIONS.AIP_IND%TYPE,
                           I_start_date        IN      SUB_ITEMS_DETAIL.START_DATE%TYPE,
                           I_end_date          IN      SUB_ITEMS_DETAIL.END_DATE%TYPE,
                           I_substitute_reason IN      SUB_ITEMS_DETAIL.SUBSTITUTE_REASON%TYPE)
RETURN BOOLEAN IS
   L_loop_index NUMBER  := 0;
   L_locs       locs_table_type;
   L_loc_type   SUB_ITEMS_DETAIL.LOC_TYPE%TYPE;

   cursor C_STORE_CLASS is
      select store
        from store
       where store_class      = I_loc_group_value
         and stockholding_ind = 'Y';

   cursor C_DISTRICT is
      select h.store
        from store_hierarchy h
       where h.district = to_number(I_loc_group_value)
         and exists(select 'x'
                      from store s
                     where h.store            = s.store
                       and s.stockholding_ind = 'Y');

   cursor C_REGION is
      select h.store
        from store_hierarchy h
       where h.region = to_number(I_loc_group_value)
         and exists(select 'x'
                      from store s
                     where h.store            = s.store
                       and s.stockholding_ind = 'Y');

    cursor C_AREA is
         select h.store
           from store_hierarchy h
          where h.area = to_number(I_loc_group_value)
            and exists(select 'x'
                         from store s
                        where h.store            = s.store
                          and s.stockholding_ind = 'Y');
   cursor C_TRANSFER_ZONE is
      select store
        from store
       where transfer_zone    = to_number(I_loc_group_value)
         and stockholding_ind = 'Y';

   cursor C_LOC_TRAITS is
      select l.store
        from loc_traits_matrix l
       where l.loc_trait = to_number(I_loc_group_value)
         and exists(select 'x'
                      from store s
                     where l.store            = s.store
                       and s.stockholding_ind = 'Y');

   cursor C_LOC_LIST_STORE is
      select l.location
        from loc_list_detail l
       where l.loc_type = 'S'
         and l.loc_list = to_number(I_loc_group_value)
         and exists(select 'x'
                      from store s
                     where l.location         = s.store
                       and s.stockholding_ind = 'Y');


   cursor C_LOC_LIST_WH is
      select distinct w.wh
        from wh w, loc_list_detail l
       where l.loc_list         = to_number(I_loc_group_value)
         and l.loc_type         = 'W'
         and w.repl_ind         = 'Y'
         and w.stockholding_ind = 'Y'
         and w.finisher_ind     = 'N'
         and ((l.location       = w.wh
               or l.location    = w.physical_wh)
              and w.wh         != w.physical_wh);

   cursor C_STORE is
      select store
        from store
       where stockholding_ind = 'Y';


   cursor C_WH is
      select wh
        from wh
       where repl_ind         = 'Y'
         and stockholding_ind = 'Y'
         and finisher_ind     = 'N';

   cursor C_DEFAULT_WH_STORES is
      select store
        from store
       where default_wh       = to_number(I_loc_group_value)
         and stockholding_ind = 'Y';

BEGIN
   LP_rejected_records := FALSE;
   ---
   /* Determine
      Loc. Type */
   if I_loc_group_type in ('W','LLW','AW') then
      L_loc_type := 'W';
   else
      L_loc_type := 'S';
   end if;
   /* Fetch Valid Locations Based
      On Location Group Type      */
   if I_loc_group_type in ('S','W') then
      L_locs(1) := I_loc_group_value;
   elsif I_loc_group_type = 'C' then
      open C_STORE_CLASS;
      ---
      LOOP
         L_loop_index := L_loop_index + 1;
         fetch C_STORE_CLASS into L_locs(L_loop_index);
         EXIT WHEN C_STORE_CLASS%NOTFOUND;
      END LOOP;
      ---
      close C_STORE_CLASS;
   elsif I_loc_group_type = 'D' then
      open C_DISTRICT;
      ---
      LOOP
         L_loop_index := L_loop_index + 1;
         fetch C_DISTRICT into L_locs(L_loop_index);
         EXIT WHEN C_DISTRICT%NOTFOUND;
      END LOOP;
      ---
      close C_DISTRICT;
   elsif I_loc_group_type = 'R' then
      open C_REGION;
      ---
      LOOP
         L_loop_index := L_loop_index + 1;
         fetch C_REGION into L_locs(L_loop_index);
         EXIT WHEN C_REGION%NOTFOUND;
      END LOOP;
      ---
      close C_REGION;
   elsif I_loc_group_type = 'A' then
         open C_AREA;
         ---
         LOOP
            L_loop_index := L_loop_index + 1;
            fetch C_AREA into L_locs(L_loop_index);
            EXIT WHEN C_AREA%NOTFOUND;
         END LOOP;
         ---
         close C_AREA;
   elsif I_loc_group_type = 'T' then
      open C_TRANSFER_ZONE;
      ---
      LOOP
         L_loop_index := L_loop_index + 1;
         fetch C_TRANSFER_ZONE into L_locs(L_loop_index);
         EXIT WHEN C_TRANSFER_ZONE%NOTFOUND;
      END LOOP;
      ---
      close C_TRANSFER_ZONE;
   elsif I_loc_group_type = 'L' then
      open C_LOC_TRAITS;
      ---
      LOOP
         L_loop_index := L_loop_index + 1;
         fetch C_LOC_TRAITS into L_locs(L_loop_index);
         EXIT WHEN C_LOC_TRAITS%NOTFOUND;
      END LOOP;
      ---
      close C_LOC_TRAITS;
   elsif I_loc_group_type = 'LLS' then
      open C_LOC_LIST_STORE;
      ---
      LOOP
         L_loop_index := L_loop_index + 1;
         fetch C_LOC_LIST_STORE into L_locs(L_loop_index);
         EXIT WHEN C_LOC_LIST_STORE%NOTFOUND;
      END LOOP;
      ---
      close C_LOC_LIST_STORE;
   elsif I_loc_group_type = 'LLW' then
      open C_LOC_LIST_WH;
      ---
      LOOP
         L_loop_index := L_loop_index + 1;
         fetch C_LOC_LIST_WH into L_locs(L_loop_index);
         EXIT WHEN C_LOC_LIST_WH%NOTFOUND;
      END LOOP;
      ---
      close C_LOC_LIST_WH;
   elsif I_loc_group_type = 'AS' then
      open C_STORE;
      ---
      LOOP
         L_loop_index := L_loop_index + 1;
         fetch C_STORE into L_locs(L_loop_index);
         EXIT WHEN C_STORE%NOTFOUND;
      END LOOP;
      ---
      close C_STORE;
   elsif I_loc_group_type = 'AW' then
      open C_WH;
      ---
      LOOP
         L_loop_index := L_loop_index + 1;
         fetch C_WH into L_locs(L_loop_index);
         EXIT WHEN C_WH%NOTFOUND;
      END LOOP;
      ---
      close C_WH;
   elsif I_loc_group_type = 'DW' then
      open C_DEFAULT_WH_STORES;
      ---
      LOOP
         L_loop_index := L_loop_index + 1;
         fetch C_DEFAULT_WH_STORES into L_locs(L_loop_index);
         EXIT WHEN C_DEFAULT_WH_STORES%NOTFOUND;
      END LOOP;
      ---
      close C_DEFAULT_WH_STORES;
   end if;
   ---
   /* Process All Locations
      Returned Via The Table Variable */
   FOR loop_index IN 1..L_locs.COUNT LOOP
      if I_action_type in ('U', 'UH', 'UD', 'AM', 'AS') then
         if APPLY_SUB_ITEM(O_error_message,
                           I_main_item,
                           L_locs(loop_index),
                           L_loc_type,
                           I_sub_item,
                           I_prim_repl_pack,
                           I_fill_priority,
                           I_use_sales_ind,
                           I_use_stock_ind,
                           I_use_forecast_ind,
                           I_pick_priority,
                           I_action_type,
                           I_AIP_ind,
                           I_start_date,
                           I_end_date,
                           I_substitute_reason) = FALSE then
            return FALSE;
         end if;
      elsif I_action_type = 'DS' then
         if DELETE_DETAILS(O_error_message,
                           I_main_item,
                           I_sub_item,
                           I_prim_repl_pack,
                           L_locs(loop_index),
                           L_loc_type) = FALSE then
            return FALSE;
         end if;
      elsif I_action_type = 'DM' then
         if DELETE_DETAILS(O_error_message,
                           I_main_item,
                           NULL,  -- delete all Sub. Item records for the current Item/Loc.
                           NULL,
                           L_locs(loop_index),
                           L_loc_type) = FALSE then
            return FALSE;
         end if;
         ---
         if DELETE_HEADER(O_error_message,
                          I_main_item,
                          L_locs(loop_index),
                          L_loc_type) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;
   ---
   O_rejected_records := LP_rejected_records;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.EDIT_SUB_ITEM_LOC',
                                            to_char(SQLCODE));
      return FALSE;
END EDIT_SUB_ITEM_LOC;
---------------------------------------------------------------------------------------------------
FUNCTION CHK_DUP_LOC_DATE(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_main_item         IN      SUB_ITEMS_HEAD.ITEM%TYPE,
                            I_location          IN      SUB_ITEMS_DETAIL.LOCATION%TYPE,
                            I_start_date        IN      SUB_ITEMS_DETAIL.START_DATE%TYPE,
                            I_end_date          IN      SUB_ITEMS_DETAIL.END_DATE%TYPE)
RETURN BOOLEAN IS
   L_count_index       NUMBER  := 0;

   cursor CUR_DUP_LOC_REASON is
   select count(1)
     from sub_items_detail
    where item = I_main_item
      and location = I_location
      and start_date <= I_end_date 
      and end_date >= I_start_date;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'CUR_DUP_LOC_REASON', 'SUB_ITEMS_DETAIL', NULL);
   open CUR_DUP_LOC_REASON;
   SQL_LIB.SET_MARK('FETCH', 'CUR_DUP_LOC_REASON', 'SUB_ITEMS_DETAIL', NULL);
   fetch CUR_DUP_LOC_REASON into L_count_index;
   SQL_LIB.SET_MARK('CLOSE', 'CUR_DUP_LOC_REASON', 'SUB_ITEMS_DETAIL', NULL);
   close CUR_DUP_LOC_REASON;
   /*Return false of any overlap of date is present*/
   if L_count_index = 0 then
      return TRUE;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_DATE_RANGE');
      return FALSE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUB_ITEM_SQL.CHK_DUP_LOC_DATE',
                                            to_char(SQLCODE));
      return FALSE;
END CHK_DUP_LOC_DATE;

END SUB_ITEM_SQL;
/
