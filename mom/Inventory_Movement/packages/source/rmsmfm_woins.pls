
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_WOIN AUTHID CURRENT_USER IS

FAMILY      VARCHAR2(64) := 'woin';

WO_ADD     CONSTANT  VARCHAR2(20) := 'InBdWOCre';
WO_UPD     CONSTANT  VARCHAR2(20) := 'InBdWOMod';
WO_DEL     CONSTANT  VARCHAR2(20) := 'InBdWODel';

--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_msg      OUT VARCHAR2,
                I_queue_rec      IN  WOIN_MFQUEUE%ROWTYPE,                
                I_publish_ind    IN  WO_DETAIL.PUBLISH_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1);
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT);
--------------------------------------------------------------------------------
END;
/
