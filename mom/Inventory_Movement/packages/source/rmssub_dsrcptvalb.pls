
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_DSRCPT_VALIDATE AS

--------------------------------------------------------------------------------
-- PACKAGE GLOBALS
--------------------------------------------------------------------------------

G_dsrcpt_rec   RMSSUB_DSRCPT.DSRCPT_REC_TYPE;


--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION CHECK_FIELDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message       IN     "RIB_DSRcptDesc_REC",
                      I_message_type  IN     VARCHAR2)
RETURN BOOLEAN;


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

FUNCTION CHECK_MESSAGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dsrcpt_rec    IN OUT RMSSUB_DSRCPT.DSRCPT_REC_TYPE,
                       I_message       IN     "RIB_DSRcptDesc_REC",
                       I_message_type  IN     VARCHAR2)
RETURN BOOLEAN IS

L_program   VARCHAR2(50) := 'RMSSUB_DSRCPT_VALIDATE.CHECK_MESSAGE';

BEGIN

   if I_message is null then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', null, null,
      null);
      return FALSE;
   end if;

   if CHECK_FIELDS(O_error_message,
                   I_message,
                   I_message_type) = FALSE then
      return FALSE;
   end if;

   O_dsrcpt_rec := G_dsrcpt_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;


--------------------------------------------------------------------------------
-- PRIVATE PROCEDURES
--------------------------------------------------------------------------------

FUNCTION CHECK_FIELDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message       IN     "RIB_DSRcptDesc_REC",
                      I_message_type  IN     VARCHAR2)
RETURN BOOLEAN IS

L_program    VARCHAR2(50) := 'RMSSUB_DSRCPT_VALIDATE.CHECK_FIELDS';

L_item_rec   ITEM_MASTER%ROWTYPE;

BEGIN

   -- trans_date
   if I_message.trans_date is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'trans_date',
      null, null);
      return FALSE;
   end if;

   -- item
   if I_message.item is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'item', null,
      null);
      return FALSE;
   else
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_rec,
                                         I_message.item) = FALSE then
         return FALSE;
      end if;
   end if;

   -- store
   if I_message.store is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'store', null,
      null);
      return FALSE;
   end if;

   -- units
   if I_message.units is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'units', null,
      null);
      return FALSE;
   end if;

   -- unit_cost
   if I_message.unit_cost is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'unit_cost', null,
      null);
      return FALSE;
   end if;

   G_dsrcpt_rec.trans_date := I_message.trans_date;
   G_dsrcpt_rec.item := I_message.item;
   G_dsrcpt_rec.store := I_message.store;
   G_dsrcpt_rec.units := I_message.units;
   G_dsrcpt_rec.unit_cost := I_message.unit_cost;
   --
   G_dsrcpt_rec.pack_ind := L_item_rec.pack_ind;
   G_dsrcpt_rec.dept := L_item_rec.dept;
   G_dsrcpt_rec.class := L_item_rec.class;
   G_dsrcpt_rec.subclass := L_item_rec.subclass;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FIELDS;
--------------------------------------------------------------------------------

END RMSSUB_DSRCPT_VALIDATE;
/
