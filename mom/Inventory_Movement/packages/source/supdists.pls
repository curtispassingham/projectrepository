CREATE OR REPLACE PACKAGE SUP_DIST_SQL AUTHID CURRENT_USER AS

-- define global package variables and types
---------------------------------------------------------------------------------------------------
TYPE repl_supp_dist_rec IS RECORD 
(
   supplier_parent         SUPS.SUPPLIER%TYPE,
   supplier_parent_name    SUPS.SUP_NAME%TYPE,
   supplier                REPL_ITEM_LOC_SUPP_DIST.SUPPLIER%TYPE,
   supplier_name           SUPS.SUP_NAME%TYPE,
   origin_country_id       REPL_ITEM_LOC_SUPP_DIST.ORIGIN_COUNTRY_ID%TYPE,
   unit_cost               ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
   min_ord_qty             ITEM_SUPP_COUNTRY.MIN_ORDER_QTY%TYPE,
   max_ord_qty             ITEM_SUPP_COUNTRY.MAX_ORDER_QTY %TYPE,
   dist_pct                REPL_ITEM_LOC_SUPP_DIST.DIST_PCT%TYPE,
   new_rec                 VARCHAR2(1),
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE repl_supp_dist_tbl_type IS TABLE OF repl_supp_dist_rec
  INDEX BY BINARY_INTEGER;

ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;  -- Define error for raise_application_error

---------------------------------------------------------------------------------------------------
-- Function name:  QUERY_PROCEDURE
-- Purpose      :  This procedure queries and returns item/loc/supplier records for the
--              :  input item and location, combined with supplier distribution ratios
--              :  from the REPL_ITEM_LOC_SUPP_DIST table if any exist.
-- Inputs       :  item     -- cannot be null
--              :  location -- cannot be null
-- Calls        :  none
---------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (IO_supp_dist_tbl   IN OUT    REPL_SUPP_DIST_TBL_TYPE,
                           I_item             IN        ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                           I_location         IN        ITEM_SUPP_COUNTRY_LOC.LOC%TYPE);
---------------------------------------------------------------------------------------------------
-- Function name:  LOCK_PROCEDURE
-- Purpose      :  This procedure locks a row in the REPL_ITEM_LOC_SUPP_DIST table
--              :  for each item/loc/supplier/origin_country_id combination passed as input
-- Inputs       :  item     -- cannot be null
--              :  location -- cannot be null
-- Calls        :  none
---------------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE (IO_supp_dist_tbl    IN OUT   REPL_SUPP_DIST_TBL_TYPE,
                          I_item              IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                          I_location          IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE);
---------------------------------------------------------------------------------------------------
-- Function name:  UPDATE_PROCEDURE
-- Purpose      :  This procedure updates a row in the REPL_ITEM_LOC_SUPP_DIST table for each
--              :  item/loc/supplier/origin_country_id combination passed as input where the
--              :  distribution percentage is > 0.  If distribution percentage = 0, it deletes
--              :  the corresponding row in the REPL_ITEM_LOC_SUPP_DIST table.
-- Inputs       :  item     -- cannot be null
--              :  location -- cannot be null
-- Calls        :  none
---------------------------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE (IO_supp_dist_tbl    IN OUT   REPL_SUPP_DIST_TBL_TYPE,
                            I_item              IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                            I_location          IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE);
---------------------------------------------------------------------------------------------------
-- Function name:  CHECK_ITEM_LOC
-- Purpose      :  This function verifies an item or an item/loc exists in REPL_ITEM_LOC
--              :  with a stock category of Direct to Store ('D') or Crossdocked ('C')
--              :  and in Approved status, or the item or item/loc exists in the
--              :  REPL_ATTR_UPDATE_[HEAD,ITEM,LOC] tables with a stock category of
--              :  'D' or 'C' and does not exist in REPL_ATTR_UPDATE_EXCLUDE.
-- Inputs       :  item     -- cannot be null
--              :  location -- can be null
-- Calls        :  none
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LOC (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists            IN OUT   BOOLEAN,
                         I_item              IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_location          IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END SUP_DIST_SQL;
/
