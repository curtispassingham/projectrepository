CREATE OR REPLACE PACKAGE BODY RMSSUB_XALLOC_VALIDATE AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the create and modify messages
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XAlloc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the delete messages
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XAllocRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_ALLOC_EXISTS
   -- Purpose      : This function will verify if the ALLOC record already exists.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ALLOC_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XAlloc_REC",
                            I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ALLOC_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XAllocRef_REC",
                            I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TO_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_message         IN       "RIB_XAlloc_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DELETE
   -- Purpose      : This function will verify if the ALLOC can be deleted.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_IN_PROGRESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                           I_to_loc          IN       ALLOC_DETAIL.TO_LOC%TYPE,
                           I_to_loc_type     IN       ALLOC_DETAIL.TO_LOC_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_DEFAULT_FIELDS
   -- Purpose      : This function will retrieve all values not defined in the message, but required
   --                for RMS integrity.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DEFAULT_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_alloc_rec       IN OUT   ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB ALLOC object into a ALLOC record defined in
   --                the MERCH_SQL package.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_alloc_rec       IN OUT   NOCOPY   ALLOCATION_SQL.ALLOC_REC,
                         I_message         IN       "RIB_XAlloc_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB ALLOC object into a ALLOC record defined in
   --                the MERCH_SQL package.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_alloc_rec       OUT      NOCOPY   ALLOCATION_SQL.ALLOC_REC,
                         I_message         IN       "RIB_XAllocRef_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: GET_ORDER_CURRENT_AVAIL
   -- Purpose      : This function is will return available quantity for an order which are not allocated
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_CURRENT_AVAIL
        (O_error_message  IN OUT VARCHAR2,
         O_available      IN OUT item_loc_soh.stock_on_hand%TYPE,
         I_item           IN     item_loc.item%TYPE,
         I_loc            IN     item_loc.loc%TYPE,
         I_loc_type       IN     item_loc.loc_type%TYPE,
         I_order_no       IN     ordhead.order_no%TYPE)
   RETURN BOOLEAN ;
-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_CURRENT_AVAIL
        (O_error_message  IN OUT VARCHAR2,
         O_available      IN OUT item_loc_soh.stock_on_hand%TYPE,
         I_item           IN     item_loc.item%TYPE,
         I_loc            IN     item_loc.loc%TYPE,
         I_loc_type       IN     item_loc.loc_type%TYPE,
         I_order_no       IN     ordhead.order_no%TYPE)
   RETURN BOOLEAN IS
   L_program           VARCHAR2(50)  := 'RMSSUB_XALLOC_VALIDATE.GET_ORDER_CURRENT_AVAIL';
   L_order_qty_avail   ordloc.qty_ordered%TYPE  := 0;
   L_xdoc_alloc_qty    alloc_detail.qty_allocated%TYPE := 0;
   
   cursor GET_ORDER_QTY_AVAIL IS
     select ol.qty_ordered - nvl(ol.qty_received,0)
       from ordhead oh,
            ordloc ol 
      where oh.order_no = I_order_no
        and oh.order_no = ol.order_no
        and ol.item = I_item
        and ol.location = I_loc
        and ol.loc_type = I_loc_type;
        
   cursor GET_XDOC_QTY_ALLOC IS
     select sum(ad.qty_allocated)
       from alloc_header ah,
            alloc_detail ad 
      where ah.alloc_no = ad.alloc_no
        and ah.order_no is not null
        and ah.order_no = I_order_no
        and ah.status in ('A','W','R')
        and ah.item = I_item
        and ah.wh = I_loc;
BEGIN
   open GET_ORDER_QTY_AVAIL;
   fetch GET_ORDER_QTY_AVAIL into L_order_qty_avail;
   close GET_ORDER_QTY_AVAIL;
   open GET_XDOC_QTY_ALLOC;
   fetch GET_XDOC_QTY_ALLOC into L_xdoc_alloc_qty;
   close GET_XDOC_QTY_ALLOC;
   O_available := GREATEST((L_order_qty_avail - nvl(L_xdoc_alloc_qty,0)),0);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ORDER_CURRENT_AVAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_alloc_rec       OUT      NOCOPY   ALLOCATION_SQL.ALLOC_REC,
                       I_message         IN       "RIB_XAlloc_REC",
                       I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XALLOC_VALIDATE.CHECK_MESSAGE';
   L_receive_as_type ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
   L_var          VARCHAR2(1);
   L_check_item   VARCHAR2(1);
   L_check_from_to_loc_same  VARCHAR2(1);
   cursor VERIFY_ITEM is
      select 1
        from item_master im
       where im.item = I_message.item
         and im.item_level = im.tran_level
         and im.status = 'A';
   
   cursor GET_ITEM_INFO is
      select nvl(receive_as_type, 'P'),
             pack_ind
        from item_loc il,
             item_master im
       where im.item = I_message.item
         and im.item = il.item
         and il.loc = I_message.from_loc
         and il.loc_type = 'W'
         and im.item_level = im.tran_level
         and im.status = 'A';

   cursor VERIFY_FROM_LOC is
      select 1
        from wh,
             item_loc il
       where wh = I_message.from_loc
         and il.item = I_message.item
         and il.loc = wh.wh
         and stockholding_ind = 'Y'
         and finisher_ind = 'N'
         and rownum = 1;

   cursor C_CHECK_ALLOC_DETAIL is
      select 1
        from alloc_header
       where alloc_no   = I_message.alloc_no
         and item       = I_message.item;
         
   cursor C_CHECK_FROM_TO_LOC_SAME is
      select 'Y'
        from TABLE(cast(I_message.xallocdtl_tbl as "RIB_XAllocDtl_TBL")) t 
       where t.to_loc = I_message.from_loc;      
BEGIN
   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message,
                                I_message_type) then
      return FALSE;
   end if;

   open C_CHECK_FROM_TO_LOC_SAME;
   fetch C_CHECK_FROM_TO_LOC_SAME into L_check_from_to_loc_same;
   close C_CHECK_FROM_TO_LOC_SAME;
   if L_check_from_to_loc_same = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('LOC_NOT_SAME',null,null,null);
      return FALSE;
   end if;
   open VERIFY_ITEM;

   fetch VERIFY_ITEM into L_var;
   if VERIFY_ITEM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',I_message.item,null,null);
      close VERIFY_ITEM;
      return FALSE;
   end if;
   close VERIFY_ITEM;
   open GET_ITEM_INFO;
   fetch GET_ITEM_INFO into L_receive_as_type, O_alloc_rec.pack_ind;
   if GET_ITEM_INFO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_WH','Item : '||I_message.item,'. Warehouse : '||I_message.from_loc,null);
      close GET_ITEM_INFO;
      return FALSE;
   end if;
   close GET_ITEM_INFO;
   if O_alloc_rec.pack_ind = 'Y' and L_receive_as_type != 'P' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_RECEIVE_AS_TYPE',null,null,null);
      return FALSE;
   end if;

    if I_message_type = LP_dtl_mod_type then
      open C_CHECK_ALLOC_DETAIL;
      fetch C_CHECK_ALLOC_DETAIL into L_check_item;
      if C_CHECK_ALLOC_DETAIL%NOTFOUND then
         close C_CHECK_ALLOC_DETAIL;
         O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_ALLOC',I_message.item,I_message.alloc_no,null);
         return FALSE;
      end if;
      close C_CHECK_ALLOC_DETAIL;
   end if;

   open VERIFY_FROM_LOC;
   fetch VERIFY_FROM_LOC into L_var;
   if VERIFY_FROM_LOC%NOTFOUND then
      close VERIFY_FROM_LOC;
      O_error_message := SQL_LIB.CREATE_MSG('INV_WH',I_message.from_loc,null,null);
      return FALSE;
   end if;
   close VERIFY_FROM_LOC;

   if NOT VALIDATE_TO_LOC(O_error_message,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   if not CHECK_ALLOC_EXISTS(O_error_message,
                             I_message,
                             I_message_type) then
      return FALSE;
   end if;

/*****************
** May need to set alloc chrg system option on record when persisting
****************/
   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_alloc_rec,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_alloc_rec       OUT    NOCOPY   ALLOCATION_SQL.ALLOC_REC,
                       I_message         IN              "RIB_XAllocRef_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XALLOC_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not CHECK_ALLOC_EXISTS(O_error_message,
                             I_message,
                             I_message_type) then
      return FALSE;
   end if;

   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_alloc_rec,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
---------------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XAlloc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)  := 'RMSSUB_XALLOC_VALIDATE.CHECK_REQUIRED_FIELDS';
   L_wh             WH.WH%TYPE;
   L_release_date   DATE  := I_message.release_date;

   cursor C_VAL_IN_ST_DATE is
     select passed.to_loc
       from TABLE(cast(I_message.xallocdtl_tbl as "RIB_XAllocDtl_TBL")) passed
      where NVL(to_char(passed.in_store_date, 'YYYYMMDD'), to_char(L_release_date, 'YYYYMMDD')) <
               to_char(L_release_date, 'YYYYMMDD') ;
BEGIN

   if I_message.alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Allocation number');
      return FALSE;
   end if;

   if I_message.item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Item');
      return FALSE;
   end if;

   if I_message.from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'From location');
      return FALSE;
   end if;

   if I_message_type in (RMSSUB_XALLOC.LP_cre_type, RMSSUB_XALLOC.LP_mod_type) then
      if I_message.alloc_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Allocation description');
         return FALSE;
      end if;

      if I_message.release_date is not NULL then
         if TRUNC(I_message.release_date) < GET_VDATE then
            O_error_message := SQL_LIB.CREATE_MSG('RELEASE_AFTER_VDATE', NULL, NULL, NULL);
            return FALSE;
         end if;
      end if;
   end if;

   if I_message_type = RMSSUB_XALLOC.LP_cre_type then
      if NVL(I_message.origin_ind, 'EG') NOT IN ('AIP', 'EG') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ORIGIN_IND', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   --- Validate if the in store date
   if I_message_type in (RMSSUB_XALLOC.LP_cre_type, RMSSUB_XALLOC.LP_dtl_cre_type, RMSSUB_XALLOC.LP_dtl_mod_type)
      and I_message.release_date is not NULL then

      open C_VAL_IN_ST_DATE;
      fetch C_VAL_IN_ST_DATE into L_wh;

      if C_VAL_IN_ST_DATE%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_IN_STORE_DT', NULL, NULL, NULL);
         close C_VAL_IN_ST_DATE;
         return FALSE;
      end if;

      close C_VAL_IN_ST_DATE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XAllocRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XALLOC_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Allocation number');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message_type    IN       VARCHAR2,
                               I_location        IN       ALLOC_DETAIL.TO_LOC%TYPE,
                               I_loc_type        IN       ALLOC_DETAIL.TO_LOC_TYPE%TYPE,
                               I_qty_allocated   IN       ALLOC_DETAIL.QTY_ALLOCATED%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XALLOC_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'To Location');
      return FALSE;
   end if;
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'To Location Type');
      return FALSE;
   end if;
   if I_loc_type NOT in ('S', 'W') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE',null,null,null);
      return FALSE;
   end if;
   if I_message_type in (RMSSUB_XALLOC.LP_cre_type, RMSSUB_XALLOC.LP_dtl_cre_type,
                         RMSSUB_XALLOC.LP_dtl_mod_type) then
      if I_qty_allocated is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Qty Allocated');
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ALLOC_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XAlloc_REC",
                            I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XALLOC_VALIDATE.CHECK_ALLOC_EXISTS';
   L_exist        BOOLEAN      := FALSE;
   L_var          VARCHAR2(1);

   cursor C_CHECK_ALLOC_DETAIL is
      select 1
        from TABLE(cast(I_message.xallocdtl_tbl as "RIB_XAllocDtl_TBL")) tbl
       where not exists (select 1
                           from alloc_header ah,
                                alloc_detail ad
                          where ah.alloc_no = I_message.alloc_no
                            and ad.alloc_no = ah.alloc_no
                            and ah.wh = I_message.from_loc
                            and ad.to_loc = tbl.to_loc
                            and rownum = 1);
BEGIN
      if not ALLOC_VALIDATE_SQL.EXIST(O_error_message,
                                      I_message.alloc_no,
                                      L_exist) then
         return FALSE;
      end if;

   if I_message_type != RMSSUB_XALLOC.LP_cre_type then
      if not L_exist then
         O_error_message := SQL_LIB.CREATE_MSG('ALLOC_NOT_EXIST', I_message.alloc_no,null,null);
         return FALSE;
      end if;

      if I_message_type = RMSSUB_XALLOC.LP_dtl_mod_type then
         open C_CHECK_ALLOC_DETAIL;
         fetch C_CHECK_ALLOC_DETAIL into L_var;
         if C_CHECK_ALLOC_DETAIL%FOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_LOC',null,null,null);
            close C_CHECK_ALLOC_DETAIL;
            return FALSE;
         end if;
         close C_CHECK_ALLOC_DETAIL;
      end if;
   elsif I_message_type = RMSSUB_XALLOC.LP_cre_type then
      if L_exist then
         O_error_message := SQL_LIB.CREATE_MSG('ALLOC_EXIST', I_message.alloc_no,null,null);
         return FALSE;
      end if;
   end if;
   O_error_message := NULL;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ALLOC_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ALLOC_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XAllocRef_REC",
                            I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XALLOC_VALIDATE.CHECK_ALLOCATION_EXISTS';
   L_exist        BOOLEAN      := FALSE;
   L_var          VARCHAR2(1);

   cursor C_CHECK_ALLOC_DETAIL is
      select 1
        from TABLE(cast(I_message.xallocdtlref_tbl as "RIB_XAllocDtlRef_TBL")) tbl
       where not exists (select 1
                           from alloc_header ah,
                                alloc_detail ad
                          where ah.alloc_no = I_message.alloc_no
                            and ad.alloc_no = ah.alloc_no
                            and ad.to_loc = tbl.to_loc
                            and rownum = 1);
BEGIN
   if not ALLOC_VALIDATE_SQL.EXIST(O_error_message,
                                   I_message.alloc_no,
                                   L_exist) then
      return FALSE;
   end if;

   if not L_exist then
      O_error_message := SQL_LIB.CREATE_MSG('ALLOC_NOT_EXIST', I_message.alloc_no,null,null);
      return FALSE;
   end if;
   if I_message_type = RMSSUB_XALLOC.LP_dtl_del_type then
      open C_CHECK_ALLOC_DETAIL;
      fetch C_CHECK_ALLOC_DETAIL into L_var;
      if C_CHECK_ALLOC_DETAIL%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOC',null,null,null);
         close C_CHECK_ALLOC_DETAIL;
         return FALSE;
      end if;
      close C_CHECK_ALLOC_DETAIL;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ALLOC_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_IN_PROGRESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_alloc_no        IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                           I_to_loc          IN       ALLOC_DETAIL.TO_LOC%TYPE,
                           I_to_loc_type     IN       ALLOC_DETAIL.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XALLOC_VALIDATE.CHECK_IN_PROGRESS';
   L_in_progress  VARCHAR2(1)  := 'N';

BEGIN

   if NOT ALLOC_ATTRIB_SQL.CHECK_IN_PROGRESS(O_error_message,
                                             L_in_progress,
                                             I_alloc_no,
                                             I_to_loc,
                                             I_to_loc_type) then
      return FALSE;
   end if;

   if L_in_progress = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('ALLOC_IN_PROGRESS',
                                            I_alloc_no,
                                            I_to_loc,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_IN_PROGRESS;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DEFAULT_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_alloc_rec       IN OUT   ALLOCATION_SQL.ALLOC_REC)
   RETURN BOOLEAN IS

   L_program VARCHAR2(50)  := 'RMSSUB_XALLOC_VALIDATE.POPULATE_DEFAULT_FIELDS';
   L_exists  BOOLEAN;

   cursor C_GET_ORDER_INFO is
      select po_type,
             'PREDIST',
             decode(status, 'W', 'W', 
                            'S', 'W', 
                            'A', 'A')
        from ordhead
       where order_no = I_alloc_rec.header.order_no;

BEGIN

   I_alloc_rec.header.alloc_method := SYSTEM_OPTIONS_SQL.GP_system_options_row.alloc_method;

   if I_alloc_rec.header.order_no is not null then
      --Verify wh and item are on order
      if not ORDER_ITEM_VALIDATE_SQL.EXIST_ORDLOC(O_error_message,
                                                  L_exists,
                                                  I_alloc_rec.header.order_no,
                                                  I_alloc_rec.header.item,
                                                  I_alloc_rec.header.wh,
                                                  'W') then
         return FALSE;
      end if;
      if not L_exists then
         O_error_message := SQL_LIB.CREATE_MSG('ORD_LOC_NOT_EXIST',
                                               I_alloc_rec.header.item,
                                               I_alloc_rec.header.wh,
                                               I_alloc_rec.header.order_no);
         return FALSE;
      end if;
      -- po type and order type for cross-dock alloc.
      open C_GET_ORDER_INFO;
      fetch C_GET_ORDER_INFO into I_alloc_rec.header.po_type,
                                  I_alloc_rec.header.order_type,
                                  I_alloc_rec.header.status;
      if C_GET_ORDER_INFO%NOTFOUND then
         close C_GET_ORDER_INFO;
         O_error_message := SQL_LIB.CREATE_MSG('ORDER_NOT_EXIST',
                                               I_alloc_rec.header.order_no,
                                               null,
                                               null);
         return FALSE;
      end if;
      close C_GET_ORDER_INFO;

   else
      I_alloc_rec.header.order_type := SYSTEM_OPTIONS_SQL.GP_system_options_row.default_order_type;
      I_alloc_rec.header.status := 'A';
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_DEFAULT_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT             RTK_ERRORS.RTK_TEXT%TYPE,
                         O_alloc_rec       IN OUT    NOCOPY   ALLOCATION_SQL.ALLOC_REC,
                         I_message         IN                 "RIB_XAlloc_REC",
                         I_message_type    IN                 VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XALLOC_VALIDATE.POPULATE_RECORD';
   L_item             ITEM_MASTER.ITEM%TYPE := I_message.item;
   L_order_no         ORDHEAD.ORDER_NO%TYPE := I_message.order_no;
   L_qty_allocated    ALLOC_DETAIL.QTY_ALLOCATED%TYPE := 0;
   L_avail_qty        ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_from_loc_type    ITEM_LOC_SOH.LOC_TYPE%TYPE := 'W';
   L_to_loc_type      ITEM_LOC_SOH.LOC_TYPE%TYPE;
   L_var              VARCHAR2(1);
   L_status           ALLOC_HEADER.STATUS%TYPE := NULL;

--The cursors takes into account the following updates:
--item_loc_soh.stock_on_hand for pack at the warehouse
--item_loc_soh.pack_comp_soh for component items at the warehouse
--item_loc_soh.stock_on_hand for component items at the store
--item_loc_soh.stock_on_hand for regular items at both stores and warehouses.

   cursor C_EXISTING_LOCS is
      select /*+ FIRST_ROWS */
             il.item,
             il.loc,
             il.rowid,
             temp.qty_allocated
        from (select L_item item,
                     to_loc,
                     to_loc_type,
                     t.qty_allocated
                from TABLE(cast(I_message.xallocdtl_tbl as "RIB_XAllocDtl_TBL")) t
               where O_alloc_rec.pack_ind = 'N'           --Condition to fetch the regular item
                  or O_alloc_rec.to_loc_type = 'W'        --and Pack Item in case of Warehouses.
              UNION ALL
              select v.item,
                     to_loc,
                     to_loc_type,
                     DECODE(im.inventory_ind, 'Y', t.qty_allocated*v.qty, 0) qty_allocated
                from TABLE(cast(I_message.xallocdtl_tbl as "RIB_XAllocDtl_TBL")) t,
                     v_packsku_qty v,
                     item_master im
               where O_alloc_rec.pack_ind = 'Y'         --Fetches the component Items.
                 and v.pack_no = L_item
                 and im.item = v.item) temp,
             item_loc_soh il
       where il.loc      = temp.to_loc
         and il.loc_type = temp.to_loc_type
         and il.item     = temp.item;

   cursor C_NEW_LOCS is
      select temp.item,
             temp.to_loc,
             temp.to_loc_type,
             temp.qty_allocated
        from (select L_item item,
                     to_loc,
                     to_loc_type,
                     case  --The stock is not maintained for packs at Stores.
                        when O_alloc_rec.pack_ind = 'Y' and O_alloc_rec.to_loc_type = 'S' then 0
                        else qty_allocated
                     end qty_allocated
                from TABLE(cast(I_message.xallocdtl_tbl as "RIB_XAllocDtl_TBL")) t
              UNION ALL
              select v.item,
                     to_loc,
                     to_loc_type,
                     DECODE(im.inventory_ind, 'Y',t.qty_allocated*v.qty, 0) qty_allocated
                from TABLE(cast(I_message.xallocdtl_tbl as "RIB_XAllocDtl_TBL")) t,
                     v_packsku_qty v,
                     item_master im
               where O_alloc_rec.pack_ind = 'Y'          --Fetches the component Items.
                 and v.pack_no = L_item
                 and im.item = v.item) temp
       where not exists (select 1
                           from item_loc_soh i
                          where i.item = temp.item
                            and i.loc  = temp.to_loc
                            and rownum = 1);

   cursor C_GET_ALLOC_HEADER_STATUS is
      select status
        from alloc_header
       where alloc_no = I_message.alloc_no;

   cursor C_CHECK_QTY_GTR_ZERO is
      select 1
        from alloc_detail ad,
             TABLE(cast(I_message.xallocdtl_tbl as "RIB_XAllocDtl_TBL")) passed_in
       where ad.alloc_no = I_message.alloc_no
         and ad.to_loc   = passed_in.to_loc
         and (ad.qty_allocated + passed_in.qty_allocated) <= 0
         and rownum      = 1;

BEGIN
   O_alloc_rec.header.alloc_no         := I_message.alloc_no;
   O_alloc_rec.header.alloc_desc       := I_message.alloc_desc;
   O_alloc_rec.header.item             := L_item;
   O_alloc_rec.header.release_date     := TRUNC(I_message.release_date);
   O_alloc_rec.header.wh               := I_message.from_loc;
   
   open  C_GET_ALLOC_HEADER_STATUS;
   fetch C_GET_ALLOC_HEADER_STATUS into L_status;
   close C_GET_ALLOC_HEADER_STATUS;

   if I_message_type in(RMSSUB_XALLOC.LP_dtl_cre_type, RMSSUB_XALLOC.LP_mod_type, RMSSUB_XALLOC.LP_dtl_mod_type) then
      if L_status = 'C' then
         O_error_message := SQL_LIB.CREATE_MSG('MOD_NOT_ALLOWED',
                                               O_alloc_rec.header.alloc_no,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   if I_message.origin_ind is NULL then
      O_alloc_rec.header.origin_ind := 'EG';
   else
      O_alloc_rec.header.origin_ind := I_message.origin_ind;
   end if;

   -- If order no is populated, then we are dealing with a X-dock allocation
   -- item-loc qty's are not maintained for X-dock allocations.
   O_alloc_rec.header.order_no         := L_order_no;

   --default fields for create message only.
   if I_message_type = RMSSUB_XALLOC.LP_cre_type then
      if not POPULATE_DEFAULT_FIELDS(O_error_message,
                                     O_alloc_rec) then
         return FALSE;
      end if;
   else
      open C_GET_ALLOC_HEADER_STATUS;
      fetch C_GET_ALLOC_HEADER_STATUS into O_alloc_rec.header.status;
      close C_GET_ALLOC_HEADER_STATUS;
   end if;

   --- if order_no is populated then there is no need for current available validation
   --- as allocation is a X-dock allocation
   if L_order_no is NULL then
      L_avail_qty  := 0;
      ---
      if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(O_error_message,
                                                    L_avail_qty,
                                                    L_item,
                                                    I_message.from_loc,
                                                    L_from_loc_type) = FALSE then
         return FALSE;
      end if;
   elsif L_order_no is not null then
      if GET_ORDER_CURRENT_AVAIL(O_error_message,
                                 L_avail_qty,
                                 L_item,
                                 I_message.from_loc,
                                 L_from_loc_type,
                                 L_order_no) = FALSE then
         return FALSE;
      end if;
   end if;

   -- initialize table
   O_alloc_rec.alloc_detail.to_locs           := LOC_TBL();
   O_alloc_rec.alloc_detail.to_loc_types      := INDICATOR_TBL();
   O_alloc_rec.alloc_detail.qtys              := QTY_TBL();
   O_alloc_rec.alloc_detail.in_store_dates    := DATE_TBL();

   O_alloc_rec.new_itemloc.items        := ITEM_TBL();
   O_alloc_rec.new_itemloc.loc_types    := LOC_TYPE_TBL();
   O_alloc_rec.new_itemloc.locations    := LOC_TBL();
   O_alloc_rec.new_itemloc.units        := QTY_TBL();

   O_alloc_rec.itemloc.items_to_update := ITEM_TBL();
   O_alloc_rec.itemloc.locs_to_update := LOC_TBL();
   O_alloc_rec.itemloc.qtys_to_update := QTY_TBL();
   O_alloc_rec.itemloc.rows_to_update := ALLOCATION_SQL.ROWID_TBL();

   -- process details
   if I_message_type in (RMSSUB_XALLOC.LP_cre_type, RMSSUB_XALLOC.LP_dtl_cre_type) then
      if I_message.xallocdtl_tbl is not null then
         O_alloc_rec.total_qty_allocated := 0;
         --To-locs on an allocation are either all stores or all warehouses
         O_alloc_rec.to_loc_type := I_message.xallocdtl_tbl(1).to_loc_type;

         FOR i in I_message.xallocdtl_tbl.first..I_message.xallocdtl_tbl.last LOOP
            if not CHECK_REQUIRED_FIELDS(O_error_message,
                                         I_message_type,
                                         I_message.xallocdtl_tbl(i).to_loc,
                                         I_message.xallocdtl_tbl(i).to_loc_type,
                                         I_message.xallocdtl_tbl(i).qty_allocated) then
               return FALSE;
            end if;

            L_qty_allocated := I_message.xallocdtl_tbl(i).qty_allocated;

            if L_qty_allocated != ROUND(L_qty_allocated,0) then
               O_error_message := SQL_LIB.CREATE_MSG('QTY_MUST_BE_INTEGER',null,null,null);
               return FALSE;
            end if;
            if L_qty_allocated <= 0 then
               O_error_message := SQL_LIB.CREATE_MSG('ALLOC_QTY_ZERO',null,null,null);
               return FALSE;
            end if;

            O_alloc_rec.alloc_detail.to_locs.EXTEND;
            O_alloc_rec.alloc_detail.to_loc_types.EXTEND;
            O_alloc_rec.alloc_detail.qtys.EXTEND;
            O_alloc_rec.alloc_detail.in_store_dates.EXTEND;
            O_alloc_rec.alloc_detail.to_locs(i)         := I_message.xallocdtl_tbl(i).to_loc;
            O_alloc_rec.alloc_detail.to_loc_types(i)    := I_message.xallocdtl_tbl(i).to_loc_type;

            O_alloc_rec.alloc_detail.qtys(i)            := L_qty_allocated;

            O_alloc_rec.alloc_detail.in_store_dates(i)  := I_message.xallocdtl_tbl(i).in_store_date;
            O_alloc_rec.total_qty_allocated             := O_alloc_rec.total_qty_allocated + L_qty_allocated;
         end LOOP;

         if L_order_no is NULL and O_alloc_rec.total_qty_allocated > L_avail_qty then
            O_error_message := SQL_LIB.CREATE_MSG('QTY_AVAIL_ST_ALLOC',
                                                  L_item,
                                                  I_message.from_loc,
                                                  L_avail_qty);
            return FALSE;
         
	 elsif L_order_no is NOT NULL then
            if (L_avail_qty - O_alloc_rec.total_qty_allocated) < 0 then
               O_error_message := SQL_LIB.CREATE_MSG('QTY_AVAIL_PO_ALLOC',
                                                     L_item,
                                                     L_order_no,
                                                     L_avail_qty);
               return FALSE;
            end if; 
         end if;    
         --get existing locs to update, unless xdock allocation.
         if L_order_no is null then
            open C_EXISTING_LOCS;
            fetch C_EXISTING_LOCS bulk collect into O_alloc_rec.itemloc.items_to_update,
                                                    O_alloc_rec.itemloc.locs_to_update,
                                                    O_alloc_rec.itemloc.rows_to_update,
                                                    O_alloc_rec.itemloc.qtys_to_update;
            close C_EXISTING_LOCS;
         end if;-- order_no is NULL

         open C_NEW_LOCS;
         fetch C_NEW_LOCS bulk collect into O_alloc_rec.new_itemloc.items,
                                            O_alloc_rec.new_itemloc.locations,
                                            O_alloc_rec.new_itemloc.loc_types,
                                            O_alloc_rec.new_itemloc.units;
         close C_NEW_LOCS;

      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',null,null,null);
         return FALSE;
      end if;

   elsif I_message_type = RMSSUB_XALLOC.LP_dtl_mod_type then

      if I_message.xallocdtl_tbl is not null then
         open C_CHECK_QTY_GTR_ZERO;
         fetch C_CHECK_QTY_GTR_ZERO into L_var;

         if C_CHECK_QTY_GTR_ZERO%FOUND then -- mod qtys will move a detail below zero
            close C_CHECK_QTY_GTR_ZERO;
            O_error_message := SQL_LIB.CREATE_MSG('ALLOC_QTY_NOT_ZERO',null,null,null);
            return FALSE;
         end if;

         close C_CHECK_QTY_GTR_ZERO;
         O_alloc_rec.total_qty_allocated := 0;
         --To-locs on an allocation are either all stores or all warehouses
         O_alloc_rec.to_loc_type := I_message.xallocdtl_tbl(1).to_loc_type;
         FOR i in I_message.xallocdtl_tbl.first..I_message.xallocdtl_tbl.last LOOP

            if not CHECK_REQUIRED_FIELDS(O_error_message,
                                         I_message_type,
                                         I_message.xallocdtl_tbl(i).to_loc,
                                         I_message.xallocdtl_tbl(i).to_loc_type,
                                         I_message.xallocdtl_tbl(i).qty_allocated) then
               return FALSE;
            end if;

            if not CHECK_IN_PROGRESS(O_error_message,
                                     O_alloc_rec.header.alloc_no,
                                     I_message.xallocdtl_tbl(i).to_loc,
                                     I_message.xallocdtl_tbl(i).to_loc_type) then
               return FALSE;
            end if;

            L_qty_allocated := I_message.xallocdtl_tbl(i).qty_allocated;

            if L_qty_allocated != ROUND(L_qty_allocated) then
               O_error_message := SQL_LIB.CREATE_MSG('QTY_MUST_BE_INTEGER',null,null,null);
               return FALSE;
            end if;

            O_alloc_rec.alloc_detail.to_locs.EXTEND;
            O_alloc_rec.alloc_detail.to_loc_types.EXTEND;
            O_alloc_rec.alloc_detail.qtys.EXTEND;
            O_alloc_rec.alloc_detail.in_store_dates.EXTEND;
            O_alloc_rec.alloc_detail.to_locs(i)   := I_message.xallocdtl_tbl(i).to_loc;
            O_alloc_rec.alloc_detail.to_loc_types(i) := I_message.xallocdtl_tbl(i).to_loc_type;
            O_alloc_rec.alloc_detail.qtys(i)         := L_qty_allocated;
            O_alloc_rec.alloc_detail.in_store_dates(i) := I_message.xallocdtl_tbl(i).in_store_date;
            O_alloc_rec.total_qty_allocated   := O_alloc_rec.total_qty_allocated + L_qty_allocated;
         END LOOP;

         if L_order_no is NULL and O_alloc_rec.total_qty_allocated > L_avail_qty then
            O_error_message := SQL_LIB.CREATE_MSG('QTY_AVAIL_ST_ALLOC',
                                                   L_item,
                                                   I_message.from_loc,
                                                   L_avail_qty);
               return FALSE;
         
         elsif L_order_no is NOT NULL then
            if (L_avail_qty - O_alloc_rec.total_qty_allocated) < 0 then
               O_error_message := SQL_LIB.CREATE_MSG('QTY_AVAIL_PO_ALLOC',
                                                     L_item,
                                                     L_order_no,
                                                     L_avail_qty);
               return FALSE;
            end if; 
         end if;   

         --get existing locs to update, unless xdock allocation.
         if L_order_no is null then
            open C_EXISTING_LOCS;
            fetch C_EXISTING_LOCS bulk collect into O_alloc_rec.itemloc.items_to_update,
                                                    O_alloc_rec.itemloc.locs_to_update,
                                                    O_alloc_rec.itemloc.rows_to_update,
                                                    O_alloc_rec.itemloc.qtys_to_update;
            close C_EXISTING_LOCS;
         end if;

      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',null,null,null);
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_alloc_rec       OUT    NOCOPY   ALLOCATION_SQL.ALLOC_REC,
                         I_message         IN              "RIB_XAllocRef_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XALLOC_VALIDATE.POPULATE_RECORD';
   L_pack_comp_qty    QTY_TBL;
   L_status           ALLOC_HEADER.STATUS%TYPE := NULL;
   L_loc_priv         ITEM_LOC_SOH.LOC%TYPE :=NULL;
   L_to_loc_type      ITEM_LOC_SOH.LOC_TYPE%TYPE;
   L_item             ITEM_MASTER.ITEM%TYPE;

   cursor C_TO_LOC_TYPE is
      select ad.to_loc_type
        from alloc_detail ad
       where ad.alloc_no = O_alloc_rec.header.alloc_no
         and rownum =1;

   cursor C_GET_ALLOC_HEADER_STATUS is
      select status
        from alloc_header
       where alloc_no = I_message.alloc_no;

   cursor C_GET_PACK_IND is
      select im.pack_ind,
             ah.wh,
             ah.item,
             ah.order_no
        from item_master im,
             alloc_header ah
       where ah.alloc_no = I_message.alloc_no
         and ah.item = im.item;

--The cursors takes into account the following updates:
--item_loc_soh.stock_on_hand for pack at the warehouse
--item_loc_soh.pack_comp_soh for component items at the warehouse
--item_loc_soh.stock_on_hand for component items at the store
--item_loc_soh.stock_on_hand for regular items at both stores and warehouses.

   cursor C_EXISTING_LOCS is
      select il.item,
             il.loc,
             il.rowid,
             temp.qty,
             temp.pack_comp_qty
        from (select L_item item,
                     t.to_loc,
                     ad.qty_allocated*-1 qty,
                     1 pack_comp_qty
                from TABLE(cast(I_message.xallocdtlref_tbl as "RIB_XAllocDtlRef_TBL")) t,
                      alloc_detail ad,
                      alloc_header ah
               where ad.alloc_no = O_alloc_rec.header.alloc_no
                 and ah.alloc_no = ad.alloc_no
                 and ad.to_loc = t.to_loc
                 and (O_alloc_rec.pack_ind = 'N'          --Condition to fetch regular item
                     or O_alloc_rec.to_loc_type = 'W')    --And Pack Item for Warehouses.
              UNION ALL
              select v.item,
                     t.to_loc,
                     DECODE(im.inventory_ind, 'Y', ad.qty_allocated*-1, 0),
                     v.qty pack_comp_qty
                from TABLE(cast(I_message.xallocdtlref_tbl as "RIB_XAllocDtlRef_TBL")) t,
                      alloc_detail ad,
                      alloc_header ah,
                      item_master im,
                      v_packsku_qty v
               where ad.alloc_no = O_alloc_rec.header.alloc_no
                 and ah.alloc_no = ad.alloc_no
                 and ad.to_loc = t.to_loc
                 and v.pack_no = L_item
                 and im.item = v.item
                 and O_alloc_rec.pack_ind = 'Y') temp,   --Fetches component items in case of Packs
             item_loc_soh il
       where il.loc = temp.to_loc
         and il.item = temp.item
    order by il.loc;

   cursor C_ALL_LOCS is
      select il.item,
             il.loc,
             il.rowid,
             temp.qty,
             temp.pack_comp_qty
        from (select L_item item,
                     ad.to_loc,
                     ad.qty_allocated*-1 qty,
                     1 pack_comp_qty
                from alloc_detail ad,
                     alloc_header ah
               where ad.alloc_no = O_alloc_rec.header.alloc_no
                 and ah.alloc_no = ad.alloc_no
                 and (O_alloc_rec.pack_ind = 'N'          --Condition to fetch regular item
                     or O_alloc_rec.to_loc_type = 'W')    --And Pack Item for Warehouses.
              UNION ALL
              select v.item,
                     ad.to_loc,
                     DECODE(im.inventory_ind, 'Y', ad.qty_allocated*-1, 0) qty,
                     v.qty pack_comp_qty
                from alloc_detail ad,
                     alloc_header ah,
                     item_master im,
                     v_packsku_qty v
               where ad.alloc_no = O_alloc_rec.header.alloc_no
                 and ah.alloc_no = ad.alloc_no
                 and v.pack_no = L_item
                 and im.item = v.item
                 and O_alloc_rec.pack_ind = 'Y') temp,--Fetches component items in case of Packs.
             item_loc_soh il
       where il.loc = temp.to_loc
         and il.item = temp.item
    order by il.loc;

BEGIN
   O_alloc_rec.header.alloc_no    := I_message.alloc_no;
   O_alloc_rec.itemloc.items_to_update := ITEM_TBL();
   O_alloc_rec.itemloc.locs_to_update := LOC_TBL();
   O_alloc_rec.itemloc.qtys_to_update := QTY_TBL();
   O_alloc_rec.itemloc.rows_to_update := ALLOCATION_SQL.ROWID_TBL();
   open C_GET_ALLOC_HEADER_STATUS;
   fetch C_GET_ALLOC_HEADER_STATUS into O_alloc_rec.header.status;
   close C_GET_ALLOC_HEADER_STATUS;

   if O_alloc_rec.header.status = 'C' then
      O_error_message := SQL_LIB.CREATE_MSG('ALLOC_CLOSED',
                                            O_alloc_rec.header.alloc_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_PACK_IND;
   fetch C_GET_PACK_IND into O_alloc_rec.pack_ind,
                             O_alloc_rec.header.wh,
                             O_alloc_rec.header.item,
                             O_alloc_rec.header.order_no;
   close C_GET_PACK_IND;

   L_item := O_alloc_rec.header.item;

   open C_TO_LOC_TYPE;
   fetch C_TO_LOC_TYPE into O_alloc_rec.to_loc_type;
   close C_TO_LOC_TYPE;

   if I_message_type = RMSSUB_XALLOC.LP_dtl_del_type then
      if I_message.xallocdtlref_tbl is not null then

         -- initialize table
         O_alloc_rec.alloc_detail.to_locs      := LOC_TBL();
         O_alloc_rec.alloc_detail.to_loc_types := INDICATOR_TBL();

         FOR i in I_message.xallocdtlref_tbl.first..I_message.xallocdtlref_tbl.last LOOP
            if not CHECK_REQUIRED_FIELDS(O_error_message,
                                         I_message_type,
                                         I_message.xallocdtlref_tbl(i).to_loc,
                                         I_message.xallocdtlref_tbl(i).to_loc_type,
                                         null) then
               return FALSE;
            end if;
            if not CHECK_IN_PROGRESS(O_error_message,
                                     O_alloc_rec.header.alloc_no,
                                     I_message.xallocdtlref_tbl(i).to_loc,
                                     I_message.xallocdtlref_tbl(i).to_loc_type) then
               return FALSE;
            end if;

            O_alloc_rec.alloc_detail.to_locs.EXTEND;
            O_alloc_rec.alloc_detail.to_loc_types.EXTEND;
            O_alloc_rec.alloc_detail.to_locs(i)   := I_message.xallocdtlref_tbl(i).to_loc;
            O_alloc_rec.alloc_detail.to_loc_types(i) := I_message.xallocdtlref_tbl(i).to_loc_type;
         END LOOP;

         if O_alloc_rec.header.order_no is null then
            O_alloc_rec.total_qty_allocated := 0;
            open C_EXISTING_LOCS;
            fetch C_EXISTING_LOCS bulk collect into O_alloc_rec.itemloc.items_to_update,
                                                    O_alloc_rec.itemloc.locs_to_update,
                                                    O_alloc_rec.itemloc.rows_to_update,
                                                    O_alloc_rec.itemloc.qtys_to_update,
                                                    L_pack_comp_qty;
            close C_EXISTING_LOCS;
            FOR i in O_alloc_rec.itemloc.qtys_to_update.first..O_alloc_rec.itemloc.qtys_to_update.last LOOP
            -- total qty filled with allocated pack qty (not by component because updating warehouse where
            -- pack is allocated from).
               if i > 1 then
                  L_loc_priv := O_alloc_rec.itemloc.locs_to_update(i-1);
                  if L_loc_priv != O_alloc_rec.itemloc.locs_to_update(i) then
                     O_alloc_rec.total_qty_allocated := O_alloc_rec.total_qty_allocated + O_alloc_rec.itemloc.qtys_to_update(i);
                  end if;
               else
                  O_alloc_rec.total_qty_allocated  := O_alloc_rec.total_qty_allocated + O_alloc_rec.itemloc.qtys_to_update(i);
               end if;
               -- qty's to update is multiplied by pack qty because updated store where components allocated to.
               O_alloc_rec.itemloc.qtys_to_update(i) := O_alloc_rec.itemloc.qtys_to_update(i)*L_pack_comp_qty(i);
            END LOOP;
         end if;
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',null,null,null);
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XALLOC.LP_del_type then
      if not CHECK_IN_PROGRESS(O_error_message,
                               O_alloc_rec.header.alloc_no,
                               null,
                               null) then
         return FALSE;
      end if;
      if O_alloc_rec.header.order_no is null then
         O_alloc_rec.total_qty_allocated := 0;
         open C_ALL_LOCS;
         fetch C_ALL_LOCS bulk collect into O_alloc_rec.itemloc.items_to_update,
                                            O_alloc_rec.itemloc.locs_to_update,
                                            O_alloc_rec.itemloc.rows_to_update,
                                            O_alloc_rec.itemloc.qtys_to_update,
                                            L_pack_comp_qty;
         close C_ALL_LOCS;

         FOR i in O_alloc_rec.itemloc.qtys_to_update.first..O_alloc_rec.itemloc.qtys_to_update.last LOOP
              -- total qty filled with allocated pack qty (not by component because updating warehouse where
              -- pack is allocated from).
            if i > 1 then
               L_loc_priv := O_alloc_rec.itemloc.locs_to_update(i-1);
               if L_loc_priv != O_alloc_rec.itemloc.locs_to_update(i) then
                  O_alloc_rec.total_qty_allocated  := O_alloc_rec.total_qty_allocated + O_alloc_rec.itemloc.qtys_to_update(i);
               end if;
            else
               O_alloc_rec.total_qty_allocated  := O_alloc_rec.total_qty_allocated + O_alloc_rec.itemloc.qtys_to_update(i);
            end if;
            -- qty's to update is multiplied by pack qty because updated store where components allocated to.
            O_alloc_rec.itemloc.qtys_to_update(i) := O_alloc_rec.itemloc.qtys_to_update(i)*L_pack_comp_qty(i);
         END LOOP;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
---------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TO_LOC(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         I_message         IN              "RIB_XAlloc_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(50)               := 'RMSSUB_XALLOC_VALIDATE.VALIDATE_TO_LOC';
   L_to_loc             ITEM_LOC.LOC%TYPE          := NULL;
   L_to_loc_type        ITEM_LOC_SOH.LOC_TYPE%TYPE := NULL;
   L_count              NUMBER;

   cursor VALIDATE is
      select passed_in.to_loc, passed_in.to_loc_type
        from TABLE(cast(I_message.xallocdtl_tbl as "RIB_XAllocDtl_TBL")) passed_in
       where NOT EXISTS (select store
                           from store s
                          where s.store               = passed_in.to_loc
                            and passed_in.to_loc_type = 'S'
                            and s.stockholding_ind    = 'Y'
                         union all
                         select wh
                           from wh w
                          where w.wh                  = passed_in.to_loc
                            and passed_in.to_loc_type = 'W'
                            and w.stockholding_ind    = 'Y')
         and rownum = 1;

   cursor C_TO_LOC_TYPE is
      select count(distinct t.to_loc_type)
        from TABLE(cast(I_message.xallocdtl_tbl as "RIB_XAllocDtl_TBL")) t;

BEGIN
   if I_message_type in (RMSSUB_XALLOC.LP_cre_type, RMSSUB_XALLOC.LP_dtl_cre_type) then
      open VALIDATE;
      fetch VALIDATE into L_to_loc, L_to_loc_type;
      if VALIDATE%FOUND then
         close VALIDATE;
         if L_to_loc_type NOT in ('S','W') then
            O_error_message := SQL_LIB.CREATE_MSG('INVAVAIL_INVALID_LOC_TYPE',L_to_loc_type,NULL,NULL);
         else
         O_error_message := SQL_LIB.CREATE_MSG('TO_LOC_STOCKHOLDING_S_W',L_to_loc,NULL,NULL);
         end if;
         return FALSE;
      end if;
      close VALIDATE;

      open C_TO_LOC_TYPE;
      fetch C_TO_LOC_TYPE into L_count;
      if L_count > 1 then
         close C_TO_LOC_TYPE;
         O_error_message := SQL_LIB.CREATE_MSG('ALLOC_ST_OR_WH', NULL, NULL, NULL);
         return FALSE;
      end if;
      close C_TO_LOC_TYPE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_TO_LOC;
---------------------------------------------------------------------------------------------------------
END RMSSUB_XALLOC_VALIDATE;
/