
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STOREORDER_SQL AUTHID CURRENT_USER AS


/* Function and Procedure Bodies */
-------------------------------------------------------------------------
FUNCTION PROCESS (O_error_message  IN OUT  VARCHAR2,
                  I_store          IN      STORE_ORDERS.STORE%TYPE,
                  I_item           IN      STORE_ORDERS.ITEM%TYPE,
                  I_need_qty       IN      STORE_ORDERS.NEED_QTY%TYPE,
                  I_uop            IN      UOM_CLASS.UOM%TYPE,
                  I_need_date      IN      STORE_ORDERS.NEED_DATE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
FUNCTION INIT(O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------
FUNCTION FLUSH(O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------
END STOREORDER_SQL;
/
