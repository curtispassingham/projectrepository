
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_RECEIPT AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
FUNCTION CONSUME (O_status_code          IN OUT  VARCHAR2,
                  O_error_message        IN OUT  VARCHAR2,
                  I_rib_receiptdesc_rec  IN      "RIB_ReceiptDesc_REC",
                  I_message_type         IN      VARCHAR2,
                  O_rib_otbdesc_rec         OUT  "RIB_OTBDesc_REC",
                  O_rib_error_tbl           OUT  RIB_ERROR_TBL)
return BOOLEAN;
--------------------------------------------------------------------------------
END RMSSUB_RECEIPT;
/
