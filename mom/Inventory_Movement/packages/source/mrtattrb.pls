CREATE OR REPLACE PACKAGE BODY MRT_ATTRIB_SQL AS

FUNCTION IS_NUMBER (I_string   IN   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION MRT_ITEM_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_diff          IN OUT VARCHAR2,
                       l_mrt_no        IN     MRT_ITEM.MRT_NO%TYPE)
   RETURN BOOLEAN IS

   L_Program VARCHAR2(64) := 'MRT_ATTRIB_SQL.MRT_ITEM_LIST';
   cursor C_COUNT_MRT_ITEM is
      select COUNT(mrt_no)
        from mrt_item
       where mrt_no = l_mrt_no;

   cursor C_COUNT_VMRT_ITEM is
      select COUNT(mrt_no)
        from v_mrt_item
       where mrt_no = l_mrt_no;

L_mrt_item  NUMBER(6);
L_vmrt_item NUMBER(6);

BEGIN
   if  l_mrt_no is not NULL then
      open c_count_mrt_item;
      fetch c_count_mrt_item into L_mrt_item;
      close c_count_mrt_item;

      open c_count_vmrt_item;
      fetch c_count_vmrt_item into L_vmrt_item;
      close c_count_vmrt_item;

      if NVL(L_vmrt_item,0) != NVL(L_mrt_item,0) then
         O_diff := 'Y';
      else
         O_diff := 'N';
      end if;

      return TRUE;
   else
      O_error_message := Sql_Lib.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mrt_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_Program,
                                            to_char(SQLCODE));
      return FALSE;


END MRT_ITEM_LIST;

-------------------------------------------------------------------------------------------------

FUNCTION GET_MRT_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_mrt_rec         IN OUT   MRT%ROWTYPE,
                      O_exists          IN OUT   BOOLEAN,
                      l_mrt_no          IN       MRT.MRT_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'MRT_ATTRIB_SQL.GET_MRT_INFO';

   cursor C_GET_ATTRIBUTES is
      select *
        from MRT
       where mrt_no = L_mrt_no;

BEGIN

   if l_mrt_no is NULL then
      O_error_message := Sql_Lib.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mrt_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   O_mrt_rec.mrt_no := NULL;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: ' || L_mrt_no);
   open C_GET_ATTRIBUTES;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: '|| L_mrt_no);
   fetch C_GET_ATTRIBUTES into O_mrt_rec;

   Sql_Lib.SET_MARK('CLOSE',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: '|| L_mrt_no);
   close C_GET_ATTRIBUTES;

   O_exists := (O_mrt_rec.mrt_no is not NULL);

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_MRT_INFO;
------------------------------------------------------------------------------------
FUNCTION GET_MRT_ITEM_ROW (O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           O_mrt_item      IN OUT MRT_ITEM%ROWTYPE,
                           I_mrt_no        IN     MRT_ITEM.MRT_NO%TYPE,
                           I_item          IN     MRT_ITEM.ITEM%TYPE)

RETURN BOOLEAN

IS

   L_program CONSTANT VARCHAR2(64) := 'MRT_ATTRIB_SQL.GET_MRT_ITEM_ROW';

   cursor C_MRI is
      select mri.*
        from mrt_item   mri
       where mri.mrt_no = I_mrt_no
         and mri.item   = I_item;

BEGIN

   if I_mrt_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_mrt_no',
                                            'NULL');
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_item',
                                            'NULL');
      return FALSE;
   end if;

   open  C_MRI;

   fetch C_MRI into O_mrt_item;

   O_exists := C_MRI%FOUND;

   close C_MRI;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END GET_MRT_ITEM_ROW;
------------------------------------------------------------------------------------
FUNCTION GET_MRT_ITEM_LOC_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_mrt_rec         IN OUT   MRT_ITEM_LOC%ROWTYPE,
                               O_exists          IN OUT   BOOLEAN,
                               I_mrt_no          IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                               I_item            IN       MRT_ITEM_LOC.ITEM%TYPE,
                               I_location        IN       MRT_ITEM_LOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'MRT_ATTRIB_SQL.GET_MRT_ITEM_INFO';

   cursor C_GET_ATTRIBUTES is
      select *
        from MRT_ITEM_LOC
       where mrt_no   = I_mrt_no
         and item     = I_item
         and location = I_location;

BEGIN

   if I_mrt_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mrt_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   O_mrt_rec.mrt_no := NULL;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ATTRIBUTES',
                    'MRT_ITEM_LOC',
                    'MRT_NO: '||I_mrt_no);
   open C_GET_ATTRIBUTES;

   Sql_Lib.SET_MARK('FETCH',
                    'C_GET_ATTRIBUTES',
                    'MRT_ITEM_LOC',
                    'MRT_NO: '||I_mrt_no);
   fetch C_GET_ATTRIBUTES into O_mrt_rec;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ATTRIBUTES',
                    'MRT_ITEM_LOC',
                    'MRT_NO: '||I_mrt_no);
   close C_GET_ATTRIBUTES;

   O_exists := (O_mrt_rec.mrt_no is not NULL);

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_MRT_ITEM_LOC_INFO;
------------------------------------------------------------------------------------
FUNCTION GET_MRT_ITEM_TOTALS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_total_return_avail_qty IN OUT   MRT_ITEM_LOC.RETURN_AVAIL_QTY%TYPE,
                             O_total_tsf_qty          IN OUT   MRT_ITEM_LOC.TSF_QTY%TYPE,
                             O_total_retail           IN OUT   MRT_ITEM_LOC.UNIT_RETAIL%TYPE,
                             O_total_received_qty     IN OUT   MRT_ITEM_LOC.RECEIVED_QTY%TYPE,
                             O_total_cost             IN OUT   MRT_ITEM_LOC.UNIT_COST%TYPE,
                             I_mrt_no                 IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                             I_item                   IN       MRT_ITEM_LOC.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'MRT_ATTRIB_SQL.GET_MRT_ITEM_TOTALS';

   cursor C_GET_MRT_ITEM_TOTALS is
      select NVL(SUM(return_avail_qty), 0) budgeted_qty,
             NVL(SUM(tsf_qty), 0) tsf_qty,
             NVL(SUM(tsf_qty * unit_retail), 0) total_retail,
             NVL(SUM(tsf_qty * tsf_cost), 0) total_cost,
             NVL(SUM(received_qty), 0) total_received_qty
        from MRT_ITEM_LOC
       where mrt_no = I_mrt_no
         and item   = I_item;

BEGIN

   if I_mrt_no is NULL then
      O_error_message := Sql_Lib.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mrt_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := Sql_Lib.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_MRT_ITEM_TOTALS',
                    'MRT_ITEM_LOC',
                    'MRT_NO: '||I_mrt_no);
   open C_GET_MRT_ITEM_TOTALS;

   Sql_Lib.SET_MARK('FETCH',
                    'C_GET_MRT_ITEM_TOTALS',
                    'MRT_ITEM_LOC',
                    'MRT_NO: '||I_mrt_no);
   fetch C_GET_MRT_ITEM_TOTALS into O_total_return_avail_qty,
                                    O_total_tsf_qty,
                                    O_total_retail,
                                    O_total_cost,
                                    O_total_received_qty;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_MRT_ITEM_TOTALS',
                    'MRT_ITEM_LOC',
                    'MRT_NO: '||I_mrt_no);
   close C_GET_MRT_ITEM_TOTALS;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_MRT_ITEM_TOTALS;
------------------------------------------------------------------------------------
----------------------------------------------------------------------------------
FUNCTION MRT_ITEM_TOTALS_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_diff          IN OUT VARCHAR2,
                                     I_mrt_no        IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN IS

   cursor C_COUNT_MRT is
      select nvl(count(mrt_no), 0)
        from mrt_item
       where mrt_no = I_mrt_no;

   cursor C_COUNT_VMRT is
      select nvl(count(mrt_no), 0)
        from v_mrt_item_totals
       where mrt_no = I_mrt_no;

   L_mrt      NUMBER(6) := NULL;
   L_vmrt     NUMBER(6) := NULL;
   L_program  VARCHAR2(64) := 'MRT_ATTRIB_SQL.MRT_ITEM_TOTALS_FILTER_LIST';

BEGIN

   if I_mrt_no is NULL then
      O_error_message := Sql_Lib.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mrt_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_COUNT_MRT;
   fetch C_COUNT_MRT into L_mrt;
   close C_COUNT_MRT;

   open C_COUNT_VMRT;
   fetch C_COUNT_VMRT into L_vmrt;
   close C_COUNT_VMRT;

   if L_vmrt != L_mrt then
      O_diff := 'Y';
   else
      O_diff := 'N';
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END MRT_ITEM_TOTALS_FILTER_LIST;
----------------------------------------------------------------------------------

FUNCTION MRT_ITEM_LOC_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_rec_exists     IN OUT  BOOLEAN,
                             I_mrt_no         IN      MRT_ITEM_LOC.MRT_NO%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)       := 'MRT_ATTRIB_SQL.MRT_ITEM_LOC_EXISTS';
   L_inv_status   INV_STATUS_QTY.INV_STATUS%TYPE;
   L_qty_avail    MRT_ITEM_LOC.TSF_QTY%TYPE;
   L_rec_exists   BOOLEAN;

   cursor C_CHECK_MRT_ITEM_LOC is
      select m.inventory_type,
             mrl.item,
             mrl.location,
             mrl.loc_type,
             mrl.tsf_qty
        from mrt m,
             mrt_item_loc mrl
       where m.mrt_no = I_mrt_no
         and mrl.mrt_no = m.mrt_no;

BEGIN

   if I_mrt_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_mrt_no',L_program,NULL);
      return FALSE;
   end if;

   FOR mrt_item_loc_rec IN c_check_mrt_item_loc LOOP
      if mrt_item_loc_rec.inventory_type = 'A' then
         L_inv_status := NULL;
      else
         L_inv_status := mrt_item_loc_rec.inventory_type;
      end if;
      ---
      if TRANSFER_SQL.QUANTITY_AVAIL(O_error_message,
                                     L_qty_avail,
                                     mrt_item_loc_rec.item,
                                     L_inv_status,
                                     mrt_item_loc_rec.loc_type,
                                     mrt_item_loc_rec.location) = FALSE then
         return FALSE;
      end if;
      ---
      if mrt_item_loc_rec.tsf_qty > L_qty_avail then
         O_error_message := SQL_LIB.CREATE_MSG('QTY_AVAIL_ST_TSF',
                                               mrt_item_loc_rec.item,
                                               mrt_item_loc_rec.location,
                                               to_char(L_qty_avail));
         return FALSE;
      end if;
      L_rec_exists := TRUE;
   END LOOP;

   if L_rec_exists then
      O_rec_exists := TRUE;
   else
      O_rec_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MRT_ITEM_LOC_EXISTS;
----------------------------------------------------------------------------------
FUNCTION GET_RTV_INFO_FOR_MRT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_rtv_rec       IN OUT RTV_HEAD%ROWTYPE,
                              O_exists        IN OUT BOOLEAN,
                              I_mrt_no        IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'MRT_ATTRIB_SQL.GET_RTV_INFO_FOR_MRT';

   cursor C_GET_ATTRIBUTES is
      select *
        from rtv_head
       where mrt_no = I_mrt_no;

BEGIN

   if I_mrt_no is NULL then
      O_error_message := Sql_Lib.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mrt_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: ' || I_mrt_no);
   open C_GET_ATTRIBUTES;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: '|| I_mrt_no);
   fetch C_GET_ATTRIBUTES into O_rtv_rec;

   O_exists := C_GET_ATTRIBUTES%FOUND;

   Sql_Lib.SET_MARK('CLOSE',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: '|| I_mrt_no);
   close C_GET_ATTRIBUTES;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_RTV_INFO_FOR_MRT;
------------------------------------------------------------------------------------
FUNCTION GET_TSF_INFO_FOR_MRT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_tsf_rec       IN OUT TSFHEAD%ROWTYPE,
                              O_exists        IN OUT BOOLEAN,
                              I_mrt_no        IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'MRT_ATTRIB_SQL.GET_TSF_INFO_FOR_MRT';

   cursor C_GET_ATTRIBUTES is
      select *
        from tsfhead
       where mrt_no = I_mrt_no;

BEGIN

   if I_mrt_no is NULL then
      O_error_message := Sql_Lib.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mrt_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: ' || I_mrt_no);
   open C_GET_ATTRIBUTES;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: '|| I_mrt_no);
   fetch C_GET_ATTRIBUTES into O_tsf_rec;

   O_exists := C_GET_ATTRIBUTES%FOUND;

   Sql_Lib.SET_MARK('CLOSE',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: '|| I_mrt_no);
   close C_GET_ATTRIBUTES;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_TSF_INFO_FOR_MRT;
------------------------------------------------------------------------------------
FUNCTION GET_MRT_ITEM_INFO_FOR_MRT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_mrt_item_rec  IN OUT MRT_ITEM%ROWTYPE,
                                   O_exists        IN OUT BOOLEAN,
                                   I_mrt_no        IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'MRT_ATTRIB_SQL.GET_MRT_ITEM_INFO_FOR_MRT';

   cursor C_GET_ATTRIBUTES is
      select *
        from mrt_item
       where mrt_no = I_mrt_no;

BEGIN

   if I_mrt_no is NULL then
      O_error_message := Sql_Lib.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mrt_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: ' || I_mrt_no);
   open C_GET_ATTRIBUTES;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: '|| I_mrt_no);
   fetch C_GET_ATTRIBUTES into O_mrt_item_rec;

   O_exists := C_GET_ATTRIBUTES%FOUND;

   Sql_Lib.SET_MARK('CLOSE',
                    'C_GET_ATTRIBUTES',
                    'MRT',
                    'MRT_NO: '|| I_mrt_no);
   close C_GET_ATTRIBUTES;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_MRT_ITEM_INFO_FOR_MRT;
--------------------------------------------------------------------------------
FUNCTION CHECK_MRT_TSF_SHIPPED_STATUS
   (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_shipped_status IN OUT BOOLEAN,
    I_mrt_no         IN     MRT.MRT_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'MRT_ATTRIB_SQL.CHECK_MRT_TSF_SHIPPED_STATUS';
   L_dummy     VARCHAR2(1);
   cursor C_SHIPPED_STATUS_EXISTS is
      select 1
        from tsfhead
       where mrt_no = I_mrt_no
         and status = 'S';

BEGIN

   if I_mrt_no is NULL then
      O_error_message := Sql_Lib.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mrt_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_SHIPPED_STATUS_EXISTS;
   fetch C_SHIPPED_STATUS_EXISTS into L_dummy;
   O_shipped_status := C_SHIPPED_STATUS_EXISTS%FOUND;
   close C_SHIPPED_STATUS_EXISTS;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END CHECK_MRT_TSF_SHIPPED_STATUS;
----------------------------------------------------------------------------------
FUNCTION CHECK_MRT_ITEM_LOCATIONS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_check             IN OUT   VARCHAR2,
                                   I_item_apply_type   IN       VARCHAR2,
                                   I_group_loc_type    IN       MRT_ITEM_LOC.LOC_TYPE%TYPE,
                                   I_group_loc_value   IN       VARCHAR2,
                                   I_mrt_no            IN       MRT_ITEM_LOC.MRT_NO%TYPE,
                                   I_item              IN       MRT_ITEM_LOC.ITEM%TYPE,
                                   I_inter_intra_ind   IN       VARCHAR2,
                                   I_tsf_entity_id     IN       V_TRANSFER_FROM_LOC.TSF_ENTITY_ID%TYPE,
                                   I_set_of_books_id   IN       TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID%TYPE)

   RETURN BOOLEAN IS

   L_program                        VARCHAR(64) := 'MRT_ATTRIB_SQL.CHECK_MRT_ITEM_LOCATIONS';
   L_system_options_rec             SYSTEM_OPTIONS%ROWTYPE;
   L_intercompany_transfer_basis    SYSTEM_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE;

   cursor C_CHECK_MRT_ITEM_LOCS is
      select 'X'
        from item_loc_soh ils,
             v_store s
       where s.stockholding_ind = 'Y'
         and district           = I_group_loc_value
         and ils.item           = I_item
         and ils.loc            = s.store
         and ils.loc_type       = 'S'
         and ils.stock_on_hand  > 0
         and exists (select *
                       from mrt_item_loc mrl
                      where mrl.mrt_no   = I_mrt_no
                        and mrl.item     = I_item
                        and mrl.location = s.store
                        and mrl.loc_type = 'S')
         and (I_inter_intra_ind is NULL
             or (I_inter_intra_ind = 'A'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id = I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id = I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store))
             or (I_inter_intra_ind = 'E'
                 and exists (select 1
                               from v_transfer_from_store vs
                              where L_intercompany_transfer_basis = 'T'
                                and vs.tsf_entity_id != I_tsf_entity_id
                                and s.store = vs.store
                              union all
                             select 1
                               from v_transfer_from_store vs,
                                    tsf_entity_org_unit_sob teo
                              where L_intercompany_transfer_basis = 'B'
                                and teo.set_of_books_id != I_set_of_books_id
                                and teo.tsf_entity_id = vs.tsf_entity_id
                                and teo.org_unit_id = vs.org_unit_id
                                and vs.store = s.store)));


BEGIN

   /* make sure required parameters are populated */
   if I_item_apply_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item_apply_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_group_loc_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_group_loc_type',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_group_loc_type in ('S','W') then
      if I_group_loc_value is NULL then
         O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              I_group_loc_value,
                                              L_program,
                                              NULL);
         return FALSE;
      end if;
   end if;

   if I_mrt_no is NULL then
   O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                        'I_mrt_no',
                                        L_program,
                                        NULL);
      return FALSE;
   end if;

   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;
   L_intercompany_transfer_basis := L_system_options_rec.intercompany_transfer_basis;
   ---
   if I_item_apply_type = 'S' then
      if I_group_loc_type = 'D' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_MRT_ITEM_LOCS',
                          'MRT_ITEM_LOC',
                          'MRT_NO: '||I_mrt_no);
         open C_CHECK_MRT_ITEM_LOCS;
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_MRT_ITEM_LOCS',
                          'MRT_ITEM_LOC',
                          'MRT_NO: '||I_mrt_no);
         fetch C_CHECK_MRT_ITEM_LOCS into O_check;
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_MRT_ITEM_LOCS',
                          'MRT_ITEM_LOC',
                          'MRT_NO: '||I_mrt_no);
         close C_CHECK_MRT_ITEM_LOCS;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CHECK_MRT_ITEM_LOCATIONS;
--------------------------------------------------------------------------------
FUNCTION CHECK_MRT_LOC_LOCALIZED(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_localized       IN OUT   VARCHAR2,
                                 O_country_id         OUT   COUNTRY_ATTRIB.COUNTRY_ID%TYPE,
                                 I_mrt_no          IN       MRT_ITEM_LOC.MRT_NO%TYPE)

RETURN BOOLEAN IS

   L_program           VARCHAR2(64)    := 'MRT_ATTRIB_SQL.CHECK_MRT_LOC_LOCALIZED';
   L_localized         VARCHAR2(1);
   L_l10n_exists_rec   L10N_EXISTS_REC := L10N_EXISTS_REC();

   cursor C_CHECK_MRT_LOCS_LOCALIZED IS
      select mvl.country_id
        from mrt_item_loc mrt,
             mv_l10n_entity mvl
       where mrt.mrt_no         = I_mrt_no
         and mvl.entity_id      = TO_CHAR(mrt.location)
         and mvl.entity         = 'LOC'
         and mvl.localized_ind  = 'Y';

BEGIN

   for c_rec in C_CHECK_MRT_LOCS_LOCALIZED loop
      L_l10n_exists_rec.procedure_key := 'CHECK_IF_BRAZIL_LOCALIZED';
      L_l10n_exists_rec.doc_type      := 'MRT';
      L_l10n_exists_rec.country_id    := c_rec.country_id;

      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                 L_l10n_exists_rec) = FALSE then
         return FALSE;
      end if;
      ---
      if L_l10n_exists_rec.exists_ind = 'Y' then
         O_country_id := L_l10n_exists_rec.country_id;
         O_localized  := L_l10n_exists_rec.exists_ind;
         return TRUE;
      end if;
      ---
   end loop;

   O_country_id := L_l10n_exists_rec.country_id;
   O_localized  := 'N';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CHECK_MRT_LOC_LOCALIZED;
--------------------------------------------------------------------------------
FUNCTION GET_WH_TOTALS(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_error_flag                    IN OUT   VARCHAR2,
                       O_item                          IN OUT   MRT_ITEM.ITEM%TYPE,
                       IO_budget_return_qty            IN OUT   NUMBER,
                       IO_received_qty                 IN OUT   NUMBER,
                       IO_tsf_qty                      IN OUT   MRT_ITEM_LOC.TSF_QTY%TYPE,
                       IO_total_retail                 IN OUT   NUMBER,
                       IO_supplier_total_unit_retail   IN OUT   NUMBER,
                       IO_primary_total_unit_retail    IN OUT   NUMBER,
                       IO_total_cost                   IN OUT   NUMBER,
                       IO_total_price                  IN OUT   NUMBER,
                       IO_supplier_total_cost          IN OUT   NUMBER,
                       IO_primary_total_cost           IN OUT   NUMBER,
                       IO_supplier_total_price         IN OUT   NUMBER,
                       IO_primary_total_price          IN OUT   NUMBER,
                       IO_tot_tsf_qty                  IN OUT   NUMBER,
                       IO_tot_total_retail             IN OUT   NUMBER,
                       IO_tot_supp_total_unit_retail   IN OUT   NUMBER,
                       IO_tot_pri_total_unit_retail    IN OUT   NUMBER,
                       IO_tot_total_cost               IN OUT   NUMBER,
                       IO_tot_supplier_total_cost      IN OUT   NUMBER,
                       IO_tot_primary_total_cost       IN OUT   NUMBER,
                       IO_tot_total_price              IN OUT   NUMBER,
                       IO_tot_supplier_total_price     IN OUT   NUMBER,
                       IO_tot_primary_total_price      IN OUT   NUMBER,
                       I_item                          IN       MRT_ITEM.ITEM%TYPE,
                       I_include_wh_inv                IN       MRT.INCLUDE_WH_INV%TYPE,
                       I_inventory_type                IN       MRT.INVENTORY_TYPE%TYPE,
                       I_wh                            IN       MRT.WH%TYPE,
                       I_supplier                      IN       MRT.SUPPLIER%TYPE,
                       I_rtv_cost                      IN       MRT_ITEM.RTV_COST%TYPE,
                       I_currency_code                 IN       MRT.CURRENCY_CODE%TYPE,
                       I_sups_currency_code            IN       MRT.CURRENCY_CODE%TYPE,
                       I_prim_currency_code            IN       MRT.CURRENCY_CODE%TYPE,
                       I_restock_pct                   IN       MRT_ITEM.RESTOCK_PCT%TYPE,
                       I_selected_ind                  IN       MRT_ITEM.SELECTED_IND%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64)    := 'MRT_ATTRIB_SQL.GET_WH_TOTALS';

   L_available                 INV_STATUS_QTY.QTY%TYPE := 0;
   L_wh_total_cost_price       MRT_ITEM_LOC.TSF_PRICE%TYPE := 0;
   L_wh_supp_total_cost_price  MRT_ITEM_LOC.TSF_PRICE%TYPE := 0;
   L_wh_prim_total_cost_price  MRT_ITEM_LOC.TSF_PRICE%TYPE := 0;
   L_supp_rtv_cost             MRT_ITEM.RTV_COST%TYPE := 0;
   L_prim_rtv_cost             MRT_ITEM.RTV_COST%TYPE := 0;
   L_wh_total_unit_retail      MRT_ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_wh_supp_total_unit_retail MRT_ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_wh_prim_total_unit_retail MRT_ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_wh_supp_unit_retail       MRT_ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_wh_prim_unit_retail       MRT_ITEM_LOC.UNIT_RETAIL%TYPE := 0;
   L_av_cost                   ITEM_LOC_SOH.AV_COST%TYPE;
   L_unit_cost                 ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_wh_unit_retail            ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom               ITEM_LOC.SELLING_UOM%TYPE;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_inventory_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_inventory_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   O_error_flag := 'N';
   O_item := NULL;
   if I_include_wh_inv = 'Y' then
      if I_inventory_type = 'A' then
         if TRANSFER_SQL.QUANTITY_AVAIL(O_error_message,
                                        L_available,
                                        I_item,
                                        NULL,
                                        'W',
                                        I_wh) = FALSE then
            return FALSE;
         end if;
         ---
      else
         if TRANSFER_SQL.QUANTITY_AVAIL(O_error_message,
                                        L_available,
                                        I_item,
                                        I_inventory_type,
                                        'W',
                                        I_wh) = FALSE then
            return FALSE;
         end if;
         ---
      end if;

      if I_supplier is not null then
         if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                     I_item,
                                                     I_wh,
                                                     'W',
                                                     L_av_cost,
                                                     L_unit_cost,
                                                     L_wh_unit_retail,
                                                     L_selling_unit_retail,
                                                     L_selling_uom) = FALSE then
            O_error_flag := 'Y';
            O_item := I_item;
            return FALSE;
         end if;
         ---
      end if;
      ---

      if NVL(I_rtv_cost,0) > 0 then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 I_rtv_cost,
                                 I_currency_code,
                                 I_sups_currency_code,--supplier currency code
                                 L_supp_rtv_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.CONVERT(O_error_message,
                                 I_rtv_cost,
                                 I_currency_code,
                                 I_prim_currency_code,--primary currency code
                                 L_prim_rtv_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
         ---
      --Convert unit_retail
      if NVL(L_wh_unit_retail,0) > 0 then
         --supplier
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_wh_unit_retail,
                                 I_currency_code,
                                 I_sups_currency_code,--supplier currency code
                                 L_wh_supp_unit_retail,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
         ---
         --primary
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_wh_unit_retail,
                                 I_currency_code,
                                 I_prim_currency_code,--primary currency code
                                 L_wh_prim_unit_retail,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
      --Calculate the total cost/total price and retail for the warehouse
      L_wh_total_cost_price       := NVL(L_available,0) * (nvl(I_rtv_cost,0) * (1 - (nvl(I_restock_pct,0)/100)));
      L_wh_total_unit_retail      := NVL(L_available,0) * nvl(L_wh_unit_retail,0);


      L_wh_supp_total_cost_price  := NVL(L_available,0) * (nvl(L_supp_rtv_cost,0) * (1 - (nvl(I_restock_pct,0)/100)));
      L_wh_prim_total_cost_price  := NVL(L_available,0) * (nvl(L_prim_rtv_cost,0) * (1 - (nvl(I_restock_pct,0)/100)));
      L_wh_supp_total_unit_retail := NVL(L_available,0) * nvl(L_wh_supp_unit_retail,0);
      L_wh_prim_total_unit_retail := NVL(L_available,0) * nvl(L_wh_prim_unit_retail,0);
      end if;
      ---
   end if;
   ---
   IO_budget_return_qty           := IO_budget_return_qty          + NVL(L_available,0);
   IO_received_qty                := IO_received_qty               + NVL(L_available,0);
   IO_tsf_qty                     := IO_tsf_qty                    + NVL(L_available,0);
   IO_total_retail                := IO_total_retail               + L_wh_total_unit_retail;
   IO_supplier_total_unit_retail  := IO_supplier_total_unit_retail + L_wh_supp_total_unit_retail;
   IO_primary_total_unit_retail   := IO_primary_total_unit_retail  + L_wh_prim_total_unit_retail;
   IO_total_cost                  := IO_total_cost                 + L_wh_total_cost_price;
   IO_total_price                 := IO_total_price                + L_wh_total_cost_price;
   IO_supplier_total_cost         := IO_supplier_total_cost        + L_wh_supp_total_cost_price;
   IO_primary_total_cost          := IO_primary_total_cost         + L_wh_prim_total_cost_price;
   IO_supplier_total_price        := IO_supplier_total_price       + L_wh_supp_total_cost_price;
   IO_primary_total_price         := IO_primary_total_price        + L_wh_prim_total_cost_price;
   ---
   if I_selected_ind = 'Y' then
      IO_tot_tsf_qty                      := NVL(IO_tot_tsf_qty,0)                    + IO_tsf_qty;
      IO_tot_total_retail                 := NVL(IO_tot_total_retail,0)               + IO_total_retail;
      IO_tot_supp_total_unit_retail       := NVL(IO_tot_supp_total_unit_retail,0)     + IO_supplier_total_unit_retail;
      IO_tot_pri_total_unit_retail        := NVL(IO_tot_pri_total_unit_retail,0)      + IO_primary_total_unit_retail;
      IO_tot_total_cost                   := NVL(IO_tot_total_cost,0)                 + IO_total_cost;
      IO_tot_supplier_total_cost          := NVL(IO_tot_supplier_total_cost,0)        + IO_supplier_total_cost;
      IO_tot_primary_total_cost           := NVL(IO_tot_primary_total_cost,0)         + IO_primary_total_cost;
      IO_tot_total_price                  := NVL(IO_tot_total_price,0)                + IO_total_price;
      IO_tot_supplier_total_price         := NVL(IO_tot_supplier_total_price,0)       + IO_supplier_total_price;
      IO_tot_primary_total_price          := NVL(IO_tot_primary_total_price,0)        + IO_primary_total_price;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END GET_WH_TOTALS ;
-------------------------------------------------------------------------------
FUNCTION IS_NUMBER (I_string   IN   VARCHAR2)
RETURN BOOLEAN IS

   L_dummy   NUMBER;

BEGIN

   L_dummy := to_number(I_string);
   return TRUE;

EXCEPTION
   when VALUE_ERROR then
   return FALSE;
END IS_NUMBER;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP_VALUE(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_group_value_desc         IN OUT   VARCHAR2,
                              O_tsf_qty                  IN OUT   MRT_ITEM_LOC.TSF_QTY%TYPE,
                              O_tsf_price                IN OUT   MRT_ITEM_LOC.TSF_PRICE%TYPE,
                              O_tsf_cost                 IN OUT   MRT_ITEM_LOC.TSF_COST%TYPE,
                              O_franchise_store_exists   IN OUT   VARCHAR2,
                              I_group_type               IN       CODE_DETAIL.CODE%TYPE,
                              I_group_value              IN       VARCHAR2,
                              I_v_or_p_wh_ind            IN       VARCHAR2,
                              I_currency_code            IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                              I_intercompany_tsf_basis   IN       SYSTEM_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE,
                              I_mrt_no                   IN       MRT.MRT_NO%TYPE,
                              I_mrt_type                 IN       MRT.MRT_TYPE%TYPE,
                              I_inventory_type           IN       MRT.INVENTORY_TYPE%TYPE,
                              I_item                     IN       MRT_ITEM.ITEM%TYPE,
                              I_wh_tsf_entity_id         IN       WH.TSF_ENTITY_ID%TYPE,
                              I_wh_set_of_books_id       IN       ORG_UNIT.SET_OF_BOOKS_ID%TYPE,
                              I_supplier                 IN       MRT.SUPPLIER%TYPE,
                              I_wh                       IN       MRT.WH%TYPE,
                              I_rtv_cost                 IN       MRT_ITEM.RTV_COST%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(64)    := 'MRT_ATTRIB_SQL.VALIDATE_GROUP_VALUE';
   L_valid                 BOOLEAN;
   L_franchise_found       VARCHAR2(1);
   L_company_found         VARCHAR2(1);
   L_second_value          VARCHAR2(10);
   L_loc_entity_id         TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_mrt_rec               MRT_ITEM_LOC%ROWTYPE;
   L_qty_avail             MRT_ITEM_LOC.TSF_QTY%TYPE;
   L_inv_status            MRT.INVENTORY_TYPE%TYPE;
   L_av_cost               ITEM_LOC_SOH.AV_COST%TYPE;
   L_unit_cost             ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_unit_retail           ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom           ITEM_LOC.SELLING_UOM%TYPE;
   L_currency_code         CURRENCIES.CURRENCY_CODE%TYPE;
   L_multi_currency_exists BOOLEAN;
   L_stockholding_ind      VARCHAR2(1);
   L_loc_name              STORE.STORE_NAME%TYPE;
   L_stock_store_exists    BOOLEAN;
   L_store_type            STORE.STORE_TYPE%TYPE;
   L_store_name            STORE.STORE_NAME%TYPE;
   L_loc_list_desc         LOC_LIST_HEAD.LOC_LIST_DESC%TYPE;
   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE;
   L_sob_id                FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE;
   L_sob_desc              FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE;


BEGIN

   O_franchise_store_exists := 'N';
   if I_mrt_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mrt_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;


   if I_group_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_group_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_group_value is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_group_value',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_group_type != 'C' then
      if IS_NUMBER(I_group_value) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('GROUP_VALUE_NUMBER',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
   end if;
   ---
   if I_group_type = 'W' then
      if WH_ATTRIB_SQL.WH_EXIST(O_error_message,
                                L_valid,
                                I_group_value) = FALSE
         or not L_valid then
         O_group_value_desc := NULL;
         return FALSE;
      end if;
      ---
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_TRANSFER_WH(O_error_message,
                                                      L_valid,
                                                      O_group_value_desc,
                                                      L_loc_entity_id,
                                                      I_group_value,
                                                      I_v_or_p_wh_ind,
                                                      'F') = FALSE
         or not L_valid then
         O_group_value_desc := NULL;
         return FALSE;
      end if;
      ---
      if LOCATION_ATTRIB_SQL.CHECK_STOCKHOLDING(O_error_message,
                                                L_stockholding_ind,
                                                L_loc_name,
                                                I_group_value,
                                                'W') = FALSE then
         return FALSE;
      end if;
      ---
      if L_stockholding_ind = 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('ONLY_STOCK_LOCS',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
      ---
   elsif I_group_type = 'S' then

      if STORE_VALIDATE_SQL.EXIST(O_error_message,
                                  I_group_value,
                                  L_valid) = FALSE
         or not L_valid then
         O_group_value_desc := NULL;
         return FALSE;
      end if;
      ---
      if FILTER_LOV_VALIDATE_SQL.STOCKHOLDING_STORE(L_error_message,
                                                    L_stock_store_exists,
                                                    L_store_name,
                                                    L_store_type,
                                                    I_group_value) = FALSE then
         return FALSE;
      end if;
      ---
      if L_stock_store_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_STKHLDING',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;

      if FILTER_LOV_VALIDATE_SQL.VALIDATE_TRANSFER_STORE(O_error_message,
                                                         L_valid,
                                                         O_group_value_desc,
                                                         L_loc_entity_id,
                                                         I_group_value,
                                                         'F') = FALSE
         or not L_valid then
         O_group_value_desc := NULL;
         return FALSE;
      end if;
      ---
      if LOCATION_ATTRIB_SQL.CHECK_STOCKHOLDING(O_error_message,
                                                L_stockholding_ind,
                                                L_loc_name,
                                                I_group_value,
                                                'S') = FALSE then
         return FALSE;
      end if;
      ---
      if L_stockholding_ind = 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('ONLY_STOCK_LOCS',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
      ---


      if ITEM_LOC_SQL.WF_STORE_EXIST(O_error_message,
                                     L_franchise_found,
                                     L_company_found,
                                     I_group_type,
                                     I_group_value) = FALSE then
         return FALSE;
      end if;


      if L_franchise_found = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('F_STORE_MRT',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;

   else
      if (FILTER_LOV_VALIDATE_SQL.VALIDATE_LOCATION_TYPE(O_error_message,
                                                         L_valid,
                                                         O_group_value_desc,
                                                         L_second_value,
                                                         I_group_type,
                                                         I_group_value) = FALSE)
       or not L_valid then
         O_group_value_desc := NULL;
         return FALSE;
      end if;
      ---
      if I_group_type = 'LLS' then
         if FILTER_LOV_VALIDATE_SQL.LOC_LIST_HEAD_TYPE(L_error_message,
                                                       L_loc_list_desc,
                                                       I_group_value) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
   end if;

   if I_group_type not in('S', 'W') then
      if ITEM_LOC_SQL.LOCS_EXIST_FOR_GROUP(O_error_message,
                                           L_valid,
                                           I_group_type,
                                           I_group_value) = FALSE
         or not L_valid then
         return FALSE;
      end if;

      ---
      if I_group_type IN ('AL', 'AS', 'LLS', 'DW', 'L', 'T', 'A', 'R', 'D', 'C') then
         if ITEM_LOC_SQL.WF_STORE_EXIST(O_error_message,
                                        L_franchise_found,
                                        L_company_found,
                                        I_group_type,
                                        I_group_value) = FALSE then
             return FALSE;
         end if;
         ---

         if L_franchise_found = 'Y' and
            L_company_found = 'Y' then

            O_franchise_store_exists := 'Y';

         elsif L_franchise_found = 'Y' and
            L_company_found = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('F_STORE_MRT',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            return FALSE;
         end if;
      end if;
   end if;
   ---
   if I_group_type in ('S','W') then
      if I_intercompany_tsf_basis = 'B' then
         if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                         L_sob_desc,
                                         L_sob_id,
                                         I_group_value,
                                         I_group_type) = FALSE then
            return FALSE;
         end if;
      end if;
      if I_group_type = 'W' then
         if I_group_value = I_wh then
            O_error_message := SQL_LIB.CREATE_MSG('TF_WH_NOT_SAME',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            return FALSE;
         end if;
         ---
      end if;
      ---

      if I_mrt_type = 'A' then
         if I_intercompany_tsf_basis = 'T' then
            if I_wh_tsf_entity_id != L_loc_entity_id then
               O_error_message := SQL_LIB.CREATE_MSG('LOC_NOTIN_TSF_ENT',
                                                      I_group_value,
                                                      I_wh_tsf_entity_id,
                                                      NULL);
               return FALSE;
            end if;
         elsif I_intercompany_tsf_basis = 'B' then
            if I_wh_set_of_books_id != L_sob_id then
               O_error_message := SQL_LIB.CREATE_MSG('LOC_NOTIN_TSF_SOB',
                                                      I_group_value,
                                                      I_wh_set_of_books_id,
                                                      NULL);
               return FALSE;
            end if;
         end if;
         ---
      elsif I_mrt_type = 'E' then
         --Inter
         if I_intercompany_tsf_basis = 'T' then
            if I_wh_tsf_entity_id = L_loc_entity_id then
               O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_FROM_TO_ENT',
                                                      NULL,
                                                      NULL,
                                                      NULL);
               return FALSE;
            end if;
         elsif I_intercompany_tsf_basis = 'B' then
            if I_wh_set_of_books_id = L_sob_id then
               O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_FROM_TO_SOB',
                                                      NULL,
                                                      NULL,
                                                      NULL);
               return FALSE;
            end if;
         end if;
         ---
      end if;
      ---
      if I_group_value is not NULL then
         if MRT_ATTRIB_SQL.GET_MRT_ITEM_LOC_INFO(O_error_message,
                                                 L_mrt_rec,
                                                 L_valid,
                                                 I_mrt_no,
                                                 I_item,
                                                 I_group_value) = FALSE then
             return FALSE;
         end if;
         ---
         if L_valid then
            O_error_message := SQL_LIB.CREATE_MSG('MRT_ITEM_LOC_EXISTS',
                                                   I_mrt_no,
                                                   NULL,
                                                   NULL);
            return FALSE;
         end if;
         ---
      end if;
      ---

      if I_inventory_type = 'A' then
         L_inv_status := NULL;
      else
         L_inv_status := I_inventory_type;
      end if;
      ---
      if TRANSFER_SQL.QUANTITY_AVAIL(O_error_message,
                                     L_qty_avail,
                                     I_item,
                                     L_inv_status,
                                     I_group_type,
                                     I_group_value)  = FALSE then
          return FALSE;
      end if;
      ---
      if L_qty_avail > 0 then
         O_tsf_qty := L_qty_avail;
      else
         O_tsf_qty := 0;
         O_error_message := SQL_LIB.CREATE_MSG('QTY_AVAIL_ST_TSF',
                                                I_item,
                                                I_group_value,
                                                to_char(L_qty_avail));
         return FALSE;
      end if;
      ---

      if I_supplier is not null then

         if I_mrt_type = 'A' then 
            O_tsf_cost  := I_rtv_cost;
         elsif I_mrt_type = 'E' then 
            O_tsf_price := I_rtv_cost;
         end if; 
      else

         if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                     I_item,
                                                     I_group_value,
                                                     I_group_type,
                                                     'N',
                                                     L_av_cost,
                                                     L_unit_cost,
                                                     L_unit_retail,
                                                     L_selling_unit_retail,
                                                     L_selling_uom) = FALSE then
            return FALSE;
         end if;
         ---

         if LOCATION_ATTRIB_SQL.GET_LOCATION_CURRENCY(O_error_message,
                                                      L_multi_currency_exists,
                                                      L_currency_code,
                                                      I_group_type,
                                                      I_group_value) = FALSE then
               return FALSE;
         end if;
         ---
         -- Average cost is in the location currency, need to convert it
         -- to the MRT currency
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_av_cost,
                                 L_currency_code, -- Location currency
                                 I_currency_code, -- MRT currency
                                 L_av_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if I_mrt_type = 'A' then
            O_tsf_cost  := L_av_cost;
         elsif I_mrt_type = 'E' then
            O_tsf_price := L_av_cost;       
         end if;
      end if;
      ---
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END VALIDATE_GROUP_VALUE ;
--------------------------------------------------------------------------------
END;

/
