CREATE OR REPLACE PACKAGE BODY SVCPROV_FULFILORD AS

-------------------------------------------------------------------------------------------------------------
-- Private subprogram specs
-------------------------------------------------------------------------------------------------------------
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
-------------------------------------------------------------------------------------------------------------
-- Function Name  : BUILD_CONFIRM_MSG
-- Purpose        : This function builds a collection of customer order fulfillment confirmations 
-- based on ORDCUST and ORDCUST_DETAILs created in RMS. It includes ORDCUST records created in 'C'
-- (order fully created), 'P'artial (order partially created) or 'X' (order not created) status.
-------------------------------------------------------------------------------------------------------------
FUNCTION BUILD_CONFIRM_MSG(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_confirm_obj             IN OUT   "RIB_FulfilOrdCfmCol_REC", 
                           I_ordcust_ids             IN       ID_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name  : PARSE_ERR_MSG
-- Purpose        : This function parses ERROR_MSG on fulfillment staging tables and add parsed
-- errors to FailStatus_TBL in ServiceOpStatus object. This will pass the errors to the web service client.
-------------------------------------------------------------------------------------------------------------
FUNCTION PARSE_ERR_MSG(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_serviceOperationStatus  IN OUT   "RIB_ServiceOpStatus_REC", 
                       I_process_id              IN       SVC_FULFILORD.PROCESS_ID%TYPE, 
                       I_action_type             IN       VARCHAR2)
RETURN BOOLEAN;
$end
-------------------------------------------------------------------------------------------------------------
-- Public subprogram body
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
PROCEDURE CREATE_FULFILLMENT(O_serviceOperationStatus    IN OUT   "RIB_ServiceOpStatus_REC",
                             O_businessObject            IN OUT   "RIB_FulfilOrdCfmCol_REC",
                             I_serviceOperationContext   IN       "RIB_ServiceOpContext_REC",
                             I_businessObject            IN       "RIB_FulfilOrdColDesc_REC")
IS
   L_program         VARCHAR2(61) := 'SVCPROV_FULFILORD.CREATE_FULFILLMENT';

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_process_id      SVC_FULFILORD.PROCESS_ID%TYPE;
   L_chunk_id        SVC_FULFILORD.CHUNK_ID%TYPE := 1;
   L_ordcust_ids     ID_TBL;

   PROGRAM_ERROR     EXCEPTION;

BEGIN

   --check input object contains data
   if I_businessObject is NULL or
      I_businessObject.FulfilOrdDesc_TBL is NULL or 
      I_businessObject.FulfilOrdDesc_TBL.count <= 0 then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   --push objects to staging tables
   if STGSVC_FULFILORD.POP_CREATE_TABLES(L_error_message,
                                         L_process_id,
                                         I_businessObject,
                                         CORESVC_FULFILORD.ACTION_TYPE_CREATE) = FALSE then   -- 'create'
      raise PROGRAM_ERROR;
   end if;

   -- call core logic
   if CORESVC_FULFILORD.CREATE_FULFILLMENT(L_error_message,
                                           L_ordcust_ids,
                                           L_process_id,
                                           L_chunk_id) = FALSE then

      --parse error on the staging tables and build failure collection
      if PARSE_ERR_MSG(L_error_message,
                       O_serviceOperationStatus,  
                       L_process_id,
                       CORESVC_FULFILORD.ACTION_TYPE_CREATE) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      raise PROGRAM_ERROR;
   end if;

   if BUILD_CONFIRM_MSG(L_process_id,
                        O_businessObject,
                        L_ordcust_ids) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   --cleanup parameter/work tables
   if STGSVC_FULFILORD.CLEANUP_TABLES(L_error_message,
                                      L_process_id,
                                      CORESVC_FULFILORD.ACTION_TYPE_CREATE) = FALSE then  -- 'create'
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
      return;

   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
      return;
END CREATE_FULFILLMENT;
-------------------------------------------------------------------------------------------------------------
PROCEDURE CANCEL_FULFILLMENT(O_serviceOperationStatus    IN OUT   "RIB_ServiceOpStatus_REC",
                             O_businessObject            IN OUT   "RIB_InvocationSuccess_REC",
                             I_serviceOperationContext   IN       "RIB_ServiceOpContext_REC",
                             I_businessObject            IN       "RIB_FulfilOrdColRef_REC")
IS
   L_program         VARCHAR2(61) := 'SVCPROV_FULFILORD.CANCEL_FULFILLMENT';

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_process_id      SVC_FULFILORDREF.PROCESS_ID%TYPE;
   L_chunk_id        SVC_FULFILORDREF.CHUNK_ID%TYPE := 1;

   PROGRAM_ERROR     EXCEPTION;

BEGIN

   --check input object contains data
   if I_businessObject is NULL or
      I_businessObject.FulfilOrdRef_TBL is NULL or 
      I_businessObject.FulfilOrdRef_TBL.count <= 0 then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   --push objects to staging tables, return a process_id
   if STGSVC_FULFILORD.POP_CANCEL_TABLES(L_error_message,
                                         L_process_id,
                                         I_businessObject,
                                         CORESVC_FULFILORD.ACTION_TYPE_CANCEL) = FALSE then   --'cancel'
      raise PROGRAM_ERROR;
   end if;

   -- call core logic
   if CORESVC_FULFILORD.CANCEL_FULFILLMENT(L_error_message,
                                           L_process_id,
                                           L_chunk_id) = FALSE then

      --parse error on the staging tables and build failure collection
      if PARSE_ERR_MSG(L_error_message,
                       O_serviceOperationStatus,  
                       L_process_id,
                       CORESVC_FULFILORD.ACTION_TYPE_CANCEL) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      raise PROGRAM_ERROR;
   end if;

   --cleanup parameter/work tables
   if STGSVC_FULFILORD.CLEANUP_TABLES(L_error_message,
                                      L_process_id,
                                      CORESVC_FULFILORD.ACTION_TYPE_CANCEL) = FALSE then  -- 'cancel'
      raise PROGRAM_ERROR;
   end if;

   --build the output O_businessObject 
   O_businessObject := "RIB_InvocationSuccess_REC"(0, 'cancelFulfilOrdColRef service call was successful.');

   return;

EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
      return;

   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
      return;
END CANCEL_FULFILLMENT;
-------------------------------------------------------------------------------------------------------------
-- Private subprogram body
-------------------------------------------------------------------------------------------------------------
FUNCTION BUILD_CONFIRM_MSG(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_confirm_obj             IN OUT   "RIB_FulfilOrdCfmCol_REC", 
                           I_ordcust_ids             IN       ID_TBL)
RETURN BOOLEAN IS

   L_program         VARCHAR2(61) := 'SVCPROV_FULFILORD.BUILD_CONFIRM_MSG';

   L_fulfilOrdCfmDesc_TBL "RIB_FulfilOrdCfmDesc_TBL";
   L_collection_size      INTEGER := 0;

   cursor C_CONFIRM_MSG is
      select "RIB_FulfilOrdCfmDesc_REC"(0,                      --rib_oid
                                        oc.customer_order_no,   --customer_order_no varchar2(48),
                                        oc.fulfill_order_no,    --fulfill_order_no varchar2(48),
                                        oc.status,              --confirm_type varchar2(1),
                                        th.tsf_no,              --confirm_no number(12),
                                        --FulfilOrdCfmDtl_TBL 
                                        CAST(MULTISET(select "RIB_FulfilOrdCfmDtl_REC"(0,               --rib_oid
                                                                                       od.item,         --item varchar2(25),
                                                                                       od.ref_item,     --ref_item varchar2(25),
                                                                                       td.tsf_qty,      --confirm_qty number(12,4),
                                                                                       od.standard_uom) --confirm_qty_uom varchar2(4)
                                                        from ordcust_detail od,
                                                             tsfdetail td
                                                       where od.ordcust_no = oc.ordcust_no
                                                         and td.tsf_no = th.tsf_no
                                                         and td.item = od.item) as "RIB_FulfilOrdCfmDtl_TBL"))
        from ordcust oc,
             tsfhead th,
             TABLE(CAST(I_ordcust_ids AS ID_TBL)) input_tbl
       where oc.ordcust_no = value(input_tbl)
         and oc.tsf_no = th.tsf_no(+)  --outer join to account for ORDCUST created in 'X' status with no associated tsf
         and oc.source_loc_type in ('ST', 'WH')  --store or warehouse
         and oc.status in ('X', 'P')  --only publish exceptional cases where orders that are NOT created or only partially created
       union all
      select "RIB_FulfilOrdCfmDesc_REC"(0,
                                        oc.customer_order_no,   --customer_order_no varchar2(48),
                                        oc.fulfill_order_no,    --fulfill_order_no varchar2(48),
                                        oc.status,              --confirm_type varchar2(1),
                                        oh.order_no,            --confirm_no number(12),
                                        --FulfilOrdCfmDtl_TBL 
                                        CAST(MULTISET(select "RIB_FulfilOrdCfmDtl_REC"(0,               --rib_oid
                                                                                       od.item,         --item varchar2(25),
                                                                                       od.ref_item,     --ref_item varchar2(25),
                                                                                       ol.qty_ordered,  --confirm_qty number(12,4),
                                                                                       od.standard_uom) --confirm_qty_uom varchar2(4)
                                                        from ordcust_detail od,
                                                             ordloc ol
                                                       where od.ordcust_no = oc.ordcust_no
                                                         and ol.order_no = oh.order_no
                                                         and ol.item = od.item
                                                         and ol.location = oc.fulfill_loc_id) as "RIB_FulfilOrdCfmDtl_TBL"))
        from ordcust oc,
             ordhead oh,
             TABLE(CAST(I_ordcust_ids AS ID_TBL)) input_tbl
       where oc.ordcust_no = value(input_tbl)
         and oc.order_no = oh.order_no(+) --outer join to account for ORDCUST created in 'X' status with no associated order
         and oc.source_loc_type = 'SU'   --supplier
         and oc.status in ('X', 'P');  --only publish exceptional cases where orders that are NOT created or only partially created

BEGIN

   if I_ordcust_ids is NOT NULL and I_ordcust_ids.COUNT > 0 then
      open C_CONFIRM_MSG;
      fetch C_CONFIRM_MSG bulk collect into L_fulfilOrdCfmDesc_TBL;
      close C_CONFIRM_MSG;

      if L_fulfilOrdCfmDesc_TBL is NOT NULL and L_fulfilOrdCfmDesc_TBL.COUNT > 0 then
         L_collection_size := L_fulfilOrdCfmDesc_TBL.COUNT;
      end if;
   end if;

   -- In case no confirmation is needed (e.g. Inventory Reservation scenario), send
   -- a "RIB_FulfilOrdCfmCol_REC" with collection size 0 since web service always
   -- expects a response object.
   O_confirm_obj := "RIB_FulfilOrdCfmCol_REC"(0,  --rib_oid
                                              L_collection_size,
                                              L_fulfilOrdCfmDesc_TBL);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_CONFIRM_MSG;
-------------------------------------------------------------------------------------------------------------
FUNCTION PARSE_ERR_MSG(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_serviceOperationStatus  IN OUT   "RIB_ServiceOpStatus_REC", 
                       I_process_id              IN       SVC_FULFILORD.PROCESS_ID%TYPE, 
                       I_action_type             IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(61) := 'SVCPROV_FULFILORD.PARSE_ERR_MSG';

   L_l10n_obj    L10N_OBJ;

   cursor C_LOCALIZED_COUNTRY is
      select country_id
        from country_attrib
       where localized_ind = 'Y';

BEGIN

   if O_serviceOperationStatus is NULL then
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(NULL, NULL);
   end if;

   if I_action_type = CORESVC_FULFILORD.ACTION_TYPE_CREATE then

      if SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(O_error_message,
                                              O_serviceOperationStatus.FailStatus_TBL,
                                              'SVC_FULFILORD',
                                              I_process_id) = FALSE then
         return FALSE;
      end if;

      if SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(O_error_message,
                                              O_serviceOperationStatus.FailStatus_TBL,
                                              'SVC_FULFILORDDTL',
                                              I_process_id) = FALSE then
         return FALSE;
      end if;

      if SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(O_error_message,
                                              O_serviceOperationStatus.FailStatus_TBL,
                                              'SVC_FULFILORDCUST',
                                              I_process_id) = FALSE then
         return FALSE;
      end if;

      FOR rec in C_LOCALIZED_COUNTRY LOOP

         -- Call out to countries that are supported for localization, e.g. 'BR' for Brazil.
         L_l10n_obj := L10N_OBJ();
         -- this returns a collection of staging table names for the localized country (e.g. SVC_BRFULFILORDCUST)
         L_l10n_obj.procedure_key := 'GET_SVC_TABLES_FULFILORD_L10N';
         L_l10n_obj.doc_type := NULL;
         L_l10n_obj.doc_id := NULL;
         L_l10n_obj.country_id := rec.country_id;
 
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_obj) = FALSE then
            return FALSE;
         end if;

         -- parse error messages on the staging tables for the localized country
         if L_l10n_obj is NOT NULL and L_l10n_obj.table_names is NOT NULL and L_l10n_obj.table_names.COUNT > 0 then
            FOR i in L_l10n_obj.table_names.FIRST .. L_l10n_obj.table_names.LAST LOOP
               if SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(O_error_message,
                                                       O_serviceOperationStatus.FailStatus_TBL,
                                                       L_l10n_obj.table_names(i).table_name,
                                                       I_process_id) = FALSE then
                  return FALSE;
               end if;
            END LOOP;
         end if;

     END LOOP;

   elsif I_action_type = CORESVC_FULFILORD.ACTION_TYPE_CANCEL then

      if SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(O_error_message,
                                              O_serviceOperationStatus.FailStatus_TBL,
                                              'SVC_FULFILORDREF',
                                              I_process_id) = FALSE then
         return FALSE;
      end if;

      if SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG(O_error_message,
                                              O_serviceOperationStatus.FailStatus_TBL,
                                              'SVC_FULFILORDDTLREF',
                                              I_process_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_ERR_MSG;
------------------------------------------------------------------------------------------------------------- 
END SVCPROV_FULFILORD;
/

