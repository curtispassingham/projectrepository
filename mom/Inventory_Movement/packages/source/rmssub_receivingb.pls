CREATE OR REPLACE PACKAGE BODY RMSSUB_RECEIVING AS

PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  VARCHAR2,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2);
-------------------------------------------------------------------------------------------------------
-- Function Name: CONSUME_RECEIPT_CORE
-- Purpose: This private function contains the core logic of receipt consume process.
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME_RECEIPT_CORE(O_status_code          IN OUT  VARCHAR2,
                              O_error_message        IN OUT  VARCHAR2,
                              I_message              IN      RIB_OBJECT,
                              I_message_type         IN      VARCHAR2,
                              O_rib_otbdesc_rec         OUT  RIB_OBJECT,
                              O_rib_error_tbl           OUT  RIB_ERROR_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE --
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code          IN OUT  VARCHAR2,
                  O_error_message        IN OUT  VARCHAR2,
                  I_message              IN      RIB_OBJECT,
                  I_message_type         IN      VARCHAR2,
                  O_rib_otbdesc_rec         OUT  RIB_OBJECT,
                  O_rib_error_tbl           OUT  RIB_ERROR_TBL)
IS
   L_program        VARCHAR2(255) := 'RMSSUB_RECEIVING.CONSUME';

   L_check_l10n_ind VARCHAR2(1) := 'Y';

   PROGRAM_ERROR  EXCEPTION;

BEGIN

   O_status_code := API_CODES.SUCCESS;

   -- Perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Check message type
   if I_message_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;

   --When SIM publishes ReceiptDesc during receiving process, it will publish with message type "ReceiptCre"
   --if the receipt is not related to a customer order and message type "ReceiptOrdCre" if it is associated 
   --to a customer order. All message will continue to flow to RMS. Messages of type "ReceiptOrdCre" will 
   --also flow to OMS. RMS will treat 'ReceiptCre' and 'ReceiptOrdCre' exactly the same.
   if lower(I_message_type) in (RECEIPT_ADD, RECEIPT_UPD, RECEIPT_ORDADD) then
      CONSUME_RECEIPT(O_status_code,
                      O_error_message,
                      I_message,
                      I_message_type,
                      L_check_l10n_ind,  -- 'Y'
                      O_rib_otbdesc_rec,
                      O_rib_error_tbl);

      if O_status_code != API_CODES.SUCCESS then
         raise PROGRAM_ERROR;
      end if;
   elsif lower(I_message_type) in (APPOINT_HDR_ADD, APPOINT_HDR_UPD, APPOINT_HDR_DEL,
                            APPOINT_DTL_ADD, APPOINT_DTL_UPD, APPOINT_DTL_DEL) then
      if RMSSUB_APPOINT.CONSUME(O_status_code,
                                O_error_message,
                                I_message,
                                I_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END CONSUME;
-------------------------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE --
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME_RECEIPT(O_status_code          IN OUT  VARCHAR2,
                          O_error_message        IN OUT  VARCHAR2,
                          I_message              IN      RIB_OBJECT,
                          I_message_type         IN      VARCHAR2,
                          I_check_l10n_ind       IN      VARCHAR2,
                          O_rib_otbdesc_rec         OUT  RIB_OBJECT,
                          O_rib_error_tbl           OUT  RIB_ERROR_TBL)
IS

   L_program VARCHAR2(100) := 'RMSSUB_RECEIVING.CONSUME_RECEIPT';

   L_head_index       BINARY_INTEGER           := 0;
   L_detail_index     BINARY_INTEGER           := 0;

   L_rib_receiptdesc_rec "RIB_ReceiptDesc_REC" := null;
   L_l10n_rib_rec    "L10N_RIB_REC"            := L10N_RIB_REC();
   --
   L_po_nbr          NUMBER;
   L_from_loc        TSFHEAD.FROM_LOC%TYPE;
   L_from_loc_type   TSFHEAD.FROM_LOC_TYPE%TYPE;
   --
   cursor C_GET_FROM_LOC is
      select from_loc,
             from_loc_type
        from tsfhead
       where tsf_no = L_po_nbr;

BEGIN

   -- Check message type
   if I_message_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;

   O_rib_error_tbl   := NULL;
   O_rib_otbdesc_rec := NULL;
   --
   L_l10n_rib_rec.rib_msg         :=  I_message;
   L_l10n_rib_rec.rib_msg_type    :=  I_message_type;
   L_l10n_rib_rec.procedure_key   := 'CONSUME_RECEIPT';
   L_l10n_rib_rec.rib_err_tbl     := NULL;
   L_l10n_rib_rec.rib_otbdesc_rec := NULL;
   --
   if I_check_l10n_ind = 'Y' then
      ---
      --- This code below is to find out the info need to find the countries in L10N_SQL.EXEC_FUNCTION 
      ---     
      if lower(I_message_type) in (RECEIPT_ADD, RECEIPT_UPD, RECEIPT_ORDADD) then
         L_rib_receiptdesc_rec := treat (L_l10n_rib_rec.rib_msg as "RIB_ReceiptDesc_REC");

         if L_rib_receiptdesc_rec is NULL or L_rib_receiptdesc_rec.receipt_tbl is NULL or L_rib_receiptdesc_rec.receipt_tbl.COUNT <= 0 then
            O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
            raise PROGRAM_ERROR;
         end if;

         -- Even though the Receipt Message can contain multiple receipts base on the RIB
         -- object definition, SIM and RWMS will ONLY send ONE receipt in the receipt message.
         -- Since RMS does NOT support splitting the Receipt message to multiple messages and
         -- route to different applications (e.g. RFM) based on localization, the assumption
         -- is the receipt message will ONLY contain ONE receipt.

         L_head_index := L_rib_receiptdesc_rec.receipt_tbl.FIRST;
         while L_head_index is NOT NULL loop
            --- call PO receiving for each line item
            if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type = 'P' then
               L_l10n_rib_rec.doc_type        := 'PO';
               L_l10n_rib_rec.doc_id := L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr;
            else
               L_l10n_rib_rec.doc_type        := 'TSF';
               if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).from_loc IS NULL then
                  L_po_nbr := L_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr;

                  open C_GET_FROM_LOC;
                  fetch C_GET_FROM_LOC into L_from_loc,L_from_loc_type;
                  close C_GET_FROM_LOC;

                  if L_from_loc_type = 'E' then
                     L_l10n_rib_rec.source_entity   := 'PTNR';
                  else
                     L_l10n_rib_rec.source_entity   := 'LOC';
                  end if;
                  L_l10n_rib_rec.source_id       := L_from_loc;
                  L_l10n_rib_rec.source_type     := L_from_loc_type;
               else
                  L_l10n_rib_rec.source_id       := L_rib_receiptdesc_rec.receipt_tbl(L_head_index).from_loc;
                  L_l10n_rib_rec.source_type     := L_rib_receiptdesc_rec.receipt_tbl(L_head_index).from_loc_type;
                  if L_rib_receiptdesc_rec.receipt_tbl(L_head_index).from_loc_type = 'E' then
                     L_l10n_rib_rec.source_entity   := 'PTNR';
                  else
                     L_l10n_rib_rec.source_entity   := 'LOC';
                  end if;
               end if;
               L_l10n_rib_rec.dest_id         := L_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id;
               L_l10n_rib_rec.dest_type       := NULL;
               L_l10n_rib_rec.dest_entity     := 'LOC';
            end if;
            -- Assumption: only 1 receipt in the RIB_ReceiptDesc_REC message
            exit;
         end loop;
         --- The code above is to find out the info need to find the countries in L10N_SQL.EXEC_FUNCTION
         ---
         if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                    L_l10n_rib_rec)= FALSE then
            raise PROGRAM_ERROR;
         end if;
         ---
      elsif lower(I_message_type) in (APPOINT_HDR_ADD, APPOINT_HDR_UPD, APPOINT_HDR_DEL,
                                   APPOINT_DTL_ADD, APPOINT_DTL_UPD, APPOINT_DTL_DEL) then
         if RMSSUB_APPOINT.CONSUME(O_status_code,
                                   O_error_message,
                                   L_l10n_rib_rec.rib_msg,
                                   L_l10n_rib_rec.rib_msg_type) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;

   else
      -- When Localization Indicator is other than 'Y'.
      --- This is for the base functionality and also will be called for the country which is not localized.
      if RMSSUB_RECEIVING.CONSUME_RECEIPT(O_error_message,
                                          L_l10n_rib_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;      
   end if;   
   --
   O_rib_otbdesc_rec := L_l10n_rib_rec.rib_otbdesc_rec;
   --
   if L_l10n_rib_rec.rib_err_tbl IS NOT NULL and L_l10n_rib_rec.rib_err_tbl.count > 0  then
      O_rib_error_tbl := L_l10n_rib_rec.rib_err_tbl;
   end if;
   --
   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;
   
EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END CONSUME_RECEIPT;
-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION -- Used by L10N_SQL for the countries this needs to called
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME_RECEIPT(O_error_message        IN OUT  VARCHAR2,
                         IO_L10N_RIB_REC        IN OUT  L10N_OBJ)
RETURN BOOLEAN IS
   --
   L_program         VARCHAR2(100) := 'RMSSUB_RECEIVING.CONSUME_RECEIPT';
   L_l10n_rib_rec    "L10N_RIB_REC" := L10N_RIB_REC();
   --
BEGIN

   L_l10n_rib_rec                 := treat (IO_L10N_RIB_REC as "L10N_RIB_REC");
   L_l10n_rib_rec.rib_otbdesc_rec := NULL;
   L_l10n_rib_rec.rib_err_tbl     := NULL;

   if RMSSUB_RECEIVING.CONSUME_RECEIPT_CORE(L_l10n_rib_rec.status_code,
                                            O_error_message,
                                            L_l10n_rib_rec.rib_msg,
                                            L_l10n_rib_rec.rib_msg_type,
                                            L_l10n_rib_rec.rib_otbdesc_rec,
                                            L_l10n_rib_rec.rib_err_tbl) = FALSE then
      return FALSE;
   end if;
   --
   IO_L10N_RIB_REC := L_l10n_rib_rec;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONSUME_RECEIPT;
-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION --
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME_RECEIPT_CORE(O_status_code          IN OUT  VARCHAR2,
                              O_error_message        IN OUT  VARCHAR2,
                              I_message              IN      RIB_OBJECT,
                              I_message_type         IN      VARCHAR2,
                              O_rib_otbdesc_rec         OUT  RIB_OBJECT,
                              O_rib_error_tbl           OUT  RIB_ERROR_TBL)
RETURN BOOLEAN IS

   L_program         VARCHAR2(100) := 'RMSSUB_RECEIVING.CONSUME_RECEIPT_CORE';
   
   L_rib_otbdesc_rec "RIB_OTBDesc_REC" := NULL;
   
BEGIN

   if API_LIBRARY.INIT(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if lower(I_message_type) in (RECEIPT_ADD, RECEIPT_UPD, RECEIPT_ORDADD) then
      if RMSSUB_RECEIPT.CONSUME(O_status_code,
                                O_error_message,
                                treat (I_message as "RIB_ReceiptDesc_REC"),
                                I_message_type,
                                L_rib_otbdesc_rec,
                                O_rib_error_tbl) = FALSE then
         return FALSE;
      end if;
      -- For RIB's Stubby framework to work, only generic RIB_OBJECT can be returned
      -- in consume signature.
      O_rib_otbdesc_rec := L_rib_otbdesc_rec;
      
   elsif lower(I_message_type) in (APPOINT_HDR_ADD, APPOINT_HDR_UPD, APPOINT_HDR_DEL,
                                   APPOINT_DTL_ADD, APPOINT_DTL_UPD, APPOINT_DTL_DEL) then
      if RMSSUB_APPOINT.CONSUME(O_status_code,
                                O_error_message,
                                I_message,
                                I_message_type) = FALSE then
         return FALSE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_message_type',
                                            I_message_type,L_program);
      return FALSE;
   end if;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONSUME_RECEIPT_CORE;
----------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code     IN OUT  VARCHAR2,
                        IO_error_message  IN OUT  VARCHAR2,
                        I_cause           IN      VARCHAR2,
                        I_program         IN      VARCHAR2)
IS
   L_program  VARCHAR2(100)  := 'RMSSUB_RECEIVING.HANDLE_ERRORS';

BEGIN
   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             To_Char(SQLCODE));
      ---
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;
----------------------------------------------------------------------------------------
END RMSSUB_RECEIVING;
/

