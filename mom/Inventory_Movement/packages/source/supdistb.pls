CREATE OR REPLACE PACKAGE BODY SUP_DIST_SQL AS
-------------------------------------------------------------------------------------------
-- Private function
-- 
FUNCTION INSERT_DEFAULT_DIST (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item               IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                              I_location           IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'SUP_DIST_SQL.INSERT_DEFAULT_DIST';
   L_count_iscl      NUMBER := 0;

   cursor C_GET_ISCL_CNT_MSOB_Y is
      select count(1)
        from item_supp_country_loc iscl,
             store s,
             wh wh,
             partner_org_unit pou
       where iscl.item       = I_item
         and iscl.loc        = I_location
         and wh.wh(+)        = iscl.loc 
         and s.store(+)      = iscl.loc 
         and pou.partner     = iscl.supplier
         and (pou.org_unit_id = DECODE(iscl.loc_type, 'W', wh.org_unit_id,
                                                           s.org_unit_id) or
              exists (select 'x'
                        from sups_imp_exp sie
                       where sie.supplier = iscl.supplier
                         and rownum = 1));

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ISCL_CNT_MSOB_Y',
                    'ITEM_SUPP_COUNTRY_LOC, PARTNER_ORG_UNIT, STORE, WH',
                    I_item||' - '||I_location);
   open C_GET_ISCL_CNT_MSOB_Y;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ISCL_CNT_MSOB_Y',
                    'ITEM_SUPP_COUNTRY_LOC, PARTNER_ORG_UNIT, STORE, WH',
                    I_item||' - '||I_location);
   fetch C_GET_ISCL_CNT_MSOB_Y into L_count_iscl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ISCL_CNT_MSOB_Y',
                    'ITEM_SUPP_COUNTRY_LOC, PARTNER_ORG_UNIT, STORE, WH',
                    I_item||' - '||I_location);
   close C_GET_ISCL_CNT_MSOB_Y;
   
   -- Insert into repl_item_loc_supp_dist
   if L_count_iscl > 0 then
      insert into repl_item_loc_supp_dist(item,
                                          location,
                                          supplier,
                                          origin_country_id,
                                          dist_pct,
                                          last_update_datetime,
                                          last_update_id)
                                  (select I_item,
                                          I_location,
                                          iscl.supplier,
                                          iscl.origin_country_id,
                                          round(100/L_count_iscl,2) dist_pct,
                                          sysdate,
                                          get_user
                                     from item_supp_country_loc iscl,
                                          partner_org_unit pou,
                                          store s,
                                          wh wah
                                    where iscl.item = I_item
                                      and iscl.loc = I_location
                                      and iscl.loc = s.store(+)
                                      and iscl.loc = wah.wh(+)
                                      and pou.partner = iscl.supplier 
                                      and (pou.org_unit_id = DECODE(iscl.loc_type,'S',s.org_unit_id,
                                                                                      wah.org_unit_id) or
                                          exists (select 'x'
                                                    from sups_imp_exp sie
                                                   where sie.supplier = iscl.supplier
                                                     and rownum = 1))
                                      and round(100/L_count_iscl,2) > 0);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_DEFAULT_DIST;
-------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (IO_supp_dist_tbl    IN OUT   REPL_SUPP_DIST_TBL_TYPE,
                           I_item              IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                           I_location          IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
IS

   L_program             VARCHAR2(64)             := 'SUP_DIST_SQL.QUERY_PROCEDURE';
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_count               NUMBER := 0;

   cursor C_GET_REPL_SUPP_DIST_CNT is
      select count(1)
        from repl_item_loc_supp_dist r
       where r.item = I_item
         and r.location = I_location;

   cursor C_ITEM_LOC_DIST_EXISTS_MSOB_Y is
      select sups.supplier_parent,
             s2.sup_name supplier_parent_name,
             iscl.supplier,
             vsups.sup_name,
             iscl.origin_country_id,
             isc.unit_cost,
             isc.min_order_qty,
             isc.max_order_qty,
             nvl(rilsd.dist_pct,0) dist_pct,
             DECODE(rilsd.item, NULL, 'Y', 'N'),
             NULL error_message,
             'TRUE' return_code
        from item_supp_country isc,
             item_supp_country_loc iscl,
             repl_item_loc_supp_dist rilsd,
             sups sups,
             v_sups_tl s2,
             store st,
             wh wh,
             partner_org_unit pou,
             v_sups_tl vsups
       where iscl.item = isc.item
         and iscl.supplier = isc.supplier
         and iscl.origin_country_id = isc.origin_country_id
         and iscl.item = rilsd.item(+)
         and iscl.loc = rilsd.location(+)
         and iscl.supplier = rilsd.supplier(+)
         and iscl.origin_country_id = rilsd.origin_country_id(+)
         and iscl.supplier = sups.supplier
         and iscl.item = I_item
         and iscl.loc = I_location
         and s2.supplier(+) = sups.supplier_parent
         and wh.wh(+) = iscl.loc
         and st.store(+) = iscl.loc
         and pou.partner = iscl.supplier
         and sups.supplier=vsups.supplier
         and (pou.org_unit_id = DECODE(iscl.loc_type, 'W', wh.org_unit_id, st.org_unit_id) or
              exists (select 'x'
                        from sups_imp_exp sie
                       where sie.supplier = iscl.supplier
                         and rownum = 1))
    order by dist_pct desc,
             sups.supplier_parent,
             iscl.supplier,
             origin_country_id;

BEGIN

   if I_item is NULL then
      IO_supp_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                               'I_item',
                                                               L_program,
                                                               NULL);
      IO_supp_dist_tbl(1).return_code := 'FALSE';
      return;
   elsif I_location is null then
      IO_supp_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                               'I_location',
                                                               L_program,
                                                               NULL);
      IO_supp_dist_tbl(1).return_code := 'FALSE';
      return;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_REPL_SUPP_DIST_CNT',
                    'REPL_ITEM_LOC_SUPP_DIST',
                    I_item||' - '||I_location);
   open  C_GET_REPL_SUPP_DIST_CNT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_REPL_SUPP_DIST_CNT',
                    'REPL_ITEM_LOC_SUPP_DIST',
                    I_item||' - '||I_location);
   fetch C_GET_REPL_SUPP_DIST_CNT INTO L_count;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_REPL_SUPP_DIST_CNT',
                    'REPL_ITEM_LOC_SUPP_DIST',
                    I_item||' - '||I_location);
   close C_GET_REPL_SUPP_DIST_CNT;
   ---
   if L_count = 0 then
      if INSERT_DEFAULT_DIST(L_error_message,
                             I_item,
                             I_location) = FALSE then
         raise_application_error(ERRNUM_PACKAGE_CALL, L_error_message);
      end if;
   end if;
   ----
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_LOC_DIST_EXISTS_MSOB_Y',
                    'REPL_ITEM_LOC_SUPP_DIST, PARTNER_ORG_UNIT',
                    I_item||' - '||I_location);
   open  C_ITEM_LOC_DIST_EXISTS_MSOB_Y;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_LOC_DIST_EXISTS_MSOB_Y',
                    'REPL_ITEM_LOC_SUPP_DIST, PARTNER_ORG_UNIT',
                    I_item||' - '||I_location);
   fetch C_ITEM_LOC_DIST_EXISTS_MSOB_Y BULK COLLECT INTO IO_supp_dist_tbl;


   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_LOC_DIST_EXISTS_MSOB_Y',
                    'REPL_ITEM_LOC_SUPP_DIST, PARTNER_ORG_UNIT',
                    I_item||' - '||I_location);
   close C_ITEM_LOC_DIST_EXISTS_MSOB_Y;
   ---  
   

EXCEPTION
   when OTHERS then
      IO_supp_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                               SQLERRM,
                                                               L_program,
                                                               to_char(SQLCODE));
      IO_supp_dist_tbl(1).return_code := 'FALSE';
      return;
      
END QUERY_PROCEDURE;
-------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE (IO_supp_dist_tbl    IN OUT   REPL_SUPP_DIST_TBL_TYPE,
                          I_item              IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                          I_location          IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
IS

   L_program          VARCHAR2(60)     := 'SUP_DIST_SQL.LOCK_PROCEDURE';
   L_table            VARCHAR2(30)     := 'REPL_ITEM_LOC_SUPP_DIST';
   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);


   cursor C_LOCK_RILSD (I_supplier          SUPS.SUPPLIER%TYPE,
                        I_origin_country_id REPL_ITEM_LOC_SUPP_DIST.ORIGIN_COUNTRY_ID%TYPE) is
     select 'x'
       from repl_item_loc_supp_dist rilsd
      where rilsd.item = I_item
        and rilsd.location = I_location
        and rilsd.supplier = I_supplier
        and rilsd.origin_country_id = I_origin_country_id
        for update nowait;

BEGIN

   if I_item is NULL then
      IO_supp_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                               'I_item',
                                                               L_program,
                                                               NULL);
      IO_supp_dist_tbl(1).return_code := 'FALSE';
      return;
   elsif I_location is null then
      IO_supp_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                               'I_location',
                                                               L_program,
                                                               NULL);
      IO_supp_dist_tbl(1).return_code := 'FALSE';
      return;
   end if;
   ---
   for i IN 1..IO_supp_dist_tbl.COUNT loop
      open c_lock_rilsd (IO_supp_dist_tbl(i).supplier,
                         IO_supp_dist_tbl(i).origin_country_id);
      close c_lock_rilsd;
   end loop;
   ---
EXCEPTION
   when RECORD_LOCKED then
      IO_supp_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_item,
                                                               to_char(I_location));
      IO_supp_dist_tbl(1).return_code := 'FALSE';
      return;

   when OTHERS then
      IO_supp_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                               SQLERRM,
                                                               L_program,
                                                               to_char(SQLCODE));
      IO_supp_dist_tbl(1).return_code := 'FALSE';
      return;
END LOCK_PROCEDURE;
-------------------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE (IO_supp_dist_tbl    IN OUT   REPL_SUPP_DIST_TBL_TYPE,
                            I_item              IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                            I_location          IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
IS

   L_program         VARCHAR2(60)              := 'SUP_DIST_SQL.UPDATE_PROCEDURE';

BEGIN

   if I_item is NULL then
      IO_supp_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                               'I_item',
                                                               L_program,
                                                               NULL);
      IO_supp_dist_tbl(1).return_code := 'FALSE';
      return;
   elsif I_location is null then
      IO_supp_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                               'I_location',
                                                               L_program,
                                                               NULL);
      IO_supp_dist_tbl(1).return_code := 'FALSE';
      return;
   end if;
   ---
   for i IN 1..IO_supp_dist_tbl.COUNT loop
      if IO_supp_dist_tbl(i).dist_pct = 0 then
         delete from repl_item_loc_supp_dist r
          where r.item = I_item
            and r.location = I_location
            and r.supplier = IO_supp_dist_tbl(i).supplier
            and r.origin_country_id = IO_supp_dist_tbl(i).origin_country_id;
      elsif IO_supp_dist_tbl(i).dist_pct > 0 then
         merge into repl_item_loc_supp_dist r
              using (select I_item item,
                            I_location location,
                            IO_supp_dist_tbl(i).supplier supplier,
                            IO_supp_dist_tbl(i).origin_country_id origin_country_id,
                            IO_supp_dist_tbl(i).dist_pct dist_pct
                       from dual) d
                 on (r.item = d.item
                 and r.location = d.location
                 and r.supplier = d.supplier
                 and r.origin_country_id = d.origin_country_id)
             when matched then update set r.dist_pct = d.dist_pct,
                                          r.last_update_datetime = SYSDATE,
                                          r.last_update_id = GET_USER
             when not matched then insert (item,
                                           location,
                                           supplier,
                                           origin_country_id,
                                           dist_pct,
                                           last_update_datetime,
                                           last_update_id)
                                   values (d.item,
                                           d.location,
                                           d.supplier,
                                           d.origin_country_id,
                                           d.dist_pct,
                                           SYSDATE,
                                           GET_USER);
      end if;
   end loop;
   
EXCEPTION
   when OTHERS then
      IO_supp_dist_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                               SQLERRM,
                                                               L_program,
                                                               to_char(SQLCODE));
      IO_supp_dist_tbl(1).return_code := 'FALSE';
      return;
END UPDATE_PROCEDURE;
-------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LOC (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists            IN OUT   BOOLEAN,
                         I_item              IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_location          IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60)  := 'SUP_DIST_SQL.CHECK_ITEM_LOC';
   L_exists    VARCHAR2(1)   := 'N';

   cursor C_ITEM_LOC is
      select 'Y'
        from repl_item_loc r
       where r.stock_cat = 'D'
         and nvl(r.status, 'A') = 'A'
         and r.item = I_item
         and r.location = NVL(I_location,r.location)
         and not exists (select 'x'
                           from repl_attr_update_exclude x
                          where x.item = r.item
                            and x.location = r.location
                            and rownum = 1)
         and rownum = 1
      ---------
      UNION ALL
      ---------
       select 'Y'
         from repl_item_loc r
        where r.stock_cat = 'C'
          and nvl(r.status, 'A') = 'A'
          and r.item = I_item
          and r.source_wh = NVL(I_location,r.source_wh)
          and not exists (select 'x'
                           from repl_attr_update_exclude x
                          where x.item = r.item
                            and x.location = r.location
                            and rownum = 1)
         and rownum = 1
      ---------
      UNION ALL
      ---------
      select 'Y'
        from repl_attr_update_head ruh,
             repl_attr_update_item rui,
             repl_attr_update_loc  rul
       where ruh.stock_cat = 'D'
         and ruh.repl_attr_id = rui.repl_attr_id
         and rui.repl_attr_id = rul.repl_attr_id
         and rui.item = I_item
         and rul.loc = NVL(I_location,rul.loc)
         and not exists (select 'x'
                           from repl_attr_update_exclude rue
                          where rue.repl_attr_id = ruh.repl_attr_id
                            and rownum = 1)
         and rownum = 1
      ---------
      UNION ALL
      ---------
      select 'Y'
        from repl_attr_update_head ruh,
             repl_attr_update_item rui
       where ruh.stock_cat = 'C'
         and ruh.repl_attr_id = rui.repl_attr_id
         and rui.item = I_item
         and ruh.sourcing_wh = NVL(I_location,ruh.sourcing_wh)
         and not exists (select 'x'
                           from repl_attr_update_exclude rue
                          where rue.repl_attr_id = ruh.repl_attr_id
                            and rownum = 1)
         and rownum = 1;

BEGIN
   O_exists := FALSE;
   ---
   if I_item is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             I_item,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_LOC',
                    'REPL_ATTR_UPDATE_HEAD, REPL_ITEM_LOC',
                    I_item ||' - '||I_location);
   open  C_ITEM_LOC;

   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_LOC',
                    'REPL_ATTR_UPDATE_HEAD, REPL_ITEM_LOC',
                    I_item ||' - '||I_location);
   fetch C_ITEM_LOC INTO L_exists;

   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_LOC',
                    'REPL_ATTR_UPDATE_HEAD, REPL_ITEM_LOC',
                    I_item ||' - '||I_location);
   close C_ITEM_LOC;

   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_ITEM_LOC;
-------------------------------------------------------------------------------------
END SUP_DIST_SQL;
/

