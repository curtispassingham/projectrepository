
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_DSRCPT_SQL AS

--------------------------------------------------------------------------------
-- PACKAGE GLOBALS
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION DSRCPT_ADD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_dsrcpt_rec    IN     RMSSUB_DSRCPT.DSRCPT_REC_TYPE)
RETURN BOOLEAN;

FUNCTION PROCESS_DS_RCPT_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE,
                        I_loc            IN     STORE.STORE%TYPE,
                        I_tran_date      IN     DATE,
                        I_unit_cost      IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                        I_qty            IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        I_pack_ind       IN     ITEM_MASTER.PACK_IND%TYPE,
                        I_dept           IN     ITEM_MASTER.DEPT%TYPE,
                        I_class          IN     ITEM_MASTER.CLASS%TYPE,
                        I_subclass       IN     ITEM_MASTER.SUBCLASS%TYPE,
                        I_pack_unit_cost IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                        I_pack_qty       IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN;

FUNCTION CHECK_ITEM_LOC(O_error_message IN OUT VARCHAR2,
                        I_item          IN     ITEM_LOC.ITEM%TYPE,
                        I_loc           IN     ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

FUNCTION PERSIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message_type  IN     VARCHAR2,
                 I_dsrcpt_rec    IN     RMSSUB_DSRCPT.DSRCPT_REC_TYPE)
RETURN BOOLEAN IS

L_program   VARCHAR2(50) := 'RMSSUB_DSRCPT_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_DSRCPT.DSRCPT_ADD then
      if DSRCPT_ADD(O_error_message,
                    I_dsrcpt_rec) = FALSE then
         return FALSE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE_TYPE',
      I_message_type, null, null);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;


--------------------------------------------------------------------------------
-- PRIVATE PPROCEDURES
--------------------------------------------------------------------------------

FUNCTION DSRCPT_ADD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_dsrcpt_rec    IN     RMSSUB_DSRCPT.DSRCPT_REC_TYPE)
RETURN BOOLEAN IS

L_program              VARCHAR2(50) := 'RMSSUB_DSRCPT_SQL.DSRCPT_ADD';

L_item                 ITEM_MASTER.ITEM%TYPE := I_dsrcpt_rec.item;
L_loc                  STORE.STORE%TYPE := I_dsrcpt_rec.store;
L_tran_date            DATE := I_dsrcpt_rec.trans_date;
L_unit_cost            ITEM_LOC_SOH.UNIT_COST%TYPE := I_dsrcpt_rec.unit_cost;
L_qty                  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := I_dsrcpt_rec.units;
--
L_pack_ind             ITEM_MASTER.PACK_IND%TYPE := I_dsrcpt_rec.pack_ind;
L_dept                 ITEM_MASTER.DEPT%TYPE := I_dsrcpt_rec.dept;
L_class                ITEM_MASTER.CLASS%TYPE := I_dsrcpt_rec.class;
L_subclass             ITEM_MASTER.SUBCLASS%TYPE := I_dsrcpt_rec.subclass;

L_pack_comp_item       PACKITEM.ITEM%TYPE;
L_pack_comp_item_qty   PACKITEM.PACK_QTY%TYPE;

L_av_cost              ITEM_LOC_SOH.AV_COST%TYPE;
L_pack_unit_cost       ITEM_LOC_SOH.UNIT_COST%TYPE;
L_unit_retail          ITEM_LOC.UNIT_RETAIL%TYPE;
L_sell_unit_retail     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
L_sell_uom             ITEM_LOC.SELLING_UOM%TYPE;

CURSOR C_get_pack_items is
select item,
       pack_qty
  from packitem
 where pack_no = L_item;

BEGIN

   if L_pack_ind = 'Y' then
      if CHECK_ITEM_LOC(O_error_message,
                        L_item,
                        L_loc) = FALSE then
         return FALSE;
      end if;

      if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                  L_item,
                                                  L_loc,
                                                  'S',
                                                  L_av_cost,
                                                  L_pack_unit_cost,
                                                  L_unit_retail,
                                                  L_sell_unit_retail,
                                                  L_sell_uom) = FALSE then
         return FALSE;
      end if;

      for C_record in C_get_pack_items LOOP
         L_pack_comp_item := C_record.item;
         L_pack_comp_item_qty := C_record.pack_qty;  

         if PROCESS_DS_RCPT_ITEM(O_error_message,
                                 L_pack_comp_item,
                                 L_loc,
                                 L_tran_date,
                                 L_unit_cost,
                                 L_pack_comp_item_qty,
                                 L_pack_ind,
                                 L_dept,
                                 L_class,
                                 L_subclass,
                                 L_pack_unit_cost,
                                 L_qty)= FALSE then
            return FALSE;
         end if;
      end LOOP;
   else
      if PROCESS_DS_RCPT_ITEM(O_error_message,
                              L_item,
                              L_loc,
                              L_tran_date,
                              L_unit_cost,
                              L_qty,
                              L_pack_ind,
                              L_dept,
                              L_class,
                              L_subclass,
                              null,
                              null)= FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DSRCPT_ADD;
--------------------------------------------------------------------------------

FUNCTION PROCESS_DS_RCPT_ITEM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE,
                        I_loc            IN     STORE.STORE%TYPE,
                        I_tran_date      IN     DATE,
                        I_unit_cost      IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                        I_qty            IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        I_pack_ind       IN     ITEM_MASTER.PACK_IND%TYPE,
                        I_dept           IN     ITEM_MASTER.DEPT%TYPE,
                        I_class          IN     ITEM_MASTER.CLASS%TYPE,
                        I_subclass       IN     ITEM_MASTER.SUBCLASS%TYPE,
                        I_pack_unit_cost IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                        I_pack_qty       IN     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN IS

L_av_cost              ITEM_LOC_SOH.AV_COST%TYPE := null;
L_comp_unit_cost       ITEM_LOC_SOH.UNIT_COST%TYPE := null;
L_unit_retail          ITEM_LOC.UNIT_RETAIL%TYPE := null;
L_sell_unit_retail     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE := null;
L_sell_uom             ITEM_LOC.SELLING_UOM%TYPE := null;
L_stock_on_hand        ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := null;
L_pack_comp_soh        ITEM_LOC_SOH.PACK_COMP_SOH%TYPE := null;

L_comp_unit_cost_prc   NUMBER(20,4) := null;
L_comp_new_unit_cost   ITEM_LOC_SOH.UNIT_COST%TYPE := null;
L_cal_ave_cost         TRAN_DATA.AV_COST%TYPE := null;
L_current_tot_cost     TRAN_DATA.TOTAL_COST%TYPE := null;
L_comp_item_tot_cost   TRAN_DATA.TOTAL_COST%TYPE := null;
L_item_tot_cost        TRAN_DATA.TOTAL_COST%TYPE := null;
L_new_tot_qty          ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := null;

L_qty                  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := null;
L_total_cost           TRAN_DATA.TOTAL_COST%TYPE := null;
L_total_retail         TRAN_DATA.TOTAL_RETAIL%TYPE := null;

L_program              VARCHAR2(45) := 'RMSSUB_DSRCPT_SQL.PROCESS_DS_RCPT_ITEM';
L_prog_name            VARCHAR2(25) := 'rmssub_dscrpt_sql';
L_tran_code            TRAN_DATA.TRAN_CODE%TYPE := 20;
L_cost_variance        ITEM_LOC_SOH.AV_COST%TYPE;

cursor C_lock_item_loc_soh is
select 'x'
  from item_loc_soh
 where item    = I_item
  and loc     = I_loc
   for update nowait;

BEGIN

   if CHECK_ITEM_LOC(O_error_message,
                     I_item,
                     I_loc) = FALSE then
      return FALSE;
   end if;

   if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                               I_item,
                                               I_loc,
                                               'S',
                                               L_av_cost,
                                               L_comp_unit_cost,
                                               L_unit_retail,
                                               L_sell_unit_retail,
                                               L_sell_uom) = FALSE then
      return FALSE;
   end if;

   if ITEMLOC_ATTRIB_SQL.GET_AV_COST_SOH(O_error_message, 
                                         I_item,
                                         I_loc,
                                         'S',
                                         L_av_cost,
                                         L_stock_on_hand,
                                         L_pack_comp_soh) = FALSE then
      return FALSE;
   end if;

   --CALCULATE the average cost
   if I_pack_ind = 'Y' then
      L_qty                  := I_qty * I_pack_qty;
      --- calculate item's unit cost based on current percent item cost based on the pack
      L_comp_unit_cost_prc   := L_comp_unit_cost / I_pack_unit_cost;
      L_comp_new_unit_cost   := L_comp_unit_cost_prc * I_unit_cost;

      L_total_cost           := L_qty * L_comp_new_unit_cost;
      L_total_retail         := L_qty * L_unit_retail;

      --- Calculate average cost when receiving items.
      if STKLEDGR_ACCTING_SQL.WAC_CALC_QTY_CHANGE(O_error_message,
                                                  L_cal_ave_cost,
                                                  L_cost_variance,
                                                  L_av_cost,
                                                  L_stock_on_hand,
                                                  L_comp_new_unit_cost,
                                                  L_qty,
                                                  L_total_cost,
                                                  FALSE) = FALSE then
         return FALSE;
      end if;
   else
      L_qty                  := I_qty;

      L_total_cost           := L_qty * I_unit_cost;
      L_total_retail         := L_qty * L_unit_retail;

      --- Calculate average cost when receiving items.
      if STKLEDGR_ACCTING_SQL.WAC_CALC_QTY_CHANGE(O_error_message,
                                                  L_cal_ave_cost,
                                                  L_cost_variance,
                                                  L_av_cost,
                                                  L_stock_on_hand,
                                                  I_unit_cost,
                                                  L_qty,
                                                  L_total_cost,
                                                  FALSE) = FALSE then
         return FALSE;
      end if;
   end if;

   --Updating average cost item_loc_soh table
   open C_lock_item_loc_soh;
   close C_lock_item_loc_soh;

   update item_loc_soh
      set av_cost = L_cal_ave_cost,
          stock_on_hand = (stock_on_hand + L_qty),
          soh_update_datetime = sysdate,
          last_update_datetime = sysdate,
          last_update_id = get_user,
          last_received = I_tran_date,
          qty_received = L_qty
    where item = I_item
      and loc = I_loc; 

   --Insert the transaction data into the tran_data table 
   if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                    I_item,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    I_loc,
                                    'S',
                                    I_tran_date,
                                    L_tran_code,
                                    null,
                                    L_qty,
                                    L_total_cost,
                                    L_total_retail,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    L_prog_name,
                                    null) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_DS_RCPT_ITEM;
--------------------------------------------------------------------------------

FUNCTION CHECK_ITEM_LOC(O_error_message IN OUT VARCHAR2,
                        I_item          IN     ITEM_LOC.ITEM%TYPE,
                        I_loc           IN     ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

L_program   VARCHAR2(50) := 'RMSSUB_DSRCPT_SQL.CHECK_ITEM_LOC';
L_exists    VARCHAR2(1) := null;

cursor C_ITEM_LOC is
select 'x'
  from item_loc
 where item = I_item
   and loc  = I_loc
   and loc_type = 'S';

BEGIN

   open C_ITEM_LOC;
   fetch C_ITEM_LOC into L_exists;
   close C_ITEM_LOC;

   if L_exists is null then
      if NEW_ITEM_LOC(O_error_message,           
                      I_item,                      -- I_item
                      I_loc,                       -- I_location
                      null,                        -- I_item_parent
                      null,                        -- I_item_grandparent
                      'S',                         -- I_loc_type
                      null,                        -- I_short_desc
                      null,                        -- I_dept
                      null,                        -- I_class
                      null,                        -- I_subclass
                      null,                        -- I_item_level
                      null,                        -- I_tran level
                      null,                        -- I_item_status
                      null,                        -- I_waste_type
                      null,                        -- I_daily_waste_pct
                      null,                        -- I_sellable_ind
                      null,                        -- I_orderable_ind
                      null,                        -- I_pack_ind
                      null,                        -- I_pack_type
                      null,                        -- I_unit_cost_loc
                      null,                        -- I_unit_retail_loc
                      null,                        -- I_selling_retail_loc
                      null,                        -- I_selling_uom
                      null,                        -- I_item_loc_status
                      null,                        -- I_taxable_ind
                      null,                        -- I_ti
                      null,                        -- I_hi
                      null,                        -- I_store_ord_mult
                      null,                        -- I_meas_of_each
                      null,                        -- I_meas_of_price
                      null,                        -- I_uom_of_price
                      null,                        -- I_primary_varient
                      null,                        -- I_primary_supp
                      null,                        -- I_primary_cntry
                      null,                        -- I_local_item_desc
                      null,                        -- I_local_short_desc
                      null,                        -- I_primary_cost_pack
                      null,                        -- I_receive_as_type
                      sysdate,                     -- I_date
                      null) = FALSE then           -- I_default_to_children
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ITEM_LOC;
--------------------------------------------------------------------------------

END RMSSUB_DSRCPT_SQL;
/
