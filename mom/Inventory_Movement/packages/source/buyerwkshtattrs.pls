CREATE OR REPLACE PACKAGE BUYER_WKSHT_ATTRIB_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------
--- Function Name: CALC_TOTAL
--- Purpose:       This function will calculate the totals of the selected line items from 
---                REPL_RESULTS, IB_RESULTS and BUYER_WKSHT_MANUAL.  If I_uom_type is 'AMNT'
---                then the total unit cost of the line items selected will be converted to 
---                the currency passed through I_to_uom.  Otherwise, the total ordered will be
---                converted to the unit of measure passed through I_to_uom.
------------------------------------------------------------------------------
FUNCTION CALC_TOTAL(O_error_message   IN OUT   VARCHAR2,
                    O_total           IN OUT   REPL_RESULTS.UNIT_COST%TYPE,
                    I_audsid          IN       REPL_RESULTS.AUDSID%TYPE,
                    I_uom_type        IN       CODE_DETAIL.CODE%TYPE,
                    I_to_uom          IN       ITEM_MASTER.STANDARD_UOM%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: CHECK_ADD_TO_PO
--- Purpose:       This function calls CHECK_ID, CHECK_MULT_SUPP_ITEM and CHECK_ORDER_QTY
---                to validate the selected line items before displaying the PO List Window
---                on the Buyer Worksheet form.  The following is checked in this function:
---                1.  Line items are selected.
---                2.  Duplicate source type, item, supplier, origin country and location line 
---                    items have not been selected.
---                3.  Selected line items have the same supplier.
---                4.  The same item selected more than once has the same origin country.
---                5.  The same item/location selected more than once has the same unit cost.
---                6.  No selected line items have an order quantity that is less than
---                    or equal to zero.
------------------------------------------------------------------------------
FUNCTION CHECK_ADD_TO_PO(O_error_message            IN OUT   VARCHAR2,
                         O_selected_exists          IN OUT   BOOLEAN,
                         O_dup_source_type_exists   IN OUT   BOOLEAN,
                         O_mult_supp_exists         IN OUT   BOOLEAN,
                         O_mult_item_cntry_exists   IN OUT   BOOLEAN,
                         O_mult_unit_cost_exists    IN OUT   BOOLEAN,
                         O_zero_qty_exists          IN OUT   BOOLEAN,
                         I_audsid                   IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: CHECK_ID
--- Purpose:       This function checks if there exist any records on REPL_RESULTS, IB_RESULTS
---                and BUYER_WKSHT_MANUAL for the entered audsid.
------------------------------------------------------------------------------
FUNCTION CHECK_ID(O_error_message   IN OUT   VARCHAR2,
                  O_exists          IN OUT   BOOLEAN,
                  I_audsid          IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: CHECK_MULT_EXISTS
--- Purpose:       This function checks if multiple suppliers, pool supplier, item/origin countries,
---                item/location/unit costs, departments and locations have been selected. The
---                function will perform the check only if its associated I_check parameter contains
---                a 'Y'.  Also, O_supplier, O_pool_supplier, O_dept, O_location, O_loc_type and O_vwh
---                will only contain values if the associated O_mult_exists parameter is FALSE.
---                If I_exit_when_mult is 'Y', the function will return TRUE as soon as multiple 
---                information is found.
------------------------------------------------------------------------------
FUNCTION CHECK_MULT_EXISTS(O_error_message            IN OUT   VARCHAR2,
                           O_mult_supp_exists         IN OUT   BOOLEAN,
                           O_supplier                 IN OUT   REPL_RESULTS.PRIMARY_REPL_SUPPLIER%TYPE,
                           O_mult_pool_supp_exists    IN OUT   BOOLEAN,
                           O_pool_supplier            IN OUT   REPL_RESULTS.POOL_SUPPLIER%TYPE,
                           O_mult_item_cntry_exists   IN OUT   BOOLEAN,
                           O_mult_unit_cost_exists    IN OUT   BOOLEAN,
                           O_mult_dept_exists         IN OUT   BOOLEAN,
                           O_dept                     IN OUT   REPL_RESULTS.DEPT%TYPE,
                           O_mult_loc_exists          IN OUT   BOOLEAN,
                           O_location                 IN OUT   REPL_RESULTS.LOCATION%TYPE,
                           O_loc_type                 IN OUT   REPL_RESULTS.LOC_TYPE%TYPE,
                           O_vwh                      IN OUT   REPL_RESULTS.LOCATION%TYPE,
                           I_check_supp               IN       VARCHAR2,
                           I_check_pool_supp          IN       VARCHAR2,
                           I_check_item               IN       VARCHAR2,
                           I_check_unit_cost          IN       VARCHAR2,
                           I_check_dept               IN       VARCHAR2,
                           I_check_loc                IN       VARCHAR2,
                           I_exit_when_mult           IN       VARCHAR2,
                           I_audsid                   IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: CHECK_ORDER_QTY
--- Purpose:       This function checks if there exist any selected records on REPL_RESULTS, 
---                IB_RESULTS and BUYER_WKSHT_MANUAL that have an order quantity less than
---                or equal to zero.
------------------------------------------------------------------------------
FUNCTION CHECK_ORDER_QTY(O_error_message   IN OUT   VARCHAR2,
                         O_exists          IN OUT   BOOLEAN,
                         I_audsid          IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: CHECK_PO_UNIT_COST
--- Purpose:       This function checks if the specified order item's unit cost is different
---                from the selected worksheet item's unit cost.
------------------------------------------------------------------------------
FUNCTION CHECK_PO_UNIT_COST(O_error_message           IN OUT   VARCHAR2,
                            O_mult_unit_cost_exists   IN OUT   BOOLEAN,
                            I_order_no                IN       ORDHEAD.ORDER_NO%TYPE,
                            I_audsid                  IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: CONSTRAINTS_AND_CALC
--- Purpose:       This function retrieves the appropriate scaling constraints and calculates
---                and converts the total unit cost or total order quantity of the selected
---                line items, depending on the scaling constraint types of the scaling 
---                constraints retrieved.
------------------------------------------------------------------------------
FUNCTION CONSTRAINTS_AND_CALC(O_error_message           IN OUT   VARCHAR2,
                              O_exists                  IN OUT   BOOLEAN,
                              O_supplier                IN OUT   SUPS.SUPPLIER%TYPE,
                              O_sup_name                IN OUT   SUPS.SUP_NAME%TYPE,
                              O_scale_cnstr_type1       IN OUT   SUP_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                              O_scale_cnstr_uom1        IN OUT   SUP_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                              O_scale_cnstr_curr1       IN OUT   SUP_INV_MGMT.SCALE_CNSTR_CURR1%TYPE,
                              O_scale_cnstr_min_val1    IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL1%TYPE,
                              O_scale_cnstr_max_val1    IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL1%TYPE,
                              O_actual1                 IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL1%TYPE,
                              O_scale_cnstr_type2       IN OUT   SUP_INV_MGMT.SCALE_CNSTR_TYPE2%TYPE,
                              O_scale_cnstr_uom2        IN OUT   SUP_INV_MGMT.SCALE_CNSTR_UOM2%TYPE,
                              O_scale_cnstr_curr2       IN OUT   SUP_INV_MGMT.SCALE_CNSTR_CURR2%TYPE,
                              O_scale_cnstr_min_val2    IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL2%TYPE,
                              O_scale_cnstr_max_val2    IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL2%TYPE,
                              O_actual2                 IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL2%TYPE,
                              O_total                   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL2%TYPE,  
                              I_audsid                  IN       REPL_RESULTS.AUDSID%TYPE,
                              I_uom_type                IN       CODE_DETAIL.CODE%TYPE,
                              I_to_uom                  IN       ITEM_MASTER.STANDARD_UOM%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: DUP_EXISTS
--- Purpose:       This function will check if a record exists on REPL_RESULTS or
---                BUYER_WKSHT_MANUAL for the passed in item, supplier, origin country
---                and location.  If more than one record exists on REPL_RESULTS, the 
---                record with the most recent repl_date will be retrieved.
------------------------------------------------------------------------------
FUNCTION DUP_EXISTS(O_error_message       IN OUT   VARCHAR2,
                    O_exists              IN OUT   BOOLEAN,
                    O_rowid               IN OUT   ROWID,
                    O_source_type         IN OUT   BUYER_WKSHT_MANUAL.SOURCE_TYPE%TYPE,
                    I_item                IN       BUYER_WKSHT_MANUAL.ITEM%TYPE,
                    I_supplier            IN       BUYER_WKSHT_MANUAL.SUPPLIER%TYPE,
                    I_origin_country_id   IN       BUYER_WKSHT_MANUAL.ORIGIN_COUNTRY_ID%TYPE,
                    I_location            IN       BUYER_WKSHT_MANUAL.LOCATION%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: DUP_EXISTS
--- Purpose:       This function will check if multiple worksheet items were selected
---                that have the same source type, item, supplier, origin country and
---                location.
------------------------------------------------------------------------------
FUNCTION DUP_SOURCE_TYPE_EXISTS(O_error_message   IN OUT   VARCHAR2,
                                O_exists          IN OUT   BOOLEAN,
                                I_audsid          IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: GET_BUYER_WKSHT_MANUAL_INFO
--- Purpose:       This function will grab the information needed for inserting
---                into BUYER_WKSHT_MANUAL from the Buyer Worksheet form.
------------------------------------------------------------------------------
FUNCTION GET_BUYER_WKSHT_MANUAL_INFO(O_error_message       IN OUT   VARCHAR2,
                                     O_dept                IN OUT   ITEM_MASTER.DEPT%TYPE,
                                     O_class               IN OUT   ITEM_MASTER.CLASS%TYPE,
                                     O_subclass            IN OUT   ITEM_MASTER.SUBCLASS%TYPE,
                                     O_item_type           IN OUT   BUYER_WKSHT_MANUAL.ITEM_TYPE%TYPE,
                                     O_comp_item           IN OUT   ITEM_MASTER.ITEM%TYPE,
                                     O_buyer               IN OUT   DEPS.BUYER%TYPE,
                                     O_pool_supplier       IN OUT   SUPS.SUPPLIER%TYPE,
                                     O_physical_wh         IN OUT   WH.WH%TYPE,
                                     O_repl_wh_link        IN OUT   WH.REPL_WH_LINK%TYPE,
                                     O_supp_lead_time      IN OUT   ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                                     O_pickup_lead_time    IN OUT   ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                                     O_supp_unit_cost      IN OUT   ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                                     O_supp_pack_size      IN OUT   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                     O_ti                  IN OUT   ITEM_SUPP_COUNTRY.TI%TYPE,
                                     O_hi                  IN OUT   ITEM_SUPP_COUNTRY.HI%TYPE,
                                     I_item                IN       BUYER_WKSHT_MANUAL.ITEM%TYPE,
                                     I_supplier            IN       BUYER_WKSHT_MANUAL.SUPPLIER%TYPE,
                                     I_origin_country_id   IN       BUYER_WKSHT_MANUAL.ORIGIN_COUNTRY_ID%TYPE,
                                     I_loc_type            IN       BUYER_WKSHT_MANUAL.LOC_TYPE%TYPE,
                                     I_location            IN       BUYER_WKSHT_MANUAL.LOCATION%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: NON_PROCESSED_EXISTS
--- Purpose:       This function will check if any non-processed transfer line items exist.
------------------------------------------------------------------------------
FUNCTION NON_PROCESSED_EXISTS(O_error_message   IN OUT   VARCHAR2,
                              O_exists          IN OUT   BOOLEAN,
                              I_tsf_no          IN       TSFDETAIL.TSF_NO%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: VALIDATE_ADD_ITEM
--- Purpose:       This function will check if records exist on ITEM_SUPP_COUNTRY_LOC for the
---                the specified item, supplier, origin country and/or location. 
------------------------------------------------------------------------------
FUNCTION VALIDATE_ADD_ITEM(O_error_message       IN OUT   VARCHAR2,
                           O_exists              IN OUT   BOOLEAN,
                           O_unit_cost           IN OUT   ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                           I_item                IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                           I_supplier            IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                           I_origin_country_id   IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                           I_location            IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- Function Name: VALIDATE_PO
--- Purpose:       This function will check if the entered order satisfies several requirements including:
---                1. The order has the same supplier.
---                2. If one department was selected and the order has a department specified, the two
---                   departments are the same.
---                3. If the order contains the same items as the ones selected, their origin
---                   countries are the same.
---                4. If several locations were selected, that ordhead's location field is NULL.
---                5. If one location was selected, ordhead's location field contains the location or is
---                   NULL.
------------------------------------------------------------------------------
FUNCTION VALIDATE_PO(O_error_message   IN OUT   VARCHAR2,
                     O_valid           IN OUT   BOOLEAN,
                     I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                     I_audsid          IN       REPL_RESULTS.AUDSID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- Function Name: CHECK_AOQ
--- Purpose:       This function will compare the value of the AOQ with the min order qty and 
---                the max order qty for that item that is stored in the item_supp_country table. 
---                The value of AOQ should always be within the min order qty and the max order qty.
------------------------------------------------------------------------------
FUNCTION CHECK_AOQ(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item             IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                   I_aoq              IN       NUMBER,
                   I_supplier         IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                   I_origin_ctry_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- Function Name: DUP_EXISTS_FOR_ITEM_LOC
--- Purpose:       This function will check if a record exists on REPL_RESULTS or
---                BUYER_WKSHT_MANUAL for the passed in item,location but supplier or
---                origin country other than the passed supplier and origin country
------------------------------------------------------------------------------
FUNCTION DUP_EXISTS_FOR_ITEM_LOC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists              IN OUT   BOOLEAN,
                                 I_item                IN       BUYER_WKSHT_MANUAL.ITEM%TYPE,
                                 I_location            IN       BUYER_WKSHT_MANUAL.LOCATION%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name: DUP_EXISTS_MANUAL
--- Purpose:       This function will check if a record exists on 
---                BUYER_WKSHT_MANUAL for the passed in item,location and origin country
------------------------------------------------------------------------------
FUNCTION DUP_EXISTS_MANUAL(O_error_message       IN OUT   VARCHAR2,
                           O_exists              IN OUT   BOOLEAN,
                           O_rowid               IN OUT   ROWID,
                           I_item                IN       BUYER_WKSHT_MANUAL.ITEM%TYPE,
                           I_supplier            IN       BUYER_WKSHT_MANUAL.SUPPLIER%TYPE,
                           I_origin_country_id   IN       BUYER_WKSHT_MANUAL.ORIGIN_COUNTRY_ID%TYPE,
                           I_location            IN       BUYER_WKSHT_MANUAL.LOCATION%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------------------------------
END BUYER_WKSHT_ATTRIB_SQL;
/
