
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_RECEIPT_ERROR AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
-- Usage:
--
-- In the receiving process, call INIT before processing any receipts.  This
-- will initialize all of the global variables.
--
-- For each receipt record, call BEGIN_RECEIPT.  This function will hold the
-- header level values in global variables which may be used to build an error
-- record when necessary.
--
-- When an error is encountered in the receiving process, call ADD_ERROR.  This
-- function save the error information in a temp PL/SQL table.
--
-- When an entire receipt has been processed, call END_RECEIPT.  This function
-- will go through the temp table and group all similar errors (error type).
-- It then creates a complete receipt message for each error type, adding the
-- similar errors as detail nodes.  Then it creates an error object for each
-- receipt message and adds the error object to the global error table.
--
-- At the end of the receiving process, call FINISH.  This will pass out a copy
-- of the global error table (if any errors exist) which is then sent to the
-- RIB for further processing.
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- INIT
-- Initializes global variables.
-------------------------------------------------------------------------------
FUNCTION INIT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
              I_message_type   IN      VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- BEGIN_RECEIPT
-- Copies header level info for a receipt into global variables.  Used to
-- create an error record during carton level receiving.
-------------------------------------------------------------------------------
FUNCTION BEGIN_RECEIPT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_appt_nbr         IN      APPT_HEAD.APPT%TYPE,
                       I_rib_receipt_rec  IN      "RIB_Receipt_REC")
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- ADD_ERROR
-- Used whenever an item or carton error occurs within the receiving process.
-- Saves the error information to a temp PL/SQL table of errors.
-------------------------------------------------------------------------------
FUNCTION ADD_ERROR(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_error_type     IN      VARCHAR2,
                   I_error_code     IN      VARCHAR2,
                   I_error_desc     IN      VARCHAR2,
                   I_error_level    IN      VARCHAR2,
                   I_error_object   IN      RIB_OBJECT)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- END_RECEIPT
-- Creates error objects by grouping all similar error records and creating
-- receipt messages for them.  Adds these error objects to the global error
-- table.
-------------------------------------------------------------------------------
FUNCTION END_RECEIPT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- FINISH
-- Copies global error table (if any errors exist) to the output parameter.
-- This table is then sent back to the RIB for further processing.
-------------------------------------------------------------------------------
FUNCTION FINISH(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                O_rib_error_tbl     OUT  RIB_ERROR_TBL)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
END RMSSUB_RECEIPT_ERROR;
/
