CREATE OR REPLACE PACKAGE REC_UNIT_ADJ_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
-- Function Name: CHECK_RECORDS
-- Purpose : This function is used to either create a new child shipment or update
--           an existing shipment when a Receiver Unit Adjustment is made.
-- Created : 26-MAR-04 by Cathal Crawford
--------------------------------------------------------------------------------------
FUNCTION CHECK_RECORDS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_shipment              IN       SHIPMENT.SHIPMENT%TYPE,
                       I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                       I_seq_no                IN       SHIPSKU.SEQ_NO%TYPE,
                       I_adjusted_qty          IN       SHIPSKU.QTY_RECEIVED%TYPE,
                       I_carton                IN       SHIPSKU.CARTON%TYPE,
                       I_calling_prg           IN       VARCHAR2,
                       I_adjusted_weight       IN       SHIPSKU.WEIGHT_RECEIVED%TYPE DEFAULT NULL,
                       I_adjusted_weight_uom   IN       SHIPSKU.WEIGHT_RECEIVED_UOM%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: LOCK_SHIPSKU_RECORD
-- Purpose : This function locks the shipsku record for update of qty received for
--           receiver unit adjustment
-- Created : 31-MAY-13 by Kristine Ignacio
--------------------------------------------------------------------------------------
FUNCTION LOCK_SHIPSKU_RECORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_shipment        IN       SHIPMENT.SHIPMENT%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_carton          IN       SHIPSKU.CARTON%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
END REC_UNIT_ADJ_SQL;
/
