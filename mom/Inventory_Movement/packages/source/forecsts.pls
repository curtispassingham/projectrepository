
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE FORECASTS_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: GET_ITEM_FORECAST_IND 
-- Purpose  : Checks the forecast_ind on the item table.  Returns a Y or N
--            in the O_forecast_ind parameter.  
-- Calls    : <none>
-- Created  : 7-APR-97 By Greg Nathe 
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_FORECAST_IND(O_error_message IN OUT VARCHAR2,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE,
                               O_forecast_ind  IN OUT VARCHAR2) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_SYSTEM_FORECAST_IND 
-- Purpose  : Checks the forecast_ind on the system_options table.  Returns a Y or N
--                in the O_forecast_ind parameter. 
-- Calls    : <none>
-- Created  : 7-APR-97 By Greg Nathe 
---------------------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_FORECAST_IND(O_error_message IN OUT VARCHAR2,
                                 O_forecast_ind  IN OUT SYSTEM_OPTIONS.FORECAST_IND%TYPE) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_FORECAST_MIN_MAX_DATES 
-- Purpose  : Retrieves the minimum and maximum dates of forecasts available in  
--                the system. 
-- Calls    : <none>
-- Created  : 21-APR-97 By Ryan Nelson 
---------------------------------------------------------------------------------------------
FUNCTION GET_FORECAST_MIN_MAX_DATES(O_error_message      IN OUT VARCHAR2,
                                    O_forecast_min_date  IN OUT DATE,
                                    O_forecast_max_date  IN OUT DATE) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DOMAIN_EXISTS 
-- Purpose  : This function will be used to determine if an item's merchandise  
--                hierarchy is associated to a forecast domain. 
-- Calls    : <none>
-- Created  : 21-APR-97 By Ryan Nelson 
---------------------------------------------------------------------------------------------
FUNCTION DOMAIN_EXISTS(O_error_message  IN OUT VARCHAR2,
                       I_dept           IN     DEPS.DEPT%TYPE,
                       I_class          IN     CLASS.CLASS%TYPE,
                       I_subclass       IN     SUBCLASS.SUBCLASS%TYPE,
                       O_domain_exists  IN OUT BOOLEAN) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_DOMAIN 
-- Purpose  : This function will be used to retrieve the domain  
--                for a merchandise hierarchy. 
-- Calls    : GET_DOMAIN_LEVEL 
-- Created  : 21-APR-97 By Greg Nathe 
--------------------------------------------------------------------------
FUNCTION GET_DOMAIN (O_error_message IN OUT VARCHAR2,
                     I_dept          IN     DEPS.DEPT%TYPE,
                     I_class         IN     CLASS.CLASS%TYPE,
                     I_subclass      IN     SUBCLASS.SUBCLASS%TYPE,
                     O_domain_id     IN OUT NUMBER) RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name: GET_DOMAIN_LEVEL 
-- Purpose  : This function will be used to retrieve the domain  
--                level from system options. 
-- Calls    : <none>
-- Created  : 21-APR-97 By Greg Nathe 
--------------------------------------------------------------------------
FUNCTION GET_DOMAIN_LEVEL  (O_error_message IN OUT VARCHAR2,
                            O_domain_level  IN OUT SYSTEM_OPTIONS.DOMAIN_LEVEL%TYPE) RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name: FORECAST_SUB_ITEMS 
-- Purpose  : This function will be used to determine if the item  
--            is a main item for substitute items that will be
--            use forecasted sales. 
-- Calls    : <none>
-- Created  : 25-APR-97 By Catherine Gilmore 
--------------------------------------------------------------------------
FUNCTION FORECAST_SUB_ITEMS  (O_error_message      IN OUT VARCHAR2,
                              I_item               IN     ITEM_MASTER.ITEM%TYPE,
                              O_forecast_sub_items IN OUT BOOLEAN) RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name: FORECAST_MAIN_ITEMS 
-- Purpose  : This function will be used to determine if the item  
--            is a subsitute item for a main item that is  
--            using forecasted sales. 
-- Calls    : <none>
-- Created  : 19-MAY-97  
-------------------------------------------------------------------------
FUNCTION FORECAST_MAIN_ITEMS(O_error_message       IN OUT VARCHAR2,
                             I_item                IN     ITEM_MASTER.ITEM%TYPE,
                             O_forecast_main_items IN OUT BOOLEAN) RETURN BOOLEAN;
--------------------------------------------------------------------------- 
-- Function Name: CLEAR_ITEM_LAST_EXPRT_DATE
-- Purpose  : This function will check if there exists a last sales export date
--            for the item.  If so, it will clear the date for that item as well as
--            for the item's children.
-- Calls    : <none>
-- Created  : 14-SEP-00
--------------------------------------------------------------------------- 
FUNCTION CLEAR_ITEM_LAST_EXPRT_DATE(O_error_message         IN OUT   VARCHAR2,
                                    I_item                  IN       ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------- 
-- Function Name: GET_DOMAIN_DESC
-- Purpose  : This function will check if the domain id exists and if it exists 
--            the description will be returned. 
-- Calls    : <none>
-- Created  : 29-SEP-03
--------------------------------------------------------------------------- 
FUNCTION GET_DOMAIN_DESC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_domain_id     IN     DOMAIN.DOMAIN_ID%TYPE,
                         O_domain_desc   IN OUT DOMAIN.DOMAIN_DESC%TYPE,
                         O_valid         IN OUT BOOLEAN)
return BOOLEAN;
--------------------------------------------------------------------------- 
-- Function Name: RETAIN_FORECAST_HIST
-- Purpose  : This function preserves 4 weeks of history of item forecasted sales 
--            data on the item_forecast_hist table. 4 weeks of history is defined
--            as the last eow_date with regard to the vdate and 3 prior to that.
--            This function will be invoked in a weekly batch process before
--            rmsl_rpas_forecast.ksh truncates the data in item_forecast.
-- Calls    : <none>
-- Created  : 6-MAY-2016
--------------------------------------------------------------------------- 
FUNCTION RETAIN_FORECAST_HIST(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------- 
END;
/
