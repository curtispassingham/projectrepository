----------------------------------------------------------------------------
-- OBJECT CREATED :  OBJ_ALLOC_DETAIL_REC, OBJ_ALLOC_DETAIL_TBL 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object OBJ_ALLOC_DETAIL_TBL

DROP TYPE OBJ_ALLOC_DETAIL_TBL FORCE
/

PROMPT Dropping Object OBJ_ALLOC_DETAIL_REC

DROP TYPE OBJ_ALLOC_DETAIL_REC FORCE
/

--------------------------------------
--       Creating Types
--------------------------------------

PROMPT Creating Object OBJ_ALLOC_DETAIL_REC

CREATE OR REPLACE TYPE OBJ_ALLOC_DETAIL_REC AS OBJECT
(
  ALLOC_NO           NUMBER(10),
  ORDER_NO           NUMBER(12),
  WH                 NUMBER(10),
  TO_LOC             NUMBER(10),
  TO_LOC_TYPE        VARCHAR2(1),
  QTY_TRANSFERRED    NUMBER(12,4),
  QTY_ALLOCATED      NUMBER(12,4),
  NON_SCALE_IND      VARCHAR2(1),
  QTY_PRESCALED      NUMBER(12,4)
)
/ 

PROMPT Creating Object OBJ_ALLOC_DETAIL_TBL

CREATE OR REPLACE TYPE OBJ_ALLOC_DETAIL_TBL AS TABLE OF OBJ_ALLOC_DETAIL_REC
/ 
