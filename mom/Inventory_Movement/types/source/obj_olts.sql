drop type OBJ_OLTS_TBL force
/

drop type OBJ_OLTS_REC force
/

create or replace TYPE OBJ_OLTS_REC as object (
   I_ITEM                   VARCHAR2(25),
   I_LOCN                   NUMBER(10),
   THREAD_ID                NUMBER(10),
   I_CURR_ORDER_LEAD_TIME   NUMBER(5),
   I_NEXT_ORDER_LEAD_TIME   NUMBER(5),
   I_CURR_DELIVERY_DATE     DATE,
   I_NEXT_DELIVERY_DATE     DATE,
   I_LAST_DELIVERY_DATE     DATE,
   I_DAYS_ADDED_TO_COLT     NUMBER(3),
   I_DAYS_ADDED_TO_NOLT     NUMBER(3),
   I_SUPP_LEAD_TIME         NUMBER(4),
   I_WH_LEAD_TIME           NUMBER(4),
constructor function OBJ_OLTS_REC
(
   I_ITEM                   VARCHAR2   DEFAULT NULL,
   I_LOCN                   NUMBER     DEFAULT NULL,
   THREAD_ID                NUMBER     DEFAULT NULL,
   I_CURR_ORDER_LEAD_TIME   NUMBER     DEFAULT NULL,
   I_NEXT_ORDER_LEAD_TIME   NUMBER     DEFAULT NULL,
   I_CURR_DELIVERY_DATE     DATE       DEFAULT NULL,
   I_NEXT_DELIVERY_DATE     DATE       DEFAULT NULL,
   I_LAST_DELIVERY_DATE     DATE       DEFAULT NULL,
   I_DAYS_ADDED_TO_COLT     NUMBER     DEFAULT NULL,
   I_DAYS_ADDED_TO_NOLT     NUMBER     DEFAULT NULL,
   I_SUPP_LEAD_TIME         NUMBER     DEFAULT NULL,
   I_WH_LEAD_TIME           NUMBER     DEFAULT NULL
) return self as result
)
/

create or replace type body OBJ_OLTS_REC is
constructor function OBJ_OLTS_REC
(
   I_ITEM                   VARCHAR2   DEFAULT NULL,
   I_LOCN                   NUMBER     DEFAULT NULL,
   THREAD_ID                NUMBER     DEFAULT NULL,
   I_CURR_ORDER_LEAD_TIME   NUMBER     DEFAULT NULL,
   I_NEXT_ORDER_LEAD_TIME   NUMBER     DEFAULT NULL,
   I_CURR_DELIVERY_DATE     DATE       DEFAULT NULL,
   I_NEXT_DELIVERY_DATE     DATE       DEFAULT NULL,
   I_LAST_DELIVERY_DATE     DATE       DEFAULT NULL,
   I_DAYS_ADDED_TO_COLT     NUMBER     DEFAULT NULL,
   I_DAYS_ADDED_TO_NOLT     NUMBER     DEFAULT NULL,
   I_SUPP_LEAD_TIME         NUMBER     DEFAULT NULL,
   I_WH_LEAD_TIME           NUMBER     DEFAULT NULL
) return self as result is
begin
  self.I_item                   := I_item;
  self.I_locn                   := I_locn;
  self.thread_id                := thread_id;
  self.I_curr_order_lead_time   := I_curr_order_lead_time;
  self.I_next_order_lead_time   := I_next_order_lead_time;
  self.I_curr_delivery_date     := I_curr_delivery_date;
  self.I_next_delivery_date     := I_next_delivery_date;
  self.I_last_delivery_date     := I_last_delivery_date;
  self.I_days_added_to_colt     := I_days_added_to_colt;
  self.I_days_added_to_nolt     := I_days_added_to_nolt;
  self.I_supp_lead_time         := I_supp_lead_time;
  self.I_wh_lead_time           := I_wh_lead_time;
  return;
end;
end;
/

CREATE OR REPLACE TYPE OBJ_OLTS_TBL AS TABLE OF OBJ_OLTS_REC
/
