DROP TYPE WRP_ITEM_RECV_TBL FORCE
/
DROP TYPE WRP_ITEM_RECV_REC FORCE
/

CREATE OR REPLACE TYPE WRP_ITEM_RECV_REC AS OBJECT (ITEM  VARCHAR2(25),
                                ITEM_DESC             VARCHAR2(250) ,
                                CARTON                VARCHAR2(20),
                                DISTRO_NO             NUMBER(12),
                                DISTRO_TYPE           VARCHAR2(1) ,
                                QTY_EXPECTED          NUMBER(12,4) ,
                                QTY_RECEIVED          NUMBER(12,4) ,
                                RECEIPT_UNIT         VARCHAR2(4),
                                WEIGHT_RECEIVED        NUMBER(12,4),
                                WEIGHT_RECEIVED_UOM   VARCHAR2(4) ,
                                ERROR_MESSAGE         VARCHAR2(255),
                                RETURN_CODE           VARCHAR2(5),
                                INV_STATUS            VARCHAR2(10) )
/
CREATE OR REPLACE TYPE WRP_ITEM_RECV_TBL AS TABLE OF WRP_ITEM_RECV_REC
/