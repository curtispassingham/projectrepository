----------------------------------------------------------------------------
-- OBJECT CREATED :                    OBJ_ALLOC_STATUS_TBL 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object OBJ_ALLOC_STATUS_TBL

DROP TYPE OBJ_ALLOC_STATUS_TBL FORCE
/

PROMPT Dropping Object OBJ_ALLOC_STATUS_REC

DROP TYPE OBJ_ALLOC_STATUS_REC FORCE
/

--------------------------------------
--       Creating Types
--------------------------------------

PROMPT Creating Object OBJ_ALLOC_STATUS_REC

CREATE OR REPLACE TYPE OBJ_ALLOC_STATUS_REC AS OBJECT
(
  ALLOC_NO           NUMBER(10),
  STATUS             VARCHAR2(1)
)
/

PROMPT Creating Object OBJ_ALLOC_STATUS_TBL

CREATE OR REPLACE TYPE OBJ_ALLOC_STATUS_TBL AS TABLE OF OBJ_ALLOC_STATUS_REC
/ 
