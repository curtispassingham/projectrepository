drop type obj_shipsku_tbl FORCE
/

drop type obj_shipsku_rec FORCE
/

create or replace TYPE obj_shipsku_rec AS OBJECT (
  SHIPMENT               NUMBER(12,0),
  SEQ_NO                 NUMBER(10,0),
  ITEM                   VARCHAR2(25),
  DISTRO_NO              NUMBER(10,0),
  DISTRO_TYPE            VARCHAR2(1),
  REF_ITEM               VARCHAR2(25),
  CARTON                 VARCHAR2(20),
  INV_STATUS             NUMBER(2,0),
  STATUS_CODE            VARCHAR2(1),
  QTY_RECEIVED           NUMBER(12,4),
  UNIT_COST              NUMBER(20,4),
  UNIT_RETAIL            NUMBER(20,4),
  QTY_EXPECTED           NUMBER(12,4),
  MATCH_INVC_ID          NUMBER(10,0),
  ADJUST_TYPE            VARCHAR2(6),
  ACTUAL_RECEIVING_STORE NUMBER(10,0),
  RECONCILE_USER_ID      VARCHAR2(30),
  RECONCILE_DATE         DATE,
  TAMPERED_IND           VARCHAR2(1),
  DISPOSITIONED_IND      VARCHAR2(1),
  QTY_MATCHED            NUMBER(12,4),
  WEIGHT_RECEIVED        NUMBER(12,4),
  WEIGHT_RECEIVED_UOM    VARCHAR2(4),
  WEIGHT_EXPECTED        NUMBER(12,4),
  WEIGHT_EXPECTED_UOM    VARCHAR2(4)
)
/

create or replace TYPE obj_shipsku_tbl AS TABLE OF obj_shipsku_rec
/
