
prompt dropping WRP_CARTON_RECV_REC AND WRP_CARTON_RECV_TBL
------------------------------------------------------------------------------------------
BEGIN
  EXECUTE immediate 'DROP type WRP_CARTON_RECV_TBL force';
  EXECUTE immediate 'DROP type WRP_CARTON_RECV_REC force';
EXCEPTION
WHEN OTHERS THEN
  NULL;
END;
/
------------------------------------------------------------------------------------------
prompt creating WRP_CARTON_RECV_REC

CREATE OR REPLACE TYPE WRP_CARTON_RECV_REC AS OBJECT (CONTAINER_ID          VARCHAR2(20),
                                                      RECEIVED_CARTON_IND   VARCHAR2(1),
                                                      ERROR_MESSAGE         VARCHAR2(255),
                                                      RETURN_CODE           VARCHAR2(5));
/

------------------------------------------------------------------------------------------
prompt creating WRP_CARTON_RECV_TBL
CREATE OR REPLACE TYPE WRP_CARTON_RECV_TBL AS TABLE OF WRP_CARTON_RECV_REC;
/
      
------------------------------------------------------------------------------------------