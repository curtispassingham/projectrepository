DROP TYPE OBJ_BOOK_TSF_CO_RTN_TBL FORCE
/

DROP TYPE OBJ_BOOK_TSF_CO_RTN_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_BOOK_TSF_CO_RTN_REC AS OBJECT
(
   I_from_loc                       NUMBER(10),
   I_from_loc_type                  VARCHAR2(1),
   I_to_loc                         NUMBER(10),
   I_to_loc_type                    VARCHAR2(1),
   I_item                           VARCHAR2(25),
   I_return_qty                     NUMBER(12,4),
   I_return_disposition             VARCHAR2(10),
   I_return_without_inventory_ind   VARCHAR2(1),
   I_tran_date                      DATE,
   tsf_no                           NUMBER(12),
   dept                             NUMBER(4),
   class                            NUMBER(4),
   subclass                         NUMBER(4),
   pack_ind                         VARCHAR2(1),
   supp_pack_size                   NUMBER(12,4),
   inv_status                       NUMBER(2),
   virtual_wh                       NUMBER(10)
)
/

CREATE OR REPLACE TYPE OBJ_BOOK_TSF_CO_RTN_TBL AS TABLE OF OBJ_BOOK_TSF_CO_RTN_REC
/
