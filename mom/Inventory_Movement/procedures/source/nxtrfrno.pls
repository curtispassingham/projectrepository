
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------------
-- Name:    NEXT_TRANSFER_NUMBER
-- Purpose: This program generates the next transfer number.
---------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_TRANSFER_NUMBER 
                            (tsf_no        IN OUT NUMBER,
                             return_code   IN OUT VARCHAR2,
                             error_message IN OUT VARCHAR2) AUTHID CURRENT_USER IS

   L_transfer_number_sequence    TSFHEAD.TSF_NO%TYPE;
   wrap_sequence_number          TSFHEAD.TSF_NO%TYPE;
   first_time                    VARCHAR2(3) := 'Yes';
   dummy                         VARCHAR2(1);

   CURSOR c_transfer_exists(transfer_number_param NUMBER) IS
      SELECT 'x'
         FROM tsfhead
         WHERE tsfhead.tsf_no = transfer_number_param;

BEGIN
    LOOP
        SELECT transfer_number_sequence.NEXTVAL
          INTO L_transfer_number_sequence
          FROM sys.dual;

        IF (first_time = 'Yes') THEN
            wrap_sequence_number := L_transfer_number_sequence;
            first_time := 'No';
        ELSIF (L_transfer_number_sequence = wrap_sequence_number) THEN
            error_message := 'Fatal error - no available transfer numbers';
            return_code := 'FALSE';
            EXIT;
        END IF;

        tsf_no := L_transfer_number_sequence;

         OPEN  c_transfer_exists(tsf_no);
         FETCH c_transfer_exists into dummy;
         IF (c_transfer_exists%notfound) THEN
             return_code := 'TRUE';
             CLOSE c_transfer_exists;
             EXIT;
         END IF;
         CLOSE c_transfer_exists;
    END LOOP;
EXCEPTION
    WHEN OTHERS THEN
        error_message := SQLERRM || ' from NEXT_TRANSFER_NO proc.';
        return_code := 'FALSE';
END NEXT_TRANSFER_NUMBER;
/
