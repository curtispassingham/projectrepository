
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------
-- Name:    TRANSFER_ZONE_VAL
-- Purpose: Validate the transfer_zone by seeing if it exists on file.
--   If it does then return 'TRUE' and the transfer_zone name.
--   If it doesn't, return 'FALSE' and the error of not finding it on file.
--   If there was any other error then return 'NULL' and the error message. 
---------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE TRANSFER_ZONE_VAL 
                            (in_transfer_zone       IN     NUMBER,
                             out_transfer_zone_name IN OUT VARCHAR2,
                             return_code            IN OUT VARCHAR2,
                             error_message          IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   cursor C_TRANSFER_ZONE_EXISTS is
      SELECT description
        FROM  v_tsfzone_tl
       WHERE  transfer_zone = in_transfer_zone;
BEGIN
   open  c_transfer_zone_exists;
   fetch c_transfer_zone_exists into out_transfer_zone_name;
   if c_transfer_zone_exists%NOTFOUND then
      return_code := 'FALSE';
      error_message := 'Invalid Transfer Zone Number entered.';
   else
         return_code := 'TRUE';
   end if;
   close c_transfer_zone_exists;
EXCEPTION
    when OTHERS then
        return_code := 'NULL';
        error_message := SQLERRM || ' from TRANSFER_ZONE_VAL proc.';
END TRANSFER_ZONE_VAL;
/


