
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------
-- Name:    CHECK_VEND_MIN
-- Purpose: This procedure determines the minimum order requirements for a supplier
--          and then checks that value against the current order
--          to determine if the minimum order amount is satisfied.
--
--          If the minimum level for the supplier is 'O'rder, this procedure will
--          compare the order total to the supplier minimum(s).  The order total is
--          calculated and compared in the UOM of the supplier minimum (the UOM can
--          be a currency, volume, mass, pallet, case, each or stat case).
--
--          If the minimum level for the supplier is 'L'ocation, this procedure will
--          retrieve each location on the order and compare the order/location total to
--          the supplier minimum(s).  In a multi-channel environment, it will be the
--          physical warehouse location that is compared to the supplier minimum(s),
--          not each individual virtual warehouse on the order.
--
--          If only a single location on the order needs to be compared to the
--          supplier minimum(s), this location should be passed into the I_store
--          parameter if it is a store (pass -1 in the I_wh parameter).  If it is
--          is a warehouse that needs to be compared, it should be passed into the
--          I_wh parameter (pass -1 in the I_store parameter).  In a multi-channel
--          environment, the warehouse passed into the procedure should be a
--          physical warehouse.
---------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CHECK_VEND_MIN(I_order_no      IN  ORDHEAD.ORDER_NO%TYPE,
                                           I_store         IN  STORE.STORE%TYPE,
                                           I_wh            IN  WH.WH%TYPE,
                                           O_min_qty       OUT NUMBER,
                                           O_ord_qty       OUT NUMBER,
                                           O_min_flag      OUT VARCHAR2,
                                           return_code     OUT VARCHAR2,
                                           O_error_message OUT VARCHAR2) AUTHID CURRENT_USER IS
   ABORTING            EXCEPTION;
   QUICKEXIT           EXCEPTION;
   L_error_message     VARCHAR2(255);
   O_code_flag         VARCHAR2(5);
   L_min_level         ORD_INV_MGMT.MIN_CNSTR_LVL%TYPE;
   L_min_type1         ORD_INV_MGMT.MIN_CNSTR_TYPE1%TYPE;
   L_min_uom1          ORD_INV_MGMT.MIN_CNSTR_UOM1%TYPE;
   L_min_curr1         ORD_INV_MGMT.MIN_CNSTR_CURR1%TYPE;
   L_min_conj          ORD_INV_MGMT.MIN_CNSTR_CONJ%TYPE;
   L_min_type2         ORD_INV_MGMT.MIN_CNSTR_TYPE2%TYPE;
   L_min_uom2          ORD_INV_MGMT.MIN_CNSTR_UOM2%TYPE;
   L_min_curr2         ORD_INV_MGMT.MIN_CNSTR_CURR2%TYPE;
   L_uom               UOM_CLASS.UOM%TYPE;
   L_uom_class         UOM_CLASS.UOM_CLASS%TYPE;
   L_supplier          SUPS.SUPPLIER%TYPE;
   L_order_status      ORDHEAD.STATUS%TYPE;
   L_min_value1        ORD_INV_MGMT.MIN_CNSTR_VAL1%TYPE := 0;
   L_min_value2        ORD_INV_MGMT.MIN_CNSTR_VAL2%TYPE := 0;
   L_act_value         ORD_INV_MGMT.MIN_CNSTR_VAL1%TYPE := 0;
   L_location          ORDLOC.LOCATION%TYPE             := 0;

   cursor C_SUPPLIER is
      select ord_inv_mgmt.min_cnstr_lvl,
             ord_inv_mgmt.min_cnstr_type1,
             ord_inv_mgmt.min_cnstr_uom1,
             ord_inv_mgmt.min_cnstr_curr1,
             ord_inv_mgmt.min_cnstr_val1,
             ord_inv_mgmt.min_cnstr_conj,
             ord_inv_mgmt.min_cnstr_type2,
             ord_inv_mgmt.min_cnstr_uom2,
             ord_inv_mgmt.min_cnstr_curr2,
             ord_inv_mgmt.min_cnstr_val2,
             ordhead.supplier,
             ordhead.status
        from ord_inv_mgmt,
             ordhead
       where ordhead.order_no = I_order_no
         and ord_inv_mgmt.order_no = I_order_no;


   /******************************/
   PROCEDURE MAKE_ERROR_MESSAGE (L_location  IN  ORDLOC.LOCATION%TYPE,
                                 L_type      IN  VARCHAR2,
                                 L_uom       IN  UOM_CLASS.UOM%TYPE,
                                 L_curr      IN  VARCHAR2,
                                 L_min       IN  NUMBER) IS

      L_type_decode CODE_DETAIL.CODE_DESC%TYPE;
      L_min_uom     VARCHAR2(4) := '';

   BEGIN
      if L_type = 'A' THEN
         L_min_uom := L_curr;
      else
         L_min_uom := L_uom;
      end if;
      if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                    'SCT',
                                    L_type,
                                    L_type_decode) = FALSE then
         raise ABORTING;
      end if;
      ---
      if L_location > 0 then
         L_error_message := sql_lib.create_msg('VEND_MIN_STORE',TO_CHAR(L_location),L_type_decode,TO_CHAR(L_min) || ' ' ||L_min_uom);
      else
         L_error_message := sql_lib.create_msg('VEND_MIN_ORDER',TO_CHAR(I_order_no),L_type_decode,TO_CHAR(L_min) || ' ' ||L_min_uom);
      end if;
   END MAKE_ERROR_MESSAGE;

   /************************/
   FUNCTION CALCULATE_UOM (L_min_value  IN ORD_INV_MGMT.MIN_CNSTR_VAL1%TYPE,
                           L_type       IN ORD_INV_MGMT.MIN_CNSTR_TYPE1%TYPE,
                           L_uom        IN UOM_CLASS.UOM%TYPE)
   RETURN BOOLEAN IS

      L_actual_value     NUMBER := NULL;
      L_pre_scale_value  NUMBER := NULL;
      L_location_uom     ORDLOC.LOCATION%TYPE;
      L_uom_calc         UOM_CLASS.UOM%TYPE;

      L_error_message_2  VARCHAR2(255);

      cursor C_ALL_LOCATION is
         select distinct location
           from ordloc
          where order_no = I_order_no
		    and loc_type = 'S'
        union
        select distinct wh.physical_wh
          from ordloc ol,
               wh wh
         where ol.order_no = I_order_no
           and ol.location = wh.wh
           and ol.loc_type = 'W';

   BEGIN
      if (L_type ='E') then
         L_uom_calc := 'EA';
      else
         L_uom_calc := L_uom;
      end if;

      if L_min_level = 'O' then
         --- retrieve total order cost
         if L_type in ('V', 'M', 'E') then
            if ORDER_ATTRIB_SQL.GET_TOTAL_UOM(I_order_no,
                                              NULL,                    -- item
                                              NULL,                    -- location
                                              L_uom_calc,
                                              L_supplier,
                                              'O',                      -- we want only the post-scaled order value
                                              L_actual_value,           -- qty
                                              L_pre_scale_value,        -- ignored
                                              L_error_message) = FALSE then
               return FALSE;
            end if;
         elsif L_type = 'C' then
            if ORDER_ATTRIB_SQL.GET_TOTAL_CASES(L_error_message,
                                                L_actual_value,         -- total cases
                                                L_pre_scale_value,
                                                I_order_no,
                                                NULL,                   -- item
                                                NULL) = FALSE then      --location
               return FALSE;
            end if;
         elsif L_type = 'P' then
            if ORDER_ATTRIB_SQL.GET_TOTAL_PALLETS(L_error_message,
                                                  L_actual_value,       -- total pallets
                                                  L_pre_scale_value,
                                                  I_order_no,
                                                  L_supplier,
                                                  NULL,                 -- item
                                                  NULL) = FALSE then    --location
               return FALSE;
            end if;
         elsif L_type = 'S' then
            if ORDER_ATTRIB_SQL.GET_TOTAL_STATCASE(L_error_message,
                                                   L_actual_value,      -- total statcase
                                                   L_pre_scale_value,
                                                   I_order_no,
                                                   L_supplier,
                                                   NULL,                -- item
                                                   NULL) = FALSE then   -- location
               return FALSE;
            end if;
         else
            L_error_message_2 := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                    'L_type',
                                                     L_type,
                                                    'V, M, P, C, E , S');
            L_error_message := 'CHECK_VEND_MIN.CALCULATE_UOM: ' || L_error_message_2;
            return FALSE;
         end if;

         if L_actual_value < L_min_value then
            O_min_qty := L_min_value;
            O_ord_qty := L_actual_value;
            MAKE_ERROR_MESSAGE(0,L_type,L_uom,NULL,L_min_value);
            return FALSE;
         end if;
      elsif (L_location > 0) then
         --- L_min_level = 'L'
         if L_type in ('V', 'M', 'E') then
            if ORDER_ATTRIB_SQL.GET_TOTAL_UOM(I_order_no,
                                              NULL,                          -- item
                                              L_location,                    -- location
                                              L_uom_calc,
                                              L_supplier,
                                              'O',                           -- we want only the post-scaled order value
                                              L_actual_value,                -- qty
                                              L_pre_scale_value,             -- ignored
                                              L_error_message) = FALSE then
               return FALSE;
            end if;
         elsif L_type = 'C' then
            if ORDER_ATTRIB_SQL.GET_TOTAL_CASES(L_error_message,
                                                L_actual_value,              -- total cases
                                                L_pre_scale_value,
                                                I_order_no,
                                                NULL,                        -- item
                                                L_location) = FALSE then     --location
               return FALSE;
            end if;
         elsif L_type = 'P' then
            if ORDER_ATTRIB_SQL.GET_TOTAL_PALLETS(L_error_message,
                                                  L_actual_value,            -- total pallets
                                                  L_pre_scale_value,
                                                  I_order_no,
                                                  L_supplier,
                                                  NULL,                      -- item
                                                  L_location) = FALSE then   --location
               return FALSE;
            end if;
         elsif L_type = 'S' then
            if ORDER_ATTRIB_SQL.GET_TOTAL_STATCASE(L_error_message,
                                                   L_actual_value,           -- total statcase
                                                   L_pre_scale_value,
                                                   I_order_no,
                                                   L_supplier,
                                                   NULL,                     -- item
                                                   L_location) = FALSE then  -- location
               return FALSE;
            end if;
         else
            L_error_message_2 := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                    'L_type',
                                                     L_type,
                                                    'V, M, P, C, E , S');
            L_error_message := 'CHECK_VEND_MIN.CALCULATE_UOM: ' || L_error_message_2;
            return FALSE;
         end if;

         if L_actual_value < L_min_value then
            O_min_qty := L_min_value;
            O_ord_qty := L_actual_value;
            MAKE_ERROR_MESSAGE(L_location,L_type,L_uom,NULL,L_min_value);
            return FALSE;
         end if;
      else
         --- L_min_level = 'L' and location is -1
         FOR C_rec in C_ALL_LOCATION LOOP
            L_location := C_rec.location;
            ---
            if L_type in ('V', 'M', 'E') then
               if ORDER_ATTRIB_SQL.GET_TOTAL_UOM(I_order_no,
                                                 NULL,                         -- item
                                                 L_location,                   -- location
                                                 L_uom_calc,
                                                 L_supplier,
                                                 'O',                          -- we want only the post-scaled order value
                                                 L_actual_value,               -- qty
                                                 L_pre_scale_value,            -- ignored
                                                 L_error_message) = FALSE then --location
                  return FALSE;
               end if;
            elsif L_type = 'C' then
               if ORDER_ATTRIB_SQL.GET_TOTAL_CASES(L_error_message,
                                                   L_actual_value,             -- total cases
                                                   L_pre_scale_value,
                                                   I_order_no,
                                                   NULL,                       -- item
                                                   L_location) = FALSE then    --location
                  return FALSE;
               end if;
            elsif L_type = 'P' then
               if ORDER_ATTRIB_SQL.GET_TOTAL_PALLETS(L_error_message,
                                                     L_actual_value,           -- total pallets
                                                     L_pre_scale_value,
                                                     I_order_no,
                                                     L_supplier,
                                                     NULL,                     -- item
                                                     L_location) = FALSE then  -- location
                  return FALSE;
               end if;
            elsif L_type = 'S' then
               if ORDER_ATTRIB_SQL.GET_TOTAL_STATCASE(L_error_message,
                                                      L_actual_value,          -- total statcase
                                                      L_pre_scale_value,
                                                      I_order_no,
                                                      L_supplier,
                                                      NULL,                    -- item
                                                      L_location) = FALSE then -- location
               return FALSE;
            end if;
            else
               L_error_message_2 := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                       'L_type',
                                                        L_type,
                                                       'V, M, P, C, E , S');
               L_error_message := 'CHECK_VEND_MIN.CALCULATE_UOM: ' || L_error_message_2;
               return FALSE;
            end if;
            ---
            if L_actual_value < L_min_value then
               O_min_qty := L_min_value;
               O_ord_qty := L_actual_value;
               MAKE_ERROR_MESSAGE(L_location, L_type, L_uom, NULL, L_min_value);
               return FALSE; -- no need to process the rest
            end if;
         end loop;

      end if;
      return TRUE;
   EXCEPTION
      when OTHERS then
         O_code_flag := 'TRUE';
         L_error_message := SQLERRM || 'from CHECK_VEND_MIN proc.';
         return FALSE;
   END CALCULATE_UOM;

   FUNCTION GET_ORDER_COSTS(O_error_message      IN OUT VARCHAR2,
                            O_supp_order_cost    IN OUT ORDLOC.UNIT_COST%TYPE,
                            I_order_no           IN     ORDHEAD.ORDER_NO%TYPE,
                            I_location           IN     ORDLOC.LOCATION%TYPE,
                            I_supplier           IN     SUPS.SUPPLIER%TYPE,
                            I_order_status       IN     ORDHEAD.STATUS%TYPE)
   RETURN BOOLEAN IS


      L_program             VARCHAR2(64) := 'CHECK_VEND_MIN.GET_ORDER_COSTS';
      L_ord_currency_code   CURRENCIES.CURRENCY_CODE%TYPE;
      L_ord_exchange_rate   CURRENCY_RATES.EXCHANGE_RATE%TYPE;
      L_order_cost          ORDLOC.UNIT_COST%TYPE;
      L_supp_order_cost     ORDLOC.UNIT_COST%TYPE;
      L_supp_currency_code  SUPS.CURRENCY_CODE%TYPE;


      cursor C_ORDHEAD is
         select currency_code,
                exchange_rate
           from ordhead
          where order_no = I_order_no;

      cursor C_ORD_UNAPP is
         select SUM(qty_ordered   * decode(cost_source,'MANL',o.unit_cost,0)),
                SUM(qty_ordered   * decode(cost_source,'MANL',0,i.unit_cost))
           from ordloc o,
                ordsku s,
                item_supp_country_loc i
          where o.order_no          = I_order_no
            and o.order_no          = s.order_no
            and o.item              = i.item
            and o.item              = s.item
            and i.supplier          = I_supplier
            and i.loc               = o.location
            and i.origin_country_id = s.origin_country_id;



      cursor C_ORD_LOC_UNAPP is
         select SUM(qty_ordered   * decode(cost_source,'MANL',o.unit_cost,0)),
                SUM(qty_ordered   * decode(cost_source,'MANL',0,i.unit_cost))
           from ordloc o,
                ordsku s,
                item_supp_country_loc i
          where o.order_no          = I_order_no
            and o.location          = I_location
            and o.order_no          = s.order_no
            and o.item              = s.item
            and o.item              = i.item
            and i.supplier          = I_supplier
            and o.location          = i.loc
            and i.origin_country_id = s.origin_country_id
         UNION
         select SUM(qty_ordered   * decode(cost_source,'MANL',o.unit_cost,0)),
                SUM(qty_ordered   * decode(cost_source,'MANL',0,i.unit_cost))
           from ordloc o,
                ordsku s,
                item_supp_country_loc i,
                wh
          where o.order_no          = I_order_no
            and o.order_no          = s.order_no
            and o.item              = s.item
            and o.item              = i.item
            and i.supplier          = I_supplier
            and o.location          = i.loc
            and (wh.physical_wh     = I_location
                 and wh.stockholding_ind = 'Y')
            and i.loc               = wh.wh
            and i.origin_country_id = s.origin_country_id;

   BEGIN
      O_supp_order_cost         := 0;
      SQL_LIB.SET_MARK('OPEN','C_ORDHEAD','ordhead','Order No: '||to_char(I_order_no));
      open C_ORDHEAD;
      SQL_LIB.SET_MARK('FETCH','C_ORDHEAD','ordhead','Order No: '||to_char(I_order_no));
      fetch C_ORDHEAD into L_ord_currency_code,
                           L_ord_exchange_rate;
      if C_ORDHEAD%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_ORDHEAD','ordhead','Order No: '||to_char(I_order_no));
         close C_ORDHEAD;
         raise NO_DATA_FOUND;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_ORDHEAD','ordhead','Order No: '||to_char(I_order_no));
      close C_ORDHEAD;

      if I_location is null then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no));
         open C_ORD_UNAPP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no));
         fetch C_ORD_UNAPP into L_order_cost,
                                L_supp_order_cost;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no));
         close C_ORD_UNAPP;

      --- order/item
      elsif I_location is not null then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_LOC_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Location: '||to_char(I_location));
         open C_ORD_LOC_UNAPP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_LOC_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Location: '||to_char(I_location));
         fetch C_ORD_LOC_UNAPP into L_order_cost,L_supp_order_cost;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_LOC_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Location: '||to_char(I_location));
         close C_ORD_LOC_UNAPP;

      end if;

      if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                           L_supp_currency_code,
                                           L_supplier) = FALSE then
         return FALSE;
      end if;

      if L_supp_order_cost != 0 and L_ord_currency_code != L_supp_currency_code then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_supp_order_cost,
                                 L_supp_currency_code,
                                 L_ord_currency_code,
                                 L_supp_order_cost,
                                 'C',
                                 NULL,
                                 'P',
                                 NULL,
                                 L_ord_exchange_rate) = FALSE then
            return FALSE;
         end if;
      end if;

      O_supp_order_cost := nvl(L_supp_order_cost,0) + nvl(L_order_cost,0);

     return TRUE;
   EXCEPTION
      when NO_DATA_FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_CURSOR',
                                               'C_ORDHEAD',
                                               L_program,
                                               NULL);
         RETURN FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         RETURN FALSE;
   END;

   /************************/
   FUNCTION CALCULATE_DOLLAR (L_min_value  IN ORD_INV_MGMT.MIN_CNSTR_VAL1%TYPE,
                              L_curr       IN ORD_INV_MGMT.MIN_CNSTR_CURR1%TYPE)
   RETURN BOOLEAN IS

     L_error_message      VARCHAR2(255);
     L_supp_prescale_cost ORDLOC.UNIT_COST%TYPE;
     L_prescale_cost      ORDLOC.UNIT_COST%TYPE;
     L_order_cost         ORDLOC.UNIT_COST%TYPE;
     L_qty_prescaled      ORDLOC.QTY_PRESCALED%TYPE;
     L_qty_ordered        ORDLOC.QTY_ORDERED%TYPE;
     L_supp_item_cost     ORDLOC.UNIT_COST%TYPE;
     L_ord_item_cost      ORDLOC.UNIT_COST%TYPE;
     ---
     cursor C_ALL_LOCATION is
        select distinct location
           from ordloc
          where order_no = I_order_no
		    and loc_type = 'S'
        union
        select distinct wh.physical_wh
          from ordloc ol,
               wh wh
         where ol.order_no = I_order_no
           and ol.location = wh.wh
           and ol.loc_type = 'W';

   BEGIN
      ---
      if L_min_level = 'O' then
         --- retrieve total order cost for unapproved orders
         if L_order_status != 'A' and L_order_status != 'C' then
            if GET_ORDER_COSTS(L_error_message,
                               L_act_value,  -- supplier (gross) order_cost
                               I_order_no,
                               NULL, -- location
                               L_supplier,
                               L_order_status) = FALSE then
              return FALSE;
            end if;
         else
         --- call supplier cost units for approved orders to get supplier cost
         if ORDER_CALC_SQL.SUPPLIER_COST_UNITS(L_error_message,
                                               L_supp_prescale_cost,
                                               L_act_value,  -- supplier (gross) order_cost
                                               L_prescale_cost,
                                               L_order_cost,
                                               L_qty_prescaled,
                                               L_qty_ordered,
                                               L_supp_item_cost,
                                               L_ord_item_cost,
                                               I_order_no,
                                               NULL, -- item
                                               NULL, -- location
                                               NULL, -- alloc no
                                               L_supplier,
                                               L_order_status,
                                               NULL,
                                               NULL) = FALSE then
            return FALSE;
         end if;
         end if;
         ---
         if L_act_value < L_min_value then
            O_min_qty := L_min_value;
            O_ord_qty := L_act_value;
            MAKE_ERROR_MESSAGE(0,'A',NULL,L_curr,L_min_value);
            return FALSE;
         end if;
      elsif L_location > 0  then
         --- L_min_level = 'L'
         --- retrieve total order cost for unapproved orders
         if L_order_status != 'A' and L_order_status != 'C' then
            if GET_ORDER_COSTS(L_error_message,
                               L_act_value,  -- supplier (gross) order_cost
                               I_order_no,
                               L_location, -- location
                               L_supplier,
                               L_order_status) = FALSE then
              return FALSE;
            end if;
         else
         if ORDER_CALC_SQL.SUPPLIER_COST_UNITS(L_error_message,
                                               L_supp_prescale_cost,
                                               L_act_value,  -- supplier (gross) order_cost
                                               L_prescale_cost,
                                               L_order_cost,
                                               L_qty_prescaled,
                                               L_qty_ordered,
                                               L_supp_item_cost,
                                               L_ord_item_cost,
                                               I_order_no,
                                               NULL, -- item
                                               L_location,
                                               NULL, -- alloc no
                                               L_supplier,
                                               L_order_status,
                                               NULL,
                                               NULL) = FALSE then
            return FALSE;
         end if;
         end if;
         ---
         if L_act_value < L_min_value then
            O_min_qty := L_min_value;
            O_ord_qty := L_act_value;
            MAKE_ERROR_MESSAGE(L_location, 'A', NULL, L_curr, L_min_value);
            return FALSE;
         end if;
      else
         --- L_min_level = 'L' and location is -1
         FOR C_rec IN C_ALL_LOCATION LOOP
            L_location := C_rec.location;
            if L_order_status != 'A' and L_order_status != 'C' then
               if GET_ORDER_COSTS(L_error_message,
                                  L_act_value,  -- supplier (gross) order_cost
                                  I_order_no,
                                  L_location, -- location
                                  L_supplier,
                                  L_order_status) = FALSE then
                  return FALSE;
               end if;
            else
            if ORDER_CALC_SQL.SUPPLIER_COST_UNITS(L_error_message,
                                                  L_supp_prescale_cost,
                                                  L_act_value,  -- supplier (gross) order_cost
                                                  L_prescale_cost,
                                                  L_order_cost,
                                                  L_qty_prescaled,
                                                  L_qty_ordered,
                                                  L_supp_item_cost,
                                                  L_ord_item_cost,
                                                  I_order_no,
                                                  NULL, -- item
                                                  L_location,
                                                  NULL, -- alloc no
                                                  L_supplier,
                                                  L_order_status,
                                                  NULL,
                                                  NULL) = FALSE then
               return FALSE;
            end if;
            end if;
            ---
            if L_act_value < L_min_value then
               O_min_qty := L_min_value;
               O_ord_qty := L_act_value;
               MAKE_ERROR_MESSAGE(L_location, 'A', NULL, L_curr, L_min_value);
               return FALSE; -- no need to process the rest
            end if;
         end loop;

      end if;
      return TRUE;
   EXCEPTION
      when OTHERS then
         O_code_flag := 'TRUE';
         L_error_message := SQLERRM || 'from CHECK_VEND_MIN proc.';
         return FALSE;
   END CALCULATE_DOLLAR;


/*******MAIN PROCEDURE*******/
BEGIN
   ---
   if I_store > 0 then
      L_location := I_store;
   else
      L_location := I_wh;
   end if;
   ---
   O_code_flag     := 'FALSE';
   O_min_flag      := 'FALSE';
   return_code     := 'TRUE';
   O_error_message :=  NULL;

   open C_SUPPLIER;
   fetch C_SUPPLIER into L_min_level,
                         L_min_type1,
                         L_min_uom1,
                         L_min_curr1,
                         L_min_value1,
                         L_min_conj,
                         L_min_type2,
                         L_min_uom2,
                         L_min_curr2,
                         L_min_value2,
                         L_supplier,
                         L_order_status;
   close C_SUPPLIER;

   if L_min_level is NOT NULL then
      if L_min_conj is NULL THEN
         if L_min_type1 = 'A' then
            -- min1 is amount
            if CALCULATE_DOLLAR(L_min_value1,
                                L_min_curr1) then
               O_min_flag := 'FALSE';
            elsif O_code_flag = 'TRUE' then
               raise ABORTING;
            else
               raise QUICKEXIT;
            end if;
         else
            -- min1 is either volume, mass, pallet, case , statcase or each
            if CALCULATE_UOM(L_min_value1,
                             L_min_type1,
                             L_min_uom1) then
               O_min_flag := 'FALSE';
            elsif O_code_flag = 'TRUE' then
               raise ABORTING;
            else
               raise QUICKEXIT;
            end if;
         end if;
      elsif L_min_conj = 'O' then  /* conjuction is OR */
         if L_min_type1 = 'A' then
            -- min1 is amount and
            -- min2 is either volume, mass, pallet, case ,statcase or each
            if CALCULATE_DOLLAR(L_min_value1,
                                L_min_curr1) or
               CALCULATE_UOM(L_min_value2,
                             L_min_type2,
                             L_min_uom2) then
               O_min_flag := 'FALSE';
            elsif O_code_flag = 'TRUE' then
               raise ABORTING;
            else
               raise QUICKEXIT;
            end if;
         else
            if L_min_type2 = 'A' then
               -- min1 is either volume, mass, pallet, case , statcase or each, and
               -- min2 is amount
               if CALCULATE_UOM(L_min_value1,
                                L_min_type1,
                                L_min_uom1) or
                  CALCULATE_DOLLAR(L_min_value2,
                                   L_min_curr2) then
                  O_min_flag := 'FALSE';
               elsif O_code_flag = 'TRUE' then
                  raise ABORTING;
               else
                  raise QUICKEXIT;
               end if;
            else
               -- min1 and min2 are either volume, mass, pallet, case , statcase or each
               if CALCULATE_UOM(L_min_value1,
                                L_min_type1,
                                L_min_uom1) or
                  CALCULATE_UOM(L_min_value2,
                                L_min_type2,
                                L_min_uom2) then
                  O_min_flag := 'FALSE';
               elsif O_code_flag = 'TRUE' then
                  raise ABORTING;
               else
                  raise QUICKEXIT;
               end if;
            end if;
         end if;
      else  /* conjunction is AND */
         if L_min_type1 = 'A' then
            -- min1 is amount and
            -- min2 is either volume, mass, pallet, case ,statcase or each
               if CALCULATE_DOLLAR(L_min_value1,
                                   L_min_curr1) and
               CALCULATE_UOM(L_min_value2,
                             L_min_type2,
                             L_min_uom2) then
               O_min_flag := 'FALSE';
            elsif O_code_flag = 'TRUE' then
               raise ABORTING;
            else
               raise QUICKEXIT;
            end if;
         else
            if L_min_type2 = 'A' then
               -- min1 is either volume, mass, pallet, case , statcase or each, and
               -- min2 is amount
               if CALCULATE_UOM(L_min_value1,
                                L_min_type1,
                                L_min_uom1) and
                  CALCULATE_DOLLAR(L_min_value2,
                                   L_min_curr2) then
                  O_min_flag := 'FALSE';
               elsif O_code_flag = 'TRUE' then
                  raise ABORTING;
               else
                  raise QUICKEXIT;
               end if;
            else
               -- min1 and min2 are either volume, mass, pallet, case , statcase or each
               if CALCULATE_UOM(L_min_value1,
                                L_min_type1,
                                L_min_uom1) and
                  CALCULATE_UOM(L_min_value2,
                                L_min_type2,
                                L_min_uom2) then
                  O_min_flag := 'FALSE';
               elsif O_code_flag = 'TRUE' then
                  raise ABORTING;
               else
                  raise QUICKEXIT;
               end if;
            end if;
         end if;
      end if;
   end if;
EXCEPTION
   when QUICKEXIT then
      O_min_flag      := 'TRUE';
      return_code     := 'TRUE';
      O_error_message := L_error_message;
   when ABORTING then
      O_min_flag      := 'FALSE';
      return_code     := 'FALSE';
      O_error_message := L_error_message;
   when OTHERS then
      O_error_message := 'CHECK_VEND_MIN:' || SQLERRM;
      return_code     := 'FALSE';

END CHECK_VEND_MIN;
/
