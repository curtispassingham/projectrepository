SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PROCEDURE CHK_DEPENDENCY_EXISTS (I_order_no           IN      ORDHEAD.ORDER_NO%TYPE,
                                                   I_rpl_ord_hist_days  IN      SYSTEM_OPTIONS.REPL_ORDER_HISTORY_DAYS%TYPE,
                                                   I_vdate              IN      PERIOD.VDATE%TYPE,
                                                   I_hist_months        IN      PURGE_CONFIG_OPTIONS.ORDER_HISTORY_MONTHS%TYPE,
                                                   I_invc_match_ind     IN      NUMBER,
                                                   O_exists             IN OUT  VARCHAR2,
                                                   O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE) AUTHID CURRENT_USER AS

   L_obligation         OBLIGATION.OBLIGATION_KEY%TYPE;
   L_custom_id          CE_ORD_ITEM.CE_ID%TYPE;
   L_exists             VARCHAR2(1) :='N';
   L_program            VARCHAR2(50) := 'CHK_DEPENDENCY_EXISTS';

   cursor C_OBLIGATION_KEY IS
         SELECT distinct obligation_key
           FROM alc_head
          WHERE order_no = TO_NUMBER(I_order_no)
            AND obligation_key IS NOT NULL;
   
   cursor C_CUSTOM_ID IS
         SELECT distinct ce_id
           FROM alc_head
          WHERE order_no = TO_NUMBER(I_order_no)
            AND ce_id IS NOT NULL;
   
   cursor C_ORD_OBL_DPND_NO_INVC IS
         SELECT 'Y'
           FROM alc_head
          WHERE order_no <> TO_NUMBER(I_order_no)
            AND obligation_key = L_obligation
            AND order_no NOT IN (SELECT oh.order_no FROM ordhead oh,ordlc lc
            WHERE lc.order_no(+) = oh.order_no 
              AND ((0 < (NVL(MONTHS_BETWEEN(TO_DATE(I_vdate,'YYYYMMDD'),oh.close_date),0) - I_hist_months)) OR
                                (oh.status = 'W' AND oh.orig_ind = 0 AND oh.contract_no is NULL AND (TO_DATE(I_vdate,'YYYYMMDD') - oh.written_date) >= I_rpl_ord_hist_days)));
                                
   cursor C_ORD_OBL_DPND_INVC IS
         SELECT 'Y'
           FROM alc_head 
          WHERE order_no <> TO_NUMBER(I_order_no)
            AND obligation_key = L_obligation
            AND order_no NOT IN (
          SELECT oh.order_no 
            FROM ordhead oh,
                 shipment sh,
                 shipsku ss,
                 invc_head ih,
                 ordlc lc
           WHERE oh.order_no = sh.order_no
             AND lc.order_no(+) = oh.order_no
             AND sh.shipment = ss.shipment
             AND ss.match_invc_id = ih.invc_id(+)
             AND (0 < (NVL(MONTHS_BETWEEN(TO_DATE(I_vdate,'YYYYMMDD'),
                        oh.close_date),0) - I_hist_months))
             AND ((0 < (NVL(MONTHS_BETWEEN(TO_DATE(I_vdate,'YYYYMMDD'),
                           sh.invc_match_date),0) - I_hist_months))
                  OR (sh.invc_match_date IS NULL))         
             AND (ss.match_invc_id IS NULL OR ih.status = 'P')
             AND sh.invc_match_status = 'C'
             AND 'C' = (SELECT decode(MAX(ship.invc_match_status),  MIN(ship.invc_match_status),'C','X')
                          FROM shipment ship
                         WHERE ship.order_no = oh.order_no
                      GROUP BY ship.order_no)
           UNION 
           SELECT oh.order_no               
             FROM ordhead oh,
                  ordlc lc
            WHERE lc.order_no(+) = oh.order_no
              AND oh.status = 'W'
              AND oh.orig_ind = 0
              AND oh.contract_no IS NULL
              AND (TO_DATE(I_vdate,'YYYYMMDD') - oh.written_date) >= I_rpl_ord_hist_days
           UNION 
           SELECT oh.order_no
             FROM ordhead oh,
                  ordlc lc
            WHERE lc.order_no(+) = oh.order_no
              AND (0 < (NVL(MONTHS_BETWEEN(TO_DATE(I_vdate,'YYYYMMDD'),
                        oh.close_date),0) - I_hist_months))
              AND oh.status = 'C'
              AND NOT EXISTS (SELECT 'x'
                                FROM shipment sh
                               WHERE sh.order_no = oh.order_no)); 
                               
   cursor C_ORD_CUS_DPND_NO_INVC IS
         SELECT 'Y'
           FROM alc_head
          WHERE order_no <> TO_NUMBER(I_order_no)
            AND ce_id = L_custom_id
            AND order_no NOT IN 
                (SELECT oh.order_no 
                   FROM ordhead oh,ordlc lc
                  WHERE lc.order_no(+) = oh.order_no 
                        AND ((0 < (NVL(MONTHS_BETWEEN(TO_DATE(I_vdate,'YYYYMMDD'),oh.close_date),0) - I_hist_months)) 
                              OR (oh.status = 'W' AND oh.orig_ind = 0 AND oh.contract_no IS NULL AND (TO_DATE(I_vdate,'YYYYMMDD') - oh.written_date) >= I_rpl_ord_hist_days)));
                                
   cursor C_ORD_CUS_DPND_INVC IS
         SELECT 'Y'
           FROM alc_head 
          WHERE order_no <> TO_NUMBER(I_order_no)
            AND ce_id = L_custom_id
            AND order_no NOT IN 
                (SELECT oh.order_no 
                   FROM ordhead oh,
                        shipment sh,
                        shipsku ss,
                        invc_head ih,
                        ordlc lc
                  WHERE oh.order_no = sh.order_no
                    AND lc.order_no(+) = oh.order_no
                    AND sh.shipment = ss.shipment
                    AND ss.match_invc_id = ih.invc_id(+)
                    AND (0 < (NVL(MONTHS_BETWEEN(TO_DATE(I_vdate,'YYYYMMDD'),
                               oh.close_date),0) - I_hist_months))
                    AND ((0 < (NVL(MONTHS_BETWEEN(TO_DATE(I_vdate,'YYYYMMDD'),
                                  sh.invc_match_date),0) - I_hist_months))
                         OR (sh.invc_match_date IS NULL))         
                    AND (ss.match_invc_id IS NULL OR ih.status = 'P')
                    AND sh.invc_match_status = 'C'
                    AND 'C' = (SELECT decode(MAX(ship.invc_match_status),  MIN(ship.invc_match_status),'C','X')
                               FROM shipment ship
                              WHERE ship.order_no = oh.order_no
                           GROUP BY ship.order_no)
           UNION 
           SELECT oh.order_no               
             FROM ordhead oh,
                  ordlc lc
            WHERE lc.order_no(+) = oh.order_no
              AND oh.status = 'W'
              AND oh.orig_ind = 0
              AND oh.contract_no IS NULL
              AND (TO_DATE(I_vdate,'YYYYMMDD') - oh.written_date) >= I_rpl_ord_hist_days
           UNION 
           SELECT oh.order_no
             FROM ordhead oh,
                  ordlc lc
            WHERE lc.order_no(+) = oh.order_no
              AND (0 < (NVL(MONTHS_BETWEEN(TO_DATE(I_vdate,'YYYYMMDD'),
                        oh.close_date),0) - I_hist_months))
              AND oh.status = 'C'
              AND NOT EXISTS (SELECT 'x'
                                FROM shipment sh
                               WHERE sh.order_no = oh.order_no)); 
                               
                                
BEGIN

   OPEN  C_OBLIGATION_KEY;
   
   LOOP
   FETCH C_OBLIGATION_KEY into L_obligation;
    
      if L_obligation IS NULL then 
         OPEN C_CUSTOM_ID;
      
         LOOP 
         FETCH C_CUSTOM_ID into L_custom_id;
         EXIT WHEN C_CUSTOM_ID%NOTFOUND;
        
            if I_invc_match_ind = '0' then
               OPEN C_ORD_CUS_DPND_NO_INVC;
          
               FETCH C_ORD_CUS_DPND_NO_INVC into L_exists;
          
               CLOSE C_ORD_CUS_DPND_NO_INVC;
        
            else
               
               OPEN C_ORD_CUS_DPND_INVC;
          
               FETCH C_ORD_CUS_DPND_INVC into L_exists;
          
               CLOSE C_ORD_CUS_DPND_INVC;
            
            end if;
        
            EXIT WHEN L_exists = 'Y';
      
         END LOOP;
        
         CLOSE C_CUSTOM_ID;
       
      else 
        
         if I_invc_match_ind = '0' then
            OPEN C_ORD_OBL_DPND_NO_INVC;
            
            FETCH C_ORD_OBL_DPND_NO_INVC into L_exists;
            
            CLOSE C_ORD_OBL_DPND_NO_INVC;
        
         else
         
            OPEN C_ORD_OBL_DPND_INVC;
            
            FETCH C_ORD_OBL_DPND_INVC into L_exists;
          
            CLOSE C_ORD_OBL_DPND_INVC;
            
         end if;
        
         EXIT WHEN L_exists = 'Y';
    
    end if;
    
    EXIT WHEN C_OBLIGATION_KEY%NOTFOUND;
    
    END LOOP;
    
    CLOSE C_OBLIGATION_KEY;
      
    if L_exists IS NULL then
       O_exists := 'N';
    else 
       O_exists := L_exists;
    end if;
   
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
END CHK_DEPENDENCY_EXISTS;
/
