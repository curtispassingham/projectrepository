
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------
-- Name:    RTV_ORDER_NO
-- Purpose: If successful this procedure will return a verified
--          RTV (Return To Vendor) number (including check digit)
--          and the status of 'TRUE'.  
--          But if unsuccessful will return a null for the number
--          and the status of 'FALSE'.
----------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_RTV_ORDER_NO ( O_rtv_order_no IN OUT rtv_head.rtv_order_no%TYPE,
                                                O_ret_code     IN OUT VARCHAR2,
                                                O_err_msg      IN OUT VARCHAR2
                                              ) AUTHID CURRENT_USER IS

   L_rtv_tmp   rtv_head.rtv_order_no%TYPE := NULL;
   L_wrap_sequence_number   ORDHEAD.ORDER_NO%TYPE;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_exists    VARCHAR2(1); 
 

   cursor C_SEQ is
      select rtv_order_no_seq.nextval seq
         from sys.dual;

   cursor C_EXISTS ( S_RTV_ORDER_NO rtv_head.rtv_order_no%TYPE ) is
      select 'Y'
         from rtv_head
        where rtv_head.rtv_order_no = S_RTV_ORDER_NO;

BEGIN

   LOOP                                   -- this WILL loop forever

      open C_SEQ;
      fetch C_SEQ into L_rtv_tmp;
      close C_SEQ;

	 IF (L_first_time = 'Yes') THEN 
         L_wrap_sequence_number := L_rtv_tmp; 
         L_first_time := 'No'; 
     ELSIF (L_rtv_tmp = L_wrap_sequence_number) THEN 
         O_err_msg := 'Fatal error - no RTV order numbers available.'; 
         O_ret_code := 'FALSE'; 
         EXIT; 
     END IF; 

     O_rtv_order_no := L_rtv_tmp;
	  
	 open C_EXISTS (O_rtv_order_no); 
     fetch C_EXISTS into L_exists;
	  IF (C_EXISTS%notfound) THEN
	       O_ret_code := 'TRUE';
	 close C_EXISTS;
	  EXIT;

      end if;
      close C_EXISTS;      
   end LOOP;


EXCEPTION
   when OTHERS then
      O_err_msg      := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           'RTV_ORDER_NO',
                                           to_char(SQLCODE));
      O_ret_code     := 'FALSE';
      O_rtv_order_no := NULL;

END NEXT_RTV_ORDER_NO;
/
