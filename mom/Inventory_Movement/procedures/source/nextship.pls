
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------------------------------
-- Name:    NEXT_SHIPMENT
-- Purpose: This shipment number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) a shipment number.
--          Upon failure (FALSE) an appropriate error message for display purposes by
--          the calling program/procedure.
--------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_SHIPMENT (shipment_number IN OUT NUMBER,
                                           return_code     IN OUT VARCHAR2,
                                           error_message   IN OUT VARCHAR2) AUTHID CURRENT_USER IS
    L_dummy  VARCHAR2(1) := NULL;

    CURSOR c_get_shipment IS
        SELECT 'x'
          FROM shipment
         WHERE shipment.shipment = shipment_number;
BEGIN

    LOOP
        SELECT shipment_sequence.NEXTVAL
          INTO shipment_number
          FROM sys.dual;

        OPEN  c_get_shipment;
        FETCH c_get_shipment INTO L_dummy;
        IF c_get_shipment%NOTFOUND THEN
           EXIT;
        END IF;
        CLOSE c_get_shipment;

    END LOOP;

    return_code := 'TRUE';

EXCEPTION
    WHEN OTHERS THEN
        error_message := SQLERRM || ' from NEXT_SHIPMENT proc.';
        return_code := 'FALSE';
END NEXT_SHIPMENT;
/


