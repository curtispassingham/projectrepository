
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------------------------------
-- Name:    NEXT_BILL_OF_LADING
-- Purpose: This BOL number sequence generator will return to the calling
--          program.
--------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_BILL_OF_LADING (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                                 O_bol_no        IN OUT BOL_SHIPMENT.BOL_NO%TYPE,
                                                 O_return_code   IN OUT VARCHAR2) AUTHID CURRENT_USER IS

   L_bol_sequence           BOL_SHIPMENT.BOL_NO%TYPE;
   L_wrap_sequence_number   BOL_SHIPMENT.BOL_NO%TYPE;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1);

   cursor C_BOL_EXISTS(bol_number_param BOL_SHIPMENT.BOL_NO%TYPE) IS
      select 'x'
        from bol_shipment b
       where b.bol_no = bol_number_param
          or exists (select 'y'
                       from shipment s
                      where s.bol_no = bol_number_param
                        and rownum = 1)
         and rownum = 1;
        
   cursor C_BOL_NO_SEQUENCE is        
      select bill_of_lading_sequence.NEXTVAL
        from sys.dual;

BEGIN
   LOOP
      open C_BOL_NO_SEQUENCE;
      fetch C_BOL_NO_SEQUENCE into L_bol_sequence;
      close C_BOL_NO_SEQUENCE;

      if (L_first_time = 'Yes') then
         L_wrap_sequence_number := L_bol_sequence;
         L_first_time := 'No';
      elsif (L_bol_sequence = L_wrap_sequence_number) then
         O_error_message := 'Fatal error - no available BOL numbers';
         O_return_code := 'FALSE';
         EXIT;
      end if;

      O_bol_no := L_bol_sequence;

      open C_BOL_EXISTS(O_bol_no);
      fetch C_BOL_EXISTS into L_dummy;
      if C_BOL_EXISTS%NOTFOUND then
         O_return_code := 'TRUE';
         close C_BOL_EXISTS;
         EXIT;
      end if;
      close C_BOL_EXISTS;
   END LOOP;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM|| ' from NEXT_BILL_OF_LADING procedure';
      O_return_code := 'FALSE';
END NEXT_BILL_OF_LADING;
/
