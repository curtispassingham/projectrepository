PROMPT Creating Trigger 'RMS_TABLE_SHP_BDR'
CREATE OR REPLACE TRIGGER RMS_TABLE_SHP_BDR
 BEFORE DELETE
 ON SHIPMENT
 FOR EACH ROW
DECLARE


   cursor C_GET_ORDER( C_order_no NUMBER) is
      select supplier, 
             currency_code
        from ordhead
       where order_no = C_order_no;

   L_supplier       ordhead.supplier%TYPE      := null;
   L_currency_code  ordhead.currency_code%TYPE := null;
BEGIN

   if(:old.order_no is not null) then
   
      open C_GET_ORDER(:old.order_no);
      fetch C_GET_ORDER into L_supplier,
                             L_currency_code;
      close C_GET_ORDER;

      INSERT INTO STAGE_PURGED_SHIPMENTS(SHIPMENT,
                                         ORDER_NO,
                                         STATUS_CODE,
                                         INVC_MATCH_STATUS,
                                         ORDER_SUPPLIER,
                                         ORDER_CURRENCY_CODE,
                                         TO_LOC,
                                         LOC_TYPE,
                                         RECEIVE_DATE)
                                   values( :old.shipment,
                                           :old.order_no,
                                           :old.status_code,
                                           :old.invc_match_status,
                                           L_supplier,
                                           L_currency_code,
                                           :old.to_loc,
                                           :old.to_loc_type,
                                           :old.receive_date);
      
  
   end if;

END;
/
