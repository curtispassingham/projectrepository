PROMPT Creating Trigger 'RMS_TABLE_RDT_BIR'
CREATE OR REPLACE TRIGGER RMS_TABLE_RDT_BIR
 BEFORE INSERT
 ON RTV_DETAIL
 FOR EACH ROW
DECLARE

   L_valid_record   VARCHAR2(1) := 'N';
   cursor C_CHECK_RTV_DETAIL is
      select 'Y'
          from shipsku
       where shipsku.shipment = :new.shipment
           and shipsku.item = :new.item
           and (:new.inv_status is NULL or
                    shipsku.inv_status = :new.inv_status);
BEGIN
   if :new.shipment is not NULL then
      open C_CHECK_RTV_DETAIL;
      fetch C_CHECK_RTV_DETAIL into L_valid_record;
      close C_CHECK_RTV_DETAIL;
       ---
      if L_valid_record = 'N' then
         raise_application_error(-20502, 'Invalid record being inserted
   into RTV_DETAIL');
      end if;
   end if;
END;
/
