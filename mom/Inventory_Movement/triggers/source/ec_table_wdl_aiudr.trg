PROMPT Creating Trigger 'EC_TABLE_WDL_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_WDL_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON WO_DETAIL
 FOR EACH ROW
DECLARE


   L_queue_rec        WOIN_MFQUEUE%ROWTYPE := NULL;
   L_error_message    VARCHAR2(255) := NULL;
   L_publish_ind      WO_DETAIL.PUBLISH_IND%TYPE := 'Y';
   L_status           VARCHAR2(1) := NULL;

   cursor C_GET_ORDER_NO is
      select order_no
        from wo_head
       where wo_id = L_queue_rec.wo_id;
       
   PROGRAM_ERROR   EXCEPTION;
   QUICKEXIT       EXCEPTION;
BEGIN
   if DELETING then
      L_queue_rec.wo_id := :old.wo_id;
      L_queue_rec.wh := :old.wh;
      L_queue_rec.item := :old.item;
      L_queue_rec.loc_type := :old.loc_type;
      L_queue_rec.location := :old.location;
      L_queue_rec.wo_seq_no := :old.seq_no;
      L_queue_rec.wip_code := :old.wip_code;
      L_publish_ind          := :old.publish_ind;
      ---
      L_queue_rec.message_type := RMSMFM_WOIN.WO_DEL;
   else
      if INSERTING then
         L_queue_rec.message_type := RMSMFM_WOIN.WO_ADD;
      else

         -- updating, only continue if wip_code            --
         -- has changed, since this is the only field sent --
         -- to the bus for work orders that is modifiable  --
         -- after work order has been created              --

         if (:old.wip_code = :new.wip_code) then
            raise QUICKEXIT;
         end if;

         L_queue_rec.message_type := RMSMFM_WOIN.WO_UPD;
      end if;

      /* For inserts and updates, more of the values from the table
         will be published.
      */

      L_queue_rec.wo_id := :new.wo_id;
      L_queue_rec.wh := :new.wh;
      L_queue_rec.item := :new.item;
      L_queue_rec.location := :new.location;
      L_queue_rec.loc_type := :new.loc_type;
      L_queue_rec.wo_seq_no := :new.seq_no;
      L_queue_rec.wip_code := :new.wip_code;

   end if;

   open C_GET_ORDER_NO;
   fetch C_GET_ORDER_NO into L_queue_rec.order_no;
   close C_GET_ORDER_NO;

   /* Adds a record to the message queue table.
   */
   if RMSMFM_WOIN.ADDTOQ(L_error_message,
                         L_queue_rec,
                         L_publish_ind) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when QUICKEXIT then
      null;
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_WDL_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/
