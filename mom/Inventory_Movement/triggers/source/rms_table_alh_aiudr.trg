PROMPT Creating Trigger 'RMS_TABLE_ALH_AIUDR'
CREATE OR REPLACE TRIGGER RMS_TABLE_ALH_AIUDR
 AFTER DELETE OR INSERT
 ON ALLOC_HEADER
 FOR EACH ROW
DECLARE

   L_order_no     ALLOC_HEADER.ORDER_NO%TYPE := NULL;
   L_exists       VARCHAR2(1)                := NULL;
   L_exists_order VARCHAR2(1)                := NULL;
   L_alloc_no     ALLOC_DETAIL.ALLOC_NO%TYPE := NULL;
   ---
   cursor C_ORDHEAD_EXISTS is
      select 'X'
        from ordhead
       where order_no = L_order_no
         and pre_mark_ind = 'Y';

   cursor C_REV_ORDERS_EXISTS is
      select 'X'
        from rev_orders
       where order_no = L_order_no;
   ---
BEGIN
   /* records will be inserted into rev_orders table, only if the order_no exists in ordhead */
   /* since the alloc_header.order_no has been modified to NUMBER(10) to accomodate transfer */
   /* numbers and allocation numbers.                                                        */
   IF DELETING THEN
      L_alloc_no := :OLD.alloc_no;
      L_order_no := :OLD.order_no;
   ELSE
      L_alloc_no := :NEW.alloc_no;
      L_order_no := :NEW.order_no;
   END IF;
   ---
   IF L_order_no IS NOT NULL THEN 
      OPEN C_ORDHEAD_EXISTS;
      FETCH C_ORDHEAD_EXISTS INTO L_exists_order;
      CLOSE C_ORDHEAD_EXISTS;
      ---
      IF L_exists_order IS NOT NULL THEN
         OPEN C_REV_ORDERS_EXISTS;
         FETCH C_REV_ORDERS_EXISTS INTO L_exists;
         CLOSE C_REV_ORDERS_EXISTS;
      
         IF L_exists IS NULL THEN /* revision exists */
            INSERT INTO rev_orders (order_no)
            VALUES(L_order_no);
         END IF; /* if rev_orders not found */
      END IF; /* L_exists_order is NOT NULL */
      ---
   END IF; /* L_order_no */
END RMS_TABLE_ALH_AIUDR;
/
