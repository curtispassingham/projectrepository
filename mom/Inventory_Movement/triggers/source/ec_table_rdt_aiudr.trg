PROMPT Creating Trigger 'EC_TABLE_RDT_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_RDT_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF REASON
, QTY_REQUESTED
 ON RTV_DETAIL
 FOR EACH ROW
DECLARE
                                                                         
                                                                                
                                                                                
   L_rtv_order_no    RTV_DETAIL.RTV_ORDER_NO%TYPE       :=  NULL;               
   L_rtv_seq_no      RTV_DETAIL.SEQ_NO%TYPE             :=  NULL;               
   L_rtv_item        RTV_DETAIL.ITEM%TYPE               :=  NULL;               
   L_publish_ind     RTV_DETAIL.PUBLISH_IND%TYPE        :=  NULL;               
   L_error_message   VARCHAR2(255)                      :=  NULL;               

   L_message_type    RTVREQ_MFQUEUE.MESSAGE_TYPE%TYPE   :=  NULL;               
                                                                                
   PROGRAM_ERROR     EXCEPTION;
BEGIN                                                                           
   if DELETING then                                                             
      
      if :old.updated_by_rms_ind = 'Y' then
         L_message_type := RMSMFM_RTVREQ.DTL_DEL;
         L_rtv_order_no := :old.rtv_order_no;                                      
         L_rtv_seq_no   := :old.seq_no;                                            
         L_rtv_item     := :old.item;   -- Needed by SIM for delete                
         L_publish_ind  := :old.publish_ind;
      end if;
   else                                                                         
      L_rtv_order_no := :new.rtv_order_no;                                      
      L_rtv_item     := :new.item;
      L_rtv_seq_no   := :new.seq_no;                                            
      L_publish_ind  := :new.publish_ind;          
                             
      if :new.updated_by_rms_ind = 'Y' then
         if INSERTING then                                                         
            L_message_type := RMSMFM_RTVREQ.DTL_ADD;                               
         elsif UPDATING then                                                       
            if ((:old.qty_requested != :new.qty_requested) or                      
                (:old.reason != :new.reason)) then                                 
               L_message_type := RMSMFM_RTVREQ.DTL_UPD;                            
            end if;                                                                
         end if;                                                                   
      end if;   
   end if;                                                                      
                                                                                

   if L_message_type is not NULL then  
      if RMSMFM_RTVREQ.ADDTOQ(L_error_message,                                  
                              L_message_type,                                   
                              L_rtv_order_no,                                   
                              NULL,                 -- Status                   
                              NULL,                 -- External ref no
                              L_rtv_seq_no,                                     
                              L_rtv_item,                                       
                              L_publish_ind) = FALSE then                       
         raise PROGRAM_ERROR;                                                   
      end if;                                                                   
   end if;                                                                      
                                                                                
EXCEPTION                                                                       

   when OTHERS then  
      if L_error_message is NULL then                                           
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                 
                                               SQLERRM,                         
                                               'EC_TABLE_RDT_AIUDR',            
                                               NULL);                           
      end if;                                                                   
                                                                                
      RAISE_APPLICATION_ERROR(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);  
                                                                                
END EC_TABLE_RDT_AIUDR;
/
