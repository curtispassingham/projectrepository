PROMPT Creating Trigger 'RMS_TABLE_SHS_BDR'
CREATE OR REPLACE TRIGGER RMS_TABLE_SHS_BDR
 BEFORE DELETE
 ON SHIPSKU
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
   INSERT INTO STAGE_PURGED_SHIPSKUS(SHIPMENT,
                                     SEQ_NO,
                                     ITEM,
                                     QTY_RECEIVED,
                                     UNIT_COST,
                                     WEIGHT_RECEIVED,
                                     WEIGHT_RECEIVED_UOM)
                              values(:old.shipment,
                                     :old.seq_no,
                                     :old.item,
                                     :old.qty_received,
                                     :old.unit_cost,
                                     :old.weight_received,
                                     :old.weight_received_uom);

END;
/
