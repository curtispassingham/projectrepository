CREATE OR REPLACE TRIGGER EC_TABLE_TDT_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF TSF_QTY
 ON TSFDETAIL
 FOR EACH ROW
DECLARE

   L_tsf_no         TSFHEAD.TSF_NO%TYPE           := NULL;
   L_item           ITEM_MASTER.ITEM%TYPE         := NULL;
   L_publish_ind    TSFDETAIL.PUBLISH_IND%TYPE    := NULL;
   L_message_type   TSF_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;
   L_tsf_type       TSFHEAD.TSF_TYPE%TYPE         := NULL;
   L_text           VARCHAR2(255) := NULL;
   PROGRAM_ERROR    EXCEPTION;

   cursor C_TSF_TYPE is
      select tsf_type
        from tsfhead
       where tsf_no = :new.tsf_no;

BEGIN

   open C_TSF_TYPE;
   fetch C_TSF_TYPE into L_tsf_type;
   close C_TSF_TYPE;

   if L_tsf_type != 'SG' then

      if DELETING and (:old.publish_ind is not null) then
         L_message_type  := RMSMFM_TRANSFERS.DTL_DEL;
         L_tsf_no        := :old.tsf_no;
         L_item          := :old.item;
         L_publish_ind   := :old.publish_ind;

      else

         L_tsf_no        := :new.tsf_no;
         L_item          := :new.item;
         L_publish_ind   := :new.publish_ind;

         if :new.updated_by_rms_ind = 'Y' then
            if INSERTING then
               if :new.ship_qty > 0 then
                  L_message_type := NULL;
               else
                  L_message_type := RMSMFM_TRANSFERS.DTL_ADD;
               end if;
            else
               if (nvl(:old.tsf_qty, 0) != nvl(:new.tsf_qty,0)) and
                  (:old.publish_ind is not null) then
                  L_message_type := RMSMFM_TRANSFERS.DTL_UPD;
               end if;
            end if;
         end if;
      end if;

      if L_message_type is NOT NULL then
         if RMSMFM_TRANSFERS.ADDTOQ(L_text,
                                    L_message_type,
                                    L_tsf_no,
                                    NULL,
                                    NULL,
                                    L_item,
                                    L_publish_ind) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;

   end if;

EXCEPTION
   when OTHERS then
      if L_text is NULL then
         L_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                      SQLERRM,
                                      'EC_TABLE_TDT_AIUDR',
                                      NULL);
      end if;

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/
