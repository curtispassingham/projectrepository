PROMPT Creating Trigger 'EC_TABLE_RHD_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_RHD_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON RTV_HEAD
 FOR EACH ROW
DECLARE


   L_error_message      VARCHAR2(255)                      :=  NULL;
   
   L_rtv_order_no       RTV_HEAD.RTV_ORDER_NO%TYPE         :=  NULL;
   L_message_type       RTVREQ_MFQUEUE.MESSAGE_TYPE%TYPE   :=  NULL;
   L_status             RTV_HEAD.STATUS_IND%TYPE           :=  NULL;
   L_old_status         RTV_HEAD.STATUS_IND%TYPE           :=  NULL;
   L_new_status         RTV_HEAD.STATUS_IND%TYPE           :=  NULL;
   L_old_ret_auth_num   RTV_HEAD.RET_AUTH_NUM%TYPE         :=  NULL;
   L_new_ret_auth_num   RTV_HEAD.RET_AUTH_NUM%TYPE         :=  NULL;
   L_old_comment_desc   RTV_HEAD.COMMENT_DESC%TYPE         :=  NULL;
   L_new_comment_desc   RTV_HEAD.COMMENT_DESC%TYPE         :=  NULL;
   L_old_restock_pct    RTV_HEAD.RESTOCK_PCT%TYPE          :=  NULL;
   L_new_restock_pct    RTV_HEAD.RESTOCK_PCT%TYPE          :=  NULL;
   L_old_not_after_date RTV_HEAD.NOT_AFTER_DATE%TYPE       :=  NULL;
   L_new_not_after_date RTV_HEAD.NOT_AFTER_DATE%TYPE       :=  NULL;
   L_new_ext_ref_no     RTV_HEAD.EXT_REF_NO%TYPE           :=  NULL;

   PROGRAM_ERROR        EXCEPTION;
BEGIN
   L_old_status       := :old.status_ind;
   L_new_status       := :new.status_ind;
   L_old_ret_auth_num := nvl(:old.ret_auth_num, -1);
   L_new_ret_auth_num := nvl(:new.ret_auth_num, -1);
   L_old_comment_desc := nvl(:old.comment_desc, ' ');
   L_new_comment_desc := nvl(:new.comment_desc, ' ');
   L_old_restock_pct  := nvl(:old.restock_pct, 0);
   L_new_restock_pct  := nvl(:new.restock_pct, 0);
   L_old_not_after_date := :old.not_after_date;
   L_new_not_after_date := :new.not_after_date;
   L_new_ext_ref_no   := :new.ext_ref_no;
   
   if DELETING then
      L_message_type := RMSMFM_RTVREQ.HDR_DEL;
      
      L_rtv_order_no := :old.rtv_order_no;
      L_status       := :old.status_ind;
   else
      L_rtv_order_no := :new.rtv_order_no;
      L_status       := :new.status_ind;
      
      if INSERTING then
         L_message_type := RMSMFM_RTVREQ.HDR_ADD;
      elsif UPDATING then
         if ((L_new_status != L_old_status) or
             (L_new_ret_auth_num != L_old_ret_auth_num) or
	       (L_new_comment_desc != L_old_comment_desc) or
	       (L_new_restock_pct != L_old_restock_pct) or
             (L_new_not_after_date != L_old_not_after_date)) then
            L_message_type := RMSMFM_RTVREQ.HDR_UPD;
         end if;
      end if;
   end if;
   
   if L_message_type is not NULL then
      if RMSMFM_RTVREQ.ADDTOQ(L_error_message,
                              L_message_type,
                              L_rtv_order_no,
                              L_status,
                              L_new_ext_ref_no,
                              NULL,               -- Seq_no (from Rtv_detail)
                              NULL,               -- Item (from Rtv_detail)
                              NULL) = FALSE then  -- Publish_ind (from Rtv_detail)
         raise PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'EC_TABLE_RHD_AIUR',
                                               NULL);
      end if;

      RAISE_APPLICATION_ERROR(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
   
END EC_TABLE_RHD_AIUDR;
/
