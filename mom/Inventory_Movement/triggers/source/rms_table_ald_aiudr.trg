PROMPT Creating Trigger 'RMS_TABLE_ALD_AIUDR'
CREATE OR REPLACE TRIGGER RMS_TABLE_ALD_AIUDR
AFTER DELETE OR INSERT OR UPDATE OF QTY_ALLOCATED
ON ALLOC_DETAIL
FOR EACH ROW
DECLARE

   L_exists       VARCHAR2(1)                := NULL;
   L_alloc_no     ALLOC_DETAIL.ALLOC_NO%TYPE;
   ---
   cursor C_PRE_MARK_IND is
      select 'x'
        from ordhead oh,
             alloc_header ah
       where oh.order_no = ah.order_no
         and oh.pre_mark_ind = 'Y'
         and ah.alloc_no = L_alloc_no;
   ---
BEGIN
   if inserting then
      L_alloc_no := :new.alloc_no;
   else
      L_alloc_no := :old.alloc_no;
   end if;

   open C_PRE_MARK_IND;
   fetch C_PRE_MARK_IND into L_exists;
   ---
   if L_exists is not NULL and NVL(:new.qty_allocated,-9999) <> NVL(:old.qty_allocated,-9999) then   /* Check if order is pre-marked */
      insert into rev_orders 
                ( order_no )
           select ah.order_no
             from alloc_header ah
            where ah.alloc_no = L_alloc_no
              and ah.order_no is NOT NULL
              and ah.order_no not in (select ro.order_no
                                        from rev_orders ro);
   end if;
   close C_PRE_MARK_IND;
END RMS_TABLE_ALD_AIUDR;
/
