PROMPT Creating Trigger 'EC_TABLE_ORC_AUR'
CREATE OR REPLACE TRIGGER EC_TABLE_ORC_AUR
 AFTER UPDATE OF CUSTOMER_ORDER_NO                      
,FULFILL_ORDER_NO
,PARTIAL_DELIVERY_IND                   
,DELIVERY_TYPE                          
,CARRIER_CODE                           
,CARRIER_SERVICE_CODE                   
,CONSUMER_DELIVERY_DATE                 
,CONSUMER_DELIVERY_TIME                 
,BILL_FIRST_NAME                        
,BILL_PHONETIC_FIRST
,BILL_LAST_NAME                         
,BILL_PHONETIC_LAST                     
,BILL_PREFERRED_NAME
,BILL_COMPANY_NAME                      
,BILL_ADD1                              
,BILL_ADD2                              
,BILL_ADD3                              
,BILL_COUNTY                            
,BILL_CITY                              
,BILL_STATE                             
,BILL_COUNTRY_ID                        
,BILL_POST                              
,BILL_PHONE                             
,DELIVER_FIRST_NAME                     
,DELIVER_PHONETIC_FIRST                 
,DELIVER_LAST_NAME                      
,DELIVER_PHONETIC_LAST                  
,DELIVER_PREFERRED_NAME                 
,DELIVER_COMPANY_NAME                   
,DELIVER_ADD1                           
,DELIVER_ADD2                           
,DELIVER_ADD3                           
,DELIVER_CITY                           
,DELIVER_STATE                          
,DELIVER_COUNTRY_ID                     
,DELIVER_POST                           
,DELIVER_COUNTY                         
,DELIVER_PHONE
 ON ORDCUST
 FOR EACH ROW

DECLARE

   L_error_msg           VARCHAR2(255) := NULL;
   L_message_type        TSF_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;
   L_tsf_no              TSFHEAD.TSF_NO%TYPE := NULL;
   L_tsf_row             TSFHEAD%ROWTYPE := NULL;

   PROGRAM_ERROR         EXCEPTION;

   cursor C_TSF is 
      select th.*
        from tsfhead th,
             transfers_pub_info p
       where th.tsf_no = L_tsf_no
         and th.tsf_no = p.tsf_no
         and th.tsf_type = 'CO'
         and p.published = 'Y';

BEGIN

   if UPDATING then
      L_tsf_no := :NEW.tsf_no;

      -- Only need to publish ORDCUST change through transfer publishing ("RIB_TsfDesc_REC")
      -- if ORDCUST is linked to a transfer, and the transfer has already been published.
      if L_tsf_no is NOT NULL then
         open C_TSF;
         fetch C_TSF into L_tsf_row;
         close C_TSF;

         if L_tsf_row.tsf_no is NOT NULL then
            L_message_type := RMSMFM_TRANSFERS.HDR_UPD;

            if RMSMFM_TRANSFERS.ADDTOQ(L_error_msg,
                                       L_message_type,
                                       L_tsf_no,
                                       L_tsf_row.tsf_type,
                                       L_tsf_row.status,
                                       NULL,  --I_item
                                       NULL,  --I_publish_ind
                                       L_tsf_row.to_loc_type,
                                       L_tsf_row.tsf_parent_no,
                                       L_tsf_row.from_loc_type) = FALSE then
               raise PROGRAM_ERROR;
            end if;
         end if;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      if L_error_msg is NULL then
         L_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           'EC_TABLE_ORC_AUR',
                                           NULL);
      end if;
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
