PROMPT Creating Trigger 'EC_TABLE_SPT_AIR'
CREATE OR REPLACE TRIGGER EC_TABLE_SPT_AIR
 AFTER INSERT
 ON SHIPMENT_PUB_TEMP
 FOR EACH ROW
DECLARE

   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_to_loc               SHIPMENT.TO_LOC%TYPE;
   L_to_loc_type          SHIPMENT.TO_LOC_TYPE%TYPE;
   L_shipment             SHIPMENT.SHIPMENT%TYPE;
   L_message_type         SHIPMENT_PUB_INFO.MESSAGE_TYPE%TYPE   := 'asnoutcre';
   L_status               VARCHAR2(1)                           := NULL;

   cursor C_GET_SHIPMENT_VALUES is
      select to_loc_type,
             to_loc
        from shipment
       where shipment = L_shipment;
BEGIN
   L_shipment   := :new.shipment;

   open C_GET_SHIPMENT_VALUES;
   fetch C_GET_SHIPMENT_VALUES into L_to_loc_type,
                                    L_to_loc;
   close C_GET_SHIPMENT_VALUES;

   if RMSMFM_SHIPMENT.ADDTOQ(L_error_message,
                             L_message_type,
                             L_shipment,
                             L_to_loc,
                             L_to_loc_type) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         API_LIBRARY.HANDLE_ERRORS(L_status,
                                   L_error_message,
                                   API_LIBRARY.FATAL_ERROR,
                                   'EC_TABLE_SPT_AIR');
      end if;

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END;
/
