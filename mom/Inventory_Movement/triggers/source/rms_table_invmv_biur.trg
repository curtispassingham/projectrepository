--------------------------------------------------------
-- Copyright (c) 2012, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          RMS_TABLE_INVMV_BIUR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'RMS_TABLE_INVMV_BIUR'
CREATE OR REPLACE TRIGGER RMS_TABLE_INVMV_BIUR
   BEFORE INSERT OR UPDATE
   ON INV_MOVE_UNIT_OPTIONS
   FOR EACH ROW
DECLARE
   L_user      VARCHAR2(30) := GET_USER;
   L_sysdate   DATE         := SYSDATE;
BEGIN
   if UPDATING then
      :NEW.last_update_id       := L_user;
      :NEW.last_update_datetime := L_sysdate;
   elsif INSERTING then
      :NEW.create_id            := L_user;
      :NEW.create_datetime      := L_sysdate;
      :NEW.last_update_id       := L_user;
      :NEW.last_update_datetime := L_sysdate;
   end if;
END;
/
