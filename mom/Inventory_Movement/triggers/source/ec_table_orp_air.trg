PROMPT Creating Trigger 'EC_TABLE_ORP_AIR'
CREATE OR REPLACE TRIGGER EC_TABLE_ORP_AIR
 AFTER INSERT
 ON ORDCUST_PUB_TEMP
 FOR EACH ROW
DECLARE

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_status          VARCHAR2(1)                        := NULL;
   L_ordcust_no      ORDCUST_PUB_INFO.ORDCUST_NO%TYPE   := NULL;
   L_message_type    ORDCUST_PUB_INFO.MESSAGE_TYPE%TYPE := RMSMFM_ORDCUST.LP_cre_type;

   PROGRAM_ERROR     EXCEPTION;

BEGIN
   L_ordcust_no := :new.ordcust_no;

   if RMSMFM_ORDCUST.ADDTOQ(L_error_message,
                            L_message_type,
                            L_ordcust_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         API_LIBRARY.HANDLE_ERRORS(L_status,
                                   L_error_message,
                                   API_LIBRARY.FATAL_ERROR,
                                   'EC_TABLE_ORP_AIR');
      end if;

      RAISE_APPLICATION_ERROR(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/
