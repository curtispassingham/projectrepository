PROMPT Creating Trigger 'EC_TABLE_THD_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_THD_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF EXP_DC_DATE
, STATUS
, COMMENT_DESC
, NOT_AFTER_DATE
, DELIVERY_DATE
 ON TSFHEAD
 FOR EACH ROW
DECLARE
   L_status              VARCHAR2(1) := NULL;
   L_error_msg           VARCHAR2(255) := NULL;
   L_message_type        tsf_mfqueue.message_type%TYPE := NULL;
   L_woout_message_type  woout_mfqueue.message_type%TYPE :=NULL;
   L_tsf_no              tsfhead.tsf_no%TYPE := null;
   L_tsf_type            tsfhead.tsf_type%TYPE := NULL;
   L_new_status          tsfhead.status%TYPE := NULL;
   L_old_status          tsfhead.status%TYPE := NULL;
   L_old_comment_desc    tsfhead.comment_desc%TYPE := NULL;
   L_new_comment_desc    tsfhead.comment_desc%TYPE := NULL;
   L_dummy_name          wh.wh_name%TYPE;
   L_finisher_flag       BOOLEAN := FALSE;
   L_old_from_loc_type   tsfhead.from_loc_type%TYPE := NULL;
   L_old_to_loc_type     tsfhead.to_loc_type%TYPE := NULL;
   L_new_from_loc_type   tsfhead.to_loc_type%TYPE := NULL;
   L_new_to_loc_type     tsfhead.to_loc_type%TYPE := NULL;
   L_new_from_loc        tsfhead.from_loc%TYPE    := NULL;
   L_old_from_loc        tsfhead.from_loc%TYPE    := NULL;
   L_old_exp_dc_date     tsfhead.exp_dc_date%TYPE := NULL;
   L_new_exp_dc_date     tsfhead.exp_dc_date%TYPE := NULL;
   L_from_pwh            wh.wh%TYPE := NULL;
   L_to_pwh              wh.wh%TYPE := NULL;
   L_same_pwh            BOOLEAN := FALSE;
   L_old_not_after_date  tsfhead.not_after_date%TYPE := NULL;
   L_new_not_after_date  tsfhead.not_after_date%TYPE := NULL;
   L_old_delivery_date   tsfhead.delivery_date%TYPE := NULL;
   L_new_delivery_date   tsfhead.delivery_date%TYPE := NULL;

   L_tsf_wo_id           tsf_wo_head.tsf_wo_id%TYPE := NULL;

   L_tsf_parent          TSFHEAD.TSF_PARENT_NO%TYPE := NULL;
   L_to_loc_type_tsf     TSFHEAD.TO_LOC_TYPE%TYPE   := NULL;
   L_from_loc_type_tsf   TSFHEAD.FROM_LOC_TYPE%TYPE := NULL;
   L_exists               VARCHAR2(1) := 'N';
   L_error_message       VARCHAR2(255);

   PROGRAM_ERROR         EXCEPTION;

   cursor C_GET_WO_ID is
     select tsf_wo_id
       from tsf_wo_head
      where tsf_no = :new.tsf_parent_no;
	
	cursor C_TSF_MFQUEUE_EXIST is 
     select 'Y' 
       from tsf_mfqueue 
     where tsf_no = :old.tsf_no; 

BEGIN

   L_tsf_type   := NVL(:old.tsf_type, :new.tsf_type);

   /* if the tsf_type is not Book Transfer or Non-Salable Book Transfer or System Generated Transfer */
   if L_tsf_type not in ('BT', 'NB', 'SG') then

      L_new_status := :new.status;
      L_old_status := :old.status;
      L_new_comment_desc := NVL(:new.comment_desc, ' ');
      L_old_comment_desc := NVL(:old.comment_desc, ' ');
      L_old_from_loc_type  := :old.from_loc_type;
      L_old_to_loc_type    := :old.to_loc_type;
      L_new_from_loc_type  := :new.from_loc_type;
      L_new_to_loc_type    := :new.to_loc_type;
      L_new_from_loc       := :new.from_loc;
      L_old_from_loc       := :old.from_loc;
      L_old_exp_dc_date  := :old.exp_dc_date;
      L_new_exp_dc_date  := :new.exp_dc_date;
      L_new_not_after_date := :new.not_after_date;
      L_old_not_after_date := :old.not_after_date;
      L_old_delivery_date  := :old.delivery_date;
      L_new_delivery_date  := :new.delivery_date;

      if DELETING then 
         L_tsf_parent  := :old.tsf_parent_no;
         L_to_loc_type_tsf  := :old.to_loc_type;
         L_from_loc_type_tsf := :old.from_loc_type;
      else
         L_tsf_parent  := :new.tsf_parent_no;
         L_to_loc_type_tsf  := :new.to_loc_type;
         L_from_loc_type_tsf := :new.from_loc_type;
      end if;

      if DELETING or (UPDATING and L_new_status = 'D' and L_old_status != 'D') then

         if DELETING and (L_old_status = 'D' or L_tsf_type = 'EG')then

            L_message_type  := NULL;
			open C_TSF_MFQUEUE_EXIST;
			fetch C_TSF_MFQUEUE_EXIST into L_exists;
			close C_TSF_MFQUEUE_EXIST;
			            
			if L_exists = 'N' then 
			   delete from transfers_pub_info
			   where tsf_no = :old.tsf_no; 
		    end if;

         else

            L_message_type  := RMSMFM_TRANSFERS.HDR_DEL;
            L_woout_message_type := RMSMFM_WOOUT.HDR_DEL;
            L_tsf_no        := :old.tsf_no;
            L_tsf_type      := :old.tsf_type;
            L_status        := L_old_status;

         end if;

      else

         L_tsf_no        := :new.tsf_no;
         L_tsf_type      := :new.tsf_type;
         L_status        := L_new_status;

         if INSERTING then

            -- Don't need to track the inserts for 2nd leg if from loc is Ext finisher and to loc is a Wh.
            -- Wh just needs ASN info of 2nd leg.
            if (L_tsf_type != 'EG' and (:new.tsf_parent_no is NULL or (:new.tsf_parent_no is NOT NULL and 
                                                                                              (:new.from_loc_type != 'E' or :new.to_loc_type = 'S')))) or
                -- if transfer is externally generated then need to publish the 
                -- 2nd leg of transfer details to other system from RMS except when from loc is Ext finisher and to loc is a Wh.
                -- Wh just needs ASN info of 2nd leg.
                -- No need of tracking the 1st leg of Externally generated transfer since ext system already knows about it.
                (L_tsf_type = 'EG' and 
                 :new.tsf_parent_no is NOT NULL and (:new.from_loc_type != 'E' or :new.to_loc_type = 'S')) then
               L_message_type := RMSMFM_TRANSFERS.HDR_ADD;
            end if;

         else

            -- For transfers with finishing a Stock Order message will only be published when
            -- the tsf 'from' and 'to' loc are not in the same physical warehouse.
            -- However, upon approval or un-approval (status goes back to 'I')the
            -- Work Order message is always published for the second leg,
            -- reguardless of the 'from' or 'to' locs physical loc.


            -- if the transfer was set back to 'I'nput status from 'A'pproved
            if (L_new_status = 'I' and L_old_status = 'A') then

               if L_old_from_loc_type = 'W' and L_old_to_loc_type = 'W' then
                  if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(L_error_msg,
                                                   L_from_pwh,
                                                   L_old_from_loc) = FALSE then
                     raise PROGRAM_ERROR;
                  end if;
                  if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(L_error_msg,
                                                   L_to_pwh,
                                                   :old.to_loc) = FALSE then
                     raise PROGRAM_ERROR;
                  end if;

                  if L_from_pwh = L_to_pwh then
                     L_same_pwh := TRUE;
                  end if;

               end if;
               if L_old_from_loc_type = 'W' then

                  -- determine if from loc is a finisher.  This will only be true for second leg tranfsers.
                  if WH_ATTRIB_SQL.CHECK_FINISHER(L_error_msg,
                                                  L_finisher_flag,
                                                  L_dummy_name,
                                                  L_old_from_loc) = FALSE then
                     raise PROGRAM_ERROR;
                  end if;
               end if;

               if NOT L_same_pwh then
                  --From S,W,E not in same physical loc as to loc
                  L_message_type := RMSMFM_TRANSFERS.HDR_UNAPRV;
               end if;

               -- if from_loc is a finisher, internal or external, then this is the second
               -- leg of the tsf.  Set finisher flag to generate WO Delete message for SECOND leg only.
               if L_finisher_flag = TRUE or L_old_from_loc_type = 'E' then
                  L_woout_message_type := RMSMFM_WOOUT.HDR_UNAPRV;
               end if;


            else --not un-approve

               if L_new_from_loc_type = 'W' and L_new_to_loc_type = 'W' then
                  if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(L_error_msg,
                                                   L_from_pwh,
                                                   L_new_from_loc) = FALSE then
                     raise PROGRAM_ERROR;
                  end if;
                  if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(L_error_msg,
                                                   L_to_pwh,
                                                   :new.to_loc) = FALSE then
                     raise PROGRAM_ERROR;
                  end if;

                  if L_from_pwh = L_to_pwh then
                     L_same_pwh := TRUE;
                  end if;
               end if;

               if NOT L_same_pwh then
                  --Send Stock Order update messages when from and to loc not in same physical loc

                  -- if the status has changed and the new status is 'A'pproved or 'C'losed OR
                  -- if the comment_desc has changed and the new status is 'A'pproved or 'S'hipped OR
                  -- if the not_after_date has changed and the new status is 'A'pproved or 'S'hipped
                  if (L_new_status in ('A','C') and L_new_status != L_old_status and L_old_status != 'X') OR
                     (L_old_comment_desc != L_new_comment_desc and L_new_status in ('A', 'S')) OR
                     (L_old_not_after_date != L_new_not_after_date and L_new_status in ('A', 'S')) then
                     L_message_type := RMSMFM_TRANSFERS.HDR_UPD;

                  -- if the exp_dc_date or delivery_date has changed and the status is approved
                  elsif ((L_old_exp_dc_date is NULL and L_new_exp_dc_date is not NULL) OR
                         (L_old_exp_dc_date is NOT NULL and L_new_exp_dc_date is NULL) OR
                         (L_old_exp_dc_date != L_new_exp_dc_date))OR
                        ((L_old_delivery_date is NULL and L_new_delivery_date is not NULL) OR
                         (L_old_delivery_date is NOT NULL and L_new_delivery_date is NULL) OR
                         (L_old_delivery_date != L_new_delivery_date)) and L_new_status = 'A' then
                     L_message_type := RMSMFM_TRANSFERS.HDR_UPD;

                  end if;
               end if;

               -- Anytime the tsf is set to approved determine if it's a child tsf (from finisher)
               -- and send the WO message.
               if L_new_status = 'A' and L_old_status != L_new_status then
                  if L_new_from_loc_type = 'W' then
                     if WH_ATTRIB_SQL.CHECK_FINISHER(L_error_msg,
                                                     L_finisher_flag,
                                                     L_dummy_name,
                                                     L_new_from_loc) = FALSE then
                        raise PROGRAM_ERROR;
                     end if;
                  end if;

                  if L_finisher_flag = TRUE or L_new_from_loc_type = 'E' then
                     L_woout_message_type := RMSMFM_WOOUT.HDR_ADD;
                  end if;
               end if;

            end if; -- un-approved / approve or other updates
         end if; --insert / update
      end if; --delete

      if L_message_type is NOT NULL then
         if RMSMFM_TRANSFERS.ADDTOQ(L_error_msg,
                                    L_message_type,
                                    L_tsf_no,
                                    L_tsf_type,
                                    L_status,
                                    NULL,
                                    NULL,
                                    L_to_loc_type_tsf,
                                    L_tsf_parent,
                                    L_from_loc_type_tsf) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;

      if L_woout_message_type is NOT NULL then
         --get tsf_wo_id
         open C_GET_WO_ID;
         fetch C_GET_WO_ID into L_tsf_wo_id;
         if C_GET_WO_ID%FOUND and L_woout_message_type != RMSMFM_WOOUT.HDR_DEL then
             if RMSMFM_WOOUT.ADDTOQ(L_error_msg,
                                    L_woout_message_type,
                                    L_tsf_wo_id ) = FALSE then
               raise PROGRAM_ERROR;
            end if;
         end if;
         close C_GET_WO_ID;
      end if;

   end if;  /* if L_tsf_type not in ('BT', 'NB') */

EXCEPTION
   when OTHERS then

      if L_error_msg is NULL then
         L_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                      SQLERRM,
                                      'EC_TABLE_THD_AIUDR',
                                      NULL);
      end if;

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
