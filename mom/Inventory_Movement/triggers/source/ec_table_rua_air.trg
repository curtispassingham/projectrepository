PROMPT Creating Trigger 'EC_TABLE_RUA_AIR'
CREATE OR REPLACE TRIGGER EC_TABLE_RUA_AIR
 AFTER INSERT
 ON RUA_RIB_INTERFACE
 FOR EACH ROW
DECLARE

   L_message_type    	VARCHAR2(15) := NULL;
   L_event           	VARCHAR2(1) := 'A';
   O_error_msg      	VARCHAR2(100) := NULL;
   L_business_obj       RMSMFM_RCVUNITADJ.RCVUNITADJ_KEY_REC;
BEGIN
   if INSERTING then
      L_event := 'A';
      L_business_obj.order_no     := :new.order_no;
      L_business_obj.asn	         := :new.asn;
      L_business_obj.location     := :new.location;
      L_business_obj.loc_type     := :new.loc_type;
      L_business_obj.item         := :new.item;
      L_business_obj.carton       := :new.carton;
      L_business_obj.adj_qty      := :new.adj_qty;
      L_message_type := RMSMFM_RCVUNITADJ.RCVUNITADJ_ADD;
   end if;
   if L_message_type is not NULL then
      if rmsmfm_rcvunitadj.addtoq(O_error_msg,
                                  L_message_type,
                                  L_business_obj) = FALSE then
        raise PROGRAM_ERROR;
      end if;
   end if;
EXCEPTION
   when OTHERS then
      if O_error_msg is NULL then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                      'EC_TABLE_RUA_AIR',
                                      NULL);
      end if;

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, O_error_msg);
END;
/
