PROMPT Creating Trigger 'EC_TABLE_ALD_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_ALD_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF QTY_ALLOCATED
 ON ALLOC_DETAIL
 FOR EACH ROW
DECLARE



   L_alloc_no       ALLOC_HEADER.ALLOC_NO%TYPE := NULL;
   L_to_loc         ITEM_LOC.LOC%TYPE := NULL;
   L_qty_allocated  ALLOC_DETAIL.QTY_ALLOCATED%TYPE := NULL;
   L_message_type   ALLOC_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;
   L_text           VARCHAR2(255) := NULL;

   L_item           ITEM_LOC.ITEM%TYPE := NULL;
   L_wh             ITEM_LOC.LOC%TYPE := NULL;
   L_physical_wh    ITEM_LOC.LOC%TYPE := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN


   IF DELETING THEN

      L_alloc_no      := :OLD.alloc_no;
      L_to_loc        := :OLD.to_loc;
      L_message_type  := RMSMFM_ALLOC.DTL_DEL;

   ELSE

      L_alloc_no      := :NEW.alloc_no;
      L_to_loc        := :NEW.to_loc;
      L_qty_allocated := :NEW.qty_allocated;

      IF INSERTING THEN

         IF NVL(:NEW.qty_transferred,0) <= 0 THEN
            L_message_type := RMSMFM_ALLOC.DTL_ADD;
         END IF;

      ELSE  --UPDATING

         IF ((:OLD.qty_allocated != :NEW.qty_allocated) OR
             (NVL(:OLD.qty_cancelled,0) != NVL(:NEW.qty_cancelled,0))) THEN
            L_message_type := RMSMFM_ALLOC.DTL_UPD;
         END IF;

      END IF;

   END IF;

   IF L_message_type IS NOT NULL THEN

      IF RMSMFM_ALLOC.ADDTOQ(L_text,
                             L_message_type,
                             L_alloc_no,
                             NULL,             --alloc_header_status is null from detail trigger
                             L_to_loc) = FALSE THEN
         RAISE PROGRAM_ERROR;
      END IF;

   END IF;

EXCEPTION
   WHEN OTHERS THEN
      IF L_text IS NULL THEN
         L_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                      SQLERRM,
                                      'EC_TABLE_ALD_AIUDR',
                                      NULL);
      END IF;

      RAISE_APPLICATION_ERROR(API_LIBRARY.TRIGGER_EXCEPTION, L_text);
END;
/
