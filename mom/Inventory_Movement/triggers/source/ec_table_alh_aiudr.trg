PROMPT Creating Trigger 'EC_TABLE_ALH_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_ALH_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF STATUS
, RELEASE_DATE
 ON ALLOC_HEADER
 FOR EACH ROW
DECLARE


 
   L_alloc_no              ALLOC_HEADER.ALLOC_NO%TYPE := NULL;
   L_alloc_header_status   ALLOC_HEADER.STATUS%TYPE := NULL;
   L_old_release_date      VARCHAR2(255);
   L_new_release_date      VARCHAR2(255);
   L_message_type          ALLOC_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;
   L_text                  VARCHAR2(255) := NULL;
   PROGRAM_ERROR           EXCEPTION;
BEGIN
 
   if DELETING then
      L_message_type        := RMSMFM_ALLOC.HDR_DEL;
      L_alloc_no            := :OLD.alloc_no;
      L_alloc_header_status := :OLD.status;
 
   else
      L_old_release_date    := NVL(to_char(:old.release_date), 0);
      L_new_release_date    := NVL(to_char(:new.release_date), 0);
      L_alloc_no            := :NEW.alloc_no;
      L_alloc_header_status := :NEW.status;
 
      if INSERTING then
 
         L_message_type := RMSMFM_ALLOC.HDR_ADD;
 
      else
 
         if (UPDATING AND :NEW.status IN ('A', 'C')
            and :NEW.status != :OLD.status AND :OLD.status != 'X')
            or L_old_release_date != L_new_release_date then
            L_message_type := RMSMFM_ALLOC.HDR_UPD;
         end if;
 
      end if;
 
   end if;
 
   if L_message_type is NOT NULL then
 
      if RMSMFM_ALLOC.ADDTOQ(L_text,
                             L_message_type,
                             L_alloc_no,
                             L_alloc_header_status,
                             NULL) = FALSE then                 --TO_LOC is null for a header level call
         raise PROGRAM_ERROR;
      end if;
 
   end if;
 
EXCEPTION
   when OTHERS then
      if L_text is NULL then
         L_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                      SQLERRM,
                                      'EC_TABLE_ALH_AIUDR',
                                      NULL);
      end if;
 
      RAISE_APPLICATION_ERROR(API_LIBRARY.TRIGGER_EXCEPTION, L_text);
 
END;
/
