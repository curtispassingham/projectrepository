PROMPT Creating Trigger 'RMS_VIEW_BWS_UR'
CREATE OR REPLACE TRIGGER RMS_VIEW_BWS_UR 
   INSTEAD OF UPDATE ON v_buyer_wksht_search
   FOR EACH ROW
DECLARE
   L_error_message  VARCHAR2(1000) := NULL;
   L_table_name     VARCHAR2(1000) := NULL;
   
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_REPL_RESULTS is
      select 'x' 
        from repl_results
       where rowid = chartorowid(:new.row_id)
         and source_type = 'R'
         for update nowait;

   cursor C_LOCK_IB_RESULTS is
      select 'x' 
        from ib_results
       where rowid = chartorowid(:new.row_id)
         and source_type = 'I'
         for update nowait;

   cursor C_LOCK_MANUAL is
      select 'x' 
        from buyer_wksht_manual
       where rowid = chartorowid(:new.row_id)
         and source_type = 'M'
         for update nowait;

BEGIN

   if :old.source_type = 'R' then
      L_table_name := 'REPL_RESULTS';
      ---
      if nvl(:old.audsid, -999) != nvl(:new.audsid, -999) then
         open C_LOCK_REPL_RESULTS;
         close C_LOCK_REPL_RESULTS;

         update repl_results
            set audsid = :new.audsid
          where rowid = chartorowid(:new.row_id);
      end if;

      if nvl(:old.unit_cost, 0) != nvl(:new.unit_cost, 0) OR
         nvl(:old.order_roq, 0) != nvl(:new.order_roq, 0) then
         open C_LOCK_REPL_RESULTS;
         close C_LOCK_REPL_RESULTS;

         update repl_results
            set unit_cost = :new.unit_cost,
                order_roq = :new.order_roq
          where rowid = chartorowid(:new.row_id);
      end if;
   elsif :old.source_type = 'I' then
      L_table_name := 'IB_RESULTS';
      ---
      if nvl(:old.audsid, -999) != nvl(:new.audsid, -999) then
         open C_LOCK_IB_RESULTS;
         close C_LOCK_IB_RESULTS;

         update ib_results
            set audsid = :new.audsid
          where rowid = chartorowid(:new.row_id);
      end if;

      if nvl(:old.unit_cost, 0) != nvl(:new.unit_cost, 0) OR
         nvl(:old.order_roq, 0) != nvl(:new.order_roq, 0) then
         open C_LOCK_IB_RESULTS;
         close C_LOCK_IB_RESULTS;

         update ib_results
            set unit_cost = :new.unit_cost,
                order_roq = :new.order_roq
          where rowid = chartorowid(:new.row_id);
      end if;
   elsif :old.source_type = 'M' then
      L_table_name := 'BUYER_WKSHT_MANUAL';
      ---
      if nvl(:old.audsid, -999) != nvl(:new.audsid, -999) then
         open C_LOCK_MANUAL;
         close C_LOCK_MANUAL;

         update buyer_wksht_manual
            set audsid = :new.audsid
          where rowid = chartorowid(:new.row_id);
      end if;

      if nvl(:old.unit_cost, 0) != nvl(:new.unit_cost, 0) OR
         nvl(:old.order_roq, 0) != nvl(:new.order_roq, 0) then
         open C_LOCK_MANUAL;
         close C_LOCK_MANUAL;

         update buyer_wksht_manual
            set unit_cost = :new.unit_cost,
                order_roq = :new.order_roq
          where rowid = chartorowid(:new.row_id);
      end if;

   end if;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table_name,
                                            NULL,
                                            NULL);
     RAISE_APPLICATION_ERROR(-20001, L_error_message);
   WHEN OTHERS THEN
     L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           'RMS_VIEW_BWS_UR',
                                           to_char(SQLCODE));
     RAISE_APPLICATION_ERROR(-20001, L_error_message);
END RMS_VIEW_BWS_UR;
/
