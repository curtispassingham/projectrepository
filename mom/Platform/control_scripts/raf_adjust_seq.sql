PROMPT Executing control script raf_adjust_seq.sql;

whenever sqlerror exit
set serveroutput on size 1000000;


PROMPT Creating procedure raf_adjust_db_seq

create or replace procedure raf_adjust_db_seq(in_table_name in varchar2,
                                              in_id_column in varchar2,
                                              in_seq_name in varchar2) as 
  l_max_num number;
  l_seq_count number;
  l_sql varchar2(500 char);
  l_new_next_val number;
  l_seq_min_val number;
  l_seq_max_val number;
  l_seq_incr number;
  l_seq_cache number;

begin
  sys.dbms_output.put_line('*******************************************');
  sys.dbms_output.put_line('Starting RAF sequence adjustment for sequence '||in_seq_name||'...');
  l_sql := 'select max('||in_id_column||') from '||in_table_name;
  SYS.DBMS_OUTPUT.PUT_LINE('Executing '||l_sql);
  execute immediate l_sql into l_max_num;
  
  if l_max_num is not null and l_max_num > 0 then
      sys.dbms_output.put_line('Max value of '||in_table_name||'.'||in_id_column||' is '||l_max_num);
      sys.dbms_output.put_line('Modifying sequence '||in_seq_name||' starting value to max value + increment');
      select count(*) into l_seq_count from all_sequences where sequence_name = in_seq_name AND SEQUENCE_OWNER=USER;
      if l_seq_count > 0 then
      
        -- Get information about sequence...
        select min_value into l_seq_min_val from all_sequences where sequence_name = upper(in_seq_name) and sequence_owner=user;
        select max_value into l_seq_max_val from all_sequences where sequence_name = upper(in_seq_name) and sequence_owner=user;
        select increment_by into l_seq_incr from all_sequences where sequence_name = upper(in_seq_name) and sequence_owner=user;
        select cache_size into l_seq_cache from all_sequences where sequence_name = upper(in_seq_name) and sequence_owner=user;
        
        -- Drop the sequence...
        l_sql:='DROP SEQUENCE '||in_seq_name;
        SYS.DBMS_OUTPUT.PUT_LINE('executing '||l_sql);
        execute immediate l_sql;
        
        -- Recreate the sequence...
        l_sql := 'CREATE SEQUENCE '||in_seq_name||' MINVALUE '||l_seq_min_val||' MAXVALUE '||l_seq_max_val||' INCREMENT BY '||(l_seq_incr)||' START WITH '||(l_max_num+l_seq_incr)||' CACHE '||l_seq_cache||' NOORDER  NOCYCLE';
        SYS.DBMS_OUTPUT.PUT_LINE('executing '||l_sql);
        execute immediate l_sql;
        
        -- Report result...
        select last_number into l_new_next_val from all_sequences where sequence_name = upper(in_seq_name) and sequence_owner=user;
        SYS.DBMS_OUTPUT.PUT_LINE('Next value for '||in_seq_name||' is '||l_new_next_val);
        
      else
        SYS.DBMS_OUTPUT.put_line('Sequence NOT found :'||in_seq_name);
      end if;
  else
      sys.dbms_output.put_line('Max value of '||in_table_name||'.'||in_id_column||' is zero or null.  No adjustments made.');
  end if;
  sys.dbms_output.put_line('RAF sequence adjustment completed successfully.');
end;
/


PROMPT Executing procedure raf_adjust_db_seq

BEGIN
  RAF_ADJUST_DB_SEQ('RTC_LOOKUP_TYPES_B', 'LOOKUP_TYPE_ID', 'RTC_LOOKUP_TYPES_B_S1');
  RAF_ADJUST_DB_SEQ('RTC_LOOKUP_VALUES_B', 'LOOKUP_VALUE_ID', 'RTC_LOOKUP_VALUES_B_S1');
END;
/
