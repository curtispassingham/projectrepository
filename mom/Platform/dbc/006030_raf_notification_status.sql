--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RAF_NOTIFICATION_STATUS'
ALTER TABLE RAF_NOTIFICATION_STATUS ADD CREATED_BY VARCHAR2 (64 ) NULL
/

COMMENT ON COLUMN RAF_NOTIFICATION_STATUS.CREATED_BY is 'Who column: indicates the user who created the row.'
/

ALTER TABLE RAF_NOTIFICATION_STATUS ADD CREATE_DATE TIMESTAMP  NULL
/

COMMENT ON COLUMN RAF_NOTIFICATION_STATUS.CREATE_DATE is 'Who column: indicates the date and time of the creation of the row.'
/

ALTER TABLE RAF_NOTIFICATION_STATUS ADD LAST_UPDATED_BY VARCHAR2 (64 ) NULL
/

COMMENT ON COLUMN RAF_NOTIFICATION_STATUS.LAST_UPDATED_BY is 'Who column: indicates the user who last updated the row.'
/

ALTER TABLE RAF_NOTIFICATION_STATUS ADD LAST_UPDATE_DATE TIMESTAMP  NULL
/

COMMENT ON COLUMN RAF_NOTIFICATION_STATUS.LAST_UPDATE_DATE is 'Who column: indicates the date and time of the last update of the row.'
/

ALTER TABLE RAF_NOTIFICATION_STATUS ADD OBJECT_VERSION_NUMBER NUMBER (9) NULL
/

COMMENT ON COLUMN RAF_NOTIFICATION_STATUS.OBJECT_VERSION_NUMBER is 'Who column: Object Version Number used for concurrency purposes'
/

PROMPT Updating Table 'RAF_NOTIFICATION_STATUS'
UPDATE RAF_NOTIFICATION_STATUS 
SET CREATED_BY = (SELECT CREATED_BY FROM RAF_NOTIFICATION WHERE RAF_NOTIFICATION_STATUS.RAF_NOTIFICATION_ID = RAF_NOTIFICATION.RAF_NOTIFICATION_ID ),
CREATE_DATE = (SELECT CREATE_DATE FROM RAF_NOTIFICATION WHERE RAF_NOTIFICATION_STATUS.RAF_NOTIFICATION_ID = RAF_NOTIFICATION.RAF_NOTIFICATION_ID ),
LAST_UPDATED_BY = (SELECT LAST_UPDATED_BY FROM RAF_NOTIFICATION WHERE RAF_NOTIFICATION_STATUS.RAF_NOTIFICATION_ID = RAF_NOTIFICATION.RAF_NOTIFICATION_ID ),
LAST_UPDATE_DATE = (SELECT LAST_UPDATE_DATE FROM RAF_NOTIFICATION WHERE RAF_NOTIFICATION_STATUS.RAF_NOTIFICATION_ID = RAF_NOTIFICATION.RAF_NOTIFICATION_ID ),
OBJECT_VERSION_NUMBER = (SELECT OBJECT_VERSION_NUMBER FROM RAF_NOTIFICATION WHERE RAF_NOTIFICATION_STATUS.RAF_NOTIFICATION_ID = RAF_NOTIFICATION.RAF_NOTIFICATION_ID )
/

PROMPT Modifying Table 'RAF_NOTIFICATION_STATUS'
ALTER TABLE RAF_NOTIFICATION_STATUS MODIFY CREATED_BY VARCHAR2 (64 ) NOT NULL
/

ALTER TABLE RAF_NOTIFICATION_STATUS MODIFY CREATE_DATE TIMESTAMP  NOT NULL
/

ALTER TABLE RAF_NOTIFICATION_STATUS MODIFY LAST_UPDATED_BY VARCHAR2 (64 ) NOT NULL
/

ALTER TABLE RAF_NOTIFICATION_STATUS MODIFY LAST_UPDATE_DATE TIMESTAMP  NOT NULL
/

ALTER TABLE RAF_NOTIFICATION_STATUS MODIFY OBJECT_VERSION_NUMBER NUMBER (9) NOT NULL
/
