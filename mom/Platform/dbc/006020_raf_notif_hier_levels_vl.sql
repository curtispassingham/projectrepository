--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

------------------------------------------------------
--       View Creation      RAF_NOTIF_HIER_LEVELS_VL           
------------------------------------------------------
PROMPT CREATING NOTIFICATION HIERARCHY LEVELS VIEW ;
CREATE OR REPLACE FORCE VIEW RAF_NOTIF_HIER_LEVELS_VL (HIER_LEVEL_ID, TOKEN_NAME, HIER_LEVEL_DETAIL, CATEGORY, PARENT_HIER_LEVEL_ID ,LANGUAGE, NAME, DESCRIPTION,  CREATED_BY, CREATE_DATE, LAST_UPDATE_DATE, LAST_UPDATED_BY, APPLICATION_CODE) AS 
  SELECT b.HIER_LEVEL_ID,
	   b.TOKEN_NAME,
	   b.HIER_LEVEL_DETAIL,
	   b.CATEGORY,
	   b.PARENT_HIER_LEVEL_ID,
       tl.LANGUAGE,
       tl.NAME,
       tl.DESCRIPTION,
       b.CREATED_BY,
       b.CREATE_DATE,
       b.LAST_UPDATE_DATE,
       b.LAST_UPDATED_BY,
	     b.APPLICATION_CODE
  from RAF_NOTIF_HIER_LEVELS_B b,
       RAF_NOTIF_HIER_LEVELS_TL tl
 where tl.HIER_LEVEL_ID = b.HIER_LEVEL_ID
   and tl.language = userenv('lang') 
/
