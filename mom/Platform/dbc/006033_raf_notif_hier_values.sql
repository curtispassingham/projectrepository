--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RAF_NOTIF_HIER_VALUES'
ALTER TABLE RAF_NOTIF_HIER_VALUES ADD HIER_VALUE_9 VARCHAR2 (250 ) NULL
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_VALUE_9 is 'Column used to store additional information about the intersection.'
/

