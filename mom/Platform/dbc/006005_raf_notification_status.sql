--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_NOTIFICATION_STATUS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_NOTIFICATION_STATUS'
CREATE TABLE RAF_NOTIFICATION_STATUS
 (RAF_NOTIFICATION_ID NUMBER(18) NOT NULL,
  STATUS VARCHAR2(1 ) NOT NULL,
  USER_ID VARCHAR2(64 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_NOTIFICATION_STATUS is 'This table holds information about the READ/UNREAD status of each Notification'
/

COMMENT ON COLUMN RAF_NOTIFICATION_STATUS.RAF_NOTIFICATION_ID is 'ID of the Notification'
/

COMMENT ON COLUMN RAF_NOTIFICATION_STATUS.STATUS is 'Status of the Notification. R | U are possible values'
/

COMMENT ON COLUMN RAF_NOTIFICATION_STATUS.USER_ID is 'Username of the user who modified the Notification'
/


PROMPT Creating Primary Key on 'RAF_NOTIFICATION_STATUS'
ALTER TABLE RAF_NOTIFICATION_STATUS
 ADD CONSTRAINT RAF_NOTIF_STATUS_PK PRIMARY KEY
  (RAF_NOTIFICATION_ID,
   USER_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'RAF_NOTIFICATION_STATUS'
 ALTER TABLE RAF_NOTIFICATION_STATUS
  ADD CONSTRAINT RAF_NOTIF_STATUS_FK1
  FOREIGN KEY (RAF_NOTIFICATION_ID)
 REFERENCES RAF_NOTIFICATION (RAF_NOTIFICATION_ID)
 ON DELETE CASCADE
/

