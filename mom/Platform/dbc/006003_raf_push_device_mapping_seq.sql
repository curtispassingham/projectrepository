--------------------------------------------------------
-- Copyright (c) 2010, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE ADDED:				RAF_PUSH_DEVICE_MAPPING_SEQ 
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------
--       ADDING sequence 
--------------------------------
CREATE SEQUENCE RAF_PUSH_DEVICE_MAPPING_SEQ 
INCREMENT BY 1 
START WITH 100
MAXVALUE 9999999 
MINVALUE 1 
CACHE 20
/
