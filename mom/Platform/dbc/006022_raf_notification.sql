--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT Populating Notification_context column with values from RAF_NOTIFICATION
INSERT INTO RAF_NOTIFICATION_CONTEXT (RAF_NOTIFICATION_ID,NOTIFICATION_CONTEXT, SOURCE)(SELECT RAF_NOTIFICATION_ID,TO_CLOB(NOTIFICATION_CONTEXT), 'TEXT' FROM RAF_NOTIFICATION WHERE NOTIFICATION_CONTEXT IS NOT NULL)
/

PROMPT Modifying Table 'RAF_NOTIFICATION'
ALTER TABLE RAF_NOTIFICATION DROP COLUMN NOTIFICATION_CONTEXT
/

