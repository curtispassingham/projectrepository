--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RAF_USER_NOTIF_SET_GROUPS'

PROMPT Modifying Primary Key on 'RAF_USER_NOTIF_SET_GROUPS'
DECLARE
  L_constraint_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constraint_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'RAF_USER_NOTIF_SG_PK1';

  if (L_constraint_exists != 0) then
      execute immediate 'ALTER TABLE RAF_USER_NOTIF_SET_GROUPS DROP CONSTRAINT RAF_USER_NOTIF_SG_PK1 DROP INDEX';
  end if;
end;
/

PROMPT Re-Creating Primary Key on 'RAF_USER_NOTIF_SET_GROUPS'
ALTER TABLE RAF_USER_NOTIF_SET_GROUPS
 ADD CONSTRAINT RAF_USER_NOTIF_SG_PK1 PRIMARY KEY
  (NOTIF_SETTING_ID,
   HIER_LEVEL_ID,
   HIER_ORDER
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
