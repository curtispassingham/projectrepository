--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_NOTIFICATION_CONTEXT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_NOTIFICATION_CONTEXT'
CREATE TABLE RAF_NOTIFICATION_CONTEXT
 (RAF_NOTIFICATION_ID NUMBER(18) NOT NULL,
  NOTIFICATION_CONTEXT CLOB NOT NULL,
  SOURCE VARCHAR2(10 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_NOTIFICATION_CONTEXT is 'This table holds the context for each Notification'
/

COMMENT ON COLUMN RAF_NOTIFICATION_CONTEXT.RAF_NOTIFICATION_ID is 'ID of the Notification'
/

COMMENT ON COLUMN RAF_NOTIFICATION_CONTEXT.NOTIFICATION_CONTEXT is 'Context information for the Notification'
/

COMMENT ON COLUMN RAF_NOTIFICATION_CONTEXT.SOURCE is 'Origin of the Notification Context'
/


PROMPT Creating Primary Key on 'RAF_NOTIFICATION_CONTEXT'
ALTER TABLE RAF_NOTIFICATION_CONTEXT
 ADD CONSTRAINT RAF_NOTIF_CXT_PK PRIMARY KEY
  (RAF_NOTIFICATION_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'RAF_NOTIFICATION_CONTEXT'
 ALTER TABLE RAF_NOTIFICATION_CONTEXT
  ADD CONSTRAINT RAF_NOTIF_CXT_FK1
  FOREIGN KEY (RAF_NOTIFICATION_ID)
 REFERENCES RAF_NOTIFICATION (RAF_NOTIFICATION_ID)
 ON DELETE CASCADE
/

