--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE ADDED:				RAF_CONVERSATION_MAPPING_SEQ
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       ADDING SEQUENCE
--------------------------------------
PROMPT Creating Sequence 'RAF_CONVERSATION_MAPPING_SEQ'
CREATE SEQUENCE RAF_CONVERSATION_MAPPING_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 99999999999999999999
  MINVALUE 1
  CACHE 20
  NOORDER
/