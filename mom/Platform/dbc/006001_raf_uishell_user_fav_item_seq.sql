--------------------------------------------------------
-- Copyright (c) 2010, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE ADDED:				RAF_UISHELL_USER_FAV_ITEM_SEQ
----------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------
--       ADDING sequence 
--------------------------------
CREATE SEQUENCE RAF_UISHELL_USER_FAV_ITEM_SEQ 
MINVALUE 100 
MAXVALUE 999999999999999999999999999 
INCREMENT BY 10 
START WITH 100
/
