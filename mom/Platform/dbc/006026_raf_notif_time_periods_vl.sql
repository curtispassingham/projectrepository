--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RMS_OI_DEPT_OPTIONS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating VIEW               
--------------------------------------
CREATE OR REPLACE FORCE VIEW RAF_NOTIF_TIME_PERIODS_VL (PERIOD_ID, LANGUAGE, NAME, DESCRIPTION, START_DATE, END_DATE, CREATED_BY, CREATE_DATE, LAST_UPDATE_DATE, LAST_UPDATED_BY, APPLICATION_CODE) AS 
  SELECT b.PERIOD_ID,
       tl.LANGUAGE,
       tl.NAME,
       tl.DESCRIPTION,
       TRUNC(SYSDATE +START_DATE_OFFSET) AS START_DATE,
       TRUNC(SYSDATE +END_DATE_OFFSET) AS END_DATE,
       b.CREATED_BY,
       b.CREATE_DATE,
       b.LAST_UPDATE_DATE,
       b.LAST_UPDATED_BY,
	     b.APPLICATION_CODE
  from RAF_NOTIF_TIME_PERIODS_B b,
       RAF_NOTIF_TIME_PERIODS_TL tl
 where tl.PERIOD_ID = b.PERIOD_ID
   and tl.language = userenv('lang')
/
   