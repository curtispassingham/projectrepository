--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_PUSH_DEVICE_MAPPING 
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_PUSH_DEVICE_MAPPING'
CREATE TABLE RAF_PUSH_DEVICE_MAPPING
 (PAIRING_ID NUMBER NOT NULL,
  DEVICE_TOKEN CLOB NOT NULL,
  USERNAME VARCHAR2(64 CHAR) NOT NULL,
  PLATFORM VARCHAR2(10 CHAR) NOT NULL,
  TOKEN_HASH NUMBER DEFAULT ORA_HASH('DEVICE_TOKEN'),
  CREATED_BY VARCHAR2(64 CHAR) NOT NULL,
  CREATION_DATE TIMESTAMP NOT NULL,
  LAST_UPDATED_BY VARCHAR2(64 CHAR) NOT NULL,
  LAST_UPDATE_DATE TIMESTAMP NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_PUSH_DEVICE_MAPPING is 'A table that holds user and mobile device mappings for push notification features.'
/

COMMENT ON COLUMN RAF_PUSH_DEVICE_MAPPING.PAIRING_ID is 'ID column. Sequence-generated primary key'
/

COMMENT ON COLUMN RAF_PUSH_DEVICE_MAPPING.DEVICE_TOKEN is 'Device token for sending notifications to this user'
/

COMMENT ON COLUMN RAF_PUSH_DEVICE_MAPPING.USERNAME is 'Username associated with this device.'
/

COMMENT ON COLUMN RAF_PUSH_DEVICE_MAPPING.PLATFORM is 'OS the device is runnning. Used to determine which protocol to send with.'
/

COMMENT ON COLUMN RAF_PUSH_DEVICE_MAPPING.TOKEN_HASH is 'Hash of device token. Used for unique constraint since token must be a CLOB for Android tokens'
/

COMMENT ON COLUMN RAF_PUSH_DEVICE_MAPPING.CREATED_BY is 'Who column: indicates the user who created the row.'
/

COMMENT ON COLUMN RAF_PUSH_DEVICE_MAPPING.CREATION_DATE is 'Who column: indicates the date and time of the creation of the row.'
/

COMMENT ON COLUMN RAF_PUSH_DEVICE_MAPPING.LAST_UPDATED_BY is 'Who column: indicates the user who last updated the row.'
/

COMMENT ON COLUMN RAF_PUSH_DEVICE_MAPPING.LAST_UPDATE_DATE is 'Time and date when this row was last updated.'
/


PROMPT Creating Primary Key on 'RAF_PUSH_DEVICE_MAPPING'
ALTER TABLE RAF_PUSH_DEVICE_MAPPING
 ADD CONSTRAINT RAF_PUSH_DEVICE_MAPPING_PK PRIMARY KEY
  (PAIRING_ID
 )
 USING INDEX
 INITRANS 6
 TABLESPACE RETAIL_DATA
/


PROMPT Creating Unique Key on 'RAF_PUSH_DEVICE_MAPPING'
ALTER TABLE RAF_PUSH_DEVICE_MAPPING
 ADD CONSTRAINT RAF_PUSH_DEVICE_MAPPING_UK1 UNIQUE
  (USERNAME,
   TOKEN_HASH
 )
 USING INDEX
 INITRANS 6
 TABLESPACE RETAIL_DATA
/
