--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RAF_NOTIF_HIER_LEVELS_B'
ALTER TABLE RAF_NOTIF_HIER_LEVELS_B ADD HIER_VALUE_INDEX NUMBER (3,0) NULL
/

COMMENT ON COLUMN RAF_NOTIF_HIER_LEVELS_B.HIER_VALUE_INDEX is 'Stores the index of the Hierarchy Level'
/

