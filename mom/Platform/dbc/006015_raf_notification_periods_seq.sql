--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE ADDED:				RAF_NOTIFICATION_PERIODS_SEQ
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       ADDING SEQUENCE
--------------------------------------
PROMPT Creating Sequence 'RAF_NOTIFICATION_PERIODS_SEQ'
CREATE SEQUENCE RAF_NOTIFICATION_PERIODS_SEQ
  INCREMENT BY 1
  START WITH 100
  MAXVALUE 9999999
  MINVALUE 1
  CACHE 20
  NOORDER
/