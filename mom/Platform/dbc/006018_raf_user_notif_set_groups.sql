--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_USER_NOTIF_SET_GROUPS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_USER_NOTIF_SET_GROUPS'
CREATE TABLE RAF_USER_NOTIF_SET_GROUPS
 (NOTIF_SETTING_ID NUMBER(18) NOT NULL,
  HIER_LEVEL_ID NUMBER(18) NOT NULL,
  HIER_ORDER NUMBER(1) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_USER_NOTIF_SET_GROUPS is 'Holds the user Notification Groupings'
/

COMMENT ON COLUMN RAF_USER_NOTIF_SET_GROUPS.NOTIF_SETTING_ID is 'Unique ID identifying the Setting '
/

COMMENT ON COLUMN RAF_USER_NOTIF_SET_GROUPS.HIER_LEVEL_ID is 'Unique ID identifying the Hierarchy Level'
/

COMMENT ON COLUMN RAF_USER_NOTIF_SET_GROUPS.HIER_ORDER is 'Indicates the order in which the groupings have to be applied'
/


PROMPT Creating Primary Key on 'RAF_USER_NOTIF_SET_GROUPS'
ALTER TABLE RAF_USER_NOTIF_SET_GROUPS
 ADD CONSTRAINT RAF_USER_NOTIF_SG_PK1 PRIMARY KEY
  (NOTIF_SETTING_ID,
   HIER_LEVEL_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'RAF_USER_NOTIF_SET_GROUPS'
 ALTER TABLE RAF_USER_NOTIF_SET_GROUPS
  ADD CONSTRAINT RAF_USER_NOTIF_SG_FK1
  FOREIGN KEY (HIER_LEVEL_ID)
 REFERENCES RAF_NOTIF_HIER_LEVELS_B (HIER_LEVEL_ID)
/


PROMPT Creating FK on 'RAF_USER_NOTIF_SET_GROUPS'
 ALTER TABLE RAF_USER_NOTIF_SET_GROUPS
  ADD CONSTRAINT RAF_USER_NOTIF_SG_FK2
  FOREIGN KEY (NOTIF_SETTING_ID)
 REFERENCES RAF_USER_NOTIF_SETTINGS (NOTIF_SETTING_ID)
/

