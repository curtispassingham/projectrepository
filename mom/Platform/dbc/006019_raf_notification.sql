--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT Populating RECIPIENT_ID column with values from RAF_NOTIFICATION
INSERT INTO RAF_NOTIFICATION_RECIPIENTS (RAF_NOTIFICATION_ID,RECIPIENT_ID)(SELECT RAF_NOTIFICATION_ID,ASSIGNED_TO FROM RAF_NOTIFICATION WHERE ASSIGNED_TO IS NOT NULL)
/

PROMPT Modifying Table 'RAF_NOTIFICATION'
ALTER TABLE RAF_NOTIFICATION DROP COLUMN ASSIGNED_TO
/
