--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_USER_NOTIF_SET_SEVS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_USER_NOTIF_SET_SEVS'
CREATE TABLE RAF_USER_NOTIF_SET_SEVS
 (NOTIF_SETTING_ID NUMBER(18) NOT NULL,
  SEVERITY NUMBER(9) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_USER_NOTIF_SET_SEVS is 'Stores the severities saved by the user in the Notification filter'
/

COMMENT ON COLUMN RAF_USER_NOTIF_SET_SEVS.NOTIF_SETTING_ID is 'Unique ID identifying the Setting '
/

COMMENT ON COLUMN RAF_USER_NOTIF_SET_SEVS.SEVERITY is 'Severity of a Notification'
/


PROMPT Creating Primary Key on 'RAF_USER_NOTIF_SET_SEVS'
ALTER TABLE RAF_USER_NOTIF_SET_SEVS
 ADD CONSTRAINT RAF_USER_NOTIF_SS_PK PRIMARY KEY
  (NOTIF_SETTING_ID,
   SEVERITY
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'RAF_USER_NOTIF_SET_SEVS'
 ALTER TABLE RAF_USER_NOTIF_SET_SEVS
  ADD CONSTRAINT RAF_USER_NOTIF_SS_FK1
  FOREIGN KEY (NOTIF_SETTING_ID)
 REFERENCES RAF_USER_NOTIF_SETTINGS (NOTIF_SETTING_ID)
/

