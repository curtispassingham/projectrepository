--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_NOTIF_TIME_PERIODS_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_NOTIF_TIME_PERIODS_TL'
CREATE TABLE RAF_NOTIF_TIME_PERIODS_TL
 (PERIOD_ID NUMBER(18) NOT NULL,
  LANGUAGE VARCHAR2(4 ) NOT NULL,
  NAME VARCHAR2(80 ) NOT NULL,
  DESCRIPTION VARCHAR2(240 ),
  SOURCE_LANG VARCHAR2(4 ) NOT NULL,
  CREATED_BY VARCHAR2(64 ) NOT NULL,
  CREATE_DATE TIMESTAMP NOT NULL,
  LAST_UPDATED_BY VARCHAR2(64 ) NOT NULL,
  LAST_UPDATE_DATE TIMESTAMP NOT NULL,
  OBJECT_VERSION_NUMBER NUMBER(9) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_NOTIF_TIME_PERIODS_TL is 'Translation table to hold the time period strings'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_TL.PERIOD_ID is 'Unique ID identifying that period'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_TL.LANGUAGE is 'Indicates the code of the language into which the contents of the translatable columns are translated.'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_TL.NAME is 'Name'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_TL.DESCRIPTION is 'Description'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_TL.SOURCE_LANG is 'Indicates the code of the language in which the contents of the translatable columns were originally created.'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_TL.CREATED_BY is 'Who column: indicates the user who created the row.'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_TL.CREATE_DATE is 'Who column: indicates the date and time of the creation of the row.'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_TL.LAST_UPDATED_BY is 'Who column: indicates the user who last updated the row.'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_TL.LAST_UPDATE_DATE is 'Who column: indicates the date and time of the last update of the row.'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_TL.OBJECT_VERSION_NUMBER is 'Who column: Object Version Number used for concurrency purposes'
/


PROMPT Creating Primary Key on 'RAF_NOTIF_TIME_PERIODS_TL'
ALTER TABLE RAF_NOTIF_TIME_PERIODS_TL
 ADD CONSTRAINT RAF_NOTIF_TIME_PERIODS_FK1 PRIMARY KEY
  (PERIOD_ID,
   LANGUAGE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'RAF_NOTIF_TIME_PERIODS_TL'
 ALTER TABLE RAF_NOTIF_TIME_PERIODS_TL
  ADD CONSTRAINT RAF_NOTIF_TP_TL_FK1
  FOREIGN KEY (PERIOD_ID)
 REFERENCES RAF_NOTIF_TIME_PERIODS_B (PERIOD_ID)
 ON DELETE CASCADE
/

