--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_CONVERSATION_MAPPING
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_CONVERSATION_MAPPING'
CREATE TABLE RAF_CONVERSATION_MAPPING
 (CONVERSATION_MAPPING_ID NUMBER(30) NOT NULL,
  CONVERSATION_ID VARCHAR2(100 ) NOT NULL,
  CONVERSATION_FLAVOR VARCHAR2(30 ) NOT NULL,
  SOCIAL_OBJECT_TYPE VARCHAR2(30 ) NOT NULL,
  SOCIAL_OBJECT_ID VARCHAR2(100 ) NOT NULL,
  CREATED_DATE TIMESTAMP,
  CREATED_BY_RGBU_USER VARCHAR2(64 ),
  CREATED_BY_FRAMEWORK_USER VARCHAR2(64 ),
  CREATED_BY_TASKFLOW VARCHAR2(100 ),
  CREATED_BY_APPLICATION VARCHAR2(50 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_CONVERSATION_MAPPING is 'This table holds the mappings from application business object context to conversation id'
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CONVERSATION_MAPPING_ID is 'Surrogate key to uniquely identify mappings in this table.'
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CONVERSATION_ID is 'Unique identifier for a conversation in the selected framework.'
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CONVERSATION_FLAVOR is 'Id for the backend framework that generated this conversation.'
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.SOCIAL_OBJECT_TYPE is 'Id for the type of business object to use for the social context.'
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.SOCIAL_OBJECT_ID is 'Id of the business object to use for the social context.'
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CREATED_DATE is 'Who column: indicates the date and time of the creation of the row.'
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CREATED_BY_RGBU_USER is 'Who column: indicates the user from the RGBU app who created the row.'
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CREATED_BY_FRAMEWORK_USER is 'Who column: indicates the user from the conversation framework who created the row.'
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CREATED_BY_TASKFLOW is 'Who column: indicates the taskflow that created this row.'
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CREATED_BY_APPLICATION is 'Who column: indicates the application that created this row.'
/


PROMPT Creating Primary Key on 'RAF_CONVERSATION_MAPPING'
ALTER TABLE RAF_CONVERSATION_MAPPING
 ADD CONSTRAINT RAF_CONV_MAPPING_PK PRIMARY KEY
  (CONVERSATION_MAPPING_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'RAF_CONVERSATION_MAPPING'
ALTER TABLE RAF_CONVERSATION_MAPPING
 ADD CONSTRAINT RAF_CONV_MAPPING_UK1 UNIQUE
  (SOCIAL_OBJECT_TYPE,
   SOCIAL_OBJECT_ID,
   CONVERSATION_FLAVOR,
   CONVERSATION_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

