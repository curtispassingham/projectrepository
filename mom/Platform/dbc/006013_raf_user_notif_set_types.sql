--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_USER_NOTIF_SET_TYPES
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_USER_NOTIF_SET_TYPES'
CREATE TABLE RAF_USER_NOTIF_SET_TYPES
 (NOTIF_SETTING_ID NUMBER(18) NOT NULL,
  NOTIFICATION_TYPE NUMBER(18) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_USER_NOTIF_SET_TYPES is 'Stores the Types chosen for the Notification Filter'
/

COMMENT ON COLUMN RAF_USER_NOTIF_SET_TYPES.NOTIF_SETTING_ID is 'Unique ID identifying the Setting '
/

COMMENT ON COLUMN RAF_USER_NOTIF_SET_TYPES.NOTIFICATION_TYPE is 'Unique ID identifying the Notification Type'
/


PROMPT Creating Primary Key on 'RAF_USER_NOTIF_SET_TYPES'
ALTER TABLE RAF_USER_NOTIF_SET_TYPES
 ADD CONSTRAINT RAF_USER_NOTIF_ST_PK PRIMARY KEY
  (NOTIF_SETTING_ID,
   NOTIFICATION_TYPE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'RAF_USER_NOTIF_SET_TYPES'
 ALTER TABLE RAF_USER_NOTIF_SET_TYPES
  ADD CONSTRAINT RAF_USER_NOTIF_ST_FK1
  FOREIGN KEY (NOTIF_SETTING_ID)
 REFERENCES RAF_USER_NOTIF_SETTINGS (NOTIF_SETTING_ID)
/


PROMPT Creating FK on 'RAF_USER_NOTIF_SET_TYPES'
 ALTER TABLE RAF_USER_NOTIF_SET_TYPES
  ADD CONSTRAINT RAF_USER_NOTIF_ST_FK2
  FOREIGN KEY (NOTIFICATION_TYPE)
 REFERENCES RAF_NOTIFICATION_TYPE_B (NOTIFICATION_TYPE)
/

