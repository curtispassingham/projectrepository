--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_NOTIF_SET_SEVERITIES
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_NOTIF_SET_SEVERITIES'
CREATE TABLE RAF_NOTIF_SET_SEVERITIES
 (NOTIF_SETTING_ID NUMBER(18) NOT NULL,
  SEVERITY NUMBER(9) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_NOTIF_SET_SEVERITIES is 'Stores the severities saved by the user in the Notification filter'
/

COMMENT ON COLUMN RAF_NOTIF_SET_SEVERITIES.NOTIF_SETTING_ID is 'Unique ID identifying the Setting '
/

COMMENT ON COLUMN RAF_NOTIF_SET_SEVERITIES.SEVERITY is 'Severity of a Notification'
/


PROMPT Creating Primary Key on 'RAF_NOTIF_SET_SEVERITIES'
ALTER TABLE RAF_NOTIF_SET_SEVERITIES
 ADD CONSTRAINT RAF_NOTIF_SEVS_PK1 PRIMARY KEY
  (NOTIF_SETTING_ID,
   SEVERITY
 )
 USING INDEX
 INITRANS 6
 TABLESPACE RETAIL_DATA
/


PROMPT Creating FK on 'RAF_NOTIF_SET_SEVERITIES'
 ALTER TABLE RAF_NOTIF_SET_SEVERITIES
  ADD CONSTRAINT RAF_NOTIF_SEVS_FK1
  FOREIGN KEY (NOTIF_SETTING_ID)
 REFERENCES RAF_NOTIF_USER_SETTINGS (NOTIF_SETTING_ID)
 ON DELETE CASCADE
/

