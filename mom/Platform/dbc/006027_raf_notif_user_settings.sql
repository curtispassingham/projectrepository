--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_NOTIF_USER_SETTINGS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_NOTIF_USER_SETTINGS'
CREATE TABLE RAF_NOTIF_USER_SETTINGS
 (NOTIF_SETTING_ID NUMBER(18) NOT NULL,
  USER_ID VARCHAR2(64 ) NOT NULL,
  APPLICATION_CODE VARCHAR2(50 ) NOT NULL,
  PERIOD_ID NUMBER(18),
  LIST_VIEW VARCHAR2(1 ),
  CREATED_BY VARCHAR2(64 ) NOT NULL,
  CREATE_DATE TIMESTAMP NOT NULL,
  LAST_UPDATED_BY VARCHAR2(64 ) NOT NULL,
  LAST_UPDATE_DATE TIMESTAMP NOT NULL,
  OBJECT_VERSION_NUMBER NUMBER(9) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_NOTIF_USER_SETTINGS is 'Table that holds the user Notification settings'
/

COMMENT ON COLUMN RAF_NOTIF_USER_SETTINGS.NOTIF_SETTING_ID is 'Unique ID identifying the Setting'
/

COMMENT ON COLUMN RAF_NOTIF_USER_SETTINGS.USER_ID is 'Username of the user'
/

COMMENT ON COLUMN RAF_NOTIF_USER_SETTINGS.APPLICATION_CODE is 'Application the Setting belongs to'
/

COMMENT ON COLUMN RAF_NOTIF_USER_SETTINGS.PERIOD_ID is 'Chosen Period in the filter'
/

COMMENT ON COLUMN RAF_NOTIF_USER_SETTINGS.LIST_VIEW is 'Chosen view on the Notification Sidebar'
/

COMMENT ON COLUMN RAF_NOTIF_USER_SETTINGS.CREATED_BY is 'Who column: indicates the user who created the row.'
/

COMMENT ON COLUMN RAF_NOTIF_USER_SETTINGS.CREATE_DATE is 'Who column: indicates the date and time of the creation of the row.'
/

COMMENT ON COLUMN RAF_NOTIF_USER_SETTINGS.LAST_UPDATED_BY is 'Who column: indicates the user who last updated the row.'
/

COMMENT ON COLUMN RAF_NOTIF_USER_SETTINGS.LAST_UPDATE_DATE is 'Who column: indicates the date and time of the last update of the row.'
/

COMMENT ON COLUMN RAF_NOTIF_USER_SETTINGS.OBJECT_VERSION_NUMBER is 'Who column: Object Version Number used for concurrency purposes'
/


PROMPT Creating Primary Key on 'RAF_NOTIF_USER_SETTINGS'
ALTER TABLE RAF_NOTIF_USER_SETTINGS
 ADD CONSTRAINT RAF_NOTIF_USR_PK1 PRIMARY KEY
  (NOTIF_SETTING_ID
 )
 USING INDEX
 INITRANS 6
 TABLESPACE RETAIL_DATA
/


PROMPT Creating FK on 'RAF_NOTIF_USER_SETTINGS'
 ALTER TABLE RAF_NOTIF_USER_SETTINGS
  ADD CONSTRAINT RAF_NOTIF_USR_SET_FK1
  FOREIGN KEY (PERIOD_ID)
 REFERENCES RAF_NOTIF_TIME_PERIODS_B (PERIOD_ID)
 ON DELETE CASCADE
/

