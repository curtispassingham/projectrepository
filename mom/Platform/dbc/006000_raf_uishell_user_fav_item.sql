--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_UISHELL_USER_FAV_ITEM
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_UISHELL_USER_FAV_ITEM'
CREATE TABLE RAF_UISHELL_USER_FAV_ITEM
 (FAV_ITEM_ID NUMBER(10) NOT NULL,
  APPLICATION_CODE VARCHAR2(50 CHAR),
  USER_ID VARCHAR2(30 CHAR),
  SEQUENCE_NUMBER NUMBER(5),
  REF_ITEM_ID VARCHAR2(300 CHAR),
  CREATED_BY VARCHAR2(64 CHAR),
  CREATE_DATE TIMESTAMP,
  LAST_UPDATED_BY VARCHAR2(64 CHAR),
  LAST_UPDATE_DATE TIMESTAMP,
  OBJECT_VERSION_NUMBER NUMBER(9)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_UISHELL_USER_FAV_ITEM is 'A table that holds items marked as favorites in the UIShell-based Retail Web Application.'
/

COMMENT ON COLUMN RAF_UISHELL_USER_FAV_ITEM.FAV_ITEM_ID is 'Unique row identifier. System generated via a database sequence.'
/

COMMENT ON COLUMN RAF_UISHELL_USER_FAV_ITEM.APPLICATION_CODE is 'The application for which the favorite item is found in.'
/

COMMENT ON COLUMN RAF_UISHELL_USER_FAV_ITEM.USER_ID is 'The user who owns the favorite item. Value in UPPER CASE.'
/

COMMENT ON COLUMN RAF_UISHELL_USER_FAV_ITEM.SEQUENCE_NUMBER is 'The order of the favorite item when all the items are shown in a list. Must be > 0 '
/

COMMENT ON COLUMN RAF_UISHELL_USER_FAV_ITEM.REF_ITEM_ID is 'A reference to the task or report item.'
/

COMMENT ON COLUMN RAF_UISHELL_USER_FAV_ITEM.CREATED_BY is 'The user who created the row'
/

COMMENT ON COLUMN RAF_UISHELL_USER_FAV_ITEM.CREATE_DATE is 'The date and time when the row was created'
/

COMMENT ON COLUMN RAF_UISHELL_USER_FAV_ITEM.LAST_UPDATED_BY is 'The user who last updated the row'
/

COMMENT ON COLUMN RAF_UISHELL_USER_FAV_ITEM.LAST_UPDATE_DATE is 'The date and time when the rows was updated'
/

COMMENT ON COLUMN RAF_UISHELL_USER_FAV_ITEM.OBJECT_VERSION_NUMBER is 'The Object version number'
/


PROMPT Creating Primary Key on 'RAF_UISHELL_USER_FAV_ITEM'
ALTER TABLE RAF_UISHELL_USER_FAV_ITEM
 ADD CONSTRAINT RAF_UISHELL_USER_FAV_ITEM_PK PRIMARY KEY
  (FAV_ITEM_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'RAF_UISHELL_USER_FAV_ITEM'
ALTER TABLE RAF_UISHELL_USER_FAV_ITEM
 ADD CONSTRAINT RAF_UISHELL_USER_FAV_ITEM_UK1 UNIQUE
  (APPLICATION_CODE,
   USER_ID,
   REF_ITEM_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

