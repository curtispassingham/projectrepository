--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RAF_INSTALLED_APPS'
ALTER TABLE RAF_INSTALLED_APPS MODIFY ROLE VARCHAR2(100) NULL
/

COMMENT ON COLUMN RAF_INSTALLED_APPS.ROLE is 'Role of the user logging in to the Installed Applications'
/

ALTER TABLE RAF_INSTALLED_APPS ADD SYSTEM_ENTRY VARCHAR2(1) DEFAULT 'N' NOT NULL
/

COMMENT ON COLUMN RAF_INSTALLED_APPS.SYSTEM_ENTRY is 'Column to indicate a system row. Will be used by  an application to locate the instance of another application for the purpose of in-context launching as well as application navigation'
/

PROMPT dropping PRIMARY KEY CONSTRAINT 'RAF_INSTALLED_APPS_PK'
DECLARE
  L_constarint_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constarint_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'RAF_INSTALLED_APPS_PK'
     AND constraint_TYPE = 'P';

  if (L_constarint_exists != 0) then
      execute immediate 'ALTER TABLE RAF_INSTALLED_APPS DROP CONSTRAINT RAF_INSTALLED_APPS_PK DROP INDEX';
  end if;
end;
/

PROMPT Creating Primary Key on 'RAF_INSTALLED_APPS_PK'
ALTER TABLE RAF_INSTALLED_APPS
 ADD CONSTRAINT RAF_INSTALLED_APPS_PK PRIMARY KEY
  (APPLICATION_ID,
   NAME,
   SYSTEM_ENTRY
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
