--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_NOTIF_HIER_VALUES
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_NOTIF_HIER_VALUES'
CREATE TABLE RAF_NOTIF_HIER_VALUES
 (RAF_NOTIFICATION_ID NUMBER(18) NOT NULL,
  HIER_LEVEL_ID NUMBER(18) NOT NULL,
  HIER_VALUE VARCHAR2(250 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_NOTIF_HIER_VALUES is 'Stores the hierarchial information for a Notification'
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.RAF_NOTIFICATION_ID is 'Unique ID identifying the Notification'
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_LEVEL_ID is 'Unique ID identifying the Hierarchy Level'
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_VALUE is 'String that holds the value of the intersection'
/


PROMPT Creating Primary Key on 'RAF_NOTIF_HIER_VALUES'
ALTER TABLE RAF_NOTIF_HIER_VALUES
 ADD CONSTRAINT RAF_NOTIF_HV_PK PRIMARY KEY
  (RAF_NOTIFICATION_ID,
   HIER_LEVEL_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'RAF_NOTIF_HIER_VALUES'
 ALTER TABLE RAF_NOTIF_HIER_VALUES
  ADD CONSTRAINT RAF_NOTIF_HV_FK1
  FOREIGN KEY (HIER_LEVEL_ID)
 REFERENCES RAF_NOTIF_HIER_LEVELS_B (HIER_LEVEL_ID)
 ON DELETE CASCADE
/


PROMPT Creating FK on 'RAF_NOTIF_HIER_VALUES'
 ALTER TABLE RAF_NOTIF_HIER_VALUES
  ADD CONSTRAINT RAF_NOTIF_HV_FK2
  FOREIGN KEY (RAF_NOTIFICATION_ID)
 REFERENCES RAF_NOTIFICATION (RAF_NOTIFICATION_ID)
 ON DELETE CASCADE
/

