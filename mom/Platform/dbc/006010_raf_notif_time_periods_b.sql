--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_NOTIF_TIME_PERIODS_B
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_NOTIF_TIME_PERIODS_B'
CREATE TABLE RAF_NOTIF_TIME_PERIODS_B
 (PERIOD_ID NUMBER(18) NOT NULL,
  START_DATE_OFFSET NUMBER(10),
  END_DATE_OFFSET NUMBER(10),
  APPLICATION_CODE VARCHAR2(50 ) NOT NULL,
  CREATED_BY VARCHAR2(64 ) NOT NULL,
  CREATE_DATE TIMESTAMP NOT NULL,
  LAST_UPDATED_BY VARCHAR2(64 ) NOT NULL,
  LAST_UPDATE_DATE TIMESTAMP NOT NULL,
  OBJECT_VERSION_NUMBER NUMBER(9) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_NOTIF_TIME_PERIODS_B is 'Holds generic time periods for an application'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_B.PERIOD_ID is 'Unique ID identifying that period'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_B.START_DATE_OFFSET is 'Number of days offset from SYSDATE for period start'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_B.END_DATE_OFFSET is 'Number of days offset from SYSDATE for period end'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_B.APPLICATION_CODE is 'Application ID '
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_B.CREATED_BY is 'Who column: indicates the user who created the row.'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_B.CREATE_DATE is 'Who column: indicates the date and time of the creation of the row.'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_B.LAST_UPDATED_BY is 'Who column: indicates the user who last updated the row.'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_B.LAST_UPDATE_DATE is 'Who column: indicates the date and time of the last update of the row.'
/

COMMENT ON COLUMN RAF_NOTIF_TIME_PERIODS_B.OBJECT_VERSION_NUMBER is 'Who column: Object Version Number used for concurrency purposes'
/

PROMPT Creating Index on 'RAF_NOTIF_TIME_PERIODS_B'
 CREATE INDEX RAF_NOTIF_TP_B_I1 on RAF_NOTIF_TIME_PERIODS_B
 (PERIOD_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

PROMPT Creating Primary Key on 'RAF_NOTIF_TIME_PERIODS_B'
ALTER TABLE RAF_NOTIF_TIME_PERIODS_B
 ADD CONSTRAINT RAF_NOTIF_TP_B_PK PRIMARY KEY
  (PERIOD_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
