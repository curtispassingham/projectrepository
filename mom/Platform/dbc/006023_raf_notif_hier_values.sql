--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT Modifying Primary Key on 'RAF_NOTIF_HIER_VALUES'
ALTER TABLE RAF_NOTIF_HIER_VALUES
 DROP CONSTRAINT RAF_NOTIF_HV_PK DROP INDEX
/

PROMPT Dropping Foreign Key on 'RAF_NOTIF_HIER_VALUES'
ALTER TABLE RAF_NOTIF_HIER_VALUES
 DROP CONSTRAINT RAF_NOTIF_HV_FK1
/

PROMPT Modifying Table 'RAF_NOTIF_HIER_VALUES'
ALTER TABLE RAF_NOTIF_HIER_VALUES DROP COLUMN HIER_LEVEL_ID
/

ALTER TABLE RAF_NOTIF_HIER_VALUES DROP COLUMN HIER_VALUE
/

ALTER TABLE RAF_NOTIF_HIER_VALUES ADD HIER_VALUE_1 VARCHAR2 (250 ) NULL
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_VALUE_1 is 'String that holds the value of the intersection'
/

ALTER TABLE RAF_NOTIF_HIER_VALUES ADD HIER_VALUE_2 VARCHAR2 (250 ) NULL
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_VALUE_2 is 'String that holds the value of the intersection'
/

ALTER TABLE RAF_NOTIF_HIER_VALUES ADD HIER_VALUE_3 VARCHAR2 (250 ) NULL
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_VALUE_3 is 'String that holds the value of the intersection'
/

ALTER TABLE RAF_NOTIF_HIER_VALUES ADD HIER_VALUE_4 VARCHAR2 (250 ) NULL
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_VALUE_4 is 'String that holds the value of the intersection'
/

ALTER TABLE RAF_NOTIF_HIER_VALUES ADD HIER_VALUE_5 VARCHAR2 (250 ) NULL
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_VALUE_5 is 'String that holds the value of the intersection'
/

ALTER TABLE RAF_NOTIF_HIER_VALUES ADD HIER_VALUE_6 VARCHAR2 (250 ) NULL
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_VALUE_6 is 'String that holds the value of the intersection'
/

ALTER TABLE RAF_NOTIF_HIER_VALUES ADD HIER_VALUE_7 VARCHAR2 (250 ) NULL
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_VALUE_7 is 'String that holds the value of the intersection'
/

ALTER TABLE RAF_NOTIF_HIER_VALUES ADD HIER_VALUE_8 VARCHAR2 (250 ) NULL
/

COMMENT ON COLUMN RAF_NOTIF_HIER_VALUES.HIER_VALUE_8 is 'String that holds the value of the intersection'
/

ALTER TABLE RAF_NOTIF_HIER_VALUES
 ADD CONSTRAINT RAF_NOTIF_HV_PK PRIMARY KEY
  (RAF_NOTIFICATION_ID
 )
 USING INDEX
 INITRANS 6
 TABLESPACE RETAIL_DATA
/
