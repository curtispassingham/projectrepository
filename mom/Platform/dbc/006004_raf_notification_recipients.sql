--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RAF_NOTIFICATION_RECIPIENTS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RAF_NOTIFICATION_RECIPIENTS'
CREATE TABLE RAF_NOTIFICATION_RECIPIENTS
 (RAF_NOTIFICATION_ID NUMBER(18) NOT NULL,
  RECIPIENT_ID VARCHAR2(64 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RAF_NOTIFICATION_RECIPIENTS is 'This table the IDs of the recipients of each Notification'
/

COMMENT ON COLUMN RAF_NOTIFICATION_RECIPIENTS.RAF_NOTIFICATION_ID is 'ID of the Notification'
/

COMMENT ON COLUMN RAF_NOTIFICATION_RECIPIENTS.RECIPIENT_ID is 'Username of the Recipient'
/


PROMPT Creating Primary Key on 'RAF_NOTIFICATION_RECIPIENTS'
ALTER TABLE RAF_NOTIFICATION_RECIPIENTS
 ADD CONSTRAINT RAF_NOTIF_RCPTS_PK PRIMARY KEY
  (RAF_NOTIFICATION_ID,
   RECIPIENT_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'RAF_NOTIFICATION_RECIPIENTS'
 ALTER TABLE RAF_NOTIFICATION_RECIPIENTS
  ADD CONSTRAINT RAF_NOTIF_RCPTS_FK1
  FOREIGN KEY (RAF_NOTIFICATION_ID)
 REFERENCES RAF_NOTIFICATION (RAF_NOTIFICATION_ID)
 ON DELETE CASCADE
/

