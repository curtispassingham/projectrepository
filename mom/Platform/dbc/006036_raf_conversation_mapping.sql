--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RAF_CONVERSATION_MAPPING'
ALTER TABLE RAF_CONVERSATION_MAPPING MODIFY CREATED_BY_TASKFLOW VARCHAR2 (300 )
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CREATED_BY_TASKFLOW is 'Who column: indicates the taskflow that created this row.'
/

ALTER TABLE RAF_CONVERSATION_MAPPING MODIFY CONVERSATION_FLAVOR VARCHAR2 (150 )
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CONVERSATION_FLAVOR is 'Name of the provider class which accesses the backend framework to get this conversation.'
/

ALTER TABLE RAF_CONVERSATION_MAPPING RENAME COLUMN CONVERSATION_FLAVOR to CONVERSATION_PROVIDER
/

COMMENT ON COLUMN RAF_CONVERSATION_MAPPING.CONVERSATION_PROVIDER is 'Name of the provider class which accesses the backend framework to get this conversation.'
/
