 
 DECLARE
  v_count NUMBER := 0;
BEGIN
DBMS_OUTPUT.PUT_LINE('Inserting DEFAULT Notification Translations ');

SELECT COUNT(*) INTO v_count FROM RAF_NOTIFICATION_TYPE_VL WHERE NAME='Reassign Notification' AND NOTIFICATION_TYPE_CODE=NAME AND APPLICATION_CODE='NOTIFICATION_SYSTEM';
if(v_count = 0 ) then
	DBMS_OUTPUT.PUT_LINE('Inserting Generic Notification Types as its missing');
 
 Insert into RAF_NOTIFICATION_TYPE_B (NOTIFICATION_TYPE,NOTIFICATION_TYPE_CODE,RETENTION_DAYS,APPLICATION_CODE,CREATED_BY,CREATE_DATE,LAST_UPDATE_DATE,LAST_UPDATED_BY,LAST_UPDATE_LOGIN) values (RAF_NOTIFICATION_TYPE_SEQ.NEXTVAL,'Reassign Notification',-1,'NOTIFICATION_SYSTEM','Notification Sys', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'Notification Sys','Notification Sys');

 Insert into RAF_NOTIFICATION_TYPE_TL (NOTIFICATION_TYPE,LANGUAGE,NAME,DESCRIPTION,SOURCE_LANG,CREATE_DATE,CREATED_BY,LAST_UPDATE_DATE,LAST_UPDATED_BY,LAST_UPDATE_LOGIN) values (RAF_NOTIFICATION_TYPE_SEQ.CURRVAL,'US','Reassign Notification','Reassigned', 'US',CURRENT_TIMESTAMP,'Notification Sys',CURRENT_TIMESTAMP,'Notification Sys','Notification Sys');

 end if;
 
 UPDATE RAF_NOTIFICATION_TYPE_TL SET DESCRIPTION = 'General' WHERE  NAME = 'All Notifications' AND LANGUAGE='US';
 
COMMIT;
DBMS_OUTPUT.PUT_LINE('Completed inserting Translations ');
END;
/
 
 
 
