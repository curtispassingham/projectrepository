PROMPT INSTALLING ALL LANGUAGES;

UPDATE RTC_LANGUAGES_B SET INSTALLED_FLAG='B' WHERE LANGUAGE_TAG = 'en';

UPDATE RTC_LANGUAGES_B SET INSTALLED_FLAG='I' WHERE LANGUAGE_TAG IN ('ar','tr','sv','es','ru','pt','pl','ko','ja','it','hu','el','de','fr','hr','zh-TW','zh-CN','nl');

commit;
