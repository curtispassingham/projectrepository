PROMPT CREATING BASEMODEL RAF_BOOLEAN_WRAPPER_PKG;

CREATE OR REPLACE package body RAF_BOOLEAN_WRAPPER_PKG as

	function INT2BOOLEAN(param INT) return BOOLEAN is
	begin
		return case param 
                      when 1 then true 
                      when 0 then false 
                      else false 
			   end;
	end;
	
	function BOOLEAN2INT(param BOOLEAN) return INT is
	begin
		return case param 
                      when true then 1 
                      when false then 0 
                      else 0
			    end;
	end;

end;
/

