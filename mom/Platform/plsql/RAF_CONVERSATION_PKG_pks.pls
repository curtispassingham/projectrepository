CREATE OR REPLACE PACKAGE RAF_CONVERSATION_PKG AUTHID CURRENT_USER AS

------------------------------------------------------------------------------------------------
-- This function allows you the Delete a Conversation Mapping 
-- To use this function you must provide SOCIAL_OBJECT_ID and SOCIAL_OBJECT_TYPE
-- This functional will delete all conversation mappings associated with social object id as well as social object type

FUNCTION DELETE_CONVERSATION_MAPPINGS(O_error_message OUT  VARCHAR2,IN_SOCIAL_OBJECT_ID IN VARCHAR2,IN_SOCIAL_OBJECT_TYPE IN VARCHAR2) RETURN NUMBER;
FUNCTION DELETE_CONVERSATION_MAPPING(O_error_message OUT  VARCHAR2,IN_SOCIAL_OBJECT_ID IN VARCHAR2,IN_SOCIAL_OBJECT_TYPE IN VARCHAR2,IN_CONVERSATION_PROVIDER IN VARCHAR2,IN_CONVERSATION_ID IN VARCHAR2) RETURN NUMBER;

------------------------------------------------------------------------------------------------

END RAF_CONVERSATION_PKG;
/