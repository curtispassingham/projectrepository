create or replace PACKAGE BODY RAF_FACETED_FILTER_PKG AS

--Below RUN_SEARCH will be DEPRECATED from 4.0.4.

    PROCEDURE RUN_SEARCH ( IN_SEARCH_TEXT IN VARCHAR2,
                        IN_FACET_ATTR_CFG_ID IN NUMBER,
                        IN_FACET_SESSION_ID IN VARCHAR2,
                        IN_SEARCH_TYPE IN VARCHAR2,
                        IN_DEBUG_IND IN NUMBER DEFAULT 0,
                        OUT_OUTPUT_MSG OUT VARCHAR2) IS
        O_error_message VARCHAR2(1000);
        v_ouput VARCHAR2(2000);
    BEGIN
        v_ouput := RAF_FACETED_FILTER_PKG.RUN_FACET_ATTRIBUTE_SEARCH(O_error_message,
                                                                IN_SEARCH_TEXT,
                                                                IN_FACET_ATTR_CFG_ID,
                                                                IN_FACET_SESSION_ID,
                                                                IN_SEARCH_TYPE,
                                                                IN_DEBUG_IND,
                                                                OUT_OUTPUT_MSG);
    END RUN_SEARCH;

-- End package body
-----------------------------------------------------------------------------------------------

 FUNCTION RUN_FACET_ATTRIBUTE_SEARCH (
            O_error_message OUT  VARCHAR2,
            IN_SEARCH_TEXT IN VARCHAR2,
            IN_FACET_ATTR_CFG_ID IN NUMBER,
            IN_FACET_SESSION_ID IN VARCHAR2,
            IN_SEARCH_TYPE IN VARCHAR2,
            IN_DEBUG_IND IN NUMBER DEFAULT 0,
            OUT_OUTPUT_MSG OUT VARCHAR2
            ) RETURN NUMBER AS
    dynamic_sql_var VARCHAR2(10000);
    search_text_uppr_var VARCHAR2(1000);
BEGIN
     IF ((IN_SEARCH_TEXT IS NOT NULL)
          AND (LENGTH(IN_SEARCH_TEXT) > 0)
          AND (IN_FACET_ATTR_CFG_ID IS NOT NULL)
          AND (IN_FACET_SESSION_ID IS NOT NULL)) THEN

          -- To make the search case insensitive
        search_text_uppr_var := UPPER(IN_SEARCH_TEXT);

        -- Clear out the search matched indicator for the current session and facet configuration id
        -- before running a new search
        UPDATE raf_facet_session_attribute
        SET search_matched_ind = NULL
        WHERE facet_attribute_cfg_id = IN_FACET_ATTR_CFG_ID
        and facet_session_id = IN_FACET_SESSION_ID;

        IF (IN_DEBUG_IND = 1)
        THEN
            DBMS_OUTPUT.PUT_LINE('RAF_FACETED_FILTER_PKG.RUN_SEARCH(): column search_matched_ind cleared on the table ''raf_facet_session_attribute'' for ''facet_attribute_cfg_id'' = ' || IN_FACET_ATTR_CFG_ID || ' ''facet_session_id'' = ' || IN_FACET_SESSION_ID);
            OUT_OUTPUT_MSG := OUT_OUTPUT_MSG || 'RAF_FACETED_FILTER_PKG.RUN_SEARCH(): column search_matched_ind cleared on the table ''raf_facet_session_attribute'' for ''facet_attribute_cfg_id'' = ' || IN_FACET_ATTR_CFG_ID || ' ''facet_session_id'' = ' || IN_FACET_SESSION_ID;
        END IF;

        -- Now run the search and update the search matched indicator for the search string passed
        dynamic_sql_var := 'UPDATE raf_facet_session_attribute ';
        dynamic_sql_var := dynamic_sql_var || 'SET search_matched_ind = 1 ';
        dynamic_sql_var := dynamic_sql_var || 'WHERE facet_attribute_cfg_id = :param_facet_attr_cfg_id ';
        dynamic_sql_var := dynamic_sql_var || 'and facet_session_id = :param_facet_session_id ';

        IF (IN_SEARCH_TYPE = 'CONTAIN')
        THEN dynamic_sql_var := dynamic_sql_var || 'and UPPER(value || '' - '' || value_desc) like ''%'' || :search_text_uppr_var || ''%'' ';
        ELSIF (IN_SEARCH_TYPE = 'START')
        THEN dynamic_sql_var := dynamic_sql_var || 'and UPPER(value || '' - '' || value_desc) like :search_text_uppr_var || ''%'' ';
        ELSIF (IN_SEARCH_TYPE = 'END')
        THEN dynamic_sql_var := dynamic_sql_var || 'and UPPER(value || '' - '' || value_desc) like ''%'' || :search_text_uppr_var';
        END IF;

        IF (IN_DEBUG_IND = 1)
        THEN
            DBMS_OUTPUT.PUT_LINE('Dynamic SQL = ' || dynamic_sql_var);
        END IF;

        -- Execute the dynamic SQL
        IF (IN_SEARCH_TYPE = 'CONTAIN' OR IN_SEARCH_TYPE = 'START' OR IN_SEARCH_TYPE = 'END')
        THEN EXECUTE IMMEDIATE dynamic_sql_var USING IN_FACET_ATTR_CFG_ID,IN_FACET_SESSION_ID, search_text_uppr_var;
        ELSE EXECUTE IMMEDIATE dynamic_sql_var USING IN_FACET_ATTR_CFG_ID,IN_FACET_SESSION_ID;
        END IF;

        IF (IN_DEBUG_IND = 1)
        THEN
            DBMS_OUTPUT.PUT_LINE('RAF_FACETED_FILTER_PKG.RUN_SEARCH(): column search_matched_ind set on the table ''raf_facet_session_attribute'' for ''facet_attribute_cfg_id'' = ' || IN_FACET_ATTR_CFG_ID || ' and ''facet_session_id'' = ' || IN_FACET_SESSION_ID || ' and ''search text'' = ' || search_text_uppr_var);
            OUT_OUTPUT_MSG := OUT_OUTPUT_MSG || ' :RAF_FACETED_FILTER_PKG.RUN_SEARCH(): column search_matched_ind set on the table ''raf_facet_session_attribute'' for ''facet_attribute_cfg_id'' = ' || IN_FACET_ATTR_CFG_ID || ' and ''facet_session_id'' = ' || IN_FACET_SESSION_ID || ' and ''search text'' = ' || search_text_uppr_var;
        END IF;

        RETURN 1;
    END IF; -- End of the outer most If
EXCEPTION
WHEN others then
    O_error_message := RAF_SQL_LIB.RAF_CREATE_MSG(SQLERRM,
                                     'RAF_FACETED_FILTER_PKG RUN_SEARCH FAILED',
                                     to_char(SQLCODE));
    ROLLBACK;

    RETURN 0;

-- End Function
END RUN_FACET_ATTRIBUTE_SEARCH;

END;
/