PROMPT CREATING BASEMODEL RAF_SQL_LIB;

CREATE OR REPLACE PACKAGE BODY RAF_SQL_LIB AS
 

----------------------------------------------------------------------------
/* Public Functions and procedure Bodies*/
-------------------------------------------------------------------------

FUNCTION RAF_CREATE_MSG(I_key   IN varchar2,
                    I_txt_1 IN varchar2 := NULL,
                    I_txt_2 IN varchar2 := NULL) RETURN VARCHAR2 IS

   L_merged_text  VARCHAR2 (4000);
   L_total_length NUMBER := 0;

BEGIN

   --- make sure the msg will fit
   if I_key is NULL then
      return NULL;
   else
      L_total_length := L_total_length + lengthb (I_key) + 2;
   end if;

   if I_txt_1 is not NULL then
      L_total_length := L_total_length + lengthb (I_txt_1) + 2;
   end if;

   if I_txt_2 is not NULL then
      L_total_length := L_total_length + lengthb (I_txt_2) + 2;
   end if;

   if L_total_length > 255 then
      return (RTRIM(substrb(I_key||':'||I_txt_1||':'||I_txt_2, 1, 255)));
   end if;


   --- create message
   L_merged_text := '@0' || I_key;

   if I_txt_1 is not NULL then
      L_merged_text := L_merged_text || '@1' || I_txt_1;
   end if;


   if I_key != 'PACKAGE_ERROR' and I_txt_2 is not NULL then
      L_merged_text := L_merged_text || '@2' || I_txt_2;
   end if;

   return (L_merged_text);

END;

END RAF_SQL_LIB;
/