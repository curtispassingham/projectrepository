CREATE OR REPLACE PACKAGE RAF_ASYNC_TASK_PKG AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------
/*To support old and new PL/SQL procedures untill all consumer apps migrate their latest changes using both functions,
  in the data base, but soon below 2 functions will be deprecated.
*/
---------------------------------------------------------------------------------------
-- @DEPRECATED since 4.0.3.    
 FUNCTION UPDATE_ASYNC_TASK_AUTOTRAN(
        IN_ASYNC_TASK_ID IN NUMBER,
        IN_STATUS        IN VARCHAR2,
        IN_PROCESS_START_TIMESTAMP IN TIMESTAMP,
        IN_PROCESS_END_TIMESTAMP IN TIMESTAMP,
        IN_ERROR_TEXT IN VARCHAR2,
        IN_LAST_UPDATE_DATE IN TIMESTAMP,        
        IN_USER_ID IN VARCHAR2,
        IN_OBJ_VER_NUM IN NUMBER
    ) RETURN NUMBER;
	
----------------------------------------------------------------------------	
-- @DEPRECATED since 4.0.3.    
FUNCTION DELETE_ASYNC_TASK(
		IN_NUMBER_OF_DAYS   IN NUMBER
    ) RETURN NUMBER;

-----------------------------------------------------------------------------
-- Use UPDATE_ASYNC_TASK_AUTOTRAN with new O_error_message parameter.
   
    FUNCTION UPDATE_ASYNC_TASK_AUTOTRAN(
        O_error_message      OUT  VARCHAR2,
        IN_ASYNC_TASK_ID IN NUMBER,
        IN_STATUS        IN VARCHAR2,
        IN_PROCESS_START_TIMESTAMP IN TIMESTAMP,
        IN_PROCESS_END_TIMESTAMP IN TIMESTAMP,
        IN_ERROR_TEXT IN VARCHAR2,
        IN_LAST_UPDATE_DATE IN TIMESTAMP,           
        IN_USER_ID IN VARCHAR2,
        IN_OBJ_VER_NUM IN NUMBER
    ) RETURN NUMBER;
    
-------------------------------------------------------------------------------   
-- Use DELETE_ASYNC_TASK with new O_error_message parameter.
 FUNCTION DELETE_ASYNC_TASK(
        O_error_message      OUT  VARCHAR2,
        IN_NUMBER_OF_DAYS   IN NUMBER
    ) RETURN NUMBER;

END RAF_ASYNC_TASK_PKG;
/