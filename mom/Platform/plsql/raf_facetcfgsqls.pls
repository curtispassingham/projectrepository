create or replace
PACKAGE RAF_FACET_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------------------------------
-- Function   : FILTER_SOURCE
-- Purpose    : Performs the query applying/unapplying facets as filters.
--  			I_FACET_SESSION_ID : This is a session Id which would be used for creating results for a specific Session.
--				I_FACET_SOURCE_QUERY : This can be used by Application for Executing a specific Query with in the PL/SQL
--				I_QUERY_NAME : Refers to the Entity name which can queried to get the Package Name.
--				I_DISABLED_FACET_ATTRIBUTES : Collection of Disabled Facets which would be used for Filtering ResultSet.
---------------------------------------------------------------------------------------------------------------------
FUNCTION FILTER_SOURCE(O_ERROR_MESSAGE   IN OUT VARCHAR2,
					   I_FACET_SESSION_ID IN VARCHAR2,
					   I_FACET_SOURCE_QUERY IN VARCHAR2,
					   I_QUERY_NAME IN VARCHAR2,
					   I_DISABLED_FACET_ATTRIBUTES IN DISABLED_FACET_ATTRIBUTES_TBL) 
					   RETURN NUMBER ;
	
---------------------------------------------------------------------------------------------------------------------
-- Function   : FILTER_SOURCE
-- Purpose    : Ends the use of facet framework for a particular session.
--				Returns 0 success, 1 failed.
--				I_FACET_SESSION_ID : The Session ID is used to remove the Facet related sessions table.	
--				I_QUERY_NAME : Refers to the Entity name which can queried to get the Package Name. 
---------------------------------------------------------------------------------------------------------------------
FUNCTION CLOSE_FACET_SESSION (O_ERROR_MESSAGE   IN OUT VARCHAR2,
							  I_FACET_SESSION_ID IN VARCHAR2,
							  I_QUERY_NAME IN VARCHAR2) 
							  RETURN NUMBER;

END;
/