create or replace
PACKAGE BODY RAF_FACET_SQL is

---------------------------------------------------------------------------------------------------------------------
-- Function   : FILTER_SOURCE
-- Purpose    : Performs the query applying/unapplying facets as filters.
--              O_ERROR_MESSAGE : Error Message 
--  			I_FACET_SESSION_ID : This is a session Id which would be used for creating results for a specific Session.
--				I_FACET_SOURCE_QUERY : This can be used by Application for Executing a specific Query with in the PL/SQL
--				I_QUERY_NAME : Refers to the Entity name which can queried to get the Package Name.
--				I_DISABLED_FACET_ATTRIBUTES : Collection of Disabled Facets which would be used for Filtering ResultSet.
---------------------------------------------------------------------------------------------------------------------
FUNCTION FILTER_SOURCE(O_ERROR_MESSAGE   IN OUT VARCHAR2,
					   I_FACET_SESSION_ID IN VARCHAR2,
					   I_FACET_SOURCE_QUERY IN VARCHAR2,
					   I_QUERY_NAME IN VARCHAR2,
					   I_DISABLED_FACET_ATTRIBUTES IN DISABLED_FACET_ATTRIBUTES_TBL) RETURN NUMBER  IS

	function_name  VARCHAR2(255)  := NULL;
	function_call  VARCHAR2(255)  := NULL;
	query_name_var  VARCHAR2(255)  := NULL;
  L_error_message VARCHAR2(255) := NULL;
  L_result        NUMBER        := NULL;

BEGIN

	query_name_var :=I_QUERY_NAME;
	select cfg.FACET_CFG_FUNCTION into function_name from RAF_FACET_CFG cfg where cfg.QUERY_NAME=query_name_var;
  
	function_call := 'declare result number; begin result :=' || function_name ||'.FILTER_SOURCE(:v1, :v2, :v3, :v4, :v5); end;';

	execute immediate  (function_call) USING IN OUT L_ERROR_MESSAGE,I_FACET_SESSION_ID,I_FACET_SOURCE_QUERY,I_QUERY_NAME,I_DISABLED_FACET_ATTRIBUTES;
  O_error_message := L_error_message;
  return 1;     
	
END FILTER_SOURCE;
	
	
---------------------------------------------------------------------------------------------------------------------
-- Function   : FILTER_SOURCE
-- Purpose    : Ends the use of facet framework for a particular session.
--				Returns 0 success, 1 failed.
--              O_ERROR_MESSAGE : Error Message
--				I_FACET_SESSION_ID : The Session ID is used to remove the Facet related sessions table.	
--				I_QUERY_NAME : Refers to the Entity name which can queried to get the Package Name. 
---------------------------------------------------------------------------------------------------------------------
FUNCTION CLOSE_FACET_SESSION (O_ERROR_MESSAGE   IN OUT VARCHAR2,
							  I_FACET_SESSION_ID IN VARCHAR2,
							  I_QUERY_NAME IN VARCHAR2)
RETURN NUMBER IS

	facet_session_id_var  VARCHAR2(50) := NULL;
	function_name  VARCHAR2(255)  := NULL;
	function_call  VARCHAR2(255)  := NULL;
	query_name_var  VARCHAR2(255)  := NULL; BEGIN

	facet_session_id_var := I_FACET_SESSION_ID;
	query_name_var :=I_QUERY_NAME;
	select cfg.FACET_CFG_FUNCTION into function_name from RAF_FACET_CFG cfg where cfg.QUERY_NAME=query_name_var;
  
	function_call := 'declare  result number; begin result :=' || function_name ||'.CLOSE_FACET_SESSION(:v1,:v2); end;';

	execute immediate (function_call) USING IN OUT O_ERROR_MESSAGE,I_FACET_SESSION_ID;

--  Delete the session from RAF tables.

	delete from raf_facet_session_attribute where facet_session_id=facet_session_id_var;
	delete from raf_facet_session where facet_session_id=facet_session_id_var;
	return 0;
END CLOSE_FACET_SESSION;	

END;
/
