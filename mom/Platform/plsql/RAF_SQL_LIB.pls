create or replace PACKAGE RAF_SQL_LIB AUTHID CURRENT_USER AS
---------------------------------------------------------------------
-- Create message concatenates the message key and parameters
-- to pass the string back to be decoded by emessage. Create
-- message looks like emessage, except all four parameters must
-- be passed even if the message key accepts fewer than three
-- parameters. In this case NULL parameters must be passed in the
-- unused parameter positions. The NULL defaulting implemented
-- here should allow the user to not have to pass NULLs, but an
-- Oracle bug seems to prevent that from working.
--
-- When create message is used in the OTHERS section of stored
-- procedure exceptions handling it should be called as:
-- O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM,
-- 'hard-code program.function name', SQLCODE). Create message
-- stores the SQLCODE and function name for later retrieval by
-- the batch message program discussed below.
---------------------------------------------------------------------

FUNCTION RAF_CREATE_MSG(I_key IN varchar2,
         I_txt_1 IN varchar2 := null,
         I_txt_2 IN varchar2 := null) RETURN VARCHAR2;

END RAF_SQL_LIB;
/