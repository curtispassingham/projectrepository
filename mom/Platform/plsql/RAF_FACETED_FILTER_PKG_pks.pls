create or replace PACKAGE RAF_FACETED_FILTER_PKG AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------------
/*To support old and new PL/SQL procedures untill all consumer apps migrate their latest changes using both functions,
  in the data base, but soon below  procedure will be deprecated.
*/
---------------------------------------------------------------------------------------

--Below RUN_SEARCH will be DEPRECATED from 4.0.4.

  PROCEDURE RUN_SEARCH ( IN_SEARCH_TEXT IN VARCHAR2,
                          IN_FACET_ATTR_CFG_ID IN NUMBER,
                          IN_FACET_SESSION_ID IN VARCHAR2,
                          IN_SEARCH_TYPE IN VARCHAR2,
                          IN_DEBUG_IND IN NUMBER DEFAULT 0,
                          OUT_OUTPUT_MSG OUT VARCHAR2);

----------------------------------------------------------------------------

   FUNCTION RUN_FACET_ATTRIBUTE_SEARCH (
            O_error_message OUT  VARCHAR2,
            IN_SEARCH_TEXT IN VARCHAR2,
            IN_FACET_ATTR_CFG_ID IN NUMBER,
            IN_FACET_SESSION_ID IN VARCHAR2,
            IN_SEARCH_TYPE IN VARCHAR2,
            IN_DEBUG_IND IN NUMBER DEFAULT 0,
            OUT_OUTPUT_MSG OUT VARCHAR2
            )RETURN NUMBER ;

-------------------------------------------------------------------------------
End RAF_FACETED_FILTER_PKG;
/

