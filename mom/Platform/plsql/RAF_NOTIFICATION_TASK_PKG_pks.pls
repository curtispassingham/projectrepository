CREATE OR REPLACE PACKAGE RAF_NOTIFICATION_TASK_PKG AUTHID CURRENT_USER AS

------------------------------------------------------------------------------------------------
-- DELETE FUNCTIONS
------------------------------------------------------------------------------------------------

-- *******************************
-- OVERVIEW ( NOT RECOMMENDED )
-- *******************************
-- This function deletes Notifications. It deletes all Notifications that are older than the specified number of Days. 
-- This function is outdated as it doesnt take into consideration the Application Code for the Notifications to be deleted and it may potentially delete Notifications across different applications.
-- It is NOT RECOMMENDED to continue using this function as this is DEPRECATED and may be removed in future releases. 
-- *******************************
-- PARAMETER DESCRIPTION 
-- *******************************
-- O_error_message : Will contain an error code if this PL-SQL function fails
-- IN_NUMBER_OF_DAYS : Max permissible age of a Notification in days. Notifications older than this must be deleted.

	FUNCTION DELETE_NOTIFICATION_TASK(
        O_error_message     OUT  VARCHAR2,
        IN_NUMBER_OF_DAYS   IN NUMBER
    ) 
	RETURN NUMBER ;

------------------------------------------------------------------------------------------------

-- *******************************
-- OVERVIEW ( RECOMMENDED )
-- *******************************
-- This function deletes Notifications. Notifications that have exceeded their retention duration are purged. 
-- A retention limit is imposed on a Notification by virtue of the Notification Type it is created against. See column RETENTION_DAYS in table RAF_NOTIFICATION_TYPE_B
-- This is the RECOMMENDED approach to purge Notifications.
-- *******************************
-- PARAMETER DESCRIPTION 
-- *******************************
-- O_error_message : Will contain an error code if this PL-SQL function fails
-- IN_APP_CODE : Application Code for which Notifications need to be purged.

	FUNCTION DEL_NOTIF_PAST_RETENTION(
		O_error_message      OUT  VARCHAR2,
		IN_APP_CODE 		 IN VARCHAR2
    ) 
	RETURN NUMBER ;
        
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------
-- CREATE FUNCTIONS
------------------------------------------------------------------------------------------------

-- *******************************
-- OVERVIEW ( NOT RECOMMENDED )
-- *******************************
-- This function creates a Notification. It uses a Notification Type ID to create the Notification.
-- This function is outdated as it doesnt cater to the newer aspects of Notifications (5.0.4 onwards) and has only been retained for backward-compatibility purposes.
-- It is NOT RECOMMENDED to continue using this function as this is DEPRECATED and may be removed in future releases. 
-- *******************************
-- PARAMETER DESCRIPTION 
-- *******************************
-- O_error_message : Will contain an error code if this PL-SQL function fails
-- IN_NOTIFICATION_ID : Unique ID identifying the Notification. Use latest value from Sequence RAF_NOTIFICATION_SEQ 
-- IN_NOTIFICATION_CONTEXT : Pipe-delimited string of key value pairs. At a minimum provide the title and url attributes. Example : 'title=Test Tab|url=/WEB-INF/TestNotificationLaunchFlow.xml#TestNotificationLaunchFlow'
-- IN_NOTIFICATION_TYPE : ID of an existing Notification Type. See table RAF_NOTIFICATION_TYPE_B. 
-- IN_SEVERITY : Severity of the Notification. Default value of Low (1-Critical, 2-Important, 3-Normal)
-- IN_APPLICATION_CODE : Application to which the Notification belongs. This must match the Application Code of the Notification Type passed in. See column APPLICATION_CODE in table RAF_NOTIFICATION_TYPE_B.
-- IN_NOTIFICATION_DESC : Description of the Notification.
-- IN_NOTIFICATION_ACTIVE : Status of the Notification. Valid values are 'U'(UNREAD) or 'R'(READ)
-- IN_NOTIF_COMMAND_CLS_NAME : Deprecated parameter. Pass null.
-- IN_CREATE_DATE : Creation Date
-- IN_RECIPIENT : Recipient assigned to this Notification
-- IN_USER_ID : Created by
-- IN_LAUNCHABLE : Indicates if the Notification can be launched. Valid values are Y or N. Default value of N
-- IN_OBJ_VER_NUM : Object version number. Pass in any number here.

	FUNCTION CREATE_NOTIFY_TASK_AUTOTRAN(
        O_error_message      OUT  VARCHAR2,
        IN_NOTIFICATION_ID   IN NUMBER,
        IN_NOTIFICATION_CONTEXT   IN VARCHAR2,
        IN_NOTIFICATION_TYPE IN NUMBER,
        IN_SEVERITY IN NUMBER,
        IN_APPLICATION_CODE IN VARCHAR2,
        IN_NOTIFICATION_DESC IN VARCHAR2,
        IN_NOTIFICATION_ACTIVE IN VARCHAR2,
        IN_NOTIF_COMMAND_CLS_NAME IN VARCHAR2,
        IN_CREATE_DATE IN TIMESTAMP,
        IN_RECIPIENT IN VARCHAR2,
        IN_USER_ID IN VARCHAR2,
        IN_LAUNCHABLE IN VARCHAR2,
        IN_OBJ_VER_NUM IN NUMBER
    ) 
	RETURN NUMBER ;

------------------------------------------------------------------------------------------------

-- *******************************
-- OVERVIEW ( NOT RECOMMENDED )
-- *******************************
-- This function creates a Notification. To use this function you must be aware of the Notification Type Code that will be used to fetch a Notification Type ID during creation.
-- This function is outdated as it doesnt cater to the newer aspects of Notifications (5.0.4 onwards) and has only been retained for backward-compatibility purposes.
-- It is NOT RECOMMENDED to continue using this function as this is DEPRECATED and may be removed in future releases. 
-- *******************************
-- PARAMETER DESCRIPTION 
-- *******************************
-- O_error_message : Will contain an error code if this PL-SQL function fails
-- IN_NOTIFICATION_ID : Unique ID identifying the Notification. Use latest value from Sequence RAF_NOTIFICATION_SEQ 
-- IN_NOTIFICATION_CONTEXT : Pipe-delimited string of key value pairs. At a minimum provide the title and url attributes. Example : 'title=Test Tab|url=/WEB-INF/TestNotificationLaunchFlow.xml#TestNotificationLaunchFlow'
-- IN_NOTIFICATION_TYPE_CODE : Type Code of an existing Notification Type. See table RAF_NOTIFICATION_TYPE_B. 
-- IN_SEVERITY : Severity of the Notification. Default value of Low (1-Critical, 2-Important, 3-Normal)
-- IN_APPLICATION_CODE : Application to which the Notification belongs. This must match the Application Code of the Notification Type passed in. See column APPLICATION_CODE in table RAF_NOTIFICATION_TYPE_B.
-- IN_NOTIFICATION_DESC : Description of the Notification.
-- IN_NOTIFICATION_ACTIVE : Status of the Notification. Valid values are 'U'(UNREAD) or 'R'(READ)
-- IN_NOTIF_COMMAND_CLS_NAME : Deprecated parameter. Pass null.
-- IN_CREATE_DATE : Creation Date
-- IN_RECIPIENT : Recipient assigned to this Notification.
-- IN_USER_ID : Created by
-- IN_LAUNCHABLE : Indicates if the Notification can be launched. Valid values are Y or N. Default value of N
-- IN_OBJ_VER_NUM : Object version number. Pass in any number here.

	FUNCTION CREATE_NOTIFY_TASK_AUTOTRAN(
        O_error_message      OUT  VARCHAR2,
        IN_NOTIFICATION_ID   IN NUMBER,
        IN_NOTIFICATION_CONTEXT   IN VARCHAR2,
        IN_NOTIFICATION_TYPE_CODE IN VARCHAR2,
		IN_SEVERITY IN NUMBER,
        IN_APPLICATION_CODE IN VARCHAR2,
        IN_NOTIFICATION_DESC IN VARCHAR2,
        IN_NOTIFICATION_ACTIVE IN VARCHAR2,
        IN_NOTIF_COMMAND_CLS_NAME IN VARCHAR2,
        IN_CREATE_DATE IN TIMESTAMP,
        IN_RECIPIENT IN VARCHAR2,
        IN_USER_ID IN VARCHAR2,
		IN_LAUNCHABLE IN VARCHAR2,
        IN_OBJ_VER_NUM IN NUMBER
    ) 
	RETURN NUMBER ;	
	
------------------------------------------------------------------------------------------------

-- *******************************
-- OVERVIEW ( NOT RECOMMENDED )
-- *******************************
-- This function creates a Notification. It uses a Notification Type ID to create the Notification.
-- This function is outdated as it cannot handle the 'Additional Information' parameter (added in 5.0.5) and has only been retained for backward-compatibility purposes.
-- It is NOT RECOMMENDED to continue using this function as this is DEPRECATED and may be removed in future releases. 
-- *******************************
-- PARAMETER DESCRIPTION 
-- *******************************
-- O_error_message : Will contain an error code if this PL-SQL function fails
-- IN_NOTIFICATION_ID : Unique ID identifying the Notification. Use latest value from Sequence RAF_NOTIFICATION_SEQ 
-- IN_NOTIFICATION_CONTEXT : A Clob that contains contextual information that can be used to launch a Notification. 
-- IN_SOURCE : This distinguished the different types of Context Clob objects from which the Context Clob object was generated.  
-- ( RAF can create and handle two kinds of Context Clob objects. If the NavigationResource object is stored within the Context Clob (as JSON), then the Source column will be 'RAFPARAM'. If a simple pipe-delimited string of
-- key-value pairs is stored within the Context Clob, then the Source column will be 'TEXT'. Consuming applications can insert objects within the Context Clob, provided they also supply RAF, a NotificationSelectionHandler that
-- knows how to interpret the Clob for that Source type. See the Dev Guide for details on NotificationSelectionHandlers )
-- IN_NOTIFICATION_TYPE : ID of an existing Notification Type. See table RAF_NOTIFICATION_TYPE_B. 
-- IN_SEVERITY : Severity of the Notification. Default value of Low (1-Critical, 2-Important, 3-Normal)
-- IN_APPLICATION_CODE : Application to which the Notification belongs. This must match the Application Code of the Notification Type passed in. See column APPLICATION_CODE in table RAF_NOTIFICATION_TYPE_B.
-- IN_NOTIFICATION_DESC : Description of the Notification.
-- IN_NOTIFICATION_ACTIVE : Status of the Notification. Valid values are 'U'(UNREAD) or 'R'(READ)
-- IN_NOTIF_COMMAND_CLS_NAME : Deprecated parameter. Pass null.
-- IN_CREATE_DATE : Creation Date
-- IN_RECIPIENTS : Use this variable to pass in either a single or multiple recipients. Basically an array of Strings. See type NOTIF_RECIPIENTS_ARR 
-- IN_USER_ID : Created by
-- IN_LAUNCHABLE : Indicates if the Notification can be launched. Valid values are Y or N. Default value of N
-- IN_HIERARCHICAL_VALUE1 - Refers to a Department. 
-- IN_HIERARCHICAL_VALUE2 - Refers to a Class. 
-- IN_HIERARCHICAL_VALUE3 - Refers to a Subclass. 
-- IN_HIERARCHICAL_VALUE4 - Refers to a Location. 
-- IN_HIERARCHICAL_VALUE5 - Refers to a Supplier. 
-- IN_HIERARCHICAL_VALUE6 - Refers to the Peformance. 
-- IN_HIERARCHICAL_VALUE7 - Refers to a Brand. 
-- IN_HIERARCHICAL_VALUE8 - Refers to a Rollup-count. 
-- IN_OBJ_VER_NUM : Object version number. Pass in any number here.

	FUNCTION CREATE_NOTIFY_TASK_AUTOTRAN(
        O_error_message      OUT  VARCHAR2,
        IN_NOTIFICATION_ID   IN NUMBER,
        IN_NOTIFICATION_CONTEXT   IN CLOB,
		IN_SOURCE IN VARCHAR2,
        IN_NOTIFICATION_TYPE IN NUMBER,
        IN_SEVERITY IN NUMBER,
        IN_APPLICATION_CODE IN VARCHAR2,
        IN_NOTIFICATION_DESC IN VARCHAR2,
        IN_NOTIFICATION_ACTIVE IN VARCHAR2,
        IN_NOTIF_COMMAND_CLS_NAME IN VARCHAR2,
        IN_CREATE_DATE IN TIMESTAMP,
        IN_RECIPIENTS IN NOTIF_RECIPIENTS_ARR,
        IN_USER_ID IN VARCHAR2,
        IN_LAUNCHABLE IN VARCHAR2,
        IN_HIERARCHICAL_VALUE1 VARCHAR2,
        IN_HIERARCHICAL_VALUE2 VARCHAR2,
        IN_HIERARCHICAL_VALUE3 VARCHAR2,
        IN_HIERARCHICAL_VALUE4 VARCHAR2,
        IN_HIERARCHICAL_VALUE5 VARCHAR2,
        IN_HIERARCHICAL_VALUE6 VARCHAR2,
        IN_HIERARCHICAL_VALUE7 VARCHAR2,
        IN_HIERARCHICAL_VALUE8 VARCHAR2,
        IN_OBJ_VER_NUM IN NUMBER
    )
	RETURN NUMBER ;    

------------------------------------------------------------------------------------------------

-- *******************************
-- OVERVIEW ( NOT RECOMMENDED )
-- *******************************
-- This function creates a Notification. To use this function you must be aware of the Notification Type Code that will be used to fetch a Notification Type ID during creation.
-- This function is outdated as it cannot handle the 'Additional Information' parameter (added in 5.0.5) and has only been retained for backward-compatibility purposes.
-- It is NOT RECOMMENDED to continue using this function as this is DEPRECATED and may be removed in future releases. 
-- *******************************
-- PARAMETER DESCRIPTION 
-- *******************************
-- O_error_message : Will contain an error code if this PL-SQL function fails
-- IN_NOTIFICATION_ID : Unique ID identifying the Notification. Use latest value from Sequence RAF_NOTIFICATION_SEQ 
-- IN_NOTIFICATION_CONTEXT : A Clob that contains contextual information that can be used to launch a Notification. 
-- IN_SOURCE : This distinguished the different types of Context Clob objects from which the Context Clob object was generated.  
-- ( RAF can create and handle two kinds of Context Clob objects. If the NavigationResource object is stored within the Context Clob (as JSON), then the Source column will be 'RAFPARAM'. If a simple pipe-delimited string of
-- key-value pairs is stored within the Context Clob, then the Source column will be 'TEXT'. Consuming applications can insert objects within the Context Clob, provided they also supply RAF, a NotificationSelectionHandler that
-- knows how to interpret the Clob for that Source type. See the Dev Guide for details on NotificationSelectionHandlers )
-- IN_NOTIFICATION_TYPE_CODE : Type Code of an existing Notification Type. See table RAF_NOTIFICATION_TYPE_B. 
-- IN_SEVERITY : Severity of the Notification. Default value of Low (1-Critical, 2-Important, 3-Normal)
-- IN_APPLICATION_CODE : Application to which the Notification belongs. This must match the Application Code of the Notification Type passed in. See column APPLICATION_CODE in table RAF_NOTIFICATION_TYPE_B.
-- IN_NOTIFICATION_DESC : Description of the Notification.
-- IN_NOTIFICATION_ACTIVE : Status of the Notification. Valid values are 'U'(UNREAD) or 'R'(READ)
-- IN_NOTIF_COMMAND_CLS_NAME : Deprecated parameter. Pass null.
-- IN_CREATE_DATE : Creation Date
-- IN_RECIPIENTS : Use this variable to pass in either a single or multiple recipients.  Basically an array of Strings. See type NOTIF_RECIPIENTS_ARR 
-- IN_USER_ID : Created by
-- IN_LAUNCHABLE : Indicates if the Notification can be launched. Valid values are Y or N. Default value of N
-- IN_HIERARCHICAL_VALUE1 - Refers to a Department. 
-- IN_HIERARCHICAL_VALUE2 - Refers to a Class. 
-- IN_HIERARCHICAL_VALUE3 - Refers to a Subclass. 
-- IN_HIERARCHICAL_VALUE4 - Refers to a Location. 
-- IN_HIERARCHICAL_VALUE5 - Refers to a Supplier. 
-- IN_HIERARCHICAL_VALUE6 - Refers to the Peformance. 
-- IN_HIERARCHICAL_VALUE7 - Refers to a Brand. 
-- IN_HIERARCHICAL_VALUE8 - Refers to a Rollup-count. 
-- IN_OBJ_VER_NUM : Object version number. Pass in any number here.


	FUNCTION CREATE_NOTIFY_TASK_AUTOTRAN(
        O_error_message     OUT  VARCHAR2,
        IN_NOTIFICATION_ID   IN NUMBER,
        IN_NOTIFICATION_CONTEXT   IN CLOB,
        IN_SOURCE IN VARCHAR2,
        IN_NOTIFICATION_TYPE_CODE IN VARCHAR2,
        IN_SEVERITY IN NUMBER,
        IN_APPLICATION_CODE IN VARCHAR2,
        IN_NOTIFICATION_DESC IN VARCHAR2,
        IN_NOTIFICATION_ACTIVE IN VARCHAR2,
        IN_NOTIF_COMMAND_CLS_NAME IN VARCHAR2,
        IN_CREATE_DATE IN TIMESTAMP,
        IN_RECIPIENTS IN NOTIF_RECIPIENTS_ARR,
        IN_USER_ID IN VARCHAR2,
        IN_LAUNCHABLE IN VARCHAR2,
        IN_HIERARCHICAL_VALUE1 VARCHAR2,
        IN_HIERARCHICAL_VALUE2 VARCHAR2,
        IN_HIERARCHICAL_VALUE3 VARCHAR2,
        IN_HIERARCHICAL_VALUE4 VARCHAR2,
        IN_HIERARCHICAL_VALUE5 VARCHAR2,
        IN_HIERARCHICAL_VALUE6 VARCHAR2,
        IN_HIERARCHICAL_VALUE7 VARCHAR2,
        IN_HIERARCHICAL_VALUE8 VARCHAR2,
        IN_OBJ_VER_NUM IN NUMBER
    )
  	RETURN NUMBER ;

------------------------------------------------------------------------------------------------

-- *******************************
-- OVERVIEW ( RECOMMENDED )
-- *******************************
-- This function creates a Notification. It uses a Notification Type ID to create the Notification.
-- This is the RECOMMENDED approach for creating Notifications using Notification Type ID. 
-- *******************************
-- PARAMETER DESCRIPTION 
-- *******************************
-- O_error_message : Will contain an error code if this PL-SQL function fails
-- IN_NOTIFICATION_ID : Unique ID identifying the Notification. Use latest value from Sequence RAF_NOTIFICATION_SEQ 
-- IN_NOTIFICATION_CONTEXT : A Clob that contains contextual information that can be used to launch a Notification. 
-- IN_SOURCE : This distinguished the different types of Context Clob objects from which the Context Clob object was generated.  
-- ( RAF can create and handle two kinds of Context Clob objects. If the NavigationResource object is stored within the Context Clob (as JSON), then the Source column will be 'RAFPARAM'. If a simple pipe-delimited string of
-- key-value pairs is stored within the Context Clob, then the Source column will be 'TEXT'. Consuming applications can insert objects within the Context Clob, provided they also supply RAF, a NotificationSelectionHandler that
-- knows how to interpret the Clob for that Source type. See the Dev Guide for details on NotificationSelectionHandlers )
-- IN_NOTIFICATION_TYPE : ID of an existing Notification Type. See table RAF_NOTIFICATION_TYPE_B. 
-- IN_SEVERITY : Severity of the Notification. Default value of Low (1-Critical, 2-Important, 3-Normal)
-- IN_APPLICATION_CODE : Application to which the Notification belongs. This must match the Application Code of the Notification Type passed in. See column APPLICATION_CODE in table RAF_NOTIFICATION_TYPE_B.
-- IN_NOTIFICATION_DESC : Description of the Notification.
-- IN_NOTIFICATION_ACTIVE : Status of the Notification. Valid values are 'U'(UNREAD) or 'R'(READ)
-- IN_CREATE_DATE : Creation Date
-- IN_RECIPIENTS : Use this variable to pass in either a single or multiple recipients. Basically an array of Strings. See type NOTIF_RECIPIENTS_ARR 
-- IN_USER_ID : Created by
-- IN_LAUNCHABLE : Indicates if the Notification can be launched. Valid values are Y or N. Default value of N
-- IN_HIERARCHICAL_VALUE1 - Refers to a Department parameter. 
-- IN_HIERARCHICAL_VALUE2 - Refers to a Class parameter. 
-- IN_HIERARCHICAL_VALUE3 - Refers to a Subclass parameter. 
-- IN_HIERARCHICAL_VALUE4 - Refers to a Location parameter. 
-- IN_HIERARCHICAL_VALUE5 - Refers to a Supplier parameter. 
-- IN_HIERARCHICAL_VALUE6 - Refers to the Peformance parameter. 
-- IN_HIERARCHICAL_VALUE7 - Refers to a Brand parameter. 
-- IN_HIERARCHICAL_VALUE8 - Refers to a Rollup-count parameter. 
-- IN_HIERARCHICAL_VALUE9 - Refers to a Additional-Info parameter. 
-- IN_OBJ_VER_NUM : Object version number. Pass in any number here.

	FUNCTION CREATE_NOTIFY_TASK_AUTOTRAN(
        O_error_message      OUT  VARCHAR2,
        IN_NOTIFICATION_ID   IN NUMBER,
        IN_NOTIFICATION_CONTEXT   IN CLOB,
		IN_SOURCE IN VARCHAR2,
        IN_NOTIFICATION_TYPE IN NUMBER,
        IN_SEVERITY IN NUMBER,
        IN_APPLICATION_CODE IN VARCHAR2,
        IN_NOTIFICATION_DESC IN VARCHAR2,
        IN_NOTIFICATION_ACTIVE IN VARCHAR2,
        IN_CREATE_DATE IN TIMESTAMP,
        IN_RECIPIENTS IN NOTIF_RECIPIENTS_ARR,
        IN_USER_ID IN VARCHAR2,
        IN_LAUNCHABLE IN VARCHAR2,
        IN_HIERARCHICAL_VALUE1 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE2 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE3 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE4 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE5 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE6 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE7 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE8 IN VARCHAR2,
		IN_HIERARCHICAL_VALUE9 IN VARCHAR2,
        IN_OBJ_VER_NUM IN NUMBER
    )
	RETURN NUMBER ;    

------------------------------------------------------------------------------------------------

-- *******************************
-- OVERVIEW ( RECOMMENDED )
-- *******************************
-- This function creates a Notification. To use this function you must be aware of the Notification Type Code that will be used to fetch a Notification Type ID during creation.
-- This is the RECOMMENDED approach for creating Notifications using Notification Type Code. 
-- *******************************
-- PARAMETER DESCRIPTION 
-- *******************************
-- O_error_message : Will contain an error code if this PL-SQL function fails
-- IN_NOTIFICATION_ID : Unique ID identifying the Notification. Use latest value from Sequence RAF_NOTIFICATION_SEQ 
-- IN_NOTIFICATION_CONTEXT : A Clob that contains contextual information that can be used to launch a Notification. 
-- IN_SOURCE : This distinguished the different types of Context Clob objects from which the Context Clob object was generated.  
-- ( RAF can create and handle two kinds of Context Clob objects. If the NavigationResource object is stored within the Context Clob (as JSON), then the Source column will be 'RAFPARAM'. If a simple pipe-delimited string of
-- key-value pairs is stored within the Context Clob, then the Source column will be 'TEXT'. Consuming applications can insert objects within the Context Clob, provided they also supply RAF, a NotificationSelectionHandler that
-- knows how to interpret the Clob for that Source type. See the Dev Guide for details on NotificationSelectionHandlers )
-- IN_NOTIFICATION_TYPE_CODE : ID of an existing Notification Type. See table RAF_NOTIFICATION_TYPE_B. 
-- IN_SEVERITY : Severity of the Notification. Default value of Low (1-Critical, 2-Important, 3-Normal)
-- IN_APPLICATION_CODE : Application to which the Notification belongs. This must match the Application Code of the Notification Type passed in. See column APPLICATION_CODE in table RAF_NOTIFICATION_TYPE_B.
-- IN_NOTIFICATION_DESC : Description of the Notification.
-- IN_NOTIFICATION_ACTIVE : Status of the Notification. Valid values are 'U'(UNREAD) or 'R'(READ)
-- IN_CREATE_DATE : Creation Date
-- IN_RECIPIENTS : Use this variable to pass in either a single or multiple recipients. Basically an array of Strings. See type NOTIF_RECIPIENTS_ARR 
-- IN_USER_ID : Created by
-- IN_LAUNCHABLE : Indicates if the Notification can be launched. Valid values are Y or N. Default value of N
-- IN_HIERARCHICAL_VALUE1 - Refers to a Department parameter. 
-- IN_HIERARCHICAL_VALUE2 - Refers to a Class parameter. 
-- IN_HIERARCHICAL_VALUE3 - Refers to a Subclass parameter. 
-- IN_HIERARCHICAL_VALUE4 - Refers to a Location parameter. 
-- IN_HIERARCHICAL_VALUE5 - Refers to a Supplier parameter. 
-- IN_HIERARCHICAL_VALUE6 - Refers to the Peformance parameter. 
-- IN_HIERARCHICAL_VALUE7 - Refers to a Brand parameter. 
-- IN_HIERARCHICAL_VALUE8 - Refers to a Rollup-count parameter. 
-- IN_HIERARCHICAL_VALUE9 - Refers to a Additional-Info parameter. 
-- IN_OBJ_VER_NUM : Object version number. Pass in any number here.

	FUNCTION CREATE_NOTIFY_TASK_AUTOTRAN(
        O_error_message     OUT  VARCHAR2,
        IN_NOTIFICATION_ID   IN NUMBER,
        IN_NOTIFICATION_CONTEXT   IN CLOB,
        IN_SOURCE IN VARCHAR2,
        IN_NOTIFICATION_TYPE_CODE IN VARCHAR2,
        IN_SEVERITY IN NUMBER,
        IN_APPLICATION_CODE IN VARCHAR2,
        IN_NOTIFICATION_DESC IN VARCHAR2,
        IN_NOTIFICATION_ACTIVE IN VARCHAR2,
        IN_CREATE_DATE IN TIMESTAMP,
        IN_RECIPIENTS IN NOTIF_RECIPIENTS_ARR,
        IN_USER_ID IN VARCHAR2,
        IN_LAUNCHABLE IN VARCHAR2,
        IN_HIERARCHICAL_VALUE1 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE2 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE3 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE4 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE5 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE6 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE7 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE8 IN VARCHAR2,
        IN_HIERARCHICAL_VALUE9 IN VARCHAR2,		
        IN_OBJ_VER_NUM IN NUMBER
    )
  	RETURN NUMBER ;

------------------------------------------------------------------------------------------------

END RAF_NOTIFICATION_TASK_PKG;
/