CREATE OR REPLACE PACKAGE RAF_AUXSCR_PKG AUTHID CURRENT_USER AS 
    SUCCESS_RETURN CONSTANT NUMBER := 1;
    FAIL_RETURN CONSTANT NUMBER := 0;

    FUNCTION REGISTER_USER (O_ERR_MSG OUT VARCHAR2,
                            I_USER_ID IN VARCHAR2, 
                            I_PAIRING_KEY IN VARCHAR2,
                            I_REPLACE_KEY IN VARCHAR2)
                            RETURN NUMBER;
                            
    FUNCTION DEREGISTER_USER (O_ERR_MSG OUT VARCHAR2,
                              I_USER_ID IN VARCHAR2)
                              RETURN NUMBER;
                            
                            
    FUNCTION GET_PAIRING_KEY (O_ERR_MSG OUT VARCHAR2,
                              I_USER_ID IN VARCHAR2, 
                              O_PAIRING_KEY OUT VARCHAR2)
                              RETURN NUMBER;
                            

END RAF_AUXSCR_PKG;

/