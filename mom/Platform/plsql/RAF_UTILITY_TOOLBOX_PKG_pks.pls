create or replace PACKAGE RAF_UTILITY_TOOLBOX_PKG AUTHID CURRENT_USER AS

FUNCTION ACTIVATE_LANGUAGES(O_ERROR_MESSAGE OUT VARCHAR2,
									IN_SCHEMA_NAME IN VARCHAR2) RETURN NUMBER;

FUNCTION GET_ALL_COLS_TABLE(I_table_name IN varchar2,
                             I_prefix IN varchar2, 
							 I_schema_name IN varchar2) RETURN VARCHAR2;

 FUNCTION GET_PK_COLS_CLAUSE(I_table_name IN varchar2, I_schema_name IN varchar2)
    RETURN VARCHAR2;

  FUNCTION GET_MERGE_QUERY(I_table_name IN varchar2, I_schema_name IN varchar2)
    RETURN VARCHAR2;
END RAF_UTILITY_TOOLBOX_PKG;
/