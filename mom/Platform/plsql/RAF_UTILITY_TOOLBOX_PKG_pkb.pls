PROMPT CREATING BASEMODEL RAF_UTILITY_TOOLBOX_PKG;

create or replace PACKAGE BODY RAF_UTILITY_TOOLBOX_PKG AS

	FUNCTION ACTIVATE_LANGUAGES(O_ERROR_MESSAGE       OUT VARCHAR2,
								                   IN_SCHEMA_NAME       IN  VARCHAR2)
	RETURN NUMBER IS
  TYPE STRING_ARR IS TABLE OF VARCHAR2(100);
  L_langs STRING_ARR;
		BEGIN
      --Initialize the languages collection with all the languages INSTALLED 
      L_langs := STRING_ARR();
      for j in (select distinct language_code from rtc_languages_b where installed_flag in ('B','I'))
      loop
        L_langs.extend;
        L_langs(L_langs.last) := j.language_code;
      end loop;

		--Query to fetch all the table names that end with _TL
		for i in (select A.table_name from all_tables A where A.owner=IN_SCHEMA_NAME and A.table_name like '%\_TL' ESCAPE '\' AND exists ( SELECT c.TABLE_NAME, COUNT(*) FROM all_TAB_COLS c WHERE c.TABLE_NAME=A.table_name and c.owner=A.owner AND c.COLUMN_NAME IN ('SOURCE_LANG','LANGUAGE') AND c.DATA_TYPE LIKE 'VARCHAR%' GROUP BY c.TABLE_NAME HAVING COUNT(*) = 2 ) )
		loop
        --Update table with all the languages 
        for j in 1..L_langs.count
        loop
            execute immediate replace(GET_MERGE_QUERY(i.table_name, IN_SCHEMA_NAME), 'LANGS(J)',L_langs(j));
        end loop;
			end loop;
			commit;
			return 1;
			exception
			when others then
				O_error_message := RAF_SQL_LIB.RAF_CREATE_MSG(SQLERRM,
													'RAF_UTILITY_TOOLBOX_PKG ACTIVATE_ALL_LANGUAGES FAILED',
													to_char(SQLCODE));
			return 0;
	END ACTIVATE_LANGUAGES;

------------------------------------------------------------------------------------------------------------------------

  FUNCTION GET_MERGE_QUERY(I_table_name IN varchar2, I_schema_name varchar2)
    RETURN VARCHAR2
  IS
    L_merge_query VARCHAR2(10000) := '';
    L_sel_all_clause VARCHAR2(4000) := '';
    L_vals_clause VARCHAR2(4000) := '';
  BEGIN
    L_sel_all_clause := GET_ALL_COLS_TABLE(I_table_name,'', I_schema_name);
    L_sel_all_clause := REPLACE(L_sel_all_clause,',LANGUAGE,',',''LANGS(J)'' AS LANGUAGE,');
    L_sel_all_clause := REPLACE(L_sel_all_clause,',SOURCE_LANG,',',''LANGS(J)'' AS SOURCE_LANG,');

    L_vals_clause := GET_ALL_COLS_TABLE(I_table_name,'SRC.', I_schema_name);
    L_vals_clause := REPLACE(L_vals_clause,',SRC.LANGUAGE,',',''LANGS(J)'',');
    L_vals_clause := REPLACE(L_vals_clause,',SRC.SOURCE_LANG,',',''LANGS(J)'',');

    L_merge_query := 'MERGE INTO '|| I_table_name || ' DEST ' ||
                     ' USING (SELECT ' || L_sel_all_clause || ' FROM ' || I_table_name ||  ' WHERE LANGUAGE=''US'') SRC' ||
                     ' ON (' || GET_PK_COLS_CLAUSE(I_table_name,I_schema_name) || ' AND DEST.LANGUAGE = ''LANGS(J)'' )' ||
                     ' WHEN NOT MATCHED THEN ' ||
                     ' INSERT( ' || GET_ALL_COLS_TABLE(I_table_name,'', I_schema_name) || ')' ||
                     ' VALUES( ' || L_vals_clause || ')';
    return L_merge_query;
  END GET_MERGE_QUERY;

------------------------------------------------------------------------------------------------------------------------

  FUNCTION GET_ALL_COLS_TABLE(I_table_name IN varchar2,
                             I_prefix IN varchar2,
							 I_schema_name IN varchar2)
    RETURN VARCHAR2
  IS
    L_comma_delim_str VARCHAR2(4000) := '';
  BEGIN
    for i in (select column_name from all_tab_cols where table_name = UPPER(I_table_name) and owner=I_schema_name)
    loop
      if(length(L_comma_delim_str) <> 0 )then
        L_comma_delim_str := L_comma_delim_str || ',';
      end if;
      if(length(I_prefix) <> 0 )then
        L_comma_delim_str := L_comma_delim_str || I_prefix || i.column_name;
      else
        L_comma_delim_str := L_comma_delim_str || i.column_name;
      end if;
    end loop;

    RETURN L_comma_delim_str;
  END GET_ALL_COLS_TABLE;

------------------------------------------------------------------------------------------------------------------------

  FUNCTION GET_PK_COLS_CLAUSE(I_table_name IN varchar2,
							   I_schema_name IN varchar2)
    RETURN VARCHAR2
  IS
    L_pk_clause VARCHAR2(4000) := '';
  BEGIN
    for i in (select column_name FROM all_cons_columns WHERE constraint_name =
    (select constraint_name FROM all_constraints WHERE UPPER(table_name) = I_table_name AND CONSTRAINT_TYPE = 'P' and owner=I_schema_name))
    loop
      if(length(L_pk_clause) <> 0) then
        L_pk_clause := L_pk_clause || ' and ';
      end if;
      L_pk_clause := L_pk_clause || ' SRC.' || i.column_name || ' = DEST.' || i.column_name;
    end loop;
    return L_pk_clause;
  END GET_PK_COLS_CLAUSE;

END RAF_UTILITY_TOOLBOX_PKG;
/