PROMPT CREATING BASEMODEL RAF_ASYNC_TASK_PKG;

CREATE OR REPLACE PACKAGE BODY RAF_ASYNC_TASK_PKG AS

    --------------------------------------------------------------------------------------
    /*To support old and new PL/SQL procedures untill all consumer apps migrate their latest changes using both functions,
      in the data base, but soon below 2 functions will be deprecated.
    */
    ---------------------------------------------------------------------------------------
    -- @DEPRECATED since 4.0.3.

    FUNCTION UPDATE_ASYNC_TASK_AUTOTRAN(
            IN_ASYNC_TASK_ID IN NUMBER,
            IN_STATUS        IN VARCHAR2,
            IN_PROCESS_START_TIMESTAMP IN TIMESTAMP,
            IN_PROCESS_END_TIMESTAMP IN TIMESTAMP,
            IN_ERROR_TEXT IN VARCHAR2,
            IN_LAST_UPDATE_DATE IN TIMESTAMP,
            IN_USER_ID IN VARCHAR2,
            IN_OBJ_VER_NUM IN NUMBER)
        RETURN NUMBER AS
        PRAGMA AUTONOMOUS_TRANSACTION;

            v_output NUMBER;
            O_error_message VARCHAR2(2000);

    BEGIN
        v_output:= UPDATE_ASYNC_TASK_AUTOTRAN(O_error_message,
            IN_ASYNC_TASK_ID,
            IN_STATUS,
            IN_PROCESS_START_TIMESTAMP,
            IN_PROCESS_END_TIMESTAMP,
            IN_ERROR_TEXT,
            IN_LAST_UPDATE_DATE,
            IN_USER_ID,
            IN_OBJ_VER_NUM);
        return v_output;
    END;

    -----------------------------------------------------------------------
    -- @DEPRECATED since 4.0.3.
    FUNCTION DELETE_ASYNC_TASK(IN_NUMBER_OF_DAYS   IN NUMBER)
        RETURN NUMBER IS
        v_output NUMBER;
        O_error_message VARCHAR2(2000);
    BEGIN
        v_output := DELETE_ASYNC_TASK(O_error_message, IN_NUMBER_OF_DAYS);
        return v_output;
    END;

    ------------------------------------------------------------------------
    -- Use UPDATE_ASYNC_TASK_AUTOTRAN with new O_error_message parameter.
    FUNCTION UPDATE_ASYNC_TASK_AUTOTRAN(
            O_error_message      OUT  VARCHAR2,
            IN_ASYNC_TASK_ID IN NUMBER,
            IN_STATUS        IN VARCHAR2,
            IN_PROCESS_START_TIMESTAMP IN TIMESTAMP,
            IN_PROCESS_END_TIMESTAMP IN TIMESTAMP,
            IN_ERROR_TEXT IN VARCHAR2,
            IN_LAST_UPDATE_DATE IN TIMESTAMP,
            IN_USER_ID IN VARCHAR2,
            IN_OBJ_VER_NUM IN NUMBER)
    RETURN NUMBER AS
        PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
        UPDATE RAF_ASYNC_TASK
        SET STATUS = IN_STATUS,
            PROCESS_START_TIMESTAMP = IN_PROCESS_START_TIMESTAMP,
            PROCESS_END_TIMESTAMP = IN_PROCESS_END_TIMESTAMP,
            PROCESS_ERROR_TXT = IN_ERROR_TEXT,
            LAST_UPDATE_DATE = IN_LAST_UPDATE_DATE,
            LAST_UPDATED_BY = IN_USER_ID,
            OBJECT_VERSION_NUMBER = IN_OBJ_VER_NUM
        WHERE ASYNC_TASK_ID = IN_ASYNC_TASK_ID;

        COMMIT;

        RETURN 1;

    EXCEPTION

    WHEN OTHERS THEN
        O_error_message := RAF_SQL_LIB.RAF_CREATE_MSG(SQLERRM,
                                                'RAF_ASYNC_TASK_PKG UPDATE_ASYNC_TASK_AUTOTRAN FAILED',
                                                to_char(SQLCODE));
        ROLLBACK;
        RETURN 0;

    END;


    -------------------------------------------------------------------------------------
    -- Use UPDATE_ASYNC_TASK_AUTOTRAN with new O_error_message parameter.
    FUNCTION DELETE_ASYNC_TASK(O_error_message     OUT  VARCHAR2,
                               IN_NUMBER_OF_DAYS   IN NUMBER)
    RETURN NUMBER IS
    BEGIN

        DELETE from RAF_ASYNC_TASK where last_update_date < sysdate-IN_NUMBER_OF_DAYS;
        RETURN 1;

    EXCEPTION

    WHEN OTHERS THEN
        O_error_message := RAF_SQL_LIB.RAF_CREATE_MSG(SQLERRM,
                                                    'RAF_ASYNC_TASK_PKG DELETE_ASYNC_TASK FAILED',
                                                    to_char(SQLCODE));
        ROLLBACK;

        RETURN 0;

    END;

END RAF_ASYNC_TASK_PKG;
/

