CREATE OR REPLACE PACKAGE BODY RAF_AUXSCR_PKG AS

    FUNCTION REGISTER_USER (O_ERR_MSG OUT VARCHAR2,
                            I_USER_ID IN VARCHAR2,
                            I_PAIRING_KEY IN VARCHAR2,
                            I_REPLACE_KEY IN VARCHAR2)
                            RETURN NUMBER AS
        PRAGMA AUTONOMOUS_TRANSACTION;
        L_RETURN NUMBER := SUCCESS_RETURN;
        L_EXISTING_KEY VARCHAR2(500);
    BEGIN

        L_RETURN := RAF_AUXSCR_PKG.GET_PAIRING_KEY(O_ERR_MSG, I_USER_ID, L_EXISTING_KEY);

        IF L_RETURN = SUCCESS_RETURN THEN

            IF L_EXISTING_KEY IS NOT NULL THEN
                IF I_REPLACE_KEY = 'Y' THEN
                    UPDATE RAF_AUXSCR_USER
                        SET PAIRING_KEY = I_PAIRING_KEY
                        WHERE USER_ID = UPPER(I_USER_ID);
                END IF;
            ELSE
                INSERT INTO RAF_AUXSCR_USER (USER_ID, PAIRING_KEY)
                  VALUES (UPPER(I_USER_ID), I_PAIRING_KEY);
            END IF;

        END IF;

        COMMIT;
        RETURN L_RETURN;

    EXCEPTION

    WHEN OTHERS THEN

        O_ERR_MSG := RAF_SQL_LIB.RAF_CREATE_MSG(SQLERRM,
                                                'RAF_AUXSCR_PKG.REGISTER_USER FAILED',
                                                TO_CHAR(SQLCODE));
        ROLLBACK;

        RETURN FAIL_RETURN;

    END REGISTER_USER;


    FUNCTION DEREGISTER_USER (O_ERR_MSG OUT VARCHAR2,
                          I_USER_ID IN VARCHAR2)
                          RETURN NUMBER AS
        PRAGMA AUTONOMOUS_TRANSACTION;
        L_RETURN NUMBER := SUCCESS_RETURN;
        L_EXISTING_KEY VARCHAR2(500);
    BEGIN
        DELETE FROM RAF_AUXSCR_USER WHERE USER_ID = UPPER(I_USER_ID);
        COMMIT;
        RETURN SUCCESS_RETURN;
    EXCEPTION
    WHEN OTHERS THEN
        O_ERR_MSG := RAF_SQL_LIB.RAF_CREATE_MSG(SQLERRM,
                                                'RAF_AUXSCR_PKG.REGISTER_USER FAILED',
                                                TO_CHAR(SQLCODE));
        ROLLBACK;
        RETURN FAIL_RETURN;
    END DEREGISTER_USER;



    FUNCTION GET_PAIRING_KEY (O_ERR_MSG OUT VARCHAR2,
                          I_USER_ID IN VARCHAR2,
                          O_PAIRING_KEY OUT VARCHAR2)
                          RETURN NUMBER AS
        L_COUNT NUMBER := 0;
    BEGIN
        -- CHECK NUMBER OF ENTRIES...
        SELECT COUNT(*) INTO L_COUNT FROM RAF_AUXSCR_USER WHERE USER_ID = UPPER(I_USER_ID);

        IF L_COUNT = 1 THEN
            SELECT PAIRING_KEY INTO O_PAIRING_KEY FROM RAF_AUXSCR_USER WHERE USER_ID = UPPER(I_USER_ID);
        ELSIF L_COUNT = 0 THEN
            O_PAIRING_KEY := NULL;
        ELSE
            O_ERR_MSG := 'USER HAS MORE THAN ONE PAIRING ENTRIES '||L_COUNT||'.';
            RETURN FAIL_RETURN;
        END IF;

        RETURN SUCCESS_RETURN;
    EXCEPTION
    WHEN OTHERS THEN

        O_ERR_MSG := RAF_SQL_LIB.RAF_CREATE_MSG(SQLERRM,
                                                'RAF_AUXSCR_PKG.GET_PAIRING_KEY FAILED',
                                                TO_CHAR(SQLCODE));
        ROLLBACK;
        RETURN FAIL_RETURN;
    END GET_PAIRING_KEY;

END RAF_AUXSCR_PKG;
/