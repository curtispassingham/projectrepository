############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package Parallel;

#import the WNOHANG symbol
use POSIX ":sys_wait_h";

%Parallel::CHILD_PIDS=();
@Parallel::SIGNALS=('TERM','INTR','INT','ABRT','HUP');

########################
# Sub:  StopChildren
# Desc: A signal handler for signals, to stop any parallel children we have running
# Ret:  None
########################
sub StopChildren {
	my ($signal)=@_;
	&::LogMsg("Received $signal signal, aborting!");
	foreach my $pid (keys(%Parallel::CHILD_PIDS)) {
		kill('TERM',$pid);
	}
	exit 1;
}

########################
# Sub:  RegisterSignalHandler
# Desc: Register our signal handler for all the signals that might be sent to kill orpatch
# Ret:  1
########################
sub RegisterSignalHandler {
	foreach my $signame (@Parallel::SIGNALS) {
        $::SIG{$signame}=sub {&StopChildren($signame);};
	}
	return 1;
}

########################
# Sub:  UnRegisterSignalHandler
# Desc: Unregister our signal handler for all the signals that might be sent to kill orpatch
# Ret:  1
########################
sub UnRegisterSignalHandler {
	foreach my $signame (@Parallel::SIGNALS) {
        $::SIG{$signame}='';
	}
	return 1;
}

########################
# Sub:  RegisterChildExit
# Desc: Remove a child from our list of running children
# Ret:  1
########################
sub RegisterChildExit {
	my ($pid)=@_;
	delete($Parallel::CHILD_PIDS{$pid});
	return 1;
}

########################
# Sub:  RegisterChild
# Desc: Record that a child was spawned, plus arbitrary information about that child
# Ret:  1
########################
sub RegisterChild {
	my ($pid,$value)=@_;
	$Parallel::CHILD_PIDS{$pid}=$value;
	return 1;
}

########################
# Sub:  GetKids
# Desc: Get a list of all of our children
# Ret:  list of kids
########################
sub GetKids {
	return keys(%Parallel::CHILD_PIDS);
}

########################
# Sub:  GetChildInfo
# Desc: Get the information that was stored for a specific kid
# Ret:  info originally passed into RegisterChild for this child
########################
sub GetChildInfo {
	my ($pid)=@_;
	return $Parallel::CHILD_PIDS{$pid};
}

########################
# Sub:  RemoveAllChildren
# Desc: Remove any children that were being tracked
# Ret:  1
########################
sub RemoveAllChildren {
	%Parallel::CHILD_PIDS=();
	return 1;
}

#########################
# Sub:  Tee
# Desc: Run a subroutine in a subprocess, printing it's output to the screen as it runs
# Args: child_sub - reference to the subroutine the child should call
#       child_arg_ref - Ref to the arguments to pass to the child (in addition to the Log object pointed at the log file)
#       output_ref - Ref to the list that should get the output from the command
#       log_only - 1 if child output should only go to the log file, otherwise it goes to log file and stdout
# Ret:  1 if the subroutine completes successfully, 0 if not
#########################
sub Parallel::Tee {
	my ($child_sub,$child_arg_ref,$output_ref,$log_only)=@_;
	
	&Parallel::RemoveAllChildren();
	&Parallel::RegisterSignalHandler();

	my $lfile=$::LOG->GetLogFile();
	my ($ldir,$fname)=&::GetFileParts($lfile,2);

	my $child_log_file=&::ConcatPathString($ldir,"orpatch_tee${$}_child_1.log");

	my $cpid=&_SpawnChild($child_sub,$child_arg_ref,$child_log_file);
	if (!defined($cpid)) {
		&::LogMsg("Child failed to spawn!");
		return 0;
	} else {
		&::LogMsg("Child spawned as pid $cpid");
		&Parallel::RegisterChild($cpid,$child_log_file);
	}

	my $overall_ret=1;
	my $child_ret=0;
	my $last_line=0;

	#Child spawned, wait for it to finish
	while (scalar(&Parallel::GetKids())>0) {
		while((my $kid=waitpid(-1,WNOHANG)) > 0) {
			$child_ret=($?>>8);
			&Parallel::RegisterChildExit($kid);
			if ($child_ret!=0) {
				$overall_ret=0;
			}
		}
		&_PrintChildLogs($child_log_file,\$last_line,'',$log_only);
		sleep 1;
	}
	
	&Parallel::UnRegisterSignalHandler();
	
	&_PrintChildLogs($child_log_file,\$last_line,$output_ref,$log_only);

	#Now that we have the full contents of the log in $output_ref, remove it
	unlink($child_log_file);
	
	#We do this here so it comes after the end of the child's output
	my $status_text=($child_ret==0) ? 'success' : 'errors';
	&::LogMsg("Child process $cpid completed with status $child_ret ($status_text)");

	return $overall_ret;
}

#########################
# Sub:  _SpawnChild
# Desc: Create a copy of ourselves to run a procedure
# Args: child_sub - reference to the subroutine the child should call
#       child_arg_ref - Ref to the arguments to pass to the child (in addition to the Log object pointed at the log file)
#       child_log_file - The file the child should write to
# Ret:  process id of the child process, or undef if the child failed to spawn
#########################
sub Parallel::_SpawnChild {
	my ($child_sub,$child_arg_ref,$child_log_file)=@_;

	#Fork ourselves, immediately return undef if the fork failed
	defined(my $kid=fork) or return undef;

	unless ($kid) {
		#Child process
		#open a log to a specific filename, no appending, no timestamp
		my $child_log=new Log($child_log_file,0,'',0);
		#Clean-up the children array in the child process as it does not have children
		&Parallel::RemoveAllChildren();
		my $ret=&$child_sub($child_log,@$child_arg_ref);
		exit(($ret==1) ? 0 : 1);  #The child always exits
	}

	#parent process
	return $kid;
}

#########################
# Sub:  _PrintChildLogs
# Desc: Print any new contents in a child's log
# Args: child_log_file - The file the child should write to
#       last_list_sref - reference to the scalar that contains the last line printed
#       output_ref - Reference to an array that should get the full current log contents
#       log_only - 1 if child output should only go to the log file, otherwise it goes to log file and stdout
# Ret:  1
#########################
sub Parallel::_PrintChildLogs {
   my ($child_log_file,$last_line_sref,$output_ref,$log_only)=@_;

	return 1 unless -f $child_log_file;
	unless(open(CHILD_LOG,"<$child_log_file")) {
		return 1;
	}
	my $line=0;
	while(<CHILD_LOG>) {
		$line++;
		push(@$output_ref,$_) if $output_ref ne '';
		next unless ($line>$$last_line_sref);
		my $line_text=$_;
		#Strip the leading date/time on the output since we will add our own in LogMsg
		$line_text=~s/^\d{2}:\d{2}:\d{2} \d{2}\/\d{2}\/\d{4}//;
		if ($log_only==1) {
			$::LOG->LogMsgNoVerbose($line_text);
		} else {
			&::LogMsg($line_text);
		}
	}
	close(CHILD_LOG);
	$$last_line_sref=$line;
	return 1;
}

1;
