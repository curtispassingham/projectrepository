############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
package JarFile;
use strict;
use Cwd;

#The registered signing alias
$JarFile::SIGNING_ALIAS='';
$JarFile::DEST_DIR='';

#Jars which needed to be signed but could not be
@JarFile::JARS_TO_BE_SIGNED=();

############################
# Sub:  Expand
# Desc: Expand a jar file (either all files or select file
# Args: jar_file - path to the jar file
#       expand_dir - path to the directory to temporarily expand the jar in
#       extract_file - file to extract, or ref to multiple files to extract
#       extract_file_req - 1 if we should treat the failure to extract extract_file(s) as an error
#                          0 if we ignore failure to extract.  Default is 1 if extract_file is specified
# Returns: 1 on success, 0 on failure
############################
sub JarFile::Expand {
	my ($jar_file,$expand_dir,$extract_file,$extract_file_req)=@_;
	
	my $extract_file_ref=$extract_file;
	if ($extract_file ne '' && !ref($extract_file_ref)) {
		$extract_file_ref=[$extract_file_ref];
	}
	if ($extract_file ne '') {
		$extract_file_req=1 if $extract_file_req eq '';
	}
	
	if (&CreateExpandDir($expand_dir)!=1) {
		return 0;
	}
	
	my $old_dir=cwd;
	unless(chdir($expand_dir)) {
		&::LogMsg("Failed changing to $expand_dir: $!");
		return 0;
	}
	
	#If we were given a relative path name, we need to fully qualify it after chdir
	if (!-f $jar_file) {
		my $full_jar_file=&::ConcatPathString($old_dir,$jar_file);
		if (-f $full_jar_file) {
			$jar_file=$full_jar_file;
		}
	}
	
	my $cmd="jar xvf '$jar_file'";
	if ($extract_file ne '') {
		my $all_files=join("' '",@$extract_file_ref);
		$cmd.=" '$all_files'";
	}
	
	my $ret=1;
	my @output=`$cmd 2>&1`;
	if ($?!=0) {
		&::LogMsg("Unjar of $jar_file ($extract_file) failed: $!");
		&::DebugOutput('jar output',@output);
		$ret=0;
	}
	
	chdir($old_dir);
	
	if ($extract_file ne '' && $extract_file_req==1) {
		#Check for all files
		foreach my $ef (@$extract_file_ref) {
			my $full_ef=&::ConcatPathString($expand_dir,$ef);
			#We use -e here because they could have handed us a directory name
			if (!-e $full_ef) {
				&::LogMsg("Failed to extract $ef from $jar_file!");
				&::DebugOutput('jar output',@output);
				$ret=0;
			}
		}
	}

	return $ret;
}

#########################
# Sub:  Update
# Desc: Update a jar file with a new file
# Arg:  jar_file - The jar file to update
#       base_dir - The base directory that update_file is relative to
#       update_file - file name to update, relative to base_dir
#       silent - 1 if we should not talk about updating the jar, default is 0
# Ret:  1 on success, 0 on failure
#########################
sub JarFile::Update {
	my ($jar_file,$base_dir,$update_file,$silent)=@_;
	
	if ($update_file eq '') {
		&::LogMsg("No file specified for UpdateJarFile!");
		return 0;
	}
	
	&::LogMsg("Updating $jar_file with $update_file") if ($silent!=1);
	my @output=`jar uvf '$jar_file' -C '$base_dir' '$update_file' 2>&1`;
	
	if ($?!=0) {
		&::LogMsg("Jar update command for $jar_file failed!");
		&::DebugOutput('jar output',@output);
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  UpdateMulti
# Desc: Update a jar file with a new file
# Arg:  jar_file - The jar file to update
#       update_files_ref - Ref to a list of [base_dir,update_file] entries
#                          update file names must be relative to base_dir
#       silent - 1 if we should not talk about updating the jar, default is 0
# Ret:  1 on success, 0 on failure
#########################
sub JarFile::UpdateMulti {
	my ($jar_file,$update_files_ref,$silent)=@_;
	
	if (!ref($update_files_ref)) {
		&::LogMsg("UpdateJarFileMulti requires a reference for update_file_ref");
		return 0;
	}
	
	if (scalar(@$update_files_ref)==0) {
		&::LogMsg("At least one file must be specified to update into $jar_file!");
		return 0;
	}
	
	my $update_file_list='';
	foreach my $ref (@$update_files_ref) {
		my ($base_dir,$update_file)=@$ref;
		$update_file_list.="-C '$base_dir' '$update_file' ";
	}
	
	&::LogMsg("Updating $jar_file with ".scalar(@$update_files_ref)." files") if ($silent!=1);
	my @output=`jar uvf '$jar_file' $update_file_list 2>&1`;
	
	if ($?!=0) {
		&::LogMsg("Jar update command for $jar_file failed!");
		&::DebugOutput('jar output',@output);
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  Create
# Desc: Create a jar file from files in a directory
# Arg:  jar_file - The jar file to create
#       base_dir - The base directory
#       include_files - Ref to an array of files to include or '' to include all files
#       manifest - The manifest file
#       silent - 1 if we should not talk about creating the jar, default is 0
# Ret:  1 on success, 0 on failure
#########################
sub JarFile::Create {
	my ($jar_file,$base_dir,$include_file_ref,$manifest,$silent)=@_;
	
	my $cfm='cvfm';
	if ($manifest eq '') {
		$cfm='cvf';
	} else {
		$manifest="'$manifest'";
	}
	
	#If they did not say what files to include, include everything in base_dir
	$include_file_ref=['.'] if $include_file_ref eq '';

	my $files='';
	foreach my $file (@$include_file_ref) {
		$files.="-C '$base_dir' '$file' ";
	}
	
	&::LogMsg("Creating $jar_file") if ($silent!=1);
	my @output=`jar $cfm '$jar_file' $manifest $files 2>&1`;
	
	if ($?!=0) {
		&::LogMsg("Jar create command for $jar_file failed!");
		&::DebugOutput('jar output',@output);
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  CheckJarForFile
# Desc: Determine it a jar contains a specific file
# Arg:  jar_file - The jar file to check
#       contains_file - The file name to check for
# Ret:  -1 on an error while checking, 0 if the jar does not contain the file, 1 if the jar does contain the file
#########################
sub JarFile::CheckJarForFile {
	my ($jar_file,$contains_file)=@_;

	my %file_hash=();
	if (&GetTOC($jar_file,\%file_hash,1,$contains_file)!=1) {
		return -1;
	}
	
	if (exists($file_hash{$contains_file})) {
		return 1;
	} else {
		return 0;
	}
}

#########################
# Sub:  IsJarSigned
# Desc: Check if a given jar file is signed or not
# Arg:  jar_file
# Ret:  1 if the jar is signed, 0 if not, -1 if there is an error checking
#########################
sub JarFile::IsJarSigned {
	my ($jar_file)=@_;
	
	my %file_hash=();
	if (&GetTOC($jar_file,\%file_hash,0,'META-INF')!=1) {
		return -1;
	}
		
	my $pattern_ref=&GetSignedJarPatterns(1);
	
	#Look for META-INF/*.dsa, *.rsa, *.sf files
	my $signed=0;
	foreach my $file (keys(%file_hash)) {
		foreach my $pr (@$pattern_ref) {
			if ($file=~/$pr/) {
				$signed=1;
				last;
			}
		}
	}
	
	return $signed;
}

#########################
# Sub:  GetTOC
# Desc: Get the table of contents for a jar
# Arg:  jar_file - The jar file to list TOC for
#       output_hash_ref - ref to the hash that will receive the table of contents
#                         hash will have keys of the file/dir names
#                         values will be either 'FILE' or 'DIR' depending on the type of TOC object
#       include_dirs - 1 to include directories in the output_hash_ref
#       contains_file - get only the TOC entry for a specific file
# Ret: 1 on success, 0 on failure
#########################
sub JarFile::GetTOC {
	my ($jar_file,$output_href,$include_dirs,$contains_file)=@_;

	$contains_file="'$contains_file'" if $contains_file ne '';
	
	my @output=`jar tf '$jar_file' $contains_file 2>&1`;
	if ($?!=0) {
		&::LogMsg("Jar -tf command for $jar_file failed!");
		&::DebugOutput('jar output',@output);
		return 0;
	}
	
	foreach my $line (@output) {
		chomp($line);
		if ($line=~/\/$/) {
			#Directory
			if ($include_dirs==1) {
				$output_href->{$line}='DIR';
			}
		} else {
			$output_href->{$line}='FILE';
		}
	}

	return 1;
}

#########################
# Sub:  Unsign
# Desc: Unsign a jar file
# Arg:  jar_file,indent_level
# Ret:  1 on success, 0 on failure
#########################
sub JarFile::Unsign {
	my ($jar_file,$indent_level)=@_;
	
	my ($dir,$base_jar)=&::GetFileParts($jar_file,2);
	my $spaces=" "x($indent_level*3);

	&::LogMsg(sprintf("%sRemoving signing from %s",$spaces,$base_jar));
	
	#Without relying on zip to delete/insert files from a jar directly
	#We need to fully expand and then rejar the whole file
	my $expand_dir=&GetTempExpandDir("${jar_file}_unsign");
	
	if (&Expand($jar_file,$expand_dir,'',0)!=1) {
		return 0;
	}
	
	my $new_manifest='';
	if (&_CleanSigningFiles($expand_dir,\$new_manifest)!=1) {
		return 0;
	}
	
	#Create new jar using the new manifest
	unless(unlink($jar_file)) {
		&::LogMsg("Unable to remove $jar_file: $!");
		return 0;
	}
	if (&Create($jar_file,$expand_dir,'',$new_manifest,1)!=1) {
		return 0;
	}
	
	if (&RemoveExpandDir($expand_dir,1)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  Sign
# Desc: Sign a jar file, or record it as a jar that needs to be signed for later
# Arg:  jar_file,indent_level
# Ret:  1 on success, 0 on failure
#########################
sub JarFile::Sign {
	my ($jar_file,$indent_level)=@_;	
	
	#Ant's base dir will mess up relative file names
	if ($jar_file!~/^\//) {
		my $this_dir=cwd;
		$jar_file=&::ConcatPathString($this_dir,$jar_file);
	}
	
	my ($dir,$base_jar)=&::GetFileParts($jar_file,2);
	my $spaces=" "x($indent_level*3);
	
	if ($JarFile::SIGNING_ALIAS eq '') {
		#No signing alias registered
		&::LogMsg(sprintf("%sRegistering %s jar for manual signing",$spaces,$base_jar));
		push(@JarFile::JARS_TO_BE_SIGNED,$base_jar);
		return 1;
	}
	
	&::LogMsg(sprintf("%sSigning $base_jar with alias $JarFile::SIGNING_ALIAS",$spaces));
	
	return &_RunSignJar('signjar',$jar_file,$JarFile::SIGNING_ALIAS);
}

#########################
# Sub:  GetJarsToSign
# Desc: Get the list of jars that needed to be signed but could not due to missing signing alias
#       Note: this resets the internal tracking for signed jars so a new list is maintained after this point
# Arg:  None
# Ret:  List of jars needing signing, or () if nothing needs signing
#########################
sub JarFile::GetJarsToSign {
	my @to_sign=@JarFile::JARS_TO_BE_SIGNED;
	@JarFile::JARS_TO_BE_SIGNED=();
	return @to_sign;
}

#########################
# Sub:  CreateExpandDir
# Desc: Create a temporary expand area
# Arg:  expand_dir - The temporary expand area to remove and recreate
# Ret:  1 on success, 0 on failure
#########################
sub JarFile::CreateExpandDir {
	my ($expand_dir)=@_;
	
	if (&RemoveExpandDir($expand_dir,0)!=1) {
		return 0;
	}
	&::CreateDirs('0700',$expand_dir);
	
	return 1;
}

#########################
# Sub:  RemoveExpandDir
# Desc: Remove a temporary expand area
# Arg:  expand_dir - The temporary expand area to remove
#       silent - 1 if we should not talk about fight club
# Ret:  1 on success, 0 on failure
#########################
sub JarFile::RemoveExpandDir {
	my ($expand_dir,$silent)=@_;
	
	if (-d $expand_dir) {
		&::LogMsg("Cleaning $expand_dir") if ($silent!=1);
		my @output=`rm -rf "$expand_dir" 2>&1`;
		my $status=$?;
	
		if ($status!=0) {
			&::DebugOutput("rm output",@output);
			&::LogMsg("Failed to remove $expand_dir");
			return 0;
		}
	}
	return 1;
}

#########################
# Sub:  GetTempExpandDir
# Desc: Generate the path to a temporary area to expand files
# Arg:  jar_file - The jar file that will be expanded, to be used to influence the name of the expand area
# Ret:  Returns the temporary directory name
#########################
sub JarFile::GetTempExpandDir {
	my ($jar_file)=@_;
	
	my $temp_file=&::GetTempName();
	my ($temp_dir,$discard)=&::GetFileParts($temp_file,2);
	my ($discard_dir,$base_file)=&::GetFileParts($jar_file,2);
	
	my $full_temp=&::ConcatPathString($temp_dir,$base_file);
	
	return $full_temp;
}

#########################
# Sub:  RegisterSigningInfo
# Desc: Register signing info, so that JarFile::Sign can sign jars
# Arg:  key_alias - Alias of the private key to sign with
#       dest_dir - destination directory for this session
# Ret:  1
#########################
sub JarFile::RegisterSigningInfo {
	my ($key_alias,$dest_dir)=@_;
	
	$JarFile::SIGNING_ALIAS=$key_alias;
	$JarFile::DEST_DIR=$dest_dir;
	
	return 1 if $JarFile::SIGNING_ALIAS eq '';
	
	&::LogMsg("Verifying alias $key_alias in keystore");
	
	return &_RunSignJar('checkalias',$JarFile::SIGNING_ALIAS);
}

#########################
# Sub:  GetSignedJarPatterns
# Desc: Get the list of file name patterns that indicate this jar is signed
# Arg:  with_dir - 1 if the META-INF directory should be included in the pattern, otherwise it is not
# Ret:  ref to array of file name patterns
#########################
sub JarFile::GetSignedJarPatterns {
	my ($with_dir)=@_;
	
	my $meta_inf='';
	if ($with_dir==1) {
		$meta_inf='META-INF\/';
	}

	my @patterns=(
qr/^${meta_inf}[^\/]+\.dsa$/i,
qr/^${meta_inf}[^\/]+\.rsa$/i,
qr/^${meta_inf}[^\/]+\.sf$/i,
qr/^${meta_inf}[^\/]+\.ec$/i,
qr/^${meta_inf}SIG\-/,
);

	return \@patterns;
}

#########################
# Sub:  _CleanSigningFiles
# Desc: Clean the signing files from an expand directory
# Arg:  expand_dir
# Ret:  1 on success, 0 on failure
#########################
sub JarFile::_CleanSigningFiles {
	my ($expand_dir,$new_manifest_sref)=@_;
	
	#Remove *.dsa, *.rsa, *.sf
	my $meta_inf_dir=&::ConcatPathString($expand_dir,'META-INF');
	
	my $pattern_ref=&GetSignedJarPatterns(0);
	
	unless(opendir(METADIR,$meta_inf_dir)) {
		&::LogMsg("Unable to open directory $meta_inf_dir: $!");
		return 0;
	}
	my @remove_files=();
	while(my $this_file=readdir(METADIR)) {
		next if ($this_file eq '.' || $this_file eq '..');
		foreach my $pr (@$pattern_ref) {
			if ($this_file=~/$pr/) {
				push(@remove_files,&::ConcatPathString($meta_inf_dir,$this_file));
				last;
			}
		}
	}
	closedir(METADIR);
	
	foreach my $rm_file (@remove_files) {
		#&::LogMsg("DEBUG: Removing signing file $rm_file");
		unless(unlink($rm_file)) {
			&::LogMsg("Unable to remove signing file $rm_file: $!");
			return 0;
		}
	}
	
	#Create a new manifest file with all lines removed after the header section
	if (&_CreateShortManifestMF($meta_inf_dir,$new_manifest_sref)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  _CreateShortManifestMF
# Desc: Create a new manifest file with no signature lines, and remove the MANIFEST.MF
# Arg:  meta_inf_dir - path to the META-INF directory
# Ret:  1 on success, 0 on failure
#########################
sub JarFile::_CreateShortManifestMF {
	my ($meta_inf_dir,$new_manifest_sref)=@_;
	
	my $manifest_mf=&::ConcatPathString($meta_inf_dir,'MANIFEST.MF');
	$$new_manifest_sref=&::GetTempName('.mf');
	
	unless(open(MANIFEST,"<$manifest_mf")) {
		&::LogMsg("Unable to open expanded manifest $manifest_mf: $!");
		return 0;
	}
	my @all_lines=();
	my @current_section=();
	while(<MANIFEST>) {
		chomp;
		if (/^\s*$/) {
			#End of a section
			#save it if the section was not empty, did not have any digest lines, or is the first section
			if (scalar(@current_section)!=0 && 
			   (scalar(grep(/Digest:/,@current_section))==0 || scalar(@all_lines)==0)) {
				push(@all_lines,@current_section,'');
			}
			@current_section=();
		} else {
			push(@current_section,$_);
		}
	}
	close(MANIFEST);
	
	if (scalar(@current_section)!=0 && 
	   (scalar(grep(/Digest:/,@current_section))==0 || scalar(@all_lines)==0)) {
		push(@all_lines,@current_section,'');
	}
	
	unless(open(NEW_MANIFEST,">$$new_manifest_sref")) {
		&::LogMsg("Unable to open new manifest; $$new_manifest_sref: $!");
		return 0;
	}
	#Write all saved lines to our new manifest
	foreach my $line (@all_lines) {
		unless(print NEW_MANIFEST "$line\n") {
			&::LogMsg("Error while writing to $$new_manifest_sref: $!");
			close(NEW_MANIFEST);
			return 0;
		}
	}
	close(NEW_MANIFEST);
	
	unless(unlink($manifest_mf)) {
		&::LogMsg("Unable to remove $manifest_mf: $!");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  _RunSignJar
# Desc: Run the sign_jar.sh script in orpatch/deploy/bin
# Arg:  list of args to pass to sign_jar.sh
# Ret:  1 on success, 0 on failure
#########################
sub _RunSignJar {
	my (@args)=@_;
	
	my $arg_string=join(' ',@args);
	
	#JavaApp sets this during PrePatchCheck, but just to be safe, ensure JAVA_HOME is set
	if ($ENV{JAVA_HOME} eq '') {
		&::LogMsg("ERROR: JAVA_HOME not set for JarFile::Sign");
		return 0;
	}
	
	my $run_signjar=&::ConcatPathString($JarFile::DEST_DIR,'orpatch','deploy','bin','sign_jar.sh');
	
	my @output=`$run_signjar $arg_string 2>&1`;
	my $ret=$?;

	if ($ret!=0) {
		&::DebugOutput('sign_jar.sh output',@output);
		&::LogMsg("Error while executing sign_jar.sh!");
		return 0;
	}
	
	return 1;
}

1;