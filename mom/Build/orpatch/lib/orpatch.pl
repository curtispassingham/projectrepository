#!/usr/bin/perl
############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

#Accept MMHOME environment variable to accommodate auto-update from non-RETAIL_HOME orpatch shell script
if ($ENV{RETAIL_HOME} eq '') {
	if ($ENV{MMHOME} ne '') {
		$ENV{RETAIL_HOME}=$ENV{MMHOME};
	} else {
		die "ERROR: RETAIL_HOME must be defined!";
	}
}
$::ORP_DIR="$ENV{RETAIL_HOME}/orpatch";
$::ORP_LIB_DIR="$::ORP_DIR/lib";

require "$::ORP_LIB_DIR/orpatch_include.pl";

$::ORP_CONFIG=new ConfigFile();
$::ORP_GLOBAL_STATE=new ORPatchGlobalState($::ORP_DIR,'',$::ORP_CONFIG,$ENV{RETAIL_HOME},'RETAIL_HOME');

$::ORPATCH_MODE='APPLY';
$::ORP_SUPPORT_DIR='';
$::ORP_COMPARE_TYPE=1;
$::ORP_EXPORT_COMPARE=1;
$::ORP_SRC_NAME='';
$::ORP_SELF_ONLY=0;
$::ORP_SELF_UPDATE_RESTART=0;
$::ORP_SHOW_OBSOLETE=0;

&ValidateConfig();

@::ACTIONS=();
$::ORPATCH_NAME=&::PopulateActions(\@::ACTIONS,$::ORP_CONFIG);

#Save our unfiltered list of actions with the global state
$::ORP_GLOBAL_STATE->SetActionList(@::ACTIONS);

#register our global state with all actions
foreach my $action (@::ACTIONS) {
	$action->SetORPatchGlobalState($::ORP_GLOBAL_STATE);
}

#Allow actions to finalize their configuration
foreach my $action (@::ACTIONS) {
	$action->FinalizeConfiguration();
}

if ($::ORP_SELF_ONLY==1) {
	#If we are only operating on orpatch, remove all other actions
	my $orpatch_action=$::ACTIONS[0];
	@::ACTIONS=($orpatch_action);
}

#Inventory can be listed before we filter the action list
if ($::ORPATCH_MODE eq 'LSINVENTORY') {
	my $ret=&ListInventory($::ORP_SUPPORT_DIR,$::ORP_SHOW_OBSOLETE);
	&ExitORPatch((($ret==1) ? 0 : 1),0);
}

#Exporting manifest info is done before filtering the list
if ($::ORPATCH_MODE eq 'EXPORT') {
	my $ret=&ExportManifest(\@::ACTIONS,$::ORP_SUPPORT_DIR);
	&ExitORPatch((($ret==1) ? 0 : 1),0);
}

#Comparing manifest info is done before filtering the list
if ($::ORPATCH_MODE eq 'COMPARE') {
	my $ret=&CompareManifest(\@::ACTIONS,$::ORP_SUPPORT_DIR,$::ORP_COMPARE_TYPE,$::ORP_EXPORT_COMPARE,$::ORP_SRC_NAME);
	&ExitORPatch((($ret==1) ? 0 : 1),0);
}

#Reverting a patch is done before filtering the list
if ($::ORPATCH_MODE eq 'REVERT') {
	my $ret=&RevertPatch($::ACTIONS[0]);
	&ExitORPatch((($ret==1) ? 0 : 1),0);
}

my @host_actions=();
if (&FinalizeActionList(\@::ACTIONS,\@host_actions)!=1) {
	#Filtering/Finalizing Config failed, exit with failure
	&ExitORPatch(1,0);
}

#Analyzing the patch is done next
if ($::ORPATCH_MODE eq 'ANALYZE') {
	my $ret=&AnalyzePatchContents(\@host_actions,$::ORP_COMPARE_TYPE);
	&ExitORPatch((($ret==1) ? 0 : 1),0);
}

#At this point we are going to apply a patch

if (scalar(@host_actions)==0) {
	&::LogMsg("No files in this patch were recognized for action!");
	&ExitORPatch(1,0);
}

#ORPatch update is always the first of the actions, if files are included
my $first_action=shift(@host_actions);
if ($first_action->GetActionName() eq $::ORPATCH_NAME) {
	if (&CheckSelfUpdate($first_action)==1) {
		#We need to restart
		&::LogMsg("ORPatch was updated, restarting");
		&ExitORPatch(0,0);
	}
} else {
	#No orpatch files included, put the action back on the list
	unshift(@host_actions,$first_action);
}

my $exit_status=&ApplyPatch(@host_actions);

&::ExitORPatch($exit_status,1);

#########################
# Sub:  ApplyPatch
# Desc: run all the actions necessary to apply a patch
# Ret:  exit status to exit orpatch with (0 success, non-zero on error)
#########################
sub ApplyPatch {
	my @host_actions=@_;
	
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=$::ORP_GLOBAL_STATE->GetDetailsForActions();
	my $custom_hooks=$::ORP_GLOBAL_STATE->GetCustomHooks();
	
	my $orp_action_handle=$::ORP_GLOBAL_STATE->GetActionHandle('ORPATCH');
	#Call the custom pre-patch hook if there is one
	unless ($custom_hooks->RunCustomHook($orp_action_handle,'PATCH','START')==1) {
		&::LogMsg("Failed to run custom-hook prior to PATCH");
		return 4;
	}
	
	#First do pre-patch checks for all actions
	unless (&RunPreChecks(@host_actions)==1) {
		&::LogMsg("ORPatch aborting due to failed pre-patch checks");
		#pre-check failures are exit status 3
		return 3;
	}

	my $restart_phase=$restart_state->GetCurrentPhase();
	my $restore_dir=$restart_state->GetCurrentRestoreDir();
	
	my $restore_point=$::ORP_GLOBAL_STATE->GetRestorePoint();
	my $patch_info_cfg=$::ORP_GLOBAL_STATE->GetPatchInfo()->GetPatchInfoFile();
	
	if ($restore_dir ne '') {
		#We are restarting, continue using the previous restore dir
		if ($restore_point->ContinueRestorePoint($restore_dir)!=1) {
			return 5;
		}
	} else {
		my $rest_dir="backup-".&::GetTimeStamp('DATE-TIME');
		#We are not restarting, create a new restore dir
		if ($restore_point->StartRestorePoint($rest_dir,$patch_info_cfg,0)!=1) {
			return 6;
		}
		$restart_state->SetCurrentRestoreDir($restore_point->GetRestoreDir());
	}

	my $copy_phase_num=$restart_state->GetCopyPatchPhaseNum();
	
	my $exit_status=0;
	while(1) {
		my ($method,$phase_name)=$restart_state->GetORPMethodForPhase($restart_phase);
		if ($method eq '') {
			#We completed the last phase
			last;
		}
		
		#If we are starting with a specific action, skip ahead in the host_actions array
		my $restart_action=$restart_state->GetCurrentAction();
		my $index=0;
		if ($restart_action ne '') {
			$index=&::FindIndexForAction($restart_action,\@host_actions);
		}
		
		#Loop through actions, calling the phase method on each one
		for(;$index<scalar(@host_actions);$index++) {
			my $action=$host_actions[$index];
			my $name=$action->GetActionName();
			&::LogMsg("Processing $phase_name for action $name");
			$restart_state->SetAndSaveRestartPoint($name,$restart_phase);
			
			#Call the custom hook if there is one
			unless ($custom_hooks->RunCustomHook($action,$phase_name,'START')==1) {
				&::LogMsg("Failed to run custom-hook prior to $phase_name for $name");
				return 4;
			}
			
			#Run the action
			unless($action->$method($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)==1) {
				&::LogMsg("Failed to complete $phase_name for action: $name");
				return 2;
			}
			
			#Call the custom hook if there is one
			unless ($custom_hooks->RunCustomHook($action,$phase_name,'END')==1) {
				&::LogMsg("Failed to run custom-hook after $phase_name for $name");
				return 4;
			}
		}
		
		#If we just completed file copies, make sure that at least one action has claimed to have done some work
		if ($restart_phase==$copy_phase_num) {
			if ($::ORP_GLOBAL_STATE->GetPatchMadeChangesFlag()==0) {
				if ($::ORP_SELF_UPDATE_RESTART==1) {
					&::LogMsg("ORPatch updates complete, no other files were modified.  Skipping remaining actions");
				} else {
					&::LogMsg("No patch actions created, updated or removed files.  Skipping remaining actions");
				}
				#We have a successful completion but immediately return to avoid updating the 'applied' date in the patch inventory
				#by re-registering the patch
				return 0;
			}
		}
		
		$restart_phase=$restart_state->IncrementPhase();
	}

	#Call the custom post-patch hook if there is one
	unless ($custom_hooks->RunCustomHook($orp_action_handle,'PATCH','END')==1) {
		&::LogMsg("Failed to run custom-hook after PATCH");
		#The rest of the patch was applied successfully, don't abort
		#return 4;
	}
	
	#We only get here if all actions were successful
	#The check of exit status is not required, but seems safe
	if ($exit_status==0) {
		#Record the patch info
		unless ($::ORP_GLOBAL_STATE->RegisterPatchWithInventory()==1) {
			&::LogMsg("ERROR: Patch successfully applied, but registration in inventory failed!");
			$exit_status=4;
		}
	}
	
	return $exit_status;
}

#########################
# Sub:  RunPreChecks
# Desc: Run the precheck for each action on this host
# Ret:  1 if all are successful, 0 if not
#########################
sub RunPreChecks {
	my @host_actions=@_;

	my $phase_name='PRECHECK';
	my $custom_hooks=$::ORP_GLOBAL_STATE->GetCustomHooks();
	
	#Run full checks
	my %checks_run=();
	foreach my $action (@host_actions) {
		my $name=$action->GetActionName();
		
		#Call the custom hook if there is one
		unless ($custom_hooks->RunCustomHook($action,$phase_name,'START')==1) {
			&::LogMsg("Failed to run custom-hook prior to $phase_name for $name");
			return 0;
		}
			
		if ($action->PrePatchCheck(1)!=1) {
			&::LogMsg("Pre-patch check for action $name failed!");
			return 0;
		}
		$checks_run{$name}=1;
		
		#Call the custom hook if there is one
		unless ($custom_hooks->RunCustomHook($action,$phase_name,'END')==1) {
			&::LogMsg("Failed to run custom-hook after $phase_name for $name");
			return 0;
		}
	}

	#Get the list of other action checks that things we run are going to need
	my $check_ref=$::ORP_GLOBAL_STATE->GetExtraActionsForPreCheck();
	unless(defined($check_ref)) {
		return 0;
	}
	
	&::LogMsg("Checking dependent configuration, if any");
	
	#Run config-only pre-checks for any actions that haven't already been run
	foreach my $action (@$check_ref) {
		my $name=$action->GetActionName();
		if (exists($checks_run{$name})) {
			#Full check has already been run, no need to run a config-only pre-check
			next;
		} else {
			if ($action->PrePatchCheck(0)!=1) {
				&::LogMsg("Extra pre-patch config check for action $name failed!");
				return 0;
			}
		}
	}
	
	return 1;
}

#########################
# Sub:  CheckSelfUpdate
# Desc: Check if orpatch needs update and set the restart flag if so
# Ret:  1 if a restart is needed, 0 if not 
#########################
sub CheckSelfUpdate {
	my ($action)=@_;
	
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=$::ORP_GLOBAL_STATE->GetDetailsForActions();
	
	#We use the detailed form of CheckPatchForAction to ensure the files actually
	#are newer than what we have
	my ($proceed,@files)=$action->CheckPatchForAction(1);
	if ($proceed!=1) {
		&::LogMsg("Failed to calculate if self-update actions are necessary!");
		&ExitORPatch(1,0);
	}
	if (scalar(@files)==0) {
		if ($::ORP_SELF_ONLY==1) {
			if ($::ORP_SELF_UPDATE_RESTART==1) {
				&::LogMsg("ORPatch updates complete");
			} else {
				&::LogMsg("No necessary updates to ORPatch found in this patch");
			}
			#If there are no (more) files to update and we are only updating ourselves, we need to exit
			#to avoid registering the patch with our inventory
			&ExitORPatch(0,0);
		}
		return 0;
	}
	
	my $restore_point=$::ORP_GLOBAL_STATE->GetRestorePoint();
	my $patch_info_cfg=$::ORP_GLOBAL_STATE->GetPatchInfo()->GetPatchInfoFile();
	
	my $rest_dir="backup-".&::GetTimeStamp('DATE-TIME');
	#We are not restarting, create a new restore dir
	if ($restore_point->StartRestorePoint($rest_dir,$patch_info_cfg,1)!=1) {
		&::LogMsg("Failed initializing restore directory during self-patch, aborting!");
		&ExitORPatch(1,0);
	}
	
	#Call the custom pre-patch hook if there is one
	my $custom_hooks=$::ORP_GLOBAL_STATE->GetCustomHooks();
	unless ($custom_hooks->RunCustomHook($action,'COPYPATCHFILES','START')==1) {
		&::LogMsg("Failed to run custom-hook prior to CopyPatchFiles for ORPATCH");
		&ExitORPatch(1,0);
	}
	
	&::LogMsg("New ORPatch files included in patch, starting self-update");
	if ($action->CopyPatchFiles($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)!=1) {
		&::LogMsg("Failed to copy new orpatch files, aborting!");
		&ExitORPatch(1,0);
	}
	
	unless ($custom_hooks->RunCustomHook($action,'COPYPATCHFILES','END')==1) {
		&::LogMsg("Failed to run custom-hook after CopyPatchFiles for ORPATCH");
		&ExitORPatch(1,0);
	}
	
	#Set our restart flag that will be picked up by orpatch the shell script
	my $restart_flag=&::ConcatPathString($dest_dir,'orpatch/bin','orpatch_self_update.flg');
	open(RESTART_FLAG,">$restart_flag") || die "Unable to open $restart_flag: $!";
	print RESTART_FLAG "ORPATCH_RESTART=YES\n";
	close(RESTART_FLAG);
	
	#Make sure we sleep at least 1 second to ensure that any calculated restore directory is not the same as the one used for orpatch update
	sleep 1;
	
	return 1;
}

#########################
# Sub:  FinalizeActionList
# Desc: Take the complete list of actions and filter it down to: 
#          Things that apply to this patch
#          Things that are available on this host
#          Things that are the same or after our restart point
#########################
sub FinalizeActionList {
	my ($actions_ref,$host_actions_ref)=@_;
	
	my @patch_actions=();
	my %file_info=();
	
	my $orp_action=$actions_ref->[0];
	
	#Read the source manifest once, so we can pass to all the CheckPatchForAction calls
	$::ORP_GLOBAL_STATE->ReadSourceManifest();
	
	#Filter Action list by what is in the patch
	foreach my $action (@$actions_ref) {
		my ($proceed,@files)=$action->CheckPatchForAction(0);
		if ($proceed!=1) {
			&::LogMsg("Failed to calculate basic actions for patch, aborting!");
			return 0;
		}
		if (scalar(@files)==0) {
			#Action not included in this patch
			$action->DisableAction();
		} else {
			push(@patch_actions,$action);
			$file_info{$action->GetActionName()}=scalar(@files);
		}
	}	

	#Filter patch action list by what is on this host
	my @remember_actions=();
	foreach my $action (@patch_actions) {
		if ($action->IsValidOnThisHost()==1) {
			push(@$host_actions_ref,$action);
		} else {
			$action->DisableAction();
			push(@remember_actions,$action);
		}
	}

	&::LogMsg("--------------- Begin Patch Action Summary ------------");
	&::LogMsg("Actions that will be run on this Host:");
	foreach my $a (@$host_actions_ref) {
		my $name=$a->GetActionName();
		next if ($name eq $::ORPATCH_NAME);  #Don't show orpatch as an action
		&::LogMsg(sprintf("%20s : %5d files",$name,$file_info{$name}));
	}
	&::LogMsg("Actions that must be run elsewhere:");
	foreach my $a (@remember_actions) {
		my $name=$a->GetActionName();
		&::LogMsg(sprintf("%20s : %5d files",$name,$file_info{$name}));
	}
	&::LogMsg("---------------  End  Patch Action Summary ------------");
	
	foreach my $a (@$host_actions_ref) {
		if ($a->FinalizeEnabledConfiguration()!=1) {
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  FindIndexForAction
# Desc: Find the index into our action array for a specific action name
#########################
sub FindIndexForAction {
	my ($action_name,$action_list_ref)=@_;
	
	my $index=0;
	foreach my $action (@$action_list_ref) {
		my $this_name=$action->GetActionName();
		if ($this_name eq $action_name) {
			return $index;
		}
		$index++;
	}
	
	#Unknown action name, be safe and for the whole action list to be processed
	return 0;
}

#########################
# Sub:  AnalyzePatchContents
# Desc: Get the details on exactly what files will be updated by this patch
# Ret:  1 on success, 0 on failure
#########################
sub AnalyzePatchContents {
	my ($action_ref,$orp_detail_type)=@_;
	
	&::LogMsg("Analyzing details of patch against environment...");

	#Setup a detail analyze area
	my $analyze_details='';
	my $dest_dir=$::ORP_GLOBAL_STATE->GetDestDir();
	my $support_dir=&::ConcatPathString($dest_dir,'support');
	my $full_support_name=&::ConcatPathString($support_dir,'analyze');
	my $extra_details_always=(($orp_detail_type==1) ? 0 : 1);
	if (&ORPEnvMetadata::SetupAnalyzeDetailArea(\$analyze_details,$support_dir,$full_support_name,$::ORP_GLOBAL_STATE,$extra_details_always)!=1) {
		return 0;
	}
	
	#Calculate impacted files for each action
	my %updated_files=();
	foreach my $action (@$action_ref) {
		my ($proceed,@files)=$action->CheckPatchForAction(2);
		if ($proceed!=1) {
			&::LogMsg("Failed to analyze patch contents against actions, aborting!");
			return 0;
		}
		
		$updated_files{$action->GetActionName()}=\@files;
		
		if ($action->CreateAnalyzeDetailFiles($analyze_details)!=1) {
			return 0;
		}
	}
	
	my $patch_info_ref=$::ORP_GLOBAL_STATE->GetPatchInfo();
	
	if (&ORPEnvMetadata::PrintAnalyzeResults(\%updated_files,$patch_info_ref,$analyze_details)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  ExportManifest
# Desc: Collect all the manifest files and dbmanifest contents
# Ret:  1 on success, 0 on failure
#########################
sub ExportManifest {
	my ($action_list_ref,$support_name)=@_;
	
	my $dest_dir=$::ORP_GLOBAL_STATE->GetDestDir();
	my $support_dir=&::ConcatPathString($dest_dir,'support');
	my $full_support_name=&::ConcatPathString($support_dir,$support_name);
	
	if (-d $full_support_name) {
		&::LogMsg("ERROR: Specified export dir $full_support_name already exists.");
		return 0;
	}
	
	return &ORPEnvMetadata::Export($action_list_ref,$dest_dir,$support_dir,$full_support_name,$::ORP_GLOBAL_STATE,1);
}

#########################
# Sub:  CompareManifest
# Desc: Compare an exported set of metadata with this environment
# Ret:  1 on success, 0 on failure
#########################
sub CompareManifest {
	my ($action_list_ref,$support_name,$module_details,$export_for_compare,$src_name)=@_;
	
	my $dest_dir=$::ORP_GLOBAL_STATE->GetDestDir();
	my $support_dir=&::ConcatPathString($dest_dir,'support');
	my $full_support_name=&::ConcatPathString($support_dir,$support_name);
	
	if (!-d $full_support_name) {
		&::LogMsg("Specified export dir $full_support_name does not exist!");
		return 0;
	}
	
	return &ORPEnvMetadata::Compare($action_list_ref,$dest_dir,$support_dir,$full_support_name,$export_for_compare,$module_details,$::ORP_GLOBAL_STATE,$src_name);
}

#########################
# Sub:  RevertPatch
# Desc: restore the environment from a restore point
# Ret:  1 on success, 0 on failure
#########################
sub RevertPatch {
	my ($action)=@_;
	
	my $dest_dir=$::ORP_GLOBAL_STATE->GetDestDir();
	my $restore_dir=$::ORP_GLOBAL_STATE->GetSourceDir();
	
	#We don't run ValidateConfig, so have to check this ourselves
	unless (-d $dest_dir ) {
		&::LogMsg("Destination dir: $dest_dir does not exist, ensure RETAIL_HOME is correctly defined");
		return 0;
	}

	my $restore_point=$::ORP_GLOBAL_STATE->GetRestorePoint();
	
	return $restore_point->RestoreFiles($restore_dir,$action,$::ORP_GLOBAL_STATE);
}

#########################
# Sub:  ListInventory
# Desc: Print either our inventory, or the inventory of an exported metadata directory
# Ret:  1 on success, 0 on failure
#########################
sub ListInventory {
	my ($support_name,$show_obsolete)=@_;
	
	my $ret=1;
	if ($support_name eq '') {
		$ret=$::ORP_GLOBAL_STATE->GetPatchInventory()->PrintInventory(1,$show_obsolete);
	} else {
		my $dest_dir=$::ORP_GLOBAL_STATE->GetDestDir();
		my $support_dir=&::ConcatPathString($dest_dir,'support');
		my $full_support_name=&::ConcatPathString($support_dir,$support_name);
		
		$ret=&ORPEnvMetadata::ListExportedInventory($dest_dir,$full_support_name,$::ORP_GLOBAL_STATE,$support_name,$show_obsolete);
	}
	
	return $ret;
}

#########################
# Sub:  ValidateConfig
# Desc: parse our command-line arguments and validate our configuration
#########################
sub ValidateConfig {
	$::ORPATCH_MODE='';
	my $reset_state_file=0;
	my $fresh_install=0;
	while(my $arg=shift(@ARGV)) {
		$arg=~tr/a-z/A-Z/;
		if ($arg eq 'APPLY') {
			$::ORPATCH_MODE='APPLY';
		} elsif ($arg eq 'LSINVENTORY' || $arg eq '-LSINVENTORY') {
			$::ORPATCH_MODE='LSINVENTORY';
		} elsif ($arg eq 'ANALYZE' || $arg eq '-ANALYZEONLY') {
			$::ORPATCH_MODE='ANALYZE';
			&::LogMsg("Analyzing patch contents versus environment, only");
		} elsif ($arg eq 'EXPORTMETADATA') {
			$::ORPATCH_MODE='EXPORT';
		} elsif ($arg eq 'DIFFMETADATA') {
			$::ORPATCH_MODE='COMPARE';
		} elsif ($arg eq 'REVERT') {
			$::ORPATCH_MODE='REVERT';
		} elsif ($arg eq '-S') {
			$::ORP_GLOBAL_STATE->SetSourceDir(shift(@ARGV));
		} elsif ($arg eq '-NEW') {
			$reset_state_file=1;
		} elsif ($arg eq '-RESTARTFILE') {
			my $restart_file=shift(@ARGV);
			if ($restart_file eq '') {
				&::LogMsg("-RESTARTFILE option requires an argument");
				&Usage();
			}
			if ($restart_file=~/\//) {
				&::LogMsg("-RESTARTFILE argument must be only a file name without path");
				&Usage();
			}
			#Point our restart state at the specified file in the same directory as norml restart files
			my $restart_state=$::ORP_GLOBAL_STATE->GetRestartState();
			my ($rs_dir,$rs_file)=&::GetFileParts($restart_state->GetRestartFile(),2);
			$restart_state->SetRestartFile(&::ConcatPathString($rs_dir,$restart_file));
		} elsif ($arg eq '-EXPNAME') {
			$::ORP_SUPPORT_DIR=shift(@ARGV);
			if ($::ORP_SUPPORT_DIR eq '') {
				&::LogMsg("-EXPNAME option requires an argument");
				&Usage();
			}
		} elsif ($arg eq '-SRCNAME') {
			$::ORP_SRC_NAME=shift(@ARGV);
			if ($::ORP_SRC_NAME eq '') {
				&::LogMsg("-SRCNAME option requires an argument");
				&Usage();
			}
		} elsif ($arg eq '-DBMODULES') {
			if ($::ORP_COMPARE_TYPE!=1) {
				&::LogMsg("WARNING: both -DBMODULES and -JARMODULES specified, only one can be chosen at a time.  Using -DBMODULES");
			}
			$::ORP_COMPARE_TYPE=2;
		} elsif ($arg eq '-JARMODULES') {
			if ($::ORP_COMPARE_TYPE!=1) {
				&::LogMsg("WARNING: both -DBMODULES and -JARMODULES specified, only one can be chosen at a time.  Using -JARMODULES");
			}
			$::ORP_COMPARE_TYPE=3;
		} elsif ($arg eq '-NOEXPORTCOMPARE') {
			$::ORP_EXPORT_COMPARE=0;
		} elsif ($arg eq '-SELFONLY') {
			$::ORP_SELF_ONLY=1;
		} elsif ($arg eq '-SHOWOBSOLETE') {
			$::ORP_SHOW_OBSOLETE=1;
		} elsif ($arg eq '-RESTARTED') {
			$::ORP_SELF_UPDATE_RESTART=1;
		} elsif ($arg eq '-FRESHINSTALL') {
			$fresh_install=1;
		} elsif ($arg eq '-HELP') {
			&Usage();
		} else {
			&::LogMsg("Unknown argument: $arg");
			#unknown arguments might be new things that are added after a self-patch
			#don't abort, just ignore
			#&Usage();
		}
	}
	
	if ($::ORPATCH_MODE eq '') {
		&::LogMsg("Error: ORPatch mode must be specified!");
		&Usage();
	}
	
	#We do this after parsing all arguments to ensure that -new and -restartfile specified
	#on the same command-line with -new first doesn't reset the standard state file
	if ($reset_state_file==1) {
		&::LogMsg("New orpatch session requested, removing existing state information");
		$::ORP_GLOBAL_STATE->ResetRestartState();
	}
	
	#We set this up right away in case they are lsinventory
	unless ($::ORP_GLOBAL_STATE->OpenPatchInventory()==1) {
		exit 1;
	}
	
	my $env_info_cfg="$::ORP_DIR/config/env_info.cfg";
	unless (-f $env_info_cfg) {
		&::LogMsg("Environment info file $env_info_cfg not found");
		exit 1;
	}
	
	if ($::ORP_CONFIG->LoadFile($env_info_cfg)!=1) {
		&::LogMsg("Unable to read env_info.cfg!");
		exit 1;
	}

	return 1 if ($::ORPATCH_MODE eq 'LSINVENTORY');
	
	if ($::ORPATCH_MODE eq 'EXPORT' || $::ORPATCH_MODE eq 'COMPARE') {
		if ($::ORP_SUPPORT_DIR eq '') {
			&::LogMsg("exportmetadata and diffmetadata require -EXPNAME parameter");
			&Usage();
		}
	}
	
	if ($::ORPATCH_MODE eq 'COMPARE' && $::ORP_SRC_NAME eq '') {
		&::LogMsg("diffmetadata requires -SRCNAME parameter");
		&Usage();
	}
	
	return 1 if ($::ORPATCH_MODE eq 'EXPORT' || $::ORPATCH_MODE eq 'COMPARE');
	
	my $sdir=$::ORP_GLOBAL_STATE->GetSourceDir();
	
	if ($::ORPATCH_MODE eq 'REVERT') {
		if ($sdir eq '') {
			&::LogMsg("Required argument -s <restore point dir> missing");
			&::Usage();
		}
		#No further standard validation applies
		return 1;
	}
	
	#Apply and analyze require a source dir
	if ($sdir eq '') {
		&::LogMsg("Required argument -s <source patch dir> missing");
		&::Usage();
	}
	
	#Do not load the restart file if this is a selfonly patch or we are just analyzing something
	my $skip_load_restart=0;
	if ($::ORP_SELF_ONLY==1 || $::ORPATCH_MODE eq 'ANALYZE') {
		$skip_load_restart=1;
	}
	
	unless ($::ORP_GLOBAL_STATE->ValidateConfig($skip_load_restart)==1) {
		exit 1;
	}
	
	my $nls_lang=$::ORP_CONFIG->Get('NLS_LANG');
	if ($nls_lang ne '') {
		&::LogMsg("Setting NLS_LANG = $nls_lang");
		$ENV{NLS_LANG}=$nls_lang;
	}
	
	my $patch_mode=$::ORP_GLOBAL_STATE->GetPatchMode();
	$::ORP_CONFIG->Set('PATCH_MODE',$patch_mode);
	
	if ($fresh_install==1) {
		if ($skip_load_restart==1) {
			&::LogMsg("-FRESHINSTALL cannot be specified with selfonly applies, or analyze.  Ignoring");
		} elsif ($patch_mode eq 'HOTFIX') {
			&::LogMsg("-FRESHINSTALL cannot be specified with incremental patches. Ignoring");
		} else {
			#Register that this is a fresh install with the restart state
			my $restart_state=$::ORP_GLOBAL_STATE->GetRestartState();
			$restart_state->SetCurrentFreshInstall(1);
		}
	}
	
	return 1;
}

sub ExitORPatch {
	my ($exit_status,$allow_remove)=@_;
	$allow_remove=0 if ($allow_remove eq '');
	
	#If we are exiting successfully, and allowed to remove the restart state, remove it
	if ($exit_status==0 && $allow_remove==1) {
		$::ORP_GLOBAL_STATE->ResetRestartState();
	}
	
	my $success=($exit_status==0) ? "successfully" : "with errors";
	
	&::LogMsg("ORPatch session completed $success");
	exit $exit_status;
}

sub Usage {
	&::LogMsg("Usage:");
	&::LogMsg("");
	&::LogMsg("Applying a patch:");
	&::LogMsg("  orpatch apply -s <source patch directory> [-new] [-selfonly]");
	&::LogMsg("     The argument to -s must point to the directory containing a manifest.csv and patch_info.cfg");
	&::LogMsg("     The -new option will ignore any existing restart state files that may be present");
	&::LogMsg("     The -selfonly option will only update orpatch files");
	&::LogMsg("Analyzing changes in a patch:");
	&::LogMsg("  orpatch analyze -s <source patch directory>");
	&::LogMsg("Listing inventory of patches in this RETAIL_HOME:");
	&::LogMsg("  orpatch lsinventory [-showobsolete]");
	&::LogMsg("Exporting environment metadata:");
	&::LogMsg("  orpatch exportmetadata -expname <name>");
	&::LogMsg("Comparing environment metadata:");
	&::LogMsg("  orpatch diffmetadata -expname <name> -srcname <name> [-dbmodules|-jarmodules]");
	exit 1;
}
