#!/usr/bin/perl
############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;
use File::Copy;

($ENV{RETAIL_HOME} eq '') && (die "ERROR: RETAIL_HOME must be defined!");
$::ORP_DIR="$ENV{RETAIL_HOME}/orpatch";
$::ORP_LIB_DIR="$::ORP_DIR/lib";

require "$::ORP_LIB_DIR/orpatch_include.pl";

$::SOURCE_DIR='';
$::DEST_DIR='';
$::MERGED_NAME='';
$::MANIFEST_NAME='manifest.csv';
$::DELETE_MANIFEST_NAME='delete_manifest.csv';
$::PATCH_INFO_NAME='patch_info.cfg';
$::SKIP_NON_PATCH=0;
$::IN_PLACE_MERGE=0;
$::PACKAGE_PATCH_PREFIX=0;

&ValidateConfig();

my $ret=&MergePatches($::SOURCE_DIR,$::DEST_DIR,$::MERGED_NAME);

&::LogMsg("ORMerge session complete");

exit (($ret==1) ? 0 : 1);

#########################
# Sub:  MergePatches
# Desc: Merge all patches in source directory into a single patch in dest dir
#########################
sub MergePatches {
	my ($source,$dest,$merged_name)=@_;

	my $patch_list=&FindPatchesInSourceDir($source);
	
	#If we are in place merging, it is legal to have only one patch in source, otherwise two are required
	my $min_patches=(($::IN_PLACE_MERGE==1) ? 1 : 2);
	if (scalar(@$patch_list)<$min_patches) {
		&::LogMsg("ERROR: Merging patches requires at least $min_patches patches in source directory!");
		return 0;
	}

	my $merged_source_info_ref=[];
	my $merged_delete_info_ref=[];
	my $source_ver_lookup_ref={};
	my $delete_ver_lookup_ref={};
	
	my $dest_patch_info='';
	if ($::IN_PLACE_MERGE==1) {
		my $success=0;
		($success,$dest_patch_info)=&ReadInPlaceDest($dest,$merged_source_info_ref,$merged_delete_info_ref,$source_ver_lookup_ref,$delete_ver_lookup_ref);
		unless($success==1) {
			&::LogMsg("Failed to read destination patch for in place merge!");
			return 0;
		}
	}
	
	my ($sorted_list,$merged_patch_type,$merged_product_list,$all_name_ref)=&SortAndValidatePatchList($patch_list,$dest_patch_info);
	
	&::LogMsg("Creating merge patch...");
	
	foreach my $ref (@$sorted_list) {
		my ($si_ref,$pi_ref,$patch_path,$di_ref,$patch_prefix)=@$ref;
		unless(&MergePatchToDest($si_ref,$pi_ref,$patch_path,$merged_source_info_ref,$dest,$di_ref,$merged_delete_info_ref,$source_ver_lookup_ref,$delete_ver_lookup_ref,$patch_prefix)) {
			&::LogMsg("Failed to merge patch!");
			return 0;
		}
	}
	
	my $sorted_msi_ref=&SortMergedManifest($merged_source_info_ref,$dest);

	my $used_patch_ref='';
	($used_patch_ref,$merged_name)=&PrintMergeSummary($sorted_msi_ref,$merged_name,$merged_product_list,$merged_patch_type,$merged_delete_info_ref);
	
	&::LogMsg("Saving patch metadata for merged patch");
	
	#Write merged manifest
	unless(&SaveMergedManifest($dest,$sorted_msi_ref,$::MANIFEST_NAME)) {
		&::LogMsg("Failed to save merged manifest!");
		return 0;
	}
	
	if (scalar(@$merged_delete_info_ref)!=0) {
		unless(&SaveMergedManifest($dest,$merged_delete_info_ref,$::DELETE_MANIFEST_NAME)) {
			&::LogMsg("Failed to save merged delete manifest!");
			return 0;
		}
	}
	
	#Write merged patch_info.cfg
	unless(&SaveMergedPatchInfo($dest,$merged_name,$merged_patch_type,$merged_product_list,$used_patch_ref)) {
		&::LogMsg("Failed to save merged patch info!");
		return 0;
	}
	
	&::LogMsg("Merge patch created successfully!");
	
	return 1;
}

#########################
# Sub:  PrintMergeSummary
# Desc: Print a summary of the merge patch
# Ret:  The list of patches that actually contributed to the patch
#########################
sub PrintMergeSummary {
	my ($msi_ref,$mp_name,$mp_plist,$mp_ptype,$mdi_ref)=@_;

	#Determine statistics on where files came from
	my %used_names=();
	my %patch_file_count=();
	foreach my $ref (@$msi_ref) {
		my ($sfile,$frevision,$ftype,$pname,@discard)=&Manifest::DecodeSourceLine($ref);
		$patch_file_count{$pname}++;
		$used_names{$pname}++;
	}
	my %patch_delete_file_count=();
	foreach my $ref (@$mdi_ref) {
		my ($sfile,$frevision,$ftype,$pname,@discard)=&Manifest::DecodeSourceLine($ref);
		$patch_delete_file_count{$pname}++;
		$used_names{$pname}++;
	}
	
	my @used_patches=sort(keys(%used_names));
	
	if ($mp_name eq '') {
		#Generate a name from the used patches
		$mp_name=join(':',@used_patches);
		$mp_name=substr($mp_name,0,200);
	}

	my $cummulative_patch_type=&ORPatchInfo::GetCummulativePatchType();
	&::LogMsg(sprintf("Final merged patch $mp_name for product $mp_plist (%s)",($mp_ptype eq $cummulative_patch_type) ? 'Cummulative' : 'Incremental'));
	
	foreach my $pname (@used_patches) {
		if ($patch_file_count{$pname} ne '') {
			&::LogMsg(sprintf("%5d files from $pname",$patch_file_count{$pname}));
		}
		if ($patch_delete_file_count{$pname} ne '') {
			&::LogMsg(sprintf("%5d delete files from $pname",$patch_delete_file_count{$pname}));
		}
	}

	return (\@used_patches,$mp_name);
}

#########################
# Sub:  SaveMergedManifest
# Desc: Save the merged manifest into the merged patch area
# Args: ref to source patch manifest info
# Rets: 1 if successful, 0 if not
#########################
sub SaveMergedManifest {
	my ($dest_dir,$msi_ref,$manifest_name)=@_;
	
	my $merged_manifest=&::ConcatPathString($dest_dir,$manifest_name);
	return &Manifest::WriteSourceManifest($merged_manifest,$msi_ref);
}

#########################
# Sub:  SaveMergedPatchInfo
# Desc: Save the merged patch info the merged patch area
# Args: dest_dir,merged_name,merged patch type, merged product list
# Rets: 1 if successful, 0 if not
#########################
sub SaveMergedPatchInfo {
	my ($dest_dir,$merged_name,$merged_patch_type,$merged_product_list,$all_name_ref)=@_;
	
	my $merged_patch_info=&::ConcatPathString($dest_dir,$::PATCH_INFO_NAME);
	unless(open(PATCH_INFO,">$merged_patch_info")) {
		&::LogMsg("Unable to open $merged_patch_info: $!");
		return 0;
	}
	print PATCH_INFO <<ENDPRINT;
PRODUCT_NAME=$merged_product_list
PATCH_TYPE=$merged_patch_type
PATCH_NAME=$merged_name
MERGED_PATCH=Y
ENDPRINT

	#Save the merged patch info
	for(my $i=1;$i<=scalar(@$all_name_ref);$i++) {
		print PATCH_INFO "SUBPATCH_NAME_${i}=".$$all_name_ref[$i-1]."\n";
	}

	close(PATCH_INFO);
	return 1;
}

#########################
# Sub:  MergePatchToDest
# Desc: Merge a patch into our destination area
# Args: ref to patch source info, ref to source patch info, path to patch, ref to merged source info,dest dir,ref to delete info, ref to merged delete info, patch_prefix
# Rets: 1 if successful, 0 if not
#########################
sub MergePatchToDest {
	my ($si_ref,$pi_ref,$patch_path,$msi_ref,$dest_dir,$di_ref,$mdi_ref,$source_ver_lookup_ref,$delete_ver_lookup_ref,$patch_prefix)=@_;
	
	my $patch_name=$pi_ref->GetPatchName();
	
	#Merge in the regular manifest files, copying them
	if (&MergeAndCopyFiles($si_ref,$msi_ref,$patch_name,$patch_path,$dest_dir,$source_ver_lookup_ref,$patch_prefix)!=1) {
		return 0;
	}
	
	#Merge in the delete manifest files, which are not copied
	if (&MergeAndCopyFiles($di_ref,$mdi_ref,$patch_name,'','',$delete_ver_lookup_ref,$patch_prefix)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  MergeAndCopyFiles
# Desc: Take a source info array and merge it into the destination area's manifest.  If the source info came from a manifest.csv
#       then the files are copied into the destination area.  If the source info came from a delete_manifest
#       then there are no files to copy.
# Args: ref to patch source info, ref to merged source info, path to patch, dest dir, patch_prefix
# Rets: 1 if successful, 0 if not
#########################
sub MergeAndCopyFiles {
	my ($si_ref,$msi_ref,$patch_name,$patch_path,$dest_dir,$v_lookup_ref,$patch_prefix)=@_;
	
	my $total=scalar(@$si_ref);
	
	my $files_text=($patch_path ne '') ? 'files' : 'delete files';
	&::LogMsg(sprintf("Patch %s contains %d %s",$patch_name,$total,$files_text));

	#No files to process
	if ($total==0) {
		return 1;
	}
	
	my $copied=0;
	foreach my $ref (@$si_ref) {
		my ($sfile,$frevision,$ftype,$pname,$unit_test,$localization,$customized,$fwp_rev,$future1,$future2,$future3,$future4)=&Manifest::DecodeSourceLine($ref);
		
		#If the file was from a different patch than the patch name, carry that forward
		if ($pname eq '') {
			$pname=$patch_name;
		}
		
		#We skip files without a type because they can't be interesting to actions
		if ($ftype eq '') {
			$::LOG->LogMsgNoVerbose("File $sfile has no file type, skipping");
			next;
		}
		
		#We skip files with unit_test type because they are internal only
		if ($unit_test ne '') {
			$::LOG->LogMsgNoVerbose("File $sfile is a unit test file, skipping");
			next;
		}
		
		#If we are packaging with prefixes, generate the destination file name
		my $prefixed_sfile=$sfile;
		my $suffixed_dest_dir=$dest_dir;
		if ($::PACKAGE_PATCH_PREFIX==1) {
			$prefixed_sfile=&::ConcatPathString($patch_prefix,$sfile);
			$suffixed_dest_dir=&::ConcatPathString($dest_dir,$patch_prefix);
		}
		
		#Check if this file is newer than the current file in the merged destination
		if (&MergeFileIsNewer($prefixed_sfile,$frevision,$v_lookup_ref)) {
			if ($patch_path ne '') {
				#We copy sfile to suffixed_dest_dir because sfile is the true name of the file
				#and suffixed_dest_dir will ensure that the file ends up with the patch_prefix
				unless(&CopyPatchFile($sfile,$patch_path,$suffixed_dest_dir)) {
					&::LogMsg("Error while copying patch files, aborting");
					return 0;
				}
			}
			$copied++;

			#Now update our intended merged manifest
			&Manifest::UpdateSourceInfo($prefixed_sfile,$frevision,$ftype,$unit_test,$localization,$pname,$customized,$fwp_rev,$future1,$future2,$future3,$future4,$msi_ref); 
			#Update our cross reference
			$v_lookup_ref->{$prefixed_sfile}=$frevision;
		} else {
			#We only print this to the log
			$::LOG->LogMsgNoVerbose("File $prefixed_sfile:$frevision is older then current version in merged patch");
		}
	}
	
	if ($copied!=0) {
		&::LogMsg("  $copied/$total $files_text had updates and were merged");
	}
	my $not_copied=$total-$copied;
	if ($not_copied!=0) {
		&::LogMsg("  $not_copied/$total $files_text did not need to be merged");
	}

	return 1;
}

###################
# Sub:   CopyPatchFile
# Desc:  Copy a patch file into the merge area
#        Note: the merged manifest is NOT updated in this function
# Args:  sfile - The source manifest's filename
#        patch_path - The path to the source directory of this patch
#        dst_dir - The path to the merged directory
# Ret:   1 if the file was successfully copied, 0 if not
####################
sub CopyPatchFile {
	my ($sfile,$patch_path,$dst_dir)=@_;
	
	my $full_source_file=&::ConcatPathString($patch_path,$sfile);
	my $full_dest_file=&::ConcatPathString($dst_dir,$sfile);
	
	my ($final_dir_string,$junk)=&::GetFileParts($full_dest_file,2);
	#Create our directory tree if necessary
	&::CreateDirs('0755',$final_dir_string);

	if (! -f $full_source_file) {
		&::LogMsg("ERROR: Source File $full_source_file does not exist!");
		return 0;
	}

	#Copy the file
	$::LOG->LogMsgNoVerbose("Copying $sfile to $full_dest_file");
	if (&File::Copy::copy($full_source_file,$full_dest_file)) {
		#Successful
	} else {
		#Copy failed!
		&::LogMsg("Unable to copy $full_source_file to $full_dest_file: $!");
		return 0;
	}
	  
	return 1;
}


###################
# Sub:   MergeFileIsNewer
# Desc:  Check if a file included in the patch is newer than what is
#        in our merged patch area
# Args:  fname - The source file name
#        frevision - The source file revision
#        v_lookup_ref - A ref to a hash mapping source file names to revisions currently in the merged patch
# Ret:   1 if the patch file is newer, 0 if not
####################
sub MergeFileIsNewer {
	my ($fname,$frevision,$v_lookup_ref)=@_;
	
	my $msi_frevision=$v_lookup_ref->{$fname};
	
	if ($msi_frevision ne '') {
		my $compare_result=&PatchFileCopy::RevisionCompare($frevision,$msi_frevision);
		if ($compare_result==1) {
			$::LOG->LogMsgNoVerbose("$fname:$msi_frevision will be updated to revision $frevision");
			return 1;
		} else {
			return 0;
		}
	}

	#Unable to find the file, treat it as new
	return 1;
}

#########################
# Sub:  SortAndValidatePatchList
# Desc: Sort a list of patches by size of manifest, validate they are compatible
# Args: ref to patch_list as returned by FindPatchesInSourceDir
# Rets: ref to a sorted list,merged patch type,merged product list
#########################
sub SortAndValidatePatchList {
	my ($patch_list,$dest_patch_info)=@_;
	
	#Currently all patches are considered compatible
	
	my %product_list=();
	my $merged_patch_type=&ORPatchInfo::GetIncrementalPatchType();
	my @all_names=();
	
	my @loop_patch_list=@$patch_list;
	
	#If we are merging in place, include the patch info from the destination as the first patch info
	if ($dest_patch_info ne '') {
		unshift(@loop_patch_list,['',$dest_patch_info]);
	}
	
	&::LogMsg("Patches to merge:");
	foreach my $ref (@loop_patch_list) {
		my $pi_ref=$$ref[1];
		
		#Product name could already be a list, if this is a merged patch
		foreach my $p ($pi_ref->GetProductList()) {
			$product_list{$p}=1;
		}
		
		#One cumulative patch makes the merged patch cumulative
		if ($pi_ref->IsPatchCummulative()) {
			$merged_patch_type=$pi_ref->GetCummulativePatchType();
		}
		
		if ($pi_ref->IsMergedPatch()) {
			#If it was already a merged patch, include all the subpatches
			my @sub_patches=$pi_ref->GetPatchList();
			push(@all_names,@sub_patches);
		} else {
			#It was a regular patch, include the name
			push(@all_names,$pi_ref->GetPatchName());
		}
		
		&::LogMsg(sprintf("  Patch %s for product %s (%s)",$pi_ref->GetPatchName(),$pi_ref->GetProductName(),$pi_ref->GetPatchType()));
	}
	my $merged_product_list=join(',',sort(keys(%product_list)));
	
	#Sort by size of source manifest info
	#a and b are references to [source_info_ref,patch_info_ref,patch_dir_path,patch_prefix]
	#so we take the first element (the source info ref), access it as an array and then get it's length
	#by converting to a scalar.  We compare b to a to sort in descending size order
	my @sorted_list=sort {scalar(@{$$b[0]}) <=> scalar(@{$$a[0]})} @$patch_list;
	
	return (\@sorted_list,$merged_patch_type,$merged_product_list,\@all_names);
}

#########################
# Sub:  FindPatchesInSourceDir
# Desc: Find all of the manifest files in the source directory
# Args: source - The source directory
# Rets: [[source_manifest_info_ref1,patch_info1,patch_dir1,patch_prefix1],[smi_ref2,patch_info2,patch_dir2,patch_prefix2],etc]
#########################
sub FindPatchesInSourceDir {
	my ($source)=@_;
	
	&::LogMsg("Looking for patches in source directory $source");
	
	my @patch_list=();
	unless(opendir(SOURCE_DIR,"$source")) {
		&::LogMsg("Unable to open source directory $source: $!");
		exit 1;
	}
	while(my $this_file=readdir(SOURCE_DIR)) {
		next if ($this_file eq '.' || $this_file eq '..');
		
		my $full_path=&::ConcatPathString($source,$this_file);

		next unless (-d $full_path);
		
		my ($smi_ref,$pi_ref,$sdi_ref)=&ReadPatchDir($full_path);
		
		if ($smi_ref ne '') {
			push(@patch_list,[$smi_ref,$pi_ref,$full_path,$sdi_ref,$this_file]);
		} else {
			#Problem reading a particular patch dir
			if ($::SKIP_NON_PATCH==1) {
				&::LogMsg("Directory $this_file does not appear to be a patch, skipping");
			} else {
				&::LogMsg("Unable to process patch dir $this_file in $source, aborting");
				exit 1;
			}
		}
    }
    closedir(SOURCE_DIR);
	
	return \@patch_list;
}

#########################
# Sub:  SortMergedManifest
# Desc: Sort the merged manifest
# Args: ref to the merged patch manifest info
#########################
sub SortMergedManifest {
	my ($mpi_ref,$dest_dir)=@_;
	
	#Silence warning about using embedded_file_source only once
	my $count=scalar(@ORManifestOrder::embedded_file_source);
	
	my @embed=();
	foreach my $ref (@ORManifestOrder::embedded_file_source) {
		my ($type,$type_pat,$file)=@$ref;
		push(@embed,[$type,$type_pat,&::ConcatPathString($dest_dir,$file)]);
	}

	&ORManifestOrder::SQL_Order_From_File(@embed);
	
	my @sorted_array=sort ORManifestOrder::SortSourceManifest @$mpi_ref;
	
	return \@sorted_array;
}

#########################
# Sub:  ReadPatchDir
# Desc: Read the manifest and patch info in a patch directory
#########################
sub ReadPatchDir {
	my ($patch_dir)=@_;
	
	my $manifest=&::ConcatPathString($patch_dir,$::MANIFEST_NAME);
	my $patch_info=&::ConcatPathString($patch_dir,$::PATCH_INFO_NAME);
	my $delete_manifest=&::ConcatPathString($patch_dir,$::DELETE_MANIFEST_NAME);
	
	my $pi_ref=new ORPatchInfo($patch_info);
	unless ($pi_ref->ReadPatchInfo()) {
		&::LogMsg("Unable to read patch info from $patch_dir");
		return ('','');
	}
	
	unless(-f $manifest) {
		&::LogMsg("Directory $patch_dir is missing $::MANIFEST_NAME");
		return ('','');
	}
	&::LogMsg("Reading patch manifest $manifest");
	my @smi=&Manifest::ReadManifest($manifest);
	
	#Check if they have a delete manifest
	my @del_source_info=();
	if (-f $delete_manifest) {
		&::LogMsg("Reading delete manifest $delete_manifest");
		@del_source_info=&Manifest::ReadManifest($delete_manifest);
	}

	return (\@smi,$pi_ref,\@del_source_info);
}

#########################
# Sub:  ReadInPlaceDest
# Desc: Read the manifest and patch info in the destination area for an in place merge
#########################
sub ReadInPlaceDest {
	my ($dest,$merged_source_info_ref,$merged_delete_info_ref,$source_ver_lookup_ref,$delete_ver_lookup_ref)=@_;
	
	&::LogMsg("Evaluating in-place destination patch");
	
	my ($dmi_ref,$pi_ref,$ddi_ref)=&ReadPatchDir($dest);
	if ($dmi_ref eq '') {
		return (0,'');
	}
	
	#Initialize our manifest and delete manifest arrays
	@$merged_source_info_ref=@$dmi_ref;
	@$merged_delete_info_ref=@$ddi_ref;

	#Initialize the lookup hashes used for file copy determinations
	foreach my $ref (@$dmi_ref) {
		my ($sfile,$frevision,@discard)=&Manifest::DecodeSourceLine($ref);
		$source_ver_lookup_ref->{$sfile}=$frevision;
	}
	foreach my $ref (@$ddi_ref) {
		my ($sfile,$frevision,@discard)=&Manifest::DecodeSourceLine($ref);
		$delete_ver_lookup_ref->{$sfile}=$frevision;
	}
	
	return (1,$pi_ref);
}

#########################
# Sub:  IsDirEmpty
# Desc: Check if a directory is empty
#########################
sub IsDirEmpty {
	my ($dir)=@_;
	
	unless(opendir(CHECK_DIR,"$dir")) {
		&::LogMsg("Unable to open directory $dir: $!");
		exit 1;
	}
	my $files=0;
	while(my $this_file=readdir(CHECK_DIR)) {
		next if ($this_file eq '.' || $this_file eq '..');
		
		$files++;
		last;  #Any files mean we're not empty
    }
    closedir(CHECK_DIR);
	
	return ($files==0);
}

#########################
# Sub:  ValidateConfig
# Desc: parse our command-line arguments and validate our configuration
#########################
sub ValidateConfig {
	while(my $arg=shift(@ARGV)) {
		$arg=~tr/a-z/A-Z/;
		if ($arg eq '-D') {
			$::DEST_DIR=shift(@ARGV);
		} elsif ($arg eq '-S') {
			$::SOURCE_DIR=shift(@ARGV);
		} elsif ($arg eq '-NAME') {
			$::MERGED_NAME=shift(@ARGV);
			#No commas in the merged name
			$::MERGED_NAME=~s/\,//;
		} elsif ($arg eq '-SKIPNONPATCH') {
			$::SKIP_NON_PATCH=1;
		} elsif ($arg eq '-INPLACE') {
			$::IN_PLACE_MERGE=1;
		} elsif ($arg eq '-PREFIXPATCH') {
			$::PACKAGE_PATCH_PREFIX=1;
			&::LogMsg("Retaining source directory patch prefixes!");
			&::LogMsg("WARNING: Do not use unless directed by Oracle Support");
		} else {
			&::LogMsg("Unknown argument: $arg");
			&Usage();
		}
	}

	if ($::SOURCE_DIR eq '') {
		&::LogMsg("Required argument -s <source patch dir> missing");
		&Usage();
	}

	unless (-d $::SOURCE_DIR ) {
		&::LogMsg("Source dir: $::SOURCE_DIR does not exist");
		&::Usage();
	}
	
	if ($::DEST_DIR eq '') {
		&::LogMsg("Required argument -d <dest patch dir> missing");
		&Usage();
	}
	
	unless (-d $::DEST_DIR ) {
		&::LogMsg("Destination dir: $::DEST_DIR does not exist");
		&Usage();
	}
	
	if ($::IN_PLACE_MERGE!=1) {
		unless(&IsDirEmpty($::DEST_DIR)) {
			&::LogMsg("Destination dir: $::DEST_DIR must be empty unless this is an in-place merge");
			exit 1;
		}
	}
}

sub Usage {
	&::LogMsg("Usage:");
	&::LogMsg("ormerge -s <source patch dir> -d <dest patch directory> [-name <merged patch name>] [-inplace] [-skipnonpatch]");
	exit 1;
}
