############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RAFTestDBSQL;
our @ISA = qw(ORPAction::DBSQL);

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;

	my $parent='DBSQL_RAFTEST';
	if ($self->CompileInvalidsInSharedSchema($parent)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  GetCheckLockedObjectsSQL
# Desc: Override the default check locked object SQL statement so that we only look for RAF objects if
#       only RAF objects are being migrated
# Ret:  SQL statement to use when checking locked objects
#########################
sub GetCheckLockedObjectSQL {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my @product_list=$patch_info->GetProductList();
	
	my $SQL='';
	
	if (scalar(@product_list)==1 && $product_list[0] eq 'RAFTEST') {
		#If there is only one product and it is RAFTEST, only look for locked object names that belong to RAFTEST
		$SQL=<<ENDSQL;
alter session set "_optimizer_cartesian_enabled"=false;
set escape \\
SELECT a.session_id||'|'||a.oracle_username||'|'||count(1)
FROM V\$LOCKED_OBJECT A,user_objects b
WHERE A.OBJECT_ID = B.OBJECT_ID
and (b.object_name like 'RAFTEST\\_\%')
group by a.session_id,a.oracle_username
ENDSQL
	} else {
		#If multiple products are involved, use the standard SQL
		$SQL=$self->SUPER::GetCheckLockedObjectSQL();
	}

	return $SQL;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='DBSQL_RAFTEST';

	my $rib_subtype_name='raftest_db_rib';
	
	#Allow skipping all DBC scripts when DBSQL_RAF_RUN_DBC=N
	my $dbc_mode=&DBSQL::ValidateRunDBC($config_ref,$action_name);
	
	#There is no separate RAF product yet, so we don't set this
	my $product_name='';
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors,patch_product,run_phase]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   patch_product
	#   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
	my @file_type_info=(
	['raftest_db_ddl',				1,'',				1,$product_name,'PATCHACTION'],
	['raftest_db_types',			2,'LoadTypes',		1,$product_name,'PATCHACTION'],
	[$rib_subtype_name,				1,'LoadRIBSubTypes',	1,$product_name,'PATCHACTION'],
	['raftest_db_rib_top',				1,'LoadRIBTopTypes',	1,$product_name,'PATCHACTION'],
	['raftest_db_change_scripts',	$dbc_mode,'',		1,$product_name,'PATCHACTION'],
	['raftest_db_packages',			2,'',				1,$product_name,'PATCHACTION'],
	['raftest_db_triggers',			2,'',				1,$product_name,'PATCHACTION'],
	['raftest_db_install_scripts',	3,'',				1,$product_name,'PATCHACTION'],
	['raftest_db_control_scripts',	2,'',				1,$product_name,'PATCHACTION'],
	);
	
	my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
	$self->{'RIB_SUBTYPE_NAME'}=$rib_subtype_name;
	bless($self,$class);
	
	return $self;
}

1;
