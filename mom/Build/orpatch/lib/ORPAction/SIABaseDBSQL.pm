############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::SIABaseDBSQL;
our @ISA = qw(ORPAction::DBSQL);

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;

	#Run the default DBSQL check
	if ($self->SUPER::PrePatchCheck($full_check)!=1) {
		return 0;
	}
	
	#If this is a full check, register our need for a working DBSQL_RADM configuration
	if ($full_check==1) {
		my $global_state=$self->GetORPatchGlobalState();
		
		#Register a new dependent prepatchcheck to run
		$global_state->AddExtraPrePatchCheck('DBSQL_RADM');
		
		my $radm_grantee=$self->GetORPatchGlobalState()->GetCrossActionFlag('DBSQL_SIABASE_RADM_GRANTEE');
		
		unless($radm_grantee==1) {
			#Make sure the RADM grantee looks reasonable
			if ($self->CheckRADMGrantee()!=1) {
				return 0;
			}
			$self->GetORPatchGlobalState()->SetCrossActionFlag('DBSQL_SIABASE_RADM_GRANTEE',1);
		}
	}
	
	return 1;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;

	#Compile invalids if they haven't been
	if ($self->CompileInvalidsInSharedSchema('DBSQL_SIABASE')!=1) {
		return 0;
	}
	
	return 1;
}

########################
# Sub:  LoadRSETypes
# Desc: A function to load RSE types and ensure they are compiled after loading
# Ret:  1 if the files ran successfully, 0 if not
########################
sub LoadRSETypes {
	my $self=shift;
	my ($full_dest,$filtered_dest_info)=@_;
	
	if (scalar(@$filtered_dest_info)==0) {
		return 1;
	}
	
	#Run all the types
	if ($self->RunDestFileList($full_dest,$filtered_dest_info,[])!=1) {
		#Problems running a script, abort
		return 0;
	}
	
	#Try to compile all types
	return $self->ForceCompileTypes();
}

#########################
# Sub:  LoadSynonyms
# Desc: Run the synonym script, passing the RA schema name as an argument
#########################
sub LoadSynonyms {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
		
	#Find the RADM username
	my $radm_user=$self->GetCrossActionSchemaName('DBSQL_RADM');
	return 0 if $radm_user eq '';
	
	my $method_ref=new FileCopyMethodSubst([qr/.*/],{'RADM_DB_USERNAME'=>$radm_user},'%{','}%');
	
	if ($self->RunDestFileListWithSubstitutions($full_dest,$filtered_dest_info,[],$method_ref)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  LoadGrantsToRSE
# Desc: Run the grant script as RADM user
#########################
sub LoadGrantsToRSE {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
	
	my $action_name='DBSQL_RADM';
	
	#Find our username
	my $schema_name=$self->GetSchemaName();
	return 0 if $schema_name eq '';
	
	#Get a handle to the DBSQL_RADM action	
	my $global_state=$self->GetORPatchGlobalState();
	my $radm_handle=$global_state->GetActionHandle($action_name);
	
	if (!defined($radm_handle)) {
		&::LogMsg("Unable to get handle to $action_name action");
		return 0;
	}
	
	my $method_ref=new FileCopyMethodSubst([qr/.*/],{'RSECORE_DB_USERNAME'=>$schema_name,'RSEMBA_DB_USERNAME'=>$schema_name},'%{','}%');

	return $radm_handle->RunGrantsToRSE($full_dest,$method_ref,$orig_f_dest_info);
}

#########################
# Sub:  LoadGrantsToRA
# Desc: Run the grants to RA user or role
#########################
sub LoadGrantsToRA {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
		
	#Find the RADM username
	my $radm_grantee=$self->GetRADMGrantee();
	return 0 if $radm_grantee eq '';
	
	my $method_ref=new FileCopyMethodSubst([qr/.*/],{'RABATCH_GRANT_RECIPIENT'=>$radm_grantee},'%{','}%');
	
	if ($self->RunDestFileListWithSubstitutions($full_dest,$orig_f_dest_info,[],$method_ref)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  CheckRADMGrantee
# Desc: Verify that we were given configuration for RADM Grantee and that it appears to be a user or a role
# Args: None
# Ret:  1 on success, 0 on failure
#########################
sub CheckRADMGrantee {
	my $self=shift;
	
	#make sure we were given an RADM_GRANTEE
	my $radm_grantee=$self->GetRADMGrantee();
	if ($radm_grantee eq '') {
		&::LogMsg("Unable to find DBSQL_RASECORE_RADM_GRANTEE configuration setting!");
		&::LogMsg("Ensure this value is set in env_info.cfg");
		return 0;
	}
	
	if ($self->DoesGranteeExist($radm_grantee)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  DoesGranteeExist
# Desc: Confirm that the RADM grantee is a user or role
# Arg:  None
# Ret:  1 if grantee appears valid, 0 if not or on error
#########################
sub DoesGranteeExist {
	my $self=shift;
	my ($grantee)=@_;
	
	&::LogMsg("Checking for existance of grantee $grantee");
	my $exists_sql=<<ENDSQL;
select count(1)
from (select username from dba_users where username = upper('$grantee')
      UNION ALL 
	  select role from dba_roles where role = upper('grantee'))
ENDSQL

	my ($output_ref,$error_ref)=$self->CallExecSQL($exists_sql);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to check grantee existence");
		&::DebugOutput("Grantee Exists",@$output_ref);
		return 0;
	}

	my $obj_count=$$output_ref[0];
	chomp($obj_count);
	$obj_count=~s/\s*//g;
	
	if ($obj_count==0) {
		#Grantee does not exist as a user or role
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  GetRADMGrantee
# Desc: Get the user or role for RADM grants
# Args: None
# Ret:  the username or role name to grant to
#########################
sub GetRADMGrantee {
	my $self=shift;
	return $self->{'RADM_GRANTEE'};
}

#########################
# Sub:  IsFreshInstall
# Desc: Check if this install is the initial install of this component
# Args: None
# Ret:  1 if it is an initial install, 0 if not, -1 on error
#########################
sub IsFreshInstall {
	my $self=shift;
	
	my $component=$self->{'RSE_COMPONENT_NAME'};
	
	my $SQL=<<ENDSQL;
set serveroutput on
DECLARE
	pkg_count int;
	v_result VARCHAR2(1);
	v_stmt VARCHAR2(32000);
	component_name VARCHAR2(1000);
BEGIN
	component_name:='$component';
	select count(1) into pkg_count from user_objects where object_name = 'RSE_INSTALL_UTIL' and object_type = 'PACKAGE';
	if pkg_count > 0 then
		v_stmt:='SELECT rse_install_util.is_component_installed(:component_name) from dual';
		execute immediate v_stmt into v_result using component_name;
		
		if v_result = 'N' then
			dbms_output.put_line('0');
		else
			dbms_output.put_line('1');
		end if;
	else
		dbms_output.put_line('0');
	end if;
END;
/
select 'COMPLETE - check rse_version_info' from dual
ENDSQL

	my ($output_ref,$error_ref)=$self->CallExecSQL($SQL);
	
	if (scalar(@$error_ref)!=0) {
		&::LogMsg("Unable to check RSE Component $component install status");
		&::DebugOutput("RSE Version Info",@$output_ref);
		return -1;
	}

	my $obj_count=$$output_ref[0];
	chomp($obj_count);
	$obj_count=~s/\s*//g;
	
	if ($obj_count==1) {
		#Some version of this component has been installed, not a fresh install
		return 0;
	} elsif ($obj_count==0) {
		#No version of this component has been installed, a fresh install
		return 1;
	} else {
		&::LogMsg("Unexpected result for RSE Component: $obj_count");
		&::DebugOutput("RSE Version Output",@$output_ref);
		return -1;
	}
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref,$action_name,$type_prefix,$patch_product,$rse_component_name)=@_;
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors,patch_product,run_phase]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest,
    #	            4 - on fresh install only register with dbmanifest, otherwise treat as run type 1, 5 - on a non fresh install only register with dbmanifest, otherwise treat as run type 5
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   patch_product
	#   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
	my @file_type_info=(
	["${type_prefix}_db_grants_to_rse",		2,'LoadGrantsToRSE',		1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_type_spec",			5,'LoadRSETypes',			1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_tables",			5,'',						1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_seqs",				5,'',						1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_synonyms",			2,'LoadSynonyms',			1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_change_scripts",	4,'',						1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_pkg_spec",			2,'',						1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_views",				2,'',						1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_mviews",			5,'',						1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_type_body",			2,'',						1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_pkg_body",			2,'',						1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_constraints",		5,'',						1,$patch_product,'PATCHACTION'],
	["${type_prefix}_db_grants_to_ra",		2,'LoadGrantsToRA',			1,$patch_product,'PATCHACTION'],
	);
	
	my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
	bless($self,$class);
	
	$self->SetUniqueDBManifest(0);
	
	$self->{'RSE_COMPONENT_NAME'}=$rse_component_name;
	
	#We override the connect information to use CORE_DB's info
	my $name='DBSQL_RASECORE';
	$self->{'CONNECT_STRING'}=$config_ref->Get("${name}_CONNECT");
	$self->{'TNS_ADMIN'}=$self->ResolveConfigPath($config_ref->Get("${name}_WALLET"));
	
	$self->{'RADM_GRANTEE'}=$config_ref->Get("DBSQL_RASECORE_RADM_GRANTEE");
	
	return $self;
}

1;
