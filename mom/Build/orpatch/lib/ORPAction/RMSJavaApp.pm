############################################################################
# Copyright © 2016, Oracle and/or its affiliates. All rights reserved.
############################################################################

package ORPAction::RMSJavaApp;
use strict;
our @ISA = qw(ORPAction::JavaApp);
	

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='JAVAAPP_RMS';

    my $reports_subdir='reports';
    my $reports_type='rms_reports';
    my $reports_fcp=new FileCopyParams($reports_type,$reports_subdir,-2);
	
	my @file_type_info=(
	#Type               Archive copies
	['rms_java_app',	1,	0,	undef],
	#Non-Archive copies
	#Type               Archive copies, Sign jars, FileCopyParam
	[$reports_type,		0,	0,	$reports_fcp],
	);
	
	my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
	
    return $self;
}

1;
