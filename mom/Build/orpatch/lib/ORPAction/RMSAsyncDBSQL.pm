############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RMSAsyncDBSQL;
our @ISA = qw(ORPAction::DBSQL);

########################
# Sub:  PrePatchCheck
# Desc: Since RMSAsyncDBSQL is obsolete, fails if prepatch check is called for this action
# Ret:  0
########################
sub PrePatchCheck {
	my $self=shift;

	&::LogMsg("Orpatch Action RMSAsyncDBSQL is obsolete and cannot be run");
	return 0;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;
	
	#Compile Invalid Objects
	if ($self->CompileInvalids()!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  LoadRMSAsyncInstallScripts
# Desc: a custom function to load the RMS Async Install Scripts
#########################
sub LoadRMSAsyncInstallScripts {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	my @install_scripts=('rmsseeddata_async.sql');
	
	return $self->FindAndRunScriptList($full_dest,$filtered_dest_info,$orig_f_dest_info,@install_scripts);
}


#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	my @file_type_info=(
	['rmsasync_db_install_scripts',	3,'LoadRMSAsyncInstallScripts',1],
	);
	
	my $self=$class->SUPER::new('DBSQL_RMSASYNC',\@file_type_info,$config_ref);
	$self->SetCheckLockedObjects(0);
	
    bless($self,$class);

	return $self;
}

1;
