############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################

package ORPAction::ResaJavaApp;
use strict;
our @ISA = qw(ORPAction::JavaApp);
	

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='JAVAAPP_RESA';
	my $batch_subdir='resa-batch/batch';
	
	my $batch_type='resa_java_batch';
	my $batch_fcp=new FileCopyParams($batch_type,$batch_subdir,0);
	
	my @file_type_info=(
	#Type               Archive copies
	['resa_java_app',	1,				0,			undef],
	#Non-Archive copies
	#Type               Archive copies, Sign jars, FileCopyParam
	[$batch_type,		0,				0,		   $batch_fcp],
	);
	
	my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
	
	$self->{'BATCH_SUBDIR'}=$batch_subdir;
	
    return $self;
}

1;
