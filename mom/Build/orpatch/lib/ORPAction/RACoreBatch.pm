############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RACOREBATCH;
our @ISA = qw(ORPAction);

sub FinalizeConfiguration {
	my $self=shift;
    #No need to finalize configuration if we are not enabled on this host
    if ($self->IsValidOnThisHost()!=1) {
        return 1;
     }

    my $global_state=$self->GetORPatchGlobalState();

	
	#Finally update the Subst FileCopy Method with our substitution property file
	my $dest_dir=$global_state->GetDestDir();
	my $property_file=&::ConcatPathString($dest_dir,'orpatch','config','ra_config.properties');
	
	my $subst_method=$self->{'SUBST_FILE_COPY'};
	$subst_method->SetSubstPropertyFile($property_file);
	#Prepare the substitution file copy method immediately so that we know if the property file is bad right away
	if ($subst_method->PrepareForCopies()!=1) {
		return 0;
	}
	
	return 1;
}


#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($config_ref)=@_;
	
	#my $mm_home='mmhome';
	my $dest_prefix="mmhome";
	my $type_prefix="ra_mmhome";
    
     
	
	my $subst_method=new FileCopyMethodSubst([qr/ra\.env$/],{},'@','@');
	
    
    
my @file_copy_params=();
	
	#Copy        
    my $config_fcp=new FileCopyParams(["${type_prefix}_cfg"],"$dest_prefix",-2);
    $config_fcp->SetFileCopyMethod({"${type_prefix}_cfg"=>$subst_method});
	push(@file_copy_params,$config_fcp);
    
         
    my $mmhome_fcp=new FileCopyParams(["${type_prefix}_files"],"$dest_prefix",-2);
	push(@file_copy_params,$mmhome_fcp);
    
         
    my $mmhome_fcp=new FileCopyParams(["${type_prefix}_aps_files"],"",-2);
	push(@file_copy_params,$mmhome_fcp);
    
   
	
	my $self=$class->SUPER::new('RACOREBATCH',\@file_copy_params,$config_ref);
	bless($self,$class);
	
	$self->{'DEST_PREFIX'}=$dest_prefix;
	$self->{'SUBST_FILE_COPY'}=$subst_method;
	
    return $self;
}

1;
