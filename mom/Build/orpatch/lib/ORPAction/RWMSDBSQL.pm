############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RWMSDBSQL;
our @ISA = qw(ORPAction::DBSQL);

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;

	#Run the default DBSQL check
	if ($self->SUPER::PrePatchCheck($full_check)!=1) {
		return 0;
	}
	
	#If this is a full check, register our need for a working DBSQL_RWMSUSER configuration
	if ($full_check==1) {
		my $global_state=$self->GetORPatchGlobalState();
		
		#Register a new dependent prepatchcheck to run
		$global_state->AddExtraPrePatchCheck('DBSQL_RWMSUSER');
	}
	
	return 1;
}

#########################
# Sub:  PostAction
# Desc: Recompile invalid objects
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name)=@_;
	
	#Compile Invalid Objects
	if ($self->CompileInvalids()!=1) {
		return 0;
	}
	
	#Run our PostAction registered SQL scripts (grant scripts)
	if ($self->SUPER::PostAction()!=1) {
		return 0;
	}
	
	#Get the RWMS User name using the DBSQL_RWMSUSER connection information
	my $rwms_user=$self->GetCrossActionSchemaName('DBSQL_RWMSUSER');
	return 0 if $rwms_user eq '';
	
	#Create synonyms in their schema, pointing to RWMS objects
	unless($self->CreateSynonymsForUser($rwms_user,'')==1) {
		&::LogMsg("Failed to create synonyms to $rwms_user schema");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  LoadWMSDataScriptsSub
# Desc: a custom function to check if the install script sub scripts have changed so that
#       we can trigger a full reload of the install script 'top' scripts
#########################
sub LoadWMSDataScriptsSub {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;

	#Note that we do not run any scripts at this point, we are only checking if any
	#of the scripts we don't track in DBmanifest have changed
	#If so, we set a flag so the WMSIScriptTop function forces a full load
	
	#Filter the complete dest manifest for things updated with this patch
	my $f_dest_info=&Manifest::FilterDestManifest($orig_f_dest_info,'',$patch_name,$subpatch_ref);
	
	if (scalar(@$f_dest_info)!=0) {
		&::LogMsg("One or more second-level install scripts have been updated, forcing install script load");
		$self->{'ISCRIPTS_TYPE_LOAD'}=1;
	}
	
	return 1;
}

#########################
# Sub:  LoadWMSDataScriptsTop
# Desc: a custom function for running WMS install scripts
#       Using this function requires that you have defined $self->{'ISCRIPT_SUBTYPE_NAME'}
#########################
sub LoadWMSDataScriptsTop {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state,$subpatch_ref)=@_;

	#First, we set our restart point back to the WMS install sub scripts so if there is a problem
	#we will restart their to reset the ISCRIPTS_TYPE_LOAD flag
	#
	#Extract our dest_subdir out of full_dest
	my @parts=split(/\//,$full_dest);
	my $dest_subdir=pop(@parts);
	$self->MarkRestartPoint($restart_state,$dest_subdir,$self->{'ISCRIPT_SUBTYPE_NAME'});
	
	#Filter for top-level scripts updated with this patch
	my $f_dest_info=&Manifest::FilterDestManifest($orig_f_dest_info,'',$patch_name,$subpatch_ref);
	
	if ($self->{'ISCRIPTS_TYPE_LOAD'}!=1 && scalar(@$f_dest_info)==0) {
		&::LogMsg("No changes to Install scripts in this patch, skipping install load");
		return 1;
	}
	
	#Load all top-level install scripts

	if ($self->RunDestFileList($full_dest,$orig_f_dest_info,1)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  LoadWMSUserADFScripts
# Desc: Create the synonyms from RWMS Runtime User->RWMS_ADF_USER
#       We do this here because the runtime user doesn't have permissions to create the dbmanifest table
#       so RWMSUserDBSQL can't run PatchAction
#########################
sub LoadWMSUserADFScripts {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info,$restart_state)=@_;
		
	#Find the RWMS Runtime username
	my $rwms_user=$self->GetCrossActionSchemaName('DBSQL_RWMSUSER');
	return 0 if $rwms_user eq '';
	
	my $args="$rwms_user";
	if ($self->RunDestFileListWithArgs($full_dest,$args,$filtered_dest_info,[])!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  LoadWMSGrants
# Desc: Grant permissions to rwms_app
#       This is a custom function because we always run the grant script(s) even on a hotfix
#       to unrelated files, so runtype 2 won't work
#########################
sub LoadWMSGrants {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	if (scalar(@$orig_f_dest_info)==0) {
		return 1;
	}
	
	#Run the original unfiltered list, always
	return $self->RunDestFileList($full_dest,$orig_f_dest_info,1);
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='DBSQL_RWMS';
	
	#We use this in the db_rib_top function, so we want to make sure the
	#name matches identically
	my $rib_subtype_name='rwms_db_rib';
	my $iscripts_subtype_name='rwms_db_installscripts';
	
	#Allow skipping all DBC scripts when DBSQL_RWMS_RUN_DBC=N
	my $dbc_mode=&DBSQL::ValidateRunDBC($config_ref,$action_name);
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors,patch_product,run_phase,del_func]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	#   patch_product
	#   run_phase - PATCHACTION to run at default time, POSTACTION or CLEANUPACTION to run at a later processing phase
	#   del_func - Optional function that should be run whenever a file of this type is deleted
	my @file_type_info=(
	['rwms_db_ddl',					3,'',						1,'RWMS','PATCHACTION',''],
	['rwms_db_types',				2,'LoadTypes',				1,'RWMS','PATCHACTION',''],
	[$rib_subtype_name,				1,'LoadRIBSubTypes',		1,'RWMS','PATCHACTION',''],
	['rwms_db_rib_top',				1,'LoadRIBTopTypes',		1,'RWMS','PATCHACTION',''],
	['rwms_db_change_scripts',		$dbc_mode,'',				1,'RWMS','PATCHACTION',''],
	['rwms_db_procedures',			2,'',						1,'RWMS','PATCHACTION',''],
	['rwms_db_packages',			2,'',						1,'RWMS','PATCHACTION',''],
	['rwms_db_triggers',			2,'',						1,'RWMS','PATCHACTION',''],
	['rwms_db_sp_jars',				2,'LoadDBJarsSingly',		1,'RWMS','PATCHACTION','DropDBJar'],
	[$iscripts_subtype_name,		2,'LoadWMSDataScriptsSub',	1,'RWMS','PATCHACTION',''],
	['rwms_db_installscripts_top',	2,'LoadWMSDataScriptsTop',	1,'RWMS','PATCHACTION',''],
	['rwms_db_control_scripts',		2,'',						1,'RWMS','PATCHACTION',''],
	['rwms_db_adf_objects',			1,'',						1,'RWMS','PATCHACTION',''],
	['rwms_db_user_install',		1,'LoadWMSUserADFScripts',	1,'RWMS','PATCHACTION',''],
	['rwms_db_grants',				2,'LoadWMSGrants',			1,'RWMS','POSTACTION',''],
	);
	
	my $self=$class->SUPER::new('DBSQL_RWMS',\@file_type_info,$config_ref);
	$self->{'RIB_SUBTYPE_NAME'}=$rib_subtype_name;
	$self->{'ISCRIPT_SUBTYPE_NAME'}=$iscripts_subtype_name;
	
	return $self;
}

1;
