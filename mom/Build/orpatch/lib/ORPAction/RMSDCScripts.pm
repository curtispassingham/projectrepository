############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RMSDCScripts;
our @ISA = qw(ORPAction);

sub PatchAction {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	my $link_dir=&::ConcatPathString($dest_dir,'external');
	
	&::CreateDirs('0755',"$link_dir/data","$link_dir/logs");
	
	return 1;
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($config_ref)=@_;

    my $action_name='RMSDCSCRIPTS';	
	my $r_scripts='rms_data_conversion';
	my $rc_scripts='rms_data_conversion_config';
	my $rf_scripts='rfm_data_conversion';
	my $rfc_scripts='rfm_data_conversion_config';

	
	my @types=($r_scripts,$rc_scripts);
    my %update_criteria=($r_scripts=>'IFNEW',$rc_scripts=>'NEVER',
                         $rf_scripts=>'IFNEW',$rfc_scripts=>'NEVER');
	

	# If the RFM is enabled in the Config ,migrate the data conversion scripts.
	my $rfmdc_mode=$config_ref->ValidateTFVar("${action_name}_INCLUDE_RFM",0);
	if ($rfmdc_mode==1) {
		&::LogMsg("Retail Fiscal Management Data Conversion Scripts Enabled. ");
     push(@types,$rf_scripts,$rfc_scripts); 
		}
     my $fcp=new FileCopyParams(\@types,'external/scripts',0);
     $fcp->SetUpdateCriteria(\%update_criteria);
	my @file_copy_params=($fcp);
	
	my $self=$class->SUPER::new('RMSDCSCRIPTS',\@file_copy_params,$config_ref);
    return $self;
}

1;
