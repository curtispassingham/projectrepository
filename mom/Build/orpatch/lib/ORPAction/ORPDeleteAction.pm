############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::ORPDeleteAction;
our @ISA = qw(ORPAction);

#This is a unique action in that it does not ever copy files, it only removes files
#and it removes files from every other action that may be enabled on this host
#It uses a 'delete' source manifest in the patch contents to determine what may need to be deleted

use File::Copy;

#########################
# Sub:  CheckPatchForAction
# Desc: Check if a delete manifest is included in the patch and whether any files will be deleted
# Args: version_check   - 1 if details of files to remove are returned, or just a list of files
# Ret:  in scalar context, returns 1 if there are any files matching this action's file types
#       in array context
#           if version_check is 1 returns a list of [file name,'DELETE',relative name,deleted dest manifest ref,patch name]
#           if version_check is 0 returns the list of file names that would be deleted
#########################
sub CheckPatchForAction {
	my $self=shift;
	my ($version_check)=@_;
	
	my $delete_files_ref=$self->{'DELETE_FILES'};
	if (!defined($delete_files_ref)) {
		$delete_files_ref=$self->GetDeleteFileList();
		#save the list of files to delete for processing in PreAction
		$self->{'DELETE_FILES'}=$delete_files_ref;
	} 
	
	#Setup our array of files that would be deleted
	my @interest=();
	foreach my $dst_manifest (keys(%$delete_files_ref)) {
		my $dm_ref=$delete_files_ref->{$dst_manifest};
		my ($df_ref,$action)=@$dm_ref;
		foreach my $ref (@$df_ref) {
			my ($relative_name,$full_name,$patch_name)=$self->DecodeDeleteFileInfo($ref);
			if ($version_check>0) {
				#If they want the full details, we have to search the destination manifest for the file being replaced
				my $di_ref=$self->FindDestInfo($relative_name,$dst_manifest);
				push(@interest,[$full_name,'DELETE',$relative_name,$di_ref,$patch_name]);
			} else {
				push(@interest,$full_name);
			}
		}
	}
	
	return (1,@interest);
}

#########################
# Sub:  PreAction
# Desc: This is called before copying files for a patch
#########################
sub PreAction {
	my $self=shift;
	
	#CheckPatchAction MUST be called before PreAction or we can't do our work
	my $df_ref=$self->{'DELETE_FILES'};
	return 1 unless defined($df_ref);
	
	foreach my $dst_manifest (keys(%$df_ref)) {
		my $ref=$df_ref->{$dst_manifest};
		my ($file_info_ref,$action)=@$ref;
		if ($self->DeleteFiles($dst_manifest,$file_info_ref,$action)!=1) {
			return 0;
		}
	}

	#If we removed any files, mark this patch as having made changes
	if (scalar(keys(%$df_ref))!=0) {
		my $global_state=$self->GetORPatchGlobalState();
		$global_state->SetPatchMadeChangesFlag();
	}	
	
	return 1;
}

#########################
# Sub:  DeleteFiles
# Desc: Remove a list of files from a specific destination manifest
#       This is the safe way for other actions to delete files
# Args: dst_manifest - full path to the destination manifest
#       file_info_ref - ref to a list of delete file info rows (from AssembleDeleteFileInfo)
#       action_handle - Handle to the action that owns the destination manifest
# Ret:  1 on success, 0 on failure
#########################
sub DeleteFiles {
	my $self=shift;
	my ($dst_manifest,$file_info_ref,$action_handle)=@_;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $restore_point=$global_state->GetRestorePoint();
	
	my $desired=scalar(@$file_info_ref);
		
	&::LogMsg("Processing deletes related to $dst_manifest");

	my ($removed_from_manifest,$remove_info_ref)=$self->RemoveFilesFromDestManifest($dst_manifest,$file_info_ref);
	
	#Abort if there were problems updating the manifest
	return 0 if ($removed_from_manifest<0);
	
	&::LogMsg("   $removed_from_manifest/$desired moved to deleted env manifest");
	
	#Remove all the files, even if they weren't removed from the dest manifest
	my $status=1;
	my $files_renamed=0;
	foreach my $ref (@$file_info_ref) {
		my ($relative_dest_name,$full_dest_name,$patch_name)=$self->DecodeDeleteFileInfo($ref);
		
		if (-f $full_dest_name) {
			#Get the former dest manifest info
			my $old_dest_info=$remove_info_ref->{$relative_dest_name};
			if ($old_dest_info eq '') {
				&::LogMsg("Warning file $full_dest_name did not exist in $dst_manifest, reverting this patch will not be able to restore");
			} else {
				#Call the PreFileDelete method on the owning action to give them a chance to take extra actions
				if ($action_handle->PreFileDelete($full_dest_name,$old_dest_info)!=1) {
					return 0;
				}
			}
			
			#Make a backup copy
			my $backup_mode=$restore_point->GetBackupModeDelete();
			if ($restore_point->CreateBackupFile($backup_mode,'',$full_dest_name,$dst_manifest,$old_dest_info)!=1) {
				return 0;
			}
			
			$::LOG->LogMsgNoVerbose("Removing $full_dest_name");
			
			if (unlink($full_dest_name)==1) {
				#Success
				$files_renamed++;
			} else {
				&::LogMsg("Unable to remove $full_dest_name: $!");
				$status=0;
			}
		}
	}
	
	&::LogMsg("   $files_renamed/$desired files removed");
	
	if ($files_renamed!=0) {
		$global_state->SetPatchMadeChangesFlag();
	}
	
	return $status;
}

#########################
# Sub:  CopyPatchFiles
# Desc: Override the default CopyPatchFiles to do nothing
#########################
sub CopyPatchFiles {
	my $self=shift;
	#No files to copy for a delete action
	return 1;
}


##########################################################################################
# Helper routines

#########################
# Sub:  GetDeleteFileList
# Desc: Read a delete manifest and find any files that exist on this host
# Args: None
# Ret:  a reference to a hash keyed by destination manifest with each value a ref to a list of [relative file,full file name] to remove
#########################
sub GetDeleteFileList {
	my $self=shift;
	
	#Get the configuration for our patch
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	my $patch_name=$patch_info->GetPatchName();
	
	my $delete_manifest=$global_state->GetSourceDeleteManifest();
	my $dest_dir=$global_state->GetDestDir();
	
	my %delete_files=();
	
	if (!-f $delete_manifest) {
		#a delete manifest does not have to exist in a patch
		return \%delete_files;
	}
	
	&::LogMsg("Evaluating delete manifest against environment contents");
	
	#Read the source delete manifest
	my @delete_info=&Manifest::ReadManifest($delete_manifest);
	
	#Get the Distinct Types in the manifest
	my %type_lookup=();
	my @file_types=&Manifest::BuildSourceManifestTypeHash(\@delete_info,\%type_lookup);
	
	foreach my $type (@file_types) {
		my $fcp_list_ref=$self->GetFCPForFileType($global_state,$dest_dir,$type);
		next if (scalar(@$fcp_list_ref)==0);
		foreach my $fcp_ref (@$fcp_list_ref) {
			my ($fcp,$action_handle)=@$fcp_ref;
			
			next unless defined($fcp);
			
			#get the full destination path and the manifest that is responsible for this file
			my ($ft_ref,$dest_subdir)=$fcp->GetBasicParams();
			my ($full_dest,$dest_manifest)=$self->GetDestDirAndManifest($dest_dir,$dest_subdir);

			#Find all files in the delete manifest that have this type
			my $filtered_si_ref=&Manifest::FilterSourceManifest(\%type_lookup,[$type],[]);
			
			foreach my $ref (@$filtered_si_ref) {
				my ($sfile,$frevision,$ftype,$fpatch_name,$funit_test,$flocalized,$fcustomized,@discard)=&Manifest::DecodeSourceLine($ref);

				#If the request to delete this file came from a specific patch, use that name otherwise use the top-level patch name
				if ($fpatch_name eq '') {
					$fpatch_name=$patch_name;
				}
				
				#Get the filename where this file would end up
				my ($full_src_name,$full_dest_name,$relative_dest_name)=$fcp->GenFileNames($sfile,$ftype,'',$full_dest);
				
				if (-f $full_dest_name) {
					if ($self->IsFileInSourceManifest($sfile,$ftype)==1) {
						$::LOG->LogMsgNoVerbose("File to delete $sfile also exists in source manifest, NOT deleting file");
					} else {
						my $df_ref=$delete_files{$dest_manifest};
						if ($df_ref eq '') {
							$df_ref=[[],$action_handle];
						}
						my $delete_file_info_ref=$self->AssembleDeleteFileInfo($relative_dest_name,$full_dest_name,$fpatch_name);
						push(@{$df_ref->[0]},$delete_file_info_ref);
						$delete_files{$dest_manifest}=$df_ref;
					}
				}
			}
		}
	}
	
	return \%delete_files;
}

#########################
# Sub:  IsFileInSourceManifest
# Desc: Check if a file we are going to delete also exists in the source manifest
# Args: delete_file - The source file name from the delete manifest that we are planning to delete
#       delete_ftype - the file type of the file being deleted
# Rets: 1 if the same file exists in the source manifest, 0 if not
#########################
sub IsFileInSourceManifest {
	my $self=shift;
	my ($delete_file,$delete_ftype)=@_;
	
	my $global_state=$self->GetORPatchGlobalState();
	my ($src_info_ref,$type_lookup_href,$distinct_types)=$global_state->GetSourceManifestInfo();
	my $type_ref=$type_lookup_href->{$delete_ftype};
	
	#No files of this type in the source manifest
	if ($type_ref eq '') {
		return 0;
	}
	
	foreach my $ref (@$type_ref) {
		my ($sname,$src_revision,$src_ftype,@discard)=&Manifest::DecodeSourceLine($ref);
		if ($sname eq $delete_file) {
			#File exists in source manifest!
			return 1;
		}
	}
	
	return 0;
}

#########################
# Sub:  AssembleDeleteFileInfo
# Desc: Create a deletefileinfo that is compatible with DeleteFiles
# Args: rel_dest_name - Name of the file relative to the location of the dest manifest
#       full_dest_name - The full file path of the file to remove
#       patch_name - The patch name that will be recorded as 'removing' this file
# Rets: ref to a deletefileinfo entry
#########################
sub AssembleDeleteFileInfo {
	my $self=shift;
	my ($rel_dest_name,$full_dest_name,$fpatch_name)=@_;
	
	return [$rel_dest_name,$full_dest_name,$fpatch_name];
}

#########################
# Sub:  DecodeDeleteFileInfo
# Desc: Disassemble a deletefileinfo row
# Args: ref to a deletefileinfo row
# Rets: rel_dest_name, full_dest_name, patch_name
#########################
sub DecodeDeleteFileInfo {
	my $self=shift;
	my ($dfi_ref)=@_;
	
	my $relative_dest_name=$$dfi_ref[0];
	my $full_dest_name=$$dfi_ref[1];
	my $patch_name=$$dfi_ref[2];
	
	return ($relative_dest_name,$full_dest_name,$patch_name);
}

#########################
# Sub:  GetFCPForFileType
# Desc: Search through actions looking for a file copy params that applies to a specific file type
# Args: global_state - Handle to the global state object
#       dest_dir - Path to the base destination directory
#       file_type - file type to find the FCP for
# Rets: ref to list [fcp,action] where fcp is a handle to the matching file copy params and action is a handle to the associated action
#       a ref to an empty list is returned if the type is not found
#########################
sub GetFCPForFileType {
	my $self=shift;
	my ($global_state,$dest_dir,$file_type)=@_;

	my $action_list=$global_state->GetActionList();
	
	my @fcp_list=();
	foreach my $action (@$action_list) {
		#Check all of the file copy params for this action
		foreach my $fcp ($action->GetFileCopyParams()) {
			my ($ft_ref,$dest_subdir)=$fcp->GetBasicParams();
			
			#Check all of the file types that go to this destination
			foreach my $this_ft (@$ft_ref) {
				if ($this_ft eq $file_type) {
					#Found the action and file copy params that handles the file type
					
					#However, we may have actions in the list that don't exist on this host
					#Before we say this is the action, make sure the dest_subdir and dest manifest exists
					my ($full_dest,$dest_manifest)=$self->GetDestDirAndManifest($dest_dir,$dest_subdir);
					
					if (-d $full_dest && -f $dest_manifest) {
						push(@fcp_list,[$fcp,$action]);
					} else {
						#This was the right action, but it doesn't exist here
					}
				}
			}
		}
	}
	
	#No action found that would process this file type
	return \@fcp_list;
}

#########################
# Sub:  FindDestInfo
# Desc: Read a destination manifest and find a relative file
# Ret:  ref to destination info, '' if relative file is not found
#########################
sub FindDestInfo {
	my $self=shift;
	my ($relative_name,$dst_manifest)=@_;
	
	#Read the destination manifest
	my @dest_info=&Manifest::ReadManifest($dst_manifest);

	foreach my $ref (@dest_info) {
		my ($di_dfile,@discard)=&::Manifest::DecodeDestLine($ref);
		if ($di_dfile eq $relative_name) {
			return $ref;
		}
	}
	
	&::LogMsg("WARNING: Unable to find destination entry for planned delete file: $relative_name");
	
	return '';
}
	
#########################
# Sub:  RemoveFilesFromDestManifest
# Desc: Remove a list of relative dest file names from a dest manifest
# Ret:  remove_count - -1 on errors, otherwise the number of files that were removed from the manifest
#       remove info  - ref to hash by relative dest name of ref to dest_info_row for that file
#########################
sub RemoveFilesFromDestManifest {
	my $self=shift;
	my ($dst_manifest,$file_info_ref)=@_;

	my %patch_lookup=();
	my @rel_fn_list=();
	foreach my $ref (@$file_info_ref) {
		my ($relative_dest_name,$full_name,$patch_name)=$self->DecodeDeleteFileInfo($ref);
		push(@rel_fn_list,$relative_dest_name);
		$patch_lookup{$relative_dest_name}=$patch_name;
	}
	
	#Read the destination manifest
	my @dest_info=&Manifest::ReadManifest($dst_manifest);
	
	my %remove_row_info=();
	
	#Remove lines for all files from this destination manifest
	my ($keep_info,$remove_info)=&Manifest::DeleteDestInfo(\@dest_info,\@rel_fn_list);
		
	#If some files were found in the destination manifest
	if (scalar(@$remove_info)!=0) {
		foreach my $ref (@$remove_info) {
			my ($di_dfile,$di_sfile,$di_ftype,$di_revision,@discard)=&::Manifest::DecodeDestLine($ref);
			$::LOG->LogMsgNoVerbose("Removing $di_dfile from manifest");
			#Save this information for our caller
			$remove_row_info{$di_dfile}=$ref;
		}

		my $deleted_manifest=$self->GetDeletedManifestName($dst_manifest);
		
		#Save the removed lines to the destination deleted manifest
		if (&Manifest::AppendDeletedDestManifest($deleted_manifest,$remove_info,\%patch_lookup)!=1) {
			&::LogMsg("Unable to append deleted files to delete manifest $deleted_manifest");
			return (-1,\%remove_row_info);
		}
		
		#Save the destination manifest
		if (&Manifest::SaveDestManifest($dst_manifest,[],$keep_info,{},1)!=1) {
			&::LogMsg("Unable to save destination manifest $dst_manifest after removing lines!");
			return (-1,\%remove_row_info);
		}
	}
	
	return (scalar(@$remove_info),\%remove_row_info);
}
	
	
#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($config_ref)=@_;
	
	#This name is used in JavaApp.pm, do not change it here without changing it in JavaApp::_GetORPDeleteHandle
	my $self=$class->SUPER::new('ORPDELETE',[],$config_ref);
	
	$self->SetValidOnAnyHost(1);
	$self->{'DELETE_FILES'}=undef;

    return $self;
}

1;
