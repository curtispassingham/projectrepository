############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################

package ORPAction::RASRMJavaApp;
use strict;
our @ISA = qw(ORPAction::JavaApp);
	

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	my $action_name='JAVAAPP_RASRM';
	
	my @file_type_info=(
	#Type               Archive copies
	['rasrm_java_app',	1,				0,			undef],
	#Non-Archive copies
	#Type               Archive copies, Sign jars, FileCopyParam
	);
	
	my $self=$class->SUPER::new($action_name,\@file_type_info,$config_ref);
	
    return $self;
}

1;
