############################################################################
# Copyright © 2014,2015, Oracle and/or its affiliates. All rights reserved.
############################################################################
# This is the base Action class for actions which need to manage JAR/EAR/WAR files
# It contains helper functions related to splicing patches into them, deploying them, etc

package ORPAction::JavaApp;
use strict;
our @ISA = qw(ORPAction);

use File::Copy;


########################
# Sub:  PrePatchCheck
# Desc: Validate our configuration looks reasonable
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;
	
	my $name=$self->GetActionName();
	&::LogMsg("Pre-patch check for $name");
	
	if ($self->SetJavaHome()!=1) {
		return 0;
	}
	
	#Check required WLS configuration
	foreach my $var_name ('WEBLOGIC_DOMAIN_HOME','WL_HOME','MW_HOME') {
		my $var=$self->{$var_name};
		
		if (!-d $var) {
			&::LogMsg("ERROR: $var_name $var does not exist!");
			&::LogMsg("Should be defined in config file as ${name}_${var_name}");
			return 0;
		}
	}
	
	if ($self->CheckRequiredBinaryOnce('jar')!=1) {
		return 0;
	}
	
	if ($full_check==1) {
		if ($self->SetIncludeCustom()!=1) {
			return 0;
		}
		if ($self->RunStandardDeploy('precheck')!=1) {
			return 0;
		}
		
		if ($self->RegisterSigningInfo()!=1) {
			return 0;
		}
		
		$self->SetRevertAllHotfixes();
		
		if ($self->SetBaseTypeUpdateCriteria()!=1) {
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  CheckPatchForAction
# Desc: Check if this patch includes any files of the type that this action involves
#       This overrides the default CheckPatchForAction because of the complexities of _internal file splicing
# Args: version_check   - 1 if file versions should be checked, 0 if not
# Ret:  in scalar context, returns 1 if there are any files matching this action's file types
#       in array context
#           if version_check is 1 returns a list of [file name,new revision,destination file,replaced_file]
#           if version_check is 0 returns the list of source manifest entries that are relevant
#########################
sub CheckPatchForAction {
	my $self=shift;
	my ($version_check)=@_;

	if ($self->SetBaseTypeUpdateCriteria()!=1) {
		return 0;
	}
	
	my ($proceed,@files)=$self->SUPER::CheckPatchForAction($version_check);
	if ($proceed!=1) {
		return($proceed,@files);
	}
	
	if ($version_check>0 && scalar(@files)!=0) {
		if ($self->SetupMetadataEnvironment()!=1) {
			return 0;
		}
		
		$self->SetRevertAllHotfixes();

		my $global_state=$self->GetORPatchGlobalState();
		my $source_dir=$global_state->GetSourceDir();
		
		#Make a to-be revision map for lookups in FindOutdatedInternalFiles
		my %dfname_lookup=();
		foreach my $int_ref (@files) {
			my ($fname,$new_rev,$dfname,$di_ref)=@$int_ref;
			#Check to make sure this isn't a conflict entry
			if ($di_ref ne '') {
				my ($dfile,$sfile,$ftype,$frevision,@discard)=&Manifest::DecodeDestLine($di_ref);
				#If the environment is newer, the new_rev isn't actually going to take place
				if (&PatchFileCopy::RevisionCompare($frevision,$new_rev)==1) {
					next;
				}
			}
			$dfname_lookup{$dfname}=$new_rev;
		}

		#Find the files that change for each base type
		my $updated_files_href=$self->FindBaseTypePatchChanges(\@files);
		
		#Only report conflicts when version_check==2
		my $conflicts_are_outdated=0;
		if ($version_check==2) {
			$conflicts_are_outdated=1;
		}
		
		my @internal_files=();
		foreach my $dest_ref ($self->GetBaseTypeDestinations()) {
			my ($base_type,$full_dest,$dest_manifest,$dest_subdir)=@$dest_ref;
			
			#now look through all the files for this base type
			my $changed_files_ref=$updated_files_href->{$base_type};
			next if ($changed_files_ref eq '');
			
			my @dest_info=&Manifest::ReadManifest($dest_manifest);
			
			foreach my $cf_ref (@$changed_files_ref) {
				my ($di_ref,$source_patch_name)=@$cf_ref;
				my ($dfile,$sfile,$ftype,$frevision,$fdesc,$fcustomized,@discard)=&Manifest::DecodeDestLine($di_ref);
				
				my $source_file=&::ConcatPathString($source_dir,$sfile);
				my $full_dest_file=&::ConcatPathString($full_dest,$dfile);
			
				my $new_rev=$dfname_lookup{$full_dest_file};
				if ($new_rev eq '') {
					#This file isn't actually going to be updated, so don't check for outdated files
					next;
				}
				
				&::LogMsg("Checking for hotfixes outdated by $dfile:$new_rev");
			
				#Find any _internal files that that will become outdated and thus removed when this ear is copied into place
				my @outdated_files=();
				if ($self->FindOutdatedInternalFiles($dfile,$ftype,$new_rev,$source_file,\@outdated_files,\%dfname_lookup,$full_dest,\@dest_info,$conflicts_are_outdated)!=1) {
					&::LogMsg("Unable to identify files outdated by $dfile:$new_rev");
					return(0,@files);
				}
				
				#Add any outdated files for this jar to our overall list of internal files
				foreach my $outdated_ref(@outdated_files) {
					my ($rel_name,$replaced_diref,$incoming_rev)=@$outdated_ref;
					my $full_name=&::ConcatPathString($full_dest,$rel_name);
					my $outdated_action='DELETE';
					if ($replaced_diref ne '') {
						my ($outd_dfile,$outd_sfile,$outd_ftype,$outd_frevision,$outd_patch_name,$outd_custom,$outd_fwp,$outd_has_jar,$outd_future2,$outd_future3,$outd_future4)=&Manifest::DecodeDestLine($replaced_diref);
						
						my $new_rev=$dfname_lookup{$full_name};
						if ($new_rev ne '') {
							#Because this file is also being updated by the patch, we have to update the replaced dest line to reflect what
							#the revision will be after the patch is applied
							$outd_frevision=$new_rev;
							$replaced_diref=&Manifest::AssembleDestLine($outd_dfile,$outd_sfile,$outd_ftype,$outd_frevision,$outd_patch_name,$outd_custom,$outd_fwp,$outd_has_jar,$outd_future2,$outd_future3,$outd_future4);
						}
						
						if ($incoming_rev ne '') {
							#If the file is outdated because it is a potential conflict, don't report it as a DELETE
							if (&PatchFileCopy::RevisionCompare($outd_frevision,$incoming_rev)==1) {
								$outdated_action=$incoming_rev;
							}
						}
					}
					push(@internal_files,[$full_name,$outdated_action,$rel_name,$replaced_diref,$source_patch_name]);
				}
				
				#Save this base file information in case we are asked later to produce extra details about it
				push(@{$self->{'EXTRA_DETAILS'}},[$sfile,$source_dir,$dfile,$full_dest,$ftype]);
			}
		}
	
		if (scalar(@internal_files)!=0) {
			push(@files,@internal_files);
		}
	}
	
	return (1,@files);
}

#########################
# Sub:  CreateAnalyzeDetailFiles
# Desc: Create any extra detail files that could be used during patch analysis.  By default actions do not have any extra detail files.
# Args: analyze_details - Handle to the ORPAnalyzeDetails object that will be used for registering detail files
# Ret:  1 on success, 0 on failure
#########################
sub CreateAnalyzeDetailFiles {
	my $self=shift;
	my ($analyze_details)=@_;
	
	my $action_name=$self->GetActionName();
	my $name=$action_name;
	$name=~tr/A-Z/a-z/;
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	my $generated=0;
	foreach my $ref (@{$self->{'EXTRA_DETAILS'}}) {
		my ($sfile,$source_dir,$dfile,$full_dest,$ftype)=@$ref;
		
		#Use the deploy (compiled) version of the jar when extracting the manifest
		my $full_deploy_path=&::ConcatPathString($full_dest,$self->GetDeployNameForJar($dfile));
		my $full_jar_path=&::ConcatPathString($full_dest,$dfile);
		my $full_source_path=&::ConcatPathString($source_dir,$sfile);
			
		#Skip non jar base files
		my ($dir,$base_name,$ext)=&::GetFileParts($full_jar_path,3);
		if ($ext!~/jar|war|ear/i) {
			next;
		}
		
		#If we aren't forcing extra details, check if there are custom files for the archive
		if ($analyze_details->GetExtraDetailsAlwaysFlag()!=1) {
			my @custom_files=();
			my ($dir,$jar_basename)=&::GetFileParts($full_jar_path,2);
			if ($self->GetCustomFilesForArchive($jar_basename,$ftype,$full_dest,\@custom_files)!=1) {
				return 0;
			}
			
			#If no custom files, skip this file since we aren't forcing extra details
			next if (scalar(@custom_files)==0);
		}
		
		#extract both jar manifests, or get them from memory if they were already extracted
		my @current_jm_info=();
		my @new_jm_info=();
		my $high_rev=0;
		
		if ($self->ExtractAndRetainFullManifest($full_deploy_path,\@current_jm_info,\$high_rev,1,0)!=1) {
			return 0;
		}
		if ($self->ExtractAndRetainFullManifest($full_source_path,\@new_jm_info,\$high_rev,1,0)!=1) {
			return 0;
		}
		
		#Use the information from the extracted jar manifests to calculate which custom files might be a problem
		my $impacts=0;
		if ($self->CalculateCustomImpacts(\@current_jm_info,\@new_jm_info,$analyze_details,\$impacts)!=1) {
			return 0;
		}
	
		#If there were custom impacts or if forcing extra details, write both manifests to analyze area
		if ($impacts!=0 || $analyze_details->GetExtraDetailsAlwaysFlag()==1) {
			my $base_jar=&::GetRelativePathName($full_jar_path,$full_dest);
			foreach my $fref ([$full_deploy_path,$analyze_details->Current(),\@current_jm_info],[$full_source_path,$analyze_details->New(),\@new_jm_info]) {
				my ($full_file_path,$current,$jm_sref)=@$fref;
				my $gen_dir=$analyze_details->GetGenDir($action_name,$current);
				my $export_name=&::ConcatPathString($gen_dir,$base_jar,"jar_manifest.csv");
				
				if ($self->SaveJarManifestToFile($export_name,$jm_sref)!=1) {
					return 0;
				}
				
				my $metadata=$analyze_details->GetMetadataMap($current);
				$metadata->RegisterFile($action_name,$metadata->GetJarManifestType(),$export_name);
				$generated=1;
			}
		}
	}
	
	if ($generated==1) {
		$analyze_details->SetDetailFileType($action_name,3);
	}

	return 1;
}

#########################
# Sub:  PreAction
# Desc: Override the default PreAction so that we always set PatchMadeChanges if JAVAAPP_FORCE_REDEPLOY=1 is set
#########################
sub PreAction {
	my $self=shift;
	
	if ($self->SUPER::PreAction()!=1) {
		return 0;
	}
	
	if ($self->{'FORCE_REDEPLOY'}==1) {
		my $global_state=$self->GetORPatchGlobalState();
		$global_state->SetPatchMadeChangesFlag();
	}
	
	return 1;
}

#########################
# Sub:  CopyPatchFiles
# Desc: Copy all our files, and then remove any outdated _internal files
#########################
sub CopyPatchFiles {
	my $self=shift;
	
	#Copy our files as normal
	if ($self->SUPER::CopyPatchFiles()!=1) {
		return 0;
	}
	
	#Now look for any internal files which are outdated due to new base files 
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	my $patch_name=$patch_info->GetPatchName();
	my @patch_list=$patch_info->GetPatchList();
	
	my $orpd=$self->_GetORPDeleteHandle();
	
	foreach my $dest_ref ($self->GetBaseTypeDestinations()) {
		my ($base_type,$full_dest,$dest_manifest,$dest_subdir)=@$dest_ref;

		my @dest_info=&Manifest::ReadManifest($dest_manifest);
		
		#Look for any base files that were updated in this patch
		my $base_file_info=&Manifest::FilterDestManifest(\@dest_info,$base_type,$patch_name,\@patch_list);
	
		#Skip ahead if no files were updated for this type
		next if (scalar(@$base_file_info)==0);
		
		&::LogMsg("Looking for outdated hotfix files for $base_type");
		
		my @outdated_files=();
		foreach my $ref (@$base_file_info) {
			my ($dfile,$sfile,$ftype,$frevision,$fdesc,$fcustomized,@discard)=&Manifest::DecodeDestLine($ref);
			my $full_jar_path=&::ConcatPathString($full_dest,$dfile);
			if ($self->FindOutdatedInternalFiles($dfile,$ftype,$frevision,$full_jar_path,\@outdated_files,{},$full_dest,\@dest_info,0)!=1) {
				return 0;
			}
		}
		
		#Now construct a remove file info memory structure so we can make ORPDeleteAction remove our files
		my @delete_info=();
		foreach my $of_ref (@outdated_files) {
			my ($rel_name,$del_di_ref,$incoming_rev)=@$of_ref;
			my $full_name=&::ConcatPathString($full_dest,$rel_name);
			push(@delete_info,$orpd->AssembleDeleteFileInfo($rel_name,$full_name,$patch_name));
		}
		
		if (scalar(@delete_info)!=0) {
			if ($orpd->DeleteFiles($dest_manifest,\@delete_info,$self)!=1) {
				return 0;
			}
			
			#Remove any empty directories
			my @rel_names=sort {length $b <=> length $a} map {$_->[0]} @delete_info;
			if ($self->RemoveEmptyDirs(\@rel_names,$full_dest)!=1) {
				return 0;
			}
		}
	}
	
	return 1;
}

#########################
# Sub:  PatchAction
# Desc: This is called after the files are copied, it should do the bulk of the work for an action
#########################
sub PatchAction {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $restart_state=$global_state->GetRestartState();
	my $restart_action_info=$restart_state->GetActionInfo();
	my ($phase,$restart_dest_subdir,$restart_file_type)=$self->DecodeRestartPoint($restart_action_info);
	
	if ($phase ne 'DEPLOY') {
		if ($self->CompileJars($restart_dest_subdir,$restart_file_type)!=1) {
			return 0;
		}
	}
	
	$self->MarkRestartPoint($restart_state,'DEPLOY','','');

	#Run the deploy
	if ($self->DeployApp()!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  CleanupAction
# Desc: Print a list of jars that need to be signed, if any
#########################
sub CleanupAction {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $restart_state=$global_state->GetRestartState();
	
	my @jars=$restart_state->GetCrossPhaseActionInfo($self->GetActionName());
	
	return 1 if scalar(@jars)==0;
	
	&::LogMsg("============== Manual Post Steps Required ==============");
	&::LogMsg("One or more jar files were modified during deploy which could not be signed");
	&::LogMsg("automatically because the keystore and signing alias are not configured.");
	&::LogMsg("The following jars MUST be manually signed after orpatch completes in order");
	&::LogMsg("for the deployed application to function correctly.");
	&::LogMsg("------- Start jars to sign -------");
	foreach my $jar (@jars) {
		&::LogMsg($jar);
	}
	&::LogMsg("-------  End  jars to sign -------");
	&::LogMsg("For more details on manually signing jars, please see the installation guide");
	&::LogMsg("============== Manual Post Steps Required ==============");
	
	return 1;
}

###############################################################################################

#########################
# Sub:  DeployApp
# Desc: Do the deploy of a Java app
# Arg:  None
# Ret:  1 on success, 0 on failure
#########################
sub DeployApp {
	my $self=shift;
	
	#Clean our detail log area
	my $detail_dir=$self->GetActionName();
	$detail_dir=~tr/A-Z/a-z/;
	my $detail_subdir='deploy';
	&DetailLog::CleanDetailLogDir($detail_dir,$detail_subdir);
	
	&::LogMsg("Executing application deployment for $detail_dir");
	
	if ($self->RunStandardDeploy('deploy')!=1) {
		return 0;
	}
	
	&::LogMsg("Deployment completed successfully");

	return 1;
}

#########################
# Sub:  RunStandardDeploy
# Desc: Run a standard deploy or precheck using the run_ant.sh script
# Arg:  action_phase - the target in the ant script to call (deploy or precheck)
# Ret:  1 on success, 0 on failure
#########################
sub RunStandardDeploy {
	my $self=shift;
	my ($action_phase)=@_;
	
	my $name=$self->GetActionName();
	$name=~tr/A-Z/a-z/;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	my %var_hash=();
	foreach my $var ('WEBLOGIC_DOMAIN_HOME','WL_HOME','MW_HOME','JAVA_HOME') {
		$var_hash{$var}=$self->{$var};
	}
	
	&::LogMsg("Calling run_ant.sh for $name ${action_phase}...");
	
	my $log_only=(($action_phase eq 'precheck') ? 1 : 0);
	
	my @output=();
	if (&Parallel::Tee(\&ORPAction::JavaApp::RunAnt,[$name,$dest_dir,$action_phase,\%var_hash],\@output,$log_only)!=1) {
		if ($log_only==1) {
			&::DebugOutput('run_ant.sh output',@output);
		}
		&::LogMsg("Error while executing run_ant.sh!");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  RunAnt
# Desc: Run the run_ant.sh script
# Arg:  log_obj - Object handle to our log file
#       name - name of the action this is being run for
#       dest_dir - RETAIL_HOME directory
#       action_phase - the target in the ant script to call (deploy or precheck)
#       env_var_href - ref to the hash of variables to set in ENV before calling run_ant
# Ret:  1 on success, 0 on failure
#########################
sub RunAnt {
	my ($log_obj,$name,$dest_dir,$action_phase,$env_var_href)=@_;
	
	my $ant_xml="${name}.xml";
	my $config_dir=&::ConcatPathString($dest_dir,'orpatch','config',$name);
	
	my $run_ant=&::ConcatPathString($dest_dir,'orpatch','deploy','bin','run_ant.sh');
	
	my $log_file=$log_obj->GetLogFile();
	
	my %old_vars=();
	foreach my $var (keys(%$env_var_href)) {
		$::ENV{$var}=$old_vars{$var}=$env_var_href->{$var};
	}
	
	my @output=`$run_ant $ant_xml $config_dir $action_phase > $log_file 2>&1`;
	my $ret=$?;
	
	foreach my $var (keys(%old_vars)) {
		$::ENV{$var}=$old_vars{$var};
	}
	
	if ($ret!=0) {
		$log_obj->LogMsgNoDate("Error while executing run_ant.sh!");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  GetBaseTypeDestinations
# Desc: Get a list of all of the base type destinations
# Arg:  restart_dest_subdir - an optional dest subdirectory to start at
# Ret:  a list of [base_type,full_dest,dest_manifest] entries for each base type destination
#########################
sub GetBaseTypeDestinations {
	my $self=shift;
	my ($restart_dest_subdir)=@_;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	my $fcp_ref=$self->{'FILE_COPY_PARAMS'};
	my $bt_ref=$self->GetBaseFileTypes();
	
	my $fcp_index=0;
	if ($restart_dest_subdir ne '') {
		#Skip ahead to the appropriate file copy param
		&::LogMsg("Restarting with subdirectory: $restart_dest_subdir");
		$fcp_index=$self->FindFileCopyParamsIndex($restart_dest_subdir,$fcp_ref);
	}
	
	#Loop through each file destination
	my @base_dests=();
	for (;$fcp_index<scalar(@$fcp_ref);$fcp_index++) {
		my $ref=$$fcp_ref[$fcp_index];
		my ($ft_ref,$dest_subdir)=$ref->GetBasicParams();
		
		my ($full_dest,$dest_manifest)=$self->GetDestDirAndManifest($dest_dir,$dest_subdir);
		
		my $base_type=$$bt_ref[$fcp_index];
		
		next if ($base_type eq '');
		
		push(@base_dests,[$base_type,$full_dest,$dest_manifest,$dest_subdir]);
	}

	return @base_dests;
}

#########################
# Sub:  SetBaseTypeUpdateCriteria
# Desc: If this is a cummulative patch, set all base types to ALWAYS update criteria
# Arg:  None
# Ret:  1 on success, 0 on failure
#########################
sub SetBaseTypeUpdateCriteria {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	return 1 unless ($patch_info->IsPatchCummulative());

	my $fcp_ref=$self->{'FILE_COPY_PARAMS'};
	my $bt_ref=$self->GetBaseFileTypes();
	
	#Loop through each file destination and update the FileCopyParams
	for (my $fcp_index=0;$fcp_index<scalar(@$fcp_ref);$fcp_index++) {
		my $base_type=$$bt_ref[$fcp_index];
		
		next if ($base_type eq '');
	
		my $ref=$$fcp_ref[$fcp_index];
		my ($ft_ref,$dest_subdir)=$ref->GetBasicParams();
		
		my %criteria=();
		foreach my $type (@$ft_ref) {
			my $current=$ref->GetUpdateCriteria($type);
			if ($type eq $base_type) {
				$current='ALWAYS';
			}
			$criteria{$type}=$current;
		}
		
		if ($ref->SetUpdateCriteria(\%criteria)!=1) {
			return 0;
		}
	}	

	return 1;
}

#########################
# Sub:  CompileJars
# Desc: Splice the base jars together with the config and internal files
# Arg:  None
# Ret:  1 on success, 0 on failure
#########################
sub CompileJars {
	my $self=shift;
	my ($restart_dest_subdir,$restart_file_type)=@_;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $restart_state=$global_state->GetRestartState();
	
	my $patch_mode=$self->{'PATCH_MODE'};
	
	my $patch_info=$global_state->GetPatchInfo();
	my $patch_name=$patch_info->GetPatchName();
	my @patch_list=$patch_info->GetPatchList();
	
	my @jars_to_sign=$restart_state->GetCrossPhaseActionInfo($self->GetActionName());
	
	foreach my $dest_ref ($self->GetBaseTypeDestinations($restart_dest_subdir)) {
		my ($base_type,$full_dest,$dest_manifest,$dest_subdir)=@$dest_ref;

		#Read the destination manifest
		my @dest_info=&Manifest::ReadManifest($dest_manifest);
		
		$self->MarkRestartPoint($restart_state,'COMPILE',$dest_subdir,$base_type);
		
		#Filter dest manifest by type
		my $all_f_dest_info=&Manifest::FilterDestManifest(\@dest_info,$base_type,'');
		#Filter out any files in env_manifest.csv that don't actually exist in the environment
		my $orig_f_dest_info=&Manifest::FilterDestManifestNonExistentFiles($full_dest,$all_f_dest_info);
		
		my $filtered_dest_info=$orig_f_dest_info;
		
		if ($self->CompileJarsForType($base_type,$filtered_dest_info,$full_dest,\@dest_info)!=1) {
			return 0;
		}
		
		#If there are any new jars needing signing, save them to the restart state
		my @new_jars_to_sign=&JarFile::GetJarsToSign();
		if (scalar(@new_jars_to_sign)!=0) {
			push(@jars_to_sign,@new_jars_to_sign);
			$restart_state->SetAndSaveCrossPhaseActionInfo($self->GetActionName(),\@jars_to_sign);
		}
	}

	return 1;
}

#########################
# Sub:  CompileJarsForType
# Desc: Compile all of the jars for a specific base type
# Arg:  base_type - the base type to process archives for
#       dest_info - a ref to the filtered dest manifest list of base files
#       full_dest - the full destination directory
#       full_dest_info_ref - Ref to the unfiltered destination manifest
# Ret:  1 on success, 0 on failure
#########################
sub CompileJarsForType {
	my $self=shift;
	my ($base_type,$dest_info_ref,$full_dest,$full_dest_info_ref)=@_;
	
	&::LogMsg("Compiling $base_type archives for deployment");
	
	foreach my $ref (@$dest_info_ref) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$fcustomized,@discard)=&Manifest::DecodeDestLine($ref);
		
		my $deploy_jar_file='';
		if ($self->CopyBaseJarForDeploy($full_dest,$dfile,\$deploy_jar_file)!=1) {
			return 0;
		}
		
		#Find Hotfix files to splice
		my ($dir,$base_jar_file)=&::GetFileParts($dfile,2);
		my $internal_dest_info=$self->GetInternalFilesForArchive($base_jar_file,$ftype,$full_dest,$full_dest_info_ref);
		
		#Find any config files to splice
		my @config_dest_info=();
		if ($self->GetConfigFilesForArchive($base_jar_file,$ftype,$full_dest,\@config_dest_info)!=1) {
			return 0;
		}
		
		#Find any custom files to splice
		my @custom_dest_info=();
		if ($self->{'INCLUDE_CUSTOM'}==1) {
			if ($self->GetCustomFilesForArchive($base_jar_file,$ftype,$full_dest,\@custom_dest_info)!=1) {
				return 0;
			}
		}
		
		#Consolidate the lists of splice files
		my @splice_files=();
		foreach my $dl_ref ($internal_dest_info,\@config_dest_info,\@custom_dest_info) {
			if (scalar(@$dl_ref)!=0) {
				push(@splice_files,@$dl_ref);
			}
		}

		if (scalar(@splice_files)!=0) {
			if ($self->CompileSingleJar($deploy_jar_file,$full_dest,\@splice_files)!=1) {
				return 0;
			}
		}
	}
	
	return 1;
}

#########################
# Sub:  GetConfigFilesForArchive
# Desc: Get all of the config files for a specific file type/jar
# Arg:  base_jar_file - The base jar file name
#       file_type - the file type of the jar
#       full_dest - the path to the destination area
#       cf_info_ref - ref to a list that will receive the fake dest manifest entries for the config files
# Ret:  1 on success, 0 on failure
#########################
sub GetConfigFilesForArchive {
	my $self=shift;
	my ($base_jar_file,$file_type,$full_dest,$cf_info_ref)=@_;
	
	my $config_type=$self->_GetConfigType($file_type);
	
	my $config_dir=$self->_GetConfigSubDirPrefix();
	my $full_path=&::ConcatPathString($full_dest,$config_dir,$base_jar_file);
	
	&::LogMsg("Looking for config files in $full_path");
	
	return 1 unless (-d $full_path);
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	my $patch_name='';
	my $frevision='';
	
	return $self->_FindConfigOrCustomFiles($config_type,$full_path,$full_dest,$patch_name,$frevision,'N',$cf_info_ref);
}

#########################
# Sub:  GetCustomFilesForArchive
# Desc: Get all of the custom files for a specific file type/jar
# Arg:  base_jar_file - The base jar file name
#       file_type - the file type of the jar
#       full_dest - the path to the destination area
#       cm_info_ref - ref to a list that will receive the fake dest manifest entries for the custom files
# Ret:  1 on success, 0 on failure
#########################
sub GetCustomFilesForArchive {
	my $self=shift;
	my ($base_jar_file,$file_type,$full_dest,$cm_info_ref)=@_;
	
	my $custom_type=$self->_GetCustomType($file_type);
	
	my $custom_dir=$self->_GetCustomSubDirPrefix();
	my $full_path=&::ConcatPathString($full_dest,$custom_dir,$base_jar_file);
	
	&::LogMsg("Looking for custom files in $full_path");
	
	if (!-d $full_path) {
		#Create the custom directory if it doesn't exist
		&::CreateDirs('0750',$full_path);
		return 1;
	}
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	my $patch_name='';
	my $frevision='';
	
	return $self->_FindConfigOrCustomFiles($custom_type,$full_path,$full_dest,$patch_name,$frevision,'Y',$cm_info_ref);
}

#########################
# Sub:  CopyBaseJarForDeploy
# Desc: Copy a base jar over to the deploy area
# Arg:  full_dest - the path to the full destination
#       jar_file - the relative path to the base jar
#       dest_file_ref - ref to a scalar that will receive the full destination path name of the copied file
# Ret:  1 on success, 0 on failure
#########################
sub CopyBaseJarForDeploy {
	my $self=shift;
	my ($full_dest,$jar_file,$dest_file_ref)=@_;
	
	my $full_src_path=&::ConcatPathString($full_dest,$jar_file);
	my $full_dst_path=&::ConcatPathString($full_dest,$self->GetDeployNameForJar($jar_file));

	my ($final_dir_string,$base_jar)=&::GetFileParts($full_dst_path,2);
	
	&::CreateDirs('0750',$final_dir_string);
	
	#Note this copy is not revertable so the customer must run the utility to recompile the jars after reverting a java patch
	&::LogMsg("Copying $base_jar to $full_dst_path");
	if (&File::Copy::copy($full_src_path,$full_dst_path)) {
		#Successful
	} else {
		#Copy failed!
		&::LogMsg("Unable to copy $full_src_path to $full_dst_path: $!");
		return 0;
	}
	
	$$dest_file_ref=$full_dst_path;
	
	return 1;	
}

#########################
# Sub:  GetDeployNameForJar
# Desc: Get the name where the deploy version of a base jar will be stored
# Arg:  jar_file - the relative path to the base jar, from full dest
# Ret:  The relative path to the deploy jar, from full dest
#########################
sub GetDeployNameForJar {
	my $self=shift;
	my ($jar_file)=@_;
	
	my ($dir,$base_jar)=&::GetFileParts($jar_file,2);
	return &::ConcatPathString($self->_GetDeploySubDirPrefix(),$base_jar);
}

#########################
# Sub:  CompileSingleJar
# Desc: Splice a list of files into a deployable jar
# Arg:  jar_file - the full path to the deploy jar
#       full_dest - the path to the full destination
#       splice_info_ref - ref to a list of destination manifest entries of the files to splice
# Ret:  1 on success, 0 on failure
#########################
sub CompileSingleJar {
	my $self=shift;
	my ($jar_file,$full_dest,$splice_info_ref)=@_;
	
	my $all_sign_ref=$self->GetOneSignAllSign();
	
	return &JarManifest::SpliceDestManifestIntoJar($jar_file,$full_dest,$splice_info_ref,$all_sign_ref);
}

#########################
# Sub:  FindOutdatedInternalFiles
# Desc: Find any _internal files that are now outdated when a particular base file is updated to a new rev
# Arg:  dfile - Relative filename that is being updated
#       ftype - the file type of the file being updated
#       new_rev - The new revision that the file will be updated to
#       new_jar - The full path to the new jar
#       outdated_ref - ref to an array that will be filled with [relative name,replaced di ref,incoming rev]
#                      if REVERT_ALL_HOTFIXES==1, incoming rev will always be ''.  It may also be '' if the file is not incoming in the new ear
#       df_ref - Ref to a hash by destination relative file name of files which are getting new revisions with this patch
#       full_dest - The full path to the destination directory
#       dest_info_ref - Ref to a list of the destination manifest entries (unfiltered)
#       conflicts_are_outdated - 1 to consider potential conflict files as outdated, 0 to not
# Ret:  1 on success, 0 on failure
#########################
sub FindOutdatedInternalFiles {
	my $self=shift;
	my ($dfile,$ftype,$new_rev,$new_jar,$outdated_ref,$df_ref,$full_dest,$dest_info_ref,$conflicts_are_outdated)=@_;

	my ($dir,$base_jar_file)=&::GetFileParts($dfile,2);
	
	#Get the list of internal files that apply to this jar
	my $internal_dest_info=$self->GetInternalFilesForArchive($base_jar_file,$ftype,$full_dest,$dest_info_ref);
	
	return 1 if (scalar(@$internal_dest_info)==0);
	
	#If an ear file overrides all hotfixes, consider all internal files for this archive outdated
	if ($self->{'REVERT_ALL_HOTFIXES'}==1) {
		&::LogMsg("Considering all hotfix files for $base_jar_file as outdated due to configuration");
		foreach my $ref (@$internal_dest_info) {
			my ($dfile,@discard)=&Manifest::DecodeDestLine($ref);
			push(@$outdated_ref,[$dfile,$ref,'']);
		}			
		return 1;
	}
	
	#Extract the full recursive jar manifest from the new jar
	my @jm_info=();
	my $jm_highest_rev=0;
	if ($self->ExtractAndRetainFullManifest($new_jar,\@jm_info,\$jm_highest_rev,1,0)!=1) {
		return 0;
	}
	
	foreach my $ref (@$internal_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$fcustomized,@discard)=&Manifest::DecodeDestLine($ref);
		
		#If an existing internal file is included in the patch
		#use the incoming revision instead of the destination manifest revision for comparisons
		my $full_dest_file=&::ConcatPathString($full_dest,$dfile);
		my $incoming_rev=$df_ref->{$full_dest_file};
		$frevision=$incoming_rev if $incoming_rev ne '';
		
		#If the same file exists in the ear, use that revision otherwise ear's highest revision
		my $jm_file_ref=$self->FindFileInJarManifest($dfile,\@jm_info);
		my $ear_rev=$jm_highest_rev;
		if ($jm_file_ref ne '') {
			my ($jm_dfile,$jm_sfile,$jm_ftype,$jm_rev,@discard)=&JarManifest::DecodeLine($jm_file_ref);
			$ear_rev=$jm_rev;
		}
		
		#If the file is newer than the ear it can stay, otherwise it either needs to be deleted or is a conflict
		my $keep=1;
		my $incoming_jm_rev='';
		if (&PatchFileCopy::RevisionCompare($ear_rev,$frevision)>=0) {
			#File is older or the same revision as the ear, don't keep it
			$keep=0;
		} else {
			#File is newer than the ear, don't keep it if conflicts are considered outdated
			if ($conflicts_are_outdated==1) {
				$keep=0;
			}
			$incoming_jm_rev=$ear_rev;
		}
		
		if ($keep==0) {
			#An outdated file
			push(@$outdated_ref,[$dfile,$ref,$incoming_jm_rev]);
		}	
	}
	
	return 1;
}

#########################
# Sub:  GetInternalFilesForArchive
# Desc: Get all of the internal files associated with a specific jar
# Arg:  base_jar_file - basename of the jar file (rpm14.ear, for example)
#       base_type - The type of the jar file
#       full_dest - full path to the destination directory
#       dest_info_ref - Ref to the unfiltered destination manifest info
# Ret:  ref to the list of destination info refs that apply to this archive
#########################
sub GetInternalFilesForArchive {
	my $self=shift;
	my ($base_jar_file,$base_type,$full_dest,$dest_info_ref)=@_;
	
	my $internal_type=$self->_GetInternalType($base_type);
	
	#Find internal files for the file type
	my $all_int_dest_info=&Manifest::FilterDestManifest($dest_info_ref,$internal_type,'');
	my $internal_dest_info=&Manifest::FilterDestManifestNonExistentFiles($full_dest,$all_int_dest_info);

	my @filtered_dest_info=();
	
	my $search=&::ConcatPathString($self->_GetInternalSubDirPrefix(),$base_jar_file);
	
	#Filter the internal files to find ones that are in this jar
	foreach my $di_ref (@$internal_dest_info) {
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$fcustomized,@discard)=&Manifest::DecodeDestLine($di_ref);
		
		if ($dfile=~/^$search/) {
			push(@filtered_dest_info,$di_ref);
		}
	}
	
	return \@filtered_dest_info;
}

#########################
# Sub:  GetFileTypeInfo
# Desc: Get the details on how to run a specific file type
# Arg:  file type
# Ret:  sign_jars
#########################
sub GetFileTypeInfo {
	my $self=shift;
	my ($file_type)=@_;
	
	my $def_base_type=1;
	my $def_sign_jars=0;
	foreach my $ref (@{$self->{'FILE_TYPE_INFO'}}) {
		if ($$ref[0] eq $file_type) {
			my $base_type=$$ref[1];
			$base_type=$def_base_type if ($base_type eq '');
			my $sign_jars=$$ref[2];
			$sign_jars=$def_sign_jars if ($sign_jars eq '');
			return ($base_type,$sign_jars);
		}
	}

	return ($def_base_type,$def_sign_jars);
}

#########################
# Sub:  FindFileInJarManifest
# Desc: Search a jar manifest for a specific file
# Arg:  dfile - Destination filename to search for
#       jm_info_ref - Ref to the list of jar manifest rows
# Ret:  ref of first row to match the dfile, '' if not found 
#########################
sub FindFileInJarManifest {
	my $self=shift;
	my ($dfile,$jm_info_ref)=@_;
	
	my $search_file=$dfile;
	my $internal_prefix=$self->_GetInternalSubDirPrefix();
	$search_file=~s/^$internal_prefix[\/\\]//;
	
	#&::LogMsg("Searching jar manifest for $search_file");
	
	foreach my $ref (@$jm_info_ref) {
		my ($jm_dfile,@discard)=&JarManifest::DecodeLine($ref);
		if ($search_file eq $jm_dfile) {
			return $ref;
		}
	}
	
	return '';
}

#########################
# Sub:  FindBaseTypePatchChanges
# Desc: Look through a list of patch changes and save the files that will be changed for each base type
# Arg:  files_ref - Ref to the list of changes from CheckPatchForAction
# Ret:  ref to a hash by base_type where each value is a ref to a list of the dest manifest rows that will be replaced
#########################
sub FindBaseTypePatchChanges {
	my $self=shift;
	my ($files_ref)=@_;

	#Make a base type lookup map
	my %bt_lookup=();
	my $bt_ref=$self->GetBaseFileTypes();
	foreach my $base_type (@$bt_ref) {
		$bt_lookup{$base_type}=1;
	}
	
	#Organize changing files by base type
	my %updated_files=();
	foreach my $ref (@$files_ref) {
		my ($fname,$new_rev,$dfname,$di_ref,$patch_name)=@$ref;
		
		#New ear files (that don't have replaced di_ref entries) shouldn't have hotfix files already existing
		#this could cause the analyze to be slightly incorrect, but the files that would be impacted
		#were orphaned at some point so weren't being used anyways
		#Fixing requires the base checkpatchforaction to include the source file type for new files
		next if ($di_ref eq '');
		
		my ($dfile,$sfile,$ftype,$frevision,$fdesc,$fcustomized,@discard)=&Manifest::DecodeDestLine($di_ref);
		if ($bt_lookup{$ftype}==1) {
			my $uf_ref=$updated_files{$ftype};
			if ($uf_ref eq '') {
				$uf_ref=[];
			}

			push(@$uf_ref,[$di_ref,$patch_name]);
			$updated_files{$ftype}=$uf_ref;
		}
	}
	return \%updated_files;
}

#########################
# Sub:  _GetInternalType
# Desc: Get the proper 'internal' type name for a given base type
#########################
sub _GetInternalType {
	my $self=shift;
	my ($type)=@_;	
	return "${type}_internal";
}

#########################
# Sub:  _GetConfigType
# Desc: Get the proper 'config' type name for a given base type
#########################
sub _GetConfigType {
	my $self=shift;
	my ($type)=@_;
	return "${type}_config";
}

#########################
# Sub:  _GetCustomType
# Desc: Get the proper 'custom' type name for a given base type
#########################
sub _GetCustomType {
	my $self=shift;
	my ($type)=@_;
	return "${type}_custom";
}

#########################
# Sub:  _GetInternalSubDirPrefix
# Desc: Get the directory name where internal files will be stored
#########################
sub _GetInternalSubDirPrefix {
	return 'internal';
}

#########################
# Sub:  _GetBaseSubDirPrefix
# Desc: Get the directory name where the base files will be stored
#########################
sub _GetBaseSubDirPrefix {
	return 'base';
}

#########################
# Sub:  _GetDeploySubDirPrefix
# Desc: Get the directory name where deployable files will be stored
#########################
sub _GetDeploySubDirPrefix {
	return 'deploy';
}

#########################
# Sub:  _GetConfigSubDirPrefix
# Desc: Get the directory name where internal files will be stored
#########################
sub _GetConfigSubDirPrefix {
	return 'config';
}

#########################
# Sub:  _GetCustomSubDirPrefix
# Desc: Get the directory name where custom files will be stored
#########################
sub _GetCustomSubDirPrefix {
	return 'custom';
}

#########################
# Sub:  _GetORPDeleteHandle
# Desc: Get a handle to the ORPDeleteAction action object so we can call methods on it
# Note: This function depends on the name of the action defined in ORPDeleteAction.pm
#########################
sub _GetORPDeleteHandle {
	my $self=shift;
	my $global_state=$self->GetORPatchGlobalState();
	
	my $orpdelete_handle=$global_state->GetActionHandle('ORPDELETE');
	return $orpdelete_handle;
}

#########################
# Sub:  DecodeRestartPoint
# Desc: Decode the action info that was saved in the restart point by this action
# Args: action info string returned from restart state
# Ret:  processing phase,dest subdir, file type
#########################
sub DecodeRestartPoint {
	my $self=shift;
	my ($restart_point)=@_;
	
	my ($phase,$dest_subdir,$file_type)=split(/\:/,$restart_point);
	return ($phase,$dest_subdir,$file_type);
}

#########################
# Sub:  MarkRestartPoint
# Desc: Record a restart point within a particular action
# Args: Restart state handle, processing phase, dest subdir, file type
# Ret:  1 on success, 0 on failure
#########################
sub MarkRestartPoint {
	my $self=shift;
	my ($restart_state,$phase,$dest_subdir,$type)=@_;
	
	return $restart_state->SetAndSaveActionInfo("$phase:$dest_subdir:$type");
}

#########################
# Sub:  GetBaseFileTypes
# Desc: Get the base types that have been registered for file copies
# Args: None
# Ret:  ref to the base file types array
#########################
sub GetBaseFileTypes {
	my $self=shift;
	return $self->{'BASE_FILE_TYPES'};
}

#########################
# Sub:  _FindConfigOrCustomFiles
# Desc: Find all of the files in a config or custom directory tree and create fake destination manifest entries for them
# Args: file_type - the type to apply to the files found
#       full_path - the path to the directory to read
#       base_dir - The directory to make files relative to
#       patch_name - the patch name to associate the config files with
#       frevision  - the revision to attach to the files
#       custom_flag - Y or N depending on whether the files should be marked as custom
#       cf_info_ref - ref to the list that will receive the entries
# Ret:  1 on success, 0 on failure
#########################
sub _FindConfigOrCustomFiles {
	my $self=shift;
	my ($file_type,$full_path,$base_dir,$patch_name,$frevision,$custom_flag,$cf_info_ref)=@_;
	
	my @dirs=();
	if (&::GetFilesInDir($full_path,\@dirs,1,'','',0)!=1) {
		return 0;
	}
	
	foreach my $file (@dirs) {
		my $rel_file=&::GetRelativePathName($file,$base_dir);
		my $ref=&Manifest::AssembleDestLine($rel_file,$rel_file,$file_type,$frevision,$patch_name,$custom_flag,'','N','','','');
		push(@$cf_info_ref,$ref);
	}
	
	return 1;
}

#########################
# Sub:  ExportGeneratedConfig
# Desc: Export any configuration that does not exist as a file
# Args: gen_dir - path to the dir to place generated files (must be created before use)
#       metadata_map - handle to the ORPMetadataMap object to register fies with
#       manifest_list_ref - ref to the list to add exported files to
#       support_dir - ref to the base directory for exports
# Ret:  1 if successful, 0 if not
#########################
sub ExportGeneratedConfig {
	my $self=shift;
	my ($gen_dir,$metadata_map,$manifest_list_ref,$support_dir)=@_;
	
	my $action_name=$self->GetActionName();
	my $lower_name=$action_name;
	$lower_name=~tr/A-Z/a-z/;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	#Find all our base files 
	foreach my $dest_ref ($self->GetBaseTypeDestinations()) {
		my ($base_type,$full_dest,$dest_manifest,$dest_subdir)=@$dest_ref;

		my @dest_info=&Manifest::ReadManifest($dest_manifest);
		
		#Look for any base files
		my $base_file_info=&Manifest::FilterDestManifest(\@dest_info,$base_type,'','');
		
		foreach my $ref (@$base_file_info) {
			my ($dfile,$sfile,$ftype,$frevision,$fdesc,$fcustomized,@discard)=&Manifest::DecodeDestLine($ref);

			#Use the deploy (compiled) version of the jar when extracting the manifest
			my $full_jar_path=&::ConcatPathString($full_dest,$self->GetDeployNameForJar($dfile));
			
			#Skip non jar base files
			my ($dir,$base_name,$ext)=&::GetFileParts($full_jar_path,3);
			if ($ext!~/jar|war|ear/i) {
				next;
			}
			
			my $base_jar=&::GetRelativePathName($full_jar_path,$full_dest);
			my $export_name=&::ConcatPathString($gen_dir,$base_jar,"jar_manifest.csv");
			
			if ($self->ExtractJarManifestToFile($full_jar_path,$export_name)!=1) {
				return 0;
			}
			
			if (-f $export_name) {
				push(@$manifest_list_ref,$export_name);
				
				#Files must be registered with the MetadataMap as if they were relative to RETAIL_HOME
				#so pretend that these existed at $RETAIL_HOME/generated
				my $fake_name=&::ConcatPathString($dest_dir,&::GetRelativePathName($export_name,$support_dir));
				$metadata_map->RegisterFile($action_name,$metadata_map->GetJarManifestType(),$fake_name);
			}
		}
	}
	
	return 1;
}

#########################
# Sub:  ExtractJarManifestToFile
# Desc: Export the full env manifest for a given jar file into a file
# Args: full_jar_path - Full path to the jar to extract the manifest from
#       export_file - full path to the file to save the jar manifest to
# Ret:  1 if successful, 0 if not
#########################
sub ExtractJarManifestToFile {
	my $self=shift;
	my ($full_jar_path,$export_file)=@_;
	
	my @jm_info=();
	my $high_rev=0;
	if ($self->ExtractAndRetainFullManifest($full_jar_path,\@jm_info,\$high_rev,0,0)!=1) {
		return 0;
	}
	
	return $self->SaveJarManifestToFile($export_file,\@jm_info);
}

#########################
# Sub:  SaveJarManifestToFile
# Desc: Save a jar manifest into a file
# Args: export_file - full path to the file to save the jar manifest to
#       jm_info_ref - Ref to the jar manifest info to save
# Ret:  1 if successful, 0 if not
#########################
sub SaveJarManifestToFile {
	my $self=shift;
	my ($export_file,$jm_info_ref)=@_;
	
	my ($exp_dir,$file)=&::GetFileParts($export_file,2);
	&::CreateDirs('0750',$exp_dir);
	
	if (&Manifest::SaveDestManifest($export_file,[],$jm_info_ref,{},1)!=1) {
		&::LogMsg("Unable to save extracted manifest to $export_file");
		return 0;
	}
	
	chmod(0750,$export_file);
	
	return 1;
}

#########################
# Sub:  GetActionConfigList
# Desc: Get a list of config files for this app
# Args: manifest_list_ref - ref to the list onto which we add our exported manifests
# Ret:  1 if successf, 0 if not
#########################
sub GetActionConfigList {
	my $self=shift;
	my ($manifest_list_ref)=@_;
	
	if ($self->SUPER::GetActionConfigList($manifest_list_ref)!=1) {
		return 0;
	}
	
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	my $config_dir=$self->_GetConfigSubDirPrefix();
	
	#Find all our config files in each base destination
	foreach my $dest_ref ($self->GetBaseTypeDestinations()) {
		my ($base_type,$full_dest,$dest_manifest,$dest_subdir)=@$dest_ref;

		my @dest_info=&Manifest::ReadManifest($dest_manifest);
		
		#Look for any base files
		my $base_file_info=&Manifest::FilterDestManifest(\@dest_info,$base_type,'','');
	
		#Skip ahead if no base files
		next if (scalar(@$base_file_info)==0);
		
		my $full_config_dir=&::ConcatPathString($full_dest,$config_dir);
	
		my @config_list=();
		#We pass mostly nonsense when finding files, which is OK because all we care about is the file name
		if ($self->_FindConfigOrCustomFiles('',$full_config_dir,$full_config_dir,'','','N',\@config_list)!=1) {
			return 0;
		}
	
		foreach my $di_ref (@config_list) {
			my ($src_config,@discard)=&Manifest::DecodeDestLine($di_ref);
			
			$src_config=&::ConcatPathString($full_config_dir,$src_config);
			
			#We don't copy jar config files (i.e. hibernate for RPM)
			my ($dest_dir,$discard,$ext)=&::GetFileParts($src_config,3);
			next if ($ext eq 'jar');
			
			push(@$manifest_list_ref,$src_config);
		}
	}
	
	#Grab the ant.deploy.properties file and weblogic policy file if they exist
	my $name=$self->GetActionName();
	$name=~tr/A-Z/a-z/;
	my $ant_deploy=&::ConcatPathString($dest_dir,'orpatch','config',$name,'ant.deploy.properties');
	my $wl_policy=&::ConcatPathString($self->{'WL_HOME'},'server','lib','weblogic.policy');
	my $nm_properties=&::ConcatPathString($self->{'WL_HOME'},'common','nodemanager','nodemanager.properties');
	my $jps_config=&::ConcatPathString($self->{'WEBLOGIC_DOMAIN_HOME'},'config','fmwconfig','jps-config.xml');
	foreach my $cfg_file ($ant_deploy,$wl_policy,$nm_properties,$jps_config) {
		if (-f $cfg_file) {
			push(@$manifest_list_ref,$cfg_file);
		}
	}
	
	return 1;
}

#########################
# Sub:  SetupMetadataEnvironment
# Desc: Setup any necessary settings to support comparing/exporting metadata
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub SetupMetadataEnvironment {
	my $self=shift;
	
	if ($self->SetJavaHome()!=1) {
		return 0;
	}
	
	if ($self->CheckRequiredBinaryOnce('jar')!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  SetJavaHome
# Desc: Setup the JAVA_HOME environment variable and ensure it is in the path
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub SetJavaHome {
	my $self=shift;
	
	if (exists($ENV{JAVA_HOME})) {
		my $cfg_java_home=$self->{'JAVA_HOME'};
		my $env_java_home=$ENV{'JAVA_HOME'};
		
		#If they have JAVA_HOME defined in the environment, use that instead
		&::LogMsg("Ignoring env_info.cfg setting for JAVA_HOME, using JAVA_HOME=$env_java_home");
		$self->{'JAVA_HOME'}=$env_java_home;
	} else {
		if ($self->{'JAVA_HOME'} eq '') {
			&::LogMsg("ERROR: JAVA_HOME must be set, either in env_info.cfg or in environment");
			return 0;
		}
		$ENV{JAVA_HOME}=$self->{'JAVA_HOME'};
		&::LogMsg("Using JAVA_HOME: ".$self->{'JAVA_HOME'});
	}
	my $java_home=$self->{'JAVA_HOME'};
	
	if (!-d $java_home) {
		&::LogMsg("ERROR: Specified JAVA_HOME $java_home does not exist!");
		return 0;
	}
	
	#Make sure JAVA_HOME/bin is in the path
	$ENV{PATH}="$java_home/bin:".$ENV{PATH};
	return 1;
}

sub RegisterSigningInfo {
	my $self=shift;
	
	#only proceed if there are types that have signed jars
	return 1 if $self->{'JARSIGN_ENABLE'}==0;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $dest_dir=$global_state->GetDestDir();
	
	my $sign_info_registered=$self->GetORPatchGlobalState()->GetCrossActionFlag("JARSIGN_REGISTER");
	unless ($sign_info_registered==1) {
		my $alias=$self->{'JARSIGN_ALIAS'};
		if ($alias ne '') {
			&::LogMsg("Registering jar signing with alias: $alias");
			if ($self->CheckRequiredBinaryOnce('jarsigner')!=1) {
				return 0;
			}
			if ($self->CheckRequiredBinaryOnce('keytool')!=1) {
				return 0;
			}
			if (&JarFile::RegisterSigningInfo($alias,$dest_dir)!=1) {
				return 0;
			}
			$self->GetORPatchGlobalState()->SetCrossActionFlag("JARSIGN_REGISTER",1);
		}
	}
	
	return 1;
}

#########################
# Sub:  CalculateCustomImpacts
# Desc: Take the current and incoming jar manifest information and try to determine
#       which customized files might conflict with the incoming patch
# Args: current_jm_info - Ref to the current jar manifest information
#       new_jm_info - Ref to the new jar manifest information
#       analyze_details - Handle to the ORPAnalyzeDetails object where we will store the custom impacts
#       impact_count_sref - ref to a scalar to receive the number of impacts
# Ret:  1 if successful, 0 if not
#########################
sub CalculateCustomImpacts {
	my $self=shift;
	my ($current_jm_info,$new_jm_info,$analyze_details,$impact_count_sref)=@_;
	
	#Convert the new jm info to a hash by file name
	my %new_lookup=();
	foreach my $ref (@$new_jm_info) {
		my ($dfile,$sfile,$ftype,$frevision,@discard)=&Manifest::DecodeDestLine($ref);
		$new_lookup{$dfile}=[$frevision,$ref];
	}
	
	my $impact_count=0;
	my $pure_custom_count=0;
	#Now find all customized files in the current jm info and look for
	foreach my $cur_ref (@$current_jm_info) {
		my ($cur_dfile,$cur_sfile,$cur_ftype,$cur_frevision,$cur_fdesc,$cur_custom,@discard)=&Manifest::DecodeDestLine($cur_ref);
		
		#Keep only customized files
		next unless ($cur_custom eq 'Y');
		
		#If the file has a blank current revision, it is a pure custom file
		#keep a count of these files, but don't treat them as archivecustomimpacts
		if ($cur_frevision eq '') {
			$pure_custom_count++;
			next;
		}

		#If we are here, we are a custom file that overrode a base file which had a revision

		#Get the revision for the same file from the new ear
		my $new_line=$new_lookup{$cur_dfile};
		$new_line=[] if ($new_line eq '');
		my ($new_rev,$new_ref)=@$new_line;
		
		if ($new_rev eq '' || &PatchFileCopy::RevisionCompare($cur_frevision,$new_rev)!=0) {
			#This custom file is likely a problem because they overrode a file that was updated in this patch
			#or overrode a file we used to provide but now don't ship
			$analyze_details->RegisterArchiveCustomImpact($self->GetActionName(),"custom/$cur_dfile");
			$impact_count++;
		}
	}
	
	$$impact_count_sref=$impact_count;
	
	$analyze_details->RegisterPureCustomCount($self->GetActionName(),$pure_custom_count);
	
	return 1;
}

#########################
# Sub:  ORDeployWrapper
# Desc: Wrapper for orcompile/ordeploy to ensure that manually signed jars are printed
#       after the deployment
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub ORDeployWrapper {
	my $self=shift;
	if ($self->PatchAction()!=1) {
		return 0;
	}
	if ($self->CleanupAction()!=1) {
		return 0;
	}
	return 1;
}

#########################
# Sub:  ORDeployWrapperNoCustom
# Desc: Wrapper for orcompile/ordeploy , forcing INCLUDE_CUSTOM=0
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub ORDeployWrapperNoCustom {
	my $self=shift;
	$self->SetIncludeCustom(0);
	return $self->ORDeployWrapper();
}

#########################
# Sub:  ORDeployWrapperWithCustom
# Desc: Wrapper for orcompile/ordeploy , forcing INCLUDE_CUSTOM=1
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub ORDeployWrapperWithCustom {
	my $self=shift;
	$self->SetIncludeCustom(1);
	return $self->ORDeployWrapper();
}

#########################
# Sub:  SetIncludeCustom
# Desc: Set the INCLUDE_CUSTOM flag depending on config settings, using the patch type as the default value
# Args: exact_value - Force the setting to an exact 1 or 0 value, if not set use the configuration settings
# Ret:  1
#########################
sub SetIncludeCustom {
	my $self=shift;
	my ($exact_value)=@_;
	
	if ($exact_value ne '') {
		$self->{'INCLUDE_CUSTOM'}=$exact_value;
		return 1;
	}
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	my $config_ref=$global_state->GetConfig();
	
	#The default is to NOT include custom files on a cummulative patch
	my $default_value=($patch_info->IsPatchCummulative() ? 0 : 1);
	
	$self->SetTFFlagFromConfigVar($config_ref,'INCLUDE_CUSTOM','JAVAAPP',$default_value);
	
	return 1;
}

#########################
# Sub:  ExtractAndRetainFullManifest
# Desc: Extract a full manifest for a jar, or get the cached jar manifest information if it has been previously extracted
# Args: jar_file - Full path to the jar file to extract from
#       jm_info_ref - Ref to the list that will receive the jar manifest info.  Do NOT modify the subrefs inside the jm_info_ref
#       jm_highest_rev_sref - Ref to the scalar that will receive the highest revision in the jar
#       require_extract - 1 to require extract to succeed
#       silent - 1 to not talk about extracting the manifest
# Ret:  1 on success, 0 on failure
#########################
sub ExtractAndRetainFullManifest {
	my $self=shift;
	my ($jar_file,$jm_info_ref,$jm_highest_rev_sref,$require_extract,$silent)=@_;
	
	my $extracted_href=$self->{'EXTRACTED_MANIFESTS'};
	
	#Check if we have already extracted this manifest
	my $ref=$extracted_href->{$jar_file};
	
	if ($ref ne '') {
		#already extracted, return the cached info
		@$jm_info_ref=@{$$ref[0]};
		$$jm_highest_rev_sref=$$ref[1];
	} else {
		#Extract the full manifest
		if (&JarManifest::ExtractFullManifest($jar_file,$jm_info_ref,$jm_highest_rev_sref,$require_extract,$silent)!=1) {
			&::LogMsg("Unable to extract jar manifest from $jar_file");
			return 0;
		}
		
		#We save a new list of the jar manifest info and the actual high rev value in-case the caller modifies the refs
		#It is still possible the caller could modify a deep ref, but unlikely because most of our code makes copies
		$extracted_href->{$jar_file}=[[@$jm_info_ref],$$jm_highest_rev_sref];
	}
	
	return 1;
}

#########################
# Sub:  SetRevertAllHotfixes
# Desc: force revert_all_hotfixes to 1 if this is a cumulative patch
# Args: None
# Ret:  1
#########################
sub SetRevertAllHotfixes {
	my $self=shift;
	
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	if ($patch_info->IsPatchCummulative()) {
		my $name=$self->GetActionName();
		&::LogMsg("Forcing ${name}_REVERT_ALL_HOTFIXES to 1 as this is a cumulative patch");
		$self->{'REVERT_ALL_HOTFIXES'}=1;
	}
	return 1;
}

#########################
# Sub:  RemoveEmptyDirs
# Desc: Remove all empty dirs left by removing internal hotfixes
# Args: path_ref - Ref to the list of paths that were removed
#       full_dest - the full_dest that the paths are relative to
# Ret:  1 on succes, 0 on failure
#########################
sub RemoveEmptyDirs {
	my $self=shift;
	my ($path_ref,$full_dest)=@_;
	foreach my $path (@$path_ref) {
		my ($dir,$file)=&::GetFileParts($path,2);
		
		#Split the directory string up
		my @parts=&::SplitPathString($dir);
		#Work back, stopping prior to trying to remove the first directory (internal)
		while(scalar(@parts)>1) {
			my $full_path=&::ConcatPathString($full_dest,@parts);
			#try to rmdir, but don't worry if it can't be removed because it may still have files
			rmdir($full_path) if -d $full_path;
			#now remove one level and continue
			pop(@parts);
		}
	}
	return 1;
}

#########################
# Sub:  SetOneSignAllSign
# Desc: Set a list of jar files where if we sign one of the files, we must sign them all
# Args: ref to a list of relative jar files that should all be signed together if any file is signed
# Ret:  1 on success, 0 on failure
#########################
sub SetOneSignAllSign {
	my $self=shift;
	my ($jar_list_ref)=@_;
	
	$self->{'ONE_SIGN_ALL_SIGN'}=[@$jar_list_ref];

	return 1;
}

#########################
# Sub:  GetOneSignAllSign
# Desc: Get the list of jar files where if we sign one of the files, we must sign them all
# Args: None
# Ret:  ref to the list of Jars
#########################
sub GetOneSignAllSign {
	my $self=shift;
	
	return $self->{'ONE_SIGN_ALL_SIGN'};
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
    my $class=shift;
	my ($name,$file_type_info_ref,$config_ref)=@_;
	
	my $dest_dir=$name;
	$dest_dir=~tr/A-Z/a-z/;
	
	my $need_signing=0;
	
	my @base_types=();
	my @file_copy_params=();
	foreach my $ref (@$file_type_info_ref) {
		my ($type,$archive_copies,$sign_jars,$fcp_ref)=@$ref;
		
		if ($archive_copies==1) {
			my $internal_type=$class->_GetInternalType($type);
			my $config_type=$class->_GetConfigType($type);
			my $custom_type=$class->_GetCustomType($type);
			
			my @type_list=($internal_type,$type,$config_type,$custom_type);
			my %type_subdir=();
			$type_subdir{$internal_type}=$class->_GetInternalSubDirPrefix();
			$type_subdir{$type}=$class->_GetBaseSubDirPrefix();
			$type_subdir{$config_type}=$class->_GetConfigSubDirPrefix();
			$type_subdir{$custom_type}=$class->_GetCustomSubDirPrefix();
			
			my $fcp=new FileCopyParams(\@type_list,$dest_dir,1);
			$fcp->SetFilePerms('0750');
			$fcp->SetExtraSubDirs(\%type_subdir);
			push(@file_copy_params,$fcp);

			push(@base_types,$type);
		} else {
			push(@file_copy_params,$fcp_ref);
			push(@base_types,'');
		}
		
		$need_signing=1 if $sign_jars==1;
	}
	
	my $self=$class->SUPER::new($name,\@file_copy_params,$config_ref);
	$self->{'FILE_TYPE_INFO'}=$file_type_info_ref;
	$self->{'BASE_FILE_TYPES'}=\@base_types;
	my $hfm=$self->{'PATCH_MODE'}=$config_ref->Get('PATCH_MODE');
	
	$self->{'EXTRA_DETAILS'}=[];
	$self->{'EXTRACTED_MANIFESTS'}={};
	
	my $wdh=$self->{'WEBLOGIC_DOMAIN_HOME'}=$config_ref->Get("${name}_WEBLOGIC_DOMAIN_HOME");
	my $wlh=$self->{'WL_HOME'}=$config_ref->Get("${name}_WL_HOME");
	my $mwh=$self->{'MW_HOME'}=$config_ref->Get("${name}_MW_HOME");
	
	my $java_home=$self->{'JAVA_HOME'}=$config_ref->Get("${name}_JAVA_HOME");
	if ($java_home eq '') {
		$self->{'JAVA_HOME'}=$config_ref->Get("JAVA_HOME");
	}
	
	$self->{'JARSIGN_ENABLE'}=$need_signing;
	if ($need_signing==1) {
		#Get the signing key
		$self->{'JARSIGN_ALIAS'}=$config_ref->Get("JARSIGN_ALIAS");
	}
	
	$self->{'ONE_SIGN_ALL_SIGN'}=[];

	$self->SetTFFlagFromConfigVar($config_ref,'FORCE_REDEPLOY','JAVAAPP',1);
	$self->SetTFFlagFromConfigVar($config_ref,'REVERT_ALL_HOTFIXES','JAVAAPP',0);
	
    return $self;
}

1;
