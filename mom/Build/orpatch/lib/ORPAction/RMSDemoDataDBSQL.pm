############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction::RMSDemoDataDBSQL;
our @ISA = qw(ORPAction::DBSQL);

use Cwd;

########################
# Sub:  PrePatchCheck
# Desc: Validate our Oracle environment and the connection to the DB before starting the patch
# Ret:  1 if the check was successful, 0 if not
########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;

	#Run the default DBSQL check
	if ($self->SUPER::PrePatchCheck($full_check)!=1) {
		return 0;
	}
	
	#If this is a full check, register our need for a working DBSQL_RMS configuration
	if ($full_check==1) {
		my $global_state=$self->GetORPatchGlobalState();
		
		#Register a new dependent prepatchcheck to run
		$global_state->AddExtraPrePatchCheck('DBSQL_RMS');
	}
	
	return 1;
}

#########################
# Sub:  PatchAction
# Desc: override the default PatchAction, doing nothing as our work is done in the PostAction phase
#########################
sub PatchAction {
	my $self=shift;
	
	return 1;
}

#########################
# Sub:  PostAction
# Desc: Run the RMS Demo script if this is an initial install
#########################
sub PostAction {
	my $self=shift;
	my ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,$subpatch_ref)=@_;
	
	#We call the default DBSQL PatchAction as our PostAction to run the action RMS demo data script
	#We can't run this during PatchAction because RMS objects aren't valid until all patchactions are complete
	#and the RMS postaction has run to compile invalid objects
	return $self->SUPER::PatchAction(@_);
}

#########################
# Sub:  LoadRMSDemoData
# Desc: a custom function to run demo data
#########################
sub LoadRMSDemoData {
	my $self=shift;
	my ($full_dest,$filtered_dest_info,$patch_name,$dest_manifest,$orig_f_dest_info)=@_;
	
	my $cscripts=&::ConcatPathString($full_dest,'Cross_Pillar/control_scripts/source');
	local $ENV{SQLPATH}="$cscripts:$ENV{SQLPATH}";
	
	my @demo_scripts=('run_rmsdemodata.sql');
	
	my ($proceed,$found_scripts_ref,$missing_scrips_ref)=$self->FindFilteredFilesInOrder($filtered_dest_info,$orig_f_dest_info,@demo_scripts);
	
	#If required files were missing, abort
	unless($proceed==1) {
		return 0;
	}

	if (scalar(@$found_scripts_ref)==0) {
		#No demo scripts to run
		return 1;
	}
	
	#Get the RMS schema owner name using the DBSQL_RMS connection information
	my $rms_name=$self->GetCrossActionSchemaName('DBSQL_RMS');
	return 0 if $rms_name eq '';
	
	#Create synonyms in our schema, pointing to RMS objects
	unless($self->CreateSynonymsForUser('',$rms_name)==1) {
		&::LogMsg("Failed to create synonyms to $rms_name schema");
		return 0;
	}
	
	#Run each install script by changing into the install scripts dir and executing it without a path
	return $self->RunDestFileListByBaseName($full_dest,$found_scripts_ref,[]);
}

#########################
# Sub:  new
# Desc: Setup a new Action object
#########################
sub new {
	my $class=shift;
	my ($config_ref)=@_;
	
	#This is a more detailed data structure then what is sent to the ORPAction constructor
	#It allows us to keep track of how to handle files in a particular file type category
	#For example, always run all files or never run files (because they are run some other way),etc
	#
	#[file_type,when_run,process_function,std_run_catch_errors]
	#   when_run is 0 - never run, 1 - always run filtered by dbmanifest, 2 - always run, 3 - only if updated and filtered by dbmanifest
	#   process_function is '' to use the standard DBSQL logic for running files, or the custom function name to call
	#   std_run_catch_errors is 1 if we should catch errors, 0 if not
	my @file_type_info=(
	['rmsdemo_db_install_scripts',	3,'LoadRMSDemoData',1],
	);
	
	my $self=$class->SUPER::new('DBSQL_RMSDEMO',\@file_type_info,$config_ref);
	$self->SetCheckLockedObjects(0);
	
    bless($self,$class);

	return $self;
}

1;
