#!/usr/bin/perl
############################################################################
# Copyright © 2016, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;
use File::Copy;

($ENV{RETAIL_HOME} eq '') && (die "ERROR: RETAIL_HOME must be defined!");
$::ORP_DIR="$ENV{RETAIL_HOME}/orpatch";
$::ORP_LIB_DIR="$::ORP_DIR/lib";

require "$::ORP_LIB_DIR/orpatch_include.pl";

$::ORP_CONFIG=new ConfigFile();

#CSV positioning variables
$::PRODUCT_COL=0;
$::FILE_COL=1;
$::DEST_DIR_COL=2;
$::REQUIRED_COL=3;
$::FILE_DIRECTION_COL=4;
$::NUM_FILES_COL=5;
%::FREQ_TO_COL=('DAILY'=>6,'WEEKLY'=>7,'QUARTERLY'=>8,'INTRADAY'=>9);
%::MODULE_TO_COL=('COMMONRI'=>10,'RMI'=>11,'RCI'=>12,'RSI'=>13,
                  'CLUSTERING'=>14,'CUSTOMERSEGMENTS'=>15,'ASO'=>16,'CDT'=>17,'DT'=>18);
$::TRIGGER_NAME_COL=19;

$::BFV_MODE='';
$::BFV_EXPAND_DIR='';
$::BFV_ARCHIVE_DIR='';

$::BFV_VALIDATE_FREQ='';
$::BFV_FILTER_LINES=[];
$::BFV_DEST_DIR='';

#Wait variables
$::BFV_WAIT_LIST=[];
$::BFV_INPUT_DIR='';
$::BFV_WAIT_TIME='';
$::BFV_WAIT_COMPLETE=1;
$::BFV_WAIT_REQUIRED=1;

#Validate variables
$::BFV_FILE_INPUT_CONFIG='';
$::BFV_VALIDATE_SERVICES=[];
$::BFV_VALIDATE_PRODUCT='';

#Export variables
$::BFV_OUTGOING_DIR='';
$::BFV_COLLECT_DIR='';

$::BFV_ZIP_FILE='';

&ValidateConfig();

my $exit_status=0;

if ($::BFV_MODE eq 'WAIT') {
	if (&WaitForFiles($::BFV_INPUT_DIR,$::BFV_WAIT_LIST,$::BFV_EXPAND_DIR,$::BFV_ARCHIVE_DIR,$::BFV_WAIT_TIME,$::BFV_WAIT_REQUIRED)!=1) {
		#Failure
		$exit_status=1;
	}
} elsif ($::BFV_MODE eq 'STAGE') {
	if (&StageFiles($::BFV_FILE_INPUT_CONFIG,$::BFV_INPUT_DIR,$::BFV_EXPAND_DIR,$::BFV_ARCHIVE_DIR,$::BFV_VALIDATE_SERVICES,$::BFV_VALIDATE_FREQ,$::BFV_VALIDATE_PRODUCT,$::BFV_FILTER_LINES)!=1) {
		#Failure
		$exit_status=1;
	}
} elsif ($::BFV_MODE eq 'VALIDATE') {
	if (&ValidateFiles($::BFV_FILE_INPUT_CONFIG,$::BFV_EXPAND_DIR,$::BFV_VALIDATE_SERVICES,$::BFV_DEST_DIR,$::BFV_VALIDATE_FREQ,$::BFV_VALIDATE_PRODUCT,$::BFV_FILTER_LINES)!=1) {
		#Failure
		$exit_status=1;
	}
} else {
	if (&ExportFiles($::BFV_ZIP_FILE,$::BFV_FILE_INPUT_CONFIG,$::BFV_COLLECT_DIR,$::BFV_OUTGOING_DIR,$::BFV_DEST_DIR,$::BFV_VALIDATE_FREQ,$::BFV_ARCHIVE_DIR,$::BFV_VALIDATE_SERVICES,$::BFV_VALIDATE_PRODUCT,$::BFV_FILTER_LINES)!=1) {
		#Failure
		$exit_status=1;
	}
}

&::ExitFileValidate($exit_status);

#########################
# Sub:  WaitForFiles
# Desc: Wait for the appearance of a list of files, up to a timeout
# Args: input_dir - Directory to find relative file names in
#       wait_list - ref to the list of filenames to wait for
#       expand_dir - directory to unzip/place the files into when they 'arrive'
#       base_archive_dir - directory to archive files in once they are processed
#       wait_time - max time in seconds to wait for files
#       required - 1 if the arrival of the file is required
# Rets: 1 on success, 0 on failure
#########################
sub WaitForFiles {
	my ($input_dir,$wait_list,$expand_dir,$base_archive_dir,$wait_time,$required)=@_;
	
	my @actual_wait=@$wait_list;
	my $file_age=0;
	if ($::BFV_WAIT_COMPLETE==1) {
		push(@actual_wait,'COMMAND/COMPLETE');
	} else {
		#If we don't wait on a complete file, then require the file
		#to be a minimum age
		$file_age=$::ORP_CONFIG->GetWithDefault('BATCH_NOCOMPLETE_FILE_AGE',15);
	}
	
	#Wait for the files to appear
	my @actual_files=();
	my $files_arrived=&WaitFileList($input_dir,\@actual_wait,$wait_time,$file_age,\@actual_files);
	if ($files_arrived!=1 && $required==1) {
		#Files did not arrive, and their arrival was required
		&::LogMsg("Not all expected files arrived within timelimit, aborting!");
		return 0;
	}
	
	#Clean/recreate expand dir
	if (&JarFile::CreateExpandDir($expand_dir)!=1) {
		return 0;
	}
	
	if ($files_arrived!=1) {
		#Files did not arrive, and were not required, so there is no more work to be done
		#Note that this comes after CreateExpandDir to ensure that the expand dir is empty when the file didn't arrive and was optional
		&::LogMsg("Not all expected files arrived, but their arrival was not required.  Exiting without error");
		return 1;
	}
	
	#Create the archive area
	my $archive_dir='';
	if (&CreateArchiveDir($base_archive_dir,\$archive_dir,'incoming')!=1) {
		return 0;
	}
	
	#Copy/expand files into expand dir, then archive
	my @remove_files=();
	if (&CopyFilesToExpand($input_dir,\@actual_files,$expand_dir,$archive_dir,\@remove_files,1,0,1)!=1) {
		return 0;
	}
	
	#Cleanup the original files
	if (&CleanSourceFiles(\@remove_files)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  WaitFileList
# Desc: Core loop that checks for a list of files and sleeps until they arrive
# Args: input_dir - Directory to find relative file names in
#       wait_list - ref to the list of filenames to wait for
#       wait_time - max time in seconds to wait for files
#       file_age  - Age in minutes of the last modification to the file before it is considered 'Present' - 0 disables
#       found_list - ref to the list of filenames we actually discovered
# Rets: 1 on success, 0 on failure
#########################
sub WaitFileList {
	my ($input_dir,$wait_list,$wait_time,$file_age,$found_list)=@_;
	
	my $end_time=time+$wait_time;
	my $missing=0;
	while(time<$end_time) {
		&::LogMsg("Checking for expected files in $input_dir:");
		$missing=0;
		my @found=();
		foreach my $file (@$wait_list) {
			my ($file_status,$full_file,$short_file)=&CheckFileStatus($input_dir,$file);
			if ($file_status==1) {
				my $cur_age=(time-(stat($full_file))[9])/60;
				if ($file_age!=0 && $cur_age<$file_age) {
					&::LogMsg("   TOO_NEW: $short_file ($cur_age mins)");
					$missing++;
				} else {						
					&::LogMsg("   PRESENT: $short_file");
					push(@found,$short_file);
				}
			} else {
				&::LogMsg("   MISSING: $short_file");
				$missing++;
			}
		}

		if ($missing==0) {
			&::LogMsg("All files present!");
			@$found_list=@found;
			return 1;
		}
		
		&::LogMsg("Missing $missing file(s), sleeping 30 seconds");
		sleep 30;
	}
	
	&::LogMsg("Wait time expired with $missing files still not present");
	return 0;
}

#########################
# Sub:  StageFiles
# Desc: Wait for the appearance of a list of files, up to a timeout
# Args: input_file_csv - batch input CSV file
#       input_dir - Directory to find relative file names in
#       expand_dir - directory to unzip/place the files into when they 'arrive'
#       base_archive_dir - directory to archive files in once they are processed, '' to disable archiving
#       enabled_server_ref - Ref to the list of enabled services
#       validate_freq - Frequency to filter against
#       validate_product - Product to validate against
#       filter_lines_ref - Ref to the list of filter lines
# Rets: 1 on success, 0 on failure
#########################
sub StageFiles {
	my ($input_file_csv,$input_dir,$expand_dir,$base_archive_dir,$enabled_service_ref,$validate_freq,$validate_product,$filter_lines_ref)=@_;
	
	&::LogMsg("--------------- Begin Stage Summary ------------");
	foreach my $ref (['BATCH_INPUT_CONFIG',$input_file_csv],['BATCH_EXPAND_DIR',$expand_dir],
	                 ['VALIDATE_PRODUCT',$validate_product],['VALIDATE_FREQUENCY',$validate_freq],['VALIDATE_SERVICES',join(',',@$enabled_service_ref)],
					 ['FILTER_TRIGGERS',join(',',@$filter_lines_ref)]) {
		&::LogMsg(sprintf("%-20s : %-30s",$ref->[0],$ref->[1]));
	}
	&::LogMsg("---------------  End  Stage Summary ------------");
	
	my ($status,$input_file_ref)=&ReadInputCSV($input_file_csv,$enabled_service_ref,$validate_freq,'INBOUND',$validate_product,$filter_lines_ref);
	return 0 if $status!=1;
	
	&::LogMsg("Staging files related to ".scalar(@$input_file_ref)." process(es)");
	
	my %verified=();
	my @actual_files=();
	foreach my $ref (@$input_file_ref) {
		my ($expect_file,$dest_dir,$required,$file_count)=&DecodeInputFileLine($ref);
		
		&::LogMsg("Checking for file $expect_file");
		
		if ($verified{$expect_file}==1) {
			&::LogMsg("  Skipping as file is already verified");
		} else {
			my $before_files=scalar(@actual_files);
			if (&FindExpectedFiles($expect_file,$input_dir,\@actual_files,0)!=1) {
				return 0;
			}
			my $after_files=scalar(@actual_files);
			&::LogMsg("  Found ".($after_files-$before_files)." file(s)");
			$verified{$expect_file}=1;
		}
	}
	
	#This would be weird but isn't technically a problem because they could be triggering only export jobs
	if (scalar(@actual_files)==0) {
		&::LogMsg("No incoming files found to stage!");
		return 1;
	}
	
	#Clean/recreate expand dir
	if (&JarFile::CreateExpandDir($expand_dir)!=1) {
		return 0;
	}
	
	#Create the archive area
	my $archive_dir='';
	if (&CreateArchiveDir($base_archive_dir,\$archive_dir,'incoming')!=1) {
		return 0;
	}
	
	#Copy/expand files into expand dir, then archive
	my @remove_files=();
	if (&CopyFilesToExpand($input_dir,\@actual_files,$expand_dir,$archive_dir,\@remove_files,0,0,0)!=1) {
		return 0;
	}
	
	#Cleanup the original files
	if (&CleanSourceFiles(\@remove_files)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  CheckFileStatus
# Desc: Check if a given file in a directory has appeared
#         If the file does not exist and has a zip extension, also check for .tar.gz and .tgz files
# Args: input_dir,file
# Rets: file_status - 1 if the file exists, 0 if not
#       full_file - the full path to the found file
#       short_file - the short file that we found
#########################
sub CheckFileStatus {
	my ($input_dir,$file)=@_;
	
	my $full_base_location=&::ConcatPathString($input_dir,$file);
	
	#First check if the exact file we were expecting is present
	if (-f $full_base_location) {
		return (1,$full_base_location,$file);
	}
	
	#Base file is not present

	#If the target file is not a zip file, report the file as missing
	my ($rel_dir,$base_file,$ext)=&::GetFileParts($full_base_location,3);
	if ($ext ne 'zip') {
		return (0,$full_base_location,$file);
	}
	
	#Check other supported extensions to see if those were uploaded instead of a zip
	my @exts=('tar.gz','tgz');
	foreach my $this_ext (@exts) {
		my $short_file="${base_file}.${this_ext}";
		my $full_file=&::ConcatPathString($rel_dir,$short_file);
		if (-f $full_file) {
			return (1,$full_file,$short_file);
		}
	}

	#No luck finding a file that looks like what we were expecting
	return (0,$full_base_location,$file);
}

#########################
# Sub:  ValidateFiles
# Desc: Validate that all required input files are present
# Args: input_file_csv, source dir, enabled_service_ref,default_dest_dir,validate_freq,validate_product,filter_lines_ref
# Rets: 1 on success, 0 on failure
#########################
sub ValidateFiles {
	my ($input_file_csv,$source_dir,$enabled_service_ref,$default_dest_dir,$validate_freq,$validate_product,$filter_lines_ref)=@_;
	
	&::LogMsg("--------------- Begin Validate Summary ------------");
	foreach my $ref (['BATCH_INPUT_CONFIG',$input_file_csv],['BATCH_EXPAND_DIR',$source_dir],['BATCH_DEST_DIR',$default_dest_dir],
	                 ['VALIDATE_PRODUCT',$validate_product],['VALIDATE_FREQUENCY',$validate_freq],['VALIDATE_SERVICES',join(',',@$enabled_service_ref)],
					 ['FILTER_TRIGGERS',join(',',@$filter_lines_ref)]) {
		&::LogMsg(sprintf("%-20s : %-30s",$ref->[0],$ref->[1]));
	}
	&::LogMsg("---------------  End  Validate Summary ------------");
	
	my ($status,$input_file_ref)=&ReadInputCSV($input_file_csv,$enabled_service_ref,$validate_freq,'INBOUND',$validate_product,$filter_lines_ref);
	return 0 if $status!=1;
	
	&::LogMsg("Verifying ".scalar(@$input_file_ref)." expected files");
	
	my %verified=();
	my @remove_files=();
	foreach my $ref (@$input_file_ref) {
		my ($expect_file,$dest_dir,$required,$file_count)=&DecodeInputFileLine($ref);
		
		if ($dest_dir eq '') {
			$dest_dir=$default_dest_dir;
		} else {
			$dest_dir=&ExpandEnvVars($dest_dir);
		}

		&::LogMsg("Verifying file $expect_file");
		
		if ($verified{$expect_file}==1) {
			&::LogMsg("  Skipping as file is already verified");
		} else {
			if (&VerifyFile($expect_file,$source_dir,$required,$file_count,$dest_dir)!=1) {
				&::LogMsg("File verification failed");
				return 0;
			}
			$verified{$expect_file}=1;
		}
	}
	
	&::LogMsg("File verification succeeded");
	return 1;
}

#########################
# Sub:  VerifyFile
# Desc: Confirm a file we expect to be present was actually received
# Args: expect_file - the file we are checking for
#       source_dir - the directory to find files
#       required - 1 if the file is required, 0 if it is optional
#       file_count - number of files we expect to receive (-1 allows any number>1, -2 allows any number>2, etc)
#       dest_dir - path where the files should be copied
# Rets: 1 on success, 0 on failure
#########################
sub VerifyFile {
	my ($expect_file,$source_dir,$required,$file_count,$dest_dir)=@_;
	
	my $strict=0;
	
	my @all_found_files=();
	if (&FindExpectedFiles($expect_file,$source_dir,\@all_found_files,$strict)!=1) {
		return 0;
	}
	
	#We may find .ctx files in the directory, but they don't count towards the counts and verifications below
	#so we create another list without them
	my @found_files=();
	foreach my $this_file (@all_found_files) {
		if ($this_file!~/\.ctx(\.gz)?/) {
			push(@found_files,$this_file);
		}
	}
	
	my $found_file_count=scalar(@found_files);
	
	#No files received
	if ($found_file_count==0) {
		#If this is a required file, we have a problem
		if ($required) {
			&::LogMsg("  ERROR: Required expected file $expect_file not received, aborting");
			return 0;
		}
		
		#If this was an optional file, we need to make sure the destination file is cleaned up
		#This is to prevent yesterday's file from being reused
		&::LogMsg("  WARNING: Optional file $expect_file not received, continuing");
		my ($dir,$short_name)=&::GetFileParts($expect_file,2);
		my $dest_file=&::ConcatPathString($dest_dir,$short_name);
		if (-f $dest_file) {
			&::LogMsg("  Prior input file $expect_file exists in $dest_dir, removing");
			if (&::RemoveFile($dest_file)!=1) {
				&::LogMsg("ERROR: error while removing $dest_file, aborting");
				return 0;
			}
		}
		
		return 1;
	}

	#we found some amount of files
	
	#Complain if we found the wrong number of files
	my $right_file_count=1;
	
	if ($file_count<0) {
		$right_file_count=($found_file_count>=(0-$file_count));
	} else {
		$right_file_count=($found_file_count==$file_count);
	}

	unless ($right_file_count) {
		$file_count=">=".(0-$file_count) if ($file_count<0);  #If we have an 'at least X' file count, convert it to something understandable in the log
		&::LogMsg("  ERROR: While checking $expect_file, expected $file_count files received $found_file_count, aborting");
		&::LogMsg("  Received files:");
		foreach my $file (@found_files) {
			my ($dir,$file)=&::GetFileParts($file,2);
			&::LogMsg("    $file");
		}
		return 0;
	}
	
	#At this point we found some files and want them, so do the copies
	#note that we copy everything in all_found_files, not just found_files to ensure we get .ctx files
	foreach my $file (@all_found_files) {
		my ($dir,$short_name)=&::GetFileParts($file,2);
		my $rel_name=&::GetRelativePathName($file,$source_dir);
		if (&PlaceFileIntoDir($file,$rel_name,$dest_dir,0,1)!=1) {
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  FindExpectedFiles
# Desc: Search the source directory for files that match a specific expected file
# Args: expect_file - The expected file
#       source_dir - The source directory
#       found_files_ref - Ref to the found files list to populate
#       strict - 1 to match only the expect file and the expect file.gz
# Rets: 1 on success, 0 on failure
#########################
sub FindExpectedFiles {
	my ($expect_file,$source_dir,$found_file_ref,$strict)=@_;
	
	my ($dir,$base,$ext)=&::GetFileParts($expect_file,3);

	my @match_patterns=();
	if ($strict) {
		push(@match_patterns,qr/^${base}\.${ext}(\.gz)?$/);
	} else {
		#.ctx exists in this pattern match because RI wants to be able to expect a W_PRODUCT_DS.dat file along with an optional W_PRODUCT_DS.dat.ctx file
		push(@match_patterns,qr/^${base}[0-9\-\.]*\.${ext}(\.ctx)?(\.gz)?$/);
	}
	
	#A matching file is anything that looks like the expected file
	# with optionally trailing numbers, dashes, or periods
	# and optionally with a compressed suffix
	#For Example these files would match an expected file of steve.csv
	#  steve.1.csv
	#  steve.2.csv
	#  steve1-2.csv
	#  steve2-2.csv
	#  steve.csv.gz
	# With strict matching only steve.csv and steve.csv.gz would match
	if (&::GetFilesInDir($source_dir,$found_file_ref,0,\@match_patterns)!=1) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  CleanSourceFiles
# Desc: Remove all the source files which were verified, copied and archived 
# Args: remove_file_ref - ref to the list of files to remove
#       source_dir - The source directory
# Rets: 1 on success, 0 on failure
#########################
sub CleanSourceFiles {
	my ($remove_file_ref)=@_;
	
	return 1 if (scalar(@$remove_file_ref)==0);
	
	&::LogMsg("  Removing original source files");
	foreach my $file (@$remove_file_ref) {
		if (-f $file) {
			if (&::RemoveFile($file)!=1) {
				&::LogMsg("ERROR: error while removing $file, aborting");
				return 0;
			}
		}
	}
	
	return 1;
}

#########################
# Sub:  CopyFilesToExpand
# Desc: Copy files we received over to the expand directory
# Args: input_dir - input directory
#       found_file_ref - ref to the list of files received
#       expand_dir - path where the files should be copied/expanded
#       archive_dir - path where archive copies of the files should be kept
#       remove_file_ref - ref to the list that will receive the names of the original upload files
#       expand_archives - 1 to expand archives into the expand directory, 0 to copy them
#       uncompress_files - 1 to uncompress regular files in the expand directory, 0 to copy them
#       remove_files - 1 to remove files, 0 to leave them
# Rets: 1 on success, 0 on failure
#########################
sub CopyFilesToExpand {
	my ($input_dir,$found_file_ref,$expand_dir,$archive_dir,$remove_file_ref,$expand_archives,$uncompress_files,$remove_files)=@_;
	
	&::LogMsg("Processing received files");
	foreach my $rel_file (@$found_file_ref) {
		my $file='';
		#If the 'relative' filename is already fully-qualified, don't prepend the input_dir
		if ($rel_file=~/^\//) {
			$file=$rel_file;
		} else {
			$file=&::ConcatPathString($input_dir,$rel_file);
		}
		my ($dir,$src_file)=&::GetFileParts($file,2);
		my $dest_file=&::ConcatPathString($expand_dir,$src_file);
		
		push(@$remove_file_ref,$file) if $remove_files==1;
		
		#We don't need to copy/archive COMMAND/COMPLETE files
		#but we do need them in remove_file_ref so they get cleaned up, which is why this is after the push
		next if $rel_file eq 'COMMAND/COMPLETE';

		#Place the file into the expand directory, extracting zip and tar.gz files
		if (&PlaceFileIntoDir($file,$src_file,$expand_dir,$expand_archives,$uncompress_files)!=1) {
			return 0;
		}
		
		if (&ArchiveFile($file,$archive_dir)!=1) {
			return 0;
		}
	}
	
	return 1;	
}

#########################
# Sub:  PlaceFileIntoDir
# Desc: Place files into a directory either by copying, uncompressing or exploding archives
#           Gzip compressed files will be uncompressed or copied depending on uncompress_files
#           tars and zip files will be unzipped or copied depending on expand_archives
#           Other files will be copied
# Args: src_file - full path to file in staging directory
#       rel_name - relative name of the file
#       dest_dir - path to the destination directory
#       expand_archives - 1 to unzip/untar archive files, 0 to copy them directly
#       uncompress_files - 1 to ungzip regular files, 0 to copy them directly
# Rets: 1 on success, 0 on failure
#########################
sub PlaceFileIntoDir {
	my ($src_file,$rel_name,$dest_dir,$expand_archives,$uncompress_files)=@_;
	
	my $dest_file=&::ConcatPathString($dest_dir,$rel_name);
	
	if ($dest_file=~/\.t?gz$/) {
		#The source file is gzipped
		
		#If it is a gzip'd tar, expand or copy it as per expand_archives setting
		if ($dest_file=~/(\.tar\.gz|\.tgz)$/) {
			if ($expand_archives==1) {
				&::LogMsg("  Untarring $rel_name to $dest_dir");
				my @output=`gzip -dc "$src_file" | tar -xvf - -C "$dest_dir"`;
				if ($?!=0) {
					&::LogMsg("ERROR: Unable to untar $src_file into $dest_dir: $!");
					&::DebugOutput('gzip+tar output',@output);
					return 0;
				}
			} else {
				&::LogMsg("  Copying file $rel_name to $dest_file");
				if (&_DoCopy($src_file,$dest_file)!=1) {
					return 0;
				}
			}
		} else {
			#Just a normal gzip file, copy or uncompress it per uncompress_files setting
			if ($uncompress_files==1) {
				$dest_file=~s/\.gz//;
				&::LogMsg("  Uncompressing $rel_name to $dest_file");
				my @output=`gzip -dc $src_file > $dest_file`;
				if ($?!=0) {
					&::LogMsg("ERROR: Unable to uncompress $src_file to $dest_file: $!");
					&::DebugOutput('gzip output',@output);
					return 0;
				}
			} else {
				&::LogMsg("  Copying file $rel_name to $dest_file");
				if (&_DoCopy($src_file,$dest_file)!=1) {
					return 0;
				}
			}
		} 
	} elsif ($dest_file=~/\.zip$/) {
		#If it is a zip file, expand or copy it as per expand_archives setting
		if ($expand_archives==1) {
			&::LogMsg("  Unzipping $rel_name to $dest_dir");
			my @output=`unzip -o "$src_file" -d "$dest_dir"`;
			if ($?!=0) {
				&::LogMsg("ERROR: Unable to unzip $src_file into $dest_dir: $!");
				&::DebugOutput('zip output',@output);
				return 0;
			}
		} else {
			&::LogMsg("  Copying file $rel_name to $dest_file");
			if (&_DoCopy($src_file,$dest_file)!=1) {
				return 0;
			}
		}
	} else {
		&::LogMsg("  Copying file $rel_name to $dest_file");
		if (&_DoCopy($src_file,$dest_file)!=1) {
			return 0;
		}
	}
	
	return 1;	
}

#########################
# Sub:  ExportFiles
# Desc: Collect available export files, create a zip of them and push it to the SFTP outgoing area
# Args: export_zip - The zip name to create
#       input_file_csv - The CSV describing the files associated with batches
#       collect_dir - The directory to put the export files in for zipping
#       outgoing_dir - The SFTP outgoing directory
#       default_dest_dir - The default directory to find export files in
#       validate_freq - The frequency we are collecting for
#       base_archive_dir - directory to archive files in once they are processed
#       enabled_service_ref - Ref to the list of services to collect export files for
#       validate_product - The product we are collecting for
#       filter_lines_ref - Ref to list of lines
# Rets: 1 on success, 0 on failure
#########################
sub ExportFiles {
	my ($export_zip,$input_file_csv,$collect_dir,$outgoing_dir,$default_dest_dir,$validate_freq,$base_archive_dir,$enabled_service_ref,$validate_product,$filter_lines_ref)=@_;
	
	&::LogMsg("--------------- Begin Export Summary ------------");
	foreach my $ref (['BATCH_INPUT_CONFIG',$input_file_csv],['BATCH_COLLECT_DIR',$collect_dir],['BATCH_OUTGOING_DIR',$outgoing_dir],
					 ['BATCH_ARCHIVE_DIR',$base_archive_dir],
	                 ['VALIDATE_PRODUCT',$validate_product],['VALIDATE_FREQUENCY',$validate_freq],['VALIDATE_SERVICES',join(',',@$enabled_service_ref)],['BATCH_EXPORT_NAME',$export_zip],
					 ['FILTER_TRIGGERS',join(',',@$filter_lines_ref)]) {
		&::LogMsg(sprintf("%-20s : %-30s",$ref->[0],$ref->[1]));
	}
	&::LogMsg("---------------  End  Export Summary ------------");
	
	my ($status,$input_file_ref)=&ReadInputCSV($input_file_csv,$enabled_service_ref,$validate_freq,'OUTBOUND',$validate_product,$filter_lines_ref);
	return 0 if $status!=1;
	
	&::LogMsg("Collecting ".scalar(@$input_file_ref)." possible export file(s)");
	
	if (scalar(@$input_file_ref)!=0) {
		#Clean/recreate collect dir
		if (&JarFile::CreateExpandDir($collect_dir)!=1) {
			return 0;
		}
	}
	
	#Collect the files
	my @remove_files=();
	if (&CollectFiles($input_file_ref,$collect_dir,$default_dest_dir,\@remove_files)!=1) {
		return 0;
	}
	
	if (scalar(@remove_files)!=0) {
		#Create the export zip and archive it
		if (&CreateExportZip($export_zip,$collect_dir,$outgoing_dir,$base_archive_dir)!=1) {
			&::LogMsg("Failed to create export files, aborting");
			return 0;
		}
		
		#Cleanup the original files
		if (&CleanSourceFiles(\@remove_files)!=1) {
			return 0;
		}
	} else {
		&::LogMsg("No files found to export");
	}
	
	&::LogMsg("File export succeeded");
	return 1;
}

#########################
# Sub:  CollectFiles
# Desc: Copy files into the collect_dir and track the source filenames
# Args: input_csv - The CSV describing the files associated with batches
#       collect_dir - The directory to put the export files in for zipping
#       default_dest_dir - The default directory to pick files from
#       remove_file_ref - Ref to the list that will get all collected source file names for later removal
# Rets: 1 on success, 0 on failure
#########################
sub CollectFiles {
	my ($input_file_ref,$collect_dir,$default_dest_dir,$remove_file_ref)=@_;
	
	my %verified=();
	foreach my $ref (@$input_file_ref) {
		my ($expect_file,$dest_dir,$required,$file_count)=&DecodeInputFileLine($ref);
		
		if ($dest_dir eq '') {
			$dest_dir=$default_dest_dir;
		} else {
			$dest_dir=&ExpandEnvVars($dest_dir);
		}

		&::LogMsg("Collecting file $expect_file");
		
		if ($verified{$expect_file}==1) {
			&::LogMsg("  Skipping as file is already collected");
		} else {
			my $source_file=&::ConcatPathString($dest_dir,$expect_file);
			my $collect_file=&::ConcatPathString($collect_dir,$expect_file);
			if (-f $source_file) {
				&::LogMsg("  File found, copying to collection directory");
				if (&_DoCopy($source_file,$collect_file)!=1) {
					return 0;
				}
				push(@$remove_file_ref,$source_file);
			} else {
				&::LogMsg("  File does not exist, skipping");
			}
			$verified{$expect_file}=1;
		}
	}
	
	return 1;
}

#########################
# Sub:  CreateExportZip
# Desc: create a zip of all files in the collect directory
# Args: export_zip - Zip name to create
#       collect_dir - Directory holding all our collected files
#       outgoing_dir - Directory to place the export zip in
#       base_archive_dir - Directory to copy the archive zip into
# Rets: 1 on success, 0 on failure
#########################
sub CreateExportZip {
	my ($export_zip,$collect_dir,$outgoing_dir,$base_archive_dir)=@_;
	
	#Create the archive area
	my $archive_dir='';
	if (&CreateArchiveDir($base_archive_dir,\$archive_dir,'outgoing')!=1) {
		return 0;
	}
	
	#We create the zip directly into the archive directory, and then subsequently copy to outgoing
	#since the outgoing area can be an NFS area this avoids one network transfer for the export file
	#and avoids making two copies of the files on local storage
	
	my $zip_file=&::ConcatPathString($archive_dir,$export_zip);
	if (-f $zip_file) {
		&::LogMsg("WARNING: Target outgoing zip file $zip_file already exists, it will be overwritten");
	}
	
	&::LogMsg("Creating export zip $zip_file");
	my @output=`zip -j "$zip_file" "$collect_dir"/* 2>&1`;
	if ($?!=0) {
		&::LogMsg("Error while creating zip file: $!");
		&::DebugOutput('zip output',@output);
		return 0;
	}
	
	#Copy zip to outgoing directory
	my $outgoing_zip=&::ConcatPathString($outgoing_dir,$export_zip);
	&::LogMsg("Copying export zip to $outgoing_dir");
	if (&_DoCopy($zip_file,$outgoing_zip)!=1) {
		return 0;
	}
	
	#Touch the COMMAND/COMPLETE file
	my $command_dir=&::ConcatPathString($outgoing_dir,'COMMAND');
	&::CreateDirs('0770',$command_dir);
	my $command_complete=&::ConcatPathString($command_dir,'COMPLETE');
	my @output2=`touch "$command_complete" 2>&1`;
	if ($?!=0) {
		&::LogMsg("Error while touching $command_complete: $!");
		&::DebugOutput('touch output',@output2);
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  _DoCopy
# Desc: Copy a file with error checking
# Args: src_file - File to copy
#       dest_file - Destination location
# Rets: 1 on success, 0 on failure
#########################
sub _DoCopy {
	my ($src_file,$dest_file)=@_;
	
	my ($dir,$file_name)=&::GetFileParts($dest_file,2);
	if (!-d $dir) {
		&::CreateDirs('0755',$dir);
	}
	
	if (File::Copy::copy($src_file,$dest_file)) {
		#Success
	} else {
		&::LogMsg("ERROR: Unable to copy $src_file to $dest_file: $!");
		return 0;
	}
	return 1;
}

#########################
# Sub:  ArchiveFile
# Desc: Archive the originally received files to our archive directory
# Args: file - the upload file
#       archive_dir - path where archive copies of the files should be kept
# Rets: 1 on success, 0 on failure
#########################
sub ArchiveFile {
	my ($file,$archive_dir)=@_;
	
	return 1 if $archive_dir eq '';
	
	my ($dir,$src_file)=&::GetFileParts($file,2);
	my $dest_file=&::ConcatPathString($archive_dir,$src_file);
	
	#If the source file is gzipped, just copy it
	if ($dest_file=~/\.gz$/ || $dest_file=~/\.zip$/ || $dest_file=~/\.tgz$/) {
		&::LogMsg("  Archiving $src_file");
		if (&_DoCopy($file,$dest_file)!=1) {
			return 0;
		}
	} else {
		$dest_file.='.gz';
		&::LogMsg("  Archiving and compressing $src_file");
		my @output=`gzip -c $file > $dest_file`;
		if ($?!=0) {
			&::LogMsg("ERROR: Unable to compress $file to $dest_file: $!");
			&::DebugOutput('gzip output',@output);
			return 0;
		}
	}
	
	return 1;
}

#########################
# Sub:  _CSVLineApplies
# Desc: Determine if a line from the CSV input file applies to our current enabled services and frequency we are checking
# Args: ref to a row returned from ReadManifest,ref to enabled services,frequency we are checking,file direction,product we are checking,filter_lines_ref
# Rets: 1 if the line applies, 0 if not
#########################
sub _CSVLineApplies {
	my ($line_ref,$enabled_module_ref,$freq,$file_direction,$validate_product,$filter_lines_ref)=@_;
	
	#Dump the CSV line for debugging because with 20 columns it is hard to eyeball it
	# for(my $i=0;$i<@$line_ref;$i++) {
		# &::LogMsg("$i: ".$line_ref->[$i]);
	# }
	
	#If we were given a list of trigger names, we filter by that not by product+services+freq
	if (ref($filter_lines_ref) && scalar(@$filter_lines_ref)!=0) {
		my $enabled=0;
		my @all_triggers=split(/\s+/,$line_ref->[$::TRIGGER_NAME_COL]);
		foreach my $trigger_name (@$filter_lines_ref) {
			if (scalar(grep(/^$trigger_name$/,@all_triggers))!=0) {
				$enabled=1;
				last;
			}
		}
		
		return 0 unless $enabled==1;
	} else {
		#Check that the line is applicable to our product
		if ($validate_product ne '') {
			my $product=$line_ref->[$::PRODUCT_COL];
			$product=~tr/a-z/A-Z/;
			if ($product ne $validate_product) {
				return 0;
			}
		}
		
		#Check that the line is applicable for one of our modules
		my $enabled=0;	
		foreach my $module (@$enabled_module_ref) {
			my $module_flag=$line_ref->[$::MODULE_TO_COL{$module}];
			if (&ValidateYNFlag($module_flag,0)==1) {
				$enabled=1;
				last;
			}
		}
		
		return 0 unless $enabled==1;
		
		#Make sure the line is enabled for our frequency
		my $freq_flag=$line_ref->[$::FREQ_TO_COL{$freq}];
		if (&ValidateYNFlag($freq_flag,0)!=1) {
			return 0;
		}
	}
	
	#Make sure this line has an input file
	if ($line_ref->[0] eq '') {
		return 0;
	}
	
	#Make sure the direction is relevant
	my $this_direction=$line_ref->[$::FILE_DIRECTION_COL];
	if ($this_direction eq '') {
		&::LogMsg("WARNING: empty file direction found on line, skipping");
		&::LogMsg("  ".join(',',@$line_ref));
	}
	if ($this_direction!~/^$file_direction$/i) {
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  DecodeInputFileLine
# Desc: Convert a ref to list of fields in an input file config, to defined fields
# Args: ref to a row returned from ReadManifest
# Rets: '' if the row is not enabled for this service+frequency
#       otherwise:
#       expected file
#       destination dir
#       required_flag
#       expected file count
#########################
sub DecodeInputFileLine {
	my ($line_ref)=@_;

	#Enabled for our scenario, go ahead with extracting the data
	my $product=$line_ref->[$::PRODUCT_COL];
	my $expected_file=$line_ref->[$::FILE_COL];
	my $dest_dir=$line_ref->[$::DEST_DIR_COL];
	my $required_file_flg=$line_ref->[$::REQUIRED_COL];
	my $multiple_file_flg=$line_ref->[$::NUM_FILES_COL];
	
	#Now clean up the data
	
	#If they gave us backslashes, convert them to forward slashes
	$expected_file=~s/\\/\//g;
	#If there is a leading slash, remove it
	$expected_file=~s/^\///;
	
	$required_file_flg=&ValidateROFlag($required_file_flg,1);
	$multiple_file_flg=1 if ($multiple_file_flg eq '');
	
	return ($expected_file,$dest_dir,$required_file_flg,$multiple_file_flg);
}

sub ValidateYNFlag {
	my ($val,$default)=@_;
	
	if ($val=~/^(y|yes|true|on)$/i) {
		$val=1;
	} elsif ($val=~/^(n|no|false|off)$/i) {
		$val=0;
	} else {
		$val=$default;
	}
	
	return $val;
}

sub ValidateROFlag {
	my ($val,$default)=@_;
	if ($val=~/^(r|required|req)$/i) {
		$val=1;
	} elsif ($val=~/^(o|optional|opt)$/i) {
		$val=0;
	} else {
		$val=$default;
	}
	
	return $val;
}

########################
# Sub:  ReadInputCSV
# Desc: Read the input CSV file.
# Args: csv_file - The CSV file to read
#       enabled_modules_ref - Ref to a list of enabled modules
#       freq - Validation frequency we are interested in
#       file_direction - INBOUND or OUTBOUND, the types of lines we are worried about
#       product - Validation product we are interested in
#       filter_lines_ref - Ref to a list of trigger lines to filter by
# Ret:  1 or 0 on success, a ref to [[row],[row],[row]]
########################
sub ReadInputCSV {
	my ($csv_file,$enabled_modules_ref,$freq,$file_direction,$product,$filter_lines_ref)=@_;
	
	&::LogMsg("Reading expected files CSV");
	unless(open(CSV_FILE,"<$csv_file")) {
		&::LogMsg("Unable to open $csv_file: $!");
		return(0,undef);
	}
	
	#We skip the first line, which is always header
	my $first_line=<CSV_FILE>;

	my @expect_files=();
	while(<CSV_FILE>) {
		chomp;
		s/\r|\n//g;  #Handle windows carriage returns
		next if (/^\#/);
		next if (/^\s*$/);
		my @row=();
		foreach my $col (split(/\s*\,\s*/,$_,-1)) {
			push(@row,&::CleanString($col));
		}
		if (&_CSVLineApplies(\@row,$enabled_modules_ref,$freq,$file_direction,$product,$filter_lines_ref)==1) {
			push(@expect_files,\@row);
		}
	}
	close(CSV_FILE);
	
	return (1,\@expect_files);
}

########################
# Sub:  CreateArchiveDir
# Desc: Create a date-stamped folder in a base archive directory
# Args: base archive dir,ref to the scalar that receives the calculated archive directory,prefix for the archive directory
# Ret:  1 on success, 0 on failure
########################
sub CreateArchiveDir {
	my ($base_archive_dir,$archive_dir_sref,$archive_prefix)=@_;
	
	if ($base_archive_dir eq '') {
		#&::LogMsg("WARNING: No archive directory specified, not saving uploaded files!");
		$$archive_dir_sref='';
		return 1;
	}
	
	my $arch_dir="${archive_prefix}-".&::GetTimeStamp('DATE-TIME');
	
	$$archive_dir_sref=&::ConcatPathString($base_archive_dir,$arch_dir);
	
	&::LogMsg("Creating archive directory $$archive_dir_sref");
	if (&::CreateDirs('0750',$$archive_dir_sref)!=1) {
		&::LogMsg("Unable to create directory $$archive_dir_sref");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  ValidateConfig
# Desc: parse our command-line arguments and validate our configuration
#########################
sub ValidateConfig {
	my $bfv_mode='';
	my $bfv_filter_file='';
	while(my $arg=shift(@ARGV)) {
		$arg=~tr/a-z/A-Z/;
		if ($arg eq 'WAIT' || $arg eq 'VALIDATE' || $arg eq 'EXPORT' || $arg eq 'STAGE') {
			$::BFV_MODE=$arg;
		} elsif ($arg eq '-F') {
			my $file=shift(@ARGV);
			if ($file eq '') {
				&::LogMsg("-F requires an argument");
				&Usage();
			}
			push(@{$::BFV_WAIT_LIST},$file);
		} elsif ($arg eq '-SERVICE') {
			my $service='';
			my $col='';
			&ParseHashLookupArg('-service',\%::MODULE_TO_COL,\$service,\$col);
			push(@{$::BFV_VALIDATE_SERVICES},$service);
		} elsif ($arg eq '-FREQ') {
			my $col='';
			&ParseHashLookupArg('-freq',\%::FREQ_TO_COL,\$::BFV_VALIDATE_FREQ,\$col);
		} elsif ($arg eq '-INPUTCSV') {
			&ParseSingleArg('-inputcsv',\$::BFV_FILE_INPUT_CONFIG);
		} elsif ($arg eq '-FILTERFILE') {
			&ParseSingleArg('-filterfile',\$bfv_filter_file);
		} elsif ($arg eq '-S') {
			&ParseSingleArg('-s',\$::BFV_INPUT_DIR);
		} elsif ($arg eq '-X') {
			&ParseSingleArg('-x',\$::BFV_EXPAND_DIR);
		} elsif ($arg eq '-D') {
			&ParseSingleArg('-d',\$::BFV_DEST_DIR);
		} elsif ($arg eq '-A') {
			&ParseSingleArg('-a',\$::BFV_ARCHIVE_DIR);
		} elsif ($arg eq '-C') {
			&ParseSingleArg('-c',\$::BFV_COLLECT_DIR);
		} elsif ($arg eq '-O') {
			&ParseSingleArg('-o',\$::BFV_OUTGOING_DIR);
		} elsif ($arg eq '-TIMEOUT') {
			&ParseSingleArg('-timeout',\$::BFV_WAIT_TIME);
		} elsif ($arg eq '-NOCOMPLETE') {
			$::BFV_WAIT_COMPLETE=0;
		} elsif ($arg eq '-OPTIONAL') {
			$::BFV_WAIT_REQUIRED=0;
		} elsif ($arg eq '-PRODUCT') {
			&ParseSingleArg('-product',\$::BFV_VALIDATE_PRODUCT,1);
		} else {
			&::LogMsg("Unknown argument: $arg");
			&Usage();
		}
	}
	
	if ($::BFV_MODE ne 'WAIT' && $::BFV_MODE ne 'VALIDATE' && $::BFV_MODE ne 'EXPORT' && $::BFV_MODE ne 'STAGE') {
		&::LogMsg("Unknown validation mode ($::BFV_MODE).  Specify wait, validate, stage or export");
		&::Usage();
	}
	
	my $restart_file="$::ORP_DIR/logs/orbatchfv_restart.state";
	$::ORP_GLOBAL_STATE=new ORPatchGlobalState($::ORP_DIR,$restart_file,$::ORP_CONFIG,$ENV{RETAIL_HOME},'RETAIL_HOME');
	
	unless ($::ORP_GLOBAL_STATE->ValidatePatchlessConfig()==1) {
		exit 1;
	}
	
	my $env_info_cfg="$::ORP_DIR/config/env_info.cfg";
	unless (-f $env_info_cfg) {
		&::LogMsg("Environment info file $env_info_cfg not found");
		exit 1;
	}
	
	if ($::ORP_CONFIG->LoadFile($env_info_cfg)!=1) {
		&::LogMsg("Unable to read env_info.cfg!");
		exit 1;
	}
	
	if ($::BFV_MODE eq 'WAIT') {
		#Wait
		&CheckRequiredDirArg('BATCH_EXPAND_DIR',\$::BFV_EXPAND_DIR,1);
		&CheckRequiredDirArg('BATCH_INPUT_DIR',\$::BFV_INPUT_DIR,0);
		&CheckRequiredDirArg('BATCH_ARCHIVE_DIR',\$::BFV_ARCHIVE_DIR,1);
		if (&::CheckRequiredBinary('unzip','')!=1) {
			return 0;
		}
		if (&::CheckRequiredBinary('gzip','')!=1) {
			return 0;
		}
		if (&::CheckRequiredBinary('tar','')!=1) {
			return 0;
		}
		if (scalar(@{$::BFV_WAIT_LIST})==0) {
			&::LogMsg("Empty wait file list, specify with -f");
			&Usage();
		}
		if ($::BFV_WAIT_TIME eq '') {
			$::BFV_WAIT_TIME=1800;
		} else {
			if ($::BFV_WAIT_TIME!~/^\d+$/ || $::BFV_WAIT_TIME==0) {
				&::LogMsg("Wait timeout must be a positive whole number");
				&Usage();
			}
		}
	} elsif ($::BFV_MODE eq 'STAGE') {
		#Stage
		&CheckRequiredDirArg('BATCH_EXPAND_DIR',\$::BFV_EXPAND_DIR,1);
		&CheckRequiredDirArg('BATCH_INPUT_DIR',\$::BFV_INPUT_DIR,0);
		#Stage does not archive files
		$::BFV_ARCHIVE_DIR='';
		if (&::CheckRequiredBinary('unzip','')!=1) {
			return 0;
		}
		if (&::CheckRequiredBinary('gzip','')!=1) {
			return 0;
		}
		if (&::CheckRequiredBinary('tar','')!=1) {
			return 0;
		}
		
		if (&::ValidateFilterCriteria($bfv_filter_file)!=1) {
			return 0;
		}
	} elsif ($::BFV_MODE eq 'VALIDATE') {
		#Validate
		&CheckRequiredDirArg('BATCH_EXPAND_DIR',\$::BFV_EXPAND_DIR,1);
		if ($::BFV_FILE_INPUT_CONFIG eq '') {
			$::BFV_FILE_INPUT_CONFIG=$::ORP_CONFIG->Get('BATCH_INPUT_CONFIG');
		}
		unless(-f $::BFV_FILE_INPUT_CONFIG) {
			&::LogMsg("Unable to find specified input CSV: $::BFV_FILE_INPUT_CONFIG");
			&Usage();
		}
		if (&::ValidateFilterCriteria($bfv_filter_file)!=1) {
			return 0;
		}

		#We don't necessarily NEED a BATCH_DEST_DIR, if every line has a specific dest dir, so we don't use CheckRequiredDirArg here
		if ($::BFV_DEST_DIR eq '') {
			my $dest_dir=$::ORP_CONFIG->Get('BATCH_DEST_DIR');
			if ($dest_dir ne '') {
				&::LogMsg("Default dest dir not specified, using $dest_dir");
				$::BFV_DEST_DIR=$dest_dir;
			}
		}
		$::BFV_DEST_DIR=&ExpandEnvVars($::BFV_DEST_DIR);
	} else {
		#Export
		&CheckRequiredDirArg('BATCH_COLLECT_DIR',\$::BFV_COLLECT_DIR,1);
		&CheckRequiredDirArg('BATCH_OUTGOING_DIR',\$::BFV_OUTGOING_DIR,1);
		&CheckRequiredDirArg('BATCH_ARCHIVE_DIR',\$::BFV_ARCHIVE_DIR,1);
		if (&::CheckRequiredBinary('zip','')!=1) {
			return 0;
		}
		if ($::BFV_FILE_INPUT_CONFIG eq '') {
			$::BFV_FILE_INPUT_CONFIG=$::ORP_CONFIG->Get('BATCH_INPUT_CONFIG');
		}
		
		if (&::ValidateFilterCriteria($bfv_filter_file)!=1) {
			return 0;
		}
		
		if (scalar(@{$::BFV_WAIT_LIST})==0) {
			&::LogMsg("Export filename missing, specify with -f");
			&Usage();
		}
		
		if (scalar(@{$::BFV_WAIT_LIST})>1) {
			&::LogMsg("Only one export filename can be specified with -f");
			&Usage();
		}
		
		$::BFV_ZIP_FILE=$::BFV_WAIT_LIST->[0];
	}

	return 1;
}

sub ValidateFilterCriteria {
	my ($filter_file)=@_;
	
	if ($filter_file ne '') {
		&::LogMsg("Reading filter file: $filter_file");
		unless(open(FILTER_FILE,"<$filter_file")) {
			&::LogMsg("Unable to open $filter_file: $!");
			&Usage();
		}
		
		my @lines=();
		while(<FILTER_FILE>) {
			chomp;
			s/\r|\n//g;  #Handle windows carriage returns
			next if (/^\#/);
			next if (/^\s*$/);
			s/^\s+//;
			s/\s+$//;
			push(@lines,&::CleanString($_));
		}
		$::BFV_FILTER_LINES=\@lines;
		
		close(FILTER_FILE);
	} else {
		if ($::BFV_VALIDATE_FREQ eq '') {
			&::LogMsg("Empty validation frequency, specify with -freq");
			&Usage();
		}
		
		if ($::BFV_MODE eq 'EXPORT') {
			if (scalar(@{$::BFV_VALIDATE_SERVICES})==0) {
				#If they didn't specify any services, default to checking all possible outbound files
				push(@{$::BFV_VALIDATE_SERVICES},keys(%::MODULE_TO_COL));
			}
			if ($::BFV_VALIDATE_PRODUCT eq '') {
				#This is OK for now, we will collect for all products
				&::LogMsg("Empty validation product, collecting files for all products");
			}
		} else {
			if ($::BFV_VALIDATE_PRODUCT eq '') {
				&::LogMsg("Empty validation product, specify with -product");
				&Usage();
			}

			if (scalar(@{$::BFV_VALIDATE_SERVICES})==0) {
				&::LogMsg("Empty list of enabled services, specify with -service");
				&Usage();
			}
		}
	}
	return 1;
}

sub ExpandEnvVars {
	my ($in_str)=@_;
	
	my $replace_str=$in_str;
	$replace_str=~s!\${?(\w+)}?! defined($ENV{$1}) ? $ENV{$1} : $& !ge;
	
	if ($in_str ne $replace_str) {
		&::LogMsg("Expanded string $in_str to $replace_str");
	}
	
	return $replace_str;
}

sub CheckRequiredDirArg {
	my ($var_name,$var_sref,$create)=@_;
	
	if ($$var_sref eq '') {
		$$var_sref=$::ORP_CONFIG->Get($var_name);
		if ($$var_sref eq '') {
			&::LogMsg("Required directory $var_name not defined in env_info.cfg nor on command-line");
			&Usage();
		}
	}
	
	if ($create==1 && !-d $$var_sref) {
		&::CreateDirs('0750',$$var_sref);
	}
	
	unless (-d $$var_sref) {
		&::LogMsg("Specified $var_name $$var_sref does not exist!");
		&Usage();
	}
	return 1;
}

sub ParseSingleArg {
	my ($arg_name,$dest_var_sref,$upper_case)=@_;
	
	my $value=shift(@ARGV);
	if ($value eq '') {
		&::LogMsg("$arg_name requires an argument");
		&Usage();
	}
	if ($upper_case==1) {
		$value=~tr/a-z/A-Z/;
	}
	
	$$dest_var_sref=$value;
}

sub ParseHashLookupArg {
	my ($arg_name,$hash_lookup,$dest_value,$dest_lookup_value)=@_;
	
	&ParseSingleArg($arg_name,$dest_value,1);
	
	$$dest_lookup_value=$hash_lookup->{$$dest_value};
	
	if ($$dest_lookup_value eq '') {
		&::LogMsg("Unknown argument to $arg_name: $$dest_value");
		&::LogMsg("Valid values: ".join(',',keys(%$hash_lookup)));
		&Usage();
	}
	
	return 1;
}

sub ExitFileValidate {
	my ($exit_status)=@_;
	
	#We always remove the restart file as we never load it
	$::ORP_GLOBAL_STATE->ResetRestartState();
	
	&::LogMsg("orbatchfv session complete");
	exit $exit_status;
}

sub Usage {
	&::LogMsg("Usage:");
	&::LogMsg("");
	&::LogMsg("Wait for a file to appear:");
	&::LogMsg("   orbatchfv wait -f <zip file> [-timeout <timeout in secs>] [-nocomplete] [-s <source dir> -x <expand dir> -a <archive dir>] [-optional]");
	&::LogMsg("Stage incoming files for validation:");
	&::LogMsg("   orbatchfv stage -inputcsv <file> -filterfile <file> [-s <source dir> -x <expand dir> -a <archive dir>]");
	&::LogMsg("Validate an expected set of files:");
	&::LogMsg("   orbatchfv validate -inputcsv <file> -service <service> -freq <frequency> -product <product> [-x <expand dir> -d <default dest dir>]");
	&::LogMsg("Collect all outbound files and create a zip from them:");
	&::LogMsg("   orbatchfv export -f <zip file> -inputcsv <file> -freq <frequency> [-c <collect dir> -o <outgoing dir> -a <archive dir>]");

	exit 1;
}
