############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPCustomHook;

#########################
# Sub:  RunCustomHook
# Desc: Checks if a custom hook exists for an action+phase and runs it
# Args: action - handle to an action object
#       phase - The name of the current phase
#       sequence - START if this is before the phase action is called, END if it is after
# Ret:  1 if successful, 0 if not
#########################
sub RunCustomHook {
	my $self=shift;
	my ($action,$phase_name,$sequence)=@_;
	
	my $action_name=$action->GetActionName();
	my $cfg_hook_name=$self->ConstructHookCfgName($action_name,$phase_name,$sequence);
	
	my $patch_hook_ref=$self->GetPatchInfoHook($cfg_hook_name);
	my $custom_hook=$self->GetCustomHook($cfg_hook_name);
	
	my @hooks=();
	if ($patch_hook_ref ne '') {
		push(@hooks,['patch info',$patch_hook_ref]);
	}
	if ($custom_hook ne '') {
		my @hook_list=split(/\,/,$custom_hook);
		push(@hooks,['custom',\@hook_list]);
	}
	
	#Run patch info hooks, then custom hooks
	my $clean_detail_dir=1;
	foreach my $ref (@hooks) {
		if ($self->RunHooks($action,$cfg_hook_name,$$ref[0],$$ref[1],$clean_detail_dir)!=1) {
			return 0;
		}
		$clean_detail_dir=0;
	}

	return 1;
}

#########################
# Sub:  ReadHookConfig
# Desc: Read the hook configuration file
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub ReadHookConfig {
	my $self=shift;
	my ($patch_info,$source_dir)=@_;
	
	my $cfg_file=$self->_GetHookConfigFile();
	
	if (-f $cfg_file) {
		if ($self->{'HOOK_CONFIG'}->LoadFile($cfg_file)!=1) {
			return 0;
		}
	}
	
	#Save hooks defined in the patch info
	foreach my $ref ($patch_info->GetPatchHooks()) {
		my ($hook_name,$hook_scripts)=@$ref;
		my @script_list=();
		foreach my $script (split(/\,/,$hook_scripts)) {
			if (!-f $script) {
				my $full_script=&::ConcatPathString($source_dir,$script);
				if (!-f $full_script) {
					&::LogMsg("Unable to find patch info hook script $script!");
					return 0;
				}
				$script=$full_script;
			}
			push(@script_list,$script);
			#Ensure the script is executable
			if (!-x $script) {
				chmod(0750,$script);
			}
		}
		$self->{'PATCH_HOOK_CONFIG'}->{$hook_name}=\@script_list;
	}
	
	return 1;
}



#####################################################

sub RunHooks {
	my $self=shift;
	my ($action,$cfg_hook_name,$hook_type,$hook_ref,$clean_detail)=@_;
	
	&::LogMsg("Running $hook_type hooks defined for $cfg_hook_name");
	$cfg_hook_name=~tr/A-Z/a-z/;
	&DetailLog::CleanDetailLogDir('hooks',$cfg_hook_name) if ($clean_detail==1);
	
	foreach my $script (@$hook_ref) {
		unless($action->CallCustomHook($script,$cfg_hook_name)==1) {
			return 0;
		}
	}
	return 1;
}

sub GetPatchInfoHook {
	my $self=shift;
	my ($cfg_key_name)=@_;
	
	return $self->{'PATCH_HOOK_CONFIG'}->{$cfg_key_name};
}

sub GetCustomHook {
	my $self=shift;
	my ($cfg_key_name)=@_;
	
	#Look for a key in our hook hash
	return $self->{'HOOK_CONFIG'}->Get($cfg_key_name);
}

sub ConstructHookCfgName {
	my $self=shift;
	my ($action_name,$phase_name,$before_after)=@_;
	
	$phase_name=~s/\-//;
	$phase_name=~tr/a-z/A-Z/;
	
	#Names look like:
	#DBSQL_RMS_PREACTION_START=/my/script/name/here
	#RMSBATCH_POSTACTION_END=/my/other/script/here
	return "${action_name}_${phase_name}_${before_after}";
}

sub _GetHookConfigFile {
	my $self=shift;
	return $self->{'CUSTOM_HOOK_CFG'};
}



#####################################################################
# Sub:  new
# Desc: Initialize the custom hook configuration
# Args: config_file - The name of the custom hook config file
#####################################################################
sub new {
    my $class=shift;
	my ($config_file)=@_;
    my $self={};
	
	$self->{'CUSTOM_HOOK_CFG'}=$config_file;
	$self->{'HOOK_CONFIG'}=new ConfigFile();
	$self->{'PATCH_HOOK_CONFIG'}={};
	
    bless($self,$class);	
    return $self;
}

1;
