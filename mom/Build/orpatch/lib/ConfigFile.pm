############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ConfigFile;

sub LoadFile {
	my $self=shift;
	my ($cfg_file)=@_;
	
	unless(open(CONFIG_FILE,"<$cfg_file") ) {
		&::LogMsg("Unable to open $cfg_file; $!");
		return 0;
	}
	
	my %config=();
	while(<CONFIG_FILE>) {
		chomp;
		next if (/^\#/);
		my ($name,$value)=split(/\s*\=\s*/);
		$name=~s/^\s*//;
		$value=~s/\s*$//;
		#Remove trailing comments
		$value=~s/\#.*$//;
		$config{$name}=$value;
	}
	close(CONFIG_FILE);
	
	$self->{'CFG'}=\%config;
	$self->_SetLoadFile($cfg_file);
	
	return 1;
}

sub LoadHash {
	my $self=shift;
	my ($hash_ref)=@_;
	
	$self->{'CFG'}=$hash_ref;
	$self->_SetLoadFile('');
}

sub Get {
	my $self=shift;
	my ($key)=@_;
	
	return $self->{'CFG'}->{$key};
}

sub GetWithDefault {
	my $self=shift;
	my ($key,$default)=@_;
	
	my $value=$self->{'CFG'}->{$key};
	
	if ($value eq '') {
		return $default;
	} else {
		return $value;
	}
}

sub Set {
	my $self=shift;
	my ($key,$value)=@_;
	
	$self->{'CFG'}->{$key}=$value;
	return $value;
}

sub Keys {
	my $self=shift;
	return keys(%{$self->{'CFG'}});
}

sub SetIfEmpty {
	my $self=shift;
	my ($key,$value)=@_;
	
	my $cur_val=$self->Get($key);
	
	if ($cur_val eq '') {
		$self->Set($key,$value);
		return $value;
	}
	return $cur_val;
}

sub ValidateTFVar {
	my $self=shift;
	my ($name,$default)=@_;
	
	my $val=$self->Get($name);
	my $new_val=$val;
	
	if ($val eq '') {
		$new_val=$default;
	} else {
		if ($val=~/^(TRUE|ON|YES|Y|1)$/i) {
			$new_val=1;
		} elsif ($val=~/^(FALSE|OFF|NO|N|0)$/i) {
			$new_val=0;
		} else {
			$new_val=$default;
		}
	}
	
	if ($new_val ne $val) {
		$self->Set($name,$new_val);
	}
	
	return $new_val;
}

sub ValidateStringVar {
	my $self=shift;
	my ($name,@valid_values)=@_;
	
	my $default=$valid_values[0];
	
	my $val=$self->Get($name);
	my $new_val=$val;
	
	if ($val eq '') {
		$new_val=$default;
	} else {
		my $found=0;
		foreach my $check_val (@valid_values) {
			if ($val=~/^$check_val$/i) {
				$new_val=$check_val;
				$found=1;
				last;
			}
		}
		if ($found==0) {
			$new_val=$default;
		}
	}
	
	if ($new_val ne $val) {
		$self->Set($name,$new_val);
	}
	
	return $new_val;
}

sub GetLoadFile {
	my $self=shift;
	return $self->{'LOAD_FILE'};
}

sub _SetLoadFile {
	my $self=shift;
	my ($cfg_file)=@_;
	$self->{'LOAD_FILE'}=$cfg_file;
	return $self->{'LOAD_FILE'};
}

#####################################################################
# Sub:  new
# Desc: Create a new ConfigFile object
#####################################################################
sub new {
    my $class=shift;
    my $self={};
	
	bless($self,$class);	
	
	$self->{'CFG'}={};
	$self->_SetLoadFile('');
	
    return $self;
}

1;
