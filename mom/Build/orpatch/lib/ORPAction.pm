############################################################################
# Copyright © 2013,2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAction;

#########################
# Sub:  GetActionName
# Desc: Get the name of this action
# Args: None
# Ret:  The action name
#########################
sub GetActionName {
	my ($self)=@_;
	return $self->{'ACTION_NAME'};
}

#########################
# Sub:  CheckPatchForAction
# Desc: Check if this patch includes any files of the type that this action involves
#       Optionally checks not just files that are relevant, but whether any files are newer
# Args: version_check   - 1 if file versions should be checked, 0 if not, 2 if newer files should be reported
# Ret:  $proceed - 1 if the function was successful, 0 if not
#       @files
#           if version_check is 1, a list of [file name,new revision,destination file,replaced dest manifest ref,source patch name]
#           if version_check is 0, the list of source manifest entries that are relevant
#########################
sub CheckPatchForAction {
	my $self=shift;
	my ($version_check)=@_;

	#Get the configuration for our patch
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my $patch_name=$patch_info->GetPatchName();
	
	my $source_manifest=$global_state->GetSourceManifest();
	my $dest_dir=$global_state->GetDestDir();
	
	my ($src_info_ref,$type_lookup_href,$distinct_types)=$global_state->GetSourceManifestInfo();

	#Look for files in the source manifest that appear interesting
	my @interest=();
	foreach my $fcp_ref ($self->GetFileCopyParams()) {
		my ($ftype_ref,$dest_sub_dir)=$fcp_ref->GetBasicParams();
		my $ignore_pattern_ref=$fcp_ref->GetIgnorePatterns();
		
		my $this_interest_ref=&Manifest::FilterSourceManifest($type_lookup_href,$ftype_ref,$ignore_pattern_ref);
		my $total=scalar(@$this_interest_ref);

		#Not included, nothing further to do
		next if ($total==0);
		
		if ($version_check>0) {
			#If they want us to check versions, read the destination manifest
			my ($full_dest,$dest_manifest)=$self->GetDestDirAndManifest($dest_dir,$dest_sub_dir);
			my @dest_info=&Manifest::ReadManifest($dest_manifest);
			
			#Restructure dest_info as a hash for quicker lookups by PatchFileIsNewer
			my %dest_info_hash=();
			foreach my $ref (@dest_info) {
				my ($dfile,$sfile,@discard)=&Manifest::DecodeDestLine($ref);
				$dest_info_hash{$sfile}=$ref;
			}
			
			#Now compare each file with the destination file and determine if the patch version is newer
			foreach my $ref (@$this_interest_ref) {   
				my ($fname,$frevision,$ftype,$fpatch_name,@discard)=&Manifest::DecodeSourceLine($ref);
				
				#Check if this file is newer than the destination file
				my ($replace_mode,$replaced_diref)=&PatchFileCopy::PatchFileIsNewer($fname,$frevision,\%dest_info_hash,$full_dest,$fcp_ref,$ftype,$patch_info);
				if ($replace_mode>0 || ($version_check==2 && $replace_mode==-1)) {
					#Note that we don't pass the src_dir in, so real_src_path returned here is not valid
					my ($real_src_path,$real_dst_path,$dfname)=$fcp_ref->GenFileNames($fname,$ftype,'',$full_dest);

					if ($fpatch_name eq '') {
						$fpatch_name=$patch_name;
					}
					push(@interest,[$fname,$frevision,$real_dst_path,$replaced_diref,$fpatch_name]);
				}
			}
		} else {
			#No version check, include all files
			push(@interest,@$this_interest_ref);
		}
	}
	
	#Return success, and the files we are interested in
	return (1,@interest);
}

#########################
# Sub:  CreateAnalyzeDetailFiles
# Desc: Create any extra detail files that could be used during patch analysis.  By default actions do not have any extra detail files.
# Args: extra_detail_type - The type of extra details to create
#       extra_detail_action_ref - Ref to a list that this action name should be added to if there are extra detail files
#       current_metadata_map - Handle to the metadata map object that will be used for registering the current detail
#       new_metadata_map - Handle to the metadata map object that will be used for registering the new detail
# Ret:  1 on success, 0 on failure
#########################
sub CreateAnalyzeDetailFiles {
	my $self=shift;
	my ($extra_detail_type,$extra_detail_action_ref,$current_metadata_map,$new_metadata_map)=@_;
	
	return 1;
}

#########################
# Sub:  PrePatchCheck
# Desc: This is called before any actions are taken.
#       It allows an action to double-check important config, particularly database connections
#########################
sub PrePatchCheck {
	my $self=shift;
	my ($full_check)=@_;
	
	#The default pre-patch check does nothing
	return 1;
}

#########################
# Sub:  PreAction
# Desc: This is called before copying files for a patch
#########################
sub PreAction {
	my $self=shift;
	
	#The default pre-action does nothing
	return 1;
}

#########################
# Sub:  CopyPatchFiles
# Desc: This is the function to copy all of the necessary files for a patch to their destination
#       The default function loops through each file copy param and calls PatchFileCopy::DoFileCopy for each
#########################
sub CopyPatchFiles {
	my $self=shift;

	#Get the configuration for our patch
	my $global_state=$self->GetORPatchGlobalState();
	my $patch_info=$global_state->GetPatchInfo();
	
	my $source_dir=$global_state->GetSourceDir();
	my $source_manifest=$global_state->GetSourceManifest();
	my $dest_dir=$global_state->GetDestDir();
	my $patch_name=$patch_info->GetPatchName();
	
	my $restore_point=$global_state->GetRestorePoint();
	
	my ($src_info_ref,$type_lookup_href,$distinct_types)=$global_state->GetSourceManifestInfo();
	
	my $ret=1;
	my $total_files=0;
	my $files_copied=0;
	foreach my $fcp_ref ($self->GetFileCopyParams()) {
		my ($ftype_ref,$dest_sub_dir)=$fcp_ref->GetBasicParams();
		my ($full_dest,$dest_manifest)=$self->GetDestDirAndManifest($dest_dir,$dest_sub_dir);
		
		&::LogMsg("Initiating file copy for ".join(',',@$ftype_ref)." files to $full_dest");
		
		if ($fcp_ref->PrepareFileCopyMethods()!=1) {
			return 0;
		}

		($ret,$files_copied)=&PatchFileCopy::DoFileCopy($patch_name,$source_dir,$src_info_ref,$type_lookup_href,$full_dest,$dest_manifest,$restore_point,$fcp_ref,$patch_info);
		
		$total_files+=$files_copied;
		
		if ($ret==1) {
			#Successful copy
			&::LogMsg("Completed copying files for ".join(',',@$ftype_ref)." files");
		} else {
			&::LogMsg("Failed to copy ".join(',',@$ftype_ref)." files!");
			last;
		}
	}
	
	if ($ret==1 && $total_files!=0) {
		$global_state->SetPatchMadeChangesFlag();
	}
	
	return $ret;
}

#########################
# Sub:  PatchAction
# Desc: This is called after the files are copied, it should do the bulk of the work for an action
#########################
sub PatchAction {
	my $self=shift;
	
	#The default action does nothing after copying files
	return 1;
}

#########################
# Sub:  PostAction
# Desc: This is called after the PatchAction is completed
#########################
sub PostAction {
	my $self=shift;
	#The default post action does nothing
	return 1;
}

#########################
# Sub:  CleanupAction
# Desc: This is called after the PostAction
#########################
sub CleanupAction {
	my $self=shift;
	#The default clean-up action does nothing
	return 1;
}

#########################
# Sub:  PreFileDelete
# Desc: This is called just prior to a file that we are responsible for being deleted
# Args: delete_file - The full path to the file that is going to be deleted
#       dm_info_ref - The destination manifest row related to this file
# Ret:  1 on success, 0 on failure
# Note: returning 0 stops removing the file, but it will already have been removed from the dest manifest
#########################
sub PreFileDelete {
	my $self=shift;
	my ($delete_file,$dm_info_ref)=@_;
	return 1;
}

#########################
# Sub:  CallCustomHook
# Desc: Execute a custom hook script
# Arg:  custom script to execute
#       hook_name - the hook name we are running under
# Ret:  1 if the script exited with 0 status, 0 if not
#########################
sub CallCustomHook {
	my $self=shift;
	my ($custom_script,$hook_name)=@_;
	
	unless (-x $custom_script) {
		&::LogMsg("Custom hook script $custom_script is not executable!");
		return 0;
	}
	
	#Run the script
	my $command="$custom_script 2>&1";
	&::LogMsg("Executing $custom_script");
	my @output=`$command`;
	
	my $rc=($?>>8);
	
	&DetailLog::CreateLog('hooks',$hook_name,$custom_script,\@output,($rc!=0) ? 'err' : 'log');

	if ($rc!=0) {
		&::LogMsg("Custom script $custom_script exited with non-zero status $rc");
		&::DebugOutput('custom hook',@output);
		return 0;
	}
	&::LogMsg("Script $custom_script completed with status 0");
	
	return 1;
}

#########################
# Sub:  GetFileCopyParams
# Desc: Get the full list of file copy params
#########################	
sub GetFileCopyParams {
	my ($self)=@_;
	return @{$self->{'FILE_COPY_PARAMS'}};
}
	
#########################
# Sub:  GetDestDirAndManifest
# Desc: Take a destination dir and a sub dir and return the location of the manifest and manifest name
#########################
sub GetDestDirAndManifest {
	my $self=shift;
	my ($dest_dir,$dest_sub_dir)=@_;
	
	my $full_dest=&::ConcatPathString($dest_dir,$dest_sub_dir);
	my $dest_manifest=&::ConcatPathString($full_dest,'env_manifest.csv');
	
	return ($full_dest,$dest_manifest);
}

#########################
# Sub:  GetDeletedManifestName
# Desc: Take a destination manifest name and return the deleted manifest name
#########################
sub GetDeletedManifestName {
	my $self=shift;
	my ($dst_manifest)=@_;
	
	my ($dir,$msft)=&::GetFileParts($dst_manifest,2);
	my $deleted_manifest=&::ConcatPathString($dir,"deleted_${msft}");
	
	return $deleted_manifest;
}

#########################
# Sub:  FindFileCopyParamsIndex
# Desc: Look up a specific destination subdirectory in a file copy params array
#########################
sub FindFileCopyParamsIndex {
	my $self=shift;
	my ($restart_dest_subdir,$fcp_ref)=@_;
	
	my $fcp_index=0;
	for(;$fcp_index<scalar(@$fcp_ref);$fcp_index++) {
		my $ref=$$fcp_ref[$fcp_index];
		my $dest_subdir=$ref->GetDestSubDir();
		
		#If this dest subdir matches our desired subdir, return this index
		if ($dest_subdir eq $restart_dest_subdir) {
			return $fcp_index;
		}
	}
	#Couldn't find it, start from beginning
	return 0;
}

#########################
# Sub:  FinalizeConfiguration
# Desc: Provides a hook to finalize any configuration after all other actions have been instantiated but
#       before the action list is filtered.  File types can safely be added/removed at this point.
# Arg:  None
# Ret:  1 on successful configuration, 0 on failure
#########################
sub FinalizeConfiguration {
	my $self=shift;

	return 1;
}

#########################
# Sub:  FinalizeEnabledConfiguration
# Desc: Provides a hook to finalize any configuration after the action list has been filtered by the patch.
# Arg:  None
# Ret:  1 on successful configuration, 0 on failure
#########################
sub FinalizeEnabledConfiguration {
	my $self=shift;

	return 1;
}

#########################
# Sub:  SetORPatchGlobalState
# Desc: Set the global state pointer for our ORPatch
# Arg:  orp_global_state - The global state object handle
#########################
sub SetORPatchGlobalState {
	my $self=shift;
	my ($orp_global_state)=@_;
	$self->{'ORP_GLOBAL_STATE'}=$orp_global_state;
}

#########################
# Sub:  GetORPatchGlobalState
# Desc: Get the global state pointer for our ORPatch
# Arg:  None
# Ret:  The handle to our global state
#########################
sub GetORPatchGlobalState {
	my $self=shift;
	return $self->{'ORP_GLOBAL_STATE'};
}

#########################
# Sub:  ResolveConfigPath
# Desc: Resolve a path specified in a configuration file which may be relative to $RETAIL_HOME/orpatch or fully-qualified
# Arg:  config value
# Ret:  A fully-qualified path, or '' if the config value was null
#########################
sub ResolveConfigPath {
	my $self=shift;
	my ($config_val)=@_;
	
	if ($config_val=~/^\//) {
		#Path starts with /, likely a fully-qualified path
		return $config_val;
	}
		
	return &::ConcatPathString($ENV{RETAIL_HOME},'orpatch',$config_val);
}

#########################
# Sub:  DisableAction
# Desc: Tell this action that it is disabled
# Arg:  None
# Ret:  1
#########################
sub DisableAction {
	my $self=shift;
	$self->{'ACTION_ENABLED'}=0;
	return 1;
}

#########################
# Sub:  EnableAction
# Desc: Tell this action that it is enabled
# Arg:  None
# Ret:  1
#########################
sub EnableAction {
	my $self=shift;
	$self->{'ACTION_ENABLED'}=1;
	return 1;
}

#########################
# Sub:  IsActionEnabled
# Desc: Check if an action is enabled
# Arg:  None
# Ret:  1 if the action is enabled, undef if not
#########################
sub IsActionEnabled {
	my $self=shift;
	return ($self->{'ACTION_ENABLED'}==1);
}

#########################
# Sub:  SetValidOnAnyHost
# Desc: Set the flag that says this action is valid on any host
# Arg:  None
# Ret:  1
#########################
sub SetValidOnAnyHost {
	my $self=shift;
	$self->{'VALID_ANY_HOST'}=1;
}

#########################
# Sub:  IsValidOnAnyHost
# Desc: Check if an action is valid on any host
# Arg:  None
# Ret:  1 if the action is always valid, undef if not
#########################
sub IsValidOnAnyHost {
	my $self=shift;
	return ($self->{'VALID_ANY_HOST'}==1);
}

#########################
# Sub:  IsValidOnThisHost
# Desc: Check if an action is valid on this host
# Arg:  None
# Ret:  1 if the action is valid on this host, 0 if not
#########################
sub IsValidOnThisHost {
	my $self=shift;
	
	my $do_this_action=0;
	if ($self->IsValidOnAnyHost()) {
		#This action is always valid
		$do_this_action=1;
	} else {
		#Check if this action is enabled on this host or not
		my $name=$self->GetActionName();
		my $val=$self->{'ORP_CONFIG'}->Get("ACTION_${name}");
		if ($val ne '') {
			if ($val=~/^y$/i) {
				$do_this_action=1;
			} elsif ($val=~/^n$/i) {
				$do_this_action=0;
			} else {
				#we also allow a comma-separated list of valid hostnames
				#To allow env_info.cfg to be on shared storage
				my $host_cmd=`hostname`;
				chomp($host_cmd);
				my ($this_host,$domain)=split(/\./,$host_cmd,2);
				foreach my $host_value (split(/\,/,$val)) {
					my ($match_host,$mdomain)=split(/\./,$host_value,2);
					if ($this_host=~/^$match_host$/i) {
						$do_this_action=1;
						last;
					}
				}
			}
		}
	}
	
	return $do_this_action;
}

#########################
# Sub:  GetEnvManifestList
# Desc: Get a list of environment manifest files and their full destination path
# Arg:  None
# Ret:  ref to list of [full_dest,env manifest file name] for all env manifest files that exist
#########################
sub GetEnvManifestList {
	my $self=shift;
	my ($dest_dir)=@_;

	my @manifest_list=();
	
	return \@manifest_list unless ($self->IsValidOnThisHost()==1);
	
	my @fcp_list=$self->GetFileCopyParams();
		
	return \@manifest_list unless(scalar(@fcp_list)>0);
		
	foreach my $fcp (@fcp_list) {
		my ($ft_ref,$dest_subdir)=$fcp->GetBasicParams();
		
		my ($full_dest,$dest_manifest)=$self->GetDestDirAndManifest($dest_dir,$dest_subdir);
		
		if (-f $dest_manifest) {			
			push(@manifest_list,[$full_dest,$dest_manifest]);
		}
	}
	
	return \@manifest_list;
}

#########################
# Sub:  GetDeleteManifestList
# Desc: Get a list of deleted environment manifest files and their full destination path
# Arg:  None
# Ret:  ref to list of [full_dest,delete manifest file name] for all delete manifest files that exist
#########################
sub GetDeleteManifestList {
	my $self=shift;
	my ($dest_dir)=@_;
		
	my $m_list_ref=$self->GetEnvManifestList($dest_dir);
	
	my @del_manifest_list=();
	foreach my $ref (@$m_list_ref) {
		my ($full_dest,$env_manifest)=@$ref;
		
		my $delete_manifest=$self->GetDeletedManifestName($env_manifest);
		
		if (-f $delete_manifest) {
			push(@del_manifest_list,[$full_dest,$delete_manifest]);
		}
	}
	return \@del_manifest_list;
}

#########################
# Sub:  GetActionConfigList
# Desc: Get a list of config files for this app
# Args: manifest_list_ref - ref to the list onto which we add our exported manifests
# Ret:  1 if successf, 0 if not
#########################
sub GetActionConfigList {
	my $self=shift;
	my ($manifest_list_ref)=@_;
	
	#Nothing by default
	
	return 1;
}

#########################
# Sub:  ExportGeneratedConfig
# Desc: Export any configuration that does not exist as a file
# Args: gen_dir - path to the dir to place generated files (must be created before use)
#       metadata_map - handle to the ORPMetadataMap object to register fies with
#       manifest_list_ref - ref to the list to add exported files to
#       support_dir - ref to the base directory for exports
# Ret:  1 if successf, 0 if not
#########################
sub ExportGeneratedConfig {
	my $self=shift;
	my ($gen_dir,$metadata_map,$manifest_list_ref,$support_dir)=@_;
	
	#Nothing by default
	
	return 1;
}

#########################
# Sub:  SetupMetadataEnvironment
# Desc: Setup any necessary settings to support exporting/comparing metadata
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub SetupMetadataEnvironment {
	my $self=shift;

	return 1;
}

#########################
# Sub:  CheckRequiredBinaryOnce
# Desc: Check for a required binary that might already have been checked by some other action
# Arg:  binary
# Ret:  1 if the binary is in the path, 0 if not
#########################
sub CheckRequiredBinaryOnce {
	my $self=shift;
	my ($binary)=@_;
	
	my $check_binary=$binary;
	$check_binary=~tr/a-z/A-Z/;
	
	my $check_name="CHECK_REQUIRED_${check_binary}";
	
	my $bin_check=$self->GetORPatchGlobalState()->GetCrossActionFlag($check_name);
	unless($bin_check==1) {
		if (&::CheckRequiredBinary($binary)!=1) {
			return 0;
		}
		$self->GetORPatchGlobalState()->SetCrossActionFlag($check_name,1);
	}
	
	return 1;
}

#########################
# Sub:  SetFileCopyParams
# Desc: Set the full list of file copy params
#########################	
sub SetFileCopyParams {
	my $self=shift;
	my ($fcp_ref)=@_;
	$self->{'FILE_COPY_PARAMS'}=$fcp_ref;
}

sub GetConfig {
	my $self=shift;
	return $self->{'ORP_CONFIG'};
}

#########################
# Sub:  SetTFFlagFromConfigVar
# Desc: Set a flag to either 1 or 0 depending on a action-specific configuration variable or a base-class configuration variable
# Arg:  config_ref - Handle to the ConfigFile object
#       variable_base - Base portion of the variable name in the config file, and the name of the flag to set on self
#       base_class_name - Name of the prefix used for 'global' variable names
#       default_value - 1 or 0, depending on what the default value should be
#       action_name - Optional action name to use, if not set the result of GetActionName will be used
# Example:  variable_base='REVERT_HOTFIXES', base_class_name = 'JAVAAPP'
#           $self->{'REVERT_HOTFIXES'} will be set to 1 or 0 depending on the logic below
#             If ${action_name}_REVERT_ALL_HOTFIXES is set, this is used
#             else if JAVAAPP_REVERT_ALL_HOTFIXES is set, this is used
#             else default value is used
# Ret:  1 or 0 depending on the flag setting
#########################
sub SetTFFlagFromConfigVar {
	my $self=shift;
	my ($config_ref,$variable_base,$base_class_name,$default_value,$action_name)=@_;
	
	my $name=$action_name;
	if ($name eq '') {
		$name=$self->GetActionName();
	}
	my $specific_var_name="${name}_${variable_base}";
	my $global_var_name="${base_class_name}_${variable_base}";
	
	my $flag_setting=$default_value;
	
	my $specific_fs=$config_ref->Get($specific_var_name);
	if ($specific_fs ne '') {
		#They set the parameter for this action specifically
		$flag_setting=$config_ref->ValidateTFVar($specific_var_name,$default_value);
	} else {
		#Specific var was not set, use the generic one
		$flag_setting=$config_ref->ValidateTFVar($global_var_name,$default_value);
	}
	
	$self->{$variable_base}=$flag_setting;
	
	return $flag_setting;
}

#####################################################################
# Sub:  new
# Desc: Initialize the action
# Args: Name - The name this action should be referenced to in config files
#       fcp_ref - A ref to an array of FileCopyParam objects
#       config_ref - A ref to a ConfigFile object
#####################################################################
sub new {
    my $class=shift;
	my ($name,$fcp_ref,$config_ref)=@_;
    my $self={};
	
	$self->{'ACTION_NAME'}=$name;
	$self->{'ORP_CONFIG'}=$config_ref;
	$self->{'ORP_GLOBAL_STATE'}='';
	$self->{'VALID_ANY_HOST'}=0;
	
    bless($self,$class);
	$self->EnableAction();
	$self->SetFileCopyParams($fcp_ref);
	
    return $self;
}

1;
