############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPatchInventory;

sub GetInventoryFile {
	my $self=shift;
	return $self->{'INVENTORY_FILE'};
}

sub SetInventoryFile {
	my $self=shift;
	my ($inventory_file)=@_;
	$self->{'INVENTORY_FILE'}=$inventory_file;
}

sub DecodePatchType {
	my $self=shift;
	my ($patch_type)=@_;
	$patch_type=$self if ($patch_type eq '');
	return ($patch_type eq &GetCummulativePatchType()) ? 'Cummulative' : 'Incremental';
}

sub IsPatchCummulative {
	my $self=shift;
	my ($patch_type)=@_;
	$patch_type=$self if ($patch_type eq '');
	return ($patch_type eq &GetCummulativePatchType());
}

sub IsPatchIncremental {
	my $self=shift;
	my ($patch_type)=@_;
	$patch_type=$self if ($patch_type eq '');
	return ($patch_type eq &GetIncrementalPatchType());
}

sub GetCummulativePatchType {
	return 'C';
}

sub GetIncrementalPatchType {
	return 'I';
}

#########################
# Sub:  PrintInventory
# Desc: print out the patch inventory file
#       Note, the inventory file must exist for this function to be successful
# Args: include_subpatch - 1 (default) to include sub-patch information, 0 to not
# Ret:  1 if successful, 0 if not
#########################
sub PrintInventory {
	my $self=shift;
	my ($include_subpatch,$include_obsolete)=@_;
	$include_subpatch=1 if $include_subpatch eq '';
	$include_obsolete=0 if $include_obsolete eq '';

	my ($top_patch_ref,$sub_patch_ref)=$self->ReadInventoryFile();
	if ($top_patch_ref eq '') {
		return 0;
	}
	
	&::LogMsg("Current patch inventory:");
	foreach my $ref (@$top_patch_ref) {
		my ($top_patch,$applied_date,$patch_type,$products,$obsolete_date)=$self->DecodeTopPatchInfo($ref);
		
		#If this patch is obsolete, don't include it unless we were asked to
		if ($obsolete_date ne '') {
			next unless ($include_obsolete==1);
		}
		
		$::LOG->LogMsgNoDate("Patch: $top_patch");
		$::LOG->LogMsgNoDate("   Applied:    $applied_date");
		$::LOG->LogMsgNoDate("   Products:   $products");
		$::LOG->LogMsgNoDate("   Patch Type: ".$self->DecodePatchType($patch_type));
		if ($include_obsolete==1 && $obsolete_date ne '') {
			$::LOG->LogMsgNoDate("   Obsoleted:  $obsolete_date");
		}
		
		if ($include_subpatch==1) {
			my $sub_ref=$sub_patch_ref->{$top_patch};
			if ($sub_ref eq '') {
				#$::LOG->LogMsgNoDate("   No Subpatches");
			} else {
				foreach my $subpatch (@$sub_ref) {
					$::LOG->LogMsgNoDate(sprintf("   Sub-Patch:  %s",$subpatch));
				}
			}
		}
		$::LOG->LogMsgNoDate("");
	}
	
	return 1;
}

#########################
# Sub:  RegisterPatch
# Desc: Update the inventory with a new patch
# Args: top_patch - the name of the top-level patch,
#       patch_type - the patch type,
#       products  - string representing the products this patch applies to
#       spatch_ref - ref to the array of sub-patches
#       used_patch_ref - ref to hash of active patches in environment
# Ret:  1 if successful, 0 if not
#########################
sub RegisterPatch {
	my $self=shift;
	my ($top_patch,$patch_type,$products,$spatch_ref,$used_patch_ref)=@_;
	
	#if the inventory file does not exist, initialize it
	unless ($self->InitializeInventoryFile()==1) {
		return 0;
	}
	
	my ($top_patch_ref,$sub_patch_ref)=$self->ReadInventoryFile();
	if ($top_patch_ref eq '') {
		return 0;
	}
	
	&::LogMsg("Registering patch $top_patch in inventory");

	my $applied_date=&::GetTimeStamp('DATE_TIME');
	
	#patch_name,applied_date,patch_type,product_list,obsolete_date
	my @patch_info=$self->AssembleTopPatchInfo($top_patch,$applied_date,$patch_type,$products,'');

	my $patch_index=-1;
	for(my $i=0;$i<scalar(@$top_patch_ref);$i++) {
		my $p_ref=$top_patch_ref->[$i];
		my ($patch_name,@discard)=$self->DecodeTopPatchInfo($p_ref);
		if ($top_patch eq $patch_name) {
			#Path that was already applied, update the info
			&::LogMsg("Patch $top_patch already exists in inventory, updating");
			$patch_index=$i;
			last;
		}
	}
	if ($patch_index>=0) {
		$top_patch_ref->[$patch_index]=\@patch_info;
	} else {
		push(@$top_patch_ref,\@patch_info);
	}
	
	#Now handle sub-patches
	if (scalar(@$spatch_ref)!=0) {
		$sub_patch_ref->{$top_patch}=$spatch_ref;
	}
	
	if ($self->_MarkObsoletePatches($top_patch_ref,$sub_patch_ref,$used_patch_ref,$applied_date)!=1) {
		return 0;
	}

	return $self->_WriteInventoryFile($top_patch_ref,$sub_patch_ref);
}

#########################
# Sub:  _MarkObsoletePatches
# Desc: Mark patches that are no longer in use as obsoleted
# Args: top patch ref - ref to the top patch info array
#       sub_patch_ref - ref to the sub patch info array
#       used_patch_ref - ref to the hash of used patches
#       obsolete date - date that should be used for patches newly obsoleted
# Ret:  1 on success, 0 on failure
#########################
sub _MarkObsoletePatches {
	my $self=shift;
	my ($top_patch_ref,$sub_patch_ref,$used_patch_ref,$obsolete_date)=@_;
	
	#Make a hash of all non-obsolete top-level patch names and a hash of patch names to index in the top patch array
	my %all_top_patches=();
	my %all_top_patch_indexes=();
	for(my $i=0;$i<scalar(@$top_patch_ref);$i++) {
		my ($patch_name,$applied_date,$patch_type,$products,$this_obsolete_date)=$self->DecodeTopPatchInfo($$top_patch_ref[$i]);
		#If the obsolete date isn't already defined for this patch, record it
		if ($this_obsolete_date eq '') {
			$all_top_patches{$patch_name}=0;
			$all_top_patch_indexes{$patch_name}=$i;
		}
	}
	
	#look through sub patches still active and mark their top-level patch as active
	foreach my $top_patch (keys(%$sub_patch_ref)) {
		foreach my $sub_patch (@{$sub_patch_ref->{$top_patch}}) {
			#If this sub-patch is still used, mark the top-level patch as still used
			if ($used_patch_ref->{$sub_patch} ne '' && $used_patch_ref->{$sub_patch}>0) {
				$all_top_patches{$top_patch}=1;
				last;
			}
		}
	}
	
	#Look through all top-level patches and mark the ones still in use
	foreach my $ref (@$top_patch_ref) {
		my ($top_patch,$applied_date,$patch_type,$products,$this_obsolete_date)=$self->DecodeTopPatchInfo($ref);
		if ($used_patch_ref->{$top_patch} ne '' && $used_patch_ref->{$top_patch}>0) {
			$all_top_patches{$top_patch}=1;
		}
	}
	
	#Now find top-level patches in all_top_patches that were not flagged as still in use
	foreach my $top_patch (keys(%all_top_patches)) {
		my $used=$all_top_patches{$top_patch};
		if ($used==0) {
			&::LogMsg("Marking $top_patch as obsolete");
			#Patch no longer in use, get it's index
			my $index=$all_top_patch_indexes{$top_patch};
			#Get the patch info
			my ($this_top_patch,$applied_date,$patch_type,$products,$this_obsolete_date)=$self->DecodeTopPatchInfo($$top_patch_ref[$index]);
			#Replace the obsolete date with the current date
			my @info=$self->AssembleTopPatchInfo($this_top_patch,$applied_date,$patch_type,$products,$obsolete_date);
			$$top_patch_ref[$index]=\@info;
		}
	}
	
	return 1;
}

#########################
# Sub:  RemovePatch
# Desc: Remove a patch from the inventory
# Args: top_patch - the name of the top-level patch,
# Ret:  1 if successful, 0 if not
#########################
sub RemovePatch {
	my $self=shift;
	my ($top_patch)=@_;
	
	#if the inventory file does not exist, initialize it
	unless ($self->InitializeInventoryFile()==1) {
		return 0;
	}
	
	my ($top_patch_ref,$sub_patch_ref)=$self->ReadInventoryFile();
	if ($top_patch_ref eq '') {
		return 0;
	}
	
	&::LogMsg("Removing $top_patch from inventory");

	my $obsolete_date=&::GetTimeStamp('DATE_TIME');

	my $patch_index=-1;
	for(my $i=0;$i<scalar(@$top_patch_ref);$i++) {
		my ($this_top_patch,$applied_date,$patch_type,$products,$this_obsolete_date)=$self->DecodeTopPatchInfo($top_patch_ref->[$i]);
		if ($top_patch eq $this_top_patch) {
			#Replace the obsolete date with the current date
			my @info=$self->AssembleTopPatchInfo($this_top_patch,$applied_date,$patch_type,$products,$obsolete_date);
			$top_patch_ref->[$i]=\@info;
			$patch_index=$i;
			last;
		}
	}
	
	if ($patch_index<0) {
		&::LogMsg("WARNING: Unable to find patch $top_patch in inventory!");
	}

	return $self->_WriteInventoryFile($top_patch_ref,$sub_patch_ref);
}

#########################
# Sub:  ReadInventoryFile
# Desc: read the ORPatch Inventory file
#       Note, the inventory file must exist for this function to be successful
# Args: None
# Ret:  ref to array of top patches, ref to hash of sub-patches keyed on top patch name
#########################
sub ReadInventoryFile {
	my $self=shift;
	my ($quiet)=@_;

	my $ifile=$self->GetInventoryFile();
	
	&::LogMsg("Reading patch inventory") if ($quiet!=1);
	unless(open(INV_FILE,"<$ifile")) {
		&::LogMsg("Unable to open inventory file $ifile: $!");
		return '','';
	}
	
	my @top_patches=();
	my %sub_patches=();
	while(<INV_FILE>) {
		chomp;
		s/\r$//;
		next if (/^#/);
		
		#patch_name,sub_patch_name,applied_date,patch_type,product_list
		my ($top_patch,$sub_patch,$applied_date,$patch_type,$products,$obsolete_date)=$self->DecodeInventoryLine($_);

		if ($top_patch eq $sub_patch || $sub_patch eq '') {
			#Top-level patch
			my @info=$self->AssembleTopPatchInfo($top_patch,$applied_date,$patch_type,$products,$obsolete_date);
			push(@top_patches,\@info);
		} else {
			#sub patches do not have any extra information
			my $ref=$sub_patches{$top_patch};
			if ($ref eq '') {
				$ref=[];
			}
			push(@$ref,$sub_patch);
			$sub_patches{$top_patch}=$ref;
		}	
	}
	close(INV_FILE);
	
	return (\@top_patches,\%sub_patches);
}

#########################
# Sub:  _WriteInventoryFile
# Desc: write an orpatch inventory file
# Args: top_patch_ref,subpatch_ref
# Ret:  1 if successful, 0 if not
#########################
sub _WriteInventoryFile {
	my $self=shift;
	my ($top_patch_ref,$subpatch_ref)=@_;
	
	my $interim_file=$self->_GetInterimName();
	
	unless (open(INV_FILE,">$interim_file")) {
		&::LogMsg("Unable to open $interim_file for writing: $!");
		return 0;
	}
	print INV_FILE $self->_GetInventoryHeader()."\n";
	
	#Now print each row
	foreach my $ref (@$top_patch_ref) {
		my ($top_patch,$applied_date,$patch_type,$products,$obsolete_date)=$self->DecodeTopPatchInfo($ref);
		my $line=$self->AssembleInventoryLine($top_patch,$top_patch,$applied_date,$patch_type,$products,$obsolete_date);
		print INV_FILE "$line\n";
		
		my $subs=$subpatch_ref->{$top_patch};
		if ($subs ne '') {
			foreach my $sub_patch (@$subs) {
				my $sline=$self->AssembleInventoryLine($top_patch,$sub_patch,'','','','');
				print INV_FILE "$sline\n";
			}
		}
	}
	close(INV_FILE);
	
	my $real_file=$self->GetInventoryFile();
	
	unless (File::Copy::move($interim_file,$real_file)) {
		&::LogMsg("Unable to move $interim_file to $real_file: $!");
		return 0;
	}
	
	return 1;
}

sub _GetInterimName {
	my $self=shift;
	
	my $ifile=$self->GetInventoryFile();
	
	return $ifile.".tmp";
}

#########################
# Sub:  InitializeInventoryFile
# Desc: create an empty inventory file if one does not already exist
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub InitializeInventoryFile {
	my $self=shift;
	
	my $ifile=$self->GetInventoryFile();
	
	#If the inventory already exists, no need to initialize
	if (-f $ifile) {
		return 1;
	}
	
	my ($inv_dir,$file)=&::GetFileParts($ifile,2);
	&::CreateDirs('0755',$inv_dir);
	
	&::LogMsg("Initializing patch inventory $ifile");
	unless(open(INV_FILE,">$ifile")) {
		&::LogMsg("Unable to open inventory file $ifile: $!");
		return 0;
	}
	print INV_FILE $self->_GetInventoryHeader()."\n";
	close(INV_FILE);

	return 1;
}

sub _GetInventoryHeader {
	my $self=shift;
	return '#TOP_PATCH,SUBPATCH,APPLIED_DATE,PATCH_TYPE,PRODUCTS,OBSOLETE_DATE';
}

sub DecodeTopPatchInfo {
	my $self=shift;
	my ($ref)=@_;

	my $top_patch=$$ref[0];
	my $applied_date=$$ref[1];
	my $patch_type=$$ref[2];
	my $products=$$ref[3];
	my $obsolete_date=$$ref[4];
	
	return ($top_patch,$applied_date,$patch_type,$products,$obsolete_date);
}

sub AssembleTopPatchInfo {
	my $self=shift;
	my ($top_patch,$applied_date,$patch_type,$products,$obsolete_date)=@_;
	return ($top_patch,$applied_date,$patch_type,$products,$obsolete_date);
}

sub DecodeInventoryLine {
	my $self=shift;
	my ($line)=@_;
	
	my ($top_patch,$sub_patch,$applied_date,$patch_type,$products,$obsolete_date)=split(/\,/,$line);
	$products=~s/\:/,/g;
	
	return ($top_patch,$sub_patch,$applied_date,$patch_type,$products,$obsolete_date);
}

sub AssembleInventoryLine {
	my $self=shift;
	my ($top_patch,$sub_patch,$applied_date,$patch_type,$products,$obsolete_date)=@_;
	
	#Make sure top patch and patch type do not contain commas
	$top_patch=~s/\,//g;
	$patch_type=~s/\,//g;
	#Convert commas in the product list to colons
	$products=~s/\,/:/g;
	
	return join(',',$top_patch,$sub_patch,$applied_date,$patch_type,$products,$obsolete_date);
}

#####################################################################
# Sub:  new
# Desc: Initialize an interface to the ORPatchInventory file
# Args: inventory_file - The name of the inventory file
#####################################################################
sub new {
    my $class=shift;
	my ($inventory_file)=@_;
    my $self={};
	
    bless($self,$class);
	
	$self->SetInventoryFile($inventory_file);
	
    return $self;
}

1;
