############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPatchGlobalState;

#########################
# Sub:  OpenPatchInventory
# Desc: Initialize the patch inventory if it doesn't exist
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub OpenPatchInventory {
	my $self=shift;
	
	unless ($self->{'ORP_PATCH_INVENTORY'}->InitializeInventoryFile()==1) {
		&::LogMsg("Unable to initialize patch inventory file, aborting");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  GetPatchInventory
# Desc: Get the patch inventory handle
# Args: None
# Ret:  the handle to the patch inventory object
#########################
sub GetPatchInventory {
	my $self=shift;
	return $self->{'ORP_PATCH_INVENTORY'};
}

#########################
# Sub:  GetDetailsForActions
# Desc: Get the details used by ORPAction phase methods
# Args: None
# Ret:  source_dir,source_manifest,dest_dir,patch_name,restart_state,ref to list of patches
#########################
sub GetDetailsForActions {
	my $self=shift;
	my $source_dir=$self->GetSourceDir();
	my $source_manifest=$self->GetSourceManifest();
	my $dest_dir=$self->GetDestDir();
	my $patch_name=$self->GetPatchInfo()->GetPatchName();
	my $restart_state=$self->{'RESTART_STATE'};
	my @patch_list=$self->GetPatchInfo()->GetPatchList();
	
	return ($source_dir,$source_manifest,$dest_dir,$patch_name,$restart_state,\@patch_list);
}

#########################
# Sub:  SetSourceDir
# Desc: Set the source dir for the patch to apply
# Args: source_dir
# Ret:  source_dir
#########################
sub SetSourceDir {
	my $self=shift;
	my ($source_dir)=@_;
	$self->{'SOURCE_DIR'}=$source_dir;
	$self->{'SRC_MANIFEST'}=&::ConcatPathString($source_dir,$self->GetManifestShortName());
	$self->{'SRC_DEL_MANIFEST'}=&::ConcatPathString($source_dir,$self->GetDeleteManifestShortName());
	$self->{'PATCH_INFO'}=new ORPatchInfo(&::ConcatPathString($source_dir,$self->GetPatchInfoShortName()));
	return $source_dir;
}

sub GetPatchInfoShortName {
	return 'patch_info.cfg';
}
sub GetManifestShortName {
	return 'manifest.csv';
}

sub GetDeleteManifestShortName {
	return 'delete_manifest.csv';
}

#########################
# Sub:  GetSourceDir
# Desc: Get the source dir
# Args: None
# Ret:  the source directory for the patch
#########################
sub GetSourceDir {
	my $self=shift;
	return $self->{'SOURCE_DIR'};
}

#########################
# Sub:  GetDestDir
# Desc: Get the destination dir
# Args: None
# Ret:  the destionation directory for the patch
#########################
sub GetDestDir {
	my $self=shift;
	return $self->{'DEST_DIR'};
}

#########################
# Sub:  GetSourceManifest
# Desc: Get the source manifest file name
# Args: None
# Ret:  the source manifest file name
#########################
sub GetSourceManifest {
	my $self=shift;
	return $self->{'SRC_MANIFEST'};
}

#########################
# Sub:  GetSourceDeleteManifest
# Desc: Get the source delete manifest file name
# Args: None
# Ret:  the source delete manifest file name
#########################
sub GetSourceDeleteManifest {
	my $self=shift;
	return $self->{'SRC_DEL_MANIFEST'};
}

#########################
# Sub:  GetPatchInfo
# Desc: Get the handle to the patch info object
# Args: None
# Ret:  the handle to the patch info object
#########################
sub GetPatchInfo {
	my $self=shift;
	return $self->{'PATCH_INFO'};
}

#########################
# Sub:  GetPatchMode
# Desc: Get the patch mode for orpatch
# Args: None
# Ret:  the patch mode
#########################
sub GetPatchMode {
	my $self=shift;
	return $self->{'PATCH_MODE'};
}

#########################
# Sub:  GetCustomHooks
# Desc: Get the handle to the custom hook object
# Args: None
# Ret:  the handle to the custom hook object
#########################
sub GetCustomHooks {
	my $self=shift;
	return $self->{'CUSTOM_HOOKS'};
}

#########################
# Sub:  GetConfig
# Desc: Get the handle to the configfile object
# Args: None
# Ret:  the handle to the configfile object
#########################
sub GetConfig {
	my $self=shift;
	return $self->{'ORP_CONFIG'};
}

#########################
# Sub:  GetRestorePoint
# Desc: Get the handle to the restore point object
# Args: None
# Ret:  the handle to the restore point object
#########################
sub GetRestorePoint {
	my $self=shift;
	return $self->{'RESTORE_POINT'};
}

#########################
# Sub:  ResetRestartState
# Desc: Reset the restart state so that all phases and actions are run
# Args: None
# Ret:  None
#########################
sub ResetRestartState {
	my $self=shift;
	$self->{'RESTART_STATE'}->RemoveStateFile();
}

#########################
# Sub:  RegisterPatchWithInventory
# Desc: register our patch with the patch inventory
# Args: None
# Ret:  1 if successful, 0 if not
#########################
sub RegisterPatchWithInventory {
	my $self=shift;
	
	my $patch_info=$self->GetPatchInfo();
	my $patch_inventory=$self->GetPatchInventory();
	
	#Get the list of distinct patches so that the patch inventory can identify patches which have been removed
	my $env_manifest_ref=$self->GetEnvManifestList();
	my $del_manifest_ref=$self->GetDelManifestList();
	
	#Convert the [action_name,$full_dest,$manifest_file] lists into just a manifest file list
	my @env_manifests=map {$_->[2]} @$env_manifest_ref;
	my @del_manifests=map {$_->[2]} @$del_manifest_ref;
	
	#Get the list of distinct patch names still in the environment
	my $patch_name_ref=&Manifest::GetDistinctPatchesInDestManifestList(\@env_manifests,\@del_manifests);
	
	return $patch_info->RegisterWithInventory($patch_inventory,$patch_name_ref);
}

#########################
# Sub:  SetCrossActionFlag
# Desc: Set a flag/scalar that will be used by a different action
# Args: flag name to set, value to set it to
# Ret:  None
#########################
sub SetCrossActionFlag {
	my $self=shift;
	my ($key,$value)=@_;
	
	$self->{'CROSS_ACTION_FLAGS'}->{$key}=$value;
}

#########################
# Sub:  GetCrossActionFlag
# Desc: Get the value of a specific flag
# Args: key to get value of
# Ret:  value of specific key
#########################
sub GetCrossActionFlag {
	my $self=shift;
	my ($key)=@_;
	
	return $self->{'CROSS_ACTION_FLAGS'}->{$key};
}

#########################
# Sub:  SetPatchMadeChangesFlag
# Desc: Set a flag that this patch has made changes to the environment
# Args: None (calling this always set the flag to true)
# Ret:  1
#########################
sub SetPatchMadeChangesFlag {
	my $self=shift;
	
	$self->GetRestartState()->SetPatchMadeChangesFlag(1);
	return 1;
}

#########################
# Sub:  GetPatchMadeChangesFlag
# Desc: Get a flag on whether this patch has made changes to the environment
# Args: None
# Ret:  Value of the flag (1 if the patch has made changes, 0 if not)
#########################
sub GetPatchMadeChangesFlag {
	my $self=shift;
	
	return $self->GetRestartState()->GetPatchMadeChangesFlag();
}

#########################
# Sub:  SetActionList
# Desc: Store the list of all known actions
# Args: list of handles to actions
# Ret:  ref to action list
#########################
sub SetActionList {
	my $self=shift;
	my (@action_list)=@_;
	
	$self->{'FULL_ACTION_LIST'}=\@action_list;
}

#########################
# Sub:  GetActionHandle
# Desc: Get a handle to a particular handle
# Args: action name to find
# Ret:  handle to action object, or undef if not found
#########################
sub GetActionHandle {
	my $self=shift;
	my ($action_name)=@_;
	
	my $action_ref=$self->{'FULL_ACTION_LIST'};
	foreach my $action (@$action_ref) {
		if ($action->GetActionName() eq $action_name) {
			return $action;
		}
	}
	return undef;
}

#########################
# Sub:  GetActionList
# Desc: Get the full list of actions
# Args: None
# Ret:  ref to list of actions
#########################
sub GetActionList {
	my $self=shift;
	
	return $self->{'FULL_ACTION_LIST'};
}

#########################
# Sub:  GetRestartState
# Desc: Get the restart state object
# Args: None
# Ret:  the handle to the restart state object
#########################
sub GetRestartState {
	my $self=shift;
	return $self->{'RESTART_STATE'};
}

#########################
# Sub:  AddExtraPrePatchCheck
# Desc: Get the restart state object
# Args: None
# Ret:  the handle to the restart state object
#########################
sub AddExtraPrePatchCheck {
	my $self=shift;
	my ($action_name)=@_;
	
	$self->{'EXTRA_PRE_CHECKS'}->{$action_name}=1;
	
	return 1;
}

#########################
# Sub:  GetExtraActionsForPreCheck
# Desc: Get a reference to the array of extra action checks that have been registered
# Args: None
# Ret:  undef on error, or a ref to an array of action handles
#########################
sub GetExtraActionsForPreCheck {
	my $self=shift;
	
	my @handles=();
	
	my $href=$self->{'EXTRA_PRE_CHECKS'};
	foreach my $name (keys(%{$href})) {
		my $action=$self->GetActionHandle($name);
		
		if (defined($action)) {
			push(@handles,$action);
		} else {
			&::LogMsg("Error: Unable to find handle for $name action!");
			return(undef);
		}
	}
	return \@handles;
}

#########################
# Sub:  GetManifestList
# Desc: Get a list of the manifests from all Actions that exist on this host
# Args: env_manifest - 1 if environment manifests should be included
#       delete_manifest - 1 if delete manifests should be included
# Ret:  ref to list of [action_name,full_dest,manifest name]
#########################
sub GetManifestList {
	my $self=shift;
	my ($env_manifest,$del_manifest)=@_;
	
	my $dest_dir=$self->GetDestDir();
	
	my @manifest_list=();
	my $action_ref=$self->GetActionList();
	foreach my $action (@$action_ref) {
		my $name=$action->GetActionName();
		if ($env_manifest==1) {
			my $list=$action->GetEnvManifestList($dest_dir);
			foreach my $ref (@$list) {
				push(@manifest_list,[$name,@$ref]);
			}
		}
		if ($del_manifest==1) {
			my $list=$action->GetDeleteManifestList($dest_dir);
			foreach my $ref (@$list) {
				push(@manifest_list,[$name,@$ref]);
			}
		}
	}
	
	return \@manifest_list;
}

#########################
# Sub:  ReadSourceManifest
# Desc: Read our source manifest and build a type lookup object, saving for later retreival by actions
# Args: None
# Ret:  1 on success, 0 on failure
#########################
sub ReadSourceManifest {
	my $self=shift;
	
	my @src_info=&Manifest::ReadManifest($self->GetSourceManifest());

	$self->{'SRC_MANIFEST_INFO'}=\@src_info;
	
	my %type_lookup=();
	my @types=&Manifest::BuildSourceManifestTypeHash(\@src_info,\%type_lookup);
	
	$self->{'SRC_MANIFEST_TYPES'}=\@types;
	$self->{'SRC_MANIFEST_TYPE_LOOKUP'}=\%type_lookup;
	
	return 1;
}

#########################
# Sub:  GetSourceManifestInfo
# Desc: Get refs to the source manifest info, cross references, and summaries
# Args: None
# Ret:  list of (ref to source info list, ref to type lookup hash,ref to list of distinct types)
#########################
sub GetSourceManifestInfo {
	my $self=shift;
	
	return ($self->{'SRC_MANIFEST_INFO'},$self->{'SRC_MANIFEST_TYPE_LOOKUP'},$self->{'SRC_MANIFEST_TYPES'});
}

sub GetEnvManifestList {
	my $self=shift;
	return $self->GetManifestList(1,0);
}

sub GetDelManifestList {
	my $self=shift;
	return $self->GetManifestList(0,1);
}

sub GetFullManifestList {
	my $self=shift;
	return $self->GetManifestList(1,1);
}


#########################
# Sub:  ValidateConfig
# Desc: Ensure our global state setup looks valid
#########################
sub ValidateConfig {
	my $self=shift;
	my ($skip_load_restart)=@_;
	
	my $source_dir=$self->GetSourceDir();

	unless (-d $source_dir ) {
		&::LogMsg("Source dir: $source_dir does not exist");
		return 0;
	}
	unless (-d $self->GetDestDir() ) {
		my $dest_dir_var_name=$self->{'DEST_DIR_VAR_NAME'};
		&::LogMsg("Destination dir: ".$self->GetDestDir()." does not exist, ensure $dest_dir_var_name is correctly defined");
		return 0;
	}
	
	my $s_manifest=$self->GetSourceManifest();
	unless (-f $s_manifest) {
		&::LogMsg("Patch manifest $s_manifest not found");
		return 0;
	}
	
	unless ($self->{'PATCH_INFO'}->ReadPatchInfo()==1) {
		return 0;
	}
	
	my $pname=$self->{'PATCH_INFO'}->GetPatchName();
	if ($pname eq '') {
		&::LogMsg("Unable to determine patch name, invalid patch?");
		return 0;
	}
	
	unless ($self->_ValidatePatchInfo()==1) {
		return 0;
	}
	
	unless ($skip_load_restart==1) {
		if ($self->{'RESTART_STATE'}->LoadRestartPoint($pname)!=1) {
			&::LogMsg("Load of orpatch restart state failed, aborting");
			return 0;
		}
	}
	
	unless ($self->{'CUSTOM_HOOKS'}->ReadHookConfig($self->{'PATCH_INFO'},$source_dir)==1) {
		&::LogMsg("Unable to load custom hook configuration file, aborting");
		return 0;
	}
	
	return 1;
}

#########################
# Sub:  ValidatePatchlessConfig
# Desc: Setup a global state for a patchless configuration
#########################
sub ValidatePatchlessConfig {
	my $self=shift;
	
	$self->SetSourceDir('');
	
	unless (-d $self->GetDestDir() ) {
		my $dest_dir_var_name=$self->{'DEST_DIR_VAR_NAME'};
		&::LogMsg("Destination dir: ".$self->GetDestDir()." does not exist, ensure $dest_dir_var_name is correctly defined");
		return 0;
	}

	my $pname='DUMMY_PATCH';
	#Make a dummy cummulative patch for any possible product
	$self->{'PATCH_INFO'}->DummyPatchInfo($pname,$self->{'PATCH_INFO'}->GetCummulativePatchType(),'RMS,RPM,REIM,ALLOC,RWMS');

	unless ($self->_ValidatePatchInfo()==1) {
		return 0;
	}
	
	return 1;
}


sub _ValidatePatchInfo() {
	my $self=shift;
	
	my $patch_mode='PATCHSET';
	if ($self->{'PATCH_INFO'}->IsPatchCummulative()) {
		$patch_mode='PATCHSET';
	} else {
		#Incremental patch
		$patch_mode='HOTFIX';
	}
	
	$self->{'PATCH_MODE'}=$patch_mode;
	
	return 1;
}

#####################################################################
# Sub:  new
# Desc: Initialize a new global state object
# Args: orp_dir - the base directory for ORPatch files and structures
#####################################################################
sub new {
    my $class=shift;
	my ($orp_dir,$restart_file,$config_ref,$dest_dir,$dest_dir_var_name)=@_;
	
	if ($restart_file eq '') {
		$restart_file=&::ConcatPathString($orp_dir,'logs','orpatch_restart.state');
	}
	
    my $self={};
	
	$self->{'ORP_DIR'}=$orp_dir;
	$self->{'ORP_PATCH_INVENTORY'}=new ORPatchInventory(&::ConcatPathString($orp_dir,'inventory','orpatch_inventory.csv'));
	$self->{'SOURCE_DIR'}='';
	$self->{'DEST_DIR'}=$dest_dir;
	$self->{'DEST_DIR_VAR_NAME'}=$dest_dir_var_name;
	$self->{'SRC_MANIFEST'}='';
	$self->{'PATCH_INFO'}='';
	$self->{'RESTART_STATE'}=new ORPRestartState($restart_file);
	$self->{'CUSTOM_HOOKS'}=new ORPCustomHook(&::ConcatPathString($orp_dir,'config','custom_hooks.cfg'));
	$self->{'RESTORE_POINT'}=new ORPRestorePoint($self->{'DEST_DIR'});
	$self->{'PATCH_MODE'}='PATCHSET';
	$self->{'CROSS_ACTION_FLAGS'}={};
	$self->{'FULL_ACTION_LIST'}=[];
	$self->{'EXTRA_PRE_CHECKS'}={};
	$self->{'ORP_CONFIG'}=$config_ref;
	$self->{'SRC_MANIFEST_INFO'}=[];
	$self->{'SRC_MANIFEST_TYPES'}=[];
	$self->{'SRC_MANIFEST_TYPE_LOOKUP'}={};
	
    bless($self,$class);
    return $self;
}

1;
