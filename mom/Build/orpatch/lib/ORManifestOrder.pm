############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
package ORManifestOrder;

use strict;

BEGIN {
    use Exporter   ();
    our ( $VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS );

    # set the version for version checking
    $VERSION = 1.00;

    @ISA = qw( Exporter );
    @EXPORT =
      qw(
          %embedded_file_order
          @embedded_file_source
          &SortSourceManifest
          &SortDestManifest
          &SQL_Order_From_File
       );
    %EXPORT_TAGS = ( );                                                 # eg: TAG => [ qw!name1 name2! ],

    # your exported package globals go here,
    # as well as any optionally exported functions
    @EXPORT_OK =
      qw(
          %rms_form_pll_order
          %rms_toolset_pll_order
          %type_order
       );
}
our @EXPORT_OK;

####################################################################################################

use Symbol;
use File::Basename qw( basename );

use subs qw( SQL_Order_From_File );

our %embedded_file_order;

our %rms_toolset_pll_order;
our %rms_form_pll_order;
our %type_order;

our @embedded_file_source;

####################################################################################################

# Initialize %embedded_file_order with the RMS and RWMS locations for RIB, and also RWMS base data..

@embedded_file_source=(
    ['RFM_RIB',         qr/^rfm_db_rib_top$/,               'Localization/rib_objects/db_objects/InstallAndCompileAllRFMRibOracleObjects.sql'],
    ['RMS_RIB',         qr/^rms_db_rib_top$/,               'Cross_Pillar/rib_objects/db_objects/InstallAndCompileAllRibOracleObjects.sql'],
    ['RWMS_RIB',        qr/^rwms_db_rib_top$/,              'Rib_Objects/Src/InstallAndCompileAllRibOracleObjects.sql'],
    ['RAFTEST_RIB',     qr/^raftest_db_rib_top$/,           'RetailAppsFrameworkDatabase/DatabaseScripts/src/testonly/rib_objects/db_objects/InstallAndCompileAllRibOracleObjects.sql'],
    ['RWMS_BASE',       qr/^rwms_db_installscripts_top$/,   'Install_Data/Source/create_base_data.sql'],
    ['RASECORE_TYPES',  qr/^rasecore_db_type_spec$/,        'core_db/db/types/create_all.sql'],
    ['RASEMBA_TYPES',   qr/^rasemba_db_type_spec$/,         'cdm/mba/db/types/create_all.sql'],
    ['RASECIS_TYPES',   qr/^rasecis_db_type_spec$/,         'cdm/cis/db/types/create_all.sql'],
    ['RASECDT_TYPES',   qr/^rasecdt_db_type_spec$/,         'cdm/cdt/db/types/create_all.sql'],
    ['RASEDT_TYPES',    qr/^rasedt_db_type_spec$/,          'cdm/dt/db/types/create_all.sql'],
    ['RASEAE_TYPES',    qr/^raseae_db_type_spec$/,          'cdm/ae/db/types/create_all.sql'],
    ['RASEASO_TYPES',   qr/^raseaso_db_type_spec$/,         'so/db/types/create_all.sql'],
	['RASERL_TYPES',   qr/^raserl_db_type_spec$/,           'ai/rl/db/types/create_all.sql'],
    ['RASECORE_VIEWS',  qr/^rasecore_db_views$/,            'core_db/db/views/create_all.sql'],
    ['RASEMBA_VIEWS',   qr/^rasemba_db_views$/,             'cdm/mba/db/views/create_all.sql'],
    ['RASECIS_VIEWS',   qr/^rasecis_db_views$/,             'cdm/cis/db/views/create_all.sql'],
    ['RASECDT_VIEWS',   qr/^rasecdt_db_views$/,             'cdm/cdt/db/views/create_all.sql'],
    ['RASEDT_VIEWS',    qr/^rasedt_db_views$/,              'cdm/dt/db/views/create_all.sql'],
    ['RASEAE_VIEWS',    qr/^raseae_db_views$/,              'cdm/ae/db/views/create_all.sql'],
    ['RASEASO_VIEWS',   qr/^raseaso_db_views$/,             'so/db/views/create_all.sql'],
	['RASERL_VIEWS',   qr/^raserl_db_views$/,               'ai/rl/db/views/create_all.sql'],
	['RASECORE_PATCHES',qr/^rasecore_db_change_scripts$/,	'core_db/db/patches/create_all.sql'],
	['RASEMBA_PATCHES',	qr/^rasemba_db_change_scripts$/,	'cdm/mba/db/patches/create_all.sql'],
	['RASECIS_PATCHES',	qr/^rasecis_db_change_scripts$/,	'cdm/cis/db/patches/create_all.sql'],
	['RASECDT_PATCHES',	qr/^rasecdt_db_change_scripts$/,	'cdm/cdt/db/patches/create_all.sql'],
	['RASEDT_PATCHES',	qr/^rasedt_db_change_scripts$/,		'cdm/dt/db/patches/create_all.sql'],
    ['RASEAE_PATCHES',	qr/^raseae_db_change_scripts$/,		'cdm/ae/db/patches/create_all.sql'],
	['RASEASO_PATCHES',	qr/^raseaso_db_change_scripts$/,	'so/db/patches/create_all.sql'],
	['RASERL_PATCHES',	qr/^raserl_db_change_scripts$/,	    'ai/rl/db/patches/create_all.sql'],
    );

&SQL_Order_From_File(@embedded_file_source);

# Scoped variables are visible only to Manifest_Order.
my @type_order =
  qw(
     orpatch_bin
     orpatch_lib
     orpatch_lib_actions
     orpatch_utils
     orpatch_deploy
     orpatch_cfg_template

     rmsbdi_db_ddl_install
     rmsbdi_db_ddl_rerun
     rmsbdi_db_rtg_packages
     rmsbdi_db_grants

     raf_db_control_scripts
     raf_db_change_scripts
     raf_db_ddl
     raf_db_install_scripts
     raf_db_packages

     raftest_db_control_scripts
     raftest_db_change_scripts
     raftest_db_ddl
     raftest_db_install_scripts
     raftest_db_packages

     rms_db_ddl_rib_kernel_clob
     rms_db_ddl_rib_clob_data
     rms_db_change_scripts_rib
     rms_db_ddl_rib_packages

     rms_db_ddl
     rms_db_ddl_mv
     rms_db_ddl_vw
     rms_db_logger_install

     rmsdas_db_ddl
     rmsdas_db_change_scripts
     rmsdas_db_types
     rmsdas_db_views

     rfm_db_ddl
     rfm_db_types
     rfm_db_change_scripts
     rfm_db_packages
     rfm_db_taxweb_packages
     rfm_db_synchro_packages
     rfm_db_triggers
     rfm_db_form_menu_elements
     rfm_db_installscripts
     rfm_db_control_scripts
     rfm_db_taxweb_control_scripts
     rfm_db_synchro_control_scripts
     rfm_db_languages

     rfm_pc_batch_lib
     rfm_tw_pc_batch_lib
     rfm_sy_pc_batch_lib
     rfm_pc_batch_proc
     rfm_tw_pc_batch_proc
     rfm_sy_pc_batch_proc
     rfm_frm_forms

     rfm_data_conversion
     rfm_data_conversion_config
     rfm_java_taxweb_urlinvoker
     rfm_db_taxweb_urlinvoker

     rarmsbatch_db_rms_syn
     rarmsbatch_db_rms_grant
     rarmsbatch_db_ddl
     rarmsbatch_db_change_scripts
     radm_db_ddl
     radm_db_change_scripts
     radm_db_control_scripts
     radm_db_packages
     radm_db_languages
     radm_db_role_grants
     radm_db_rabatch_grant
     radm_db_rabatch_syn
     radm_db_rafe_syn
     rafedm_db_utility
     rabatch_db_ddl
     rabatch_db_change_scripts
     rabatch_db_control_scripts
     rabatch_db_packages

     rde_rmsbatch_db_rms_syn
     rde_rmsbatch_db_rms_grant
     rde_rmsbatch_db_ddl
     rde_rmsbatch_db_change_scripts
     rde_dm_db_ddl
     rde_dm_db_change_scripts
     rde_dm_db_control_scripts
     rde_dm_db_packages
     rde_dm_db_languages
     rde_dm_db_role_grants
     rde_dm_db_rabatch_grant
     rde_dm_db_rabatch_syn
     rde_batch_db_ddl
     rde_batch_db_change_scripts
     rde_batch_db_control_scripts
     rde_batch_db_utility_files

     rwms_db_ddl
     rpm_db_ddl
     reim_db_ddl
     alcrms_db_ddl
     alc_db_ddl

     rms_db_rib_top
     rfm_db_rib_top
     rwms_db_rib_top
     rms_db_rib
     rfm_db_rib
     rwms_db_rib

     rms_db_types
     rwms_db_types
     rpm_db_types
     reim_db_types
     alcrms_db_types

     rms_db_change_scripts
     rpm_db_change_scripts
     reim_db_change_scripts
     rwms_db_change_scripts
     alcrms_db_change_scripts
     alc_db_change_scripts

     rms_db_form_elements
     rms_db_languages
     alcrms_db_languages

     rms_db_packages
     rmsbdi_db_rms_packages
     rwms_db_packages
     rpm_db_packages
     reim_db_packages
     rms_db_external_ref
     rms_db_partitioning

     alcrms_db_packages

     rms_db_procedures
     rwms_db_procedures

     rms_db_triggers
     rpm_db_triggers
     rwms_db_triggers
     alcrms_db_triggers
     reim_db_triggers

     rms_db_control_scripts
     alcrms_db_control_scripts
     rpm_db_control_scripts

     rms_pc_batch_lib
     rms_pc_batch_proc
     rfm_s9t_templates
     rms_s9t_templates

     rms_data_conversion
     rpm_data_conversion
     rms_data_conversion_config

     rms_retl_scripts
     rms_retl_scripts_config

     rms_frm_toolset
     rms_frm_forms
     rwms_frm_reference
     rwms_frm_forms

     rfm_reports
     rms_reports
     rwms_reports

     rwms_db_installscripts_top
     rms_db_installscripts
     rwms_db_installscripts
     rpm_db_install_scripts
     reim_db_install_scripts
     alc_db_install_scripts
     alcrms_db_install_scripts
     alcrms_db_seed_scripts

     rms_dummy_libraries

     rwmsadf_db_install_scripts
     rwms_db_adf_objects
     rwms_db_user_install

     rmsdemo_db_install_scripts

     rwms_db_sp_jars
     rms_db_ws_consumer_libs
     rms_db_ws_consumer_jars
     rms_db_ws_consumer_sql

     rwms_db_grants

     rms_db_upgrade_scripts

     rms_db_ut_dbc
     rpm_db_ut_dbc
     rpm_db_ut_packages
     rfm_db_ut_packages
     rms_db_ut_packages
     rms_db_ut_control
     rpm_db_ut_control

     radm_db_rse_grants
     rasecore_db_type_spec

     rasecore_db_tables
     rasecore_db_seqs
     rasecore_db_synonyms
     rasecore_db_change_scripts
     rasecore_db_pkg_spec
     rasecore_db_views
     rasecore_db_mviews
     rasecore_db_type_body
     rasecore_db_pkg_body
     rasecore_db_constraints

     rasecdt_db_type_spec
     rasecdt_db_tables
     rasecdt_db_seqs
     rasecdt_db_synonyms
     rasecdt_db_change_scripts
     rasecdt_db_pkg_spec
     rasecdt_db_views
     rasecdt_db_mviews
     rasecdt_db_type_body
     rasecdt_db_pkg_body

     rasecis_db_type_spec
     rasecis_db_tables
     rasecis_db_seqs
     rasecis_db_synonyms
     rasecis_db_change_scripts
     rasecis_db_pkg_spec
     rasecis_db_views
     rasecis_db_mviews
     rasecis_db_type_body
     rasecis_db_pkg_body

     rasedt_db_type_spec
     rasedt_db_tables
     rasedt_db_seqs
     rasedt_db_synonyms
     rasedt_db_change_scripts
     rasedt_db_pkg_spec
     rasedt_db_views
     rasedt_db_mviews
     rasedt_db_type_body
     rasedt_db_pkg_body
     rasedt_db_constraints
     
     raseae_db_type_spec
     raseae_db_tables
     raseae_db_seqs
     raseae_db_synonyms
     raseae_db_change_scripts
     raseae_db_pkg_spec
     raseae_db_views
     raseae_db_mviews
     raseae_db_type_body
     raseae_db_pkg_body
     raseae_db_constraints

     rasemba_db_type_spec
     rasemba_db_tables
     rasemba_db_seqs
     rasemba_db_synonyms
     rasemba_db_change_scripts
     rasemba_db_pkg_spec
     rasemba_db_views
     rasemba_db_mviews
     rasemba_db_type_body
     rasemba_db_pkg_body
     rasemba_db_constraints
     rasemba_db_controlfiles
     rasemba_db_seedscripts
     
	 raseaso_db_type_spec
     raseaso_db_tables
     raseaso_db_seqs
     raseaso_db_synonyms
     raseaso_db_change_scripts
     raseaso_db_pkg_spec
     raseaso_db_views
     raseaso_db_mviews
     raseaso_db_type_body
     raseaso_db_pkg_body
     raseaso_db_constraints
	 
	 raserl_db_type_spec
     raserl_db_tables
     raserl_db_seqs
     raserl_db_synonyms
     raserl_db_change_scripts
     raserl_db_pkg_spec
     raserl_db_views
     raserl_db_mviews
     raserl_db_type_body
     raserl_db_pkg_body
     raserl_db_constraints

     rasecore_db_ra_grants
     rasecdt_db_ra_grants
     rasecis_db_ra_grants
     rasedt_db_ra_grants
     rasemba_db_ra_grants
     raseaso_db_ra_grants
	 raserl_db_ra_grants

     rasebatch_seed_files
     rasebatch_scripts_bin
     rasebatch_scripts_lib
     
     ui_db_install_scripts
   );

@type_order{@type_order} = 1 .. @type_order;

my @rms_toolset_pll_order =
  qw(
      messge45.pll
      ariiflib.pll
      stand45.pll
      calend45.pll
      find45.pll
      item45.pll
      tools45.pll
      mblock45.pll
      mview45.pll
      nav45.pll
      work45.pll
      itnumtype.pll
      hierfilter.pll
      rmslib.pll
      cflex.pll
   );

@rms_toolset_pll_order{@rms_toolset_pll_order} = 1 .. @rms_toolset_pll_order;

my @rms_form_pll_order =
  qw(
      links45.pll
      itemuda.pll
      og.pll
      l10nflexattr.pll
   );

@rms_form_pll_order{@rms_form_pll_order} = 1 .. @rms_form_pll_order;

####################################################################################################

sub SQL_Order_From_File {
    my (@embedded_files)=@_;

    foreach my $ref (@embedded_files) {
        my ($type_name,$type_pat,$file)=@$ref;

        next unless (-f $file);

        my @top;
        my $f = gensym;

        my %embedded_order=();

        unless(open(EMBEDDED_FILE,"<$file")) {
            die "cannot open top-level sql install script for reading ... exiting\n";
        }
        push @top, map { /^\s*\@\@\s*(.*?)(?:\s*;)?\s*$/; $1 } grep { /^\s*\@\@/ && !/compileinvalid.*sql/i } <EMBEDDED_FILE>;
        close(EMBEDDED_FILE);

        # We number all included names in the order in which they appeared.
        @embedded_order{@top}= 1..@top;
        $embedded_file_order{$type_name}=\%embedded_order;
    }
}

####################################################################################################

# This sorts a source manifest (where source filename is in position 0 and file type is in position 2)
sub SortSourceManifest ($$) {
  return &_SortManifest(0,2,@_);
}

# This sorts a destination manifest (where source filename is in position 1 and file type is in position 3)
sub SortDestManifest ($$) {
  return &_SortManifest(1,3,@_);
}

sub _SortManifest {
    my $fn_index=shift(@_);
    my $ft_index=shift(@_);

    my ( $A, $B );

    return -1 if   $_[0]->[$ft_index] && ! $_[1]->[$ft_index];            # Missing types get sorted last.
    return  1 if ! $_[0]->[$ft_index] &&   $_[1]->[$ft_index];

    if ( $_[0]->[$ft_index] ne $_[1]->[$ft_index] ) {                     # Sort first by types.
        return &UseOrderHash(\%type_order,$_[0]->[$ft_index],$_[1]->[$ft_index]);
    }

    #If this is a type that is ordered by embedded file contents, use the Order Hash
    foreach my $ref (@embedded_file_source) {
        my ($efo_hash_type,$this_type,$src_file)=@$ref;

        if ( $_[0]->[$ft_index]=~/$this_type/) {
            return &UseOrderHash($embedded_file_order{$efo_hash_type},basename( $_[0]->[$fn_index] ),basename( $_[1]->[$fn_index] ));
        }
    }

    if ( $_[0]->[$ft_index] eq 'rms_db_change_scripts' || $_[0]->[$ft_index] eq 'rms_db_ut_dbc' ||
         $_[0]->[$ft_index] eq 'rpm_db_ut_dbc' || $_[0]->[$ft_index] eq 'rms_db_change_scripts_rib') {                # Then by the dbc basename.
        return basename( $_[0]->[$fn_index] ) cmp basename( $_[1]->[$fn_index] )
    }

    if ( $_[0]->[$ft_index] =~ /^rms_frm_/ ) {
        $A = $_[0]->[$fn_index] =~ m</[^/]*\.pll$>;
        $B = $_[1]->[$fn_index] =~ m</[^/]*\.pll$>;
        return -1 if   $A && ! $B;                                          # *.pll forms first.
        return  1 if ! $A &&   $B;

        if ( $A && $_[0]->[$ft_index] eq 'rms_frm_toolset' ) {              # Use toolset pll order.
            return &UseOrderHash(\%rms_toolset_pll_order,basename($_[0]->[$fn_index]),basename($_[1]->[$fn_index]));
        } elsif ( $A && $_[0]->[$ft_index] eq 'rms_frm_forms' ) {           # Use form pll order.
            return &UseOrderHash(\%rms_form_pll_order,basename($_[0]->[$fn_index]),basename($_[1]->[$fn_index]));
        }

        $A = $_[0]->[$fn_index] =~ m</fm_[^/]*\.fmb$>;
        $B = $_[1]->[$fn_index] =~ m</fm_[^/]*\.fmb$>;
        return -1 if   $A && ! $B;                                          # fm_*.fmb reference forms next.
        return  1 if ! $A &&   $B;

        $A = $_[0]->[$fn_index] =~ m</[^/]*\.fmb$>;
        $B = $_[1]->[$fn_index] =~ m</[^/]*\.fmb$>;
        return -1 if   $A && ! $B;                                          # Then *.fmb before *.mmb?
        return  1 if ! $A &&   $B;

        return $_[0]->[$fn_index] cmp $_[1]->[$fn_index];                   # Lastly, sort alphabetically by the file path.
    }

    if ( $_[0]->[$ft_index] =~ m<(db_packages$|db_ut_packages$|db_ddl_rib_packages$)> ) {                        # This rule is used for all packages throughout all products.
        $A = $_[0]->[$fn_index] =~ m<\.pks$|s\.pls$|[Ss]pec\.pls$>;
        $B = $_[1]->[$fn_index] =~ m<\.pks$|s\.pls$|[Ss]pec\.pls$>;
        return -1 if   $A && ! $B;
        return  1 if ! $A &&   $B;
    }

    if ( $_[0]->[$ft_index] eq 'rms_db_control_scripts' ) {               # Put sa_parm.sql later since it depends on its type file.
        $A = $_[0]->[$fn_index] =~ m</sa_parm\.sql$>;
        $B = $_[1]->[$fn_index] =~ m</sa_parm\.sql$>;
        return  1 if   $A && ! $B;
        return -1 if ! $A &&   $B;
    }

    return $_[0]->[$fn_index] cmp $_[1]->[$fn_index];                     # Lastly, sort alphabetically by the file path.
}

sub UseOrderHash {
  my ($order_hash_ref,$key1,$key2)=@_;

  # If neither of the keys exist, sort them equally
  if (!exists($order_hash_ref->{ $key1 }) && !exists($order_hash_ref->{ $key2 })) {
    return ($key1 cmp $key2);
  }

  # If one of the keys doesn't exist in the order hash, sort it last
  if (exists($order_hash_ref->{ $key1 }) && !exists($order_hash_ref->{ $key2 })) {
    return -1;
  }
  if (!exists($order_hash_ref->{ $key1 }) && exists($order_hash_ref->{ $key2 })) {
    return 1;
  }

  # If both exist, sort them by the hash
  return $order_hash_ref->{ $key1 } <=> $order_hash_ref->{ $key2 }
}

1;
