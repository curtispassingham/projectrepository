############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

package ORPAnalyzeDetails;

#########################
# Sub:  Current
# Ret:  A value that can be passed to GetGenDir, GetMetadataHandle, GetMetadataMap indicating to retrieve from current environment data
#########################
sub Current {
	return 1;
}

#########################
# Sub:  New
# Ret:  A value that can be passed to GetGenDir, GetMetadataHandle, GetMetadataMap indicating to retrieve from new environment data
#########################
sub New {
	return 0;
}

#########################
# Sub:  GetGenDir
# Desc: Get a specific generated dir for metadata
# Args: action_name - Action the file will be associated with
#       current - the return of either $self->Current() or $self->New()
# Ret:  The generated directory to use
#########################
sub GetGenDir {
	my $self=shift;
	my ($action,$current)=@_;
	
	my $metadata=$self->GetExportedMetadata($current);

	my $support_dir=$metadata->GetExportDir();
	my $gen_dir=&::ConcatPathString($support_dir,'generated',$action);
	
	return $gen_dir;
}

#########################
# Sub:  GetExportedMetadata
# Desc: Get a specific exported metadata handle
# Args: current - the return of either $self->Current() or $self->New()
# Ret:  The handle of the appropriate exported metadata object
#########################
sub GetExportedMetadata {
	my $self=shift;
	my ($current)=@_;
	return (($current==1) ? $self->{'CURRENT_METADATA'} : $self->{'NEW_METADATA'});
}

#########################
# Sub:  GetCurrentExportedMetadata
# Desc: Get the handle for the current Exported Metadata
# Args: None
# Ret:  the handle to the current exported metadata
#########################
sub GetCurrentExportedMetadata {
	my $self=shift;
	return $self->GetExportedMetadata($self->Current());
}

#########################
# Sub:  GetNewExportedMetadata
# Desc: Get the handle for the new Exported Metadata
# Args: None
# Ret:  the handle to the new exported metadata
#########################
sub GetNewExportedMetadata {
	my $self=shift;
	return $self->GetExportedMetadata($self->New());
}

#########################
# Sub:  GetMetadataMap
# Desc: Get the handle to the MetadataMap for a given Exported Metadata object
# Args: current - the return of either $self->Current() or $self->New()
# Ret:  the handle to the appropriate metadata map
#########################
sub GetMetadataMap {
	my $self=shift;
	my ($current)=@_;
	return $self->GetExportedMetadata($current)->GetMetadataMapHandle();
}

#########################
# Sub:  GetCurrentMetadataMap
# Desc: Get the handle for the current Metadata Map
# Args: None
# Ret:  the handle to the current metadata map
#########################
sub GetCurrentMetadataMap {
	my $self=shift;
	return $self->GetCurrentExportedMetadata()->GetMetadataMapHandle();
}

#########################
# Sub:  GetNewMetadataMap
# Desc: Get the handle for the current Metadata Map
# Args: None
# Ret:  the handle to the current metadata map
#########################
sub GetNewMetadataMap {
	my $self=shift;
	return $self->GetNewExportedMetadata()->GetMetadataMapHandle();
}

#########################
# Sub:  SetDetailFileType
# Desc: Set the type of file type to use for comparisons of any detail files registered
# Args: action_name, compare_type
# Ret:  1
#########################
sub SetDetailFileType {
	my $self=shift;
	my ($action,$compare_type)=@_;
	
	my $action_map_ref=$self->{'ACTION_DETAIL_MAP'};
	my $ref=$action_map_ref->{$action};
	
	if ($ref eq '') {
		$ref={};
	}
	
	$ref->{$compare_type}=1;
	
	$action_map_ref->{$action}=$ref;
	
	return 1;
}

#########################
# Sub:  GetDetailFileType
# Desc: Get the type of file type to use for comparisons of any detail files registered for an action
# Args: action_name
# Ret:  array of file types to compare for this action or an empty list of no detail files were registered
#########################
sub GetDetailFileType {
	my $self=shift;
	my ($action)=@_;
	
	my $action_map_ref=$self->{'ACTION_DETAIL_MAP'};
	my $ref=$action_map_ref->{$action};
	
	my @types=();
	
	if ($ref ne '') {
		@types=keys(%$ref);
	}
	
	return(@types);
}

#########################
# Sub:  RegisterPureCustomCount
# Desc: Register the count of pure custom files to subtract from the impact of an archive copy from an action
# Args: action_name, pure_custom_count
# Ret:  1
#########################
sub RegisterPureCustomCount {
	my $self=shift;
	my ($action,$pure_custom_count)=@_;
	
	$self->{'ACTION_CUSTOM_ONLY_COUNT'}->{$action}=$pure_custom_count;

	return 1;
}

#########################
# Sub:  GetPureCustomCount
# Desc: Get the count of pure custom files to subtract from the impact of an archive copy
# Args: action_name
# Ret:  pure_custom_count
#########################
sub GetPureCustomCount {
	my $self=shift;
	my ($action)=@_;
	
	my $extra_details_ref=$self->{'ACTION_CUSTOM_ONLY_COUNT'};
	my $count=$extra_details_ref->{$action};
	
	$count=0 if $count eq '';

	return $count;
}

#########################
# Sub:  RegisterArchiveCustomImpact
# Desc: Register an additional custom file that is impacted by an archive copy from an action
# Args: action_name, custom_file
# Ret:  1
#########################
sub RegisterArchiveCustomImpact {
	my $self=shift;
	my ($action,$custom_file)=@_;
	
	my $extra_detail_ref=$self->{'ACTION_EXTRA_CUSTOM'};
	my $ref=$extra_detail_ref->{$action};
	
	if ($ref eq '') {
		$ref=[];
	}
	
	push(@$ref,$custom_file);
	$extra_detail_ref->{$action}=$ref;
	return 1;
}

#########################
# Sub:  GetArchiveCustomImpacts
# Desc: Get the list of additional custom files that are impacted by an archive copy
# Args: action_name
# Ret:  array of custom file names
#########################
sub GetArchiveCustomImpacts {
	my $self=shift;
	my ($action)=@_;
	
	my $extra_details_ref=$self->{'ACTION_EXTRA_CUSTOM'};
	my $ref=$extra_details_ref->{$action};
	
	my @files=();
	
	if ($ref ne '') {
		@files=@$ref;
	}
	
	return(@files);
}

sub GetExtraDetailsAlwaysFlag {
	my $self=shift;
	return $self->{'EXTRA_DETAILS_ALWAYS'};
}

#####################################################################
# Sub:  new
# Args: global_state - Handle to the global state object
#       current_support_dir - Directory where any extra detail files related to the current environment are put
#       new_support_dir - Directory where any extra detail files related to the expected new environment are put
#####################################################################
sub new {
    my $class=shift;
	my ($global_state,$current_support_dir,$new_support_dir,$always_extra)=@_;
    my $self={};
	
	$self->{'CURRENT_METADATA'}=new ORPExportedMetadata($current_support_dir,'current',$global_state);
	$self->{'NEW_METADATA'}=new ORPExportedMetadata($new_support_dir,'new',$global_state);
	
	$self->{'ACTION_DETAIL_MAP'}={};
	$self->{'ACTION_EXTRA_CUSTOM'}={};
	$self->{'ACTION_CUSTOM_ONLY_COUNT'}={};
	
	$self->{'EXTRA_DETAILS_ALWAYS'}=$always_extra;
	
    bless($self,$class);	
    return $self;
}

1;
