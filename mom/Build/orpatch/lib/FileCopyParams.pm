############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################

package FileCopyParams;
use strict;

#########################
# Sub:  GetBasicParams
# Desc: Get the basic file copy params
# Args: none
# Ret:  (ref to file type list,dest subdir)
#########################
sub GetBasicParams {
	my $self=shift;
	my $file_types=$self->GetFileTypes();
	my $dest_subdir=$self->GetDestSubDir();
	return ($file_types,$dest_subdir);
}

#########################
# Sub:  SetStripDirs
# Desc: Set the number of source directories to remove when preserving directories
# Args: strip_dirs - number of dirs to strip, or hash ref to set some types and make all others the default
# Ret:  1
#########################
sub SetStripDirs {
	my $self=shift;
	my ($type_lookup)=@_;
	#Convert negative numbers to positive numbers
	if (!ref($type_lookup)) {
		if ($type_lookup<0) {
			$type_lookup=0-$type_lookup;
		}
	} else {
		my %new_hash=();
		foreach my $key (keys(%$type_lookup)) {
			$new_hash{$key}=$type_lookup->{$key};
			if ($new_hash{$key}<0) {
				$new_hash{$key}=0-$new_hash{$key};
			}
		}
		$type_lookup=\%new_hash;
	}
	return $self->_SetTypeHashValues($type_lookup,'TYPE_STRIP_DIRS',0);
}

#########################
# Sub:  SetIgnorePatterns
# Desc: Set the list of filename patterns to ignore when copying files
# Args: ref to a list of ignore patterns
# Ret:  1
#########################
sub SetIgnorePatterns {
	my $self=shift;
	my ($ignore_pat_ref)=@_;
	$self->{'IGNORE_PATTERNS'}=$ignore_pat_ref;
	return 1;
}

#########################
# Sub:  SetUpdateCriteria
# Desc: set how a file can be overwritten
# Args: 'ALWAYS' or 'NEVER' or 'IFNEW' to set for all types, or hash ref to set some types and make all others the default
# Ret:  1
#########################
sub SetUpdateCriteria {
	my $self=shift;
	my ($type_lookup)=@_;
	
	my $default='IFNEW';
	if ($self->_SetTypeHashValues($type_lookup,'TYPE_ALLOW_OVERWRITE',$default)!=1) {
		return 0;
	}
	#Make sure all the values are uppercase and 
	my $href=$self->{'TYPE_ALLOW_OVERWRITE'};
	foreach my $key (keys(%$href)) {
		my $val=$href->{$key};
		$val=~tr/a-z/A-Z/;
		if ($val ne 'IFNEW' && $val ne 'ALWAYS' && $val ne 'NEVER' && $val ne 'IFDIFF' && $val ne 'IFHIGHER') {
			$val=$default;
		}
		$href->{$key}=$val;
	}
	return 1;
}

#########################
# Sub:  SetExtraSubDirs
# Desc: set extra subdirectories on a per-type basis
# Args: a directory string to set for all types, or hash ref to set some types and make all others the default
# Ret:  1
#########################
sub SetExtraSubDirs {
	my $self=shift;
	my ($type_lookup)=@_;
	return $self->_SetTypeHashValues($type_lookup,'TYPE_EXTRA_SUBDIR','');
}

#########################
# Sub:  SetFileCopyMethod
# Desc: set the file copy method
# Args: [[<file copy type1>,[<filename pattern1>,<pattern2>]],
#        [<file copy type2>,[<filename pattern3>,<pattern4>]]] or hash ref to set some types and make all others the default
# Ret:  1
#########################
sub SetFileCopyMethod {
	my $self=shift;
	my ($type_lookup)=@_;
	return $self->_SetTypeHashValues($type_lookup,'TYPE_FILE_COPY_METHOD',$self->GetBasicFileCopy());
}

#########################
# Sub:  SetFilePerms
# Desc: set files perms on a per-type basis
# Args: a permissions string to set for all types, or hash ref to set some types and make all others the default
# Ret:  1
#########################
sub SetFilePerms {
	my $self=shift;
	my ($type_lookup)=@_;
	return $self->_SetTypeHashValues($type_lookup,'TYPE_FILE_PERMS','');
}
	
#########################
# Sub:  GenFileNames
# Desc: Take a relative source filename, a source directory, a destination directory, and a file type
#       and generate the full source name, full destination name and relative destination name
#       the destination directory should already include the dest_subdir for this FileCopyParams
# Args: relative source filename,source file type,source dir,dest dir
# Ret:  (full source name, full dest name, rel dest name)
#########################
sub GenFileNames {
	my $self=shift;
	my ($fname,$file_type,$src_dir,$dst_dir)=@_;
	
	#source manifest's fname is a relative path
	my ($sdir,$f_basename)=&::GetFileParts($fname);
	$sdir=~s/\/$//;
	
	my $with_dirs=$self->GetPreserveDirs();
	
	#If we are preserving directories
	if ($with_dirs==1) {
		#And we are removing some number of directories from the source path
		my $strip_dirs=$self->GetStripDirs($file_type);
		if ($strip_dirs>0) {
			my @parts=&::SplitPathString($sdir);
			for(my $i=0;$i<$strip_dirs;$i++) {
				shift(@parts);
			}
			$sdir=&::ConcatPathString(@parts);
		}
	}
	
	#The full path to find the actual file in the patch
	my $real_src_path=&::ConcatPathString($src_dir,$fname);

	#dfname is the relative destination path
	my $dfname='';
	if ($with_dirs!=0) {
		if ($sdir eq '') {
			$dfname=$f_basename;
		} else {
			$dfname=&::ConcatPathString($sdir,$f_basename);
		}
	} else {
		$dfname=$f_basename;
	}
	
	#If this file type has an extra subdir, include it
	my $extra_subdir=$self->GetExtraSubdir($file_type);
	if ($extra_subdir ne '') {
		$dfname=&::ConcatPathString($extra_subdir,$dfname);
	}
	
	#real_dst_path is the full path to copy the file to 
	my $real_dst_path=&::ConcatPathString($dst_dir,$dfname);
	
	return ($real_src_path,$real_dst_path,$dfname);
}

sub GetBasicFileCopy {
	my $self=shift;
	return $self->{'BASIC_COPY'};
}

sub GetPreserveDirs {
	my $self=shift;
	return $self->{'PRESERVE_DIRS'};
}

sub GetStripDirs {
	my $self=shift;
	my ($type)=@_;
	return $self->_GetTypeHashValue($type,'TYPE_STRIP_DIRS');
}

sub GetFileTypes {
	my $self=shift;
	return $self->{'FILE_TYPES'};
}

sub GetDestSubDir {
	my $self=shift;
	return $self->{'DEST_SUBDIR'};
}

sub GetIgnorePatterns {
	my $self=shift;
	return $self->{'IGNORE_PATTERNS'};
}

sub GetExtraSubdir {
	my $self=shift;
	my ($type)=@_;
	return $self->_GetTypeHashValue($type,'TYPE_EXTRA_SUBDIR');
}
sub GetFilePerms {
	my $self=shift;
	my ($type)=@_;
	return $self->_GetTypeHashValue($type,'TYPE_FILE_PERMS');
}
sub GetUpdateCriteria {
	my $self=shift;
	my ($type)=@_;
	my $criteria=$self->_GetTypeHashValue($type,'TYPE_ALLOW_OVERWRITE');
	if ($criteria eq '') {
		$criteria='IFNEW';
	}
	return $criteria;
}

sub GetFileCopyMethod {
	my $self=shift;
	my ($type,$filename)=@_;
	
	my $basic=$self->GetBasicFileCopy();
	
	my $method_ref=$self->_GetTypeHashValue($type,'TYPE_FILE_COPY_METHOD');
	if ($method_ref eq '') {
		return $basic;
	}
	if (ref($method_ref) ne 'ARRAY') {
		$method_ref=[$method_ref];
	}
	
	foreach my $method (@$method_ref) {
		if ($method->DoesMethodApply($filename)==1) {
			return $method;
		}
	}
	return $basic;
}

sub PrepareFileCopyMethods {
	my $self=shift;
	my $hash_ref=$self->{'TYPE_FILE_COPY_METHOD'};
	foreach my $method (values(%$hash_ref)) {
		next if ($method eq '');
		next if ($method->GetIsPrepared()==1);
		
		if ($method->PrepareForCopies()!=1) {
			return 0;
		}
	}
	return 1;
}

sub AddFileTypes {
	my $self=shift;
	my (@types)=@_;
	push(@{$self->{'FILE_TYPES'}},@types);
}

sub _GetTypeHashValue {
	my $self=shift;
	my ($type,$hash_name)=@_;
	my $href=$self->{$hash_name};
	return $href->{$type};
}

sub _SetTypeHashValues {
	my $self=shift;
	my ($type_lookup,$hash_name,$def)=@_;
	my $href=$self->{$hash_name};
	
	if (ref($type_lookup) ne 'HASH') {
		$def=$type_lookup;
		$type_lookup={};
	}
	
	foreach my $type (@{$self->GetFileTypes()}) {
		my $val=$type_lookup->{$type};
		if ($val eq '') {
			$val=$def;
		}
		$href->{$type}=$val;
	}
	return 1;
}

#####################################################################
# Sub:  new
# Desc: A class encapsulating the parameters configuring how files are copied
# Args: file_type or ref to file types,dest_subdir,preserve_dirs
#####################################################################
sub new {
    my $class=shift;
	my ($file_types,$dest_subdir,$preserve_dirs)=@_;
    my $self={};
	
    bless($self,$class);
	
	if (!ref($file_types)) {
		$file_types=[$file_types];
	}
	
	my $strip_dirs=0;
	if ($preserve_dirs<0) {
		$strip_dirs=0-$preserve_dirs;
		$preserve_dirs=1;
	}
	
	if ($preserve_dirs!=1 && $preserve_dirs!=0) {
		die "Invalid setting for preserve dirs: $preserve_dirs, aborting\n";
	}
	
	$self->{'FILE_TYPES'}=$file_types;
	$self->{'DEST_SUBDIR'}=$dest_subdir;
	$self->{'PRESERVE_DIRS'}=$preserve_dirs;
	#$self->{'STRIP_DIRS'}=$strip_dirs;
	$self->{'IGNORE_PATTERNS'}=[];
	$self->{'BASIC_COPY'}=new FileCopyMethod([qr/.*/]);
	foreach my $t ('TYPE_ALLOW_OVERWRITE','TYPE_FILE_PERMS','TYPE_EXTRA_SUBDIR','TYPE_FILE_COPY_METHOD','TYPE_STRIP_DIRS') {
		$self->{$t}={};
	}
	$self->SetUpdateCriteria({});
	$self->SetExtraSubDirs({});
	$self->SetFilePerms({});
	$self->SetFileCopyMethod({});
	$self->SetStripDirs($strip_dirs);
	
    return $self;
}

1;
