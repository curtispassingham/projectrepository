############################################################################
# Copyright © 2014, Oracle and/or its affiliates. All rights reserved.
############################################################################
package FileCopyMethodSubst;
our @ISA = qw(FileCopyMethod);
use strict;

#########################
# Sub:  DoCopy
# Desc: Do the actual copy of a source file to a destination file
# Args: full path to source file, full path to dest file, ref to a scalar that receives error details
# Ret:  1 on success, 0 on failure
#########################
sub DoCopy {
	my $self=shift;
	my ($src_file,$dest_file,$error_ref)=@_;
	
	if ($self->GetIsPrepared()!=1) {
		if ($self->PrepareForCopies()!=1) {
			return 0;
		}
	}
	
	$$error_ref='';
	
	my $ret=0;
	unless (open(SOURCE,"<$src_file")) {
		$$error_ref="Unable to open $src_file: $!";
		return 0;
	}
	unless (open(DEST,">$dest_file")) {
		$$error_ref="Unable to open $dest_file: $!";
		close(SOURCE);
		return 0;
	}
	
	my $val_hash=$self->{'SUBST_HASH'};
	
	while(<SOURCE>) {
		my $line=$_;
		foreach my $key (keys(%$val_hash)) {
			if ($line=~/$key/) {
				my $val=$val_hash->{$key};
				$line=~s/$key/$val/g;
			}
		}
		unless(print DEST $line) {
			$$error_ref="Error while writing to $dest_file: $!";
			close(SOURCE);
			close(DEST);
			return 0;
		}
	}
	close(SOURCE);
	close(DEST);

	return 1;
}

#########################
# Sub:  PrepareForCopies
# Desc: Prepare this FileCopyMethod for copying
# Args: None
# Ret:  1 if preparations were successful, 0 if not
#########################
sub PrepareForCopies {
	my $self=shift;
	
	my $filename=$self->{'SUBST_PROPERTY_FILE'};
	if ($filename eq '') {
		#not constructed with a hash, and no property file specified, use an empty replace hash
		return $self->SetSubstHash({});
	}
	
	if (!-f $filename) {
		&::LogMsg("ERROR: Unable to locate substitution property file $filename");
		return 0;
	}
	
	&::LogMsg("Loading substution property file $filename");
	
	my $cfg_file=new ConfigFile();
	if ($cfg_file->LoadFile($filename)!=1) {
		&::LogMsg("ERROR: Unable to load substitution property file $filename");
		return 0;
	}
	my $subst_hash_ref={};
	foreach my $key ($cfg_file->Keys()) {
		$subst_hash_ref->{$key}=$cfg_file->Get($key);
	}
	
	return $self->SetSubstHash($subst_hash_ref);
}

#########################
# Sub:  SetSubstHash
# Desc: Configure this FileCopyMethod's substitution hash.  This method also marks the FileCopyMethod as 'prepared' for file copies
# Args: ref to hash of substitution keys/values
# Ret:  1
#########################
sub SetSubstHash {
	my $self=shift;
	my ($subst_hash_ref)=@_;

	my $trigger_start_char=$self->{'TRIGGER_START'};
	my $trigger_end_char=$self->{'TRIGGER_END'};
	
	my %final_hash=();
	foreach my $key (keys(%$subst_hash_ref)) {
		my $value=$subst_hash_ref->{$key};
		$key="${trigger_start_char}${key}${trigger_end_char}";
		$final_hash{$key}=$value;
	}
	
	$self->{'SUBST_HASH'}=\%final_hash;	
	$self->SetIsPrepared(1);
	return 1;
}

#########################
# Sub:  SetSubstPropertyFile
# Desc: Set the name of the property file this file copy method should use for substitution name/values
# Args: property_file - file name of the property file
# Ret:  1
#########################
sub SetSubstPropertyFile {
	my $self=shift;
	my ($property_file)=@_;
	$self->{'SUBST_PROPERTY_FILE'}=$property_file;
	$self->SetIsPrepared(0);
	return 1;
}

#####################################################################
# Sub:  new
# Desc: A class encapsulating a method for accomplishing file copies by substituting values
# Args: ref to a list of filename patterns this method applies to,filename of config file to read or ref to hash of name/value pairs,start/end character
#####################################################################
sub new {
    my $class=shift;
	my ($filename_pat_ref,$subst_hash_ref,$trigger_start_char,$trigger_end_char)=@_;
    
	my $self=$class->SUPER::new($filename_pat_ref);
	
	if ($trigger_start_char eq '') {
		$trigger_start_char='@';
	}
	if ($trigger_end_char eq '') {
		$trigger_end_char=$trigger_start_char;
	}
	
	$self->{'TRIGGER_START'}=$trigger_start_char;
	$self->{'TRIGGER_END'}=$trigger_end_char;
	
	if (ref($subst_hash_ref) eq 'HASH') {
		$self->SetSubstHash($subst_hash_ref);
	} else {
		$self->SetSubstPropertyFile($subst_hash_ref);
	}

	return $self;
}

1;