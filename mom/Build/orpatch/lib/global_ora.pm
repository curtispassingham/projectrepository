############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################

require "$ENV{RETAIL_HOME}/orpatch/lib/global.pm";
require "$ENV{RETAIL_HOME}/orpatch/lib/Oracle.pm";

1;