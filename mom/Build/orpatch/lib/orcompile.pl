#!/usr/bin/perl
############################################################################
# Copyright © 2013, Oracle and/or its affiliates. All rights reserved.
############################################################################
use strict;

($ENV{RETAIL_HOME} eq '') && (die "ERROR: RETAIL_HOME must be defined!");
$::ORP_DIR="$ENV{RETAIL_HOME}/orpatch";
$::ORP_LIB_DIR="$::ORP_DIR/lib";

require "$::ORP_LIB_DIR/orpatch_include.pl";

$::ORP_CONFIG=new ConfigFile();
$::ORP_GLOBAL_STATE='';

$::ORP_COMPILE_APP='';
$::ORP_COMPILE_TYPE='';

#Map of Application and Type to Action Name and Method
$::ORP_COMPILE_MAP={
     #App        #Type               #Action Name            #Method
     'RMS'       =>{
                    'BATCH'       =>['RMSBATCH',             'PostAction'],
                    'FORMS'       =>['ORAFORMS_RMS',         'PostAction'],
                    'DB'          =>['DBSQL_RMS',            'CompileInvalids'],
                    'DB-DEMO'     =>['DBSQL_RMSDEMO',        'CompileInvalids'],
                    'DB-BDIINT'   =>['DBSQL_RMSBDIINT',      'CompileInvalids'],
                   },
     'RWMS'      =>{
                    'FORMS'       =>['ORAFORMS_RWMS',        'PostAction'],
                    'DB'          =>['DBSQL_RWMS',           'CompileInvalids'],
                    'DB-ADF'      =>['DBSQL_RWMSADF',        'CompileInvalids'],
                   },
     'REIM'      =>{
                    'DB'          =>['DBSQL_RMS',            'CompileInvalids'],
                   },
     'ALLOC'     =>{
                    'DB-ALC'      =>['DBSQL_ALLOC',          'CompileInvalids'],
                    'DB-RMS'      =>['DBSQL_RMS',            'CompileInvalids'],
                   },
     'RME'       =>{
                    'DB'          =>['DBSQL_RASECORE',       'CompileInvalids'],
                   },
     'ASO'       =>{
                    'DB'          =>['DBSQL_RASECORE',       'CompileInvalids'],
                   },
     'RI'       =>{
                   'DB-DM'        =>['DBSQL_RADM',           'CompileInvalids'],
                   'DB-RIBATCH'   =>['DBSQL_RABATCH',        'CompileInvalids'],
                   'DB-RMSBATCH'  =>['DBSQL_RARMSBATCH',     'CompileInvalids'],
                   'DB-FEDM'      =>['DBSQL_RAFEDM',         'CompileInvalids'],
                  },
     'RDE'      =>{
                   'DB-DM'       =>['DBSQL_RDEDM',          'CompileInvalids'],
                   'DB-RDEBATCH' =>['DBSQL_RDEBATCH',       'CompileInvalids'],
                   'DB-RMSBATCH' =>['DBSQL_RDERMSBATCH',    'CompileInvalids'],
                  },
};

$::ORP_DEPLOY_MAP={
    #App       #Type             #Action Name     #Method
    'RPM'   =>{
               'JAVA'         =>['JAVAAPP_RPM',   'ORDeployWrapperWithCustom'],
               'JAVANOCUSTOM' =>['JAVAAPP_RPM',   'ORDeployWrapperNoCustom'],
              },
    'REIM'  =>{
               'JAVA'         =>['JAVAAPP_REIM',  'ORDeployWrapperWithCustom'],
               'JAVANOCUSTOM' =>['JAVAAPP_REIM',  'ORDeployWrapperNoCustom'],
              },
    'ALLOC' =>{
               'JAVA'         =>['JAVAAPP_ALLOC', 'ORDeployWrapperWithCustom'],
               'JAVANOCUSTOM' =>['JAVAAPP_ALLOC', 'ORDeployWrapperNoCustom'],
              },
    'RESA'  =>{
               'JAVA'         =>['JAVAAPP_RESA',  'ORDeployWrapperWithCustom'],
               'JAVANOCUSTOM' =>['JAVAAPP_RESA',  'ORDeployWrapperNoCustom'],
              },
    'RMS'   =>{
               'JAVA'         =>['JAVAAPP_RMS',   'ORDeployWrapperWithCustom'],
               'JAVANOCUSTOM' =>['JAVAAPP_RMS',   'ORDeployWrapperNoCustom'],
              },
    'RASRM' =>{
               'JAVA'         =>['JAVAAPP_RASRM', 'ORDeployWrapperWithCustom'],
               'JAVANOCUSTOM' =>['JAVAAPP_RASRM', 'ORDeployWrapperNoCustom'],
              },
};

$::ORP_COMPILE_MODE='COMPILE';
$::ORP_MY_NAME='orcompile';
$::ORP_ACTIVE_MAP=$::ORP_COMPILE_MAP;

&ValidateConfig();

@::ACTIONS=();
&::PopulateActions(\@::ACTIONS,$::ORP_CONFIG);

my $exit_status=&DoCompile($::ORP_COMPILE_APP,$::ORP_COMPILE_TYPE,\@::ACTIONS);

&::ExitORCompile($exit_status);

sub DoCompile {
    my ($app,$type,$action_list_ref)=@_;

    my $what_verb='Compiling';
    my $what_noun='compile';
    if ($::ORP_COMPILE_MODE ne 'COMPILE') {
        $what_verb='Deploying';
        $what_noun='deploy';
    }

    my ($app_type_ref,$ret_val)=&::GetAppTypeRef($app,$type);
    if (!defined($app_type_ref)) {
        &::LogMsg("Unable to locate $what_noun function for $app:$type");
        return 2;
    }

    &::LogMsg("$what_verb $type for application $app");

    my ($action_name,$method)=@$app_type_ref;

    my $action=&::FindHandleForAction($action_name,$action_list_ref);
    if (!defined($action)) {
        &::LogMsg("Unable to find action: $action_name!");
        return 3;
    }

    if ($action->IsValidOnThisHost()!=1) {
        &::LogMsg("Unable to proceed, action $action_name is not valid on this host");
        return 4;
    }

    #register our global state with all actions
    foreach my $act (@$action_list_ref) {
        $act->SetORPatchGlobalState($::ORP_GLOBAL_STATE);
    }

    #Save our unfiltered list of actions with the global state
    $::ORP_GLOBAL_STATE->SetActionList(@$action_list_ref);

    unless (&RunPreChecks($action)==1) {
        &::LogMsg("$::ORP_MY_NAME aborting due to failed pre-patch checks");
        return 5;
    }

    #Everything looks in order, call the method
    if ($action->$method()!=1) {
        &::LogMsg("Failure in $what_noun method!");
        return 6;
    }

    #We're good
    return 0;
}

#########################
# Sub:  RunPreChecks
# Desc: Run the precheck for each action on this host
# Ret:  1 if all are successful, 0 if not
#########################
sub RunPreChecks {
    my @host_actions=@_;

    #Run full checks
    my %checks_run=();
    foreach my $action (@host_actions) {
        my $name=$action->GetActionName();
        if ($action->PrePatchCheck(1)!=1) {
            &::LogMsg("Pre-patch check for action $name failed!");
            return 0;
        }
        $checks_run{$name}=1;
    }

    #Get the list of other action checks that things we run are going to need
    my $check_ref=$::ORP_GLOBAL_STATE->GetExtraActionsForPreCheck();
    unless(defined($check_ref)) {
        return 0;
    }

    &::LogMsg("Checking dependent configuration, if any");

    #Run config-only pre-checks for any actions that haven't already been run
    foreach my $action (@$check_ref) {
        my $name=$action->GetActionName();
        if (exists($checks_run{$name})) {
            #Full check has already been run, no need to run a config-only pre-check
            next;
        } else {
            if ($action->PrePatchCheck(0)!=1) {
                &::LogMsg("Extra pre-patch config check for action $name failed!");
                return 0;
            }
        }
    }

    return 1;
}

#########################
# Sub:  FindHandleForAction
# Desc: Find an action handle for a specific action name
#########################
sub FindHandleForAction {
    my ($action_name,$action_list_ref)=@_;

    foreach my $action (@$action_list_ref) {
        my $this_name=$action->GetActionName();
        if ($this_name eq $action_name) {
            return $action;
        }
    }

    #Unknown action name
    return undef;
}

#########################
# Sub:  ValidateConfig
# Desc: parse our command-line arguments and validate our configuration
#########################
sub ValidateConfig {
    my $show_help=0;
    while(my $arg=shift(@ARGV)) {
        $arg=~tr/a-z/A-Z/;
        if ($arg eq '-A') {
            $::ORP_COMPILE_APP=shift(@ARGV);
        } elsif ($arg eq '-T') {
            $::ORP_COMPILE_TYPE=shift(@ARGV);
        } elsif ($arg eq '-HELP') {
            $show_help=1;
        } elsif ($arg eq '-DEPLOY') {
            $::ORP_COMPILE_MODE='DEPLOY';
            $::ORP_MY_NAME='ordeploy';
            $::ORP_ACTIVE_MAP=$::ORP_DEPLOY_MAP;
            my $log_file=$::LOG->GetLogFile();
            $log_file=~s/orcompile/ordeploy/;
            $::LOG=new Log($log_file,0,'',0);
        } else {
            &::LogMsg("Unknown argument: $arg");
            $show_help=1;
        }
    }

    my $restart_file="$::ORP_DIR/logs/${main::ORP_MY_NAME}_restart.state";
    $::ORP_GLOBAL_STATE=new ORPatchGlobalState($::ORP_DIR,$restart_file,$::ORP_CONFIG,$ENV{RETAIL_HOME},'RETAIL_HOME');

    unless ($::ORP_GLOBAL_STATE->ValidatePatchlessConfig()==1) {
        exit 1;
    }

    my $env_info_cfg="$::ORP_DIR/config/env_info.cfg";
    unless (-f $env_info_cfg) {
        &::LogMsg("Environment info file $env_info_cfg not found");
        exit 1;
    }

    if ($::ORP_CONFIG->LoadFile($env_info_cfg)!=1) {
        &::LogMsg("Unable to read env_info.cfg!");
        exit 1;
    }

    &FilterProductList($::ORP_CONFIG);

    if ($show_help==1) {
        &::Usage();
    }

    $::ORP_COMPILE_APP=~tr/a-z/A-Z/;
    if ($::ORP_COMPILE_APP eq '') {
        &::LogMsg("Required argument -a missing");
        &::Usage();
    }

    $::ORP_COMPILE_TYPE=~tr/a-z/A-Z/;
    if ($::ORP_COMPILE_TYPE eq '') {
        &::LogMsg("Required argument -t missing");
        &::Usage();
    }

    my ($app_type_ref,$ret_val)=&GetAppTypeRef($::ORP_COMPILE_APP,$::ORP_COMPILE_TYPE);
    if ($ret_val==0) {
        &::LogMsg("Unknown app: $::ORP_COMPILE_APP");
        &::Usage();
    } elsif ($ret_val==-1) {
        &::LogMsg("Unknown type: $::ORP_COMPILE_TYPE for app $::ORP_COMPILE_APP");
        &::Usage();
    }

    my $nls_lang=$::ORP_CONFIG->Get('NLS_LANG');
    if ($nls_lang ne '') {
        &::LogMsg("Setting NLS_LANG = $nls_lang");
        $ENV{NLS_LANG}=$nls_lang;
    }

    my $patch_mode=$::ORP_GLOBAL_STATE->GetPatchMode();
    $::ORP_CONFIG->Set('PATCH_MODE',$patch_mode);

    return 1;
}

sub GetAppTypeRef {
    my ($app,$type)=@_;

    my $ret=1;
    my $app_type_ref=undef;

    my $app_hash=$::ORP_ACTIVE_MAP->{$app};
    if ($app_hash eq '' || !ref($app_hash)) {
        $ret=0;
    } else {
        my $type_ref=$app_hash->{$type};
        if ($type_ref eq '' || !ref($type_ref)) {
            $ret=-1;
        } else {
            $app_type_ref=$type_ref;
        }
    }

    return ($app_type_ref,$ret);
}

sub FilterProductList {
    my ($config_ref)=@_;

    my @products=keys(%$::ORP_ACTIVE_MAP);
    foreach my $product (@products) {
        my $product_name=$product;
        $product_name=~tr/a-z/A-Z/;
        if ($config_ref->ValidateTFVar("PRODUCT_${product_name}",0)!=1) {
            #Product not installed in this home, remove it from the map
            delete($::ORP_ACTIVE_MAP->{$product});
        }
    }

    return 1;
}


sub ExitORCompile {
    my ($exit_status)=@_;

    #If we are exiting successfully, remove the restart state
    #if ($exit_status==0) {
        #We always remove the restart file as we never load it
        $::ORP_GLOBAL_STATE->ResetRestartState();
    #}
    &::LogMsg("$::ORP_MY_NAME session complete");
    exit $exit_status;
}

sub Usage {
    &::LogMsg("Usage:");
    &::LogMsg("   $::ORP_MY_NAME -a <app> -t <type>");
    &::LogMsg("");
    &::LogMsg("Potential Apps and Types:");

    foreach my $app (sort(keys(%$::ORP_ACTIVE_MAP))) {
        my $app_hash=$::ORP_ACTIVE_MAP->{$app};
        my @type_list=sort(keys(%$app_hash));
        &::LogMsg(sprintf("   %-8s => %s",$app,join(',',@type_list)));
    }
    exit 1;
}
