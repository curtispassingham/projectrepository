########################################################################
# Imported modules
########################################################################

import sys
import os

########################################################################
# wlsCreateOrUpdateCredentials class
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################

#Reading the arguments passed
args=[sys.argv[0]]
for line in sys.stdin:
   args.append(line.rstrip())

adminUser=args[1]   
adminPassword=args[2]  
adminUrl=args[3]   
user_value=args[4]  
password_value=args[5]   
key_value=args[6]  
map_value=args[7]   

#connecting to weblogic domain
connect(adminUser,adminPassword,adminUrl) 
  
print 'Storing credentials for user:' +user_value

try:
	print 'Trying to create new credential'
	print 'A JPS-01007 error is normal if the credential already exists'
	createCred(map=map_value, key=key_value, user=user_value, password=password_value)
except MBeanException:
	print 'Updating existing credential'
	updateCred(map=map_value, key=key_value, user=user_value, password=password_value)
  
print 'Successfully stored credentials for user:' +user_value 
