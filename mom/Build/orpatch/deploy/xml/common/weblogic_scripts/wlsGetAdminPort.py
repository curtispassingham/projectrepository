########################################################################
# Imported modules
########################################################################

import sys

import java.lang.String as String
import java.lang.Integer as Integer
import java.io.File as File

from xml.dom import minidom

########################################################################
# MAIN SCRIPT
########################################################################

# Method to get the admin server port
def getAdminServerPort(domainHome, SSL):
  filename = domainHome+"/config/config.xml"

  doc = minidom.parse(open(filename, "r"))
  servers = doc.getElementsByTagName('server')

  # set defaults in case of an error     
  enabled = "false"
  found = "false"
  listenport = 7001

  for servernode in servers:
    namenodes = servernode.getElementsByTagName('name')[0].childNodes
    for namenode in namenodes:
      if namenode.nodeType == namenode.TEXT_NODE and namenode.data == 'AdminServer':
        found = "false"
        if SSL == "true":
          sslnode = servernode.getElementsByTagName('ssl')[0]

          # Is the port enabled?
          enablenodes = sslnode.getElementsByTagName('enabled')[0].childNodes
          for enablenode in enablenodes:
            if enablenode.data == "false" and enablenode.nodeType == enablenode.TEXT_NODE:
              enabled = "false"
              found = "true"
              print "Error: the SSL port for the AdminServer is unexpectedly disabled.  The SSL port will be used anyway.  However, it must be enabled in the WebLogic admin console for the installer and application to function properly."
            elif enablenode.data == "true" and enablenode.nodeType == enablenode.TEXT_NODE:
              enabled = "true"
              found = "true"
              print "The SSL port for the AdminServer is enabled as expected."

          # if the ssl xml tags were missing, then assumed enabled
          if found == "false":
            enabled = "true"
            print "The SSL port for the AdminServer is enabled as expected."
 
          # Get the listen port value
          listenportnodes = sslnode.getElementsByTagName('listen-port')[0].childNodes
          for listenportnode in listenportnodes:
            if listenportnode.nodeType == listenportnode.TEXT_NODE:
              listenport = listenportnode.data
              print "Using SSL port "+listenportnode.data+" for the AdminServer."
        else:
          found = "false"
          for node in servernode.childNodes:
 
            # Is the port enabled?
            if node.nodeName == "listen-port-enabled":
              for enablenode in node.childNodes:
                if enablenode.data == "false" and enablenode.nodeType == enablenode.TEXT_NODE:
                  enabled = "false"
                  found = "true"
                  print "Error: the non-SSL port for the AdminServer is unexpectedly disabled.  The non-SSL port will be used anyway.  However, it must be enabled in the WebLogic admin console for the installer and application to function properly."
                elif enablenode.data == "true" and enablenode.nodeType == enablenode.TEXT_NODE:
                  enabled = "true"
                  found = "true"
                  print "The non-SSL port for the AdminServer is enabled as expected."

            # Get the listen port value
            if node.nodeName == "listen-port":
              for listenportnode in node.childNodes:
                if listenportnode.nodeType == listenportnode.TEXT_NODE:
                  listenport = listenportnode.data
                  print "Using non-SSL port "+listenportnode.data+" for the AdminServer."

          # if the enabled xml tags were missing, then assumed enabled
          if found == "false":
            enabled = "true"
            print "The non-SSL port for the AdminServer is enabled as expected."

  # Write port to adminport.txt    
  portstring = Integer.toString(listenport)
  output = open('/tmp/adminport.txt','w')
  output.write('temp.admin.port.number='+portstring+'\n')
  output.close()

  if enabled == "true":
    return 0
  else:
    return 1

def usage():
  print "Usage: wlsGetAdminPort.py <domain dir> <ssl>"
  exit(exitcode=1)

args=[sys.argv[0]]
for line in sys.stdin:
   args.append(line.rstrip())

# Get the command-line arguments
if len(args) < 2:
  usage()

print "Executing command: getAdminServerPort"
retval = getAdminServerPort(args[1], args[2])

# Exit cleanly
exitvalue = Integer.toString(retval)
print 'Command getAdminServerPort exited with code '+exitvalue+'.'
exit(exitcode=retval)
