########################################################################
# Imported modules
########################################################################

import sys

import wl

########################################################################
# SecureDatasourceCommands class
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################
class SecureDatasourceCommands:

   def __init__(self):
      """Initialize a WebLogic MBean connection."""

   # print an error message
   def printError(self):
      print '********************'
      print '* Unexpected error *'
      print '********************'
      wl.dumpStack()
      print '*********************************************************'
      print '* Please view the weblogic server logs for more details *'
      print '*********************************************************'

   def createDatasource(self, dsName, serverTarget, jndiName, databaseUrl, databaseUser, databasePassword, databaseClassName, wrapDataTypes, transactionOption, keyStoreType, keyStorelocation, keyStorePassword, trustStoreType, trustStorelocation, trustStorePassword):
      # If datasource does not exist, create it
      dsExists = "false"
      sources = wl.cmo.getJDBCSystemResources()
      for tmpDS in sources:
        if tmpDS.getName() == dsName:   
          dsExists = "true"
          break
 
      if dsExists == "true":  
        print 'Datasource '+dsName+'exists, it will be deleted and re-created...'
        self.deleteDatasource(dsName, serverTarget)

      print 'Creating data source with name '+dsName

      wl.edit()
      wl.startEdit(600000,600000,"true")
     
      try: 
        jdbcSR = wl.create(dsName,"JDBCSystemResource")
        theJDBCResource = jdbcSR.getJDBCResource()
        theJDBCResource.setName(dsName)
        connectionPoolParams = theJDBCResource.getJDBCConnectionPoolParams()
        connectionPoolParams.setConnectionReserveTimeoutSeconds(30)
        connectionPoolParams.setMaxCapacity(100)
        connectionPoolParams.setTestTableName("SQL SELECT 1 FROM DUAL")
        connectionPoolParams.setTestConnectionsOnReserve(wl.true)

        if wrapDataTypes == "false":
          print "Setting Wrap Data Types [false]"
          connectionPoolParams.setWrapTypes(0)
        elif wrapDataTypes == "true":
          print "Setting Wrap Data Types [true]"
          connectionPoolParams.setWrapTypes(1)

        dsParams = theJDBCResource.getJDBCDataSourceParams()

        print "Setting Transaction Option [" + transactionOption + "]"
        dsParams.setGlobalTransactionsProtocol(transactionOption)
     
        # Only add jndiName if it does not exist
        jndiNames=dsParams.getJNDINames()
        jndiExists="false"
        for jndi in jndiNames:
          if jndi == jndiName:
            jndiExists="true"
            break
     
        if jndiExists == "false":
          dsParams.addJNDIName(jndiName)
     
        driverParams = theJDBCResource.getJDBCDriverParams()
        driverParams.setUrl(databaseUrl)
        driverParams.setDriverName(databaseClassName)
        driverParams.setPassword(databasePassword)
        driverProperties = driverParams.getProperties()
       
        proper1 = driverProperties.createProperty("javax.net.ssl.keyStoreType")
        proper1.setValue(keyStoreType)
        proper2 = driverProperties.createProperty("javax.net.ssl.keyStore")
        proper2.setValue(keyStorelocation)
        proper3 = driverProperties.createProperty("javax.net.ssl.keyStorePassword")
        proper3.setEncryptedValue(keyStorePassword)
        proper4 = driverProperties.createProperty("javax.net.ssl.trustStoreType")
        proper4.setValue(trustStoreType)
        proper5 = driverProperties.createProperty("javax.net.ssl.trustStore")
        proper5.setValue(trustStorelocation)
        proper6 = driverProperties.createProperty("javax.net.ssl.trustStorePassword")
        proper6.setEncryptedValue(trustStorePassword)

        properties = driverProperties.getProperties()
        userPropertyExists="false"
        for property in properties:
          propertyName=property.getName()
          if propertyName == "user":
            userPropertyExists="true"
            proper=property
            break
     
        # Only create user property if it does not exist, other an error would be thrown
        if userPropertyExists == "false":
          proper = driverProperties.createProperty("user")
     
        proper.setValue(databaseUser)
        # This can be a server or a cluster.  Try server first, then cluster.  Otherwise an error will occur.
        target=wl.getMBean("/Servers/"+serverTarget)
        if target == None:
          target=wl.getMBean("/Clusters/"+serverTarget)
          if target == None:
            self.printError()
            print 'No server or cluster with name '+serverTarget+' was found, rolling back changes.'
            wl.cancelEdit('y')
            return 0
        jdbcSR.addTarget(target)
      except:
        self.printError()
        print 'Could not configure data source '+dsName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      try:
        wl.save()
      except:
        self.printError()
        print 'Could not save the addition of data source '+dsName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      try:
        wl.activate(600000,block="true")
        print 'Done configuring the data source '+dsName
      except:
        self.printError()
        print 'Could not activate datasource creation for '+dsName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      return 0

   def deleteDatasource(self, dsName, serverTarget):
      # get JDBC information
      deleteds = "false"
      sources = wl.cmo.getJDBCSystemResources()
      for jdbcSR in sources:
        if jdbcSR.getName() == dsName:
          deleteds = "true"
          wl.edit()
          wl.startEdit(600000,600000,"true")

          try: 
            print 'Deleting data source with name '+dsName
            wl.delete(dsName,"JDBCSystemResource")
          except:
            self.printError()
            print 'Could not remove data source '+dsName+', rolling back changes.'
            wl.cancelEdit('y')
            return 1
  
          try:
            wl.save()
          except:
            self.printError()
            print 'Could not save data source '+dsName+' deletion, rolling back changes.'
            wl.cancelEdit('y')
            return 1
            
          try:
            wl.activate(600000,block="true")
            print 'Done removing the data source '+dsName
            return 0 
          except:
            self.printError()
            print 'Could not activate data source '+dsName+' deletion, rolling back changes.'
            wl.cancelEdit('y')
            return 1

      if deleteds == "false":
        print 'Could not delete datasource ' +dsName+', because it does not exist.'
        return 0
