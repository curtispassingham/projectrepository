########################################################################
# Imported modules
########################################################################

import sys

import java

import wl

import wlsServerCommands

########################################################################
# Example class
# This class serves as a template for future classes of weblogic commands.
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################
class DeployCommands:

   def __init__(self):
      """Initialize a WebLogic MBean connection."""

   def printError(self):
      print '********************'
      print '* Unexpected error *'
      print '********************'
      wl.dumpStack()
      print '*********************************************************'
      print '* Please view the weblogic server logs for more details *'
      print '*********************************************************'

   def deployArchive(self, archiveName, applicationName, serverTarget, stagingMode, isLibrary):
      server = wlsServerCommands.ServerCommands()
      type = server.checkServerOrClusterExists(serverTarget)
      if type == None:
        print "Error: server or cluster " + serverTarget + " must exist in order to deploy this application."
        return 1

      try:
        listOfApps = wl.cmo.getAppDeployments()
        if listOfApps is not None:
          for app in listOfApps:
            if app.getName() == applicationName:
              for target in app.getTargets():
                if(target.getName() == serverTarget):            
                  print "Undeploying the archive ..."
                  undeployProgress = wl.undeploy(appName=applicationName,targets=serverTarget, timeout=600000, libraryModule=isLibrary)
                  print "Done Undeploying the archive ..."
                  undeployProgress.printStatus()

        # Get the timestamp of the .ear file to show when was it last modified
        f = java.io.File(archiveName)
        dt = java.util.Date(f.lastModified())
        print "The "+archiveName+" file was last modified on "+dt.toString()

        print "Deploying the archive ..."
        progress = wl.deploy(appName=applicationName, path=archiveName, upload="true", libraryModule=isLibrary, targets=serverTarget, stageMode=stagingMode, timeout=600000)
        progress.printStatus()
        print "Finished Deploying the ear ..."
      except:
        self.printError()
        return 1

      return 0

   def redeployArchive(self, archiveName, applicationName, serverTarget, isLibrary):
      server = wlsServerCommands.ServerCommands()
      type = server.checkServerOrClusterExists(serverTarget)
      if type == None:
        print "Error: server or cluster " + serverTarget + " must exist in order to redeploy this application."
        return 1

      try:
        # Get the timestamp of the .ear file to show when was it last modified
        f = java.io.File(archiveName)
        dt = java.util.Date(f.lastModified())
        print "The "+archiveName+" file was last modified on "+dt.toString()

        print "Redeploying the archive ..."
        progress = wl.redeploy(appName=applicationName, appPath=archiveName, upload="true", libraryModule=isLibrary, targets=serverTarget, timeout=600000)
        progress.printStatus()
        print "Finished redeploying the ear ..."
      except:
        self.printError()
        return 1

      return 0


   def getAbsolutePath(self, applicationName):
      try:
        listOfApps = wl.cmo.getAppDeployments()
        if listOfApps is not None:
          for app in listOfApps:
            if app.getName() == applicationName:
              appSourcePath = app.getAbsoluteSourcePath()
              print "The appSourcePath is "+appSourcePath+""
              output = open('/tmp/absolutepath.txt','w')
              output.write('temp.appSourcePath='+appSourcePath)
              output.close()                                

      except:
        self.printError()
        return 1

      return 0      
      
   def applyPolicy(self, applicationName, serverTarget, appplanPath):
      server = wlsServerCommands.ServerCommands()
      type = server.checkServerOrClusterExists(serverTarget)
      
      if type == None:
        print "Error: server or cluster " + serverTarget + " must exist in order to redeploy this application."
        return 1

      try:
        listOfApps = wl.cmo.getAppDeployments()
        if listOfApps is not None:
          for app in listOfApps:
            if app.getName() == applicationName:
              appSourcePath = app.getAbsoluteSourcePath()
      # Get the timestamp of the .ear file to show when was it last modified
        f = java.io.File(appSourcePath)
        dt = java.util.Date(f.lastModified())
        print "The "+appSourcePath+" file was last modified on "+dt.toString()

        print "Redeploying the archive ..."
        progress = wl.redeploy(appName=applicationName, appPath=appSourcePath, upload="true", targets=serverTarget, timeout=600000, planPath=appplanPath)
        progress.printStatus()
        print "Finished redeploying the ear ..."
      except:
        self.printError()
        return 1

      return 0      
      
   def undeployArchive(self, applicationName, serverTarget, isLibrary):
      server = wlsServerCommands.ServerCommands()
      type = server.checkServerOrClusterExists(serverTarget)
      if type == None:
        print "Error: server or cluster " + serverTarget + " must exist in order to undeploy this application."
        return 1

      try:
        listOfApps = wl.cmo.getAppDeployments()
        if listOfApps is not None:
          for app in listOfApps:
            if app.getName() == applicationName:
              for target in app.getTargets():
                if(target.getName() == serverTarget):            
                  print "Undeploying the archive ..."
                  undeployProgress = wl.undeploy(appName=applicationName,targets=serverTarget, libraryModule=isLibrary, timeout=600000)
                  print "Done Undeploying the archive ..."
                  undeployProgress.printStatus()
                  return 0
        print "Application "+applicationName+" was not found for server target "+serverTarget+".  Nothing to undeploy."
        return 1
      except:
        self.printError()
        return 1
