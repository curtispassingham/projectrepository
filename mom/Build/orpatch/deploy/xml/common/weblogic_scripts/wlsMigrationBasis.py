########################################################################
# Imported modules
########################################################################

import sys

import wl

########################################################################
# MigrationBasisCommands class
# Exit codes: 0=success, 1=unexpected failure, 2=invalid syntax
########################################################################
class MigrationBasisCommands:

   def __init__(self):
      """Initialize a WebLogic MBean connection."""

   # print an error message
   def printError(self):
      print '********************'
      print '* Unexpected error *'
      print '********************'
      wl.dumpStack()
      print '*********************************************************'
      print '* Please view the weblogic server logs for more details *'
      print '*********************************************************'

   def SetMigrationBasis(self, serverTarget, dsName):

      print 'Configuring migration basis to database with data source name '+dsName

      wl.edit()
      wl.startEdit(600000,600000,"true")
     
      try: 
        target=wl.getMBean("/Clusters/"+serverTarget)
        if target == None:
          self.printError()
          print 'No server or cluster with name '+serverTarget+' was found, rolling back changes.'
          wl.cancelEdit('y')
          return 0
        DatasourceName=wl.getMBean("/SystemResources/"+dsName)
        if DatasourceName == None:
          self.printError()
          print 'No datasource with name '+dsName+' was found, rolling back changes.'
          wl.cancelEdit('y')
          return 0
        target.setMigrationBasis('consensus')
        #target.setDataSourceForAutomaticMigration(DatasourceName)
      except:
        self.printError()
        print 'Could not configure data source '+dsName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      try:
        wl.save()
      except:
        self.printError()
        print 'Could not save the addition of data source '+dsName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      try:
        wl.activate(600000,block="true")
        print 'Done configuring the data source '+dsName
      except:
        self.printError()
        print 'Could not activate datasource creation for '+dsName+', rolling back changes.'
        wl.cancelEdit('y')
        return 1

      return 0