#
# dump_credentials.sh  - script to list out all the credentials from a secure 
#		  		 		credential wallet at a specified location. 
#    
echo " "
echo "============================================="
echo 	"Retail Public Security API Utility"
echo "============================================="

if [ -z "$JAVA_HOME" ]; then
  echo "Error: The JAVA_HOME environment variable is not defined"
  echo "This environment variable is needed to run this program"
  exit 1
fi


export CLASSPATH=../lib/*
exec "$JAVA_HOME/bin/java" com.oracle.retail.integration.common.security.credential.CredentialStoreReader $*
