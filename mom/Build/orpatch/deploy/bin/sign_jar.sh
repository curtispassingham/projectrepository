#!/bin/ksh
# Copyright © 2014 Oracle and/or its affiliates. All rights reserved.

if [ -z "$RETAIL_HOME" ]; then
	if [ ! -z "$MMHOME" ]; then
		RETAIL_HOME=$MMHOME
		export RETAIL_HOME
	else
		echo "RETAIL_HOME environment variable must be defined"
		exit 1
	fi
fi

function usage
{
   echo "Usage: "
   echo "sign_jar.sh signjar <jar_file> <alias>"
   echo "  or"
   echo "sign_jar.sh changepwd"
   echo " or"
   echo "sign_jar.sh checkalias <alias>"
   exit 1
}

function kill_child
{
   if [ ! -z "$CHILD_PID" ]; then
      kill -TERM $CHILD_PID
   fi
   exit 1
}

trap 'kill_child' TERM INT

#First argument is the action
ACTION=$1

if [ "$ACTION" = "signjar" ]; then
	ANT_TARGET="signjar"
elif [ "$ACTION" = "changepwd" ]; then
   ANT_TARGET="changepwd"
elif [ "$ACTION" = "checkalias" ]; then
   ANT_TARGET="checkalias"
else
   echo "ERROR: Unknown action phase: $ACTION"
   usage
fi

FULL_SIGNING_XML=$RETAIL_HOME/orpatch/deploy/xml/common/jarsign.xml

if [ ! -f "$FULL_SIGNING_XML" ]; then
   echo "ERROR: Specified signing xml $FULL_SIGNING_XML does not exist"
   usage
fi

if [ "$ANT_TARGET" = "signjar" ]; then
	#Second argument is the jar file
	JAR_FILE=$2

	if [ -z "$JAR_FILE" ]; then
	   echo "ERROR: Required argument jar_file missing"
	   usage
	fi

	if [ ! -f "$JAR_FILE" ]; then
	   echo "ERROR: Specified jar_file $JAR_FILE does not exist"
	   usage
	fi

	#Third argument is the alias
	SIGN_ALIAS=$3

	if [ -z "$SIGN_ALIAS" ]; then
	   echo "ERROR: Required argument sign_alias missing"
	   usage
	fi
fi

if [ "$ANT_TARGET" = "checkalias" ]; then
	#Second argument is the sign alias
	SIGN_ALIAS=$2
	
	if [ -z "$SIGN_ALIAS" ]; then
	   echo "ERROR: Required argument sign_alias missing"
	   usage
	fi
fi

#Setup ant execution environment
if [ -z "$JAVA_HOME" ]; then
   echo "JAVA_HOME must be defined"
   exit 1
fi

PATH=$JAVA_HOME/bin:$PATH
export PATH

ORPDEPLOY_HOME=$RETAIL_HOME/orpatch/deploy
ANT_HOME=$ORPDEPLOY_HOME/ant
export ANT_HOME
ANT_EXT_HOME=$ORPDEPLOY_HOME/ant-ext
RPS_LIB_HOME=$ORPDEPLOY_HOME/retail-public-security-api/lib

# Ant jars
CLASSPATH=$ANT_EXT_HOME/ant-contrib.jar

ANT_LIB_JARS=`ls $ANT_EXT_HOME/*.jar $RPS_LIB_HOME/*.jar 2>/dev/null`
for j in  $ANT_LIB_JARS; do
    CLASSPATH=$CLASSPATH:$j
done
export CLASSPATH

if [ ! -x "$ANT_HOME/bin/ant" ]; then
   echo "ERROR: $ANT_HOME/bin/ant is not executable"
   exit 1
fi

if [ "$ANT_TARGET" = "signjar" -o "$ANT_TARGET" = "checkalias" ]; then
	$ANT_HOME/bin/ant -f "$FULL_SIGNING_XML" -Dinput.retail.home=$RETAIL_HOME -Dinput.jar.file=$JAR_FILE -Dinput.signing.alias=$SIGN_ALIAS $ANT_TARGET &
	CHILD_PID=$!
	wait $CHILD_PID
	RET=$?
else
	$ANT_HOME/bin/ant -f "$FULL_SIGNING_XML" -Dinput.retail.home=$RETAIL_HOME $ANT_TARGET
	RET=$?
fi

exit $RET

