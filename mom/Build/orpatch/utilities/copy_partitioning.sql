declare

   L_partition_name varchar2(30);
   L_partitioning_type VARCHAR2(30);
   L_subpartitioning_type VARCHAR2(30);
   L_part_key VARCHAR2(30);
   L_subpart_key VARCHAR2(30);
   L_part_ddl dbms_sql.varchar2s;  --Table of varchar2(256)
   L_part_ddl_counter number := 1;
   sql_cursor   integer;
   retval       integer;

   cursor c_part_type is
   select partitioning_type, subpartitioning_type
   from user_part_tables
   where table_name=UPPER('&2');

   cursor c_part_key is
   select column_name
   from user_part_key_columns
   where name=UPPER('&2');

   cursor c_subpart_key is
   select column_name
   from user_subpart_key_columns
   where name=UPPER('&2');

   cursor c_part is
   select partition_name,
          high_value,
          decode(pct_free, NULL, NULL, ' PCTFREE '||pct_free) pct_free,
          decode(pct_used, NULL, NULL, ' PCTUSED '||pct_used) pct_used,
          decode(ini_trans, NULL, NULL, ' INITRANS '||ini_trans) ini_trans,
          decode(max_trans, NULL, NULL, ' MAXTRANS '||max_trans) max_trans,
          decode(logging, 'YES',' LOGGING','NO',' NOLOGGING',NULL) logging,
          decode(compression, 'ENABLED',' COMPRESS','DISBALED',' NOCOMPRESS',NULL) compression,
          decode(initial_extent, NULL, NULL, ' INITIAL '||initial_extent) initial_extent,
          decode(next_extent, NULL, NULL, ' NEXT '||next_extent) next_extent,
          decode(min_extent, NULL, NULL, ' MINEXTENTS '||min_extent) min_extent,
          decode(max_extent, NULL, NULL, ' MAXEXTENTS '||max_extent) max_extent,
          decode(pct_increase, NULL, NULL, ' PCTINCREASE '||pct_increase) pct_increase,
          decode(freelists, NULL, NULL, ' FREELISTS '||freelists) freelists,
          decode(freelist_groups, NULL, NULL, ' FREELIST GROUPS '||freelist_groups) freelist_groups,
          decode(buffer_pool, NULL, NULL, ' BUFFER_POOL '||buffer_pool) buffer_pool,
          decode(tablespace_name, NULL, NULL, ' TABLESPACE '||tablespace_name) tablespace_name,
          decode(partition_position, 1, '', ',') comma
     from user_tab_partitions
    where table_name=UPPER('&2')
    order by partition_position;

   cursor c_subpart is
   select subpartition_name,
          high_value,
          decode(tablespace_name, NULL, NULL, ' TABLESPACE "'||tablespace_name||'"') tablespace_name,
          decode(subpartition_position, 1, '', ',') comma
     from user_tab_subpartitions
    where table_name=UPPER('&2')
      and partition_name = L_partition_name
    order by subpartition_position;

   procedure output_line (I_line IN varchar2) is
   begin
      L_part_ddl(L_part_ddl_counter) := I_line;
      L_part_ddl_counter := L_part_ddl_counter + 1;
   end;

   procedure exec_ddl_statement is

   begin

--   To Test, uncomment these three lines and comment out the four lines beneath them

--   for i in 1 .. l_part_ddl.last loop
--      dbms_output.put_line(l_part_ddl(i));
--   end loop;

      sql_cursor := dbms_sql.open_cursor;
      dbms_sql.parse(sql_cursor, L_part_ddl, L_part_ddl.FIRST, L_part_ddl.LAST, TRUE, dbms_sql.native);
      retval := dbms_sql.execute(sql_cursor);
      dbms_sql.close_cursor(sql_cursor);

   end; --exec_ddl_statement

   procedure clob_to_varchar (I_clob IN clob) is

      L_length number;
      L_offset number := 1;
      L_cr_pos number := -1;

   begin

      l_length := dbms_lob.getlength(I_clob);

      if L_length >= 1 then
         loop
            L_cr_pos := dbms_lob.instr(I_clob, chr(10), L_offset, 1);

            if L_cr_pos = 0 then
               L_cr_pos := L_length;
            end if;

            if l_cr_pos != l_offset then
               output_line(dbms_lob.substr(I_clob, L_cr_pos - L_offset, l_offset));
            else
               output_line('');
            end if;

            if L_cr_pos = L_length then
               exit;
            end if;

            L_offset := L_cr_pos +1;
         end loop;
      end if;
   end; -- clob_to_varchar

begin -- Main processing

   open c_part_type;
   fetch c_part_type into L_partitioning_type, L_subpartitioning_type;
   close c_part_type;

   if L_partitioning_type IS NOT NULL then

      execute immediate 'alter table &1 allocate extent';
      clob_to_varchar (dbms_metadata.get_ddl('TABLE',upper('&1')));

      dbms_utility.exec_ddl_statement('DROP TABLE &1');

      open c_part_key;
      fetch c_part_key into L_part_key;
      close c_part_key;

      output_line(' PARTITION BY '||L_partitioning_type||' ("'||L_part_key||'")');

      if L_subpartitioning_type != 'NONE' then
         open c_subpart_key;
         fetch c_subpart_key into L_subpart_key;
         close c_subpart_key;

         output_line(' SUBPARTITION BY '||L_subpartitioning_type||' ("'||L_subpart_key||'")');
      end if;

      output_line('(');
      for partrec in c_part loop

         if L_partitioning_type = 'RANGE' then
            output_line(partrec.comma||'PARTITION "'||partrec.partition_name||'" VALUES LESS THAN ('||partrec.high_value||')');
         elsif L_partitioning_type = 'LIST' then
            output_line(partrec.comma||'PARTITION "'||partrec.partition_name||'" VALUES ('||partrec.high_value||')');
         else
            output_line(partrec.comma||'PARTITION "'||partrec.partition_name||'"');
         end if;

         if L_partitioning_type != 'HASH' then
            output_line(partrec.pct_free||partrec.pct_used||partrec.ini_trans||partrec.max_trans||partrec.tablespace_name||partrec.logging||partrec.compression);
            if not (partrec.initial_extent is null and partrec.next_extent is null and partrec.min_extent is null
            and partrec.max_extent is null and partrec.pct_increase is null and partrec.freelists is null
            and partrec.freelist_groups is null and partrec.buffer_pool is null)
            then
               output_line(' STORAGE ('||partrec.initial_extent||partrec.next_extent||partrec.min_extent||partrec.max_extent||partrec.pct_increase||partrec.freelists||partrec.freelist_groups||partrec.buffer_pool||')');
            end if;
         else
            output_line(partrec.tablespace_name);
         end if;

         if L_subpartitioning_type != 'NONE' then
            L_partition_name := partrec.partition_name;
            output_line('(');
            for subpartrec in c_subpart loop
               if L_subpartitioning_type = 'LIST' then
                  output_line(subpartrec.comma||'SUBPARTITION "'||subpartrec.subpartition_name||'" VALUES ('||subpartrec.high_value||')');
               else
                  output_line(subpartrec.comma||'SUBPARTITION "'||subpartrec.subpartition_name||'"');
               end if;

               output_line(subpartrec.tablespace_name);

            end loop; --subpartrec
            output_line(')');
         end if;
      end loop; --partrec
      output_line(')');
      exec_ddl_statement;
   end if;
end;
/
