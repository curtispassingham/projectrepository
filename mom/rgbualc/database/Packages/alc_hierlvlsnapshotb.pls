CREATE OR REPLACE PACKAGE BODY ALC_HIER_LVL_INV_SNAPSHOT_SQL AS
-------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_IL_SOH(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER  IS

   L_program      VARCHAR2(61) := 'ALC_HIER_LVL_INV_SNAPSHOT_SQL.ROLLUP_IL_SOH';
   L_owner        VARCHAR2(30);
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   select table_owner
     into L_owner
     from system_options;

   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.alc_subclass_item_loc_soh_eod';

   insert /*+ append */ into alc_subclass_item_loc_soh_eod (dept,
                                                            class,
                                                            subclass,
                                                            loc,
                                                            loc_type,
                                                            stock_on_hand,
                                                            in_transit_qty,
                                                            pack_comp_intran,
                                                            pack_comp_soh,
                                                            tsf_reserved_qty,
                                                            pack_comp_resv,
                                                            tsf_expected_qty,
                                                            pack_comp_exp,
                                                            rtv_qty,
                                                            non_sellable_qty,
                                                            customer_resv,
                                                            customer_backorder,
                                                            pack_comp_cust_resv,
                                                            pack_comp_cust_back,
                                                            pack_comp_non_sellable,
                                                            no_clr_stock_on_hand,
                                                            no_clr_in_transit_qty,
                                                            no_clr_pack_comp_intran,
                                                            no_clr_pack_comp_soh,
                                                            no_clr_tsf_reserved_qty,
                                                            no_clr_pack_comp_resv,
                                                            no_clr_tsf_expected_qty,
                                                            no_clr_pack_comp_exp,
                                                            no_clr_rtv_qty,
                                                            no_clr_non_sellable_qty,
                                                            no_clr_customer_resv,
                                                            no_clr_customer_backorder,
                                                            no_clr_pack_comp_cust_resv,
                                                            no_clr_pack_comp_cust_back,
                                                            no_clr_pack_comp_non_sellable)
                                                     select /*+ PARALLEL*/
                                                            im.dept,
                                                            im.class,
                                                            im.subclass,
                                                            --
                                                            ilsoh.loc,
                                                            ilsoh.loc_type,
                                                            --
                                                            SUM(GREATEST(ilsoh.stock_on_hand,0)),
                                                            SUM(GREATEST(ilsoh.in_transit_qty,0)),
                                                            SUM(GREATEST(ilsoh.pack_comp_intran,0)),
                                                            SUM(GREATEST(ilsoh.pack_comp_soh,0)),
                                                            SUM(GREATEST(ilsoh.tsf_reserved_qty,0)),
                                                            SUM(GREATEST(ilsoh.pack_comp_resv,0)),
                                                            SUM(GREATEST(ilsoh.tsf_expected_qty,0)),
                                                            SUM(GREATEST(ilsoh.pack_comp_exp,0)),
                                                            SUM(GREATEST(ilsoh.rtv_qty,0)),
                                                            SUM(GREATEST(ilsoh.non_sellable_qty,0)),
                                                            SUM(GREATEST(ilsoh.customer_resv,0)),
                                                            SUM(GREATEST(ilsoh.customer_backorder,0)),
                                                            SUM(GREATEST(ilsoh.pack_comp_cust_resv,0)),
                                                            SUM(GREATEST(ilsoh.pack_comp_cust_back,0)),
                                                            SUM(GREATEST(ilsoh.pack_comp_non_sellable,0)),
                                                            --
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.stock_on_hand),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.in_transit_qty),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.pack_comp_intran),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.pack_comp_soh),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.tsf_reserved_qty),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.pack_comp_resv),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.tsf_expected_qty),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.pack_comp_exp),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.rtv_qty),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.non_sellable_qty),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.customer_resv),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.customer_backorder),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.pack_comp_cust_resv),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.pack_comp_cust_back),0)),
                                                            SUM(GREATEST(DECODE(il.clear_ind,'Y',0,ilsoh.pack_comp_non_sellable),0))
                                                       from item_loc_soh ilsoh,
                                                            item_master im,
                                                            item_loc il
                                                      where ilsoh.item     = im.item
                                                        and ilsoh.item     = il.item
                                                        and ilsoh.loc      = il.loc
                                                        and im.status      = 'A'
                                                        and im.pack_ind    = 'N'
                                                      group by im.dept,
                                                               im.class,
                                                               im.subclass,
                                                               ilsoh.loc,
                                                               ilsoh.loc_type;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SUBCLASS_ITEM_LOC_SOH_EOD - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ROLLUP_IL_SOH;
-------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_CUSTOMER_ORDER(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_HIER_LVL_INV_SNAPSHOT_SQL.ROLLUP_CUSTOMER_ORDER';
   L_owner        VARCHAR2(30);
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   select table_owner
     into L_owner
     from system_options;

   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.alc_subclass_cust_order_eod';

   insert /*+ append */ into alc_subclass_cust_order_eod (dept,
                                                          class,
                                                          subclass,
                                                          loc,
                                                          loc_type,
                                                          co_alloc_in_qty,
                                                          co_intran_qty,
                                                          no_clr_co_alloc_in_qty,
                                                          no_clr_co_intran_qty)
                                                   select i.dept,
                                                          i.class,
                                                          i.subclass,
                                                          i.to_loc,
                                                          i.to_loc_type,
                                                          SUM(co_alloc_in),
                                                          SUM(co_intran),
                                                          SUM(no_clr_co_alloc_in),
                                                          SUM(no_clr_intran)
                                                     from (select /*+ PARALLEL*/
                                                                  im.dept,
                                                                  im.class,
                                                                  im.subclass,
                                                                  th.to_loc,
                                                                  th.to_loc_type,
                                                                  NVL(td.tsf_qty,0) - NVL(td.ship_qty,0) co_alloc_in,
                                                                  DECODE(il.clear_ind,'Y',0,NVL(td.tsf_qty,0) - NVL(td.ship_qty,0)) no_clr_co_alloc_in,
                                                                  NVL(td.ship_qty,0) - NVL(td.received_qty,0) co_intran,
                                                                  DECODE(il.clear_ind,'Y',0, NVL(td.ship_qty,0) - NVL(td.received_qty,0)) no_clr_intran
                                                             from tsfhead th,
                                                                  tsfdetail td,
                                                                  item_master im,
                                                                  item_loc il
                                                            where th.tsf_type = 'CO'
                                                              and th.status   in ('A','S','P','L','C')
                                                              and th.tsf_no   = td.tsf_no
                                                              and td.item     = im.item
                                                              and im.pack_ind = 'N'
                                                              and td.item     = il.item
                                                              and th.to_loc   = il.loc
                                                           union all
                                                           select /*+ PARALLEL*/
                                                                  im.dept,
                                                                  im.class,
                                                                  im.subclass,
                                                                  th.to_loc,
                                                                  th.to_loc_type,
                                                                  (NVL(td.tsf_qty,0) - NVL(td.ship_qty,0)) * pb.pack_item_qty co_alloc_in,
                                                                  DECODE(il.clear_ind, 'Y', 0,
                                                                         (NVL(td.tsf_qty,0) - NVL(td.ship_qty,0)) * pb.pack_item_qty) no_clr_co_alloc_in,
                                                                  (NVL(td.ship_qty,0) - NVL(td.received_qty,0)) * pb.pack_item_qty co_intran,
                                                                  DECODE(il.clear_ind, 'Y', 0,
                                                                         (NVL(td.ship_qty,0) - NVL(td.received_qty,0)) * pb.pack_item_qty) no_clr_co_intran
                                                             from tsfhead th,
                                                                  tsfdetail td,
                                                                  item_master im,
                                                                  packitem_breakout pb,
                                                                  item_loc il
                                                            where th.tsf_type = 'CO'
                                                              and th.status   in ('A','S','P','L','C')
                                                              and th.tsf_no   = td.tsf_no
                                                              and td.item     = im.item
                                                              and im.pack_ind = 'Y'
                                                              and im.item     = pb.pack_no
                                                              and td.item     = il.item
                                                              and th.to_loc   = il.loc) i
                                                    group by i.dept,
                                                             i.class,
                                                             i.subclass,
                                                             i.to_loc,
                                                             i.to_loc_type;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SUBCLASS_CUST_ORDER_EOD - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ROLLUP_CUSTOMER_ORDER;
-------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_ON_ORDER(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER  IS

   L_program      VARCHAR2(61) := 'ALC_HIER_LVL_INV_SNAPSHOT_SQL.ROLLUP_ON_ORDER';
   L_owner        VARCHAR2(30);
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   select table_owner
     into L_owner
     from system_options;

   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.alc_subclass_on_order_eod';

   insert /*+ append */ into alc_subclass_on_order_eod (dept,
                                                        class,
                                                        subclass,
                                                        loc,
                                                        loc_type,
                                                        on_order_date,
                                                        all_orders_on_order_qty,
                                                        not_all_orders_on_order_qty,
                                                        no_clr_all_ord_on_ord_qty,
                                                        no_clr_not_all_ord_on_ord_qty)
                                                 select inner.dept,
                                                        inner.class,
                                                        inner.subclass,
                                                        inner.location,
                                                        inner.loc_type,
                                                        inner.not_before_date,
                                                        SUM(all_orders_on_order_qty),
                                                        SUM(not_all_orders_on_order_qty),
                                                        SUM(no_clr_all_ord_on_ord_qty),
                                                        SUM(no_clr_not_all_ord_on_ord_qty)
                                                   from (select /*+ PARALLEL*/
                                                                im.dept,
                                                                im.class,
                                                                im.subclass,
                                                                --
                                                                ol.location,
                                                                ol.loc_type,
                                                                --
                                                                oh.not_before_date,
                                                                SUM(ol.qty_ordered - NVL(ol.qty_received,0)) all_orders_on_order_qty,
                                                                SUM(DECODE(oh.include_on_order_ind,
                                                                           'N', 0,
                                                                           (ol.qty_ordered - NVL(ol.qty_received,0)))
                                                                   ) not_all_orders_on_order_qty,
                                                                --
                                                                SUM(DECODE(il.clear_ind,'Y',0,ol.qty_ordered - NVL(ol.qty_received,0))) no_clr_all_ord_on_ord_qty,
                                                                SUM(DECODE(il.clear_ind,'Y',0,
                                                                           DECODE(oh.include_on_order_ind,
                                                                                  'N', 0,
                                                                                  (ol.qty_ordered - NVL(ol.qty_received,0))))
                                                                   ) no_clr_not_all_ord_on_ord_qty
                                                           from item_master im,
                                                                ordhead oh,
                                                                ordloc ol,
                                                                item_loc il
                                                          where oh.status      = 'A'
                                                            and oh.order_type != 'CO'
                                                            and oh.order_no    = ol.order_no
                                                            and ol.qty_ordered > NVL(ol.qty_received,0)
                                                            and ol.item        = im.item
                                                            and im.pack_ind    = 'N'
                                                            and il.item        = ol.item
                                                            and il.loc         = ol.location
                                                          group by im.dept,
                                                                   im.class,
                                                                   im.subclass,
                                                                   ol.location,
                                                                   ol.loc_type,
                                                                   oh.not_before_date,
                                                                   oh.include_on_order_ind
                                                         union all
                                                         select /*+ PARALLEL*/
                                                                im.dept,
                                                                im.class,
                                                                im.subclass,
                                                                --
                                                                ol.location,
                                                                ol.loc_type,
                                                                --
                                                                oh.not_before_date,
                                                                SUM(pb.pack_item_qty * (ol.qty_ordered - NVL(ol.qty_received,0))) all_orders_on_order_qty,
                                                                SUM(DECODE(oh.include_on_order_ind,
                                                                           'N', 0,
                                                                           pb.pack_item_qty * (ol.qty_ordered - NVL(ol.qty_received,0)))
                                                                   ) not_all_orders_on_order_qty,

                                                                --
                                                                SUM(DECODE(il.clear_ind,'Y',0,
                                                                           pb.pack_item_qty * (ol.qty_ordered - NVL(ol.qty_received,0)))) no_clr_all_ord_on_ord_qty,
                                                                SUM(DECODE(il.clear_ind,'Y',0,
                                                                           DECODE(oh.include_on_order_ind,
                                                                                  'N', 0,
                                                                                  pb.pack_item_qty * (ol.qty_ordered - NVL(ol.qty_received,0))))
                                                                   ) no_clr_not_all_ord_on_ord_qty
                                                           from item_master im,
                                                                packitem_breakout pb,
                                                                ordhead oh,
                                                                ordloc ol,
                                                                item_loc il
                                                          where oh.status      = 'A'
                                                            and oh.order_type != 'CO'
                                                            and oh.order_no    = ol.order_no
                                                            and ol.qty_ordered > NVL(ol.qty_received,0)
                                                            and ol.item        = pb.pack_no
                                                            and pb.item        = im.item
                                                            and im.pack_ind    = 'N'
                                                            and il.item        = ol.item
                                                            and il.loc         = ol.location
                                                          group by im.dept,
                                                                   im.class,
                                                                   im.subclass,
                                                                   ol.location,
                                                                   ol.loc_type,
                                                                   oh.not_before_date,
                                                                   oh.include_on_order_ind) inner
                                                 group by inner.dept,
                                                          inner.class,
                                                          inner.subclass,
                                                          inner.location,
                                                          inner.loc_type,
                                                          inner.not_before_date;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SUBCLASS_ON_ORDER_EOD - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ROLLUP_ON_ORDER;
-------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_ALLOC_IN(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER  IS

   L_program      VARCHAR2(61) := 'ALC_HIER_LVL_INV_SNAPSHOT_SQL.ROLLUP_ALLOC_IN';
   L_owner        VARCHAR2(30);
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   select table_owner
     into L_owner
     from system_options;

   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.alc_subclass_alloc_in_eod';

   insert /*+ append */ into alc_subclass_alloc_in_eod (dept,
                                                        class,
                                                        subclass,
                                                        loc,
                                                        loc_type,
                                                        alloc_in_date,
                                                        all_orders_alloc_in_qty,
                                                        not_all_orders_alloc_in_qty,
                                                        no_clr_all_ord_alc_in_qty,
                                                        no_clr_not_all_ord_alc_in_qty)
                                                 select inner.dept,
                                                        inner.class,
                                                        inner.subclass,
                                                        inner.to_loc,
                                                        inner.to_loc_type,
                                                        inner.alloc_in_date,
                                                        SUM(all_orders_alloc_in_qty),
                                                        SUM(not_all_orders_alloc_in_qty),
                                                        SUM(no_clr_all_ord_alc_in_qty),
                                                        SUM(no_clr_not_all_ord_alc_in_qty)
                                                   from (
                                                         --
                                                         --LINKED TO POs
                                                         --
                                                         select /*+ PARALLEL*/
                                                                im.dept,
                                                                im.class,
                                                                im.subclass,
                                                                --
                                                                ad.to_loc,
                                                                ad.to_loc_type,
                                                                --
                                                                oh.not_before_date alloc_in_date,
                                                                SUM(ad.qty_allocated - NVL(ad.qty_transferred,0)) all_orders_alloc_in_qty,
                                                                SUM(DECODE(oh.include_on_order_ind,
                                                                           'N', 0,
                                                                           ad.qty_allocated - NVL(ad.qty_transferred,0))
                                                                   ) not_all_orders_alloc_in_qty,
                                                                --
                                                                SUM(DECODE(il.clear_ind,'Y',0,
                                                                           ad.qty_allocated - NVL(ad.qty_transferred,0))) no_clr_all_ord_alc_in_qty,
                                                                SUM(decode(il.clear_ind,'Y',0,
                                                                           DECODE(oh.include_on_order_ind,
                                                                                  'N', 0,
                                                                                  ad.qty_allocated - NVL(ad.qty_transferred,0)))
                                                                   ) no_clr_not_all_ord_alc_in_qty
                                                           from item_master im,
                                                                alloc_header ah,
                                                                alloc_detail ad,
                                                                ordhead oh,
                                                                item_loc il
                                                          where ah.status        IN ('A','R')
                                                            and ah.alloc_no      = ad.alloc_no
                                                            and ad.qty_allocated > NVL(ad.qty_transferred,0)
                                                            and ah.item          = im.item
                                                            and im.pack_ind      = 'N'
                                                            --
                                                            and oh.order_no      = ah.order_no
                                                            and oh.status        IN ('A','C')
                                                            --
                                                            and il.item          = ah.item
                                                            and il.loc           = ad.to_loc
                                                          group by im.dept,
                                                                   im.class,
                                                                   im.subclass,
                                                                   ad.to_loc,
                                                                   ad.to_loc_type,
                                                                   oh.not_before_date,
                                                                   oh.include_on_order_ind
                                                         union all
                                                         select /*+ PARALLEL*/
                                                                im.dept,
                                                                im.class,
                                                                im.subclass,
                                                                --
                                                                ad.to_loc,
                                                                ad.to_loc_type,
                                                                --
                                                                oh.not_before_date alloc_in_date,
                                                                SUM(pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0))) all_orders_alloc_in_qty,
                                                                SUM(DECODE(oh.include_on_order_ind,
                                                                           'N', 0,
                                                                           pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0)))
                                                                   ) not_all_orders_alloc_in_qty,
                                                                --
                                                                SUM(DECODE(il.clear_ind,'Y',0,
                                                                           pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0)))) no_clr_all_ord_alc_in_qty,
                                                                SUM(DECODE(il.clear_ind,'Y',0,
                                                                           DECODE(oh.include_on_order_ind,
                                                                                  'N', 0,
                                                                                  pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0))))
                                                                   ) no_clr_not_all_ord_alc_in_qty
                                                           from item_master im,
                                                                packitem_breakout pb,
                                                                alloc_header ah,
                                                                alloc_detail ad,
                                                                ordhead oh,
                                                                item_loc il
                                                          where ah.status        IN ('A','R')
                                                            and ah.alloc_no      = ad.alloc_no
                                                            and ad.qty_allocated > NVL(ad.qty_transferred,0)
                                                            and ah.item          = pb.pack_no
                                                            and pb.item          = im.item
                                                            and im.pack_ind      = 'N'
                                                            --
                                                            and oh.order_no      = ah.order_no
                                                            and oh.status        IN ('A','C')
                                                            --
                                                            and il.item          = ah.item
                                                            and il.loc           = ad.to_loc
                                                          group by im.dept,
                                                                   im.class,
                                                                   im.subclass,
                                                                   ad.to_loc,
                                                                   ad.to_loc_type,
                                                                   oh.not_before_date,
                                                                   oh.include_on_order_ind
                                                         union all
                                                         --
                                                         --LINKED TO ALLOCs
                                                         --
                                                         select /*+ PARALLEL*/
                                                                im.dept,
                                                                im.class,
                                                                im.subclass,
                                                                --
                                                                ad.to_loc,
                                                                ad.to_loc_type,
                                                                --
                                                                link_ah.release_date alloc_in_date,
                                                                SUM(ad.qty_allocated - NVL(ad.qty_transferred,0)) all_orders_alloc_in_qty,
                                                                SUM(ad.qty_allocated - NVL(ad.qty_transferred,0)) not_all_orders_alloc_in_qty,
                                                                --
                                                                SUM(DECODE(il.clear_ind,'Y',0,ad.qty_allocated - NVL(ad.qty_transferred,0))) no_clr_all_ord_alc_in_qty,
                                                                SUM(DECODE(il.clear_ind,'Y',0,ad.qty_allocated - NVL(ad.qty_transferred,0))) no_clr_not_all_ord_alc_in_qty
                                                           from item_master im,
                                                                alloc_header ah,
                                                                alloc_detail ad,
                                                                alloc_header link_ah,
                                                                item_loc il
                                                          where ah.status        IN ('A','R')
                                                            and ah.alloc_no      = ad.alloc_no
                                                            and ad.qty_allocated > NVL(ad.qty_transferred,0)
                                                            and ah.item          = im.item
                                                            and im.pack_ind      = 'N'
                                                            --
                                                            and link_ah.alloc_no = ah.order_no
                                                            and link_ah.status   IN ('A','R')
                                                            --
                                                            and il.item          = ah.item
                                                            and il.loc           = ad.to_loc
                                                          group by im.dept,
                                                                   im.class,
                                                                   im.subclass,
                                                                   ad.to_loc,
                                                                   ad.to_loc_type,
                                                                   link_ah.release_date
                                                         union all
                                                         select /*+ PARALLEL*/
                                                                im.dept,
                                                                im.class,
                                                                im.subclass,
                                                                --
                                                                ad.to_loc,
                                                                ad.to_loc_type,
                                                                --
                                                                link_ah.release_date alloc_in_date,
                                                                SUM(pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0))) all_orders_alloc_in_qty,
                                                                SUM(pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0))) not_all_orders_alloc_in_qty,
                                                                --
                                                                SUM(DECODE(il.clear_ind,'Y',0,pb.pack_item_qty *
                                                                   (ad.qty_allocated - NVL(ad.qty_transferred,0)))) no_clr_all_ord_alc_in_qty,
                                                                SUM(DECODE(il.clear_ind,'Y',0,pb.pack_item_qty *
                                                                   (ad.qty_allocated - NVL(ad.qty_transferred,0)))) no_clr_not_all_ord_alc_in_qty
                                                           from item_master im,
                                                                packitem_breakout pb,
                                                                alloc_header ah,
                                                                alloc_detail ad,
                                                                alloc_header link_ah,
                                                                item_loc il
                                                          where ah.status        IN ('A','R')
                                                            and ah.alloc_no      = ad.alloc_no
                                                            and ad.qty_allocated > NVL(ad.qty_transferred,0)
                                                            and ah.item          = pb.pack_no
                                                            and pb.item          = im.item
                                                            and im.pack_ind      = 'N'
                                                            --
                                                            and link_ah.alloc_no = ah.order_no
                                                            and link_ah.status   IN ('A','R')
                                                            --
                                                            and il.item          = ah.item
                                                            and il.loc           = ad.to_loc
                                                          group by im.dept,
                                                                   im.class,
                                                                   im.subclass,
                                                                   ad.to_loc,
                                                                   ad.to_loc_type,
                                                                   link_ah.release_date
                                                         union all
                                                         --
                                                         --LINKED TO TSFs
                                                         --
                                                         select /*+ PARALLEL*/
                                                                im.dept,
                                                                im.class,
                                                                im.subclass,
                                                                --
                                                                ad.to_loc,
                                                                ad.to_loc_type,
                                                                --
                                                                th.not_after_date alloc_in_date,
                                                                SUM(ad.qty_allocated - NVL(ad.qty_transferred,0)) all_orders_alloc_in_qty,
                                                                SUM(ad.qty_allocated - NVL(ad.qty_transferred,0)) not_all_orders_alloc_in_qty,
                                                                --
                                                                SUM(DECODE(il.clear_ind,'Y',0,ad.qty_allocated - NVL(ad.qty_transferred,0))) no_clr_all_ord_alc_in_qty,
                                                                SUM(DECODE(il.clear_ind,'Y',0,ad.qty_allocated - NVL(ad.qty_transferred,0))) no_clr_not_all_ord_alc_in_qty
                                                           from item_master im,
                                                                alloc_header ah,
                                                                alloc_detail ad,
                                                                tsfhead th,
                                                                item_loc il
                                                          where ah.status        IN ('A','R')
                                                            and ah.alloc_no      = ad.alloc_no
                                                            and ad.qty_allocated > NVL(ad.qty_transferred,0)
                                                            and ah.item          = im.item
                                                            and im.pack_ind      = 'N'
                                                            --
                                                            and th.tsf_no        = ah.order_no
                                                            and th.status        IN ('A','S','P','L','C')
                                                            --
                                                            and il.item          = ah.item
                                                            and il.loc           = ad.to_loc
                                                          group by im.dept,
                                                                   im.class,
                                                                   im.subclass,
                                                                   ad.to_loc,
                                                                   ad.to_loc_type,
                                                                   th.not_after_date
                                                         union all
                                                         select /*+ PARALLEL*/
                                                                im.dept,
                                                                im.class,
                                                                im.subclass,
                                                                --
                                                                ad.to_loc,
                                                                ad.to_loc_type,
                                                                --
                                                                th.not_after_date alloc_in_date,
                                                                SUM(pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0))) all_orders_alloc_in_qty,
                                                                SUM(pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0))) not_all_orders_alloc_in_qty,
                                                                --
                                                                SUM(DECODE(il.clear_ind,'Y',0,
                                                                    pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0)))) no_clr_all_ord_alc_in_qty,
                                                                SUM(DECODE(il.clear_ind,'Y',0,
                                                                    pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0)))) no_clr_not_all_ord_alc_in_qty
                                                           from item_master im,
                                                                packitem_breakout pb,
                                                                alloc_header ah,
                                                                alloc_detail ad,
                                                                tsfhead th,
                                                                item_loc il
                                                          where ah.status        IN ('A','R')
                                                            and ah.alloc_no      = ad.alloc_no
                                                            and ad.qty_allocated > NVL(ad.qty_transferred,0)
                                                            and ah.item          = pb.pack_no
                                                            and pb.item          = im.item
                                                            and im.pack_ind      = 'N'
                                                            --
                                                            and th.tsf_no        = ah.order_no
                                                            and th.status        IN ('A','S','P','L','C')
                                                            --
                                                            and il.item          = ah.item
                                                            and il.loc           = ad.to_loc
                                                          group by im.dept,
                                                                   im.class,
                                                                   im.subclass,
                                                                   ad.to_loc,
                                                                   ad.to_loc_type,
                                                                   th.not_after_date
                                                 ) inner
                                                  group by inner.dept,
                                                           inner.class,
                                                           inner.subclass,
                                                           inner.to_loc,
                                                           inner.to_loc_type,
                                                           inner.alloc_in_date;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SUBCLASS_ALLOC_IN_EOD - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ROLLUP_ALLOC_IN;
-------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_ALLOC_OUT(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER  IS

   L_program      VARCHAR2(61) := 'ALC_HIER_LVL_INV_SNAPSHOT_SQL.ROLLUP_ALLOC_OUT';
   L_owner        VARCHAR2(30);
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   select table_owner
     into L_owner
     from system_options;

   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.alc_subclass_alloc_out_eod';

   insert /*+ append */ into alc_subclass_alloc_out_eod (dept,
                                                         class,
                                                         subclass,
                                                         loc,
                                                         loc_type,
                                                         alloc_out_date,
                                                         alloc_out_qty,
                                                         no_clr_alloc_out_qty)
                                                  select inner.dept,
                                                         inner.class,
                                                         inner.subclass,
                                                         inner.loc,
                                                         inner.loc_type,
                                                         inner.alloc_out_date,
                                                         SUM(alloc_out_qty),
                                                         SUM(no_clr_alloc_out_qty)
                                                    from (
                                                          --
                                                          --LINKED TO POs
                                                          --
                                                          select /*+ PARALLEL*/
                                                                 im.dept,
                                                                 im.class,
                                                                 im.subclass,
                                                                 --
                                                                 ah.wh loc,
                                                                 'W' loc_type,
                                                                 --
                                                                 oh.not_before_date alloc_out_date,
                                                                 SUM(ad.qty_allocated - NVL(ad.qty_transferred,0)) alloc_out_qty,
                                                                 SUM(DECODE(il.clear_ind,'Y',0,
                                                                            ad.qty_allocated - NVL(ad.qty_transferred,0))) no_clr_alloc_out_qty
                                                            from item_master im,
                                                                 alloc_header ah,
                                                                 alloc_detail ad,
                                                                 ordhead oh,
                                                                 item_loc il
                                                           where ah.status        IN ('A','R')
                                                             and ah.alloc_no      = ad.alloc_no
                                                             and ad.qty_allocated > nvl(ad.qty_transferred,0)
                                                             and ah.item          = im.item
                                                             and im.pack_ind      = 'N'
                                                             --
                                                             and oh.order_no      = ah.order_no
                                                             and oh.status        IN ('A','C')
                                                             --
                                                             and il.item          = ah.item
                                                             and il.loc           = ah.wh
                                                           group by im.dept,
                                                                    im.class,
                                                                    im.subclass,
                                                                    ah.wh,
                                                                    oh.not_before_date
                                                          union all
                                                          select /*+ PARALLEL*/
                                                                 im.dept,
                                                                 im.class,
                                                                 im.subclass,
                                                                 --
                                                                 ah.wh loc,
                                                                 'W' loc_type,
                                                                 --
                                                                 oh.not_before_date alloc_out_date,
                                                                 sum(pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0))) alloc_out_qty,
                                                                 sum(DECODE(il.clear_ind,'Y',0,
                                                                            pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0)))) no_clr_alloc_out_qty
                                                            from item_master im,
                                                                 packitem_breakout pb,
                                                                 alloc_header ah,
                                                                 alloc_detail ad,
                                                                 ordhead oh,
                                                                 item_loc il
                                                           where ah.status        IN ('A','R')
                                                             and ah.alloc_no      = ad.alloc_no
                                                             and ad.qty_allocated > NVL(ad.qty_transferred,0)
                                                             and ah.item          = pb.pack_no
                                                             and pb.item          = im.item
                                                             and im.pack_ind      = 'N'
                                                             --
                                                             and oh.order_no      = ah.order_no
                                                             and oh.status        IN ('A','C')
                                                             --
                                                             and il.item          = ah.item
                                                             and il.loc           = ah.wh
                                                           group by im.dept,
                                                                    im.class,
                                                                    im.subclass,
                                                                    ah.wh,
                                                                    oh.not_before_date
                                                          union all
                                                          --
                                                          --LINKED TO ALLOCs
                                                          --
                                                          select /*+ PARALLEL*/
                                                                 im.dept,
                                                                 im.class,
                                                                 im.subclass,
                                                                 --
                                                                 ah.wh loc,
                                                                 'W' loc_type,
                                                                 --
                                                                 link_ah.release_date alloc_out_date,
                                                                 SUM(ad.qty_allocated - NVL(ad.qty_transferred,0)) alloc_out_qty,
                                                                 SUM(DECODE(il.clear_ind,'Y',0,ad.qty_allocated - NVL(ad.qty_transferred,0))) no_clr_alloc_out_qty
                                                            from item_master im,
                                                                 alloc_header ah,
                                                                 alloc_detail ad,
                                                                 alloc_header link_ah,
                                                                 item_loc il
                                                           where ah.status        IN ('A','R')
                                                             and ah.alloc_no      = ad.alloc_no
                                                             and ad.qty_allocated > NVL(ad.qty_transferred,0)
                                                             and ah.item          = im.item
                                                             and im.pack_ind      = 'N'
                                                             --
                                                             and link_ah.alloc_no = ah.order_no
                                                             and link_ah.status   IN ('A','R')
                                                             --
                                                             and il.item          = ah.item
                                                             and il.loc           = ah.wh
                                                           group by im.dept,
                                                                    im.class,
                                                                    im.subclass,
                                                                    ah.wh,
                                                                    link_ah.release_date
                                                          union all
                                                          select /*+ PARALLEL*/
                                                                 im.dept,
                                                                 im.class,
                                                                 im.subclass,
                                                                 --
                                                                 ah.wh loc,
                                                                 'W' loc_type,
                                                                 --
                                                                 link_ah.release_date alloc_out_date,
                                                                 SUM(pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0))) alloc_out_qty,
                                                                 SUM(DECODE(il.clear_ind,'Y',0,
                                                                    pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0)))) no_clr_alloc_out_qty
                                                            from item_master im,
                                                                 packitem_breakout pb,
                                                                 alloc_header ah,
                                                                 alloc_detail ad,
                                                                 alloc_header link_ah,
                                                                 item_loc il
                                                           where ah.status        IN ('A','R')
                                                             and ah.alloc_no      = ad.alloc_no
                                                             and ad.qty_allocated > NVL(ad.qty_transferred,0)
                                                             and ah.item          = pb.pack_no
                                                             and pb.item          = im.item
                                                             and im.pack_ind      = 'N'
                                                             --
                                                             and link_ah.alloc_no = ah.order_no
                                                             and link_ah.status   IN('A','R')
                                                             --
                                                             and il.item          = ah.item
                                                             and il.loc           = ah.wh
                                                           group by im.dept,
                                                                    im.class,
                                                                    im.subclass,
                                                                    ah.wh,
                                                                    link_ah.release_date
                                                          union all
                                                          --
                                                          --LINKED TO TSFs
                                                          --
                                                          select /*+ PARALLEL*/
                                                                 im.dept,
                                                                 im.class,
                                                                 im.subclass,
                                                                 --
                                                                 ah.wh loc,
                                                                 'W' loc_type,
                                                                 --
                                                                 th.not_after_date alloc_out_date,
                                                                 SUM(ad.qty_allocated - NVL(ad.qty_transferred,0)) alloc_out_qty,
                                                                 SUM(DECODE(il.clear_ind,'Y',0,ad.qty_allocated - NVL(ad.qty_transferred,0))) no_clr_alloc_out_qty
                                                            from item_master im,
                                                                 alloc_header ah,
                                                                 alloc_detail ad,
                                                                 tsfhead th,
                                                                 item_loc il
                                                           where ah.status        IN ('A','R')
                                                             and ah.alloc_no      = ad.alloc_no
                                                             and ad.qty_allocated > NVL(ad.qty_transferred,0)
                                                             and ah.item          = im.item
                                                             and im.pack_ind      = 'N'
                                                             --
                                                             and th.tsf_no        = ah.order_no
                                                             and th.status        IN ('A','S','P','L','C')
                                                             --
                                                             and il.item          = ah.item
                                                             and il.loc           = ah.wh
                                                           group by im.dept,
                                                                    im.class,
                                                                    im.subclass,
                                                                    ah.wh,
                                                                    th.not_after_date
                                                          union all
                                                          select /*+ PARALLEL*/
                                                                 im.dept,
                                                                 im.class,
                                                                 im.subclass,
                                                                 --
                                                                 ah.wh loc,
                                                                 'W' loc_type,
                                                                 --
                                                                 th.not_after_date alloc_out_date,
                                                                 SUM(pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0))) alloc_out_qty,
                                                                 SUM(DECODE(il.clear_ind,'Y',0,
                                                                    pb.pack_item_qty * (ad.qty_allocated - NVL(ad.qty_transferred,0)))) no_clr_alloc_out_qty
                                                            from item_master im,
                                                                 packitem_breakout pb,
                                                                 alloc_header ah,
                                                                 alloc_detail ad,
                                                                 tsfhead th,
                                                                 item_loc il
                                                           where ah.status        IN ('A','R')
                                                             and ah.alloc_no      = ad.alloc_no
                                                             and ad.qty_allocated > nvl(ad.qty_transferred,0)
                                                             and ah.item          = pb.pack_no
                                                             and pb.item          = im.item
                                                             and im.pack_ind      = 'N'
                                                             --
                                                             and th.tsf_no        = ah.order_no
                                                             and th.status        IN ('A','S','P','L','C')
                                                             --
                                                             and il.item          = ah.item
                                                             and il.loc           = ah.wh
                                                           group by im.dept,
                                                                    im.class,
                                                                    im.subclass,
                                                                    ah.wh,
                                                                    th.not_after_date
                                                  ) inner
                                                  group by inner.dept,
                                                           inner.class,
                                                           inner.subclass,
                                                           inner.loc,
                                                           inner.loc_type,
                                                           inner.alloc_out_date;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SUBCLASS_ALLOC_OUT_EOD - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ROLLUP_ALLOC_OUT;
-------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_CROSSLINK_IN(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER  IS

   L_program            VARCHAR2(61) := 'ALC_HIER_LVL_INV_SNAPSHOT_SQL.ROLLUP_CROSSLINK_IN';
   L_owner              VARCHAR2(30);
   L_wh_crosslink_ind   SYSTEM_OPTIONS.WH_CROSS_LINK_IND%TYPE := NULL;
   L_start_time         TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   select table_owner,
          wh_cross_link_ind
     into L_owner,
          L_wh_crosslink_ind
     from system_options;

   if L_wh_crosslink_ind = 'Y' then

      EXECUTE IMMEDIATE 'truncate table '||L_owner||'.alc_subclass_crosslink_eod';

      insert /*+ append */ into alc_subclass_crosslink_eod (dept,
                                                            class,
                                                            subclass,
                                                            loc,
                                                            loc_type,
                                                            crosslink_qty,
                                                            no_clr_crosslink_qty)
                                                     select inner.dept,
                                                            inner.class,
                                                            inner.subclass,
                                                            inner.to_loc,
                                                            inner.to_loc_type,
                                                            SUM(crosslink_qty),
                                                            SUM(no_clr_crosslink_qty)
                                                       from (select /*+ PARALLEL*/
                                                                    im.dept,
                                                                    im.class,
                                                                    im.subclass,
                                                                    --
                                                                    th.to_loc,
                                                                    th.to_loc_type,
                                                                    --
                                                                    SUM(td.tsf_qty - NVL(td.ship_qty,0)) crosslink_qty,
                                                                    SUM(DECODE(il.clear_ind,'Y',0,td.tsf_qty - NVL(td.ship_qty,0))) no_clr_crosslink_qty
                                                            from item_master im,
                                                                 tsfhead th,
                                                                 tsfdetail td,
                                                                 item_loc il
                                                              where th.status      IN ('A','E','S','B')
                                                                and th.tsf_type    = 'PL'
                                                                and th.tsf_no      = td.tsf_no
                                                                and td.tsf_qty     > nvl(td.ship_qty,0)
                                                                and td.item        = im.item
                                                                and im.pack_ind    = 'N'
                                                                --
                                                                and il.item        = td.item
                                                                and il.loc         = th.to_loc
                                                              group by im.dept,
                                                                       im.class,
                                                                       im.subclass,
                                                                       th.to_loc,
                                                                       th.to_loc_type
                                                             union all
                                                             select /*+ PARALLEL*/
                                                                    im.dept,
                                                                    im.class,
                                                                    im.subclass,
                                                                    --
                                                                    th.to_loc,
                                                                    th.to_loc_type,
                                                                    --
                                                                    SUM(pb.pack_item_qty * td.tsf_qty - NVL(td.ship_qty,0)) crosslink_qty,
                                                                    SUM(pb.pack_item_qty * DECODE(il.clear_ind,'Y',0,td.tsf_qty - NVL(td.ship_qty,0))) no_clr_crosslink_qty
                                                               from item_master im,
                                                                    packitem_breakout pb,
                                                                    tsfhead th,
                                                                    tsfdetail td,
                                                                    item_loc il
                                                              where th.status      IN ('A','E','S','B')
                                                                and th.tsf_type    = 'PL'
                                                                and th.tsf_no      = td.tsf_no
                                                                and td.tsf_qty     > NVL(td.ship_qty,0)
                                                                and td.item        = pb.pack_no
                                                                and pb.item        = im.item
                                                                and im.pack_ind    = 'N'
                                                                --
                                                                and il.item        = td.item
                                                                and il.loc         = th.to_loc
                                                              group by im.dept,
                                                                       im.class,
                                                                       im.subclass,
                                                                       th.to_loc,
                                                                       th.to_loc_type) inner
                                                     group by inner.dept,
                                                              inner.class,
                                                              inner.subclass,
                                                              inner.to_loc,
                                                              inner.to_loc_type;

   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ROLLUP_CROSSLINK_IN;
-------------------------------------------------------------------------------------------------------------
END ALC_HIER_LVL_INV_SNAPSHOT_SQL;
/
