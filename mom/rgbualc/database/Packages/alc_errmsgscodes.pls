CREATE OR REPLACE PACKAGE ALC_ERRMSG_CODE_SQL AS

   -- Maps the Error Constants to the Error Key in Messages.xlif
   -- To be used by Packages to send the error code back to the UI to display translated messages


     ERRMSG_MIN_QL_AVAILABLE_QTY    CONSTANT   VARCHAR2(55) := 'ERRMSG_MIN_QL_AVAILABLE_QTY';
     ERRMSG_MAX_QL_AVAILABLE_QTY    CONSTANT   VARCHAR2(55) := 'ERRMSG_MAX_QL_AVAILABLE_QTY';

END ALC_ERRMSG_CODE_SQL;
/