CREATE OR REPLACE PACKAGE BODY ALC_HUB_FACET_CFG_SQL IS
------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_FACETS_ON_RULE(O_error_message               IN  OUT VARCHAR2,
                               I_facet_session_id            IN      VARCHAR2,
                               I_LEVEL                       IN      NUMBER,
                               I_disabled_facet_attributes   IN  OUT DISABLED_FACET_ATTRIBUTES_TBL)
RETURN NUMBER;
--
FUNCTION POP_DESCRIPTIONS(O_error_message      IN  OUT VARCHAR2,
                          I_facet_session_id   IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
-- Name:    LOAD_FACETS
-- Purpose: This function is used to load data into FACET tables.
--          Based on configuration from RAF_FACET_ATTRIBUTE_CFG table
--          it loads RAF_FACET_SESSION and RAF_FACET_SESSION_ATTRIBUTE
--          table.
------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_FACETS(O_error_message        IN OUT VARCHAR2,
                     I_facet_session_id     IN     VARCHAR2,
                     I_facet_source_query   IN     VARCHAR2,
                     I_query_name           IN     VARCHAR2)
RETURN NUMBER IS

   L_program          VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.LOAD_FACETS';
   L_start_time       TIMESTAMP     := SYSTIMESTAMP;
   L_work_header_id   NUMBER(15);
   L_query            VARCHAR2(4000);
   L_what_if_ind      NUMBER(1)     := 0;

   cursor C_WHAT_IF is
      select 1
        from alc_work_item_source awis
       where awis.work_header_id = L_work_header_id
         and awis.source_type    = ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_WHAT_IF;


BEGIN

   L_work_header_id := TO_NUMBER(SUBSTR(I_facet_source_query, INSTR(I_facet_source_query,'=') + 1));

   open C_WHAT_IF;
   fetch C_WHAT_IF into L_what_if_ind;
   close C_WHAT_IF;

   delete from raf_facet_session_attribute
    where facet_session_id = I_facet_session_id;

   delete from raf_facet_session
    where facet_session_id = I_facet_session_id;

   insert into raf_facet_session(facet_session_id,
                                 facet_cfg_id,
                                 created_by,
                                 updated_by,
                                 created_date,
                                 updated_date)
                          select DISTINCT
                                 I_facet_session_id,
                                 rfc.facet_cfg_id,
                                 USER,
                                 USER,
                                 SYSDATE,
                                 SYSDATE
                            from raf_facet_cfg rfc
                           where rfc.query_name = I_query_name;

   -- Get All Facet Attributes that does not have any Parent Facet Attribute and NOT ITEM NODE
   for crec IN (select DISTINCT
                       rfac.facet_attribute_cfg_id,
                       rfac.attribute_name
                  from raf_facet_session rfs,
                       raf_facet_attribute_cfg rfac
                 where rfs.facet_session_id = I_facet_session_id
                   and rfac.facet_cfg_id    = rfs.facet_cfg_id
                   and rfac.parent_facet_attr_cfg_id is NULL
                   and rfac.attribute_name NOT IN (ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT,
                                                   ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT_DIFF,
                                                   ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PACK,
                                                   ALC_HUB_FACET_CFG_SQL.ITEM_FACET_SKU)
                   and ( (L_what_if_ind = 0)
                      or (    L_what_if_ind = 1
                          and rfac.attribute_name NOT IN (ALC_HUB_FACET_CFG_SQL.FACET_ATTRIB_SOURCE_TYPE,
                                                          ALC_HUB_FACET_CFG_SQL.FACET_ATTRIB_WH)))) loop
      L_query :=
      'insert into raf_facet_session_attribute '||
        ' (facet_session_id, ' ||
        '  facet_session_attribute_id, ' ||
        '  facet_attribute_cfg_id, '||
        '  value, ' ||
        '  selected, ' ||
        '  parent_facet_session_attr_id) '||
      'select '''||I_facet_session_id||''', ' ||
            ' raf_facet_session_attr_seq.nextval, '||
              crec.facet_attribute_cfg_id ||' ,'||
            ' awis.'||crec.attribute_name ||' , '||
            '''Y''' ||','||
            ' NULL '||
       ' from (select DISTINCT '||
                      crec.attribute_name ||
               ' from alc_work_item_source '||
              ' where '|| I_facet_source_query ||
              '   and '||crec.attribute_name||' is NOT NULL ) awis ';

   EXECUTE IMMEDIATE L_query;

   end loop;

   -- Get All Facet Attributes that have a Parent Facet Attribute
   for crec IN (select DISTINCT
                       rfsa.facet_session_attribute_id parent_facet_session_attr_id,
                       rfac.facet_attribute_cfg_id,
                       rfac.attribute_name,
                       rfac.parent_facet_attr_cfg_id,
                       rfac.parent_attribute_value,
                       prfac.attribute_name parent_attribute_name
                  from raf_facet_session rfs,
                       raf_facet_session_attribute rfsa,
                       raf_facet_attribute_cfg rfac,
                       raf_facet_attribute_cfg prfac
                 where rfs.facet_session_id         = I_facet_session_id
                   and rfac.facet_cfg_id            = rfs.facet_cfg_id
                   and rfac.parent_facet_attr_cfg_id is NOT NULL
                   and prfac.facet_cfg_id           = rfac.facet_cfg_id
                   and prfac.facet_attribute_cfg_id = rfac.parent_facet_attr_cfg_id
                   and rfsa.facet_session_id        = rfs.facet_session_id
                   and rfsa.value                   = rfac.parent_attribute_value) loop
      L_query :=
      'insert into raf_facet_session_attribute '||
        ' (facet_session_id, ' ||
        '  facet_session_attribute_id, ' ||
        '  facet_attribute_cfg_id, '||
        '  value, ' ||
        '  selected, ' ||
        '  parent_facet_session_attr_id) '||
      'select '''||I_facet_session_id||''', ' ||
            ' raf_facet_session_attr_seq.nextval, '||
              crec.facet_attribute_cfg_id ||' ,'||
            ' awis.'||crec.attribute_name ||' , '||
            '''Y''' ||','||
              crec.parent_facet_session_attr_id||
       ' from (select distinct '||
                      crec.attribute_name ||
               ' from alc_work_item_source '||
              ' where '|| I_facet_source_query ||
              '   and '||crec.parent_attribute_name||' = '''||crec.parent_attribute_value||''' ) awis ';

      EXECUTE IMMEDIATE L_query;

   end loop;

   -- Get All Facet Attributes that ARE ITEM NODE
   for crec IN (select DISTINCT
                       rfac.facet_attribute_cfg_id,
                       rfac.attribute_name facet_name,
                       ALC_HUB_FACET_CFG_SQL.ITEM_NODE_ATTRIBUTE attribute_name
                  from raf_facet_session rfs,
                       raf_facet_attribute_cfg rfac
                 where rfs.facet_session_id = I_facet_session_id
                   and rfac.facet_cfg_id = rfs.facet_cfg_id
                   and rfac.parent_facet_attr_cfg_id is NULL
                   and rfac.attribute_name IN (ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT,
                                               ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT_DIFF,
                                               ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PACK,
                                               ALC_HUB_FACET_CFG_SQL.ITEM_FACET_SKU) ) loop
      L_query :=
      'insert into raf_facet_session_attribute '||
        ' (facet_session_id, ' ||
        '  facet_session_attribute_id, ' ||
        '  facet_attribute_cfg_id, '||
        '  value, ' ||
        '  selected, ' ||
        '  parent_facet_session_attr_id) '||
      'select '''||I_facet_session_id||''', ' ||
            ' raf_facet_session_attr_seq.nextval, '||
              crec.facet_attribute_cfg_id ||' ,'||
            ' awis.'||crec.attribute_name ||' , '||
            '''Y''' ||','||
            ' NULL '||
       ' from (select DISTINCT '||
                      crec.attribute_name ||
               ' from alc_work_item_source '||
              ' where '|| I_facet_source_query;
      if crec.facet_name = ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT then
         L_query := L_query ||
                   '   and '||ALC_HUB_FACET_CFG_SQL.ITEM_NODE_TYPE_ATTRIBUTE||' = '''||ALC_CONSTANTS_SQL.STYLE||''' ';
      elsif crec.facet_name = ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT_DIFF then
         L_query := L_query ||
                    '   and '||ALC_HUB_FACET_CFG_SQL.ITEM_NODE_TYPE_ATTRIBUTE||' = '''||ALC_CONSTANTS_SQL.FASHIONITEM||''' ';
      elsif crec.facet_name = ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PACK then
         L_query := L_query ||
                    '   and '||ALC_HUB_FACET_CFG_SQL.ITEM_NODE_TYPE_ATTRIBUTE||' IN ('''||ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK||''', '||
                                                                                     ''''||ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK||''', '||
                                                                                     ''''||ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK||''', '||
                                                                                     ''''||ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK||''', '||
                                                                                     ''''||ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK||''', '||
                                                                                     ''''||ALC_CONSTANTS_SQL.SELLABLEPACK||''') ';
      elsif crec.facet_name = ALC_HUB_FACET_CFG_SQL.ITEM_FACET_SKU then
         L_query := L_query ||
                    '   and '||ALC_HUB_FACET_CFG_SQL.ITEM_NODE_TYPE_ATTRIBUTE||' IN ('''||ALC_CONSTANTS_SQL.STAPLEITEM||''', '||
                                         ''''||ALC_CONSTANTS_SQL.FASHIONSKU||''') ';
      end if;
       L_query := L_query ||
                  '   and '||crec.attribute_name||' is NOT NULL ) awis ';

      EXECUTE IMMEDIATE L_query;

   end loop;

   if POP_DESCRIPTIONS(O_error_message,
                       I_facet_session_id) = FALSE then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END LOAD_FACETS;
------------------------------------------------------------------------------------------------------------
-- Name:    LOAD_SOURCE
-- Purpose: This function is used to load data into FACET tables by calling
--          LOAD_FACETS function and then loads to ALC_WORK_SESSION_ITEM and
--          ALC_WORK_SESSION_ITEM_LOC table.
------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_SOURCE(O_error_message        IN OUT VARCHAR2,
                     I_facet_session_id     IN     VARCHAR2,
                     I_facet_source_query   IN     VARCHAR2,
                     I_query_name           IN     VARCHAR2)
RETURN NUMBER IS

   L_program          VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.LOAD_SOURCE';
   L_start_time       TIMESTAMP     := SYSTIMESTAMP;
   L_work_header_id   NUMBER(15);
   L_query            VARCHAR2(4000);
   L_what_if_ind      NUMBER(1)     := 0;

   cursor C_WHAT_IF is
      select 1
        from alc_work_item_source awis
       where awis.work_header_id = L_work_header_id
         and awis.source_type    = ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_WHAT_IF;
BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   L_work_header_id := TO_NUMBER(SUBSTR(I_facet_source_query, INSTR(I_facet_source_query,'=') + 1));

   open C_WHAT_IF;
   fetch C_WHAT_IF into L_what_if_ind;
   close C_WHAT_IF;

   delete
     from alc_work_session_item_all
    where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_SESSION_ITEM_ALL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_work_session_item_loc
    where work_item_session_id IN (select DISTINCT
                                          work_item_session_id
                                     from alc_work_session_item
                                    where facet_session_id = I_facet_session_id);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_SESSION_ITEM_LOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_work_session_item
    where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_SESSION_ITEM - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if LOAD_FACETS(O_error_message,
                  I_facet_session_id,
                  I_facet_source_query,
                  I_query_name) = 0 then
      return 0;
   end if;

   -- Get all Non Pack Item, children, grand children, great
   -- grand children, great great grand children
   insert into alc_work_session_item(work_item_session_id,
                                     facet_session_id,
                                     item,
                                     thumbnail,
                                     pack_configuration,
                                     ancestor_work_id,
                                     item_source_desc,
                                     item_type,
                                     item_id,
                                     diff_1,
                                     diff_2,
                                     diff_3,
                                     disable_ind)
                              select alc_work_session_item_seq.NEXTVAL,
                                     I_facet_session_id facet_session_id,
                                     t.item,
                                     t.image,
                                     t.pack_configuration,
                                     NULL ancestor_work_id,
                                     t.item_source_desc,
                                     t.item_type,
                                     t.item_id,
                                     t.diff_1,
                                     t.diff_2,
                                     t.diff_3,
                                     t.disable_ind
                                from (select DISTINCT
                                             awis.item,
                                             iim.image_addr||iim.image_name image,
                                             NULL pack_configuration,
                                             awis.item_type,
                                             awis.item_id,
                                             awis.item_source_desc,
                                             awis.diff_1,
                                             awis.diff_2,
                                             awis.diff_3,
                                             awis.disable_ind,
                                             row_number() OVER (PARTITION BY awis.item
                                                                    ORDER BY awis.item)num
                                        from alc_work_item_source awis,
                                             item_master im,
                                             item_image iim
                                       where work_header_id        = L_work_header_id
                                         and awis.item_type       != 'PACKCOMP'
                                         and awis.item             = im.item (+)
                                         and awis.item             = iim.item (+)
                                         and NVL(im.pack_ind, 'N') = 'N'
                                         and awis.ancestor_id is NULL) t
                               where t.num = 1;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_SESSION_ITEM_LOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Children
   insert into alc_work_session_item(work_item_session_id,
                                     facet_session_id,
                                     item,
                                     thumbnail,
                                     pack_configuration,
                                     ancestor_work_id,
                                     item_source_desc,
                                     item_type,
                                     item_id,
                                     diff_1,
                                     diff_2,
                                     diff_3,
                                     disable_ind)
                              select alc_work_session_item_seq.NEXTVAL,
                                     I_facet_session_id facet_session_id,
                                     g.item,
                                     g.image,
                                     g.pack_configuration,
                                     g.ancestor_work_id,
                                     g.item_source_desc,
                                     g.item_type,
                                     g.item_id,
                                     g.diff_1,
                                     g.diff_2,
                                     g.diff_3,
                                     g.disable_ind
                               from (select t.item,
                                            t.image,
                                            SUM(pb.item_qty) pack_configuration,
                                            t.ancestor_work_id,
                                            t.item_source_desc,
                                            t.item_type,
                                            t.item_id,
                                            t.diff_1,
                                            t.diff_2,
                                            t.diff_3,
                                            t.disable_ind
                                       from (select DISTINCT
                                                    awis.item,
                                                    NULL pack_configuration,
                                                    NULL image,
                                                    awsi.work_item_session_id ancestor_work_id,
                                                    awis.item_source_desc,
                                                    awis.item_type,
                                                    awis.item_id,
                                                    awis.diff_1,
                                                    awis.diff_2,
                                                    awis.diff_3,
                                                    awis.disable_ind
                                               from alc_work_item_source awis,
                                                    alc_work_item_source awis2,
                                                    alc_work_session_item awsi
                                              where awis.work_header_id = L_work_header_id
                                                and awis.item_type     != 'PACKCOMP'
                                                and awis.ancestor_id is NOT NULL
                                                and awis2.work_item_source_id = awis.ancestor_id
                                                and awis2.ancestor_id is NULL
                                                and awsi.item             = awis2.item
                                                and awsi.facet_session_id = I_facet_session_id) t,
                                            packitem_breakout pb
                                      where pb.pack_no(+) = t.item
                                      group by t.item,
                                               t.image,
                                               t.ancestor_work_id,
                                               t.item_source_desc,
                                               t.item_type,
                                               t.item_id,
                                               t.diff_1,
                                               t.diff_2,
                                               t.diff_3,
                                               t.disable_ind) g;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_SESSION_ITEM_LOC - CHILDREN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Grand Children
   insert into alc_work_session_item(work_item_session_id,
                                     facet_session_id,
                                     item,
                                     thumbnail,
                                     pack_configuration,
                                     ancestor_work_id,
                                     item_source_desc,
                                     item_type,
                                     item_id,
                                     diff_1,
                                     diff_2,
                                     diff_3,
                                     disable_ind)
                              select alc_work_session_item_seq.NEXTVAL,
                                     I_facet_session_id facet_session_id,
                                     g.item,
                                     g.image,
                                     g.pack_configuration,
                                     g.ancestor_work_id,
                                     g.item_source_desc,
                                     g.item_type,
                                     g.item_id,
                                     g.diff_1,
                                     g.diff_2,
                                     g.diff_3,
                                     g.disable_ind
                                from (select t.item,
                                             t.image,
                                             SUM(pb.item_qty) pack_configuration,
                                             t.ancestor_work_id,
                                             t.item_source_desc,
                                             t.item_type,
                                             t.item_id,
                                             t.diff_1,
                                             t.diff_2,
                                             t.diff_3,
                                             t.disable_ind
                                        from (select DISTINCT
                                                     awis.item,
                                                     iim.image_addr||iim.image_name image,
                                                     NULL pack_configuration,
                                                     awsi.work_item_session_id ancestor_work_id,
                                                     awis.item_source_desc,
                                                     awis.item_type,
                                                     awis.item_id,
                                                     awis.diff_1,
                                                     awis.diff_2,
                                                     awis.diff_3,
                                                     awis.disable_ind,
                                                     row_number() OVER (PARTITION BY im.item
                                                                            ORDER BY im.item) num
                                                from alc_work_item_source awis,
                                                     item_master im,
                                                     item_image iim,
                                                     alc_work_item_source awis2,
                                                     alc_work_session_item awsi
                                               where awis.work_header_id = L_work_header_id
                                                 and awis.item_type           != 'PACKCOMP'
                                                 and awis.item                 = im.item (+)
                                                 and awis.item                 = iim.item (+)
                                                 and awis.ancestor_id is NOT NULL
                                                 and awis2.work_item_source_id = awis.ancestor_id
                                                 and awis2.ancestor_id is NOT NULL
                                                 and awsi.item             = awis2.item
                                                 and awsi.facet_session_id = i_facet_session_id) t,
                                             packitem_breakout pb
                                       where t.num         = 1
                                         and pb.pack_no(+) = t.item
                                       group by t.item,
                                                t.image,
                                                t.ancestor_work_id,
                                                t.item_source_desc,
                                                t.item_type,
                                                t.item_id,
                                                t.diff_1,
                                                t.diff_2,
                                                t.diff_3,
                                                t.disable_ind) g;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_SESSION_ITEM_LOC - GRANDCHILDREN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Get all Pack Item
   insert into alc_work_session_item(work_item_session_id,
                                     facet_session_id,
                                     item,
                                     thumbnail,
                                     pack_configuration,
                                     ancestor_work_id,
                                     item_source_desc,
                                     item_type,
                                     item_id,
                                     diff_1,
                                     diff_2,
                                     diff_3,
                                     disable_ind)
                              select alc_work_session_item_seq.NEXTVAL,
                                     I_facet_session_id facet_session_id,
                                     t.item,
                                     iim.image_addr||iim.image_name image,
                                     t.pack_configuration,
                                     NULL ancestor_work_id,
                                     t.item_source_desc,
                                     t.item_type,
                                     t.item_id,
                                     t.diff_1,
                                     t.diff_2,
                                     t.diff_3,
                                     t.disable_ind
                                from (select DISTINCT
                                             awis.item,
                                             SUM(pb.item_qty) pack_configuration,
                                             awis.item_source_desc,
                                             awis.item_type,
                                             awis.item_id,
                                             awis.diff_1,
                                             awis.diff_2,
                                             awis.diff_3,
                                             awis.disable_ind
                                        from (select DISTINCT
                                                     item,
                                                     item_source_desc,
                                                     item_type,
                                                     item_id, diff_1,
                                                     diff_2,diff_3,
                                                     disable_ind
                                                from alc_work_item_source
                                               where work_header_id = L_work_header_id
                                                 and item_type     != 'PACKCOMP'
                                                 and ancestor_id is NULL) awis,
                                             item_master im,
                                             packitem_breakout pb
                                       where awis.item   = im.item
                                         and im.pack_ind = 'Y'
                                         and pb.pack_no  = awis.item
                                       group by awis.item,
                                                awis.item_type,
                                                awis.item_id,
                                                awis.item_source_desc,
                                                awis.diff_1,
                                                awis.diff_2,
                                                awis.diff_3,
                                                awis.disable_ind) t,
                                     item_image iim
                               where t.item = iim.item (+);

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_SESSION_ITEM_LOC - PACKITEM - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Get all pack component except for the SELLPACK
   insert into alc_work_session_item(work_item_session_id,
                                     facet_session_id,
                                     item,
                                     thumbnail,
                                     pack_configuration,
                                     ancestor_work_id,
                                     item_source_desc,
                                     item_type)
                              select alc_work_session_item_seq.NEXTVAL,
                                     I_facet_session_id facet_session_id,
                                     t.item,
                                     t.image,
                                     t.pack_configuration,
                                     t.ancestor_work_id,
                                     t.item_source_desc,
                                     'PACKCOMP' item_type
                                from (select pb.item,
                                             iim.image_addr||iim.image_name image,
                                             pb.item_qty pack_configuration,
                                             awsi.work_item_session_id ancestor_work_id,
                                             im_comp.item_desc item_source_desc
                                        from alc_work_session_item awsi,
                                             item_master im,
                                             item_master im_comp,
                                             (select item,
                                                     image_addr,
                                                     image_name
                                                from (select item,
                                                             image_addr,image_name,
                                                             row_number() OVER (PARTITION BY item
                                                                                 ORDER BY item) num
                                                        from item_image)
                                                       where num = 1) iim,
                                             packitem_breakout pb
                                       where awsi.facet_session_id = I_facet_session_id
                                         and iim.item(+)           = pb.item
                                         and im_comp.item          = pb.item
                                         and pb.pack_no            = awsi.item
                                         and pb.pack_no            = im.item
                                         and im.pack_ind           = 'Y'
                                         and im.sellable_ind       = 'N'
                                     )t;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_SESSION_ITEM_LOC - PACKCOMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Populate alc_work_session_item_loc
   -- It should not rely on the quantity on the ALC_WORK_ITEM_SOURCE for the style and style/color level
   -- Populate for SKU (include Sellable Pack) and Packs (This will reflect the total quantity of the components)

   insert into alc_work_session_item_loc(work_item_loc_session_id,
                                         facet_session_id,
                                         work_item_session_id,
                                         wh,
                                         wh_avail_qty,
                                         po_avail_qty,
                                         tsf_avail_qty,
                                         bol_avail_qty,
                                         asn_avail_qty,
                                         alloc_avail_qty)
                                  select alc_work_session_item_loc_seq.NEXTVAL,
                                         t.facet_session_id,
                                         t.work_item_session_id,
                                         t.wh,
                                         t.wh_avail_qty,
                                         t.po_avail_qty,
                                         t.tsf_avail_qty,
                                         t.bol_avail_qty,
                                         t.asn_avail_qty,
                                         t.alloc_avail_qty
                                    from (select s.facet_session_id,
                                                 s.work_item_session_id,
                                                 s.wh,
                                                 (s.wh_avail_qty  * pack_configuration) wh_avail_qty,
                                                 (s.po_avail_qty  * pack_configuration) po_avail_qty,
                                                 (s.tsf_avail_qty * pack_configuration) tsf_avail_qty,
                                                 (s.bol_avail_qty * pack_configuration) bol_avail_qty,
                                                 (s.asn_avail_qty * pack_configuration) asn_avail_qty,
                                                 (s.alloc_avail_qty * pack_configuration) alloc_avail_qty
                                            from (select awsi.facet_session_id,
                                                         awsi.work_item_session_id,
                                                         case
                                                            when awis.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                                                    ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                                                    ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                                                    ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                                                    ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK) then
                                                               awsi.pack_configuration
                                                         else
                                                            1
                                                         end as pack_configuration,
                                                         awis.wh,
                                                         SUM(case
                                                                when awis.source_type = 'OH' then
                                                                   awis.available_quantity
                                                                else
                                                                   0
                                                             end) OVER(PARTITION BY awis.work_header_id,
                                                                                    awis.item,
                                                                                    awis.wh) wh_avail_qty,
                                                         SUM(case
                                                                when awis.source_type = 'PO' then
                                                                   awis.available_quantity
                                                                else
                                                                   0
                                                             end) OVER(PARTITION BY awis.work_header_id,
                                                                                    awis.item,
                                                                                    awis.wh) po_avail_qty,
                                                         SUM(case
                                                                when awis.source_type = 'TSF' then
                                                                   awis.available_quantity
                                                                else
                                                                   0
                                                             end) OVER(PARTITION BY awis.work_header_id,
                                                                                    awis.item,
                                                                                    awis.wh) tsf_avail_qty,
                                                         SUM(case
                                                                when awis.source_type = 'BOL' then
                                                                   awis.available_quantity
                                                                else
                                                                   0
                                                             end) OVER(PARTITION BY awis.work_header_id,
                                                                                    awis.item,
                                                                                    awis.wh) bol_avail_qty,
                                                         SUM(case
                                                                when awis.source_type = 'ASN' then
                                                                   awis.available_quantity
                                                                else
                                                                   0
                                                             end) OVER(PARTITION BY awis.work_header_id,
                                                                                    awis.item,
                                                                                    awis.wh) asn_avail_qty,
                                                         SUM(case
                                                                when awis.source_type = 'ALLOC' then
                                                                   awis.available_quantity
                                                                else
                                                                   0
                                                             end) OVER(PARTITION BY awis.work_header_id,
                                                                                    awis.item,
                                                                                    awis.wh) alloc_avail_qty,
                                                         row_number() OVER(PARTITION BY awis.work_header_id,
                                                                                        awis.item,
                                                                                        awis.wh
                                                                               ORDER BY awis.wh) RNK
                                                    from (select awsi.facet_session_id,
                                                                 awsi.work_item_session_id,
                                                                 awsi.item,
                                                                 awsi2.item parent_node,
                                                                 SUM(pb.item_qty) OVER(PARTITION BY awsi.work_item_session_id) pack_configuration,
                                                                 row_number() OVER(PARTITION BY awsi.work_item_session_id
                                                                                       ORDER BY pb.seq_no) rnk
                                                            from alc_work_session_item awsi,
                                                                 alc_work_session_item awsi2,
                                                                 packitem_breakout pb
                                                           where awsi.facet_session_id = I_facet_session_id
                                                             and awsi.item_type IN (ALC_CONSTANTS_SQL.FASHIONSKU,
                                                                                    ALC_CONSTANTS_SQL.STAPLEITEM,
                                                                                    ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                                                    ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                                                    ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                                                    ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                                                    ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                                                                    ALC_CONSTANTS_SQL.SELLABLEPACK)
                                                             and awsi.ancestor_work_id = awsi2.work_item_session_id (+)
                                                             and pb.pack_no (+)        = awsi.item) awsi,
                                                         alc_work_item_source awis
                                                   where awsi.rnk            = 1
                                                     and awis.item           = awsi.item
                                                     and awis.work_header_id = L_work_header_id)s
                                           where s.rnk = 1) t
                                  where (L_what_if_ind = 0 and
                                         (NVL(t.wh_avail_qty, 0)    > 0 or
                                          NVL(t.po_avail_qty, 0)    > 0 or
                                          NVL(t.tsf_avail_qty,0)    > 0 or
                                          NVL(t.bol_avail_qty,0)    > 0 or
                                          NVL(t.asn_avail_qty,0)    > 0 or
                                          NVL(t.alloc_avail_qty,0)  > 0))
                                     or L_what_if_ind = 1;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_SESSION_ITEM_LOC - SKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Populate for FA (This would be the total of all children (SKU and Fashion PACK))

   insert into alc_work_session_item_loc(work_item_loc_session_id,
                                         facet_session_id,
                                         work_item_session_id,
                                         wh,
                                         wh_avail_qty,
                                         po_avail_qty,
                                         tsf_avail_qty,
                                         bol_avail_qty,
                                         asn_avail_qty,
                                         alloc_avail_qty)
                                  select alc_work_session_item_loc_seq.NEXTVAL,
                                         t.facet_session_id,
                                         t.work_item_session_id,
                                         t.wh,
                                         t.wh_avail_qty,
                                         t.po_avail_qty,
                                         t.tsf_avail_qty,
                                         t.bol_avail_qty,
                                         t.asn_avail_qty,
                                         t.alloc_avail_qty
                                    from (select awsi.facet_session_id,
                                                 awsi.work_item_session_id,
                                                 awsi.item,
                                                 awsil.wh wh,
                                                 SUM(awsil.wh_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                           awsil.wh) wh_avail_qty,
                                                 SUM(awsil.po_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                           awsil.wh) po_avail_qty,
                                                 SUM(awsil.tsf_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                            awsil.wh) tsf_avail_qty,
                                                 SUM(awsil.bol_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                            awsil.wh) bol_avail_qty,
                                                 SUM(awsil.asn_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                            awsil.wh) asn_avail_qty,
                                                 SUM(awsil.alloc_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                              awsil.wh) alloc_avail_qty,
                                                 row_number() OVER(PARTITION BY awsi.work_item_session_id,
                                                                                awsil.wh
                                                                       ORDER BY awsil.work_item_loc_session_id) rnk
                                            from alc_work_session_item awsi,
                                                 alc_work_session_item awsi2,
                                                 alc_work_session_item_loc awsil
                                           where awsi.facet_session_id      = I_facet_session_id
                                             and awsi.item_type             = ALC_CONSTANTS_SQL.FASHIONITEM
                                             and awsi.work_item_session_id  = awsi2.ancestor_work_id
                                             and awsil.work_item_session_id = awsi2.work_item_session_id) t
                                   where t.rnk = 1;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_SESSION_ITEM_LOC - FA - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Populate for STYLE (This would be the total of all children (FA))

   insert into alc_work_session_item_loc(work_item_loc_session_id,
                                         facet_session_id,
                                         work_item_session_id,
                                         wh,
                                         wh_avail_qty,
                                         po_avail_qty,
                                         tsf_avail_qty,
                                         bol_avail_qty,
                                         asn_avail_qty,
                                         alloc_avail_qty)
                                  select alc_work_session_item_loc_seq.NEXTVAL,
                                         t.facet_session_id,
                                         t.work_item_session_id,
                                         t.wh,
                                         t.wh_avail_qty,
                                         t.po_avail_qty,
                                         t.tsf_avail_qty,
                                         t.bol_avail_qty,
                                         t.asn_avail_qty,
                                         t.alloc_avail_qty
                                    from (select awsi.facet_session_id,
                                                 awsi.work_item_session_id,
                                                 awsi.item,
                                                 awsil.wh wh,
                                                 SUM(awsil.wh_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                           awsil.wh) wh_avail_qty,
                                                 SUM(awsil.po_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                           awsil.wh) po_avail_qty,
                                                 SUM(awsil.tsf_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                            awsil.wh) tsf_avail_qty,
                                                 SUM(awsil.bol_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                            awsil.wh) bol_avail_qty,
                                                 SUM(awsil.asn_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                            awsil.wh) asn_avail_qty,
                                                 SUM(awsil.alloc_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                                              awsil.wh) alloc_avail_qty,
                                                 row_number() OVER(PARTITION BY awsi.work_item_session_id, awsil.wh
                                                                       ORDER BY awsil.work_item_loc_session_id) rnk
                                            from alc_work_session_item awsi,
                                                 alc_work_session_item awsi2,
                                                 alc_work_session_item_loc awsil
                                           where awsi.facet_session_id      = I_facet_session_id
                                             and awsi.item_type             = ALC_CONSTANTS_SQL.STYLE
                                             and awsi.work_item_session_id  = awsi2.ancestor_work_id
                                             and awsil.work_item_session_id = awsi2.work_item_session_id) t
                                   where t.rnk = 1;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_SESSION_ITEM_LOC - STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Remove All Items that has no stock of any type

   delete from alc_work_session_item awsi
    where awsi.facet_session_id = I_facet_session_id
      and awsi.item_type != 'PACKCOMP'
      and NOT EXISTS (select 1
                        from alc_work_session_item_loc awsil
                       where awsil.facet_session_id     = I_facet_session_id
                         and awsil.work_item_session_id = awsi.work_item_session_id);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_SESSION_ITEM - NOT PACKCOMP SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from alc_work_session_item awsi
    where awsi.facet_session_id = I_facet_session_id
      and awsi.ancestor_work_id is NOT NULL
      and NOT EXISTS (select 1
                        from alc_work_session_item awsi1
                       where awsi1.facet_session_id     = I_facet_session_id
                         and awsi1.work_item_session_id = awsi.ancestor_work_id);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_SESSION_ITEM - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Delete Item Facets
   delete from raf_facet_session_attribute rfsa
    where rfsa.facet_session_id       = I_facet_session_id
      and rfsa.facet_attribute_cfg_id IN (select fac.facet_attribute_cfg_id
                                            from raf_facet_attribute_cfg fac,
                                                 raf_facet_cfg fc
                                           where fc.query_name      = 'ALC_WORK_ITEM_SOURCE'
                                             and fac.facet_cfg_id   = fc.facet_cfg_id
                                             and fac.attribute_name IN ('PARENT',
                                                                        'PARENTDIFF',
                                                                        'SKU',
                                                                        'PACK'))
      and NOT EXISTS (select 1
                        from alc_work_session_item awsi
                       where awsi.facet_session_id = I_facet_session_id
                         and awsi.item             = rfsa.value);

   LOGGER.LOG_INFORMATION(L_program||' Delete RAF_FACET_SESSION_ATTRIBUTE - ITEM FACET - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Delete WH Facets
   delete from raf_facet_session_attribute rfsa
    where rfsa.facet_session_id = I_facet_session_id
      and rfsa.facet_attribute_cfg_id IN (select fac.facet_attribute_cfg_id
                                            from raf_facet_attribute_cfg fac,
                                                 raf_facet_cfg fc
                                           where fc.query_name      = 'ALC_WORK_ITEM_SOURCE'
                                             and fac.facet_cfg_id   = fc.facet_cfg_id
                                             and fac.attribute_name = 'WH'
                                             and fac.parent_facet_attr_cfg_id is NULL)
      and NOT EXISTS (select 1
                        from alc_work_session_item_loc awsi
                       where awsi.facet_session_id = I_facet_session_id
                         and TO_CHAR(awsi.wh)      = rfsa.value);

   LOGGER.LOG_INFORMATION(L_program||' Delete RAF_FACET_SESSION_ATTRIBUTE - WH FACET - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Populate alc_work_session_item_all. This will be used during the filter

   insert into alc_work_session_item_all(work_item_session_id,
                                         facet_session_id,
                                         item,
                                         thumbnail,
                                         pack_configuration,
                                         ancestor_work_id,
                                         item_type,
                                         item_id,
                                         diff_1,
                                         diff_2,
                                         diff_3)
                                  select work_item_session_id,
                                         facet_session_id,
                                         item,
                                         thumbnail,
                                         pack_configuration,
                                         ancestor_work_id,
                                         item_type,
                                         item_id,
                                         diff_1,
                                         diff_2,
                                         diff_3
                                    from alc_work_session_item
                                   where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_SESSION_ITEM_ALL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END LOAD_SOURCE;
------------------------------------------------------------------------------------------------------------
-- Name:    FILTER_SOURCE
-- Purpose: This function is used to filter data based on the input object
--          DISABLED_FACET_ATTRIBUTES_TBL sent from User interface.It merges
--          and deletes the record depending on the records.
--          Based on configuration from RAF_FACET_ATTRIBUTE_CFG table
--          it loads RAF_FACET_SESSION and RAF_FACET_SESSION_ATTRIBUTE
--          table.Also it loads to ALC_WORK_SESSION_ITEM and
--          alc_work_session_item_loc table.
------------------------------------------------------------------------------------------------------------
FUNCTION FILTER_SOURCE(O_error_message               IN OUT VARCHAR2,
                       I_facet_session_id            IN     VARCHAR2,
                       I_facet_source_query          IN     VARCHAR2,
                       I_query_name                  IN     VARCHAR2,
                       I_disabled_facet_attributes   IN     DISABLED_FACET_ATTRIBUTES_TBL)
RETURN NUMBER IS

   L_program                       VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.FILTER_SOURCE';
   L_start_time                    TIMESTAMP     := SYSTIMESTAMP;
   L_query                         VARCHAR2(4000);
   L_work_header_id                NUMBER(15);
   L_work_item_session_ids         OBJ_NUMERIC_ID_TABLE;
   L_disabled_facet_lvl2_updated   DISABLED_FACET_ATTRIBUTES_TBL;
   L_what_if_ind                   NUMBER(1) := 0;

   cursor C_WHAT_IF is
      select 1
        from alc_work_item_source awis
       where awis.work_header_id = L_work_header_id
         and awis.source_type    = ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_WHAT_IF;

   cursor C_REMOVED_ITEM is
      select /*+ CARDINALITY(filter 100) */
             work_item_session_id
        from raf_facet_cfg rfc,
             raf_facet_attribute_cfg rfac,
             table (cast(L_disabled_facet_lvl2_updated as DISABLED_FACET_ATTRIBUTES_TBL)) filter,
             alc_work_session_item awsi
       where rfc.query_name      = I_query_name
         and rfac.facet_cfg_id   = rfc.facet_cfg_id
         and rfac.attribute_name IN (ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT,
                                     ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT_DIFF,
                                     ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PACK,
                                     ALC_HUB_FACET_CFG_SQL.ITEM_FACET_SKU)
         and filter.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
         and awsi.facet_session_id         = I_facet_session_id
         and awsi.item                     = filter.facet_attribute_value;

    cursor C_ITEMS_TO_REMOVE is
       select t.work_item_session_id,
              t.ancestor_work_id,
              t.lvl
         from (select awsi.work_item_session_id,
                      awsi.ancestor_work_id,
                      LEVEL lvl
                 from alc_work_session_item awsi
                where awsi.facet_Session_id = I_facet_session_id
                start with awsi.work_item_session_id IN (select /*+ CARDINALITY(ids 100) */
                                                                value(ids)
                                                           from table (cast(L_work_item_session_ids as OBJ_NUMERIC_ID_TABLE)) ids
                                                          where rownum > 0)
              connect by prior awsi.work_item_session_id = awsi.ancestor_work_id) t
        order by t.lvl desc,
                 t.work_item_session_id;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   L_work_header_id := TO_NUMBER(SUBSTR(I_facet_source_query, INSTR(I_facet_source_query,'=') + 1));

   open C_WHAT_IF;
   fetch C_WHAT_IF into L_what_if_ind;
   close C_WHAT_IF;

   -- Set all FACEST SESSION ATTRIBUTE to 'SELECTED'
   update raf_facet_session_attribute
      set selected = 'Y'
    where facet_session_id = I_facet_session_id;


   -- Merge back deleted alc_work_session_item
   merge into alc_work_session_item target
   using (select DISTINCT
                 awsia.work_item_session_id,
                 awsia.facet_session_id,
                 awsia.item,
                 awis.item_source_desc,
                 awsia.thumbnail,
                 awsia.pack_configuration,
                 awsia.ancestor_work_id,
                 awsia.item_type,
                 awsia.item_id,
                 awsia.diff_1,
                 awsia.diff_2,
                 awsia.diff_3
            from alc_work_session_item_all awsia,
                 alc_work_item_source awis
           where awsia.facet_session_id = I_facet_session_id
             and awsia.item             = awis.item
             and awis.work_header_id    = L_work_header_id ) source
   on (    target.facet_session_id     = source.facet_session_id
       and target.work_item_session_id = source.work_item_session_id)
   when NOT MATCHED then
      insert(work_item_session_id,
             facet_session_id,
             item,
             item_source_desc,
             thumbnail,
             pack_configuration,
             ancestor_work_id,
             item_type,
             item_id,
             diff_1,
             diff_2,
             diff_3)
      values(source.work_item_session_id,
             source.facet_session_id,
             source.item,
             source.item_source_desc,
             source.thumbnail,
             source.pack_configuration,
             source.ancestor_work_id,
             source.item_type,
             source.item_id,
             source.diff_1,
             source.diff_2,
             source.diff_3);


   -- Re-Populate alc_work_session_item_loc
   -- First Merge Back for SKU (include Sellable Pack) and Packs (This will reflect the total quantity of the components)
   merge into alc_work_session_item_loc target
   using (select t.facet_session_id,
                 t.work_item_session_id,
                 t.wh,
                 t.wh_avail_qty,
                 t.po_avail_qty,
                 t.tsf_avail_qty,
                 t.bol_avail_qty,
                 t.asn_avail_qty,
                 t.alloc_avail_qty
            from (select s.facet_session_id,
                         s.work_item_session_id,
                         s.wh,
                         (s.wh_avail_qty * pack_configuration) wh_avail_qty,
                         (s.po_avail_qty * pack_configuration) po_avail_qty,
                         (s.tsf_avail_qty * pack_configuration) tsf_avail_qty,
                         (s.bol_avail_qty * pack_configuration) bol_avail_qty,
                         (s.asn_avail_qty * pack_configuration) asn_avail_qty,
                         (s.alloc_avail_qty * pack_configuration) alloc_avail_qty
                    from (select awsi.facet_session_id,
                                 awsi.work_item_session_id,
                                 case
                                    when awis.item_type in (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK) then
                                       awsi.pack_configuration
                                    else
                                       1
                                 end as pack_configuration,
                                 awis.wh,
                                 SUM(case
                                        when awis.source_type = 'OH' then
                                           awis.available_quantity
                                        else
                                           0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) wh_avail_qty,
                                 SUM(case
                                        when awis.source_type = 'PO' then
                                           awis.available_quantity
                                        else
                                           0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) po_avail_qty,
                                 SUM(case
                                        when awis.source_type = 'TSF' then
                                           awis.available_quantity
                                        else
                                           0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) tsf_avail_qty,
                                 SUM(case
                                        when awis.source_type = 'BOL' then
                                           awis.available_quantity
                                        else
                                        0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) bol_avail_qty,
                                 SUM(case
                                        when awis.source_type = 'ASN' then
                                           awis.available_quantity
                                        else
                                           0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) ASN_AVAIL_QTY,
                                 SUM(case
                                        when awis.source_type = 'ALLOC' then
                                           awis.available_quantity
                                        else
                                           0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) ALLOC_AVAIL_QTY,
                                 row_number() OVER(PARTITION BY awis.work_header_id,
                                                                awis.item,
                                                                awis.wh
                                                       ORDER BY awis.wh) RNK
                            from (select awsi.facet_session_id,
                                         awsi.work_item_session_id,
                                         awsi.item,
                                         awsi2.item parent_node,
                                         SUM(pb.item_qty) OVER(PARTITION by awsi.work_item_session_id) pack_configuration,
                                         row_number() OVER(PARTITION by awsi.work_item_session_id
                                                            ORDER BY pb.seq_no) rnk
                                    from alc_work_session_item awsi,
                                         alc_work_session_item awsi2,
                                         packitem_breakout pb
                                   where awsi.facet_session_id = I_facet_session_id
                                     and awsi.item_type IN (ALC_CONSTANTS_SQL.FASHIONSKU,
                                                            ALC_CONSTANTS_SQL.STAPLEITEM,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                                            ALC_CONSTANTS_SQL.SELLABLEPACK)
                                     and awsi.ancestor_work_id = awsi2.work_item_session_id (+)
                                     and pb.pack_no (+)        = awsi.item) awsi,
                                 alc_work_item_source awis
                           where awsi.rnk = 1
                             and awis.item = awsi.item
                             and awis.work_header_id = L_work_header_id)s
                   where RNK = 1) t
           where (L_what_if_ind = 0 and
                  (NVL(t.wh_avail_qty, 0)    > 0 or
                   NVL(t.po_avail_qty, 0)    > 0 or
                   NVL(t.tsf_avail_qty,0)    > 0 or
                   NVL(t.bol_avail_qty,0)    > 0 or
                   NVL(t.asn_avail_qty,0)    > 0 or
                   NVL(t.alloc_avail_qty,0)  > 0))
              or L_what_if_ind = 1) source
   on (    target.work_item_session_id = source.work_item_session_id
       and target.wh                   = source.wh
       and target.facet_session_id     = source.facet_session_id)
   when MATCHED then
      update
         set wh_avail_qty    = source.wh_avail_qty,
             po_avail_qty    = source.po_avail_qty,
             tsf_avail_qty   = source.tsf_avail_qty,
             bol_avail_qty   = source.bol_avail_qty,
             asn_avail_qty   = source.asn_avail_qty,
             alloc_avail_qty = source.alloc_avail_qty
   when NOT MATCHED then
      insert(work_item_loc_session_id,
             facet_session_id,
             work_item_session_id,
             wh,
             wh_avail_qty,
             po_avail_qty,
             tsf_avail_qty,
             bol_avail_qty,
             asn_avail_qty,
             alloc_avail_qty)
      values(alc_work_session_item_loc_seq.NEXTVAL,
             source.facet_session_id,
             source.work_item_session_id,
             source.wh,
             source.wh_avail_qty,
             source.po_avail_qty,
             source.tsf_avail_qty,
             source.bol_avail_qty,
             source.asn_avail_qty,
             source.alloc_avail_qty);

   -- Merge Back for FA (This would be the total of all children (SKU and Fashion PACK))
   merge into alc_work_session_item_loc target
   using (select t.facet_session_id,
                 t.work_item_session_id,
                 t.wh,
                 t.wh_avail_qty,
                 t.po_avail_qty,
                 t.tsf_avail_qty,
                 t.bol_avail_qty,
                 t.asn_avail_qty,
                 t.alloc_avail_qty
            from (select awsi.facet_session_id,
                         awsi.work_item_session_id,
                         awsi.item,
                         awsil.wh wh,
                         SUM(awsil.wh_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                   awsil.wh) wh_avail_qty,
                         SUM(awsil.po_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                   awsil.wh) po_avail_qty,
                         SUM(awsil.tsf_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) tsf_avail_qty,
                         SUM(awsil.bol_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) bol_avail_qty,
                         SUM(awsil.asn_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) asn_avail_qty,
                         SUM(awsil.alloc_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                      awsil.wh) alloc_avail_qty,
                         row_number() OVER(PARTITION BY awsi.work_item_session_id,
                                                        awsil.wh
                                               ORDER BY awsil.work_item_loc_session_id) rnk
                    from alc_work_session_item awsi,
                         alc_work_session_item awsi2,
                         alc_work_session_item_loc awsil
                   where awsi.facet_session_id      = I_facet_session_id
                     and awsi.item_type             = ALC_CONSTANTS_SQL.FASHIONITEM
                     and awsi.work_item_session_id  = awsi2.ancestor_work_id
                     and awsil.work_item_session_id = awsi2.work_item_session_id) t
           where t.rnk = 1) source
   on (    target.work_item_session_id = source.work_item_session_id
       and target.wh                   = source.wh
       and target.facet_session_id     = source.facet_session_id)
   when MATCHED then
      update
         set wh_avail_qty    = source.wh_avail_qty,
             po_avail_qty    = source.po_avail_qty,
             tsf_avail_qty   = source.tsf_avail_qty,
             bol_avail_qty   = source.bol_avail_qty,
             asn_avail_qty   = source.asn_avail_qty,
             alloc_avail_qty = source.alloc_avail_qty
   when NOT MATCHED then
      insert(work_item_loc_session_id,
             facet_session_id,
             work_item_session_id,
             wh,
             wh_avail_qty,
             po_avail_qty,
             tsf_avail_qty,
             bol_avail_qty,
             asn_avail_qty,
             alloc_avail_qty)
      values(alc_work_session_item_loc_seq.NEXTVAL,
             source.facet_session_id,
             source.work_item_session_id,
             source.wh,
             source.wh_avail_qty,
             source.po_avail_qty,
             source.tsf_avail_qty,
             source.bol_avail_qty,
             source.asn_avail_qty,
             source.alloc_avail_qty);

   -- Merge Back for STYLE (This would be the total of all children (SKU and Fashion PACK))
   merge into alc_work_session_item_loc target
   using (select t.facet_session_id,
                 t.work_item_session_id,
                 t.wh,
                 t.wh_avail_qty,
                 t.po_avail_qty,
                 t.tsf_avail_qty,
                 t.bol_avail_qty,
                 t.asn_avail_qty,
                 t.alloc_avail_qty
            from (select awsi.facet_session_id,
                         awsi.work_item_session_id,
                         awsi.item,
                         awsil.wh wh,
                         SUM(awsil.wh_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                   awsil.wh) wh_avail_qty,
                         SUM(awsil.po_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                   awsil.wh) po_avail_qty,
                         SUM(awsil.tsf_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) tsf_avail_qty,
                         SUM(awsil.bol_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) bol_avail_qty,
                         SUM(awsil.asn_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) asn_avail_qty,
                         SUM(awsil.alloc_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                      awsil.wh) alloc_avail_qty,
                         row_number() OVER(PARTITION BY awsi.work_item_session_id,
                                                        awsil.wh
                                               ORDER BY awsil.work_item_loc_session_id) rnk
                    from alc_work_session_item awsi,
                         alc_work_session_item awsi2,
                         alc_work_session_item_loc awsil
                   where awsi.facet_session_id      = I_facet_session_id
                     and awsi.item_type             = ALC_CONSTANTS_SQL.STYLE
                     and awsi.work_item_session_id  = awsi2.ancestor_work_id
                     and awsil.work_item_session_id = awsi2.work_item_session_id) t
           where t.rnk = 1) source
   on (    target.work_item_session_id = source.work_item_session_id
       and target.wh                   = source.wh
       and target.facet_session_id     = source.facet_session_id)
   when MATCHED then
      update
         set wh_avail_qty    = source.wh_avail_qty,
             po_avail_qty    = source.po_avail_qty,
             tsf_avail_qty   = source.tsf_avail_qty,
             bol_avail_qty   = source.bol_avail_qty,
             asn_avail_qty   = source.asn_avail_qty,
             alloc_avail_qty = source.alloc_avail_qty
   when NOT MATCHED then
      insert(work_item_loc_session_id,
             facet_session_id,
             work_item_session_id,
             wh,
             wh_avail_qty,
             po_avail_qty,
             tsf_avail_qty,
             bol_avail_qty,
             asn_avail_qty,
             alloc_avail_qty)
      values(alc_work_session_item_loc_seq.NEXTVAL,
             source.facet_session_id,
             source.work_item_session_id,
             source.wh,
             source.wh_avail_qty,
             source.po_avail_qty,
             source.tsf_avail_qty,
             source.bol_avail_qty,
             source.asn_avail_qty,
             source.alloc_avail_qty);

   -- Filter is empty, so reset everything
   if I_disabled_facet_attributes is NULL or
      I_disabled_facet_attributes.count() = 0 then
      return 1;
   end if;

    L_disabled_facet_lvl2_updated :=I_disabled_facet_attributes;

   -- Pass for  Style/Style/Color
   if (UPDATE_FACETS_ON_RULE(O_error_message, I_facet_session_id,1, L_disabled_facet_lvl2_updated) = 0 ) then
      return 0;
   end if;
   -- Pass for  Style/Color SKu
   if (UPDATE_FACETS_ON_RULE(O_error_message, I_facet_session_id,2, L_disabled_facet_lvl2_updated) = 0 ) then
      return 0;
   end if;

   -- Now based on the filter set some of them to 'NOT SELECTED'

   merge into raf_facet_session_attribute target
   using (select /*+ CARDINALITY(filter 100) */
                 rfsa.facet_session_attribute_id
            from table (cast(L_disabled_facet_lvl2_updated as DISABLED_FACET_ATTRIBUTES_TBL)) filter,
                 raf_facet_session_attribute rfsa
           where rfsa.facet_session_id       = I_facet_session_id
             and rfsa.facet_attribute_cfg_id = filter.facet_attribute_cfg_id
             and rfsa.value                  = filter.facet_attribute_value) source
   on (target.facet_session_attribute_id = source.facet_session_attribute_id)
   when MATCHED then
      update
         set target.selected = 'N';

   -- Delete all the Items are filtered out
   open C_REMOVED_ITEM;
   fetch C_REMOVED_ITEM BULK COLLECT into L_work_item_session_ids;
   close C_REMOVED_ITEM;

   for rec in C_ITEMS_TO_REMOVE loop

      delete alc_work_session_item_loc
       where work_item_session_id = rec.work_item_session_id;

      delete alc_work_session_item
       where work_item_session_id = rec.work_item_session_id;

   end loop;

   delete alc_work_session_item_loc
    where facet_session_id = I_facet_session_id
      and wh IN (select /*+ CARDINALITY(filter 100) */
                        DISTINCT filter.facet_attribute_value
                   from table (cast(L_disabled_facet_lvl2_updated as DISABLED_FACET_ATTRIBUTES_TBL)) filter,
                        raf_facet_attribute_cfg rfac
                  where rfac.facet_attribute_cfg_id = filter.facet_attribute_cfg_id
                    and rfac.attribute_name         = 'WH'
                    and rfac.parent_facet_attr_cfg_id is NULL);

   -- First Merge Back for SKU (include Sellable Pack) and Packs (This will reflect the total quantity of the components)
   merge into alc_work_session_item_loc target
   using (select t.facet_session_id,
                 t.work_item_session_id,
                 t.wh,
                 t.wh_avail_qty,
                 t.po_avail_qty,
                 t.tsf_avail_qty,
                 t.bol_avail_qty,
                 t.asn_avail_qty,
                 t.alloc_avail_qty
            from (select s.facet_session_id,
                         s.work_item_session_id,
                         s.wh,
                         (s.wh_avail_qty * pack_configuration) wh_avail_qty,
                         (s.po_avail_qty * pack_configuration) po_avail_qty,
                         (s.tsf_avail_qty * pack_configuration) tsf_avail_qty,
                         (s.bol_avail_qty * pack_configuration) bol_avail_qty,
                         (s.asn_avail_qty * pack_configuration) asn_avail_qty,
                         (s.alloc_avail_qty * pack_configuration) alloc_avail_qty
                    from (select awsi.facet_session_id,
                                 awsi.work_item_session_id,
                                 case
                                    when awis.item_type in (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK) then
                                       awsi.pack_configuration
                                    else
                                       1
                                 end as pack_configuration,
                                 awis.wh,
                                 SUM(case
                                        when awis.source_type = 'OH' and
                                             NOT EXISTS (select /*+ CARDINALITY(filter 100) */ 1
                                                           from table (cast(L_disabled_facet_lvl2_updated as DISABLED_FACET_ATTRIBUTES_TBL)) filter,
                                                                raf_facet_attribute_cfg rfac
                                                          where rfac.facet_attribute_cfg_id = filter.facet_attribute_cfg_id
                                                            and rfac.parent_attribute_value = 'OH'
                                                            and TO_CHAR(awis.wh)            = filter.facet_attribute_value) then
                                          awis.available_quantity
                                        else 0
                                    end) OVER(PARTITION BY awis.work_header_id,
                                                           awis.item,
                                                           awis.wh) wh_avail_qty,
                                 SUM(case
                                        when awis.source_type = 'PO' and
                                             NOT EXISTS (select /*+ CARDINALITY(filter 100) */ 1
                                                           from table (cast(L_disabled_facet_lvl2_updated as DISABLED_FACET_ATTRIBUTES_TBL)) filter,
                                                                raf_facet_attribute_cfg rfac
                                                          where rfac.facet_attribute_cfg_id = filter.facet_attribute_cfg_id
                                                            and rfac.parent_attribute_value = 'PO'
                                                            and awis.doc_no                 = filter.facet_attribute_value) then
                                           awis.available_quantity
                                        else
                                              0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) po_avail_qty,
                                 SUM(case
                                        when awis.source_type = 'TSF' and
                                             NOT EXISTS (select /*+ CARDINALITY(filter 100) */ 1
                                                           from table (cast(L_disabled_facet_lvl2_updated as DISABLED_FACET_ATTRIBUTES_TBL)) filter,
                                                                raf_facet_attribute_cfg rfac
                                                           where rfac.facet_attribute_cfg_id = filter.facet_attribute_cfg_id
                                                             and rfac.parent_attribute_value = 'TSF'
                                                             and awis.doc_no                 = filter.facet_attribute_value) then
                                           awis.available_quantity
                                        else
                                           0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) tsf_avail_qty,
                                 SUM(case
                                        when awis.source_type = 'BOL' and
                                             NOT EXISTS (select /*+ CARDINALITY(filter 100) */ 1
                                                           from table (cast(L_disabled_facet_lvl2_updated as DISABLED_FACET_ATTRIBUTES_TBL)) filter,
                                                                raf_facet_attribute_cfg rfac
                                                          where rfac.facet_attribute_cfg_id = filter.facet_attribute_cfg_id
                                                            and rfac.parent_attribute_value = 'BOL'
                                                            and awis.doc_no                 = filter.facet_attribute_value) then
                                           awis.available_quantity
                                        else
                                           0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) bol_avail_qty,
                                 SUM(case
                                        when awis.source_type = 'ASN' and
                                             NOT EXISTS (select /*+ CARDINALITY(filter 100) */ 1
                                                           from table (cast(L_disabled_facet_lvl2_updated as DISABLED_FACET_ATTRIBUTES_TBL)) filter,
                                                                raf_facet_attribute_cfg rfac
                                                          where rfac.facet_attribute_cfg_id = filter.facet_attribute_cfg_id
                                                            and rfac.parent_attribute_value = 'ASN'
                                                            and awis.doc_no = filter.facet_attribute_value) then
                                              awis.available_quantity
                                        else
                                           0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) asn_avail_qty,
                                 SUM(case
                                        when awis.source_type = 'ALLOC' and
                                             NOT EXISTS (select /*+ CARDINALITY(filter 100) */ 1
                                                           from table (cast(L_disabled_facet_lvl2_updated as DISABLED_FACET_ATTRIBUTES_TBL)) filter,
                                                                raf_facet_attribute_cfg rfac
                                                          where rfac.facet_attribute_cfg_id = filter.facet_attribute_cfg_id
                                                            and rfac.parent_attribute_value = 'ALLOC'
                                                            and awis.doc_no = filter.facet_attribute_value) then
                                              awis.available_quantity
                                        else
                                           0
                                     end) OVER(PARTITION BY awis.work_header_id,
                                                            awis.item,
                                                            awis.wh) alloc_avail_qty,
                                 row_number() OVER(PARTITION BY awis.work_header_id,
                                                                awis.item,
                                                                awis.wh order by awis.wh) RNK
                            from (select awsi.facet_session_id,
                                         awsi.work_item_session_id,
                                         awsi.item,
                                         awsi2.item parent_node,
                                         SUM(pb.item_qty) OVER(PARTITION BY awsi.work_item_session_id) pack_configuration,
                                         row_number() OVER(PARTITION BY awsi.work_item_session_id
                                                               ORDER BY pb.seq_no) rnk
                                    from alc_work_session_item awsi,
                                         alc_work_session_item awsi2,
                                         packitem_breakout pb
                                   where awsi.facet_session_id = I_facet_session_id
                                     and awsi.item_type IN (ALC_CONSTANTS_SQL.FASHIONSKU,
                                                            ALC_CONSTANTS_SQL.STAPLEITEM,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                                            ALC_CONSTANTS_SQL.SELLABLEPACK)
                                     and awsi.ancestor_work_id = awsi2.work_item_session_id (+)
                                     and pb.pack_no (+)        = awsi.item) awsi,
                                 alc_work_item_source awis
                           where awsi.rnk            = 1
                             and awis.item           = awsi.item
                             and awis.work_header_id = L_work_header_id)s
                   where rnk = 1) t) source
    on (    target.facet_session_id     = source.facet_session_id
        and target.work_item_session_id = source.work_item_session_id
        and target.wh                   = source.wh)
    when MATCHED then
       update
          set target.wh_avail_qty    = source.wh_avail_qty,
              target.po_avail_qty    = source.po_avail_qty,
              target.tsf_avail_qty   = source.tsf_avail_qty,
              target.bol_avail_qty   = source.bol_avail_qty,
              target.asn_avail_qty   = source.asn_avail_qty,
              target.alloc_avail_qty = source.alloc_avail_qty;

   -- Merge Back for FA (This would be the total of all children (SKU and Fashion PACK))
   merge into alc_work_session_item_loc target
   using (select t.facet_session_id,
                 t.work_item_session_id,
                 t.wh,
                 t.wh_avail_qty,
                 t.po_avail_qty,
                 t.tsf_avail_qty,
                 t.bol_avail_qty,
                 t.asn_avail_qty,
                 t.alloc_avail_qty
            from (select awsi.facet_session_id,
                         awsi.work_item_session_id,
                         awsi.item,
                         awsil.wh wh,
                         SUM(awsil.wh_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                   awsil.wh) wh_avail_qty,
                         SUM(awsil.po_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                   awsil.wh) po_avail_qty,
                         SUM(awsil.tsf_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) tsf_avail_qty,
                         SUM(awsil.bol_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) bol_avail_qty,
                         SUM(awsil.asn_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) asn_avail_qty,
                         SUM(awsil.alloc_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                      awsil.wh) alloc_avail_qty,
                         row_number() OVER(PARTITION BY awsi.work_item_session_id,
                                                        awsil.wh
                                               ORDER BY awsil.work_item_loc_session_id) rnk
                    from alc_work_session_item awsi,
                         alc_work_session_item awsi2,
                         alc_work_session_item_loc awsil
                   where awsi.facet_session_id      = I_facet_session_id
                     and awsi.item_type             = ALC_CONSTANTS_SQL.FASHIONITEM
                     and awsi.work_item_session_id  = awsi2.ancestor_work_id
                     and awsil.work_item_session_id = awsi2.work_item_session_id) t
           where t.rnk = 1) source
   on (    target.work_item_session_id = source.work_item_session_id
       and target.wh                   = source.wh
       and target.facet_session_id     = source.facet_session_id)
   when MATCHED then
      update
         set wh_avail_qty    = source.wh_avail_qty,
             po_avail_qty    = source.po_avail_qty,
             tsf_avail_qty   = source.tsf_avail_qty,
             bol_avail_qty   = source.bol_avail_qty,
             asn_avail_qty   = source.asn_avail_qty,
             alloc_avail_qty = source.alloc_avail_qty;

   -- Merge Back for STYLE (This would be the total of all children)
   merge into alc_work_session_item_loc target
   using (select t.facet_session_id,
                 t.work_item_session_id,
                 t.wh,
                 t.wh_avail_qty,
                 t.po_avail_qty,
                 t.tsf_avail_qty,
                 t.bol_avail_qty,
                 t.asn_avail_qty,
                 t.alloc_avail_qty
            from (select awsi.facet_session_id,
                         awsi.work_item_session_id,
                         awsi.item,
                         awsil.wh wh,
                         SUM(awsil.wh_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                   awsil.wh) wh_avail_qty,
                         SUM(awsil.po_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                   awsil.wh) po_avail_qty,
                         SUM(awsil.tsf_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) tsf_avail_qty,
                         SUM(awsil.bol_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) bol_avail_qty,
                         SUM(awsil.asn_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                    awsil.wh) asn_avail_qty,
                         SUM(awsil.alloc_avail_qty) OVER(PARTITION BY awsi.work_item_session_id,
                                                                      awsil.wh) alloc_avail_qty,
                         row_number() OVER(PARTITION BY awsi.work_item_session_id,
                                                        awsil.wh
                                               ORDER BY awsil.work_item_loc_session_id) rnk
                    from alc_work_session_item awsi,
                         alc_work_session_item awsi2,
                         alc_work_session_item_loc awsil
                   where awsi.facet_session_id      = I_facet_session_id
                     and awsi.item_type             = ALC_CONSTANTS_SQL.STYLE
                     and awsi.work_item_session_id  = awsi2.ancestor_work_id
                     and awsil.work_item_session_id = awsi2.work_item_session_id) t
           where t.rnk = 1) source
   on (    target.work_item_session_id = source.work_item_session_id
       and target.wh                   = source.wh
       and target.facet_session_id     = source.facet_session_id)
   when MATCHED then
      update
         set wh_avail_qty    = source.wh_avail_qty,
             po_avail_qty    = source.po_avail_qty,
             tsf_avail_qty   = source.tsf_avail_qty,
             bol_avail_qty   = source.bol_avail_qty,
             asn_avail_qty   = source.asn_avail_qty,
             alloc_avail_qty = source.alloc_avail_qty;

   -- Remove All Items that has no stock of any type

   delete from alc_work_session_item_loc  awsil
    where L_what_if_ind                 = 0
      and awsil.facet_session_id        = I_facet_session_id
      and NVL(awsil.wh_avail_qty,  0)   = 0
      and NVL(awsil.po_avail_qty,  0)   = 0
      and NVL(awsil.tsf_avail_qty, 0)   = 0
      and NVL(awsil.bol_avail_qty, 0)   = 0
      and NVL(awsil.asn_avail_qty, 0)   = 0
      and NVL(awsil.alloc_avail_qty, 0) = 0;

   delete from alc_work_session_item awsi
    where awsi.facet_session_id = I_facet_session_id
      and awsi.item_type       != 'PACKCOMP'
      and NOT EXISTS (select 1
                        from alc_work_session_item_loc awsil
                       where awsil.facet_session_id     = I_facet_session_id
                         and awsil.work_item_session_id = awsi.work_item_session_id);

   delete from alc_work_session_item awsi
    where awsi.facet_session_id = I_facet_session_id
      and awsi.ancestor_work_id is NOT NULL
      and NOT EXISTS (select 1
                        from alc_work_session_item awsi1
                       where awsi1.facet_session_id     = I_facet_session_id
                         and awsi1.work_item_session_id = awsi.ancestor_work_id);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END FILTER_SOURCE;

------------------------------------------------------------------------------------------------------------
-- Name:    CREATE_ALLOC_SOURCE
-- Purpose: This function is used to create the allocation item source
--          It will populate the ALC_WORK_ITEM_SOURCE for the new work_header_id
--          and delete all the items from ALC_WORK_ITEM_SOURCE for the old work_header_id
--          with all the items that the user selects to create the allocation
------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ALLOC_SOURCE(O_error_message        IN OUT VARCHAR2,
                             O_new_work_header_id      OUT NUMBER,
                             O_alloc_type              OUT VARCHAR2,
                             I_facet_session_id     IN     VARCHAR2,
                             I_work_header_id       IN     NUMBER,
                             I_items                IN     ALC_VARCHAR_ID_TABLE,
                             I_what_if_ind          IN     NUMBER DEFAULT 0,
                             I_fpg_alloc_ind        IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program              VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.CREATE_ALLOC_SOURCE';
   L_start_time           TIMESTAMP     := SYSTIMESTAMP;
   L_new_work_header_id   NUMBER(15);
   L_awis                 OBJ_NUM_NUM_NUM_STR_TBL;
   L_obj_num_num_1        OBJ_NUM_NUM_DATE_TBL;
   L_obj_num_num_2        OBJ_NUM_NUM_DATE_TBL;
   L_max_level            NUMBER(10);
   L_min_level            NUMBER(10);
   L_fashion_item         NUMBER(10);
   L_fashion_pack         NUMBER(10);
   L_staple               NUMBER(10);
   L_what_if_ind          NUMBER(1)    := 0;
   L_doc_no               VARCHAR2(30) := NULL;

   cursor C_WHAT_IF is
      select 1
        from alc_work_item_source awis
       where awis.work_header_id = I_work_header_id
         and awis.source_type    = ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_WHAT_IF;

   cursor C_DOC is
      select DISTINCT i.doc_no
        from (select DISTINCT NVL(awis.doc_no,'-1') doc_no
                from table(cast(I_items as ALC_VARCHAR_ID_TABLE)) ids,
                     alc_work_item_source awis,
                     (select DISTINCT rfsa.value doc_no
                        from raf_facet_session_attribute rfsa,
                             raf_facet_attribute_cfg rfac
                       where rfsa.facet_session_id       = I_facet_session_id
                         and rfsa.selected               = 'Y'
                         and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                         and rfsa.parent_facet_session_attr_id is NOT NULL
                         and L_what_if_ind               = 0) raf
               where awis.work_header_id = I_work_header_id
                 and awis.item           = value(ids)
                 and awis.doc_no         = raf.doc_no
               union all
              select DISTINCT NVL(doc_no,'-1') doc_no
                from table(cast(I_items as ALC_VARCHAR_ID_TABLE)) ids,
                     alc_work_item_source awis
               where awis.work_header_id = I_work_header_id
                 and awis.item           = value(ids)) i;

   cursor C_AWIS is
      select OBJ_NUM_NUM_NUM_STR_REC(t.work_item_source_id,
                                     t.ancestor_id,
                                     t.lvl,
                                     NULL)
        from (select s.work_item_source_id,
                     s.ancestor_id,
                     s.lvl,
                     rank() OVER(PARTITION BY s.work_item_source_id
                                     ORDER BY s.lvl asc) rnk
                from (select awis.work_item_source_id,
                             awis.ancestor_id,
                             LEVEL lvl
                        from alc_work_item_source awis,
                             (select DISTINCT
                                     rfsa.value wh
                                from raf_facet_session_attribute rfsa,
                                     raf_facet_attribute_cfg rfac
                               where rfsa.facet_session_id       = I_facet_session_id
                                 and rfsa.selected               = 'Y'
                                 and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                 and rfac.attribute_name         = 'WH'
                                 and rfac.parent_attribute_value is NULL
                                 and L_what_if_ind               = 0
                              union all
                              select '-1'
                                from dual
                               where L_what_if_ind = 1
                             ) fct
                       where awis.work_header_id   = I_work_header_id
                         and NVL(awis.doc_no,'-1') = L_doc_no
                         and TO_CHAR(awis.wh)      = fct.wh
                       start with (    awis.item IN (select value(ids)
                                                       from table(cast(I_items as ALC_VARCHAR_ID_TABLE)) ids)
                                   and awis.item_type         != 'PACKCOMP'
                                   and awis.work_header_id     = I_work_header_id)
                     connect by prior awis.work_item_source_id = awis.ancestor_id
                     union all --1 LEVEL UP
                     select awis2.work_item_source_id,
                            awis2.ancestor_id,
                            0
                       from alc_work_item_source awis2,
                            (select DISTINCT awis.ancestor_id
                               from table(cast(I_items as ALC_VARCHAR_ID_TABLE)) ids,
                                    alc_work_item_source awis,
                                    (select DISTINCT
                                            rfsa.value wh
                                       from raf_facet_session_attribute rfsa,
                                            raf_facet_attribute_cfg rfac
                                      where rfsa.facet_session_id       = I_facet_session_id
                                        and rfsa.selected               = 'Y'
                                        and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                        and rfac.attribute_name         = 'WH'
                                        and rfac.parent_attribute_value is NULL
                                        and L_what_if_ind               = 0
                                     union all
                                     select '-1'
                                       from dual
                                      where L_what_if_ind = 1
                                    ) fct
                              where awis.work_header_id   = I_work_header_id
                                and NVL(awis.doc_no,'-1') = L_doc_no
                                and awis.item             = value(ids)
                                and awis.item_type       != 'PACKCOMP'
                                and TO_CHAR(awis.wh)      = fct.wh
                            ) i
                      where awis2.work_item_source_id = i.ancestor_id
                     union all --2 LEVEL UP
                     select DISTINCT awis3.work_item_source_id,
                                     awis3.ancestor_id,
                                     -1
                       from alc_work_item_source awis3,
                            alc_work_item_source awis2,
                            (select DISTINCT awis.ancestor_id
                               from table(cast(I_items as ALC_VARCHAR_ID_TABLE)) ids,
                                    alc_work_item_source awis,
                                    (select DISTINCT
                                            rfsa.value wh
                                       from raf_facet_session_attribute rfsa,
                                            raf_facet_attribute_cfg rfac
                                      where rfsa.facet_session_id       = I_facet_session_id
                                        and rfsa.selected               = 'Y'
                                        and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                        and rfac.attribute_name         = 'WH'
                                        and rfac.parent_attribute_value is NULL
                                        and L_what_if_ind               = 0
                                     union all
                                     select '-1'
                                       from dual
                                      where L_what_if_ind = 1
                                    ) fct
                              where awis.work_header_id   = I_work_header_id
                                and NVL(awis.doc_no,'-1') = L_doc_no
                                and awis.item             = value(ids)
                                and awis.item_type       != 'PACKCOMP'
                                and TO_CHAR(awis.wh)      = fct.wh
                            ) i
                      where awis2.work_item_source_id = i.ancestor_id
                        and awis3.work_item_source_id = awis2.ancestor_id
                    ) s) t
       where t.rnk = 1;

   cursor C_ALLOC_TYPE is
      select SUM(case
                    when awis.item_type IN (ALC_CONSTANTS_SQL.FASHIONITEM) then
                       1
                    else
                       0
                 end) fashion_item,
             SUM(case
                    when awis.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                            ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                            ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK) then
                       1
                    else
                       0
                 end) fashion_pack,
             SUM(case
                    when awis.item_type IN (ALC_CONSTANTS_SQL.FASHIONSKU,
                                            ALC_CONSTANTS_SQL.STAPLEITEM,
                                            ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                            ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                            ALC_CONSTANTS_SQL.SELLABLEPACK) then
                       1
                    else
                       0
                 end) staple
        from alc_work_item_source awis,
             table(cast(I_items as ALC_VARCHAR_ID_TABLE)) ids
       where awis.work_header_id = I_work_header_id
         and awis.item           = value(ids);

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   select alc_work_header_seq.NEXTVAL
     into L_new_work_header_id
     from dual;

   open C_WHAT_IF;
   fetch C_WHAT_IF into L_what_if_ind;
   close C_WHAT_IF;

   O_new_work_header_id := L_new_work_header_id;

   insert into alc_work_header(work_header_id,
                               work_type,
                               work_desc,
                               created_by,
                               updated_by,
                               created_date,
                               updated_date)
                        values(L_new_work_header_id,
                               'WD',
                               L_new_work_header_id,
                               USER,
                               USER,
                               SYSDATE,
                               SYSDATE);

   for rec in C_DOC loop

      L_doc_no := rec.doc_no;

      open C_AWIS;
      fetch C_AWIS BULK COLLECT into L_awis;
      close C_AWIS;

      if L_awis is NULL or L_awis.COUNT = 0 then
         O_error_message := ALC_CONSTANTS_SQL.ERRMSG_NO_WKST_ITEM;
         return 0;
      end if;

      select MAX(awis.number_3),
             MIN(awis.number_3)
        into L_max_level,
             L_min_level
        from table(cast(L_awis as OBJ_NUM_NUM_NUM_STR_TBL)) awis;

      delete gtt_num_num_str_str_date_date;

      for I in L_min_level..L_max_level loop

         if I = L_min_level then

            --this does top level (no ancestors) no matter the level
            insert all into alc_work_item_source(work_item_source_id,
                                                 work_header_id,
                                                 item,
                                                 item_type,
                                                 ancestor_id,
                                                 item_source_desc,
                                                 available_quantity,
                                                 backorder_qty,
                                                 wh,
                                                 doc_no,
                                                 source_type,
                                                 refresh_ind,
                                                 deleted_ind,
                                                 expected_to_date,
                                                 created_by,
                                                 updated_by,
                                                 created_date,
                                                 updated_date,
                                                 item_id)
                                          values(alc_work_item_source_seq.NEXTVAL,
                                                 L_new_work_header_id,
                                                 item,
                                                 item_type,
                                                 NULL,
                                                 item_source_desc,
                                                 available_quantity,
                                                 backorder_qty,
                                                 wh,
                                                 doc_no,
                                                 source_type,
                                                 refresh_ind,
                                                 'N',
                                                 expected_to_date,
                                                 USER,
                                                 USER,
                                                 SYSDATE,
                                                 SYSDATE,
                                                 item_id)
              into gtt_num_num_str_str_date_date(number_1,
                                                 number_2)
                                          values(org_work_item_source_id,
                                                 alc_work_item_source_seq.CURRVAL)
                                          select awis.item,
                                                 awis.item_type,
                                                 awis.item_source_desc,
                                                 awis.available_quantity,
                                                 awis.backorder_qty,
                                                 awis.wh,
                                                 awis.doc_no,
                                                 awis.source_type,
                                                 awis.refresh_ind,
                                                 awis.deleted_ind,
                                                 awis.expected_to_date,
                                                 awis.item_id,
                                                 ids.number_1 org_work_item_source_id
                                            from table(cast(L_awis as OBJ_NUM_NUM_NUM_STR_TBL)) ids,
                                                 alc_work_item_source awis
                                           where awis.work_item_source_id = ids.number_1
                                             and ids.number_2 is NULL;

            LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_ITEM_SOURCE L_min_level - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

         else

            insert all into alc_work_item_source(work_item_source_id,
                                                 work_header_id,
                                                 item,
                                                 item_type,
                                                 ancestor_id,
                                                 item_source_desc,
                                                 available_quantity,
                                                 backorder_qty,
                                                 wh,
                                                 doc_no,
                                                 source_type,
                                                 refresh_ind,
                                                 deleted_ind,
                                                 expected_to_date,
                                                 created_by,
                                                 updated_by,
                                                 created_date,
                                                 updated_date,
                                                 item_id,
                                                 diff_1,
                                                 diff_2,
                                                 diff_3)
                                          values(alc_work_item_source_seq.NEXTVAL,
                                                 L_new_work_header_id,
                                                 item,
                                                 item_type,
                                                 ancestor_id,
                                                 item_source_desc,
                                                 available_quantity,
                                                 backorder_qty,
                                                 wh,
                                                 doc_no,
                                                 source_type,
                                                 refresh_ind,
                                                 'N',
                                                 expected_to_date,
                                                 USER,
                                                 USER,
                                                 SYSDATE,
                                                 SYSDATE,
                                                 item_id,
                                                 diff_1,
                                                 diff_2,
                                                 diff_3)
              into gtt_num_num_str_str_date_date(number_1,
                                                 number_2)
                                          values(org_work_item_source_id,
                                                 alc_work_item_source_seq.CURRVAL)
                                          select awis.item,
                                                 awis.item_type,
                                                 awis.item_source_desc,
                                                 awis.available_quantity,
                                                 awis.backorder_qty,
                                                 awis.wh,
                                                 awis.doc_no,
                                                 awis.source_type,
                                                 awis.refresh_ind,
                                                 awis.deleted_ind,
                                                 awis.expected_to_date,
                                                 awis.item_id,
                                                 gtt.number_2 ancestor_id,
                                                 case
                                                    when im.diff_1_aggregate_ind = 'Y' then
                                                       awis.diff_1
                                                    else
                                                       NULL
                                                 end as diff_1,
                                                 case
                                                    when im.diff_2_aggregate_ind = 'Y' then
                                                       awis.diff_2
                                                    else
                                                       NULL
                                                 end as diff_2,
                                                 case
                                                    when im.diff_3_aggregate_ind = 'Y' then
                                                       awis.diff_3
                                                    else
                                                       NULL
                                                 end as diff_3,
                                                 ids.number_1 org_work_item_source_id
                                            from table(cast(L_awis as OBJ_NUM_NUM_NUM_STR_TBL)) ids,
                                                 alc_work_item_source awis,
                                                 gtt_num_num_str_str_date_date gtt,
                                                 item_master im
                                           where ids.number_3 = I
                                             and ids.number_2 is NOT NULL --make sure it does not try to process top level items
                                             and ((L_what_if_ind = 0
                                                   or (L_what_if_ind = 1 and awis.item_type NOT IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                                                                    ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK)))
                                                or (awis.item IN (select value(ids)
                                                                    from table(cast(I_items as ALC_VARCHAR_ID_TABLE)) ids)))
                                             and im.item(+)               = awis.item_id
                                             and awis.work_item_source_id = ids.number_1
                                             and gtt.number_1             = ids.number_2;

            LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_ITEM_SOURCE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

         end if;

      end loop;

   end loop;

   -- now remove all facets that are not selected
   -- Remove Un-Selected OH Quantity
   delete alc_work_item_source awis1
    where awis1.work_item_source_id IN (select awis.work_item_source_id
                                          from alc_work_item_source awis,
                                               (select DISTINCT rfsa.value wh
                                                  from raf_facet_session_attribute rfsa,
                                                       raf_facet_attribute_cfg rfac
                                                 where rfsa.facet_session_id       = I_facet_session_id
                                                   and NVL(rfsa.selected, 'N')     = 'N'
                                                   and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                                   and rfac.attribute_name         = 'WH'
                                                   and rfac.parent_attribute_value = 'OH') fct
                                         where awis.work_header_id = L_new_work_header_id
                                           and TO_CHAR(awis.wh)    = fct.wh
                                           and awis.source_type    = 'OH');

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_ITEM_SOURCE - Unselected OH Qty - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --- Deletes the items source
   if I_what_if_ind != 1 then
      delete alc_work_item_source
       where work_header_id = L_new_work_header_id
         and NVL(available_quantity,-9999) < 1;
   end if;

   -- Remove Un-Selected PO
   -- Delete the record from the alc_work_item_source
   delete alc_work_item_source
    where work_item_source_id IN (select work_item_source_id
                                    from alc_work_item_source awis,
                                         (select DISTINCT rfsa.value doc_no
                                            from raf_facet_session_attribute rfsa,
                                                 raf_facet_attribute_cfg rfac
                                           where rfsa.facet_session_id       = I_facet_session_id
                                             and NVL(rfsa.selected, 'N')     = 'N'
                                             and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                             and rfac.attribute_name         = 'DOC_NO'
                                             and rfac.parent_attribute_value = 'PO') fct
                                   where awis.work_header_id = L_new_work_header_id
                                     and awis.source_type    = 'PO'
                                     and awis.doc_no         = fct.doc_no);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_ITEM_SOURCE - Unselected PO - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Remove Un-Selected ASN
   -- Delete the record from the alc_work_item_source
   delete alc_work_item_source
    where work_item_source_id IN (select work_item_source_id
                                    from alc_work_item_source awis,
                                         (select DISTINCT rfsa.value doc_no
                                            from raf_facet_session_attribute rfsa,
                                                 raf_facet_attribute_cfg rfac
                                           where rfsa.facet_session_id       = I_facet_session_id
                                             and NVL(rfsa.selected, 'N')     = 'N'
                                             and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                             and rfac.attribute_name         = 'DOC_NO'
                                             and rfac.parent_attribute_value = 'ASN') fct
                                   where awis.work_header_id = L_new_work_header_id
                                     and awis.source_type    = 'ASN'
                                     and awis.doc_no         = fct.doc_no);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_ITEM_SOURCE - Unselected ASN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Remove Un-Selected BOL
   -- Delete the record from the alc_work_item_source
   delete alc_work_item_source
    where work_item_source_id IN (select work_item_source_id
                                    from alc_work_item_source awis,
                                         (select DISTINCT rfsa.value doc_no
                                            from raf_facet_session_attribute rfsa,
                                                 raf_facet_attribute_cfg rfac
                                           where rfsa.facet_session_id       = I_facet_session_id
                                             and NVL(rfsa.selected, 'N')     = 'N'
                                             and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                             and rfac.attribute_name         = 'DOC_NO'
                                             and rfac.parent_attribute_value = 'BOL') fct
                                   where awis.work_header_id = L_new_work_header_id
                                     and awis.source_type    = 'BOL'
                                     and awis.doc_no         = fct.doc_no);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_ITEM_SOURCE - Unselected BOL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Remove Un-Selected TSF
   -- Delete the record from the alc_work_item_source
   delete alc_work_item_source
    where work_item_source_id in (select work_item_source_id
                                    from alc_work_item_source awis,
                                         (select DISTINCT rfsa.value doc_no
                                            from raf_facet_session_attribute rfsa,
                                                 raf_facet_attribute_cfg rfac
                                           where rfsa.facet_session_id       = I_facet_session_id
                                             and NVL(rfsa.selected, 'N')     = 'N'
                                             and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                             and rfac.attribute_name         = 'DOC_NO'
                                             and rfac.parent_attribute_value = 'TSF') fct
                                   where awis.work_header_id = L_new_work_header_id
                                     and awis.source_type    = 'TSF'
                                     and awis.doc_no         = fct.doc_no);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_ITEM_SOURCE - Unselected TSF - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Remove Un-Selected ALLOC
   -- Delete the record from the alc_work_item_source
   delete alc_work_item_source
    where work_item_source_id IN (select work_item_source_id
                                    from alc_work_item_source awis,
                                         (select DISTINCT rfsa.value doc_no
                                            from raf_facet_session_attribute rfsa,
                                                 raf_facet_attribute_cfg rfac
                                           where rfsa.facet_session_id       = I_facet_session_id
                                             and NVL(rfsa.selected, 'N')     = 'N'
                                             and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                             and rfac.attribute_name         = 'DOC_NO'
                                             and rfac.parent_attribute_value = 'ALLOC') fct
                                   where awis.work_header_id = L_new_work_header_id
                                     and awis.source_type    = 'ALLOC'
                                     and awis.doc_no         = fct.doc_no);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_ITEM_SOURCE - Unselected ALLOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_ALLOC_TYPE;
   fetch C_ALLOC_TYPE into L_fashion_item,
                           L_fashion_pack,
                           L_staple;
   close C_ALLOC_TYPE;

   if L_fashion_item > 0 then
      O_alloc_type := 'FA';
   elsif L_fashion_pack > 0 and NVL(I_fpg_alloc_ind, 0) = 1 then
      O_alloc_type := 'FPG';
   else
      O_alloc_type := 'SA';
   end if;

   if POPULATE_WORK_ITEM_SOURCE_DIFF(O_error_message,
                                     L_new_work_header_id) = FALSE then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END CREATE_ALLOC_SOURCE;
------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_WORK_ITEM_SOURCE_DIFF(O_error_message    IN OUT VARCHAR2,
                                        I_work_header_id   IN     ALC_WORK_ITEM_SOURCE.WORK_HEADER_ID%TYPE)
RETURN BOOLEAN IS


   L_program     VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.POPULATE_WORK_ITEM_SOURCE_DIFF';
   L_work_type   ALC_WORK_HEADER.WORK_TYPE%TYPE;
   L_insert      VARCHAR2(25000) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program);

   select work_type
     into L_work_type
     from alc_work_header
    where work_header_id = I_work_header_id;

   if L_work_type = ALC_CONSTANTS_SQL.WORKSHEET_TYPE_WORK then
      return TRUE;
   end if;

   delete
     from alc_work_item_source_diff
    where work_header_id = I_work_header_id;

   insert into alc_work_item_source_diff(work_header_id,
                                         item,
                                         item_type,
                                         item_aggregate_ind,
                                         diff_1_aggregate_ind,
                                         diff_2_aggregate_ind,
                                         diff_3_aggregate_ind,
                                         diff_4_aggregate_ind,
                                         item_parent_desc,
                                         diff_1,
                                         diff_2,
                                         diff_3,
                                         diff_4,
                                         diff_1_desc,
                                         diff_2_desc,
                                         diff_3_desc,
                                         diff_4_desc)
                                  select DISTINCT s.work_header_id,
                                         s.item,
                                         s.item_type,
                                         --
                                         imp.item_aggregate_ind,
                                         imp.diff_1_aggregate_ind,
                                         imp.diff_2_aggregate_ind,
                                         imp.diff_3_aggregate_ind,
                                         imp.diff_4_aggregate_ind,
                                         imp.item_desc,
                                         --
                                         im.diff_1,
                                         im.diff_2,
                                         im.diff_3,
                                         im.diff_4,
                                         --
                                         d1.diff_desc,
                                         d2.diff_desc,
                                         d3.diff_desc,
                                         d4.diff_desc
                                    from alc_work_item_source s,
                                         item_master im,
                                         item_master imp,
                                         diff_ids d1,
                                         diff_ids d2,
                                         diff_ids d3,
                                         diff_ids d4
                                   where s.work_header_id  = I_work_header_id
                                     and s.item_type       = ALC_CONSTANTS_SQL.FASHIONSKU
                                     and s.item            = im.item
                                     and im.item_parent    = imp.item
                                     and im.diff_1         = d1.diff_id(+)
                                     and im.diff_2         = d2.diff_id(+)
                                     and im.diff_3         = d3.diff_id(+)
                                     and im.diff_4         = d4.diff_id(+)
                                   --
                                  union all
                                   --
                                  select DISTINCT s.work_header_id,
                                         s.item,
                                         s.item_type,
                                         --
                                         NULL item_aggregate_ind,
                                         NULL diff_1_aggregate_ind,
                                         NULL diff_2_aggregate_ind,
                                         NULL diff_3_aggregate_ind,
                                         NULL diff_4_aggregate_ind,
                                         NULL item_parent_desc,
                                         --
                                         NULL diff_1,
                                         NULL diff_2,
                                         NULL diff_3,
                                         NULL diff_4,
                                         --
                                         NULL diff_desc,
                                         NULL diff_desc,
                                         NULL diff_desc,
                                         NULL diff_desc
                                    from alc_work_item_source s
                                   where s.work_header_id  = I_work_header_id
                                     and s.item_type       IN (ALC_CONSTANTS_SQL.STAPLEITEM,
                                                               ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                               ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                               ALC_CONSTANTS_SQL.SELLABLEPACK)
                                  --
                                  union all
                                  --
                                  select DISTINCT s.work_header_id,
                                         s.item,
                                         s.item_type,
                                         --
                                         imp.item_aggregate_ind,
                                         imp.diff_1_aggregate_ind,
                                         imp.diff_2_aggregate_ind,
                                         imp.diff_3_aggregate_ind,
                                         imp.diff_4_aggregate_ind,
                                         imp.item_desc,
                                         --
                                         im.diff_1,
                                         im.diff_2,
                                         im.diff_3,
                                         im.diff_4,
                                         --
                                         d1.diff_desc,
                                         d2.diff_desc,
                                         d3.diff_desc,
                                         d4.diff_desc
                                    from alc_work_item_source s,
                                         packitem_breakout pb,
                                         item_master im,
                                         item_master imp,
                                         diff_ids d1,
                                         diff_ids d2,
                                         diff_ids d3,
                                         diff_ids d4
                                   where s.work_header_id  = I_work_header_id
                                     and s.item_type       IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK)
                                     and s.item            = pb.pack_no
                                     and pb.item           = im.item
                                     and im.item_parent    = imp.item
                                     and im.diff_1         = d1.diff_id(+)
                                     and im.diff_2         = d2.diff_id(+)
                                     and im.diff_3         = d3.diff_id(+)
                                     and im.diff_4         = d4.diff_id(+);

   L_insert := q'[
      insert into alc_work_item_source_diff(work_header_id,
                                            item,
                                            item_type,
                                            item_aggregate_ind,
                                            diff_1_aggregate_ind,
                                            diff_2_aggregate_ind,
                                            diff_3_aggregate_ind,
                                            diff_4_aggregate_ind,
                                            item_parent_desc,
                                            diff_1,
                                            diff_2,
                                            diff_3,
                                            diff_4,
                                            diff_1_desc,
                                            diff_2_desc,
                                            diff_3_desc,
                                            diff_4_desc)
                                     select DISTINCT s.work_header_id,
                                            s.item,
                                            s.item_type,
                                            --
                                            imp.item_aggregate_ind,
                                            imp.diff_1_aggregate_ind,
                                            imp.diff_2_aggregate_ind,
                                            imp.diff_3_aggregate_ind,
                                            imp.diff_4_aggregate_ind,
                                            imp.item_desc,
                                            --
                                            STRAGG(DISTINCT im.diff_1),
                                            STRAGG(DISTINCT im.diff_2),
                                            STRAGG(DISTINCT im.diff_3),
                                            STRAGG(DISTINCT im.diff_4),
                                            --
                                            STRAGG(DISTINCT d1.diff_desc),
                                            STRAGG(DISTINCT d2.diff_desc),
                                            STRAGG(DISTINCT d3.diff_desc),
                                            STRAGG(DISTINCT d4.diff_desc)
                                       from alc_work_item_source s,
                                            packitem_breakout pb,
                                            item_master im,
                                            item_master imp,
                                            diff_ids d1,
                                            diff_ids d2,
                                            diff_ids d3,
                                            diff_ids d4
                                      where s.work_header_id  = :1
                                        and s.item_type       IN ('NSFSCP','NSFMCP') --ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                                                     --ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK
                                        and s.item            = pb.pack_no
                                        and pb.item           = im.item
                                        and im.item_parent    = imp.item
                                        and im.diff_1         = d1.diff_id(+)
                                        and im.diff_2         = d2.diff_id(+)
                                        and im.diff_3         = d3.diff_id(+)
                                        and im.diff_4         = d4.diff_id(+)
                                      group by s.work_header_id,
                                               s.item,
                                               s.item_type,
                                               --
                                               imp.item_aggregate_ind,
                                               imp.diff_1_aggregate_ind,
                                               imp.diff_2_aggregate_ind,
                                               imp.diff_3_aggregate_ind,
                                               imp.diff_4_aggregate_ind,
                                               imp.item_desc
   ]';

   EXECUTE IMMEDIATE L_insert using I_work_header_id;

   LOGGER.LOG_INFORMATION('END: ' || L_program);

   return TRUE;

EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_WORK_ITEM_SOURCE_DIFF;
------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
-- Name:    ADD_ITEM_TO_ALLOC_SOURCE
-- Purpose: This function adds item to the Allocation Item Source
------------------------------------------------------------------------------------------------------------
FUNCTION ADD_ITEM_TO_ALLOC_SOURCE(O_error_message          IN OUT VARCHAR2,
                                  O_work_item_source_ids      OUT OBJ_NUMERIC_ID_TABLE,
                                  I_facet_session_id       IN     VARCHAR2,
                                  I_work_header_id         IN     NUMBER,
                                  I_item_nodes             IN     ALC_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program      VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.ADD_ITEM_TO_ALLOC_SOURCE';
   L_start_time   TIMESTAMP     := SYSTIMESTAMP;

   cursor C_ITEM_SOURCE is
      select /*+ CARDINALITY(ids 10) */
             awis.work_item_source_id
        from alc_work_item_source awis,
             table(cast(I_item_nodes as ALC_VARCHAR_ID_TABLE)) ids
       where awis.work_header_id = I_work_header_id
         and awis.item = value(ids)
         -- Remove Un-selected WH
         and NOT EXISTS (select 1
                           from (select rfsa.value wh
                                   from raf_facet_session_attribute rfsa,
                                        raf_facet_attribute_cfg rfac
                                  where rfsa.facet_session_id       = I_facet_session_id
                                    and NVL(rfsa.selected, 'N')     = 'N'
                                    and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                    and rfac.attribute_name         = 'WH'
                                    and rfac.parent_attribute_value is NULL
                                    and rownum > 0) rfsa
                          where awis.wh = rfsa.wh)
         -- Remove Un-selected OH Quantity
         and NOT EXISTS (select 1
                           from (select rfsa.value wh
                                   from raf_facet_session_attribute rfsa,
                                        raf_facet_attribute_cfg rfac
                                  where rfsa.facet_session_id       = I_facet_session_id
                                    and NVL(rfsa.selected, 'N')     = 'N'
                                    and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                                    and rfac.attribute_name         = 'WH'
                                    and rfac.parent_attribute_value = 'OH'
                                    and rownum > 0) rfsa
                          where awis.wh          = rfsa.wh
                            and awis.source_type = 'OH')
         -- Remove Un-selected PO
         and NOT EXISTS (select 1
                           from raf_facet_session_attribute rfsa,
                                raf_facet_attribute_cfg rfac
                          where rfsa.facet_session_id       = I_facet_session_id
                            and NVL(rfsa.selected, 'N')     = 'N'
                            and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                            and rfac.attribute_name         = 'DOC_NO'
                            and rfac.parent_attribute_value = 'PO'
                            and awis.doc_no                 = rfsa.value
                            and awis.source_type            = 'PO')
         -- Remove Un-selected ASN
         and NOT EXISTS (select 1
                           from raf_facet_session_attribute rfsa,
                                raf_facet_attribute_cfg rfac
                          where rfsa.facet_session_id       = I_facet_session_id
                            and NVL(rfsa.selected, 'N')     = 'N'
                            and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                            and rfac.attribute_name         = 'DOC_NO'
                            and rfac.parent_attribute_value = 'ASN'
                            and awis.doc_no                 = rfsa.value
                            and awis.source_type            = 'ASN')
         -- Remove Un-selected BOL
         and NOT EXISTS (select 1
                           from raf_facet_session_attribute rfsa,
                                raf_facet_attribute_cfg rfac
                          where rfsa.facet_session_id       = I_facet_session_id
                            and NVL(rfsa.selected, 'N')     = 'N'
                            and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                            and rfac.attribute_name         = 'DOC_NO'
                            and rfac.parent_attribute_value = 'BOL'
                            and awis.doc_no                 = rfsa.value
                            and awis.source_type            = 'BOL')
         -- Remove Un-selected TSF
         and NOT EXISTS (select 1
                           from raf_facet_session_attribute rfsa,
                                raf_facet_attribute_cfg rfac
                          where rfsa.facet_session_id       = I_facet_session_id
                            and NVL(rfsa.selected, 'N')     = 'N'
                            and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                            and rfac.attribute_name         = 'DOC_NO'
                            and rfac.parent_attribute_value = 'TSF'
                            and awis.doc_no                 = rfsa.value
                            and awis.source_type            = 'TSF')
         -- Remove Un-selected ALLOC
         and NOT EXISTS (select 1
                           from raf_facet_session_attribute rfsa,
                                raf_facet_attribute_cfg rfac
                          where rfsa.facet_session_id       = I_facet_session_id
                            and NVL(rfsa.selected, 'N')     = 'N'
                            and rfsa.facet_attribute_cfg_id = rfac.facet_attribute_cfg_id
                            and rfac.attribute_name         = 'DOC_NO'
                            and rfac.parent_attribute_value = 'ALLOC'
                            and awis.doc_no                 = rfsa.value
                            and awis.source_type            = 'ALLOC');

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   open C_ITEM_SOURCE;
   fetch C_ITEM_SOURCE BULK COLLECT into O_work_item_source_ids;
   close C_ITEM_SOURCE;

   delete alc_work_item_source
    where work_header_id = I_work_header_id
	  and source_type != 'WHIF'
      and NVL(available_quantity,-9999) < 1;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ADD_ITEM_TO_ALLOC_SOURCE;
------------------------------------------------------------------------------------------------------------
-- Name    : GET_NSF_PACK_STYLE_COUNT
--
-- Purpose : This function accepts a list of non sellable fashion packs
--         : and returns a count of those that have skus from a single style
--         : and a count of those that have skus from multiple styles.
------------------------------------------------------------------------------------------------------------
FUNCTION GET_NSF_PACK_STYLE_COUNT(O_error_message        IN OUT VARCHAR2,
                                  O_single_style_count      OUT NUMBER,
                                  O_multi_style_count       OUT NUMBER,
                                  I_items                IN     ALC_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program   VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.GET_NSF_PACK_STYLE_COUNT';

   cursor C_SINGLE_STYLE is
      select SUM(DECODE(i.parent_cnt, 1, 1, 0)) single_parent,
             SUM(DECODE(i.parent_cnt, 1, 0, 1)) multiple_parent
        from (select DISTINCT im_pack.item,
                     COUNT(DISTINCT im_comp.item_parent) OVER (PARTITION BY im_pack.item) parent_cnt
                from table(cast(I_items as ALC_VARCHAR_ID_TABLE)) input,
                     item_master im_pack,
                     packitem_breakout pb,
                     item_master im_comp,
                     item_master im_parent
               where value(input)        = im_pack.item
                 and im_pack.item        = pb.pack_no
                 and pb.item             = im_comp.item
                 and im_comp.item_parent = im_parent.item(+)) i;

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program);

   open C_SINGLE_STYLE;
   fetch C_SINGLE_STYLE into O_single_style_count,
                             O_multi_style_count;
   close C_SINGLE_STYLE;

   LOGGER.LOG_INFORMATION('END: ' || L_program);

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END GET_NSF_PACK_STYLE_COUNT;
------------------------------------------------------------------------------------------------------------
-- Name:    VALIDATE_FPG_ITEMS
-- Purpose: This function is to validate that all Packs and SKUs that are
--          selection for Fashion Pack Group (FPG) allocation are valid.
--          Valid is defined as all Pack Components and SKUs are from
--          ONE Style (i.e. all Pack Components and SKUs have the same Parent
--          Items). There must also be no Staple items in a FPG allocation
--          O_valid : empty ==> Valid, FPG Allocation can be created.
--                    ALC_CONSTANTS_SQL.ERRMSG_FPG_SINGLEPARENT_VALID ==> Invalid, FPG Allocation should not be allowed
--            ALC_CONSTANTS_SQL.ERRMSG_FPG_STAPLE_VALID ==> Invalid, FPG Allocation should not be allowed
------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_FPG_ITEMS(O_error_message   IN OUT VARCHAR2,
                            O_valid              OUT ALC_VARCHAR_ID_TABLE,
                            I_items           IN     ALC_VARCHAR_ID_TABLE)

RETURN NUMBER IS

   L_program   VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.VALIDATE_FPG_ITEMS';
   L_parents   NUMBER;
   L_staples   NUMBER;
   L_valid     ALC_VARCHAR_ID_TABLE := ALC_VARCHAR_ID_TABLE();


BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program);

   select COUNT(DISTINCT item_parent)
     into L_parents
     from (select NVL(im.item_parent,'A'||rownum) item_parent
             from table (cast(I_items as ALC_VARCHAR_ID_TABLE)) ids,
                          item_master im
            where im.item = value(ids)
              and im.pack_ind != 'Y'
          union all
           select NVL(im.item_parent,'B'||rownum) item_parent
             from table (cast (I_items as ALC_VARCHAR_ID_TABLE)) ids,
                  packitem_breakout pb,
                  item_master im
            where value(ids) = pb.pack_no
              and im.item = pb.item);

    select COUNT(DISTINCT item)
      into L_staples
      from (select NVL(item, to_char(-1 * rownum)) item
              from (select im.item
                      from table (cast(I_items as ALC_VARCHAR_ID_TABLE)) ids,
                           item_master im
                     where im.item      = value(ids)
                       and im.pack_ind != 'Y'
                       and im.item_parent is NULL
                     union
                    select im.item
                      from table (cast(I_items as ALC_VARCHAR_ID_TABLE)) ids,
                           item_master im,
                           Item_master parent
                     where im.item                   = value(ids)
                       and im.pack_ind              != 'Y'
                       and parent.item               = im.item_parent
                       and parent.item_aggregate_ind = 'N'
                     union
                      select DISTINCT pb.pack_no item
                        from table (cast(I_items as ALC_VARCHAR_ID_TABLE)) ids,
                             item_master im,
                             packitem_breakout pb
                       where pb.pack_no   = value(ids)
                         and im.item      = pb.item
                         and im.pack_ind != 'Y'
                         and im.item_parent is NULL
                     union
                      select DISTINCT pb.pack_no item
                        from table (cast(I_items as ALC_VARCHAR_ID_TABLE)) ids,
                             item_master im,
                             item_master parent,
                             packitem_breakout pb
                       where pb.pack_no                = value(ids)
                         and im.item                   = pb.item
                         and im.pack_ind              != 'Y'
                         and parent.item               = im.item_parent
                         and parent.item_aggregate_ind = 'N'
                        ));


   if L_staples > 0 then
      L_valid.extend;
      L_valid(L_valid.COUNT) := ALC_CONSTANTS_SQL.ERRMSG_FPG_STAPLE_VALID;
   end if;

   if L_valid.COUNT > 0 then
      O_valid := L_valid;
   end if;

   LOGGER.LOG_INFORMATION('END: ' || L_program);

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN 0;
END VALIDATE_FPG_ITEMS;

------------------------------------------------------------------------------------------------------------
-- Name:    PURGE_SESSION_TABLES
-- Purpose: This function is used to purge session tables
--          ALC_WORK_SESSION_ITEM
--          ALC_WORK_SESSION_ITEM_LOC
--          RAF_FACET_SESSION
--          RAF_FACET_SESSION_ATTRIBUTE
--          if I_facet_session_id is NULL then it will truncate tables
--          if I_facet_session_id is NOT NULL then it will delete records
--             for that session
------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_SESSION_TABLES(O_error_message      IN OUT VARCHAR2,
                              I_facet_session_id   IN     VARCHAR2)
RETURN NUMBER IS

   L_program                       VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.PURGE_SESSION_TABLES';
   L_truncate_session_item         VARCHAR2(100) := 'truncate table ALC_WORK_SESSION_ITEM reuse storage';
   L_truncate_session_item_all     VARCHAR2(100) := 'truncate table ALC_WORK_SESSION_ITEM_ALL reuse storage';
   L_truncate_session_item_loc     VARCHAR2(100) := 'truncate table alc_work_session_item_loc reuse storage';
   L_truncate_facet_session        VARCHAR2(100) := 'truncate table RAF_FACET_SESSION reuse storage';
   L_truncate_facet_session_attr   VARCHAR2(100) := 'truncate table RAF_FACET_SESSION_ATTRIBUTE reuse storage';

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program);

   if I_facet_session_id is NOT NULL then

      delete
        from alc_work_session_item_loc
       where facet_session_id = I_facet_session_id;

      delete
        from alc_work_session_item
       where facet_session_id = I_facet_session_id;

      delete
        from alc_work_session_item_all
       where facet_session_id = I_facet_session_id;

      delete
        from raf_facet_session_attribute
       where facet_session_id = I_facet_session_id;

      delete
        from raf_facet_session
       where facet_session_id = I_facet_session_id;

   else

      EXECUTE IMMEDIATE L_truncate_session_item_loc;
      EXECUTE IMMEDIATE L_truncate_session_item;
      EXECUTE IMMEDIATE L_truncate_session_item_all;
      EXECUTE IMMEDIATE L_truncate_facet_session_attr;
      EXECUTE IMMEDIATE L_truncate_facet_session;
   end if;

   LOGGER.LOG_INFORMATION('END: ' || L_program);

   return 1;

EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END PURGE_SESSION_TABLES;
-----------------------------------------------------------------------------------
FUNCTION CLOSE_FACET_SESSION(O_error_message      IN OUT VARCHAR2,
                             I_facet_session_id   IN     ALC_SESSION_ITEM_LOC.FACET_SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program   VARCHAR2(50) := 'ALC_HUB_FACET_CFG_SQL.CLOSE_FACET_SESSION';

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program);

   delete
     from alc_work_session_item_loc
    where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_SESSION_ITEM_LOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_work_session_item
    where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_SESSION_ITEM - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_work_session_item_all
    where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_SESSION_ITEM_ALL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('END: ' || L_program);

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return ALC_CONSTANTS_SQL.FAILURE;
END CLOSE_FACET_SESSION;
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
--UPDATES the Style /Color items only if sku's underneath a style color gets deleted in ALC_WORK_ITEM_SOURCE table.
-----------------------------------------------------------------------------------
FUNCTION UPDATE_DISABLE_IND(O_error_message      IN OUT VARCHAR2,
                            I_work_header_id     IN     NUMBER,
                            I_facet_session_id   IN     VARCHAR2,
                            I_items              IN     ALC_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program   VARCHAR2(100)             := 'ALC_ALLOC_QUANTITY_LIMITS_SQL.UPDATE_DISABLE_IND';
   L_awis      OBJ_NUM_NUM_NUM_STR_TBL;
   L_awis1     OBJ_NUM_NUM_NUM_STR_TBL;

   cursor C_AWIS is
      select OBJ_NUM_NUM_NUM_STR_REC(NULL,
                                     NULL,
                                     t.work_item_source_id ,
                                     t.item)
        from (select work_item_source_id,
                     item
                from alc_work_item_source
               where work_header_id = I_work_header_id
                 and item_type='FA'
               start with (work_item_source_id IN (select DISTINCT ancestor_id
                                                     from alc_work_item_source
                                                    where item IN (select value(ids)
                                                                     from table(cast(I_items as ALC_VARCHAR_ID_TABLE)) ids)
                                                      and work_header_id =I_work_header_id
                                                      and item_type IN ('FASHIONSKU',
                                                                        'NSFSCP',
                                                                        'NSFSP')))
             connect by prior work_item_source_id = ancestor_id) t;

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program);

   open C_AWIS;
   fetch C_AWIS BULK COLLECT into L_awis;
   close C_AWIS;

   if L_awis is NULL or
      L_awis.count = 0 then
      return 1;
   end if;

   merge into alc_work_item_source target
   using (select ids.NUMBER_3 work_item_source_id
            from table(cast(L_awis as OBJ_NUM_NUM_NUM_STR_TBL)) ids ) source
   on (target.work_item_source_id = source.work_item_source_id)
   when MATCHED then
      update
         set target.disable_ind = 'Y';

   merge into alc_work_session_item target
   using ( select DISTINCT ids.string item
             from table(cast(L_awis as OBJ_NUM_NUM_NUM_STR_TBL)) ids ) source
      on (    target.item      = source.item
          and facet_session_id = I_facet_session_id)
   when MATCHED then
      update
         set target.disable_ind = 'Y';

   LOGGER.LOG_INFORMATION('END: ' || L_program);

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END UPDATE_DISABLE_IND;
-----------------------------------------------------------------------------------
-- Two Passes performed in Filter Source Function Before the Actual Filter is Performed.
-- The Return Value DISABLED_FACET_ATTRIBUTES_TBL is the actual search criteria which is constructed based on the below rules.
-- Parent-Style/Color:
-----------------------------------------------------------------------------------
-- S.No  Parent   Parent-Diff        Remark    Results         Facets
-- 1     Checked  All Checked                                   Parent: CHecked , Parent-diff :Checked
-- 2     Checked  At least UnChecked                            Parent: CHecked , Parent-diff : Selected Parent Diff UnChecked
-- 3   UnChecked  All Checked                                   Parent: UnChecked , Parent-diff :All UnChecked
-- 4   UnChecked  At least UnChecked                            Parent: CHecked , Parent-diff :Selected Parent Diff UnChecked
-- 5     Checked  All UnChecked                                Parent: CHecked , Parent-diff :Checked
-----------------------------------------------------------------------------------

-- Style/Color: Sku/Packs
-----------------------------------------------------------------------------------
-- S.No Style/Color   Sku/Packs         Remark    Results         Facets
-- 1     Checked      All Checked                                 Style/Color: Checked , Sku/Packs :Checked
-- 2     Checked      At least UnChecked                          Style/Color: Checked , Sku/Packs : Selected  Sku/Packs  UnChecked
-- 3   UnChecked      All Checked                                 Style/Color: UnChecked , Sku/Packs :All UnChecked
-- 4   UnChecked      At least UnChecked                          Style/Color: CHecked , Sku/Packs :Selected  Sku/Packs UnChecked
-- 5     Checked      All UnChecked                               Style/Color: CHecked , Sku/Packs :Checked
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
FUNCTION UPDATE_FACETS_ON_RULE(O_error_message             IN OUT VARCHAR2,
                             I_facet_session_id            IN     VARCHAR2,
                             I_LEVEL                       IN     NUMBER,
                             I_disabled_facet_attributes   IN OUT DISABLED_FACET_ATTRIBUTES_TBL)
RETURN NUMBER IS

   L_program     VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.UPDATE_FACETS_ON_RULE';
   L_awis        OBJ_STR_STR_STR_STR_STR_TBL;
   L_predicate   NUMBER := NULL;
   L_level       NUMBER := NULL;
   L_disable     DISABLED_FACET_ATTRIBUTES_TBL;

   cursor C_AWIS is
      select OBJ_STR_STR_STR_STR_STR_REC(t.item,
                                         t.child_item,
                                         TO_CHAR(t.facet_attribute_cfg_id),
                                         NULL,
                                         NULL)
        from (select DISTINCT
                     i.item,
                     i.child_item,
                     i.child_count,
                     i.child_uncheck_ind_count,
                     i.checked,
                     i.facet_attribute_cfg_id
                from (select awsi.item,
                             awsi.item_type,
                             awsi.work_item_session_id,
                               awsi.ancestor_work_id,
                             awsi_child.item child_item,
                             raf.facet_attribute_cfg_id,
                              --
                              --
                             DECODE(parent_inds.facet_attribute_value,
                                    awsi.item, 'N',
                                    'Y') as checked,
                             COUNT(awsi_child.item) OVER (PARTITION BY awsi.item) as child_count,
                             COUNT(child_inds.facet_attribute_value) OVER (PARTITION BY awsi.item) as child_uncheck_ind_count
                        from alc_work_session_item awsi,
                             alc_work_session_item awsi_child,
                             raf_facet_session_attribute raf,
                             raf_facet_attribute_cfg rfac,
                             table (cast(I_disabled_facet_attributes as DISABLED_FACET_ATTRIBUTES_TBL) ) child_inds,
                             table (cast(I_disabled_facet_attributes as DISABLED_FACET_ATTRIBUTES_TBL) ) parent_inds
                       where awsi.facet_session_id       = I_facet_session_id
                         and awsi.work_item_session_id   = awsi_child.ancestor_work_id(+)
                         and awsi_child.item             = child_inds.facet_attribute_value(+)
                         and awsi.item                   = parent_inds.facet_attribute_value(+)
                         and awsi_child.item             is NOT NULL
                         and awsi_child.item             = raf.value(+)
                         and rfac.facet_attribute_cfg_id = raf.facet_attribute_cfg_id
                         and rfac.attribute_name in (ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT,
                                                     ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT_DIFF,
                                                     ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PACK,
                                                     ALC_HUB_FACET_CFG_SQL.ITEM_FACET_SKU)
                         and raf.facet_session_id  = I_facet_session_id
                         and (case
                                 when L_level = 1 and awsi.ancestor_work_id is NULL then
                                    1
                                 when (L_level = 2 and awsi.ancestor_work_id is NOT NULL) then
                                    1
                                 else
                                    0
                              end) = 1
                     ) i
               where (case
                         when (L_predicate = 1 and i.checked = 'N' and i.child_uncheck_ind_count = 0 and i.child_count > 0) then
                            1
                         when (L_predicate = 2 and  i.checked = 'N' and i.child_uncheck_ind_count  > 0) then
                            1
                         when (L_predicate = 3 and  i.checked = 'Y' and i.child_count  = i.child_uncheck_ind_count) then
                            1
                         else
                            0
                      end) = 1) t;


    cursor C_DISABLE is
    select DISABLED_FACET_ATTRIBUTE_REC(t.level1,
                                        t.item)
      from (select DISTINCT number_1 level1,
                   varchar2_1 item
             from  gtt_num_num_str_str_date_date tempobjectgtt) t;

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program);

   insert into gtt_num_num_str_str_date_date(number_1,
                                             varchar2_1)
                                      select filter.facet_attribute_cfg_id,
                                             filter.facet_attribute_value
                                        from table (cast(I_disabled_facet_attributes as DISABLED_FACET_ATTRIBUTES_TBL)) filter;
    -- Condition 3 : See The Conditions Above

   L_level := I_level;

   L_predicate := 1;
   L_awis      := NULL;

   open C_AWIS;
   fetch C_AWIS BULK COLLECT into L_awis;
   close C_AWIS;

   if L_awis is NOT NULL and
      L_awis.COUNT > 0 then

      insert into gtt_num_num_str_str_date_date (number_1,
                                                 varchar2_1)
                                          select TO_NUMBER(t.facet_attribute_cfg_id) childfacet_cfg_id,
                                                 t.childitem childvalue
                                            from (select ids.string_1 item,
                                                         ids.string_2 childitem,
                                                         ids.string_3 facet_attribute_cfg_id
                                                    from table(cast(L_awis as OBJ_STR_STR_STR_STR_STR_TBL)) ids ) t,
                                                         raf_facet_session_attribute rafsa
                                           where t.item = rafsa.value
                                            and  rafsa.facet_session_id = I_facet_session_id;
   end if;

   L_predicate := 2;
   L_awis      := NULL;

   open C_AWIS;
   fetch C_AWIS BULK COLLECT into L_awis;
   close C_AWIS;

   if L_awis is NOT NULL and
      L_awis.COUNT > 0 then
      -- Condition 4 : See The Conditions Above
         delete gtt_num_num_str_str_date_date
          where varchar2_1 IN (select ids.string_1 item
                                 from table(cast(L_awis as OBJ_STR_STR_STR_STR_STR_TBL)) ids);
   end if;

   L_predicate := 3;
   L_awis      := NULL;

   open C_AWIS;
   fetch C_AWIS BULK COLLECT into L_awis;
   close C_AWIS;

   if L_awis is NOT NULL and
      L_awis.COUNT > 0 then
         --Condition 4 : See The Conditions Above
         delete gtt_num_num_str_str_date_date
          where varchar2_1 IN (select ids.string_2 childitem
                                 from table(cast(L_awis as OBJ_STR_STR_STR_STR_STR_TBL)) ids);
   end if;

    I_disabled_facet_attributes  := NULL;
 --- Take the values from GTT and place it in the Object.
    open C_DISABLE;
    fetch C_DISABLE BULK COLLECT into I_disabled_facet_attributes;
    close C_DISABLE;

    if I_disabled_facet_attributes is NULL or
       I_disabled_facet_attributes.COUNT = 0 then
       return 0;
    end if;

   LOGGER.LOG_INFORMATION('END: ' || L_program);

   return 1;

EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END UPDATE_FACETS_ON_RULE;
-----------------------------------------------------------------------------------
FUNCTION POP_DESCRIPTIONS(O_error_message      IN  OUT VARCHAR2,
                          I_facet_session_id   IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'ALC_HUB_FACET_CFG_SQL.POP_DESCRIPTIONS';

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program);

   merge into raf_facet_session_attribute target
   using (select a.facet_session_attribute_id,
                 im.item_desc||' '||SUBSTR(value, INSTR(value,' ')) value_desc
            from raf_facet_session_attribute a,
                 raf_facet_attribute_cfg c,
                 item_master im
           where a.facet_session_id       = I_facet_session_id
             and a.facet_attribute_cfg_id = c.facet_attribute_cfg_id
             and im.item                  = RTRIM(SUBSTR(a.value, 1, INSTR(a.value,' ')))
             and c.attribute_name         = ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT_DIFF
          union all
          select a.facet_session_attribute_id,
                 im.item_desc value_desc
            from raf_facet_session_attribute a,
                 raf_facet_attribute_cfg c,
                 item_master im
           where a.facet_session_id       = I_facet_session_id
             and a.facet_attribute_cfg_id = c.facet_attribute_cfg_id
             and a.value                  = im.item
             and c.attribute_name         IN (ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PARENT,
                                              ALC_HUB_FACET_CFG_SQL.ITEM_FACET_PACK,
                                              ALC_HUB_FACET_CFG_SQL.ITEM_FACET_SKU)
          union all
          select a.facet_session_attribute_id,
                 w.wh_name value_desc
            from raf_facet_session_attribute a,
                 raf_facet_attribute_cfg c,
                 wh w
           where a.facet_session_id            = I_facet_session_id
             and a.facet_attribute_cfg_id      = c.facet_attribute_cfg_id
             and a.value                       = TO_CHAR(w.wh)
             and c.attribute_name              = ALC_HUB_FACET_CFG_SQL.FACET_ATTRIB_WH
             and c.parent_facet_attr_cfg_id    is NULL
         ) use_this
   on (target.facet_session_attribute_id = use_this.facet_session_attribute_id)
   when MATCHED then
      update
         set target.value_desc = use_this.value_desc;

   LOGGER.LOG_INFORMATION('END: ' || L_program);

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_DESCRIPTIONS;
-----------------------------------------------------------------------------------
END ALC_HUB_FACET_CFG_SQL;
/

