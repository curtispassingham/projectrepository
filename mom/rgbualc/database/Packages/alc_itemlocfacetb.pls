CREATE OR REPLACE PACKAGE BODY ALC_ITEM_LOC_FACET_SQL IS
-----------------------------------------------------------------------------------------------------------------------
FUNCTION CLOSE_FACET_SESSION(O_error_message      IN OUT VARCHAR2,
                             I_facet_session_id   IN     ALC_SESSION_ITEM_LOC.FACET_SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(50) := 'ALC_ITEM_LOC_FACET_SQL.CLOSE_FACET_SESSION';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   delete
     from alc_session_quantity_limits
    where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_QUANTITY_LIMITS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_session_item_loc
    where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_ITEM_LOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_session_item_loc_excl
    where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_ITEM_LOC_EXCL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return ALC_CONSTANTS_SQL.FAILURE;
END CLOSE_FACET_SESSION;
-----------------------------------------------------------------------------------------------------------------------
END ALC_ITEM_LOC_FACET_SQL;
/
