CREATE OR REPLACE PACKAGE ALC_ITEM_LOC_FACET_SQL AS
-----------------------------------------------------------------------------------------------------------------------
FUNCTION CLOSE_FACET_SESSION(O_error_message      IN OUT VARCHAR2,
                             I_facet_session_id   IN     ALC_SESSION_ITEM_LOC.FACET_SESSION_ID%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------------------
END ALC_ITEM_LOC_FACET_SQL;
/


