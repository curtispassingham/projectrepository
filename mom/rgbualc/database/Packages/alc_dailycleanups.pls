CREATE OR REPLACE PACKAGE ALC_DAILY_CLEAN_UP_SQL AS
-------------------------------------------------------------------------------------------------------------
FUNCTION CLEAR_DATA(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_ALLOC_PURGE(O_error_message     IN OUT VARCHAR2,
                           I_max_thread_size   IN     NUMBER,
                           O_num_threads          OUT NUMBER)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_ALLOC(O_error_message   IN OUT VARCHAR2,
                     I_thread_id       IN     NUMBER)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_WORKSHEET_PURGE(O_error_message     IN OUT VARCHAR2,
                               I_max_thread_size   IN     NUMBER,
                               O_num_threads          OUT NUMBER)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_WORKSHEET(O_error_message    IN OUT VARCHAR2,
                         I_thread_id        IN     NUMBER)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION SHRINK_SESSION_TEMP_TBLS(O_error_message IN OUT VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
END ALC_DAILY_CLEAN_UP_SQL;
/
