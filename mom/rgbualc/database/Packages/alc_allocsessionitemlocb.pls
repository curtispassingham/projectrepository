CREATE OR REPLACE PACKAGE BODY ALC_ALLOC_SESSION_ITEM_LOC_SQL IS
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
--alter session set plsql_ccflags = 'UTPLSQL:TRUE'
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
------------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_SESSION_ITEM_LOC(O_error_message          IN OUT VARCHAR2,
                                   I_alloc_id               IN     NUMBER,
                                   I_facet_session_id       IN     VARCHAR2,
                                   I_work_header_id         IN     NUMBER,
                                   I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                                   I_alc_location_attr      IN     ALC_LOCATION_ATTR_TBL,
                                   I_alloc_type             IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SESSION_ITEM_LOC_NOT_FPG(O_error_message          IN OUT VARCHAR2,
                                      I_alloc_id               IN     NUMBER,
                                      I_facet_session_id       IN     VARCHAR2,
                                      I_work_header_id         IN     NUMBER,
                                      I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                                      I_alc_location_attr      IN     ALC_LOCATION_ATTR_TBL,
                                      I_alloc_type             IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SESSION_ITEM_LOC_FPG(O_error_message          IN OUT VARCHAR2,
                                  I_alloc_id               IN     NUMBER,
                                  I_facet_session_id       IN     VARCHAR2,
                                  I_work_header_id         IN     NUMBER,
                                  I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                                  I_alc_location_attr      IN     ALC_LOCATION_ATTR_TBL,
                                  I_alloc_type             IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_SESSION_QTY_LIMITS(O_error_message      IN OUT VARCHAR2,
                                     I_alloc_id           IN     NUMBER,
                                     I_facet_session_id   IN     VARCHAR2,
                                     I_alloc_type         IN     ALC_ALLOC.TYPE%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_QTY(O_error_message    IN OUT VARCHAR2,
                      I_facet_session_id IN     VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_EXCLUSIONS(O_error_message          IN OUT VARCHAR2,
                        I_facet_session_id       IN     VARCHAR2,
                        I_work_header_id         IN     NUMBER,
                        I_alloc_type             IN     ALC_ALLOC.TYPE%TYPE,
                        I_what_if_ind            IN     NUMBER DEFAULT 0,
                        I_nfrc_supp_chain_ind    IN     NUMBER DEFAULT 0,
                        I_size_profile           IN     ALC_RULE.SIZE_PROFILE%TYPE DEFAULT NULL,
                        I_size_profile_type      IN     ALC_RULE.SIZE_PROFILE_TYPE%TYPE DEFAULT NULL,
                        I_pack_threshold         IN     ALC_RULE.PACK_THRESHOLD%TYPE DEFAULT NULL,
                        I_rule_mode              IN     ALC_RULE.RULE_MODE%TYPE DEFAULT NULL,
                        I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                        I_spwhavailpct_ind       IN     ALC_RULE.SIZE_PROFILE_WH_AVAIL_PCT_IND%TYPE DEFAULT NULL,
                        I_spdflthier_ind         IN     ALC_RULE.SIZE_PROFILE_DEFAULT_HIER_IND%TYPE DEFAULT NULL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------
FUNCTION FIND_SIZE_PROFILE(O_error_message      IN OUT VARCHAR2,
                           I_facet_session_id   IN     VARCHAR2,
                           I_alloc_type         IN     ALC_ALLOC.TYPE%TYPE,
                           I_size_profile       IN     ALC_RULE.SIZE_PROFILE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SESSION_ITEM_LOC_FROM_TEMP(O_error_message      IN OUT VARCHAR2,
                                        I_facet_session_id   IN     VARCHAR2,
                                        I_work_header_id     IN     NUMBER)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_SESSION_TEMP_TABLES(O_error_message      IN OUT VARCHAR2,
                                   I_facet_session_id   IN     VARCHAR2,
                                   I_work_header_id     IN     NUMBER)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_SESSION_RULE_PRIORITY(O_error_message      IN OUT VARCHAR2,
                                        I_facet_session_id   IN     VARCHAR2,
                                        I_alloc_id           IN     NUMBER)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SESSION_STORE_WH_PRIORITY(O_error_message      IN OUT VARCHAR2,
                                       I_facet_session_id   IN     VARCHAR2,
                                       I_alloc_id           IN     NUMBER)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SAMEORG_SAMECHANNEL_RULE(O_error_message      IN OUT VARCHAR2,
                                      I_facet_session_id   IN     VARCHAR2,
                                      I_alloc_id           IN     NUMBER,
                                      I_transfer_basis     IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SAMEORG_DIFFCHANNEL_RULE(O_error_message      IN OUT VARCHAR2,
                                      I_facet_session_id   IN     VARCHAR2,
                                      I_alloc_id           IN     NUMBER,
                                      I_transfer_basis     IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_DIFFORG_SAMECHANNEL_RULE(O_error_message      IN OUT VARCHAR2,
                                      I_facet_session_id   IN     VARCHAR2,
                                      I_alloc_id           IN     NUMBER,
                                      I_transfer_basis     IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_DIFFORG_DIFFCHANNEL_RULE(O_error_message      IN OUT VARCHAR2,
                                      I_facet_session_id   IN     VARCHAR2,
                                      I_alloc_id           IN     NUMBER,
                                      I_transfer_basis     IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------
$end
------------------------------------------------------------------------------------------------------------------
FUNCTION ADD_ITEM_LOC(O_error_message          IN OUT VARCHAR2,
                      I_alloc_id               IN     NUMBER,
                      I_facet_session_id       IN     VARCHAR2,
                      I_work_header_id         IN     NUMBER,
                      I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                      I_alc_location_attr      IN     ALC_LOCATION_ATTR_TBL,
                      I_alloc_type             IN     VARCHAR2 DEFAULT NULL,
                      I_what_if_ind            IN     NUMBER DEFAULT 0,
                      I_nfrc_supp_chain_ind    IN     NUMBER DEFAULT 0,
                      I_size_profile           IN     ALC_RULE.SIZE_PROFILE%TYPE DEFAULT NULL,
                      I_size_profile_type      IN     ALC_RULE.SIZE_PROFILE_TYPE%TYPE DEFAULT NULL,
                      I_pack_threshold         IN     ALC_RULE.PACK_THRESHOLD%TYPE DEFAULT NULL,
                      I_rule_mode              IN     ALC_RULE.RULE_MODE%TYPE DEFAULT NULL,
                      I_spwhavailpct_ind       IN     ALC_RULE.SIZE_PROFILE_WH_AVAIL_PCT_IND%TYPE DEFAULT NULL,
                      I_spdflthier_ind         IN     ALC_RULE.SIZE_PROFILE_DEFAULT_HIER_IND%TYPE DEFAULT NULL)
RETURN NUMBER AS

   L_program      VARCHAR2(50) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.ADD_ITEM_LOC';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if PURGE_SESSION_TEMP_TABLES(O_error_message,
                                I_facet_session_id,
                                I_work_header_id) = FALSE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   if POPULATE_SESSION_ITEM_LOC(O_error_message,
                                I_alloc_id,
                                I_facet_session_id,
                                I_work_header_id,
                                I_alc_item_source_attr,
                                I_alc_location_attr,
                                I_alloc_type) = 0 then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   if I_nfrc_supp_chain_ind = 0 then
      if POPULATE_SESSION_RULE_PRIORITY(O_error_message,
                                        I_facet_session_id,
                                        I_alloc_id) = 0 then
         return ALC_CONSTANTS_SQL.FAILURE;
      end if;
   end if;

   if POP_EXCLUSIONS(O_error_message,
                     I_facet_session_id,
                     I_work_header_id,
                     I_alloc_type,
                     I_what_if_ind,
                     I_nfrc_supp_chain_ind,
                     I_size_profile,
                     I_size_profile_type,
                     I_pack_threshold,
                     I_rule_mode,
                     I_alc_item_source_attr,
                     I_spwhavailpct_ind,
                     I_spdflthier_ind) = FALSE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   if POPULATE_SESSION_QTY_LIMITS(O_error_message,
                                  I_alloc_id,
                                  I_facet_session_id,
                                  I_alloc_type) = 0 then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   if POP_SESSION_ITEM_LOC_FROM_TEMP(O_error_message,
                                     I_facet_session_id,
                                     I_work_header_id) = FALSE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return ALC_CONSTANTS_SQL.FAILURE;
END ADD_ITEM_LOC;
------------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_SESSION_ITEM_LOC(O_error_message          IN OUT VARCHAR2,
                                   I_alloc_id               IN     NUMBER,
                                   I_facet_session_id       IN     VARCHAR2,
                                   I_work_header_id         IN     NUMBER,
                                   I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                                   I_alc_location_attr      IN     ALC_LOCATION_ATTR_TBL,
                                   I_alloc_type             IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER IS

   L_program  VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POPULATE_SESSION_ITEM_LOC';

BEGIN

   -- Process non Fashion Pack Group (FPG) Allocation
   if NVL(I_alloc_type, 'NOT FPG') != ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION then

      if POP_SESSION_ITEM_LOC_NOT_FPG(O_error_message,
                                      I_alloc_id,
                                      I_facet_session_id,
                                      I_work_header_id,
                                      I_alc_item_source_attr,
                                      I_alc_location_attr,
                                      I_alloc_type) = ALC_CONSTANTS_SQL.FAILURE then
         return ALC_CONSTANTS_SQL.FAILURE;
      end if;

   else --FPG

      if POP_SESSION_ITEM_LOC_FPG(O_error_message,
                                  I_alloc_id,
                                  I_facet_session_id,
                                  I_work_header_id,
                                  I_alc_item_source_attr,
                                  I_alc_location_attr,
                                  I_alloc_type) = ALC_CONSTANTS_SQL.FAILURE then
         return ALC_CONSTANTS_SQL.FAILURE;
      end if;

   end if;


   if SET_PACK_QTY(O_error_message,
                   I_facet_session_id) = FALSE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return ALC_CONSTANTS_SQL.FAILURE;
END POPULATE_SESSION_ITEM_LOC;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SESSION_ITEM_LOC_NOT_FPG(O_error_message          IN OUT VARCHAR2,
                                      I_alloc_id               IN     NUMBER,
                                      I_facet_session_id       IN     VARCHAR2,
                                      I_work_header_id         IN     NUMBER,
                                      I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                                      I_alc_location_attr      IN     ALC_LOCATION_ATTR_TBL,
                                      I_alloc_type             IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER IS

   L_program      VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POP_SESSION_ITEM_LOC_NOT_FPG';
   L_start_time   TIMESTAMP     := SYSTIMESTAMP;
   L_awis         OBJ_NUM_NUM_NUM_STR_TBL;
   L_max_level    NUMBER(10);


BEGIN

   -- Insert all items that has no parents
   merge into alc_session_itemloc_gtt target
   using (with session_no_parents as
             (select I_facet_session_id facet_session_id,
                     I_alloc_id alloc_id,
                     i.item_node item_id,
                     i.item_desc,
                     i.item_type,
                     i.wh,
                     i.release_date,
                     i.loc_id location_id,
                     i.loc_desc,
                     i.group_id,
                     i.group_desc,
                     i.group_type,
                     i.loc_group_id,
                     'N' freeze_ind,
                     i.diff1_id,
                     i.diff1_desc,
                     i.diff2_id,
                     i.diff2_desc,
                     i.diff3_id,
                     i.diff3_desc,
                     i.item_parent_id,
                     i.in_store_date,
                     i.order_no,
                     i.source_type,
                     'N'  filtered_ind,
                     USER created_by,
                     USER updated_by,
                     SYSDATE created_date,
                     SYSDATE update_date,
                     i.loc_type
               from (select /*+ CARDINALITY(it 100) CARDINALITY(loc 100) */
                            DISTINCT
                            it.item_node,
                            awis.item_source_desc item_desc,
                            it.item_type,
                            it.wh,
                            it.release_date,
                            loc.loc_id,
                            loc.loc_desc,
                            loc.group_id,
                            loc.group_desc,
                            loc.group_type,
                            loc.loc_group_id,
                            awis.diff_1 diff1_id,
                            diff1.diff_desc diff1_desc,
                            awis.diff_2 diff2_id,
                            diff2.diff_desc diff2_desc,
                            awis.diff_3 diff3_id,
                            diff3.diff_desc diff3_desc,
                            awis.item_id item_parent_id,
                            loc.in_store_date,
                            it.order_no,
                            it.source_type,
                            loctype.loc_type
                       from alc_work_item_source awis,
                            table (cast(I_alc_item_source_attr as ALC_ITEM_SOURCE_ATTR_TBL)) it,
                            table (cast(I_alc_location_attr as ALC_LOCATION_ATTR_TBL)) loc,
                            diff_ids diff1,
                            diff_ids diff2,
                            diff_ids diff3,
                            (select store loc,
                                    'S' loc_type
                               from store s
                              union all
                             select w.wh loc,
                                   'W' loc_type
                               from wh w) loctype
                      where awis.work_header_id    = I_work_header_id
                        and it.item_node           = awis.item
                        and it.item_type           = awis.item_type
                        and it.wh                  = awis.wh
                        and loc.loc_id             = loctype.loc
                        --
                        and NVL(it.order_no,-9999) = NVL(awis.doc_no,-9999)
                        and DECODE(awis.source_type,
                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_PO,      ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_PO,
                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ASN,     ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ASN,
                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_OH,      ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_OH,
                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_BOL,     ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_BOL,
                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_WHAT_IF, ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF,
                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_TSF,     ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_TSF,
                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ALLOC,   ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ALLOC) = it.source_type
                        --
                        and NVL(awis.disable_ind, 'N') != 'Y'
                        and awis.diff_1                 = diff1.diff_id (+)
                        and awis.diff_2                 = diff2.diff_id (+)
                        and awis.diff_3                 = diff3.diff_id (+)) i)
          --
          select snp.facet_session_id,
                 NULL item_loc_id,
                 snp.alloc_id,
                 snp.item_id,
                 snp.item_desc,
                 snp.item_type,
                 snp.wh,
                 snp.release_date,
                 snp.location_id,
                 snp.loc_desc,
                 snp.group_id,
                 snp.group_desc,
                 snp.group_type,
                 snp.loc_group_id,
                 NVL(asil.allocated_qty, NULL) allocated_qty,
                 NULL calculated_qty,
                 NULL need_qty,
                 NULL on_hand_qty,
                 NULL som_qty,
                 NULL backorder_qty,
                 snp.freeze_ind,
                 NULL next_1_week_qty,
                 NULL next_2_week_qty,
                 NULL next_3_week_qty,
                 snp.diff1_id,
                 snp.diff1_desc,
                 snp.diff2_id,
                 snp.diff2_desc,
                 snp.diff3_id,
                 snp.diff3_desc,
                 snp.item_parent_id,
                 NULL created_order_no,
                 NULL created_supplier_id,
                 NULL future_unit_retail,
                 NULL rush_flag,
                 NULL cost,
                 snp.in_store_date,
                 NULL future_on_hand_qty,
                 snp.order_no,
                 snp.source_type,
                 NULL gross_need_qty,
                 NULL rloh_qty,
                 snp.filtered_ind,
                 NULL parent_item_loc_session_id,
                 NVL(asil.pack_comp_qty, NULL) pack_comp_qty,
                 snp.created_by,
                 snp.updated_by,
                 snp.created_date,
                 snp.update_date,
                 NULL object_version_id,
                 snp.loc_type
            from session_no_parents snp,
                 alc_session_itemloc_gtt asil
           where snp.facet_session_id  = I_facet_session_id
             and snp.facet_session_id  = asil.facet_session_id (+)
             and snp.item_id           = asil.item_id (+)
             and snp.wh                = asil.wh_id (+)
             and snp.location_id       = asil.location_id (+)
         ) use_this
   on (    target.facet_session_id = use_this.facet_session_id
       and target.item_id          = use_this.item_id
       and target.location_id      = use_this.location_id
      )
   when NOT MATCHED then
      insert (item_loc_session_id,
              facet_session_id,
              item_loc_id,
              alloc_id,
              item_id,
              item_desc,
              item_type,
              wh_id,
              release_date,
              location_id,
              location_desc,
              group_id,
              group_desc,
              group_type,
              loc_group_id,
              allocated_qty,
              calculated_qty,
              need_qty,
              on_hand_qty,
              som_qty,
              backorder_qty,
              freeze_ind,
              next_1_week_qty,
              next_2_week_qty,
              next_3_week_qty,
              diff1_id,
              diff1_desc,
              diff2_id,
              diff2_desc,
              diff3_id,
              diff3_desc,
              parent_item_id,
              created_order_no,
              created_supplier_id,
              future_unit_retail,
              rush_flag,
              cost,
              in_store_date,
              future_on_hand_qty,
              order_no,
              source_type,
              gross_need_qty,
              rloh_qty,
              filtered_ind,
              parent_item_loc_session_id,
              pack_comp_qty,
              created_by,
              updated_by,
              created_date,
              update_date,
              object_version_id,
              loc_type)
      values (alc_session_item_loc_seq.NEXTVAL,
              use_this.facet_session_id,
              use_this.item_loc_id,
              use_this.alloc_id,
              use_this.item_id,
              use_this.item_desc,
              use_this.item_type,
              use_this.wh,
              use_this.release_date,
              use_this.location_id,
              use_this.loc_desc,
              use_this.group_id,
              use_this.group_desc,
              use_this.group_type,
              use_this.loc_group_id,
              use_this.allocated_qty,
              use_this.calculated_qty,
              use_this.need_qty,
              use_this.on_hand_qty,
              use_this.som_qty,
              use_this.backorder_qty,
              use_this.freeze_ind,
              use_this.next_1_week_qty,
              use_this.next_2_week_qty,
              use_this.next_3_week_qty,
              use_this.diff1_id,
              use_this.diff1_desc,
              use_this.diff2_id,
              use_this.diff2_desc,
              use_this.diff3_id,
              use_this.diff3_desc,
              use_this.item_parent_id,
              use_this.created_order_no,
              use_this.created_supplier_id,
              use_this.future_unit_retail,
              use_this.rush_flag,
              use_this.cost,
              use_this.in_store_date,
              use_this.future_on_hand_qty,
              use_this.order_no,
              use_this.source_type,
              use_this.gross_need_qty,
              use_this.rloh_qty,
              use_this.filtered_ind,
              use_this.parent_item_loc_session_id,
              use_this.pack_comp_qty,
              use_this.created_by,
              use_this.updated_by,
              use_this.created_date,
              use_this.update_date,
              use_this.object_version_id,
              use_this.loc_type);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_SESSION_ITEMLOC_GTT - NO PARENTS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Insert all descendants.
   merge into alc_session_itemloc_gtt target
   using (with session_descendants as
             (select I_facet_session_id facet_session_id,
                     I_alloc_id alloc_id,
                     i.item item_id,
                     i.item_desc,
                     i.item_type,
                     i.wh wh_id,
                     i.release_date,
                     i.loc_id location_id,
                     i.loc_desc location_desc,
                     i.group_id,
                     i.group_desc,
                     i.group_type,
                     i.loc_group_id,
                     'N' freeze_ind,
                     i.parent_item_id,
                     i.in_store_date,
                     i.order_no,
                     i.source_type,
                     'N' filtered_ind,
                     i.item_loc_session_id,
                     USER created_by,
                     USER updated_by,
                     SYSDATE created_date,
                     SYSDATE update_date,
                     i.loc_type
                from (select /*+ ORDERED CARDINALITY(it 100) CARDINALITY(loc 100) */
                             DISTINCT
                             awis.item,
                             awis.item_source_desc item_desc,
                             awis.item_type,
                             it.wh,
                             it.release_date,
                             loc.loc_id,
                             loc.loc_desc,
                             loc.group_id,
                             loc.group_desc,
                             loc.group_type,
                             loc.loc_group_id,
                             parent.item_id parent_item_id,
                             loc.in_store_date,
                             it.order_no,
                             it.source_type,
                             asil.item_loc_session_id,
                             loctype.loc_type
                        from table (cast(I_alc_item_source_attr as ALC_ITEM_SOURCE_ATTR_TBL)) it,
                             alc_work_item_source parent,
                             alc_work_item_source awis,
                             alc_session_itemloc_gtt asil,
                             table (cast(I_alc_location_attr as ALC_LOCATION_ATTR_TBL)) loc,
                             (select store loc,
                                     'S' loc_type
                                from store s
                               union all
                              select w.wh loc,
                                     'W' loc_type
                                 from wh w) loctype
                       where parent.work_header_id       = I_work_header_id
                         and it.item_node                = parent.item
                         and it.item_type                = parent.item_type
                         and it.wh                       = parent.wh
                         and it.item_type                = ALC_CONSTANTS_SQL.FASHIONITEM
                         and loc.loc_id                  = loctype.loc
                         --
                         and awis.ancestor_id            = parent.work_item_source_id
                         --
                         and NVL(it.order_no,-9999)      = NVL(awis.doc_no,-9999)
                         and DECODE(awis.source_type,
                                    ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_PO,      ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_PO,
                                    ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ASN,     ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ASN,
                                    ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_OH,      ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_OH,
                                    ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_BOL,     ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_BOL,
                                    ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_WHAT_IF, ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF,
                                    ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_TSF,     ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_TSF,
                                    ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ALLOC,   ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ALLOC) = it.source_type
                         --
                         and NVL(awis.disable_ind, 'N') != 'Y'
                         --
                         and asil.facet_session_id       = I_facet_session_id
                         and asil.item_id                = it.item_node
                         and asil.wh_id                  = it.wh
                         and NVL(asil.order_no, -9999)   = NVL(it.order_no, -9999)
                         and asil.source_type            = it.source_type
                         and asil.location_id            = loc.loc_id) i)
          --
          select sd.facet_session_id,
                 NULL item_loc_id,
                 sd.alloc_id,
                 sd.item_id,
                 sd.item_desc,
                 sd.item_type,
                 sd.wh_id,
                 sd.release_date,
                 sd.location_id,
                 sd.location_desc,
                 sd.group_id,
                 sd.group_desc,
                 sd.group_type,
                 sd.loc_group_id,
                 NVL(asil.allocated_qty, NULL) allocated_qty,
                 NULL calculated_qty,
                 NULL need_qty,
                 NULL on_hand_qty,
                 NULL som_qty,
                 NULL backorder_qty,
                 sd.freeze_ind,
                 NULL next_1_week_qty,
                 NULL next_2_week_qty,
                 NULL next_3_week_qty,
                 NULL diff1_id,
                 NULL diff1_desc,
                 NULL diff2_id,
                 NULL diff2_desc,
                 NULL diff3_id,
                 NULL diff3_desc,
                 sd.parent_item_id,
                 NULL created_order_no,
                 NULL created_supplier_id,
                 NULL future_unit_retail,
                 NULL rush_flag,
                 NULL cost,
                 sd.in_store_date,
                 NULL future_on_hand_qty,
                 sd.order_no,
                 sd.source_type,
                 NULL gross_need_qty,
                 NULL rloh_qty,
                 sd.filtered_ind,
                 sd.item_loc_session_id parent_item_loc_session_id,
                 sd.created_by,
                 sd.updated_by,
                 sd.created_date,
                 sd.update_date,
                 NULL object_version_id,
                 sd.loc_type
            from session_descendants sd,
                 alc_session_itemloc_gtt asil
           where sd.facet_session_id   = I_facet_session_id
             and sd.facet_session_id   = asil.facet_session_id (+)
             and sd.item_id            = asil.item_id (+)
             and sd.wh_id              = asil.wh_id (+)
             and sd.location_id        = asil.location_id (+)
         ) use_this
   on (    target.facet_session_id = use_this.facet_session_id
       and target.item_id          = use_this.item_id
       and target.location_id      = use_this.location_id
      )
   when NOT MATCHED then
      insert (item_loc_session_id,
              facet_session_id,
              item_loc_id,
              alloc_id,
              item_id,
              item_desc,
              item_type,
              wh_id,
              release_date,
              location_id,
              location_desc,
              group_id,
              group_desc,
              group_type,
              loc_group_id,
              allocated_qty,
              calculated_qty,
              need_qty,
              on_hand_qty,
              som_qty,
              backorder_qty,
              freeze_ind,
              next_1_week_qty,
              next_2_week_qty,
              next_3_week_qty,
              diff1_id,
              diff1_desc,
              diff2_id,
              diff2_desc,
              diff3_id,
              diff3_desc,
              parent_item_id,
              created_order_no,
              created_supplier_id,
              future_unit_retail,
              rush_flag,
              cost,
              in_store_date,
              future_on_hand_qty,
              order_no,
              source_type,
              gross_need_qty,
              rloh_qty,
              filtered_ind,
              parent_item_loc_session_id,
              created_by,
              updated_by,
              created_date,
              update_date,
              object_version_id,
              loc_type)
      values (alc_session_item_loc_seq.NEXTVAL,
              use_this.facet_session_id,
              use_this.item_loc_id,
              use_this.alloc_id,
              use_this.item_id,
              use_this.item_desc,
              use_this.item_type,
              use_this.wh_id,
              use_this.release_date,
              use_this.location_id,
              use_this.location_desc,
              use_this.group_id,
              use_this.group_desc,
              use_this.group_type,
              use_this.loc_group_id,
              use_this.allocated_qty,
              use_this.calculated_qty,
              use_this.need_qty,
              use_this.on_hand_qty,
              use_this.som_qty,
              use_this.backorder_qty,
              use_this.freeze_ind,
              use_this.next_1_week_qty,
              use_this.next_2_week_qty,
              use_this.next_3_week_qty,
              use_this.diff1_id,
              use_this.diff1_desc,
              use_this.diff2_id,
              use_this.diff2_desc,
              use_this.diff3_id,
              use_this.diff3_desc,
              use_this.parent_item_id,
              use_this.created_order_no,
              use_this.created_supplier_id,
              use_this.future_unit_retail,
              use_this.rush_flag,
              use_this.cost,
              use_this.in_store_date,
              use_this.future_on_hand_qty,
              use_this.order_no,
              use_this.source_type,
              use_this.gross_need_qty,
              use_this.rloh_qty,
              use_this.filtered_ind,
              use_this.parent_item_loc_session_id,
              use_this.created_by,
              use_this.updated_by,
              use_this.created_date,
              use_this.update_date,
              use_this.object_version_id,
              use_this.loc_type);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_SESSION_ITEMLOC_GTT - ALL DESCENDANTS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return ALC_CONSTANTS_SQL.FAILURE;
END POP_SESSION_ITEM_LOC_NOT_FPG;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SESSION_ITEM_LOC_FPG(O_error_message        IN OUT VARCHAR2,
                                  I_alloc_id             IN     NUMBER,
                                  I_facet_session_id     IN     VARCHAR2,
                                  I_work_header_id       IN     NUMBER,
                                  I_alc_item_source_attr IN     ALC_ITEM_SOURCE_ATTR_TBL,
                                  I_alc_location_attr    IN     ALC_LOCATION_ATTR_TBL,
                                  I_alloc_type           IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER IS

   L_program      VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POP_SESSION_ITEM_LOC_FPG';
   L_start_time   TIMESTAMP     := SYSTIMESTAMP;
   L_parent_item_loc_session_id   NUMBER(15);

   cursor C_FPG is
      select DISTINCT
             t.fpg_style_id,
             t.fpg_style_desc,
             t.item_node,
             t.item_type,
             t.item_desc,
             t.wh,
             t.release_date,
             t.diff1_id,
             t.diff2_id,
             t.diff3_id,
             t.item_parent_id,
             t.order_no,
             t.source_type,
             t.loc_id,
             t.loc_desc,
             t.group_id,
             t.group_desc,
             t.group_type,
             t.loc_group_id,
             t.in_store_date,
             t.diff1_desc,
             t.diff2_desc,
             t.diff3_desc,
             t.rnk_wh_loc,
             t.loc_type
        from (select /*+ CARDINALITY(it 100) CARDINALITY(loc 100) */
                     DISTINCT
                     it.fpg_style_id,
                     awis_inner.style_desc fpg_style_desc,
                     it.item_node,
                     it.item_type,
                     awis_inner.item_source_desc item_desc,
                     it.wh,
                     it.release_date,
                     awis_inner.diff1_id,
                     awis_inner.diff2_id,
                     awis_inner.diff3_id,
                     awis_inner.style_id item_parent_id,
                     it.order_no,
                     it.source_type,
                     loc.loc_id,
                     loc.loc_desc,
                     loc.group_id,
                     loc.group_desc,
                     loc.group_type,
                     loc.loc_group_id,
                     loc.in_store_date,
                     diff1.diff_desc diff1_desc,
                     diff2.diff_desc diff2_desc,
                     diff3.diff_desc diff3_desc,
                     row_number() OVER (PARTITION BY it.fpg_style_id,
                                                     it.wh,
                                                     loc.loc_id,
                                                     it.source_type,
                                                     it.order_no
                                            ORDER BY it.fpg_style_id,
                                                     it.item_node) rnk_wh_loc,
                     loctype.loc_type loc_type
                from (select DISTINCT
                             awis.work_header_id,
                             im1.item item,
                             NVL(im1.item_parent, im1.item) style_id,
                             im2.item_desc style_desc,
                             awis.item_type,
                             awis.item_source_desc,
                             awis.diff_1 diff1_id,
                             awis.diff_2 diff2_id,
                             awis.diff_3 diff3_id,
                             awis.wh,
                             awis.source_type,
                             awis.doc_no
                        from item_master im1,
                             item_master im2,
                             alc_work_item_source awis
                       where awis.work_header_id            = I_work_header_id
                         and NVL(awis.item_id, awis.item)   = im1.item
                         and NVL(im1.item_parent, im1.item) = im2.item
                         and awis.item_type NOT IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                    ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                    ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                    ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                    ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                                    ALC_CONSTANTS_SQL.SELLABLEPACK)
                       union all
                      select DISTINCT
                             awis.work_header_id,
                             pb.pack_no item,
                             im1.item_parent style_id,
                             im2.item_desc  style_desc,
                             awis.item_type,
                             awis.item_source_desc,
                             awis.diff_1 diff1_id,
                             awis.diff_2 diff2_id,
                             awis.diff_3 diff3_id,
                             awis.wh,
                             awis.source_type,
                             awis.doc_no
                        from packitem_breakout pb,
                             item_master im1,
                             item_master im2,
                             alc_work_item_source awis
                       where awis.work_header_id = I_work_header_id
                         and awis.item_id        = pb.pack_no
                         and im1.item            = pb.item
                         and im1.item_parent     = im2.item
                         and awis.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                                ALC_CONSTANTS_SQL.SELLABLEPACK)) awis_inner,
                     --
                     table (cast(I_alc_item_source_attr as ALC_ITEM_SOURCE_ATTR_TBL)) it,
                     table (cast(I_alc_location_attr as ALC_LOCATION_ATTR_TBL)) loc,
                     --
                     diff_ids diff1,
                     diff_ids diff2,
                     diff_ids diff3,
                     --
                     (select store loc,
                             'S' loc_type
                        from store s
                       union all
                      select w.wh loc,
                             'W' loc_type
                        from wh w) loctype
                     --
               where awis_inner.work_header_id    = I_work_header_id
                 and awis_inner.item              = it.item_node
                 and awis_inner.item_type         = it.item_type
                 and awis_inner.style_id          = it.fpg_style_id
                 and awis_inner.wh                = it.wh
                 and NVL(awis_inner.doc_no,-9999) = NVL(it.order_no,-9999)
                 and loc.loc_id                   = loctype.loc
                 --
                 and DECODE(awis_inner.source_type,
                            ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_PO,      ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_PO,
                            ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ASN,     ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ASN,
                            ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_OH,      ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_OH,
                            ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_BOL,     ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_BOL,
                            ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_WHAT_IF, ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF,
                            ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_TSF,     ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_TSF,
                            ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ALLOC,   ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ALLOC) = it.source_type
                 --
                 and awis_inner.diff1_id            = diff1.diff_id (+)
                 and awis_inner.diff2_id            = diff2.diff_id (+)
                 and awis_inner.diff3_id            = diff3.diff_id (+)) t
       order by t.fpg_style_id,
                t.wh,
                t.loc_id,
                t.rnk_wh_loc;

BEGIN

   -- Process Fashion Pack Group (FPG) Allocation
   for rec in C_FPG loop

      if rec.rnk_wh_loc = 1 then
      -- Insert the FPG Level
         insert into alc_session_itemloc_gtt (item_loc_session_id,
                                              facet_session_id,
                                              item_loc_id,
                                              alloc_id,
                                              item_id,
                                              item_desc,
                                              item_type,
                                              wh_id,
                                              release_date,
                                              location_id,
                                              location_desc,
                                              group_id,
                                              group_desc,
                                              group_type,
                                              loc_group_id,
                                              allocated_qty,
                                              calculated_qty,
                                              need_qty,
                                              on_hand_qty,
                                              som_qty,
                                              backorder_qty,
                                              freeze_ind,
                                              next_1_week_qty,
                                              next_2_week_qty,
                                              next_3_week_qty,
                                              diff1_id,
                                              diff1_desc,
                                              diff2_id,
                                              diff2_desc,
                                              diff3_id,
                                              diff3_desc,
                                              parent_item_id,
                                              created_order_no,
                                              created_supplier_id,
                                              future_unit_retail,
                                              rush_flag,
                                              cost,
                                              in_store_date,
                                              future_on_hand_qty,
                                              order_no,
                                              source_type,
                                              gross_need_qty,
                                              rloh_qty,
                                              filtered_ind,
                                              parent_item_loc_session_id,
                                              created_by,
                                              updated_by,
                                              created_date,
                                              update_date,
                                              object_version_id,
                                              loc_type)
                                      values (alc_session_item_loc_seq.NEXTVAL,
                                              I_facet_session_id,
                                              NULL,            -- item_loc_id
                                              I_alloc_id,      -- alloc_id
                                              rec.fpg_style_id,
                                              rec.fpg_style_desc,
                                              'FPG',
                                              rec.wh,
                                              rec.release_date,
                                              rec.loc_id,
                                              rec.loc_desc,
                                              rec.group_id,
                                              rec.group_desc,
                                              rec.group_type,
                                              rec.loc_group_id,
                                              NULL,                -- allocated_qty,
                                              NULL,                -- calculated_qty,
                                              NULL,                -- need_qty,
                                              NULL,                -- on_hand_qty,
                                              NULL,                -- som_qty,
                                              NULL,                -- backorder_qty,
                                              'N',                 -- freeze_ind,
                                              NULL,                -- next_1_week_qty,
                                              NULL,                -- next_2_week_qty,
                                              NULL,                -- next_3_week_qty,
                                              NULL,                -- diff1_id,
                                              NULL,                -- diff1_desc,
                                              NULL,                -- diff2_id,
                                              NULL,                -- diff2_desc,
                                              NULL,                -- diff3_id,
                                              NULL,                -- diff3_desc,
                                              NULL,                -- -- parent_item_id,
                                              NULL,                -- created_order_no,
                                              NULL,                -- created_supplier_id,
                                              NULL,                -- future_unit_retail,
                                              NULL,                -- rush_flag,
                                              NULL,                -- cost,
                                              NULL,                -- in_store_date,
                                              NULL,                -- future_on_hand_qty,
                                              rec.order_no,
                                              rec.source_type,
                                              NULL,                -- gross_need_qty,
                                              NULL,                -- rloh_qty,
                                              'N',                 -- filtered_ind,
                                              NULL,                -- parent_item_loc_session_id,
                                              USER,                -- created_by,
                                              USER,                -- updated_by,
                                              SYSDATE,             -- created_date,
                                              SYSDATE,             -- update_date,
                                              NULL,                -- object_version_id
                                              rec.loc_type)
                                    returning item_loc_session_id into L_parent_item_loc_session_id;

      end if;

      -- Insert the Fashion Pack or Fashion SKU
      insert into alc_session_itemloc_gtt (item_loc_session_id,
                                           facet_session_id,
                                           item_loc_id,
                                           alloc_id,
                                           item_id,
                                           item_desc,
                                           item_type,
                                           wh_id,
                                           release_date,
                                           location_id,
                                           location_desc,
                                           group_id,
                                           group_desc,
                                           group_type,
                                           loc_group_id,
                                           allocated_qty,
                                           calculated_qty,
                                           need_qty,
                                           on_hand_qty,
                                           som_qty,
                                           backorder_qty,
                                           freeze_ind,
                                           next_1_week_qty,
                                           next_2_week_qty,
                                           next_3_week_qty,
                                           diff1_id,
                                           diff1_desc,
                                           diff2_id,
                                           diff2_desc,
                                           diff3_id,
                                           diff3_desc,
                                           parent_item_id,
                                           created_order_no,
                                           created_supplier_id,
                                           future_unit_retail,
                                           rush_flag,
                                           cost,
                                           in_store_date,
                                           future_on_hand_qty,
                                           order_no,
                                           source_type,
                                           gross_need_qty,
                                           rloh_qty,
                                           filtered_ind,
                                           parent_item_loc_session_id,
                                           created_by,
                                           updated_by,
                                           created_date,
                                           update_date,
                                           object_version_id,
                                           loc_type)
                                   values (alc_session_item_loc_seq.NEXTVAL,
                                           I_facet_session_id,
                                           NULL,                -- item_loc_id
                                           I_alloc_id,          -- alloc_id
                                           rec.item_node,
                                           rec.item_desc,
                                           rec.item_type,
                                           rec.wh,
                                           rec.release_date,
                                           rec.loc_id,
                                           rec.loc_desc,
                                           rec.group_id,
                                           rec.group_desc,
                                           rec.group_type,
                                           rec.loc_group_id,
                                           NULL,                -- allocated_qty,
                                           NULL,                -- calculated_qty,
                                           NULL,                -- need_qty,
                                           NULL,                -- on_hand_qty,
                                           NULL,                -- som_qty,
                                           NULL,                -- backorder_qty,
                                           'N',                 -- freeze_ind,
                                           NULL,                -- next_1_week_qty,
                                           NULL,                -- next_2_week_qty,
                                           NULL,                -- next_3_week_qty,
                                           rec.diff1_id,
                                           rec.diff1_desc,
                                           rec.diff2_id,
                                           rec.diff2_desc,
                                           rec.diff3_id,
                                           rec.diff3_desc,
                                           rec.fpg_style_id,
                                           NULL,                -- created_order_no,
                                           NULL,                -- created_supplier_id,
                                           NULL,                -- future_unit_retail,
                                           NULL,                -- rush_flag,
                                           NULL,                -- cost,
                                           rec.in_store_date,
                                           NULL,                -- future_on_hand_qty,
                                           rec.order_no,
                                           rec.source_type,
                                           NULL,                -- gross_need_qty,
                                           NULL,                -- rloh_qty,
                                           'N',                 -- filtered_ind,
                                           L_parent_item_loc_session_id,
                                           USER,                -- created_by,
                                           USER,                -- updated_by,
                                           SYSDATE,             -- created_date,
                                           SYSDATE,             -- update_date,
                                           NULL,                -- object_version_id
                                           rec.loc_type);

   end loop;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SESSION_ITEMLOC_GTT - FPG - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return ALC_CONSTANTS_SQL.FAILURE;
END POP_SESSION_ITEM_LOC_FPG;
------------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_SESSION_QTY_LIMITS(O_error_message      IN OUT VARCHAR2,
                                     I_alloc_id           IN     NUMBER,
                                     I_facet_session_id   IN     VARCHAR2,
                                     I_alloc_type         IN     ALC_ALLOC.TYPE%TYPE)
RETURN NUMBER IS

   L_program                      VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POPULATE_SESSION_QTY_LIMITS';
   L_start_time                   TIMESTAMP    := SYSTIMESTAMP;

   L_dept                         DEPS.DEPT%TYPE;
   L_class                        CLASS.CLASS%TYPE;
   L_subclass                     SUBCLASS.SUBCLASS%TYPE;
   L_fp_default_auto_qty_limits   ALC_SYSTEM_OPTIONS.FP_DEFAULT_AUTO_QTY_LIMITS%TYPE;

   CURSOR C_HIER
    IS
      SELECT im.dept,
        im.class,
        im.subclass
      FROM item_master im,
        (SELECT i.parent_item_id 
        FROM alc_session_itemloc_gtt i
        WHERE i.facet_session_id = I_facet_session_id
        AND i.parent_item_id    IS NOT NULL
        AND rownum               = 1
        ) asig
    WHERE asig.parent_item_id = im.item
    AND rownum                = 1 ;

BEGIN

   --here for FPG.  alloc_type is an optional parameter, so to be safe always get this.
   open C_HIER;
   fetch C_HIER into L_dept,
                     L_class,
                     L_subclass;
   close C_HIER;

   select fp_default_auto_qty_limits
     into L_fp_default_auto_qty_limits
     from alc_system_options;

   merge into alc_session_quantity_limits target
   using (select /*+ ORDERED */
                 DISTINCT i.quantity_limits_session_id,
                 i.facet_session_id,
                 i.item_id,
                 i.location_id,
                 i.loc_type,
                 i.alloc_id,
                 i.dept,
                 i.class,
                 i.subclass,
                 i.min,
                 i.max,
                 i.threshold,
                 i.trend,
                 i.wos,
                 i.min_need,
                 i.min_pack,
                 i.max_pack
            from (SELECT DISTINCT asql.quantity_limits_session_id,
                         asql.facet_session_id,
                         asql.item_id,
                         asql.location_id,
                         asql.loc_type,
                         I_alloc_id alloc_id,
                         NVL(im.dept, L_dept) dept,
                         NVL(im.class, L_class) class,
                         NVL(im.subclass, L_subclass) subclass,
                         asql.min,
                         asql.max,
                         asql.threshold,
                         asql.trend,
                         asql.wos,
                         asql.min_need,
                         asql.min_pack,
                         asql.max_pack
                    from alc_session_quantity_limits asql,
                         item_master im
                   where asql.facet_session_id = I_facet_session_id
                     and im.item               = asql.item_id(+)
                     and EXISTS (select 'x'
                                  from alc_session_item_loc a
                                 where a.facet_session_id = I_facet_session_id
                                   and a.item_id          = asql.item_id
                                   and a.location_id      = asql.location_id)
                     union all
                     -- Non-FPG
                    select NULL quantity_limits_session_id,
                           il.facet_session_id,
                           il.item_id,
                           il.location_id,
                           il.loc_type,
                           il.alloc_id,
                           NVL(im.dept, L_dept) dept,
                           NVL(im.class, L_class) class,
                           NVL(im.subclass, L_subclass) subclass,
                           NULL min,
                           NULL max,
                           NULL threshold,
                           NULL trend,
                           NULL wos,
                           NULL min_need,
                           NULL min_pack,
                           NULL max_pack
                      from alc_session_itemloc_gtt il,
                           item_master im
                     where il.facet_session_id           = I_facet_session_id
                       and il.parent_item_id             = im.item
                       and il.parent_item_loc_session_id is NULL
                       and L_fp_default_auto_qty_limits  = 'N'
                       and I_alloc_type                 != ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION
                       and NOT EXISTS (select 'x'
                                         from alc_session_quantity_limits aql
                                        where aql.facet_session_id = I_facet_session_id
                                          and aql.item_id          = il.item_id
                                          and aql.location_id      = il.location_id)
                     union all
                     -- FPG
                    select NULL quantity_limits_session_id,
                           il.facet_session_id,
                           il.parent_item_id,
                           il.location_id,
                           il.loc_type,
                           il.alloc_id,
                           NVL(im.dept, L_dept) dept,
                           NVL(im.class, L_class) class,
                           NVL(im.subclass, L_subclass) subclass,
                           NULL min,
                           NULL max,
                           NULL threshold,
                           NULL trend,
                           NULL wos,
                           NULL min_need,
                           NULL min_pack,
                           NULL max_pack
                      from alc_session_itemloc_gtt il,
                           item_master im
                     where il.facet_session_id          = I_facet_session_id
                       and il.parent_item_id            = im.item
                       and L_fp_default_auto_qty_limits = 'N'
                       and I_alloc_type                 = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION
                       and NOT EXISTS (select 'x'
                                         from alc_session_quantity_limits aql
                                        where aql.facet_session_id = I_facet_session_id
                                          and aql.item_id          = il.item_id
                                          and aql.location_id      = il.location_id)) i
         ) use_this
   on (    target.facet_session_id           = use_this.facet_session_id
       and target.item_id                    = use_this.item_id
       and target.location_id                = use_this.location_id
       )
   when MATCHED then
      update
         set target.alloc_id  = use_this.alloc_id,
             target.dept      = use_this.dept,
             target.class     = use_this.class,
             target.subclass  = use_this.subclass,
             target.min       = use_this.min,
             target.max       = use_this.max,
             target.threshold = use_this.threshold,
             target.trend     = use_this.trend,
             target.wos       = use_this.wos,
             target.min_need  = use_this.min_need,
             target.min_pack  = use_this.min_pack,
             target.max_pack  = use_this.max_pack
   when NOT MATCHED then
      insert (quantity_limits_session_id,
              item_id,
              location_id,
              facet_session_id,
              quantity_limits_id,
              alloc_id,
              dept,
              class,
              subclass,
              min,
              max,
              threshold,
              trend,
              wos,
              min_need,
              min_pack,
              max_pack)
      values (alc_session_qty_limits_seq.NEXTVAL,
              use_this.item_id,
              use_this.location_id,
              I_facet_session_id,
              NULL,
              use_this.alloc_id,
              use_this.dept,
              use_this.class,
              use_this.subclass,
              NULL,
              NULL,
              NULL,
              NULL,
              NULL,
              NULL,
              NULL,
              NULL);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_SESSION_QUANTITY_LIMITS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return ALC_CONSTANTS_SQL.FAILURE;
END POPULATE_SESSION_QTY_LIMITS;
------------------------------------------------------------------------------------------------------------------
FUNCTION SAVE_ALC_ITEM_LOC(O_error_message      IN OUT VARCHAR2,
                           I_alloc_id           IN     NUMBER,
                           I_facet_session_id   IN     VARCHAR2,
                           I_alloc_type         IN     VARCHAR2,
                           I_flush_ind          IN     NUMBER)
RETURN NUMBER IS

   L_program      VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.SAVE_ALC_ITEM_LOC';
   L_start_time   TIMESTAMP     := SYSTIMESTAMP;

BEGIN

   delete
     from alc_item_parent_loc
    where alloc_id = I_alloc_id;

   delete
     from alc_item_loc
    where alloc_id = I_alloc_id;

   delete
     from alc_quantity_limits
    where alloc_id = I_alloc_id;

   delete
     from alc_item_loc_exclusion
    where alloc_id = I_alloc_id;

   --
   if I_alloc_type in (ALC_CONSTANTS_SQL.FASHION_ALLOCATION, ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) then

      insert into alc_item_parent_loc (item_parent_loc_id,
                                       alloc_id,
                                       item_node,
                                       wh_id,
                                       location_id,
                                       release_date,
                                       item_desc,
                                       item_type,
                                       parent_item_id,
                                       allocated_qty,
                                       calculated_qty,
                                       need_qty,
                                       total_on_hand_qty,
                                       som_qty,
                                       backorder_qty,
                                       gross_need_qty,
                                       rloh_qty,
                                       stock_on_hand,
                                       in_transit,
                                       on_order,
                                       on_alloc,
                                       alloc_out,
                                       future_unit_retail,
                                       cost,
                                       in_store_date,
                                       order_no,
                                       source_type,
                                       rush_flag,
                                       freeze_ind,
                                       loc_type)
                                select alc_item_parent_loc_seq.NEXTVAL,
                                       I_alloc_id alloc_id,
                                       asil.item_id item_node,
                                       asil.wh_id,
                                       asil.location_id,
                                       asil.release_date,
                                       asil.item_desc,
                                       asil.item_type,
                                       asil.parent_item_id,
                                       asil.allocated_qty,
                                       asil.calculated_qty,
                                       asil.need_qty,
                                       asil.on_hand_qty total_on_hand_qty,
                                       asil.som_qty,
                                       asil.backorder_qty,
                                       asil.gross_need_qty,
                                       asil.rloh_qty,
                                       NULL stock_on_hand,
                                       NULL in_transit,
                                       NULL on_order,
                                       NULL on_alloc,
                                       NULL alloc_out,
                                       asil.future_unit_retail,
                                       asil.cost,
                                       asil.in_store_date,
                                       asil.order_no,
                                       asil.source_type,
                                       asil.rush_flag,
                                       asil.freeze_ind,
                                       asil.loc_type
                                  from alc_session_item_loc asil
                                 where asil.facet_session_id = I_facet_session_id
                                   and asil.item_type        IN (ALC_CONSTANTS_SQL.FASHIONITEM,
                                                                 'FPG');
   end if;

   insert into alc_item_loc (item_loc_id,
                             alloc_id,
                             item_id,
                             wh_id,
                             release_date,
                             location_id,
                             location_desc,
                             allocated_qty,
                             calculated_qty,
                             need_qty,
                             som_qty,
                             backorder_qty,
                             freeze_ind,
                             diff1_id,
                             diff1_desc,
                             diff2_id,
                             diff2_desc,
                             diff3_id,
                             diff3_desc,
                             parent_item_id,
                             created_order_no,
                             created_supplier_id,
                             future_unit_retail,
                             rush_flag,
                             cost,
                             in_store_date,
                             order_no,
                             source_type,
                             gross_need_qty,
                             rloh_qty,
                             item_desc,
                             item_type,
                             total_on_hand_qty,
                             stock_on_hand,
                             in_transit,
                             on_order,
                             on_alloc,
                             alloc_out,
                             loc_type)
                      select alc_item_loc_seq.NEXTVAL,
                             I_alloc_id,
                             i.item_id,
                             i.wh_id,
                             i.release_date,
                             i.location_id,
                             i.location_desc,
                             i.allocated_qty,
                             i.calculated_qty,
                             i.need_qty,
                             i.som_qty,
                             i.backorder_qty,
                             i.freeze_ind,
                             i.diff1_id,
                             i.diff1_desc,
                             i.diff2_id,
                             i.diff2_desc,
                             i.diff3_id,
                             i.diff3_desc,
                             i.parent_item_id,
                             i.created_order_no,
                             i.created_supplier_id,
                             i.future_unit_retail,
                             i.rush_flag,
                             i.cost,
                             i.in_store_date,
                             i.order_no,
                             i.source_type,
                             i.gross_need_qty,
                             i.rloh_qty,
                             i.item_desc,
                             i.item_type,
                             i.on_hand_qty + i.in_transit + i.on_order + i.on_alloc - i.alloc_out,
                             i.on_hand_qty,
                             i.in_transit,
                             i.on_order,
                             i.on_alloc,
                             i.alloc_out,
                             i.loc_type
                       from (select DISTINCT
                                    asil.item_id,
                                    asil.wh_id,
                                    asil.release_date,
                                    asil.location_id,
                                    asil.location_desc,
                                    asil.allocated_qty,
                                    asil.calculated_qty,
                                    asil.need_qty,
                                    asil.som_qty,
                                    asil.backorder_qty,
                                    asil.freeze_ind,
                                    asil.diff1_id,
                                    asil.diff1_desc,
                                    asil.diff2_id,
                                    asil.diff2_desc,
                                    asil.diff3_id,
                                    asil.diff3_desc,
                                    asil.parent_item_id,
                                    asil.created_order_no,
                                    asil.created_supplier_id,
                                    asil.future_unit_retail,
                                    asil.rush_flag,
                                    asil.cost,
                                    asil.in_store_date,
                                    asil.future_on_hand_qty,
                                    asil.order_no,
                                    asil.source_type,
                                    asil.gross_need_qty,
                                    asil.rloh_qty,
                                    asil.item_desc,
                                    asil.item_type,
                                    asil.on_hand_qty,
                                    asil.in_transit,
                                    asil.on_order,
                                    asil.on_alloc,
                                    asil.alloc_out,
                                    asil.loc_type
                               from alc_session_item_loc asil
                              where asil.facet_session_id = I_facet_session_id
                                and asil.item_type NOT IN (ALC_CONSTANTS_SQL.FASHIONITEM,
                                                           'FPG')
                                and asil.parent_item_loc_session_id is NULL
                             union all
                             select DISTINCT
                                    asil.item_id,
                                    asil.wh_id,
                                    asil.release_date,
                                    asil.location_id,
                                    asil.location_desc,
                                    asil.allocated_qty,
                                    asil.calculated_qty,
                                    asil.need_qty,
                                    asil.som_qty,
                                    asil.backorder_qty,
                                    asil.freeze_ind,
                                    NVL2(asil2.diff1_id,'1~',NULL)||SUBSTR(asil2.diff1_id, INSTR(asil2.diff1_id,'~')+1) diff1_id,
                                    asil2.diff1_desc,
                                    NVL2(asil2.diff2_id,'2~',NULL)||SUBSTR(asil2.diff2_id, INSTR(asil2.diff2_id,'~')+1) diff2_id,
                                    asil2.diff2_desc,
                                    NVL2(asil2.diff3_id,'3~',NULL)||SUBSTR(asil2.diff3_id, INSTR(asil2.diff3_id,'~')+1) diff3_id,
                                    asil2.diff3_desc,
                                    asil2.parent_item_id,
                                    asil.created_order_no,
                                    asil.created_supplier_id,
                                    asil.future_unit_retail,
                                    asil.rush_flag,
                                    asil.cost,
                                    asil.in_store_date,
                                    asil.future_on_hand_qty,
                                    asil.order_no,
                                    asil.source_type,
                                    asil.gross_need_qty,
                                    asil.rloh_qty,
                                    asil.item_desc,
                                    asil.item_type,
                                    asil.on_hand_qty,
                                    asil.in_transit,
                                    asil.on_order,
                                    asil.on_alloc,
                                    asil.alloc_out,
                                    asil.loc_type
                               from alc_session_item_loc asil,
                                    alc_session_item_loc asil2
                              where asil.facet_session_id     = I_facet_session_id
                                and asil2.item_loc_session_id = asil.parent_item_loc_session_id) i;

   insert into alc_quantity_limits (quantity_limits_id,
                                    alloc_id,
                                    location_id,
                                    location_desc,
                                    dept,
                                    class,
                                    subclass,
                                    item_id,
                                    min,
                                    max,
                                    treshold,
                                    trend,
                                    wos,
                                    min_need,
                                    min_pack,
                                    max_pack)
                             select alc_quantity_limits_seq.NEXTVAL,
                                    I_alloc_id,
                                    ql.location_id,
                                    loc.loc_name,
                                    ql.dept,
                                    ql.class,
                                    ql.subclass,
                                    ql.item_id,
                                    ql.min,
                                    ql.max,
                                    ql.threshold,
                                    ql.trend,
                                    ql.wos,
                                    ql.min_need,
                                    ql.min_pack,
                                    ql.max_pack
                               from alc_session_quantity_limits ql,
                                    (select s.store loc,
                                            s.store_name loc_name
                                       from store s
                                     union all
                                     select w.wh loc,
                                            w.wh_name loc_name
                                       from wh w) loc
                              where ql.facet_session_id = I_facet_session_id
                                and loc.loc             = ql.location_id;

   insert into alc_item_loc_exclusion (item_loc_exclusion_id,
                                       alloc_id,
                                       item_id,
                                       item_desc,
                                       location_id,
                                       location_desc,
                                       reason_code,
                                       diff1_id,
                                       source_location_id,
                                       order_no,
                                       source_type)
                                select alc_item_loc_exclusion_seq.NEXTVAL,
                                       I_alloc_id,
                                       asile.item_id,
                                       asile.item_desc,
                                       asile.location_id,
                                       asile.location_desc,
                                       asile.reason_code,
                                       asile.diff1_id,
                                       asile.source_location_id,
                                       asile.order_no,
                                       asile.source_type
                                  from alc_session_item_loc_excl asile
                                 where asile.facet_session_id = I_facet_session_id;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return ALC_CONSTANTS_SQL.FAILURE;
END SAVE_ALC_ITEM_LOC;
------------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_ALC_ITEM_LOC(O_error_message      IN OUT VARCHAR2,
                           I_alloc_id           IN     NUMBER,
                           I_facet_session_id   IN     VARCHAR2)
RETURN NUMBER IS

   L_program                      VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.LOAD_ALC_ITEM_LOC';
   L_start_time                   TIMESTAMP     := SYSTIMESTAMP;

   L_alloc_type                   VARCHAR2(5);
   L_parent_item_loc_session_id   NUMBER(15);
   L_style_id                     ITEM_MASTER.ITEM%TYPE;
   L_style_desc                   ITEM_MASTER.ITEM_DESC%TYPE;

   cursor C_TYPE is
      select type
        from alc_alloc
       where alloc_id = I_alloc_id;

BEGIN

   -- Clean up alc_session_item_loc and alc_session_quantity_limits
   -- This delete statement should be changed by a truncate partition later
   delete
     from alc_session_quantity_limits
    where facet_session_id = I_facet_session_id;

   delete
     from alc_session_item_loc
    where facet_session_id = I_facet_session_id;

   delete
     from alc_session_item_loc_excl
    where facet_session_id = I_facet_session_id;

   -- Get the Allocation Type. Valid Values are
   --   FA : Style/Color
   --   SA : Staple Item (This include all the type of Packs Allocation except Fashion Pack Group)
   --   FPG: Fashion Pack Group

   open C_TYPE;
   fetch C_TYPE into L_alloc_type;
   close C_TYPE;

    -- If an 'FA' allocation then it will need the style/color in the ALC_SESSION_ITEM_LOC table
   if L_alloc_type = ALC_CONSTANTS_SQL.FASHION_ALLOCATION then

      -- Insert the Style/Color
      insert when 1 = 1 then into alc_session_item_loc (item_loc_session_id,
                                                        facet_session_id,
                                                        item_loc_id,
                                                        alloc_id,
                                                        item_id,
                                                        item_desc,
                                                        item_type,
                                                        wh_id,
                                                        release_date,
                                                        location_id,
                                                        location_desc,
                                                        group_id,
                                                        group_desc,
                                                        group_type,
                                                        loc_group_id,
                                                        allocated_qty,
                                                        calculated_qty,
                                                        need_qty,
                                                        on_hand_qty,
                                                        in_transit,
                                                        on_order,
                                                        on_alloc,
                                                        alloc_out,
                                                        som_qty,
                                                        backorder_qty,
                                                        freeze_ind,
                                                        next_1_week_qty,
                                                        next_2_week_qty,
                                                        next_3_week_qty,
                                                        diff1_id,
                                                        diff1_desc,
                                                        diff2_id,
                                                        diff2_desc,
                                                        diff3_id,
                                                        diff3_desc,
                                                        parent_item_id,
                                                        created_order_no,
                                                        created_supplier_id,
                                                        future_unit_retail,
                                                        rush_flag,
                                                        cost,
                                                        in_store_date,
                                                        future_on_hand_qty,
                                                        order_no,
                                                        source_type,
                                                        gross_need_qty,
                                                        rloh_qty,
                                                        filtered_ind,
                                                        parent_item_loc_session_id,
                                                        created_by,
                                                        updated_by,
                                                        created_date,
                                                        update_date,
                                                        object_version_id,
                                                        loc_type)
                                                values (alc_session_item_loc_seq.NEXTVAL,
                                                        I_facet_session_id,
                                                        NULL,                -- item_loc_id
                                                        I_alloc_id,
                                                        item_node,
                                                        item_desc,
                                                        item_type,
                                                        wh,
                                                        release_date,
                                                        loc_id,
                                                        loc_desc,
                                                        group_id,
                                                        group_desc,
                                                        group_type,
                                                        loc_group_id,
                                                        allocated_qty,
                                                        calculated_qty,
                                                        need_qty,
                                                        on_hand_qty,         -- on_hand_qty,
                                                        in_transit,
                                                        on_order,
                                                        on_alloc,
                                                        alloc_out,
                                                        som_qty,
                                                        backorder_qty,
                                                        freeze_ind,
                                                        NULL,                -- next_1_week_qty,
                                                        NULL,                -- next_2_week_qty,
                                                        NULL,                -- next_3_week_qty,
                                                        diff1_id,
                                                        NULL,                -- diff1_desc,
                                                        diff2_id,
                                                        NULL,                -- diff2_desc,
                                                        NULL,                -- diff3_id,
                                                        NULL,                -- diff3_desc,
                                                        item_parent_id,      -- parent_item_id,
                                                        NULL,                -- created_order_no,
                                                        NULL,                -- created_supplier_id,
                                                        future_unit_retail,
                                                        rush_flag,
                                                        cost,
                                                        in_store_date,
                                                        NULL,                -- future_on_hand_qty,
                                                        order_no,
                                                        source_type,
                                                        gross_need_qty,      -- gross_need_qty,
                                                        rloh_qty,            -- rloh_qty,
                                                        'N',                 -- filtered_ind,
                                                        NULL,                -- parent_item_loc_session_id,
                                                        USER,                -- created_by,
                                                        USER,                -- updated_by,
                                                        SYSDATE,             -- created_date,
                                                        SYSDATE,             -- update_date,
                                                        NULL,                -- object_version_id
                                                        loc_type)
                                                 select s.item_node,
                                                        s.item_type,
                                                        s.alloc_id,
                                                        s.wh_id wh,
                                                        s.location_id loc_id,
                                                        s.release_date,
                                                        s.item_parent_id,
                                                        im.item_desc,
                                                        s.diff1_id,
                                                        s.diff2_id,
                                                        s.location_desc loc_desc,
                                                        s.allocated_qty,
                                                        s.calculated_qty,
                                                        s.need_qty,
                                                        s.gross_need_qty,
                                                        s.rloh_qty,
                                                        s.on_hand_qty,
                                                        s.in_transit,
                                                        s.on_order,
                                                        s.on_alloc,
                                                        s.alloc_out,
                                                        s.som_qty,
                                                        s.backorder_qty,
                                                        s.future_unit_retail,
                                                        s.rush_flag,
                                                        s.cost,
                                                        s.order_no,
                                                        s.source_type,
                                                        s.in_store_date,
                                                        s.freeze_ind,
                                                        g.group_id,
                                                        g.group_desc,
                                                        g.group_type,
                                                        g.loc_group_id,
                                                        s.loc_type
                                                   from (select t.item_node,
                                                                t.item_type,
                                                                t.alloc_id,
                                                                t.wh_id,
                                                                t.location_id,
                                                                t.release_date,
                                                                t.item_parent_id,
                                                                t.ais_diff1_id,
                                                                t.ais_diff2_id,
                                                                t.diff1_id,
                                                                t.diff2_id,
                                                                t.location_desc,
                                                                SUM(t.allocated_qty * DECODE(t.packcomp_qty,
                                                                                             0, 1,
                                                                                             t.packcomp_qty)) OVER(PARTITION BY t.item_node,
                                                                                                                                t.wh_id,
                                                                                                                                t.location_id,
                                                                                                                                t.source_type,
                                                                                                                                t.order_no) allocated_qty,
                                                                SUM(t.calculated_qty * DECODE(t.packcomp_qty,
                                                                                              0, 1,
                                                                                              t.packcomp_qty)) OVER(PARTITION BY t.item_node,
                                                                                                                                 t.wh_id,
                                                                                                                                 t.location_id,
                                                                                                                                 t.source_type,
                                                                                                                                 t.order_no) calculated_qty,
                                                                FIRST_VALUE(t.need_qty) OVER(PARTITION BY t.item_node,
                                                                                                          t.wh_id,
                                                                                                          t.location_id,
                                                                                                          t.source_type,
                                                                                                          t.order_no
                                                                                                 ORDER BY fashion_sku) need_qty,
                                                                FIRST_VALUE(t.gross_need_qty) OVER(PARTITION BY t.item_node,
                                                                                                                t.wh_id,
                                                                                                                t.location_id,
                                                                                                                t.source_type,
                                                                                                                t.order_no
                                                                                                       ORDER BY fashion_sku) gross_need_qty,
                                                                FIRST_VALUE(t.rloh_qty) OVER(PARTITION BY t.item_node,
                                                                                                          t.wh_id,
                                                                                                          t.location_id,
                                                                                                          t.source_type,
                                                                                                          t.order_no
                                                                                                 ORDER BY fashion_sku) rloh_qty,
                                                                SUM(t.stock_on_hand * DECODE(t.packcomp_qty,
                                                                                             0, 1,
                                                                                             t.packcomp_qty)) OVER(PARTITION BY t.item_node,
                                                                                                                                t.wh_id,
                                                                                                                                t.location_id,
                                                                                                                                t.source_type,
                                                                                                                                t.order_no) on_hand_qty,
                                                                SUM(t.in_transit * DECODE(t.packcomp_qty,
                                                                                          0, 1,
                                                                                          t.packcomp_qty)) OVER(PARTITION BY t.item_node,
                                                                                                                             t.wh_id,
                                                                                                                             t.location_id,
                                                                                                                             t.source_type,
                                                                                                                             t.order_no) in_transit,
                                                                SUM(t.on_order * DECODE(t.packcomp_qty,
                                                                                        0, 1,
                                                                                        t.packcomp_qty)) OVER(PARTITION BY t.item_node,
                                                                                                                           t.wh_id,
                                                                                                                           t.location_id,
                                                                                                                           t.source_type,
                                                                                                                           t.order_no) on_order,
                                                                SUM(t.on_alloc * DECODE(t.packcomp_qty,
                                                                                        0, 1,
                                                                                        t.packcomp_qty)) OVER(PARTITION BY t.item_node,
                                                                                                                           t.wh_id,
                                                                                                                           t.location_id,
                                                                                                                           t.source_type,
                                                                                                                           t.order_no) on_alloc,
                                                                SUM(t.alloc_out * DECODE(t.packcomp_qty,
                                                                                         0, 1,
                                                                                         t.packcomp_qty)) OVER(PARTITION BY t.item_node,
                                                                                                                            t.wh_id,
                                                                                                                            t.location_id,
                                                                                                                            t.source_type,
                                                                                                                            t.order_no) alloc_out,
                                                                FIRST_VALUE(t.som_qty) OVER(PARTITION BY t.item_node,
                                                                                                         t.wh_id,
                                                                                                         t.location_id,
                                                                                                         t.source_type,
                                                                                                         t.order_no
                                                                                                ORDER BY fashion_sku) som_qty,
                                                                FIRST_VALUE(t.backorder_qty) OVER(PARTITION BY t.item_node,
                                                                                                               t.wh_id,
                                                                                                               t.location_id,
                                                                                                               t.source_type,
                                                                                                               t.order_no
                                                                                                      ORDER BY fashion_sku) backorder_qty,
                                                                AVG(t.future_unit_retail) OVER(PARTITION BY t.item_node,
                                                                                                            t.wh_id,
                                                                                                            t.location_id,
                                                                                                            t.source_type,
                                                                                                            t.order_no) future_unit_retail,
                                                                t.rush_flag,
                                                                t.cost,
                                                                t.order_no,
                                                                t.source_type,
                                                                t.in_store_date,
                                                                MAX(NVL(t.freeze_ind, 0)) OVER(PARTITION BY t.item_node,
                                                                                                            t.wh_id,
                                                                                                            t.location_id,
                                                                                                            t.source_type,
                                                                                                            t.order_no) freeze_ind,
                                                                row_number() OVER(PARTITION BY t.item_node,
                                                                                               t.wh_id,
                                                                                               t.location_id,
                                                                                               t.order_no,
                                                                                               t.source_type
                                                                                      ORDER BY t.in_store_date) rank,
                                                                t.loc_type
                                                           from (select ais.item_id||' '||ais.diff1_id||NVL2(ais.diff2_id,' ',NULL)||NVL2(ais.diff2_id, NULL, ais.diff2_id) item_node,
                                                                        ais.item_type,
                                                                        ais.alloc_id,
                                                                        ais.wh_id,
                                                                        ail.location_id,
                                                                        ais.release_date,
                                                                        ais.item_id item_parent_id,
                                                                        ais.diff1_id ais_diff1_id,
                                                                        ais.diff2_id ais_diff2_id,
                                                                        SUBSTR(ail.diff1_id, INSTR(ail.diff1_id,'~')+1) diff1_id,
                                                                        SUBSTR(ail.diff2_id, INSTR(ail.diff2_id,'~')+1) diff2_id,
                                                                        ail.fashion_sku,
                                                                        ail.location_desc,
                                                                        ail.allocated_qty,
                                                                        ail.calculated_qty,
                                                                        ail.need_qty,
                                                                        ail.gross_need_qty,
                                                                        ail.rloh_qty,
                                                                        ail.stock_on_hand,
                                                                        ail.in_transit,
                                                                        ail.on_order,
                                                                        ail.on_alloc,
                                                                        ail.alloc_out,
                                                                        ail.som_qty,
                                                                        ail.backorder_qty,
                                                                        ail.future_unit_retail,
                                                                        ail.rush_flag,
                                                                        ail.cost,
                                                                        ail.order_no,
                                                                        ail.source_type,
                                                                        ail.in_store_date,
                                                                        ail.freeze_ind,
                                                                        ail.packcomp_qty,
                                                                        ail.loc_type
                                                                   from alc_item_source ais,
                                                                        (select ail.item_loc_id,
                                                                                ail.location_id,
                                                                                ail.wh_id,
                                                                                ail.item_id,
                                                                                ail.fashion_sku,
                                                                                ail.diff1_id,
                                                                                ail.diff2_id,
                                                                                ail.diff3_id,
                                                                                ail.location_desc,
                                                                                ail.allocated_qty,
                                                                                ail.calculated_qty,
                                                                                ail.need_qty,
                                                                                ail.gross_need_qty,
                                                                                ail.rloh_qty,
                                                                                ail.stock_on_hand,
                                                                                ail.in_transit,
                                                                                ail.on_order,
                                                                                ail.on_alloc,
                                                                                ail.alloc_out,
                                                                                ail.som_qty,
                                                                                ail.backorder_qty,
                                                                                ail.future_unit_retail,
                                                                                ail.rush_flag,
                                                                                ail.cost,
                                                                                ail.order_no,
                                                                                ail.source_type,
                                                                                ail.in_store_date,
                                                                                ail.freeze_ind,
                                                                                ail.packcomp_qty,
                                                                                im.item_parent,
                                                                                ail.loc_type
                                                                           from (select ail.item_loc_id,
                                                                                        ail.location_id,
                                                                                        ail.wh_id,
                                                                                        case
                                                                                           when pb.pack_no is NOT NULL then
                                                                                              pb.item
                                                                                        else
                                                                                           ail.item_id
                                                                                        end as item_id,
                                                                                        ail.item_id fashion_sku,
                                                                                        ail.diff1_id,
                                                                                        ail.diff2_id,
                                                                                        ail.diff3_id,
                                                                                        ail.location_desc,
                                                                                        ail.allocated_qty,
                                                                                        ail.calculated_qty,
                                                                                        ail.need_qty,
                                                                                        ail.gross_need_qty,
                                                                                        ail.rloh_qty,
                                                                                        ail.stock_on_hand,
                                                                                        ail.in_transit,
                                                                                        ail.on_order,
                                                                                        ail.on_alloc,
                                                                                        ail.alloc_out,
                                                                                        ail.som_qty,
                                                                                        ail.backorder_qty,
                                                                                        ail.future_unit_retail,
                                                                                        ail.rush_flag,
                                                                                        ail.cost,
                                                                                        ail.order_no,
                                                                                        ail.source_type,
                                                                                        ail.in_store_date,
                                                                                        ail.freeze_ind,
                                                                                        SUM(NVL(pb.item_qty,0)) OVER (PARTITION BY ail.item_loc_id) packcomp_qty,
                                                                                        row_number() OVER (PARTITION BY ail.item_loc_id
                                                                                                               ORDER BY pb.item) rnk,
                                                                                        ail.loc_type
                                                                                   from alc_item_loc ail,
                                                                                        packitem_breakout pb
                                                                                  where ail.alloc_id   = I_alloc_id
                                                                                    and pb.pack_no (+) = ail.item_id) ail,
                                                                                item_master im
                                                                          where ail.rnk = 1
                                                                            and im.item = ail.item_id) ail
                                                                  where ais.alloc_id    = I_alloc_id
                                                                    and ail.item_parent = ais.item_id
                                                                    and (ail.diff1_id   = ais.diff1_id
                                                                      or ail.diff2_id   = ais.diff1_id
                                                                      or ail.diff3_id   = ais.diff1_id)
                                                                    and ail.wh_id       = ais.wh_id
                                                                    and NVL(ail.order_no, -999999999) = NVL(ais.order_no, -999999999)) t) s,
                                                        item_master im,
                                                        (select alg.alloc_id,
                                                                alg.loc_group_id,
                                                                al.location_id,
                                                                al.group_id,
                                                                alg.group_desc,
                                                                algd.group_type
                                                           from alc_loc_group alg,
                                                                alc_location al,
                                                                alc_loc_group_detail algd
                                                          where alg.alloc_id      = I_alloc_id
                                                            and algd.loc_group_id = alg.loc_group_id
                                                            and al.loc_group_id   = alg.loc_group_id
                                                            and al.group_id       = algd.group_id
                                                            and alg.alloc_id is NOT NULL) g
                                                  where s.rank            = 1
                                                    and im.item           = item_parent_id
                                                    and g.location_id (+) = s.location_id
                                                    and g.alloc_id (+)    = s.alloc_id;

      -- Insert the FASHIONSKU
      insert all into alc_session_item_loc (item_loc_session_id,
                                            facet_session_id,
                                            item_loc_id,
                                            alloc_id,
                                            item_id,
                                            item_desc,
                                            item_type,
                                            wh_id,
                                            release_date,
                                            location_id,
                                            location_desc,
                                            group_id,
                                            group_desc,
                                            group_type,
                                            loc_group_id,
                                            allocated_qty,
                                            calculated_qty,
                                            need_qty,
                                            on_hand_qty,
                                            in_transit,
                                            on_order,
                                            on_alloc,
                                            alloc_out,
                                            som_qty,
                                            backorder_qty,
                                            freeze_ind,
                                            next_1_week_qty,
                                            next_2_week_qty,
                                            next_3_week_qty,
                                            diff1_id,
                                            diff1_desc,
                                            diff2_id,
                                            diff2_desc,
                                            diff3_id,
                                            diff3_desc,
                                            parent_item_id,
                                            created_order_no,
                                            created_supplier_id,
                                            future_unit_retail,
                                            rush_flag,
                                            cost,
                                            in_store_date,
                                            future_on_hand_qty,
                                            order_no,
                                            source_type,
                                            gross_need_qty,
                                            rloh_qty,
                                            filtered_ind,
                                            parent_item_loc_session_id,
                                            created_by,
                                            updated_by,
                                            created_date,
                                            update_date,
                                            object_version_id,
                                            loc_type)
                                    values (alc_session_item_loc_seq.NEXTVAL,
                                            I_facet_session_id,
                                            item_loc_id,
                                            I_alloc_id,
                                            item_node,
                                            item_desc,
                                            item_type,
                                            wh,
                                            release_date,
                                            loc_id,
                                            loc_desc,
                                            group_id,
                                            group_desc,
                                            group_type,
                                            loc_group_id,
                                            allocated_qty,
                                            calculated_qty,
                                            need_qty,
                                            stock_on_hand,
                                            in_transit,
                                            on_order,
                                            on_alloc,
                                            alloc_out,
                                            som_qty,
                                            backorder_qty,
                                            freeze_ind,
                                            NULL,                -- next_1_week_qty,
                                            NULL,                -- next_2_week_qty,
                                            NULL,                -- next_3_week_qty,
                                            NULL,                -- diff1_id,
                                            NULL,                -- diff1_desc,
                                            NULL,                -- diff2_id,
                                            NULL,                -- diff2_desc,
                                            NULL,                -- diff3_id,
                                            NULL,                -- diff3_desc,
                                            item_parent_id,      -- parent_item_id,
                                            created_order_no,
                                            created_supplier_id,
                                            future_unit_retail,
                                            rush_flag,
                                            cost,
                                            in_store_date,
                                            NULL,                -- future_on_hand_qty,
                                            order_no,
                                            source_type,
                                            gross_need_qty,      -- gross_need_qty,
                                            rloh_qty,            -- rloh_qty,
                                            'N',                 -- filtered_ind,
                                            item_loc_session_id, -- parent_item_loc_session_id,
                                            USER,                -- created_by,
                                            USER,                -- updated_by,
                                            SYSDATE,             -- created_date,
                                            SYSDATE,             -- update_date,
                                            NULL,                -- object_version_id
                                            loc_type)
                                     select ail.item_loc_id,
                                            ail.alloc_id,
                                            ail.item_id item_node,
                                            ail.wh_id wh,
                                            ail.release_date,
                                            ail.location_id loc_id,
                                            ail.location_desc loc_desc,
                                            ail.allocated_qty,
                                            ail.calculated_qty,
                                            ail.need_qty,
                                            ail.gross_need_qty,
                                            ail.rloh_qty,
                                            ail.stock_on_hand,
                                            ail.in_transit,
                                            ail.on_order,
                                            ail.on_alloc,
                                            ail.alloc_out,
                                            ail.som_qty,
                                            ail.backorder_qty,
                                            ail.freeze_ind,
                                            ail.diff1_id,
                                            ail.diff1_desc,
                                            ail.diff2_id,
                                            ail.diff2_desc,
                                            ail.parent_item_id item_parent_id,
                                            ail.created_order_no,
                                            ail.created_supplier_id,
                                            ail.parent_diff1_id,
                                            ail.future_unit_retail,
                                            ail.rush_flag,
                                            ail.cost,
                                            ail.in_store_date,
                                            ail.order_no,
                                            ail.source_type,
                                            ail.item_desc,
                                            ail.item_type,
                                            ail.diff3_id,
                                            ail.diff3_desc,
                                            asil.item_loc_session_id,
                                            g.group_id,
                                            g.group_desc,
                                            g.group_type,
                                            g.loc_group_id,
                                            ail.loc_type
                                       from alc_item_loc ail,
                                            item_master  im,
                                            alc_session_item_loc asil,
                                            (select alg.alloc_id,
                                                    alg.loc_group_id,
                                                    al.location_id,
                                                    al.group_id,
                                                    alg.group_desc,
                                                    algd.group_type
                                               from alc_loc_group alg,
                                                    alc_location al,
                                                    alc_loc_group_detail algd
                                              where alg.alloc_id      = I_alloc_id
                                                and algd.loc_group_id = alg.loc_group_id
                                                and al.loc_group_id   = alg.loc_group_id
                                                and al.group_id       = algd.group_id
                                                and alg.alloc_id is NOT NULL
                                                and rownum > 0) g
                                      where ail.alloc_id          = I_alloc_id
                                        and ail.item_type         = ALC_CONSTANTS_SQL.FASHIONSKU
                                        and im.item               = ail.item_id
                                        and asil.facet_session_id = I_facet_session_id
                                        and asil.alloc_id         = ail.alloc_id
                                        and asil.item_type        = ALC_CONSTANTS_SQL.FASHIONITEM
                                        and asil.parent_item_id   = im.item_parent
                                        and NVL(asil.diff1_id, '-9999999999') = NVL(SUBSTR(ail.diff1_id, INSTR(ail.diff1_id,'~')+1), '-9999999999')
                                        and NVL(asil.diff2_id, '-9999999999') = NVL(SUBSTR(ail.diff2_id, INSTR(ail.diff2_id,'~')+1), '-9999999999')
                                        and asil.wh_id                = ail.wh_id
                                        and asil.location_id          = ail.location_id
                                        and NVL(asil.order_no,'-999') = NVL(ail.order_no,'-999')
                                        and asil.source_type          = ail.source_type
                                        and g.location_id (+)         = ail.location_id
                                        and g.alloc_id (+)            = ail.alloc_id;

      -- Insert Pack Items - With the assumption that only Single Color Pack can be in this type of Allocation
      insert all into alc_session_item_loc (item_loc_session_id,
                                            facet_session_id,
                                            item_loc_id,
                                            alloc_id,
                                            item_id,
                                            item_desc,
                                            item_type,
                                            wh_id,
                                            release_date,
                                            location_id,
                                            location_desc,
                                            group_id,
                                            group_desc,
                                            group_type,
                                            loc_group_id,
                                            allocated_qty,
                                            calculated_qty,
                                            need_qty,
                                            on_hand_qty,
                                            in_transit,
                                            on_order,
                                            on_alloc,
                                            alloc_out,
                                            som_qty,
                                            backorder_qty,
                                            freeze_ind,
                                            next_1_week_qty,
                                            next_2_week_qty,
                                            next_3_week_qty,
                                            diff1_id,
                                            diff1_desc,
                                            diff2_id,
                                            diff2_desc,
                                            diff3_id,
                                            diff3_desc,
                                            parent_item_id,
                                            created_order_no,
                                            created_supplier_id,
                                            future_unit_retail,
                                            rush_flag,
                                            cost,
                                            in_store_date,
                                            future_on_hand_qty,
                                            order_no,
                                            source_type,
                                            gross_need_qty,
                                            rloh_qty,
                                            filtered_ind,
                                            parent_item_loc_session_id,
                                            created_by,
                                            updated_by,
                                            created_date,
                                            update_date,
                                            object_version_id,
                                            loc_type)
                                    values (alc_session_item_loc_seq.NEXTVAL,
                                            I_facet_session_id,
                                            item_loc_id,
                                            I_alloc_id,
                                            item_node,
                                            item_desc,
                                            item_type,
                                            wh,
                                            release_date,
                                            loc_id,
                                            loc_desc,
                                            group_id,
                                            group_desc,
                                            group_type,
                                            loc_group_id,
                                            allocated_qty,
                                            calculated_qty,
                                            need_qty,
                                            stock_on_hand,
                                            in_transit,
                                            on_order,
                                            on_alloc,
                                            alloc_out,
                                            som_qty,
                                            backorder_qty,
                                            freeze_ind,
                                            NULL,                -- next_1_week_qty,
                                            NULL,                -- next_2_week_qty,
                                            NULL,                -- next_3_week_qty,
                                            NULL,                -- diff1_id,
                                            NULL,                -- diff1_desc,
                                            NULL,                -- diff2_id,
                                            NULL,                -- diff2_desc,
                                            NULL,                -- diff3_id,
                                            NULL,                -- diff3_desc,
                                            item_parent_id,      -- parent_item_id,
                                            created_order_no,
                                            created_supplier_id,
                                            future_unit_retail,
                                            rush_flag,
                                            cost,
                                            in_store_date,
                                            NULL,                -- future_on_hand_qty,
                                            order_no,
                                            source_type,
                                            gross_need_qty,      -- gross_need_qty,
                                            rloh_qty,            -- rloh_qty,
                                            'N',                 -- filtered_ind,
                                            item_loc_session_id, -- parent_item_loc_session_id,
                                            USER,                -- created_by,
                                            USER,                -- updated_by,
                                            SYSDATE,             -- created_date,
                                            SYSDATE,             -- update_date,
                                            NULL,                -- object_version_id
                                            loc_type)
                                     select ail.item_loc_id,
                                            ail.alloc_id,
                                            ail.item_id item_node,
                                            ail.wh_id wh,
                                            ail.release_date,
                                            ail.location_id loc_id,
                                            ail.location_desc loc_desc,
                                            ail.allocated_qty,
                                            ail.calculated_qty,
                                            ail.need_qty,
                                            ail.gross_need_qty,
                                            ail.rloh_qty,
                                            ail.stock_on_hand,
                                            ail.in_transit,
                                            ail.on_order,
                                            ail.on_alloc,
                                            ail.alloc_out,
                                            ail.som_qty,
                                            ail.backorder_qty,
                                            ail.freeze_ind,
                                            ail.diff1_id,
                                            ail.diff1_desc,
                                            ail.diff2_id,
                                            ail.diff2_desc,
                                            ail.parent_item_id item_parent_id,
                                            ail.created_order_no,
                                            ail.created_supplier_id,
                                            ail.parent_diff1_id,
                                            ail.future_unit_retail,
                                            ail.rush_flag,
                                            ail.cost,
                                            ail.in_store_date,
                                            ail.order_no,
                                            ail.source_type,
                                            ail.item_desc,
                                            ail.item_type,
                                            ail.diff3_id,
                                            ail.diff3_desc,
                                            asil.item_loc_session_id,
                                            g.group_id,
                                            g.group_desc,
                                            g.group_type,
                                            g.loc_group_id,
                                            ail.loc_type
                                       from (select ail.item_loc_id,
                                                    ail.alloc_id,
                                                    ail.item_id,
                                                    ail.wh_id,
                                                    ail.release_date,
                                                    ail.location_id,
                                                    ail.location_desc,
                                                    ail.allocated_qty,
                                                    ail.calculated_qty,
                                                    ail.need_qty,
                                                    ail.gross_need_qty,
                                                    ail.rloh_qty,
                                                    ail.stock_on_hand,
                                                    ail.in_transit,
                                                    ail.on_order,
                                                    ail.on_alloc,
                                                    ail.alloc_out,
                                                    ail.som_qty,
                                                    ail.backorder_qty,
                                                    ail.freeze_ind,
                                                    ail.diff1_id,
                                                    ail.diff1_desc,
                                                    ail.diff2_id,
                                                    ail.diff2_desc,
                                                    ail.parent_item_id,
                                                    ail.created_order_no,
                                                    ail.created_supplier_id,
                                                    ail.parent_diff1_id,
                                                    ail.future_unit_retail,
                                                    ail.rush_flag,
                                                    ail.cost,
                                                    ail.in_store_date,
                                                    ail.order_no,
                                                    ail.source_type,
                                                    ail.item_desc,
                                                    ail.item_type,
                                                    ail.diff3_id,
                                                    ail.diff3_desc,
                                                    pb.item pb_item,
                                                    row_number() OVER(PARTITION BY ail.item_loc_id
                                                                          ORDER BY pb.item) rnk,
                                                    ail.loc_type
                                               from alc_item_loc ail,
                                                    packitem_breakout pb
                                              where ail.alloc_id = I_alloc_id
                                                and ail.item_type in (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                                      ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                                      ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                                      ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                                      ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                                                      ALC_CONSTANTS_SQL.SELLABLEPACK)
                                                and pb.pack_no = ail.item_id) ail,
                                            item_master im,
                                            alc_session_item_loc asil,
                                            (select alg.alloc_id,
                                                    alg.loc_group_id,
                                                    al.location_id,
                                                    al.group_id,
                                                    alg.group_desc,
                                                    algd.group_type
                                               from alc_loc_group alg,
                                                    alc_location al,
                                                    alc_loc_group_detail algd
                                              where alg.alloc_id      = I_alloc_id
                                                and algd.loc_group_id = alg.loc_group_id
                                                and al.loc_group_id   = alg.loc_group_id
                                                and al.group_id       = algd.group_id
                                                and alg.alloc_id is NOT NULL
                                                and rownum > 0) g
                                      where ail.rnk                       = 1
                                        and im.item                       = ail.pb_item
                                        and asil.facet_session_id         = I_facet_session_id
                                        and asil.alloc_id                 = ail.alloc_id
                                        and asil.item_type                = ALC_CONSTANTS_SQL.FASHIONITEM
                                        and asil.parent_item_id           = im.item_parent
                                        and NVL(asil.diff1_id, im.diff_1) = im.diff_1
                                        and NVL(asil.diff2_id, im.diff_2) = im.diff_2
                                        and asil.wh_id                    = ail.wh_id
                                        and nvl(asil.order_no,'-999')     = NVL(ail.order_no,'-999')
                                        and asil.source_type              = ail.source_type
                                        and asil.location_id              = ail.location_id
                                        and g.location_id (+)             = ail.location_id
                                        and g.alloc_id (+)                = ail.alloc_id;

   -- If an 'FPG' (Fashion Pack Group) allocation then it will need the FPG record in the ALC_SESSION_ITEM_LOC table
   elsif L_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION then

      -- Insert the FPG
      insert when 1 = 1 then into alc_session_item_loc (item_loc_session_id,
                                                        facet_session_id,
                                                        item_loc_id,
                                                        alloc_id,
                                                        item_id,
                                                        item_desc,
                                                        item_type,
                                                        wh_id,
                                                        release_date,
                                                        location_id,
                                                        location_desc,
                                                        group_id,
                                                        group_desc,
                                                        group_type,
                                                        loc_group_id,
                                                        allocated_qty,
                                                        calculated_qty,
                                                        need_qty,
                                                        on_hand_qty,
                                                        in_transit,
                                                        on_order,
                                                        on_alloc,
                                                        alloc_out,
                                                        som_qty,
                                                        backorder_qty,
                                                        freeze_ind,
                                                        next_1_week_qty,
                                                        next_2_week_qty,
                                                        next_3_week_qty,
                                                        diff1_id,
                                                        diff1_desc,
                                                        diff2_id,
                                                        diff2_desc,
                                                        diff3_id,
                                                        diff3_desc,
                                                        parent_item_id,
                                                        created_order_no,
                                                        created_supplier_id,
                                                        future_unit_retail,
                                                        rush_flag,
                                                        cost,
                                                        in_store_date,
                                                        future_on_hand_qty,
                                                        order_no,
                                                        source_type,
                                                        gross_need_qty,
                                                        rloh_qty,
                                                        filtered_ind,
                                                        parent_item_loc_session_id,
                                                        created_by,
                                                        updated_by,
                                                        created_date,
                                                        update_date,
                                                        object_version_id,
                                                        loc_type)
                                                values (alc_session_item_loc_seq.NEXTVAL,
                                                        I_facet_session_id,
                                                        NULL,                -- item_loc_id
                                                        I_alloc_id,
                                                        item_node,
                                                        item_desc,
                                                        item_type,
                                                        wh,
                                                        release_date,
                                                        loc_id,
                                                        loc_desc,
                                                        group_id,
                                                        group_desc,
                                                        group_type,
                                                        loc_group_id,
                                                        allocated_qty,
                                                        calculated_qty,
                                                        need_qty,
                                                        on_hand_qty,         -- on_hand_qty,
                                                        in_transit,
                                                        on_order,
                                                        on_alloc,
                                                        alloc_out,
                                                        som_qty,
                                                        backorder_qty,
                                                        freeze_ind,
                                                        NULL,                -- next_1_week_qty,
                                                        NULL,                -- next_2_week_qty,
                                                        NULL,                -- next_3_week_qty,
                                                        diff1_id,
                                                        NULL,                -- diff1_desc,
                                                        diff2_id,
                                                        NULL,                -- diff2_desc,
                                                        NULL,                -- diff3_id,
                                                        NULL,                -- diff3_desc,
                                                        item_parent_id,      -- parent_item_id,
                                                        NULL,                -- created_order_no,
                                                        NULL,                -- created_supplier_id,
                                                        future_unit_retail,
                                                        rush_flag,
                                                        cost,
                                                        in_store_date,
                                                        NULL,                -- future_on_hand_qty,
                                                        order_no,
                                                        source_type,
                                                        gross_need_qty,      -- gross_need_qty,
                                                        rloh_qty,            -- rloh_qty,
                                                        'N',                 -- filtered_ind,
                                                        NULL,                -- parent_item_loc_session_id,
                                                        USER,                -- created_by,
                                                        USER,                -- updated_by,
                                                        SYSDATE,             -- created_date,
                                                        SYSDATE,             -- update_date,
                                                        NULL,                -- object_version_id
                                                        loc_type)
                                                 select inner_fpg.style_id item_node,
                                                        ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION item_type,
                                                        inner_fpg.alloc_id,
                                                        inner_fpg.wh_id wh,
                                                        inner_fpg.location_id loc_id,
                                                        inner_fpg.release_date,
                                                        NULL item_parent_id,
                                                        inner_fpg.style_desc item_desc,
                                                        NULL    diff1_id,
                                                        NULL    diff2_id,
                                                        inner_fpg.location_desc loc_desc,
                                                        inner_fpg.allocated_qty,
                                                        inner_fpg.calculated_qty,
                                                        inner_fpg.need_qty,
                                                        inner_fpg.gross_need_qty,
                                                        inner_fpg.rloh_qty,
                                                        inner_fpg.on_hand_qty,
                                                        inner_fpg.in_transit,
                                                        inner_fpg.on_order,
                                                        inner_fpg.on_alloc,
                                                        inner_fpg.alloc_out,
                                                        inner_fpg.som_qty,
                                                        inner_fpg.backorder_qty,
                                                        inner_fpg.future_unit_retail,
                                                        inner_fpg.rush_flag,
                                                        inner_fpg.cost,
                                                        inner_fpg.order_no,
                                                        inner_fpg.source_type,
                                                        inner_fpg.in_store_date,
                                                        inner_fpg.freeze_ind,
                                                        g.group_id,
                                                        g.group_desc,
                                                        g.group_type,
                                                        g.loc_group_id,
                                                        inner_fpg.loc_type
                                                  from (select DISTINCT
                                                               t.style_id,
                                                               t.style_desc,
                                                               t.alloc_id,
                                                               t.wh_id,
                                                               t.location_id,
                                                               t.release_date,
                                                               t.location_desc,
                                                               SUM(t.allocated_qty) OVER (PARTITION BY t.style_id,
                                                                                                       t.wh_id,
                                                                                                       t.location_id,
                                                                                                       t.source_type,
                                                                                                       t.order_no) allocated_qty,
                                                               SUM(t.calculated_qty) OVER (PARTITION BY t.style_id,
                                                                                                        t.wh_id,
                                                                                                        t.location_id,
                                                                                                        t.source_type,
                                                                                                        t.order_no) calculated_qty,
                                                               t.need_qty,
                                                               t.gross_need_qty,
                                                               t.rloh_qty,
                                                               t.on_hand_qty,
                                                               t.in_transit,
                                                               t.on_order,
                                                               t.on_alloc,
                                                               t.alloc_out,
                                                               t.som_qty,
                                                               t.backorder_qty,
                                                               t.future_unit_retail,
                                                               t.rush_flag,
                                                               t.cost,
                                                               t.order_no,
                                                               t.source_type,
                                                               t.in_store_date,
                                                               t.freeze_ind,
                                                               t.loc_type
                                                          from (select DISTINCT
                                                                       parent.style_id,
                                                                       parent.style_desc,
                                                                       ail.alloc_id,
                                                                       ail.item_id,
                                                                       ail.wh_id,
                                                                       ail.location_id,
                                                                       ail.release_date,
                                                                       ail.location_desc,
                                                                       SUM(ail.allocated_qty * NVL(pb.pack_item_qty, 1)) OVER (PARTITION BY ail.item_id,
                                                                                                                                            ail.wh_id,
                                                                                                                                            ail.location_id,
                                                                                                                                            ail.source_type,
                                                                                                                                            ail.order_no) allocated_qty,
                                                                       SUM(ail.calculated_qty) OVER (PARTITION BY ail.item_id,
                                                                                                                  ail.wh_id,
                                                                                                                  ail.location_id,
                                                                                                                  ail.source_type,
                                                                                                                  ail.order_no) calculated_qty,
                                                                       FIRST_VALUE(ail.need_qty) OVER(PARTITION BY ail.item_id,
                                                                                                                   ail.wh_id,
                                                                                                                   ail.location_id,
                                                                                                                   ail.source_type,
                                                                                                                   ail.order_no
                                                                                                          ORDER BY ail.item_id) need_qty,
                                                                       FIRST_VALUE(ail.gross_need_qty) OVER(PARTITION BY ail.item_id,
                                                                                                                         ail.wh_id,
                                                                                                                         ail.location_id,
                                                                                                                         ail.source_type,
                                                                                                                         ail.order_no
                                                                                                                ORDER BY ail.item_id) gross_need_qty,
                                                                       FIRST_VALUE(ail.rloh_qty) OVER(PARTITION BY ail.item_id,
                                                                                                                   ail.wh_id,
                                                                                                                   ail.location_id,
                                                                                                                   ail.source_type,
                                                                                                                   ail.order_no
                                                                                                          ORDER BY ail.item_id) rloh_qty,
                                                                       FIRST_VALUE(ail.stock_on_hand) OVER(PARTITION BY ail.item_id,
                                                                                                                        ail.wh_id,
                                                                                                                        ail.location_id,
                                                                                                                        ail.source_type,
                                                                                                                        ail.order_no
                                                                                                               ORDER BY ail.item_id) on_hand_qty,
                                                                       FIRST_VALUE(ail.in_transit) OVER(PARTITION BY ail.item_id,
                                                                                                                     ail.wh_id,
                                                                                                                     ail.location_id,
                                                                                                                     ail.source_type,
                                                                                                                     ail.order_no
                                                                                                            ORDER BY ail.item_id) in_transit,
                                                                       FIRST_VALUE(ail.on_order) OVER(PARTITION BY ail.item_id,
                                                                                                                   ail.wh_id,
                                                                                                                   ail.location_id,
                                                                                                                   ail.source_type,
                                                                                                                   ail.order_no
                                                                                                          ORDER BY ail.item_id) on_order,
                                                                       FIRST_VALUE(ail.on_alloc) OVER(PARTITION BY ail.item_id,
                                                                                                                   ail.wh_id,
                                                                                                                   ail.location_id,
                                                                                                                   ail.source_type,
                                                                                                                   ail.order_no
                                                                                                          ORDER BY ail.item_id) on_alloc,
                                                                       FIRST_VALUE(ail.alloc_out) OVER(PARTITION BY ail.item_id,
                                                                                                                    ail.wh_id,
                                                                                                                    ail.location_id,
                                                                                                                    ail.source_type,
                                                                                                                    ail.order_no
                                                                                                           ORDER BY ail.item_id) alloc_out,
                                                                       FIRST_VALUE(ail.som_qty) OVER(PARTITION BY ail.item_id,
                                                                                                                  ail.wh_id,
                                                                                                                  ail.location_id,
                                                                                                                  ail.source_type,
                                                                                                                  ail.order_no
                                                                                                         ORDER BY ail.item_id) som_qty,
                                                                       FIRST_VALUE(ail.backorder_qty) OVER(PARTITION BY ail.item_id,
                                                                                                                        ail.wh_id,
                                                                                                                        ail.location_id,
                                                                                                                        ail.source_type,
                                                                                                                        ail.order_no
                                                                                                               ORDER BY ail.item_id) backorder_qty,
                                                                       AVG(ail.future_unit_retail) OVER(PARTITION BY ail.item_id,
                                                                                                                     ail.wh_id,
                                                                                                                     ail.location_id,
                                                                                                                     ail.source_type,
                                                                                                                     ail.order_no) future_unit_retail,
                                                                       ail.rush_flag,
                                                                       ail.cost,
                                                                       ail.order_no,
                                                                       ail.source_type,
                                                                       ail.in_store_date,
                                                                       MAX(NVL(ail.freeze_ind,0)) OVER(PARTITION BY ail.item_id,
                                                                                                                    ail.wh_id,
                                                                                                                    ail.location_id,
                                                                                                                    ail.source_type,
                                                                                                                    ail.order_no) freeze_ind,
                                                                       ail.loc_type
                                                                  from alc_item_loc ail,
                                                                       packitem_breakout pb,
                                                                       (select DISTINCT
                                                                               im1.item_parent style_id,
                                                                               im2.item_desc style_desc,
                                                                               ail.item_id item
                                                                          from item_master im1,
                                                                               item_master im2,
                                                                               alc_item_loc ail
                                                                         where ail.alloc_id  = I_alloc_id
                                                                           and ail.item_type = ALC_CONSTANTS_SQL.FASHIONSKU
                                                                           and im1.item      = ail.item_id
                                                                           and im2.item      = im1.item_parent
                                                                         union
                                                                        select DISTINCT
                                                                               im1.item_parent style_id,
                                                                               im2.item_desc style_desc,
                                                                               pb.pack_no item
                                                                          from item_master im1,
                                                                               item_master im2,
                                                                               alc_item_loc ail,
                                                                               packitem_breakout pb
                                                                         where ail.alloc_id    = I_alloc_id
                                                                           and ail.item_id     = pb.pack_no
                                                                           and im1.item        = pb.item
                                                                           and im1.item_parent = im2.item) parent
                                                                 where ail.alloc_id = I_alloc_id
                                                                   and ail.item_id  = pb.pack_no(+)
                                                                   and ail.item_id  = parent.item) t
                                                         ) inner_fpg,
                                                         (select alg.alloc_id,
                                                                 alg.loc_group_id,
                                                                 al.location_id,
                                                                 al.group_id,
                                                                 alg.group_desc,
                                                                 algd.group_type
                                                            from alc_loc_group alg,
                                                                 alc_location al,
                                                                 alc_loc_group_detail algd
                                                           where alg.alloc_id      = I_alloc_id
                                                             and algd.loc_group_id = alg.loc_group_id
                                                             and al.loc_group_id   = alg.loc_group_id
                                                             and al.group_id       = algd.group_id
                                                             and alg.alloc_id is NOT NULL
                                                             and rownum > 0) g
                                                  where g.location_id (+) = inner_fpg.location_id
                                                    and g.alloc_id (+)    = inner_fpg.alloc_id;

      -- Insert the PACK and FASHIONSKU
      insert all into alc_session_item_loc (item_loc_session_id,
                                            facet_session_id,
                                            item_loc_id,
                                            alloc_id,
                                            item_id,
                                            item_desc,
                                            item_type,
                                            wh_id,
                                            release_date,
                                            location_id,
                                            location_desc,
                                            group_id,
                                            group_desc,
                                            group_type,
                                            loc_group_id,
                                            allocated_qty,
                                            calculated_qty,
                                            need_qty,
                                            on_hand_qty,
                                            in_transit,
                                            on_order,
                                            on_alloc,
                                            alloc_out,
                                            som_qty,
                                            backorder_qty,
                                            freeze_ind,
                                            next_1_week_qty,
                                            next_2_week_qty,
                                            next_3_week_qty,
                                            diff1_id,
                                            diff1_desc,
                                            diff2_id,
                                            diff2_desc,
                                            diff3_id,
                                            diff3_desc,
                                            parent_item_id,
                                            created_order_no,
                                            created_supplier_id,
                                            future_unit_retail,
                                            rush_flag,
                                            cost,
                                            in_store_date,
                                            future_on_hand_qty,
                                            order_no,
                                            source_type,
                                            gross_need_qty,
                                            rloh_qty,
                                            filtered_ind,
                                            parent_item_loc_session_id,
                                            created_by,
                                            updated_by,
                                            created_date,
                                            update_date,
                                            object_version_id,
                                            loc_type)
                                    values (alc_session_item_loc_seq.NEXTVAL,
                                            I_facet_session_id,
                                            item_loc_id,
                                            I_alloc_id,
                                            item_node,
                                            item_desc,
                                            item_type,
                                            wh,
                                            release_date,
                                            loc_id,
                                            loc_desc,
                                            group_id,
                                            group_desc,
                                            group_type,
                                            loc_group_id,
                                            allocated_qty,
                                            calculated_qty,
                                            need_qty,
                                            stock_on_hand,
                                            in_transit,
                                            on_order,
                                            on_alloc,
                                            alloc_out,
                                            som_qty,
                                            backorder_qty,
                                            freeze_ind,
                                            NULL,                -- next_1_week_qty,
                                            NULL,                -- next_2_week_qty,
                                            NULL,                -- next_3_week_qty,
                                            diff1_id,
                                            diff1_desc,
                                            diff2_id,
                                            diff2_desc,
                                            diff3_id,
                                            diff3_desc,
                                            item_parent_id,      -- parent_item_id,
                                            created_order_no,
                                            created_supplier_id,
                                            future_unit_retail,
                                            rush_flag,
                                            cost,
                                            in_store_date,
                                            NULL,                -- future_on_hand_qty,
                                            order_no,
                                            source_type,
                                            gross_need_qty,      -- gross_need_qty,
                                            rloh_qty,            -- rloh_qty,
                                            'N',                 -- filtered_ind,
                                            item_loc_session_id, -- parent_item_loc_session_id,
                                            USER,                -- created_by,
                                            USER,                -- updated_by,
                                            SYSDATE,             -- created_date,
                                            SYSDATE,             -- update_date,
                                            NULL,                -- object_version_id
                                            loc_type)
                                     select t.item_loc_id,
                                            t.alloc_id,
                                            t.item_node,
                                            t.wh,
                                            t.release_date,
                                            t.loc_id,
                                            t.loc_desc,
                                            t.allocated_qty,
                                            t.calculated_qty,
                                            t.need_qty,
                                            t.gross_need_qty,
                                            t.rloh_qty,
                                            t.stock_on_hand,
                                            t.in_transit,
                                            t.on_order,
                                            t.on_alloc,
                                            t.alloc_out,
                                            t.som_qty,
                                            t.backorder_qty,
                                            t.freeze_ind,
                                            t.diff1_id,
                                            t.diff1_desc,
                                            t.diff2_id,
                                            t.diff2_desc,
                                            t.item_parent_id,
                                            t.created_order_no,
                                            t.created_supplier_id,
                                            t.parent_diff1_id,
                                            t.future_unit_retail,
                                            t.rush_flag,
                                            t.cost,
                                            t.in_store_date,
                                            t.order_no,
                                            t.source_type,
                                            t.item_desc,
                                            t.item_type,
                                            t.diff3_id,
                                            t.diff3_desc,
                                            t.item_loc_session_id,
                                            t.group_id,
                                            t.group_desc,
                                            t.group_type,
                                            t.loc_group_id,
                                            t.loc_type
                                       from (select ail.item_loc_id,
                                                    ail.alloc_id,
                                                    ail.item_id     item_node,
                                                    ail.wh_id wh,
                                                    ail.release_date,
                                                    ail.location_id loc_id,
                                                    ail.location_desc loc_desc,
                                                    ail.allocated_qty,
                                                    ail.calculated_qty,
                                                    ail.need_qty,
                                                    ail.gross_need_qty,
                                                    ail.rloh_qty,
                                                    ail.stock_on_hand,
                                                    ail.in_transit,
                                                    ail.on_order,
                                                    ail.on_alloc,
                                                    ail.alloc_out,
                                                    ail.som_qty,
                                                    ail.backorder_qty,
                                                    ail.freeze_ind,
                                                    ail.diff1_id,
                                                    ail.diff1_desc,
                                                    ail.diff2_id,
                                                    ail.diff2_desc,
                                                    style.st_item_parent item_parent_id,
                                                    ail.created_order_no,
                                                    ail.created_supplier_id,
                                                    ail.parent_diff1_id,
                                                    ail.future_unit_retail,
                                                    ail.rush_flag,
                                                    ail.cost,
                                                    ail.in_store_date,
                                                    ail.order_no,
                                                    ail.source_type,
                                                    ail.item_desc,
                                                    ail.item_type,
                                                    ail.diff3_id,
                                                    ail.diff3_desc,
                                                    asil.item_loc_session_id,
                                                    g.group_id,
                                                    g.group_desc,
                                                    g.group_type,
                                                    g.loc_group_id,
                                                    ail.loc_type,
                                                    style.st_item_parent,
                                                    style.st_item,
                                                    ROW_NUMBER() OVER(PARTITION BY ail.item_id,
                                                                                   ail.wh_id,
                                                                                   ail.location_id,
                                                                                   ail.source_type,
                                                                                   ail.order_no
                                                                          ORDER BY ail.in_store_date) rank
                                               from alc_item_loc ail,
                                                    item_master  im,
                                                   (select DISTINCT
                                                           im1.item_parent st_item_parent,
                                                           im1.item st_item
                                                      from item_master im1,
                                                           alc_item_loc ail
                                                     where ail.alloc_id  = I_alloc_id
                                                       and ail.item_type = ALC_CONSTANTS_SQL.FASHIONSKU
                                                       and im1.item      = ail.item_id
                                                     union
                                                    select DISTINCT
                                                           im2.item_parent st_item_parent,
                                                           ail.item_id st_item
                                                      from item_master im2,
                                                           alc_item_loc ail,
                                                           packitem_breakout pb
                                                     where ail.alloc_id = I_alloc_id
                                                       and ail.item_id  = pb.pack_no
                                                       and im2.item     = pb.item )style,
                                                    packitem_breakout pb,
                                                    (select alg.alloc_id,
                                                            alg.loc_group_id,
                                                            al.location_id,
                                                            al.group_id,
                                                            alg.group_desc,
                                                            algd.group_type
                                                       from alc_loc_group alg,
                                                            alc_location al,
                                                            alc_loc_group_detail algd
                                                      where alg.alloc_id      = I_alloc_id
                                                        and algd.loc_group_id = alg.loc_group_id
                                                        and al.loc_group_id   = alg.loc_group_id
                                                        and al.group_id       = algd.group_id
                                                        and alg.alloc_id is NOT NULL
                                                        and rownum > 0) g,
                                                    alc_session_item_loc asil
                                              where ail.alloc_id              = I_alloc_id
                                                and im.item                   = ail.item_id
                                                and asil.facet_session_id     = I_facet_session_id
                                                and asil.alloc_id             = ail.alloc_id
                                                and asil.item_type            = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION
                                                and ail.item_id               = pb.pack_no(+)
                                                and asil.wh_id                = ail.wh_id
                                                and asil.location_id          = ail.location_id
                                                and asil.source_type          = ail.source_type
                                                and NVL(asil.order_no,'-999') = NVL(ail.order_no,'-999')
                                                and g.location_id (+)         = ail.location_id
                                                and g.alloc_id (+)            = ail.alloc_id
                                                and ail.item_id               = style.st_item
                                                and asil.item_id              = style.st_item_parent ) t
                                   where t.rank = 1;

   else
      -- 'SA' Allocation
      insert when 1 = 1 then into alc_session_item_loc (item_loc_session_id,
                                                        facet_session_id,
                                                        item_loc_id,
                                                        alloc_id,
                                                        item_id,
                                                        item_desc,
                                                        item_type,
                                                        wh_id,
                                                        release_date,
                                                        location_id,
                                                        location_desc,
                                                        group_id,
                                                        group_desc,
                                                        group_type,
                                                        loc_group_id,
                                                        allocated_qty,
                                                        calculated_qty,
                                                        need_qty,
                                                        on_hand_qty,
                                                        in_transit,
                                                        on_order,
                                                        on_alloc,
                                                        alloc_out,
                                                        som_qty,
                                                        backorder_qty,
                                                        freeze_ind,
                                                        next_1_week_qty,
                                                        next_2_week_qty,
                                                        next_3_week_qty,
                                                        diff1_id,
                                                        diff1_desc,
                                                        diff2_id,
                                                        diff2_desc,
                                                        diff3_id,
                                                        diff3_desc,
                                                        parent_item_id,
                                                        created_order_no,
                                                        created_supplier_id,
                                                        future_unit_retail,
                                                        rush_flag,
                                                        cost,
                                                        in_store_date,
                                                        future_on_hand_qty,
                                                        order_no,
                                                        source_type,
                                                        gross_need_qty,
                                                        rloh_qty,
                                                        filtered_ind,
                                                        parent_item_loc_session_id,
                                                        created_by,
                                                        updated_by,
                                                        created_date,
                                                        update_date,
                                                        object_version_id,
                                                        loc_type)
                                                values (alc_session_item_loc_seq.NEXTVAL,
                                                        I_facet_session_id,
                                                        item_loc_id,
                                                        I_alloc_id,
                                                        item_node,
                                                        item_desc,
                                                        item_type,
                                                        wh,
                                                        release_date,
                                                        loc_id,
                                                        loc_desc,
                                                        group_id,
                                                        group_desc,
                                                        group_type,
                                                        loc_group_id,
                                                        allocated_qty,
                                                        calculated_qty,
                                                        need_qty,
                                                        stock_on_hand,
                                                        in_transit,
                                                        on_order,
                                                        on_alloc,
                                                        alloc_out,
                                                        som_qty,
                                                        backorder_qty,
                                                        freeze_ind,
                                                        NULL,                -- next_1_week_qty,
                                                        NULL,                -- next_2_week_qty,
                                                        NULL,                -- next_3_week_qty,
                                                        diff1_id,
                                                        diff1_desc,
                                                        diff2_id,
                                                        diff2_desc,
                                                        diff3_id,
                                                        diff3_desc,
                                                        item_parent_id,      -- parent_item_id,
                                                        created_order_no,
                                                        created_supplier_id,
                                                        future_unit_retail,
                                                        rush_flag,
                                                        cost,
                                                        in_store_date,
                                                        NULL,                -- future_on_hand_qty,
                                                        order_no,
                                                        source_type,
                                                        gross_need_qty,      -- gross_need_qty,
                                                        rloh_qty,            -- rloh_qty,
                                                        'N',                 -- filtered_ind,
                                                        NULL,                -- parent_item_loc_session_id,
                                                        USER,                -- created_by,
                                                        USER,                -- updated_by,
                                                        SYSDATE,             -- created_date,
                                                        SYSDATE,             -- update_date,
                                                        NULL,                -- object_version_id
                                                        loc_type)
                                                 select ail.item_loc_id,
                                                        ail.alloc_id,
                                                        ail.item_id item_node,
                                                        ail.wh_id wh,
                                                        ail.release_date,
                                                        ail.location_id loc_id,
                                                        ail.location_desc loc_desc,
                                                        ail.allocated_qty,
                                                        ail.calculated_qty,
                                                        ail.need_qty,
                                                        ail.gross_need_qty,
                                                        ail.rloh_qty,
                                                        ail.stock_on_hand,
                                                        ail.in_transit,
                                                        ail.on_order,
                                                        ail.on_alloc,
                                                        ail.alloc_out,
                                                        ail.som_qty,
                                                        ail.backorder_qty,
                                                        ail.freeze_ind,
                                                        ail.diff1_id,
                                                        ail.diff1_desc,
                                                        ail.diff2_id,
                                                        ail.diff2_desc,
                                                        ail.parent_item_id item_parent_id,
                                                        ail.created_order_no,
                                                        ail.created_supplier_id,
                                                        ail.parent_diff1_id,
                                                        ail.future_unit_retail,
                                                        ail.rush_flag,
                                                        ail.cost,
                                                        ail.in_store_date,
                                                        ail.order_no,
                                                        ail.source_type,
                                                        ail.item_desc,
                                                        ail.item_type,
                                                        ail.diff3_id,
                                                        ail.diff3_desc,
                                                        g.group_id,
                                                        g.group_desc,
                                                        g.group_type,
                                                        g.loc_group_id,
                                                        ail.loc_type
                                                   from alc_item_loc ail,
                                                       (select alg.alloc_id,
                                                                alg.loc_group_id,
                                                                al.location_id,
                                                                al.group_id,
                                                                alg.group_desc,
                                                                algd.group_type
                                                           from alc_loc_group alg,
                                                                alc_location al,
                                                                alc_loc_group_detail algd
                                                          where alg.alloc_id      = I_alloc_id
                                                            and algd.loc_group_id = alg.loc_group_id
                                                            and al.loc_group_id   = alg.loc_group_id
                                                            and al.group_id       = algd.group_id
                                                            and alg.alloc_id is NOT NULL
                                                            and rownum > 0) g
                                                  where g.location_id (+) = ail.location_id
                                                    and g.alloc_id (+)    = ail.alloc_id
                                                    and ail.alloc_id      = I_alloc_id;

   end if;

   --insert to qty limits
   insert into alc_session_quantity_limits (quantity_limits_session_id,
                                            item_id,
                                            location_id,
                                            facet_session_id,
                                            quantity_limits_id,
                                            alloc_id,
                                            dept,
                                            class,
                                            subclass,
                                            min,
                                            max,
                                            threshold,
                                            trend,
                                            wos,
                                            min_need,
                                            min_pack,
                                            max_pack)
                                     select alc_session_qty_limits_seq.NEXTVAL,
                                            ql.item_id,
                                            ql.location_id,
                                            I_facet_session_id,
                                            ql.quantity_limits_id,
                                            ql.alloc_id,
                                            ql.dept,
                                            ql.class,
                                            ql.subclass,
                                            ql.min,
                                            ql.max,
                                            ql.treshold,
                                            ql.trend,
                                            ql.wos,
                                            ql.min_need,
                                            ql.min_pack,
                                            ql.max_pack
                                       from alc_quantity_limits ql
                                      where ql.alloc_id = I_alloc_id;

   insert all into alc_session_item_loc_excl (item_loc_excl_session_id,
                                              facet_session_id,
                                              alloc_id,
                                              item_loc_excl_id,
                                              item_id,
                                              item_desc,
                                              location_id,
                                              location_desc,
                                              reason_code,
                                              diff1_id,
                                              source_location_id,
                                              order_no,
                                              source_type,
                                              loc_type)
                                       values(alc_session_item_loc_excl_seq.NEXTVAL,
                                              facet_session_id,
                                              alloc_id,
                                              item_loc_excl_id,
                                              item_id,
                                              item_desc,
                                              location_id,
                                              location_desc,
                                              reason_code,
                                              diff1_id,
                                              source_location_id,
                                              order_no,
                                              source_type,
                                              loc_type)
                                       select I_facet_session_id facet_session_id,
                                              aile.alloc_id alloc_id,
                                              aile.item_loc_exclusion_id item_loc_excl_id,
                                              aile.item_id item_id,
                                              aile.item_desc item_desc,
                                              aile.location_id location_id,
                                              aile.location_desc location_desc,
                                              aile.reason_code reason_code, -- Item-loc status validation reason code
                                              aile.diff1_id diff1_id,
                                              aile.source_location_id source_location_id,
                                              aile.order_no,
                                              aile.source_type,
                                              v_lcan.loc_type
                                         from alc_item_loc_exclusion aile,
                                              v_loc_comm_attrib_nosec v_lcan
                                        where aile.alloc_id    = I_alloc_id
                                          and aile.location_id = v_lcan.loc;

   if SET_PACK_QTY(O_error_message,
                   I_facet_session_id) = FALSE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;


   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return ALC_CONSTANTS_SQL.FAILURE;
END LOAD_ALC_ITEM_LOC;
------------------------------------------------------------------------------------------------------------------
FUNCTION SPREAD_ALL_LOC(O_error_message              IN OUT VARCHAR2,
                        I_spread_method              IN     NUMBER,
                        I_facet_session_id           IN     VARCHAR2,
                        I_alc_spread_attr_tbl        IN     ALC_SPREAD_ATTR_TBL)
RETURN NUMBER IS

   L_program           VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.SPREAD_ALL_LOC';

   L_old_total_qty     NUMBER := 0;
   L_total_qty         NUMBER := 0;
   L_update_qty        NUMBER := 0;

   L_update_rec        OBJ_NUM_NUM_STR_REC;
   L_update_tbl        OBJ_NUM_NUM_STR_TBL := OBJ_NUM_NUM_STR_TBL();

   cursor C_SUM is
      select asil.item_loc_session_id,
             asil.item_id,
             asil.location_id,
             asil.wh_id,
             asil.source_type,
             asil.order_no,
             asil.freeze_ind,
             asil.result_filter_ind,
             asil.qbe_filter_ind,
             NVL(children.allocqty, asil.allocated_qty) allocated_qty,
             --
             SUM(case
                    when (asil.freeze_ind = 'N' and asil.result_filter_ind = 'N' and asil.qbe_filter_ind = 'N') then
                       NVL(children.allocqty, asil.allocated_qty)
                 else
                    0
                 end)
             OVER(PARTITION BY asil.item_id,
                               asil.wh_id,
                               spr.source_type,
                               NVL(spr.source_id,'-999')) sum_qty,
             --
             spr.allocated_qty -
                SUM(case
                       when (asil.freeze_ind = 'Y' or asil.result_filter_ind = 'Y' or asil.qbe_filter_ind = 'Y') then
                          asil.allocated_qty
                    else
                       0
                    end)
                OVER(PARTITION BY asil.item_id,
                                  asil.wh_id,
                                  spr.source_type,
                                  NVL(spr.source_id,'-999')) total_to_spread,
             --
             SUM(case
                    when (asil.freeze_ind = 'N' and asil.result_filter_ind = 'N' and asil.qbe_filter_ind = 'N') then
                       1
                    else
                       0
                   end)
                OVER(PARTITION BY asil.item_id,
                                  asil.wh_id,
                                  spr.source_type,
                                  NVL(spr.source_id,'-999')) target_cnt,
             --
             COUNT(asil.item_loc_session_id)
                OVER(PARTITION BY asil.item_id,
                                  asil.wh_id,
                                  spr.source_type,
                                  NVL(spr.source_id,'-999')) row_cnt,
             --
             row_number()
                OVER(PARTITION BY asil.item_id,
                                  asil.wh_id,
                                  spr.source_type,
                                  NVL(spr.source_id,'-999')
                         order by asil.item_id,
                                  asil.wh_id,
                                  spr.source_type,
                                  NVL(spr.source_id,'-999'), asil.location_id) current_row
        from alc_session_item_loc asil,
             table(cast(I_alc_spread_attr_tbl as ALC_SPREAD_ATTR_TBL)) spr,
             --for staple allocations this will not find anything
             (select SUM(NVL(pack_comp_qty,1)*allocated_qty) allocqty,
                     item_id,
                     wh_id,
                     source_type,
                     order_no,
                     location_id
                from alc_session_item_loc
               where facet_session_id           = I_facet_session_id
                 and parent_item_loc_session_id is NOT NULL
               group by item_id,
                        wh_id,
                        source_type,
                        order_no,
                        location_id) children
       where asil.facet_session_id                = I_facet_session_id
             and asil.parent_item_loc_session_id  is NULL
             and asil.item_id                     = children.item_id(+)
             and asil.wh_id                       = children.wh_id(+)
             and asil.source_type                 = NVL(children.source_type(+), asil.source_type)
             and NVL(asil.order_no,'-999')        = NVL(children.order_no(+), NVL(asil.order_no,'-999'))
             and asil.location_id                 = children.location_id(+)
             and asil.item_id                     = spr.item_id
             and asil.wh_id                       = spr.wh
             and asil.source_type                 = NVL(DECODE(spr.source_type,
                                                               '-1', NULL,
                                                               spr.source_type),
                                                        asil.source_type)
             and NVL(asil.order_no,'-999')        = NVL(DECODE(spr.source_id,
                                                               '-1', NULL,
                                                               spr.source_id),
                                                        NVL(asil.order_no,'-999'))
       order by asil.item_id,
                asil.wh_id,
                asil.source_type,
                NVL(asil.order_no,'-999'),
                asil.location_id;

BEGIN

   --
   --spread qty at top-level-item/wh level to top-level-item/wh/store/source level.
   --

   for rec in C_SUM loop

   if rec.freeze_ind = 'N' and rec.result_filter_ind = 'N' and rec.qbe_filter_ind = 'N' then

      L_old_total_qty := L_total_qty;
      if I_spread_method = ALC_CONSTANTS_SQL.SPREAD_METHOD_PROPORTIONAL then
         if rec.sum_qty != 0 then
            L_total_qty := L_total_qty + ((rec.allocated_qty/rec.sum_qty)*rec.total_to_spread);
         else
            L_total_qty := L_total_qty + ((1/rec.target_cnt)*rec.total_to_spread);
         end if;
      elsif I_spread_method = ALC_CONSTANTS_SQL.SPREAD_METHOD_EVENLY then
         L_total_qty := L_total_qty + ((1/rec.target_cnt)*rec.total_to_spread);
      end if;



      L_update_qty := ROUND(L_total_qty) - ROUND(L_old_total_qty);

      if L_update_qty < 0 then
         L_update_qty := 0;
      end if;

      L_update_rec := OBJ_NUM_NUM_STR_REC (rec.item_loc_session_id, L_update_qty, NULL);
      L_update_tbl.EXTEND;
      L_update_tbl(L_update_tbl.COUNT) := L_update_rec;

   end if;

   if rec.current_row = rec.row_cnt then
      L_old_total_qty := 0;
      L_total_qty     := 0;
      L_update_qty    := 0;
   end if;

   end loop;

   merge into alc_session_item_loc target
   using (select i.number_1 item_loc_session_id,
                 i.number_2 allocated_qty
            from table(cast(L_update_tbl as OBJ_NUM_NUM_STR_TBL)) i) use_this
   on (target.item_loc_session_id = use_this.item_loc_session_id)
   when MATCHED then
      update
         set target.allocated_qty = use_this.allocated_qty;

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return ALC_CONSTANTS_SQL.FAILURE;
END SPREAD_ALL_LOC;
------------------------------------------------------------------------------------------------------------------
FUNCTION SPREAD_SINGLE_LOC(O_error_message              IN OUT VARCHAR2,
                           I_facet_session_id           IN     VARCHAR2,
                           I_alc_spread_loc_attr_tbl    IN     ALC_SPREAD_LOC_ATTR_TBL)
RETURN NUMBER IS

   L_program           VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.SPREAD_SINGLE_LOC';

   L_old_total_qty     number := 0;
   L_total_qty         number := 0;
   L_update_qty        number := 0;

   L_update_rec        OBJ_NUM_NUM_STR_REC;
   L_update_tbl        OBJ_NUM_NUM_STR_TBL := OBJ_NUM_NUM_STR_TBL();

   cursor C_SUM is
    select asil.item_loc_session_id,
           asil.item_id,
           asil.location_id,
           asil.wh_id,
           asil.freeze_ind,
           NVL(children.allocqty, asil.allocated_qty) allocated_qty,
           --
           SUM(DECODE(asil.freeze_ind,
                      'N', NVL(children.allocqty, asil.allocated_qty),
                      0))  OVER(PARTITION BY asil.item_id,
                                             asil.wh_id,
                                             asil.location_id) sum_qty,
           --
           spr.allocated_qty -
           SUM(DECODE(asil.freeze_ind,
                      'Y', asil.allocated_qty,0)) OVER(PARTITION BY asil.item_id,
                                                                    asil.wh_id,
                                                                    asil.location_id) total_to_spread,
           --
           COUNT(asil.item_loc_session_id) OVER(PARTITION BY asil.item_id,
                                                             asil.wh_id,
                                                             asil.location_id) target_cnt,
           --
           row_number() OVER(PARTITION BY asil.item_id,
                                          asil.wh_id,
                                          asil.location_id
                                 ORDER BY asil.order_no) current_row
     from alc_session_item_loc asil,
          table(cast(I_alc_spread_loc_attr_tbl as ALC_SPREAD_LOC_ATTR_TBL)) spr,
          --for staple allocations this will not find anything
          (select SUM(NVL(pack_comp_qty,1)*allocated_qty) allocqty,
                  item_id,
                  wh_id,
                  location_id
             from alc_session_item_loc
            where facet_session_id           = I_facet_session_id
              and parent_item_loc_session_id is NOT NULL
            group by item_id,
                     wh_id,
                     location_id) children
    where asil.facet_session_id            = I_facet_session_id
      and asil.parent_item_loc_session_id  is NULL
      and asil.item_id                     = children.item_id(+)
      and asil.location_id                 = children.location_id(+)
      and asil.location_id                 = children.location_id(+)
      and asil.item_id                     = spr.item_id
      and asil.wh_id                       = spr.wh
      and asil.location_id                 = spr.loc
    order by asil.wh_id,
             asil.item_id,
             asil.location_id,
             asil.order_no;

BEGIN
   --
   --spread qty at top-level-item/wh/store level to top-level-item/wh/store/source level.
   --

   for rec in C_SUM loop

   if rec.freeze_ind = 'N' then

      L_old_total_qty := L_total_qty;
      if rec.sum_qty != 0 then
         L_total_qty := L_total_qty + ((rec.allocated_qty/rec.sum_qty)*rec.total_to_spread);
               else
         L_total_qty := L_total_qty + ((1/rec.target_cnt)*rec.total_to_spread);
      end if;
      L_update_qty := ROUND(L_total_qty) - ROUND(L_old_total_qty);

      if L_update_qty < 0 then
         L_update_qty := 0;
     end if;

      L_update_rec := OBJ_NUM_NUM_STR_REC (rec.item_loc_session_id, L_update_qty, null);
      L_update_tbl.extend;
      L_update_tbl(L_update_tbl.COUNT) := L_update_rec;

       end if;

   if rec.current_row = rec.target_cnt then
      L_old_total_qty := 0;
      L_total_qty     := 0;
      L_update_qty    := 0;
   end if;

   end loop;

   merge into alc_session_item_loc target
   using (select i.number_1 item_loc_session_id,
                 i.number_2 allocated_qty
            from table(cast(L_update_tbl as OBJ_NUM_NUM_STR_TBL)) i) use_this
   on (target.item_loc_session_id = use_this.item_loc_session_id)
   when MATCHED then
      update
         set target.allocated_qty = use_this.allocated_qty;

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return ALC_CONSTANTS_SQL.FAILURE;
END SPREAD_SINGLE_LOC;
------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------
-- I_rule_need_type is 1 then the Need is Exact , If I_rule_need_type is 2 then Need is Proportional.
-- O_valid_ind is 0 Sum(min_need) > available (Error Condition)
-- O_valid_ind is 1 Sum(max need) < available (Error Condition) and rule should be Proportional
-- O_valid_ind is 2 Sum(min need) > available  Sum(max need) < available  only when the Rule is Proportional
-- O_valid_ind is 3 then the Validation is Successful.
------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_AVAIL_QTY(O_error_message              IN OUT VARCHAR2,
                                 O_valid_ind_tbl              IN OUT ALC_VALID_IND_TBL,
                                 I_rule_need_type             IN     NUMBER,
                                 I_facet_session_id           IN     VARCHAR2,
                                 I_alc_item_source_attr       IN     ALC_ITEM_SOURCE_ATTR_TBL)
RETURN NUMBER IS

   L_program   VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.VALIDATE_ITEM_AVAIL_QTY';
   L_min       NUMBER;
   L_max       NUMBER;
   L_min_pack  NUMBER;
   L_max_pack  NUMBER;

    cursor C_AWIS is
       select ALC_VALID_IND_REC (out.item_id,
                                 out.avail_sum_qty,
                                 case
                                    when (out.L_min > 0) then
                                       0
                                    when (out.L_max > 0) then
                                       1
                                    when (out.L_min > 0 and out.L_max > 0 ) then
                                       2
                                    when (out.L_min_pack > 0) then
                                       0
                                    when (out.L_max_pack > 0) then
                                       1
                                    when (out.L_min_pack > 0 and out.L_max_pack > 0 ) then
                                       2
                                    else
                                       3
                                 end )
                           from (select t.L_min,
                                        t.L_max,
                                        t.L_min_pack,
                                        t.L_max_pack,
                                        t.avail_sum_qty,
                                        t.item_id
                                   from (select case
                                                   when (availcomp.min_need_qty is null) then
                                                      0
                                                   when availcomp.avail_sum_qty >= availcomp.min_need_qty then
                                                      0
                                                else
                                                   1
                                                end as l_min,
                                                case
                                                   when (availcomp.max_need_qty is NULL) then
                                                      0
                                                   when (I_rule_need_type=1) then
                                                      0
                                                   when (availcomp.null_exist = -999 and I_rule_need_type = 2) then
                                                      0
                                                   when (availcomp.avail_sum_qty <= availcomp.max_need_qty and I_rule_need_type = 2) then
                                                      0
                                                else
                                                   1
                                                end as l_max,
                                                case
                                                   when (availcomp.min_pack_qty is NULL) then
                                                      0
                                                   when availcomp.avail_sum_qty >= availcomp.min_pack_qty then
                                                      0
                                                else
                                                   1
                                                end as l_min_pack,
                                                case
                                                   when (availcomp.max_pack_qty is null) then
                                                      0
                                                   when (I_rule_need_type=1) then
                                                      0
                                                   when (availcomp.null_exist_pack = -999 and I_rule_need_type = 2) then
                                                      0
                                                   when (availcomp.avail_sum_qty <= availcomp.max_pack_qty and I_rule_need_type = 2) then
                                                      0
                                                 else
                                                    1
                                                end as l_max_pack,
                                                availcomp.avail_sum_qty,
                                                availcomp.item_id
                                           from (select DISTINCT
                                                        asql.item_id,
                                                        avail_sum_qty,
                                                        SUM(asql.min) min_need_qty,
                                                        SUM(asql.max) max_need_qty,
                                                        SUM(asql.min_pack) min_pack_qty,
                                                        SUM(asql.max_pack) max_pack_qty,
                                                        MIN(NVL(asql.max, -999)) null_exist,
                                                        MIN(NVL(asql.max_pack, -999)) null_exist_pack
                                                   from alc_session_quantity_limits asql,
                                                        (select DISTINCT
                                                                aisat.ITEM_NODE as item,
                                                                SUM(aisat.AVAILABLE_QTY) as avail_sum_qty
                                                           from table(cast(I_alc_item_source_attr as ALC_ITEM_SOURCE_ATTR_TBL)) aisat
                                                          group by aisat.ITEM_NODE) itemsrc
                                                  where asql.facet_session_id = I_facet_session_id
                                                  --
                                                    and asql.item_id = itemsrc.item
                                                  group by asql.item_id,avail_sum_qty) availcomp)t ) out;
BEGIN

   open  C_AWIS;
   fetch C_AWIS BULK COLLECT into O_valid_ind_tbl;
   close C_AWIS;


   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return ALC_CONSTANTS_SQL.FAILURE;
END VALIDATE_ITEM_AVAIL_QTY;
------------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_QTY(O_error_message      IN OUT VARCHAR2,
                      I_facet_session_id   IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.SET_PACK_QTY';

BEGIN

   merge into alc_session_item_loc target
   using (select DISTINCT sil.item_loc_session_id,
                 SUM(pb.item_qty) OVER (PARTITION BY sil.item_loc_session_id) pack_comp_qty
            from alc_session_item_loc sil,
                 packitem_breakout pb
           where sil.facet_session_id  = I_facet_session_id
             and sil.item_type        != ALC_CONSTANTS_SQL.SELLABLEPACK
             and sil.item_id           = pb.pack_no
         ) use_this
   on (target.item_loc_session_id = use_this.item_loc_session_id)
   when MATCHED then
      update
         set target.pack_comp_qty = use_this.pack_comp_qty;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_PACK_QTY;
------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_ALLOCATED_QTY(O_error_message      IN OUT VARCHAR2,
                                     O_failure_key           OUT VARCHAR2,
                                     I_freeze_ind         IN     VARCHAR2,
                                     I_facet_session_id   IN     VARCHAR2,
                                     I_alc_spread_attr    IN     ALC_SPREAD_ATTR_TBL)
RETURN NUMBER IS
   L_program         VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.VALIDATE_ITEM_ALLOCATED_QTY';

   cursor C_ALLOC_ITEM is
   select total_alloc.item_id,
          total_alloc.allocated_qty - NVL(detailtotal.allocated_qty,0) check_val
     from (select item_id,
                  SUM(allocated_qty) allocated_qty
             from table(cast(i_alc_spread_attr as alc_spread_attr_tbl))
            group by item_id) total_alloc,
          (select item_id,
                  SUM(allocated_qty) allocated_qty
             from alc_session_item_loc asil
            where I_freeze_ind           = 'Y'
              and (asil.freeze_ind        = 'Y' or asil.result_filter_ind = 'Y' or qbe_filter_ind    = 'Y')
              and asil.facet_session_id  = I_facet_session_id
            group by item_id
            union all
           select item_id,
                  SUM(allocated_qty) allocated_qty
             from alc_session_item_loc asil2
            where asil2.facet_session_id = I_facet_session_id
              and I_freeze_ind <> 'Y'
              and asil2.result_filter_ind = 'N'
              and asil2.qbe_filter_ind    = 'N'
            group by item_id) detailtotal
    where total_alloc.item_id (+)= detailtotal.item_id;

BEGIN

   for rec in C_ALLOC_ITEM loop
      if rec.check_val <0 then
        -- need to return item as a table to UI for display
        O_failure_key:= ALC_CONSTANTS_SQL.ERRMSG_ALLOC_QTY_GT_RST_QTY;
        return ALC_CONSTANTS_SQL.FAILURE;
      end if;
   end loop;

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
    WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(sqlcode));
      O_failure_key:= ALC_CONSTANTS_SQL.ERRMSG_DEFAULT_MSG;
      return ALC_CONSTANTS_SQL.FAILURE;
END VALIDATE_ITEM_ALLOCATED_QTY;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_EXCLUSIONS(O_error_message          IN OUT VARCHAR2,
                        I_facet_session_id       IN     VARCHAR2,
                        I_work_header_id         IN     NUMBER,
                        I_alloc_type             IN     ALC_ALLOC.TYPE%TYPE,
                        I_what_if_ind            IN     NUMBER DEFAULT 0,
                        I_nfrc_supp_chain_ind    IN     NUMBER DEFAULT 0,
                        I_size_profile           IN     ALC_RULE.SIZE_PROFILE%TYPE DEFAULT NULL,
                        I_size_profile_type      IN     ALC_RULE.SIZE_PROFILE_TYPE%TYPE DEFAULT NULL,
                        I_pack_threshold         IN     ALC_RULE.PACK_THRESHOLD%TYPE DEFAULT NULL,
                        I_rule_mode              IN     ALC_RULE.RULE_MODE%TYPE DEFAULT NULL,
                        I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL,
                        I_spwhavailpct_ind       IN     ALC_RULE.SIZE_PROFILE_WH_AVAIL_PCT_IND%TYPE DEFAULT NULL,
                        I_spdflthier_ind         IN     ALC_RULE.SIZE_PROFILE_DEFAULT_HIER_IND%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POP_EXCLUSIONS';
   L_start_time                TIMESTAMP    := SYSTIMESTAMP;

   L_range_required            NUMBER(1)   := 0;
   L_invalid_status            OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_tp_distribution_level     ALC_SYSTEM_OPTIONS.TP_DISTRIBUTION_LEVEL%TYPE   := '1';
   L_tp_sizeprof_validn_enbl   ALC_SYSTEM_OPTIONS.TP_SIZEPROF_VALIDN_ENBL%TYPE := 'Y';
   L_process_staple            NUMBER(10)  := 0;
   L_check_size_profile        VARCHAR2(1) := 'Y';
   L_fp_pack_ranging           ALC_SYSTEM_OPTIONS.FP_PACK_RANGING%TYPE;

   cursor C_RANGED_REQ is
      select DECODE(invalid_status, 'NULL', 1, 0)
        from (select REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd,'[^ ]+', 1, level) invalid_status
                from alc_system_options
     connect by REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd, '[^ ]+', 1, level) is NOT NULL)
       where UPPER(invalid_status) = 'NULL'
         and I_what_if_ind = 0
      union all
      select DECODE(invalid_status, 'NULL', 1, 0)
        from (select REGEXP_SUBSTR(tp_loc_excp_rsn_what_if,'[^ ]+', 1, level) invalid_status
                from alc_system_options
     connect by REGEXP_SUBSTR(tp_loc_excp_rsn_what_if, '[^ ]+', 1, level) is NOT NULL)
       where UPPER(invalid_status) = 'NULL'
         and I_what_if_ind = 1;

   cursor C_INVALID_STATUS is
      select invalid_status
        from (select REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd,'[^ ]+', 1, level) invalid_status
                from alc_system_options
     connect by REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd, '[^ ]+', 1, level) is NOT NULL)
       where UPPER(invalid_status) != 'NULL'
         and I_what_if_ind = 0
      union all
      select invalid_status
        from (select REGEXP_SUBSTR(tp_loc_excp_rsn_what_if,'[^ ]+', 1, level) invalid_status
                from alc_system_options
     connect by REGEXP_SUBSTR(tp_loc_excp_rsn_what_if, '[^ ]+', 1, level) is NOT NULL)
       where UPPER(invalid_status) != 'NULL'
         and I_what_if_ind = 1;

   cursor C_ITEM_TYPE_PACK is
      select NVL(SUM(invalid_cnt), 0)
        from (select 1 invalid_cnt
                from alc_session_itemloc_gtt a
               where a.facet_session_id  = I_facet_session_id
                 and a.item_type NOT IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                         ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                         ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                         ALC_CONSTANTS_SQL.PACKCOMP)
              union all
              select COUNT(DISTINCT im.item_parent) - 1 invalid_cnt
                from alc_session_itemloc_gtt a,
                     packitem_breakout pb,
                     item_master im
               where a.facet_session_id  = I_facet_session_id
                 and a.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                     ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                     ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK)
                 and a.item_id           = pb.pack_no
                 and pb.item             = im.item
               group by pb.pack_no);

BEGIN

   delete
     from gtt_num_num_str_str_date_date;

   LOGGER.LOG_INFORMATION(L_program||' Delete GTT_NUM_NUM_STR_STR_DATE_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from numeric_id_gtt;

   LOGGER.LOG_INFORMATION(L_program||' Delete NUMERIC_ID_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select tp_distribution_level,
          tp_sizeprof_validn_enbl,
          fp_pack_ranging
     into L_tp_distribution_level,
          L_tp_sizeprof_validn_enbl,
          L_fp_pack_ranging
     from alc_system_options;

   open C_RANGED_REQ;
   fetch C_RANGED_REQ into L_range_required;
   close C_RANGED_REQ;

   open C_INVALID_STATUS;
   fetch C_INVALID_STATUS BULK COLLECT into L_invalid_status;
   close C_INVALID_STATUS;

   if I_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION then

      if I_rule_mode = ALC_CONSTANTS_SQL.RULE_MODE_PACK_DISTRIBUTION then
         L_process_staple := 1;
      else
         open C_ITEM_TYPE_PACK;
         fetch C_ITEM_TYPE_PACK into L_process_staple;
         close C_ITEM_TYPE_PACK;
      end if;

   end if;

   if (I_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION and L_process_staple > 0) or
       I_size_profile is NULL or L_tp_sizeprof_validn_enbl = 'N' or I_size_profile_type = 'SO' then
       L_check_size_profile := 'N';
   else
      if FIND_SIZE_PROFILE(O_error_message,
                           I_facet_session_id,
                           I_alloc_type,
                           I_size_profile) = FALSE then
         return FALSE;
      end if;

      if I_spdflthier_ind = 'Y' and I_size_profile != 'Hierarchy' then

         if FIND_SIZE_PROFILE(O_error_message,
                              I_facet_session_id,
                              I_alloc_type,
                              'Hierarchy') = FALSE then
            return FALSE;
         end if;
      end if;

   end if;

   insert into gtt_num_num_str_str_date_date(number_1,
                                             number_2,
                                             varchar2_1,
                                             varchar2_2)
      with asil_im as
         (select asil.facet_session_id,
                 asil.alloc_id,
                 asil.parent_item_loc_session_id,
                 asil.item_loc_session_id,
                 asil.location_id,
                 asil.location_desc,
                 asil.release_date,
                 asil.loc_type,
                 asil.wh_id,
                 im.item,
                 im.item_level,
                 im.tran_level,
                 im.item_parent,
                 im.item_grandparent,
                 im.dept,
                 im.class,
                 im.subclass
            from alc_session_itemloc_gtt asil,
                 item_master im
           where asil.facet_session_id = I_facet_session_id
             and asil.item_id          = im.item
             and im.item_level         = im.tran_level
         ),
         asil_im_il as
           (select DISTINCT
                   asil_inner.facet_session_id,
                   asil_inner.parent_item_loc_session_id,
                   asil_inner.item_loc_session_id,
                   asil_inner.location_id,
                   asil_inner.location_desc,
                   asil_inner.loc_type,
                   asil_inner.wh_id,
                   asil_inner.item,
                   asil_inner.item_level,
                   asil_inner.tran_level,
                   il.status,
                   asil_inner.item il_item,
                   il.loc,
                   NVL(DECODE(il.source_method, 'W',
                              il.source_wh),
                       l.default_wh) source_wh
              from (select DISTINCT
                           asil.facet_session_id,
                           asil.parent_item_loc_session_id,
                           asil.item_loc_session_id,
                           asil.location_id,
                           asil.location_desc,
                           asil.loc_type,
                           asil.wh_id,
                           im.item,
                           im.item_level,
                           im.tran_level,
                           case
                              when asil.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                      ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                      ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                      ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                      ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                                      ALC_CONSTANTS_SQL.SELLABLEPACK)
                                 then pb.item
                              else
                                 asil.item_id
                           end as join_item
                      from alc_session_itemloc_gtt asil,
                           item_master im,
                           packitem_breakout pb
                     where asil.facet_session_id = I_facet_session_id
                       and asil.item_id          = im.item
                       and im.item_level         = im.tran_level
                       and asil.item_id          = pb.pack_no(+)) asil_inner,
                     item_loc il,
                     v_alloc_supply_chain l
              where asil_inner.facet_session_id = I_facet_session_id
                and asil_inner.join_item        = il.item (+)
                and asil_inner.location_id      = il.loc(+)
                and asil_inner.location_id      = l.location
                and L_fp_pack_ranging           = 'C'
              union all
             select asil.facet_session_id,
                    asil.parent_item_loc_session_id,
                    asil.item_loc_session_id,
                    asil.location_id,
                    asil.location_desc,
                    asil.loc_type,
                    asil.wh_id,
                    im.item,
                    im.item_level,
                    im.tran_level,
                    il.status,
                    il.item il_item,
                    il.loc,
                    --
                    NVL(DECODE(il.source_method, 'W',
                               il.source_wh),
                        l.default_wh) source_wh
               from alc_session_itemloc_gtt asil,
                    item_master im,
                    item_loc il,
                    (select store loc,
                            default_wh
                       from store
                      union all
                     select wh loc,
                            default_wh
                       from wh) l
              where asil.facet_session_id = I_facet_session_id
                and asil.item_id          = im.item
                and im.item_level         = im.tran_level
                and asil.item_id          = il.item(+)
                and asil.location_id      = il.loc(+)
                and asil.location_id      = l.loc
                and L_fp_pack_ranging     = 'P')
      --delete size profile
      select DISTINCT
             NVL(asil_im.parent_item_loc_session_id, asil_im.item_loc_session_id) item_loc_session_id,
             asil_im.location_id,
             asil_im.location_desc,
             ALC_CONSTANTS_SQL.EXCLUDE_SIZE_PROFILE reason
        from asil_im
       where L_check_size_profile  = 'Y'
         and asil_im.item_loc_session_id NOT IN (select gtt.numeric_id
                                                   from numeric_id_gtt gtt
                                                  where gtt.numeric_id = asil_im.item_loc_session_id)
         and (case
                 when (I_spwhavailpct_ind = 'Y' and asil_im.loc_type = 'W') then
                    0
                 else
                    1
              end) = 1
      union all
      --delete non-ranged
      select DISTINCT
             NVL(asil_im_il.parent_item_loc_session_id, asil_im_il.item_loc_session_id) item_loc_session_id,
             asil_im_il.location_id,
             asil_im_il.location_desc,
             ALC_CONSTANTS_SQL.EXCLUDE_ITEM_LOC_STATUS reason
        from asil_im_il
       where asil_im_il.status     is NULL
         and L_range_required      = 1
         and I_nfrc_supp_chain_ind = '1'
      union all
      --delete non 'A' status on item_loc
      select /*+ CARDINALITY (v 5)*/
             DISTINCT
             NVL(asil_im_il.parent_item_loc_session_id, asil_im_il.item_loc_session_id) item_loc_session_id,
             asil_im_il.location_id,
             asil_im_il.location_desc,
             ALC_CONSTANTS_SQL.EXCLUDE_ITEM_LOC_STATUS reason
        from asil_im_il,
             table(cast(L_invalid_status as OBJ_VARCHAR_ID_TABLE)) v
       where asil_im_il.status  = value(v)
      union all
      --delete store open/close dates
      select DISTINCT
             NVL(asil_im.parent_item_loc_session_id, asil_im.item_loc_session_id) item_loc_session_id,
             asil_im.location_id,
             asil_im.location_desc,
             ALC_CONSTANTS_SQL.EXCLUDE_STORE_CLOSED reason
        from asil_im,
             store s
       where asil_im.location_id      = s.store
         and asil_im.loc_type         = 'S'
         and (asil_im.release_date    < (s.store_open_date - s.start_order_days)
              or
              asil_im.release_date    >= (s.store_close_date -  NVL(s.stop_order_days,0)))
      union all
      -- delete MLD
      -- warehouse to store / warehouse allocations
      select DISTINCT
             NVL(asil_im_il.parent_item_loc_session_id, asil_im_il.item_loc_session_id) item_loc_session_id,
             asil_im_il.location_id,
             asil_im_il.location_desc,
             ALC_CONSTANTS_SQL.EXCLUDE_MLD_PATH reason
        from asil_im_il
       where asil_im_il.wh_id        != NVL(asil_im_il.source_wh, -999)
         --
         and L_tp_distribution_level = '0'
         and I_nfrc_supp_chain_ind   = '1'
         and I_what_if_ind           = 0
     union all
     -- delete wh cycles
     select DISTINCT
            NVL(asil_im.parent_item_loc_session_id, asil_im.item_loc_session_id) item_loc_session_id,
            asil_im.location_id,
            asil_im.location_desc,
            ALC_CONSTANTS_SQL.EXCLUDE_WH_CYCLE reason
       from asil_im,
            table(cast(I_alc_item_source_attr as ALC_ITEM_SOURCE_ATTR_TBL)) input
      where asil_im.location_id       = input.wh
         --
        and I_what_if_ind             = 0
	 union all
      -- delete locations that do not pass Cross Legal Entities
      select distinct
             nvl(asil_im.parent_item_loc_session_id, asil_im.item_loc_session_id) item_loc_session_id,
             asil_im.location_id,
             asil_im.location_desc,
             ALC_CONSTANTS_SQL.EXCLUDE_CROSS_LEGAL_ENTITIES reason
        from alc_Session_item_loc asil_im,
             store s,
             wh w,
             alc_system_options aso
            WHERE s.TSF_ENTITY_ID <> w.TSF_ENTITY_ID 
            and s.store = asil_im.location_id 
            and w.wh = asil_im.wh_id
            and aso.FP_ENFORCE_MLE = 'Y'
            and asil_im.facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert GTT_NUM_NUM_STR_STR_DATE_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- if no WH rule priority has been identified when enforce supply chain = N, exclude the records
   insert into gtt_num_num_str_str_date_date(number_1,
                                             number_2,
                                             varchar2_1,
                                             varchar2_2)
                                      select NVL(asil.parent_item_loc_session_id, asil.item_loc_session_id) item_loc_session_id,
                                             asil.location_id,
                                             asil.location_desc,
                                             ALC_CONSTANTS_SQL.EXCLUDE_SUPPLY_CHAIN_PRIORITY reason
                                        from alc_session_itemloc_gtt asil
                                       where asil.facet_session_id = I_facet_session_id
                                         and I_nfrc_supp_chain_ind = 0
                                         and I_what_if_ind         = 0
                                         and NOT EXISTS (select 'x'
                                                           from alc_session_wh_priority_gtt gtt
                                                          where gtt.facet_session_id = I_facet_session_id
                                                            and gtt.facet_session_id = asil.facet_session_id
                                                            and gtt.item_id          = asil.item_id
                                                            and gtt.location_id      = asil.location_id);

   LOGGER.LOG_INFORMATION(L_program||' Insert GTT_NUM_NUM_STR_STR_DATE_DATE - NO WH RULE PRIORITY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into alc_session_item_loc_excl(item_loc_excl_session_id,
                                         facet_session_id,
                                         alloc_id,
                                         item_id,
                                         item_desc,
                                         location_id,
                                         location_desc,
                                         reason_code,
                                         diff1_id,
                                         source_location_id,
                                         order_no,
                                         source_type,
                                         loc_type)
                                  select alc_session_item_loc_excl_seq.NEXTVAL,
                                         I_facet_session_id,
                                         NULL,
                                         sil.item_id,
                                         sil.item_desc,
                                         sil.location_id,
                                         sil.location_desc,
                                         gtt.varchar2_2,
                                         sil.diff1_id,
                                         sil.wh_id,
                                         sil.order_no,
                                         sil.source_type,
                                         sil.loc_type
                                    from gtt_num_num_str_str_date_date gtt,
                                         alc_session_itemloc_gtt sil
                                   where gtt.number_1 = sil.item_loc_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SESSION_ITEM_LOC_EXCL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_session_itemloc_gtt sil
    where sil.item_loc_session_id IN (select g1.number_1
                                        from gtt_num_num_str_str_date_date g1
                                       union all
                                      select ilsi.item_loc_session_id
                                        from alc_session_itemloc_gtt ilsi,
                                             gtt_num_num_str_str_date_date g2
                                       where ilsi.parent_item_loc_session_id = g2.number_1);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_ITEMLOC_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_EXCLUSIONS;
------------------------------------------------------------------------------------------------------------------
FUNCTION FIND_SIZE_PROFILE(O_error_message        IN OUT VARCHAR2,
                           I_facet_session_id     IN     VARCHAR2,
                           I_alloc_type           IN     ALC_ALLOC.TYPE%TYPE,
                           I_size_profile         IN     ALC_RULE.SIZE_PROFILE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.FIND_SIZE_PROFILE';

   L_dept_ind                   VARCHAR2(1) := 'N';
   L_class_ind                  VARCHAR2(1) := 'N';
   L_subclass_ind               VARCHAR2(1) := 'N';
   L_style_ind                  VARCHAR2(1) := 'N';
   L_stylecolor_ind             VARCHAR2(1) := 'N';
   L_gid_profile_id             NUMBER(10)  := 0;

   cursor C_VALID_LEVELS is
      select valid_lvl
        from ( select REGEXP_SUBSTR(tp_sizeprof_valid_levels,'[^,]+', 1, level) valid_lvl
                 from alc_system_options
              connect by REGEXP_SUBSTR(tp_sizeprof_valid_levels, '[^,]+', 1, level) is NOT NULL);

BEGIN

   if I_size_profile = 'Hierarchy' then
      L_gid_profile_id := -1;
   else
      select MIN(gid_profile_id)
        into L_gid_profile_id
        from alc_gid_profile
       where gid_id = I_size_profile;
   end if;

   for rec in C_VALID_LEVELS loop

      if rec.valid_lvl = 'DEPT' then
         L_dept_ind := 'Y';
      elsif rec.valid_lvl = 'CLASS' then
         L_class_ind := 'Y';
      elsif rec.valid_lvl = 'SUBCLASS' then
         L_subclass_ind := 'Y';
      elsif rec.valid_lvl = 'STYLE' then
         L_style_ind := 'Y';
      elsif rec.valid_lvl = 'STYLE/COLOR' then
         L_stylecolor_ind := 'Y';
      end if;

   end loop;

   if L_dept_ind = 'Y' or L_class_ind = 'Y' or  L_subclass_ind = 'Y' then

      insert into numeric_id_gtt(numeric_id)
      select TO_NUMBER(il.item_loc_session_id)
        from alc_size_profile p,
             (select item.item_loc_session_id,
                     item.location_id store,
                     im.dept,
                     im.class,
                     im.subclass,
                     DECODE(p.diff_1_aggregate_ind,
                            'N', NULL,
                            im.diff_1) source_diff_1,
                     DECODE(p.diff_2_aggregate_ind,
                            'N', NULL,
                            im.diff_2) source_diff_2,
                     DECODE(p.diff_3_aggregate_ind,
                            'N', NULL,
                            im.diff_3) source_diff_3,
                     im.diff_1 tran_diff_1,
                     im.diff_2 tran_diff_2,
                     im.diff_3 tran_diff_3
                from alc_session_itemloc_gtt parent,
                     alc_session_itemloc_gtt item,
                     item_master im,
                     item_master p
               where parent.facet_session_id           = I_facet_session_id
                 and parent.facet_session_id           = item.facet_session_id
                 and parent.parent_item_loc_session_id is NULL
                 and parent.item_loc_session_id        = item.parent_item_loc_session_id
                 and item.item_id                      = im.item
                 and im.item_parent                    = p.item
                 and im.pack_ind                       = 'N'
              union all
              select item.item_loc_session_id,
                     item.location_id store,
                     im.dept,
                     im.class,
                     im.subclass,
                     DECODE(p.diff_1_aggregate_ind,
                            'N', NULL,
                            im.diff_1) source_diff_1,
                     DECODE(p.diff_2_aggregate_ind,
                            'N', NULL,
                            im.diff_2) source_diff_2,
                     DECODE(p.diff_3_aggregate_ind,
                            'N', NULL,
                            im.diff_3) source_diff_3,
                     im.diff_1 tran_diff_1,
                     im.diff_2 tran_diff_2,
                     im.diff_3 tran_diff_3
                from alc_session_itemloc_gtt parent,
                     alc_session_itemloc_gtt item,
                     packitem_breakout pb,
                     item_master im,
                     item_master p
               where parent.facet_session_id           = I_facet_session_id
                 and parent.facet_session_id           = item.facet_session_id
                 and parent.parent_item_loc_session_id is NULL
                 and parent.item_loc_session_id        = item.parent_item_loc_session_id
                 and item.item_id                      = pb.pack_no
                 and pb.item                           = im.item
                 and p.item                            = im.item_parent
                 and item.item_type                   != ALC_CONSTANTS_SQL.SELLABLEPACK
              union all
              select item.item_loc_session_id,
                     item.location_id store,
                     im.dept,
                     im.class,
                     im.subclass,
                     DECODE(p.diff_1_aggregate_ind,
                            'N', NULL,
                            im.diff_1) source_diff_1,
                     DECODE(p.diff_2_aggregate_ind,
                            'N', NULL,
                            im.diff_2) source_diff_2,
                     DECODE(p.diff_3_aggregate_ind,
                            'N', NULL,
                            im.diff_3) source_diff_3,
                     im.diff_1 tran_diff_1,
                     im.diff_2 tran_diff_2,
                     im.diff_3 tran_diff_3
                from alc_session_itemloc_gtt item,
                     packitem_breakout pb,
                     item_master im,
                     item_master p
               where item.facet_session_id           = I_facet_session_id
                 and item.parent_item_loc_session_id is NULL
                 and item.item_id                    = pb.pack_no
                 and pb.item                         = im.item
                 and p.item                          = im.item_parent
                 and item.item_type                 != ALC_CONSTANTS_SQL.SELLABLEPACK) il
       where NVL(p.gid_profile_id, -1) = L_gid_profile_id
         and p.loc                     = il.store
         and NVL(p.size1,0)            = NVL(DECODE(il.source_diff_1,
                                                    il.tran_diff_1, NULL,
                                                    il.tran_diff_1),
                                             0)
         and NVL(p.size2,0)            = NVL(DECODE(il.source_diff_2,
                                                    il.tran_diff_2, NULL,
                                                    il.tran_diff_2),
                                             0)
         and NVL(p.size3,0)            = NVL(DECODE(il.source_diff_3,
                                                    il.tran_diff_3, NULL,
                                                    il.tran_diff_3),
                                             0)
         and p.style                   is NULL
         and p.dept                    = il.dept
         and ((L_dept_ind = 'Y' and p.class is NULL)
              or p.class = il.class)
         and ((L_subclass_ind = 'Y' and p.subclass = il.subclass)
              or p.subclass is NULL);

   end if;

   if L_style_ind = 'Y' then

      insert into numeric_id_gtt(numeric_id)
      select TO_NUMBER(il.item_loc_session_id)
        from alc_size_profile p,
             (select item.item_loc_session_id,
                     item.location_id store,
                     im.item_parent
                from alc_session_itemloc_gtt parent,
                     alc_session_itemloc_gtt item,
                     item_master im,
                     item_master p
               where parent.facet_session_id           = I_facet_session_id
                 and parent.facet_session_id           = item.facet_session_id
                 and parent.parent_item_loc_session_id is NULL
                 and parent.item_loc_session_id        = item.parent_item_loc_session_id
                 and item.item_id                      = im.item
                 and im.item_parent                    = p.item
                 and im.pack_ind                       = 'N'
              union all
              select item.item_loc_session_id,
                     item.location_id store,
                     im.item_parent
                from alc_session_itemloc_gtt parent,
                     alc_session_itemloc_gtt item,
                     packitem_breakout pb,
                     item_master im,
                     item_master p
               where parent.facet_session_id           = I_facet_session_id
                 and parent.facet_session_id           = item.facet_session_id
                 and parent.parent_item_loc_session_id is NULL
                 and parent.item_loc_session_id        = item.parent_item_loc_session_id
                 and item.item_id                      = pb.pack_no
                 and pb.item                           = im.item
                 and p.item                            = im.item_parent
                 and item.item_type                   != ALC_CONSTANTS_SQL.SELLABLEPACK
              union all
              select item.item_loc_session_id,
                     item.location_id store,
                     im.item_parent
                from alc_session_itemloc_gtt item,
                     packitem_breakout pb,
                     item_master im,
                     item_master p
               where item.facet_session_id             = I_facet_session_id
                 and item.parent_item_loc_session_id is NULL
                 and item.item_id                      = pb.pack_no
                 and pb.item                           = im.item
                 and p.item                            = im.item_parent
                 and item.item_type                   != ALC_CONSTANTS_SQL.SELLABLEPACK  ) il
       where NVL(p.gid_profile_id, -1) = L_gid_profile_id
         and p.loc                     = il.store
         and p.style                   = il.item_parent
         and p.size_profile_level      = ALC_CONSTANTS_SQL.SIZE_PROFILE_LEVEL_STYLE;

    end if;

    if L_stylecolor_ind = 'Y' then

      insert into numeric_id_gtt(numeric_id)
      select il.item_loc_session_id
        from alc_size_profile p,
             (select item.item_loc_session_id,
                     item.location_id store,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item_parent,
                     parent.diff1_id source_diff_1,
                     parent.diff2_id source_diff_2,
                     parent.diff3_id source_diff_3,
                     im.diff_1 tran_diff_1,
                     im.diff_2 tran_diff_2,
                     im.diff_3 tran_diff_3
                from alc_session_itemloc_gtt parent,
                     alc_session_itemloc_gtt item,
                     item_master im
               where parent.facet_session_id           = I_facet_session_id
                 and parent.facet_session_id           = item.facet_session_id
                 and parent.parent_item_loc_session_id is NULL
                 and parent.item_loc_session_id        = item.parent_item_loc_session_id
                 and item.item_id                      = im.item
                 and im.pack_ind                       = 'N'
              union all
              select item.item_loc_session_id,
                     item.location_id store,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item_parent,
                     parent.diff1_id source_diff_1,
                     parent.diff2_id source_diff_2,
                     parent.diff3_id source_diff_3,
                     im.diff_1 tran_diff_1,
                     im.diff_2 tran_diff_2,
                     im.diff_3 tran_diff_3
                from alc_session_itemloc_gtt parent,
                     alc_session_itemloc_gtt item,
                     packitem_breakout pb,
                     item_master im
               where parent.facet_session_id           = I_facet_session_id
                 and parent.facet_session_id           = item.facet_session_id
                 and parent.parent_item_loc_session_id is NULL
                 and parent.item_loc_session_id        = item.parent_item_loc_session_id
                 and item.item_id                      = pb.pack_no
                 and pb.item                           = im.item
                 and item.item_type                   != ALC_CONSTANTS_SQL.SELLABLEPACK
              union all
              select item.item_loc_session_id,
                     item.location_id store,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item_parent,
                     item.diff1_id source_diff_1,
                     item.diff2_id source_diff_2,
                     item.diff3_id source_diff_3,
                     im.diff_1 tran_diff_1,
                     im.diff_2 tran_diff_2,
                     im.diff_3 tran_diff_3
                from alc_session_itemloc_gtt item,
                     packitem_breakout pb,
                     item_master im
               where item.facet_session_id             = I_facet_session_id
                 and item.parent_item_loc_session_id  is NULL
                 and item.item_id                      = pb.pack_no
                 and pb.item                           = im.item
                 and item.item_type                   != ALC_CONSTANTS_SQL.SELLABLEPACK ) il
       where NVL(p.gid_profile_id, -1)         = L_gid_profile_id
         and p.loc                             = il.store
         and NVL(p.size1, 0)                   = NVL(il.tran_diff_1, 0)
         and NVL(p.size2, 0)                   = NVL(il.tran_diff_2, 0)
         and NVL(p.size3, 0)                   = NVL(il.tran_diff_3, 0)
         and p.style                           = il.item_parent
         and p.dept                            is NULL
         and p.class                           is NULL
         and p.subclass                        is NULL;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FIND_SIZE_PROFILE;
------------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_FREEZE_IND(O_error_message          IN OUT VARCHAR2,
                           I_facet_session_id       IN     VARCHAR2,
                           I_freeze_ind             IN     VARCHAR2,
                           I_item_source_dest_tbl   IN     ALC_ITEM_SOURCE_DEST_TBL)
RETURN NUMBER AS

   L_program           VARCHAR2(50) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.UPDATE_FREEZE_IND';

BEGIN

   merge into alc_session_item_loc target
   using (select asil.item_loc_session_id
            from table(cast(I_item_source_dest_tbl as ALC_ITEM_SOURCE_DEST_TBL)) input,
                 alc_session_item_loc asil
           where asil.facet_session_id = I_facet_session_id
             and asil.item_id          = input.item_node
             and asil.wh_id            = input.source_loc
             and asil.location_id      = input.dest_loc
          union all
          select asil.item_loc_session_id
            from table(cast(I_item_source_dest_tbl as ALC_ITEM_SOURCE_DEST_TBL)) input,
                 alc_session_item_loc parent,
                 alc_session_item_loc asil
           where parent.facet_session_id    = I_facet_session_id
             and parent.item_id             = input.item_node
             and parent.wh_id               = input.source_loc
             and parent.location_id         = input.dest_loc
             and parent.item_loc_session_id = asil.parent_item_loc_session_id
         ) use_this
   on (target.item_loc_session_id = use_this.item_loc_session_id)
   when MATCHED then
      update
         set target.freeze_ind = I_freeze_ind;

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return ALC_CONSTANTS_SQL.FAILURE;
END UPDATE_FREEZE_IND;
------------------------------------------------------------------------------------------------------------------
FUNCTION RESET_FILTERED_IND(O_error_message        IN OUT VARCHAR2,
                            I_facet_session_id     IN     VARCHAR2,
                            I_filtered_ind_value   IN     VARCHAR2)
RETURN NUMBER AS

   L_program   VARCHAR2(50) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.RESET_FILTERED_IND';

BEGIN

   update alc_session_item_loc
      set qbe_filter_ind   = I_filtered_ind_value
    where facet_session_id = I_facet_session_id;

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return ALC_CONSTANTS_SQL.FAILURE;

END RESET_FILTERED_IND;
------------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_FILTERED_IND(O_error_message             IN OUT VARCHAR2,
                             I_facet_session_id          IN     VARCHAR2,
                             I_alc_spread_loc_attr_tbl   IN     ALC_SPREAD_LOC_ATTR_TBL)
RETURN NUMBER AS

   L_program           VARCHAR2(50) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.UPDATE_FILTERED_IND';

BEGIN

   if RESET_FILTERED_IND(O_error_message,
                         I_facet_session_id,
                         'Y') = 0 then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   merge into alc_session_item_loc target
   using (select asil.item_loc_session_id
            from table(cast(I_alc_spread_loc_attr_tbl as ALC_SPREAD_LOC_ATTR_TBL)) input,
                 alc_session_item_loc asil
           where asil.facet_session_id = I_facet_session_id
             and asil.item_id          = input.item_id
             and asil.wh_id            = input.wh
             and asil.location_id      = input.loc
          union all
          select asil.item_loc_session_id
            from table(cast(I_alc_spread_loc_attr_tbl as ALC_SPREAD_LOC_ATTR_TBL)) input,
                 alc_session_item_loc parent,
                 alc_session_item_loc asil
           where parent.facet_session_id    = I_facet_session_id
             and parent.item_id             = input.item_id
             and parent.wh_id               = input.wh
             and parent.location_id         = input.loc
             and parent.item_loc_session_id = asil.parent_item_loc_session_id
         ) use_this
   on (target.item_loc_session_id = use_this.item_loc_session_id)
   when MATCHED then
      update
         set qbe_filter_ind = 'N';

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return ALC_CONSTANTS_SQL.FAILURE;
END UPDATE_FILTERED_IND;
------------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_SESSION_RELEASE_DATE(O_error_message          IN OUT VARCHAR2,
                                     I_facet_session_id       IN     VARCHAR2,
                                     I_alc_item_source_attr   IN     ALC_ITEM_SOURCE_ATTR_TBL)
RETURN NUMBER AS

   L_program   VARCHAR2(100) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.UPDATE_SESSION_RELEASE_DATE';

BEGIN

   merge into alc_session_item_loc target
   using (select /*+ CARDINALITY(i 100) */
                 sil.item_loc_session_id,
                 i.release_date
            from table(cast(I_alc_item_source_attr as ALC_ITEM_SOURCE_ATTR_TBL)) i,
                 alc_session_item_loc sil
           where sil.facet_session_id      = I_facet_session_id
             and sil.item_id               = i.item_node
             and sil.wh_id                 = i.wh
             and sil.source_type           = i.source_type
             and NVL(sil.order_no, '-999') = NVL(i.order_no, '-999')
           union all
          select sil.item_loc_session_id,
                 i.release_date
            from table(cast(I_alc_item_source_attr as ALC_ITEM_SOURCE_ATTR_TBL)) i,
                 alc_session_item_loc sil,
                 alc_session_item_loc parent
           where parent.facet_session_id      = I_facet_session_id
             and parent.item_id               = i.item_node
             and parent.wh_id                 = i.wh
             and parent.source_type           = i.source_type
             and NVL(parent.order_no, '-999') = NVL(i.order_no, '-999')
             and parent.item_loc_session_id   = sil.parent_item_loc_session_id) source
      on (target.item_loc_session_id = source.item_loc_session_id)
   when MATCHED then
      update
         set target.release_date = source.release_date;

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return ALC_CONSTANTS_SQL.FAILURE;

END UPDATE_SESSION_RELEASE_DATE;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SESSION_ITEM_LOC_FROM_TEMP(O_error_message      IN OUT VARCHAR2,
                                        I_facet_session_id   IN     VARCHAR2,
                                        I_work_header_id     IN     NUMBER)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POP_SESSION_ITEM_LOC_FROM_TEMP';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete
     from alc_session_item_loc asil
    where asil.facet_session_id = I_facet_session_id
      and EXISTS (select 'x'
                    from alc_work_item_source awis
                   where awis.work_header_id = I_work_header_id
                     and (   awis.item_id    = asil.item_id
                          or awis.item       = asil.item_id)
                     and awis.wh             = asil.wh_id);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_ITEM_LOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into alc_session_item_loc(item_loc_session_id,
                                    facet_session_id,
                                    item_loc_id,
                                    alloc_id,
                                    item_id,
                                    item_desc,
                                    item_type,
                                    wh_id,
                                    release_date,
                                    location_id,
                                    location_desc,
                                    group_id,
                                    group_desc,
                                    group_type,
                                    loc_group_id,
                                    allocated_qty,
                                    calculated_qty,
                                    need_qty,
                                    on_hand_qty,
                                    in_transit,
                                    on_order,
                                    on_alloc,
                                    alloc_out,
                                    som_qty,
                                    backorder_qty,
                                    freeze_ind,
                                    next_1_week_qty,
                                    next_2_week_qty,
                                    next_3_week_qty,
                                    diff1_id,
                                    diff1_desc,
                                    diff2_id,
                                    diff2_desc,
                                    diff3_id,
                                    diff3_desc,
                                    parent_item_id,
                                    created_order_no,
                                    created_supplier_id,
                                    future_unit_retail,
                                    rush_flag,
                                    cost,
                                    in_store_date,
                                    future_on_hand_qty,
                                    order_no,
                                    source_type,
                                    gross_need_qty,
                                    rloh_qty,
                                    filtered_ind,
                                    result_filter_ind,
                                    quantity_limits_filter_ind,
                                    parent_item_loc_session_id,
                                    pack_comp_qty,
                                    created_by,
                                    updated_by,
                                    created_date,
                                    update_date,
                                    object_version_id,
                                    qbe_filter_ind,
                                    loc_type)
                             select item_loc_session_id,
                                    facet_session_id,
                                    item_loc_id,
                                    alloc_id,
                                    item_id,
                                    item_desc,
                                    item_type,
                                    wh_id,
                                    release_date,
                                    location_id,
                                    location_desc,
                                    group_id,
                                    group_desc,
                                    group_type,
                                    loc_group_id,
                                    allocated_qty,
                                    calculated_qty,
                                    need_qty,
                                    on_hand_qty,
                                    in_transit,
                                    on_order,
                                    on_alloc,
                                    alloc_out,
                                    som_qty,
                                    backorder_qty,
                                    freeze_ind,
                                    next_1_week_qty,
                                    next_2_week_qty,
                                    next_3_week_qty,
                                    diff1_id,
                                    diff1_desc,
                                    diff2_id,
                                    diff2_desc,
                                    diff3_id,
                                    diff3_desc,
                                    parent_item_id,
                                    created_order_no,
                                    created_supplier_id,
                                    future_unit_retail,
                                    rush_flag,
                                    cost,
                                    in_store_date,
                                    future_on_hand_qty,
                                    order_no,
                                    source_type,
                                    gross_need_qty,
                                    rloh_qty,
                                    filtered_ind,
                                    result_filter_ind,
                                    quantity_limits_filter_ind,
                                    parent_item_loc_session_id,
                                    pack_comp_qty,
                                    created_by,
                                    updated_by,
                                    created_date,
                                    update_date,
                                    object_version_id,
                                    qbe_filter_ind,
                                    loc_type
                               from alc_session_itemloc_gtt
                              where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SESSION_ITEM_LOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END POP_SESSION_ITEM_LOC_FROM_TEMP;
------------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_SESSION_TEMP_TABLES(O_error_message      IN OUT VARCHAR2,
                                   I_facet_session_id   IN     VARCHAR2,
                                   I_work_header_id     IN     NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.PURGE_SESSION_TEMP_TABLES';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   --- Clean up temp tables and alc tables

   delete
     from alc_session_itemloc_gtt;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_ITEMLOC_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_session_item_loc_excl asile
    where asile.facet_session_id = I_facet_session_id
      and EXISTS (select 'x'
                    from alc_work_item_source awis
                   where awis.work_header_id = I_work_header_id
                     and awis.item_id        = asile.item_id);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_ITEM_LOC_EXCL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_session_quantity_limits asql
    where asql.facet_session_id = I_facet_session_id
      and (    asql.min is NULL
           and asql.max is NULL
           and asql.threshold is NULL
           and asql.trend is NULL
           and asql.wos is NULL
           and asql.min_need is NULL
           and asql.min_pack is NULL
           and asql.max_pack is NULL)
       and EXISTS (select 'x'
                    from alc_work_item_source awis
                   where awis.work_header_id = I_work_header_id
                     and awis.item_id        = asql.item_id);
					 
    delete
    from alc_session_quantity_limits asql
    where asql.facet_session_id = I_facet_session_id
    and not exists (select 'x' from alc_session_item_loc ail 
                    where ail.facet_session_id  = asql.facet_session_id 
                    and ail.item_id = asql.item_id 
                    and ail.location_id = asql.location_id);   					 

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_QUANTITY_LIMITS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END PURGE_SESSION_TEMP_TABLES;
------------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_SESSION_RULE_PRIORITY(O_error_message      IN OUT VARCHAR2,
                                        I_facet_session_id   IN     VARCHAR2,
                                        I_alloc_id           IN     NUMBER)
RETURN NUMBER IS

   L_program                       VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POPULATE_SESSION_RULE_PRIORITY';
   L_start_time                    TIMESTAMP    := SYSTIMESTAMP;
   L_intercompany_transfer_basis   INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE;

   cursor C_GET_TRANSFER_BASIS is
      select intercompany_transfer_basis
        from inv_move_unit_options;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   delete
     from alc_session_wh_priority_gtt gtt
    where gtt.facet_session_id = I_facet_session_id
      and gtt.alloc_id         = I_alloc_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_SESSION_WH_PRIORITY_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_GET_TRANSFER_BASIS;
   fetch C_GET_TRANSFER_BASIS into L_intercompany_transfer_basis;
   close C_GET_TRANSFER_BASIS;

   --no need to populate for rule 1 since item/loc will always be validated within exclusion

   if POP_SESSION_STORE_WH_PRIORITY(O_error_message,
                                    I_facet_session_id,
                                    I_alloc_id) = FALSE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   if POP_SAMEORG_SAMECHANNEL_RULE(O_error_message,
                                   I_facet_session_id,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis) = FALSE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   if POP_SAMEORG_DIFFCHANNEL_RULE(O_error_message,
                                   I_facet_session_id,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis) = FALSE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   if POP_DIFFORG_SAMECHANNEL_RULE(O_error_message,
                                   I_facet_session_id,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis) = FALSE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   if POP_DIFFORG_DIFFCHANNEL_RULE(O_error_message,
                                   I_facet_session_id,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis) = FALSE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return ALC_CONSTANTS_SQL.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return ALC_CONSTANTS_SQL.FAILURE;

END POPULATE_SESSION_RULE_PRIORITY;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SESSION_STORE_WH_PRIORITY(O_error_message      IN OUT VARCHAR2,
                                        I_facet_session_id   IN     VARCHAR2,
                                        I_alloc_id           IN     NUMBER)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POP_SESSION_STORE_WH_PRIORITY';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_session_wh_priority_gtt(facet_session_id,
                                           item_loc_session_id,
                                           parent_item_loc_session_id,
                                           alloc_id,
                                           location_id,
                                           location_desc,
                                           loc_type, wh_id,
                                           item_id,
                                           item_desc,
                                           source_wh,
                                           rule_priority)
                                    select asil.facet_session_id,
                                           asil.item_loc_session_id,
                                           asil.parent_item_loc_session_id,
                                           asil.alloc_id,
                                           asil.location_id,
                                           asil.location_desc,
                                           asil.loc_type,
                                           asil.wh_id,
                                           asil.item_id,
                                           asil.item_desc,
                                           v.default_wh source_wh,
                                           ALC_CONSTANTS_SQL.PRIORITY_DEFAULT_WH rule_priority
                                      from alc_session_itemloc_gtt asil,
                                           v_alloc_supply_chain v
                                     where asil.facet_session_id = I_facet_session_id
                                       and asil.alloc_id         = I_alloc_id
                                       and asil.location_id      = v.location
                                       and NOT EXISTS (select 'x'
                                                         from alc_session_wh_priority_gtt gtt
                                                        where gtt.facet_session_id = I_facet_session_id
                                                          and gtt.alloc_id         = I_alloc_id
                                                          and gtt.location_id      = asil.location_id
                                                          and gtt.location_id      = v.location
                                                          and gtt.source_wh        = v.default_wh);

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SESSION_WH_RULE_PRIORITY - RULE 2 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END POP_SESSION_STORE_WH_PRIORITY;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SAMEORG_SAMECHANNEL_RULE(O_error_message      IN OUT VARCHAR2,
                                      I_facet_session_id   IN     VARCHAR2,
                                      I_alloc_id           IN     NUMBER,
                                      I_transfer_basis     IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POP_SAMEORG_SAMECHANNEL_RULE';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_session_wh_priority_gtt(facet_session_id,
                                           item_loc_session_id,
                                           parent_item_loc_session_id,
                                           alloc_id,
                                           location_id,
                                           location_desc,
                                           loc_type,
                                           wh_id,
                                           item_id,
                                           item_desc,
                                           source_wh,
                                           rule_priority)
                                    select i.facet_session_id,
                                           i.item_loc_session_id,
                                           i.parent_item_loc_session_id,
                                           i.alloc_id,
                                           i.location_id,
                                           i.location_desc,
                                           i.loc_type,
                                           i.wh_id,
                                           i.item_id,
                                           i.item_desc,
                                           i.source_wh,
                                           i.rule_priority
                                      from (with same_org_same_channel_session as
                                               (select inner.facet_session_id,
                                                       inner.item_loc_session_id,
                                                       inner.parent_item_loc_session_id,
                                                       inner.alloc_id,
                                                       inner.location_id,
                                                       inner.location_desc,
                                                       inner.loc_type,
                                                       inner.wh_id,
                                                       inner.item_id,
                                                       inner.item_desc,
                                                       v_sc.location source_wh,
                                                       v_sc.physical_wh,
                                                       v_sc.protected_ind
                                                  from (select asil.facet_session_id,
                                                               asil.item_loc_session_id,
                                                               asil.parent_item_loc_session_id,
                                                               asil.alloc_id,
                                                               asil.location_id,
                                                               asil.location_desc,
                                                               asil.loc_type,
                                                               asil.wh_id,
                                                               asil.item_id,
                                                               asil.item_desc,
                                                               v.org_unit_id,
                                                               v.channel_id,
                                                               v.tsf_entity_id
                                                          from alc_session_itemloc_gtt asil,
                                                               v_alloc_supply_chain v
                                                         where asil.facet_session_id = I_facet_session_id
                                                           and asil.alloc_id         = I_alloc_id
                                                           and asil.location_id      = v.location) inner,
                                                      v_alloc_supply_chain v_sc
                                                where inner.channel_id    = v_sc.channel_id
                                                  and inner.org_unit_id   = v_sc.org_unit_id
                                                  and inner.tsf_entity_id = v_sc.tsf_entity_id
                                                  and v_sc.loc_type       = 'W'
                                                  and I_transfer_basis    = ALC_CONSTANTS_SQL.BASED_ON_TRANSFER
                                                union all
                                                select inner.facet_session_id,
                                                       inner.item_loc_session_id,
                                                       inner.parent_item_loc_session_id,
                                                       inner.alloc_id,
                                                       inner.location_id,
                                                       inner.location_desc,
                                                       inner.loc_type,
                                                       inner.wh_id,
                                                       inner.item_id,
                                                       inner.item_desc,
                                                       v_sc.location source_wh,
                                                       v_sc.physical_wh,
                                                       v_sc.protected_ind
                                                  from (select asil.facet_session_id,
                                                               asil.item_loc_session_id,
                                                               asil.parent_item_loc_session_id,
                                                               asil.alloc_id,
                                                               asil.location_id,
                                                               asil.location_desc,
                                                               asil.loc_type,
                                                               asil.wh_id,
                                                               asil.item_id,
                                                               asil.item_desc,
                                                               v.org_unit_id,
                                                               v.channel_id,
                                                               v.set_of_books_id
                                                          from alc_session_itemloc_gtt asil,
                                                               v_alloc_supply_chain v
                                                         where asil.facet_session_id = I_facet_session_id
                                                           and asil.alloc_id         = I_alloc_id
                                                           and asil.location_id      = v.location) inner,
                                                       v_alloc_supply_chain v_sc
                                                 where inner.channel_id      = v_sc.channel_id
                                                   and inner.org_unit_id     = v_sc.org_unit_id
                                                   and inner.set_of_books_id = v_sc.set_of_books_id
                                                   and v_sc.loc_type         = 'W'
                                                   and I_transfer_basis      = ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS)
                                           ---------------------------------------------------------------
                                           -- RULE 3 - Primary VIRTUAL WH of PHYSICAL WH linked
                                           --          with the SAME ORG_UNIT_ID and CHANNEL_ID with PROTECTED_IND = 'N'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_SOSC_NOTPRTCT_PRIMARY rule_priority
                                             from (select soscs.facet_session_id,
                                                          soscs.item_loc_session_id,
                                                          soscs.parent_item_loc_session_id,
                                                          soscs.alloc_id,
                                                          soscs.location_id,
                                                          soscs.location_desc,
                                                          soscs.loc_type,
                                                          soscs.wh_id,
                                                          soscs.item_id,
                                                          soscs.item_desc,
                                                          soscs.source_wh,
                                                          row_number() OVER (PARTITION BY soscs.facet_session_id,
                                                                                          soscs.alloc_id,
                                                                                          soscs.location_id
                                                                                 ORDER BY soscs.source_wh,
                                                                                          soscs.wh_id) rank
                                                     from same_org_same_channel_session soscs,
                                                          wh w
                                                    where soscs.physical_wh    = w.wh
                                                      and soscs.source_wh      = w.primary_vwh
                                                      and soscs.protected_ind  = 'N'
                                                      and soscs.source_wh      = soscs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 4 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                           --          with the SAME ORG_UNIT_ID and CHANNEL_ID with PROTECTED_IND = 'N'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_SOSC_NOTPRTCT_NOTPRI rule_priority
                                             from (select soscs.facet_session_id,
                                                          soscs.item_loc_session_id,
                                                          soscs.parent_item_loc_session_id,
                                                          soscs.alloc_id,
                                                          soscs.location_id,
                                                          soscs.location_desc,
                                                          soscs.loc_type,
                                                          soscs.wh_id,
                                                          soscs.item_id,
                                                          soscs.item_desc,
                                                          soscs.source_wh,
                                                          row_number() OVER (PARTITION BY soscs.facet_session_id,
                                                                                          soscs.alloc_id,
                                                                                          soscs.location_id
                                                                                 ORDER BY soscs.source_wh,
                                                                                          soscs.wh_id) rank
                                                     from same_org_same_channel_session soscs,
                                                          wh w
                                                    where soscs.physical_wh    = w.wh
                                                      and soscs.source_wh     != w.primary_vwh
                                                      and soscs.protected_ind  = 'N'
                                                      and soscs.source_wh      = soscs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 5 - Primary VIRTUAL WH of PHYSICAL WH linked
                                           --          with the SAME ORG_UNIT_ID and CHANNEL_ID with PROTECTED_IND = 'Y'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_SOSC_PROTECT_PRIMARY rule_priority
                                             from (select soscs.facet_session_id,
                                                          soscs.item_loc_session_id,
                                                          soscs.parent_item_loc_session_id,
                                                          soscs.alloc_id,
                                                          soscs.location_id,
                                                          soscs.location_desc,
                                                          soscs.loc_type,
                                                          soscs.wh_id,
                                                          soscs.item_id,
                                                          soscs.item_desc,
                                                          soscs.source_wh,
                                                          row_number() OVER (PARTITION BY soscs.facet_session_id,
                                                                                          soscs.alloc_id,
                                                                                          soscs.location_id
                                                                                 ORDER BY soscs.source_wh,
                                                                                          soscs.wh_id) rank
                                                     from same_org_same_channel_session soscs,
                                                          wh w
                                                    where soscs.physical_wh   = w.wh
                                                      and soscs.source_wh     = w.primary_vwh
                                                      and soscs.protected_ind = 'Y'
                                                      and soscs.source_wh     = soscs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 6 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                           --          with the SAME ORG_UNIT_ID and CHANNEL_ID with PROTECTED_IND = 'Y'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_SOSC_PROTECT_NOTPRI rule_priority
                                             from (select soscs.facet_session_id,
                                                          soscs.item_loc_session_id,
                                                          soscs.parent_item_loc_session_id,
                                                          soscs.alloc_id,
                                                          soscs.location_id,
                                                          soscs.location_desc,
                                                          soscs.loc_type,
                                                          soscs.wh_id,
                                                          soscs.item_id,
                                                          soscs.item_desc,
                                                          soscs.source_wh,
                                                          row_number() OVER (PARTITION BY soscs.facet_session_id,
                                                                                          soscs.alloc_id,
                                                                                          soscs.location_id
                                                                                 ORDER BY soscs.source_wh,
                                                                                          soscs.wh_id) rank
                                                     from same_org_same_channel_session soscs,
                                                          wh w
                                                    where soscs.physical_wh    = w.wh
                                                      and soscs.source_wh     != w.primary_vwh
                                                      and soscs.protected_ind  = 'Y'
                                                      and soscs.source_wh      = soscs.wh_id) so_inner
                                            where so_inner.rank = 1) i
                                     where NOT EXISTS (select 'x'
                                                         from alc_session_wh_priority_gtt gtt
                                                        where gtt.facet_session_id = I_facet_session_id
                                                          and gtt.alloc_id         = I_alloc_id
                                                          and gtt.location_id      = i.location_id
                                                          and gtt.source_wh        = i.source_wh);


   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SESSION_WH_RULE_PRIORITY - RULE 3 to 6 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END POP_SAMEORG_SAMECHANNEL_RULE;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_SAMEORG_DIFFCHANNEL_RULE(O_error_message      IN OUT VARCHAR2,
                                      I_facet_session_id   IN     VARCHAR2,
                                      I_alloc_id           IN     NUMBER,
                                      I_transfer_basis     IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POP_SAMEORG_DIFFCHANNEL_RULE';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_session_wh_priority_gtt(facet_session_id,
                                           item_loc_session_id,
                                           parent_item_loc_session_id,
                                           alloc_id,
                                           location_id,
                                           location_desc,
                                           loc_type,
                                           wh_id,
                                           item_id,
                                           item_desc,
                                           source_wh,
                                           rule_priority)
                                    select i.facet_session_id,
                                           i.item_loc_session_id,
                                           i.parent_item_loc_session_id,
                                           i.alloc_id,
                                           i.location_id,
                                           i.location_desc,
                                           i.loc_type,
                                           i.wh_id,
                                           i.item_id,
                                           i.item_desc,
                                           i.source_wh,
                                           i.rule_priority
                                      from (with same_org_diff_channel_session as
                                               (select inner.facet_session_id,
                                                       inner.item_loc_session_id,
                                                       inner.parent_item_loc_session_id,
                                                       inner.alloc_id,
                                                       inner.location_id,
                                                       inner.location_desc,
                                                       inner.loc_type,
                                                       inner.wh_id,
                                                       inner.item_id,
                                                       inner.item_desc,
                                                       v_sc.location source_wh,
                                                       v_sc.physical_wh,
                                                       v_sc.protected_ind
                                                  from (select asil.facet_session_id,
                                                               asil.item_loc_session_id,
                                                               asil.parent_item_loc_session_id,
                                                               asil.alloc_id,
                                                               asil.location_id,
                                                               asil.location_desc,
                                                               asil.loc_type,
                                                               asil.wh_id,
                                                               asil.item_id,
                                                               asil.item_desc,
                                                               v.org_unit_id,
                                                               v.channel_id,
                                                               v.tsf_entity_id
                                                          from alc_session_itemloc_gtt asil,
                                                               v_alloc_supply_chain v
                                                         where asil.facet_session_id = I_facet_session_id
                                                           and asil.alloc_id         = I_alloc_id
                                                           and asil.location_id      = v.location) inner,
                                                      v_alloc_supply_chain v_sc
                                                where inner.channel_id    != v_sc.channel_id
                                                  and inner.org_unit_id    = v_sc.org_unit_id
                                                  and inner.tsf_entity_id  = v_sc.tsf_entity_id
                                                  and v_sc.loc_type        = 'W'
                                                  and I_transfer_basis     = ALC_CONSTANTS_SQL.BASED_ON_TRANSFER
                                                union all
                                                select inner.facet_session_id,
                                                       inner.item_loc_session_id,
                                                       inner.parent_item_loc_session_id,
                                                       inner.alloc_id,
                                                       inner.location_id,
                                                       inner.location_desc,
                                                       inner.loc_type,
                                                       inner.wh_id,
                                                       inner.item_id,
                                                       inner.item_desc,
                                                       v_sc.location source_wh,
                                                       v_sc.physical_wh,
                                                       v_sc.protected_ind
                                                  from (select asil.facet_session_id,
                                                               asil.item_loc_session_id,
                                                               asil.parent_item_loc_session_id,
                                                               asil.alloc_id,
                                                               asil.location_id,
                                                               asil.location_desc,
                                                               asil.loc_type,
                                                               asil.wh_id,
                                                               asil.item_id,
                                                               asil.item_desc,
                                                               v.org_unit_id,
                                                               v.channel_id,
                                                               v.set_of_books_id
                                                          from alc_session_itemloc_gtt asil,
                                                               v_alloc_supply_chain v
                                                         where asil.facet_session_id = I_facet_session_id
                                                           and asil.alloc_id         = I_alloc_id
                                                           and asil.location_id      = v.location) inner,
                                                       v_alloc_supply_chain v_sc
                                                 where inner.channel_id     != v_sc.channel_id
                                                   and inner.org_unit_id     = v_sc.org_unit_id
                                                   and inner.set_of_books_id = v_sc.set_of_books_id
                                                   and v_sc.loc_type         = 'W'
                                                   and I_transfer_basis      = ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS)
                                           ---------------------------------------------------------------
                                           -- RULE 7 - Primary VIRTUAL WH of PHYSICAL WH linked
                                           --          with the SAME ORG_UNIT_ID but DIFF CHANNEL_ID with PROTECTED_IND = 'N'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_SODC_NOTPRTCT_PRIMARY rule_priority
                                             from (select sodcs.facet_session_id,
                                                          sodcs.item_loc_session_id,
                                                          sodcs.parent_item_loc_session_id,
                                                          sodcs.alloc_id,
                                                          sodcs.location_id,
                                                          sodcs.location_desc,
                                                          sodcs.loc_type,
                                                          sodcs.wh_id,
                                                          sodcs.item_id,
                                                          sodcs.item_desc,
                                                          sodcs.source_wh,
                                                          row_number() OVER (PARTITION BY sodcs.facet_session_id,
                                                                                          sodcs.alloc_id,
                                                                                          sodcs.location_id
                                                                                 ORDER BY sodcs.source_wh,
                                                                                          sodcs.wh_id) rank
                                                     from same_org_diff_channel_session sodcs,
                                                          wh w
                                                    where sodcs.physical_wh    = w.wh
                                                      and sodcs.source_wh      = w.primary_vwh
                                                      and sodcs.protected_ind  = 'N'
                                                      and sodcs.source_wh      = sodcs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 8 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                           --          with the SAME ORG_UNIT_ID but DIFF CHANNEL_ID with PROTECTED_IND = 'N'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_SODC_NOTPRTCT_NOTPRI rule_priority
                                             from (select sodcs.facet_session_id,
                                                          sodcs.item_loc_session_id,
                                                          sodcs.parent_item_loc_session_id,
                                                          sodcs.alloc_id,
                                                          sodcs.location_id,
                                                          sodcs.location_desc,
                                                          sodcs.loc_type,
                                                          sodcs.wh_id,
                                                          sodcs.item_id,
                                                          sodcs.item_desc,
                                                          sodcs.source_wh,
                                                          row_number() OVER (PARTITION BY sodcs.facet_session_id,
                                                                                          sodcs.alloc_id,
                                                                                          sodcs.location_id
                                                                                 ORDER BY sodcs.source_wh,
                                                                                          sodcs.wh_id) rank
                                                     from same_org_diff_channel_session sodcs,
                                                          wh w
                                                    where sodcs.physical_wh    = w.wh
                                                      and sodcs.source_wh     != w.primary_vwh
                                                      and sodcs.protected_ind  = 'N'
                                                      and sodcs.source_wh      = sodcs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 9 - Primary VIRTUAL WH of PHYSICAL WH linked
                                           --          with the SAME ORG_UNIT_ID but DIFF CHANNEL_ID with PROTECTED_IND = 'Y'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_SODC_PROTECT_PRIMARY rule_priority
                                             from (select sodcs.facet_session_id,
                                                          sodcs.item_loc_session_id,
                                                          sodcs.parent_item_loc_session_id,
                                                          sodcs.alloc_id,
                                                          sodcs.location_id,
                                                          sodcs.location_desc,
                                                          sodcs.loc_type,
                                                          sodcs.wh_id,
                                                          sodcs.item_id,
                                                          sodcs.item_desc,
                                                          sodcs.source_wh,
                                                          row_number() OVER (PARTITION BY sodcs.facet_session_id,
                                                                                          sodcs.alloc_id,
                                                                                          sodcs.location_id
                                                                                 ORDER BY sodcs.source_wh,
                                                                                          sodcs.wh_id) rank
                                                     from same_org_diff_channel_session sodcs,
                                                          wh w
                                                    where sodcs.physical_wh   = w.wh
                                                      and sodcs.source_wh     = w.primary_vwh
                                                      and sodcs.protected_ind = 'Y'
                                                      and sodcs.source_wh     = sodcs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 10 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                           --           with the SAME ORG_UNIT_ID but DIFF CHANNEL_ID with PROTECTED_IND = 'Y'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_SODC_PROTECT_NOTPRI rule_priority
                                             from (select sodcs.facet_session_id,
                                                          sodcs.item_loc_session_id,
                                                          sodcs.parent_item_loc_session_id,
                                                          sodcs.alloc_id,
                                                          sodcs.location_id,
                                                          sodcs.location_desc,
                                                          sodcs.loc_type,
                                                          sodcs.wh_id,
                                                          sodcs.item_id,
                                                          sodcs.item_desc,
                                                          sodcs.source_wh,
                                                          row_number() OVER (PARTITION BY sodcs.facet_session_id,
                                                                                          sodcs.alloc_id,
                                                                                          sodcs.location_id
                                                                                 ORDER BY sodcs.source_wh,
                                                                                          sodcs.wh_id) rank
                                                     from same_org_diff_channel_session sodcs,
                                                          wh w
                                                    where sodcs.physical_wh    = w.wh
                                                      and sodcs.source_wh     != w.primary_vwh
                                                      and sodcs.protected_ind  = 'Y'
                                                      and sodcs.source_wh      = sodcs.wh_id) so_inner
                                            where so_inner.rank = 1) i
                                     where NOT EXISTS (select 'x'
                                                         from alc_session_wh_priority_gtt gtt
                                                        where gtt.facet_session_id = I_facet_session_id
                                                          and gtt.alloc_id         = I_alloc_id
                                                          and gtt.location_id      = i.location_id
                                                          and gtt.source_wh        = i.source_wh);



   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SESSION_WH_RULE_PRIORITY - RULE 7 to 10 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END POP_SAMEORG_DIFFCHANNEL_RULE;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_DIFFORG_SAMECHANNEL_RULE(O_error_message      IN OUT VARCHAR2,
                                      I_facet_session_id   IN     VARCHAR2,
                                      I_alloc_id           IN     NUMBER,
                                      I_transfer_basis     IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POP_DIFFORG_SAMECHANNEL_RULE';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_session_wh_priority_gtt(facet_session_id,
                                           item_loc_session_id,
                                           parent_item_loc_session_id,
                                           alloc_id,
                                           location_id,
                                           location_desc,
                                           loc_type,
                                           wh_id,
                                           item_id,
                                           item_desc,
                                           source_wh,
                                           rule_priority)
                                    select i.facet_session_id,
                                           i.item_loc_session_id,
                                           i.parent_item_loc_session_id,
                                           i.alloc_id,
                                           i.location_id,
                                           i.location_desc,
                                           i.loc_type,
                                           i.wh_id,
                                           i.item_id,
                                           i.item_desc,
                                           i.source_wh,
                                           i.rule_priority
                                      from (with diff_org_same_channel_session as
                                               (select inner.facet_session_id,
                                                       inner.item_loc_session_id,
                                                       inner.parent_item_loc_session_id,
                                                       inner.alloc_id,
                                                       inner.location_id,
                                                       inner.location_desc,
                                                       inner.loc_type,
                                                       inner.wh_id,
                                                       inner.item_id,
                                                       inner.item_desc,
                                                       v_sc.location source_wh,
                                                       v_sc.physical_wh,
                                                       v_sc.protected_ind
                                                  from (select asil.facet_session_id,
                                                               asil.item_loc_session_id,
                                                               asil.parent_item_loc_session_id,
                                                               asil.alloc_id,
                                                               asil.location_id,
                                                               asil.location_desc,
                                                               asil.loc_type,
                                                               asil.wh_id,
                                                               asil.item_id,
                                                               asil.item_desc,
                                                               v.org_unit_id,
                                                               v.channel_id,
                                                               v.tsf_entity_id
                                                          from alc_session_itemloc_gtt asil,
                                                               v_alloc_supply_chain v
                                                         where asil.facet_session_id = I_facet_session_id
                                                           and asil.alloc_id         = I_alloc_id
                                                           and asil.location_id      = v.location) inner,
                                                      v_alloc_supply_chain v_sc
                                                where inner.channel_id     = v_sc.channel_id
                                                  and inner.org_unit_id   != v_sc.org_unit_id
                                                  and inner.tsf_entity_id  = v_sc.tsf_entity_id
                                                  and v_sc.loc_type        = 'W'
                                                  and I_transfer_basis     = ALC_CONSTANTS_SQL.BASED_ON_TRANSFER
                                                union all
                                                select inner.facet_session_id,
                                                       inner.item_loc_session_id,
                                                       inner.parent_item_loc_session_id,
                                                       inner.alloc_id,
                                                       inner.location_id,
                                                       inner.location_desc,
                                                       inner.loc_type,
                                                       inner.wh_id,
                                                       inner.item_id,
                                                       inner.item_desc,
                                                       v_sc.location source_wh,
                                                       v_sc.physical_wh,
                                                       v_sc.protected_ind
                                                  from (select asil.facet_session_id,
                                                               asil.item_loc_session_id,
                                                               asil.parent_item_loc_session_id,
                                                               asil.alloc_id,
                                                               asil.location_id,
                                                               asil.location_desc,
                                                               asil.loc_type,
                                                               asil.wh_id,
                                                               asil.item_id,
                                                               asil.item_desc,
                                                               v.org_unit_id,
                                                               v.channel_id,
                                                               v.set_of_books_id
                                                          from alc_session_itemloc_gtt asil,
                                                               v_alloc_supply_chain v
                                                         where asil.facet_session_id = I_facet_session_id
                                                           and asil.alloc_id         = I_alloc_id
                                                           and asil.location_id      = v.location) inner,
                                                       v_alloc_supply_chain v_sc
                                                 where inner.channel_id       = v_sc.channel_id
                                                   and inner.org_unit_id     != v_sc.org_unit_id
                                                   and inner.set_of_books_id  = v_sc.set_of_books_id
                                                   and v_sc.loc_type          = 'W'
                                                   and I_transfer_basis       = ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS)
                                           ---------------------------------------------------------------
                                           -- RULE 11 - Primary VIRTUAL WH of PHYSICAL WH linked
                                           --           with the DIFF ORG_UNIT_ID but SAME CHANNEL_ID with PROTECTED_IND = 'N'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_SODC_PROTECT_NOTPRI rule_priority
                                             from (select doscs.facet_session_id,
                                                          doscs.item_loc_session_id,
                                                          doscs.parent_item_loc_session_id,
                                                          doscs.alloc_id,
                                                          doscs.location_id,
                                                          doscs.location_desc,
                                                          doscs.loc_type,
                                                          doscs.wh_id,
                                                          doscs.item_id,
                                                          doscs.item_desc,
                                                          doscs.source_wh,
                                                          row_number() OVER (PARTITION BY doscs.facet_session_id,
                                                                                          doscs.alloc_id,
                                                                                          doscs.location_id
                                                                                 ORDER BY doscs.source_wh,
                                                                                          doscs.wh_id) rank
                                                     from diff_org_same_channel_session doscs,
                                                          wh w
                                                    where doscs.physical_wh   = w.wh
                                                      and doscs.source_wh     = w.primary_vwh
                                                      and doscs.protected_ind = 'N'
                                                      and doscs.source_wh     = doscs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 12 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                           --           with the DIFF ORG_UNIT_ID but SAME CHANNEL_ID with PROTECTED_IND = 'N'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_DOSC_NOTPRTCT_NOTPRI rule_priority
                                                  --12 rule_priority
                                             from (select doscs.facet_session_id,
                                                          doscs.item_loc_session_id,
                                                          doscs.parent_item_loc_session_id,
                                                          doscs.alloc_id,
                                                          doscs.location_id,
                                                          doscs.location_desc,
                                                          doscs.loc_type,
                                                          doscs.wh_id,
                                                          doscs.item_id,
                                                          doscs.item_desc,
                                                          doscs.source_wh,
                                                          row_number() OVER (PARTITION BY doscs.facet_session_id,
                                                                                          doscs.alloc_id,
                                                                                          doscs.location_id
                                                                                 ORDER BY doscs.source_wh,
                                                                                          doscs.wh_id) rank
                                                     from diff_org_same_channel_session doscs,
                                                          wh w
                                                    where doscs.physical_wh    = w.wh
                                                      and doscs.source_wh     != w.primary_vwh
                                                      and doscs.protected_ind  = 'N'
                                                      and doscs.source_wh      = doscs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 13 - Primary VIRTUAL WH of PHYSICAL WH linked
                                           --           with the DIFF ORG_UNIT_ID but SAME CHANNEL_ID with PROTECTED_IND = 'Y'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_DOSC_PROTECT_PRIMARY rule_priority
                                             from (select doscs.facet_session_id,
                                                          doscs.item_loc_session_id,
                                                          doscs.parent_item_loc_session_id,
                                                          doscs.alloc_id,
                                                          doscs.location_id,
                                                          doscs.location_desc,
                                                          doscs.loc_type,
                                                          doscs.wh_id,
                                                          doscs.item_id,
                                                          doscs.item_desc,
                                                          doscs.source_wh,
                                                          row_number() OVER (PARTITION BY doscs.facet_session_id,
                                                                                          doscs.alloc_id,
                                                                                          doscs.location_id
                                                                                 ORDER BY doscs.source_wh,
                                                                                          doscs.wh_id) rank
                                                     from diff_org_same_channel_session doscs,
                                                          wh w
                                                    where doscs.physical_wh   = w.wh
                                                      and doscs.source_wh     = w.primary_vwh
                                                      and doscs.protected_ind = 'Y'
                                                      and doscs.source_wh     = doscs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 14 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                           --           with the DIFF ORG_UNIT_ID but SAME CHANNEL_ID with PROTECTED_IND = 'Y'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_DOSC_PROTECT_NOTPRI rule_priority
                                             from (select doscs.facet_session_id,
                                                          doscs.item_loc_session_id,
                                                          doscs.parent_item_loc_session_id,
                                                          doscs.alloc_id,
                                                          doscs.location_id,
                                                          doscs.location_desc,
                                                          doscs.loc_type,
                                                          doscs.wh_id,
                                                          doscs.item_id,
                                                          doscs.item_desc,
                                                          doscs.source_wh,
                                                          row_number() OVER (PARTITION BY doscs.facet_session_id,
                                                                                          doscs.alloc_id,
                                                                                          doscs.location_id
                                                                                 ORDER BY doscs.source_wh,
                                                                                          doscs.wh_id) rank
                                                     from diff_org_same_channel_session doscs,
                                                          wh w
                                                    where doscs.physical_wh    = w.wh
                                                      and doscs.source_wh     != w.primary_vwh
                                                      and doscs.protected_ind  = 'Y'
                                                      and doscs.source_wh      = doscs.wh_id) so_inner
                                            where so_inner.rank = 1) i
                                     where NOT EXISTS (select 'x'
                                                         from alc_session_wh_priority_gtt gtt
                                                        where gtt.facet_session_id = I_facet_session_id
                                                          and gtt.alloc_id         = I_alloc_id
                                                          and gtt.location_id      = i.location_id
                                                          and gtt.source_wh        = i.source_wh);

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SESSION_WH_RULE_PRIORITY - RULE 11 to 14 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END POP_DIFFORG_SAMECHANNEL_RULE;
------------------------------------------------------------------------------------------------------------------
FUNCTION POP_DIFFORG_DIFFCHANNEL_RULE(O_error_message      IN OUT VARCHAR2,
                                      I_facet_session_id   IN     VARCHAR2,
                                      I_alloc_id           IN     NUMBER,
                                      I_transfer_basis     IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ALLOC_SESSION_ITEM_LOC_SQL.POP_DIFFORG_DIFFCHANNEL_RULE';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_session_wh_priority_gtt(facet_session_id,
                                           item_loc_session_id,
                                           parent_item_loc_session_id,
                                           alloc_id,
                                           location_id,
                                           location_desc,
                                           loc_type,
                                           wh_id,
                                           item_id,
                                           item_desc,
                                           source_wh,
                                           rule_priority)
                                    select i.facet_session_id,
                                           i.item_loc_session_id,
                                           i.parent_item_loc_session_id,
                                           i.alloc_id,
                                           i.location_id,
                                           i.location_desc,
                                           i.loc_type,
                                           i.wh_id,
                                           i.item_id,
                                           i.item_desc,
                                           i.source_wh,
                                           i.rule_priority
                                      from (with diff_org_diff_channel_session as
                                               (select inner.facet_session_id,
                                                       inner.item_loc_session_id,
                                                       inner.parent_item_loc_session_id,
                                                       inner.alloc_id,
                                                       inner.location_id,
                                                       inner.location_desc,
                                                       inner.loc_type,
                                                       inner.wh_id,
                                                       inner.item_id,
                                                       inner.item_desc,
                                                       v_sc.location source_wh,
                                                       v_sc.physical_wh,
                                                       v_sc.protected_ind
                                                  from (select asil.facet_session_id,
                                                               asil.item_loc_session_id,
                                                               asil.parent_item_loc_session_id,
                                                               asil.alloc_id,
                                                               asil.location_id,
                                                               asil.location_desc,
                                                               asil.loc_type,
                                                               asil.wh_id,
                                                               asil.item_id,
                                                               asil.item_desc,
                                                               v.org_unit_id,
                                                               v.channel_id,
                                                               v.tsf_entity_id
                                                          from alc_session_itemloc_gtt asil,
                                                               v_alloc_supply_chain v
                                                         where asil.facet_session_id = I_facet_session_id
                                                           and asil.alloc_id         = I_alloc_id
                                                           and asil.location_id      = v.location) inner,
                                                      v_alloc_supply_chain v_sc
                                                where inner.channel_id    = v_sc.channel_id
                                                  and inner.org_unit_id  != v_sc.org_unit_id
                                                  and inner.tsf_entity_id = v_sc.tsf_entity_id
                                                  and v_sc.loc_type       = 'W'
                                                  and I_transfer_basis    = ALC_CONSTANTS_SQL.BASED_ON_TRANSFER
                                                union all
                                                select inner.facet_session_id,
                                                       inner.item_loc_session_id,
                                                       inner.parent_item_loc_session_id,
                                                       inner.alloc_id,
                                                       inner.location_id,
                                                       inner.location_desc,
                                                       inner.loc_type,
                                                       inner.wh_id,
                                                       inner.item_id,
                                                       inner.item_desc,
                                                       v_sc.location source_wh,
                                                       v_sc.physical_wh,
                                                       v_sc.protected_ind
                                                  from (select asil.facet_session_id,
                                                               asil.item_loc_session_id,
                                                               asil.parent_item_loc_session_id,
                                                               asil.alloc_id,
                                                               asil.location_id,
                                                               asil.location_desc,
                                                               asil.loc_type,
                                                               asil.wh_id,
                                                               asil.item_id,
                                                               asil.item_desc,
                                                               v.org_unit_id,
                                                               v.channel_id,
                                                               v.set_of_books_id
                                                          from alc_session_itemloc_gtt asil,
                                                               v_alloc_supply_chain v
                                                         where asil.facet_session_id = I_facet_session_id
                                                           and asil.alloc_id         = I_alloc_id
                                                           and asil.location_id      = v.location) inner,
                                                       v_alloc_supply_chain v_sc
                                                 where inner.channel_id      = v_sc.channel_id
                                                   and inner.org_unit_id    != v_sc.org_unit_id
                                                   and inner.set_of_books_id = v_sc.set_of_books_id
                                                   and v_sc.loc_type         = 'W'
                                                   and I_transfer_basis      = ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS)
                                           ---------------------------------------------------------------
                                           -- RULE 15 - Primary VIRTUAL WH of PHYSICAL WH linked
                                           --           with the DIFF ORG_UNIT_ID and DIFF CHANNEL_ID with PROTECTED_IND = 'N'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_DODC_NOTPRTCT_PRIMARY rule_priority
                                             from (select dodcs.facet_session_id,
                                                          dodcs.item_loc_session_id,
                                                          dodcs.parent_item_loc_session_id,
                                                          dodcs.alloc_id,
                                                          dodcs.location_id,
                                                          dodcs.location_desc,
                                                          dodcs.loc_type,
                                                          dodcs.wh_id,
                                                          dodcs.item_id,
                                                          dodcs.item_desc,
                                                          dodcs.source_wh,
                                                          row_number() OVER (PARTITION BY dodcs.facet_session_id,
                                                                                          dodcs.alloc_id,
                                                                                          dodcs.location_id
                                                                                 ORDER BY dodcs.source_wh,
                                                                                          dodcs.wh_id) rank
                                                     from diff_org_diff_channel_session dodcs,
                                                          wh w
                                                    where dodcs.physical_wh    = w.wh
                                                      and dodcs.source_wh      = w.primary_vwh
                                                      and dodcs.protected_ind  = 'N'
                                                      and dodcs.source_wh      = dodcs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 16 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                           --           with the DIFF ORG_UNIT_ID and DIFF CHANNEL_ID with PROTECTED_IND = 'N'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_DODC_NOTPRTCT_NOTPRI rule_priority
                                             from (select dodcs.facet_session_id,
                                                          dodcs.item_loc_session_id,
                                                          dodcs.parent_item_loc_session_id,
                                                          dodcs.alloc_id,
                                                          dodcs.location_id,
                                                          dodcs.location_desc,
                                                          dodcs.loc_type,
                                                          dodcs.wh_id,
                                                          dodcs.item_id,
                                                          dodcs.item_desc,
                                                          dodcs.source_wh,
                                                          row_number() OVER (PARTITION BY dodcs.facet_session_id,
                                                                                          dodcs.alloc_id,
                                                                                          dodcs.location_id
                                                                                 ORDER BY dodcs.source_wh,
                                                                                          dodcs.wh_id) rank
                                                     from diff_org_diff_channel_session dodcs,
                                                          wh w
                                                    where dodcs.physical_wh    = w.wh
                                                      and dodcs.source_wh     != w.primary_vwh
                                                      and dodcs.protected_ind  = 'N'
                                                      and dodcs.source_wh      = dodcs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 17 - Primary VIRTUAL WH of PHYSICAL WH linked
                                           --           with the DIFF ORG_UNIT_ID and DIFF CHANNEL_ID with PROTECTED_IND = 'Y'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_DODC_PROTECT_PRIMARY rule_priority
                                             from (select dodcs.facet_session_id,
                                                          dodcs.item_loc_session_id,
                                                          dodcs.parent_item_loc_session_id,
                                                          dodcs.alloc_id,
                                                          dodcs.location_id,
                                                          dodcs.location_desc,
                                                          dodcs.loc_type,
                                                          dodcs.wh_id,
                                                          dodcs.item_id,
                                                          dodcs.item_desc,
                                                          dodcs.source_wh,
                                                          row_number() OVER (PARTITION BY dodcs.facet_session_id,
                                                                                          dodcs.alloc_id,
                                                                                          dodcs.location_id
                                                                                 ORDER BY dodcs.source_wh,
                                                                                          dodcs.wh_id) rank
                                                     from diff_org_diff_channel_session dodcs,
                                                          wh w
                                                    where dodcs.physical_wh   = w.wh
                                                      and dodcs.source_wh     = w.primary_vwh
                                                      and dodcs.protected_ind = 'Y'
                                                      and dodcs.source_wh     = dodcs.wh_id) so_inner
                                            where so_inner.rank = 1
                                            union all
                                           ---------------------------------------------------------------
                                           -- RULE 18 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                           --           with the DIFF ORG_UNIT_ID and DIFF CHANNEL_ID with PROTECTED_IND = 'Y'
                                           ---------------------------------------------------------------
                                           select so_inner.facet_session_id,
                                                  so_inner.item_loc_session_id,
                                                  so_inner.parent_item_loc_session_id,
                                                  so_inner.alloc_id,
                                                  so_inner.location_id,
                                                  so_inner.location_desc,
                                                  so_inner.loc_type,
                                                  so_inner.wh_id,
                                                  so_inner.item_id,
                                                  so_inner.item_desc,
                                                  so_inner.source_wh,
                                                  ALC_CONSTANTS_SQL.PRIORITY_DODC_PROTECT_NOTPRI rule_priority
                                             from (select dodcs.facet_session_id,
                                                          dodcs.item_loc_session_id,
                                                          dodcs.parent_item_loc_session_id,
                                                          dodcs.alloc_id,
                                                          dodcs.location_id,
                                                          dodcs.location_desc,
                                                          dodcs.loc_type,
                                                          dodcs.wh_id,
                                                          dodcs.item_id,
                                                          dodcs.item_desc,
                                                          dodcs.source_wh,
                                                          row_number() OVER (PARTITION BY dodcs.facet_session_id,
                                                                                          dodcs.alloc_id,
                                                                                          dodcs.location_id
                                                                                 ORDER BY dodcs.source_wh,
                                                                                          dodcs.wh_id) rank
                                                     from diff_org_diff_channel_session dodcs,
                                                          wh w
                                                    where dodcs.physical_wh    = w.wh
                                                      and dodcs.source_wh     != w.primary_vwh
                                                      and dodcs.protected_ind  = 'Y'
                                                      and dodcs.source_wh      = dodcs.wh_id) so_inner
                                            where so_inner.rank = 1) i
                                     where NOT EXISTS (select 'x'
                                                         from alc_session_wh_priority_gtt gtt
                                                        where gtt.facet_session_id = I_facet_session_id
                                                          and gtt.alloc_id         = I_alloc_id
                                                          and gtt.location_id      = i.location_id
                                                          and gtt.source_wh        = i.source_wh);

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_SESSION_WH_RULE_PRIORITY - RULE 15 to 18 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END POP_DIFFORG_DIFFCHANNEL_RULE;
------------------------------------------------------------------------------------------------------------------
END ALC_ALLOC_SESSION_ITEM_LOC_SQL;
/
