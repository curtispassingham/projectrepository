CREATE OR REPLACE PACKAGE ALC_SIZE_PROFILE_SQL IS
-----------------------------------------------------------------------------------------------------------------------
-- Name    : GET_STORES
-- Purpose : This function gets the list of stores based on the location search criteria
--           i.e., Store Grade Group, Store Grade, Location List, Location Trait, Allocation Group
--                 Single Store, All Stores
--           This function is a common function uses in all the computations of size profile whereever stores are needed
--           Returns a table of numbers (stores)
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_LOCATIONS(I_store_grade_group   IN NUMBER,
                       I_store_grade         IN NUMBER,
                       I_location_list       IN NUMBER,
                       I_location_trait      IN NUMBER,
                       I_allocation_group    IN NUMBER,
                       I_all_stores          IN NUMBER,
                       I_single_store        IN NUMBER,
                       I_all_whs             IN NUMBER,
                       I_wh                  IN NUMBER)
RETURN OBJ_NUMERIC_ID_TABLE;
-----------------------------------------------------------------------------------------------------------------------
-- Name    : GET_GID_PROFILES
-- Purpose : This function gets the list of GID Profiles (GID Headers)
--           Returns a cursor for the resultset
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_GID_PROFILES(O_error_message        IN OUT VARCHAR2,
                          I_session_id           IN     VARCHAR2,
                          I_result_type          IN     NUMBER,
                          I_id                   IN     NUMBER,
                          I_size_profile_level   IN     NUMBER,
                          I_dept                 IN     VARCHAR2,
                          I_class                IN     VARCHAR2,
                          I_subclass             IN     VARCHAR2,
                          I_style                IN     VARCHAR2,
                          I_store_grade_group    IN     NUMBER,
                          I_store_grade          IN     NUMBER,
                          I_location_list        IN     NUMBER,
                          I_location_trait       IN     NUMBER,
                          I_allocation_group     IN     NUMBER,
                          I_all_stores           IN     NUMBER,
                          I_single_store         IN     NUMBER,
                          I_all_whs              IN     NUMBER,
                          I_wh                   IN     NUMBER,
                          I_diff_group_id1       IN     VARCHAR2,
                          I_diff_group_id2       IN     VARCHAR2,
                          I_diff_group_id3       IN     VARCHAR2,
                          I_diff_group_id4       IN     VARCHAR2)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------------------
-- Name    : GET_SIZE_PROFILE_QUERY
-- Purpose : This function gets the size profile query based on the search criteria provided by the user
--           This query is used to fetch the size profile results.
--           Returns a string which is the dynamically constructed query
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_SIZE_PROFILE_QUERY(O_error_message         IN OUT VARCHAR2,
                                O_size_profile_query       OUT VARCHAR2,
                                I_gid_profile_list_id   IN     NUMBER,
                                I_size_profile_level    IN     NUMBER,
                                I_dept                  IN     VARCHAR2,
                                I_class                 IN     VARCHAR2,
                                I_subclass              IN     VARCHAR2,
                                I_style                 IN     VARCHAR2,
                                I_store_grade_group     IN     NUMBER,
                                I_store_grade           IN     NUMBER,
                                I_location_list         IN     NUMBER,
                                I_location_trait        IN     NUMBER,
                                I_allocation_group      IN     NUMBER,
                                I_all_stores            IN     NUMBER,
                                I_single_store          IN     NUMBER,
                                I_all_whs               IN     NUMBER,
                                I_wh                    IN     NUMBER,
                                I_diff_group_id1        IN     VARCHAR2,
                                I_diff_group_id2        IN     VARCHAR2,
                                I_diff_group_id3        IN     VARCHAR2,
                                I_diff_group_id4        IN     VARCHAR2)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------------------
-- Name    : GET_SIZE_PROFILES
-- Purpose : This function gets the size profiles based on the search criteria provided by the user
--           Returns a cursor for the resultset
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_SIZE_PROFILES(O_error_message         IN OUT VARCHAR2,
                           I_session_id            IN     VARCHAR2,
                           I_result_type           IN     NUMBER,
                           I_seq_id                IN     NUMBER,
                           I_gid_profile_list_id   IN     NUMBER,
                           I_size_profile_level    IN     NUMBER,
                           I_dept                  IN     VARCHAR2,
                           I_class                 IN     VARCHAR2,
                           I_subclass              IN     VARCHAR2,
                           I_style                 IN     VARCHAR2,
                           I_store_grade_group     IN     NUMBER,
                           I_store_grade           IN     NUMBER,
                           I_location_list         IN     NUMBER,
                           I_location_trait        IN     NUMBER,
                           I_allocation_group      IN     NUMBER,
                           I_all_stores            IN     NUMBER,
                           I_single_store          IN     NUMBER,
                           I_all_whs               IN     NUMBER,
                           I_wh                    IN     NUMBER,
                           I_diff_group_id1        IN     VARCHAR2,
                           I_diff_group_id2        IN     VARCHAR2,
                           I_diff_group_id3        IN     VARCHAR2,
                           I_diff_group_id4        IN     VARCHAR2)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------------------
-- Name    : DELETE_SIZE_PROFILES
-- Purpose : This function deletes the size profiles based on the search criteria provided by the user
--           Returns number, which holds 0 or 1 for false and true.
-----------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SIZE_PROFILES(O_error_message                  IN OUT VARCHAR2,
                              I_session_id                     IN     VARCHAR2,
                              I_seq_id                         IN     NUMBER,
                              I_list_of_gid_profile_list_ids   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------------------
-- Name    : GET_ROWCOUNT_FOR_SIZEPROFILES
-- Purpose : This function returns the row count of size profiles based on the search criteria
--           This is used in the function GET_GID_PROFILES, in a case where the size profiles,
--           should be listed for non-existent rows in the table ALC_SIZE_PROFILE and upon getting a rows
--           for size profiles, need to show as NON GID row in the UI (for new record insertions) - Only for NON GID
--           Returns number, which is the row count of size profiles
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ROWCOUNT_FOR_SIZEPROFILES(O_error_message        IN OUT VARCHAR2,
                                       I_session_id           IN     VARCHAR2,
                                       I_result_type          IN     NUMBER,
                                       I_seq_id               IN     NUMBER,
                                       I_size_profile_level   IN     NUMBER,
                                       I_dept                 IN     VARCHAR2,
                                       I_class                IN     VARCHAR2,
                                       I_subclass             IN     VARCHAR2,
                                       I_style                IN     VARCHAR2,
                                       I_store_grade_group    IN     NUMBER,
                                       I_store_grade          IN     NUMBER,
                                       I_location_list        IN     NUMBER,
                                       I_location_trait       IN     NUMBER,
                                       I_allocation_group     IN     NUMBER,
                                       I_all_stores           IN     NUMBER,
                                       I_single_store         IN     NUMBER,
                                       I_all_whs              IN     NUMBER,
                                       I_wh                   IN     NUMBER,
                                       I_diff_group_id1       IN     VARCHAR2,
                                       I_diff_group_id2       IN     VARCHAR2,
                                       I_diff_group_id3       IN     VARCHAR2,
                                       I_diff_group_id4       IN     VARCHAR2)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION COPY_ENTIRE_PARENT(O_error_message     IN OUT VARCHAR2,
                            I_session_id        IN     VARCHAR2,
                            I_to_seq_id         IN     NUMBER,
                            I_from_seq_id       IN     NUMBER,
                            I_multiple_stores   IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION COPY_SINGLE_DIFF(O_error_message          IN OUT VARCHAR2,
                          I_session_id             IN     VARCHAR2,
                          I_to_seq_id              IN     NUMBER,
                          I_from_seq_id            IN     NUMBER,
                          I_from_aggregated_diff   IN     VARCHAR2,
                          I_to_aggregated_diff            OBJ_VARCHAR_ID_TABLE,
                          I_multiple_stores        IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION FLUSH_SESSION_TABLES(O_error_message   IN OUT VARCHAR2,
                              I_session_id      IN     VARCHAR2)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------------------------------
END ALC_SIZE_PROFILE_SQL;
/