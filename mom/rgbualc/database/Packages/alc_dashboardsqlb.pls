CREATE OR REPLACE PACKAGE BODY ALC_DASHBOARD_SQL AS
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SESSION_ID(O_error_message   IN OUT VARCHAR2,
                             O_session_id         OUT OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_DASHBOARD_SQL.GET_NEXT_SESSION_ID';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   if OI_UTILITY.GET_NEXT_SESSION_ID(O_error_message,
                                     O_session_id) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END GET_NEXT_SESSION_ID;
-------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_PO_ARRIVAL(O_error_message   IN OUT VARCHAR2,
                                   I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_DASHBOARD_SQL.DELETE_SESSION_PO_ARRIVAL';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start: '||L_program||' session_id: '||I_session_id);

   delete
     from alc_oi_po_arrival_details
    where session_id = I_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_OI_PO_ARRIVAL_DETAILS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_PO_ARRIVAL;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_PO_ARRIVAL(O_error_message   IN OUT VARCHAR2,
                        O_result          IN OUT ALC_OI_PO_ARRIVAL_RESULT_REC,
                        I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                        I_week_no         IN     NUMBER,
                        I_depts           IN     OBJ_NUMERIC_ID_TABLE,
                        I_classes         IN     OBJ_NUMERIC_ID_TABLE,
                        I_subclasses      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program          VARCHAR2(61) := 'ALC_DASHBOARD_SQL.GET_PO_ARRIVAL';
   L_start_time       TIMESTAMP    := SYSTIMESTAMP;
   L_eow_start_date   DATE;
   L_eow_end_date     DATE;
   L_vdate            DATE;
   L_po_end           DATE;
   L_row_count        NUMBER(10);

   cursor C_GET_START_END_DATES is
      select p.vdate,
             p.vdate + 25,
             DECODE(I_week_no,
                    0, p.vdate,
                    1, dlc.eow_date + 1,
                    2, dlc.eow_date + 8,
                    3, dlc.eow_date + 15) eow_start_date,
             DECODE(I_week_no,
                    0, dlc.eow_date,
                    1, dlc.eow_date + 7,
                    2, dlc.eow_date + 14,
                    3, dlc.eow_date + 21) eow_end_date
        from period p,
             day_level_calendar dlc
       where p.vdate = dlc.day;

   cursor C_GET_TOTAL_ROWS is
      select COUNT(inner.row_count)
        from (select DISTINCT oh.order_no,
                     SUM(ol.qty_ordered - (NVL(ol.qty_received, 0) + NVL(ol.qty_cancelled, 0))) wh_order_qty,
                     COUNT(*) OVER (PARTITION BY oh.order_no,
                                                 oh.supplier,
                                                 oh.not_before_date,
                                                 oh.not_after_date) row_count
                from ordhead oh,
                     gtt_alc_items gtt,
                     day_level_calendar dlc,
                     ordloc ol,
                     v_sups ss,
                     (select sec.loc
                        from v_loc_comm_attrib_sec sec,
                             wh w
                       where sec.loc_type       = 'W'
                         and sec.loc            = w.wh
                         and w.stockholding_ind = 'Y'
                         and w.finisher_ind     = 'N'
                         and w.redist_wh_ind    = 'N') sl
               where oh.status          = 'A'
                 and oh.order_type     != 'ARB'
                 and oh.not_after_date  = dlc.day
                 and dlc.eow_date       = L_eow_end_date
                 and (   oh.not_after_date >= NVL(L_eow_start_date, oh.not_after_date)
                      or oh.not_after_date <= NVL(L_eow_end_date, oh.not_after_date))
                 and oh.supplier        = ss.supplier
                 and oh.order_no        = ol.order_no
                 and ol.location        = sl.loc
                 and ol.item            = gtt.item
               group by oh.order_no,
                        oh.supplier,
                        ss.sup_name,
                        oh.not_before_date,
                        oh.not_after_date) inner
       where inner.wh_order_qty > 0;

   cursor C_PO_ARRIVAL is
      select ALC_OI_PO_ARRIVAL_RESULT_REC(main_po.start_date,
                                          main_po.end_date,
                                          SUM(NVL(main_po.po_fully_allocated, 0)),        --fully_allocated
                                          SUM(NVL(main_po.po_partially_allocated, 0)),    --po_partially_allocated
                                          SUM(NVL(main_po.po_unallocated, 0)),            --po_unallocated
                                          (SUM(NVL(main_po.po_fully_allocated, 0))     +
                                           SUM(NVL(main_po.po_partially_allocated, 0)) +
                                           SUM(NVL(main_po.po_unallocated, 0)))           --total
                                          )
        from (select case
                        when NVL((ABS(SUM(NVL(i.wh_order_qty,0)) - SUM(NVL(i.allocated_qty,0))) / NULLIF(SUM(NVL(i.wh_order_qty, 0)), 0)), 0) * 100 = 0 then
                           COUNT(case
                                    when i.wh_order_qty is NOT NULL and i.wh_order_qty  > 0 then
                                       i.order_no
                                 end)
                        end as po_fully_allocated,
                     --
                     case
                        when     NVL((ABS(SUM(NVL(i.wh_order_qty,0)) - SUM(NVL(i.allocated_qty,0))) / NULLIF(SUM(NVL(i.wh_order_qty,0)), 0)), 0) * 100 > 0
                             and NVL(((SUM(NVL(i.wh_order_qty,0)) - SUM(NVL(i.allocated_qty,0))) / NULLIF(SUM(NVL(i.wh_order_qty, 0)), 0)), 0) * 100 < 100 then
                           COUNT(case
                                    when i.wh_order_qty is NOT NULL and i.wh_order_qty   > 0 then
                                       i.order_no
                                    end)
                        end as po_partially_allocated,
                     --
                     case
                        when NVL((ABS(SUM(NVL(i.wh_order_qty,0)) - SUM(NVL(i.allocated_qty,0))) / NULLIF(SUM(NVL(i.wh_order_qty,0)), 0)), 0) * 100 = 100 then
                           COUNT(case
                                    when i.wh_order_qty is NOT NULL and i.wh_order_qty   > 0 then
                                       i.order_no
                                    end)
                        end as po_unallocated,
                     --
                     case
                        when I_week_no = 0 then
                           L_vdate
                        else
                           L_eow_end_date - 6
                        end as start_date,
                     --
                     L_eow_end_date end_date
                     --
                from (select inner_query.session_id,
                             inner_query.week_no,
                             inner_query.order_no,
                             SUM(inner_query.wh_order_qty) wh_order_qty,
                             SUM(inner_query.allocated_qty) allocated_qty,
                             inner_query.not_after_date
                        from (select apd.session_id,
                                     apd.week_no,
                                     apd.order_no,
                                     apd.wh_order_qty,
                                     apd.allocated_qty,
                                     apd.not_after_date
                               from alc_oi_po_arrival_details apd
                              where apd.session_id = I_session_id
                                and apd.week_no    = I_week_no) inner_query
                       group by inner_query.session_id,
                                inner_query.week_no,
                                inner_query.order_no,
                                inner_query.not_after_date) i
               group by i.order_no,
                        i.not_after_date) main_po
       group by main_po.start_date,
                main_po.end_date;

BEGIN

   LOGGER.LOG_INFORMATION('Start: '||L_program);

   delete
     from alc_oi_po_arrival_details
    where session_id = I_session_id
      and week_no    = I_week_no;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_OI_PO_ARRIVAL_DETAILS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from gtt_alc_items;

   LOGGER.LOG_INFORMATION(L_program||' Delete GTT_ALC_ITEMS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   insert into gtt_alc_items (item)
                      select i.item
                        from v_item_master i,
                             gtt_6_num_6_str_6_date gtt
                       where i.dept     = gtt.number_1
                         and i.class    = gtt.number_2
                         and i.subclass = gtt.number_3;

   LOGGER.LOG_INFORMATION(L_program||' Insert GTT_ALC_ITEMS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_GET_START_END_DATES;
   fetch C_GET_START_END_DATES into L_vdate,
                                    L_po_end,
                                    L_eow_start_date,
                                    L_eow_end_date;
   close C_GET_START_END_DATES;

   open C_GET_TOTAL_ROWS;
   fetch C_GET_TOTAL_ROWS into L_row_count;
   close C_GET_TOTAL_ROWS;

   if L_row_count > 0 then
      insert into alc_oi_po_arrival_details(session_id,
                                            week_no,
                                            order_no,
                                            supplier_site,
                                            supplier_site_name,
                                            not_before_date,
                                            not_after_date,
                                            wh_order_qty,
                                            promo_ind)
                                     select I_session_id session_id,
                                            I_week_no week_no,
                                            inner.order_no,
                                            inner.supplier_site,
                                            inner.supplier_site_name,
                                            inner.not_before_date,
                                            inner.not_after_date,
                                            inner.wh_order_qty,
                                            inner.promo_ind
                                       from (with sec_loc as
                                                (select sec.loc
                                                   from v_loc_comm_attrib_sec sec,
                                                        wh w
                                                  where sec.loc_type       = 'W'
                                                    and sec.loc            = w.wh
                                                    and w.stockholding_ind = 'Y'
                                                    and w.finisher_ind     = 'N'
                                                    and w.redist_wh_ind    = 'N'),
                                             sec_sup as
                                                (select supplier,
                                                        sup_name
                                                   from v_sups)
                                             --
                                             select DISTINCT oh.order_no,
                                                    oh.supplier supplier_site,
                                                    ss.sup_name supplier_site_name,
                                                    oh.not_before_date,
                                                    oh.not_after_date,
                                                    SUM(ol.qty_ordered - (NVL(ol.qty_received, 0) + NVL(ol.qty_cancelled, 0))) wh_order_qty,
                                                    'N' promo_ind
                                               from ordhead oh,
                                                    sec_sup ss,
                                                    sec_loc sl,
                                                    gtt_alc_items gtt,
                                                    day_level_calendar dlc,
                                                    ordloc ol
                                              where oh.status          = 'A'
                                                and oh.order_type     != 'ARB'
                                                and oh.not_after_date  = dlc.day
                                                and dlc.eow_date       = L_eow_end_date
                                                and (   oh.not_after_date >= NVL(L_eow_start_date, oh.not_after_date)
                                                     or oh.not_after_date <= NVL(L_eow_end_date, oh.not_after_date))
                                                and oh.supplier        = ss.supplier
                                                and oh.order_no        = ol.order_no
                                                and ol.location        = sl.loc
                                                and ol.item            = gtt.item
                                              group by oh.order_no,
                                                       oh.supplier,
                                                       ss.sup_name,
                                                       oh.not_before_date,
                                                       oh.not_after_date) inner
                                      where inner.wh_order_qty > 0;

      LOGGER.LOG_INFORMATION(L_program||' Insert ALC_OI_PO_ARRIVAL_DETAILS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   else
      insert into alc_oi_po_arrival_details(session_id,
                                            week_no,
                                            order_no,
                                            supplier_site,
                                            supplier_site_name,
                                            not_before_date,
                                            not_after_date,
                                            wh_order_qty,
                                            promo_ind)
                                     select I_session_id session_id,
                                            I_week_no week_no,
                                            0 order_no,
                                            NULL supplier_site,
                                            NULL supplier_site_name,
                                            L_eow_end_date - 6 start_date,
                                            L_eow_end_date end_date,
                                            0 wh_order_qty,
                                            NULL promo_ind
                                       from dual;

      LOGGER.LOG_INFORMATION(L_program||' Insert SESSION ID and WEEK - ALC_OI_PO_ARRIVAL_DETAILS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   merge into alc_oi_po_arrival_details target
   using (with sec_loc as
             (select sec.loc
                from v_loc_comm_attrib_sec sec,
                     wh w
               where sec.loc_type       = 'W'
                 and sec.loc            = w.wh
                 and w.stockholding_ind = 'Y'
                 and w.finisher_ind     = 'N'
                 and w.redist_wh_ind    = 'N')
          --
          select po.session_id,
                 po.week_no,
                 po.order_no,
                 MIN(sh.est_arr_date) est_arr_date
            from alc_oi_po_arrival_details po,
                 shipsku ss,
                 shipment sh,
                 gtt_alc_items gai,
                 sec_loc sl
           where po.order_no   = sh.order_no
             and sh.shipment   = ss.shipment
             and ss.item       = gai.item
             and sh.to_loc     = sl.loc
             and po.session_id = I_session_id
             and po.week_no    = I_week_no
           group by po.session_id,
                    po.week_no,
                    po.order_no) use_this
   on (    target.session_id = use_this.session_id
       and target.week_no    = use_this.week_no
       and target.order_no   = use_this.order_no)
   when MATCHED then
      update
         set target.next_ship_date = use_this.est_arr_date;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_OI_PO_ARRIVAL_DETAILS - NEXT_SHIP_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into alc_oi_po_arrival_details target
   using (with sec_loc as
             (select sec.loc
                from v_loc_comm_attrib_sec sec,
                     wh w
               where sec.loc_type       = 'W'
                 and sec.loc            = w.wh
                 and w.stockholding_ind = 'Y'
                 and w.finisher_ind     = 'N'
                 and w.redist_wh_ind    = 'N')
          --
          select po.session_id,
                 po.week_no,
                 po.order_no,
                 MAX(ail.alloc_id) alloc_id,
                 SUM(NVL(ail.allocated_qty, 0)) allocated_qty
            from alc_oi_po_arrival_details po,
                 alc_item_loc ail,
                 alc_alloc a,
                 gtt_alc_items gai,
                 sec_loc sl
           where po.order_no     = ail.order_no
             and ail.source_type = 1
             and ail.alloc_id    = a.alloc_id
             and a.status        IN ('2','6')
             and ail.item_id     = gai.item
             and ail.wh_id       = sl.loc
             and po.session_id   = I_session_id
             and po.week_no      = I_week_no
           group by po.session_id,
                    po.week_no,
                    po.order_no) use_this
   on (    target.session_id  = use_this.session_id
       and target.week_no     = use_this.week_no
       and target.order_no    = use_this.order_no)
   when MATCHED then
      update
         set target.alloc_id      = use_this.alloc_id,
             target.allocated_qty = use_this.allocated_qty,
             target.alloc_percent = use_this.allocated_qty / target.wh_order_qty;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_OI_PO_ARRIVAL_DETAILS - ALLOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into alc_oi_po_arrival_details target
   using (with sec_loc as
             (select sec.loc
                from v_loc_comm_attrib_sec sec,
                     wh w
               where sec.loc_type       = 'W'
                 and sec.loc            = w.wh
                 and w.stockholding_ind = 'Y'
                 and w.finisher_ind     = 'N'
                 and w.redist_wh_ind    = 'N')
          --
          select po.session_id,
                 po.week_no,
                 ol.order_no,
                 'Y' promo_ind
            from alc_oi_po_arrival_details po,
                 ordloc ol,
                 gtt_alc_items gai,
                 rpm_promo_dashboard rpd,
                 sec_loc sl
           where po.order_no   = ol.order_no
             and ol.item       = rpd.item
             and ol.location   = rpd.location
             and (   L_vdate BETWEEN rpd.start_date and rpd.end_date
                  or L_po_end BETWEEN rpd.start_date and rpd.end_date)
             and ol.item       = gai.item
             and ol.location   = sl.loc
             and po.session_id = I_session_id
             and po.week_no    = I_week_no) use_this
   on (    target.session_id = use_this.session_id
       and target.week_no    = use_this.week_no
       and target.order_no   = use_this.order_no)
   when MATCHED then
      update
         set target.promo_ind = use_this.promo_ind;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_OI_PO_ARRIVAL_DETAILS - PROMO_IND - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_PO_ARRIVAL;
   fetch C_PO_ARRIVAL into O_result;
   close C_PO_ARRIVAL;

   -- if there are no PO arrivals for a particular week, remove the dummy record that was initially added to get the Start and End dates
   delete from alc_oi_po_arrival_details
    where session_id = I_session_id
      and week_no    = I_week_no
      and order_no   = 0;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_OI_PO_ARRIVAL_DETAILS - ORDER_NO 0 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END GET_PO_ARRIVAL;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_MERCH_STOCK_TO_SALES(O_error_message   IN OUT VARCHAR2,
                                  O_result          IN OUT ALC_OI_STOCK_TO_SALES_TBL,
                                  I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                  I_depts           IN     OBJ_NUMERIC_ID_TABLE,
                                  I_classes         IN     OBJ_NUMERIC_ID_TABLE,
                                  I_subclasses      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_DASHBOARD_SQL.GET_MERCH_STOCK_TO_SALES';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;
   L_start_date   DATE;
   L_end_date     DATE;

   cursor C_GET_START_END_DATES is
      select p.vdate +(7 - p.curr_454_day) - 49 start_date,
             p.vdate +(7 - p.curr_454_day) - 7 end_date
        from period p;

   cursor C_STOCK_TO_SALES is
      select ALC_OI_STOCK_TO_SALES_REC(i.eow_date,
                                       LAG(i.stock, 1) OVER (ORDER BY i.eow_date),
                                       i.sales,
                                       LAG(i.stock,1) over (ORDER BY i.eow_date) / NULLIF(i.sales, 0))
        from (select DISTINCT oi.eow_date,
                     SUM(NVL(oi.stock, 0)) OVER (PARTITION BY oi.eow_date) stock,
                     SUM(NVL(oi.sales, 0)) OVER (PARTITION BY oi.eow_date) sales
                from alc_oi_stock_to_sales oi,
                     gtt_6_num_6_str_6_date gtt
               where gtt.number_1  = oi.dept
                 and gtt.number_2  = oi.class
                 and gtt.number_3  = oi.subclass
                 and oi.eow_date BETWEEN L_start_date and L_end_date) i
       order by i.eow_date;

BEGIN

   LOGGER.LOG_INFORMATION('Start: '||L_program);

   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   open C_GET_START_END_DATES;
   fetch C_GET_START_END_DATES into L_start_date,
                                    L_end_date;
   close C_GET_START_END_DATES;

   open C_STOCK_TO_SALES;
   fetch C_STOCK_TO_SALES BULK COLLECT into O_result;
   close C_STOCK_TO_SALES;

   LOGGER.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END GET_MERCH_STOCK_TO_SALES;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_STOCK_TO_SALES(O_error_message   IN OUT VARCHAR2,
                                 O_result          IN OUT ALC_OI_STOCK_TO_SALES_TBL,
                                 I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                 I_item            IN     ITEM_MASTER.ITEM%TYPE,
                                 I_diff1_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                                 I_diff2_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                                 I_diff3_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                                 I_diff4_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                                 I_loc             IN     ITEM_LOC.LOC%TYPE,
                                 I_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_DASHBOARD_SQL.GET_ITEM_STOCK_TO_SALES';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;
   L_start_date   DATE;
   L_end_date     DATE;

   cursor C_GET_START_END_DATES is
      select p.vdate +(7 - p.curr_454_day) - 35 start_date,
             p.vdate +(7 - p.curr_454_day) - 7 end_date
        from period p;

   cursor C_ITEM_STOCK_TO_SALES is
      select ALC_OI_STOCK_TO_SALES_REC(i.eow_date,
                                       LAG(i.stock, 1) OVER (ORDER BY i.eow_date),
                                       i.sales,
                                       LAG(i.stock,1) over (ORDER BY i.eow_date) / NULLIF(i.sales, 0))
        from (with item as
                (select vim.item
                   from v_item_master vim,
                        v_item_master vimp
                  where vim.item_parent = I_item
                    and vim.item_parent = vimp.item
                    and (   NVL(vim.diff_1, '-999') = NVL(I_diff1_id, '-999')
                         or I_diff1_id is NULL)
                    and (   NVL(vim.diff_2, '-999') = NVL(I_diff2_id, '-999')
                         or I_diff2_id is NULL)
                    and (   NVL(vim.diff_3, '-999') = NVL(I_diff3_id, '-999')
                         or I_diff3_id is NULL)
                    and (   NVL(vim.diff_4, '-999') = NVL(I_diff4_id, '-999')
                         or I_diff4_id is NULL)
                    and vim.item_level = vim.tran_level
                  union all
                   select vim.item
                   from v_item_master vim
                  where vim.item       = I_item
                    and vim.item_level = vim.tran_level)
              --
              select /*+ INDEX(ilh, ITEM_LOC_HIST_I1)  */
                     DISTINCT ilh.eow_date,
                     SUM(NVL(ilh.stock, 0)) OVER (PARTITION BY ilh.eow_date) stock,
                     SUM(NVL(ilh.sales_issues, 0)) OVER (PARTITION BY ilh.eow_date) sales
                from item i,
                     v_loc_comm_attrib_sec vloc,
                     item_loc_hist ilh
               where ilh.item      = i.item
                 and ilh.loc       = I_loc
                 and ilh.loc_type  = I_loc_type
                 and ilh.loc       = vloc.loc
                 and ilh.loc_type  = vloc.loc_type
                 and ilh.eow_date BETWEEN L_start_date and L_end_date) i
       order by i.eow_date;

BEGIN

   LOGGER.LOG_INFORMATION('Start: '||L_program);

   open C_GET_START_END_DATES;
   fetch C_GET_START_END_DATES into L_start_date,
                                    L_end_date;
   close C_GET_START_END_DATES;

   open C_ITEM_STOCK_TO_SALES;
   fetch C_ITEM_STOCK_TO_SALES BULK COLLECT into O_result;
   close C_ITEM_STOCK_TO_SALES;

   LOGGER.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END GET_ITEM_STOCK_TO_SALES;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_TOP_SELLER(O_error_message   IN OUT VARCHAR2,
                        O_result          IN OUT ALC_OI_TOP_BOTTOM_SALES_REC,
                        I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                        I_depts           IN     OBJ_NUMERIC_ID_TABLE,
                        I_classes         IN     OBJ_NUMERIC_ID_TABLE,
                        I_subclasses      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_DASHBOARD_SQL.GET_TOP_SELLER';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;
   L_vdate        DATE         := GET_VDATE;

   cursor C_SALES is
      select ALC_OI_TOP_BOTTOM_SALES_REC(gtt.item,
                                         gtt.diff_1,
                                         gtt.diff_2,
                                         gtt.diff_3,
                                         gtt.diff_4,
                                         gtt.item_desc,
                                         gtt.image_name,
                                         gtt.image_addr,
                                         gtt.units,
                                         gtt.uom,
                                         gtt.sales,
                                         'USD',
                                         gtt.margin)
        from alc_oi_top_bottom_sales_gtt gtt
       where top_bottom_flag = 'TOP';

BEGIN

   LOGGER.LOG_INFORMATION('Start: '||L_program);

   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   delete
     from alc_oi_top_bottom_sales_gtt;

   LOGGER.LOG_INFORMATION(L_program||' DELETE ALC_OI_TOP_BOTTOM_SALES_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into alc_oi_top_bottom_sales_gtt(item,
                                           item_desc,
                                           dept,
                                           class,
                                           subclass,
                                           alc_item_type,
                                           item_parent,
                                           diff_1,
                                           diff_2,
                                           diff_3,
                                           diff_4,
                                           markup_calc_type,
                                           units,
                                           uom,
                                           retail,
                                           cost,
                                           sales)
                                    select tbs.item,
                                           tbs.item_desc,
                                           tbs.dept,
                                           tbs.class,
                                           tbs.subclass,
                                           tbs.alc_item_type,
                                           tbs.item_parent,
                                           tbs.diff_1,
                                           tbs.diff_2,
                                           tbs.diff_3,
                                           tbs.diff_4,
                                           d.markup_calc_type,
                                           SUM(tbs.units) units,
                                           tbs.uom,
                                           SUM(tbs.total_retail) retail,
                                           SUM(tbs.total_cost) cost,
                                           SUM(NVL(tbs.total_cost,0) * inner_currency.exchange_rate) sales
                                      from gtt_6_num_6_str_6_date gtt,
                                           v_loc_comm_attrib_sec vloc,
                                           alc_oi_top_bottom_seller tbs,
                                           deps d,
                                           (select DISTINCT curr_conv.from_currency,
                                                   curr_conv.exchange_rate,
                                                   curr_conv.to_currency
                                              from (select from_currency,
                                                           to_currency,
                                                           effective_date,
                                                           exchange_rate,
                                                           exchange_type,
                                                           rank() OVER (PARTITION BY from_currency,
                                                                                     to_currency,
                                                                                     exchange_type
                                                                            ORDER BY effective_date desc) date_rank
                                                      from mv_currency_conversion_rates
                                                     where exchange_type  = 'C' --EXCH_TYPE_CONSOLIDATED
                                                       and effective_date <= L_vdate) curr_conv,
                                                   v_loc_comm_attrib_sec v
                                             where curr_conv.to_currency   = 'USD'  --primary currency
                                               and curr_conv.from_currency = v.currency_code
                                               and curr_conv.date_rank = 1) inner_currency
                                     where tbs.dept                     = gtt.number_1
                                       and tbs.class                    = gtt.number_2
                                       and tbs.subclass                 = gtt.number_3
                                       and tbs.location                 = vloc.loc
                                       and tbs.loc_type                 = vloc.loc_type
                                       and vloc.loc_type                = 'S'
                                       and tbs.dept                     = d.dept
                                       and tbs.tran_date                = L_vdate - 1
                                       and inner_currency.from_currency = vloc.currency_code
                                     group by tbs.item,
                                              tbs.item_desc,
                                              tbs.dept,
                                              tbs.class,
                                              tbs.subclass,
                                              tbs.alc_item_type,
                                              tbs.item_parent,
                                              tbs.diff_1,
                                              tbs.diff_2,
                                              tbs.diff_3,
                                              tbs.diff_4,
                                              d.markup_calc_type,
                                              tbs.uom;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_OI_TOP_BOTTOM_SALES_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into alc_oi_top_bottom_sales_gtt target
   using (select i.item,
                 i.alc_item_type,
                 i.diff_1,
                 i.diff_2,
                 i.diff_3,
                 i.diff_4,
                 i.markup_calc_type,
                 NVL(iim.image_name, ui.default_item_image) image_name,
                 NVL(iim.image_addr, ui.image_path) image_addr,
                 case
                    when i.markup_calc_type = 'R' then
                       ROUND((i.retail - i.cost) / NULLIF(i.retail,0) * 100, 2)
                    else
                       ROUND((i.retail - i.cost) / NULLIF(i.cost,0) * 100, 2)
                    end as margin,
                 'TOP' top_bottom_flag
            from item_image iim,
                 ui_config_options ui,
                 (select tbs.item,
                         tbs.alc_item_type,
                         tbs.diff_1,
                         tbs.diff_2,
                         tbs.diff_3,
                         tbs.diff_4,
                         tbs.markup_calc_type,
                         tbs.units,
                         tbs.retail,
                         tbs.cost,
                         tbs.sales,
                         rank() OVER (ORDER BY retail DESC,
                                               item) top_retail_rnk
                    from alc_oi_top_bottom_sales_gtt tbs) i
           where i.top_retail_rnk   = 1
             and i.item             = iim.item(+)
             and iim.primary_ind(+) = 'Y') use_this
   on (    target.item                = use_this.item
       and target.alc_item_type       = use_this.alc_item_type
       and NVL(target.diff_1, '-999') = NVL(use_this.diff_1, '-999')
       and NVL(target.diff_2, '-999') = NVL(use_this.diff_2, '-999')
       and NVL(target.diff_3, '-999') = NVL(use_this.diff_3, '-999')
       and NVL(target.diff_4, '-999') = NVL(use_this.diff_4, '-999')
       and target.markup_calc_type    = use_this.markup_calc_type)
   when MATCHED then
      update
         set target.image_name      = use_this.image_name,
             target.image_addr      = use_this.image_addr,
             target.margin          = use_this.margin,
             target.top_bottom_flag = use_this.top_bottom_flag;

   LOGGER.LOG_INFORMATION(L_program||' Update ALC_OI_TOP_BOTTOM_SALES_GTT - IMAGE, MARGIN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_SALES;
   fetch C_SALES into O_result;
   close C_SALES;

   LOGGER.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END GET_TOP_SELLER;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_BOTTOM_SELLER(O_error_message   IN OUT VARCHAR2,
                           O_result          IN OUT ALC_OI_TOP_BOTTOM_SALES_REC,
                           I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                           I_depts           IN     OBJ_NUMERIC_ID_TABLE,
                           I_classes         IN     OBJ_NUMERIC_ID_TABLE,
                           I_subclasses      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_DASHBOARD_SQL.GET_BOTTOM_SELLER';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;
   L_vdate        DATE         := GET_VDATE;

   cursor C_SALES is
      select ALC_OI_TOP_BOTTOM_SALES_REC(gtt.item,
                                         gtt.diff_1,
                                         gtt.diff_2,
                                         gtt.diff_3,
                                         gtt.diff_4,
                                         gtt.item_desc,
                                         gtt.image_name,
                                         gtt.image_addr,
                                         gtt.units,
                                         gtt.uom,
                                         gtt.sales,
                                         'USD',
                                         gtt.margin)
        from alc_oi_top_bottom_sales_gtt gtt
       where top_bottom_flag = 'BOTTOM';

BEGIN

   LOGGER.LOG_INFORMATION('Start: '||L_program);

   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses) = 0 then
      return OI_UTILITY.FAILURE;
   end if;

   delete
     from alc_oi_top_bottom_sales_gtt;

   LOGGER.LOG_INFORMATION(L_program||' DELETE ALC_OI_TOP_BOTTOM_SALES_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into alc_oi_top_bottom_sales_gtt(item,
                                           item_desc,
                                           dept,
                                           class,
                                           subclass,
                                           alc_item_type,
                                           item_parent,
                                           diff_1,
                                           diff_2,
                                           diff_3,
                                           diff_4,
                                           markup_calc_type,
                                           units,
                                           uom,
                                           retail,
                                           cost,
                                           sales)
                                    select tbs.item,
                                           tbs.item_desc,
                                           tbs.dept,
                                           tbs.class,
                                           tbs.subclass,
                                           tbs.alc_item_type,
                                           tbs.item_parent,
                                           tbs.diff_1,
                                           tbs.diff_2,
                                           tbs.diff_3,
                                           tbs.diff_4,
                                           d.markup_calc_type,
                                           SUM(tbs.units) units,
                                           tbs.uom,
                                           SUM(tbs.total_retail) retail,
                                           SUM(tbs.total_cost) cost,
                                           SUM(NVL(tbs.total_cost,0) * inner_currency.exchange_rate) sales
                                      from gtt_6_num_6_str_6_date gtt,
                                           v_loc_comm_attrib_sec vloc,
                                           alc_oi_top_bottom_seller tbs,
                                           deps d,
                                           (select DISTINCT curr_conv.from_currency,
                                                   curr_conv.exchange_rate,
                                                   curr_conv.to_currency
                                              from (select from_currency,
                                                           to_currency,
                                                           effective_date,
                                                           exchange_rate,
                                                           exchange_type,
                                                           rank() OVER (PARTITION BY from_currency,
                                                                                     to_currency,
                                                                                     exchange_type
                                                                            ORDER BY effective_date desc) date_rank
                                                      from mv_currency_conversion_rates
                                                     where exchange_type  = 'C' --EXCH_TYPE_CONSOLIDATED
                                                       and effective_date <= L_vdate) curr_conv,
                                                   v_loc_comm_attrib_sec v
                                             where curr_conv.to_currency   = 'USD'  --primary currency
                                               and curr_conv.from_currency  = v.currency_code
                                               and curr_conv.date_rank = 1) inner_currency
                                     where tbs.dept                     = gtt.number_1
                                       and tbs.class                    = gtt.number_2
                                       and tbs.subclass                 = gtt.number_3
                                       and tbs.location                 = vloc.loc
                                       and tbs.loc_type                 = vloc.loc_type
                                       and vloc.loc_type                = 'S'
                                       and tbs.dept                     = d.dept
                                       and tbs.tran_date                = L_vdate - 1
                                       and inner_currency.from_currency = vloc.currency_code
                                     group by tbs.item,
                                              tbs.item_desc,
                                              tbs.dept,
                                              tbs.class,
                                              tbs.subclass,
                                              tbs.alc_item_type,
                                              tbs.item_parent,
                                              tbs.diff_1,
                                              tbs.diff_2,
                                              tbs.diff_3,
                                              tbs.diff_4,
                                              d.markup_calc_type,
                                              tbs.uom;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_OI_TOP_BOTTOM_SALES_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into alc_oi_top_bottom_sales_gtt target
   using (select i.item,
                 i.alc_item_type,
                 i.diff_1,
                 i.diff_2,
                 i.diff_3,
                 i.diff_4,
                 i.markup_calc_type,
                 NVL(iim.image_name, ui.default_item_image) image_name,
                 NVL(iim.image_addr, ui.image_path) image_addr,
                 case
                    when i.markup_calc_type = 'R' then
                       ROUND((i.retail - i.cost) / NULLIF(i.retail,0) * 100, 2)
                    else
                       ROUND((i.retail - i.cost) / NULLIF(i.cost,0) * 100, 2)
                    end as margin,
                 'BOTTOM' top_bottom_flag
            from item_image iim,
                 ui_config_options ui,
                 (select tbs.item,
                         tbs.alc_item_type,
                         tbs.diff_1,
                         tbs.diff_2,
                         tbs.diff_3,
                         tbs.diff_4,
                         tbs.markup_calc_type,
                         tbs.units,
                         tbs.uom,
                         tbs.retail,
                         tbs.cost,
                         tbs.sales,
                         rank() OVER (ORDER BY retail ASC,
                                               item) bottom_retail_rnk
                    from alc_oi_top_bottom_sales_gtt tbs) i
           where i.bottom_retail_rnk = 1
             and i.item              = iim.item(+)
             and iim.primary_ind(+)  = 'Y') use_this
   on (    target.item                = use_this.item
       and target.alc_item_type       = use_this.alc_item_type
       and NVL(target.diff_1, '-999') = NVL(use_this.diff_1, '-999')
       and NVL(target.diff_2, '-999') = NVL(use_this.diff_2, '-999')
       and NVL(target.diff_3, '-999') = NVL(use_this.diff_3, '-999')
       and NVL(target.diff_4, '-999') = NVL(use_this.diff_4, '-999')
       and target.markup_calc_type    = use_this.markup_calc_type)
   when MATCHED then
      update
         set target.image_name      = use_this.image_name,
             target.image_addr      = use_this.image_addr,
             target.margin          = use_this.margin,
             target.top_bottom_flag = use_this.top_bottom_flag;

   LOGGER.LOG_INFORMATION(L_program||' Update ALC_OI_TOP_BOTTOM_SALES_GTT - IMAGE, MARGIN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_SALES;
   fetch C_SALES into O_result;
   close C_SALES;

   LOGGER.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END GET_BOTTOM_SELLER;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_PLAN_FORECAST(O_error_message   IN OUT VARCHAR2,
                           O_result          IN OUT ALC_OI_PLAN_FORECAST_TBL,
                           I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                           I_item            IN     ITEM_MASTER.ITEM%TYPE,
                           I_diff1_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                           I_diff2_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                           I_diff3_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                           I_diff4_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                           I_loc             IN     ITEM_LOC.LOC%TYPE,
                           I_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN NUMBER IS

   L_program          VARCHAR2(61) := 'ALC_DASHBOARD_SQL.GET_PLAN_FORECAST';
   L_start_time       TIMESTAMP    := SYSTIMESTAMP;

   L_start_date       DATE;
   L_end_date         DATE;
   L_eow_date         DATE;
   L_day              DATE;
   L_week_1           DATE;
   L_week_2           DATE;
   L_week_3           DATE;
   L_week_4           DATE;

   L_need_calc_type   ALC_SYSTEM_OPTIONS_OI.NEED_CALC_TYPE%TYPE;

   cursor C_GET_CALC_TYPE is
      select need_calc_type
        from alc_system_options_oi;

   cursor C_GET_START_END_DATES is
      select dlc.eow_date - 6 start_date,
             dlc.eow_date + 28 end_date,
             dlc.day,
             dlc.eow_date,
             dlc.eow_date + 7  week_1,
             dlc.eow_date + 14 week_2,
             dlc.eow_date + 21 week_3,
             dlc.eow_date + 28 week_4
        from period p,
             day_level_calendar dlc
       where p.vdate = dlc.day;

   cursor C_PLAN_FORECAST is
      select ALC_OI_PLAN_FORECAST_REC(gtt.date_1,    --eow_date
                                      gtt.number_2,  --allocated qty
                                      gtt.number_3)  --plan/forecast qty
        from gtt_6_num_6_str_6_date gtt;

BEGIN

   LOGGER.LOG_INFORMATION('Start: '||L_program);

   delete
     from gtt_10_num_10_str_10_date;

   LOGGER.LOG_INFORMATION(L_program||' Delete GTT_10_NUM_10_STR_10_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from gtt_6_num_6_str_6_date;

   LOGGER.LOG_INFORMATION(L_program||' Delete GTT_6_NUM_6_STR_6_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_GET_CALC_TYPE;
   fetch C_GET_CALC_TYPE into L_need_calc_type;
   close C_GET_CALC_TYPE;

   open C_GET_START_END_DATES;
   fetch C_GET_START_END_DATES into L_start_date,
                                    L_end_date,
                                    L_day,
                                    L_eow_date,
                                    L_week_1,
                                    L_week_2,
                                    L_week_3,
                                    L_week_4;
   close C_GET_START_END_DATES;

   insert into gtt_10_num_10_str_10_date(varchar2_1,   --item_parent
                                         varchar2_2,   --item
                                         varchar2_3,   --diff_1
                                         varchar2_4,   --diff_2
                                         varchar2_5,   --diff_3
                                         varchar2_6)   --diff_4
                                  select vim.item_parent,
                                         vim.item,
                                         vim.diff_1,
                                         vim.diff_2,
                                         vim.diff_3,
                                         vim.diff_4
                                    from v_item_master vim,
                                         v_item_master vimp
                                   where vim.item_parent = I_item
                                     and vim.item_parent = vimp.item
                                     and (   NVL(vim.diff_1, '-999') = NVL(I_diff1_id, '-999')
                                          or I_diff1_id is NULL)
                                     and (   NVL(vim.diff_2, '-999') = NVL(I_diff2_id, '-999')
                                          or I_diff2_id is NULL)
                                     and (   NVL(vim.diff_3, '-999') = NVL(I_diff3_id, '-999')
                                          or I_diff3_id is NULL)
                                     and (   NVL(vim.diff_4, '-999') = NVL(I_diff4_id, '-999')
                                          or I_diff4_id is NULL)
                                     and vim.item_level = vim.tran_level
                                   union all
                                  select NVL(vim.item_parent, I_item) item_parent,
                                         vim.item,
                                         vim.diff_1,
                                         vim.diff_2,
                                         vim.diff_3,
                                         vim.diff_4
                                    from v_item_master vim
                                   where vim.item       = I_item
                                     and vim.item_level = vim.tran_level;

   LOGGER.LOG_INFORMATION(L_program||' Insert ITEMS - GTT_10_NUM_10_STR_10_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into gtt_6_num_6_str_6_date(number_1,
                                      number_2,
                                      number_3,
                                      varchar2_1,
                                      date_1)
                               select I_loc,
                                      0 allocated_qty,
                                      0 plan_forecast_qty,
                                      I_item,
                                      L_eow_date
                                 from dual
                                union all
                               select I_loc,
                                      0 allocated_qty,
                                      0 plan_forecast_qty,
                                      I_item,
                                      L_week_1
                                 from dual
                                union all
                               select I_loc,
                                      0 allocated_qty,
                                      0 plan_forecast_qty,
                                      I_item,
                                      L_week_2
                                 from dual
                                union all
                               select I_loc,
                                      0 allocated_qty,
                                      0 plan_forecast_qty,
                                      I_item,
                                      L_week_3
                                 from dual
                                union all
                               select I_loc,
                                      0 allocated_qty,
                                      0 plan_forecast_qty,
                                      I_item,
                                      L_week_4
                                 from dual;

   LOGGER.LOG_INFORMATION(L_program||' Insert GTT_6_NUM_6_STR_6_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- updating allocated qty
   merge into gtt_6_num_6_str_6_date target
   using (select DISTINCT ail.location_id,
                 I_item item,
                 L_eow_date eow_date,
                 SUM(NVL(ail.allocated_qty, 0 )) OVER (PARTITION BY I_item,
                                                                    ail.location_id,
                                                                    L_eow_date) allocated_qty
           from  gtt_10_num_10_str_10_date gtt,
                 alc_alloc a,
                 alc_item_loc ail,
                 v_loc_comm_attrib_sec vloc
           where a.alloc_id      = ail.alloc_id
             and ail.item_id     = gtt.varchar2_2
             and ail.location_id = vloc.loc
             and ail.location_id = I_loc
             and ail.loc_type    = I_loc_type
             and a.status        IN ('2','6')
             and ail.release_date BETWEEN L_start_date and L_end_date) use_this
   on (    target.number_1    = use_this.location_id
       and target.varchar2_1  = use_this.item
       and target.date_1      = use_this.eow_date)
   when MATCHED then
      update
         set target.number_2 = NVL(use_this.allocated_qty, 0);

   LOGGER.LOG_INFORMATION(L_program||' Update ALLOC_QTY - GTT_6_NUM_6_STR_6_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --updating plan/forecast qty
   merge into gtt_6_num_6_str_6_date target
   using (select DISTINCT I_item item,
                 ifc.loc loc,
                 ifc.eow_date eow_date,
                 SUM(NVL(ifc.forecast_sales, 0)) OVER (PARTITION BY I_item,
                                                                    ifc.loc,
                                                                    ifc.eow_date) plan_forecast_qty
            from gtt_10_num_10_str_10_date gtt,
                 item_forecast ifc,
                 v_loc_comm_attrib_sec vloc
           where (   ifc.item = gtt.varchar2_1
                  or ifc.item = gtt.varchar2_2)
             and ifc.loc  = vloc.loc
             and ifc.loc  = I_loc
             and ifc.eow_date BETWEEN L_start_date and L_end_date
             and L_need_calc_type = 'F'
           union all
          select DISTINCT I_item item,
                 TO_NUMBER(ap.loc) loc,
                 ap.eow_date eow_date,
                 SUM(NVL(ap.qty, 0)) OVER (PARTITION BY I_item,
                                                        ap.loc,
                                                        ap.eow_date) plan_forecast_qty
            from gtt_10_num_10_str_10_date gtt,
                 alc_plan ap,
                 v_loc_comm_attrib_sec vloc
           where (   ap.item_id = gtt.varchar2_2 
                  or ap.item_id = gtt.varchar2_1
                  and (    (   NVL(ap.diff1_id, '-999') = NVL(gtt.varchar2_3, '-999')
                            or ap.diff1_id is NULL)
                       and (   NVL(ap.diff2_id, '-999') = NVL(gtt.varchar2_4, '-999')
                            or ap.diff2_id is NULL)
                       and (   NVL(ap.diff3_id, '-999') = NVL(gtt.varchar2_5, '-999')
                            or ap.diff3_id is NULL)
                       and (   NVL(ap.diff4_id, '-999') = NVL(gtt.varchar2_6, '-999')
                            or ap.diff4_id is NULL)))
             and TO_NUMBER(ap.loc) = vloc.loc
             and TO_NUMBER(ap.loc) = I_loc
             and ap.eow_date BETWEEN L_start_date and L_end_date
             and L_need_calc_type  = 'P'
      ) use_this
   on (    target.number_1   = use_this.loc
       and target.varchar2_1 = use_this.item
       and target.date_1     = use_this.eow_date)
   when MATCHED then
      update
         set target.number_3 = NVL(use_this.plan_forecast_qty, 0);

   LOGGER.LOG_INFORMATION(L_program||' Update PLAN_FORECAST_QTY - GTT_6_NUM_6_STR_6_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- delete records that do not have both allocated and plan/forecast qty
   delete
     from gtt_6_num_6_str_6_date
    where number_2 = 0  -- allocated_qty
      and number_3 = 0; -- plan_forecast_qty

   LOGGER.LOG_INFORMATION(L_program||' DELETE ZERO ALLOC QTY AND PLAN_FORECAST QTY - GTT_6_NUM_6_STR_6_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_PLAN_FORECAST;
   fetch C_PLAN_FORECAST BULK COLLECT into O_result;
   close C_PLAN_FORECAST;

   LOGGER.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END GET_PLAN_FORECAST;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_OPEN_TO_BUY(O_error_message   IN OUT VARCHAR2,
                         O_result          IN OUT ALC_OI_OTB_REC,
                         I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                         I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                         I_item            IN     ITEM_MASTER.ITEM%TYPE,
                         I_diff1_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_diff2_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_diff3_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_diff4_id        IN     DIFF_IDS.DIFF_ID%TYPE)
RETURN NUMBER IS

   L_program        VARCHAR2(61) := 'ALC_DASHBOARD_SQL.GET_OPEN_TO_BUY_DETAILS';
   L_start_time     TIMESTAMP    := SYSTIMESTAMP;
   L_release_date   DATE;

   cursor C_GET_EOW_DATE is
      select DISTINCT(dlc.eow_date)
        from alc_item_loc ail,
             day_level_calendar dlc,
             (select vim.item item
                from v_item_master vim,
                     v_item_master vimp
               where vim.item_parent = I_item
                 and vim.item_parent = vimp.item
                 and (   NVL(vim.diff_1, '-999') = NVL(I_diff1_id, '-999')
                      or I_diff1_id is NULL)
                 and (   NVL(vim.diff_2, '-999') = NVL(I_diff2_id, '-999')
                      or I_diff2_id is NULL)
                 and (   NVL(vim.diff_3, '-999') = NVL(I_diff3_id, '-999')
                      or I_diff3_id is NULL)
                 and (   NVL(vim.diff_4, '-999') = NVL(I_diff4_id, '-999')
                      or I_diff4_id is NULL)
                 and vim.item_level = vim.tran_level
               union all
              select vim.item item
                from v_item_master vim
               where vim.item       = I_item
                 and vim.item_level = vim.tran_level) item
        where ail.alloc_id = I_alloc_id
          and ail.item_id  = item.item
          and TO_DATE(ail.release_date, 'DD-MON-YYYY') = TO_DATE(dlc.day, 'DD-MON-YYYY');

   cursor C_GET_OTB is
      select ALC_OI_OTB_REC(o.n_budget_amt   + o.b_budget_amt,
                            o.n_approved_amt + o.b_approved_amt,
                            o.n_receipts_amt + o.b_receipts_amt,
                            (o.n_budget_amt  + o.b_budget_amt) - (o.n_approved_amt + o.b_approved_amt + o.n_receipts_amt + o.b_receipts_amt))
        from otb o,
             (select DISTINCT vim.dept,
                     vim.class,
                     vim.subclass
                from v_item_master vim,
                     v_item_master vimp
               where vim.item_parent = I_item
                 and vim.item_parent = vimp.item
                 and (   NVL(vim.diff_1, '-999') = NVL(I_diff1_id, '-999')
                      or I_diff1_id is NULL)
                 and (   NVL(vim.diff_2, '-999') = NVL(I_diff2_id, '-999')
                      or I_diff2_id is NULL)
                 and (   NVL(vim.diff_3, '-999') = NVL(I_diff3_id, '-999')
                      or I_diff3_id is NULL)
                 and (   NVL(vim.diff_4, '-999') = NVL(I_diff4_id, '-999')
                      or I_diff4_id is NULL)
                 and vim.item_level = vim.tran_level
               union all
              select vim.dept,
                     vim.class,
                     vim.subclass
                from v_item_master vim
               where vim.item       = I_item
                 and vim.item_level = vim.tran_level) i
       where o.dept     = i.dept
         and o.class    = i.class
         and o.subclass = i.subclass
         and TO_DATE(o.eow_date, 'DD-MON-YYYY') = TO_DATE(L_release_date, 'DD-MON-YYYY');

BEGIN

   LOGGER.LOG_INFORMATION('Start: '||L_program);

   open C_GET_EOW_DATE;
   fetch C_GET_EOW_DATE into L_release_date;
   close C_GET_EOW_DATE;

   open C_GET_OTB;
   fetch C_GET_OTB into O_result;
   close C_GET_OTB;

   LOGGER.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END GET_OPEN_TO_BUY;
-------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_STOCK_TO_SALES_VIEW(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program        VARCHAR2(61) := 'ALC_DASHBOARD_SQL.REFRESH_STOCK_TO_SALES_VIEW';
   L_start_time     TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start: '||L_program);

   DBMS_MVIEW.REFRESH('ALC_OI_STOCK_TO_SALES','C');

   LOGGER.LOG_TIME(L_program, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END REFRESH_STOCK_TO_SALES_VIEW;
-------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_TOP_BOTTOM_SELLER_VIEW(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program        VARCHAR2(61) := 'ALC_DASHBOARD_SQL.REFRESH_TOP_BOTTOM_SELLER_VIEW';
   L_start_time     TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION('Start: '||L_program);

   DBMS_MVIEW.REFRESH('ALC_OI_TOP_BOTTOM_SELLER','C');

   LOGGER.LOG_TIME(L_program, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END REFRESH_TOP_BOTTOM_SELLER_VIEW;
-------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_MATERIALIZED_VIEWS(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(100) := 'ALC_DASHBOARD_SQL.MULTIVIEW_REFRESH';

BEGIN

   if REFRESH_STOCK_TO_SALES_VIEW(O_error_message) = OI_UTILITY.FAILURE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   if REFRESH_TOP_BOTTOM_SELLER_VIEW(O_error_message) = OI_UTILITY.FAILURE then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return OI_UTILITY.FAILURE;
END REFRESH_MATERIALIZED_VIEWS;
-------------------------------------------------------------------------------------------------------------
END ALC_DASHBOARD_SQL;
/
