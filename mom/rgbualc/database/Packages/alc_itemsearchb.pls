CREATE OR REPLACE PACKAGE BODY ALC_ITEM_SEARCH_SQL AS
------------------------------------------------------------------------------------------------------
TYPE ASN_DIST_REC_TYPE IS RECORD
(
   item                     VARCHAR2(25),
   diff_1                   VARCHAR2(10),
   diff_2                   VARCHAR2(10),
   diff_3                   VARCHAR2(10),
   diff_4                   VARCHAR2(10),
   item_desc                VARCHAR2(250),
   rollup_type              VARCHAR2(20),
   rollup_item              VARCHAR2(25),
   rollup_item_desc         VARCHAR2(250),
   multi_color_pack_ind     VARCHAR2(1),
   location                 NUMBER,
   doc_no                   VARCHAR2(30),
   source_type              VARCHAR2(5),
   avail_qty                NUMBER
);
TYPE ASN_DIST_TABLE_TYPE IS TABLE OF ASN_DIST_REC_TYPE INDEX BY BINARY_INTEGER;
------------------------------------------------------------------------------------------------------

LP_max_results NUMBER(6) := 300;

LP_with_queries VARCHAR2(6000) := q'[
   with sc_scalars as (
      select :1 as supplier_sites_ind,
             :2 as po_source_type_ind,
             :3 as asn_source_type_ind,
             :4 as tsf_source_type_ind,
             :5 as bol_source_type_ind,
             :6 as alloc_source_type_ind,
             :7 as wh_source_type_ind,
             :8 as dept,
             :9 as cls,
             :10 as subclass,
             :11 as season,
             :12 as phase,
             :13 as start_date,
             :14 as end_date
        from dual),
   sc_items      as (select /*+ CARDINALITY(input_items 5) */
                            value(input_items) item
                       from table(cast(:15 as OBJ_VARCHAR_ID_TABLE)) input_items),
   sc_item_lists as (select /*+ CARDINALITY(item_list 5) */
                            value(input_item_lists) item_list
                       from table(cast(:16 as OBJ_NUMERIC_ID_TABLE)) input_item_lists),
   sc_udas       as (select /*+ CARDINALITY(input_udas 5) */
                            value(input_udas) uda
                       from table(cast(:17 as OBJ_NUMERIC_ID_TABLE)) input_udas),
   sc_uda_values as (select /*+ CARDINALITY(input_uda_values 5) */
                            value(input_uda_values) uda_value
                       from table(cast(:18 as OBJ_NUMERIC_ID_TABLE)) input_uda_values),
   sc_sups       as (select /*+ CARDINALITY(input_sups 5) */
                            value(input_sups) sup
                       from table(cast(:19 as OBJ_NUMERIC_ID_TABLE)) input_sups),
   sc_sup_sites  as (select /*+ CARDINALITY(input_sup_sites 5) */
                            value(input_sup_sites) sup_site
                       from table(cast(:20 as OBJ_NUMERIC_ID_TABLE)) input_sup_sites),
   sc_pos        as (select /*+ CARDINALITY(input_pos 5) */
                            value(input_pos) po
                       from table(cast(:21 as OBJ_NUMERIC_ID_TABLE)) input_pos),
   sc_asns       as (select /*+ CARDINALITY(input_asns 5) */
                            value(input_asns) asn
                       from table(cast(:22 as OBJ_VARCHAR_ID_TABLE)) input_asns),
   sc_tsfs       as (select /*+ CARDINALITY(input_tsfs 5) */
                            value(input_tsfs) tsf
                       from table(cast(:23 as OBJ_NUMERIC_ID_TABLE)) input_tsfs),
   sc_bols       as (select /*+ CARDINALITY(input_bols 5) */
                            value(input_bols) bol
                       from table(cast(:24 as OBJ_VARCHAR_ID_TABLE)) input_bols),
   sc_appts      as (select /*+ CARDINALITY(input_appts 5) */
                            value(input_appts) appt
                       from table(cast(:25 as OBJ_NUMERIC_ID_TABLE)) input_appts),
   sc_allocs     as (select /*+ CARDINALITY(input_allocs 5) */
                            value(input_allocs) alloc
                       from table(cast(:26 as OBJ_NUMERIC_ID_TABLE)) input_allocs)
   ]';

--
--TXN queries
--
LP_pos_query VARCHAR2(600) := q'[
   select DISTINCT
          ol.item,
          ol.location
     from sc_pos,
          ordloc ol,
          alc_itemsearch_whs_gtt w
    where sc_pos.po    = ol.order_no
      and ol.loc_type  = 'W'
      and w.wh         = ol.location
   ]';

LP_asn_query VARCHAR2(600) := q'[
   select DISTINCT
          ss.item,
          ol.location
     from sc_asns,
          shipment sh,
          shipsku ss,
          ordloc ol,
          wh,
          alc_itemsearch_whs_gtt w
    where sc_asns.asn    = sh.asn
      and sh.to_loc_type = 'W'
      and sh.shipment    = ss.shipment
      and sh.order_no    = ol.order_no
      and ss.item        = ol.item
      and ol.location    = w.wh
      and ol.location    = wh.wh
      and wh.physical_wh = sh.to_loc
   ]';

LP_tsf_query VARCHAR2(600) := q'[
   select DISTINCT
          td.item,
          th.to_loc
     from sc_tsfs,
          tsfhead th,
          tsfdetail td,
          alc_itemsearch_whs_gtt w
    where sc_tsfs.tsf    = th.tsf_no
      and th.to_loc_type = 'W'
      and th.tsf_no      = td.tsf_no
      and w.wh           = th.to_loc
   ]';

LP_bol_query VARCHAR2(2000) := q'[
   select DISTINCT
          ss.item,
          th.to_loc
     from sc_bols,
          shipment sh,
          shipsku ss,
          tsfhead th,
          wh,
          alc_itemsearch_whs_gtt w
    where sc_bols.bol    = sh.bol_no
      and sh.to_loc_type = 'W'
      and sh.shipment    = ss.shipment
      and ss.distro_type = 'T'
      and ss.distro_no   = th.tsf_no
      and th.to_loc      = w.wh
      and wh.physical_wh = sh.to_loc
   union all
   select DISTINCT
          ss.item,
          ad.to_loc
     from sc_bols,
          shipment sh,
          shipsku ss,
          alloc_detail ad,
          wh,
          alc_itemsearch_whs_gtt w
    where sc_bols.bol    = sh.bol_no
      and sh.to_loc_type = 'W'
      and sh.shipment    = ss.shipment
      and ss.distro_type = 'A'
      and ss.distro_no   = ad.alloc_no
      and ad.to_loc      = w.wh
      and wh.physical_wh = sh.to_loc
   ]';

LP_appt_query VARCHAR2(600) := q'[
   select DISTINCT
          ad.item,
          ad.loc
     from sc_appts,
          appt_detail ad,
          alc_itemsearch_whs_gtt w
    where sc_appts.appt = ad.appt
      and ad.loc_type   = 'W'
      and w.wh          = ad.loc
   ]';

LP_alloc_query VARCHAR2(600) := q'[
   select DISTINCT
          ah.item,
          ad.to_loc
     from sc_allocs,
          alloc_header ah,
          alloc_detail ad,
          alc_itemsearch_whs_gtt w
    where sc_allocs.alloc = ah.alloc_no
      and ad.to_loc_type  = 'W'
      and ah.alloc_no     = ad.alloc_no
      and w.wh            = ad.to_loc
   ]';

--
--ITEM queries
--

LP_mh_query_sbc VARCHAR2(600) := q'[
   select im.item
     from sc_scalars,
          item_master im
    where im.dept       = sc_scalars.dept
      and im.class      = sc_scalars.cls
      and im.subclass   = sc_scalars.subclass
      and im.item_level = im.tran_level
      and im.status     = 'A'
   ]';

LP_mh_query_cls VARCHAR2(600) := q'[
   select im.item
     from sc_scalars,
          item_master im
    where im.dept       = sc_scalars.dept
      and im.class      = sc_scalars.cls
      and im.item_level = im.tran_level
      and im.status     = 'A'
   ]';

LP_mh_query_dept VARCHAR2(600) := q'[
   select im.item
     from sc_scalars,
          item_master im
    where im.dept       = sc_scalars.dept
      and im.item_level = im.tran_level
      and im.status     = 'A'
   ]';

LP_items_query VARCHAR2(4000) := q'[
   (select im.item
      from sc_items,
           item_master im
     where sc_items.item = im.item
       and im.item_level = im.tran_level
       and im.status     = 'A'
    union
    select im.item
      from sc_items,
           item_master im
     where sc_items.item = im.item_parent
       and im.item_level = im.tran_level
       and im.status     = 'A'
    union
    select im.item
      from sc_items,
           item_master im
     where sc_items.item = im.item_grandparent
       and im.item_level = im.tran_level
       and im.status     = 'A')
   ]';

LP_season_phase_query VARCHAR2(4000) := q'[
   (select DISTINCT
           im.item
      from sc_scalars,
           item_master im,
           item_seasons its
     where its.season_id = NVL(sc_scalars.season, its.season_id)
       and its.phase_id  = NVL(sc_scalars.phase, its.phase_id)
       and its.item      = im.item
       and im.item_level = im.tran_level
       and im.status     = 'A'
    union
    select DISTINCT
           im.item
      from sc_scalars,
           item_master im,
           item_seasons its
     where its.season_id = NVL(sc_scalars.season, its.season_id)
       and its.phase_id  = NVL(sc_scalars.phase, its.phase_id)
       and its.item      = im.item_parent
       and im.item_level = im.tran_level
       and im.status     = 'A'
    union
    select DISTINCT
           im.item
      from sc_scalars,
           item_master im,
           item_seasons its
     where its.season_id = NVL(sc_scalars.season, its.season_id)
       and its.phase_id  = NVL(sc_scalars.phase, its.phase_id)
       and its.item      = im.item_grandparent
       and im.item_level = im.tran_level
       and im.status     = 'A')
   ]';

LP_skulist_query VARCHAR2(4000) := q'[
   (select DISTINCT
           im.item
      from sc_item_lists,
           item_master im,
           skulist_detail sd
     where sc_item_lists.item_list  = sd.skulist
       and sd.item                  = im.item
       and im.item_level            = im.tran_level
       and im.status                = 'A'
    union
    select DISTINCT
           im.item
      from sc_item_lists,
           item_master im,
           skulist_detail sd
     where sc_item_lists.item_list  = sd.skulist
       and sd.item                  = im.item_parent
       and im.item_level            = im.tran_level
       and im.status                = 'A'
    union
    select DISTINCT
           im.item
      from sc_item_lists,
           item_master im,
           skulist_detail sd
     where sc_item_lists.item_list  = sd.skulist
       and sd.item                  = im.item_grandparent
       and im.item_level            = im.tran_level
       and im.status                = 'A')
   ]';

LP_sup_query VARCHAR2(700) := q'[
   select DISTINCT
          im.item
     from sc_sups,
          item_master im,
          item_supplier sd,
          sups s,
          sc_scalars
    where ((sc_sups.sup                   = s.supplier_parent and
            sc_scalars.supplier_sites_ind = 'Y')
           or
           (sc_sups.sup                   = s.supplier and
            sc_scalars.supplier_sites_ind = 'N'))
      and s.supplier           = sd.supplier
      and sd.item              = im.item
      and im.item_level        = im.tran_level
      and im.status            = 'A'
   ]';

LP_sup_site_query VARCHAR2(600) := q'[
   select DISTINCT
          im.item
     from sc_sup_sites,
          item_master im,
          item_supplier sd,
          sc_scalars
    where sc_sup_sites.sup_site  = sd.supplier
      and sd.item                = im.item
      and im.item_level          = im.tran_level
      and im.status              = 'A'
      and sc_scalars.supplier_sites_ind = 'Y'
   ]';

LP_uda_query VARCHAR2(4000) := q'[
   (select DISTINCT
           im.item
      from sc_udas,
           item_master im,
           uda_item_lov lov
     where sc_udas.uda   = lov.uda_id
       and lov.item      = im.item
       and im.item_level = im.tran_level
       and im.status     = 'A'
    union
    select DISTINCT
           im.item
      from sc_udas,
           item_master im,
           uda_item_lov lov
     where sc_udas.uda   = lov.uda_id
       and lov.item      = im.item_parent
       and im.item_level = im.tran_level
       and im.status     = 'A'
    union
    select DISTINCT
           im.item
      from sc_udas,
           item_master im,
           uda_item_lov lov
     where sc_udas.uda   = lov.uda_id
       and lov.item      = im.item_grandparent
       and im.item_level = im.tran_level
       and im.status     = 'A')
   ]';

LP_uda_value_query VARCHAR2(4000) := q'[
   (select DISTINCT
           im.item
      from sc_udas,
           sc_uda_values,
           item_master im,
           uda_item_lov lov
     where lov.uda_id    = sc_udas.uda
       and lov.uda_value = sc_uda_values.uda_value
       and lov.item      = im.item
       and im.item_level = im.tran_level
       and im.status     = 'A'
    union
    select DISTINCT
           im.item
      from sc_udas,
           sc_uda_values,
           item_master im,
           uda_item_lov lov
     where lov.uda_id    = sc_udas.uda
       and lov.uda_value = sc_uda_values.uda_value
       and lov.item      = im.item_parent
       and im.item_level = im.tran_level
       and im.status     = 'A'
    union
    select DISTINCT
           im.item
      from sc_udas,
           sc_uda_values,
           item_master im,
           uda_item_lov lov
     where lov.uda_id    = sc_udas.uda
       and lov.uda_value = sc_uda_values.uda_value
       and lov.item      = im.item_grandparent
       and im.item_level = im.tran_level
       and im.status     = 'A')
   ]';

------------------------------------------------------------------------------------------------------
FUNCTION MAP_ALC_IDS_TO_RMS_IDS(O_error_message     IN OUT VARCHAR2,
                                O_search_criteria   IN OUT ALC_ITEM_SEARCH_CRITERIA_REC,
                                I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION COMMON_SEARCH(O_error_message     IN OUT VARCHAR2,
                       I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                       I_expand_to_pack    IN     BOOLEAN,
                       I_what_if_ind       IN     BOOLEAN)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION SETUP_WHS(O_error_message     IN OUT VARCHAR2,
                   I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                   I_what_if_ind       IN     BOOLEAN)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOCS_FROM_TXNS_WHS(O_error_message     IN OUT VARCHAR2,
                                     I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                                     I_expand_to_pack    IN     BOOLEAN)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION DEL_TXNS_NOT_COVERD_BY_ITEMS(O_error_message     IN OUT VARCHAR2,
                                      I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOCS_FROM_ITEMS_WHS(O_error_message     IN OUT VARCHAR2,
                                      I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION BUILD_ITEM_INTERSECTS(O_error_message     IN OUT VARCHAR2,
                               O_intersect_built      OUT BOOLEAN,
                               O_item_intersects      OUT VARCHAR2,
                               I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_PACKS_UNDER_STYLE_STAPLE(O_error_message   IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_MULTI_PARENT_PACKS(O_error_message   IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION MARK_ROLLUP_GROUP(O_error_message   IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION FIND_MAXROWS_WITH_INVENTORY(O_error_message       IN OUT VARCHAR2,
                                     O_max_rows_exceeded   IN OUT NUMBER,
                                     I_search_criteria     IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_WH_SOURCE_INV(O_error_message        IN OUT VARCHAR2,
                           IO_current_row_count   IN OUT NUMBER,
                           O_max_rows_exceeded    IN OUT NUMBER)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_PO_SOURCE_INV(O_error_message        IN OUT VARCHAR2,
                           IO_current_row_count   IN OUT NUMBER,
                           O_max_rows_exceeded    IN OUT NUMBER,
                           I_search_criteria      IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ASN_SOURCE_INV(O_error_message        IN OUT VARCHAR2,
                            IO_current_row_count   IN OUT NUMBER,
                            O_max_rows_exceeded    IN OUT NUMBER,
                            I_search_criteria      IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_TSF_SOURCE_INV(O_error_message        IN OUT VARCHAR2,
                            IO_current_row_count   IN OUT NUMBER,
                            O_max_rows_exceeded    IN OUT NUMBER,
                            I_search_criteria      IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_BOL_SOURCE_INV(O_error_message        IN OUT VARCHAR2,
                            IO_current_row_count   IN OUT NUMBER,
                            O_max_rows_exceeded    IN OUT NUMBER,
                            I_search_criteria      IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ALLOC_SOURCE_INV(O_error_message        IN OUT VARCHAR2,
                              IO_current_row_count   IN OUT NUMBER,
                              O_max_rows_exceeded    IN OUT NUMBER,
                              I_search_criteria      IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_INVENTORY_FOR_SELECTION(O_error_message     IN OUT VARCHAR2,
                                     I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_SOURCE_INV_SELECTION(O_error_message     IN OUT VARCHAR2,
                                  I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ASN_SOURCE_FOR_SELECTION(O_error_message     IN OUT VARCHAR2,
                                      I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_NO_PARENTS(O_error_message    IN OUT VARCHAR2,
                             O_work_header_id   IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_FASHION(O_error_message    IN OUT VARCHAR2,
                          O_work_header_id   IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                          I_what_if_ind      IN     BOOLEAN)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PACK_COMP(O_error_message    IN OUT VARCHAR2,
                            O_work_header_id   IN     alc_work_header.work_header_id%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_NONSELL_PACK_BACKORD(O_error_message    IN OUT VARCHAR2,
                                       O_work_header_id   IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION INSERT_HEADER(O_error_message    IN OUT VARCHAR2,
                       O_work_header_id      OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                       I_work_type        IN     ALC_WORK_HEADER.WORK_TYPE%TYPE,
                       I_user_id          IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION QUICK_CREATE_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                                O_work_header_id       OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                                O_alloc_type           OUT ALC_ALLOC.TYPE%TYPE,
                                I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                                I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE,
                                I_what_if_ind       IN     BOOLEAN)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_AWIS_ROWS_WORK_HEADER(O_error_message    IN OUT VARCHAR2,
                                        I_work_header_id   IN ALC_WORK_HEADER.WORK_HEADER_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
FUNCTION SEARCH(O_error_message       IN OUT VARCHAR2,
                O_max_rows_exceeded      OUT NUMBER,
                I_search_criteria     IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN NUMBER IS

   L_program           VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.SEARCH';
   L_start_time        TIMESTAMP := SYSTIMESTAMP;
   L_search_criteria   ALC_ITEM_SEARCH_CRITERIA_REC := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   O_max_rows_exceeded := 0;

   if MAP_ALC_IDS_TO_RMS_IDS(O_error_message,
                             L_search_criteria,
                             I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_SEARCH(O_error_message,
                    L_search_criteria,
                    TRUE,
                    FALSE) = FALSE then
      return 0;
   end if;

   if FIND_MAXROWS_WITH_INVENTORY(O_error_message,
                                  O_max_rows_exceeded,
                                  L_search_criteria) = FALSE then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END SEARCH;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                            O_work_header_id       OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                            I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                            I_user_id           IN     alc_work_header.created_by%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.POPULATE_WORKSHEET';
   L_start_time   TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if INSERT_HEADER(O_error_message,
                    O_work_header_id,
                    ALC_CONSTANTS_SQL.WORKSHEET_TYPE_WORK,
                    I_user_id) = FALSE then
      return 0;
   end if;

   if ADD_TO_WORKSHEET(O_error_message,
                       O_work_header_id,
                       I_search_criteria,
                       I_user_id) = 0 then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END POPULATE_WORKSHEET;
------------------------------------------------------------------------------------------------------
FUNCTION ADD_TO_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                          I_work_header_id    IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                          I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                          I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER IS

   L_program           VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.ADD_TO_WORKSHEET';
   L_search_criteria   ALC_ITEM_SEARCH_CRITERIA_REC := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if MAP_ALC_IDS_TO_RMS_IDS(O_error_message,
                             L_search_criteria,
                             I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_SEARCH(O_error_message,
                    L_search_criteria,
                    TRUE,
                    FALSE) = FALSE then
      return 0;
   end if;

   if GET_INVENTORY_FOR_SELECTION(O_error_message,
                                  L_search_criteria) = FALSE then
      return 0;
   end if;

   if POPULATE_NO_PARENTS(O_error_message,
                          I_work_header_id) = FALSE then
      return 0;
   end if;
   --
   if POPULATE_FASHION(O_error_message,
                       I_work_header_id,
                       FALSE) = FALSE then
      return 0;
   end if;
   --
   if POPULATE_PACK_COMP(O_error_message,
                         I_work_header_id) = FALSE then
      return 0;
   end if;
   --
   if POPULATE_NONSELL_PACK_BACKORD(O_error_message,
                                    I_work_header_id) = FALSE then
      return 0;
   end if;
   --
   if ALC_HUB_FACET_CFG_SQL.POPULATE_WORK_ITEM_SOURCE_DIFF(O_error_message,
                                                           I_work_header_id) = FALSE then
      return 0;
   end if;
   --
   if VALIDATE_AWIS_ROWS_WORK_HEADER(O_error_message,
                                     I_work_header_id)= FALSE then
      return 0;
   end if;
   --

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ADD_TO_WORKSHEET;
------------------------------------------------------------------------------------------------------
FUNCTION WHAT_IF_POPULATE_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                                    O_work_header_id       OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                                    I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                                    I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.WHAT_IF_POPULATE_WORKSHEET';

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if INSERT_HEADER(O_error_message,
                    O_work_header_id,
                    ALC_CONSTANTS_SQL.WORKSHEET_TYPE_WORK,
                    I_user_id) = FALSE then
      return 0;
   end if;

   if WHAT_IF_ADD_TO_WORKSHEET(O_error_message,
                               O_work_header_id,
                               I_search_criteria,
                               I_user_id) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END WHAT_IF_POPULATE_WORKSHEET;
------------------------------------------------------------------------------------------------------
FUNCTION WHAT_IF_ADD_TO_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                                  I_work_header_id    IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                                  I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                                  I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.WHAT_IF_ADD_TO_WORKSHEET';
   L_search_criteria    ALC_ITEM_SEARCH_CRITERIA_REC := NULL;

BEGIN

   if MAP_ALC_IDS_TO_RMS_IDS(O_error_message,
                             L_search_criteria,
                             I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_SEARCH(O_error_message,
                    L_search_criteria,
                    TRUE,
                    TRUE) = FALSE then
      return 0;
   end if;

   --no inventory lookups needed for what-if, so just put all rows into alc_itemsearch_inv_gtt
   insert into alc_itemsearch_inv_gtt (item,
                                       diff_1,
                                       diff_2,
                                       diff_3,
                                       diff_4,
                                       item_desc,
                                       rollup_type,
                                       rollup_item,
                                       rollup_item_desc,
                                       multi_color_pack_ind,
                                       location,
                                       doc_no,
                                       source_type,
                                       avail_qty)
                                select DISTINCT gtt.item,
                                       im.diff_1,
                                       im.diff_2,
                                       im.diff_3,
                                       im.diff_4,
                                       im.item_desc,
                                       gtt.rollup_type,
                                       gtt.rollup_item,
                                       gtt.rollup_item_desc,
                                       gtt.multi_color_pack_ind,
                                       -1,  --gtt.location,
                                       NULL,
                                       ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_WHAT_IF,
                                       0
                                  from alc_itemsearch_items_gtt gtt,
                                       item_master im
                                 where gtt.item = im.item;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_ITEMSEARCH_INV_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if POPULATE_NO_PARENTS(O_error_message,
                          I_work_header_id) = FALSE then
      return 0;
   end if;
   --
   if POPULATE_FASHION(O_error_message,
                       I_work_header_id,
                       TRUE) = FALSE then
      return 0;
   end if;
   --
   if POPULATE_PACK_COMP(O_error_message,
                         I_work_header_id) = FALSE then
      return 0;
   end if;
   --
   if POPULATE_NONSELL_PACK_BACKORD(O_error_message,
                                    I_work_header_id) = FALSE then
      return 0;
   end if;
   --
   if ALC_HUB_FACET_CFG_SQL.POPULATE_WORK_ITEM_SOURCE_DIFF(O_error_message,
                                                           I_work_header_id) = FALSE then
      return 0;
   end if;
   --
   if VALIDATE_AWIS_ROWS_WORK_HEADER(O_error_message,
                                     I_work_header_id)= FALSE then
      return 0;
   end if;
   --

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END WHAT_IF_ADD_TO_WORKSHEET;
------------------------------------------------------------------------------------------------------
FUNCTION WHAT_IF_SEARCH(O_error_message     IN OUT VARCHAR2,
                        O_max_rows_exceeded    OUT NUMBER,
                        I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.SEARCH';

   L_row_count          NUMBER(6) := 0;
   L_search_criteria    ALC_ITEM_SEARCH_CRITERIA_REC := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   O_max_rows_exceeded := 0;

   if MAP_ALC_IDS_TO_RMS_IDS(O_error_message,
                             L_search_criteria,
                             I_search_criteria) = FALSE then
      return 0;
   end if;

   if COMMON_SEARCH(O_error_message,
                    L_search_criteria,
                    TRUE,
                    TRUE) = FALSE then
      return 0;
   end if;

   insert into alc_itemsearch_items_res_gtt(rollup_type,
                                            rollup_item,
                                            rollup_item_desc)
   select i.rollup_type,
          i.rollup_item,
          i.rollup_item_desc
     from (select DISTINCT gtt.rollup_type,
                  gtt.rollup_item,
                  gtt.rollup_item_desc
             from alc_itemsearch_items_gtt gtt) i
    where rownum <= LP_max_results;

   L_row_count := SQL%ROWCOUNT;

   if L_row_count >= LP_max_results then
      O_max_rows_exceeded := 1;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END WHAT_IF_SEARCH;
------------------------------------------------------------------------------------------------------
FUNCTION MAP_ALC_IDS_TO_RMS_IDS(O_error_message     IN OUT VARCHAR2,
                                O_search_criteria   IN OUT ALC_ITEM_SEARCH_CRITERIA_REC,
                                I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.MAP_ALC_IDS_TO_RMS_IDS';

   L_input_allocs_cnt     NUMBER(10) := 0;
   L_input_alc_allocs_cnt NUMBER(10) := 0;
   L_local_alc_allocs_cnt NUMBER(10) := 0;

   cursor C_ALC_TO_RMS is
      select /*+ CARDINALITY(input_allocs 5) */
             value(input_allocs) alloc
        from table(cast(O_search_criteria.allocs as OBJ_NUMERIC_ID_TABLE)) input_allocs
      union
      select /*+ CARDINALITY(input_allocs 5) */
             TO_NUMBER(x.xref_alloc_no)
        from table(cast(O_search_criteria.alc_allocs as OBJ_NUMERIC_ID_TABLE)) input_alc_allocs,
             alc_xref x
       where value(input_alc_allocs) = x.alloc_id;

BEGIN

   O_search_criteria := I_search_criteria;

   if O_search_criteria.alc_allocs is NOT NULL and O_search_criteria.alc_allocs.COUNT > 0 then
      open C_ALC_TO_RMS;
      fetch C_ALC_TO_RMS BULK COLLECT into O_search_criteria.allocs;
      close C_ALC_TO_RMS;
   end if;

   if I_search_criteria.allocs is NOT NULL and I_search_criteria.allocs.COUNT > 0 then
       L_input_allocs_cnt := I_search_criteria.allocs.COUNT;
   else
       L_input_allocs_cnt := 0;
   end if;
   if I_search_criteria.alc_allocs is NOT NULL and I_search_criteria.alc_allocs.COUNT > 0 then
       L_input_alc_allocs_cnt := I_search_criteria.alc_allocs.COUNT;
   else
       L_input_alc_allocs_cnt := 0;
   end if;
   if O_search_criteria.allocs is NOT NULL and O_search_criteria.allocs.COUNT > 0 then
       L_local_alc_allocs_cnt := O_search_criteria.allocs.COUNT;
   else
       L_local_alc_allocs_cnt := 0;
   end if;
   --

   if (L_input_allocs_cnt + L_input_alc_allocs_cnt) > L_local_alc_allocs_cnt then
      O_error_message := ALC_CONSTANTS_SQL.ERRMSG_QUICK_CREATE_INV_TXN;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END MAP_ALC_IDS_TO_RMS_IDS;
------------------------------------------------------------------------------------------------------
FUNCTION COMMON_SEARCH(O_error_message     IN OUT VARCHAR2,
                       I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                       I_expand_to_pack    IN     BOOLEAN,
                       I_what_if_ind       IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.COMMON_SEARCH';

BEGIN

   select tp_item_search_max
     into LP_max_results
     from alc_system_options;

   --clean up GTTs
   delete
     from alc_itemsearch_whs_gtt;

   delete
     from alc_itemsearch_items_gtt;

   delete
     from alc_itemsearch_items_res_gtt;

   --setup table with whs that are valid for the search
   if SETUP_WHS(O_error_message,
                I_search_criteria,
                I_what_if_ind) = FALSE then
      return FALSE;
   end if;

   --get item/locs combinations based on txns
   if (I_search_criteria.pos    is NOT NULL and I_search_criteria.pos.COUNT    >= 1 or
       I_search_criteria.asns   is NOT NULL and I_search_criteria.asns.COUNT   >= 1 or
       I_search_criteria.tsfs   is NOT NULL and I_search_criteria.tsfs.COUNT   >= 1 or
       I_search_criteria.bols   is NOT NULL and I_search_criteria.bols.COUNT   >= 1 or
       I_search_criteria.appts  is NOT NULL and I_search_criteria.appts.COUNT  >= 1 or
       I_search_criteria.allocs is NOT NULL and I_search_criteria.allocs.COUNT >= 1) then

      --find item/loc combinations based on txns
      --will always populate txn level items
      if GET_ITEM_LOCS_FROM_TXNS_WHS(O_error_message,
                                     I_search_criteria,
                                     I_expand_to_pack) = FALSE then
         return FALSE;
      end if;

      --remove items not covered by the item criteria
      if DEL_TXNS_NOT_COVERD_BY_ITEMS(O_error_message,
                                      I_search_criteria) = FALSE then
         return FALSE;
      end if;

   else -- find item/loc combinations based on item criteria

      --get txn level items covered by the item criteria and whs
      --will always populate txn level items
      if GET_ITEM_LOCS_FROM_ITEMS_WHS(O_error_message,
                                      I_search_criteria) = FALSE then
         return FALSE;
      end if;

   end if;

   if I_expand_to_pack then
      if GET_PACKS_UNDER_STYLE_STAPLE(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   --remove packs with items with more than 1 parent
   if REMOVE_MULTI_PARENT_PACKS(O_error_message) = FALSE then
      return FALSE;
   end if;

   --group to rollup level
   if MARK_ROLLUP_GROUP(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END COMMON_SEARCH;
------------------------------------------------------------------------------------------------------
FUNCTION SETUP_WHS(O_error_message   IN OUT VARCHAR2,
                   I_search_criteria IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                   I_what_if_ind     IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.SETUP_WHS';

BEGIN

   if I_what_if_ind then

      insert into alc_itemsearch_whs_gtt (wh)
      values (-1);

   elsif (I_search_criteria.whs is NOT NULL and I_search_criteria.whs.COUNT >= 1) then

      insert into alc_itemsearch_whs_gtt (wh)
      select value(input)
        from table(cast(I_search_criteria.whs as OBJ_NUMERIC_ID_TABLE)) input,
             wh
       where value(input)        = wh.wh
         and wh.stockholding_ind = 'Y'
         and wh.finisher_ind     = 'N'
         and wh.redist_wh_ind    = 'N';

   else

      insert into alc_itemsearch_whs_gtt (wh)
      select wh.wh
        from wh
       where wh.stockholding_ind = 'Y'
         and wh.finisher_ind     = 'N'
         and wh.redist_wh_ind    = 'N';

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_WHS;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOCS_FROM_TXNS_WHS(O_error_message   IN OUT VARCHAR2,
                                     I_search_criteria IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                                     I_expand_to_pack  IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_ITEM_LOCS_FROM_TXNS_WHS';

   L_insert             VARCHAR2(200) := q'[insert into alc_itemsearch_items_gtt (item, location)]';
   L_intersects         VARCHAR2(15000) := NULL;
   L_cnt                NUMBER := 0;

   L_supplier_sites_ind SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE := NULL;

BEGIN

   select supplier_sites_ind
     into L_supplier_sites_ind
     from system_options;

   if (I_search_criteria.pos   is NOT NULL and I_search_criteria.pos.COUNT   >= 1) then
      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      L_intersects := L_intersects ||LP_pos_query;
      L_cnt := L_cnt + 1;
   end if;
   if (I_search_criteria.asns  is NOT NULL and I_search_criteria.asns.COUNT  >= 1) then
      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      L_intersects := L_intersects ||LP_asn_query;
      L_cnt := L_cnt + 1;
   end if;
   if (I_search_criteria.tsfs  is NOT NULL and I_search_criteria.tsfs.COUNT  >= 1) then
      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      L_intersects := L_intersects ||LP_tsf_query;
      L_cnt := L_cnt + 1;
   end if;
   if (I_search_criteria.bols  is NOT NULL and I_search_criteria.bols.COUNT  >= 1) then
      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      L_intersects := L_intersects ||LP_bol_query;
      L_cnt := L_cnt + 1;
   end if;
   if (I_search_criteria.appts is NOT NULL and I_search_criteria.appts.COUNT >= 1) then
      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      L_intersects := L_intersects ||LP_appt_query;
      L_cnt := L_cnt + 1;
   end if;
   if (I_search_criteria.allocs is NOT NULL and I_search_criteria.allocs.COUNT >= 1) then
      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      L_intersects := L_intersects ||LP_alloc_query;
      L_cnt := L_cnt + 1;
   end if;
   ---

   EXECUTE IMMEDIATE L_insert||LP_with_queries||L_intersects
      using L_supplier_sites_ind,
            I_search_criteria.po_source_type_ind,
            I_search_criteria.asn_source_type_ind,
            I_search_criteria.tsf_source_type_ind,
            I_search_criteria.bol_source_type_ind,
            I_search_criteria.alloc_source_type_ind,
            I_search_criteria.wh_source_type_ind,
            I_search_criteria.dept,
            I_search_criteria.class,
            I_search_criteria.subclass,
            I_search_criteria.season,
            I_search_criteria.phase,
            I_search_criteria.start_date,
            I_search_criteria.end_date,
            I_search_criteria.items,
            I_search_criteria.item_lists,
            I_search_criteria.udas,
            I_search_criteria.uda_values,
            I_search_criteria.sups,
            I_search_criteria.sup_sites,
            I_search_criteria.pos,
            I_search_criteria.asns,
            I_search_criteria.tsfs,
            I_search_criteria.bols,
            I_search_criteria.appts,
            I_search_criteria.allocs;

   --get comp items of any packs returned
   if I_expand_to_pack then
      merge into alc_itemsearch_items_gtt target
      using (select DISTINCT
                    pb.item,
                    gtt.location
               from alc_itemsearch_items_gtt gtt,
                    packitem_breakout pb
              where gtt.item = pb.pack_no) use_this
      on (    target.item     = use_this.item
          and target.location = use_this.location)
      when NOT MATCHED then
         insert (item,
                 location)
         values (use_this.item,
                 use_this.location);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_LOCS_FROM_TXNS_WHS;
------------------------------------------------------------------------------------------------------
FUNCTION DEL_TXNS_NOT_COVERD_BY_ITEMS(O_error_message   IN OUT VARCHAR2,
                                      I_search_criteria IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.DEL_TXNS_NOT_COVERD_BY_ITEMS';

   L_delete             VARCHAR2(200) := q'[delete from alc_itemsearch_items_gtt where item not in (select i.item from (]';
   L_intersect_built    BOOLEAN;
   L_intersects         VARCHAR2(32000) := NULL;

   L_supplier_sites_ind system_options.supplier_sites_ind%TYPE := NULL;

BEGIN

   select supplier_sites_ind
     into L_supplier_sites_ind
     from system_options;

   if BUILD_ITEM_INTERSECTS(O_error_message,
                            L_intersect_built,
                            L_intersects,
                            I_search_criteria) = FALSE then
      return FALSE;
   end if;

   if L_intersect_built then
      EXECUTE IMMEDIATE L_delete||LP_with_queries||L_intersects||') i )'
         using L_supplier_sites_ind,
               I_search_criteria.po_source_type_ind,
               I_search_criteria.asn_source_type_ind,
               I_search_criteria.tsf_source_type_ind,
               I_search_criteria.bol_source_type_ind,
               I_search_criteria.alloc_source_type_ind,
               I_search_criteria.wh_source_type_ind,
               I_search_criteria.dept,
               I_search_criteria.class,
               I_search_criteria.subclass,
               I_search_criteria.season,
               I_search_criteria.phase,
               I_search_criteria.start_date,
               I_search_criteria.end_date,
               I_search_criteria.items,
               I_search_criteria.item_lists,
               I_search_criteria.udas,
               I_search_criteria.uda_values,
               I_search_criteria.sups,
               I_search_criteria.sup_sites,
               I_search_criteria.pos,
               I_search_criteria.asns,
               I_search_criteria.tsfs,
               I_search_criteria.bols,
               I_search_criteria.appts,
               I_search_criteria.allocs;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DEL_TXNS_NOT_COVERD_BY_ITEMS;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOCS_FROM_ITEMS_WHS(O_error_message   IN OUT VARCHAR2,
                                      I_search_criteria IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_ITEM_LOCS_FROM_ITEMS_WHS';

   L_insert             VARCHAR2(200) := q'[insert into alc_itemsearch_items_gtt (item, location)]';
   L_intersect_built    BOOLEAN;
   L_intersects         VARCHAR2(32000) := NULL;

   L_supplier_sites_ind SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE := NULL;

BEGIN

   select supplier_sites_ind
     into L_supplier_sites_ind
     from system_options;

   if BUILD_ITEM_INTERSECTS(O_error_message,
                            L_intersect_built,
                            L_intersects,
                            I_search_criteria) = FALSE then
      return FALSE;
   end if;

   if L_intersect_built = FALSE then
      O_error_message:= ALC_CONSTANTS_SQL.ERRMSG_ONE_ITEM_CRITERIA_REQ;
      return FALSE;
   end if;

   EXECUTE IMMEDIATE L_insert||LP_with_queries||' select i.item, w.wh from ( '||L_intersects||' ) i, alc_itemsearch_whs_gtt w '
      using L_supplier_sites_ind,
            I_search_criteria.po_source_type_ind,
            I_search_criteria.asn_source_type_ind,
            I_search_criteria.tsf_source_type_ind,
            I_search_criteria.bol_source_type_ind,
            I_search_criteria.alloc_source_type_ind,
            I_search_criteria.wh_source_type_ind,
            I_search_criteria.dept,
            I_search_criteria.class,
            I_search_criteria.subclass,
            I_search_criteria.season,
            I_search_criteria.phase,
            I_search_criteria.start_date,
            I_search_criteria.end_date,
            I_search_criteria.items,
            I_search_criteria.item_lists,
            I_search_criteria.udas,
            I_search_criteria.uda_values,
            I_search_criteria.sups,
            I_search_criteria.sup_sites,
            I_search_criteria.pos,
            I_search_criteria.asns,
            I_search_criteria.tsfs,
            I_search_criteria.bols,
            I_search_criteria.appts,
            I_search_criteria.allocs;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_LOCS_FROM_ITEMS_WHS;
------------------------------------------------------------------------------------------------------
FUNCTION BUILD_ITEM_INTERSECTS(O_error_message     IN OUT VARCHAR2,
                               O_intersect_built      OUT BOOLEAN,
                               O_item_intersects      OUT VARCHAR2,
                               I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.BUILD_ITEM_INTERSECTS';

   L_intersects         VARCHAR2(32000) := NULL;
   L_cnt                NUMBER := 0;

BEGIN

   if (I_search_criteria.dept is NOT NULL and
       I_search_criteria.class is NOT NULL and
       I_search_criteria.subclass is NOT NULL) then

      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      --
      L_intersects := L_intersects ||LP_mh_query_sbc;
      L_cnt := L_cnt + 1;

   elsif (I_search_criteria.dept is NOT NULL and
          I_search_criteria.class is NOT NULL) then

      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      --
      L_intersects := L_intersects ||LP_mh_query_cls;
      L_cnt := L_cnt + 1;

   elsif (I_search_criteria.dept is NOT NULL) then

      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      ---
      L_intersects := L_intersects ||LP_mh_query_dept;
      L_cnt := L_cnt + 1;
   end if;

   if (I_search_criteria.items  is NOT NULL and
       I_search_criteria.items.COUNT  >= 1) then

      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      --
      L_intersects := L_intersects ||LP_items_query;
      L_cnt := L_cnt + 1;
   end if;

   if (I_search_criteria.season is NOT NULL) then

      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      ---
      L_intersects := L_intersects ||LP_season_phase_query;
      L_cnt := L_cnt + 1;
   end if;

   if (I_search_criteria.item_lists  is NOT NULL and
       I_search_criteria.item_lists.COUNT  >= 1) then

      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      --
      L_intersects := L_intersects ||LP_skulist_query;
      L_cnt := L_cnt + 1;
   end if;
   if (I_search_criteria.sups   is NOT NULL and
       I_search_criteria.sups.COUNT   >= 1) then

      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      --
      L_intersects := L_intersects ||LP_sup_query;
      L_cnt := L_cnt + 1;
   end if;

   if (I_search_criteria.sup_sites   is NOT NULL and
       I_search_criteria.sup_sites.COUNT   >= 1) then

      if (L_cnt > 0) then
         L_intersects := L_intersects ||'INTERSECT';
      end if;
      --
      L_intersects := L_intersects ||LP_sup_site_query;
      L_cnt := L_cnt + 1;
   end if;

   if (I_search_criteria.udas  is NOT NULL and
       I_search_criteria.udas.COUNT   >= 1) then

      if (I_search_criteria.uda_values  is NOT NULL and
          I_search_criteria.uda_values.COUNT   >= 1) then
         if (L_cnt > 0) then
            L_intersects := L_intersects ||'INTERSECT';
         end if;
         L_intersects := L_intersects ||LP_uda_value_query;
         L_cnt := L_cnt + 1;
      else
         if (L_cnt > 0) then
            L_intersects := L_intersects ||'INTERSECT';
         end if;
         L_intersects := L_intersects ||LP_uda_query;
         L_cnt := L_cnt + 1;
      end if;
   end if;

   if L_cnt = 0 then
      O_intersect_built := FALSE;
   else
      O_intersect_built := TRUE;
   end if;

   O_item_intersects := L_intersects;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_ITEM_INTERSECTS;
------------------------------------------------------------------------------------------------------
FUNCTION GET_PACKS_UNDER_STYLE_STAPLE(O_error_message   IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_PACKS_UNDER_STYLE_STAPLE';

BEGIN

   merge into alc_itemsearch_items_gtt target
   using (select i.location,
                 i.pack_no
            from item_master im_parent,
                 (select gtt.location,
                         pb2.pack_no,
                         COUNT(DISTINCT im.item_parent) parent_cnt,
                         MIN(DISTINCT im.item_parent) item_parent
                    from alc_itemsearch_items_gtt gtt,
                         packitem_breakout pb,
                         packitem_breakout pb2,
                         item_master im,
                         item_master im2
                   where pb.item          = gtt.item
                     and pb.pack_no       = pb2.pack_no
                     and pb2.item         = im.item
                     and pb.pack_no       = im2.item
                     and im2.sellable_ind = 'N'
                   group by gtt.location,
                            pb2.pack_no) i
           where i.parent_cnt                 = 1
             and i.item_parent                = im_parent.item
             and im_parent.item_aggregate_ind = 'Y'
           union all
          select gtt.location,
                 pb.pack_no
            from alc_itemsearch_items_gtt gtt,
                 packitem_breakout pb, 
                 item_master im
           where pb.item = gtt.item
             and pb.item = im.item
             and im.alc_item_type = ALC_CONSTANTS_SQL.STAPLEITEM) use_this
   on (    target.location = use_this.location
       and target.item     = use_this.pack_no)
   when NOT MATCHED then
      insert (item,
              location,
              rollup_type,
              rollup_item,
              rollup_item_desc)
      values (use_this.pack_no,
              use_this.location,
              NULL,
              NULL,
              NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PACKS_UNDER_STYLE_STAPLE;
------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_MULTI_PARENT_PACKS(O_error_message   IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.REMOVE_MULTI_PARENT_PACKS';

BEGIN

   delete from alc_itemsearch_items_gtt
    where (item, location) IN (select i.item, i.location
                                 from (select gtt.item,
                                              gtt.location,
                                              pack.sellable_ind,
                                              COUNT(DISTINCT NVL(parent.item,'-999')) parent_cnt,
                                              MAX(NVL(parent.item_aggregate_ind,'N')) agg_ind
                                         from alc_itemsearch_items_gtt gtt,
                                              packitem_breakout pb,
                                              item_master pack,
                                              item_master comp,
                                              item_master parent
                                        where gtt.item          = pb.pack_no
                                          and pb.pack_no        = pack.item
                                          and pb.item           = comp.item
                                          and comp.item_parent  = parent.item(+)
                                        group by gtt.item,
                                                 gtt.location,
                                                 pack.sellable_ind) i
                                where i.sellable_ind = 'N'
                                  and i.agg_ind      = 'Y'
                                  and i.parent_cnt   > 1 );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END REMOVE_MULTI_PARENT_PACKS;
------------------------------------------------------------------------------------------------------
FUNCTION MARK_ROLLUP_GROUP(O_error_message   IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.MARK_ROLLUP_GROUP';

   L_intersects         VARCHAR2(32000) := NULL;
   L_cnt                NUMBER := 0;

BEGIN

   --item/packs to style
   merge into alc_itemsearch_items_gtt target
   using (--fashion sku rollup to style
          select 'STYLE' rollup_type,
                 im.item_parent rollup_item,
                 imp.item_desc rollup_item_desc,
                 gtt.item,
                 gtt.location,
                 NULL multi_color_pack_ind
            from alc_itemsearch_items_gtt gtt,
                 item_master im,
                 item_master imp
           where im.item                = gtt.item
             and im.item_parent         is NOT NULL --this is never true for a pack
             and im.item_parent         = imp.item
             and imp.item_aggregate_ind = 'Y'
          union all
          --packs that can rollup to style
          select i.rollup_type,
                 i.rollup_item,
                 i.rollup_item_desc,
                 i.item,
                 i.location,
                 case
                    when i.da1 = 'Y' then
                       DECODE(i.cnt_1_diffs,
                              1, 'N',
                              'Y')
                    when i.da2 = 'Y' then
                       DECODE(i.cnt_2_diffs,
                              1, 'N',
                              'Y')
                    when i.da3 = 'Y' then
                       DECODE(i.cnt_3_diffs,
                              1, 'N',
                              'Y')
                    when i.da4 = 'Y' then
                       DECODE(i.cnt_4_diffs,
                              1, 'N',
                              'Y')
                 end as multi_color_pack_ind
            from (select 'STYLE' rollup_type,
                         MIN(imp.item) rollup_item,
                         MIN(imp.item_desc) rollup_item_desc,
                         gtt.item,
                         gtt.location,
                         MIN(imp.diff_1_aggregate_ind) da1,
                         MIN(imp.diff_2_aggregate_ind) da2,
                         MIN(imp.diff_3_aggregate_ind) da3,
                         MIN(imp.diff_4_aggregate_ind) da4,
                         COUNT(DISTINCT im.item_parent) cnt,
                         COUNT(DISTINCT DECODE(imp.diff_1_aggregate_ind,
                                               'Y', im.diff_1,
                                               'Nu|L')) cnt_1_diffs,
                         COUNT(DISTINCT DECODE(imp.diff_2_aggregate_ind,
                                               'Y', im.diff_2,
                                               'Nu|L')) cnt_2_diffs,
                         COUNT(DISTINCT DECODE(imp.diff_3_aggregate_ind,
                                               'Y', im.diff_3,
                                               'Nu|L')) cnt_3_diffs,
                         COUNT(DISTINCT DECODE(imp.diff_4_aggregate_ind,
                                               'Y', im.diff_4,
                                               'Nu|L')) cnt_4_diffs
                    from alc_itemsearch_items_gtt gtt,
                         packitem_breakout pb,
                         item_master im,
                         item_master imp,
                         item_master impack
                   where gtt.item               = pb.pack_no
                     and impack.item            = pb.pack_no
                     and pb.item                = im.item
                     and im.item_parent         = imp.item
                     and imp.item_aggregate_ind = 'Y'
                     and impack.sellable_ind    = 'N'
                group by gtt.item,
                         gtt.location) i
           where i.cnt       = 1) use_this
   on (    target.item     = use_this.item
       and target.location = use_this.location)
   when MATCHED then
      update
         set target.rollup_type          = use_this.rollup_type,
             target.rollup_item          = use_this.rollup_item,
             target.rollup_item_desc     = use_this.rollup_item_desc,
             target.multi_color_pack_ind = use_this.multi_color_pack_ind;

   --

   merge into alc_itemsearch_items_gtt target
   using (--staple or pack
          select DISTINCT DECODE(im.pack_ind,
                                 'N', 'STAPLE',
                                 DECODE(im.sellable_ind,
                                        'Y', 'SELLPACK',
                                        'NONSELLPACK')) rollup_type,
                 gtt.item rollup_item,
                 im.item_desc rollup_item_desc,
                 gtt.item,
                 gtt.location
            from alc_itemsearch_items_gtt gtt,
                 item_master im
           where im.item        = gtt.item
             and gtt.rollup_type is NULL) use_this
   on (    target.item     = use_this.item
       and target.location = use_this.location)
   when MATCHED then
      update
         set target.rollup_type      = use_this.rollup_type,
             target.rollup_item      = use_this.rollup_item,
             target.rollup_item_desc = use_this.rollup_item_desc;

   --delete non-sellable packs that have both staple and fashion components
   delete from alc_itemsearch_items_gtt gtt
     where gtt.rollup_item IN (select i.rollup_item
                                 from (select gtt.rollup_item,
                                              MIN(DECODE(im.item_parent,
                                                         NULL, 1,
                                                         0)) staple_exist,
                                              MIN(DECODE(im.item_parent,
                                                         NULL, 0,
                                                         1)) fashion_exist
                                         from alc_itemsearch_items_gtt gtt,
                                              packitem_breakout pb,
                                              item_master im
                                        where gtt.rollup_type = 'NONSELLPACK'
                                          and gtt.rollup_item = pb.pack_no
                                          and im.item         = pb.item
                                        group by gtt.rollup_item) i
                                where i.staple_exist  = 1
                                  and i.fashion_exist = 1);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END MARK_ROLLUP_GROUP;
------------------------------------------------------------------------------------------------------
FUNCTION FIND_MAXROWS_WITH_INVENTORY(O_error_message     IN OUT VARCHAR2,
                                     O_max_rows_exceeded IN OUT NUMBER,
                                     I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.FIND_MAXROWS_WITH_INVENTORY';
   L_current_row_count  NUMBER(6)    := 0;

BEGIN

   if I_search_criteria.wh_source_type_ind = 1 and O_max_rows_exceeded = 0 then
      if GET_WH_SOURCE_INV(O_error_message,
                           L_current_row_count,
                           O_max_rows_exceeded) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   if I_search_criteria.po_source_type_ind = 1 and O_max_rows_exceeded = 0 then
      if GET_PO_SOURCE_INV(O_error_message,
                           L_current_row_count,
                           O_max_rows_exceeded,
                           I_search_criteria) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   if I_search_criteria.asn_source_type_ind = 1 and O_max_rows_exceeded = 0 then
      if GET_ASN_SOURCE_INV(O_error_message,
                            L_current_row_count,
                            O_max_rows_exceeded,
                            I_search_criteria) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   if I_search_criteria.tsf_source_type_ind = 1 and O_max_rows_exceeded = 0 then
      if GET_TSF_SOURCE_INV(O_error_message,
                            L_current_row_count,
                            O_max_rows_exceeded,
                            I_search_criteria) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   if I_search_criteria.bol_source_type_ind = 1 and O_max_rows_exceeded = 0 then
      if GET_BOL_SOURCE_INV(O_error_message,
                            L_current_row_count,
                            O_max_rows_exceeded,
                            I_search_criteria) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   if I_search_criteria.alloc_source_type_ind = 1 and O_max_rows_exceeded = 0 then
      if GET_ALLOC_SOURCE_INV(O_error_message,
                              L_current_row_count,
                              O_max_rows_exceeded,
                              I_search_criteria) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FIND_MAXROWS_WITH_INVENTORY;
------------------------------------------------------------------------------------------------------
FUNCTION GET_WH_SOURCE_INV(O_error_message        IN OUT VARCHAR2,
                           IO_current_row_count   IN OUT NUMBER,
                           O_max_rows_exceeded    IN OUT NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_WH_SOURCE_INV';

   cursor C_WHS is
      select w.wh
        from alc_itemsearch_whs_gtt w
       order by w.wh;

BEGIN

   for rec in C_WHS loop

      insert into alc_itemsearch_items_res_gtt (rollup_type,
                                                rollup_item,
                                                rollup_item_desc)
                                         select i.rollup_type,
                                                i.rollup_item,
                                                i.rollup_item_desc
                                           from (select gtt.rollup_type,
                                                        gtt.rollup_item,
                                                        gtt.rollup_item_desc,
                                                        --current inventory
                                                        SUM((GREATEST((ils.stock_on_hand),0)) -
                                                              (GREATEST(ils.tsf_reserved_qty, 0) +
                                                               GREATEST(ils.rtv_qty, 0) +
                                                               GREATEST(ils.non_sellable_qty, 0) +
                                                               GREATEST(ils.customer_resv, 0))
                                                        ) stock_on_hand
                                                   from alc_itemsearch_items_gtt gtt,
                                                        item_loc_soh ils
                                                  where gtt.item     = ils.item
                                                    and gtt.location = ils.loc
                                                    and gtt.location = rec.wh
                                                  group by gtt.rollup_type,
                                                           gtt.rollup_item,
                                                           gtt.rollup_item_desc) i
                                          where i.stock_on_hand > 0
                                            and rownum <= (LP_max_results - IO_current_row_count);

      IO_current_row_count := IO_current_row_count + SQL%ROWCOUNT;

      if IO_current_row_count = LP_max_results then
         O_max_rows_exceeded := 1;
         return TRUE;
      else
         delete
           from alc_itemsearch_items_gtt
          where rollup_item IN (select rollup_item
                                  from alc_itemsearch_items_res_gtt);
      end if;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_WH_SOURCE_INV;
------------------------------------------------------------------------------------------------------
FUNCTION GET_PO_SOURCE_INV(O_error_message      IN OUT VARCHAR2,
                           IO_current_row_count IN OUT NUMBER,
                           O_max_rows_exceeded  IN OUT NUMBER,
                           I_search_criteria    IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_PO_SOURCE_INV';
   L_specific_ids VARCHAR2(1)  := 'N';
   L_vdate        DATE         := GET_VDATE;

BEGIN

   if (I_search_criteria.pos is NOT NULL and I_search_criteria.pos.COUNT >= 1) then

      L_specific_ids := 'Y';

      delete from gtt_num_num_str_str_date_date;

      insert into gtt_num_num_str_str_date_date (number_1)
      select value(input_pos)
        from table(cast(I_search_criteria.pos as OBJ_NUMERIC_ID_TABLE)) input_pos;

   end if;

   insert into alc_itemsearch_items_res_gtt (rollup_type,
                                             rollup_item,
                                             rollup_item_desc)
                                      select i.rollup_type,
                                             i.rollup_item,
                                             i.rollup_item_desc
                                        from (select gtt.rollup_type,
                                                     gtt.rollup_item,
                                                     gtt.rollup_item_desc,
                                                     SUM(ol.qty_ordered - NVL(ol.qty_received,0) -
                                                     --already allocated through PO / ASN
                                                     NVL((select sum(ald.qty_allocated)
                                                            from alloc_header alh,
                                                                 alloc_detail ald
														   where (alh.origin_ind!='ALC' or 
																 (alh.origin_ind='ALC' and alh.alloc_no in (select distinct xref_alloc_no from alc_xref)))
                                                             and ald.alloc_no = alh.alloc_no
                                                             and alh.item     = gtt.item
                                                             and alh.order_no = oh.order_no
                                                             and alh.wh       = gtt.location
                                                           group by alh.item,
                                                                    alh.order_no),0)) on_order
                                                from alc_itemsearch_items_gtt gtt,
                                                     ordloc ol,
                                                     ordhead oh
                                               where gtt.item       = ol.item
                                                 and gtt.location   = ol.location
                                                 and ol.order_no    = oh.order_no
                                                 and oh.status      = 'A'
                                                 and oh.order_type != 'ARB'
                                                 and ((oh.orig_ind != 0)
                                                       or
                                                      (oh.orig_ind = 0 and
                                                       oh.contract_no is NOT NULL))
                                                 and (L_specific_ids = 'N' or
                                                      (L_specific_ids = 'Y' and
                                                       oh.order_no in(select number_1 from gtt_num_num_str_str_date_date)))
                                                 --
                                                 and NVL(oh.not_before_date, NVL(I_search_criteria.start_date, L_vdate)) >= NVL(I_search_criteria.start_date, NVL(oh.not_before_date, L_vdate))
                                                 and NVL(oh.not_before_date, NVL(I_search_criteria.end_date, L_vdate))  <=  NVL(I_search_criteria.end_date, NVL(oh.not_before_date, L_vdate))
                                               group by gtt.rollup_type,
                                                        gtt.rollup_item,
                                                        gtt.rollup_item_desc) i
                                       where i.on_order > 0
                                         and rownum <= (LP_max_results - IO_current_row_count);

   IO_current_row_count := IO_current_row_count + SQL%ROWCOUNT;

   if IO_current_row_count = LP_max_results then
      O_max_rows_exceeded := 1;
   else
      delete
        from alc_itemsearch_items_gtt
       where rollup_item IN (select rollup_item
                               from alc_itemsearch_items_res_gtt);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PO_SOURCE_INV;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ASN_SOURCE_INV(O_error_message        IN OUT VARCHAR2,
                            IO_current_row_count   IN OUT NUMBER,
                            O_max_rows_exceeded    IN OUT NUMBER,
                            I_search_criteria      IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_ASN_SOURCE_INV';
   L_specific_ids VARCHAR2(1)  := 'N';
   L_vdate        DATE         := GET_VDATE;

BEGIN

   if (I_search_criteria.asns is NOT NULL and I_search_criteria.asns.COUNT >= 1) then

      L_specific_ids := 'Y';

      delete
        from gtt_num_num_str_str_date_date;

      insert into gtt_num_num_str_str_date_date (varchar2_1)
                                         select DISTINCT value(input_asns)
                                           from table(cast(I_search_criteria.asns as OBJ_VARCHAR_ID_TABLE)) input_asns;

   end if;

   insert into alc_itemsearch_items_res_gtt (rollup_type,
                                             rollup_item,
                                             rollup_item_desc)
                                      select i.rollup_type,
                                             i.rollup_item,
                                             i.rollup_item_desc
                                        from (select distinct gtt.rollup_type,
                                                     gtt.rollup_item,
                                                     gtt.rollup_item_desc,
                                                     SUM(ss.qty_expected - NVL(ss.qty_received,0) -
                                                     --already allocated through ASN
                                                     NVL((select SUM(ald.qty_allocated)
                                                            from alloc_header alh,
                                                                 alloc_detail ald,
                                                                 alc_xref alx
                                                           where alh.alloc_no    = alx.xref_alloc_no
                                                             and ald.alloc_no    = alh.alloc_no
                                                             and alh.item        = gtt.item
                                                             and alh.doc_type    = 'ASN'
                                                             and alh.doc         = sh.asn
                                                             and alh.wh          = gtt.location
                                                           group by alh.item,
                                                                    alh.doc),0) -
                                                     --already allocated through PO
                                                     NVL((select SUM(ald.qty_allocated)
                                                            from alloc_header alh,
                                                                 alloc_detail ald,
                                                                 alc_xref alx
                                                           where alh.alloc_no    = alx.xref_alloc_no
                                                             and ald.alloc_no    = alh.alloc_no
                                                             and alh.item        = gtt.item
                                                             and alh.doc_type    = 'PO'
                                                             and alh.order_no    = oh.order_no
                                                             and alh.wh          = gtt.location
                                                           group by alh.item,
                                                                    alh.order_no),0)) ship_qty
                                                from alc_itemsearch_items_gtt gtt,
                                                     shipment sh,
                                                     shipsku ss,
                                                     wh w,
                                                     ordhead oh
                                               where gtt.item       = ss.item
                                                 and gtt.location   = w.wh
                                                 and w.physical_wh  = sh.to_loc
                                                 and sh.shipment    = ss.shipment
                                                 --
                                                 and sh.asn         is NOT NULL
                                                 and sh.status_code = 'I'
                                                 --
                                                 and (L_specific_ids  = 'N' or
                                                      (L_specific_ids = 'Y' and
                                                       sh.asn IN (select varchar2_1
                                                                    from gtt_num_num_str_str_date_date)))
                                                 --
                                                 and NVL(sh.est_arr_date, NVL(I_search_criteria.start_date, L_vdate)) >= NVL(I_search_criteria.start_date, NVL(sh.est_arr_date, L_vdate))
                                                 and NVL(sh.est_arr_date, NVL(I_search_criteria.end_date, L_vdate))  <=  NVL(I_search_criteria.end_date, NVL(sh.est_arr_date, L_vdate))
                                                 --
                                                 and sh.order_no       = oh.order_no
                                                 and oh.status         = 'A'
                                                 and oh.order_type    != 'ARB'
                                                 and ((oh.orig_ind != 0) or (oh.orig_ind = 0 and oh.contract_no IS NOT NULL))
                                               group by gtt.rollup_type,
                                                        gtt.rollup_item,
                                                        gtt.rollup_item_desc) i
                                       where i.ship_qty > 0
                                         and rownum <= (LP_max_results - IO_current_row_count);

   IO_current_row_count := IO_current_row_count + SQL%ROWCOUNT;

   if IO_current_row_count = LP_max_results then
      O_max_rows_exceeded := 1;
   else
      delete
        from alc_itemsearch_items_gtt
       where rollup_item IN (select rollup_item
                               from alc_itemsearch_items_res_gtt);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ASN_SOURCE_INV;
------------------------------------------------------------------------------------------------------
FUNCTION GET_TSF_SOURCE_INV(O_error_message        IN OUT VARCHAR2,
                            IO_current_row_count   IN OUT NUMBER,
                            O_max_rows_exceeded    IN OUT NUMBER,
                            I_search_criteria      IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_TSF_SOURCE_INV';
   L_specific_ids VARCHAR2(1)  := 'N';
   L_vdate        DATE         := GET_VDATE;

BEGIN

   if (I_search_criteria.tsfs is NOT NULL and I_search_criteria.tsfs.COUNT >= 1) then

      L_specific_ids := 'Y';

      delete
        from gtt_num_num_str_str_date_date;

      insert into gtt_num_num_str_str_date_date (number_1)
                                         select value(input_tsfs)
                                           from table(cast(I_search_criteria.tsfs as OBJ_NUMERIC_ID_TABLE)) input_tsfs;

   end if;

   insert into alc_itemsearch_items_res_gtt (rollup_type,
                                             rollup_item,
                                             rollup_item_desc)
                                      select i.rollup_type,
                                             i.rollup_item,
                                             i.rollup_item_desc
                                        from (select gtt.rollup_type,
                                                     gtt.rollup_item,
                                                     gtt.rollup_item_desc,
                                                     SUM(td.tsf_qty - NVL(td.ship_qty,0) -
                                                     NVL((select SUM(ald.qty_allocated)
                                                            from alloc_header alh,
                                                                 alloc_detail ald
														   where (alh.origin_ind!='ALC' or 
																 (alh.origin_ind='ALC' and alh.alloc_no in (select distinct xref_alloc_no from alc_xref)))
                                                             and ald.alloc_no    = alh.alloc_no
                                                             and alh.item        = gtt.item
                                                             and alh.order_no    = th.tsf_no
                                                             and alh.wh          = gtt.location
                                                           group by alh.item,
                                                                    alh.order_no),0)) tsf_qty
                                                from alc_itemsearch_items_gtt gtt,
                                                     tsfhead th,
                                                     tsfdetail td
                                               where gtt.item     = td.item
                                                 and gtt.location = th.to_loc
                                                 and td.tsf_no    = th.tsf_no
                                                 --
                                                 and th.status = 'A'
                                                 and th.tsf_type in ('SR','CF','AD','MR','EG','AIP','IC')
                                                 --
                                                 and (   L_specific_ids = 'N'
                                                      or (    L_specific_ids = 'Y'
                                                          and th.tsf_no IN (select number_1
                                                                              from gtt_num_num_str_str_date_date)))
                                                 --
                                                 and NVL(th.delivery_date, NVL(I_search_criteria.start_date, L_vdate)) >= NVL(I_search_criteria.start_date, NVL(th.delivery_date, L_vdate))
                                                 and NVL(th.delivery_date, NVL(I_search_criteria.end_date, L_vdate))  <=  NVL(I_search_criteria.end_date, NVL(th.delivery_date, L_vdate))
                                               group by gtt.rollup_type,
                                                        gtt.rollup_item,
                                                        gtt.rollup_item_desc) i
                                       where i.tsf_qty > 0
                                         and rownum <= (LP_max_results - IO_current_row_count);

   IO_current_row_count := IO_current_row_count + SQL%ROWCOUNT;

   if IO_current_row_count = LP_max_results then
      O_max_rows_exceeded := 1;
   else
      delete
        from alc_itemsearch_items_gtt
       where rollup_item IN (select rollup_item
                               from alc_itemsearch_items_res_gtt);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_TSF_SOURCE_INV;
------------------------------------------------------------------------------------------------------
FUNCTION GET_BOL_SOURCE_INV(O_error_message      IN OUT VARCHAR2,
                            IO_current_row_count IN OUT NUMBER,
                            O_max_rows_exceeded  IN OUT NUMBER,
                            I_search_criteria    IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_BOL_SOURCE_INV';
   L_specific_ids VARCHAR2(1)  := 'N';
   L_vdate        DATE         := GET_VDATE;

BEGIN

   if (I_search_criteria.bols is NOT NULL and I_search_criteria.bols.COUNT >= 1) then

      L_specific_ids := 'Y';

      delete
        from gtt_num_num_str_str_date_date;

      insert into gtt_num_num_str_str_date_date (varchar2_1)
                                         select DISTINCT value(input_bols)
                                           from table(cast(I_search_criteria.bols as OBJ_VARCHAR_ID_TABLE)) input_bols;

   end if;

   insert into alc_itemsearch_items_res_gtt (rollup_type,
                                             rollup_item,
                                             rollup_item_desc)
                                      select i.rollup_type,
                                             i.rollup_item,
                                             i.rollup_item_desc
                                        from (select gtt.rollup_type,
                                                     gtt.rollup_item,
                                                     gtt.rollup_item_desc,
                                                     SUM(ss.qty_expected - NVL(ss.qty_received,0) -
                                                     NVL((select SUM(ald.qty_allocated)
                                                            from alloc_header alh,
                                                                 alloc_detail ald,
                                                                 alc_xref alx
                                                           where alh.alloc_no    = alx.xref_alloc_no
                                                             and ald.alloc_no    = alh.alloc_no
                                                             and alh.item        = gtt.item
                                                             and alh.doc_type    = 'BOL'
                                                             and alh.doc         = sh.bol_no
                                                             and alh.wh          = gtt.location
                                                           group by alh.item,
                                                                    alh.doc),0)) ship_qty
                                                from alc_itemsearch_items_gtt gtt,
                                                     shipment sh,
                                                     shipsku ss,
                                                     wh w
                                               where gtt.item      = ss.item
                                                 and gtt.location  = w.wh
                                                 and w.physical_wh = sh.to_loc
                                                 and sh.shipment   = ss.shipment
                                                 --
                                                 and sh.bol_no     is NOT NULL
                                                 and sh.status_code = 'I'
                                                 and (   L_specific_ids = 'N'
                                                      or (    L_specific_ids = 'Y'
                                                          and sh.bol_no IN (select varchar2_1
                                                                              from gtt_num_num_str_str_date_date)))
                                                 --
                                                 and NVL(sh.est_arr_date, NVL(I_search_criteria.start_date, L_vdate)) >= NVL(I_search_criteria.start_date, NVL(sh.est_arr_date, L_vdate))
                                                 and NVL(sh.est_arr_date, NVL(I_search_criteria.end_date, L_vdate))  <=  NVL(I_search_criteria.end_date, NVL(sh.est_arr_date, L_vdate))
                                               group by gtt.rollup_type,
                                                        gtt.rollup_item,
                                                        gtt.rollup_item_desc) i
                                       where i.ship_qty > 0
                                         and rownum <= (LP_max_results - IO_current_row_count);

   IO_current_row_count := IO_current_row_count + SQL%ROWCOUNT;

   if IO_current_row_count = LP_max_results then
      O_max_rows_exceeded := 1;
   else
      delete
        from alc_itemsearch_items_gtt
       where rollup_item IN (select rollup_item
                               from alc_itemsearch_items_res_gtt);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_BOL_SOURCE_INV;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ALLOC_SOURCE_INV(O_error_message        IN OUT VARCHAR2,
                              IO_current_row_count   IN OUT NUMBER,
                              O_max_rows_exceeded    IN OUT NUMBER,
                              I_search_criteria      IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_ALLOC_SOURCE_INV';
   L_specific_ids VARCHAR2(1)  := 'N';
   L_vdate        DATE         := GET_VDATE;

BEGIN

   if (I_search_criteria.allocs is NOT NULL and I_search_criteria.allocs.COUNT >= 1) then

      L_specific_ids := 'Y';

      delete
        from gtt_num_num_str_str_date_date;

      insert into gtt_num_num_str_str_date_date (number_1)
                                         select value(input_allocs)
                                           from table(cast(I_search_criteria.allocs as OBJ_NUMERIC_ID_TABLE)) input_allocs;

   end if;

   insert into alc_itemsearch_items_res_gtt (rollup_type,
                                             rollup_item,
                                             rollup_item_desc)
                                      select i.rollup_type,
                                             i.rollup_item,
                                             i.rollup_item_desc
                                        from (select gtt.rollup_type,
                                                     gtt.rollup_item,
                                                     gtt.rollup_item_desc,
                                                     SUM(ad.qty_allocated - NVL(ad.qty_transferred,0) -
                                                     NVL((select SUM(ald.qty_allocated)
                                                            from alloc_header alh,
                                                                 alloc_detail ald,
                                                                 alc_xref alx
                                                           where alh.alloc_no    = alx.xref_alloc_no
                                                             and ald.alloc_no    = alh.alloc_no
                                                             and alh.item        = gtt.item
                                                             and alh.order_no    = ah.alloc_no
                                                             and alh.wh          = gtt.location
                                                           group by alh.item,
                                                                    alh.order_no),0)) tsf_qty
                                                from alc_itemsearch_items_gtt gtt,
                                                     alloc_header ah,
                                                     alloc_detail ad,
                                                     --can only be allocations sourced from ALC
                                                     alc_xref x
                                               where gtt.item     = ah.item
                                                 and gtt.location = ad.to_loc
                                                 and ad.alloc_no  = ah.alloc_no
                                                 and ah.alloc_no  = x.xref_alloc_no
                                                 --
                                                 and ah.status = 'A'
                                                 and (   L_specific_ids = 'N'
                                                      or (    L_specific_ids = 'Y'
                                                          and ah.alloc_no IN (select number_1
                                                                                from gtt_num_num_str_str_date_date)))
                                                 --
                                                 and NVL(ad.in_store_date, NVL(I_search_criteria.start_date, L_vdate)) >= NVL(I_search_criteria.start_date, NVL(ad.in_store_date, L_vdate))
                                                 and NVL(ad.in_store_date, NVL(I_search_criteria.end_date, L_vdate))  <=  NVL(I_search_criteria.end_date, NVL(ad.in_store_date, L_vdate))
                                               group by gtt.rollup_type,
                                                        gtt.rollup_item,
                                                        gtt.rollup_item_desc) i
                                       where i.tsf_qty > 0
                                         and rownum <= (LP_max_results - IO_current_row_count);

   IO_current_row_count := IO_current_row_count + SQL%ROWCOUNT;

   if IO_current_row_count = LP_max_results then
      O_max_rows_exceeded := 1;
   else
      delete
        from alc_itemsearch_items_gtt
       where rollup_item IN (select rollup_item
                               from alc_itemsearch_items_res_gtt);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ALLOC_SOURCE_INV;
------------------------------------------------------------------------------------------------------
FUNCTION GET_INVENTORY_FOR_SELECTION(O_error_message     IN OUT VARCHAR2,
                                     I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_INVENTORY_FOR_SELECTION';

BEGIN

   delete
     from alc_itemsearch_inv_gtt;

   if (I_search_criteria.wh_source_type_ind    = 1 or
       I_search_criteria.po_source_type_ind    = 1 or
       I_search_criteria.tsf_source_type_ind   = 1 or
       I_search_criteria.bol_source_type_ind   = 1 or
       I_search_criteria.alloc_source_type_ind = 1) then

      if GET_SOURCE_INV_SELECTION(O_error_message,
                                  I_search_criteria) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   if I_search_criteria.asn_source_type_ind = 1 then
      if GET_ASN_SOURCE_FOR_SELECTION(O_error_message,
                                      I_search_criteria) = FALSE then
         return FALSE;
      end if;
   end if;
   --

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_INVENTORY_FOR_SELECTION;
------------------------------------------------------------------------------------------------------
FUNCTION GET_SOURCE_INV_SELECTION(O_error_message     IN OUT VARCHAR2,
                                  I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_SOURCE_INVENTORY_SELECTION';
   L_wh_source_type     VARCHAR2(1)  := 'N';
   L_po_source_type     VARCHAR2(1)  := 'N';
   L_tsf_source_type    VARCHAR2(1)  := 'N';
   L_bol_source_type    VARCHAR2(1)  := 'N';
   L_alloc_source_type  VARCHAR2(1)  := 'N';
   L_po_specific_ids    VARCHAR2(1)  := 'N';
   L_tsf_specific_ids   VARCHAR2(1)  := 'N';
   L_bol_specific_ids   VARCHAR2(1)  := 'N';
   L_alloc_specific_ids VARCHAR2(1)  := 'N';
   L_vdate              DATE         := GET_VDATE;

BEGIN

   delete
     from gtt_num_num_str_str_date_date;

   if I_search_criteria.wh_source_type_ind = 1 then
      L_wh_source_type := 'Y';
   end if;

   if I_search_criteria.po_source_type_ind = 1 then
      L_po_source_type := 'Y';
      --
      if (I_search_criteria.pos is NOT NULL and I_search_criteria.pos.COUNT >= 1) then
         L_po_specific_ids := 'Y';
         --
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_2)
                                            select value(input_pos),
                                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_PO
                                              from table(cast(I_search_criteria.pos as OBJ_NUMERIC_ID_TABLE)) input_pos;
      end if;
   end if;

   if I_search_criteria.tsf_source_type_ind = 1 then
      L_tsf_source_type := 'Y';
      --
      if (I_search_criteria.tsfs is NOT NULL and I_search_criteria.tsfs.COUNT >= 1) then
         L_tsf_specific_ids := 'Y';
         --
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_2)
                                            select value(input_tsfs),
                                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_TSF
                                              from table(cast(I_search_criteria.tsfs as OBJ_NUMERIC_ID_TABLE)) input_tsfs;
      end if;
   end if;

   if I_search_criteria.bol_source_type_ind = 1 then
      L_bol_source_type := 'Y';
      --
      if (I_search_criteria.bols is NOT NULL and I_search_criteria.bols.COUNT >= 1) then
         L_bol_specific_ids := 'Y';
         ---
         insert into gtt_num_num_str_str_date_date(varchar2_1,
                                                   varchar2_2)
                                            select DISTINCT value(input_bols),
                                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_BOL
                                              from table(cast(I_search_criteria.bols as OBJ_VARCHAR_ID_TABLE)) input_bols;
      end if;
   end if;

   if I_search_criteria.alloc_source_type_ind = 1 then
      L_alloc_source_type := 'Y';
      --
      if (I_search_criteria.allocs is NOT NULL and I_search_criteria.allocs.COUNT >= 1) then
         L_alloc_specific_ids := 'Y';
         --
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   varchar2_2)
                                            select value(input_allocs),
                                                   ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ALLOC
                                              from table(cast(I_search_criteria.allocs as OBJ_NUMERIC_ID_TABLE)) input_allocs;
      end if;
      ---
   end if;

   insert into alc_itemsearch_inv_gtt (item,
                                       diff_1,
                                       diff_2,
                                       diff_3,
                                       diff_4,
                                       item_desc,
                                       rollup_type,
                                       rollup_item,
                                       rollup_item_desc,
                                       multi_color_pack_ind,
                                       location,
                                       doc_no,
                                       source_type,
                                       avail_qty,
                                       backorder_qty)
                 with search_items as (select gtt.item,
                                              im.diff_1,
                                              im.diff_2,
                                              im.diff_3,
                                              im.diff_4,
                                              im.item_desc,
                                              gtt.rollup_type,
                                              gtt.rollup_item,
                                              gtt.rollup_item_desc,
                                              gtt.multi_color_pack_ind,
                                              gtt.location
                                         from alc_itemsearch_items_gtt gtt,
                                              item_master im
                                        where gtt.item = im.item)
                               ---
                               select inner.item,
                                      inner.diff_1,
                                      inner.diff_2,
                                      inner.diff_3,
                                      inner.diff_4,
                                      inner.item_desc,
                                      inner.rollup_type,
                                      inner.rollup_item,
                                      inner.rollup_item_desc,
                                      inner.multi_color_pack_ind,
                                      inner.location,
                                      inner.doc_no,
                                      inner.source_type,
                                      inner.avail_qty,
                                      inner.backorder_qty
                                 from (----------------------
                                       -- SOURCE_TYPE = WH
                                       ----------------------
                                       select s.item,
                                              s.diff_1,
                                              s.diff_2,
                                              s.diff_3,
                                              s.diff_4,
                                              s.item_desc,
                                              s.rollup_type,
                                              s.rollup_item,
                                              s.rollup_item_desc,
                                              s.multi_color_pack_ind,
                                              s.location,
                                              NULL doc_no,
                                              ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_OH source_type,
                                              (GREATEST((ils.stock_on_hand),0)) - (GREATEST(ils.tsf_reserved_qty, 0) +
                                                                                   GREATEST(ils.rtv_qty, 0) +
                                                                                   GREATEST(ils.non_sellable_qty, 0) +
                                                                                   GREATEST(ils.customer_resv, 0)) avail_qty,
                                              ils.customer_backorder backorder_qty
                                         from search_items s,
                                              item_loc_soh ils,
                                              alc_itemsearch_whs_gtt w
                                        where s.item     = ils.item
                                          and s.location = ils.loc
                                          and s.location = w.wh
                                          and L_wh_source_type = 'Y'
                                        union all
                                        ----------------------
                                        -- SOURCE_TYPE = PO
                                        ----------------------
                                        select s.item,
                                               s.diff_1,
                                               s.diff_2,
                                               s.diff_3,
                                               s.diff_4,
                                               s.item_desc,
                                               s.rollup_type,
                                               s.rollup_item,
                                               s.rollup_item_desc,
                                               s.multi_color_pack_ind,
                                               s.location,
                                               TO_CHAR(oh.order_no) doc_no,
                                               ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_PO source_type,
                                               ol.qty_ordered - NVL(ol.qty_received,0) - NVL((select SUM(ald.qty_allocated)
                                                                                                from alloc_header alh,
                                                                                                     alloc_detail ald
																							   where (alh.origin_ind!='ALC' or 
																									 (alh.origin_ind='ALC' and alh.alloc_no in (select distinct xref_alloc_no from alc_xref)))
                                                                                                 and ald.alloc_no    = alh.alloc_no
                                                                                                 and alh.item        = s.item
                                                                                                 and alh.order_no    = oh.order_no
                                                                                                 and alh.wh          = s.location
                                                                                               group by alh.item,
                                                                                                        alh.order_no),0) avail_qty,
                                               0 backorder_qty
                                          from search_items s,
                                               ordloc ol,
                                               ordhead oh
                                         where s.item         = ol.item
                                           and s.location     = ol.location
                                           and ol.order_no    = oh.order_no
                                           and oh.status      = 'A'
                                           and oh.order_type != 'ARB'
                                           and ((oh.orig_ind != 0)
                                                or (    oh.orig_ind = 0
                                                    and oh.contract_no is NOT NULL))
                                           and (   L_po_specific_ids = 'N'
                                                or (    L_po_specific_ids = 'Y'
                                                    and oh.order_no IN (select number_1
                                                                          from gtt_num_num_str_str_date_date
                                                                         where varchar2_2 = ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_PO)))
                                           and NVL(oh.not_before_date, NVL(I_search_criteria.start_date, L_vdate)) >= NVL(I_search_criteria.start_date, NVL(oh.not_before_date, L_vdate))
                                           and NVL(oh.not_before_date, NVL(I_search_criteria.end_date, L_vdate))   <= NVL(I_search_criteria.end_date, NVL(oh.not_before_date, L_vdate))
                                           and L_po_source_type = 'Y'
                                         union all
                                        ----------------------
                                        -- SOURCE_TYPE = TSF
                                        ----------------------
                                        select s.item,
                                               s.diff_1,
                                               s.diff_2,
                                               s.diff_3,
                                               s.diff_4,
                                               s.item_desc,
                                               s.rollup_type,
                                               s.rollup_item,
                                               s.rollup_item_desc,
                                               s.multi_color_pack_ind,
                                               s.location,
                                               TO_CHAR(th.tsf_no) doc_no,
                                               ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_TSF source_type,
                                               td.tsf_qty - NVL(td.ship_qty,0) - NVL((select SUM(ald.qty_allocated)
                                                                                        from alloc_header alh,
                                                                                             alloc_detail ald
																					   where (alh.origin_ind!='ALC' or 
																							 (alh.origin_ind='ALC' and alh.alloc_no in (select distinct xref_alloc_no from alc_xref)))
                                                                                         and ald.alloc_no    = alh.alloc_no
                                                                                         and alh.item        = s.item
                                                                                         and alh.order_no    = th.tsf_no
                                                                                         and alh.wh          = s.location
                                                                                       group by alh.item,
                                                                                                alh.order_no),0) avail_qty,
                                               0 backorder_qty
                                          from search_items s,
                                               tsfhead th,
                                               tsfdetail td
                                         where s.item             = td.item
                                           and s.location         = th.to_loc
                                           and td.tsf_no          = th.tsf_no
                                           and th.status          = 'A'
                                           and th.tsf_type IN ('SR','CF','AD','MR','EG','AIP','IC')
                                           and (   L_tsf_specific_ids = 'N'
                                                or (    L_tsf_specific_ids = 'Y'
                                                    and th.tsf_no IN (select number_1
                                                                        from gtt_num_num_str_str_date_date
                                                                       where varchar2_2 = ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_TSF)))
                                           and NVL(th.delivery_date, NVL(I_search_criteria.start_date, L_vdate)) >= NVL(I_search_criteria.start_date, NVL(th.delivery_date, L_vdate))
                                           and NVL(th.delivery_date, NVL(I_search_criteria.end_date, L_vdate))   <= NVL(I_search_criteria.end_date, NVL(th.delivery_date, L_vdate))
                                           and L_tsf_source_type = 'Y'
                                         union all
                                        ----------------------
                                        -- SOURCE_TYPE = BOL
                                        ----------------------
                                        select i.item,
                                               i.diff_1,
                                               i.diff_2,
                                               i.diff_3,
                                               i.diff_4,
                                               i.item_desc,
                                               i.rollup_type,
                                               i.rollup_item,
                                               i.rollup_item_desc,
                                               i.multi_color_pack_ind,
                                               i.location,
                                               i.bol_no doc_no,
                                               ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_BOL source_type,
                                               i.ship_qty * i.ratio avail_qty,
                                               0 backorder_qty
                                          from (select si.item,
                                                       si.diff_1,
                                                       si.diff_2,
                                                       si.diff_3,
                                                       si.diff_4,
                                                       si.item_desc,
                                                       si.rollup_type,
                                                       si.rollup_item,
                                                       si.rollup_item_desc,
                                                       si.multi_color_pack_ind,
                                                       si.location,
                                                       sh.bol_no,
                                                       ss.qty_expected - NVL(ss.qty_received,0) - NVL((select SUM(ald.qty_allocated)
                                                                                                         from alloc_header alh,
                                                                                                              alloc_detail ald,
                                                                                                              alc_xref alx
                                                                                                        where alh.alloc_no    = alx.xref_alloc_no
                                                                                                          and ald.alloc_no    = alh.alloc_no
                                                                                                          and alh.item        = si.item
                                                                                                          and alh.doc_type    = 'BOL'
                                                                                                          and alh.doc         = sh.bol_no
                                                                                                          and alh.wh          = si.location
                                                                                                        group by alh.item,
                                                                                                                 alh.doc),0) ship_qty,
                                                       1 ratio
                                                  from search_items si,
                                                       --alloc bol,
                                                       shipment sh,
                                                       shipsku ss,
                                                       wh w,
                                                       tsfhead th,
                                                       tsfdetail td
                                                 where si.item        = ss.item
                                                   and si.item        = td.item
                                                   and si.location    = th.to_loc
                                                   and si.location    = w.wh
                                                   and w.physical_wh  = sh.to_loc
                                                   and sh.shipment    = ss.shipment
                                                   and sh.bol_no      is NOT NULL
                                                   and sh.status_code = 'I'
                                                   and (   L_bol_specific_ids = 'N'
                                                        or (    L_bol_specific_ids = 'Y'
                                                            and sh.bol_no IN (select varchar2_1
                                                                                from gtt_num_num_str_str_date_date
                                                                               where varchar2_2 = ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_BOL)))
                                                   and NVL(sh.est_arr_date, NVL(I_search_criteria.start_date, L_vdate)) >= NVL(I_search_criteria.start_date, NVL(sh.est_arr_date, L_vdate))
                                                   and NVL(sh.est_arr_date, NVL(I_search_criteria.end_date, L_vdate))   <= NVL(I_search_criteria.end_date, NVL(sh.est_arr_date, L_vdate))
                                                   and L_bol_source_type = 'Y'
                                                   and ss.distro_type = 'T'
                                                   and ss.distro_no   = th.tsf_no
                                                   and th.tsf_no      = td.tsf_no
                                                 union all
                                                select al.item,
                                                       al.diff_1,
                                                       al.diff_2,
                                                       al.diff_3,
                                                       al.diff_4,
                                                       al.item_desc,
                                                       al.rollup_type,
                                                       al.rollup_item,
                                                       al.rollup_item_desc,
                                                       al.multi_color_pack_ind,
                                                       al.location,
                                                       al.bol_no,
                                                       al.ship_qty,
                                                       al.ratio
                                                  from (select si.item,
                                                               si.diff_1,
                                                               si.diff_2,
                                                               si.diff_3,
                                                               si.diff_4,
                                                               si.item_desc,
                                                               si.rollup_type,
                                                               si.rollup_item,
                                                               si.rollup_item_desc,
                                                               si.multi_color_pack_ind,
                                                               si.location,
                                                               ad.to_loc,
                                                               sh.bol_no,
                                                               ss.qty_expected - NVL(ss.qty_received,0) - NVL((select SUM(ald.qty_allocated)
                                                                                                                 from alloc_header alh,
                                                                                                                      alloc_detail ald,
                                                                                                                      alc_xref alx
                                                                                                                where alh.alloc_no    = alx.xref_alloc_no
                                                                                                                  and ald.alloc_no    = alh.alloc_no
                                                                                                                  and alh.item        = si.item
                                                                                                                  and alh.doc_type    = 'BOL'
                                                                                                                  and alh.doc         = sh.bol_no
                                                                                                                  and alh.wh          = si.location
                                                                                                                group by alh.item,
                                                                                                                         alh.doc),0) ship_qty,
                                                               ad.qty_allocated / SUM(ad.qty_allocated) OVER (PARTITION BY si.item,
                                                                                                                           si.location,
                                                                                                                           sh.shipment,
                                                                                                                           ad.alloc_no,
                                                                                                                           w.physical_wh) ratio
                                                          from search_items si,
                                                               shipment sh,
                                                               shipsku ss,
                                                               wh w,
                                                               wh allocwh,
                                                               alloc_header ah,
                                                               alloc_detail ad
                                                         where si.item        = ss.item
                                                           and si.location    = w.wh
                                                           and w.physical_wh  = sh.to_loc
                                                           and sh.shipment    = ss.shipment
                                                           and sh.bol_no      is NOT NULL
                                                           and sh.status_code = 'I'
                                                           and (   L_bol_specific_ids = 'N'
                                                                or (    L_bol_specific_ids = 'Y'
                                                                    and sh.bol_no IN (select varchar2_1
                                                                                        from gtt_num_num_str_str_date_date
                                                                                       where varchar2_2 = ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_BOL)))
                                                           and NVL(sh.est_arr_date, NVL(I_search_criteria.start_date, L_vdate)) >= NVL(I_search_criteria.start_date, NVL(sh.est_arr_date, L_vdate))
                                                           and NVL(sh.est_arr_date, NVL(I_search_criteria.end_date, L_vdate))   <= NVL(I_search_criteria.end_date, NVL(sh.est_arr_date, L_vdate))
                                                           and L_bol_source_type = 'Y'
                                                           and ss.distro_type = 'A'
                                                           and ss.distro_no   = ah.alloc_no
                                                           and ah.item        = si.item
                                                           and ah.alloc_no    = ad.alloc_no
                                                           and ad.to_loc      = allocwh.wh
                                                           and w.physical_wh  = allocwh.physical_wh) al
                                                 where al.location = al.to_loc) i
                                          union all
                                        ----------------------
                                        -- SOURCE_TYPE = ALLOC
                                        ----------------------
                                        select s.item,
                                               s.diff_1,
                                               s.diff_2,
                                               s.diff_3,
                                               s.diff_4,
                                               s.item_desc,
                                               s.rollup_type,
                                               s.rollup_item,
                                               s.rollup_item_desc,
                                               s.multi_color_pack_ind,
                                               s.location,
                                               TO_CHAR(x.alloc_id) doc_no,
                                               ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ALLOC source_type,
                                               SUM(ad.qty_allocated - NVL(ad.qty_transferred,0) - NVL((select SUM(ald.qty_allocated)
                                                                                                         from alloc_header alh,
                                                                                                              alloc_detail ald,
                                                                                                              alc_xref alx
                                                                                                        where alh.alloc_no    = alx.xref_alloc_no
                                                                                                          and ald.alloc_no    = alh.alloc_no
                                                                                                          and alh.item        = s.item
                                                                                                          and alh.order_no    = ah.alloc_no
                                                                                                          and alh.wh          = s.location
                                                                                                        group by alh.item,
                                                                                                                 alh.order_no),0)) avail_qty,
                                               0 backorder_qty
                                          from search_items s,
                                               alloc_header ah,
                                               alloc_detail ad,
                                               alc_xref x
                                         where s.item      = ah.item
                                           and s.location  = ad.to_loc
                                           and ad.alloc_no = ah.alloc_no
                                           and ah.alloc_no = x.xref_alloc_no
                                           and ah.status   = 'A'
                                           and (   L_alloc_specific_ids = 'N'
                                                or (    L_alloc_specific_ids = 'Y'
                                                    and ah.alloc_no IN (select number_1
                                                                          from gtt_num_num_str_str_date_date
                                                                         where varchar2_2 = ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ALLOC)))
                                           and NVL(ad.in_store_date, NVL(I_search_criteria.start_date, L_vdate)) >= NVL(I_search_criteria.start_date, NVL(ad.in_store_date, L_vdate))
                                           and NVL(ad.in_store_date, NVL(I_search_criteria.end_date, L_vdate))   <= NVL(I_search_criteria.end_date, NVL(ad.in_store_date, L_vdate))
                                           and L_alloc_source_type = 'Y'
                                         group by s.item,
                                                  s.diff_1,
                                                  s.diff_2,
                                                  s.diff_3,
                                                  s.diff_4,
                                                  s.item_desc,
                                                  s.rollup_type,
                                                  s.rollup_item,
                                                  s.rollup_item_desc,
                                                  s.multi_color_pack_ind,
                                                  s.location,
                                                  x.alloc_id
                                      ) inner
                                where inner.avail_qty > 0;

   LOGGER.LOG_INFORMATION(L_program||' INSERT ALC_ITEMSEARCH_INV_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_SOURCE_INV_SELECTION;
--------------------------------------------------------------------------
FUNCTION GET_ASN_SOURCE_FOR_SELECTION(O_error_message     IN OUT VARCHAR2,
                                      I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.GET_ASN_SOURCE_FOR_SELECTION';
   L_specific_ids VARCHAR2(1)  := 'N';
   L_vdate        DATE         := get_vdate;

   L_dist_tab       DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   L_ctr            NUMBER(10) := 0;
   L_asn_dist_table ASN_DIST_TABLE_TYPE;


BEGIN

   if (I_search_criteria.asns is not null and I_search_criteria.asns.count >= 1) then

      L_specific_ids := 'Y';

      delete from gtt_num_num_str_str_date_date;

      insert into gtt_num_num_str_str_date_date (varchar2_1)
         select distinct value(input_asns)
           from table(cast(I_search_criteria.asns as OBJ_VARCHAR_ID_TABLE)) input_asns;

   end if;

insert into alc_itemsearch_inv_gtt (item,
                                       diff_1,
                                       diff_2,
                                       diff_3,
                                       diff_4,
                                       item_desc,
                                       rollup_type,
                                       rollup_item,
                                       rollup_item_desc,
                                       multi_color_pack_ind,
                                       location,
                                       doc_no,
                                       source_type,
                                       avail_qty)
   select i.item,
          i.diff_1,
          i.diff_2,
          i.diff_3,
          i.diff_4,
          i.item_desc,
          i.rollup_type,
          i.rollup_item,
          i.rollup_item_desc,
          i.multi_color_pack_ind,
          i.to_loc,
          i.asn,
          ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ASN,
          i.ship_qty
     from
      (select distinct
             gtt.item,
             im.diff_1,
             im.diff_2,
             im.diff_3,
             im.diff_4,
             im.item_desc,
             gtt.rollup_type,
             gtt.rollup_item,
             gtt.rollup_item_desc,
             gtt.multi_color_pack_ind,
             ss.to_loc,
             sh.asn,
             oh.order_no,
                  sum(ss.qty_expected - nvl(ss.qty_received,0) -
                     --already allocated through ASN
                     nvl((select sum(ald.qty_allocated)
                            from alloc_header alh,
                                 alloc_detail ald,
                                 alc_xref alx
                           where alh.alloc_no    = alx.xref_alloc_no
                             and ald.alloc_no    = alh.alloc_no
                             and alh.item        = gtt.item
                             and alh.doc_type    = 'ASN'
                             and alh.doc         = sh.asn
                             and alh.wh          = gtt.location
                           group by alh.item,
                                    alh.doc),0) -
                     --already allocated through PO
                     nvl((select sum(ald.qty_allocated)
                            from alloc_header alh,
                                 alloc_detail ald,
                                 alc_xref alx
                           where alh.alloc_no    = alx.xref_alloc_no
                             and ald.alloc_no    = alh.alloc_no
                             and alh.item        = gtt.item
                             and alh.doc_type    = 'PO'
                             and alh.order_no    = oh.order_no
                             and alh.wh          = gtt.location
                           group by alh.item,
                                    alh.order_no),0)) ship_qty
        from alc_itemsearch_items_gtt gtt,
             shipment sh,
             shipsku_loc ss,
             wh w,             
             item_master im,
             ordhead oh
       where gtt.item          = ss.item
         and gtt.location      = ss.to_loc
         and w.wh = ss.to_loc
         and w.physical_wh     = sh.to_loc
         and sh.shipment       = ss.shipment
         --
         and sh.asn         is not null
         and sh.status_code = 'I'
         and (L_specific_ids = 'N' or
              (L_specific_ids = 'Y' and
               sh.asn in(select varchar2_1 from gtt_num_num_str_str_date_date)))
         and sh.order_no       = oh.order_no
         --
         and nvl(sh.est_arr_date, nvl(I_search_criteria.start_date, get_vdate)) >=
                nvl(I_search_criteria.start_date, nvl(sh.est_arr_date, get_vdate))
         and nvl(sh.est_arr_date, nvl(I_search_criteria.end_date, get_vdate))  <=
                nvl(I_search_criteria.end_date, nvl(sh.est_arr_date, get_vdate))
         --
         and gtt.item          = im.item
         --
         --
         and oh.status         = 'A'
         and oh.order_type    != 'ARB'
         and ((oh.orig_ind != 0) or (oh.orig_ind = 0 and oh.contract_no IS NOT NULL))
         group by 
             gtt.item,
             im.diff_1,
             im.diff_2,
             im.diff_3,
             im.diff_4,
             im.item_desc,
             gtt.rollup_type,
             gtt.rollup_item,
             gtt.rollup_item_desc,
             gtt.multi_color_pack_ind,
             ss.to_loc,
             sh.asn,
             oh.order_no)i;



   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ASN_SOURCE_FOR_SELECTION;
--------------------------------------------------------------------------
FUNCTION POPULATE_NO_PARENTS(O_error_message    IN OUT VARCHAR2,
                             O_work_header_id   IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.POPULATE_NO_PARENTS';

BEGIN

   --staple and sellable pack
   merge into alc_work_item_source target
   using (select gtt.item,
                 im.alc_item_type,
                 gtt.item_desc,
                 gtt.avail_qty,
                 gtt.backorder_qty,
                 gtt.location,
                 gtt.doc_no,
                 gtt.source_type
            from alc_itemsearch_inv_gtt gtt,
                 item_master im
           where gtt.rollup_type IN ('NONSELLPACK','STAPLE','SELLPACK')
             and gtt.item = im.item) use_this
   on (    target.work_header_id      = O_work_header_id
       and target.item                = use_this.item
       and target.item_type           = use_this.alc_item_type
       and target.wh                  = use_this.location
       and NVL(target.doc_no, '-999') = NVL(use_this.doc_no, '-999')
       and target.source_type         = use_this.source_type)
   when NOT MATCHED then
      insert (work_item_source_id,
              work_header_id,
              item,
              item_type,
              ancestor_id,
              item_source_desc,
              available_quantity,
              backorder_qty,
              wh,
              doc_no,
              source_type,
              refresh_ind,
              deleted_ind,
              expected_to_date,
              created_by,
              updated_by,
              created_date,
              updated_date,
              item_id,
              diff_1,
              diff_2,
              diff_3,
              disable_ind)
      values (alc_work_item_source_seq.NEXTVAL,
              O_work_header_id,
              use_this.item,
              use_this.alc_item_type,
              NULL,
              use_this.item_desc,
              use_this.avail_qty,
              use_this.backorder_qty,
              use_this.location,
              use_this.doc_no,
              use_this.source_type,
              'Y',
              'N',
              NULL,
              USER,
              USER,
              SYSDATE,
              SYSDATE,
              use_this.item,
              NULL,
              NULL,
              NULL,
              NULL);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE - NO PARENTS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_NO_PARENTS;
--------------------------------------------------------------------------
FUNCTION POPULATE_FASHION(O_error_message    IN OUT VARCHAR2,
                          O_work_header_id   IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                          I_what_if_ind      IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.POPULATE_FASHION';
   L_work_type    ALC_WORK_HEADER.WORK_TYPE%TYPE;

BEGIN

   select work_type
     into L_work_type
     from alc_work_header
    where work_header_id = O_work_header_id;

   --STYLE LEVEL ROWS
   merge into alc_work_item_source target
   using (select i.rollup_item,
                 ALC_CONSTANTS_SQL.STYLE item_type,
                 i.rollup_item_desc,
                 i.avail_qty,
                 i.backorder_qty,
                 i.location,
                 i.doc_no,
                 i.source_type
            from (select DISTINCT gtt.rollup_item,
                         gtt.rollup_item_desc,
                         gtt.location,
                         gtt.doc_no,
                         gtt.source_type,
                         SUM(gtt.avail_qty) OVER (PARTITION BY gtt.rollup_item,
                                                               gtt.location,
                                                               gtt.doc_no,
                                                               gtt.source_type) avail_qty,
                         SUM(gtt.backorder_qty) OVER (PARTITION BY gtt.rollup_item,
                                                                   gtt.location,
                                                                   gtt.doc_no,
                                                                   gtt.source_type) backorder_qty
                    from alc_itemsearch_inv_gtt gtt
                   where gtt.rollup_type = 'STYLE') i) use_this
   on (    target.work_header_id      = O_work_header_id
       and target.item                = use_this.rollup_item
       and target.item_type           = use_this.item_type
       and target.wh                  = use_this.location
       and NVL(target.doc_no, '-999') = NVL(use_this.doc_no, '-999')
       and target.source_type         = use_this.source_type)
   when NOT MATCHED then
      insert (work_item_source_id,
              work_header_id,
              item,
              item_type,
              ancestor_id,
              item_source_desc,
              available_quantity,
              backorder_qty,
              wh,
              doc_no,
              source_type,
              refresh_ind,
              deleted_ind,
              expected_to_date,
              created_by,
              updated_by,
              created_date,
              updated_date,
              item_id,
              diff_1,
              diff_2,
              diff_3,
              disable_ind)
      values (alc_work_item_source_seq.NEXTVAL,
              O_work_header_id,
              use_this.rollup_item,
              use_this.item_type,
              NULL,
              use_this.rollup_item_desc,
              use_this.avail_qty,
              use_this.backorder_qty,
              use_this.location,
              use_this.doc_no,
              use_this.source_type,
              'N',
              NULL,
              NULL,
              USER,
              USER,
              SYSDATE,
              SYSDATE,
              NULL,
              NULL,
              NULL,
              NULL,
              NULL);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE - STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --STYLE-AGG_DIFF(s) level rows
   merge into alc_work_item_source target
   using (select i.rollup_item||diff_1_id||diff_2_id||diff_3_id||diff_4_id item,
                 ALC_CONSTANTS_SQL.FASHIONITEM item_type,
                 awis.work_item_source_id ancestor_id,
                 i.rollup_item_desc||diff_1_desc||diff_2_desc||diff_3_desc||diff_4_desc item_source_desc,
                 i.avail_qty,
                 i.backorder_qty,
                 i.location,
                 i.doc_no,
                 i.source_type,
                 i.rollup_item item_id,
                 i.diff_1,
                 i.diff_2,
                 i.diff_3
            from (select DISTINCT
                         gtt.rollup_item,
                         DECODE(im.diff_1_aggregate_ind,
                                'Y', gtt.diff_1,
                                NULL) diff_1,
                         DECODE(im.diff_2_aggregate_ind,
                                'Y', gtt.diff_2,
                                NULL) diff_2,
                         DECODE(im.diff_3_aggregate_ind,
                                'Y', gtt.diff_3,
                                NULL) diff_3,
                         DECODE(im.diff_4_aggregate_ind,
                                'Y', gtt.diff_4,
                                NULL) diff_4,
                         --
                         DECODE(im.diff_1_aggregate_ind,
                                'Y', ' 1~'||gtt.diff_1,
                                NULL) diff_1_id,
                         DECODE(im.diff_2_aggregate_ind,
                                'Y', ' 2~'||gtt.diff_2,
                                NULL) diff_2_id,
                         DECODE(im.diff_3_aggregate_ind,
                                'Y', ' 3~'||gtt.diff_3,
                                NULL) diff_3_id,
                         DECODE(im.diff_4_aggregate_ind,
                                'Y', ' 4~'||gtt.diff_4,
                                NULL) diff_4_id,
                         --
                         DECODE(im.diff_1_aggregate_ind,
                                'Y', ':'||gtt.diff_1,
                                NULL) diff_1_desc,
                         DECODE(im.diff_2_aggregate_ind,
                                'Y', ':'||gtt.diff_2,
                                NULL) diff_2_desc,
                         DECODE(im.diff_3_aggregate_ind,
                                'Y', ':'||gtt.diff_3,
                                NULL) diff_3_desc,
                         DECODE(im.diff_4_aggregate_ind,
                                'Y', ':'||gtt.diff_4,
                                NULL) diff_4_desc,
                         --
                         gtt.rollup_item_desc,
                         gtt.location,
                         gtt.doc_no,
                         gtt.source_type,
                         SUM(gtt.avail_qty) OVER (PARTITION BY gtt.rollup_item,
                                                               gtt.location,
                                                               gtt.doc_no,
                                                               gtt.source_type,
                                                               DECODE(im.diff_1_aggregate_ind,
                                                                      'Y', gtt.diff_1,
                                                                      NULL),
                                                               DECODE(im.diff_2_aggregate_ind,
                                                                      'Y', gtt.diff_2,
                                                                      NULL),
                                                               DECODE(im.diff_3_aggregate_ind,
                                                                      'Y', gtt.diff_3,
                                                                      NULL),
                                                               DECODE(im.diff_4_aggregate_ind,
                                                                      'Y', gtt.diff_4,
                                                                      NULL)) avail_qty,
                         SUM(gtt.backorder_qty) OVER (PARTITION BY gtt.rollup_item,
                                                                   gtt.location,
                                                                   gtt.doc_no,
                                                                   gtt.source_type,
                                                                   DECODE(im.diff_1_aggregate_ind,
                                                                          'Y', gtt.diff_1,
                                                                          NULL),
                                                                   DECODE(im.diff_2_aggregate_ind,
                                                                          'Y', gtt.diff_2,
                                                                          NULL),
                                                                   DECODE(im.diff_3_aggregate_ind,
                                                                          'Y', gtt.diff_3,
                                                                          NULL),
                                                                   DECODE(im.diff_4_aggregate_ind,
                                                                          'Y', gtt.diff_4,
                                                                          NULL)) backorder_qty
                    from (select gtt.rollup_item,
                                 gtt.rollup_item_desc,
                                 gtt.rollup_type,
                                 gtt.location,
                                 gtt.doc_no,
                                 gtt.source_type,
                                 gtt.avail_qty,
                                 gtt.backorder_qty,
                                 gtt.diff_1,
                                 gtt.diff_2,
                                 gtt.diff_3,
                                 gtt.diff_4
                            from alc_itemsearch_inv_gtt gtt
                           where gtt.rollup_type = 'STYLE'
                             and gtt.diff_1      is NOT NULL --will only be null for packs
                          union all
                          select gtt.rollup_item,
                                 gtt.rollup_item_desc,
                                 gtt.rollup_type,
                                 gtt.location,
                                 gtt.doc_no,
                                 gtt.source_type,
                                 gtt.avail_qty,
                                 gtt.backorder_qty,
                                 im.diff_1,
                                 im.diff_2,
                                 im.diff_3,
                                 im.diff_4
                            from alc_itemsearch_inv_gtt gtt,
                                 packitem_breakout pb,
                                 item_master im
                           where gtt.rollup_type          = 'STYLE'
                             and gtt.diff_1               is NULL --will be null for packs under the style
                             and gtt.item                 = pb.pack_no
                             and gtt.multi_color_pack_ind = 'N'
                             and pb.seq_no                = 1
                             and pb.item                  = im.item
                         ) gtt,
                         item_master im
                   where gtt.rollup_type = 'STYLE'
                     and gtt.diff_1      is NOT NULL --will only be null for packs
                     and gtt.rollup_item = im.item) i,
                 alc_work_item_source awis
           where awis.work_header_id      = O_work_header_id
             and awis.item_type           = ALC_CONSTANTS_SQL.STYLE
             and awis.item                = i.rollup_item
             and awis.wh                  = i.location
             and awis.source_type         = i.source_type
             and NVL(awis.doc_no, '-999') = NVL(i.doc_no,'-999')) use_this
   on (    target.work_header_id      = O_work_header_id
       and target.item                = use_this.item
       and target.item_type           = use_this.item_type
       and target.wh                  = use_this.location
       and target.ancestor_id         = use_this.ancestor_id
       and NVL(target.doc_no, '-999') = NVL(use_this.doc_no, '-999')
       and target.source_type         = use_this.source_type)
   when NOT MATCHED then
      insert (work_item_source_id,
              work_header_id,
              item,
              item_type,
              ancestor_id,
              item_source_desc,
              available_quantity,
              backorder_qty,
              wh,
              doc_no,
              source_type,
              refresh_ind,
              deleted_ind,
              expected_to_date,
              created_by,
              updated_by,
              created_date,
              updated_date,
              item_id,
              diff_1,
              diff_2,
              diff_3,
              disable_ind)
      values (alc_work_item_source_seq.NEXTVAL,
              O_work_header_id,
              use_this.item,
              use_this.item_type,
              use_this.ancestor_id,
              use_this.item_source_desc,
              use_this.avail_qty,
              use_this.backorder_qty,
              use_this.location,
              use_this.doc_no,
              use_this.source_type,
              'N',
              NULL,
              NULL,
              USER,
              USER,
              SYSDATE,
              SYSDATE,
              use_this.item_id,
              use_this.diff_1,
              use_this.diff_2,
              use_this.diff_3,
              NULL);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE - FA - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --insert fashion sku level rows
   merge into alc_work_item_source target
   using (select gtt.item,
                 ALC_CONSTANTS_SQL.FASHIONSKU item_type,
                 awis.work_item_source_id ancestor_id,
                 gtt.item_desc,
                 gtt.avail_qty,
                 gtt.backorder_qty,
                 gtt.location,
                 gtt.doc_no,
                 gtt.source_type
            from alc_itemsearch_inv_gtt gtt,
                 alc_work_item_source awis
           where gtt.rollup_type        = 'STYLE'
             and gtt.diff_1             is NOT NULL --will only be null for packs
             --
             and awis.work_header_id    = O_work_header_id
             and awis.item_type         = ALC_CONSTANTS_SQL.FASHIONITEM
             and gtt.rollup_item        = awis.item_id
             and gtt.location           = awis.wh
             and NVL(gtt.doc_no,'-999') = NVL(awis.doc_no, '-999')
             and gtt.source_type        = awis.source_type
             and NVL(gtt.diff_1, '-1')  = NVL(awis.diff_1, NVL(gtt.diff_1, '-1'))
             and NVL(gtt.diff_2, '-1')  = NVL(awis.diff_2, NVL(gtt.diff_2, '-1'))
             and NVL(gtt.diff_3, '-1')  = NVL(awis.diff_3, NVL(gtt.diff_3, '-1'))) use_this
   on (    target.work_header_id      = O_work_header_id
       and target.item                = use_this.item
       and target.item_type           = use_this.item_type
       and target.wh                  = use_this.location
       and target.ancestor_id         = use_this.ancestor_id
       and NVL(target.doc_no, '-999') = NVL(use_this.doc_no, '-999')
       and target.source_type         = use_this.source_type)
   when NOT MATCHED then
      insert (work_item_source_id,
              work_header_id,
              item,
              item_type,
              ancestor_id,
              item_source_desc,
              available_quantity,
              backorder_qty,
              wh,
              doc_no,
              source_type,
              refresh_ind,
              deleted_ind,
              expected_to_date,
              created_by,
              updated_by,
              created_date,
              updated_date,
              item_id,
              diff_1,
              diff_2,
              diff_3,
              disable_ind)
      values (alc_work_item_source_seq.NEXTVAL,
              O_work_header_id,
              use_this.item,
              use_this.item_type,
              use_this.ancestor_id,
              use_this.item_desc,
              use_this.avail_qty,
              use_this.backorder_qty,
              use_this.location,
              use_this.doc_no,
              use_this.source_type,
              'Y',
              'N',
              NULL,
              USER,
              USER,
              SYSDATE,
              SYSDATE,
              use_this.item,
              NULL,
              NULL,
              NULL,
              NULL);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE - FASHIONSKU - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --need to expand to include all children even those with no inventory for OH source type
   /*is:don't do for what-if?*/
   merge into alc_work_item_source target
   using (select im.item,
                 ALC_CONSTANTS_SQL.FASHIONSKU item_type,
                 awis.work_item_source_id ancestor_id,
                 im.item_desc,
                 awis.wh,
                 awis.doc_no,
                 awis.source_type
            from item_master im,
                 alc_work_item_source awis
        where awis.work_header_id       = O_work_header_id
              and awis.item_type        = ALC_CONSTANTS_SQL.FASHIONITEM
              and awis.source_type      = ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_OH
              --
              and im.item_parent        = awis.item_id
              and NVL(im.diff_1, '-1')  = NVL(awis.diff_1, NVL(im.diff_1, '-1'))
              and NVL(im.diff_2, '-1')  = NVL(awis.diff_2, NVL(im.diff_2, '-1'))
              and NVL(im.diff_3, '-1')  = NVL(awis.diff_3, NVL(im.diff_3, '-1'))) use_this
   on (    target.work_header_id     = O_work_header_id
       and target.item               = use_this.item
       and target.item_type          = use_this.item_type
       and target.wh                 = use_this.wh
       and target.ancestor_id        = use_this.ancestor_id
       and target.doc_no             is NULL
       and target.source_type        = use_this.source_type)
   when NOT MATCHED then
      insert (work_item_source_id,
              work_header_id,
              item,
              item_type,
              ancestor_id,
              item_source_desc,
              available_quantity,
              backorder_qty,
              wh,
              doc_no,
              source_type,
              refresh_ind,
              deleted_ind,
              expected_to_date,
              created_by,
              updated_by,
              created_date,
              updated_date,
              item_id,
              diff_1,
              diff_2,
              diff_3,
              disable_ind)
      values (alc_work_item_source_seq.NEXTVAL,
              O_work_header_id,
              use_this.item,
              ALC_CONSTANTS_SQL.FASHIONSKU,
              use_this.ancestor_id,
              use_this.item_desc,
              0,
              0,
              use_this.wh,
              use_this.doc_no,
              use_this.source_type,
              'Y',
              'N',
              NULL,
              USER,
              USER,
              SYSDATE,
              SYSDATE,
              use_this.item,
              NULL,
              NULL,
              NULL,
              'Y');

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE - FASHIONSKU NO AVAIL QTY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if L_work_type = ALC_CONSTANTS_SQL.WORKSHEET_TYPE_WORK or
     (L_work_type = ALC_CONSTANTS_SQL.WORKSHEET_TYPE_ALLOC and NOT I_what_if_ind) then

      --packs under style color
      merge into alc_work_item_source target
      using (select gtt.item,
                    DECODE(im_pack.simple_pack_ind,
                           'Y', ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                           'N', ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK) item_type,
                    awis.work_item_source_id ancestor_id,
                    gtt.item_desc,
                    gtt.avail_qty,
                    gtt.backorder_qty,
                    gtt.location,
                    gtt.doc_no,
                    gtt.source_type
               from alc_itemsearch_inv_gtt gtt,
                    item_master im_pack,
                    packitem_breakout pb,
                    item_master im,
                    alc_work_item_source awis
              where gtt.rollup_type          = 'STYLE'
                and gtt.diff_1               is NULL --will only be null for packs
                and gtt.multi_color_pack_ind = 'N'
                and gtt.item                 = im_pack.item
                and im_pack.sellable_ind     = 'N'
                --
                and gtt.item                 = pb.pack_no
                and pb.seq_no                = 1
                and pb.item                  = im.item
                --
                and awis.work_header_id      = O_work_header_id
                and awis.item_type           = ALC_CONSTANTS_SQL.FASHIONITEM
                and gtt.rollup_item          = awis.item_id
                and gtt.location             = awis.wh
                and NVL(gtt.doc_no,'-999')   = NVL(awis.doc_no, '-999')
                and gtt.source_type          = awis.source_type
                and NVL(im.diff_1, '-1')     = NVL(awis.diff_1, NVL(im.diff_1, '-1'))
                and NVL(im.diff_2, '-1')     = NVL(awis.diff_2, NVL(im.diff_2, '-1'))
                and NVL(im.diff_3, '-1')     = NVL(awis.diff_3, NVL(im.diff_3, '-1'))) use_this
      on (    target.work_header_id      = O_work_header_id
          and target.item                = use_this.item
          and target.item_type           = use_this.item_type
          and target.wh                  = use_this.location
          and target.ancestor_id         = use_this.ancestor_id
          and NVL(target.doc_no, '-999') = NVL(use_this.doc_no, '-999')
          and target.source_type         = use_this.source_type)
      when NOT MATCHED then
         insert (work_item_source_id,
                 work_header_id,
                 item,
                 item_type,
                 ancestor_id,
                 item_source_desc,
                 available_quantity,
                 backorder_qty,
                 wh,
                 doc_no,
                 source_type,
                 refresh_ind,
                 deleted_ind,
                 expected_to_date,
                 created_by,
                 updated_by,
                 created_date,
                 updated_date,
                 item_id,
                 diff_1,
                 diff_2,
                 diff_3,
                 disable_ind)
         values (alc_work_item_source_seq.NEXTVAL,
                 O_work_header_id,
                 use_this.item,
                 use_this.item_type,
                 use_this.ancestor_id,
                 use_this.item_desc,
                 use_this.avail_qty,
                 use_this.backorder_qty,
                 use_this.location,
                 use_this.doc_no,
                 use_this.source_type,
                 'Y',
                 'N',
                 NULL,
                 USER,
                 USER,
                 SYSDATE,
                 SYSDATE,
                 use_this.item,
                 NULL,
                 NULL,
                 NULL,
                 NULL);

   end if;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE - PACK UNDER ST - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --sell packs under style
   merge into alc_work_item_source target
   using (select gtt.item,
                 ALC_CONSTANTS_SQL.SELLABLEPACK item_type,
                 awis.work_item_source_id ancestor_id,
                 gtt.item_desc,
                 gtt.avail_qty,
                 gtt.backorder_qty,
                 gtt.location,
                 gtt.doc_no,
                 gtt.source_type
            from alc_itemsearch_inv_gtt gtt,
                 item_master im_pack,
                 alc_work_item_source awis
           where gtt.rollup_type          = 'STYLE'
             and gtt.diff_1               is NULL --will only be null for packs
             and gtt.multi_color_pack_ind = 'N'
             and gtt.item                 = im_pack.item
             and im_pack.pack_ind         = 'Y'
             and im_pack.sellable_ind     = 'Y'
             --
             and awis.work_header_id      = O_work_header_id
             and awis.item_type           = ALC_CONSTANTS_SQL.STYLE
             and gtt.rollup_item          = awis.item
             and gtt.location             = awis.wh
             and NVL(gtt.doc_no,'-999')   = NVL(awis.doc_no, '-999')
             and gtt.source_type          = awis.source_type
      ) use_this
   on (    target.work_header_id      = O_work_header_id
       and target.item                = use_this.item
       and target.item_type           = use_this.item_type
       and target.wh                  = use_this.location
       and target.ancestor_id         = use_this.ancestor_id
       and NVL(target.doc_no, '-999') = NVL(use_this.doc_no, '-999')
       and target.source_type         = use_this.source_type)
   when NOT MATCHED then
      insert (work_item_source_id,
              work_header_id,
              item,
              item_type,
              ancestor_id,
              item_source_desc,
              available_quantity,
              backorder_qty,
              wh,
              doc_no,
              source_type,
              refresh_ind,
              deleted_ind,
              expected_to_date,
              created_by,
              updated_by,
              created_date,
              updated_date,
              item_id,
              diff_1,
              diff_2,
              diff_3,
              disable_ind)
      values (alc_work_item_source_seq.NEXTVAL,
              O_work_header_id,
              use_this.item,
              use_this.item_type,
              use_this.ancestor_id,
              use_this.item_desc,
              use_this.avail_qty,
              use_this.backorder_qty,
              use_this.location,
              use_this.doc_no,
              use_this.source_type,
              'Y',
              'N',
              NULL,
              USER,
              USER,
              SYSDATE,
              SYSDATE,
              use_this.item,
              NULL,
              NULL,
              NULL,
              NULL);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE - SELLPACK UNDER ST - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --multi-color (with single parent item) packs under style
   merge into alc_work_item_source target
   using (select gtt.item,
                 DECODE(im.sellable_ind,
                        'Y', ALC_CONSTANTS_SQL.SELLABLEPACK,
                        ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK) item_type,
                 awis.work_item_source_id ancestor_id,
                 gtt.item_desc,
                 gtt.avail_qty,
                 gtt.backorder_qty,
                 gtt.location,
                 gtt.doc_no,
                 gtt.source_type
            from alc_itemsearch_inv_gtt gtt,
                 alc_work_item_source awis,
                 item_master im
           where gtt.rollup_type          = 'STYLE'
             and gtt.diff_1               is NULL --will only be null for packs
             and gtt.multi_color_pack_ind = 'Y'
             --
             and gtt.item                 = im.item
             --
             and awis.work_header_id      = O_work_header_id
             and awis.item_type           = ALC_CONSTANTS_SQL.STYLE
             and gtt.rollup_item          = awis.item
             and gtt.location             = awis.wh
             and NVL(gtt.doc_no,'-999')   = NVL(awis.doc_no, '-999')
             and gtt.source_type          = awis.source_type) use_this
   on (    target.work_header_id      = O_work_header_id
       and target.item                = use_this.item
       and target.item_type           = use_this.item_type
       and target.wh                  = use_this.location
       and target.ancestor_id         = use_this.ancestor_id
       and NVL(target.doc_no, '-999') = NVL(use_this.doc_no, '-999')
       and target.source_type         = use_this.source_type)
   when NOT MATCHED then
      insert (work_item_source_id,
              work_header_id,
              item,
              item_type,
              ancestor_id,
              item_source_desc,
              available_quantity,
              backorder_qty,
              wh,
              doc_no,
              source_type,
              refresh_ind,
              deleted_ind,
              expected_to_date,
              created_by,
              updated_by,
              created_date,
              updated_date,
              item_id,
              diff_1,
              diff_2,
              diff_3,
              disable_ind)
      values (alc_work_item_source_seq.NEXTVAL,
              O_work_header_id,
              use_this.item,
              use_this.item_type,
              use_this.ancestor_id,
              use_this.item_desc,
              use_this.avail_qty,
              use_this.backorder_qty,
              use_this.location,
              use_this.doc_no,
              use_this.source_type,
              'Y',
              'N',
              NULL,
              USER,
              USER,
              SYSDATE,
              SYSDATE,
              use_this.item,
              NULL,
              NULL,
              NULL,
              NULL);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE - MUTICOLOR PACK UNDER ST - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_FASHION;
--------------------------------------------------------------------------
FUNCTION POPULATE_PACK_COMP(O_error_message     IN OUT VARCHAR2,
                            O_work_header_id    IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.POPULATE_PACK_COMP';

BEGIN

   merge into alc_work_item_source target
   using (select im.item,
                 'PACKCOMP' item_type,
                 awis.work_item_source_id ancestor_id,
                 im.item_desc,
                 0 avail_quantity,
                 0 backorder_qty,
                 awis.wh,
                 awis.doc_no,
                 awis.source_type
            from alc_work_item_source awis,
                 packitem_breakout pb,
                 item_master im
           where awis.work_header_id = O_work_header_id
             and awis.item           = pb.pack_no
             and pb.item             = im.item) use_this
   on (    target.work_header_id      = O_work_header_id
       and target.item                = use_this.item
       and target.wh                  = use_this.wh
       and target.ancestor_id         = use_this.ancestor_id
       and NVL(target.doc_no, '-999') = NVL(use_this.doc_no, '-999')
       and target.source_type         = use_this.source_type)
   when NOT MATCHED then
      insert (work_item_source_id,
              work_header_id,
              item,
              item_type,
              ancestor_id,
              item_source_desc,
              available_quantity,
              backorder_qty,
              wh,
              doc_no,
              source_type,
              refresh_ind,
              deleted_ind,
              expected_to_date,
              created_by,
              updated_by,
              created_date,
              updated_date,
              item_id,
              diff_1,
              diff_2,
              diff_3,
              disable_ind)
      values (alc_work_item_source_seq.NEXTVAL,
              O_work_header_id,
              use_this.item,
              use_this.item_type,
              use_this.ancestor_id,
              use_this.item_desc,
              use_this.avail_quantity,
              use_this.backorder_qty,
              use_this.wh,
              use_this.doc_no,
              use_this.source_type,
              'N',
              'N',
              NULL,
              USER,
              USER,
              SYSDATE,
              SYSDATE,
              use_this.item,
              NULL,
              NULL,
              NULL,
              NULL);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE - PACKCOMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_PACK_COMP;
--------------------------------------------------------------------------
FUNCTION POPULATE_NONSELL_PACK_BACKORD(O_error_message    IN OUT VARCHAR2,
                                       O_work_header_id   IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.POPULATE_NONSELL_PACK_BACKORD';

BEGIN

   --non-sellable packs, baackorder qty is sum of component items
   merge into alc_work_item_source target
   using (select al.work_item_source_id,
                 SUM(ils.customer_backorder) backorder_qty
            from alc_work_item_source al,
                 packitem_breakout pb,
                 item_loc_soh ils
           where al.work_header_id = O_work_header_id
             and al.item_type      IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                       ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                       ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                       ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                       ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK)
             and al.item           = pb.pack_no
             and pb.item           = ils.item
             and al.wh             = ils.loc
           group by al.work_item_source_id) use_this
   on (target.work_item_source_id      = use_this.work_item_source_id)
   when MATCHED then
      update
         set target.backorder_qty  = NVL(target.backorder_qty,0) + use_this.backorder_qty;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE - NONSELL PACK BACKORD - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_NONSELL_PACK_BACKORD;
--------------------------------------------------------------------------
FUNCTION INSERT_HEADER(O_error_message    IN OUT VARCHAR2,
                       O_work_header_id      OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                       I_work_type        IN     ALC_WORK_HEADER.WORK_TYPE%TYPE,
                       I_user_id          IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.INSERT_HEADER';

BEGIN

   select alc_work_header_seq.NEXTVAL
     into O_work_header_id
     from dual;

   --populate the work sheet tables
   insert into alc_work_header(work_header_id,
                               work_type,
                               work_desc,
                               created_by,
                               updated_by,
                               created_date,
                               updated_date)
                       values (O_work_header_id,
                               I_work_type,
                               O_work_header_id,
                               I_user_id,
                               I_user_id,
                               SYSDATE,
                               SYSDATE);

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_WORK_HEADER - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_HEADER;
--------------------------------------------------------------------------
FUNCTION QUICK_POP_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                             O_work_header_id       OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                             O_alloc_type           OUT ALC_ALLOC.TYPE%TYPE,
                             I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                             I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.QUICK_POP_WORKSHEET';

BEGIN

   if QUICK_CREATE_WORKSHEET(O_error_message,
                             O_work_header_id,
                             O_alloc_type,
                             I_search_criteria,
                             I_user_id,
                             FALSE) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END QUICK_POP_WORKSHEET;
--------------------------------------------------------------------------
FUNCTION WHAT_IF_QUICK_POP_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                                     O_work_header_id       OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                                     O_alloc_type           OUT ALC_ALLOC.TYPE%TYPE,
                                     I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                                     I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER IS

   L_program         VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.WHAT_IF_QUICK_POP_WORKSHEET';

BEGIN

   if QUICK_CREATE_WORKSHEET(O_error_message,
                             O_work_header_id,
                             O_alloc_type,
                             I_search_criteria,
                             I_user_id,
                             TRUE) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END WHAT_IF_QUICK_POP_WORKSHEET;
--------------------------------------------------------------------------
FUNCTION QUICK_CREATE_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                                O_work_header_id       OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                                O_alloc_type           OUT ALC_ALLOC.TYPE%TYPE,
                                I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                                I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE,
                                I_what_if_ind       IN     BOOLEAN)
RETURN BOOLEAN IS

   L_program              VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.QUICK_CREATE_WORKSHEET';
   L_start_time           TIMESTAMP := SYSTIMESTAMP;

   L_invalid_item_type    VARCHAR2(1) := 'N';
   L_search_criteria      ALC_ITEM_SEARCH_CRITERIA_REC := NULL;

   cursor C_VALIDATE_ITEMS is
      select im.alc_item_type item_type
        from item_master im,
             table(cast(L_search_criteria.items as OBJ_VARCHAR_ID_TABLE)) input_items
       where im.item = value(input_items);

   cursor C_VALIDATE_TXN_IDS is
      select TO_CHAR(value(input_pos)) txn_no,
             oh.status
        from table(cast(L_search_criteria.pos as OBJ_NUMERIC_ID_TABLE)) input_pos,
             ordhead oh
       where value(input_pos) = oh.order_no(+)
         and oh.order_type(+) != 'ARB'
         and oh.status(+)     = 'A'
         and (oh.orig_ind(+) != 0 or (oh.orig_ind(+) = 0 and oh.contract_no(+) is NOT NULL))
      --
      union all
      --
      select TO_CHAR(value(input_tsfs)),
             th.status
        from table(cast(L_search_criteria.tsfs as OBJ_NUMERIC_ID_TABLE)) input_tsfs,
             tsfhead th
       where value(input_tsfs) = th.tsf_no(+)
         and th.status(+)      = 'A'
         and th.tsf_type(+)    IN ('SR','CF','AD','MR','EG','AIP','IC')
      --
      union all
      --
      select value(input_asns),
             sh.status_code
        from table(cast(L_search_criteria.asns as OBJ_VARCHAR_ID_TABLE)) input_asns,
             shipment sh
       where value(input_asns) = sh.asn(+)
         and sh.status_code(+) = 'I'
      --
      union all
      --
      select value(input_bols),
             sh.status_code
        from table(cast(L_search_criteria.bols as OBJ_VARCHAR_ID_TABLE)) input_bols,
             shipment sh
       where value(input_bols) = sh.bol_no(+)
         and sh.status_code(+) = 'I'
      --
      union all
      --
      select TO_CHAR(value(input_allocs)), i.status
        from table(cast(L_search_criteria.allocs as OBJ_NUMERIC_ID_TABLE)) input_allocs,
             (select ah.alloc_no,
                     ah.status
                from table(cast(L_search_criteria.allocs as OBJ_NUMERIC_ID_TABLE)) ia,
                     alloc_header ah
               where ah.alloc_no     = value(ia)
                 and ah.status       = 'A'
                 and ah.alloc_method = 'A'
                 and ah.origin_ind   = 'ALC') i
       where value(input_allocs) = i.alloc_no(+);

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if MAP_ALC_IDS_TO_RMS_IDS(O_error_message,
                             L_search_criteria,
                             I_search_criteria) = FALSE then
      return FALSE;
   end if;

   for rec in C_VALIDATE_ITEMS loop
      if rec.item_type is NULL then
         O_error_message := ALC_CONSTANTS_SQL.ERRMSG_QUICK_CREATE_NOT_ITEM;
         return FALSE;
      end if;
      --
      if rec.item_type IN (ALC_CONSTANTS_SQL.STYLE,
                           ALC_CONSTANTS_SQL.FASHIONSKU,
                           ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                           ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                           ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK ) then
         L_invalid_item_type := 'Y';
      end if;
   end loop;
   --
   if L_invalid_item_type = 'Y' then
      O_error_message := ALC_CONSTANTS_SQL.ERRMSG_QUICK_CREATE_STYLE;
      return FALSE;
   end if;

   for rec in C_VALIDATE_TXN_IDS loop
      if rec.status is NULL then
         O_error_message := ALC_CONSTANTS_SQL.ERRMSG_QUICK_CREATE_INV_TXN;
         return FALSE;
      end if;
   end loop;

   if INSERT_HEADER(O_error_message,
                    O_work_header_id,
                    ALC_CONSTANTS_SQL.WORKSHEET_TYPE_ALLOC,
                    I_user_id) = FALSE then
      return FALSE;
   end if;

   if COMMON_SEARCH(O_error_message,
                    L_search_criteria,
                    FALSE,
                    I_what_if_ind) = FALSE then
      return FALSE;
   end if;

   if I_what_if_ind then

      --no inventory lookups needed for what-if, so just put all rows into alc_itemsearch_inv_gtt
      insert into alc_itemsearch_inv_gtt (item,
                                          diff_1,
                                          diff_2,
                                          diff_3,
                                          diff_4,
                                          item_desc,
                                          rollup_type,
                                          rollup_item,
                                          rollup_item_desc,
                                          multi_color_pack_ind,
                                          location,
                                          doc_no,
                                          source_type,
                                          avail_qty)
                                   select DISTINCT gtt.item,
                                          im.diff_1,
                                          im.diff_2,
                                          im.diff_3,
                                          im.diff_4,
                                          im.item_desc,
                                          gtt.rollup_type,
                                          gtt.rollup_item,
                                          gtt.rollup_item_desc,
                                          gtt.multi_color_pack_ind,
                                          -1,  --gtt.location,
                                          NULL,
                                          ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_WHAT_IF,
                                          0
                                     from alc_itemsearch_items_gtt gtt,
                                          item_master im
                                    where gtt.item = im.item;

   LOGGER.LOG_INFORMATION(L_program||' INSERT ALC_ITEMSEARCH_INV_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   else

      if GET_INVENTORY_FOR_SELECTION(O_error_message,
                                     L_search_criteria) = FALSE then
         return FALSE;
      end if;

   end if;

   insert into alc_work_item_source (work_item_source_id,
                                     work_header_id,
                                     item,
                                     item_type,
                                     ancestor_id,
                                     item_source_desc,
                                     available_quantity,
                                     wh,
                                     doc_no,
                                     source_type,
                                     refresh_ind,
                                     deleted_ind,
                                     expected_to_date,
                                     created_by,
                                     updated_by,
                                     created_date,
                                     updated_date,
                                     item_id,
                                     diff_1,
                                     diff_2,
                                     diff_3,
                                     disable_ind)
                              select alc_work_item_source_seq.NEXTVAL,
                                     O_work_header_id,
                                     gtt.item,
                                     im.alc_item_type,
                                     NULL,
                                     gtt.item_desc,
                                     gtt.avail_qty,
                                     gtt.location,
                                     gtt.doc_no,
                                     gtt.source_type,
                                     'Y',
                                     NULL,
                                     NULL,
                                     USER,
                                     USER,
                                     SYSDATE,
                                     SYSDATE,
                                     gtt.item,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL
                                from alc_itemsearch_inv_gtt gtt,
                                     item_master im
                               where gtt.item        = im.item;

   LOGGER.LOG_INFORMATION(L_program||' INSERT ALC_WORK_ITEM_SOURCE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if POPULATE_PACK_COMP(O_error_message,
                         O_work_header_id) = FALSE then
      return FALSE;
   end if;

   if ALC_HUB_FACET_CFG_SQL.POPULATE_WORK_ITEM_SOURCE_DIFF(O_error_message,
                                                           O_work_header_id) = FALSE then
      return FALSE;
   end if;
   --
   if VALIDATE_AWIS_ROWS_WORK_HEADER(O_error_message,
                                     O_work_header_id)= FALSE then
      return FALSE;
   end if;
   --

   O_alloc_type := 'SA';

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END QUICK_CREATE_WORKSHEET;
--------------------------------------------------------------------------
FUNCTION VALIDATE_AWIS_ROWS_WORK_HEADER(O_error_message    IN OUT VARCHAR2,
                                        I_work_header_id   IN ALC_WORK_HEADER.WORK_HEADER_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_ITEM_SEARCH_SQL.FIND_AWIS_ROWS_FOR_WORK_HDR';
   L_count_item_packs   NUMBER(10) := 0;

 BEGIN

   select COUNT(*)
     into L_count_item_packs
     from alc_work_item_source awis
    where awis.work_header_id = I_work_header_id;

   if L_count_item_packs = 0 then
      O_error_message:= ALC_CONSTANTS_SQL.ERRMSG_QUICK_CREATE_FOUND_NDA;     --(NDA = NO DATA AVALILABLE)
      return FALSE;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_AWIS_ROWS_WORK_HEADER;
--------------------------------------------------------------------------
END ALC_ITEM_SEARCH_SQL;
/

