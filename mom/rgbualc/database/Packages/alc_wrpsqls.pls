CREATE OR REPLACE PACKAGE ALC_WRAPPER_SQL AS
--------------------------------------------------------------------
FUNCTION GET_WH_CURRENT_AVAIL(I_item    IN ITEM_MASTER.ITEM%TYPE,
                              I_color   IN ITEM_MASTER.ITEM%TYPE,
                              I_wh      IN ITEM_LOC.LOC%TYPE)
RETURN ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
--------------------------------------------------------------------
FUNCTION CONVERT_UOM(O_error_message    IN OUT VARCHAR2,
                     O_to_value         IN OUT NUMBER,
                     I_to_uom           IN     UOM_CONVERSION.TO_UOM%TYPE,
                     I_from_value       IN     NUMBER,
                     I_from_uom         IN     UOM_CONVERSION.FROM_UOM%TYPE,
                     I_item             IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                     I_supplier         IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                     I_origin_country   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------
-- Name:    CONVERT
-- Purpose: Converts a value from one currency to another.
--          If NULL is passed in for I_currency or I_currency_out
--          the function will assume the primary currency.
--          For I_cost_retail_ind, pass 'C' if passing a cost,
--          'R' if passing a retail that does not need to be converted
--          to a 'smart' retail or 'P' for a retail that needs to be
--          'smart' by passing the value through calc_adjust.
--          If NULL is passed in for I_effective_date the function
--          will use today's date, otherwise pass in the date of
--          the currency rate to be used in the conversion.
--          If NULL is passed in for I_exchange_type,
--          the default on system_options will be used. If the
--          system_options.consolidation_ind = 'Y', use the
--          consolidated rate'C', otherwise use 'O' for the
--          operational rate.  If NULL is passed in for I_exchange_rate_in
--          or I_exchange_rate_out, then the exchange rate between
--          the in/out currency and the primary currency will be
--          retrieved and used.
-------------------------------------------------------------------------------------------
FUNCTION CONVERT_CURRENCY(O_error_message       IN OUT VARCHAR2,
                          I_currency_value      IN     NUMBER,
                          I_currency            IN     CURRENCY_RATES.CURRENCY_CODE%TYPE,
                          I_currency_out        IN     CURRENCY_RATES.CURRENCY_CODE%TYPE,
                          O_currency_value      IN OUT NUMBER,
                          I_cost_retail_ind     IN     VARCHAR2,
                          I_effective_date      IN     CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                          I_exchange_type       IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                          I_in_exchange_rate    IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                          I_out_exchange_rate   IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
FUNCTION GET_PACK_CURRENT_UNIT_RETAIL(I_location   IN ITEM_LOC.LOC%TYPE,
                                      I_loc_type   IN ITEM_LOC.LOC_TYPE%TYPE,
                                      I_item       IN ITEM_MASTER.ITEM%TYPE)
RETURN ITEM_LOC.UNIT_RETAIL%TYPE;
----------------------------------------------------------------------
-- Name:    REFRESH
-- Purpose: This function covers two functionalities.First it deletes
--          the records from  ALC_WORK_ITEM_SOURCE table if the item is
--          no more valid in ITEM_MASTER.Second,it refreshes the
--          available quantities for each item in  ALC_WORK_ITEM_SOURCE
--          from RMS tables.As item in  ALC_WORK_ITEM_SOURCE can belong to
--          various source type,there are several merge statement is written
--          for the same.This function does not return anything.
--          It updates the same table  ALC_WORK_ITEM_SOURCE.
--------------------------------------------------------------------------
FUNCTION  REFRESH(O_error_message   IN OUT VARCHAR2,
                  I_wk_id           IN     ALC_WORK_ITEM_SOURCE.WORK_HEADER_ID%TYPE)
RETURN NUMBER ;
--------------------------------------------------------------------------
FUNCTION GET_PRICING_INFO(O_error_msg             OUT VARCHAR2,
                          O_prices                OUT ALC_RETAIL_INFO_TBL,
                          I_search_criteria    IN     OBJ_PRICE_INQ_SEARCH_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_FRANCHISE_CREDIT(O_error_message   IN OUT VARCHAR2,
                                   I_alloc_id        IN ALC_ALLOC.ALLOC_ID%TYPE)
RETURN NUMBER;
---------------------------------------------------------------------------------
FUNCTION CREATE_MOD_LOCPO(O_error_message     IN OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                          O_locpotsfref_rec   OUT NOCOPY  "RIB_LocPOTsfRef_REC",
                          I_locpodesc_rec     IN          "RIB_LocPOTsfDesc_REC",
                          I_action            IN          VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------------------------------
END ALC_WRAPPER_SQL;
/
