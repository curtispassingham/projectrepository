CREATE OR REPLACE PACKAGE BODY ALC_FILTER_LOC_SQL AS
-------------------------------------------------------------------------------------------------------------
FUNCTION FILTER(O_error_message      IN OUT VARCHAR2,
                I_facet_session_id   IN     ALC_SESSION_ITEM_LOC.FACET_SESSION_ID%TYPE,
                I_filter_type        IN     VARCHAR2,
                I_locations          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_FILTER_LOC_SQL.FILTER';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if RESET_FILTER(O_error_message,
                   I_facet_session_id,
                   I_filter_type,
                   'Y') = 0 then
      return ALC_CONSTANTS_SQL.FAILURE;
   end if;

   merge into alc_session_item_loc target
   using (select DISTINCT inner.item_loc_session_id
            from (select il.item_loc_session_id
                    from table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input,
                         alc_session_item_loc il
                   where il.facet_session_id = I_facet_session_id
                     and value(input)        = il.location_id) inner
          ) use_this
   on (target.item_loc_session_id = use_this.item_loc_session_id)
   when MATCHED then
      update
         set target.result_filter_ind          = DECODE(I_filter_type,
                                                        ALC_CONSTANTS_SQL.FILTER_TYPE_RESULT, 'N',
                                                        target.result_filter_ind),
             target.quantity_limits_filter_ind = DECODE(I_filter_type,
                                                        ALC_CONSTANTS_SQL.FILTER_TYPE_QTY_LIMITS, 'N',
                                                        target.quantity_limits_filter_ind);

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_SESSION_ITEM_LOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END FILTER;
-------------------------------------------------------------------------------------------------------------
FUNCTION RESET_FILTER(O_error_message      IN OUT VARCHAR2,
                      I_facet_session_id   IN     ALC_SESSION_ITEM_LOC.FACET_SESSION_ID%TYPE,
                      I_filter_type        IN     VARCHAR2,
                      I_filter_value       IN     VARCHAR2)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_FILTER_LOC_SQL.RESET_FILTER';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   update alc_session_item_loc il
      set il.result_filter_ind          = DECODE(I_filter_type,
                                                 ALC_CONSTANTS_SQL.FILTER_TYPE_RESULT, I_filter_value,
                                                 il.result_filter_ind),
          il.quantity_limits_filter_ind = DECODE(I_filter_type,
                                                 ALC_CONSTANTS_SQL.FILTER_TYPE_QTY_LIMITS, I_filter_value,
                                                 il.quantity_limits_filter_ind)
    where facet_session_id = I_facet_session_id;

   LOGGER.LOG_INFORMATION(L_program||' Update ALC_SESSION_ITEM_LOC - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END RESET_FILTER;
-------------------------------------------------------------------------------------------------------------
END ALC_FILTER_LOC_SQL;
/
