CREATE OR REPLACE PACKAGE BODY ALC_FREQUENCY_SCHEDULE_SQL AS
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ALLOC_SCHEDULE_INFO(O_error_msg             IN OUT VARCHAR2,
                                 L_alloc_shcedule_info      OUT OBJ_ALLOC_SCHEDULE_INFO_TBL)
RETURN NUMBER IS

   L_program       VARCHAR2(100) := 'ALC_FREQUENCY_SCHEDULE_SQL.GET_ALLOC_SCHEDULE_INFO';
   L_currentdate   DATE          := TRUNC(SYSDATE);
   L_currentday    VARCHAR2(10)  := TRIM(TO_CHAR(L_currentdate, 'DAY'));
   L_daystart      NUMBER(1)     := TO_CHAR(L_currentdate,'D');
   L_temp_date     DATE;
   L_start_time    TIMESTAMP     := SYSTIMESTAMP;

   L_exist_in_fortnight   BOOLEAN := NULL;

   -- Cursor picks Schedule frequency records which have current date between Start date and End date
   -- and for which no Child Allocation exists or not created today
   cursor C_GET_ALLOC_SCHEDULE_INFO is
      select schedule_id,
             parent_alloc_id,
             start_date,
             end_date,
             frequency,
             days_of_week,
             action
        from alc_schedule sch,
             alc_alloc alloc
       where sch.parent_alloc_id = alloc.alloc_id
         and alloc.parent_allocation = 'Y'
         and L_currentdate BETWEEN sch.start_date and sch.end_date
         and NOT EXISTS (select alloc_id
                           from alc_alloc
                          where parent_id            is NOT NULL
                            and parent_id            = alloc.alloc_id
                            and TRUNC(created_date) = L_currentdate)
      order by schedule_id;

   TYPE L_alc_schedule_rec is TABLE OF C_GET_ALLOC_SCHEDULE_INFO%ROWTYPE;
   L_alc_schedule_tbl      L_alc_schedule_rec;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   open C_GET_ALLOC_SCHEDULE_INFO;
   fetch C_GET_ALLOC_SCHEDULE_INFO BULK COLLECT into L_alc_schedule_tbl;
   close C_GET_ALLOC_SCHEDULE_INFO;

   L_alloc_shcedule_info := OBJ_ALLOC_SCHEDULE_INFO_TBL();

   -- check if any records exist before iteration
   if L_alc_schedule_tbl.COUNT > 0 then

   -- if the current day is SUNDAY then instead of checking the first index of daysOfWeek value
   -- which is for Monday, value at 7th index is enquired.
   if L_currentday = 'SUNDAY' then
      L_daystart := 7;
   else
      L_daystart := L_daystart - 1;
   end if;

   -- iterate through the object created which has the records got from the Cursor
   for i IN L_alc_schedule_tbl.FIRST .. L_alc_schedule_tbl.LAST loop

      -- Check that if any schedule is to be picked for today
      if SUBSTR(TRIM(L_alc_schedule_tbl(i).days_of_week), L_daystart, 1) = '1' then

         -- if the scheduling happens weekly
         if L_alc_schedule_tbl(i).frequency = 'W' then
            L_alloc_shcedule_info.EXTEND();
            L_alloc_shcedule_info(L_alloc_shcedule_info.COUNT) := NEW OBJ_ALLOC_SCHEDULE_INFO_REC(L_alc_schedule_tbl(i).schedule_id,
                                                                                                  L_alc_schedule_tbl(i).parent_alloc_id,
                                                                                                  L_alc_schedule_tbl(i).start_date,
                                                                                                  L_alc_schedule_tbl(i).end_date,
                                                                                                  L_alc_schedule_tbl(i).frequency,
                                                                                                  L_alc_schedule_tbl(i).days_of_week,
                                                                                                  L_alc_schedule_tbl(i).action);

         -- check if scheduling happens Biweekly
         elsif L_alc_schedule_tbl(i).frequency = 'B' then
            L_temp_date := L_alc_schedule_tbl(i).start_date;

            loop
               L_exist_in_fortnight := false;

               -- increment L_temp_date which has start date stored as initial value by 14 days to consider first biweek(fortnight)
               -- and to consider biweek as unit for scheduling for further iterations
               L_temp_date := L_temp_date + 14;

               -- check if current date is in the first biweek
               if L_currentdate < L_temp_date then
                  L_exist_in_fortnight := true;
                  -- check if current date is in first part of the biweek since we consider first part of every biweek (i.e first 7 days)
                  -- from the start date
                  if L_currentdate < (L_temp_date - 7) then

                     L_alloc_shcedule_info.EXTEND();
                     L_alloc_shcedule_info(L_alloc_shcedule_info.COUNT) := NEW OBJ_ALLOC_SCHEDULE_INFO_REC(L_alc_schedule_tbl(i).schedule_id,
                                                                                                           L_alc_schedule_tbl(i).parent_alloc_id,
                                                                                                           L_alc_schedule_tbl(i).start_date,
                                                                                                           L_alc_schedule_tbl(i).end_date,
                                                                                                           L_alc_schedule_tbl(i).frequency,
                                                                                                           L_alc_schedule_tbl(i).days_of_week,
                                                                                                           L_alc_schedule_tbl(i).action);
                  end if;
               end if;
               -- check whether temp date which is incremented by 14 from start date for every iteration is greater than
               -- end date. If so, exit from loop
               EXIT WHEN(L_temp_date > L_alc_schedule_tbl(i).end_date or L_exist_in_fortnight);
            end loop;
         end if; -- end of frequency if elsif condition
      end if; -- end of check whether any schedule is to be picked for today
   end loop;

   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   -- return 1 if function runs successfully without any exceptions
   return 1;

EXCEPTION
  when OTHERS then
     O_error_msg := SQLERRM || L_program;
     -- return 0 if there is any exception occured while executing the function.
     -- Get the exception message from the first OUT param which is stored in O_error_msg from the calling function
     return 0;

END GET_ALLOC_SCHEDULE_INFO;
-----------------------------------------------------------------------------------------------------------------------
END ALC_FREQUENCY_SCHEDULE_SQL;
/