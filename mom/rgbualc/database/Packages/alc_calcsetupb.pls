CREATE OR REPLACE PACKAGE BODY ALC_CALC_SETUP_SQL AS

LP_use_sister_store   ALC_SYSTEM_OPTIONS.TP_SISTER_STORE_NULL%TYPE := NULL;
LP_alloc_type         ALC_ALLOC.TYPE%TYPE                          := NULL;

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_DESTINATION_ITEMS(O_error_message   IN OUT VARCHAR2,
                                 I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_DESTINATION(O_error_message              IN OUT VARCHAR2,
                           I_alloc_id                   IN     ALC_ALLOC.ALLOC_ID%TYPE,
                           I_enforce_wh_store_rel_ind   IN     ALC_ALLOC.ENFORCE_WH_STORE_REL_IND%TYPE,
                           I_alc_rule_row               IN     ALC_RULE%ROWTYPE,
                           I_alc_calc_packs_tbl         IN     ALC_CALC_PACKS_TBL,
                           I_pack_range                 IN     VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-- FLEXIBLE SUPPLY CHAIN FUNCTIONS
FUNCTION SETUP_WH_PRIORITY(O_error_message        IN OUT VARCHAR2,
                           I_alloc_id             IN     ALC_ALLOC.ALLOC_ID%TYPE,
                           I_alc_calc_packs_tbl   IN     ALC_CALC_PACKS_TBL,
                           I_pack_range           IN     VARCHAR2)

RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_FLEXIBLE_SUPPLY_CHAIN(O_error_message        IN OUT VARCHAR2,
                                     I_alloc_id             IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_calc_packs_tbl   IN     ALC_CALC_PACKS_TBL,
                                     I_pack_range           IN     VARCHAR2,
                                     I_transfer_basis       IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_ITEM_LOC_RULE_PRIORITY(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_STORE_WH_RULE_PRIORITY(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_SAMEORG_SAMECHANNEL_RULE(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_SAMEORG_DIFFCHANNEL_RULE(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_DIFFORG_SAMECHANNEL_RULE(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_DIFFORG_DIFFCHANNEL_RULE(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
FUNCTION POP_INV_BUCKETS(O_error_message   IN OUT VARCHAR2,
                         I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                         I_all_orders      IN     VARCHAR2,
                         I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_RLOH(O_error_message   IN OUT VARCHAR2,
                  I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                  I_all_orders      IN     VARCHAR2,
                  I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RLOH_EOD_SNAPSHOT(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_all_orders      IN     VARCHAR2,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RLOH_ITEMS(O_error_message   IN OUT VARCHAR2,
                        I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                        I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION RLOH_ITEMS_FOR_HIERARCHIES(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION RLOH_ITEMS_FOR_ITEMS(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ALT_HIER_RLOH_ITEMS(O_error_message   IN OUT VARCHAR2,
                                 I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION RLOH_AH_ITEMS_FOR_HIERARCHIES(O_error_message   IN OUT VARCHAR2,
                                       I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION RLOH_AH_ITEMS_FOR_ITEM(O_error_message   IN OUT VARCHAR2,
                                I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RLOH_INV(O_error_message   IN OUT VARCHAR2,
                      I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                      I_all_orders      IN     VARCHAR2,
                      I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RLOH_HIER_LVL_INV(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_all_orders      IN     VARCHAR2,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_SIZE_PROFILE(O_error_message   IN OUT VARCHAR2,
                          I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                          I_alc_rule_row    IN     ALC_RULE%ROWTYPE,
                          O_expand_pack     IN OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_NEED_DATES(O_error_message   IN OUT VARCHAR2,
                        I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                        I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_SELLING_CURVE(O_error_message   IN OUT VARCHAR2,
                           I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                           I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_EXCLUSIONS(O_error_message          IN OUT VARCHAR2,
                        I_alloc_id               IN     ALC_ALLOC.ALLOC_ID%TYPE,
                        I_enforce_wh_store_rel   IN     ALC_ALLOC.ENFORCE_WH_STORE_REL_IND%TYPE,
                        I_alc_rule_row           IN     ALC_RULE%ROWTYPE,
                        I_alc_calc_packs_tbl     IN     ALC_CALC_PACKS_TBL,
                        I_expand_pack            IN     VARCHAR2,
                        I_pack_range             IN     VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_QTY_LIMITS(O_error_message   IN OUT VARCHAR2,
                        I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                        I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED(O_error_message   IN OUT VARCHAR2,
                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RULE_HIST_EOW_DATES(O_error_message    IN OUT VARCHAR2,
                                 I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                 I_alc_rule_row     IN     ALC_RULE%ROWTYPE,
                                 I_weight_pct       IN     ALC_RULE_MANY_TO_ONE.WEIGHT_PCT%TYPE,
                                 I_many_to_one_id   IN     ALC_RULE_MANY_TO_ONE.MANY_TO_ONE_ID%TYPE DEFAULT -1)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_DATE_RANGE_TY_LY(O_error_message       IN OUT VARCHAR2,
                              I_alloc_id            IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row        IN     ALC_RULE%ROWTYPE,
                              I_many_to_one_id      IN     ALC_RULE_MANY_TO_ONE.MANY_TO_ONE_ID%TYPE,
                              I_starting_eow_date   IN     DATE,
                              I_num_weeks           IN     ALC_RULE_MANY_TO_ONE.WK_FROM_TODAY_THIS_YR%TYPE,
                              I_weight_pct          IN     ALC_RULE_MANY_TO_ONE.WEIGHT_PCT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_DATE_RANGE_FWD_BCK(O_error_message       IN OUT VARCHAR2,
                                I_alloc_id            IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                I_alc_rule_row        IN     ALC_RULE%ROWTYPE,
                                I_many_to_one_id      IN     ALC_RULE_MANY_TO_ONE.MANY_TO_ONE_ID%TYPE,
                                I_starting_eow_date   IN     DATE,
                                I_num_forward         IN     ALC_RULE_MANY_TO_ONE.WK_FROM_TODAY_THIS_YR%TYPE,
                                I_num_back            IN     ALC_RULE_MANY_TO_ONE.WK_FROM_TODAY_THIS_YR%TYPE,
                                I_weight_pct          IN     ALC_RULE_MANY_TO_ONE.WEIGHT_PCT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_AH_RULE_HIST_EOW_DATES(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_DATE_RANGE_HIST_EOW_DATES(O_error_message    IN OUT VARCHAR2,
                                       I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_many_to_one_id   IN     ALC_RULE_MANY_TO_ONE.MANY_TO_ONE_ID%TYPE,
                                       I_start_date       IN     DATE,
                                       I_end_date         IN     DATE,
                                       I_weight_pct       IN     ALC_RULE_MANY_TO_ONE.WEIGHT_PCT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SEED_NEED_GTT(O_error_message   IN OUT VARCHAR2,
                       I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                       I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--SALES HIST FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_DEPT(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_CLASS(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_SUBCLASS(O_error_message   IN OUT VARCHAR2,
                                        I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                        I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_STYLE(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_PDIFF(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_TRAN(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--FORECAST FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST(O_error_message   IN OUT VARCHAR2,
                         I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                         I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_DEPT(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_CLASS(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_SUBCLASS(O_error_message   IN OUT VARCHAR2,
                                  I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                  I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_TRAN(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_STYLE(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_PDIFF(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--PLAN FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN(O_error_message   IN OUT VARCHAR2,
                         I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                         I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_DEPT(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_CLASS(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_SUBCLASS(O_error_message   IN OUT VARCHAR2,
                                  I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                  I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_ITEM(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_STYLE(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_PDIFF(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--PLAN REPROJECT FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN(O_error_message   IN OUT VARCHAR2,
                           I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                           I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_IWOS(O_error_message   IN OUT VARCHAR2,
                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_DEPT(O_error_message      IN OUT VARCHAR2,
                                I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                I_current_eow_date   IN     DATE,
                                I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_CLASS(O_error_message      IN OUT VARCHAR2,
                                 I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                 I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                 I_current_eow_date   IN     DATE,
                                 I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_SUBCLASS(O_error_message      IN OUT VARCHAR2,
                                    I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                    I_current_eow_date   IN     DATE,
                                    I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_STYLE(O_error_message      IN OUT VARCHAR2,
                                 I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                 I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                 I_current_eow_date   IN     DATE,
                                 I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_ITEM(O_error_message      IN OUT VARCHAR2,
                                I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                I_current_eow_date   IN     DATE,
                                I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_PDIFF(O_error_message      IN OUT VARCHAR2,
                                 I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                 I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                 I_current_eow_date   IN     DATE,
                                 I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--RECEIPT_PLAN FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN(O_error_message   IN OUT VARCHAR2,
                                 I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                 I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_DEPT(O_error_message   IN OUT VARCHAR2,
                                      I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_CLASS(O_error_message   IN OUT VARCHAR2,
                                       I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_SBC(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_ITEM(O_error_message   IN OUT VARCHAR2,
                                      I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_STYLE(O_error_message   IN OUT VARCHAR2,
                                       I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_PDIFF(O_error_message   IN OUT VARCHAR2,
                                       I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--CORP RULES FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_DEPT(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_CLASS(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_SBC(O_error_message   IN OUT VARCHAR2,
                                   I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                   I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_STYLE(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_ITEM(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_PDIFF(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_ALT_HIER(O_error_message   IN OUT VARCHAR2,
                             I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                             I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SEED_NEED_ALT_HIER_GTT(O_error_message   IN OUT VARCHAR2,
                                I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_ALT_HIER_DUPS(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION FINALIZE_OUTPUT(O_error_message   IN OUT VARCHAR2,
                         I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                         I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ALLOC_EOW_DATE(O_error_message   IN OUT VARCHAR2,
                            O_eow_date        IN OUT DATE,
                            I_date            IN     DATE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-- RECEIPT PLAN AND PACK FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_PACK_LEVEL_TBL(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION PACK_LEVEL_EXCLUSIONS(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_PACK_LEVEL_NEED(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_PACK_WH_PRIORITY(O_error_message   IN OUT VARCHAR2,
                                I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)

RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_PACK_FLEXIBLE_SUP_CHAIN(O_error_message    IN OUT VARCHAR2,
                                       I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_LOC_RULE_PRIORITY(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)

RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_SUPPLY_CHAIN_RULE(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_SAME_ORGCHANNEL_RULE(O_error_message    IN OUT VARCHAR2,
                                       I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_SAMEORG_DIFFCHANNEL(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_DIFFORG_SAMECHANNEL(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_DIFFORG_DIFFCHANNEL(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_CALCULATION(O_error_message        IN OUT VARCHAR2,
                           I_alloc_id             IN     ALC_ALLOC.ALLOC_ID%TYPE,
                           I_all_orders           IN     VARCHAR2,
                           I_alc_calc_packs_tbl   IN     ALC_CALC_PACKS_TBL)
RETURN NUMBER IS

   L_program                    VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_CALCULATION';
   L_alc_rule_row               ALC_RULE%ROWTYPE;
   L_expand_pack                VARCHAR2(1)  := 'N';
   L_fp_pack_range              ALC_SYSTEM_OPTIONS.FP_PACK_RANGING%TYPE;
   L_enforce_wh_store_rel_ind   ALC_ALLOC.ENFORCE_WH_STORE_REL_IND%TYPE;

BEGIN

   LOGGER.LOG_INFORMATION('START: '||L_program||' - I_alloc_id:'||I_alloc_id);

   if CLEANUP_WORKING_TABLES(O_error_message,
                             I_alloc_id) = 0 then
      return 0;
   end if;

   select tp_sister_store_null,
          fp_pack_ranging
     into LP_use_sister_store,
          L_fp_pack_range
     from alc_system_options;

   select type,
          enforce_wh_store_rel_ind
     into LP_alloc_type,
          L_enforce_wh_store_rel_ind
     from alc_alloc
    where alloc_id = I_alloc_id;

   select *
     into L_alc_rule_row
     from alc_rule
    where alloc_id = I_alloc_id;

   if SETUP_DESTINATION_ITEMS(O_error_message,
                              I_alloc_id) = FALSE then
      return 0;
   end if;

   if SETUP_DESTINATION(O_error_message,
                        I_alloc_id,
                        L_enforce_wh_store_rel_ind,
                        L_alc_rule_row,
                        I_alc_calc_packs_tbl,
                        L_fp_pack_range) = FALSE then
      return 0;
   end if;

   if POP_NEED_DATES(O_error_message,
                     I_alloc_id,
                     L_alc_rule_row) = FALSE then
      return 0;
   end if;

   if POP_SIZE_PROFILE(O_error_message,
                       I_alloc_id,
                       L_alc_rule_row,
                       L_expand_pack) = FALSE then
      return 0;
   end if;

   if POP_SELLING_CURVE(O_error_message,
                        I_alloc_id,
                        L_alc_rule_row) = FALSE then
      return 0;
   end if;

   if POP_EXCLUSIONS(O_error_message,
                     I_alloc_id,
                     L_enforce_wh_store_rel_ind,
                     L_alc_rule_row,
                     I_alc_calc_packs_tbl,
                     L_expand_pack,
                     L_fp_pack_range) = FALSE then
      return 0;
   end if;

   if POP_INV_BUCKETS(O_error_message,
                      I_alloc_id,
                      I_all_orders,
                      L_alc_rule_row) = FALSE then
      return 0;
   end if;

   if GET_QTY_LIMITS(O_error_message,
                     I_alloc_id,
                     L_alc_rule_row) = FALSE then
      return 0;
   end if;

   if POP_RLOH(O_error_message,
               I_alloc_id,
               I_all_orders,
               L_alc_rule_row) = FALSE then
      return 0;
   end if;

   if L_alc_rule_row.rule_level is NOT NULL then

      if SETUP_NEED(O_error_message,
                    I_alloc_id,
                    L_alc_rule_row) = FALSE then
         return 0;
      end if;

   else

      if SETUP_NEED_ALT_HIER(O_error_message,
                             I_alloc_id,
                             L_alc_rule_row) = FALSE then
         return 0;
      end if;

   end if;

   if FINALIZE_OUTPUT(O_error_message,
                      I_alloc_id,
                      L_alc_rule_row) = FALSE then
      return 0;
   end if;

   LOGGER.LOG_INFORMATION('END: '||L_program||' - I_alloc_id:'||I_alloc_id);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END SETUP_CALCULATION;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_CALC_RULE_LEVEL_PACK(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN NUMBER IS

   L_program        VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_CALC_RULE_LEVEL_PACK';
   L_alc_rule_row   ALC_RULE%ROWTYPE;

BEGIN

   if CLEANUP_WORKING_TABLES(O_error_message,
                             I_alloc_id) = 0 then
      return 0;
   end if;

   select *
     into L_alc_rule_row
     from alc_rule
    where alloc_id = I_alloc_id;

   if SETUP_PACK_LEVEL_TBL(O_error_message,
                           I_alloc_id) = FALSE then
      return 0;
   end if;

   if PACK_LEVEL_EXCLUSIONS(O_error_message,
                            I_alloc_id,
                            L_alc_rule_row) = FALSE then
      return 0;
   end if;

   if SETUP_PACK_LEVEL_NEED(O_error_message,
                            I_alloc_id,
                            L_alc_rule_row) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END SETUP_CALC_RULE_LEVEL_PACK;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_DESTINATION_ITEMS(O_error_message   IN OUT VARCHAR2,
                                 I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_DESTINATION_ITEMS';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   --style or style-color
   --explode out the diffs for the source item (if any) non-packs
   insert into alc_calc_source_temp (alloc_id,
                                     item_source_id,
                                     release_date,
                                     --item_type,
                                     source_item,
                                     source_item_level,
                                     source_tran_level,
                                     source_pack_ind,
                                     source_diff1_id,
                                     source_diff2_id,
                                     source_diff3_id,
                                     source_diff4_id,
                                     tran_item,
                                     tran_item_level,
                                     tran_tran_level,
                                     tran_pack_ind,
                                     tran_diff1_id,
                                     tran_diff2_id,
                                     tran_diff3_id,
                                     tran_diff4_id,
                                     dept,
                                     class,
                                     subclass)
                              select DISTINCT inner.alloc_id,
                                     inner.item_source_id,
                                     inner.release_date,
                                     --inner.item_type,
                                     inner.source_item_id,
                                     inner.source_item_level,
                                     inner.source_tran_level,
                                     inner.source_pack_ind,
                                     inner.source_diff1_id,
                                     inner.source_diff2_id,
                                     inner.source_diff3_id,
                                     inner.source_diff4_id,
                                     im2.item       tran_item,
                                     im2.item_level tran_item_level,
                                     im2.tran_level tran_tran_level,
                                     im2.pack_ind   tran_pack_ind,
                                     im2.diff_1  tran_diff1_id,
                                     im2.diff_2  tran_diff2_id,
                                     im2.diff_3  tran_diff3_id,
                                     im2.diff_4  tran_diff4_id,
                                     inner.dept,
                                     inner.class,
                                     inner.subclass
                                from item_master im2,
                                     (select i2.alloc_id,
                                             i2.item_source_id,
                                             i2.release_date,
                                             i2.item_type,
                                             i2.item_id           source_item_id,
                                             imparent.item_level  source_item_level,
                                             imparent.tran_level  source_tran_level,
                                             imparent.pack_ind    source_pack_ind,
                                             imparent.dept,
                                             imparent.class,
                                             imparent.subclass,
                                             --
                                             case
                                                when i2.one_pos = 1 then
                                                   i2.one_value
                                                when i2.two_pos = 1 then
                                                   i2.two_value
                                                when i2.three_pos = 1 then
                                                   i2.three_value
                                             else
                                                NULL
                                             end source_diff1_id,
                                             --
                                             case
                                                when i2.one_pos = 2 then
                                                   i2.one_value
                                                when i2.two_pos = 2 then
                                                   i2.two_value
                                                when i2.three_pos = 2 then
                                                   i2.three_value
                                                else
                                                   NULL
                                             end source_diff2_id,
                                             --
                                             case
                                                when i2.one_pos = 3 then
                                                   i2.one_value
                                                when i2.two_pos = 3 then
                                                   i2.two_value
                                                when i2.three_pos = 3 then
                                                   i2.three_value
                                                else
                                                   NULL
                                             end source_diff3_id,
                                             --
                                             case
                                                when i2.one_pos = 4 then
                                                   i2.one_value
                                                when i2.two_pos = 4 then
                                                   i2.two_value
                                                when i2.three_pos = 4 then
                                                   i2.three_value
                                                else
                                                   NULL
                                             end source_diff4_id
                                        from item_master imparent,
                                             (select i1.alloc_id,
                                                     i1.item_source_id,
                                                     i1.release_date,
                                                     i1.item_type,
                                                     i1.item_id,
                                                     SUBSTR(i1.one,1,1) one_pos,
                                                     SUBSTR(i1.one,3) one_value,
                                                     SUBSTR(i1.two,1,1) two_pos,
                                                     SUBSTR(i1.two,3) two_value,
                                                     SUBSTR(i1.three,1,1) three_pos,
                                                     SUBSTR(i1.three,3) three_value
                                                from (select DISTINCT ais.alloc_id,
                                                             ais.item_source_id,
                                                             ais.release_date,
                                                             ais.item_type,
                                                             ais.item_id,
                                                             SUBSTR(ais.diff1_id||':', 1, INSTR(ais.diff1_id||':',':')-1) one,
                                                             SUBSTR(ais.diff1_id||'::', INSTR(ais.diff1_id||'::', ':') +1,
                                                                     INSTR(ais.diff1_id||'::', ':', 1, 2 )-INSTR(ais.diff1_id||'::',':')-1) two,
                                                             RTRIM(SUBSTR(ais.diff1_id||'::', INSTR(ais.diff1_id||'::',':',1,2)+1),':') three
                                                        from alc_item_source ais
                                                       where ais.alloc_id  = I_alloc_id
                                                         and ais.item_type = ALC_CONSTANTS_SQL.FASHIONITEM
                                                     ) i1) i2
                                       where i2.item_id = imparent.item) inner
                               where inner.source_item_id    = im2.item_parent
                                 and NVL(inner.source_diff1_id, NVL(im2.diff_1,'-999'))   = NVL(im2.diff_1,'-999')
                                 and NVL(inner.source_diff2_id, NVL(im2.diff_2,'-999'))   = NVL(im2.diff_2,'-999')
                                 and NVL(inner.source_diff3_id, NVL(im2.diff_3,'-999'))   = NVL(im2.diff_3,'-999')
                                 and NVL(inner.source_diff4_id, NVL(im2.diff_4,'-999'))   = NVL(im2.diff_4,'-999');

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_SOURCE_TEMP - Style/Style_color - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Tran level items
   insert into alc_calc_source_temp (alloc_id,
                                     item_source_id,
                                     release_date,
                                     source_item,
                                     source_item_level,
                                     source_tran_level,
                                     source_pack_ind,
                                     source_diff1_id,
                                     source_diff2_id,
                                     source_diff3_id,
                                     source_diff4_id,
                                     tran_item,
                                     tran_item_level,
                                     tran_tran_level,
                                     tran_pack_ind,
                                     tran_diff1_id,
                                     tran_diff2_id,
                                     tran_diff3_id,
                                     tran_diff4_id,
                                     dept,
                                     class,
                                     subclass)
                              select DISTINCT s.alloc_id,
                                     s.item_source_id,
                                     s.release_date,
                                     NVL(p.item, s.item_id) source_item,
                                     NVL(p.item_level, im.item_level) source_item_level,
                                     NVL(p.tran_level, im.tran_level) source_tran_level,
                                     NVL(p.pack_ind, im.pack_ind) source_pack_ind,
                                     DECODE(p.diff_1_aggregate_ind,
                                            'N', NULL,
                                            im.diff_1) source_diff1_id,
                                     DECODE(p.diff_2_aggregate_ind,
                                            'N', NULL,
                                            im.diff_2) source_diff2_id,
                                     DECODE(p.diff_3_aggregate_ind,
                                            'N', NULL,
                                            im.diff_3) source_diff3_id,
                                     DECODE(p.diff_4_aggregate_ind,
                                            'N', NULL,
                                            im.diff_4) source_diff4_id,
                                     --
                                     im.item tran_item,
                                     im.item_level tran_item_level,
                                     im.tran_level tran_tran_level,
                                     im.pack_ind tran_pack_ind,
                                     im.diff_1 tran_diff1_id,
                                     im.diff_2 tran_diff2_id,
                                     im.diff_3 tran_diff3_id,
                                     im.diff_4 tran_diff4_id,
                                     --
                                     im.dept,
                                     im.class,
                                     im.subclass
                                from alc_item_source s,
                                     item_master im,
                                     item_master p
                               where s.alloc_id  = I_alloc_id
                                 and s.item_type in (ALC_CONSTANTS_SQL.STAPLEITEM,
                                                     ALC_CONSTANTS_SQL.FASHIONSKU,
                                                     ALC_CONSTANTS_SQL.PACKCOMP)
                                 and s.item_id   = im.item
                                 and p.item(+)   = im.item_parent;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_SOURCE_TEMP - TRAN_LEVEL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Packs
   --assumes that only tran_level items will be components of the pack on the allocation
   insert into alc_calc_source_temp target (alloc_id,
                                            item_source_id,
                                            item_type,
                                            release_date,
                                            source_item,
                                            source_item_level,
                                            source_tran_level,
                                            source_pack_ind,
                                            source_diff1_id,
                                            source_diff2_id,
                                            source_diff3_id,
                                            source_diff4_id,
                                            tran_item,
                                            tran_item_level,
                                            tran_tran_level,
                                            tran_pack_ind,
                                            tran_diff1_id,
                                            tran_diff2_id,
                                            tran_diff3_id,
                                            tran_diff4_id,
                                            dept,
                                            class,
                                            subclass,
                                            pack_no)
                                     select DISTINCT s.alloc_id,
                                            s.item_source_id,
                                            s.item_type,
                                            s.release_date,
                                            --
                                            NVL(p.item, im.item) source_item,
                                            NVL(p.item_level, im.item_level) source_item_level,
                                            NVL(p.tran_level, im.tran_level) source_tran_level,
                                            NVL(p.pack_ind, im.pack_ind) source_pack_ind,
                                            DECODE(p.diff_1_aggregate_ind,
                                                   'N', NULL,
                                                   im.diff_1) source_diff1_id,
                                            DECODE(p.diff_2_aggregate_ind,
                                                   'N', NULL,
                                                   im.diff_2) source_diff2_id,
                                            DECODE(p.diff_3_aggregate_ind,
                                                   'N', NULL,
                                                   im.diff_3) source_diff3_id,
                                            DECODE(p.diff_4_aggregate_ind,
                                                   'N', NULL,
                                                   im.diff_4) source_diff4_id,
                                            --
                                            im.item tran_item,
                                            im.item_level tran_item_level,
                                            im.tran_level tran_tran_level,
                                            im.pack_ind tran_pack_ind,
                                            im.diff_1 tran_diff1_id,
                                            im.diff_2 tran_diff2_id,
                                            im.diff_3 tran_diff3_id,
                                            im.diff_4 tran_diff4_id,
                                            im.dept,
                                            im.class,
                                            im.subclass,
                                            pi.pack_no pack_no
                                       from alc_item_source s,
                                            packitem_breakout pi,
                                            item_master im,
                                            item_master p
                                      where s.alloc_id = I_alloc_id
                                        and s.item_type in (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                            ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK)
                                        and s.item_id = pi.pack_no
                                        and pi.item   = im.item
                                        and p.item(+) = im.item_parent
                                      union all
                                     select DISTINCT s.alloc_id,
                                            s.item_source_id,
                                            s.item_type,
                                            s.release_date,
                                            s.item_id source_item,
                                            im.item_level source_item_level,
                                            im.tran_level source_tran_level,
                                            im.pack_ind source_pack_ind,
                                            NULL source_diff1_id,
                                            NULL source_diff2_id,
                                            NULL source_diff3_id,
                                            NULL source_diff4_id,
                                            im.item tran_item,
                                            im.item_level tran_item_level,
                                            im.tran_level tran_tran_level,
                                            im.pack_ind tran_pack_ind,
                                            im.diff_1 tran_diff1_id,
                                            im.diff_2 tran_diff2_id,
                                            im.diff_3 tran_diff3_id,
                                            im.diff_4 tran_diff4_id,
                                            im.dept,
                                            im.class,
                                            im.subclass,
                                            s.item_id pack_no
                                       from alc_item_source s,
                                            item_master im
                                      where s.alloc_id  = I_alloc_id
                                        and s.item_type = ALC_CONSTANTS_SQL.SELLABLEPACK
                                        and s.item_id   = im.item;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_SOURCE_TEMP - PACK - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_DESTINATION_ITEMS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_DESTINATION(O_error_message              IN OUT VARCHAR2,
                           I_alloc_id                   IN     ALC_ALLOC.ALLOC_ID%TYPE,
                           I_enforce_wh_store_rel_ind   IN     ALC_ALLOC.ENFORCE_WH_STORE_REL_IND%TYPE,
                           I_alc_rule_row               IN     ALC_RULE%ROWTYPE,
                           I_alc_calc_packs_tbl         IN     ALC_CALC_PACKS_TBL,
                           I_pack_range                 IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_DESTINATION';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   ---------------------------------------------------------------
   -- For ASSIGN_DEFAULT_WH
   -- 1. If I_enforce_wh_store_rel_ind is set
   --     a. Get the source_wh based on the source_method and assign as the assign_default_wh
   --     b. Otherwise, get the default_wh from the STORE or WH table.
   -- 2. If I_enforce_wh_store_rel_ind is NOT set, then only get the source_wh to cover Rule 1
   --    for Flexible Supply Chain.
   ---------------------------------------------------------------

   insert into alc_calc_allitemloc_temp (alloc_id,
                                         item_type,
                                         source_item,
                                         source_item_level,
                                         source_tran_level,
                                         source_pack_ind,
                                         source_diff1_id,
                                         source_diff2_id,
                                         source_diff3_id,
                                         source_diff4_id,
                                         tran_item,
                                         tran_item_level,
                                         tran_tran_level,
                                         tran_pack_ind,
                                         tran_diff1_id,
                                         tran_diff2_id,
                                         tran_diff3_id,
                                         tran_diff4_id,
                                         dept,
                                         class,
                                         subclass,
                                         to_loc,
                                         to_loc_type,
                                         to_loc_name,
                                         sister_store,
                                         assign_default_wh,
                                         clear_ind,
                                         item_loc_status,
                                         size_profile_qty,
                                         total_profile_qty,
                                         stock_on_hand,
                                         on_order,
                                         on_alloc,
                                         in_transit_qty)
                                  select DISTINCT inner.alloc_id,
                                         inner.item_type,
                                         inner.source_item,
                                         inner.source_item_level,
                                         inner.source_tran_level,
                                         inner.source_pack_ind,
                                         inner.source_diff1_id,
                                         inner.source_diff2_id,
                                         inner.source_diff3_id,
                                         inner.source_diff4_id,
                                         inner.tran_item,
                                         inner.tran_item_level,
                                         inner.tran_tran_level,
                                         inner.tran_pack_ind,
                                         inner.tran_diff1_id,
                                         inner.tran_diff2_id,
                                         inner.tran_diff3_id,
                                         inner.tran_diff4_id,
                                         inner.dept,
                                         inner.class,
                                         inner.subclass,
                                         inner.to_loc,
                                         inner.to_loc_type,
                                         inner.to_loc_name,
                                         inner.sister_store,
                                         inner.assign_default_wh,
                                         inner.clear_ind,
                                         inner.status,
                                         inner.size_profile_qty,
                                         inner.total_profile_qty,
                                         inner.stock_on_hand,
                                         inner.on_order,
                                         inner.on_alloc,
                                         inner.in_transit_qty
                                    from (with source_temp as
                                            (select DISTINCT cs.alloc_id,
                                                    cs.item_type,
                                                    cs.source_item,
                                                    cs.source_item_level,
                                                    cs.source_tran_level,
                                                    cs.source_pack_ind,
                                                    cs.source_diff1_id,
                                                    cs.source_diff2_id,
                                                    cs.source_diff3_id,
                                                    cs.source_diff4_id,
                                                    cs.tran_item,
                                                    cs.tran_item_level,
                                                    cs.tran_tran_level,
                                                    cs.tran_pack_ind,
                                                    cs.tran_diff1_id,
                                                    cs.tran_diff2_id,
                                                    cs.tran_diff3_id,
                                                    cs.tran_diff4_id,
                                                    cs.dept,
                                                    cs.class,
                                                    cs.subclass,
                                                    cs.pack_no,
                                                    s.loc to_loc,
                                                    s.loc_type to_loc_type,
                                                    s.loc_name to_loc_name,
                                                    s.sister_store,
                                                    s.default_wh,
                                                    case
                                                       when cs.item_type is NOT NULL then
                                                          DECODE(I_pack_range,
                                                                 'P', cs.pack_no,
                                                                 cs.tran_item)
                                                       else
                                                          cs.tran_item
                                                    end as join_item
                                               from alc_calc_source_temp cs,
                                                    alc_loc_group alg,
                                                    alc_location al,
                                                    (select s.store loc,
                                                            'S' loc_type,
                                                            s.store_name loc_name,
                                                            s.sister_store,
                                                            s.default_wh
                                                       from store s
                                                     union all
                                                     select w.wh loc,
                                                            'W' loc_type,
                                                            w.wh_name loc_name,
                                                            NULL sister_store,
                                                            w.default_wh
                                                       from wh w) s
                                              where cs.alloc_id     = I_alloc_id
                                                and alg.alloc_id    = cs.alloc_id
                                                and al.loc_group_id = alg.loc_group_id
                                                and s.loc           = al.location_id),
                                          pack_loc as
                                            (select packloc.item,
                                                    packloc.loc,
                                                    packloc.clear_ind
                                               from source_temp st,
                                                    item_loc packloc
                                              where st.alloc_id   = I_alloc_id
                                                and st.pack_no    = packloc.item
                                                and st.to_loc     = packloc.loc
                                                and I_pack_range  = 'C')
                                                ---
                                            select /*+ USE_NL_WITH_INDEX(il, PK_ITEM_LOC)*/
                                                   src.alloc_id,
                                                   src.item_type,
                                                   src.source_item,
                                                   src.source_item_level,
                                                   src.source_tran_level,
                                                   src.source_pack_ind,
                                                   src.source_diff1_id,
                                                   src.source_diff2_id,
                                                   src.source_diff3_id,
                                                   src.source_diff4_id,
                                                   src.tran_item,
                                                   src.tran_item_level,
                                                   src.tran_tran_level,
                                                   src.tran_pack_ind,
                                                   src.tran_diff1_id,
                                                   src.tran_diff2_id,
                                                   src.tran_diff3_id,
                                                   src.tran_diff4_id,
                                                   src.dept,
                                                   src.class,
                                                   src.subclass,
                                                   src.to_loc,
                                                   src.to_loc_type,
                                                   src.to_loc_name,
                                                   src.sister_store,
                                                   case
                                                      when I_enforce_wh_store_rel_ind = 'Y' then
                                                         NVL(DECODE(iloc.source_method,
                                                                    'W', iloc.source_wh),
                                                                    src.default_wh)
                                                      else
                                                         DECODE(iloc.source_method,
                                                                'W', iloc.source_wh,
                                                                NULL)
                                                      end as assign_default_wh,
                                                   case
                                                      when src.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                                             ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                                             ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                                             ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                                             ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                                                             ALC_CONSTANTS_SQL.SELLABLEPACK) then
                                                         NVL(DECODE(I_pack_range,
                                                                    'C', p.clear_ind,
                                                                    iloc.clear_ind), 'N')
                                                      else
                                                         iloc.clear_ind
                                                      end as clear_ind,
                                                   iloc.status,
                                                   NULL size_profile_qty,
                                                   NULL total_profile_qty,
                                                   NULL stock_on_hand,
                                                   NULL on_order,
                                                   NULL on_alloc,
                                                   NULL in_transit_qty
                                              from source_temp src,
                                                   item_loc iloc,
                                                   pack_loc p
                                             where src.alloc_id  = I_alloc_id
                                               and src.join_item = iloc.item(+)
                                               and src.to_loc    = iloc.loc(+)
                                               and src.pack_no   = p.item(+)
                                               and src.to_loc    = p.loc(+)) inner;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_ALLITEMLOC_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- update default_wh and status for sellable packs when pack_ranging is set to C
   merge into alc_calc_allitemloc_temp target
   using (select DISTINCT acat.alloc_id,
                 acat.tran_item,
                 acat.to_loc,
                 case
                    when I_enforce_wh_store_rel_ind = 'Y' then
                       NVL(DECODE(il.source_method,
                                  'W', il.source_wh),
                                  loc.default_wh)
                    else
                       DECODE(il.source_method,
                              'W', il.source_wh,
                              NULL)
                    end as assign_default_wh,
                 il.status
            from alc_calc_allitemloc_temp acat,
                 item_loc il,
                 packitem_breakout pack,
                 v_alloc_supply_chain loc
           where acat.alloc_id  = I_alloc_id
             and acat.to_loc    = loc.location
             and acat.to_loc    = il.loc
             and acat.tran_item = pack.pack_no
             and il.item        = pack.item
             and acat.item_type = ALC_CONSTANTS_SQL.SELLABLEPACK
             and I_pack_range   = 'C') source
   on (    target.alloc_id  = source.alloc_id
       and target.tran_item = source.tran_item
       and target.to_loc    = source.to_loc)
   when MATCHED then
      update
         set target.assign_default_wh = source.assign_default_wh,
             target.item_loc_status   = source.status;

   LOGGER.LOG_INFORMATION(L_program||' Merge SELLABLE PACK - ALC_CALC_ALLITEMLOC_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if I_enforce_wh_store_rel_ind = 'N' then

      if SETUP_WH_PRIORITY(O_error_message,
                           I_alloc_id,
                           I_alc_calc_packs_tbl,
                           I_pack_range) = FALSE then
         return FALSE;
      end if;

   end if;

   -- delete frozen
   delete /*+ INDEX(tmp, ALC_CALC_ALLITEMLOC_TEMP_I4) */
     from alc_calc_allitemloc_temp tmp
    where tmp.alloc_id = I_alloc_id
      and (tmp.tran_item, tmp.to_loc) IN (select i.item_id,
                                                 i.location_id
                                            from (select DISTINCT ail.alloc_id,
                                                         ail.item_id,
                                                         ail.location_id,
                                                         COUNT(*) OVER (PARTITION BY ail.alloc_id,
                                                                                     ail.item_id,
                                                                                     ail.location_id) cnt,
                                                         SUM(DECODE(ail.freeze_ind,
                                                                    'Y',1,
                                                                    0)) OVER (PARTITION BY ail.alloc_id,
                                                                                           ail.item_id,
                                                                                           ail.location_id) fcnt
                                                    from (select ail.alloc_id,
                                                                 ail.item_id,
                                                                 ail.location_id,
                                                                 ail.freeze_ind
                                                            from alc_item_loc ail
                                                           where alloc_id = I_alloc_id
                                                           union all
                                                          select ail.alloc_id,
                                                                 pb.item,
                                                                 ail.location_id,
                                                                 ail.freeze_ind
                                                            from alc_item_loc ail,
                                                                 packitem_breakout pb
                                                           where alloc_id = I_alloc_id
                                                             and pb.pack_no = ail.item_id
                                                             and ail.item_type != 'SELLPACK') ail) i
                                           where i.cnt = i.fcnt);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_CALC_ALLITEMLOC_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_DESTINATION;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_WH_PRIORITY(O_error_message        IN OUT VARCHAR2,
                           I_alloc_id             IN     ALC_ALLOC.ALLOC_ID%TYPE,
                           I_alc_calc_packs_tbl   IN     ALC_CALC_PACKS_TBL,
                           I_pack_range           IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program                       VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_WH_PRIORITY';
   L_start_time                    TIMESTAMP    := SYSTIMESTAMP;
   L_intercompany_transfer_basis   INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE;

   cursor C_GET_TRANSFER_BASIS is
      select intercompany_transfer_basis
        from inv_move_unit_options;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   delete
     from alc_calc_wh_rule_priority
    where alloc_id = I_alloc_id;

   LOGGER.LOG_INFORMATION(L_program||' DELETE ALC_CALC_WH_RULE_PRIORITY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from alc_calc_flex_supply_chain_gtt
    where alloc_id = I_alloc_id;

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_CALC_FLEX_SUPPLY_CHAIN_GTT - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   open C_GET_TRANSFER_BASIS;
   fetch C_GET_TRANSFER_BASIS into L_intercompany_transfer_basis;
   close C_GET_TRANSFER_BASIS;

   if SETUP_FLEXIBLE_SUPPLY_CHAIN(O_error_message,
                                  I_alloc_id,
                                  I_alc_calc_packs_tbl,
                                  I_pack_range,
                                  L_intercompany_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   if SET_ITEM_LOC_RULE_PRIORITY(O_error_message,
                                 I_alloc_id) = FALSE then
      return FALSE;
   end if;

   if SET_STORE_WH_RULE_PRIORITY(O_error_message,
                                 I_alloc_id) = FALSE then
      return FALSE;
   end if;

   if SET_SAMEORG_SAMECHANNEL_RULE(O_error_message,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   if SET_SAMEORG_DIFFCHANNEL_RULE(O_error_message,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   if SET_DIFFORG_SAMECHANNEL_RULE(O_error_message,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   if SET_DIFFORG_DIFFCHANNEL_RULE(O_error_message,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_WH_PRIORITY;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_FLEXIBLE_SUPPLY_CHAIN(O_error_message        IN OUT VARCHAR2,
                                     I_alloc_id             IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_calc_packs_tbl   IN     ALC_CALC_PACKS_TBL,
                                     I_pack_range           IN     VARCHAR2,
                                     I_transfer_basis       IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_FLEXIBLE_SUPPLY_CHAIN';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   -- Style or Style/color
   insert into alc_calc_flex_supply_chain_gtt(alloc_id,
                                              source_item,
                                              source_diff1_id,
                                              source_diff2_id,
                                              source_diff3_id,
                                              source_diff4_id,
                                              tran_item,
                                              tran_diff1_id,
                                              tran_diff2_id,
                                              tran_diff3_id,
                                              tran_diff4_id,
                                              to_loc,
                                              to_loc_type,
                                              alloc_source_wh_id,
                                              assigned_ranged_src_wh,
                                              to_loc_default_wh,
                                              org_unit_id,
                                              channel_id,
                                              tsf_entity_id,
                                              set_of_books_id,
                                              transfer_basis)
                                       select DISTINCT inner.alloc_id,
                                              inner.source_item,
                                              inner.source_diff1_id,
                                              inner.source_diff2_id,
                                              inner.source_diff3_id,
                                              inner.source_diff4_id,
                                              inner.tran_item,
                                              inner.tran_diff1_id,
                                              inner.tran_diff2_id,
                                              inner.tran_diff3_id,
                                              inner.tran_diff4_id,
                                              acat.to_loc,
                                              acat.to_loc_type,
                                              inner.wh_id alloc_source_wh_id,
                                              acat.assign_default_wh assigned_ranged_src_wh,
                                              v.default_wh to_loc_default_wh,
                                              v.org_unit_id,
                                              v.channel_id,
                                              DECODE(I_transfer_basis,
                                                     ALC_CONSTANTS_SQL.BASED_ON_TRANSFER, v.tsf_entity_id,
                                                     NULL) tsf_entity_id,
                                              DECODE(I_transfer_basis,
                                                     ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS, v.set_of_books_id,
                                                     NULL) set_of_books_id,
                                              I_transfer_basis
                                         from (select ais.alloc_id,
                                                      ais.item_source_id,
                                                      src.item_type,
                                                      src.source_item,
                                                      src.source_diff1_id,
                                                      src.source_diff2_id,
                                                      src.source_diff3_id,
                                                      src.source_diff4_id,
                                                      src.tran_item,
                                                      src.tran_diff1_id,
                                                      src.tran_diff2_id,
                                                      src.tran_diff3_id,
                                                      src.tran_diff4_id,
                                                      src.pack_no,
                                                      ais.wh_id
                                                 from alc_calc_source_temp src,
                                                      alc_item_source ais
                                                where ais.alloc_id       = src.alloc_id
                                                  and ais.alloc_id       = I_alloc_id
                                                  and ais.item_source_id = src.item_source_id
                                                  and ais.item_id        = src.source_item
                                                  and ais.item_type      = ALC_CONSTANTS_SQL.FASHIONITEM) inner,
                                              alc_calc_allitemloc_temp acat,
                                              v_alloc_supply_chain v
                                         where acat.alloc_id               = inner.alloc_id
                                           and acat.source_item            = inner.source_item
                                           and acat.tran_item              = inner.tran_item
                                           and NVL(acat.item_type, '-999') = NVL(inner.item_type, '-999')
                                           and acat.to_loc                 = v.location
                                           and acat.to_loc_type            = v.loc_type;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_FLEX_SUPPLY_CHAIN_GTT - STYLE OR STYLECOLOR - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Tran Level Items
   insert into alc_calc_flex_supply_chain_gtt(alloc_id,
                                              source_item,
                                              source_diff1_id,
                                              source_diff2_id,
                                              source_diff3_id,
                                              source_diff4_id,
                                              tran_item,
                                              tran_diff1_id,
                                              tran_diff2_id,
                                              tran_diff3_id,
                                              tran_diff4_id,
                                              to_loc,
                                              to_loc_type,
                                              alloc_source_wh_id,
                                              assigned_ranged_src_wh,
                                              to_loc_default_wh,
                                              org_unit_id,
                                              channel_id,
                                              tsf_entity_id,
                                              set_of_books_id,
                                              transfer_basis)
                                       select DISTINCT inner.alloc_id,
                                              inner.source_item,
                                              inner.source_diff1_id,
                                              inner.source_diff2_id,
                                              inner.source_diff3_id,
                                              inner.source_diff4_id,
                                              inner.tran_item,
                                              inner.tran_diff1_id,
                                              inner.tran_diff2_id,
                                              inner.tran_diff3_id,
                                              inner.tran_diff4_id,
                                              acat.to_loc,
                                              acat.to_loc_type,
                                              inner.wh_id alloc_source_wh_id,
                                              acat.assign_default_wh assigned_ranged_src_wh,
                                              v.default_wh to_loc_default_wh,
                                              v.org_unit_id,
                                              v.channel_id,
                                              DECODE(I_transfer_basis,
                                                     ALC_CONSTANTS_SQL.BASED_ON_TRANSFER, v.tsf_entity_id,
                                                     NULL) tsf_entity_id,
                                              DECODE(I_transfer_basis,
                                                     ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS, v.set_of_books_id,
                                                     NULL) set_of_books_id,
                                              I_transfer_basis
                                         from (select ais.alloc_id,
                                                      ais.item_source_id,
                                                      src.item_type,
                                                      src.source_item,
                                                      src.source_diff1_id,
                                                      src.source_diff2_id,
                                                      src.source_diff3_id,
                                                      src.source_diff4_id,
                                                      src.tran_item,
                                                      src.tran_diff1_id,
                                                      src.tran_diff2_id,
                                                      src.tran_diff3_id,
                                                      src.tran_diff4_id,
                                                      src.pack_no,
                                                      ais.wh_id
                                                 from alc_calc_source_temp src,
                                                      alc_item_source ais
                                                where ais.alloc_id       = src.alloc_id
                                                  and ais.alloc_id       = I_alloc_id
                                                  and ais.item_source_id = src.item_source_id
                                                  and ais.item_id        = src.tran_item
                                                  and ais.item_type IN (ALC_CONSTANTS_SQL.STAPLEITEM,
                                                                        ALC_CONSTANTS_SQL.FASHIONSKU,
                                                                        ALC_CONSTANTS_SQL.PACKCOMP)) inner,
                                               alc_calc_allitemloc_temp acat,
                                               v_alloc_supply_chain v
                                         where acat.alloc_id    = inner.alloc_id
                                           and acat.source_item = inner.source_item
                                           and acat.tran_item   = inner.tran_item
                                           and NVL(acat.item_type, '-999') = NVL(inner.item_type, '-999')
                                           and acat.to_loc      = v.location
                                           and acat.to_loc_type = v.loc_type;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_FLEX_SUPPLY_CHAIN_GTT - TRAN LEVEL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Packs
   insert into alc_calc_flex_supply_chain_gtt(alloc_id,
                                              source_item,
                                              source_diff1_id,
                                              source_diff2_id,
                                              source_diff3_id,
                                              source_diff4_id,
                                              tran_item,
                                              tran_diff1_id,
                                              tran_diff2_id,
                                              tran_diff3_id,
                                              tran_diff4_id,
                                              to_loc,
                                              to_loc_type,
                                              alloc_source_wh_id,
                                              assigned_ranged_src_wh,
                                              to_loc_default_wh,
                                              org_unit_id,
                                              channel_id,
                                              tsf_entity_id,
                                              set_of_books_id,
                                              transfer_basis)
                                       select DISTINCT inner.alloc_id,
                                              NULL source_item,
                                              NULL source_diff1_id,
                                              NULL source_diff2_id,
                                              NULL source_diff3_id,
                                              NULL source_diff4_id,
                                              inner.pack_no,
                                              NULL tran_diff1_id,
                                              NULL tran_diff2_id,
                                              NULL tran_diff3_id,
                                              NULL tran_diff4_id,
                                              acat.to_loc,
                                              acat.to_loc_type,
                                              inner.wh_id alloc_source_wh_id,
                                              acat.assign_default_wh assigned_ranged_src_wh,
                                              v.default_wh to_loc_default_wh,
                                              v.org_unit_id,
                                              v.channel_id,
                                              DECODE(I_transfer_basis,
                                                     ALC_CONSTANTS_SQL.BASED_ON_TRANSFER, v.tsf_entity_id,
                                                     NULL) tsf_entity_id,
                                              DECODE(I_transfer_basis,
                                                     ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS, v.set_of_books_id,
                                                     NULL) set_of_books_id,
                                              I_transfer_basis
                                         from (select ais.alloc_id,
                                                      ais.item_source_id,
                                                      src.item_type,
                                                      src.source_item,
                                                      src.source_diff1_id,
                                                      src.source_diff2_id,
                                                      src.source_diff3_id,
                                                      src.source_diff4_id,
                                                      src.tran_item,
                                                      src.tran_diff1_id,
                                                      src.tran_diff2_id,
                                                      src.tran_diff3_id,
                                                      src.tran_diff4_id,
                                                      src.pack_no,
                                                      ais.wh_id
                                                 from alc_calc_source_temp src,
                                                      alc_item_source ais
                                                where ais.alloc_id       = src.alloc_id
                                                  and ais.alloc_id       = I_alloc_id
                                                  and ais.item_source_id = src.item_source_id
                                                  and ais.item_id        = src.pack_no
                                                  and ais.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                                        ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                                        ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                                        ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                                        ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                                                        ALC_CONSTANTS_SQL.SELLABLEPACK)
                                                  ) inner,
                                               alc_calc_allitemloc_temp acat,
                                               v_alloc_supply_chain v
                                         where acat.alloc_id               = inner.alloc_id
                                           and acat.source_item            = inner.source_item
                                           and acat.tran_item              = inner.tran_item
                                           and NVL(acat.item_type, '-999') = NVL(inner.item_type, '-999')
                                           and acat.to_loc                 = v.location
                                           and acat.to_loc_type            = v.loc_type;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_FLEX_SUPPLY_CHAIN_GTT - PACKS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- Insert the packs that belong to the Style for Fashion Allocation since packs within a Style
   -- also needs to be prioritized when Pack Ranging is set

   if LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_ALLOCATION then

      insert into alc_calc_flex_supply_chain_gtt(alloc_id,
                                                 source_item,
                                                 source_diff1_id,
                                                 source_diff2_id,
                                                 source_diff3_id,
                                                 source_diff4_id,
                                                 tran_item,
                                                 tran_diff1_id,
                                                 tran_diff2_id,
                                                 tran_diff3_id,
                                                 tran_diff4_id,
                                                 to_loc,
                                                 to_loc_type,
                                                 alloc_source_wh_id,
                                                 assigned_ranged_src_wh,
                                                 to_loc_default_wh,
                                                 org_unit_id,
                                                 channel_id,
                                                 tsf_entity_id,
                                                 set_of_books_id,
                                                 transfer_basis,
                                                 pack_no)
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.alloc_source_wh_id,
                                                 inner.assigned_ranged_src_wh,
                                                 inner.to_loc_default_wh,
                                                 inner.org_unit_id,
                                                 inner.channel_id,
                                                 inner.tsf_entity_id,
                                                 inner.set_of_books_id,
                                                 inner.transfer_basis,
                                                 inner.pack_no pack_no
                                            from (with src_pack_within_style as
                                                     (select I_alloc_id alloc_id,
                                                             NULL source_item,
                                                             NULL source_diff1_id,
                                                             NULL source_diff2_id,
                                                             NULL source_diff3_id,
                                                             NULL source_diff4_id,
                                                             p.item tran_item,
                                                             NULL tran_diff1_id,
                                                             NULL tran_diff2_id,
                                                             NULL tran_diff3_id,
                                                             NULL tran_diff4_id,
                                                             TO_NUMBER(al.location_id) to_loc,
                                                             al.location_type to_loc_type,
                                                             i.wh_id alloc_source_wh_id,
                                                             i.pack_no pack_no,
                                                             DECODE(I_pack_range,
                                                                    'P', i.pack_no,
                                                                    p.item) join_item
                                                        from table(cast(I_alc_calc_packs_tbl as ALC_CALC_PACKS_TBL)) i,
                                                             alc_loc_group alg,
                                                             alc_location al,
                                                             packitem_breakout p
                                                       where alg.alloc_id     = I_alloc_id
                                                         and alg.loc_group_id = al.loc_group_id
                                                         and p.pack_no        = i.pack_no)
                                                  --
                                                  select DISTINCT spws.alloc_id,
                                                         spws.source_item,
                                                         spws.source_diff1_id,
                                                         spws.source_diff2_id,
                                                         spws.source_diff3_id,
                                                         spws.source_diff4_id,
                                                         spws.pack_no tran_item,
                                                         spws.tran_diff1_id,
                                                         spws.tran_diff2_id,
                                                         spws.tran_diff3_id,
                                                         spws.tran_diff4_id,
                                                         spws.to_loc,
                                                         spws.to_loc_type,
                                                         spws.alloc_source_wh_id,
                                                         DECODE(il.source_method,
                                                                'W', il.source_wh,
                                                                NULL) assigned_ranged_src_wh,
                                                         v.default_wh to_loc_default_wh,
                                                         v.org_unit_id,
                                                         v.channel_id,
                                                         DECODE(I_transfer_basis,
                                                                ALC_CONSTANTS_SQL.BASED_ON_TRANSFER, v.tsf_entity_id,
                                                                NULL) tsf_entity_id,
                                                         DECODE(I_transfer_basis,
                                                                ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS, v.set_of_books_id,
                                                                NULL) set_of_books_id,
                                                         I_transfer_basis transfer_basis,
                                                         spws.pack_no pack_no
                                                    from src_pack_within_style spws,
                                                         item_loc il,
                                                         v_alloc_supply_chain v
                                                   where il.item     = spws.join_item
                                                     and il.loc      = spws.to_loc
                                                     and il.loc_type = spws.to_loc_type
                                                     and il.loc      = v.location
                                                     and il.loc_type = v.loc_type) inner;

      LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_FLEX_SUPPLY_CHAIN_GTT - PACKS UNDER STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_FLEXIBLE_SUPPLY_CHAIN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_ITEM_LOC_RULE_PRIORITY(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_ITEM_LOC_RULE_PRIORITY';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_calc_wh_rule_priority(alloc_id,
                                         source_item,
                                         source_diff1_id,
                                         source_diff2_id,
                                         source_diff3_id,
                                         source_diff4_id,
                                         tran_item,
                                         tran_diff1_id,
                                         tran_diff2_id,
                                         tran_diff3_id,
                                         tran_diff4_id,
                                         to_loc,
                                         to_loc_type,
                                         source_wh,
                                         rule_priority)
                                  select DISTINCT gtt.alloc_id,
                                         gtt.source_item,
                                         gtt.source_diff1_id,
                                         gtt.source_diff2_id,
                                         gtt.source_diff3_id,
                                         gtt.source_diff4_id,
                                         gtt.tran_item,
                                         gtt.tran_diff1_id,
                                         gtt.tran_diff2_id,
                                         gtt.tran_diff3_id,
                                         gtt.tran_diff4_id,
                                         gtt.to_loc,
                                         gtt.to_loc_type,
                                         gtt.assigned_ranged_src_wh,
                                         ALC_CONSTANTS_SQL.PRIORITY_ITEM_LOC rule_priority
                                    from alc_calc_flex_supply_chain_gtt gtt
                                   where gtt.alloc_id               = I_alloc_id
                                     and gtt.assigned_ranged_src_wh = gtt.alloc_source_wh_id
                                     and NOT EXISTS (select 'x'
                                                       from alc_calc_wh_rule_priority acw
                                                      where acw.alloc_id    = gtt.alloc_id
                                                        and acw.to_loc      = gtt.to_loc
                                                        and acw.to_loc_type = gtt.to_loc_type
                                                        and acw.tran_item   = gtt.tran_item
                                                        and acw.source_wh   = gtt.assigned_ranged_src_wh);

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_WH_RULE_PRIORITY - RULE 1 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_ITEM_LOC_RULE_PRIORITY;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_STORE_WH_RULE_PRIORITY(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_STORE_WH_RULE_PRIORITY';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_calc_wh_rule_priority(alloc_id,
                                         source_item,
                                         source_diff1_id,
                                         source_diff2_id,
                                         source_diff3_id,
                                         source_diff4_id,
                                         tran_item,
                                         tran_diff1_id,
                                         tran_diff2_id,
                                         tran_diff3_id,
                                         tran_diff4_id,
                                         to_loc,
                                         to_loc_type,
                                         source_wh,
                                         rule_priority)
                                  select DISTINCT gtt.alloc_id,
                                         gtt.source_item,
                                         gtt.source_diff1_id,
                                         gtt.source_diff2_id,
                                         gtt.source_diff3_id,
                                         gtt.source_diff4_id,
                                         gtt.tran_item,
                                         gtt.tran_diff1_id,
                                         gtt.tran_diff2_id,
                                         gtt.tran_diff3_id,
                                         gtt.tran_diff4_id,
                                         gtt.to_loc,
                                         gtt.to_loc_type,
                                         gtt.to_loc_default_wh,
                                         ALC_CONSTANTS_SQL.PRIORITY_DEFAULT_WH rule_priority
                                    from alc_calc_flex_supply_chain_gtt gtt
                                   where gtt.alloc_id          = I_alloc_id
                                     and gtt.to_loc_default_wh = gtt.alloc_source_wh_id
                                     and NOT EXISTS (select 'x'
                                                       from alc_calc_wh_rule_priority acw
                                                      where acw.alloc_id    = gtt.alloc_id
                                                        and acw.to_loc      = gtt.to_loc
                                                        and acw.to_loc_type = gtt.to_loc_type
                                                        and acw.tran_item   = gtt.tran_item
                                                        and acw.source_wh   = gtt.to_loc_default_wh);

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_WH_RULE_PRIORITY - RULE 2 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_STORE_WH_RULE_PRIORITY;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_SAMEORG_SAMECHANNEL_RULE(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_SAMEORG_SAMECHANNEL_RULE';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_calc_wh_rule_priority(alloc_id,
                                         source_item,
                                         source_diff1_id,
                                         source_diff2_id,
                                         source_diff3_id,
                                         source_diff4_id,
                                         tran_item,
                                         tran_diff1_id,
                                         tran_diff2_id,
                                         tran_diff3_id,
                                         tran_diff4_id,
                                         to_loc,
                                         to_loc_type,
                                         source_wh,
                                         rule_priority)
                                  select i.alloc_id,
                                         i.source_item,
                                         i.source_diff1_id,
                                         i.source_diff2_id,
                                         i.source_diff3_id,
                                         i.source_diff4_id,
                                         i.tran_item,
                                         i.tran_diff1_id,
                                         i.tran_diff2_id,
                                         i.tran_diff3_id,
                                         i.tran_diff4_id,
                                         i.to_loc,
                                         i.to_loc_type,
                                         i.assigned_priority_wh,
                                         i.rule_priority
                                    from (with same_org_same_channel as
                                             (select gtt.alloc_id,
                                                     gtt.source_item,
                                                     gtt.source_diff1_id,
                                                     gtt.source_diff2_id,
                                                     gtt.source_diff3_id,
                                                     gtt.source_diff4_id,
                                                     gtt.tran_item,
                                                     gtt.tran_diff1_id,
                                                     gtt.tran_diff2_id,
                                                     gtt.tran_diff3_id,
                                                     gtt.tran_diff4_id,
                                                     gtt.to_loc,
                                                     gtt.to_loc_type,
                                                     gtt.alloc_source_wh_id,
                                                     v_sc.location assigned_priority_wh,
                                                     v_sc.physical_wh,
                                                     v_sc.protected_ind
                                                from alc_calc_flex_supply_chain_gtt gtt,
                                                     v_alloc_supply_chain v_sc
                                               where gtt.alloc_id            = I_alloc_id
                                                 and gtt.org_unit_id         = v_sc.org_unit_id
                                                 and gtt.channel_id          = v_sc.channel_id
                                                 and (   gtt.tsf_entity_id   = DECODE(I_transfer_basis,
                                                                                      ALC_CONSTANTS_SQL.BASED_ON_TRANSFER, v_sc.tsf_entity_id,
                                                                                      gtt.tsf_entity_id)
                                                      or gtt.set_of_books_id = DECODE(I_transfer_basis,
                                                                                      ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS, v_sc.set_of_books_id,
                                                                                      gtt.set_of_books_id))
                                                 and v_sc.loc_type           = 'W'
                                                 and gtt.transfer_basis      = I_transfer_basis)
                                          ---------------------------------------------------------------
                                          -- RULE 3 - Primary VIRTUAL WH of PHYSICAL WH linked
                                          --          with the SAME ORG_UNIT_ID and CHANNEL_ID with PROTECTED_IND = 'N'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_SOSC_NOTPRTCT_PRIMARY rule_priority
                                            from (select sosc.alloc_id,
                                                         sosc.source_item,
                                                         sosc.source_diff1_id,
                                                         sosc.source_diff2_id,
                                                         sosc.source_diff3_id,
                                                         sosc.source_diff4_id,
                                                         sosc.tran_item,
                                                         sosc.tran_diff1_id,
                                                         sosc.tran_diff2_id,
                                                         sosc.tran_diff3_id,
                                                         sosc.tran_diff4_id,
                                                         sosc.to_loc,
                                                         sosc.to_loc_type,
                                                         sosc.alloc_source_wh_id,
                                                         sosc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY sosc.alloc_id,
                                                                                         sosc.source_item,
                                                                                         sosc.tran_item,
                                                                                         sosc.to_loc,
                                                                                         sosc.alloc_source_wh_id
                                                                                ORDER BY sosc.assigned_priority_wh) rank
                                                    from same_org_same_channel sosc,
                                                         wh w
                                                   where sosc.physical_wh          = w.wh
                                                     and sosc.assigned_priority_wh = w.primary_vwh
                                                     and sosc.protected_ind        = 'N'
                                                     and sosc.assigned_priority_wh = sosc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = sosc.alloc_id
                                                                        and acw.to_loc    = sosc.to_loc
                                                                        and acw.tran_item = sosc.tran_item
                                                                        and acw.source_wh = sosc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 4 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                          --          with the SAME ORG_UNIT_ID and CHANNEL_ID with PROTECTED_IND = 'N'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_SOSC_NOTPRTCT_NOTPRI rule_priority
                                            from (select sosc.alloc_id,
                                                         sosc.source_item,
                                                         sosc.source_diff1_id,
                                                         sosc.source_diff2_id,
                                                         sosc.source_diff3_id,
                                                         sosc.source_diff4_id,
                                                         sosc.tran_item,
                                                         sosc.tran_diff1_id,
                                                         sosc.tran_diff2_id,
                                                         sosc.tran_diff3_id,
                                                         sosc.tran_diff4_id,
                                                         sosc.to_loc,
                                                         sosc.to_loc_type,
                                                         sosc.alloc_source_wh_id,
                                                         sosc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY sosc.alloc_id,
                                                                                         sosc.source_item,
                                                                                         sosc.tran_item,
                                                                                         sosc.to_loc,
                                                                                         sosc.alloc_source_wh_id
                                                                                ORDER BY sosc.assigned_priority_wh) rank
                                                    from same_org_same_channel sosc,
                                                         wh w
                                                   where sosc.physical_wh           = w.wh
                                                     and sosc.assigned_priority_wh != w.primary_vwh
                                                     and sosc.protected_ind         = 'N'
                                                     and sosc.assigned_priority_wh  = sosc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = sosc.alloc_id
                                                                        and acw.to_loc    = sosc.to_loc
                                                                        and acw.tran_item = sosc.tran_item
                                                                        and acw.source_wh = sosc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 5 - Primary VIRTUAL WH of PHYSICAL WH linked
                                          --          with the SAME ORG_UNIT_ID and CHANNEL_ID with PROTECTED_IND = 'Y'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_SOSC_PROTECT_PRIMARY rule_priority
                                            from (select sosc.alloc_id,
                                                         sosc.source_item,
                                                         sosc.source_diff1_id,
                                                         sosc.source_diff2_id,
                                                         sosc.source_diff3_id,
                                                         sosc.source_diff4_id,
                                                         sosc.tran_item,
                                                         sosc.tran_diff1_id,
                                                         sosc.tran_diff2_id,
                                                         sosc.tran_diff3_id,
                                                         sosc.tran_diff4_id,
                                                         sosc.to_loc,
                                                         sosc.to_loc_type,
                                                         sosc.alloc_source_wh_id,
                                                         sosc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY sosc.alloc_id,
                                                                                         sosc.source_item,
                                                                                         sosc.tran_item,
                                                                                         sosc.to_loc,
                                                                                         sosc.alloc_source_wh_id
                                                                                ORDER BY sosc.assigned_priority_wh) rank
                                                    from same_org_same_channel sosc,
                                                         wh w
                                                   where sosc.physical_wh          = w.wh
                                                     and sosc.assigned_priority_wh = w.primary_vwh
                                                     and sosc.protected_ind        = 'Y'
                                                     and sosc.assigned_priority_wh = sosc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = sosc.alloc_id
                                                                        and acw.to_loc    = sosc.to_loc
                                                                        and acw.tran_item = sosc.tran_item
                                                                        and acw.source_wh = sosc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 6 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                          --          with the SAME ORG_UNIT_ID and CHANNEL_ID with PROTECTED_IND = 'Y'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_SOSC_PROTECT_NOTPRI rule_priority
                                            from (select sosc.alloc_id,
                                                         sosc.source_item,
                                                         sosc.source_diff1_id,
                                                         sosc.source_diff2_id,
                                                         sosc.source_diff3_id,
                                                         sosc.source_diff4_id,
                                                         sosc.tran_item,
                                                         sosc.tran_diff1_id,
                                                         sosc.tran_diff2_id,
                                                         sosc.tran_diff3_id,
                                                         sosc.tran_diff4_id,
                                                         sosc.to_loc,
                                                         sosc.to_loc_type,
                                                         sosc.alloc_source_wh_id,
                                                         sosc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY sosc.alloc_id,
                                                                                         sosc.source_item,
                                                                                         sosc.tran_item,
                                                                                         sosc.to_loc,
                                                                                         sosc.alloc_source_wh_id
                                                                                ORDER BY sosc.assigned_priority_wh) rank
                                                    from same_org_same_channel sosc,
                                                         wh w
                                                   where sosc.physical_wh           = w.wh
                                                     and sosc.assigned_priority_wh != w.primary_vwh
                                                     and sosc.protected_ind         = 'Y'
                                                     and sosc.assigned_priority_wh  = sosc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = sosc.alloc_id
                                                                        and acw.to_loc    = sosc.to_loc
                                                                        and acw.tran_item = sosc.tran_item
                                                                        and acw.source_wh = sosc.assigned_priority_wh)) inner
                                           where inner.rank = 1) i;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_WH_RULE_PRIORITY - RULE 3 to 6 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_SAMEORG_SAMECHANNEL_RULE;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_SAMEORG_DIFFCHANNEL_RULE(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_SAMEORG_DIFFCHANNEL_RULE';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_calc_wh_rule_priority(alloc_id,
                                         source_item,
                                         source_diff1_id,
                                         source_diff2_id,
                                         source_diff3_id,
                                         source_diff4_id,
                                         tran_item,
                                         tran_diff1_id,
                                         tran_diff2_id,
                                         tran_diff3_id,
                                         tran_diff4_id,
                                         to_loc,
                                         to_loc_type,
                                         source_wh,
                                         rule_priority)
                                  select i.alloc_id,
                                         i.source_item,
                                         i.source_diff1_id,
                                         i.source_diff2_id,
                                         i.source_diff3_id,
                                         i.source_diff4_id,
                                         i.tran_item,
                                         i.tran_diff1_id,
                                         i.tran_diff2_id,
                                         i.tran_diff3_id,
                                         i.tran_diff4_id,
                                         i.to_loc,
                                         i.to_loc_type,
                                         i.assigned_priority_wh,
                                         i.rule_priority
                                    from (with same_org_diff_channel as
                                             (select gtt.alloc_id,
                                                     gtt.source_item,
                                                     gtt.source_diff1_id,
                                                     gtt.source_diff2_id,
                                                     gtt.source_diff3_id,
                                                     gtt.source_diff4_id,
                                                     gtt.tran_item,
                                                     gtt.tran_diff1_id,
                                                     gtt.tran_diff2_id,
                                                     gtt.tran_diff3_id,
                                                     gtt.tran_diff4_id,
                                                     gtt.to_loc,
                                                     gtt.to_loc_type,
                                                     gtt.alloc_source_wh_id,
                                                     v_sc.location assigned_priority_wh,
                                                     v_sc.physical_wh,
                                                     v_sc.protected_ind
                                                from alc_calc_flex_supply_chain_gtt gtt,
                                                     v_alloc_supply_chain v_sc
                                               where gtt.alloc_id            = I_alloc_id
                                                 and gtt.org_unit_id         = v_sc.org_unit_id
                                                 and gtt.channel_id         != v_sc.channel_id
                                                 and (   gtt.tsf_entity_id   = DECODE(I_transfer_basis,
                                                                                      ALC_CONSTANTS_SQL.BASED_ON_TRANSFER, v_sc.tsf_entity_id,
                                                                                      gtt.tsf_entity_id)
                                                      or gtt.set_of_books_id = DECODE(I_transfer_basis,
                                                                                      ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS, v_sc.set_of_books_id,
                                                                                      gtt.set_of_books_id))
                                                 and v_sc.loc_type           = 'W'
                                                 and gtt.transfer_basis      = I_transfer_basis)
                                          ---------------------------------------------------------------
                                          -- RULE 7 - Primary VIRTUAL WH of PHYSICAL WH linked
                                          --          with the SAME ORG_UNIT_ID but DIFF CHANNEL_ID with PROTECTED_IND = 'N'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_SODC_NOTPRTCT_PRIMARY rule_priority
                                            from (select sodc.alloc_id,
                                                         sodc.source_item,
                                                         sodc.source_diff1_id,
                                                         sodc.source_diff2_id,
                                                         sodc.source_diff3_id,
                                                         sodc.source_diff4_id,
                                                         sodc.tran_item,
                                                         sodc.tran_diff1_id,
                                                         sodc.tran_diff2_id,
                                                         sodc.tran_diff3_id,
                                                         sodc.tran_diff4_id,
                                                         sodc.to_loc,
                                                         sodc.to_loc_type,
                                                         sodc.alloc_source_wh_id,
                                                         sodc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY sodc.alloc_id,
                                                                                         sodc.source_item,
                                                                                         sodc.tran_item,
                                                                                         sodc.to_loc
                                                                                ORDER BY sodc.alloc_source_wh_id,
                                                                                         sodc.assigned_priority_wh) rank
                                                    from same_org_diff_channel sodc,
                                                         wh w
                                                   where sodc.physical_wh          = w.wh
                                                     and sodc.assigned_priority_wh = w.primary_vwh
                                                     and sodc.protected_ind        = 'N'
                                                     and sodc.assigned_priority_wh = sodc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = sodc.alloc_id
                                                                        and acw.to_loc    = sodc.to_loc
                                                                        and acw.tran_item = sodc.tran_item
                                                                        and acw.source_wh = sodc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 8 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                          --          with the SAME ORG_UNIT_ID but DIFF CHANNEL_ID with PROTECTED_IND = 'N'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_SODC_NOTPRTCT_NOTPRI rule_priority
                                            from (select sodc.alloc_id,
                                                         sodc.source_item,
                                                         sodc.source_diff1_id,
                                                         sodc.source_diff2_id,
                                                         sodc.source_diff3_id,
                                                         sodc.source_diff4_id,
                                                         sodc.tran_item,
                                                         sodc.tran_diff1_id,
                                                         sodc.tran_diff2_id,
                                                         sodc.tran_diff3_id,
                                                         sodc.tran_diff4_id,
                                                         sodc.to_loc,
                                                         sodc.to_loc_type,
                                                         sodc.alloc_source_wh_id,
                                                         sodc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY sodc.alloc_id,
                                                                                         sodc.source_item,
                                                                                         sodc.tran_item,
                                                                                         sodc.to_loc
                                                                                ORDER BY sodc.alloc_source_wh_id,
                                                                                         sodc.assigned_priority_wh) rank
                                                    from same_org_diff_channel sodc,
                                                         wh w
                                                   where sodc.physical_wh           = w.wh
                                                     and sodc.assigned_priority_wh != w.primary_vwh
                                                     and sodc.protected_ind         = 'N'
                                                     and sodc.assigned_priority_wh  = sodc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = sodc.alloc_id
                                                                        and acw.to_loc    = sodc.to_loc
                                                                        and acw.tran_item = sodc.tran_item
                                                                        and acw.source_wh = sodc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 9 - Primary VIRTUAL WH of PHYSICAL WH linked
                                          --          with the SAME ORG_UNIT_ID but DIFF CHANNEL_ID with PROTECTED_IND = 'Y'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_SODC_PROTECT_PRIMARY rule_priority
                                            from (select sodc.alloc_id,
                                                         sodc.source_item,
                                                         sodc.source_diff1_id,
                                                         sodc.source_diff2_id,
                                                         sodc.source_diff3_id,
                                                         sodc.source_diff4_id,
                                                         sodc.tran_item,
                                                         sodc.tran_diff1_id,
                                                         sodc.tran_diff2_id,
                                                         sodc.tran_diff3_id,
                                                         sodc.tran_diff4_id,
                                                         sodc.to_loc,
                                                         sodc.to_loc_type,
                                                         sodc.alloc_source_wh_id,
                                                         sodc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY sodc.alloc_id,
                                                                                         sodc.source_item,
                                                                                         sodc.tran_item,
                                                                                         sodc.to_loc
                                                                                ORDER BY sodc.alloc_source_wh_id,
                                                                                         sodc.assigned_priority_wh) rank
                                                    from same_org_diff_channel sodc,
                                                         wh w
                                                   where sodc.physical_wh          = w.wh
                                                     and sodc.assigned_priority_wh = w.primary_vwh
                                                     and sodc.protected_ind        = 'Y'
                                                     and sodc.assigned_priority_wh = sodc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = sodc.alloc_id
                                                                        and acw.to_loc    = sodc.to_loc
                                                                        and acw.tran_item = sodc.tran_item
                                                                        and acw.source_wh = sodc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 10 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                          --           with the SAME ORG_UNIT_ID but DIFF CHANNEL_ID with PROTECTED_IND = 'Y'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_SODC_PROTECT_NOTPRI rule_priority
                                            from (select sodc.alloc_id,
                                                         sodc.source_item,
                                                         sodc.source_diff1_id,
                                                         sodc.source_diff2_id,
                                                         sodc.source_diff3_id,
                                                         sodc.source_diff4_id,
                                                         sodc.tran_item,
                                                         sodc.tran_diff1_id,
                                                         sodc.tran_diff2_id,
                                                         sodc.tran_diff3_id,
                                                         sodc.tran_diff4_id,
                                                         sodc.to_loc,
                                                         sodc.to_loc_type,
                                                         sodc.alloc_source_wh_id,
                                                         sodc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY sodc.alloc_id,
                                                                                         sodc.source_item,
                                                                                         sodc.tran_item,
                                                                                         sodc.to_loc
                                                                                ORDER BY sodc.alloc_source_wh_id,
                                                                                         sodc.assigned_priority_wh) rank
                                                    from same_org_diff_channel sodc,
                                                         wh w
                                                   where sodc.physical_wh           = w.wh
                                                     and sodc.assigned_priority_wh != w.primary_vwh
                                                     and sodc.protected_ind         = 'Y'
                                                     and sodc.assigned_priority_wh  = sodc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = sodc.alloc_id
                                                                        and acw.to_loc    = sodc.to_loc
                                                                        and acw.tran_item = sodc.tran_item
                                                                        and acw.source_wh = sodc.assigned_priority_wh)) inner
                                           where inner.rank = 1) i;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_WH_RULE_PRIORITY - RULE 7 to 10 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_SAMEORG_DIFFCHANNEL_RULE;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_DIFFORG_SAMECHANNEL_RULE(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)

RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_DIFFORG_SAMECHANNEL_RULE';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_calc_wh_rule_priority(alloc_id,
                                         source_item,
                                         source_diff1_id,
                                         source_diff2_id,
                                         source_diff3_id,
                                         source_diff4_id,
                                         tran_item,
                                         tran_diff1_id,
                                         tran_diff2_id,
                                         tran_diff3_id,
                                         tran_diff4_id,
                                         to_loc,
                                         to_loc_type,
                                         source_wh,
                                         rule_priority)
                                  select i.alloc_id,
                                         i.source_item,
                                         i.source_diff1_id,
                                         i.source_diff2_id,
                                         i.source_diff3_id,
                                         i.source_diff4_id,
                                         i.tran_item,
                                         i.tran_diff1_id,
                                         i.tran_diff2_id,
                                         i.tran_diff3_id,
                                         i.tran_diff4_id,
                                         i.to_loc,
                                         i.to_loc_type,
                                         i.assigned_priority_wh,
                                         i.rule_priority
                                    from (with diff_org_same_channel as
                                             (select gtt.alloc_id,
                                                     gtt.source_item,
                                                     gtt.source_diff1_id,
                                                     gtt.source_diff2_id,
                                                     gtt.source_diff3_id,
                                                     gtt.source_diff4_id,
                                                     gtt.tran_item,
                                                     gtt.tran_diff1_id,
                                                     gtt.tran_diff2_id,
                                                     gtt.tran_diff3_id,
                                                     gtt.tran_diff4_id,
                                                     gtt.to_loc,
                                                     gtt.to_loc_type,
                                                     gtt.alloc_source_wh_id,
                                                     v_sc.location assigned_priority_wh,
                                                     v_sc.physical_wh,
                                                     v_sc.protected_ind
                                                from alc_calc_flex_supply_chain_gtt gtt,
                                                     v_alloc_supply_chain v_sc
                                               where gtt.alloc_id            = I_alloc_id
                                                 and gtt.org_unit_id        != v_sc.org_unit_id
                                                 and gtt.channel_id          = v_sc.channel_id
                                                 and (   gtt.tsf_entity_id   = DECODE(I_transfer_basis,
                                                                                      ALC_CONSTANTS_SQL.BASED_ON_TRANSFER, v_sc.tsf_entity_id,
                                                                                      gtt.tsf_entity_id)
                                                      or gtt.set_of_books_id = DECODE(I_transfer_basis,
                                                                                      ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS, v_sc.set_of_books_id,
                                                                                      gtt.set_of_books_id))
                                                 and v_sc.loc_type           = 'W'
                                                 and gtt.transfer_basis      = I_transfer_basis)
                                          ---------------------------------------------------------------
                                          -- RULE 11 - Primary VIRTUAL WH of PHYSICAL WH linked
                                          --           with the DIFF ORG_UNIT_ID but SAME CHANNEL_ID with PROTECTED_IND = 'N'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_DOSC_NOTPRTCT_PRIMARY rule_priority
                                            from (select dosc.alloc_id,
                                                         dosc.source_item,
                                                         dosc.source_diff1_id,
                                                         dosc.source_diff2_id,
                                                         dosc.source_diff3_id,
                                                         dosc.source_diff4_id,
                                                         dosc.tran_item,
                                                         dosc.tran_diff1_id,
                                                         dosc.tran_diff2_id,
                                                         dosc.tran_diff3_id,
                                                         dosc.tran_diff4_id,
                                                         dosc.to_loc,
                                                         dosc.to_loc_type,
                                                         dosc.alloc_source_wh_id,
                                                         dosc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY dosc.alloc_id,
                                                                                         dosc.source_item,
                                                                                         dosc.tran_item,
                                                                                         dosc.to_loc
                                                                                ORDER BY dosc.alloc_source_wh_id,
                                                                                         dosc.assigned_priority_wh) rank
                                                    from diff_org_same_channel dosc,
                                                         wh w
                                                   where dosc.physical_wh          = w.wh
                                                     and dosc.assigned_priority_wh = w.primary_vwh
                                                     and dosc.protected_ind        = 'N'
                                                     and dosc.assigned_priority_wh = dosc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = dosc.alloc_id
                                                                        and acw.to_loc    = dosc.to_loc
                                                                        and acw.tran_item = dosc.tran_item
                                                                        and acw.source_wh = dosc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 12 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                          --           with the DIFF ORG_UNIT_ID but SAME CHANNEL_ID with PROTECTED_IND = 'N'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_DOSC_NOTPRTCT_NOTPRI rule_priority
                                            from (select dosc.alloc_id,
                                                         dosc.source_item,
                                                         dosc.source_diff1_id,
                                                         dosc.source_diff2_id,
                                                         dosc.source_diff3_id,
                                                         dosc.source_diff4_id,
                                                         dosc.tran_item,
                                                         dosc.tran_diff1_id,
                                                         dosc.tran_diff2_id,
                                                         dosc.tran_diff3_id,
                                                         dosc.tran_diff4_id,
                                                         dosc.to_loc,
                                                         dosc.to_loc_type,
                                                         dosc.alloc_source_wh_id,
                                                         dosc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY dosc.alloc_id,
                                                                                         dosc.source_item,
                                                                                         dosc.tran_item,
                                                                                         dosc.to_loc
                                                                                ORDER BY dosc.alloc_source_wh_id,
                                                                                         dosc.assigned_priority_wh) rank
                                                    from diff_org_same_channel dosc,
                                                         wh w
                                                   where dosc.physical_wh           = w.wh
                                                     and dosc.assigned_priority_wh != w.primary_vwh
                                                     and dosc.protected_ind         = 'N'
                                                     and dosc.assigned_priority_wh  = dosc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = dosc.alloc_id
                                                                        and acw.to_loc    = dosc.to_loc
                                                                        and acw.tran_item = dosc.tran_item
                                                                        and acw.source_wh = dosc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 13 - Primary VIRTUAL WH of PHYSICAL WH linked
                                          --           with the DIFF ORG_UNIT_ID but SAME CHANNEL_ID with PROTECTED_IND = 'Y'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_DOSC_PROTECT_PRIMARY rule_priority
                                            from (select dosc.alloc_id,
                                                         dosc.source_item,
                                                         dosc.source_diff1_id,
                                                         dosc.source_diff2_id,
                                                         dosc.source_diff3_id,
                                                         dosc.source_diff4_id,
                                                         dosc.tran_item,
                                                         dosc.tran_diff1_id,
                                                         dosc.tran_diff2_id,
                                                         dosc.tran_diff3_id,
                                                         dosc.tran_diff4_id,
                                                         dosc.to_loc,
                                                         dosc.to_loc_type,
                                                         dosc.alloc_source_wh_id,
                                                         dosc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY dosc.alloc_id,
                                                                                         dosc.source_item,
                                                                                         dosc.tran_item,
                                                                                         dosc.to_loc
                                                                                ORDER BY dosc.alloc_source_wh_id,
                                                                                         dosc.assigned_priority_wh) rank
                                                    from diff_org_same_channel dosc,
                                                         wh w
                                                   where dosc.physical_wh          = w.wh
                                                     and dosc.assigned_priority_wh = w.primary_vwh
                                                     and dosc.protected_ind        = 'Y'
                                                     and dosc.assigned_priority_wh = dosc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = dosc.alloc_id
                                                                        and acw.to_loc    = dosc.to_loc
                                                                        and acw.tran_item = dosc.tran_item
                                                                        and acw.source_wh = dosc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 14 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                          --           with the DIFF ORG_UNIT_ID but SAME CHANNEL_ID with PROTECTED_IND = 'Y'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_DOSC_PROTECT_NOTPRI rule_priority
                                            from (select dosc.alloc_id,
                                                         dosc.source_item,
                                                         dosc.source_diff1_id,
                                                         dosc.source_diff2_id,
                                                         dosc.source_diff3_id,
                                                         dosc.source_diff4_id,
                                                         dosc.tran_item,
                                                         dosc.tran_diff1_id,
                                                         dosc.tran_diff2_id,
                                                         dosc.tran_diff3_id,
                                                         dosc.tran_diff4_id,
                                                         dosc.to_loc,
                                                         dosc.to_loc_type,
                                                         dosc.alloc_source_wh_id,
                                                         dosc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY dosc.alloc_id,
                                                                                         dosc.source_item,
                                                                                         dosc.tran_item,
                                                                                         dosc.to_loc
                                                                                ORDER BY dosc.alloc_source_wh_id,
                                                                                         dosc.assigned_priority_wh) rank
                                                    from diff_org_same_channel dosc,
                                                         wh w
                                                   where dosc.physical_wh           = w.wh
                                                     and dosc.assigned_priority_wh != w.primary_vwh
                                                     and dosc.protected_ind         = 'Y'
                                                     and dosc.assigned_priority_wh  = dosc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = dosc.alloc_id
                                                                        and acw.to_loc    = dosc.to_loc
                                                                        and acw.tran_item = dosc.tran_item
                                                                        and acw.source_wh = dosc.assigned_priority_wh)) inner
                                           where inner.rank = 1) i;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_WH_RULE_PRIORITY - RULE 11 to 14 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_DIFFORG_SAMECHANNEL_RULE;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_DIFFORG_DIFFCHANNEL_RULE(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_DIFFORG_DIFFCHANNEL_RULE';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_calc_wh_rule_priority(alloc_id,
                                         source_item,
                                         source_diff1_id,
                                         source_diff2_id,
                                         source_diff3_id,
                                         source_diff4_id,
                                         tran_item,
                                         tran_diff1_id,
                                         tran_diff2_id,
                                         tran_diff3_id,
                                         tran_diff4_id,
                                         to_loc,
                                         to_loc_type,
                                         source_wh,
                                         rule_priority)
                                  select i.alloc_id,
                                         i.source_item,
                                         i.source_diff1_id,
                                         i.source_diff2_id,
                                         i.source_diff3_id,
                                         i.source_diff4_id,
                                         i.tran_item,
                                         i.tran_diff1_id,
                                         i.tran_diff2_id,
                                         i.tran_diff3_id,
                                         i.tran_diff4_id,
                                         i.to_loc,
                                         i.to_loc_type,
                                         i.assigned_priority_wh,
                                         i.rule_priority
                                    from (with diff_org_diff_channel as
                                             (select gtt.alloc_id,
                                                     gtt.source_item,
                                                     gtt.source_diff1_id,
                                                     gtt.source_diff2_id,
                                                     gtt.source_diff3_id,
                                                     gtt.source_diff4_id,
                                                     gtt.tran_item,
                                                     gtt.tran_diff1_id,
                                                     gtt.tran_diff2_id,
                                                     gtt.tran_diff3_id,
                                                     gtt.tran_diff4_id,
                                                     gtt.to_loc,
                                                     gtt.to_loc_type,
                                                     gtt.alloc_source_wh_id,
                                                     v_sc.location assigned_priority_wh,
                                                     v_sc.physical_wh,
                                                     v_sc.protected_ind
                                                from alc_calc_flex_supply_chain_gtt gtt,
                                                     v_alloc_supply_chain v_sc
                                               where gtt.alloc_id            = I_alloc_id
                                                 and gtt.org_unit_id        != v_sc.org_unit_id
                                                 and gtt.channel_id         != v_sc.channel_id
                                                 and (   gtt.tsf_entity_id   = DECODE(I_transfer_basis,
                                                                                      ALC_CONSTANTS_SQL.BASED_ON_TRANSFER, v_sc.tsf_entity_id,
                                                                                      gtt.tsf_entity_id)
                                                      or gtt.set_of_books_id = DECODE(I_transfer_basis,
                                                                                      ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS, v_sc.set_of_books_id,
                                                                                      gtt.set_of_books_id))
                                                 and v_sc.loc_type           = 'W'
                                                 and gtt.transfer_basis      = I_transfer_basis)
                                          ---------------------------------------------------------------
                                          -- RULE 15 - Primary VIRTUAL WH of PHYSICAL WH linked
                                          --           with the DIFF ORG_UNIT_ID and DIFF CHANNEL_ID with PROTECTED_IND = 'N'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_DODC_NOTPRTCT_PRIMARY rule_priority
                                            from (select dodc.alloc_id,
                                                         dodc.source_item,
                                                         dodc.source_diff1_id,
                                                         dodc.source_diff2_id,
                                                         dodc.source_diff3_id,
                                                         dodc.source_diff4_id,
                                                         dodc.tran_item,
                                                         dodc.tran_diff1_id,
                                                         dodc.tran_diff2_id,
                                                         dodc.tran_diff3_id,
                                                         dodc.tran_diff4_id,
                                                         dodc.to_loc,
                                                         dodc.to_loc_type,
                                                         dodc.alloc_source_wh_id,
                                                         dodc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY dodc.alloc_id,
                                                                                         dodc.source_item,
                                                                                         dodc.tran_item,
                                                                                         dodc.to_loc
                                                                                ORDER BY dodc.alloc_source_wh_id,
                                                                                         dodc.assigned_priority_wh) rank
                                                    from diff_org_diff_channel dodc,
                                                         wh w
                                                   where dodc.physical_wh          = w.wh
                                                     and dodc.assigned_priority_wh = w.primary_vwh
                                                     and dodc.protected_ind        = 'N'
                                                     and dodc.assigned_priority_wh = dodc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = dodc.alloc_id
                                                                        and acw.to_loc    = dodc.to_loc
                                                                        and acw.tran_item = dodc.tran_item
                                                                        and acw.source_wh = dodc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 16 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                          --           with the DIFF ORG_UNIT_ID and DIFF CHANNEL_ID with PROTECTED_IND = 'N'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_DODC_NOTPRTCT_NOTPRI rule_priority
                                            from (select dodc.alloc_id,
                                                         dodc.source_item,
                                                         dodc.source_diff1_id,
                                                         dodc.source_diff2_id,
                                                         dodc.source_diff3_id,
                                                         dodc.source_diff4_id,
                                                         dodc.tran_item,
                                                         dodc.tran_diff1_id,
                                                         dodc.tran_diff2_id,
                                                         dodc.tran_diff3_id,
                                                         dodc.tran_diff4_id,
                                                         dodc.to_loc,
                                                         dodc.to_loc_type,
                                                         dodc.alloc_source_wh_id,
                                                         dodc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY dodc.alloc_id,
                                                                                         dodc.source_item,
                                                                                         dodc.tran_item,
                                                                                         dodc.to_loc
                                                                                ORDER BY dodc.alloc_source_wh_id,
                                                                                         dodc.assigned_priority_wh) rank
                                                    from diff_org_diff_channel dodc,
                                                         wh w
                                                   where dodc.physical_wh           = w.wh
                                                     and dodc.assigned_priority_wh != w.primary_vwh
                                                     and dodc.protected_ind         = 'N'
                                                     and dodc.assigned_priority_wh  = dodc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = dodc.alloc_id
                                                                        and acw.to_loc    = dodc.to_loc
                                                                        and acw.tran_item = dodc.tran_item
                                                                        and acw.source_wh = dodc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 17 - Primary VIRTUAL WH of PHYSICAL WH linked
                                          --           with the DIFF ORG_UNIT_ID and DIFF CHANNEL_ID with PROTECTED_IND = 'Y'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_DODC_PROTECT_PRIMARY rule_priority
                                            from (select dodc.alloc_id,
                                                         dodc.source_item,
                                                         dodc.source_diff1_id,
                                                         dodc.source_diff2_id,
                                                         dodc.source_diff3_id,
                                                         dodc.source_diff4_id,
                                                         dodc.tran_item,
                                                         dodc.tran_diff1_id,
                                                         dodc.tran_diff2_id,
                                                         dodc.tran_diff3_id,
                                                         dodc.tran_diff4_id,
                                                         dodc.to_loc,
                                                         dodc.to_loc_type,
                                                         dodc.alloc_source_wh_id,
                                                         dodc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY dodc.alloc_id,
                                                                                         dodc.source_item,
                                                                                         dodc.tran_item,
                                                                                         dodc.to_loc
                                                                                ORDER BY dodc.alloc_source_wh_id,
                                                                                         dodc.assigned_priority_wh) rank
                                                    from diff_org_diff_channel dodc,
                                                         wh w
                                                   where dodc.physical_wh          = w.wh
                                                     and dodc.assigned_priority_wh = w.primary_vwh
                                                     and dodc.protected_ind        = 'Y'
                                                     and dodc.assigned_priority_wh = dodc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = dodc.alloc_id
                                                                        and acw.to_loc    = dodc.to_loc
                                                                        and acw.tran_item = dodc.tran_item
                                                                        and acw.source_wh = dodc.assigned_priority_wh)) inner
                                           where inner.rank = 1
                                           union all
                                          ---------------------------------------------------------------
                                          -- RULE 18 - VIRTUAL WH that is NOT the Primary VIRTUAL WH of the PHYSICAL WH linked
                                          --           with the DIFF ORG_UNIT_ID and DIFF CHANNEL_ID with PROTECTED_IND = 'Y'
                                          ---------------------------------------------------------------
                                          select inner.alloc_id,
                                                 inner.source_item,
                                                 inner.source_diff1_id,
                                                 inner.source_diff2_id,
                                                 inner.source_diff3_id,
                                                 inner.source_diff4_id,
                                                 inner.tran_item,
                                                 inner.tran_diff1_id,
                                                 inner.tran_diff2_id,
                                                 inner.tran_diff3_id,
                                                 inner.tran_diff4_id,
                                                 inner.to_loc,
                                                 inner.to_loc_type,
                                                 inner.assigned_priority_wh,
                                                 ALC_CONSTANTS_SQL.PRIORITY_DODC_PROTECT_NOTPRI rule_priority
                                            from (select dodc.alloc_id,
                                                         dodc.source_item,
                                                         dodc.source_diff1_id,
                                                         dodc.source_diff2_id,
                                                         dodc.source_diff3_id,
                                                         dodc.source_diff4_id,
                                                         dodc.tran_item,
                                                         dodc.tran_diff1_id,
                                                         dodc.tran_diff2_id,
                                                         dodc.tran_diff3_id,
                                                         dodc.tran_diff4_id,
                                                         dodc.to_loc,
                                                         dodc.to_loc_type,
                                                         dodc.alloc_source_wh_id,
                                                         dodc.assigned_priority_wh,
                                                         row_number() OVER (PARTITION BY dodc.alloc_id,
                                                                                         dodc.source_item,
                                                                                         dodc.tran_item,
                                                                                         dodc.to_loc
                                                                                ORDER BY dodc.alloc_source_wh_id,
                                                                                         dodc.assigned_priority_wh) rank
                                                    from diff_org_diff_channel dodc,
                                                         wh w
                                                   where dodc.physical_wh           = w.wh
                                                     and dodc.assigned_priority_wh != w.primary_vwh
                                                     and dodc.protected_ind         = 'Y'
                                                     and dodc.assigned_priority_wh  = dodc.alloc_source_wh_id
                                                     and NOT EXISTS (select 'x'
                                                                       from alc_calc_wh_rule_priority acw
                                                                      where acw.alloc_id  = I_alloc_id
                                                                        and acw.alloc_id  = dodc.alloc_id
                                                                        and acw.to_loc    = dodc.to_loc
                                                                        and acw.tran_item = dodc.tran_item
                                                                        and acw.source_wh = dodc.assigned_priority_wh)) inner
                                           where inner.rank = 1) i;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_WH_RULE_PRIORITY - RULE 15 to 18 - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_DIFFORG_DIFFCHANNEL_RULE;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_INV_BUCKETS(O_error_message   IN OUT VARCHAR2,
                         I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                         I_all_orders      IN     VARCHAR2,
                         I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.POP_INV_BUCKETS';

   L_wh_crosslink_ind   SYSTEM_OPTIONS.WH_CROSS_LINK_IND%TYPE := NULL;
   L_sysdate            DATE := TRUNC(SYSDATE);
   L_hist_start_date    DATE := NULL;
   L_hist_end_date      DATE := NULL;

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program );

   if I_alc_rule_row.on_order_commit_date is NOT NULL then
      L_hist_end_date := I_alc_rule_row.on_order_commit_date;
   elsif I_alc_rule_row.on_order_commit_weeks is NOT NULL then
      L_hist_end_date := L_sysdate + (I_alc_rule_row.on_order_commit_weeks * 7);
   else
      L_hist_start_date := I_alc_rule_row.on_order_commit_range_start;
      L_hist_end_date := I_alc_rule_row.on_order_commit_range_end;
   end if;

   if NOT SYSTEM_OPTIONS_SQL.GET_WH_CROSS_LINK_IND(O_error_message,
                                                   L_wh_crosslink_ind) then
      return FALSE;
   end if;


   ------STOCK_ON_HAND AND IN_TRANSIT_QTY

   merge into alc_calc_destination_temp target
   using (select DISTINCT tmp.alloc_id,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.to_loc,
                 GREATEST(ils.stock_on_hand,0) + GREATEST(ils.pack_comp_soh,0)
                 - ( GREATEST(ils.rtv_qty,0) +
                     GREATEST(ils.non_sellable_qty,0) +
                     GREATEST(ils.pack_comp_non_sellable,0) +
                     GREATEST(ils.customer_resv,0) +
                     GREATEST(ils.pack_comp_cust_resv,0)
                   ) stock_on_hand,
                 GREATEST(ils.in_transit_qty,0) +
                 GREATEST(ils.pack_comp_intran,0) in_transit_qty,
                 --
                 GREATEST(ils.tsf_expected_qty,0) +
                 GREATEST(ils.pack_comp_exp,0) tsf_expected_qty,
                 --
                 GREATEST(ils.tsf_reserved_qty,0) +
                 GREATEST(ils.pack_comp_resv,0) tsf_resv_qty,
                 --
                 GREATEST(ils.customer_backorder,0) +
                 GREATEST(ils.pack_comp_cust_back,0) backorder_qty
            from alc_calc_destination_temp tmp,
                 item_loc_soh ils
           where tmp.alloc_id   = I_alloc_id
             and tmp.tran_item  = ils.item
             and tmp.to_loc     = ils.loc) use_this
   on (    target.alloc_id       = use_this.alloc_id
       and target.source_item    = use_this.source_item
       and target.tran_item      = use_this.tran_item
       and target.to_loc         = use_this.to_loc)
   when MATCHED then
      update
         set target.stock_on_hand    = DECODE(I_alc_rule_row.on_hand_ind,
                                              'Y', use_this.stock_on_hand,
                                              0),
             target.in_transit_qty   = DECODE(I_alc_rule_row.in_transit_ind,
                                              'Y', use_this.in_transit_qty,
                                              0),
             target.on_alloc         = DECODE(I_alc_rule_row.inbound_allocation_ind,
                                              'Y', use_this.tsf_expected_qty,
                                              0),
             target.alloc_out        = DECODE(I_alc_rule_row.outbound_allocation_ind,
                                              'Y', use_this.tsf_resv_qty,
                                              0),
             target.backorder_qty    = DECODE(I_alc_rule_row.back_order_ind,
                                              'Y', use_this.backorder_qty,
                                              0),
             target.on_order         = 0;

   ------CO TSF

   merge into alc_calc_destination_temp target
   using (
          with il_explode as
             (--item (can be a pack)
              select tmp.alloc_id,
                     tmp.source_item,
                     tmp.tran_item item,
                     tmp.to_loc loc,
                     1 pack_multiplier,
                     tmp.tran_item tran_item
                from alc_calc_destination_temp tmp
               where tmp.alloc_id = I_alloc_id
              union all
              --item (can be a pack) in a pack
              select tmp.alloc_id,
                     tmp.source_item,
                     b.pack_no item,
                     tmp.to_loc loc,
                     b.pack_item_qty pack_multiplier,
                     tmp.tran_item tran_item
                from alc_calc_destination_temp tmp,
                     packitem_breakout b
               where tmp.tran_item = b.item
                 and tmp.alloc_id  = I_alloc_id
              union all
              --pack in a pack
              select tmp.alloc_id,
                     tmp.source_item,
                     p.pack_no item,
                     tmp.to_loc loc,
                     p.pack_qty pack_multiplier,
                     tmp.tran_item tran_item
                from alc_calc_destination_temp tmp,
                     packitem p
               where tmp.tran_item_level = tmp.tran_tran_level
                 and tmp.tran_pack_ind   = 'Y'
                 and tmp.tran_item       = p.item
                 and tmp.alloc_id        = I_alloc_id)
             --
             select /*+ ORDERED */
                    DISTINCT il_explode.alloc_id,
                    il_explode.source_item,
                    il_explode.loc,
                    il_explode.tran_item,
                    NVL(SUM(il_explode.pack_multiplier * (d.tsf_qty - NVL(d.ship_qty,0))),0) co_on_alloc,
                    NVL(SUM(il_explode.pack_multiplier * (d.ship_qty - NVL(d.received_qty,0))),0) co_intran
               from il_explode,
                    tsfhead h,
                    tsfdetail d
              where d.item           = il_explode.item
                and h.to_loc         = il_explode.loc
                and h.tsf_no         = d.tsf_no
                and h.status         in ('A','S','P','L','C')
                and h.tsf_type       = 'CO'
                and d.tsf_qty        > NVL(d.ship_qty,0)
              group by il_explode.alloc_id,
                       il_explode.source_item,
                       il_explode.loc,
                       il_explode.tran_item) use_this
   on (    target.alloc_id        = use_this.alloc_id
       and target.source_item     = use_this.source_item
       and target.tran_item       = use_this.tran_item
       and target.to_loc          = use_this.loc)
   when MATCHED then
      update
         set target.on_alloc       = DECODE(I_alc_rule_row.inbound_allocation_ind,
                                            'Y', target.on_alloc - use_this.co_on_alloc,
                                            target.on_alloc),
             target.in_transit_qty = DECODE(I_alc_rule_row.in_transit_ind,
                                            'Y', target.in_transit_qty - use_this.co_intran,
                                            target.in_transit_qty);

   ------PL TSF

   if L_wh_crosslink_ind = 'Y' and I_alc_rule_row.inbound_allocation_ind = 'Y' then

      merge into alc_calc_destination_temp target
      using (
             with il_explode as
                (--item (can be a pack)
                 select tmp.alloc_id,
                        tmp.source_item,
                        tmp.tran_item item,
                        tmp.to_loc loc,
                        1 pack_multiplier,
                        tmp.tran_item tran_item
                   from alc_calc_destination_temp tmp
                  where tmp.alloc_id = I_alloc_id
                 union all
                 --item (can be a pack) in a pack
                 select tmp.alloc_id,
                        tmp.source_item,
                        b.pack_no item,
                        tmp.to_loc loc,
                        b.pack_item_qty pack_multiplier,
                        tmp.tran_item tran_item
                   from alc_calc_destination_temp tmp,
                        packitem_breakout b
                  where tmp.tran_item = b.item
                    and tmp.alloc_id  = I_alloc_id
                 union all
                 --pack in a pack
                 select tmp.alloc_id,
                        tmp.source_item,
                        p.pack_no item,
                        tmp.to_loc loc,
                        p.pack_qty pack_multiplier,
                        tmp.tran_item tran_item
                   from alc_calc_destination_temp tmp,
                        packitem p
                  where tmp.tran_item_level    = tmp.tran_tran_level
                    and tmp.tran_pack_ind = 'Y'
                    and tmp.tran_item     = p.item
                    and tmp.alloc_id      = I_alloc_id)
                --
             select /*+ ORDERED */
                    DISTINCT il_explode.alloc_id,
                    il_explode.source_item,
                    il_explode.loc,
                    il_explode.tran_item,
                    NVL(SUM(il_explode.pack_multiplier * (d.tsf_qty - NVL(d.ship_qty,0))),0) pl_tsf_qty
               from il_explode,
                    tsfhead h,
                    tsfdetail d
              where d.item           = il_explode.item
                and h.to_loc         = il_explode.loc
                and h.tsf_no         = d.tsf_no
                and h.status         IN ('A','E','S','B')
                and h.tsf_type       = 'PL'
                and d.tsf_qty        > NVL(d.ship_qty,0)
                and TRUNC(h.delivery_date) >= NVL(L_hist_start_date, TRUNC(h.delivery_date))
                and TRUNC(h.delivery_date) <= NVL(L_hist_end_date, TRUNC(h.delivery_date))
              group by il_explode.alloc_id,
                       il_explode.source_item,
                       il_explode.loc,
                       il_explode.tran_item) use_this
      on (    target.alloc_id        = use_this.alloc_id
          and target.source_item     = use_this.source_item
          and target.tran_item       = use_this.tran_item
          and target.to_loc          = use_this.loc)
      when MATCHED then
         update
            set target.on_alloc = target.on_alloc + use_this.pl_tsf_qty;

   end if;

   ------ON_ORDER

   if I_alc_rule_row.on_order_ind = 'Y' then

      merge into alc_calc_destination_temp target
      using (
             with il_explode as
                (--item (can be a pack)
                 select tmp.alloc_id,
                        tmp.source_item,
                        tmp.tran_item item,
                        tmp.to_loc loc,
                        1 pack_multiplier,
                        tmp.tran_item tran_item
                   from alc_calc_destination_temp tmp
                  where alloc_id = I_alloc_id
                 union all
                 --item (can be a pack) in a pack
                 select tmp.alloc_id,
                        tmp.source_item,
                        b.pack_no item,
                        tmp.to_loc loc,
                        b.pack_item_qty pack_multiplier,
                        tmp.tran_item tran_item
                   from alc_calc_destination_temp tmp,
                        packitem_breakout b
                  where tmp.tran_item = b.item
                    and alloc_id      = I_alloc_id
                 union all
                 --pack in a pack
                 select tmp.alloc_id,
                        tmp.source_item,
                        p.pack_no item,
                        tmp.to_loc loc,
                        p.pack_qty pack_multiplier,
                        tmp.tran_item tran_item
                   from alc_calc_destination_temp tmp,
                        packitem p
                  where tmp.tran_item_level    = tmp.tran_tran_level
                    and tmp.tran_pack_ind = 'Y'
                    and tmp.tran_item     = p.item
                    and alloc_id          = I_alloc_id)
      --
             select /*+ ORDERED INDEX(ol ordloc_i1) */
                    DISTINCT il_explode.alloc_id,
                    il_explode.source_item,
                    il_explode.tran_item,
                    il_explode.loc,
                    NVL(SUM(il_explode.pack_multiplier * (ol.qty_ordered - NVL(ol.qty_received,0))),0) on_order_qty
               from il_explode,
                    ordloc ol,
                    ordhead oh
              where oh.status           = 'A'
                and oh.order_type      != 'CO'
                and NOT (I_all_orders   = 'N' and oh.include_on_order_ind = 'N')
                and oh.order_no         = ol.order_no
                and ol.location         = il_explode.loc
                and ol.item             = il_explode.item
                and ol.qty_ordered      > NVL(ol.qty_received,0)
                and (TRUNC(oh.not_before_date) >= L_hist_start_date or L_hist_start_date is NULL)
                and (TRUNC(oh.not_before_date) <= L_hist_end_date or L_hist_end_date is NULL)
              group by il_explode.alloc_id,
                       il_explode.source_item,
                       il_explode.tran_item,
                       il_explode.loc) use_this
      on (    target.alloc_id        = use_this.alloc_id
          and target.source_item     = use_this.source_item
          and target.tran_item       = use_this.tran_item
          and target.to_loc          = use_this.loc)
      when MATCHED then
         update
            set target.on_order = use_this.on_order_qty;

   end if;

   ------ON_ALLOC

   if I_alc_rule_row.inbound_allocation_ind = 'Y' then

      merge into alc_calc_destination_temp target
      using (
             with il_explode as
             (--item (can be a pack)
              select tmp.alloc_id,
                     tmp.source_item,
                     tmp.tran_item item,
                     tmp.to_loc loc,
                     1 pack_multiplier,
                     tmp.tran_item tran_item
                from alc_calc_destination_temp tmp
               where tmp.tran_item_level = tmp.tran_tran_level
                 and tmp.alloc_id        = I_alloc_id
              union all
              --item (can be a pack) in a pack
              select tmp.alloc_id,
                     tmp.source_item,
                     b.pack_no item,
                     tmp.to_loc loc,
                     b.pack_item_qty pack_multiplier,
                     tmp.tran_item tran_item
                from alc_calc_destination_temp tmp,
                     packitem_breakout b
               where tmp.tran_item_level  = tmp.tran_tran_level
                 and tmp.tran_item        = b.item
                 and tmp.alloc_id         = I_alloc_id
              union all
              --pack in a pack
              select tmp.alloc_id,
                     tmp.source_item,
                     p.pack_no item,
                     tmp.to_loc loc,
                     p.pack_qty pack_multiplier,
                     tmp.tran_item tran_item
                from alc_calc_destination_temp tmp,
                     packitem p
               where tmp.tran_item_level = tmp.tran_tran_level
                 and tmp.tran_pack_ind   = 'Y'
                 and tmp.tran_item       = p.item
                 and tmp.alloc_id        = I_alloc_id)
             --
             select DISTINCT il_explode.alloc_id,
                    il_explode.source_item,
                    il_explode.tran_item,
                    il_explode.loc,
                    NVL(SUM(il_explode.pack_multiplier * (d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_in_qty
               from alloc_header h,
                    il_explode,
                    alloc_detail d
              where h.status        IN ('A', 'R')
                and h.item          = il_explode.item
                and d.alloc_no      = h.alloc_no
                and d.to_loc        = il_explode.loc
                and d.qty_allocated > NVL(d.qty_transferred,0)
                and (EXISTS (select 'x'
                               from ordhead o
                              where o.order_no                  = h.order_no
                                and o.status                    in ('A', 'C')
                                and NOT (I_all_orders           = 'N' and
                                         o.include_on_order_ind = 'N')
                                and NVL(L_hist_start_date, TRUNC(o.not_before_date)) <= TRUNC(o.not_before_date)
                                and NVL(L_hist_end_date, TRUNC(o.not_before_date))   >= TRUNC(o.not_before_date)
                                and rownum = 1) or
                     EXISTS (select 'x'
                               from alloc_header h1
                              where h1.alloc_no                             = h.order_no
                                and h1.status                               IN ('A', 'R')
                                and NVL(L_hist_start_date, h1.release_date) <= h1.release_date
                                and NVL(L_hist_end_date, h1.release_date)   >= h1.release_date
                                and rownum = 1) or
                     EXISTS (select 'x'
                               from tsfhead t
                              where t.tsf_no          = h.order_no
                                and t.status          IN ('A','S','P','L','C')
                                and (t.not_after_date is NULL or
                                     (NVL(L_hist_start_date, NVL(t.delivery_date,L_sysdate)) <= NVL(t.delivery_date,L_sysdate)))
                                and (t.not_after_date is NULL or
                                     (NVL(L_hist_end_date, NVL(t.not_after_date,L_sysdate))   <= NVL(t.not_after_date,L_sysdate)))
                                and rownum = 1))
              group by il_explode.alloc_id,
                       il_explode.source_item,
                       il_explode.tran_item,
                       il_explode.loc) use_this
      on (    target.alloc_id       = use_this.alloc_id
          and target.source_item    = use_this.source_item
          and target.tran_item      = use_this.tran_item
          and target.to_loc         = use_this.loc)
      when MATCHED then
         update
            set target.on_alloc = target.on_alloc + use_this.alloc_in_qty;

   end if;

   ------ALLOC_OUT

   if I_alc_rule_row.outbound_allocation_ind = 'Y' then

      merge into alc_calc_destination_temp target
      using (
             with il_explode as
             (--item (can be a pack)
              select tmp.alloc_id,
                     tmp.source_item,
                     tmp.tran_item item,
                     tmp.to_loc loc,
                     1 pack_multiplier,
                     tmp.tran_item tran_item
                from alc_calc_destination_temp tmp
               where tmp.tran_item_level = tmp.tran_tran_level
                 and tmp.alloc_id        = I_alloc_id
              union all
              --item (can be a pack) in a pack
              select tmp.alloc_id,
                     tmp.source_item,
                     b.pack_no item,
                     tmp.to_loc loc,
                     b.pack_item_qty pack_multiplier,
                     tmp.tran_item tran_item
                from alc_calc_destination_temp tmp,
                     packitem_breakout b
               where tmp.tran_item_level  = tmp.tran_tran_level
                 and tmp.tran_item        = b.item
                 and tmp.alloc_id         = I_alloc_id
              union all
              --pack in a pack
              select tmp.alloc_id,
                     tmp.source_item,
                     p.pack_no item,
                     tmp.to_loc loc,
                     p.pack_qty pack_multiplier,
                     tmp.tran_item tran_item
                from alc_calc_destination_temp tmp,
                     packitem p
               where tmp.tran_item_level = tmp.tran_tran_level
                 and tmp.tran_pack_ind   = 'Y'
                 and tmp.tran_item       = p.item
                 and tmp.alloc_id        = I_alloc_id)
             --
             select DISTINCT il_explode.alloc_id,
                    il_explode.source_item,
                    il_explode.tran_item,
                    il_explode.loc,
                    NVL(SUM(il_explode.pack_multiplier * (d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_out_qty
               from alloc_header h,
                    il_explode,
                    alloc_detail d
              where h.status        IN ('A', 'R')
                and h.item          = il_explode.item
                and d.alloc_no      = h.alloc_no
                and h.wh            = il_explode.loc
                and d.qty_allocated > NVL(d.qty_transferred,0)
                and (EXISTS (select 'x'
                               from ordhead o
                              where o.order_no                  = h.order_no
                                and o.status                    IN ('A', 'C')
                                and NOT (I_all_orders           = 'N' and
                                         o.include_on_order_ind = 'N')
                                and NVL(L_hist_start_date, TRUNC(o.not_before_date)) <= TRUNC(o.not_before_date)
                                and NVL(L_hist_end_date, TRUNC(o.not_before_date))   >= TRUNC(o.not_before_date)
                                and rownum = 1) or
                     exists (select 'x'
                               from alloc_header h1
                              where h1.alloc_no                             = h.order_no
                                and h1.status                               in ('A', 'R')
                                and NVL(L_hist_start_date, h1.release_date) <= h1.release_date
                                and NVL(L_hist_end_date, h1.release_date)   >= h1.release_date
                                and rownum = 1) or
                     exists (select 'x'
                               from tsfhead t
                              where t.tsf_no          = h.order_no
                                and t.status          IN ('A','S','P','L','C')
                                and (t.not_after_date is NULL or
                                     (NVL(L_hist_start_date, NVL(t.delivery_date,L_sysdate)) <= NVL(t.delivery_date,L_sysdate)))
                                and (t.not_after_date is NULL or
                                     (NVL(L_hist_end_date, NVL(t.not_after_date,L_sysdate))   <= NVL(t.not_after_date,L_sysdate)))
                                and rownum = 1))
              group by il_explode.alloc_id,
                       il_explode.source_item,
                       il_explode.tran_item,
                       il_explode.loc) use_this
      on (    target.alloc_id       = use_this.alloc_id
          and target.source_item    = use_this.source_item
          and target.tran_item      = use_this.tran_item
          and target.to_loc         = use_this.loc)
      when MATCHED then
         update
            set target.alloc_out = target.alloc_out + use_this.alloc_out_qty;

   end if;

   LOGGER.LOG_INFORMATION('END: ' || L_program );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_INV_BUCKETS;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--RLOH Section
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_RLOH(O_error_message   IN OUT VARCHAR2,
                  I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                  I_all_orders      IN     VARCHAR2,
                  I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.POP_RLOH';

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program );

   if I_alc_rule_row.use_rule_level_on_hand_ind = 'N' then
      return TRUE;
   end if;

   if I_alc_rule_row.use_rule_level_on_hand_ind = 'H' then

      if GET_RLOH_EOD_SNAPSHOT(O_error_message,
                               I_alloc_id,
                               I_all_orders,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   else

      if I_alc_rule_row.rule_level is NOT NULL then
         if GET_RLOH_ITEMS(O_error_message,
                           I_alloc_id,
                           I_alc_rule_row) = FALSE then
            return FALSE;
         end if;
      else
         if GET_ALT_HIER_RLOH_ITEMS(O_error_message,
                                    I_alloc_id) = FALSE then
            return FALSE;
         end if;
      end if;

      if GET_RLOH_INV(O_error_message,
                      I_alloc_id,
                      I_all_orders,
                      I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

      if GET_RLOH_HIER_LVL_INV(O_error_message,
                               I_alloc_id,
                               I_all_orders,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   LOGGER.LOG_INFORMATION('END: ' || L_program );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_RLOH;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RLOH_EOD_SNAPSHOT(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_all_orders      IN     VARCHAR2,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_RLOH_EOD_SNAPSHOT';
   L_wh_crosslink_ind   SYSTEM_OPTIONS.WH_CROSS_LINK_IND%TYPE := NULL;

   L_sysdate            DATE := TRUNC(SYSDATE);
   L_hist_start_date    DATE := NULL;
   L_hist_end_date      DATE := NULL;

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program );

   if I_alc_rule_row.on_order_commit_date is NOT NULL then
      L_hist_end_date := I_alc_rule_row.on_order_commit_date;
   elsif I_alc_rule_row.on_order_commit_weeks is NOT NULL then
      L_hist_end_date := L_sysdate + (I_alc_rule_row.on_order_commit_weeks * 7);
   else
      L_hist_start_date := I_alc_rule_row.on_order_commit_range_start;
      L_hist_end_date := I_alc_rule_row.on_order_commit_range_end;
   end if;

   if NOT SYSTEM_OPTIONS_SQL.GET_WH_CROSS_LINK_IND(O_error_message,
                                                   L_wh_crosslink_ind) then
      return FALSE;
   end if;

   if (I_alc_rule_row.rule_level NOT IN(ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT,
                                        ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS,
                                        ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS) and
       I_alc_rule_row.rule_level is NOT NULL) then
      O_error_message := ALC_CONSTANTS_SQL.ERRMSG_SNAPSHOT_HIER;
      return FALSE;
   end if;

   if I_alc_rule_row.rule_level is NOT NULL then

      insert into alc_merch_hier_rloh_temp (alloc_id,
                                            dept,
                                            class,
                                            subclass,
                                            loc,
                                            loc_type,
                                            curr_avail,
                                            future_avail)
                                     select DISTINCT I_alloc_id,
                                            dept,
                                            DECODE(I_alc_rule_row.rule_level,
                                                   ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT, NULL,
                                                   class),
                                            DECODE(I_alc_rule_row.rule_level,
                                                   ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT, NULL,
                                                   ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS, NULL,
                                                   subclass),
                                            to_loc,
                                            to_loc_type,
                                            0,
                                            0
                                       from alc_calc_destination_temp
                                      where alloc_id = I_alloc_id;

   else

      insert into alc_merch_hier_rloh_temp (alloc_id,
                                            dept,
                                            class,
                                            subclass,
                                            loc,
                                            loc_type,
                                            curr_avail,
                                            future_avail)
                                     select DISTINCT I_alloc_id,
                                            sbc.dept,
                                            DECODE(I_alc_rule_row.rule_level,
                                                   ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT, NULL,
                                                   s.class) class,
                                            DECODE(I_alc_rule_row.rule_level,
                                                   ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT, NULL,
                                                   ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS, NULL,
                                                   s.subclass) subclass,
                                            al.location_id,
                                            l.loc_type,
                                            0,
                                            0
                                       from alc_rule_many_to_one s,
                                            subclass sbc,
                                            alc_loc_group alg,
                                            alc_location al,
                                            (select store loc,
                                                    'S' loc_type
                                               from store
                                             union all
                                             select wh loc,
                                                    'W' loc_type
                                               from wh) l
                                      where s.alloc_id       = I_alloc_id
                                        and s.dept           is NOT NULL
                                        and s.dept           = sbc.dept
                                        and sbc.class        = NVL(s.class, sbc.class)
                                        and sbc.subclass     = NVL(s.subclass, sbc.subclass)
                                        --
                                        and al.location_id   = l.loc
                                        --
                                        and alg.alloc_id     = s.alloc_id
                                        and alg.loc_group_id = al.loc_group_id;

   end if;

   merge into alc_merch_hier_rloh_temp target
   using (select /*+ ORDERED */ t.alloc_id,
                 t.dept,
                 t.class,
                 t.subclass,
                 t.loc,
                 t.loc_type,
                 --
                 DECODE(I_alc_rule_row.on_hand_ind,
                        'Y', SUM (ils.stock_on_hand - (  ils.tsf_reserved_qty + ils.rtv_qty + ils.non_sellable_qty + ils.customer_resv + ils.customer_backorder)),
                        0) curr_avail,
                 --
                 SUM (  DECODE(I_alc_rule_row.on_hand_ind,
                               'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                           'Y', ils.stock_on_hand,
                                           ils.no_clr_stock_on_hand),
                               0)
                      + DECODE(I_alc_rule_row.on_hand_ind,
                               'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                           'Y', ils.pack_comp_soh,
                                           ils.no_clr_pack_comp_soh),
                               0)
                      + DECODE(I_alc_rule_row.in_transit_ind,
                               'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                           'Y', ils.in_transit_qty,
                                           ils.no_clr_in_transit_qty),
                               0)
                      + DECODE(I_alc_rule_row.in_transit_ind,
                               'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                           'Y', ils.pack_comp_intran,
                                           ils.no_clr_pack_comp_intran),
                               0)
                      + DECODE(I_alc_rule_row.inbound_allocation_ind, 'Y',
                               DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                      'Y', ils.tsf_expected_qty,
                                      ils.no_clr_tsf_expected_qty),
                               0)
                      + DECODE(I_alc_rule_row.inbound_allocation_ind,
                               'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                           'Y', ils.pack_comp_exp,
                                           ils.no_clr_pack_comp_exp),
                               0)
                      - (  DECODE(I_alc_rule_row.on_hand_ind,
                                  'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                              'Y', ils.rtv_qty,
                                              ils.no_clr_rtv_qty),
                           0)
                         + 0
                         + DECODE(I_alc_rule_row.outbound_allocation_ind,
                                  'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                              'Y', ils.tsf_reserved_qty,
                                              ils.no_clr_tsf_reserved_qty),
                                  0)
                         + DECODE(I_alc_rule_row.outbound_allocation_ind,
                                  'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                              'Y', ils.pack_comp_resv,
                                              ils.no_clr_pack_comp_resv),
                                  0)
                         + DECODE(I_alc_rule_row.on_hand_ind,
                                  'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                              'Y', ils.non_sellable_qty,
                                              ils.no_clr_non_sellable_qty),
                                  0)
                         + DECODE(I_alc_rule_row.on_hand_ind,
                                  'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                              'Y', ils.customer_resv,
                                              ils.no_clr_customer_resv),
                                  0)
                         + DECODE(I_alc_rule_row.on_hand_ind,
                                  'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                              'Y', ils.pack_comp_cust_resv,
                                              ils.no_clr_pack_comp_cust_resv),
                                  0)
                         + DECODE(I_alc_rule_row.back_order_ind, 'Y',
                                  DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                         'Y', ils.customer_backorder,
                                         ils.no_clr_customer_backorder),
                                  0)
                         + DECODE(I_alc_rule_row.back_order_ind,
                                  'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                              'Y', ils.pack_comp_cust_back,
                                              ils.no_clr_pack_comp_cust_back),
                                  0)
                        )
                 ) future_avail
            from alc_merch_hier_rloh_temp t,
                 alc_subclass_item_loc_soh_eod ils
           where t.alloc_id   = I_alloc_id
             and t.dept       = ils.dept
             and ils.class    = NVL(t.class, ils.class)
             and ils.subclass = NVL(t.subclass, ils.subclass)
             and t.loc        = ils.loc
             and t.loc_type   = ils.loc_type
           group by t.alloc_id,
                    t.dept,
                    t.class,
                    t.subclass,
                    t.loc,
                    t.loc_type) source
   on (    target.alloc_id         = source.alloc_id
       and target.dept             = source.dept
       and NVL(target.class,-1)    = NVL(source.class, -1)
       and NVL(target.subclass,-1) = NVL(source.subclass, -1)
       and target.loc              = source.loc
       and target.loc_type         = source.loc_type)
   when MATCHED then
      update
         set target.curr_avail   = source.curr_avail,
             target.future_avail = source.future_avail;


   /*subtract off CO from future avail*/
   merge into alc_merch_hier_rloh_temp target
   using (
          select /*+ ORDERED */ t.alloc_id,
                 t.dept,
                 t.class,
                 t.subclass,
                 t.loc,
                 t.loc_type,
                 --
                 SUM(DECODE(I_alc_rule_row.include_clearance_stock_ind,
                            'Y', ils.co_alloc_in_qty,
                            ils.no_clr_co_alloc_in_qty)) co_on_alloc,
                 SUM(DECODE(I_alc_rule_row.include_clearance_stock_ind,
                            'Y', ils.co_intran_qty,
                            ils.no_clr_co_intran_qty)) co_intran
            from alc_merch_hier_rloh_temp t,
                 alc_subclass_cust_order_eod ils
           where t.alloc_id   = I_alloc_id
             and t.dept       = ils.dept
             and ils.class    = NVL(t.class, ils.class)
             and ils.subclass = NVL(t.subclass, ils.subclass)
             and t.loc        = ils.loc
             and t.loc_type   = ils.loc_type
           group by t.alloc_id,
                    t.dept,
                    t.class,
                    t.subclass,
                    t.loc,
                 t.loc_type) source
   on (    target.alloc_id         = source.alloc_id
       and target.dept             = source.dept
       and NVL(target.class,-1)    = NVL(source.class, -1)
       and NVL(target.subclass,-1) = NVL(source.subclass, -1)
       and target.loc              = source.loc
       and target.loc_type         = source.loc_type)
   when MATCHED then
      update
         set target.future_avail =   target.future_avail
                                   - DECODE(I_alc_rule_row.inbound_allocation_ind,
                                            'Y', source.co_on_alloc,
                                            0)
                                   - DECODE(I_alc_rule_row.in_transit_ind,
                                            'Y', source.co_intran,
                                            0);

   if I_alc_rule_row.on_order_ind = 'Y' then

      /*no CO in the EOD table so don't need to worry about subracting it off */
      merge into alc_merch_hier_rloh_temp target
      using (select /*+ ORDERED */ t.alloc_id,
                    t.dept,
                    t.class,
                    t.subclass,
                    t.loc,
                    t.loc_type,
                    --
                    SUM(DECODE(I_all_orders,
                               'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                           'Y', ord.all_orders_on_order_qty,
                                           ord.no_clr_all_ord_on_ord_qty),
                                    DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                           'Y', ord.not_all_orders_on_order_qty,
                                           ord.no_clr_not_all_ord_on_ord_qty))) on_order
               from alc_subclass_on_order_eod ord,
                    alc_merch_hier_rloh_temp t
              where t.alloc_id                 = I_alloc_id
                and t.dept                     = ord.dept
                and ord.class                  = NVL(t.class, ord.class)
                and ord.subclass               = NVL(t.subclass, ord.subclass)
                and t.loc                      = ord.loc
                and t.loc_type                 = ord.loc_type
                and (TRUNC(ord.on_order_date) >= L_hist_start_date or L_hist_start_date is NULL)
                and (TRUNC(ord.on_order_date) <= L_hist_end_date or L_hist_end_date is NULL)
           group by t.alloc_id,
                    t.dept,
                    t.class,
                    t.subclass,
                    t.loc,
                    t.loc_type) source
      on (    target.alloc_id           = source.alloc_id
          and target.dept               = source.dept
          and NVL(target.class, -1)     = NVL(source.class, -1)
          and NVL(target.subclass, -1)  = NVL(source.subclass, -1)
          and target.loc                = source.loc
          and target.loc_type           = source.loc_type)
      when MATCHED then
         update
            set target.future_avail = target.future_avail + source.on_order;

   end if;

   if I_alc_rule_row.inbound_allocation_ind = 'Y' then

      merge into alc_merch_hier_rloh_temp target
      using (select /*+ ORDERED */ t.alloc_id,
                    t.dept,
                    t.class,
                    t.subclass,
                    t.loc,
                    t.loc_type,
                    --
                    SUM(DECODE(I_all_orders,
                               'Y', DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                           'Y', ai.all_orders_alloc_in_qty,
                                           ai.no_clr_all_ord_alc_in_qty),
                               DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                      'Y', ai.not_all_orders_alloc_in_qty,
                                      ai.no_clr_not_all_ord_alc_in_qty))) alloc_in
               from alc_merch_hier_rloh_temp t,
                    alc_subclass_alloc_in_eod ai
              where t.alloc_id               = I_alloc_id
                and t.dept                   = ai.dept
                and ai.class                 = NVL(t.class, ai.class)
                and ai.subclass              = NVL(t.subclass, ai.subclass)
                and t.loc                    = ai.loc
                and t.loc_type               = ai.loc_type
                and TRUNC(ai.alloc_in_date) >= NVL(L_hist_start_date, TRUNC(ai.alloc_in_date))
                and TRUNC(ai.alloc_in_date) <= NVL(L_hist_end_date, TRUNC(ai.alloc_in_date))
           group by t.alloc_id,
                    t.dept,
                    t.class,
                    t.subclass,
                    t.loc,
                    t.loc_type) source
      on (    target.alloc_id          = source.alloc_id
          and target.dept              = source.dept
          and NVL(target.class, -1)    = NVL(source.class, -1)
          and NVL(target.subclass, -1) = NVL(source.subclass, -1)
          and target.loc               = source.loc
          and target.loc_type          = source.loc_type)
      when MATCHED then
         update
            set target.future_avail = target.future_avail + source.alloc_in;

   end if;

   if L_wh_crosslink_ind = 'Y' and I_alc_rule_row.inbound_allocation_ind = 'Y' then

      merge into alc_merch_hier_rloh_temp target
      using (select /*+ ORDERED */ t.alloc_id,
                    t.dept,
                    t.class,
                    t.subclass,
                    t.loc,
                    t.loc_type,
                    --
                    SUM(DECODE(I_alc_rule_row.include_clearance_stock_ind,
                               'Y',cd.crosslink_qty, cd.no_clr_crosslink_qty)) crosslink_qty
               from alc_merch_hier_rloh_temp t,
                    alc_subclass_crosslink_eod cd
              where t.alloc_id  = I_alloc_id
                and t.dept      = cd.dept
                and cd.class    = NVL(t.class, cd.class)
                and cd.subclass = NVL(t.subclass, cd.subclass)
                and t.loc       = cd.loc
                and t.loc_type  = cd.loc_type
              group by  t.alloc_id,
                        t.dept,
                        t.class,
                        t.subclass,
                        t.loc,
                        t.loc_type) source
      on (    target.alloc_id          = source.alloc_id
          and target.dept              = source.dept
          and NVL(target.class, -1)    = NVL(source.class, -1)
          and NVL(target.subclass, -1) = NVL(source.subclass, -1)
          and target.loc               = source.loc
          and target.loc_type          = source.loc_type)
      when MATCHED then
         update
            set target.future_avail = target.future_avail + source.crosslink_qty;

   end if;

   if I_alc_rule_row.outbound_allocation_ind = 'Y' then

      merge into alc_merch_hier_rloh_temp target
      using (select /*+ ORDERED */ t.alloc_id,
                    t.dept,
                    t.class,
                    t.subclass,
                    t.loc,
                    t.loc_type,
                    --
                    SUM(DECODE(I_alc_rule_row.include_clearance_stock_ind, 'Y',
                               ai.alloc_out_qty, ai.no_clr_alloc_out_qty)) alloc_out
               from alc_merch_hier_rloh_temp t,
                    alc_subclass_alloc_out_eod ai
              where t.alloc_id                = I_alloc_id
                and t.dept                    = ai.dept
                and ai.class                  = NVL(t.class, ai.class)
                and ai.subclass               = NVL(t.subclass, ai.subclass)
                and t.loc                     = ai.loc
                and t.loc_type                = ai.loc_type
                and TRUNC(ai.alloc_out_date) >= NVL(L_hist_start_date, TRUNC(ai.alloc_out_date))
                and TRUNC(ai.alloc_out_date) <= NVL(L_hist_end_date, TRUNC(ai.alloc_out_date))
           group by t.alloc_id,
                    t.dept,
                    t.class,
                    t.subclass,
                    t.loc,
                    t.loc_type) source
      on (    target.alloc_id           = source.alloc_id
          and target.dept               = source.dept
          and NVL(target.class, -1)     = NVL(source.class, -1)
          and NVL(target.subclass, -1)  = NVL(source.subclass, -1)
          and target.loc                = source.loc
          and target.loc_type           = source.loc_type)
      when MATCHED then
         update
            set target.future_avail = target.future_avail - source.alloc_out;

   end if;

   LOGGER.LOG_INFORMATION('END: ' || L_program );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RLOH_EOD_SNAPSHOT;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RLOH_ITEMS(O_error_message   IN OUT VARCHAR2,
                        I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                        I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_RLOH_ITEMS';

BEGIN

   if I_alc_rule_row.use_rule_level_on_hand_ind != 'Y' then
      return TRUE;
   end if;

   if I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT or
      I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS or
      I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS then
      if RLOH_ITEMS_FOR_HIERARCHIES(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
   else
      if RLOH_ITEMS_FOR_ITEMS(O_error_message,
                              I_alloc_id,
                              I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RLOH_ITEMS;
-------------------------------------------------------------------------------------------------------------
FUNCTION RLOH_ITEMS_FOR_HIERARCHIES(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.RLOH_ITEMS_FOR_HIERARCHIES';

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program );

   insert into alc_merch_hier_rloh_temp (alloc_id,
                                         dept,
                                         class,
                                         subclass,
                                         loc,
                                         loc_type,
                                         curr_avail,
                                         future_avail)
                                  select DISTINCT I_alloc_id,
                                         dept,
                                         DECODE(I_alc_rule_row.rule_level,
                                                ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT, NULL,
                                                class),
                                         DECODE(I_alc_rule_row.rule_level,
                                                ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT, NULL,
                                                ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS, NULL,
                                                subclass),
                                         to_loc,
                                         to_loc_type,
                                         0,
                                         0
                                    from alc_calc_destination_temp
                                   where alloc_id = I_alloc_id;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_MERCH_HIER_RLOH_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('END: ' || L_program );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RLOH_ITEMS_FOR_HIERARCHIES;
-------------------------------------------------------------------------------------------------------------
FUNCTION RLOH_ITEMS_FOR_ITEMS(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.RLOH_ITEMS_FOR_ITEMS';

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program );

   if I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL then

      insert into alc_calc_rloh_item_temp(alloc_id,
                                          item,
                                          pack_ind,
                                          item_level,
                                          tran_level)
                                   select DISTINCT I_alloc_id,
                                          im.item,
                                          im.pack_ind,
                                          im.item_level,
                                          im.tran_level
                                     from alc_calc_source_temp s,
                                          item_master im
                                    where s.alloc_id    = I_alloc_id
                                      and s.source_item = im.item_parent
                                      and im.item_level = im.tran_level
                                      and im.status     = 'A';

   else

      insert into alc_calc_rloh_item_temp(alloc_id,
                                          item,
                                          pack_ind,
                                          item_level,
                                          tran_level)
                                   select DISTINCT I_alloc_id,
                                          s.tran_item,
                                          s.tran_pack_ind,
                                          s.tran_item_level,
                                          s.tran_tran_level
                                     from alc_calc_source_temp s
                                    where s.alloc_id = I_alloc_id;

   end if;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_RLOH_ITEM_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('END: ' || L_program );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RLOH_ITEMS_FOR_ITEMS;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ALT_HIER_RLOH_ITEMS(O_error_message   IN OUT VARCHAR2,
                                 I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_ALT_HIER_RLOH_ITEMS';

BEGIN

   if RLOH_AH_ITEMS_FOR_HIERARCHIES(O_error_message,
                                    I_alloc_id) = FALSE then
      return FALSE;
   end if;

   if RLOH_AH_ITEMS_FOR_ITEM(O_error_message,
                             I_alloc_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ALT_HIER_RLOH_ITEMS;
-------------------------------------------------------------------------------------------------------------
FUNCTION RLOH_AH_ITEMS_FOR_HIERARCHIES(O_error_message   IN OUT VARCHAR2,
                                       I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.RLOH_AH_ITEMS_FOR_HIERARCHIES';

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program );

   insert into alc_merch_hier_rloh_temp (alloc_id,
                                         dept,
                                         class,
                                         subclass,
                                         loc,
                                         loc_type,
                                         curr_avail,
                                         future_avail)
                                  select DISTINCT I_alloc_id,
                                         hier.dept,
                                         hier.class,
                                         hier.subclass,
                                         loc.store,
                                         'S',
                                         0,
                                         0
                                    from (select DISTINCT s.dept,
                                                 s.class,
                                                 s.subclass
                                            from alc_rule_many_to_one s
                                           where s.alloc_id = I_alloc_id
                                             and s.dept     is NOT NULL) hier,
                                         (select al.location_id store
                                            from alc_loc_group alg,
                                                 alc_location al
                                           where alg.alloc_id    = I_alloc_id
                                             and al.loc_group_id = alg.loc_group_id) loc;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_MERCH_HIER_RLOH_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('END: ' || L_program );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RLOH_AH_ITEMS_FOR_HIERARCHIES;
-------------------------------------------------------------------------------------------------------------
FUNCTION RLOH_AH_ITEMS_FOR_ITEM(O_error_message   IN OUT VARCHAR2,
                                I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.RLOH_AH_ITEMS_FOR_ITEM';

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program );

   insert into alc_calc_rloh_item_temp(alloc_id,
                                       item,
                                       pack_ind,
                                       item_level,
                                       tran_level,
                                       pack_qty)
                                --non-pack item
                                select ah.alloc_id,
                                       im.item as tran_item,
                                       im.pack_ind as tran_pack_ind,
                                       im.item_level as tran_item_level,
                                       im.tran_level as tran_tran_level,
                                       1 as pack_qty
                                  from alc_rule_many_to_one ah,
                                       item_master im
                                 where ah.alloc_id   = I_alloc_id
                                  --replace with a type column
                                   and ah.item_id    is NOT NULL
                                   and ah.diff1_id   is NULL
                                   and ah.item_id    = im.item
                                   and im.status     = 'A'
                                   and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and im.sellable_ind = 'Y'))
                                   and im.item_level = im.tran_level
                                 union
                                --non-sellable pack item explode to components
                                select ah.alloc_id,
                                       im.item as tran_item,
                                       im.pack_ind as tran_pack_ind,
                                       im.item_level as tran_item_level,
                                       im.tran_level as tran_tran_level,
                                       vp.qty as pack_qty
                                  from alc_rule_many_to_one ah,
                                       v_packsku_qty vp,
                                       item_master pim,
                                       item_master im
                                 where ah.alloc_id      = I_alloc_id
                                  --replace with a type column
                                   and ah.item_id       is NOT NULL
                                   and ah.diff1_id      is NULL
                                   and ah.item_id       = vp.pack_no
                                   and vp.pack_no       = pim.item
                                   and pim.sellable_ind = 'N'
                                   and vp.item          = im.item
                                   and im.status        = 'A'
                                   and im.pack_ind      = 'N'
                                   and im.item_level    = im.tran_level
                                 union
                                --style explode to children
                                select ah.alloc_id,
                                       im.item as tran_item,
                                       im.pack_ind as tran_pack_ind,
                                       im.item_level as tran_item_level,
                                       im.tran_level as tran_tran_level,
                                       1 as pack_qty
                                  from alc_rule_many_to_one ah,
                                       item_master im
                                 where ah.alloc_id   = I_alloc_id
                                  --replace with a type column
                                   and ah.item_id    is NOT NULL
                                   and ah.diff1_id   is NULL
                                   and ah.item_id    = im.item_parent
                                   and im.status     = 'A'
                                   and im.item_level = im.tran_level
                                 union
                                 --style color
                                select inner.alloc_id,
                                       im.item as tran_item,
                                       im.pack_ind as tran_pack_ind,
                                       im.item_level as tran_item_level,
                                       im.tran_level as tran_tran_level,
                                       1 as pack_qty
                                  from item_master im,
                                       (select DISTINCT alloc_id,
                                               many_to_one_id,
                                               item_id,
                                               DECODE(diff_pos, 1,
                                                      TRIM( SUBSTR ( txt
                                                                   , INSTR (txt, ',', 1, level ) + 1
                                                                   , INSTR (txt, ',', 1, level+1) - INSTR (txt, ',', 1, level) -1
                                                                   )),
                                                      NULL) diff_1,
                                               DECODE(diff_pos, 2,
                                                      TRIM( SUBSTR ( txt
                                                                   , INSTR (txt, ',', 1, level ) + 1
                                                                   , INSTR (txt, ',', 1, level+1) - INSTR (txt, ',', 1, level) -1
                                                                   )),
                                                      NULL) diff_2,
                                               DECODE(diff_pos, 3,
                                                      TRIM( SUBSTR ( txt
                                                                   , INSTR (txt, ',', 1, level ) + 1
                                                                   , INSTR (txt, ',', 1, level+1) - INSTR (txt, ',', 1, level) -1
                                                                   )),
                                                      NULL) diff_3,
                                               DECODE(diff_pos, 4,
                                                      TRIM( SUBSTR ( txt
                                                                   , INSTR (txt, ',', 1, level ) + 1
                                                                   , INSTR (txt, ',', 1, level+1) - INSTR (txt, ',', 1, level) -1
                                                                   )),
                                                      NULL) diff_4
                                          FROM (select alloc_id,
                                                       many_to_one_id,
                                                       item_id,
                                                       SUBSTR(diff1_id,1,1) diff_pos,
                                                       ','||SUBSTR(diff1_id,3)||',' txt
                                                  from alc_rule_many_to_one
                                                 where alloc_id = I_alloc_id
                                                   and diff1_id is NOT NULL)
                                        connect by level <= LENGTH(txt)-LENGTH(REPLACE(txt,',',''))-1) inner
                                 where im.item_parent = inner.item_id
                                   and NVL(im.diff_1,'--')      = NVL(inner.diff_1, NVL(im.diff_1,'--'))
                                   and NVL(im.diff_2,'--')      = NVL(inner.diff_2, NVL(im.diff_2,'--'))
                                   and NVL(im.diff_3,'--')      = NVL(inner.diff_3, NVL(im.diff_3,'--'))
                                   and NVL(im.diff_4,'--')      = NVL(inner.diff_4, NVL(im.diff_4,'--'))
                                union
                                --skulist SKU - sellable pack
                                select ah.alloc_id,
                                       im.item as tran_item,
                                       im.pack_ind as tran_pack_ind,
                                       im.item_level as tran_item_level,
                                       im.tran_level as tran_tran_level,
                                       1 as pack_qty
                                  from alc_rule_many_to_one ah,
                                       skulist_detail sd,
                                       item_master im
                                 where ah.alloc_id    = I_alloc_id
                                   and ah.itemlist_id = sd.skulist
                                   and sd.item_level  = sd.tran_level
                                   and sd.item        = im.item
                                   and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and im.sellable_ind = 'Y'))
                                   and im.status = 'A'
                                union
                                --skulist non-sellable PACK
                                select ah.alloc_id,
                                       im.item as tran_item,
                                       im.pack_ind as tran_pack_ind,
                                       im.item_level as tran_item_level,
                                       im.tran_level as tran_tran_level,
                                       vp.qty as pack_qty
                                  from alc_rule_many_to_one ah,
                                       skulist_detail sd,
                                       v_packsku_qty vp,
                                       item_master pim,
                                       item_master im
                                 where ah.alloc_id      = I_alloc_id
                                   and ah.itemlist_id   = sd.skulist
                                   and sd.item_level    = sd.tran_level
                                   and sd.pack_ind      = 'Y'
                                   and sd.item          = vp.pack_no
                                   and vp.pack_no       = pim.item
                                   and pim.sellable_ind = 'N'
                                   and vp.item          = im.item
                                   and im.status        = 'A'
                                   and im.pack_ind      = 'N'
                                   and im.item_level    = im.tran_level
                                union
                                --skulist PARENT
                                select ah.alloc_id,
                                       im.item as tran_item,
                                       im.pack_ind as tran_pack_ind,
                                       im.item_level as tran_item_level,
                                       im.tran_level as tran_tran_level,
                                       1 as pack_qty
                                  from alc_rule_many_to_one ah,
                                       skulist_detail sd,
                                       item_master im
                                 where ah.alloc_id   = I_alloc_id
                                   and ah.itemlist_id = sd.skulist
                                   and sd.item_level  < sd.tran_level
                                   and sd.pack_ind    = 'N'
                                   and (sd.item = im.item_parent or sd.item = im.item_grandparent)
                                   and im.status      = 'A'
                                   and im.item_level  = im.tran_level
                                union
                                --uda PARENT
                                select ah.alloc_id,
                                       im.item as tran_item,
                                       im.pack_ind as tran_pack_ind,
                                       im.item_level as tran_item_level,
                                       im.tran_level as tran_tran_level,
                                       1 as pack_qty
                                  from alc_rule_many_to_one ah,
                                       uda_item_lov ul,
                                       item_master im
                                 where ah.alloc_id   = I_alloc_id
                                   and ah.uda_id                          = ul.uda_id
                                   and NVL(ah.uda_value_id, ul.uda_value) = ul.uda_value
                                   and (ul.item = im.item_parent or ul.item = im.item_grandparent)
                                   and im.status                          = 'A'
                                   and im.pack_ind                        = 'N'
                                   and im.item_level                      = im.tran_level
                                union
                                --uda SKU - sellable pack
                                select ah.alloc_id,
                                       im.item as tran_item,
                                       im.pack_ind as tran_pack_ind,
                                       im.item_level as tran_item_level,
                                       im.tran_level as tran_tran_level,
                                       1 as pack_qty
                                  from alc_rule_many_to_one ah,
                                       uda_item_lov ul,
                                       item_master im
                                 where ah.alloc_id   = I_alloc_id
                                   and ah.uda_id      = ul.uda_id
                                   and NVL(ah.uda_value_id, ul.uda_value) = ul.uda_value
                                   and ul.item        = im.item
                                   and im.status      = 'A'
                                   and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and im.sellable_ind = 'Y'))
                                   and im.item_level  = im.tran_level
                                union
                                --uda non-sellable PACK
                                select ah.alloc_id,
                                       im.item as tran_item,
                                       im.pack_ind as tran_pack_ind,
                                       im.item_level as tran_item_level,
                                       im.tran_level as tran_tran_level,
                                       1 as pack_qty
                                  from alc_rule_many_to_one ah,
                                       v_packsku_qty vq,
                                       uda_item_lov ul,
                                       item_master pim,
                                       item_master im
                                 where ah.alloc_id      = I_alloc_id
                                   and ah.uda_id        = ul.uda_id
                                   and NVL(ah.uda_value_id, ul.uda_value) = ul.uda_value
                                   and ul.item          = vq.pack_no
                                   and vq.pack_no       = pim.item
                                   and pim.sellable_ind = 'N'
                                   and vq.item          = im.item
                                   and im.status        = 'A'
                                   and im.pack_ind      = 'N'
                                   and im.item_level    = im.tran_level;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_RLOH_ITEM_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('END: ' || L_program );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RLOH_AH_ITEMS_FOR_ITEM;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RLOH_INV(O_error_message   IN OUT VARCHAR2,
                      I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                      I_all_orders      IN     VARCHAR2,
                      I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_RLOH_INV';

   L_wh_crosslink_ind       SYSTEM_OPTIONS.WH_CROSS_LINK_IND%TYPE := NULL;
   L_sysdate                DATE := TRUNC(SYSDATE);
   L_hist_start_date        DATE := NULL;
   L_hist_end_date          DATE := NULL;

BEGIN

   if I_alc_rule_row.on_order_commit_date is NOT NULL then
      L_hist_end_date := I_alc_rule_row.on_order_commit_date;
   elsif I_alc_rule_row.on_order_commit_weeks is NOT NULL then
      L_hist_end_date := L_sysdate + (I_alc_rule_row.on_order_commit_weeks * 7);
   else
      L_hist_start_date := I_alc_rule_row.on_order_commit_range_start;
      L_hist_end_date := I_alc_rule_row.on_order_commit_range_end;
   end if;

   if NOT SYSTEM_OPTIONS_SQL.GET_WH_CROSS_LINK_IND(O_error_message,
                                                   L_wh_crosslink_ind) then
      return FALSE;
   end if;

   insert into alc_calc_rloh_temp (alloc_id,
                                  item,
                                  loc,
                                  pack_ind,
                                  item_level,
                                  tran_level,
                                  curr_avail,
                                  future_avail)
                           select /*+ ORDERED */
                                  I_alloc_id,
                                  tmp.item,
                                  il.loc,
                                  tmp.pack_ind,
                                  tmp.item_level,
                                  tmp.tran_level,
                                  --
                                  DECODE(I_alc_rule_row.on_hand_ind,
                                         'Y', (  GREATEST(ils.stock_on_hand,0)
                                               - (  GREATEST(ils.tsf_reserved_qty, 0)
                                                  + GREATEST(ils.rtv_qty, 0)
                                                  + GREATEST(ils.non_sellable_qty,0)
                                                  + GREATEST(ils.customer_resv, 0)
                                                  + GREATEST(ils.customer_backorder, 0))),
                                         0) curr_avail,
                                  --
                                  (  DECODE(I_alc_rule_row.on_hand_ind,
                                            'Y', GREATEST(ils.stock_on_hand, 0),
                                            0)
                                   + DECODE(I_alc_rule_row.on_hand_ind,
                                            'Y', GREATEST(ils.pack_comp_soh, 0),
                                            0)
                                   + DECODE(I_alc_rule_row.in_transit_ind,
                                            'Y', GREATEST(ils.in_transit_qty, 0),
                                            0)
                                   + DECODE(I_alc_rule_row.in_transit_ind,
                                            'Y', GREATEST(ils.pack_comp_intran, 0),
                                            0)
                                   + DECODE(I_alc_rule_row.inbound_allocation_ind,
                                            'Y', GREATEST(ils.tsf_expected_qty, 0),
                                            0)
                                   + DECODE(I_alc_rule_row.inbound_allocation_ind,
                                            'Y', GREATEST(ils.pack_comp_exp, 0),
                                            0)
                                   - (  DECODE(I_alc_rule_row.on_hand_ind,
                                               'Y', GREATEST(ils.rtv_qty, 0),
                                               0)
                                      + 0
                                      + DECODE(I_alc_rule_row.outbound_allocation_ind,
                                               'Y', GREATEST(ils.tsf_reserved_qty, 0),
                                               0)
                                      + DECODE(I_alc_rule_row.outbound_allocation_ind,
                                               'Y', GREATEST(ils.pack_comp_resv, 0),
                                               0)
                                      + DECODE(I_alc_rule_row.on_hand_ind,
                                               'Y', GREATEST(ils.non_sellable_qty,0),
                                               0)
                                      + DECODE(I_alc_rule_row.on_hand_ind,
                                               'Y', GREATEST(ils.customer_resv, 0),
                                               0)
                                      + DECODE(I_alc_rule_row.on_hand_ind,
                                               'Y', GREATEST(ils.pack_comp_cust_resv, 0),
                                               0)
                                      + DECODE(I_alc_rule_row.back_order_ind,
                                               'Y', GREATEST(ils.customer_backorder, 0),
                                               0)
                                      + DECODE(I_alc_rule_row.back_order_ind,
                                               'Y', GREATEST(ils.pack_comp_cust_back, 0),
                                               0)
                                     )
                                  ) future_avail
                             from alc_calc_rloh_item_temp tmp,
                                  alc_loc_group alg,
                                  alc_location al,
                                  item_loc il,
                                  item_loc_soh ils
                            where tmp.alloc_id    = I_alloc_id
                              and il.item         = tmp.item
                              --
                              and alg.alloc_id    = tmp.alloc_id
                              and al.loc_group_id = alg.loc_group_id
                              and il.loc          = al.location_id
                              --
                              and il.clear_ind    = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                                           'N', 'N',
                                                           il.clear_ind)
                              --
                              and il.item         = ils.item
                              and il.loc          = ils.loc;

   if I_alc_rule_row.on_hand_ind = 'Y' then

      /* subtract off CO from future avail */
      merge into alc_calc_rloh_temp target
      using (
             with il_explode as
             ( --pack at pack level
                select tmp.alloc_id,
                       tmp.item rloh_item,
                       tmp.item join_item,
                       tmp.loc,
                       1 pack_qty
                  from alc_calc_rloh_temp tmp
                 where tmp.alloc_id   = I_alloc_id
                   and tmp.item_level = tmp.tran_level
                   and tmp.pack_ind   = 'Y'
                union all
                --pack as inner pack
                select tmp.alloc_id,
                       tmp.item rloh_item,
                       p.pack_no join_item,
                       tmp.loc,
                       p.pack_qty
                  from alc_calc_rloh_temp tmp,
                       packitem p
                 where tmp.alloc_id   = I_alloc_id
                   and tmp.item_level = tmp.tran_level
                   and tmp.pack_ind   = 'Y'
                   and tmp.item       = p.item
                union all
                --tran_item at tran_item level
                select tmp.alloc_id,
                       tmp.item rloh_item,
                       tmp.item join_item,
                       tmp.loc,
                       1 pack_qty
                  from alc_calc_rloh_temp tmp
                 where tmp.alloc_id   = I_alloc_id
                   and tmp.item_level = tmp.tran_level
                   and tmp.pack_ind   = 'N'
                   and tmp.tran_level = tmp.item_level
                union all
                --tran_item as comp in a pack
                select tmp.alloc_id,
                       tmp.item rloh_item,
                       p.pack_no join_item,
                       tmp.loc,
                       p.pack_item_qty pack_qty
                  from alc_calc_rloh_temp tmp,
                       packitem_breakout p
                 where tmp.alloc_id   = I_alloc_id
                   and tmp.item_level = tmp.tran_level
                   and tmp.pack_ind   = 'N'
                   and tmp.item       = p.item
             )
             --
             select /*+ ORDERED */
                    il_explode.alloc_id,
                    il_explode.rloh_item,
                    il_explode.loc,
                    NVL(SUM(il_explode.pack_qty * (NVL(td.tsf_qty,0) - NVL(td.received_qty,0))),0) co_tsf_qty
               from il_explode,
                    tsfhead th,
                    tsfdetail td
              where th.tsf_type = 'CO'
                and th.status   IN ('A','S','P','L','C')
                and th.tsf_no   = td.tsf_no
                and td.item     = il_explode.join_item
                and th.to_loc   = il_explode.loc
              group by il_explode.alloc_id,
                       il_explode.rloh_item,
                       il_explode.loc) source
          on (    target.alloc_id = source.alloc_id
              and target.item     = source.rloh_item
              and target.loc      = source.loc)
          when MATCHED then
             update
                set target.future_avail = target.future_avail - source.co_tsf_qty;

   end if;

   -----ON_ORDER

   if I_alc_rule_row.on_order_ind = 'Y' then

      merge into alc_calc_rloh_temp target
      using (
             with il_explode as
             (--pack at pack level
                select tmp.alloc_id,
                       tmp.item rloh_item,
                       tmp.item join_item,
                       tmp.loc,
                       1 pack_qty
                  from alc_calc_rloh_temp tmp
                 where tmp.alloc_id   = I_alloc_id
                   and tmp.item_level = tmp.tran_level
                   and tmp.pack_ind   = 'Y'
                union all
                --pack as inner pack
                select tmp.alloc_id,
                       tmp.item rloh_item,
                       p.pack_no join_item,
                       tmp.loc,
                       p.pack_qty
                  from alc_calc_rloh_temp tmp,
                       packitem p
                 where tmp.alloc_id   = I_alloc_id
                   and tmp.item_level = tmp.tran_level
                   and tmp.pack_ind   = 'Y'
                   and tmp.item       = p.item
                union all
                --tran_item at tran_item level
                select tmp.alloc_id,
                       tmp.item rloh_item,
                       tmp.item join_item,
                       tmp.loc,
                       1 pack_qty
                  from alc_calc_rloh_temp tmp
                 where tmp.alloc_id   = I_alloc_id
                   and tmp.item_level = tmp.tran_level
                   and tmp.pack_ind   = 'N'
                   and tmp.tran_level = tmp.item_level
                union all
                --tran_item as comp in a pack
                select tmp.alloc_id,
                       tmp.item rloh_item,
                       p.pack_no join_item,
                       tmp.loc,
                       p.pack_item_qty pack_qty
                  from alc_calc_rloh_temp tmp,
                       packitem_breakout p
                 where tmp.alloc_id   = I_alloc_id
                   and tmp.item_level = tmp.tran_level
                   and tmp.pack_ind   = 'N'
                   and tmp.item       = p.item
             )
             --
             select /*+ ORDERED INDEX(ol ordloc_i1) */
                    il_explode.alloc_id,
                    il_explode.rloh_item,
                    il_explode.loc,
                    NVL(SUM(il_explode.pack_qty * (ol.qty_ordered - NVL(ol.qty_received,0))),0) on_order_qty
               from il_explode,
                    ordloc ol,
                    ordhead oh,
                    item_loc il
              where oh.status           = 'A'
                and oh.order_type      != 'CO'
                and NOT (I_all_orders   = 'N' and oh.include_on_order_ind = 'N')
                and (TRUNC(oh.not_before_date) >= L_hist_start_date or L_hist_start_date is NULL)
                and (TRUNC(oh.not_before_date) <= L_hist_end_date or L_hist_end_date is NULL)
                and oh.order_no         = ol.order_no
                and ol.location         = il_explode.loc
                and ol.item             = il_explode.join_item
                and ol.qty_ordered      > NVL(ol.qty_received,0)
                --
                and ol.item             = il.item
                and ol.location         = il.loc
                and il.clear_ind        = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                                 'N', 'N',
                                                 il.clear_ind)
              group by il_explode.alloc_id,
                       il_explode.rloh_item,
                       il_explode.loc) source
      on (    target.alloc_id = source.alloc_id
          and target.item     = source.rloh_item
          and target.loc      = source.loc)
      when MATCHED then
         update
            set target.future_avail = target.future_avail + source.on_order_qty;

   end if;

   -----ALLOC_IN

   if I_alc_rule_row.inbound_allocation_ind = 'Y' then

      merge into alc_calc_rloh_temp target
      using (
             with il_explode as
             (--pack at pack level
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     tmp.item join_item,
                     tmp.loc,
                     1 pack_qty
                from alc_calc_rloh_temp tmp
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'Y'
              union all
              --pack as inner pack
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     p.pack_no join_item,
                     tmp.loc,
                     p.pack_qty
                from alc_calc_rloh_temp tmp,
                     packitem p
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'Y'
                 and tmp.item       = p.item
              union all
              --tran_item at tran_item level
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     tmp.item join_item,
                     tmp.loc,
                     1 pack_qty
                from alc_calc_rloh_temp tmp
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'N'
                 and tmp.tran_level = tmp.item_level
              union all
              --tran_item as comp in a pack
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     p.pack_no join_item,
                     tmp.loc,
                     p.pack_item_qty pack_qty
                from alc_calc_rloh_temp tmp,
                     packitem_breakout p
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'N'
                 and tmp.item       = p.item
             )
             --
             select /*+ ORDERED USE_NL(d,h) */
                    il_explode.alloc_id,
                    il_explode.rloh_item,
                    il_explode.loc,
                    NVL(SUM(il_explode.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_in_qty
               from il_explode,
                    alloc_header h,
                    alloc_detail d,
                    item_loc il
              where h.item          = il_explode.join_item
                and d.alloc_no      = h.alloc_no
                and h.status        IN ('A', 'R')
                and d.qty_allocated > NVL(d.qty_transferred,0)
                and d.to_loc        = il_explode.loc
                and (EXISTS (select 'x'
                               from ordhead o
                              where o.order_no = h.order_no
                                and o.status   in ('A', 'C')
                                and NVL(L_hist_start_date, trunc(o.not_before_date)) <= TRUNC(o.not_before_date)
                                and NVL(L_hist_end_date, trunc(o.not_before_date))   >= TRUNC(o.not_before_date)
                                and NOT (I_all_orders = 'N' and o.include_on_order_ind = 'N')
                                and rownum = 1) or
                     EXISTS (select 'x'
                               from alloc_header h1
                              where h1.alloc_no = h.order_no
                                and h1.status   in ('A', 'R')
                                and NVL(L_hist_start_date, h1.release_date) <= h1.release_date
                                and NVL(L_hist_end_date, h1.release_date)   >= h1.release_date
                                and rownum = 1) or
                     EXISTS (select 'x'
                               from tsfhead t
                              where t.tsf_no = h.order_no
                                and t.status in ('A','S','P','L','C')
                                and (t.not_after_date is NULL or
                                     (NVL(L_hist_start_date, NVL(t.delivery_date,L_sysdate)) <= NVL(t.delivery_date,L_sysdate)))
                                and (t.not_after_date is NULL or
                                     (NVL(L_hist_end_date, NVL(t.not_after_date,L_sysdate))   <= NVL(t.not_after_date,L_sysdate)))
                                and rownum = 1))
                --
                and h.item          = il.item
                and d.to_loc        = il.loc
                and il.clear_ind    = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                             'N', 'N',
                                             il.clear_ind)
              group by il_explode.alloc_id,
                       il_explode.rloh_item,
                       il_explode.loc) source
      on (    target.alloc_id = source.alloc_id
          and target.item     = source.rloh_item
          and target.loc      = source.loc)
      when MATCHED then
         update
            set target.future_avail = target.future_avail + source.alloc_in_qty;

   end if;

   -----ALLOC_OUT

   if I_alc_rule_row.outbound_allocation_ind = 'Y' then

      merge into alc_calc_rloh_temp target
      using (
             with il_explode as
             (--pack at pack level
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     tmp.item join_item,
                     tmp.loc,
                     1 pack_qty
                from alc_calc_rloh_temp tmp
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'Y'
              union all
              --pack as inner pack
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     p.pack_no join_item,
                     tmp.loc,
                     p.pack_qty
                from alc_calc_rloh_temp tmp,
                     packitem p
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'Y'
                 and tmp.item       = p.item
              union all
              --tran_item at tran_item level
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     tmp.item join_item,
                     tmp.loc,
                     1 pack_qty
                from alc_calc_rloh_temp tmp
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'N'
                 and tmp.tran_level = tmp.item_level
              union all
              --tran_item as comp in a pack
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     p.pack_no join_item,
                     tmp.loc,
                     p.pack_item_qty pack_qty
                from alc_calc_rloh_temp tmp,
                     packitem_breakout p
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'N'
                 and tmp.item       = p.item
           )
           --
           select /*+ ORDERED USE_NL(d,h) */
                  il_explode.alloc_id,
                  il_explode.rloh_item,
                  il_explode.loc,
                  NVL(SUM(il_explode.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_in_qty
             from il_explode,
                  alloc_header h,
                  alloc_detail d,
                  item_loc il
            where h.item          = il_explode.join_item
              and d.alloc_no      = h.alloc_no
              and h.status        in ('A', 'R')
              and d.qty_allocated > NVL(d.qty_transferred,0)
              and h.wh            = il_explode.loc
              and (EXISTS (select 'x'
                             from ordhead o
                            where o.order_no = h.order_no
                              and o.status   in ('A', 'C')
                              and NVL(L_hist_start_date, TRUNC(o.not_before_date)) <= TRUNC(o.not_before_date)
                              and NVL(L_hist_end_date, TRUNC(o.not_before_date))   >= TRUNC(o.not_before_date)
                              and NOT (I_all_orders = 'N' and o.include_on_order_ind = 'N')
                              and rownum = 1) or
                   EXISTS (select 'x'
                             from alloc_header h1
                            where h1.alloc_no = h.order_no
                              and h1.status   IN ('A', 'R')
                              and NVL(L_hist_start_date, h1.release_date) <= h1.release_date
                              and NVL(L_hist_end_date, h1.release_date)   >= h1.release_date
                              and rownum = 1) or
                   EXISTS (select 'x'
                             from tsfhead t
                            where t.tsf_no = h.order_no
                              and t.status in ('A','S','P','L','C')
                              and (t.not_after_date is NULL or
                                   (NVL(L_hist_start_date, NVL(t.delivery_date,L_sysdate)) <= NVL(t.delivery_date,L_sysdate)))
                              and (t.not_after_date is NULL or
                                   (NVL(L_hist_end_date, NVL(t.not_after_date,L_sysdate))   <= NVL(t.not_after_date,L_sysdate)))
                              and rownum = 1))
              --
              and h.item          = il.item
              and d.to_loc        = il.loc
              and il.clear_ind    = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                           'N', 'N',
                                           il.clear_ind)
            group by il_explode.alloc_id,
                     il_explode.rloh_item,
                     il_explode.loc) source
      on (    target.alloc_id = source.alloc_id
          and target.item     = source.rloh_item
          and target.loc      = source.loc)
      when MATCHED then
         update
            set target.future_avail = target.future_avail - source.alloc_in_qty;

   end if;

   if L_wh_crosslink_ind = 'Y' and I_alc_rule_row.inbound_allocation_ind = 'Y' then

      merge into alc_calc_rloh_temp target
      using (
             with il_explode as
             (--pack at pack level
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     tmp.item join_item,
                     tmp.loc,
                     1 pack_qty
                from alc_calc_rloh_temp tmp
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'Y'
              union all
              --pack as inner pack
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     p.pack_no join_item,
                     tmp.loc,
                     p.pack_qty
                from alc_calc_rloh_temp tmp,
                     packitem p
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'Y'
                 and tmp.item       = p.item
              union all
              --tran_item at tran_item level
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     tmp.item join_item,
                     tmp.loc,
                     1 pack_qty
                from alc_calc_rloh_temp tmp
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'N'
                 and tmp.tran_level = tmp.item_level
              union all
              --tran_item as comp in a pack
              select tmp.alloc_id,
                     tmp.item rloh_item,
                     p.pack_no join_item,
                     tmp.loc,
                     p.pack_item_qty pack_qty
                from alc_calc_rloh_temp tmp,
                     packitem_breakout p
               where tmp.alloc_id   = I_alloc_id
                 and tmp.item_level = tmp.tran_level
                 and tmp.pack_ind   = 'N'
                 and tmp.item       = p.item
             )
             --
             select /*+ ORDERED */
                    il_explode.alloc_id,
                    il_explode.rloh_item,
                    il_explode.loc,
                    NVL(SUM(il_explode.pack_qty * (d.tsf_qty - NVL(d.ship_qty,0))),0) pl_qty
               from il_explode,
                    tsfhead h,
                    tsfdetail d,
                    item_loc il
              where d.item        = il_explode.join_item
                and h.to_loc      = il_explode.loc
                and h.tsf_no      = d.tsf_no
                and h.status      IN ('A','E','S','B')
                and h.tsf_type    = 'PL'
                and d.tsf_qty     > NVL(d.ship_qty,0)
                and TRUNC(h.delivery_date) >= NVL(L_hist_start_date, TRUNC(h.delivery_date))
                and TRUNC(h.delivery_date) <= NVL(L_hist_end_date, TRUNC(h.delivery_date))
                --
                and d.item        = il.item
                and h.to_loc      = il.loc
                and il.clear_ind  = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                           'N', 'N',
                                           il.clear_ind)
              group by il_explode.alloc_id,
                       il_explode.rloh_item,
                       il_explode.loc) source
      on (    target.alloc_id = source.alloc_id
          and target.item     = source.rloh_item
          and target.loc      = source.loc)
      when MATCHED then
         update
            set target.future_avail = target.future_avail + source.pl_qty;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RLOH_INV;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RLOH_HIER_LVL_INV(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_all_orders      IN     VARCHAR2,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_RLOH_HIER_LVL_INV';

   L_wh_crosslink_ind   SYSTEM_OPTIONS.WH_CROSS_LINK_IND%TYPE := NULL;
   L_sysdate            DATE := TRUNC(SYSDATE);
   L_hist_start_date    DATE := NULL;
   L_hist_end_date      DATE := NULL;

BEGIN

   if I_alc_rule_row.on_order_commit_date is NOT NULL then
      L_hist_end_date := I_alc_rule_row.on_order_commit_date;
   elsif I_alc_rule_row.on_order_commit_weeks is NOT NULL then
      L_hist_end_date := L_sysdate + (I_alc_rule_row.on_order_commit_weeks * 7);
   else
      L_hist_start_date := I_alc_rule_row.on_order_commit_range_start;
      L_hist_end_date := I_alc_rule_row.on_order_commit_range_end;
   end if;

   if NOT SYSTEM_OPTIONS_SQL.GET_WH_CROSS_LINK_IND(O_error_message,
                                                   L_wh_crosslink_ind) then
      return FALSE;
   end if;

   merge into alc_merch_hier_rloh_temp target
   using (select /*+ ORDERED */
                 t.alloc_id,
                 t.dept,
                 t.class,
                 t.subclass,
                 t.loc,
                 t.loc_type,
                 --
                 DECODE(I_alc_rule_row.on_hand_ind, 'Y',
                    SUM (  GREATEST(ils.stock_on_hand,0)
                         - (  GREATEST(ils.tsf_reserved_qty,0)
                            + GREATEST(ils.rtv_qty,0)
                            + GREATEST(ils.non_sellable_qty,0)
                            + GREATEST(ils.customer_resv,0)
                            + GREATEST(ils.customer_backorder,0)
                           )
                        ),
                        0) curr_avail,
                 --
                 SUM (  DECODE(I_alc_rule_row.on_hand_ind,
                               'Y', GREATEST(ils.stock_on_hand,0),
                               0)
                      + DECODE(I_alc_rule_row.on_hand_ind,
                               'Y', GREATEST(ils.pack_comp_soh, 0),
                               0)
                      + DECODE(I_alc_rule_row.in_transit_ind,
                               'Y', GREATEST(ils.in_transit_qty, 0),
                               0)
                      + DECODE(I_alc_rule_row.in_transit_ind,
                               'Y', GREATEST(ils.pack_comp_intran, 0),
                               0)
                      + DECODE(I_alc_rule_row.inbound_allocation_ind,
                               'Y', GREATEST(ils.tsf_expected_qty, 0),
                               0)
                      + DECODE(I_alc_rule_row.inbound_allocation_ind,
                               'Y', GREATEST(ils.pack_comp_exp, 0),
                               0)
                      - (  DECODE(I_alc_rule_row.on_hand_ind,
                                  'Y', GREATEST(ils.rtv_qty, 0),
                                  0)
                         + 0
                         + DECODE(I_alc_rule_row.outbound_allocation_ind,
                                  'Y', GREATEST(ils.tsf_reserved_qty, 0),
                                  0)
                         + DECODE(I_alc_rule_row.outbound_allocation_ind,
                                  'Y', GREATEST(ils.pack_comp_resv, 0),
                                  0)
                         + DECODE(I_alc_rule_row.on_hand_ind,
                                  'Y', GREATEST(ils.non_sellable_qty,0),
                                  0)
                         + DECODE(I_alc_rule_row.on_hand_ind,
                                  'Y', GREATEST(ils.customer_resv, 0),
                                  0)
                         + DECODE(I_alc_rule_row.on_hand_ind,
                                  'Y', GREATEST(ils.pack_comp_cust_resv, 0),
                                  0)
                         + DECODE(I_alc_rule_row.back_order_ind,
                                  'Y', GREATEST(ils.customer_backorder, 0),
                                  0)
                         + DECODE(I_alc_rule_row.back_order_ind,
                                  'Y', GREATEST(ils.pack_comp_cust_back, 0),
                                  0)
                        )
                 ) future_avail
            from alc_merch_hier_rloh_temp t,
                 item_master im,
                 item_loc_soh ils,
                 item_loc il
           where t.alloc_id    = I_alloc_id
             and im.dept       = t.dept
             and im.class      = NVL(t.class, im.class)
             and im.subclass   = NVL(t.subclass, im.subclass)
             and im.item_level = im.tran_level
             and im.pack_ind   = 'N'
             and im.status     = 'A'
             --
             and im.item      = ils.item
             and t.loc        = ils.loc
             and t.loc_type   = ils.loc_type
             --
             and im.item      = il.item
             and t.loc        = il.loc
             and il.clear_ind = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                       'N', 'N',
                                       il.clear_ind)
           group by t.alloc_id,
                    t.dept,
                    t.class,
                    t.subclass,
                    t.loc,
                    t.loc_type) source
   on (    target.alloc_id = source.alloc_id
       and target.dept     = source.dept
       and NVL(target.class,-999)    = NVL(source.class, -999)
       and NVL(target.subclass,-999) = NVL(source.subclass, -999)
       and target.loc      = source.loc
       and target.loc_type = source.loc_type)
   when MATCHED then
      update
         set target.curr_avail   = source.curr_avail,
             target.future_avail = source.future_avail;

   /* subtract off CO from future avail */
   merge into alc_merch_hier_rloh_temp target
   using (with il_explode as
             (--tran_item at tran_item level
              select tmp.alloc_id,
                     tmp.dept,
                     tmp.class,
                     tmp.subclass,
                     im.item rloh_item,
                     im.item join_item,
                     tmp.loc,
                     1 pack_qty
                from alc_merch_hier_rloh_temp tmp,
                     item_master im
               where tmp.alloc_id   = I_alloc_id
                 and tmp.dept       = im.dept
                 and im.class       = NVL(tmp.class, im.class)
                 and im.subclass    = NVL(tmp.subclass, im.subclass)
                 and im.item_level  = im.tran_level
                 and im.pack_ind    = 'N'
              union all
              --tran_item as comp in a pack
              select tmp.alloc_id,
                     tmp.dept,
                     tmp.class,
                     tmp.subclass,
                     im.item rloh_item,
                     p.pack_no join_item,
                     tmp.loc,
                     p.pack_item_qty pack_qty
                from alc_merch_hier_rloh_temp tmp,
                     item_master im,
                     packitem_breakout p
               where tmp.alloc_id   = I_alloc_id
                 and tmp.dept       = im.dept
                 and im.class       = NVL(tmp.class, im.class)
                 and im.subclass    = NVL(tmp.subclass, im.subclass)
                 and im.item_level  = im.tran_level
                 and im.pack_ind    = 'N'
                 and im.item        = p.item
          )
          --
          select il_explode.alloc_id,
                 il_explode.dept,
                 il_explode.class,
                 il_explode.subclass,
                 il_explode.loc,
                 NVL(SUM(il_explode.pack_qty * (NVL(td.tsf_qty,0) - NVL(td.ship_qty,0))),0) co_alloc_in,
                 NVL(SUM(il_explode.pack_qty * (NVL(td.ship_qty,0) - NVL(td.received_qty,0))),0) co_intran
            from il_explode,
                 tsfhead th,
                 tsfdetail td,
                 item_loc il
           where th.tsf_type          = 'CO'
             and th.status            IN ('A','S','P','L','C')
             and th.tsf_no            = td.tsf_no
             and td.item              = il_explode.join_item
             and th.to_loc            = il_explode.loc
             --
             and il_explode.join_item = il.item
             and il_explode.loc       = il.loc
             and il.clear_ind         = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                               'N', 'N',
                                               il.clear_ind)
           group by il_explode.alloc_id,
                    il_explode.dept,
                    il_explode.class,
                    il_explode.subclass,
                    il_explode.loc) source
   on (    target.alloc_id           = source.alloc_id
       and target.dept               = source.dept
       and NVL(target.class,-999)    = NVL(source.class,-999)
       and NVL(target.subclass,-999) = NVL(source.subclass,-999)
       and target.loc      = source.loc)
   when MATCHED then
      update
         set target.future_avail =   target.future_avail
                                   - DECODE(I_alc_rule_row.inbound_allocation_ind,
                                            'Y', source.co_alloc_in,
                                            0)
                                   - DECODE(I_alc_rule_row.in_transit_ind, 'Y', source.co_intran, 0);

   if I_alc_rule_row.on_order_ind = 'Y' then

      merge into alc_merch_hier_rloh_temp target
      using ( with il_explode as
             (--tran_item at tran_item level
               select tmp.alloc_id,
                      tmp.dept,
                      tmp.class,
                      tmp.subclass,
                      im.item rloh_item,
                      im.item join_item,
                      tmp.loc,
                      1 pack_qty
                 from alc_merch_hier_rloh_temp tmp,
                      item_master im
                where tmp.alloc_id   = I_alloc_id
                  and tmp.dept       = im.dept
                  and im.class       = NVL(tmp.class, im.class)
                  and im.subclass    = NVL(tmp.subclass, im.subclass)
                  and im.item_level  = im.tran_level
                  and im.pack_ind    = 'N'
               union all
               --tran_item as comp in a pack
               select tmp.alloc_id,
                      tmp.dept,
                      tmp.class,
                      tmp.subclass,
                      im.item rloh_item,
                      p.pack_no join_item,
                      tmp.loc,
                      p.pack_item_qty pack_qty
                 from alc_merch_hier_rloh_temp tmp,
                      item_master im,
                      packitem_breakout p
                where tmp.alloc_id   = I_alloc_id
                  and tmp.dept       = im.dept
                  and im.class       = NVL(tmp.class, im.class)
                  and im.subclass    = NVL(tmp.subclass, im.subclass)
                  and im.item_level  = im.tran_level
                  and im.pack_ind    = 'N'
                  and im.item        = p.item
             )
             --
             select il_explode.alloc_id,
                    il_explode.dept,
                    il_explode.class,
                    il_explode.subclass,
                    il_explode.loc,
                    NVL(SUM(il_explode.pack_qty * (ol.qty_ordered - NVL(ol.qty_received,0))),0) on_order_qty
               from il_explode,
                    ordloc ol,
                    ordhead oh,
                    item_loc il
              where oh.status          = 'A'
                and oh.order_type     != 'CO'
                and NOT (I_all_orders  = 'N' and oh.include_on_order_ind = 'N')
                and (TRUNC(oh.not_before_date) >= L_hist_start_date or L_hist_start_date is NULL)
                and (TRUNC(oh.not_before_date) <= L_hist_end_date or L_hist_end_date is NULL)
                and oh.order_no         = ol.order_no
                and ol.location         = il_explode.loc
                and ol.item             = il_explode.join_item
                and ol.qty_ordered      > NVL(ol.qty_received,0)
                --
                and ol.item             = il.item
                and ol.location         = il.loc
                and il.clear_ind        = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                                 'N', 'N',
                                                 il.clear_ind)
              group by il_explode.alloc_id,
                       il_explode.dept,
                       il_explode.class,
                       il_explode.subclass,
                       il_explode.loc) source
      on (    target.alloc_id           = source.alloc_id
          and target.dept               = source.dept
          and NVL(target.class,-999)    = NVL(source.class,-999)
          and NVL(target.subclass,-999) = NVL(source.subclass,-999)
          and target.loc                = source.loc)
      when MATCHED then
         update
            set target.future_avail = target.future_avail + source.on_order_qty;

   end if;

   if I_alc_rule_row.inbound_allocation_ind = 'Y' then

      merge into alc_merch_hier_rloh_temp target
      using (with il_explode as
             (--tran_item at tran_item level
              select tmp.alloc_id,
                     tmp.dept,
                     tmp.class,
                     tmp.subclass,
                     im.item rloh_item,
                     im.item join_item,
                     tmp.loc,
                     1 pack_qty
                from alc_merch_hier_rloh_temp tmp,
                     item_master im
               where tmp.alloc_id   = I_alloc_id
                 and tmp.dept       = im.dept
                 and im.class       = NVL(tmp.class, im.class)
                 and im.subclass    = NVL(tmp.subclass, im.subclass)
                 and im.item_level  = im.tran_level
                 and im.pack_ind    = 'N'
              union all
              --tran_item as comp in a pack
              select tmp.alloc_id,
                     tmp.dept,
                     tmp.class,
                     tmp.subclass,
                     im.item rloh_item,
                     p.pack_no join_item,
                     tmp.loc,
                     p.pack_item_qty pack_qty
                from alc_merch_hier_rloh_temp tmp,
                     item_master im,
                     packitem_breakout p
               where tmp.alloc_id   = I_alloc_id
                 and tmp.dept       = im.dept
                 and im.class       = NVL(tmp.class, im.class)
                 and im.subclass    = NVL(tmp.subclass, im.subclass)
                 and im.item_level  = im.tran_level
                 and im.pack_ind    = 'N'
                 and im.item        = p.item
             )
             --
             select /*+ ORDERED USE_NL(d,h) */
                    il_explode.alloc_id,
                    il_explode.dept,
                    il_explode.class,
                    il_explode.subclass,
                    il_explode.loc,
                    NVL(SUM(il_explode.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_in_qty
               from il_explode,
                    alloc_header h,
                    alloc_detail d,
                    item_loc il
              where h.item          = il_explode.join_item
                and d.alloc_no      = h.alloc_no
                and h.status        IN ('A', 'R')
                and d.qty_allocated > NVL(d.qty_transferred,0)
                and d.to_loc        = il_explode.loc
                and (EXISTS (select 'x'
                               from ordhead o
                              where o.order_no = h.order_no
                                and o.status   in ('A', 'C')
                                and NVL(L_hist_start_date, TRUNC(o.not_before_date)) <= TRUNC(o.not_before_date)
                                and NVL(L_hist_end_date, TRUNC(o.not_before_date))   >= TRUNC(o.not_before_date)
                                and NOT (I_all_orders = 'N' and o.include_on_order_ind = 'N')
                                and rownum = 1) or
                     EXISTS (select 'x'
                               from alloc_header h1
                              where h1.alloc_no = h.order_no
                                and h1.status   IN ('A', 'R')
                                and NVL(L_hist_start_date, h1.release_date) <= h1.release_date
                                and NVL(L_hist_end_date, h1.release_date)   >= h1.release_date
                                and rownum = 1) or
                     EXISTS (select 'x'
                               from tsfhead t
                              where t.tsf_no = h.order_no
                                and t.status IN ('A','S','P','L','C')
                                and (t.not_after_date is NULL or
                                     (NVL(L_hist_start_date, NVL(t.delivery_date,L_sysdate)) <= NVL(t.delivery_date,L_sysdate)))
                                and (t.not_after_date is NULL or
                                     (NVL(L_hist_end_date, NVL(t.not_after_date,L_sysdate))   <= NVL(t.not_after_date,L_sysdate)))
                                and rownum = 1))
                --
                and h.item          = il.item
                and d.to_loc        = il.loc
                and il.clear_ind    = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                             'N', 'N',
                                             il.clear_ind)
              group by il_explode.alloc_id,
                       il_explode.dept,
                       il_explode.class,
                       il_explode.subclass,
                       il_explode.loc) source
      on (    target.alloc_id           = source.alloc_id
          and target.dept               = source.dept
          and NVL(target.class,-999)    = NVL(source.class,-999)
          and NVL(target.subclass,-999) = NVL(source.subclass,-999)
          and target.loc      = source.loc)
      when MATCHED then
         update
            set target.future_avail = target.future_avail + source.alloc_in_qty;

   end if;

   if I_alc_rule_row.outbound_allocation_ind = 'Y' then

      merge into alc_merch_hier_rloh_temp target
      using (with il_explode as
                (--tran_item at tran_item level
                 select tmp.alloc_id,
                        tmp.dept,
                        tmp.class,
                        tmp.subclass,
                        im.item rloh_item,
                        im.item join_item,
                        tmp.loc,
                        1 pack_qty
                   from alc_merch_hier_rloh_temp tmp,
                        item_master im
                  where tmp.alloc_id   = I_alloc_id
                    and tmp.dept       = im.dept
                    and im.class       = NVL(tmp.class, im.class)
                    and im.subclass    = NVL(tmp.subclass, im.subclass)
                    and im.item_level  = im.tran_level
                    and im.pack_ind    = 'N'
                 union all
                 --tran_item as comp in a pack
                 select tmp.alloc_id,
                        tmp.dept,
                        tmp.class,
                        tmp.subclass,
                        im.item rloh_item,
                        p.pack_no join_item,
                        tmp.loc,
                        p.pack_item_qty pack_qty
                   from alc_merch_hier_rloh_temp tmp,
                        item_master im,
                        packitem_breakout p
                  where tmp.alloc_id   = I_alloc_id
                    and tmp.dept       = im.dept
                    and im.class       = NVL(tmp.class, im.class)
                    and im.subclass    = NVL(tmp.subclass, im.subclass)
                    and im.item_level  = im.tran_level
                    and im.pack_ind    = 'N'
                    and im.item        = p.item
             )
             --
             select /*+ ORDERED USE_NL(d,h) */
                    il_explode.alloc_id,
                    il_explode.dept,
                    il_explode.class,
                    il_explode.subclass,
                    il_explode.loc,
                    NVL(SUM(il_explode.pack_qty * (d.qty_allocated - NVL(d.qty_transferred,0))),0) alloc_out_qty
               from il_explode,
                    alloc_header h,
                    alloc_detail d,
                    item_loc il
              where h.item          = il_explode.join_item
                and d.alloc_no      = h.alloc_no
                and h.status        in ('A', 'R')
                and d.qty_allocated > NVL(d.qty_transferred,0)
                and h.wh            = il_explode.loc
                and (EXISTS (select 'x'
                               from ordhead o
                              where o.order_no = h.order_no
                                and o.status   in ('A', 'C')
                                and NVL(L_hist_start_date, TRUNC(o.not_before_date)) <= TRUNC(o.not_before_date)
                                and NVL(L_hist_end_date, TRUNC(o.not_before_date))   >= TRUNC(o.not_before_date)
                                and NOT (I_all_orders = 'N' and o.include_on_order_ind = 'N')
                                and rownum = 1) or
                     exists (select 'x'
                               from alloc_header h1
                              where h1.alloc_no = h.order_no
                                and h1.status   IN ('A', 'R')
                                and NVL(L_hist_start_date, h1.release_date) <= h1.release_date
                                and NVL(L_hist_end_date, h1.release_date)   >= h1.release_date
                                and rownum = 1) or
                     exists (select 'x'
                               from tsfhead t
                              where t.tsf_no = h.order_no
                                and t.status in ('A','S','P','L','C')
                                and (t.not_after_date is NULL or
                                     (NVL(L_hist_start_date, NVL(t.delivery_date,L_sysdate)) <= NVL(t.delivery_date,L_sysdate)))
                                and (t.not_after_date is NULL or
                                     (NVL(L_hist_end_date, NVL(t.not_after_date,L_sysdate))   <= NVL(t.not_after_date,L_sysdate)))
                                and rownum = 1))
                --
                and h.item          = il.item
                and d.to_loc        = il.loc
                and il.clear_ind    = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                             'N', 'N',
                                             il.clear_ind)
              group by il_explode.alloc_id,
                       il_explode.dept,
                       il_explode.class,
                       il_explode.subclass,
                       il_explode.loc) source
      on (    target.alloc_id           = source.alloc_id
          and target.dept               = source.dept
          and NVL(target.class,-999)    = NVL(source.class,-999)
          and NVL(target.subclass,-999) = NVL(source.subclass,-999)
          and target.loc                = source.loc)
      when MATCHED then
         update
            set target.future_avail = target.future_avail - source.alloc_out_qty;

   end if;

   if L_wh_crosslink_ind = 'Y' and I_alc_rule_row.inbound_allocation_ind = 'Y' then

      merge into alc_merch_hier_rloh_temp target
      using (with il_explode as
                (--tran_item at tran_item level
                 select tmp.alloc_id,
                        tmp.dept,
                        tmp.class,
                        tmp.subclass,
                        im.item rloh_item,
                        im.item join_item,
                        tmp.loc,
                        1 pack_qty
                   from alc_merch_hier_rloh_temp tmp,
                        item_master im
                  where tmp.alloc_id   = I_alloc_id
                    and tmp.dept       = im.dept
                    and im.class       = NVL(tmp.class, im.class)
                    and im.subclass    = NVL(tmp.subclass, im.subclass)
                    and im.item_level  = im.tran_level
                    and im.pack_ind    = 'N'
                 union all
                 --tran_item as comp in a pack
                 select tmp.alloc_id,
                        tmp.dept,
                        tmp.class,
                        tmp.subclass,
                        im.item rloh_item,
                        p.pack_no join_item,
                        tmp.loc,
                        p.pack_item_qty pack_qty
                   from alc_merch_hier_rloh_temp tmp,
                        item_master im,
                        packitem_breakout p
                  where tmp.alloc_id   = I_alloc_id
                    and tmp.dept       = im.dept
                    and im.class       = NVL(tmp.class, im.class)
                    and im.subclass    = NVL(tmp.subclass, im.subclass)
                    and im.item_level  = im.tran_level
                    and im.pack_ind    = 'N'
                    and im.item        = p.item
              )
              --
              select /*+ ORDERED */
                     il_explode.alloc_id,
                     il_explode.dept,
                     il_explode.class,
                     il_explode.subclass,
                     il_explode.loc,
                     NVL(SUM(il_explode.pack_qty * (d.tsf_qty - NVL(d.ship_qty,0))),0) pl_qty
                from il_explode,
                     tsfhead h,
                     tsfdetail d,
                     item_loc il
               where d.item        = il_explode.join_item
                 and h.to_loc      = il_explode.loc
                 and h.to_loc_type = 'S'
                 and h.tsf_no      = d.tsf_no
                 and h.status      in ('A','E','S','B')
                 and h.tsf_type    = 'PL'
                 and d.tsf_qty     > NVL(d.ship_qty,0)
                 and TRUNC(h.delivery_date) >= NVL(L_hist_start_date, TRUNC(h.delivery_date))
                 and TRUNC(h.delivery_date) <= NVL(L_hist_end_date, TRUNC(h.delivery_date))
                 --
                 and d.item        = il.item
                 and h.to_loc      = il.loc
                 and il.clear_ind  = DECODE(I_alc_rule_row.include_clearance_stock_ind,
                                            'N', 'N',
                                            il.clear_ind)
               group by il_explode.alloc_id,
                        il_explode.dept,
                        il_explode.class,
                        il_explode.subclass,
                        il_explode.loc) source
      on (    target.alloc_id           = source.alloc_id
          and target.dept               = source.dept
          and NVL(target.class,-999)    = NVL(source.class,-999)
          and NVL(target.subclass,-999) = NVL(source.subclass,-999)
          and target.loc                = source.loc)
      when MATCHED then
         update
            set target.future_avail = target.future_avail + source.pl_qty;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RLOH_HIER_LVL_INV;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_NEED_DATES(O_error_message   IN OUT VARCHAR2,
                        I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                        I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.POP_NEED_DATES';

BEGIN

   if I_alc_rule_row.rule_level is NOT NULL then

      if GET_RULE_HIST_EOW_DATES(O_error_message,
                                 I_alloc_id,
                                 I_alc_rule_row,
                                 NULL,
                                 -1) = FALSE then
         return FALSE;
      end if;

   else

      if GET_AH_RULE_HIST_EOW_DATES(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_NEED_DATES;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_SIZE_PROFILE(O_error_message   IN OUT VARCHAR2,
                          I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                          I_alc_rule_row    IN     ALC_RULE%ROWTYPE,
                          O_expand_pack     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.POP_SIZE_PROFILE';

   L_dept_ind       VARCHAR2(1) := 'N';
   L_class_ind      VARCHAR2(1) := 'N';
   L_subclass_ind   VARCHAR2(1) := 'N';
   L_style_ind      VARCHAR2(1) := 'N';
   L_stylecolor_ind VARCHAR2(1) := 'N';

   L_process_staple NUMBER(10)  := 0;
   L_expand_pack    VARCHAR2(1) := 'N';

   cursor C_VALID_LEVELS is
      select valid_lvl
        from ( select REGEXP_SUBSTR(TP_SIZEPROF_VALID_LEVELS,'[^,]+', 1, level) valid_lvl
                 from alc_system_options
              connect by regexp_substr(TP_SIZEPROF_VALID_LEVELS, '[^,]+', 1, level) is NOT NULL);

   cursor C_ITEM_TYPE_PACK is
      select NVL(SUM(invalid_cnt), 0)
        from (select 1 invalid_cnt
                from alc_item_source a
               where a.alloc_id  = I_alloc_id
                 and a.item_type NOT IN(ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                        ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                        ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK)
              union all
              select COUNT(DISTINCT im.item_parent) - 1 invalid_cnt
                from alc_item_source a,
                     packitem_breakout pb,
                     item_master im
               where a.alloc_id  = I_alloc_id
                 and a.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                     ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                     ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK)
                 and a.item_id   = pb.pack_no
                 and pb.item     = im.item
               group by pb.pack_no);

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program );

   if LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION then

      open C_ITEM_TYPE_PACK;
      fetch C_ITEM_TYPE_PACK into L_process_staple;
      close C_ITEM_TYPE_PACK;

      if NVL(I_alc_rule_row.pack_threshold,0) != -1 then
         L_expand_pack :=  'Y';
      end if;

   end if;

   if (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION and L_process_staple > 0) or I_alc_rule_row.size_profile_type = 'SO' then
      return TRUE;
   end if;

   for rec in C_VALID_LEVELS loop

      if rec.valid_lvl = 'DEPT' then
         L_dept_ind := 'Y';
      elsif rec.valid_lvl = 'CLASS' then
         L_class_ind := 'Y';
      elsif rec.valid_lvl = 'SUBCLASS' then
         L_subclass_ind := 'Y';
      elsif rec.valid_lvl = 'STYLE' then
         L_style_ind := 'Y';
      elsif rec.valid_lvl = 'STYLE/COLOR' then
         L_stylecolor_ind := 'Y';
      end if;

   end loop;

   --GID ID is set
   if I_alc_rule_row.size_profile != 'Hierarchy' then

      --STYLECOLOR
      if L_stylecolor_ind = 'Y' then

         merge /*+ INDEX(target, ALC_CALC_ALLITEMLOC_TEMP_I1) LEADING(use_this) */ into alc_calc_allitemloc_temp target
         using (
                 select il.alloc_id,
                        il.tran_item,
                        il.to_loc,
                        il.source_item,
                        il.source_diff1_id,
                        il.source_diff2_id,
                        il.source_diff3_id,
                        il.source_diff4_id,
                        p.qty size_profile_qty,
                        --
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              COUNT(p.loc) OVER (PARTITION BY il.alloc_id,
                                                              il.source_item,
                                                              il.to_loc)
                        else
                           1
                        end as group_sp_count,
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              COUNT(*) OVER (PARTITION BY il.alloc_id,
                                                          il.source_item,
                                                          il.to_loc)
                        else
                           1
                        end as group_count,
                        --
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                            il.source_item,
                                                            il.to_loc)
                        else
                           SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                         il.source_item,
                                                         il.source_diff1_id,
                                                         il.source_diff2_id,
                                                         il.source_diff3_id,
                                                         il.source_diff4_id,
                                                         il.to_loc)
                        end as total_qty
                   from (select d.alloc_id,
                                d.source_item,
                                d.source_diff1_id,
                                d.source_diff2_id,
                                d.source_diff3_id,
                                d.source_diff4_id,
                                d.tran_item,
                                d.tran_diff1_id,
                                d.tran_diff2_id,
                                d.tran_diff3_id,
                                d.tran_diff4_id,
                                d.dept,
                                d.class,
                                d.subclass,
                                d.to_loc,
                                --
                                agp.gid_profile_id
                           from alc_calc_allitemloc_temp d,
                                alc_gid_header agh,
                                alc_gid_profile agp
                          where d.alloc_id    = I_alloc_id
                            and L_expand_pack  = 'N'
                            and d.size_profile_qty is NULL
                            and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                            --
                            and agh.id              = I_alc_rule_row.size_profile
                            and agp.gid_id          = agh.id
                         union all
                         select DISTINCT d.alloc_id,
                                d.source_item,
                                DECODE(d.source_diff1_id,
                                       NULL, NULL,
                                       im.diff_1),
                                DECODE(d.source_diff2_id,
                                       NULL, NULL,
                                       im.diff_2),
                                DECODE(d.source_diff3_id,
                                       NULL, NULL,
                                       im.diff_3),
                                DECODE(d.source_diff4_id,
                                       NULL, NULL,
                                       im.diff_4),
                                --
                                im.item,
                                im.diff_1,
                                im.diff_2,
                                im.diff_3,
                                im.diff_4,
                                --
                                d.dept,
                                d.class,
                                d.subclass,
                                d.to_loc,
                                --
                                agp.gid_profile_id
                           from alc_calc_allitemloc_temp d,
                                item_master im,
                                alc_gid_header agh,
                                alc_gid_profile agp
                          where d.alloc_id     = I_alloc_id
                            and L_expand_pack  = 'Y'
                            and d.source_item  = im.item_parent
                            and d.size_profile_qty is NULL
                            and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                            --
                            and agh.id              = I_alc_rule_row.size_profile
                            and agp.gid_id          = agh.id) il,
                        alc_size_profile p
                  where il.alloc_id          = I_alloc_id
                     and p.loc(+)            = TO_CHAR(il.to_loc)
                     --
                     and p.gid_profile_id(+) = il.gid_profile_id
                     --
                     and p.style(+)          = il.source_item
                     and p.dept(+)           is NULL
                     and p.class(+)          is NULL
                     and p.subclass(+)       is NULL
                     and NVL(p.size1(+),0)   = NVL(il.tran_diff1_id, 0)
                     and NVL(p.size2(+),0)   = NVL(il.tran_diff2_id, 0)
                     and NVL(p.size3(+),0)   = NVL(il.tran_diff3_id, 0)
                     and NVL(p.size4(+),0)   = NVL(il.tran_diff4_id, 0)
         ) use_this
         on (    target.alloc_id                     = use_this.alloc_id
             and target.source_item                  = use_this.source_item
             and NVL(target.source_diff1_id, '-999') = NVL(use_this.source_diff1_id, '-999')
             and NVL(target.source_diff2_id, '-999') = NVL(use_this.source_diff2_id, '-999')
             and NVL(target.source_diff3_id, '-999') = NVL(use_this.source_diff3_id, '-999')
             and NVL(target.source_diff4_id, '-999') = NVL(use_this.source_diff4_id, '-999')
             and target.tran_item                    = use_this.tran_item
             and target.to_loc                       = use_this.to_loc
             --
             and use_this.size_profile_qty           is NOT NULL
             and use_this.group_sp_count             = use_this.group_count)
         when MATCHED then
            update
               set target.size_profile_qty  = use_this.size_profile_qty,
                   target.total_profile_qty = use_this.total_qty;

      end if;

      --STYLE
      if L_style_ind = 'Y' then

         merge /*+ INDEX(target, ALC_CALC_ALLITEMLOC_TEMP_I1) LEADING(use_this) */ into alc_calc_allitemloc_temp target
         using (
                 select il.alloc_id,
                        il.tran_item,
                        il.to_loc,
                        il.source_item,
                        il.source_diff1_id,
                        il.source_diff2_id,
                        il.source_diff3_id,
                        il.source_diff4_id,
                        p.qty size_profile_qty,
                        --
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              COUNT(p.loc) OVER (PARTITION BY il.alloc_id,
                                                              il.source_item,
                                                              il.to_loc)
                        else
                           1
                        end as group_sp_count,
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              COUNT(*) OVER (PARTITION BY il.alloc_id,
                                                          il.source_item,
                                                          il.to_loc)
                        else
                           1
                        end as group_count,
                        --
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                            il.source_item,
                                                            il.to_loc)
                        else
                           SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                         il.source_item,
                                                         il.source_diff1_id,
                                                         il.source_diff2_id,
                                                         il.source_diff3_id,
                                                         il.source_diff4_id,
                                                         il.to_loc)
                        end as total_qty
                   from (select d.alloc_id,
                                d.source_item,
                                d.source_diff1_id,
                                d.source_diff2_id,
                                d.source_diff3_id,
                                d.source_diff4_id,
                                d.tran_item,
                                d.tran_diff1_id,
                                d.tran_diff2_id,
                                d.tran_diff3_id,
                                d.tran_diff4_id,
                                d.dept,
                                d.class,
                                d.subclass,
                                d.to_loc,
                                --
                                agp.gid_profile_id
                           from alc_calc_allitemloc_temp d,
                                alc_gid_header agh,
                                alc_gid_profile agp
                          where d.alloc_id    = I_alloc_id
                            and L_expand_pack  = 'N'
                            and d.size_profile_qty is NULL
                            and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                            --
                            and agh.id              = I_alc_rule_row.size_profile
                            and agp.gid_id          = agh.id
                         union all
                         select DISTINCT d.alloc_id,
                                d.source_item,
                                DECODE(d.source_diff1_id,
                                       NULL, NULL,
                                       im.diff_1),
                                DECODE(d.source_diff2_id,
                                       NULL, NULL,
                                       im.diff_2),
                                DECODE(d.source_diff3_id,
                                       NULL, NULL,
                                       im.diff_3),
                                DECODE(d.source_diff4_id,
                                       NULL, NULL,
                                       im.diff_4),
                                --
                                im.item,
                                im.diff_1,
                                im.diff_2,
                                im.diff_3,
                                im.diff_4,
                                --
                                d.dept,
                                d.class,
                                d.subclass,
                                d.to_loc,
                                --
                                agp.gid_profile_id
                           from alc_calc_allitemloc_temp d,
                                item_master im,
                                alc_gid_header agh,
                                alc_gid_profile agp
                          where d.alloc_id     = I_alloc_id
                            and L_expand_pack  = 'Y'
                            and d.source_item  = im.item_parent
                            and d.size_profile_qty is NULL
                            and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                            --
                            and agh.id           = I_alc_rule_row.size_profile
                            and agp.gid_id          = agh.id) il,
                        alc_size_profile p
                  where il.alloc_id          = I_alloc_id
                     and p.loc(+)            = TO_CHAR(il.to_loc)
                     --
                     and p.gid_profile_id(+) = il.gid_profile_id
                     --
                     and p.style(+)          = il.source_item
                     and p.dept(+)           is NULL
                     and p.class(+)          is NULL
                     and p.subclass(+)       is NULL
                     --
                     and NVL(p.size1(+),0) = NVL(DECODE(il.source_diff1_id,
                                                        il.tran_diff1_id, NULL,
                                                        il.tran_diff1_id),0)
                     and NVL(p.size2(+),0) = NVL(DECODE(il.source_diff2_id,
                                                        il.tran_diff2_id, NULL,
                                                        il.tran_diff2_id),0)
                     and NVL(p.size3(+),0) = NVL(DECODE(il.source_diff3_id,
                                                        il.tran_diff3_id, NULL,
                                                        il.tran_diff3_id),0)
                     and NVL(p.size4(+),0) = NVL(DECODE(il.source_diff4_id,
                                                        il.tran_diff4_id, NULL,
                                                        il.tran_diff4_id),0)
                     --
         ) use_this
         on (    target.alloc_id                     = use_this.alloc_id
             and target.source_item                  = use_this.source_item
             and NVL(target.source_diff1_id, '-999') = NVL(use_this.source_diff1_id, '-999')
             and NVL(target.source_diff2_id, '-999') = NVL(use_this.source_diff2_id, '-999')
             and NVL(target.source_diff3_id, '-999') = NVL(use_this.source_diff3_id, '-999')
             and NVL(target.source_diff4_id, '-999') = NVL(use_this.source_diff4_id, '-999')
             and target.tran_item                    = use_this.tran_item
             and target.to_loc                       = use_this.to_loc
             --
             and use_this.size_profile_qty           is NOT NULL
             and use_this.group_sp_count             = use_this.group_count)
         when MATCHED then
            update
               set target.size_profile_qty  = use_this.size_profile_qty,
                   target.total_profile_qty = use_this.total_qty;

      end if;

      --SUBCLASS
      if L_subclass_ind = 'Y' then

         merge /*+ INDEX(target, ALC_CALC_ALLITEMLOC_TEMP_I1) LEADING(use_this) */ into alc_calc_allitemloc_temp target
         using (
                 select il.alloc_id,
                        il.tran_item,
                        il.to_loc,
                        il.source_item,
                        il.source_diff1_id,
                        il.source_diff2_id,
                        il.source_diff3_id,
                        il.source_diff4_id,
                        p.qty size_profile_qty,
                        --
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              COUNT(p.loc) OVER (PARTITION BY il.alloc_id,
                                                              il.source_item,
                                                              il.to_loc)
                        else
                           1
                        end as group_sp_count,
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              COUNT(*) OVER (PARTITION BY il.alloc_id,
                                                          il.source_item,
                                                          il.to_loc)
                        else
                           1
                        end as group_count,
                        --
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                            il.source_item,
                                                            il.to_loc)
                        else
                           SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                         il.source_item,
                                                         il.source_diff1_id,
                                                         il.source_diff2_id,
                                                         il.source_diff3_id,
                                                         il.source_diff4_id,
                                                         il.to_loc)
                        end as total_qty
                   from (select d.alloc_id,
                                d.source_item,
                                d.source_diff1_id,
                                d.source_diff2_id,
                                d.source_diff3_id,
                                d.source_diff4_id,
                                d.tran_item,
                                d.tran_diff1_id,
                                d.tran_diff2_id,
                                d.tran_diff3_id,
                                d.tran_diff4_id,
                                d.dept,
                                d.class,
                                d.subclass,
                                d.to_loc,
                                --
                                agp.gid_profile_id
                           from alc_calc_allitemloc_temp d,
                                alc_gid_header agh,
                                alc_gid_profile agp
                          where d.alloc_id    = I_alloc_id
                            and L_expand_pack  = 'N'
                            and d.size_profile_qty is NULL
                            and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                            --
                            and agh.id              = I_alc_rule_row.size_profile
                            and agp.gid_id          = agh.id
                         union all
                         select DISTINCT d.alloc_id,
                                d.source_item,
                                DECODE(d.source_diff1_id,
                                       NULL, NULL,
                                       im.diff_1),
                                DECODE(d.source_diff2_id,
                                       NULL, NULL,
                                       im.diff_2),
                                DECODE(d.source_diff3_id,
                                       NULL, NULL,
                                       im.diff_3),
                                DECODE(d.source_diff4_id,
                                       NULL, NULL,
                                       im.diff_4),
                                --
                                im.item,
                                im.diff_1,
                                im.diff_2,
                                im.diff_3,
                                im.diff_4,
                                --
                                d.dept,
                                d.class,
                                d.subclass,
                                d.to_loc,
                                --
                                agp.gid_profile_id
                           from alc_calc_allitemloc_temp d,
                                item_master im,
                                alc_gid_header agh,
                                alc_gid_profile agp
                          where d.alloc_id     = I_alloc_id
                            and L_expand_pack  = 'Y'
                            and d.source_item  = im.item_parent
                            and d.size_profile_qty is NULL
                            and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                     --
                     and agh.id           = I_alc_rule_row.size_profile
                            and agp.gid_id          = agh.id) il,
                        alc_size_profile p
                  where il.alloc_id          = I_alloc_id
                     and p.loc(+)            = TO_CHAR(il.to_loc)
                     --
                     and p.gid_profile_id(+) = il.gid_profile_id
                     --
                     and NVL(p.size1(+),0) = NVL(DECODE(il.source_diff1_id,
                                                        il.tran_diff1_id, NULL,
                                                        il.tran_diff1_id),
                                                 0)
                     and NVL(p.size2(+),0) = NVL(DECODE(il.source_diff2_id,
                                                        il.tran_diff2_id, NULL,
                                                        il.tran_diff2_id),
                                                 0)
                     and NVL(p.size3(+),0) = NVL(DECODE(il.source_diff3_id,
                                                        il.tran_diff3_id, NULL,
                                                        il.tran_diff3_id),
                                                 0)
                     and NVL(p.size4(+),0) = NVL(DECODE(il.source_diff4_id,
                                                        il.tran_diff4_id, NULL,
                                                        il.tran_diff4_id),
                                                 0)
                     and p.style(+)        is NULL
                     and p.dept(+)         = il.dept
                     and p.class(+)        = il.class
                     and p.subclass(+)     = il.subclass
         ) use_this
         on (    target.alloc_id                     = use_this.alloc_id
             and target.source_item                  = use_this.source_item
             and NVL(target.source_diff1_id, '-999') = NVL(use_this.source_diff1_id, '-999')
             and NVL(target.source_diff2_id, '-999') = NVL(use_this.source_diff2_id, '-999')
             and NVL(target.source_diff3_id, '-999') = NVL(use_this.source_diff3_id, '-999')
             and NVL(target.source_diff4_id, '-999') = NVL(use_this.source_diff4_id, '-999')
             and target.tran_item                    = use_this.tran_item
             and target.to_loc                       = use_this.to_loc
             --
             and use_this.size_profile_qty           is NOT NULL
             and use_this.group_sp_count             = use_this.group_count)
         when MATCHED then
            update
               set target.size_profile_qty  = use_this.size_profile_qty,
                   target.total_profile_qty = use_this.total_qty;

      end if;

      --CLASS
      if L_class_ind = 'Y' then

         merge /*+ INDEX(target, ALC_CALC_ALLITEMLOC_TEMP_I1) LEADING(use_this) */ into alc_calc_allitemloc_temp target
         using (
                 select il.alloc_id,
                        il.tran_item,
                        il.to_loc,
                        il.source_item,
                        il.source_diff1_id,
                        il.source_diff2_id,
                        il.source_diff3_id,
                        il.source_diff4_id,
                        p.qty size_profile_qty,
                        --
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              COUNT(p.loc) OVER (PARTITION BY il.alloc_id,
                                                              il.source_item,
                                                              il.to_loc)
                        else
                           1
                        end as group_sp_count,
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              COUNT(*) OVER (PARTITION BY il.alloc_id,
                                                          il.source_item,
                                                          il.to_loc)
                        else
                           1
                        end as group_count,
                        --
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                            il.source_item,
                                                            il.to_loc)
                        else
                           SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                         il.source_item,
                                                         il.source_diff1_id,
                                                         il.source_diff2_id,
                                                         il.source_diff3_id,
                                                         il.source_diff4_id,
                                                         il.to_loc)
                        end as total_qty
                   from (select d.alloc_id,
                                d.source_item,
                                d.source_diff1_id,
                                d.source_diff2_id,
                                d.source_diff3_id,
                                d.source_diff4_id,
                                d.tran_item,
                                d.tran_diff1_id,
                                d.tran_diff2_id,
                                d.tran_diff3_id,
                                d.tran_diff4_id,
                                d.dept,
                                d.class,
                                d.subclass,
                                d.to_loc,
                                --
                                agp.gid_profile_id
                           from alc_calc_allitemloc_temp d,
                                alc_gid_header agh,
                                alc_gid_profile agp
                          where d.alloc_id    = I_alloc_id
                            and L_expand_pack  = 'N'
                            and d.size_profile_qty is NULL
                            and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                            --
                            and agh.id              = I_alc_rule_row.size_profile
                            and agp.gid_id          = agh.id
                          union all
                         select DISTINCT d.alloc_id,
                                d.source_item,
                                DECODE(d.source_diff1_id,
                                       NULL,NULL,
                                       im.diff_1),
                                DECODE(d.source_diff2_id,
                                       NULL,NULL,
                                       im.diff_2),
                                DECODE(d.source_diff3_id,
                                       NULL,NULL,
                                       im.diff_3),
                                DECODE(d.source_diff4_id,
                                       NULL,NULL,
                                       im.diff_4),
                                --
                                im.item,
                                im.diff_1,
                                im.diff_2,
                                im.diff_3,
                                im.diff_4,
                                --
                                d.dept,
                                d.class,
                                d.subclass,
                                d.to_loc,
                                --
                                agp.gid_profile_id
                           from alc_calc_allitemloc_temp d,
                                item_master im,
                                alc_gid_header agh,
                                alc_gid_profile agp
                          where d.alloc_id     = I_alloc_id
                            and L_expand_pack  = 'Y'
                            and d.source_item  = im.item_parent
                            and d.size_profile_qty is NULL
                            and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                            --
                            and agh.id           = I_alc_rule_row.size_profile
                            and agp.gid_id          = agh.id) il,
                        alc_size_profile p
                  where il.alloc_id          = I_alloc_id
                     and p.loc(+)            = to_char(il.to_loc)
                     --
                     and p.gid_profile_id(+) = il.gid_profile_id
                     --
                     and NVL(p.size1(+),0) = NVL(DECODE(il.source_diff1_id,il.tran_diff1_id,
                                                        NULL,il.tran_diff1_id),
                                                 0)
                     and NVL(p.size2(+),0) = NVL(DECODE(il.source_diff2_id,il.tran_diff2_id,
                                                        NULL,il.tran_diff2_id),
                                                 0)
                     and NVL(p.size3(+),0) = NVL(DECODE(il.source_diff3_id,il.tran_diff3_id,
                                                        NULL,il.tran_diff3_id),
                                                 0)
                     and NVL(p.size4(+),0) = NVL(DECODE(il.source_diff4_id,il.tran_diff4_id,
                                                        NULL,il.tran_diff4_id),
                                                 0)
                     and p.style(+)        is NULL
                     and p.dept(+)         = il.dept
                     and p.class(+)        = il.class
                     and p.subclass(+)     is NULL
         ) use_this
         on (    target.alloc_id                     = use_this.alloc_id
             and target.source_item                  = use_this.source_item
             and NVL(target.source_diff1_id, '-999') = NVL(use_this.source_diff1_id, '-999')
             and NVL(target.source_diff2_id, '-999') = NVL(use_this.source_diff2_id, '-999')
             and NVL(target.source_diff3_id, '-999') = NVL(use_this.source_diff3_id, '-999')
             and NVL(target.source_diff4_id, '-999') = NVL(use_this.source_diff4_id, '-999')
             and target.tran_item                    = use_this.tran_item
             and target.to_loc                       = use_this.to_loc
             --
             and use_this.size_profile_qty           is NOT NULL
             and use_this.group_sp_count             = use_this.group_count)
         when MATCHED then
            update
               set target.size_profile_qty  = use_this.size_profile_qty,
                   target.total_profile_qty = use_this.total_qty;
      end if;

      --DEPT
      if L_dept_ind = 'Y' then

         merge /*+ INDEX(target, ALC_CALC_ALLITEMLOC_TEMP_I1) LEADING(use_this) */ into alc_calc_allitemloc_temp target
         using (
                 select il.alloc_id,
                        il.tran_item,
                        il.to_loc,
                        il.source_item,
                        il.source_diff1_id,
                        il.source_diff2_id,
                        il.source_diff3_id,
                        il.source_diff4_id,
                        p.qty size_profile_qty,
                        --
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                           COUNT(p.loc) OVER (PARTITION BY il.alloc_id,
                                                           il.source_item,
                                                           il.to_loc)
                        else
                           1
                        end as group_sp_count,
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                           COUNT(*) OVER (PARTITION BY il.alloc_id,
                                                       il.source_item,
                                                       il.to_loc)
                        else
                           1
                        end as group_count,
                        --
                        case
                           when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                 (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                              SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                            il.source_item,
                                                            il.to_loc)
                        else
                           SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                         il.source_item,
                                                         il.source_diff1_id,
                                                         il.source_diff2_id,
                                                         il.source_diff3_id,
                                                         il.source_diff4_id,
                                                         il.to_loc)
                        end as total_qty
                   from (select d.alloc_id,
                                d.source_item,
                                d.source_diff1_id,
                                d.source_diff2_id,
                                d.source_diff3_id,
                                d.source_diff4_id,
                                d.tran_item,
                                d.tran_diff1_id,
                                d.tran_diff2_id,
                                d.tran_diff3_id,
                                d.tran_diff4_id,
                                d.dept,
                                d.class,
                                d.subclass,
                                d.to_loc,
                                --
                                agp.gid_profile_id
                           from alc_calc_allitemloc_temp d,
                                alc_gid_header agh,
                                alc_gid_profile agp
                          where d.alloc_id    = I_alloc_id
                            and L_expand_pack  = 'N'
                            and d.size_profile_qty is NULL
                            and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                            --
                            and agh.id              = I_alc_rule_row.size_profile
                            and agp.gid_id          = agh.id
                         union all
                         select DISTINCT d.alloc_id,
                                d.source_item,
                                DECODE(d.source_diff1_id,
                                       NULL, NULL,
                                       im.diff_1),
                                DECODE(d.source_diff2_id,
                                       NULL, NULL,
                                       im.diff_2),
                                DECODE(d.source_diff3_id,
                                       NULL, NULL,
                                       im.diff_3),
                                DECODE(d.source_diff4_id,
                                       NULL, NULL,
                                       im.diff_4),
                                --
                                im.item,
                                im.diff_1,
                                im.diff_2,
                                im.diff_3,
                                im.diff_4,
                                --
                                d.dept,
                                d.class,
                                d.subclass,
                                d.to_loc,
                                --
                                agp.gid_profile_id
                           from alc_calc_allitemloc_temp d,
                                item_master im,
                                alc_gid_header agh,
                                alc_gid_profile agp
                          where d.alloc_id     = I_alloc_id
                            and L_expand_pack  = 'Y'
                            and d.source_item  = im.item_parent
                            and d.size_profile_qty is NULL
                            and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                            --
                            and agh.id         = I_alc_rule_row.size_profile
                            and agp.gid_id     = agh.id) il,
                        alc_size_profile p
                  where il.alloc_id          = I_alloc_id
                     and p.loc(+)            = TO_CHAR(il.to_loc)
                     --
                     and p.gid_profile_id(+) = il.gid_profile_id
                     --
                     and NVL(p.size1(+),0) = NVL(DECODE(il.source_diff1_id,
                                                        il.tran_diff1_id, NULL,
                                                        il.tran_diff1_id),
                                                 0)
                     and NVL(p.size2(+),0) = NVL(DECODE(il.source_diff2_id,
                                                        il.tran_diff2_id, NULL,
                                                        il.tran_diff2_id),
                                                 0)
                     and NVL(p.size3(+),0) = NVL(DECODE(il.source_diff3_id,
                                                        il.tran_diff3_id, NULL,
                                                        il.tran_diff3_id),
                                                 0)
                     and NVL(p.size4(+),0) = NVL(DECODE(il.source_diff4_id,
                                                        il.tran_diff4_id, NULL,
                                                        il.tran_diff4_id),
                                                 0)
                     and p.style(+)        is NULL
                     and p.dept(+)         = il.dept
                     and p.class(+)        is NULL
                     and p.subclass(+)     is NULL
         ) use_this
         on (    target.alloc_id                     = use_this.alloc_id
             and target.source_item                  = use_this.source_item
             and NVL(target.source_diff1_id, '-999') = NVL(use_this.source_diff1_id, '-999')
             and NVL(target.source_diff2_id, '-999') = NVL(use_this.source_diff2_id, '-999')
             and NVL(target.source_diff3_id, '-999') = NVL(use_this.source_diff3_id, '-999')
             and NVL(target.source_diff4_id, '-999') = NVL(use_this.source_diff4_id, '-999')
             and target.tran_item                    = use_this.tran_item
             and target.to_loc                       = use_this.to_loc
             --
             and use_this.size_profile_qty           is NOT NULL
             and use_this.group_sp_count             = use_this.group_count)
         when MATCHED then
            update
               set target.size_profile_qty  = use_this.size_profile_qty,
                   target.total_profile_qty = use_this.total_qty;

      end if;

   end if;

   --GID ID is set, and size_profile_default_hier_ind is Y OR GID ID is not set
   if (I_alc_rule_row.size_profile != 'Hierarchy' and I_alc_rule_row.size_profile_default_hier_ind = 'Y')
       or
      (I_alc_rule_row.size_profile = 'Hierarchy') then

      --STYLECOLOR
      if L_stylecolor_ind = 'Y' then

         merge  /*+ INDEX(target, ALC_CALC_ALLITEMLOC_TEMP_I1) LEADING(use_this) */ into alc_calc_allitemloc_temp target
         using (select DISTINCT il.alloc_id,
                       il.tran_item,
                       il.to_loc,
                       il.to_loc_type,
                       il.source_item,
                       il.source_diff1_id,
                       il.source_diff2_id,
                       il.source_diff3_id,
                       il.source_diff4_id,
                       NVL(p.qty, 0) size_profile_qty,
                       --
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                          COUNT(p.loc) OVER (PARTITION BY il.alloc_id,
                                                          il.source_item,
                                                          il.to_loc)
                       else
                          1
                       end as group_sp_count,
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                          COUNT(*) OVER (PARTITION BY il.alloc_id,
                                                      il.source_item,
                                                      il.to_loc)
                       else
                          1
                       end as group_count,
                       --
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                          SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                        il.source_item,
                                                        il.to_loc)
                       else
                          SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                        il.source_item,
                                                        il.source_diff1_id,
                                                        il.source_diff2_id,
                                                        il.source_diff3_id,
                                                        il.source_diff4_id,
                                                        il.to_loc)
                       end as total_qty
                  from (select DISTINCT d.alloc_id,
                               d.source_item,
                               d.source_diff1_id,
                               d.source_diff2_id,
                               d.source_diff3_id,
                               d.source_diff4_id,
                               d.tran_item,
                               d.tran_diff1_id,
                               d.tran_diff2_id,
                               d.tran_diff3_id,
                               d.tran_diff4_id,
                               d.dept,
                               d.class,
                               d.subclass,
                               d.to_loc,
                               d.to_loc_type
                          from alc_calc_allitemloc_temp d
                         where d.alloc_id    = I_alloc_id
                           and L_expand_pack  = 'N'
                           and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                           and d.total_profile_qty is NULL
                        UNION ALL
                        select DISTINCT d.alloc_id,
                               d.source_item,
                               DECODE(d.source_diff1_id,
                                      NULL, NULL,
                                      im.diff_1),
                               DECODE(d.source_diff2_id,
                                      NULL, NULL,
                                      im.diff_2),
                               DECODE(d.source_diff3_id,
                                      NULL, NULL,
                                      im.diff_3),
                               DECODE(d.source_diff4_id,
                                      NULL, NULL,
                                      im.diff_4),
                               --
                               im.item,
                               im.diff_1,
                               im.diff_2,
                               im.diff_3,
                               im.diff_4,
                               --
                               d.dept,
                               d.class,
                               d.subclass,
                               d.to_loc,
                               d.to_loc_type
                          from alc_calc_allitemloc_temp d,
                               item_master im
                         where d.alloc_id     = I_alloc_id
                           and L_expand_pack  = 'Y'
                           and d.source_item  = im.item_parent
                           and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                           and d.total_profile_qty is NULL) il,
                       alc_size_profile p
                 where il.alloc_id          = I_alloc_id
                    and p.loc(+)            = TO_CHAR(il.to_loc)
                    --
                    and p.style(+)          = il.source_item
                    and p.dept(+)           is NULL
                    and p.class(+)          is NULL
                    and p.subclass(+)       is NULL
                    and NVL(p.size1(+),0)   = NVL(il.tran_diff1_id, 0)
                    and NVL(p.size2(+),0)   = NVL(il.tran_diff2_id, 0)
                    and NVL(p.size3(+),0)   = NVL(il.tran_diff3_id, 0)
                    and NVL(p.size4(+),0)   = NVL(il.tran_diff4_id, 0)
                    --
                    and p.gid_profile_id(+) is NULL
         ) use_this
         on (    target.alloc_id                     = use_this.alloc_id
             and target.source_item                  = use_this.source_item
             and NVL(target.source_diff1_id, '-999') = NVL(use_this.source_diff1_id, '-999')
             and NVL(target.source_diff2_id, '-999') = NVL(use_this.source_diff2_id, '-999')
             and NVL(target.source_diff3_id, '-999') = NVL(use_this.source_diff3_id, '-999')
             and NVL(target.source_diff4_id, '-999') = NVL(use_this.source_diff4_id, '-999')
             and target.tran_item                    = use_this.tran_item
             and target.to_loc                       = use_this.to_loc
             --
             and use_this.size_profile_qty           is NOT NULL
             and use_this.group_sp_count             = use_this.group_count)
         when MATCHED then
            update
               set target.size_profile_qty  = use_this.size_profile_qty,
                   target.total_profile_qty = use_this.total_qty;

      end if;

      --STYLE
      if L_style_ind = 'Y' then

         merge  /*+ INDEX(target, ALC_CALC_ALLITEMLOC_TEMP_I1) LEADING(use_this) */ into alc_calc_allitemloc_temp target
         using (select DISTINCT il.alloc_id,
                       il.tran_item,
                       il.to_loc,
                       il.source_item,
                       il.source_diff1_id,
                       il.source_diff2_id,
                       il.source_diff3_id,
                       il.source_diff4_id,
                       NVL(p.qty, 0) size_profile_qty,
                       --
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                             COUNT(p.loc) OVER (PARTITION BY il.alloc_id,
                                                             il.source_item,
                                                             il.to_loc)
                       else
                          1
                       end as group_sp_count,
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                             COUNT(*) OVER (PARTITION BY il.alloc_id,
                                                         il.source_item,
                                                         il.to_loc)
                       else
                          1
                       end as group_count,
                       --
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                             SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                           il.source_item,
                                                           il.to_loc)
                       else
                          SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                        il.source_item,
                                                        il.source_diff1_id,
                                                        il.source_diff2_id,
                                                        il.source_diff3_id,
                                                        il.source_diff4_id,
                                                        il.to_loc)
                       end as total_qty
                  from (select DISTINCT d.alloc_id,
                               d.source_item,
                               d.source_diff1_id,
                               d.source_diff2_id,
                               d.source_diff3_id,
                               d.source_diff4_id,
                               d.tran_item,
                               d.tran_diff1_id,
                               d.tran_diff2_id,
                               d.tran_diff3_id,
                               d.tran_diff4_id,
                               d.dept,
                               d.class,
                               d.subclass,
                               d.to_loc
                          from alc_calc_allitemloc_temp d
                         where d.alloc_id    = I_alloc_id
                           and L_expand_pack  = 'N'
                           and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                           and d.total_profile_qty is NULL
                        union all
                        select DISTINCT d.alloc_id,
                               d.source_item,
                               DECODE(d.source_diff1_id,
                                      NULL, NULL,
                                      im.diff_1),
                               DECODE(d.source_diff2_id,
                                      NULL, NULL,
                                      im.diff_2),
                               DECODE(d.source_diff3_id,
                                      NULL, NULL,
                                      im.diff_3),
                               DECODE(d.source_diff4_id,
                                      NULL, NULL,
                                      im.diff_4),
                               --
                               im.item,
                               im.diff_1,
                               im.diff_2,
                               im.diff_3,
                               im.diff_4,
                               --
                               d.dept,
                               d.class,
                               d.subclass,
                               d.to_loc
                          from alc_calc_allitemloc_temp d,
                               item_master im
                         where d.alloc_id     = I_alloc_id
                           and L_expand_pack  = 'Y'
                           and d.source_item  = im.item_parent
                           and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                           and d.total_profile_qty is NULL) il,
                       alc_size_profile p
                 where il.alloc_id       = I_alloc_id
                    and p.loc(+)         = TO_CHAR(il.to_loc)
                    --
                    and p.style(+)          = il.source_item
                    and p.dept(+)           is NULL
                    and p.class(+)          is NULL
                    and p.subclass(+)       is NULL
                    --
                    and NVL(p.size1(+),0) = NVL(DECODE(il.source_diff1_id,
                                                       il.tran_diff1_id, NULL,
                                                       il.tran_diff1_id),
                                                0)
                    and NVL(p.size2(+),0) = NVL(DECODE(il.source_diff2_id,
                                                       il.tran_diff2_id, NULL,
                                                       il.tran_diff2_id),
                                                0)
                    and NVL(p.size3(+),0) = NVL(DECODE(il.source_diff3_id,
                                                       il.tran_diff3_id, NULL,
                                                       il.tran_diff3_id),
                                                0)
                    and NVL(p.size4(+),0) = NVL(DECODE(il.source_diff4_id,
                                                       il.tran_diff4_id, NULL,
                                                       il.tran_diff4_id),
                                                0)
                    --
                    and p.gid_profile_id(+) is NULL
         ) use_this
         on (    target.alloc_id                     = use_this.alloc_id
             and target.source_item                  = use_this.source_item
             and NVL(target.source_diff1_id, '-999') = NVL(use_this.source_diff1_id, '-999')
             and NVL(target.source_diff2_id, '-999') = NVL(use_this.source_diff2_id, '-999')
             and NVL(target.source_diff3_id, '-999') = NVL(use_this.source_diff3_id, '-999')
             and NVL(target.source_diff4_id, '-999') = NVL(use_this.source_diff4_id, '-999')
             and target.tran_item                    = use_this.tran_item
             and target.to_loc                       = use_this.to_loc
             --
             and use_this.size_profile_qty           is NOT NULL
             and use_this.group_sp_count             = use_this.group_count)
         when MATCHED then
            update
               set target.size_profile_qty  = NVL(use_this.size_profile_qty,-999),
                   target.total_profile_qty = use_this.total_qty;

      end if;

      --SUBCLASS
      if L_subclass_ind = 'Y' then

         merge /*+ index(target, ALC_CALC_ALLITEMLOC_TEMP_I1) leading(use_this) */ into alc_calc_allitemloc_temp target
         using (select DISTINCT il.alloc_id,
                       il.tran_item,
                       il.to_loc,
                       il.source_item,
                       il.source_diff1_id,
                       il.source_diff2_id,
                       il.source_diff3_id,
                       il.source_diff4_id,
                       NVL(p.qty, 0) size_profile_qty,
                       --
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                  (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                          COUNT(p.loc) OVER (PARTITION BY il.alloc_id,
                                                          il.source_item,
                                                          il.to_loc)
                       else
                          1
                       end as group_sp_count,
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                          COUNT(*) OVER (PARTITION BY il.alloc_id,
                                                      il.source_item,
                                                      il.to_loc)
                       else
                          1
                       end as group_count,
                       --
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                          SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                        il.source_item,
                                                        il.to_loc)
                       else
                          SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                        il.source_item,
                                                        il.source_diff1_id,
                                                        il.source_diff2_id,
                                                        il.source_diff3_id,
                                                        il.source_diff4_id,
                                                        il.to_loc)
                       end as total_qty
                  from (select DISTINCT d.alloc_id,
                               d.source_item,
                               d.source_diff1_id,
                               d.source_diff2_id,
                               d.source_diff3_id,
                               d.source_diff4_id,
                               d.tran_item,
                               d.tran_diff1_id,
                               d.tran_diff2_id,
                               d.tran_diff3_id,
                               d.tran_diff4_id,
                               d.dept,
                               d.class,
                               d.subclass,
                               d.to_loc
                          from alc_calc_allitemloc_temp d
                         where d.alloc_id    = I_alloc_id
                           and L_expand_pack  = 'N'
                           and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                           and d.total_profile_qty is NULL
                        union all
                        select DISTINCT d.alloc_id,
                               d.source_item,
                               DECODE(d.source_diff1_id,
                                      NULL, NULL,
                                      im.diff_1),
                               DECODE(d.source_diff2_id,
                                      NULL, NULL,
                                      im.diff_2),
                               DECODE(d.source_diff3_id,
                                      NULL, NULL,
                                      im.diff_3),
                               DECODE(d.source_diff4_id,
                                      NULL, NULL,
                                      im.diff_4),
                               --
                               im.item,
                               im.diff_1,
                               im.diff_2,
                               im.diff_3,
                               im.diff_4,
                               --
                               d.dept,
                               d.class,
                               d.subclass,
                               d.to_loc
                          from alc_calc_allitemloc_temp d,
                               item_master im
                         where d.alloc_id     = I_alloc_id
                           and L_expand_pack  = 'Y'
                           and d.source_item  = im.item_parent
                           and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                           and d.total_profile_qty is NULL) il,
                       alc_size_profile p
                 where il.alloc_id       = I_alloc_id
                    and p.loc(+)         = TO_CHAR(il.to_loc)
                    --
                    and NVL(p.size1(+),0) = NVL(DECODE(il.source_diff1_id,
                                                       il.tran_diff1_id, NULL,
                                                       il.tran_diff1_id),
                                                0)
                    and NVL(p.size2(+),0) = NVL(DECODE(il.source_diff2_id,
                                                       il.tran_diff2_id, NULL,
                                                       il.tran_diff2_id),
                                                0)
                    and NVL(p.size3(+),0) = NVL(DECODE(il.source_diff3_id,
                                                       il.tran_diff3_id, NULL,
                                                       il.tran_diff3_id),
                                                0)
                    and NVL(p.size4(+),0) = NVL(DECODE(il.source_diff4_id,
                                                       il.tran_diff4_id, NULL,
                                                       il.tran_diff4_id),
                                                0)
                    and p.style(+)        is NULL
                    and p.dept(+)         = il.dept
                    and p.class(+)        = il.class
                    and p.subclass(+)     = il.subclass
                    --
                    and p.gid_profile_id(+) is NULL
         ) use_this
         on (    target.alloc_id                     = use_this.alloc_id
             and target.source_item                  = use_this.source_item
             and NVL(target.source_diff1_id, '-999') = NVL(use_this.source_diff1_id, '-999')
             and NVL(target.source_diff2_id, '-999') = NVL(use_this.source_diff2_id, '-999')
             and NVL(target.source_diff3_id, '-999') = NVL(use_this.source_diff3_id, '-999')
             and NVL(target.source_diff4_id, '-999') = NVL(use_this.source_diff4_id, '-999')
             and target.tran_item                    = use_this.tran_item
             and target.to_loc                       = use_this.to_loc
             --
             and use_this.size_profile_qty           is NOT NULL
             and use_this.group_sp_count             = use_this.group_count)
         when MATCHED then
            update
               set target.size_profile_qty  = use_this.size_profile_qty,
                   target.total_profile_qty = use_this.total_qty;

      end if;

      --CLASS
      if L_class_ind = 'Y' then

         merge /*+ INDEX(target, ALC_CALC_ALLITEMLOC_TEMP_I1) LEADING(use_this) */ into alc_calc_allitemloc_temp target
         using (select DISTINCT il.alloc_id,
                       il.tran_item,
                       il.to_loc,
                       il.source_item,
                       il.source_diff1_id,
                       il.source_diff2_id,
                       il.source_diff3_id,
                       il.source_diff4_id,
                       NVL(p.qty, 0) size_profile_qty,
                       --
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                             COUNT(p.loc) OVER (PARTITION BY il.alloc_id,
                                                             il.source_item,
                                                             il.to_loc)
                       else
                          1
                       end as group_sp_count,
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                             COUNT(*) OVER (PARTITION BY il.alloc_id,
                                                         il.source_item,
                                                         il.to_loc)
                       else
                          1
                       end as group_count,
                       --
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                             SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                           il.source_item,
                                                           il.to_loc)
                       else
                          SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                        il.source_item,
                                                        il.source_diff1_id,
                                                        il.source_diff2_id,
                                                        il.source_diff3_id,
                                                        il.source_diff4_id,
                                                        il.to_loc)
                       end as total_qty
                  from (select DISTINCT d.alloc_id,
                               d.source_item,
                               d.source_diff1_id,
                               d.source_diff2_id,
                               d.source_diff3_id,
                               d.source_diff4_id,
                               d.tran_item,
                               d.tran_diff1_id,
                               d.tran_diff2_id,
                               d.tran_diff3_id,
                               d.tran_diff4_id,
                               d.dept,
                               d.class,
                               d.subclass,
                               d.to_loc
                          from alc_calc_allitemloc_temp d
                         where d.alloc_id    = I_alloc_id
                           and L_expand_pack  = 'N'
                           and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                           and d.total_profile_qty is NULL
                        UNION ALL
                        select DISTINCT d.alloc_id,
                               d.source_item,
                               DECODE(d.source_diff1_id,
                                      NULL, NULL,
                                      im.diff_1),
                               DECODE(d.source_diff2_id,
                                      NULL, NULL,
                                      im.diff_2),
                               DECODE(d.source_diff3_id,
                                      NULL, NULL,
                                      im.diff_3),
                               DECODE(d.source_diff4_id,
                                      NULL, NULL,
                                      im.diff_4),
                               --
                               im.item,
                               im.diff_1,
                               im.diff_2,
                               im.diff_3,
                               im.diff_4,
                               --
                               d.dept,
                               d.class,
                               d.subclass,
                               d.to_loc
                          from alc_calc_allitemloc_temp d,
                               item_master im
                         where d.alloc_id     = I_alloc_id
                           and L_expand_pack  = 'Y'
                           and d.source_item  = im.item_parent
                           and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                           and d.total_profile_qty is NULL) il,
                       alc_size_profile p
                 where il.alloc_id       = I_alloc_id
                    and p.loc(+)         = TO_CHAR(il.to_loc)
                    --
                    and NVL(p.size1(+),0) = NVL(DECODE(il.source_diff1_id,
                                                       il.tran_diff1_id, NULL,
                                                       il.tran_diff1_id),
                                                0)
                    and NVL(p.size2(+),0) = NVL(DECODE(il.source_diff2_id,
                                                       il.tran_diff2_id, NULL,
                                                       il.tran_diff2_id),
                                                0)
                    and NVL(p.size3(+),0) = NVL(DECODE(il.source_diff3_id,
                                                       il.tran_diff3_id, NULL,
                                                       il.tran_diff3_id),
                                                0)
                    and NVL(p.size4(+),0) = NVL(DECODE(il.source_diff4_id,
                                                       il.tran_diff4_id, NULL,
                                                       il.tran_diff4_id),
                                                0)
                    and p.style(+)        is NULL
                    and p.dept(+)         = il.dept
                    and p.class(+)        = il.class
                    and p.subclass(+)     is NULL
                    --
                    and p.gid_profile_id(+) is NULL
         ) use_this
         on (    target.alloc_id                     = use_this.alloc_id
             and target.source_item                  = use_this.source_item
             and NVL(target.source_diff1_id, '-999') = NVL(use_this.source_diff1_id, '-999')
             and NVL(target.source_diff2_id, '-999') = NVL(use_this.source_diff2_id, '-999')
             and NVL(target.source_diff3_id, '-999') = NVL(use_this.source_diff3_id, '-999')
             and NVL(target.source_diff4_id, '-999') = NVL(use_this.source_diff4_id, '-999')
             and target.tran_item                    = use_this.tran_item
             and target.to_loc                       = use_this.to_loc
             --
             and use_this.size_profile_qty           is NOT NULL
             and use_this.group_sp_count             = use_this.group_count)
         when MATCHED then
            update
               set target.size_profile_qty  = use_this.size_profile_qty,
                   target.total_profile_qty = use_this.total_qty;
      end if;

      --DEPT
      if L_dept_ind = 'Y' then

         merge /*+ INDEX(target, ALC_CALC_ALLITEMLOC_TEMP_I1) LEADING(use_this) */ into alc_calc_allitemloc_temp target
         using (select DISTINCT il.alloc_id,
                       il.tran_item,
                       il.to_loc,
                       il.source_item,
                       il.source_diff1_id,
                       il.source_diff2_id,
                       il.source_diff3_id,
                       il.source_diff4_id,
                       NVL(p.qty, 0) size_profile_qty,
                       --
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                          COUNT(p.loc) OVER (PARTITION BY il.alloc_id,
                                                          il.source_item,
                                                          il.to_loc)
                       else
                          1
                       end as group_sp_count,
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                             COUNT(*) OVER (PARTITION BY il.alloc_id,
                                                         il.source_item,
                                                         il.to_loc)
                       else
                          1
                       end as group_count,
                       --
                       case
                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                             SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                           il.source_item,
                                                           il.to_loc)
                       else
                          SUM(p.qty) OVER (PARTITION BY il.alloc_id,
                                                        il.source_item,
                                                        il.source_diff1_id,
                                                        il.source_diff2_id,
                                                        il.source_diff3_id,
                                                        il.source_diff4_id,
                                                        il.to_loc)
                       end as total_qty
                  from (select DISTINCT d.alloc_id,
                               d.source_item,
                               d.source_diff1_id,
                               d.source_diff2_id,
                               d.source_diff3_id,
                               d.source_diff4_id,
                               d.tran_item,
                               d.tran_diff1_id,
                               d.tran_diff2_id,
                               d.tran_diff3_id,
                               d.tran_diff4_id,
                               d.dept,
                               d.class,
                               d.subclass,
                               d.to_loc
                          from alc_calc_allitemloc_temp d
                         where d.alloc_id    = I_alloc_id
                           and L_expand_pack  = 'N'
                           and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                           and d.total_profile_qty is NULL
                        UNION ALL
                        select DISTINCT d.alloc_id,
                               d.source_item,
                               DECODE(d.source_diff1_id,
                                      NULL, NULL,
                                      im.diff_1),
                               DECODE(d.source_diff2_id,
                                      NULL, NULL,
                                      im.diff_2),
                               DECODE(d.source_diff3_id,
                                      NULL, NULL,
                                      im.diff_3),
                               DECODE(d.source_diff4_id,
                                      NULL, NULL,
                                      im.diff_4),
                               --
                               im.item,
                               im.diff_1,
                               im.diff_2,
                               im.diff_3,
                               im.diff_4,
                               --
                               d.dept,
                               d.class,
                               d.subclass,
                               d.to_loc
                          from alc_calc_allitemloc_temp d,
                               item_master im
                         where d.alloc_id     = I_alloc_id
                           and L_expand_pack  = 'Y'
                           and d.source_item  = im.item_parent
                           and (d.to_loc_type = 'S' or (d.to_loc_type = 'W' and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                           and d.total_profile_qty is NULL) il,
                       alc_size_profile p
                 where il.alloc_id        = I_alloc_id
                    and p.loc(+)          = TO_CHAR(il.to_loc)
                    --
                    and NVL(p.size1(+),0) = NVL(DECODE(il.source_diff1_id,
                                                       il.tran_diff1_id, NULL,
                                                       il.tran_diff1_id),
                                            0)
                    and NVL(p.size2(+),0) = NVL(DECODE(il.source_diff2_id,
                                                       il.tran_diff2_id, NULL,
                                                       il.tran_diff2_id),
                                            0)
                    and NVL(p.size3(+),0) = NVL(DECODE(il.source_diff3_id,
                                                       il.tran_diff3_id, NULL,
                                                       il.tran_diff3_id),
                                            0)
                    and NVL(p.size4(+),0) = NVL(DECODE(il.source_diff4_id,
                                                       il.tran_diff4_id, NULL,
                                                       il.tran_diff4_id),
                                            0)
                    and p.style(+)        is NULL
                    and p.dept(+)         = il.dept
                    and p.class(+)        is NULL
                    and p.subclass(+)     is NULL
                    --
                    and p.gid_profile_id(+) is NULL
         ) use_this
         on (    target.alloc_id                     = use_this.alloc_id
             and target.source_item                  = use_this.source_item
             and NVL(target.source_diff1_id, '-999') = NVL(use_this.source_diff1_id, '-999')
             and NVL(target.source_diff2_id, '-999') = NVL(use_this.source_diff2_id, '-999')
             and NVL(target.source_diff3_id, '-999') = NVL(use_this.source_diff3_id, '-999')
             and NVL(target.source_diff4_id, '-999') = NVL(use_this.source_diff4_id, '-999')
             and target.tran_item                    = use_this.tran_item
             and target.to_loc                       = use_this.to_loc
             --
             and use_this.size_profile_qty           is NOT NULL
             and use_this.group_sp_count             = use_this.group_count)
         when MATCHED then
            update
               set target.size_profile_qty  = use_this.size_profile_qty,
                   target.total_profile_qty = use_this.total_qty;

      end if;

   end if;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_CALC_ALLITEMLOC_TEMP GID SET - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_INFORMATION('END: ' || L_program );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_SIZE_PROFILE;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_SELLING_CURVE(O_error_message   IN OUT VARCHAR2,
                           I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                           I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.POP_SELLING_CURVE';

BEGIN

   if I_alc_rule_row.size_profile_type = 'SO' then

      merge into alc_calc_allitemloc_temp target
      using (select DISTINCT tmp.alloc_id,
                    tmp.source_item,
                    tmp.tran_item,
                    tmp.to_loc,
                    --
                    SUM(ilh.sales_issues) OVER (PARTITION BY tmp.alloc_id,
                                                             tmp.source_item,
                                                             tmp.tran_item,
                                                             tmp.to_loc) sales_issues,
                    --
                    case
                       when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                             (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                       SUM(ilh.sales_issues) OVER (PARTITION BY tmp.alloc_id,
                                                                tmp.source_item,
                                                                tmp.to_loc)
                    else
                       SUM(ilh.sales_issues) OVER (PARTITION BY tmp.alloc_id,
                                                                tmp.source_item,
                                                                tmp.source_diff1_id,
                                                                tmp.source_diff2_id,
                                                                tmp.source_diff3_id,
                                                                tmp.source_diff4_id,
                                                                tmp.to_loc)
                    end as sales_issues_total
                    --
               from alc_calc_allitemloc_temp tmp,
                    (select DISTINCT eow_date
                       from alc_calc_need_dates_temp
                      where alloc_id = I_alloc_id) d,
                    (select DECODE(I_alc_rule_row.regular_sales_ind,
                                   'Y','R',
                                   NULL) sales_type
                       from dual
                     union all
                     select DECODE(I_alc_rule_row.promo_sales_ind,
                                   'Y','P',
                                   NULL) sales_type
                       from dual
                     union all
                     select DECODE(I_alc_rule_row.clearance_sales_ind,
                                   'Y','C',
                                   NULL) sales_type
                       from dual
                     union all
                     select 'I' sales_type
                       from dual --WH Issues
                    ) ar,
                    item_loc_hist ilh
              where tmp.alloc_id   = I_alloc_id
                and tmp.tran_item  = ilh.item
                and tmp.to_loc     = ilh.loc
                and ilh.sales_type = ar.sales_type
                and ilh.eow_date   = d.eow_date
				and ((ilh.loc_type = 'W' and ilh.sales_type = 'I') or (ilh.loc_type = 'S' and ilh.sales_type in ('R','P','C')))
         ) use_this
      on (    target.alloc_id       = use_this.alloc_id
          and target.source_item    = use_this.source_item
          and target.tran_item      = use_this.tran_item
          and target.to_loc         = use_this.to_loc)
      when MATCHED then
         update
            set target.size_profile_qty  = use_this.sales_issues,
                target.total_profile_qty = use_this.sales_issues_total;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_SELLING_CURVE;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_EXCLUSIONS(O_error_message          IN OUT VARCHAR2,
                        I_alloc_id               IN     ALC_ALLOC.ALLOC_ID%TYPE,
                        I_enforce_wh_store_rel   IN     ALC_ALLOC.ENFORCE_WH_STORE_REL_IND%TYPE,
                        I_alc_rule_row           IN     ALC_RULE%ROWTYPE,
                        I_alc_calc_packs_tbl     IN     ALC_CALC_PACKS_TBL,
                        I_expand_pack            IN     VARCHAR2,
                        I_pack_range             IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.POP_EXCLUSIONS';
   L_start_time                 TIMESTAMP    := SYSTIMESTAMP;

   L_range_required             NUMBER(1) := 0;
   L_invalid_status             OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_tp_distribution_level      ALC_SYSTEM_OPTIONS.TP_DISTRIBUTION_LEVEL%TYPE := '1';
   L_tp_sizeprof_validn_enbl    ALC_SYSTEM_OPTIONS.TP_SIZEPROF_VALIDN_ENBL%TYPE := 'Y';
   L_process_staple             NUMBER(10)  := 0;
   L_what_if_ind                NUMBER(1)   := 0;

   cursor C_RANGED_REQ is
      select DECODE(invalid_status, 'NULL', 1, 0)
        from ( select REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd,'[^ ]+', 1, level) invalid_status
                 from alc_system_options
              connect by REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd, '[^ ]+', 1, level) is NOT NULL)
       where UPPER(invalid_status) = 'NULL'
         and L_what_if_ind = 0
      union all
      select DECODE(invalid_status, 'NULL', 1, 0)
        from ( select REGEXP_SUBSTR(tp_loc_excp_rsn_what_if,'[^ ]+', 1, level) invalid_status
                 from alc_system_options
              connect by REGEXP_SUBSTR(tp_loc_excp_rsn_what_if, '[^ ]+', 1, level) is NOT NULL)
        where UPPER(invalid_status) = 'NULL'
          and L_what_if_ind = 1;

   cursor C_INVALID_STATUS is
      select invalid_status
        from ( select REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd,'[^ ]+', 1, level) invalid_status
                 from alc_system_options
              connect by REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd, '[^ ]+', 1, level) is NOT NULL)
                where UPPER(invalid_status) != 'NULL'
                  and L_what_if_ind = 0
      union all
      select invalid_status
        from ( select REGEXP_SUBSTR(tp_loc_excp_rsn_what_if,'[^ ]+', 1, level) invalid_status
                 from alc_system_options
              connect by REGEXP_SUBSTR(tp_loc_excp_rsn_what_if, '[^ ]+', 1, level) is NOT NULL)
                where UPPER(invalid_status) != 'NULL'
                  and L_what_if_ind = 1;

   cursor C_ITEM_TYPE_PACK is
      select NVL(SUM(invalid_cnt), 0)
        from (select 1 invalid_cnt
                from alc_item_source a
               where a.alloc_id  = I_alloc_id
                 and a.item_type NOT IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                         ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                         ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK)
              union all
              select COUNT(DISTINCT im.item_parent) - 1 invalid_cnt
                from alc_item_source a,
                     packitem_breakout pb,
                     item_master im
               where a.alloc_id  = I_alloc_id
                 and a.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                     ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                     ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK)
                 and a.item_id   = pb.pack_no
                 and pb.item     = im.item
               group by pb.pack_no);

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   delete
     from gtt_num_num_str_str_date_date;

   delete
     from alc_item_loc_exclusion
    where alloc_id = I_alloc_id;

   select DECODE(MIN(source_type),
                 ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF, 1,
                 0)
     into L_what_if_ind
     from alc_item_source
    where alloc_id = I_alloc_id;

   select tp_distribution_level,
          tp_sizeprof_validn_enbl
     into L_tp_distribution_level,
          L_tp_sizeprof_validn_enbl
     from alc_system_options;

   open C_RANGED_REQ;
   fetch C_RANGED_REQ into L_range_required;
   close C_RANGED_REQ;

   open C_INVALID_STATUS;
   fetch C_INVALID_STATUS BULK COLLECT into L_invalid_status;
   close C_INVALID_STATUS;

   if LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION then

      if I_alc_rule_row.rule_mode = 3 then
         L_process_staple := 1;
      else
         open C_ITEM_TYPE_PACK;
         fetch C_ITEM_TYPE_PACK into L_process_staple;
         close C_ITEM_TYPE_PACK;
      end if;
   end if;

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2,
                                              varchar2_1)
                                       select /*+ USE_CONCAT USE_NL(a, s) USE_NL(d) INDEX(a, PK_ALC_ITEM_SOURCE) INDEX(s, ALC_CALC_SOURCE_TEMP_I1) INDEX(d, ALC_CALC_ALLITEMLOC_TEMP_I1)*/
                                              DISTINCT src.item_source_id,
                                              i.to_loc,
                                              i.reason
                                         from alc_calc_source_temp src,
                                              (--delete non-ranged
                                               select DISTINCT t.alloc_id,
                                                      t.tran_item,
                                                      t.to_loc,
                                                      t.to_loc_name,
                                                      ALC_CONSTANTS_SQL.EXCLUDE_ITEM_LOC_STATUS reason
                                                 from alc_calc_allitemloc_temp t
                                                where t.alloc_id        = I_alloc_id
                                                  and t.item_loc_status is NULL
                                                  and L_range_required  = 1
                                                  and I_enforce_wh_store_rel = 'Y'
                                               union all
                                              --delete non 'A' status on item_loc
                                               select DISTINCT t.alloc_id,
                                                      t.tran_item,
                                                      t.to_loc,
                                                      t.to_loc_name,
                                                      ALC_CONSTANTS_SQL.EXCLUDE_ITEM_LOC_STATUS reason
                                                 from alc_calc_allitemloc_temp t,
                                                      table(cast(L_invalid_status as OBJ_VARCHAR_ID_TABLE)) v
                                                where t.alloc_id         = I_alloc_id
                                                  and t.item_loc_status  is NOT NULL
                                                  and t.item_loc_status  = value(v)
                                               union all
                                              --delete wh cycles
                                               select DISTINCT t.alloc_id,
                                                      t.tran_item,
                                                      t.to_loc,
                                                      t.to_loc_name,
                                                      ALC_CONSTANTS_SQL.EXCLUDE_WH_CYCLE reason
                                                 from alc_calc_allitemloc_temp t,
                                                      alc_item_source src
                                                where t.alloc_id = I_alloc_id
                                                  and t.alloc_id = src.alloc_id
                                                  and t.to_loc   = src.wh_id
                                               union all
                                              --delete store open/close dates
                                               select t.alloc_id,
                                                      t.tran_item,
                                                      t.to_loc,
                                                      t.to_loc_name,
                                                      ALC_CONSTANTS_SQL.EXCLUDE_STORE_CLOSED reason
                                                 from alc_calc_allitemloc_temp t,
                                                      alc_calc_source_temp src,
                                                      store s
                                                where t.alloc_id    = I_alloc_id
                                                  and t.alloc_id    = src.alloc_id
                                                  and t.tran_item   = src.tran_item
                                                  and t.to_loc      = s.store
                                                  and t.to_loc_type = 'S'
                                                  and (   src.release_date < (s.store_open_date - s.start_order_days)
                                                       or src.release_date >= (s.store_close_date - NVL(s.stop_order_days,0)))
                                               union all
                                              --size profile
                                               select t.alloc_id,
                                                      t.tran_item,
                                                      t.to_loc,
                                                      t.to_loc_name,
                                                      ALC_CONSTANTS_SQL.EXCLUDE_SIZE_PROFILE reason_code
                                                 from alc_calc_allitemloc_temp t
                                                where t.alloc_id                = I_alloc_id
                                                  and t.total_profile_qty      is NULL
                                                  and (   (LP_alloc_type       != ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)
                                                       or (    LP_alloc_type    = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION
                                                           and L_process_staple = 0))
                                                  and L_tp_sizeprof_validn_enbl = 'Y'
                                                  and (    t.to_loc_type = 'S'
                                                       or (    t.to_loc_type = 'W'
                                                           and I_alc_rule_row.size_profile_wh_avail_pct_ind = 'N'))
                                              ) i
                                        where src.alloc_id  = i.alloc_id
                                          and src.tran_item = i.tran_item
                                       --
                                       union all
                                       --
                                       (select DISTINCT i.item_source_id,
                                               i.to_loc,
                                               ALC_CONSTANTS_SQL.EXCLUDE_MLD_PATH reason_code
                                          from (select a.item_source_id,
                                                       d.to_loc,
                                                       d.tran_item
                                                  from alc_calc_allitemloc_temp d,
                                                       alc_calc_source_temp s,
                                                       alc_item_source a
                                                 where d.alloc_id                 = I_alloc_id
                                                   and L_tp_distribution_level    = '0'
                                                   and I_enforce_wh_store_rel     = 'Y'
                                                   and d.alloc_id                 = s.alloc_id
                                                   and d.tran_item                = s.tran_item
                                                   and s.item_source_id           = a.item_source_id
                                                   and a.source_type             != ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF
                                                minus
                                                select a.item_source_id,
                                                       d.to_loc,
                                                       d.tran_item
                                                  from alc_calc_allitemloc_temp d,
                                                       alc_calc_source_temp s,
                                                       alc_item_source a
                                                 where d.alloc_id              = I_alloc_id
                                                   and d.alloc_id              = s.alloc_id
                                                   and d.tran_item             = s.tran_item
                                                   and s.item_source_id        = a.item_source_id
                                                   and a.wh_id                 = d.assign_default_wh) i
                                       )
                                       --
                                       union all
                                       --
                                       select i.item_source_id,
                                              i.to_loc,
                                              ALC_CONSTANTS_SQL.EXCLUDE_SUPPLY_CHAIN_PRIORITY reason_code
                                        from (select DISTINCT a.item_source_id,
                                                     a.wh_id,
                                                     d.to_loc
                                                from alc_calc_allitemloc_temp d,
                                                     alc_calc_source_temp s,
                                                     alc_item_source a
                                               where d.alloc_id        = I_alloc_id
                                                 and d.alloc_id        = s.alloc_id
                                                 and d.alloc_id        = a.alloc_id
                                                 and s.item_source_id  = a.item_source_id
                                                 and a.source_type    != ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF
                                               minus
                                              select DISTINCT a.item_source_id,
                                                     a.wh_id,
                                                     d.to_loc
                                                from alc_calc_wh_rule_priority d,
                                                     alc_calc_source_temp s,
                                                     alc_item_source a
                                               where d.alloc_id       = I_alloc_id
                                                 and d.alloc_id       = s.alloc_id
                                                 and d.alloc_id       = a.alloc_id
                                                 and s.item_source_id = a.item_source_id
                                                 and a.wh_id          = d.source_wh) i
                                       where I_enforce_wh_store_rel = 'N';

   LOGGER.LOG_INFORMATION(L_program||' Insert GTT_NUM_NUM_STR_STR_DATE_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --deal with packs
   --not all exclusion reasons depend on item, only the three included here and mld patch for WH destinations
   merge into gtt_num_num_str_str_date_date target
   using (with pack_locs as
             (select DISTINCT ps.item_source_id,
                     ps.pack_no item_id,
                     ps.location_id,
                     ps.location_type,
                     ps.release_date,
                     im.dept,
                     im.class,
                     im.subclass,
                     ps.wh_id,
                     ps.source_type,
                     iloc.loc,
                     iloc.status il_status,
                     case
                        when I_enforce_wh_store_rel = 'Y' then
                           NVL(DECODE(iloc.source_method,
                                      'W', iloc.source_wh),
                                      ps.default_wh)
                     else
                        NVL(DECODE(iloc.source_method,
                                   'W', iloc.source_wh),
                                   sc_inner.source_wh)
                     end as source_wh
                from (select ais.alloc_id,
                             ais.item_source_id,
                             ais.item_id pack_no,
                             al.location_id,
                             al.location_type,
                             ais.release_date,
                             ais.wh_id,
                             ais.source_type,
                             w.default_wh
                        from alc_item_source ais,
                             alc_loc_group alg,
                             alc_location al,
                             wh w
                       where ais.alloc_id     = I_alloc_id
                         and ais.alloc_id     = alg.alloc_id
                         and alg.loc_group_id = al.loc_group_id
                         --
                         and al.location_id   = w.wh
                         --
                         and ais.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                               ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                               ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                               ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                               ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK)
                      union all
                      select ais.alloc_id,
                             ais.item_source_id,
                             i.pack_no,
                             al.location_id,
                             al.location_type,
                             ais.release_date,
                             ais.wh_id,
                             ais.source_type,
                             w.default_wh
                        from table(cast(I_alc_calc_packs_tbl as ALC_CALC_PACKS_TBL)) i,
                             alc_item_source ais,
                             alc_loc_group alg,
                             alc_location al,
                             wh w
                       where ais.alloc_id             = I_alloc_id
                         --
                         and ais.item_id              = i.fashion_item_id
                         and NVL(ais.diff1_id,'-999') = NVL(i.diff1_id,'-999')
                         and NVL(ais.diff1_id,'-999') = NVL(i.diff1_id,'-999')
                         and ais.wh_id                = i.wh_id
                         and NVL(ais.order_no,'-999') = NVL(i.order_no,'-999')
                         and ais.source_type          = i.source_type
                         --
                         and ais.alloc_id             = alg.alloc_id
                         and alg.loc_group_id         = al.loc_group_id
                         --
                         and al.location_id           = w.wh) ps,
                     item_master im,
                     item_loc iloc,
                     packitem_breakout pb,
                     (select wrp.alloc_id,
                             wrp.tran_item,
                             wrp.to_loc,
                             wrp.source_wh
                        from alc_calc_wh_rule_priority wrp
                       where wrp.alloc_id      = I_alloc_id
                         and wrp.rule_priority = 2) sc_inner
               where ps.alloc_id    = I_alloc_id
                 and ps.pack_no     = im.item
                 and ps.location_id = iloc.loc (+)
                 and ps.pack_no     = pb.pack_no
                 and ps.alloc_id    = sc_inner.alloc_id(+)
                 and ps.pack_no     = sc_inner.tran_item(+)
                 and ps.location_id = sc_inner.to_loc(+)
                 and iloc.item (+)  = DECODE(I_pack_range,
                                             'P', pb.pack_no,
                                             pb.item)
             )
           ---
           --delete non-ranged pack
           select DISTINCT pl.item_source_id,
                  pl.location_id,
                  ALC_CONSTANTS_SQL.EXCLUDE_ITEM_LOC_STATUS reason
             from pack_locs pl
            where pl.il_status     is NULL
              and L_range_required = 1
           union all
           --delete non 'A' status on item_loc components or pack depending on pack ranging option
           select DISTINCT pl.item_source_id,
                  pl.location_id,
                  ALC_CONSTANTS_SQL.EXCLUDE_ITEM_LOC_STATUS reason
             from pack_locs pl,
                  table(cast(L_invalid_status as OBJ_VARCHAR_ID_TABLE)) v
            where pl.il_status = value(v)
           union all
           --delete MLD patch violations
           select DISTINCT pl.item_source_id,
                  pl.location_id,
                  ALC_CONSTANTS_SQL.EXCLUDE_MLD_PATH reason
             from pack_locs pl
            where pl.location_type           = 'W'
              and pl.source_type            != ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF
              and L_tp_distribution_level    = '0'
              and I_enforce_wh_store_rel     = 'Y'
              --
              and pl.wh_id                  != pl.source_wh) use_this
   on (    target.number_1   = use_this.item_source_id
       and target.number_2   = use_this.location_id
       and target.varchar2_1 = use_this.reason)
   when NOT MATCHED then
      insert (number_1,
              number_2,
              varchar2_1)
      values (use_this.item_source_id,
              use_this.location_id,
              reason);

   LOGGER.LOG_INFORMATION(L_program||' Merge GTT_NUM_NUM_STR_STR_DATE_DATE PACKS - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- if 1 item in the FPG fails, retrieve all items belonging to that same style so that the entire style is excluded.
   -- For Multi-Style FPG, all other styles that are part of the FPG should not be excluded if they pass the exclusion logic.
   -- If allocation is multi-sourced, only include the source WH of the style that did not pass the exclusion logic.

   if LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION then
      merge into gtt_num_num_str_str_date_date target
      using (select DISTINCT i.item_source_id,
                    i.to_loc location_id,
                    gtt.varchar2_1 reason
               from (select DISTINCT acst.item_source_id,
                            act.source_item,
                            act.to_loc
                       from alc_calc_allitemloc_temp act,
                            alc_calc_source_temp acst,
                            gtt_num_num_str_str_date_date gtt
                      where act.alloc_id    = I_alloc_id
                        and act.alloc_id    = acst.alloc_id
                        and act.source_item = acst.source_item
                        and act.tran_item   = acst.tran_item
                        and gtt.number_1    = acst.item_source_id
                        and gtt.number_2    = act.to_loc
                      group by acst.item_source_id,
                               act.source_item,
                               act.to_loc) i,
                    gtt_num_num_str_str_date_date gtt
              where gtt.number_2 = i.to_loc
            ) use_this
      on (    target.number_1   = use_this.item_source_id
          and target.number_2   = use_this.location_id
          and target.varchar2_1 = use_this.reason)
      when NOT MATCHED then
         insert (number_1,
                 number_2,
                 varchar2_1)
         values (use_this.item_source_id,
                 use_this.location_id,
                 reason);
   end if;

   LOGGER.LOG_INFORMATION(L_program||' Merge GTT_NUM_NUM_STR_STR_DATE_DATE FPG - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --move valid records to destination table
   insert into alc_calc_destination_temp (alloc_id,
                                          item_type,
                                          source_item,
                                          source_item_level,
                                          source_tran_level,
                                          source_pack_ind,
                                          source_diff1_id,
                                          source_diff2_id,
                                          source_diff3_id,
                                          source_diff4_id,
                                          tran_item,
                                          tran_item_level,
                                          tran_tran_level,
                                          tran_pack_ind,
                                          tran_diff1_id,
                                          tran_diff2_id,
                                          tran_diff3_id,
                                          tran_diff4_id,
                                          dept,
                                          class,
                                          subclass,
                                          to_loc,
                                          to_loc_type,
                                          to_loc_name,
                                          sister_store,
                                          assign_default_wh,
                                          clear_ind,
                                          item_loc_status,
                                          size_profile_qty,
                                          total_profile_qty,
                                          stock_on_hand,
                                          on_order,
                                          on_alloc,
                                          in_transit_qty,
                                          need_value,
                                          rloh_current_value,
                                          rloh_future_value)
                                   select acdt.alloc_id,
                                          acdt.item_type,
                                          acdt.source_item,
                                          acdt.source_item_level,
                                          acdt.source_tran_level,
                                          acdt.source_pack_ind,
                                          acdt.source_diff1_id,
                                          acdt.source_diff2_id,
                                          acdt.source_diff3_id,
                                          acdt.source_diff4_id,
                                          acdt.tran_item,
                                          acdt.tran_item_level,
                                          acdt.tran_tran_level,
                                          acdt.tran_pack_ind,
                                          acdt.tran_diff1_id,
                                          acdt.tran_diff2_id,
                                          acdt.tran_diff3_id,
                                          acdt.tran_diff4_id,
                                          acdt.dept,
                                          acdt.class,
                                          acdt.subclass,
                                          acdt.to_loc,
                                          acdt.to_loc_type,
                                          acdt.to_loc_name,
                                          acdt.sister_store,
                                          acdt.assign_default_wh,
                                          acdt.clear_ind,
                                          acdt.item_loc_status,
                                          acdt.size_profile_qty,
                                          --redo total when not expand pack Y, as the some of the inputs to the total may be removed for exclusion
                                          case when I_expand_pack = 'Y' then
                                             acdt.size_profile_qty
                                          when ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION) or
                                                (LP_alloc_type = ALC_CONSTANTS_SQL.STAPLE_ALLOCATION)) then
                                             SUM(acdt.size_profile_qty) OVER (PARTITION BY acdt.alloc_id,
                                                                                           acdt.source_item,
                                                                                           acdt.to_loc)
                                          else
                                             SUM(acdt.size_profile_qty) OVER (PARTITION BY acdt.alloc_id,
                                                                                           acdt.source_item,
                                                                                           acdt.source_diff1_id,
                                                                                           acdt.source_diff2_id,
                                                                                           acdt.source_diff3_id,
                                                                                           acdt.source_diff4_id,
                                                                                           acdt.to_loc)
                                          end as total_profile_qty,
                                          acdt.stock_on_hand,
                                          acdt.on_order,
                                          acdt.on_alloc,
                                          acdt.in_transit_qty,
                                          acdt.need_value,
                                          acdt.rloh_current_value,
                                          acdt.rloh_future_value
                                     from (select DISTINCT ail.alloc_id,
                                                  NULL item_type,
                                                  ail.source_item,
                                                  ail.source_item_level,
                                                  ail.source_tran_level,
                                                  ail.source_pack_ind,
                                                  ail.source_diff1_id,
                                                  ail.source_diff2_id,
                                                  ail.source_diff3_id,
                                                  ail.source_diff4_id,
                                                  ail.tran_item,
                                                  ail.tran_item_level,
                                                  ail.tran_tran_level,
                                                  ail.tran_pack_ind,
                                                  ail.tran_diff1_id,
                                                  ail.tran_diff2_id,
                                                  ail.tran_diff3_id,
                                                  ail.tran_diff4_id,
                                                  ail.dept,
                                                  ail.class,
                                                  ail.subclass,
                                                  ail.to_loc,
                                                  ail.to_loc_type,
                                                  ail.to_loc_name,
                                                  ail.sister_store,
                                                  ail.assign_default_wh,
                                                  ail.clear_ind,
                                                  ail.item_loc_status,
                                                  ail.size_profile_qty,
                                                  --
                                                  ail.stock_on_hand,
                                                  ail.on_order,
                                                  ail.on_alloc,
                                                  ail.in_transit_qty,
                                                  ail.need_value,
                                                  ail.rloh_current_value,
                                                  ail.rloh_future_value
                                             from alc_calc_allitemloc_temp ail,
                                                  (select val.alloc_id,
                                                          val.tran_item,
                                                          val.to_loc
                                                     from (select /*+ INDEX(d, ALC_CALC_ALLITEMLOC_TEMP_I4) LEADING(s) */
                                                                  s.alloc_id,
                                                                  s.tran_item,
                                                                  d.to_loc,
                                                                  COUNT(DISTINCT s.item_source_id) total_src_cnt
                                                             from alc_calc_source_temp s,
                                                                  alc_calc_allitemloc_temp d
                                                            where s.alloc_id  = I_alloc_id
                                                              and s.alloc_id  = d.alloc_id
                                                              and s.tran_item = d.tran_item
                                                           group by s.alloc_id,
                                                                    s.tran_item,
                                                                    d.to_loc) val,
                                                          (select /*+ INDEX(d, ALC_CALC_ALLITEMLOC_TEMP_I4) LEADING(gtt) */
                                                                  s.alloc_id,
                                                                  s.tran_item,
                                                                  d.to_loc,
                                                                  COUNT(DISTINCT s.item_source_id) invalid_src_cnt
                                                             from alc_calc_source_temp s,
                                                                  alc_calc_allitemloc_temp d,
                                                                  gtt_num_num_str_str_date_date gtt
                                                            where s.alloc_id       = I_alloc_id
                                                              and s.alloc_id       = d.alloc_id
                                                              and s.tran_item      = d.tran_item
                                                              and s.item_source_id = gtt.number_1
                                                              and d.to_loc         = gtt.number_2
                                                           group by s.alloc_id,
                                                                    s.tran_item,
                                                                    d.to_loc) inv
                                                    where inv.tran_item(+)     = val.tran_item
                                                      and inv.to_loc(+)        = val.to_loc
                                                      and val.total_src_cnt    > NVL(inv.invalid_src_cnt,0)) i
                                            where ail.alloc_id  = I_alloc_id
                                              and ail.alloc_id  = i.alloc_id
                                              and ail.tran_item = i.tran_item
                                              and ail.to_loc    = i.to_loc) acdt;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_DESTINATION_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --insert exclusions
   insert into alc_item_loc_exclusion (item_loc_exclusion_id,
                                       alloc_id,
                                       item_id,
                                       item_desc,
                                       location_id,
                                       location_desc,
                                       reason_code,
                                       diff1_id,
                                       source_location_id,
                                       order_no,
                                       source_type)
                                select alc_item_loc_exclusion_seq.NEXTVAL,
                                       I_alloc_id,
                                       ais.item_id,
                                       im.item_desc,
                                       gtt.number_2,   --to_loc
                                       l.loc_name,
                                       gtt.varchar2_1, --reason
                                       ais.diff1_id,
                                       ais.wh_id,
                                       ais.order_no,
                                       ais.source_type
                                  from (select DISTINCT g.number_1,
                                               g.number_2,
                                               g.varchar2_1
                                          from gtt_num_num_str_str_date_date g) gtt,
                                       alc_item_source ais,
                                       item_master im,
                                       (select store loc,
                                               store_name loc_name
                                          from store
                                         union all
                                        select wh loc,
                                               wh_name loc_name
                                          from wh) l
                                 where gtt.number_1 = ais.item_source_id
                                   and ais.item_id  = im.item
                                   and gtt.number_2 = l.loc;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_ITEM_LOC_EXCLUSION - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- delete records in ALC_CALC_WH_RULE if the source_wh is NULL
   if I_enforce_wh_store_rel = 'N' then
      delete
        from alc_calc_wh_rule_priority wrp
       where wrp.alloc_id  = I_alloc_id
         and wrp.source_wh is NULL;

      LOGGER.LOG_INFORMATION(L_program||' Delete ALC_CALC_WH_RULE_PRIORITY - NULL SOURCE_WH- SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_EXCLUSIONS;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_QTY_LIMITS(O_error_message   IN OUT VARCHAR2,
                        I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                        I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_QTY_LIMITS';

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program );

   --hierarchy level qty limits are not currently in scope
   if LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION then

      insert into alc_calc_qty_limits_temp (alloc_id,
                                            store,
                                            item_source_id,
                                            min,
                                            max,
                                            treshold,
                                            trend,
                                            wos,
                                            min_need,
                                            min_pack,
                                            max_pack)
                                     select DISTINCT s.alloc_id,
                                            al.location_id,
                                            s.item_source_id,
                                            ql.min,
                                            ql.max,
                                            ql.treshold,
                                            ql.trend,
                                            ql.wos,
                                            ql.min_need,
                                            ql.min_pack,
                                            ql.max_pack
                                       from alc_calc_source_temp s,
                                            alc_loc_group alg,
                                            alc_location al,
                                            alc_quantity_limits ql
                                      where s.alloc_id      = I_alloc_id
                                        and alg.alloc_id    = s.alloc_id
                                        and s.alloc_id      = ql.alloc_id
                                        and s.source_item   = ql.item_id
                                        and al.loc_group_id = alg.loc_group_id
                                        and ql.location_id  = al.location_id;

      LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_QTY_LIMITS_TEMP - FPG - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   else

      insert into alc_calc_qty_limits_temp (alloc_id,
                                            store,
                                            item_source_id,
                                            min,
                                            max,
                                            treshold,
                                            trend,
                                            wos,
                                            min_need,
                                            min_pack,
                                            max_pack)
                                     select s.alloc_id,
                                            al.location_id,
                                            s.item_source_id,
                                            ql.min,
                                            ql.max,
                                            ql.treshold,
                                            ql.trend,
                                            ql.wos,
                                            ql.min_need,
                                            ql.min_pack,
                                            ql.max_pack
                                       from alc_item_source s,
                                            alc_loc_group alg,
                                            alc_location al,
                                            alc_quantity_limits ql
                                      where s.alloc_id      = I_alloc_id
                                        and s.item_type    != ALC_CONSTANTS_SQL.FASHIONITEM
                                        and alg.alloc_id    = s.alloc_id
                                        and al.loc_group_id = alg.loc_group_id
                                        --
                                        and s.alloc_id      = ql.alloc_id
                                        and ql.location_id  = al.location_id
                                        and s.item_id       = ql.item_id
                                      union all
                                     select s.alloc_id,
                                            al.location_id,
                                            s.item_source_id,
                                            ql.min,
                                            ql.max,
                                            ql.treshold,
                                            ql.trend,
                                            ql.wos,
                                            ql.min_need,
                                            ql.min_pack,
                                            ql.max_pack
                                       from alc_item_source s,
                                            alc_loc_group alg,
                                            alc_location al,
                                            alc_quantity_limits ql
                                      where s.alloc_id      = I_alloc_id
                                        and s.item_type     = ALC_CONSTANTS_SQL.FASHIONITEM
                                        and alg.alloc_id    = s.alloc_id
                                        and al.loc_group_id = alg.loc_group_id
                                        --
                                        and s.alloc_id      = ql.alloc_id
                                        and ql.location_id  = al.location_id
                                        and (   s.item_id||' '||s.diff1_id = ql.item_id
                                             or s.item_id||' '||s.diff2_id = ql.item_id);

      LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_QTY_LIMITS_TEMP - FA - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   end if;

   LOGGER.LOG_INFORMATION('END: ' || L_program );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_QTY_LIMITS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED(O_error_message   IN OUT VARCHAR2,
                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED';

BEGIN

   if SEED_NEED_GTT(O_error_message,
                    I_alloc_id,
                    I_alc_rule_row) = FALSE then
      return FALSE;
   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_HISTORY or
      I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_HISTORY_AND_PLAN or
      I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_PLAN_REPROJECT then

      if SETUP_NEED_SALES_HIST(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_FORECAST then

      if SETUP_NEED_FCST(O_error_message,
                         I_alloc_id,
                         I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_PLAN_REPROJECT then

      if SETUP_NEED_REPLAN(O_error_message,
                           I_alloc_id,
                           I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_PLAN or
      I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_HISTORY_AND_PLAN then

      if SETUP_NEED_PLAN(O_error_message,
                         I_alloc_id,
                         I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
   end if;


   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_RECIEPT_AND_PLAN then

      if SETUP_NEED_RECEIPT_PLAN(O_error_message,
                                 I_alloc_id,
                                 I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
   end if;


   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_CORPORATE_RULES then

      if SETUP_NEED_CORP_RULES(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RULE_HIST_EOW_DATES(O_error_message    IN OUT VARCHAR2,
                                 I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                 I_alc_rule_row     IN     ALC_RULE%ROWTYPE,
                                 I_weight_pct       IN     ALC_RULE_MANY_TO_ONE.WEIGHT_PCT%TYPE,
                                 I_many_to_one_id   IN     ALC_RULE_MANY_TO_ONE.MANY_TO_ONE_ID%TYPE DEFAULT -1)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_RULE_HIST_EOW_DATES';

BEGIN

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_CORPORATE_RULES then

      --This is just a dummy row so that SEED_NEED_GTT call will work for corp rules,
      --corp rules don't use eow_dates or weight.

      insert into alc_calc_need_dates_temp (alloc_id,
                                            rule_many_to_one_id,
                                            eow_date, weight)
                                     select I_alloc_id,
                                            I_many_to_one_id,
                                            NULL,
                                            1
                                       from dual;

   else

      insert into alc_calc_need_dates_temp (alloc_id,
                                            rule_many_to_one_id,
                                            eow_date,
                                            weight)
                                     select r.alloc_id,
                                            I_many_to_one_id,
                                            TRUNC(r.eow_date),
                                            NVL(I_weight_pct/100,r.weight)
                                       from alc_rule_date r
                                      where r.alloc_id = I_alloc_id;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RULE_HIST_EOW_DATES;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_AH_RULE_HIST_EOW_DATES(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_AH_RULE_HIST_EOW_DATES';

   L_eow_date      DATE := NULL;

   cursor C_ALT_HIER is
      select ah.many_to_one_id,
             ah.start_date_period_one,
             ah.end_date_period_one,
             ah.start_date_period_two,
             ah.end_date_period_two,
             ah.wk_from_today_this_yr,
             ah.wk_from_today_last_yr,
             ah.weight_pct
        from alc_rule_many_to_one ah
       where ah.alloc_id = I_alloc_id;

BEGIN

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_CORPORATE_RULES then

      --This is just a dummy row so that SEED_NEED_GTT call will work for corp rules,
      --corp rules don't use eow_dates or weight.
      insert into alc_calc_need_dates_temp (alloc_id,
                                            rule_many_to_one_id,
                                            eow_date,
                                            weight)
                                     select r.alloc_id,
                                            r.many_to_one_id,
                                            NULL,
                                            1
                                       from alc_rule_many_to_one r
                                      where r.alloc_id = I_alloc_id;

   else

      for rec in C_ALT_HIER loop

         --if get date ranges
         if rec.start_date_period_one is NOT NULL then

            if GET_DATE_RANGE_HIST_EOW_DATES(O_error_message,
                                             I_alloc_id,
                                             rec.many_to_one_id,
                                             rec.start_date_period_one,
                                             rec.end_date_period_one,
                                             rec.weight_pct) = FALSE then
               return FALSE;
            end if;
            --
            if GET_DATE_RANGE_HIST_EOW_DATES(O_error_message,
                                             I_alloc_id,
                                             rec.many_to_one_id,
                                             rec.start_date_period_two,
                                             rec.end_date_period_two,
                                             rec.weight_pct) = FALSE then
               return FALSE;
            end if;

         --if get ty, ly number of weeks
         elsif rec.wk_from_today_this_yr is NOT NULL then

            if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_HISTORY or
               I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_FORECAST or
               I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_PLAN or
               I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_RECIEPT_AND_PLAN then

               if GET_ALLOC_EOW_DATE(O_error_message,
                                     L_eow_date,
                                     SYSDATE) = FALSE then
                  return FALSE;
               end if;

               if GET_DATE_RANGE_TY_LY(O_error_message,
                                       I_alloc_id,
                                       I_alc_rule_row,
                                       rec.many_to_one_id,
                                       L_eow_date,
                                       rec.wk_from_today_this_yr,
                                       rec.weight_pct) = FALSE then
                  return FALSE;
               end if;

               --get last year's eow_date
               if GET_ALLOC_EOW_DATE(O_error_message,
                                     L_eow_date,
                                     SYSDATE-365) = FALSE then
                  return FALSE;
               end if;

               if GET_DATE_RANGE_TY_LY(O_error_message,
                                       I_alloc_id,
                                       I_alc_rule_row,
                                       rec.many_to_one_id,
                                       L_eow_date,
                                       rec.wk_from_today_last_yr,
                                       rec.weight_pct) = FALSE then
                  return FALSE;
               end if;

            else --ALC_CONSTANTS_SQL.RULE_TYPE_HISTORY_AND_PLAN or
                 --ALC_CONSTANTS_SQL.RULE_TYPE_PLAN_REPROJECT

               if GET_ALLOC_EOW_DATE(O_error_message,
                                     L_eow_date,
                                     SYSDATE-365) = FALSE then
                  return FALSE;
               end if;

               if GET_DATE_RANGE_FWD_BCK(O_error_message,
                                         I_alloc_id,
                                         I_alc_rule_row,
                                         rec.many_to_one_id,
                                         L_eow_date,
                                         rec.wk_from_today_this_yr,
                                         rec.wk_from_today_last_yr,
                                         rec.weight_pct) = FALSE then
                  return FALSE;
               end if;

            end if;

         --if get nothing, get eow dates from the rule level
         else

            if GET_RULE_HIST_EOW_DATES(O_error_message,
                                       I_alloc_id,
                                       I_alc_rule_row,
                                       rec.weight_pct,
                                       rec.many_to_one_id) = FALSE then
               return FALSE;
            end if;

         end if;

      end loop;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_AH_RULE_HIST_EOW_DATES;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_DATE_RANGE_HIST_EOW_DATES(O_error_message    IN OUT VARCHAR2,
                                       I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_many_to_one_id   IN     ALC_RULE_MANY_TO_ONE.MANY_TO_ONE_ID%TYPE,
                                       I_start_date       IN     DATE,
                                       I_end_date         IN     DATE,
                                       I_weight_pct       IN     ALC_RULE_MANY_TO_ONE.WEIGHT_PCT%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_DATE_RANGE_HIST_EOW_DATES';

   L_eow_date  DATE;

BEGIN

   if I_start_date is NULL or I_end_date is NULL then
      return TRUE;
   end if;

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_eow_date,
                         I_start_date) = FALSE then
      return FALSE;
   end if;

   insert into alc_calc_need_dates_temp (alloc_id,
                                         rule_many_to_one_id,
                                         eow_date,
                                         weight)
                                  select I_alloc_id,
                                         I_many_to_one_id,
                                         L_eow_date,
                                         I_weight_pct/100
                                    from dual;

   while L_eow_date <= I_end_date + 1 loop

      L_eow_date := L_eow_date + 7;

      if L_eow_date <= I_end_date + 1 then

         insert into alc_calc_need_dates_temp (alloc_id,
                                               rule_many_to_one_id,
                                               eow_date,
                                               weight)
                                        select I_alloc_id,
                                               I_many_to_one_id,
                                               L_eow_date,
                                               I_weight_pct/100
                                          from dual;

      end if;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_DATE_RANGE_HIST_EOW_DATES;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_DATE_RANGE_TY_LY(O_error_message       IN OUT VARCHAR2,
                              I_alloc_id            IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row        IN     ALC_RULE%ROWTYPE,
                              I_many_to_one_id      IN     ALC_RULE_MANY_TO_ONE.MANY_TO_ONE_ID%TYPE,
                              I_starting_eow_date   IN     DATE,
                              I_num_weeks           IN     ALC_RULE_MANY_TO_ONE.WK_FROM_TODAY_THIS_YR%TYPE,
                              I_weight_pct          IN     ALC_RULE_MANY_TO_ONE.WEIGHT_PCT%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_DATE_RANGE_TY_LY';

   L_cnt           NUMBER := 0;
   L_eow_date      DATE;

BEGIN

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_HISTORY then

      L_eow_date := I_starting_eow_date - 7;
      while L_cnt < I_num_weeks loop

         insert into alc_calc_need_dates_temp (alloc_id,
                                               rule_many_to_one_id,
                                               eow_date,
                                               weight)
                                       values (I_alloc_id,
                                               I_many_to_one_id,
                                               L_eow_date,
                                               I_weight_pct/100);

         L_eow_date := L_eow_date - 7;
         L_cnt := L_cnt + 1;

      end loop;
      L_cnt := 0;

   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_PLAN or
      I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_RECIEPT_AND_PLAN or
      I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_FORECAST then

      L_eow_date := I_starting_eow_date;
      while L_cnt < I_num_weeks loop

         insert into alc_calc_need_dates_temp (alloc_id,
                                               rule_many_to_one_id,
                                               eow_date,
                                               weight)
                                       values (I_alloc_id,
                                               I_many_to_one_id,
                                               L_eow_date,
                                               I_weight_pct/100);

         L_eow_date := L_eow_date + 7;
         L_cnt := L_cnt + 1;

      end loop;
      L_cnt := 0;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_DATE_RANGE_TY_LY;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_DATE_RANGE_FWD_BCK(O_error_message       IN OUT VARCHAR2,
                                I_alloc_id            IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                I_alc_rule_row        IN     ALC_RULE%ROWTYPE,
                                I_many_to_one_id      IN     ALC_RULE_MANY_TO_ONE.MANY_TO_ONE_ID%TYPE,
                                I_starting_eow_date   IN     DATE,
                                I_num_forward         IN     ALC_RULE_MANY_TO_ONE.WK_FROM_TODAY_THIS_YR%TYPE,
                                I_num_back            IN     ALC_RULE_MANY_TO_ONE.WK_FROM_TODAY_THIS_YR%TYPE,
                                I_weight_pct          IN     ALC_RULE_MANY_TO_ONE.WEIGHT_PCT%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_DATE_RANGE_FWD_BCK';

   L_cnt           NUMBER := 0;
   L_eow_date      DATE;

BEGIN

      L_eow_date := I_starting_eow_date - 7;
      while L_cnt < I_num_back loop

         insert into alc_calc_need_dates_temp (alloc_id,
                                               rule_many_to_one_id,
                                               eow_date,
                                               weight)
                                       values (I_alloc_id,
                                               I_many_to_one_id,
                                               L_eow_date,
                                               I_weight_pct/100);

         L_eow_date := L_eow_date - 7;
         L_cnt := L_cnt + 1;

      end loop;
      L_cnt := 0;

      ---

      L_eow_date := I_starting_eow_date;
      while L_cnt < I_num_forward loop

         insert into alc_calc_need_dates_temp (alloc_id,
                                               rule_many_to_one_id,
                                               eow_date,
                                               weight)
                                       values (I_alloc_id,
                                               I_many_to_one_id,
                                               L_eow_date,
                                               I_weight_pct/100);

         L_eow_date := L_eow_date + 7;
         L_cnt := L_cnt + 1;

      end loop;
      L_cnt := 0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_DATE_RANGE_FWD_BCK;
-------------------------------------------------------------------------------------------------------------
FUNCTION SEED_NEED_GTT(O_error_message   IN OUT VARCHAR2,
                       I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                       I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SEED_NEED_GTT';

BEGIN

   if I_alc_rule_row.rule_level in(ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT,
                                   ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS,
                                   ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS) then

      insert into alc_calc_need_temp (alloc_id,
                                      rule_many_to_one_id,
                                      rule_level,
                                      dept,
                                      class,
                                      subclass,
                                      source_item,
                                      source_item_level,
                                      source_tran_level,
                                      source_pack_ind,
                                      source_diff1_id,
                                      source_diff2_id,
                                      source_diff3_id,
                                      source_diff4_id,
                                      tran_item,
                                      tran_item_level,
                                      tran_tran_level,
                                      tran_pack_ind,
                                      tran_diff1_id,
                                      tran_diff2_id,
                                      tran_diff3_id,
                                      tran_diff4_id,
                                      to_loc,
                                      sister_store,
                                      size_profile_qty,
                                      total_profile_qty,
                                      eow_date,
                                      weight,
                                      sales_hist_need,
                                      forecast_need,
                                      plan_need,
                                      plan_reproject_need,
                                      receipt_plan_need,
                                      corp_rule_need)
                               select DISTINCT cd.alloc_id,
                                      -1,
                                      I_alc_rule_row.rule_level,
                                      cd.dept,
                                      cd.class,
                                      cd.subclass,
                                      NULL,        --source_item,
                                      NULL,        --source_item_level,
                                      NULL,        --source_tran_level,
                                      NULL,        --source_pack_ind,
                                      NULL,        --source_diff1_id,
                                      NULL,        --source_diff2_id,
                                      NULL,        --source_diff3_id,
                                      NULL,        --source_diff4_id,
                                      NULL,        --tran_item,
                                      NULL,        --tran_item_level,
                                      NULL,        --tran_tran_level,
                                      NULL,        --tran_pack_ind,
                                      NULL,        --tran_diff1_id,
                                      NULL,        --tran_diff2_id,
                                      NULL,        --tran_diff3_id,
                                      NULL,        --tran_diff4_id,
                                      cd.to_loc,
                                      cd.sister_store,
                                      NULL,        --size_profile_qty,
                                      NULL,        --total_profile_qty,
                                      d.eow_date,
                                      d.weight,
                                      -1,
                                      -1,
                                      -1,
                                      -1,
                                      -1,
                                      -1
                                 from (select DISTINCT tmp.alloc_id,
                                              tmp.dept,
                                              DECODE(I_alc_rule_row.rule_level,
                                                     ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS,    tmp.class,
                                                     ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS, tmp.class,
                                                     NULL) class,
                                              DECODE(I_alc_rule_row.rule_level,
                                                     ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS, tmp.subclass,
                                                     NULL) subclass,
                                              tmp.to_loc,
                                              tmp.sister_store
                                         from alc_calc_destination_temp tmp
                                        where tmp.alloc_id = I_alloc_id) cd,
                                      alc_calc_need_dates_temp d
                                where d.alloc_id = I_alloc_id;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL then

      insert into alc_calc_need_temp (alloc_id,
                                      rule_many_to_one_id,
                                      rule_level,
                                      dept,
                                      class,
                                      subclass,
                                      source_item,
                                      source_item_level,
                                      source_tran_level,
                                      source_pack_ind,
                                      source_diff1_id,
                                      source_diff2_id,
                                      source_diff3_id,
                                      source_diff4_id,
                                      tran_item,
                                      tran_item_level,
                                      tran_tran_level,
                                      tran_pack_ind,
                                      tran_diff1_id,
                                      tran_diff2_id,
                                      tran_diff3_id,
                                      tran_diff4_id,
                                      to_loc,
                                      sister_store,
                                      size_profile_qty,
                                      total_profile_qty,
                                      eow_date,
                                      weight,
                                      sales_hist_need,
                                      forecast_need,
                                      plan_need,
                                      plan_reproject_need,
                                      receipt_plan_need,
                                      corp_rule_need)
                               select /*+ INDEX(d, ALC_CALC_NEED_DATES_TEMP_I1) */
                                      DISTINCT cd.alloc_id,
                                      -1,
                                      I_alc_rule_row.rule_level,
                                      cd.dept,
                                      cd.class,
                                      cd.subclass,
                                      cd.source_item,
                                      cd.source_item_level,
                                      cd.source_tran_level,
                                      cd.source_pack_ind,
                                      cd.source_diff1_id,
                                      cd.source_diff2_id,
                                      cd.source_diff3_id,
                                      cd.source_diff4_id,
                                      cd.tran_item,
                                      cd.tran_item_level,
                                      cd.tran_tran_level,
                                      cd.tran_pack_ind,
                                      cd.tran_diff1_id,
                                      cd.tran_diff2_id,
                                      cd.tran_diff3_id,
                                      cd.tran_diff4_id,
                                      cd.to_loc,
                                      cd.sister_store,
                                      NULL,        --size_profile_qty,
                                      NULL,        --total_profile_qty,
                                      d.eow_date,
                                      d.weight,
                                      -1,-1,-1,-1,-1,-1
                                 from (--style info in alc_calc_destination_temp
                                       select DISTINCT tmp.alloc_id,
                                              tmp.dept,
                                              tmp.class,
                                              tmp.subclass,
                                              --
                                              tmp.source_item,
                                              tmp.source_item_level,
                                              tmp.source_tran_level,
                                              tmp.source_pack_ind,
                                              NULL as source_diff1_id,
                                              NULL as source_diff2_id,
                                              NULL as source_diff3_id,
                                              NULL as source_diff4_id,
                                              im.item tran_item,
                                              im.item_level tran_item_level,
                                              im.tran_level tran_tran_level,
                                              im.pack_ind tran_pack_ind,
                                              NULL tran_diff1_id,
                                              NULL tran_diff2_id,
                                              NULL tran_diff3_id,
                                              NULL tran_diff4_id,
                                              --
                                              tmp.to_loc,
                                              tmp.sister_store
                                         from alc_calc_destination_temp tmp,
                                              item_master im
                                        where tmp.alloc_id          = I_alloc_id
                                          and tmp.source_tran_level = tmp.source_item_level + 1 --parent
                                          and tmp.source_item       = im.item_parent
                                          and im.item_level         = im.tran_level
                                          and im.status             = 'A'
                                     union all
                                       --only tran info in alc_calc_destination_temp
                                       select DISTINCT tmp.alloc_id,
                                              tmp.dept,
                                              tmp.class,
                                              tmp.subclass,
                                              --
                                              im2.item_parent,
                                              im2.item_level - 1,
                                              im2.tran_level,
                                              im2.pack_ind,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              im2.item,
                                              im2.item_level,
                                              im2.tran_level,
                                              im2.pack_ind,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              --
                                              tmp.to_loc,
                                              tmp.sister_store
                                         from alc_calc_destination_temp tmp,
                                              item_master im2  --get all children of that parent
                                        where tmp.alloc_id          = I_alloc_id
                                          and tmp.source_tran_level = tmp.source_item_level
                                          --
                                          and im2.status            = 'A'
                                          and EXISTS (select 'x'
                                                        from item_master im
                                                       where tmp.tran_item   = im.item
                                                         and im2.item_parent = im.item_parent
                                                         and im.item_parent  IS NOT NULL
                                                         and rownum          = 1)
                                      ) cd,
                                      alc_calc_need_dates_temp d
                                where d.alloc_id = I_alloc_id;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE then

      insert into alc_calc_need_temp (alloc_id,
                                      rule_many_to_one_id,
                                      rule_level,
                                      dept,
                                      class,
                                      subclass,
                                      source_item,
                                      source_item_level,
                                      source_tran_level,
                                      source_pack_ind,
                                      source_diff1_id,
                                      source_diff2_id,
                                      source_diff3_id,
                                      source_diff4_id,
                                      tran_item,
                                      tran_item_level,
                                      tran_tran_level,
                                      tran_pack_ind,
                                      tran_diff1_id,
                                      tran_diff2_id,
                                      tran_diff3_id,
                                      tran_diff4_id,
                                      to_loc,
                                      sister_store,
                                      size_profile_qty,
                                      total_profile_qty,
                                      eow_date,
                                      weight,
                                      sales_hist_need,
                                      forecast_need,
                                      plan_need,
                                      plan_reproject_need,
                                      receipt_plan_need,
                                      corp_rule_need)
                               select DISTINCT cd.alloc_id,
                                      -1,
                                      I_alc_rule_row.rule_level,
                                      cd.dept,
                                      cd.class,
                                      cd.subclass,
                                      cd.source_item,
                                      cd.source_item_level,
                                      cd.source_tran_level,
                                      cd.source_pack_ind,
                                      cd.source_diff1_id,
                                      cd.source_diff2_id,
                                      cd.source_diff3_id,
                                      cd.source_diff4_id,
                                      cd.tran_item,
                                      cd.tran_item_level,
                                      cd.tran_tran_level,
                                      cd.tran_pack_ind,
                                      cd.tran_diff1_id,
                                      cd.tran_diff2_id,
                                      cd.tran_diff3_id,
                                      cd.tran_diff4_id,
                                      cd.to_loc,
                                      cd.sister_store,
                                      NULL,        --size_profile_qty,
                                      NULL,        --total_profile_qty,
                                      d.eow_date,
                                      d.weight,
                                      -1,-1,-1,-1,-1,-1
                                 from (--style/color info in alc_calc_destination_temp
                                       select tmp.alloc_id,
                                              tmp.dept,
                                              tmp.class,
                                              tmp.subclass,
                                              tmp.source_item,
                                              tmp.source_item_level,
                                              tmp.source_tran_level,
                                              tmp.source_pack_ind,
                                              tmp.source_diff1_id,
                                              tmp.source_diff2_id,
                                              tmp.source_diff3_id,
                                              tmp.source_diff4_id,
                                              --
                                              im.item       tran_item,
                                              im.item_level tran_item_level,
                                              im.tran_level tran_tran_level,
                                              im.pack_ind   tran_pack_ind,
                                              im.diff_1     tran_diff1_id,
                                              im.diff_2     tran_diff2_id,
                                              im.diff_3     tran_diff3_id,
                                              im.diff_4     tran_diff4_id,
                                              --
                                              tmp.to_loc,
                                              tmp.sister_store
                                         from alc_calc_destination_temp tmp,
                                              item_master im
                                        where tmp.alloc_id          = I_alloc_id
                                          and tmp.source_item_level = tmp.source_tran_level - 1
                                          and tmp.source_item       = im.item_parent
                                          and im.item_level         = im.tran_level
                                          and im.status             = 'A'
                                          and NVL(tmp.source_diff1_id, NVL(im.diff_1,'-999'))   = NVL(im.diff_1,'-999')
                                          and NVL(tmp.source_diff2_id, NVL(im.diff_2,'-999'))   = NVL(im.diff_2,'-999')
                                          and NVL(tmp.source_diff3_id, NVL(im.diff_3,'-999'))   = NVL(im.diff_3,'-999')
                                          and NVL(tmp.source_diff4_id, NVL(im.diff_4,'-999'))   = NVL(im.diff_4,'-999')
                                     union all
                                       --only tran info in alc_calc_destination_temp
                                       select DISTINCT tmp.alloc_id,
                                              tmp.dept,
                                              tmp.class,
                                              tmp.subclass,
                                              --
                                              im2.item_parent,
                                              im2.item_level-1,
                                              im2.tran_level,
                                              im2.pack_ind,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              im2.item,
                                              im2.item_level,
                                              im2.tran_level,
                                              im2.pack_ind,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              --
                                              tmp.to_loc,
                                              tmp.sister_store
                                         from alc_calc_destination_temp tmp,
                                              item_master im,  --get parents of items on calc_destination
                                              item_master im2  --get all children of that parent
                                        where tmp.alloc_id          = I_alloc_id
                                          and tmp.source_tran_level = tmp.source_item_level
                                          and tmp.tran_item         = im.item
                                          and im.item_parent        is NOT NULL
                                          --
                                          and im2.item_parent       = im.item_parent
                                          and im2.status            = 'A'
                                      ) cd,
                                      alc_calc_need_dates_temp d
                                where d.alloc_id = I_alloc_id;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU then

      insert into alc_calc_need_temp (alloc_id,
                                      rule_many_to_one_id,
                                      rule_level,
                                      dept,
                                      class,
                                      subclass,
                                      source_item,
                                      source_item_level,
                                      source_tran_level,
                                      source_pack_ind,
                                      source_diff1_id,
                                      source_diff2_id,
                                      source_diff3_id,
                                      source_diff4_id,
                                      tran_item,
                                      tran_item_level,
                                      tran_tran_level,
                                      tran_pack_ind,
                                      tran_diff1_id,
                                      tran_diff2_id,
                                      tran_diff3_id,
                                      tran_diff4_id,
                                      to_loc,
                                      sister_store,
                                      size_profile_qty,
                                      total_profile_qty,
                                      eow_date,
                                      weight,
                                      sales_hist_need,
                                      forecast_need,
                                      replan_need,
                                      plan_need,
                                      plan_reproject_need,
                                      receipt_plan_need,
                                      corp_rule_need)
                               select DISTINCT cd.alloc_id,
                                      -1,
                                      I_alc_rule_row.rule_level,
                                      cd.dept,
                                      cd.class,
                                      cd.subclass,
                                      cd.source_item,
                                      cd.source_item_level,
                                      cd.source_tran_level,
                                      cd.source_pack_ind,
                                      cd.source_diff1_id,
                                      cd.source_diff2_id,
                                      cd.source_diff3_id,
                                      cd.source_diff4_id,
                                      cd.tran_item,
                                      cd.tran_item_level,
                                      cd.tran_tran_level,
                                      cd.tran_pack_ind,
                                      cd.tran_diff1_id,
                                      cd.tran_diff2_id,
                                      cd.tran_diff3_id,
                                      cd.tran_diff4_id,
                                      cd.to_loc,
                                      cd.sister_store,
                                      NULL,        --size_profile_qty,
                                      NULL,        --total_profile_qty,
                                      d.eow_date,
                                      d.weight,
                                      -1,
                                      -1,
                                      -1,
                                      -1,
                                      -1,
                                      -1,
                                      -1
                                 from (select DISTINCT tmp.alloc_id,
                                              tmp.dept,
                                              tmp.class,
                                              tmp.subclass,
                                              tmp.source_item,
                                              tmp.source_item_level,
                                              tmp.source_tran_level,
                                              tmp.source_pack_ind,
                                              tmp.source_diff1_id,
                                              tmp.source_diff2_id,
                                              tmp.source_diff3_id,
                                              tmp.source_diff4_id,
                                              tmp.tran_item,
                                              tmp.tran_item_level,
                                              tmp.tran_tran_level,
                                              tmp.tran_pack_ind,
                                              tmp.tran_diff1_id,
                                              tmp.tran_diff2_id,
                                              tmp.tran_diff3_id,
                                              tmp.tran_diff4_id,
                                              tmp.to_loc,
                                              tmp.sister_store
                                         from alc_calc_destination_temp tmp
                                        where tmp.alloc_id = I_alloc_id) cd,
                                      alc_calc_need_dates_temp d
                                where d.alloc_id = I_alloc_id;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SEED_NEED_GTT;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--SALES HIST FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_SALES_HIST';

BEGIN

   if I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT then

      if SETUP_NEED_SALES_HIST_DEPT(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS then

      if SETUP_NEED_SALES_HIST_CLASS(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS then

      if SETUP_NEED_SALES_HIST_SUBCLASS(O_error_message,
                                        I_alloc_id,
                                        I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE then

      if SETUP_NEED_SALES_HIST_PDIFF(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU then

      if SETUP_NEED_SALES_HIST_TRAN(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL then

      if SETUP_NEED_SALES_HIST_STYLE(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_SALES_HIST;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_DEPT(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_SALES_HIST_DEPT';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.eow_date,
                 SUM(NVL(dsh.sales,-1) * DECODE(tmp.weight,
                                                NULL, 1.0,
                                                tmp.weight)) sales
            from (select at.alloc_id,
                         at.to_loc,
                         at.rule_many_to_one_id,
                         at.dept,
                         at.eow_date,
                         at.weight,
                         ar.sales_type
                    from alc_calc_need_temp at,
                         (select DECODE(I_alc_rule_row.regular_sales_ind,
                                        'Y','R',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.promo_sales_ind,
                                        'Y','P',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.clearance_sales_ind,
                                        'Y','C',
                                        NULL) sales_type
                            from dual
						  union all
						  select 'I' sales_type from dual --WH Issues
						  ) ar
                   where at.alloc_id        = I_alloc_id
                     and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
                     and at.sales_hist_need = -1
                     and rownum             > 0) tmp,
                 mv_subclass_loc_hist dsh
           where dsh.eow_date    = tmp.eow_date
             and dsh.loc       = tmp.to_loc
             and dsh.sales_type  = tmp.sales_type
             and dsh.dept        = tmp.dept
			 and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.sales_hist_need = use_this.sales;

   if LP_use_sister_store = 'Y' then

      merge into alc_calc_need_temp target
      using (select tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.eow_date,
                    SUM(NVL(dsh.sales,-1) * DECODE(tmp.weight,
                                                   NULL, 1.0,
                                                   tmp.weight)) sales
               from (select at.alloc_id,
                            at.to_loc,
                            at.sister_store,
                            at.rule_many_to_one_id,
                            at.dept,
                            at.eow_date,
                            at.weight,
                            ar.sales_type
                       from alc_calc_need_temp at,
                            (select DECODE(I_alc_rule_row.regular_sales_ind,
                                           'Y','R',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.promo_sales_ind,
                                           'Y','P',
                                           NULL) sales_type from dual
                             union all
                             select DECODE(I_alc_rule_row.clearance_sales_ind,
                                           'Y','C',
                                           NULL) sales_type
                               from dual
    						 union all
						     select 'I' sales_type from dual --WH Issues
                             ) ar
                      where at.alloc_id        = I_alloc_id
                        and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
                        and at.sales_hist_need = -1
                        and rownum             > 0) tmp,
                    mv_subclass_loc_hist dsh
              where dsh.eow_date    = tmp.eow_date
                and dsh.loc       = tmp.sister_store
                and dsh.sales_type  = tmp.sales_type
                and dsh.dept        = tmp.dept
				and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
              group by tmp.alloc_id,
                       tmp.rule_many_to_one_id,
                       tmp.to_loc,
                       tmp.dept,
                       tmp.eow_date) use_this
      on (    target.alloc_id            = use_this.alloc_id
          and target.rule_many_to_one_id = use_this.rule_many_to_one_id
          and target.to_loc              = use_this.to_loc
          and target.dept                = use_this.dept
          and target.eow_date            = use_this.eow_date)
      when MATCHED then
         update
            set target.sales_hist_need = use_this.sales;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_SALES_HIST_DEPT;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_CLASS(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_SALES_HIST_CLASS';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.eow_date,
                 SUM(NVL(dsh.sales,-1) * DECODE(tmp.weight,
                                                NULL, 1.0,
                                                tmp.weight)) sales
            from (select at.alloc_id,
                         at.to_loc,
                         at.rule_many_to_one_id,
                         at.dept,
                         at.class,
                         at.eow_date,
                         at.weight,
                         ar.sales_type
                    from alc_calc_need_temp at,
                         (select DECODE(I_alc_rule_row.regular_sales_ind,
                                        'Y','R',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.promo_sales_ind,
                                        'Y','P',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.clearance_sales_ind,
                                        'Y','C',
                                        NULL) sales_type
                            from dual
						  union all
						  select 'I' sales_type from dual --WH Issues
						  ) ar
                   where at.alloc_id        = I_alloc_id
                     and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
                     and at.sales_hist_need = -1
                     and rownum             > 0) tmp,
                 mv_subclass_loc_hist dsh
           where dsh.eow_date    = tmp.eow_date
             and dsh.loc       = tmp.to_loc
             and dsh.sales_type  = tmp.sales_type
             and dsh.dept        = tmp.dept
             and dsh.class       = tmp.class
			 and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.sales_hist_need = use_this.sales;

   if LP_use_sister_store = 'Y' then

      merge into alc_calc_need_temp target
      using (select tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.eow_date,
                    SUM(NVL(dsh.sales,-1) * DECODE(tmp.weight,
                                                   NULL, 1.0,
                                                   tmp.weight)) sales
               from (select at.alloc_id,
                            at.to_loc,
                            at.sister_store,
                            at.rule_many_to_one_id,
                            at.dept,
                            at.class,
                            at.eow_date,
                            at.weight,
                            ar.sales_type
                       from alc_calc_need_temp at,
                            (select DECODE(I_alc_rule_row.regular_sales_ind,
                                           'Y','R',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.promo_sales_ind,
                                           'Y','P',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.clearance_sales_ind,
                                           'Y','C',
                                           NULL) sales_type
                               from dual
						  union all
						  select 'I' sales_type from dual --WH Issues
     					  ) ar
                      where at.alloc_id        = I_alloc_id
                        and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
                        and at.sales_hist_need = -1
                        and rownum             > 0) tmp,
                    mv_subclass_loc_hist dsh
              where dsh.eow_date    = tmp.eow_date
                and dsh.loc       = tmp.sister_store
                and dsh.sales_type  = tmp.sales_type
                and dsh.dept        = tmp.dept
                and dsh.class       = tmp.class
				and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
              group by tmp.alloc_id,
                       tmp.rule_many_to_one_id,
                       tmp.to_loc,
                       tmp.dept,
                       tmp.class,
                       tmp.eow_date) use_this
      on (    target.alloc_id            = use_this.alloc_id
          and target.rule_many_to_one_id = use_this.rule_many_to_one_id
          and target.to_loc              = use_this.to_loc
          and target.dept                = use_this.dept
          and target.class               = use_this.class
          and target.eow_date            = use_this.eow_date)
      when MATCHED then
         update
            set target.sales_hist_need = use_this.sales;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_SALES_HIST_CLASS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_SUBCLASS(O_error_message   IN OUT VARCHAR2,
                                        I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                        I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_SALES_HIST_SUBCLASS';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.subclass,
                 tmp.eow_date,
                 SUM(NVL(dsh.sales,-1) * DECODE(tmp.weight,
                                                NULL, 1.0,
                                                tmp.weight)) sales
            from (select at.alloc_id,
                         at.to_loc,
                         at.rule_many_to_one_id,
                         at.dept,
                         at.class,
                         at.subclass,
                         at.eow_date,
                         at.weight,
                         ar.sales_type
                    from alc_calc_need_temp at,
                         (select DECODE(I_alc_rule_row.regular_sales_ind,
                                        'Y','R',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.promo_sales_ind,
                                        'Y','P',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.clearance_sales_ind,
                                        'Y','C',
                                        NULL) sales_type
                            from dual
						  union all
						  select 'I' sales_type from dual --WH Issues
						  ) ar
                   where at.alloc_id        = I_alloc_id
                     and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
                     and at.sales_hist_need = -1
                     and rownum             > 0) tmp,
                 mv_subclass_loc_hist dsh
           where dsh.eow_date    = tmp.eow_date
             and dsh.loc       = tmp.to_loc
             and dsh.sales_type  = tmp.sales_type
             and dsh.dept        = tmp.dept
             and dsh.class       = tmp.class
             and dsh.subclass    = tmp.subclass
			 and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.subclass,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.subclass            = use_this.subclass
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.sales_hist_need = use_this.sales;

   if LP_use_sister_store = 'Y' then

      merge into alc_calc_need_temp target
      using (select tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.subclass,
                    tmp.eow_date,
                    SUM(NVL(dsh.sales,-1) * DECODE(tmp.weight,
                                                   NULL, 1.0,
                                                   tmp.weight)) sales
               from (select at.alloc_id,
                            at.to_loc,
                            at.sister_store,
                            at.rule_many_to_one_id,
                            at.dept,
                            at.class,
                            at.subclass,
                            at.eow_date,
                            at.weight,
                            ar.sales_type
                       from alc_calc_need_temp at,
                            (select DECODE(I_alc_rule_row.regular_sales_ind,
                                           'Y','R',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.promo_sales_ind,
                                           'Y','P',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.clearance_sales_ind,
                                           'Y','C',
                                           NULL) sales_type
                               from dual
						  union all
						  select 'I' sales_type from dual --WH Issues
						  ) ar
                      where at.alloc_id        = I_alloc_id
                        and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
                        and at.sales_hist_need = -1
                        and rownum             > 0) tmp,
                    mv_subclass_loc_hist dsh
              where dsh.eow_date    = tmp.eow_date
                and dsh.loc       = tmp.sister_store
                and dsh.sales_type  = tmp.sales_type
                and dsh.dept        = tmp.dept
                and dsh.class       = tmp.class
                and dsh.subclass    = tmp.subclass
				and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
              group by tmp.alloc_id,
                       tmp.rule_many_to_one_id,
                       tmp.to_loc,
                       tmp.dept,
                       tmp.class,
                       tmp.subclass,
                       tmp.eow_date) use_this
      on (    target.alloc_id            = use_this.alloc_id
          and target.rule_many_to_one_id = use_this.rule_many_to_one_id
          and target.to_loc              = use_this.to_loc
          and target.dept                = use_this.dept
          and target.class               = use_this.class
          and target.subclass            = use_this.subclass
          and target.eow_date            = use_this.eow_date)
      when MATCHED then
         update
            set target.sales_hist_need = use_this.sales;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_SALES_HIST_SUBCLASS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_PDIFF(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_SALES_HIST_PDIFF';

BEGIN

   merge into alc_calc_need_temp target
   using (select DISTINCT tmp.alloc_id,
                 tmp.to_loc,
                 tmp.rule_many_to_one_id,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.sales_issues,-1) * DECODE(tmp.weight,
                                                       NULL, 1.0,
                                                       tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                       tmp.rule_many_to_one_id,
                                                                                       tmp.to_loc,
                                                                                       tmp.source_item,
                                                                                       DECODE(LP_alloc_type,
                                                                                              ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                              tmp.source_diff1_id),
                                                                                       DECODE(LP_alloc_type,
                                                                                              ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                              tmp.source_diff2_id),
                                                                                       DECODE(LP_alloc_type,
                                                                                              ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                              tmp.source_diff3_id),
                                                                                       DECODE(LP_alloc_type,
                                                                                              ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                              tmp.source_diff4_id),
                                                                                       tmp.eow_date) sales
            from (select at.alloc_id,
                         at.to_loc,
                         at.rule_many_to_one_id,
                         at.source_item,
                         at.source_diff1_id,
                         at.source_diff2_id,
                         at.source_diff3_id,
                         at.source_diff4_id,
                         at.tran_item,
                         at.eow_date,
                         at.weight,
                         ar.sales_type
                    from alc_calc_need_temp at,
                         (select DECODE(I_alc_rule_row.regular_sales_ind,
                                        'Y','R',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.promo_sales_ind,
                                        'Y','P',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.clearance_sales_ind,
                                        'Y','C',
                                        NULL) sales_type
                            from dual
						  union all
						  select 'I' sales_type from dual --WH Issues
						  ) ar
                   where at.alloc_id        = I_alloc_id
                     and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                     and at.sales_hist_need = -1
                     and rownum             > 0) tmp,
                 item_loc_hist dsh
           where dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.sales_type  = tmp.sales_type
			 and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
             and dsh.item        = tmp.tran_item) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.sales_hist_need = use_this.sales;

   if LP_use_sister_store = 'Y' then

      merge into alc_calc_need_temp target
      using (select DISTINCT tmp.alloc_id,
                    tmp.to_loc,
                    tmp.rule_many_to_one_id,
                    tmp.source_item,
                    tmp.tran_item,
                    tmp.eow_date,
                    SUM(NVL(dsh.sales_issues,-1) * DECODE(tmp.weight,
                                                          NULL, 1.0,
                                                          tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                          tmp.rule_many_to_one_id,
                                                                                          tmp.to_loc,
                                                                                          tmp.source_item,
                                                                                          DECODE(LP_alloc_type,
                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                 tmp.source_diff1_id),
                                                                                          DECODE(LP_alloc_type,
                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                 tmp.source_diff2_id),
                                                                                          DECODE(LP_alloc_type,
                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                 tmp.source_diff3_id),
                                                                                          DECODE(LP_alloc_type,
                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                 tmp.source_diff4_id),
                                                                                          tmp.eow_date) sales
               from (select at.alloc_id,
                            at.to_loc,
                            at.sister_store,
                            at.rule_many_to_one_id,
                            at.source_item,
                            at.source_diff1_id,
                            at.source_diff2_id,
                            at.source_diff3_id,
                            at.source_diff4_id,
                            at.tran_item,
                            at.eow_date,
                            at.weight,
                            ar.sales_type
                       from alc_calc_need_temp at,
                            (select DECODE(I_alc_rule_row.regular_sales_ind,
                                           'Y','R',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.promo_sales_ind,
                                           'Y','P',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.clearance_sales_ind,
                                           'Y', 'C',
                                           NULL) sales_type
                               from dual
							 union all
							 select 'I' sales_type from dual --WH Issues
							 ) ar
                      where at.alloc_id        = I_alloc_id
                        and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                        and at.sales_hist_need = -1
                        and rownum             > 0) tmp,
                    item_loc_hist dsh
              where dsh.eow_date    = tmp.eow_date
                and dsh.loc         = tmp.sister_store
                and dsh.sales_type  = tmp.sales_type
				and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
                and dsh.item        = tmp.tran_item) use_this
      on (    target.alloc_id            = use_this.alloc_id
          and target.rule_many_to_one_id = use_this.rule_many_to_one_id
          and target.to_loc              = use_this.to_loc
          and target.source_item         = use_this.source_item
          and target.tran_item           = use_this.tran_item
          and target.eow_date            = use_this.eow_date)
      when MATCHED then
         update
            set target.sales_hist_need = use_this.sales;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_SALES_HIST_PDIFF;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_TRAN(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_SALES_HIST_TRAN';

BEGIN

   merge into alc_calc_need_temp target
   using (select /*+ INDEX(dsh PK_ITEM_LOC_HIST) */
                 DISTINCT tmp.alloc_id,
                 tmp.to_loc,
                 tmp.rule_many_to_one_id,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.sales_issues,-1) * DECODE(tmp.weight,
                                                       NULL, 1.0,
                                                       tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                       tmp.rule_many_to_one_id,
                                                                                       tmp.to_loc,
                                                                                       tmp.source_item,
                                                                                       tmp.tran_item,
                                                                                       tmp.eow_date) sales
            from (select at.alloc_id,
                         at.to_loc,
                         at.rule_many_to_one_id,
                         at.source_item,
                         at.tran_item,
                         at.eow_date,
                         at.weight,
                         ar.sales_type
                    from alc_calc_need_temp at,
                         (select DECODE(I_alc_rule_row.regular_sales_ind,
                                        'Y','R',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.promo_sales_ind,
                                        'Y','P',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.clearance_sales_ind,
                                        'Y','C',
                                        NULL) sales_type
                            from dual
						  union all
						  select 'I' sales_type from dual --WH Issues
						  ) ar
                   where at.alloc_id        = I_alloc_id
                     and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
                     and at.sales_hist_need = -1
                     and rownum             > 0) tmp,
                 item_loc_hist dsh
           where dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.sales_type  = tmp.sales_type
			 and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
             and dsh.item        = tmp.tran_item) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.sales_hist_need = use_this.sales;

   if LP_use_sister_store = 'Y' then

      merge into alc_calc_need_temp target
      using (select /*+ INDEX(dsh PK_ITEM_LOC_HIST) */
                    DISTINCT tmp.alloc_id,
                    tmp.to_loc,
                    tmp.rule_many_to_one_id,
                    tmp.source_item,
                    tmp.tran_item,
                    tmp.eow_date,
                    SUM(NVL(dsh.sales_issues,-1) * DECODE(tmp.weight,
                                                          NULL, 1.0,
                                                          tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                          tmp.rule_many_to_one_id,
                                                                                          tmp.to_loc,
                                                                                          tmp.source_item,
                                                                                          tmp.tran_item,
                                                                                          tmp.eow_date) sales
               from (select at.alloc_id,
                            at.to_loc,
                            at.sister_store,
                            at.rule_many_to_one_id,
                            at.source_item,
                            at.tran_item,
                            at.eow_date,
                            at.weight,
                            ar.sales_type
                       from alc_calc_need_temp at,
                            (select DECODE(I_alc_rule_row.regular_sales_ind,
                                           'Y','R',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.promo_sales_ind,
                                           'Y','P',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.clearance_sales_ind,
                                           'Y','C',
                                           NULL) sales_type
                               from dual
						  union all
						  select 'I' sales_type from dual --WH Issues
						 ) ar
                      where at.alloc_id        = I_alloc_id
                        and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
                        and at.sales_hist_need = -1
                        and rownum             > 0) tmp,
                    item_loc_hist dsh
              where dsh.eow_date    = tmp.eow_date
                and dsh.loc         = tmp.sister_store
                and dsh.sales_type  = tmp.sales_type
				and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
                and dsh.item        = tmp.tran_item) use_this
      on (    target.alloc_id            = use_this.alloc_id
          and target.rule_many_to_one_id = use_this.rule_many_to_one_id
          and target.to_loc              = use_this.to_loc
          and target.source_item         = use_this.source_item
          and target.tran_item           = use_this.tran_item
          and target.eow_date            = use_this.eow_date)
      when MATCHED then
         update
            set target.sales_hist_need = use_this.sales;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_SALES_HIST_TRAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_SALES_HIST_STYLE(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_SALES_HIST_STYLE';

BEGIN

   merge into alc_calc_need_temp target
   using (select /*+ INDEX(dsh PK_ITEM_LOC_HIST) */
                 DISTINCT tmp.alloc_id,
                 tmp.to_loc,
                 tmp.rule_many_to_one_id,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.sales_issues,-1) * DECODE(tmp.weight,
                                                       NULL, 1.0,
                                                       tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                       tmp.rule_many_to_one_id,
                                                                                       tmp.to_loc,
                                                                                       tmp.source_item,
                                                                                       tmp.eow_date) sales
            from (select at.alloc_id,
                         at.to_loc,
                         at.rule_many_to_one_id,
                         at.source_item,
                         at.tran_item,
                         at.eow_date,
                         at.weight,
                         ar.sales_type
                    from alc_calc_need_temp at,
                         (select DECODE(I_alc_rule_row.regular_sales_ind,
                                        'Y','R',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.promo_sales_ind,
                                        'Y','P',
                                        NULL) sales_type
                            from dual
                          union all
                          select DECODE(I_alc_rule_row.clearance_sales_ind,
                                        'Y','C',
                                        NULL) sales_type
                            from dual
						  union all
						  select 'I' sales_type from dual --WH Issues
						  ) ar
                   where at.alloc_id        = I_alloc_id
                     and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
                     and at.sales_hist_need = -1
                     and rownum             > 0) tmp,
                 item_loc_hist dsh
           where dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.sales_type  = tmp.sales_type
			 and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
             and dsh.item        = tmp.tran_item) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.sales_hist_need = use_this.sales;

   if LP_use_sister_store = 'Y' then

      merge into alc_calc_need_temp target
      using (select /*+ INDEX(dsh PK_ITEM_LOC_HIST) */
                    DISTINCT tmp.alloc_id,
                    tmp.to_loc,
                    tmp.rule_many_to_one_id,
                    tmp.source_item,
                    tmp.tran_item,
                    tmp.eow_date,
                    SUM(NVL(dsh.sales_issues,-1) * DECODE(tmp.weight,
                                                          NULL, 1.0,
                                                          tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                          tmp.rule_many_to_one_id,
                                                                                          tmp.to_loc,
                                                                                          tmp.source_item,
                                                                                          tmp.eow_date) sales
               from (select at.alloc_id,
                            at.to_loc,
                            at.sister_store,
                            at.rule_many_to_one_id,
                            at.source_item,
                            at.tran_item,
                            at.eow_date,
                            at.weight,
                            ar.sales_type
                       from alc_calc_need_temp at,
                            (select DECODE(I_alc_rule_row.regular_sales_ind,
                                           'Y','R',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.promo_sales_ind,
                                           'Y','P',
                                           NULL) sales_type
                               from dual
                             union all
                             select DECODE(I_alc_rule_row.clearance_sales_ind,
                                           'Y','C',
                                           NULL) sales_type
                               from dual
							 union all
							 select 'I' sales_type from dual --WH Issues
							 ) ar
                      where at.alloc_id        = I_alloc_id
                        and at.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
                        and at.sales_hist_need = -1
                        and rownum             > 0) tmp,
                    item_loc_hist dsh
              where dsh.eow_date    = tmp.eow_date
                and dsh.loc         = tmp.sister_store
                and dsh.sales_type  = tmp.sales_type
				and ((dsh.loc_type = 'W' and dsh.sales_type = 'I') or (dsh.loc_type = 'S' and dsh.sales_type in ('R','P','C')))
                and dsh.item        = tmp.tran_item) use_this
      on (    target.alloc_id            = use_this.alloc_id
          and target.rule_many_to_one_id = use_this.rule_many_to_one_id
          and target.to_loc              = use_this.to_loc
          and target.source_item         = use_this.source_item
          and target.tran_item           = use_this.tran_item
          and target.eow_date            = use_this.eow_date)
      when MATCHED then
         update
            set target.sales_hist_need = use_this.sales;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_SALES_HIST_STYLE;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--FORECAST FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST(O_error_message   IN OUT VARCHAR2,
                         I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                         I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_FCST';

BEGIN

   if I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT then

      if SETUP_NEED_FCST_DEPT(O_error_message,
                              I_alloc_id,
                              I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS then

      if SETUP_NEED_FCST_CLASS(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS then

      if SETUP_NEED_FCST_SUBCLASS(O_error_message,
                                  I_alloc_id,
                                  I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE then

      if SETUP_NEED_FCST_PDIFF(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU then

      if SETUP_NEED_FCST_TRAN(O_error_message,
                              I_alloc_id,
                              I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL then

      if SETUP_NEED_FCST_STYLE(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_FCST;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_DEPT(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_FCST_DEPT';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) sales
            from dept_sales_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id    = I_alloc_id
             and tmp.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
             and dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.dept        = tmp.dept
             and tmp.forecast_need  = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.to_loc              = use_this.to_loc
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.dept                = use_this.dept
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;


   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) sales
            from dept_sales_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id     = I_alloc_id
             and LP_use_sister_store   = 'Y'
             and tmp.rule_level   = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
             and dsh.eow_date     = tmp.eow_date
             and dsh.loc          = tmp.sister_store
             and dsh.dept         = tmp.dept
             and tmp.forecast_need   = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_FCST_DEPT;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_CLASS(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_FCST_CLASS';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) sales
            from class_sales_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id    = I_alloc_id
             and tmp.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
             and dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.dept        = tmp.dept
             and dsh.class       = tmp.class
             and tmp.forecast_need  = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.to_loc              = use_this.to_loc
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.to_loc,
                 tmp.rule_many_to_one_id,
                 tmp.dept,
                 tmp.class,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) sales
            from class_sales_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id     = I_alloc_id
             and LP_use_sister_store   = 'Y'
             and tmp.rule_level   = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
             and dsh.eow_date     = tmp.eow_date
             and dsh.loc          = tmp.sister_store
             and dsh.dept         = tmp.dept
             and dsh.class        = tmp.class
             and tmp.forecast_need   = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.to_loc              = use_this.to_loc
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_FCST_CLASS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_SUBCLASS(O_error_message   IN OUT VARCHAR2,
                                  I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                  I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_FCST_SUBCLASS';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.subclass,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) sales
            from subclass_sales_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id    = I_alloc_id
             and tmp.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
             and dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.dept        = tmp.dept
             and dsh.class       = tmp.class
             and dsh.subclass    = tmp.subclass
             and tmp.forecast_need  = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.subclass,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.subclass            = use_this.subclass
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;


   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.subclass,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) sales
            from subclass_sales_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id        = I_alloc_id
             and LP_use_sister_store = 'Y'
             and tmp.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
             and dsh.eow_date        = tmp.eow_date
             and dsh.loc             = tmp.sister_store
             and dsh.dept            = tmp.dept
             and dsh.class           = tmp.class
             and dsh.subclass        = tmp.subclass
             and tmp.forecast_need   = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.subclass,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.to_loc              = use_this.to_loc
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.subclass            = use_this.subclass
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_FCST_SUBCLASS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_PDIFF(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_FCST_PDIFF';

BEGIN

   merge into alc_calc_need_temp target
   using (select DISTINCT tmp.alloc_id,
                 tmp.to_loc,
                 tmp.rule_many_to_one_id,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                         tmp.rule_many_to_one_id,
                                                                                         tmp.to_loc,
                                                                                         tmp.source_item,
                                                                                         DECODE(LP_alloc_type,
                                                                                                ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                tmp.source_diff1_id),
                                                                                         DECODE(LP_alloc_type,
                                                                                                ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                tmp.source_diff2_id),
                                                                                         DECODE(LP_alloc_type,
                                                                                                ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                tmp.source_diff3_id),
                                                                                         DECODE(LP_alloc_type,
                                                                                                ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                tmp.source_diff4_id),
                                                                                         tmp.eow_date) sales
            from item_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id      = I_alloc_id
             and tmp.rule_level    = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
             and dsh.eow_date      = tmp.eow_date
             and dsh.loc           = tmp.to_loc
             and dsh.item          = tmp.tran_item
             and tmp.forecast_need = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.to_loc              = use_this.to_loc
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                         tmp.to_loc,
                                                                                         tmp.rule_many_to_one_id,
                                                                                         tmp.source_item,
                                                                                         DECODE(LP_alloc_type,
                                                                                                ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                tmp.source_diff1_id),
                                                                                         DECODE(LP_alloc_type,
                                                                                                ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                tmp.source_diff2_id),
                                                                                         DECODE(LP_alloc_type,
                                                                                                ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                tmp.source_diff3_id),
                                                                                         DECODE(LP_alloc_type,
                                                                                                ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                tmp.source_diff4_id),
                                                                                         tmp.eow_date) sales
            from item_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id        = I_alloc_id
             and LP_use_sister_store = 'Y'
             and tmp.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
             and dsh.eow_date        = tmp.eow_date
             and dsh.loc             = tmp.sister_store
             and dsh.item            = tmp.tran_item
             and tmp.forecast_need   = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_FCST_PDIFF;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_TRAN(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_FCST_TRAN';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                         tmp.rule_many_to_one_id,
                                                                                         tmp.to_loc,
                                                                                         tmp.source_item,
                                                                                         tmp.tran_item,
                                                                                         tmp.eow_date) sales
            from item_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id      = I_alloc_id
             and tmp.rule_level    = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
             and dsh.eow_date      = tmp.eow_date
             and dsh.loc           = tmp.to_loc
             and dsh.item          = tmp.tran_item
             and tmp.forecast_need = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                         tmp.rule_many_to_one_id,
                                                                                         tmp.to_loc,
                                                                                         tmp.source_item,
                                                                                         tmp.tran_item,
                                                                                         tmp.eow_date) sales
            from item_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id        = I_alloc_id
             and LP_use_sister_store = 'Y'
             and tmp.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
             and dsh.eow_date        = tmp.eow_date
             and dsh.loc             = tmp.sister_store
             and dsh.item            = tmp.tran_item
             and tmp.forecast_need   = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_FCST_TRAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_FCST_STYLE(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_FCST_STYLE';

BEGIN

   merge into alc_calc_need_temp target
   using (select DISTINCT tmp.alloc_id,
                 tmp.to_loc,
                 tmp.rule_many_to_one_id,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                         tmp.rule_many_to_one_id,
                                                                                         tmp.to_loc,
                                                                                         tmp.source_item,
                                                                                         tmp.eow_date) sales
            from item_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id      = I_alloc_id
             and tmp.rule_level    = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
             and dsh.eow_date      = tmp.eow_date
             and dsh.loc           = tmp.to_loc
             and dsh.item          = tmp.tran_item
             and tmp.forecast_need = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.to_loc,
                 tmp.rule_many_to_one_id,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.forecast_sales,-1) * DECODE(tmp.weight,
                                                         NULL, 1.0,
                                                         tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                         tmp.rule_many_to_one_id,
                                                                                         tmp.to_loc,
                                                                                         tmp.source_item,
                                                                                         tmp.eow_date) sales
            from item_forecast dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id        = I_alloc_id
             and LP_use_sister_store = 'Y'
             and tmp.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
             and dsh.eow_date        = tmp.eow_date
             and dsh.loc             = tmp.sister_store
             and dsh.item            = tmp.tran_item
             and tmp.forecast_need   = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.forecast_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_FCST_STYLE;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--PLAN REPROJECT FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN(O_error_message   IN OUT VARCHAR2,
                           I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                           I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_REPLAN';

   L_current_eow_date      DATE;
   L_plan_sensitivity      ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE := NULL;

BEGIN

   select fp_plan_sensitivity
     into L_plan_sensitivity
     from alc_system_options;

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow_date,
                         SYSDATE) = FALSE then
      return FALSE;
   end if;

   if SETUP_IWOS(O_error_message,
                 I_alloc_id,
                 I_alc_rule_row) = FALSE then
      return FALSE;
   end if;

   if I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT then

      if SETUP_NEED_REPLAN_DEPT(O_error_message,
                                I_alloc_id,
                                I_alc_rule_row,
                                L_current_eow_date,
                                L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS then

      if SETUP_NEED_REPLAN_CLASS(O_error_message,
                                 I_alloc_id,
                                 I_alc_rule_row,
                                 L_current_eow_date,
                                 L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS then

      if SETUP_NEED_REPLAN_SUBCLASS(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row,
                                    L_current_eow_date,
                                    L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE then

      if SETUP_NEED_REPLAN_PDIFF(O_error_message,
                                 I_alloc_id,
                                 I_alc_rule_row,
                                 L_current_eow_date,
                                 L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU then

      if SETUP_NEED_REPLAN_ITEM(O_error_message,
                                I_alloc_id,
                                I_alc_rule_row,
                                L_current_eow_date,
                                L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL then

      if SETUP_NEED_REPLAN_STYLE(O_error_message,
                                 I_alloc_id,
                                 I_alc_rule_row,
                                 L_current_eow_date,
                                 L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;

   end if;

   update alc_calc_need_temp
      set sales_hist_need = -1
    where alloc_id = I_alloc_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_REPLAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_IWOS(O_error_message   IN OUT VARCHAR2,
                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_IWOS';

BEGIN

   --get ideal weeks of supply
   if I_alc_rule_row.iwos_weeks is not null then

      update alc_calc_need_temp n
         set n.iwos_weeks = I_alc_rule_row.iwos_weeks
       where n.alloc_id   = I_alloc_id;

   else

      merge into alc_calc_need_temp target
      using (select i2.alloc_id,
                    i2.rule_many_to_one_id,
                    i2.to_loc,
                    i2.eow_date,
                    i2.tran_item,
                    i2.iwos_weeks
               from (select i1.alloc_id,
                            i1.rule_many_to_one_id,
                            i1.to_loc,
                            i1.eow_date,
                            i1.tran_item,
                            i1.iwos_weeks,
                            i1.row_level,
                            MIN(i1.row_level) OVER (PARTITION BY i1.alloc_id,
                                                                 i1.rule_many_to_one_id,
                                                                 i1.to_loc,
                                                                 i1.eow_date,
                                                                 i1.tran_item
                                                        ORDER BY i1.row_level) min_level
                       from (select n.alloc_id,
                                    n.rule_many_to_one_id,
                                    n.to_loc,
                                    n.eow_date,
                                    n.tran_item,
                                    w.iwos_weeks,
                                    1 as row_level --ITEMlevel
                               from alc_calc_need_temp n,
                                    alc_ideal_weeks_of_supply w
                              where n.alloc_id    = I_alloc_id
                                and n.to_loc      = w.location_id
                                and n.tran_item   = w.item_id
                                and w.dept        is NULL
                                and w.class       is NULL
                                and w.subclass    is NULL
                                and w.diff1_id    is NULL
                             UNION ALL
                             select n.alloc_id,
                                    n.rule_many_to_one_id,
                                    n.to_loc,
                                    n.eow_date,
                                    n.tran_item,
                                    w.iwos_weeks,
                                    2 as row_level --STYLEDIFFlevel
                               from alc_calc_need_temp n,
                                    alc_ideal_weeks_of_supply w
                              where n.alloc_id         = I_alloc_id
                                and n.to_loc           = w.location_id
                                and n.source_item      = w.item_id
                                and (n.source_diff1_id = w.diff1_id or
                                     n.source_diff2_id = w.diff1_id or
                                     n.source_diff3_id = w.diff1_id or
                                     n.source_diff4_id = w.diff1_id)
                                and w.dept             is NULL
                                and w.class            is NULL
                                and w.subclass         is NULL
                             UNION ALL
                             select n.alloc_id,
                                    n.rule_many_to_one_id,
                                    n.to_loc,
                                    n.eow_date,
                                    n.tran_item,
                                    w.iwos_weeks,
                                    3 as row_level --STYLElevel
                               from alc_calc_need_temp n,
                                    alc_ideal_weeks_of_supply w
                              where n.alloc_id    = I_alloc_id
                                and n.to_loc       = w.location_id
                                and n.source_item = w.item_id
                                and w.dept        is NULL
                                and w.class       is NULL
                                and w.subclass    is NULL
                                and w.diff1_id    is NULL
                             UNION ALL
                             select n.alloc_id,
                                    n.rule_many_to_one_id,
                                    n.to_loc,
                                    n.eow_date,
                                    n.tran_item,
                                    w.iwos_weeks,
                                    4 as row_level --SUBCLASSlevel
                               from alc_calc_need_temp n,
                                    alc_ideal_weeks_of_supply w
                              where n.alloc_id = I_alloc_id
                                and n.to_loc   = w.location_id
                                and n.dept     = w.dept
                                and n.class    = w.class
                                and n.subclass = w.subclass
                                and w.diff1_id is NULL
                                and w.item_id  is NULL
                             UNION ALL
                             select n.alloc_id,
                                    n.rule_many_to_one_id,
                                    n.to_loc,
                                    n.eow_date,
                                    n.tran_item,
                                    w.iwos_weeks,
                                    5 as row_level --CLASSlevel
                               from alc_calc_need_temp n,
                                    alc_ideal_weeks_of_supply w
                              where n.alloc_id = I_alloc_id
                                and n.to_loc   = w.location_id
                                and n.dept     = w.dept
                                and n.class    = w.class
                                and w.subclass is NULL
                                and w.diff1_id is NULL
                                and w.item_id  is NULL
                             UNION ALL
                             select n.alloc_id,
                                    n.rule_many_to_one_id,
                                    n.to_loc,
                                    n.eow_date,
                                    n.tran_item,
                                    w.iwos_weeks,
                                    6 as row_level --DEPTlevel
                               from alc_calc_need_temp n,
                                    alc_ideal_weeks_of_supply w
                              where n.alloc_id = I_alloc_id
                                and n.to_loc   = w.location_id
                                and n.dept     = w.dept
                                and w.class    is NULL
                                and w.subclass is NULL
                                and w.diff1_id is NULL
                                and w.item_id  is NULL) i1) i2
              where i2.row_level = i2.min_level
      ) use_this
      on (    target.alloc_id            = use_this.alloc_id
          and target.rule_many_to_one_id = use_this.rule_many_to_one_id
          and target.to_loc              = use_this.to_loc
          and target.eow_date            = use_this.eow_date
          and target.tran_item           = use_this.tran_item)
      when MATCHED then
         update
            set target.iwos_weeks = use_this.iwos_weeks;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_IWOS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_DEPT(O_error_message      IN OUT VARCHAR2,
                                I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                I_current_eow_date   IN     DATE,
                                I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_REPLAN_DEPT';

BEGIN

   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.dept,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.dept,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.dept
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.dept) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.dept) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.loc                         = tmp.to_loc
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.dept                        = tmp.dept
                     --
                     and dsh.class                       is NULL
                     and dsh.subclass                    is NULL
                     and dsh.item_id                     is NULL
                     and dsh.diff1_id                    is NULL
                     and dsh.diff2_id                    is NULL
                     and dsh.diff3_id                    is NULL
                     and dsh.diff4_id                    is NULL
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.to_loc
                     and dsh2.dept(+)                    = tmp.dept
                     --
                     and dsh2.class(+)                   is NULL
                     and dsh2.subclass(+)                is NULL
                     and dsh2.item_id(+)                 is NULL
                     and dsh2.diff1_id(+)                is NULL
                     and dsh2.diff2_id(+)                is NULL
                     and dsh2.diff3_id(+)                is NULL
                     and dsh2.diff4_id(+)                is NULL) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))
           group by inner.alloc_id,
                    inner.alloc_id,
                    inner.rule_many_to_one_id,
                    inner.to_loc,
                    inner.dept) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.dept                       = use_this.dept)
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.dept,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.dept,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.dept
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.dept) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.dept) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and LP_use_sister_store             = 'Y'
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.loc                         = tmp.sister_store
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.dept                        = tmp.dept
                     --
                     and dsh.class                       is NULL
                     and dsh.subclass                    is NULL
                     and dsh.item_id                     is NULL
                     and dsh.diff1_id                    is NULL
                     and dsh.diff2_id                    is NULL
                     and dsh.diff3_id                    is NULL
                     and dsh.diff4_id                    is NULL
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.sister_store
                     and dsh2.dept(+)                    = tmp.dept
                     --
                     and dsh2.class(+)                   is NULL
                     and dsh2.subclass(+)                is NULL
                     and dsh2.item_id(+)                 is NULL
                     and dsh2.diff1_id(+)                is NULL
                     and dsh2.diff2_id(+)                is NULL
                     and dsh2.diff3_id(+)                is NULL
                     and dsh2.diff4_id(+)                is NULL) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))
           group by inner.alloc_id,
                    inner.alloc_id,
                    inner.rule_many_to_one_id,
                    inner.to_loc,
                    inner.dept) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.dept                       = use_this.dept)
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_REPLAN_DEPT;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_CLASS(O_error_message      IN OUT VARCHAR2,
                                 I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                 I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                 I_current_eow_date   IN     DATE,
                                 I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_REPLAN_CLASS';

BEGIN

   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.dept,
                 inner.class,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.dept,
                         tmp.class,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.dept,
                                                         tmp.class
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept,
                                                                              tmp.class,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept,
                                                                              tmp.class) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.dept,
                                                                               tmp.class) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.dept,
                                                                             tmp.class) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.loc                         = tmp.to_loc
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.dept                        = tmp.dept
                     and dsh.class                       = tmp.class
                     --
                     and dsh.subclass                    is NULL
                     and dsh.item_id                     is NULL
                     and dsh.diff1_id                    is NULL
                     and dsh.diff2_id                    is NULL
                     and dsh.diff3_id                    is NULL
                     and dsh.diff4_id                    is NULL
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.to_loc
                     and dsh2.dept(+)                    = tmp.dept
                     and dsh2.class(+)                   = tmp.class
                     --
                     and dsh2.subclass(+)                is NULL
                     and dsh2.item_id(+)                 is NULL
                     and dsh2.diff1_id(+)                is NULL
                     and dsh2.diff2_id(+)                is NULL
                     and dsh2.diff3_id(+)                is NULL
                     and dsh2.diff4_id(+)                is NULL) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))
           group by inner.alloc_id,
                    inner.alloc_id,
                    inner.rule_many_to_one_id,
                    inner.to_loc,
                    inner.dept,
                    inner.class) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.dept                       = use_this.dept
       and target.class                      = use_this.class)
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.dept,
                 inner.class,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.dept,
                         tmp.class,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.dept,
                                                         tmp.class
                                                order by dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept,
                                                                              tmp.class,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept,
                                                                              tmp.class) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.dept,
                                                                               tmp.class) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.dept,
                                                                             tmp.class) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and LP_use_sister_store             = 'Y'
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.loc                         = tmp.sister_store
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.dept                        = tmp.dept
                     and dsh.class                       = tmp.class
                     --
                     and dsh.subclass                    is NULL
                     and dsh.item_id                     is NULL
                     and dsh.diff1_id                    is NULL
                     and dsh.diff2_id                    is NULL
                     and dsh.diff3_id                    is NULL
                     and dsh.diff4_id                    is NULL
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.sister_store
                     and dsh2.dept(+)                    = tmp.dept
                     and dsh2.class(+)                   = tmp.class
                     --
                     and dsh2.subclass(+)                is NULL
                     and dsh2.item_id(+)                 is NULL
                     and dsh2.diff1_id(+)                is NULL
                     and dsh2.diff2_id(+)                is NULL
                     and dsh2.diff3_id(+)                is NULL
                     and dsh2.diff4_id(+)                is NULL) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))
           group by inner.alloc_id,
                    inner.alloc_id,
                    inner.rule_many_to_one_id,
                    inner.to_loc,
                    inner.dept,
                    inner.class) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.dept                       = use_this.dept
       and target.class                      = use_this.class)
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_REPLAN_CLASS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_SUBCLASS(O_error_message      IN OUT VARCHAR2,
                                    I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                    I_current_eow_date   IN     DATE,
                                    I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_REPLAN_SUBCLASS';

BEGIN

   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.dept,
                 inner.class,
                 inner.subclass,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.dept,
                         tmp.class,
                         tmp.subclass,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.dept,
                                                         tmp.class,
                                                         tmp.subclass
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept,
                                                                              tmp.class,
                                                                              tmp.subclass,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept,
                                                                              tmp.class,
                                                                              tmp.subclass) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.dept,
                                                                               tmp.class,
                                                                               tmp.subclass) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.dept,
                                                                             tmp.class,
                                                                             tmp.subclass) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.loc                         = tmp.to_loc
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.dept                        = tmp.dept
                     and dsh.class                       = tmp.class
                     and dsh.subclass                    = tmp.subclass
                     --
                     and dsh.item_id                     is NULL
                     and dsh.diff1_id                    is NULL
                     and dsh.diff2_id                    is NULL
                     and dsh.diff3_id                    is NULL
                     and dsh.diff4_id                    is NULL
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.to_loc
                     and dsh2.dept(+)                    = tmp.dept
                     and dsh2.class(+)                   = tmp.class
                     and dsh2.subclass(+)                = tmp.subclass
                     --
                     and dsh2.item_id(+)                 is NULL
                     and dsh2.diff1_id(+)                is NULL
                     and dsh2.diff2_id(+)                is NULL
                     and dsh2.diff3_id(+)                is NULL
                     and dsh2.diff4_id(+)                is NULL) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))
           group by inner.alloc_id,
                    inner.alloc_id,
                    inner.rule_many_to_one_id,
                    inner.to_loc,
                    inner.dept,
                    inner.class,
                    inner.subclass) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.dept                       = use_this.dept
       and target.class                      = use_this.class
       and target.subclass                   = use_this.subclass)
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.dept,
                 inner.class,
                 inner.subclass,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.dept,
                         tmp.class,
                         tmp.subclass,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.dept,
                                                         tmp.class,
                                                         tmp.subclass
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept,
                                                                              tmp.class,
                                                                              tmp.subclass,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.dept,
                                                                              tmp.class,
                                                                              tmp.subclass) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.dept,
                                                                               tmp.class,
                                                                               tmp.subclass) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.dept,
                                                                             tmp.class,
                                                                             tmp.subclass) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and LP_use_sister_store             = 'Y'
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.loc                         = tmp.sister_store
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.dept                        = tmp.dept
                     and dsh.class                       = tmp.class
                     and dsh.subclass                    = tmp.subclass
                     --
                     and dsh.item_id                     is NULL
                     and dsh.diff1_id                    is NULL
                     and dsh.diff2_id                    is NULL
                     and dsh.diff3_id                    is NULL
                     and dsh.diff4_id                    is NULL
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.sister_store
                     and dsh2.dept(+)                    = tmp.dept
                     and dsh2.class(+)                   = tmp.class
                     and dsh2.subclass(+)                = tmp.subclass
                     --
                     and dsh2.item_id(+)                 is NULL
                     and dsh2.diff1_id(+)                is NULL
                     and dsh2.diff2_id(+)                is NULL
                     and dsh2.diff3_id(+)                is NULL
                     and dsh2.diff4_id(+)                is NULL) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))
           group by inner.alloc_id,
                    inner.alloc_id,
                    inner.rule_many_to_one_id,
                    inner.to_loc,
                    inner.dept,
                    inner.class,
                    inner.subclass) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.dept                       = use_this.dept
       and target.class                      = use_this.class
       and target.subclass                   = use_this.subclass)
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_REPLAN_SUBCLASS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_STYLE(O_error_message      IN OUT VARCHAR2,
                                 I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                 I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                 I_current_eow_date   IN     DATE,
                                 I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_REPLAN_STYLE';

BEGIN

   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.source_item,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.source_item,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.source_item
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.source_item) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,-1,0,tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                                                      tmp.rule_many_to_one_id,
                                                                                                      tmp.to_loc,
                                                                                                      tmp.source_item) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.loc                         = tmp.to_loc
                     and dsh.item_id                     = tmp.source_item
                     --
                     and dsh.diff1_id                    is NULL
                     and dsh.diff2_id                    is NULL
                     and dsh.diff3_id                    is NULL
                     and dsh.diff4_id                    is NULL
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.to_loc
                     and dsh2.item_id(+)                 = tmp.source_item
                     --
                     and dsh.diff1_id(+)                 is NULL
                     and dsh.diff2_id(+)                 is NULL
                     and dsh.diff3_id(+)                 is NULL
                     and dsh.diff4_id(+)                 is NULL) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))
           group by inner.alloc_id,
                    inner.alloc_id,
                    inner.rule_many_to_one_id,
                    inner.to_loc,
                    inner.source_item) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.source_item                = use_this.source_item)
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.source_item,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.source_item,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.source_item
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.source_item) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.source_item) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and LP_use_sister_store             = 'Y'
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.loc                         = tmp.sister_store
                     and dsh.item_id                     = tmp.source_item
                     --
                     and dsh.diff1_id                    is NULL
                     and dsh.diff2_id                    is NULL
                     and dsh.diff3_id                    is NULL
                     and dsh.diff4_id                    is NULL
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.sister_store
                     and dsh2.item_id(+)                 = tmp.source_item
                     --
                     and dsh.diff1_id(+)                 is NULL
                     and dsh.diff2_id(+)                 is NULL
                     and dsh.diff3_id(+)                 is NULL
                     and dsh.diff4_id(+)                 is NULL) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))
           group by inner.alloc_id,
                    inner.alloc_id,
                    inner.rule_many_to_one_id,
                    inner.to_loc,
                    inner.source_item) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.source_item                = use_this.source_item)
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_REPLAN_STYLE;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_ITEM(O_error_message      IN OUT VARCHAR2,
                                I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                I_current_eow_date   IN     DATE,
                                I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_REPLAN_ITEM';

BEGIN

   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.tran_item,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) OVER (PARTITION BY inner.alloc_id,
                                                                                                          inner.rule_many_to_one_id,
                                                                                                          inner.to_loc,
                                                                                                          inner.source_item,
                                                                                                          inner.tran_item) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.source_item,
                         tmp.tran_item,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.tran_item
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.tran_item,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.tran_item) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.tran_item) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.tran_item) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.loc                         = tmp.to_loc
                     and dsh.item_id                     = tmp.tran_item
                     --
                     and dsh.diff1_id                    is NULL
                     and dsh.diff2_id                    is NULL
                     and dsh.diff3_id                    is NULL
                     and dsh.diff4_id                    is NULL
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.to_loc
                     and dsh2.item_id(+)                 = tmp.tran_item
                     --
                     and dsh.diff1_id(+)                 is NULL
                     and dsh.diff2_id(+)                 is NULL
                     and dsh.diff3_id(+)                 is NULL
                     and dsh.diff4_id(+)                 is NULL) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.tran_item                  = use_this.tran_item)
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.tran_item,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) OVER (PARTITION BY inner.alloc_id,
                                                                                                          inner.rule_many_to_one_id,
                                                                                                          inner.to_loc,
                                                                                                          inner.source_item,
                                                                                                          inner.tran_item) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.source_item,
                         tmp.tran_item,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.tran_item
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.tran_item,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0, tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                                         tmp.rule_many_to_one_id,
                                                                                         tmp.to_loc,
                                                                                         tmp.tran_item) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.tran_item) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.tran_item) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and LP_use_sister_store             = 'Y'
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.loc                         = tmp.sister_store
                     and dsh.item_id                     = tmp.tran_item
                     --
                     and dsh.diff1_id                    is NULL
                     and dsh.diff2_id                    is NULL
                     and dsh.diff3_id                    is NULL
                     and dsh.diff4_id                    is NULL
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.sister_store
                     and dsh2.item_id(+)                 = tmp.tran_item
                     --
                     and dsh.diff1_id(+)                 is NULL
                     and dsh.diff2_id(+)                 is NULL
                     and dsh.diff3_id(+)                 is NULL
                     and dsh.diff4_id(+)                 is NULL) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.tran_item                  = use_this.tran_item)
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_REPLAN_ITEM;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_REPLAN_PDIFF(O_error_message      IN OUT VARCHAR2,
                                 I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                 I_alc_rule_row       IN     ALC_RULE%ROWTYPE,
                                 I_current_eow_date   IN     DATE,
                                 I_plan_sensitivity   IN     ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_REPLAN_PDIFF';

BEGIN

   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.source_item,
                 inner.source_diff1_id,
                 inner.source_diff2_id,
                 inner.source_diff3_id,
                 inner.source_diff4_id,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) OVER (PARTITION BY inner.alloc_id,
                                                                                                          inner.rule_many_to_one_id,
                                                                                                          inner.to_loc,
                                                                                                          inner.source_item,
                                                                                                          DECODE(LP_alloc_type,
                                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                                 inner.source_diff1_id),
                                                                                                          DECODE(LP_alloc_type,
                                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                                 inner.source_diff2_id),
                                                                                                          DECODE(LP_alloc_type,
                                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                                 inner.source_diff3_id),
                                                                                                          DECODE(LP_alloc_type,
                                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                                 inner.source_diff4_id)) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.source_item,
                         tmp.source_diff1_id,
                         tmp.source_diff2_id,
                         tmp.source_diff3_id,
                         tmp.source_diff4_id,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.source_item,
                                                         tmp.source_diff1_id,
                                                         tmp.source_diff2_id,
                                                         tmp.source_diff3_id,
                                                         tmp.source_diff4_id
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              tmp.source_diff1_id,
                                                                              tmp.source_diff2_id,
                                                                              tmp.source_diff3_id,
                                                                              tmp.source_diff4_id,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              tmp.source_diff1_id,
                                                                              tmp.source_diff2_id,
                                                                              tmp.source_diff3_id,
                                                                              tmp.source_diff4_id) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.source_item,
                                                                               tmp.source_diff1_id,
                                                                               tmp.source_diff2_id,
                                                                               tmp.source_diff3_id,
                                                                               tmp.source_diff4_id) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.source_item,
                                                                             tmp.source_diff1_id,
                                                                             tmp.source_diff2_id,
                                                                             tmp.source_diff3_id,
                                                                             tmp.source_diff4_id) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.loc                         = tmp.to_loc
                     and dsh.item_id                     = tmp.source_item
                     and NVL(tmp.source_diff1_id, '-1')  = NVL(dsh.diff1_id, '-1')
                     and NVL(tmp.source_diff2_id, '-1')  = NVL(dsh.diff2_id, '-1')
                     and NVL(tmp.source_diff3_id, '-1')  = NVL(dsh.diff3_id, '-1')
                     and NVL(tmp.source_diff4_id, '-1')  = NVL(dsh.diff4_id, '-1')
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.to_loc
                     and dsh2.item_id(+)                 = tmp.source_item
                     and NVL(tmp.source_diff1_id, '-1')  = NVL(dsh2.diff1_id(+), '-1')
                     and NVL(tmp.source_diff2_id, '-1')  = NVL(dsh2.diff2_id(+), '-1')
                     and NVL(tmp.source_diff3_id, '-1')  = NVL(dsh2.diff3_id(+), '-1')
                     and NVL(tmp.source_diff4_id, '-1')  = NVL(dsh2.diff4_id(+), '-1')) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.source_item                = use_this.source_item
       and NVL(target.source_diff1_id, '-1') = NVL(use_this.source_diff1_id, '-1')
       and NVL(target.source_diff2_id, '-1') = NVL(use_this.source_diff2_id, '-1')
       and NVL(target.source_diff3_id, '-1') = NVL(use_this.source_diff3_id, '-1')
       and NVL(target.source_diff4_id, '-1') = NVL(use_this.source_diff4_id, '-1'))
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select DISTINCT inner.alloc_id,
                 inner.rule_many_to_one_id,
                 inner.to_loc,
                 inner.source_item,
                 inner.source_diff1_id,
                 inner.source_diff2_id,
                 inner.source_diff3_id,
                 inner.source_diff4_id,
                 --
                 SUM(week_plan * (sales_hist/past_plan) * POWER((past_plan/all_plan),I_plan_sensitivity) +
                     week_plan * (1 - POWER((past_plan/all_plan),I_plan_sensitivity))) OVER (PARTITION BY inner.alloc_id,
                                                                                                          inner.rule_many_to_one_id,
                                                                                                          inner.to_loc,
                                                                                                          inner.source_item,
                                                                                                          DECODE(LP_alloc_type,
                                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                                 inner.source_diff1_id),
                                                                                                          DECODE(LP_alloc_type,
                                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                                 inner.source_diff2_id),
                                                                                                          DECODE(LP_alloc_type,
                                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                                 inner.source_diff3_id),
                                                                                                          DECODE(LP_alloc_type,
                                                                                                                 ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                                                 inner.source_diff4_id)) need
            from (select DISTINCT tmp.alloc_id,
                         tmp.rule_many_to_one_id,
                         tmp.to_loc,
                         tmp.source_item,
                         tmp.source_diff1_id,
                         tmp.source_diff2_id,
                         tmp.source_diff3_id,
                         tmp.source_diff4_id,
                         dsh.eow_date,
                         tmp.iwos_weeks,
                         --week
                         row_number() OVER (PARTITION BY tmp.alloc_id,
                                                         tmp.rule_many_to_one_id,
                                                         tmp.to_loc,
                                                         tmp.source_item,
                                                         tmp.source_diff1_id,
                                                         tmp.source_diff2_id,
                                                         tmp.source_diff3_id,
                                                         tmp.source_diff4_id
                                                ORDER BY dsh.eow_date) week_number,
                         --
                         --P(eow_date)
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              tmp.source_diff1_id,
                                                                              tmp.source_diff2_id,
                                                                              tmp.source_diff3_id,
                                                                              tmp.source_diff4_id,
                                                                              dsh.eow_date) week_plan,
                         --P''
                         SUM(dsh.qty * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              tmp.source_diff1_id,
                                                                              tmp.source_diff2_id,
                                                                              tmp.source_diff3_id,
                                                                              tmp.source_diff4_id) all_plan,
                         --P'
                         SUM(dsh2.qty * DECODE(tmp.weight,
                                               NULL, 1.0,
                                               tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                               tmp.rule_many_to_one_id,
                                                                               tmp.to_loc,
                                                                               tmp.source_item,
                                                                               tmp.source_diff1_id,
                                                                               tmp.source_diff2_id,
                                                                               tmp.source_diff3_id,
                                                                               tmp.source_diff4_id) past_plan,
                         --X'
                         SUM(DECODE(tmp.sales_hist_need,
                                    -1, 0,
                                    tmp.sales_hist_need)) OVER (PARTITION BY tmp.alloc_id,
                                                                             tmp.rule_many_to_one_id,
                                                                             tmp.to_loc,
                                                                             tmp.source_item,
                                                                             tmp.source_diff1_id,
                                                                             tmp.source_diff2_id,
                                                                             tmp.source_diff3_id,
                                                                             tmp.source_diff4_id) sales_hist
                         --
                    from alc_plan dsh,  --all
                         alc_plan dsh2, --past
                         alc_calc_need_temp tmp
                   where tmp.alloc_id                    = I_alloc_id
                     and LP_use_sister_store             = 'Y'
                     and tmp.rule_level                  = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                     and tmp.plan_reproject_need         = -1
                     --
                     and dsh.eow_date                    = tmp.eow_date
                     and dsh.loc                         = tmp.sister_store
                     and dsh.item_id                     = tmp.source_item
                     and NVL(tmp.source_diff1_id, '-1')  = NVL(dsh.diff1_id, '-1')
                     and NVL(tmp.source_diff2_id, '-1')  = NVL(dsh.diff2_id, '-1')
                     and NVL(tmp.source_diff3_id, '-1')  = NVL(dsh.diff3_id, '-1')
                     and NVL(tmp.source_diff4_id, '-1')  = NVL(dsh.diff4_id, '-1')
                     --
                     and dsh2.eow_date(+)                < I_current_eow_date
                     and dsh2.eow_date(+)                = tmp.eow_date
                     and dsh2.loc(+)                     = tmp.sister_store
                     and dsh2.item_id(+)                 = tmp.source_item
                     and NVL(tmp.source_diff1_id, '-1')  = NVL(dsh2.diff1_id(+), '-1')
                     and NVL(tmp.source_diff2_id, '-1')  = NVL(dsh2.diff2_id(+), '-1')
                     and NVL(tmp.source_diff3_id, '-1')  = NVL(dsh2.diff3_id(+), '-1')
                     and NVL(tmp.source_diff4_id, '-1')  = NVL(dsh2.diff4_id(+), '-1')) inner
           where inner.eow_date >= I_current_eow_date
             and inner.eow_date <= I_current_eow_date + DECODE(inner.iwos_weeks,
                                                               NULL, 0,
                                                               1,    0,
                                                               7 * (inner.iwos_weeks-1))) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.eow_date                   = I_current_eow_date
       and target.to_loc                     = use_this.to_loc
       and target.source_item                = use_this.source_item
       and NVL(target.source_diff1_id, '-1') = NVL(use_this.source_diff1_id, '-1')
       and NVL(target.source_diff2_id, '-1') = NVL(use_this.source_diff2_id, '-1')
       and NVL(target.source_diff3_id, '-1') = NVL(use_this.source_diff3_id, '-1')
       and NVL(target.source_diff4_id, '-1') = NVL(use_this.source_diff4_id, '-1'))
   when MATCHED then
      update
         set target.plan_reproject_need = use_this.need,
             target.sales_hist_need     = -1;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_REPLAN_PDIFF;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--PLAN FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN(O_error_message   IN OUT VARCHAR2,
                         I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                         I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_PLAN';

BEGIN

   if I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT then

      if SETUP_NEED_PLAN_DEPT(O_error_message,
                              I_alloc_id,
                              I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS then

      if SETUP_NEED_PLAN_CLASS(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS then

      if SETUP_NEED_PLAN_SUBCLASS(O_error_message,
                                  I_alloc_id,
                                  I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE then

      if SETUP_NEED_PLAN_PDIFF(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU then

      if SETUP_NEED_PLAN_ITEM(O_error_message,
                              I_alloc_id,
                              I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL then

      if SETUP_NEED_PLAN_STYLE(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_PLAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_DEPT(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_PLAN_DEPT';
   L_sysdate      DATE         := SYSDATE;
   L_current_eow  DATE         := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id    = I_alloc_id
             and tmp.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
             and dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.dept        = tmp.dept
             and dsh.class       is NULL
             and dsh.subclass    is NULL
             and dsh.item_id     is NULL
             and dsh.diff1_id    is NULL
             and dsh.diff2_id    is NULL
             and dsh.diff3_id    is NULL
             and dsh.diff4_id    is NULL
             and tmp.plan_need  = -1
             and dsh.eow_date   >= L_current_eow
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id     = I_alloc_id
             and LP_use_sister_store = 'Y'
             and tmp.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
             and dsh.eow_date        = tmp.eow_date
             and dsh.loc             = tmp.sister_store
             and dsh.dept            = tmp.dept
             and dsh.class           is NULL
             and dsh.subclass        is NULL
             and dsh.item_id         is NULL
             and dsh.diff1_id        is NULL
             and dsh.diff2_id        is NULL
             and dsh.diff3_id        is NULL
             and dsh.diff4_id        is NULL
             and dsh.eow_date        >= L_current_eow
             and tmp.plan_need       = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_PLAN_DEPT;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_CLASS(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_PLAN_CLASS';
   L_sysdate      DATE     := SYSDATE;
   L_current_eow  DATE     := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id    = I_alloc_id
             and tmp.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
             and dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.dept        = tmp.dept
             and dsh.class       = tmp.class
             and dsh.subclass    is NULL
             and dsh.item_id     is NULL
             and dsh.diff1_id    is NULL
             and dsh.diff2_id    is NULL
             and dsh.diff3_id    is NULL
             and dsh.diff4_id    is NULL
             and dsh.eow_date   >= L_current_eow
             and tmp.plan_need  = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id     = I_alloc_id
             and LP_use_sister_store   = 'Y'
             and tmp.rule_level   = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
             and dsh.eow_date     = tmp.eow_date
             and dsh.loc          = tmp.sister_store
             and dsh.dept         = tmp.dept
             and dsh.class        = tmp.class
             and dsh.subclass     is NULL
             and dsh.item_id      is NULL
             and dsh.diff1_id     is NULL
             and dsh.diff2_id     is NULL
             and dsh.diff3_id     is NULL
             and dsh.diff4_id     is NULL
             and dsh.eow_date   >= L_current_eow
             and tmp.plan_need   = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_PLAN_CLASS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_SUBCLASS(O_error_message   IN OUT VARCHAR2,
                                  I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                  I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_PLAN_SUBCLASS';
   L_sysdate      DATE         := SYSDATE;
   L_current_eow  DATE         := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.subclass,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id    = I_alloc_id
             and tmp.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
             and dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.dept        = tmp.dept
             and dsh.class       = tmp.class
             and dsh.subclass    = tmp.subclass
             and dsh.item_id     is NULL
             and dsh.diff1_id    is NULL
             and dsh.diff2_id    is NULL
             and dsh.diff3_id    is NULL
             and dsh.diff4_id    is NULL
             and dsh.eow_date    >= L_current_eow
             and tmp.plan_need   = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.subclass,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.to_loc              = use_this.to_loc
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.subclass            = use_this.subclass
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.subclass,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id     = I_alloc_id
             and LP_use_sister_store   = 'Y'
             and tmp.rule_level   = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
             and dsh.eow_date     = tmp.eow_date
             and dsh.loc          = tmp.sister_store
             and dsh.dept         = tmp.dept
             and dsh.class        = tmp.class
             and dsh.subclass     = tmp.subclass
             and dsh.item_id      is NULL
             and dsh.diff1_id     is NULL
             and dsh.diff2_id     is NULL
             and dsh.diff3_id     is NULL
             and dsh.diff4_id     is NULL
             and dsh.eow_date     >= L_current_eow
             and tmp.plan_need    = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.subclass,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.subclass            = use_this.subclass
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_PLAN_SUBCLASS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_PDIFF(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_PLAN_PDIFF';
   L_sysdate      DATE         := SYSDATE;
   L_current_eow  DATE         := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.source_diff1_id,
                 tmp.source_diff2_id,
                 tmp.source_diff3_id,
                 tmp.source_diff4_id,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff1_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff2_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff3_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff4_id),
                                                                              tmp.eow_date) sales
            from alc_plan dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.source_item,
                         t.source_diff1_id,
                         t.source_diff2_id,
                         t.source_diff3_id,
                         t.source_diff4_id,
                         t.eow_date,
                         t.weight
                    from alc_calc_need_temp t
                   where t.alloc_id   = I_alloc_id
                     and t.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                     and t.plan_need  = -1) tmp
           where dsh.eow_date                    = tmp.eow_date
             and dsh.eow_date                   >= L_current_eow
             and dsh.loc                         = tmp.to_loc
             and dsh.item_id                     = tmp.source_item
             and NVL(tmp.source_diff1_id, '-1')  = NVL(dsh.diff1_id, '-1')
             and NVL(tmp.source_diff2_id, '-1')  = NVL(dsh.diff2_id, '-1')
             and NVL(tmp.source_diff3_id, '-1')  = NVL(dsh.diff3_id, '-1')
             and NVL(tmp.source_diff4_id, '-1')  = NVL(dsh.diff4_id, '-1')) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.to_loc                      = use_this.to_loc
       and target.source_item                = use_this.source_item
       and NVL(target.source_diff1_id, '-1') = NVL(use_this.source_diff1_id, '-1')
       and NVL(target.source_diff2_id, '-1') = NVL(use_this.source_diff2_id, '-1')
       and NVL(target.source_diff3_id, '-1') = NVL(use_this.source_diff3_id, '-1')
       and NVL(target.source_diff4_id, '-1') = NVL(use_this.source_diff4_id, '-1')
       and target.eow_date                   = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.source_diff1_id,
                 tmp.source_diff2_id,
                 tmp.source_diff3_id,
                 tmp.source_diff4_id,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff1_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff2_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff3_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff4_id),
                                                                              tmp.eow_date) sales
            from alc_plan dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.sister_store,
                         t.source_item,
                         t.source_diff1_id,
                         t.source_diff2_id,
                         t.source_diff3_id,
                         t.source_diff4_id,
                         t.eow_date,
                         t.weight
                    from alc_calc_need_temp t
                   where t.alloc_id          = I_alloc_id
                     and LP_use_sister_store = 'Y'
                     and t.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                     and t.plan_need         = -1) tmp
           where dsh.eow_date                    = tmp.eow_date
             and dsh.eow_date                   >= L_current_eow
             and dsh.loc                         = tmp.sister_store
             and dsh.item_id                     = tmp.source_item
             and NVL(tmp.source_diff1_id, '-1')  = NVL(dsh.diff1_id, '-1')
             and NVL(tmp.source_diff2_id, '-1')  = NVL(dsh.diff2_id, '-1')
             and NVL(tmp.source_diff3_id, '-1')  = NVL(dsh.diff3_id, '-1')
             and NVL(tmp.source_diff4_id, '-1')  = NVL(dsh.diff4_id, '-1')) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.to_loc                     = use_this.to_loc
       and target.source_item                = use_this.source_item
       and NVL(target.source_diff1_id, '-1') = NVL(use_this.source_diff1_id, '-1')
       and NVL(target.source_diff2_id, '-1') = NVL(use_this.source_diff2_id, '-1')
       and NVL(target.source_diff3_id, '-1') = NVL(use_this.source_diff3_id, '-1')
       and NVL(target.source_diff4_id, '-1') = NVL(use_this.source_diff4_id, '-1')
       and target.eow_date                   = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_PLAN_PDIFF;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_STYLE(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_PLAN_STYLE';
   L_sysdate      DATE         := SYSDATE;
   L_current_eow  DATE         := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select DISTINCT tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_plan dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.source_item,
                         t.eow_date,
                         t.plan_need,
                         t.weight
                    from alc_calc_need_temp t
                   where t.alloc_id    = I_alloc_id
                     and t.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
                     and t.plan_need   = -1) tmp
             --
           where dsh.item_id     = tmp.source_item
             and dsh.eow_date    = tmp.eow_date
             and dsh.eow_date    >= L_current_eow
             and dsh.loc         = tmp.to_loc
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.source_item,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select DISTINCT tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_plan dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.sister_store,
                         t.source_item,
                         t.eow_date,
                         t.plan_need,
                         t.weight
                    from alc_calc_need_temp t
                   where t.alloc_id          = I_alloc_id
                     and LP_use_sister_store = 'Y'
                     and t.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
                     and t.plan_need         = -1) tmp
             --
           where dsh.item_id     = tmp.source_item
             and dsh.eow_date    = tmp.eow_date
             and dsh.eow_date    >= L_current_eow
             and dsh.loc         = tmp.sister_store
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.source_item,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_PLAN_STYLE;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_PLAN_ITEM(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                              I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_PLAN_ITEM';
   L_sysdate      DATE         := SYSDATE;
   L_current_eow  DATE         := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              tmp.tran_item,
                                                                              tmp.eow_date) sales
            from alc_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id    = I_alloc_id
             and tmp.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
             and dsh.item_id     = tmp.tran_item
             and dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.diff1_id    is NULL
             and dsh.diff2_id    is NULL
             and dsh.diff3_id    is NULL
             and dsh.diff4_id    is NULL
             and dsh.eow_date    >= L_current_eow
             and tmp.plan_need   = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                       NULL, 1.0,
                                       tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                       tmp.rule_many_to_one_id,
                                                                       tmp.to_loc,
                                                                       tmp.source_item,
                                                                       tmp.tran_item,
                                                                       tmp.eow_date) sales
            from alc_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id        = I_alloc_id
             and LP_use_sister_store = 'Y'
             and tmp.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
             and dsh.item_id         = tmp.tran_item
             and dsh.eow_date        = tmp.eow_date
             and dsh.loc             = tmp.sister_store
             and dsh.diff1_id        is NULL
             and dsh.diff2_id        is NULL
             and dsh.diff3_id        is NULL
             and dsh.diff4_id        is NULL
             and dsh.eow_date        >= L_current_eow
             and tmp.plan_need       = -1)use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_PLAN_ITEM;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--RECEIPT AND PLAN FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN(O_error_message   IN OUT VARCHAR2,
                                 I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                 I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_RECEIPT_PLAN';

BEGIN

   if I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT then

      if SETUP_NEED_RECEIPT_PLAN_DEPT(O_error_message,
                                      I_alloc_id,
                                      I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS then

      if SETUP_NEED_RECEIPT_PLAN_CLASS(O_error_message,
                                       I_alloc_id,
                                       I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS then

      if SETUP_NEED_RECEIPT_PLAN_SBC(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE then

      if SETUP_NEED_RECEIPT_PLAN_PDIFF(O_error_message,
                                       I_alloc_id,
                                       I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU then

      if SETUP_NEED_RECEIPT_PLAN_ITEM(O_error_message,
                                      I_alloc_id,
                                      I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL then

      if SETUP_NEED_RECEIPT_PLAN_STYLE(O_error_message,
                                       I_alloc_id,
                                       I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_RECEIPT_PLAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_DEPT(O_error_message   IN OUT VARCHAR2,
                                      I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61)     := 'ALC_CALC_SETUP_SQL.SETUP_NEED_RECEIPT_PLAN_DEPT';

   L_sysdate      DATE         := SYSDATE;
   L_current_eow  DATE         := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_receipt_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id          = I_alloc_id
             and tmp.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
             and dsh.eow_date          = tmp.eow_date
             and dsh.loc               = tmp.to_loc
             and dsh.dept              = tmp.dept
             and dsh.class             is NULL
             and dsh.subclass          is NULL
             and dsh.item              is NULL
             and dsh.diff1             is NULL
             and dsh.diff2             is NULL
             and dsh.diff3             is NULL
             and dsh.diff4             is NULL
             and dsh.eow_date          >= L_current_eow
             and tmp.receipt_plan_need  = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_receipt_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id        = I_alloc_id
             and LP_use_sister_store = 'Y'
             and tmp.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
             and dsh.eow_date        = tmp.eow_date
             and dsh.loc             = tmp.sister_store
             and dsh.dept            = tmp.dept
             and dsh.class           is NULL
             and dsh.subclass        is NULL
             and dsh.item            is NULL
             and dsh.diff1           is NULL
             and dsh.diff2           is NULL
             and dsh.diff3           is NULL
             and dsh.diff4           is NULL
             and dsh.eow_date           >= L_current_eow
             and tmp.receipt_plan_need  = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_RECEIPT_PLAN_DEPT;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_CLASS(O_error_message   IN OUT VARCHAR2,
                                       I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_RECEIPT_PLAN_CLASS';

   L_sysdate      DATE     := SYSDATE;
   L_current_eow  DATE     := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_receipt_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id    = I_alloc_id
             and tmp.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
             and dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.dept        = tmp.dept
             and dsh.class       = tmp.class
             and dsh.subclass    is NULL
             and dsh.item        is NULL
             and dsh.diff1       is NULL
             and dsh.diff2       is NULL
             and dsh.diff3       is NULL
             and dsh.diff4       is NULL
             and dsh.eow_date          >= L_current_eow
             and tmp.receipt_plan_need  = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.eow_date) use_this
   on (    target.alloc_id = use_this.alloc_id
       and target.rule_many_to_one_id  = use_this.rule_many_to_one_id
       and target.to_loc               = use_this.to_loc
       and target.dept                 = use_this.dept
       and target.class                = use_this.class
       and target.eow_date             = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.to_loc,
                 tmp.rule_many_to_one_id,
                 tmp.dept,
                 tmp.class,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_receipt_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id         = I_alloc_id
             and LP_use_sister_store  = 'Y'
             and tmp.rule_level   = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
             and dsh.eow_date     = tmp.eow_date
             and dsh.loc          = tmp.sister_store
             and dsh.dept         = tmp.dept
             and dsh.class        = tmp.class
             and dsh.subclass     is NULL
             and dsh.item         is NULL
             and dsh.diff1        is NULL
             and dsh.diff2        is NULL
             and dsh.diff3        is NULL
             and dsh.diff4        is NULL
             and dsh.eow_date           >= L_current_eow
             and tmp.receipt_plan_need   = -1
           group by tmp.alloc_id, tmp.rule_many_to_one_id, tmp.to_loc, tmp.dept, tmp.class, tmp.eow_date) use_this
   on (    target.alloc_id = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_RECEIPT_PLAN_CLASS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_SBC(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_RECEIPT_PLAN_SBC';

   L_sysdate      DATE     := SYSDATE;
   L_current_eow  DATE     := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.subclass,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_receipt_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id    = I_alloc_id
             and tmp.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
             and dsh.eow_date    = tmp.eow_date
             and dsh.loc         = tmp.to_loc
             and dsh.dept        = tmp.dept
             and dsh.class       = tmp.class
             and dsh.subclass    = tmp.subclass
             and dsh.item        is NULL
             and dsh.diff1       is NULL
             and dsh.diff2       is NULL
             and dsh.diff3       is NULL
             and dsh.diff4       is NULL
             and dsh.eow_date          >= L_current_eow
             and tmp.receipt_plan_need  = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.subclass,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.subclass            = use_this.subclass
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.subclass,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_receipt_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id     = I_alloc_id
             and LP_use_sister_store = 'Y'
             and tmp.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
             and dsh.eow_date        = tmp.eow_date
             and dsh.loc             = tmp.sister_store
             and dsh.dept            = tmp.dept
             and dsh.class           = tmp.class
             and dsh.subclass        = tmp.subclass
             and dsh.item            is NULL
             and dsh.diff1           is NULL
             and dsh.diff2           is NULL
             and dsh.diff3           is NULL
             and dsh.diff4           is NULL
             and dsh.eow_date          >= L_current_eow
             and tmp.receipt_plan_need = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.subclass,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.subclass            = use_this.subclass
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_RECEIPT_PLAN_SBC;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_PDIFF(O_error_message   IN OUT VARCHAR2,
                                       I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_RECEIPT_PLAN_PDIFF';

   L_sysdate      DATE     := SYSDATE;
   L_current_eow  DATE     := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.source_diff1_id,
                 tmp.source_diff2_id,
                 tmp.source_diff3_id,
                 tmp.source_diff4_id,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff1_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff2_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff3_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff4_id),
                                                                              tmp.eow_date) sales
            from alc_receipt_plan dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.source_item,
                         t.source_diff1_id,
                         t.source_diff2_id,
                         t.source_diff3_id,
                         t.source_diff4_id,
                         t.eow_date,
                         t.weight
                    from alc_calc_need_temp t
                   where t.alloc_id          = I_alloc_id
                     and t.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                     and t.receipt_plan_need = -1) tmp
           where dsh.eow_date                    = tmp.eow_date
             and dsh.eow_date                    >= L_current_eow
             and dsh.loc                         = tmp.to_loc
             and dsh.item                        = tmp.source_item
             and NVL(tmp.source_diff1_id, '-1')  = NVL(dsh.diff1, '-1')
             and NVL(tmp.source_diff2_id, '-1')  = NVL(dsh.diff2, '-1')
             and NVL(tmp.source_diff3_id, '-1')  = NVL(dsh.diff3, '-1')
             and NVL(tmp.source_diff4_id, '-1')  = NVL(dsh.diff4, '-1')) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.to_loc                     = use_this.to_loc
       and target.source_item                = use_this.source_item
       and NVL(target.source_diff1_id, '-1') = NVL(use_this.source_diff1_id, '-1')
       and NVL(target.source_diff2_id, '-1') = NVL(use_this.source_diff2_id, '-1')
       and NVL(target.source_diff3_id, '-1') = NVL(use_this.source_diff3_id, '-1')
       and NVL(target.source_diff4_id, '-1') = NVL(use_this.source_diff4_id, '-1')
       and target.eow_date                   = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.source_diff1_id,
                 tmp.source_diff2_id,
                 tmp.source_diff3_id,
                 tmp.source_diff4_id,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff1_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff2_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff3_id),
                                                                              DECODE(LP_alloc_type,
                                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                                     tmp.source_diff4_id),
                                                                              tmp.eow_date) sales
            from alc_receipt_plan dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.sister_store,
                         t.source_item,
                         t.source_diff1_id,
                         t.source_diff2_id,
                         t.source_diff3_id,
                         t.source_diff4_id,
                         t.eow_date,
                         t.weight
                    from alc_calc_need_temp t
                   where t.alloc_id          = I_alloc_id
                     and LP_use_sister_store = 'Y'
                     and t.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                     and t.receipt_plan_need = -1) tmp
           where dsh.eow_date                    = tmp.eow_date
             and dsh.eow_date                   >= L_current_eow
             and dsh.loc                         = tmp.sister_store
             and dsh.item                        = tmp.source_item
             and NVL(tmp.source_diff1_id, '-1')  = NVL(dsh.diff1, '-1')
             and NVL(tmp.source_diff2_id, '-1')  = NVL(dsh.diff2, '-1')
             and NVL(tmp.source_diff3_id, '-1')  = NVL(dsh.diff3, '-1')
             and NVL(tmp.source_diff4_id, '-1')  = NVL(dsh.diff4, '-1')) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.to_loc                      = use_this.to_loc
       and target.source_item                = use_this.source_item
       and NVL(target.source_diff1_id, '-1') = NVL(use_this.source_diff1_id, '-1')
       and NVL(target.source_diff2_id, '-1') = NVL(use_this.source_diff2_id, '-1')
       and NVL(target.source_diff3_id, '-1') = NVL(use_this.source_diff3_id, '-1')
       and NVL(target.source_diff4_id, '-1') = NVL(use_this.source_diff4_id, '-1')
       and target.eow_date                   = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_RECEIPT_PLAN_PDIFF;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_STYLE(O_error_message   IN OUT VARCHAR2,
                                       I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_RECEIPT_PLAN_STYLE';

   L_sysdate      DATE         := SYSDATE;
   L_current_eow  DATE         := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select DISTINCT tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_receipt_plan dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.source_item,
                         t.eow_date,
                         t.plan_need,
                         t.weight
                    from alc_calc_need_temp t
                   where t.alloc_id            = I_alloc_id
                     and t.rule_level          = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
                     and t.receipt_plan_need   = -1) tmp
             --
           where dsh.item        = tmp.source_item
             and dsh.eow_date    = tmp.eow_date
             and dsh.eow_date   >= L_current_eow
             and dsh.loc         = tmp.to_loc
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.source_item,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select DISTINCT tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) sales
            from alc_receipt_plan dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.sister_store,
                         t.source_item,
                         t.eow_date,
                         t.plan_need,
                         t.weight
                    from alc_calc_need_temp t
                   where t.alloc_id            = I_alloc_id
                     and LP_use_sister_store   = 'Y'
                     and t.rule_level          = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
                     and t.receipt_plan_need   = -1) tmp
             --
           where dsh.item        = tmp.source_item
             and dsh.eow_date    = tmp.eow_date
             and dsh.eow_date   >= L_current_eow
             and dsh.loc         = tmp.sister_store
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.source_item,
                    tmp.eow_date) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_RECEIPT_PLAN_STYLE;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_RECEIPT_PLAN_ITEM(O_error_message   IN OUT VARCHAR2,
                                      I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_RECEIPT_PLAN_ITEM';

   L_sysdate      DATE         := SYSDATE;
   L_current_eow  DATE         := SYSDATE;

BEGIN

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              tmp.tran_item,
                                                                              tmp.eow_date) sales
            from alc_receipt_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id           = I_alloc_id
             and tmp.rule_level         = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
             and dsh.item               = tmp.tran_item
             and dsh.eow_date           = tmp.eow_date
             and dsh.eow_date          >= L_current_eow
             and dsh.loc                = tmp.to_loc
             and tmp.receipt_plan_need  = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.tran_item,
                 tmp.eow_date,
                 SUM(NVL(dsh.qty,-1) * DECODE(tmp.weight,
                                              NULL, 1.0,
                                              tmp.weight)) OVER (PARTITION BY tmp.alloc_id,
                                                                              tmp.rule_many_to_one_id,
                                                                              tmp.to_loc,
                                                                              tmp.source_item,
                                                                              tmp.tran_item,
                                                                              tmp.eow_date) sales
            from alc_receipt_plan dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id          = I_alloc_id
             and LP_use_sister_store   = 'Y'
             and tmp.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
             and dsh.item              = tmp.tran_item
             and dsh.eow_date          = tmp.eow_date
             and dsh.eow_date         >= L_current_eow
             and dsh.loc               = tmp.sister_store
             and tmp.receipt_plan_need = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item
       and target.eow_date            = use_this.eow_date)
   when MATCHED then
      update
         set target.receipt_plan_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_RECEIPT_PLAN_ITEM;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--CORP RULES FUNCTIONS
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_CORP_RULES';

BEGIN

   if I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT then

      if SETUP_NEED_CORP_RULES_DEPT(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS then

      if SETUP_NEED_CORP_RULES_CLASS(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS then

      if SETUP_NEED_CORP_RULES_SBC(O_error_message,
                                   I_alloc_id,
                                   I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE then

      if SETUP_NEED_CORP_RULES_PDIFF(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU then

      if SETUP_NEED_CORP_RULES_ITEM(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL then

      if SETUP_NEED_CORP_RULES_STYLE(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_CORP_RULES;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_DEPT(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_CORP_RULES_DEPT';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 SUM(NVL(dsh.need_qty,-1)) sales
            from alc_corporate_rule_detail dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id          = I_alloc_id
             and tmp.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
             and dsh.corporate_rule_id = I_alc_rule_row.corporate_rule_id
             and dsh.location_id       = tmp.to_loc
             and dsh.dept              = tmp.dept
             and dsh.class             is NULL
             and dsh.subclass          is NULL
             and dsh.item_id           is NULL
             and tmp.corp_rule_need    = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept)
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 SUM(NVL(dsh.need_qty,-1)) sales
            from alc_corporate_rule_detail dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id          = I_alloc_id
             and LP_use_sister_store   = 'Y'
             and tmp.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
             and dsh.corporate_rule_id = I_alc_rule_row.corporate_rule_id
             and dsh.location_id       = tmp.sister_store
             and dsh.dept              = tmp.dept
             and dsh.class             is NULL
             and dsh.subclass          is NULL
             and dsh.item_id           is NULL
             and tmp.corp_rule_need    = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.to_loc              = use_this.to_loc
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.dept                = use_this.dept)
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_CORP_RULES_DEPT;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_CLASS(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_CORP_RULES_CLASS';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 SUM(NVL(dsh.need_qty,-1)) sales
            from alc_corporate_rule_detail dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id          = I_alloc_id
             and tmp.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
             and dsh.corporate_rule_id = I_alc_rule_row.corporate_rule_id
             and dsh.location_id       = tmp.to_loc
             and dsh.dept              = tmp.dept
             and dsh.class             = tmp.class
             and dsh.subclass          is NULL
             and dsh.item_id           is NULL
             and tmp.corp_rule_need        = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class)
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.to_loc,
                 tmp.rule_many_to_one_id,
                 tmp.dept,
                 tmp.class,
                 SUM(NVL(dsh.need_qty,-1)) sales
            from alc_corporate_rule_detail dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id          = I_alloc_id
             and LP_use_sister_store   = 'Y'
             and tmp.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
             and dsh.corporate_rule_id = I_alc_rule_row.corporate_rule_id
             and dsh.location_id       = tmp.sister_store
             and dsh.dept              = tmp.dept
             and dsh.class             = tmp.class
             and dsh.subclass          is NULL
             and dsh.item_id           is NULL
             and tmp.corp_rule_need   = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.to_loc              = use_this.to_loc
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.dept                = use_this.dept
       and target.class               = use_this.class)
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_CORP_RULES_CLASS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_SBC(O_error_message   IN OUT VARCHAR2,
                                   I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                   I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_CORP_RULES_SBC';

BEGIN

   merge into alc_calc_need_temp target
   using (select DISTINCT tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.subclass,
                 SUM(NVL(dsh.need_qty,-1)) sales
            from alc_corporate_rule_detail dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id          = I_alloc_id
             and tmp.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
             and dsh.corporate_rule_id = I_alc_rule_row.corporate_rule_id
             and dsh.location_id       = tmp.to_loc
             and dsh.dept              = tmp.dept
             and dsh.class             = tmp.class
             and dsh.subclass          = tmp.subclass
             and dsh.item_id           is NULL
             and tmp.corp_rule_need    = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.subclass) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.subclass            = use_this.subclass)
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select DISTINCT tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.dept,
                 tmp.class,
                 tmp.subclass,
                 SUM(NVL(dsh.need_qty,-1)) sales
            from alc_corporate_rule_detail dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id          = I_alloc_id
             and LP_use_sister_store   = 'Y'
             and tmp.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
             and dsh.corporate_rule_id = I_alc_rule_row.corporate_rule_id
             and dsh.location_id       = tmp.sister_store
             and dsh.dept              = tmp.dept
             and dsh.class             = tmp.class
             and dsh.subclass          = tmp.subclass
             and dsh.item_id           is NULL
             and tmp.corp_rule_need    = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.dept,
                    tmp.class,
                    tmp.subclass) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.dept                = use_this.dept
       and target.class               = use_this.class
       and target.subclass            = use_this.class)
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_CORP_RULES_SBC;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_PDIFF(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_CORP_RULES_PDIFF';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.source_diff1_id,
                 tmp.source_diff2_id,
                 tmp.source_diff3_id,
                 tmp.source_diff4_id,
                 MIN(NVL(dsh.need_qty,-1)) OVER (PARTITION BY tmp.alloc_id,
                                                              tmp.rule_many_to_one_id,
                                                              tmp.to_loc,
                                                              tmp.source_item,
                                                              DECODE(LP_alloc_type,
                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                     tmp.source_diff1_id),
                                                              DECODE(LP_alloc_type,
                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                     tmp.source_diff2_id),
                                                              DECODE(LP_alloc_type,
                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                     tmp.source_diff3_id),
                                                              DECODE(LP_alloc_type,
                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                     tmp.source_diff4_id)) sales
            from alc_corporate_rule_detail dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.source_item,
                         t.source_diff1_id,
                         t.source_diff2_id,
                         t.source_diff3_id,
                         t.source_diff4_id
                    from alc_calc_need_temp t
                   where t.alloc_id        = I_alloc_id
                     and t.rule_level      = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                     and t.corp_rule_need  = -1) tmp
           where dsh.corporate_rule_id           = I_alc_rule_row.corporate_rule_id
             and dsh.location_id                 = tmp.to_loc
             and dsh.item_id                     = tmp.source_item
             and NVL(tmp.source_diff1_id, '-1')  = NVL(dsh.diff1_id, '-1')
             and NVL(tmp.source_diff2_id, '-1')  = NVL(dsh.diff2_id, '-1')
             and NVL(tmp.source_diff3_id, '-1')  = NVL(dsh.diff3_id, '-1')
             and NVL(tmp.source_diff4_id, '-1')  = NVL(dsh.diff4_id, '-1')) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.to_loc                     = use_this.to_loc
       and target.source_item                = use_this.source_item
       and NVL(target.source_diff1_id, '-1') = NVL(use_this.source_diff1_id, '-1')
       and NVL(target.source_diff2_id, '-1') = NVL(use_this.source_diff2_id, '-1')
       and NVL(target.source_diff3_id, '-1') = NVL(use_this.source_diff3_id, '-1')
       and NVL(target.source_diff4_id, '-1') = NVL(use_this.source_diff4_id, '-1'))
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.source_diff1_id,
                 tmp.source_diff2_id,
                 tmp.source_diff3_id,
                 tmp.source_diff4_id,
                 MIN(NVL(dsh.need_qty,-1)) OVER (PARTITION BY tmp.alloc_id,
                                                              tmp.rule_many_to_one_id, tmp.to_loc, tmp.source_item,
                                                              DECODE(LP_alloc_type,
                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                     tmp.source_diff1_id),
                                                              DECODE(LP_alloc_type,
                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                     tmp.source_diff2_id),
                                                              DECODE(LP_alloc_type,
                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                     tmp.source_diff3_id),
                                                              DECODE(LP_alloc_type,
                                                                     ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION, '1',
                                                                     tmp.source_diff4_id)) sales
            from alc_corporate_rule_detail dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.sister_store,
                         t.source_item,
                         t.source_diff1_id,
                         t.source_diff2_id,
                         t.source_diff3_id,
                         t.source_diff4_id
                    from alc_calc_need_temp t
                   where t.alloc_id          = I_alloc_id
                     and LP_use_sister_store = 'Y'
                     and t.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                     and t.corp_rule_need    = -1) tmp
           where dsh.corporate_rule_id           = I_alc_rule_row.corporate_rule_id
             and dsh.location_id                 = tmp.sister_store
             and dsh.item_id                     = tmp.source_item
             and NVL(tmp.source_diff1_id, '-1')  = NVL(dsh.diff1_id, '-1')
             and NVL(tmp.source_diff2_id, '-1')  = NVL(dsh.diff2_id, '-1')
             and NVL(tmp.source_diff3_id, '-1')  = NVL(dsh.diff3_id, '-1')
             and NVL(tmp.source_diff4_id, '-1')  = NVL(dsh.diff4_id, '-1')) use_this
   on (    target.alloc_id                   = use_this.alloc_id
       and target.rule_many_to_one_id        = use_this.rule_many_to_one_id
       and target.to_loc                     = use_this.to_loc
       and target.source_item                = use_this.source_item
       and NVL(target.source_diff1_id, '-1') = NVL(use_this.source_diff1_id, '-1')
       and NVL(target.source_diff2_id, '-1') = NVL(use_this.source_diff2_id, '-1')
       and NVL(target.source_diff3_id, '-1') = NVL(use_this.source_diff3_id, '-1')
       and NVL(target.source_diff4_id, '-1') = NVL(use_this.source_diff4_id, '-1'))
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_CORP_RULES_PDIFF;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_STYLE(O_error_message   IN OUT VARCHAR2,
                                     I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                     I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_CORP_RULES_STYLE';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 SUM(NVL(dsh.need_qty,-1)) sales
            from alc_corporate_rule_detail dsh,
                 (select DISTINCT
                         t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.source_item,
                         t.corp_rule_need
                    from alc_calc_need_temp t
                   where t.alloc_id    = I_alloc_id
                     and t.rule_level  = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL) tmp
           where dsh.corporate_rule_id = I_alc_rule_row.corporate_rule_id
             and dsh.item_id           = tmp.source_item
             and dsh.location_id       = tmp.to_loc
             and tmp.corp_rule_need    = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.source_item) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item)
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 SUM(NVL(dsh.need_qty,-1)) sales
            from alc_corporate_rule_detail dsh,
                 (select DISTINCT t.alloc_id,
                         t.rule_many_to_one_id,
                         t.to_loc,
                         t.sister_store,
                         t.source_item,
                         t.corp_rule_need
                    from alc_calc_need_temp t
                   where t.alloc_id          = I_alloc_id
                     and LP_use_sister_store = 'Y'
                     and t.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL) tmp
           where dsh.corporate_rule_id       = I_alc_rule_row.corporate_rule_id
             and dsh.item_id                 = tmp.source_item
             and dsh.location_id             = tmp.sister_store
             and tmp.corp_rule_need          = -1
           group by tmp.alloc_id,
                    tmp.rule_many_to_one_id,
                    tmp.to_loc,
                    tmp.source_item) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item)
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_CORP_RULES_STYLE;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_CORP_RULES_ITEM(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                    I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_CORP_RULES_ITEM';

BEGIN

   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.tran_item,
                 SUM(NVL(dsh.need_qty,-1)) OVER (PARTITION BY tmp.alloc_id,
                                                              tmp.rule_many_to_one_id,
                                                              tmp.to_loc,
                                                              tmp.source_item,
                                                              tmp.tran_item) sales
            from alc_corporate_rule_detail dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id          = I_alloc_id
             and tmp.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
             and dsh.corporate_rule_id = I_alc_rule_row.corporate_rule_id
             and dsh.item_id           = tmp.tran_item
             and dsh.location_id       = tmp.to_loc
             and tmp.corp_rule_need    = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item)
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;

   if LP_use_sister_store = 'Y' then
   merge into alc_calc_need_temp target
   using (select tmp.alloc_id,
                 tmp.rule_many_to_one_id,
                 tmp.to_loc,
                 tmp.source_item,
                 tmp.tran_item,
                 SUM(NVL(dsh.need_qty,-1)) OVER (PARTITION BY tmp.alloc_id,
                                                              tmp.rule_many_to_one_id,
                                                              tmp.to_loc,
                                                              tmp.source_item,
                         tmp.tran_item) sales
            from alc_corporate_rule_detail dsh,
                 alc_calc_need_temp tmp
           where tmp.alloc_id          = I_alloc_id
             and tmp.rule_level        = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU
             and LP_use_sister_store   = 'Y'
             and dsh.corporate_rule_id = I_alc_rule_row.corporate_rule_id
             and dsh.item_id           = tmp.tran_item
             and dsh.location_id       = tmp.sister_store
             and tmp.corp_rule_need    = -1) use_this
   on (    target.alloc_id            = use_this.alloc_id
       and target.rule_many_to_one_id = use_this.rule_many_to_one_id
       and target.to_loc              = use_this.to_loc
       and target.source_item         = use_this.source_item
       and target.tran_item           = use_this.tran_item)
   when MATCHED then
      update
         set target.corp_rule_need = use_this.sales;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_CORP_RULES_ITEM;
-------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_NEED_ALT_HIER(O_error_message   IN OUT VARCHAR2,
                             I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                             I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_NEED_ALT_HIER';
   L_current_eow_date      DATE;
   L_plan_sensitivity      ALC_SYSTEM_OPTIONS.FP_PLAN_SENSITIVITY%TYPE := NULL;

BEGIN

   if SEED_NEED_ALT_HIER_GTT(O_error_message,
                             I_alloc_id,
                             I_alc_rule_row) = FALSE then
      return FALSE;
   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_HISTORY or
      I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_HISTORY_AND_PLAN or
      I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_PLAN_REPROJECT then

      if SETUP_NEED_SALES_HIST_DEPT(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_SALES_HIST_CLASS(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_SALES_HIST_SUBCLASS(O_error_message,
                                        I_alloc_id,
                                        I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_SALES_HIST_STYLE(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_SALES_HIST_PDIFF(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_SALES_HIST_TRAN(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_FORECAST then

      if SETUP_NEED_FCST_DEPT(O_error_message,
                              I_alloc_id,
                              I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_FCST_CLASS(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_FCST_SUBCLASS(O_error_message,
                                  I_alloc_id,
                                  I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_FCST_TRAN(O_error_message,
                              I_alloc_id,
                              I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_FCST_STYLE(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_FCST_PDIFF(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_PLAN_REPROJECT then

      select fp_plan_sensitivity
        into L_plan_sensitivity
        from alc_system_options;

      if GET_ALLOC_EOW_DATE(O_error_message,
                            L_current_eow_date,
                            SYSDATE) = FALSE then
         return FALSE;
      end if;

      if SETUP_IWOS(O_error_message,
                    I_alloc_id,
                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

      if SETUP_NEED_REPLAN_DEPT(O_error_message,
                                I_alloc_id,
                                I_alc_rule_row,
                                L_current_eow_date,
                                L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_REPLAN_CLASS(O_error_message,
                                 I_alloc_id,
                                 I_alc_rule_row,
                                 L_current_eow_date,
                                 L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_REPLAN_SUBCLASS(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row,
                                    L_current_eow_date,
                                    L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_REPLAN_ITEM(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row,
                                    L_current_eow_date,
                                    L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_REPLAN_STYLE(O_error_message,
                                 I_alloc_id,
                                 I_alc_rule_row,
                                 L_current_eow_date,
                                 L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_REPLAN_PDIFF(O_error_message,
                                 I_alloc_id,
                                 I_alc_rule_row,
                                 L_current_eow_date,
                                 L_plan_sensitivity) = FALSE then
         return FALSE;
      end if;

   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_PLAN or
      I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_HISTORY_AND_PLAN then

      if SETUP_NEED_PLAN_DEPT(O_error_message,
                              I_alloc_id,
                              I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_PLAN_CLASS(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_PLAN_SUBCLASS(O_error_message,
                                  I_alloc_id,
                                  I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_PLAN_ITEM(O_error_message,
                              I_alloc_id,
                              I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_PLAN_STYLE(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_PLAN_PDIFF(O_error_message,
                               I_alloc_id,
                               I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_RECIEPT_AND_PLAN then

      if SETUP_NEED_RECEIPT_PLAN_DEPT(O_error_message,
                                      I_alloc_id,
                                      I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_RECEIPT_PLAN_CLASS(O_error_message,
                                       I_alloc_id,
                                       I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_RECEIPT_PLAN_SBC(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_RECEIPT_PLAN_ITEM(O_error_message,
                                      I_alloc_id,
                                      I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_RECEIPT_PLAN_STYLE(O_error_message,
                                       I_alloc_id,
                                       I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_RECEIPT_PLAN_PDIFF(O_error_message,
                                       I_alloc_id,
                                       I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   if I_alc_rule_row.rule_type = ALC_CONSTANTS_SQL.RULE_TYPE_CORPORATE_RULES then

      if SETUP_NEED_CORP_RULES_DEPT(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_CORP_RULES_CLASS(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_CORP_RULES_SBC(O_error_message,
                                   I_alloc_id,
                                   I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_CORP_RULES_STYLE(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_CORP_RULES_ITEM(O_error_message,
                                    I_alloc_id,
                                    I_alc_rule_row) = FALSE then
         return FALSE;
      end if;
      --
      if SETUP_NEED_CORP_RULES_PDIFF(O_error_message,
                                     I_alloc_id,
                                     I_alc_rule_row) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_NEED_ALT_HIER;
-------------------------------------------------------------------------------------------------------------
FUNCTION SEED_NEED_ALT_HIER_GTT(O_error_message   IN OUT VARCHAR2,
                                I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SEED_NEED_ALT_HIER_GTT';

BEGIN

   --hierarchy level alt hiers
   insert into alc_calc_need_temp (alloc_id,
                                   rule_many_to_one_id,
                                   rule_level,
                                   dept,
                                   class,
                                   subclass,
                                   source_item,
                                   source_item_level,
                                   source_tran_level,
                                   source_pack_ind,
                                   source_diff1_id,
                                   source_diff2_id,
                                   source_diff3_id,
                                   source_diff4_id,
                                   tran_item,
                                   tran_item_level,
                                   tran_tran_level,
                                   tran_pack_ind,
                                   tran_diff1_id,
                                   tran_diff2_id,
                                   tran_diff3_id,
                                   tran_diff4_id,
                                   pack_qty,
                                   to_loc,
                                   sister_store,
                                   size_profile_qty,
                                   total_profile_qty,
                                   eow_date,
                                   weight,
                                   sales_hist_need,
                                   forecast_need,
                                   replan_need,
                                   plan_need,
                                   plan_reproject_need,
                                   receipt_plan_need,
                                   corp_rule_need)
                    with items as (--dept level alt hier
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT as rule_level,
                                          TO_NUMBER(ah.dept) as dept,
                                          NULL as class,
                                          NULL as subclass,
                                          NULL as source_item,
                                          NULL as source_item_level,
                                          NULL as source_tran_level,
                                          NULL as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          NULL as tran_item,
                                          NULL as tran_item_level,
                                          NULL as tran_tran_level,
                                          NULL as tran_pack_ind,
                                          NULL as tran_diff1_id,
                                          NULL as tran_diff2_id,
                                          NULL as tran_diff3_id,
                                          NULL as tran_diff4_id,
                                          1 as pack_qty
                                     from alc_rule_many_to_one ah
                                    where ah.alloc_id   = I_alloc_id
                                     --replace with a type column
                                      and ah.dept       is NOT NULL
                                      and ah.class      is NULL
                                      and ah.subclass   is NULL
                                   union all
                                   --class level alt hier
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS as rule_level,
                                          TO_NUMBER(ah.dept) as dept,
                                          TO_NUMBER(ah.class) as class,
                                          NULL as subclass,
                                          NULL as source_item,
                                          NULL as source_item_level,
                                          NULL as source_tran_level,
                                          NULL as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          NULL as tran_item,
                                          NULL as tran_item_level,
                                          NULL as tran_tran_level,
                                          NULL as tran_pack_ind,
                                          NULL as tran_diff1_id,
                                          NULL as tran_diff2_id,
                                          NULL as tran_diff3_id,
                                          NULL as tran_diff4_id,
                                          1 as pack_qty
                                     from alc_rule_many_to_one ah
                                    where ah.alloc_id   = I_alloc_id
                                     --replace with a type column
                                      and ah.dept       is NOT NULL
                                      and ah.class      is NOT NULL
                                      and ah.subclass   is NULL
                                   union all
                                   --subclass level alt hier
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS as rule_level,
                                          TO_NUMBER(ah.dept) as dept,
                                          TO_NUMBER(ah.class) as class,
                                          TO_NUMBER(ah.subclass) as subclass,
                                          NULL as source_item,
                                          NULL as source_item_level,
                                          NULL as source_tran_level,
                                          NULL as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          NULL as tran_item,
                                          NULL as tran_item_level,
                                          NULL as tran_tran_level,
                                          NULL as tran_pack_ind,
                                          NULL as tran_diff1_id,
                                          NULL as tran_diff2_id,
                                          NULL as tran_diff3_id,
                                          NULL as tran_diff4_id,
                                          1 as pack_qty
                                     from alc_rule_many_to_one ah
                                    where ah.alloc_id   = I_alloc_id
                                     --replace with a type column
                                      and ah.dept       is NOT NULL
                                      and ah.class      is NOT NULL
                                      and ah.subclass   is NOT NULL)
                                   select DISTINCT items.alloc_id,
                                          items.rule_many_to_one_id,
                                          items.rule_level,
                                          items.dept,
                                          items.class,
                                          items.subclass,
                                          items.source_item,
                                          items.source_item_level,
                                          items.source_tran_level,
                                          items.source_pack_ind,
                                          items.source_diff1_id,
                                          items.source_diff2_id,
                                          items.source_diff3_id,
                                          items.source_diff4_id,
                                          items.tran_item,
                                          items.tran_item_level,
                                          items.tran_tran_level,
                                          items.tran_pack_ind,
                                          items.tran_diff1_id,
                                          items.tran_diff2_id,
                                          items.tran_diff3_id,
                                          items.tran_diff4_id,
                                          items.pack_qty,
                                          l.loc,
                                          l.sister_store,
                                          NULL as size_profile_qty,
                                          NULL as total_profile_qty,
                                          d.eow_date as eow_date,
                                          d.weight as weight,
                                          -1 as sales_hist_need,
                                          -1 as forecast_need,
                                          -1 as replan_need,
                                          -1 as plan_need,
                                          -1 as plan_reproject_need,
                                          -1 as receipt_plan_need,
                                          -1 as corp_rule_need
                                     from items,
                                          alc_loc_group alg,
                                          alc_location al,
                                          (select s.store loc,
                                                  s.sister_store
                                             from store s
                                           union all
                                           select w.wh loc,
                                                  NULL sister_store
                                             from wh w) l,
                                          alc_calc_need_dates_temp d
                                    where alg.alloc_id              = I_alloc_id
                                      and al.loc_group_id           = alg.loc_group_id
                                      and l.loc                     = al.location_id
                                      and items.alloc_id            = d.alloc_id
                                      and items.rule_many_to_one_id = d.rule_many_to_one_id;

   --non-hierarchy level alt hiers
   insert into alc_calc_need_temp (alloc_id,
                                   rule_many_to_one_id,
                                   rule_level,
                                   dept,
                                   class,
                                   subclass,
                                   source_item,
                                   source_item_level,
                                   source_tran_level,
                                   source_pack_ind,
                                   source_diff1_id,
                                   source_diff2_id,
                                   source_diff3_id,
                                   source_diff4_id,
                                   tran_item,
                                   tran_item_level,
                                   tran_tran_level,
                                   tran_pack_ind,
                                   tran_diff1_id,
                                   tran_diff2_id,
                                   tran_diff3_id,
                                   tran_diff4_id,
                                   pack_qty,
                                   to_loc,
                                   sister_store,
                                   size_profile_qty,
                                   total_profile_qty,
                                   eow_date,
                                   weight,
                                   sales_hist_need,
                                   forecast_need,
                                   replan_need,
                                   plan_need,
                                   plan_reproject_need,
                                   receipt_plan_need,
                                   corp_rule_need)
                    with items as (--non-pack item
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_SKU as rule_level,
                                          im.dept,
                                          im.class,
                                          im.subclass,
                                          NVL(im.item_parent, im.item) as source_item,
                                          DECODE(im.item_parent,
                                                 NULL, im.tran_level,
                                                 im.tran_level-1) as source_item_level,
                                          im.tran_level as source_tran_level,
                                          im.pack_ind as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          im.item as tran_item,
                                          im.item_level as tran_item_level,
                                          im.tran_level as tran_tran_level,
                                          im.pack_ind as tran_pack_ind,
                                          im.diff_1 as tran_diff1_id,
                                          im.diff_2 as tran_diff2_id,
                                          im.diff_3 as tran_diff3_id,
                                          im.diff_4 as tran_diff4_id,
                                          1 as pack_qty
                                     from alc_rule_many_to_one ah,
                                          item_master im
                                    where ah.alloc_id   = I_alloc_id
                                     --replace with a type column
                                      and ah.item_id    is NOT NULL
                                      and ah.diff1_id   is NULL
                                      and ah.item_id    = im.item
                                      and im.status     = 'A'
                                      and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and im.sellable_ind = 'Y'))
                                      and im.item_level = im.tran_level
                                   union all
                                   --non-sellable pack item explode to components
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_SKU as rule_level,
                                          im.dept,
                                          im.class,
                                          im.subclass,
                                          pim.item as source_item,
                                          pim.item_level as source_item_level,
                                          pim.tran_level as source_tran_level,
                                          pim.pack_ind as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          im.item as tran_item,
                                          im.item_level as tran_item_level,
                                          im.tran_level as tran_tran_level,
                                          im.pack_ind as tran_pack_ind,
                                          im.diff_1 as tran_diff1_id,
                                          im.diff_2 as tran_diff2_id,
                                          im.diff_3 as tran_diff3_id,
                                          im.diff_4 as tran_diff4_id,
                                          vp.qty as pack_qty
                                     from alc_rule_many_to_one ah,
                                          v_packsku_qty vp,
                                          item_master pim,
                                          item_master im
                                    where ah.alloc_id   = I_alloc_id
                                     --replace with a type column
                                      and ah.item_id       is NOT NULL
                                      and ah.diff1_id      is NULL
                                      and ah.item_id       = vp.pack_no
                                      and vp.pack_no       = pim.item
                                      and pim.sellable_ind = 'N'
                                      and vp.item          = im.item
                                      and im.status        = 'A'
                                      and im.pack_ind      = 'N'
                                      and im.item_level    = im.tran_level
                                   union all
                                   --style explode to children
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL as rule_level,
                                          im.dept,
                                          im.class,
                                          im.subclass,
                                          im.item_parent as source_item,
                                          im.item_level - 1 as source_item_level,
                                          im.tran_level as source_tran_level,
                                          'N' as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          im.item as tran_item,
                                          im.item_level as tran_item_level,
                                          im.tran_level as tran_tran_level,
                                          im.pack_ind as tran_pack_ind,
                                          im.diff_1 as tran_diff1_id,
                                          im.diff_2 as tran_diff2_id,
                                          im.diff_3 as tran_diff3_id,
                                          im.diff_4 as tran_diff4_id,
                                          1 as pack_qty
                                     from alc_rule_many_to_one ah,
                                          item_master im
                                    where ah.alloc_id   = I_alloc_id
                                     --replace with a type column
                                      and ah.item_id    is NOT NULL
                                      and ah.diff1_id   is NULL
                                      and ah.item_id    = im.item_parent
                                      and im.status     = 'A'
                                      and im.item_level = im.tran_level
                                   UNION ALL
                                    --style color
                                   select inner.alloc_id,
                                          inner.many_to_one_id as rule_many_to_one_id,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE as rule_level,
                                          im.dept as dept,
                                          im.class as class,
                                          im.subclass as subclass,
                                          im.item_parent as source_item,
                                          im.item_level-1 as source_item_level,
                                          im.tran_level as source_tran_level,
                                          'N' as source_pack_ind,
                                          inner.diff_1 as source_diff1_id,
                                          inner.diff_2 as source_diff2_id,
                                          inner.diff_3 as source_diff3_id,
                                          inner.diff_4 as source_diff4_id,
                                          im.item as tran_item,
                                          im.item_level as tran_item_level,
                                          im.tran_level as tran_tran_level,
                                          im.pack_ind as tran_pack_ind,
                                          im.diff_1 as tran_diff1_id,
                                          im.diff_2 as tran_diff2_id,
                                          im.diff_3 as tran_diff3_id,
                                          im.diff_4 as tran_diff4_id,
                                          1 as pack_qty
                                     from item_master im,
                                          (select DISTINCT alloc_id,
                                                  many_to_one_id,
                                                  item_id,
                                                  DECODE(diff_pos, 1,
                                                         TRIM( SUBSTR ( txt
                                                                      , INSTR (txt, ',', 1, level ) + 1
                                                                      , INSTR (txt, ',', 1, level+1) - INSTR (txt, ',', 1, level) -1
                                                                      )),
                                                         NULL) diff_1,
                                                  DECODE(diff_pos, 2,
                                                         TRIM( SUBSTR ( txt
                                                                      , INSTR (txt, ',', 1, level ) + 1
                                                                      , INSTR (txt, ',', 1, level+1) - INSTR (txt, ',', 1, level) -1
                                                                      )),
                                                         NULL) diff_2,
                                                  DECODE(diff_pos, 3,
                                                         TRIM( SUBSTR ( txt
                                                                      , INSTR (txt, ',', 1, level ) + 1
                                                                      , INSTR (txt, ',', 1, level+1) - INSTR (txt, ',', 1, level) -1
                                                                      )),
                                                         NULL) diff_3,
                                                  DECODE(diff_pos, 4,
                                                         TRIM( SUBSTR ( txt
                                                                      , INSTR (txt, ',', 1, level ) + 1
                                                                      , INSTR (txt, ',', 1, level+1) - INSTR (txt, ',', 1, level) -1
                                                                      )),
                                                         NULL) diff_4
                                             from (select alloc_id,
                                                          many_to_one_id,
                                                          item_id,
                                                          SUBSTR(diff1_id,1,1) diff_pos,
                                                          ','||SUBSTR(diff1_id,3)||',' txt
                                                     from alc_rule_many_to_one
                                                    where alloc_id = I_alloc_id
                                                      and diff1_id is NOT NULL)
                                           connect by level <= LENGTH(txt)-LENGTH(REPLACE(txt,',',''))-1) inner
                                    where im.item_parent = inner.item_id
                                      and NVL(im.diff_1,'--')      = NVL(inner.diff_1, NVL(im.diff_1,'--'))
                                      and NVL(im.diff_2,'--')      = NVL(inner.diff_2, NVL(im.diff_2,'--'))
                                      and NVL(im.diff_3,'--')      = NVL(inner.diff_3, NVL(im.diff_3,'--'))
                                      and NVL(im.diff_4,'--')      = NVL(inner.diff_4, NVL(im.diff_4,'--'))
                                   UNION ALL
                                   --skulist SKU - sellable pack
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          --ALC_CONSTANTS_SQL.USER_RULE_LEVEL_ITEMLIST as rule_level,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_SKU as rule_level,
                                          im.dept,
                                          im.class,
                                          im.subclass,
                                          im.item as source_item,
                                          im.item_level as source_item_level,
                                          im.tran_level as source_tran_level,
                                          im.pack_ind as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          im.item as tran_item,
                                          im.item_level as tran_item_level,
                                          im.tran_level as tran_tran_level,
                                          im.pack_ind as tran_pack_ind,
                                          im.diff_1 as tran_diff1_id,
                                          im.diff_2 as tran_diff2_id,
                                          im.diff_3 as tran_diff3_id,
                                          im.diff_4 as tran_diff4_id,
                                          1 as pack_qty
                                     from alc_rule_many_to_one ah,
                                          skulist_detail sd,
                                          item_master im
                                    where ah.alloc_id   = I_alloc_id
                                      and ah.itemlist_id  = sd.skulist
                                      and sd.item_level   = sd.tran_level
                                      and sd.item         = im.item
                                      and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and im.sellable_ind = 'Y'))
                                      and im.status = 'A'
                                   UNION ALL
                                   --skulist non-sellable PACK
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          --ALC_CONSTANTS_SQL.USER_RULE_LEVEL_ITEMLIST as rule_level,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_SKU as rule_level,
                                          im.dept,
                                          im.class,
                                          im.subclass,
                                          pim.item as source_item,
                                          pim.item_level as source_item_level,
                                          pim.tran_level as source_tran_level,
                                          pim.pack_ind as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          im.item as tran_item,
                                          im.item_level as tran_item_level,
                                          im.tran_level as tran_tran_level,
                                          im.pack_ind as tran_pack_ind,
                                          im.diff_1 as tran_diff1_id,
                                          im.diff_2 as tran_diff2_id,
                                          im.diff_3 as tran_diff3_id,
                                          im.diff_4 as tran_diff4_id,
                                          vp.qty as pack_qty
                                     from alc_rule_many_to_one ah,
                                          skulist_detail sd,
                                          v_packsku_qty vp,
                                          item_master pim,
                                          item_master im
                                    where ah.alloc_id      = I_alloc_id
                                      and ah.itemlist_id   = sd.skulist
                                      and sd.item_level    = sd.tran_level
                                      and sd.pack_ind      = 'Y'
                                      and sd.item          = vp.pack_no
                                      and vp.pack_no       = pim.item
                                      and pim.sellable_ind = 'N'
                                      and vp.item          = im.item
                                      and im.status        = 'A'
                                      and im.pack_ind      = 'N'
                                      and im.item_level    = im.tran_level
                                   UNION ALL
                                   --skulist PARENT
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          --ALC_CONSTANTS_SQL.USER_RULE_LEVEL_ITEMLIST as rule_level,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL as rule_level,
                                          im.dept,
                                          im.class,
                                          im.subclass,
                                          im.item_parent as source_item,
                                          im.item_level - 1 as source_item_level,
                                          im.tran_level as source_tran_level,
                                          'N' as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          im.item as tran_item,
                                          im.item_level as tran_item_level,
                                          im.tran_level as tran_tran_level,
                                          im.pack_ind as tran_pack_ind,
                                          im.diff_1 as tran_diff1_id,
                                          im.diff_2 as tran_diff2_id,
                                          im.diff_3 as tran_diff3_id,
                                          im.diff_4 as tran_diff4_id,
                                          1 as pack_qty
                                     from alc_rule_many_to_one ah,
                                          skulist_detail sd,
                                          item_master im
                                    where ah.alloc_id   = I_alloc_id
                                      and ah.itemlist_id = sd.skulist
                                      and sd.item_level  < sd.tran_level
                                      and sd.pack_ind    = 'N'
                                      and (sd.item = im.item_parent or sd.item = im.item_grandparent)
                                      and im.status      = 'A'
                                      and im.item_level  = im.tran_level
                                   UNION ALL
                                   --uda PARENT
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          --ALC_CONSTANTS_SQL.USER_RULE_LEVEL_UDA as rule_level,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL as rule_level,
                                          im.dept,
                                          im.class,
                                          im.subclass,
                                          im.item_parent as source_item,
                                          im.item_level - 1 as source_item_level,
                                          im.tran_level as source_tran_level,
                                          'N' as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          im.item as tran_item,
                                          im.item_level as tran_item_level,
                                          im.tran_level as tran_tran_level,
                                          im.pack_ind as tran_pack_ind,
                                          im.diff_1 as tran_diff1_id,
                                          im.diff_2 as tran_diff2_id,
                                          im.diff_3 as tran_diff3_id,
                                          im.diff_4 as tran_diff4_id,
                                          1 as pack_qty
                                     from alc_rule_many_to_one ah,
                                          uda_item_lov ul,
                                          item_master im
                                    where ah.alloc_id   = I_alloc_id
                                      and ah.uda_id                          = ul.uda_id
                                      and NVL(ah.uda_value_id, ul.uda_value) = ul.uda_value
                                      and (ul.item = im.item_parent or ul.item = im.item_grandparent)
                                      and im.status                           = 'A'
                                      and im.pack_ind                         = 'N'
                                      and im.item_level                       = im.tran_level
                                   union all
                                   --uda SKU - sellable pack
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          --ALC_CONSTANTS_SQL.USER_RULE_LEVEL_UDA as rule_level,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_SKU as rule_level,
                                          im.dept,
                                          im.class,
                                          im.subclass,
                                          NVL(im.item_parent, im.item) as source_item,
                                          DECODE(im.item_parent,
                                                 NULL, im.tran_level,
                                                 im.tran_level-1) as source_item_level,
                                          im.tran_level as source_tran_level,
                                          im.pack_ind as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          im.item as tran_item,
                                          im.item_level as tran_item_level,
                                          im.tran_level as tran_tran_level,
                                          im.pack_ind as tran_pack_ind,
                                          im.diff_1 as tran_diff1_id,
                                          im.diff_2 as tran_diff2_id,
                                          im.diff_3 as tran_diff3_id,
                                          im.diff_4 as tran_diff4_id,
                                          1 as pack_qty
                                     from alc_rule_many_to_one ah,
                                          uda_item_lov ul,
                                          item_master im
                                    where ah.alloc_id    = I_alloc_id
                                      and ah.uda_id      = ul.uda_id
                                      and NVL(ah.uda_value_id, ul.uda_value) = ul.uda_value
                                      and ul.item        = im.item
                                      and im.status      = 'A'
                                      and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and im.sellable_ind = 'Y'))
                                      and im.item_level  = im.tran_level
                                   union all
                                   --uda non-sellable PACK
                                   select ah.alloc_id,
                                          ah.many_to_one_id as rule_many_to_one_id,
                                          --ALC_CONSTANTS_SQL.USER_RULE_LEVEL_UDA as rule_level,
                                          ALC_CONSTANTS_SQL.RULE_LEVEL_SKU as rule_level,
                                          im.dept,
                                          im.class,
                                          im.subclass,
                                          pim.item as source_item,
                                          pim.item_level as source_item_level,
                                          pim.tran_level as source_tran_level,
                                          pim.pack_ind as source_pack_ind,
                                          NULL as source_diff1_id,
                                          NULL as source_diff2_id,
                                          NULL as source_diff3_id,
                                          NULL as source_diff4_id,
                                          im.item as tran_item,
                                          im.item_level as tran_item_level,
                                          im.tran_level as tran_tran_level,
                                          im.pack_ind as tran_pack_ind,
                                          im.diff_1 as tran_diff1_id,
                                          im.diff_2 as tran_diff2_id,
                                          im.diff_3 as tran_diff3_id,
                                          im.diff_4 as tran_diff4_id,
                                          1 as pack_qty
                                     from alc_rule_many_to_one ah,
                                          v_packsku_qty vq,
                                          uda_item_lov ul,
                                          item_master pim,
                                          item_master im
                                    where ah.alloc_id      = I_alloc_id
                                      and ah.uda_id        = ul.uda_id
                                      and NVL(ah.uda_value_id, ul.uda_value) = ul.uda_value
                                      and ul.item          = vq.pack_no
                                      and vq.pack_no       = pim.item
                                      and pim.sellable_ind = 'N'
                                      and vq.item          = im.item
                                      and im.status        = 'A'
                                      and im.pack_ind      = 'N'
                                      and im.item_level    = im.tran_level)
                                   select DISTINCT items.alloc_id,
                                          items.rule_many_to_one_id,
                                          items.rule_level,
                                          items.dept,
                                          items.class,
                                          items.subclass,
                                          items.source_item,
                                          items.source_item_level,
                                          items.source_tran_level,
                                          items.source_pack_ind,
                                          items.source_diff1_id,
                                          items.source_diff2_id,
                                          items.source_diff3_id,
                                          items.source_diff4_id,
                                          items.tran_item,
                                          items.tran_item_level,
                                          items.tran_tran_level,
                                          items.tran_pack_ind,
                                          items.tran_diff1_id,
                                          items.tran_diff2_id,
                                          items.tran_diff3_id,
                                          items.tran_diff4_id,
                                          items.pack_qty,
                                          l.loc,
                                          l.sister_store,
                                          NULL as size_profile_qty,
                                          NULL as total_profile_qty,
                                          d.eow_date as eow_date,
                                          d.weight as weight,
                                          -1 as sales_hist_need,
                                          -1 as forecast_need,
                                          -1 as replan_need,
                                          -1 as plan_need,
                                          -1 as plan_reproject_need,
                                          -1 as receipt_plan_need,
                                          -1 as corp_rule_need
                                     from items,
                                          alc_loc_group alg,
                                          alc_location al,
                                          (select s.store loc,
                                                  s.sister_store
                                             from store s
                                           union all
                                           select w.wh loc,
                                                  NULL sister_store
                                             from wh w) l,
                                          alc_calc_need_dates_temp d
                                    where alg.alloc_id              = I_alloc_id
                                      and al.loc_group_id           = alg.loc_group_id
                                      and l.loc                     = al.location_id
                                      and items.alloc_id            = d.alloc_id
                                      and items.rule_many_to_one_id = d.rule_many_to_one_id;

   if REMOVE_ALT_HIER_DUPS(O_error_message,
                           I_alloc_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SEED_NEED_ALT_HIER_GTT;
-------------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_ALT_HIER_DUPS(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.REMOVE_ALT_HIER_DUPS';
   L_alt_hier_count  NUMBER(10);
   L_do_delete       NUMBER(10);

BEGIN


   select COUNT(*) into L_alt_hier_count
     from alc_rule_many_to_one
    where alloc_id  = I_alloc_id;

   select COUNT(*) into L_do_delete
     from alc_rule_many_to_one
    where alloc_id            = I_alloc_id
      and alt_hier_rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT;

   if L_alt_hier_count > 1 and L_do_delete > 0 then

      --have dept remove lower
      delete from alc_calc_need_temp del
       where del.alloc_id    = I_alloc_id
         and del.rule_level != ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
         and (del.dept, del.eow_date, del.to_loc) IN (select DISTINCT d.dept,
                                                             d.eow_date,
                                                             d.to_loc
                                                        from alc_calc_need_temp d
                                                       where d.alloc_id   = I_alloc_id
                                                         and d.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT
                                                         and rownum > 0);
   end if;

   select COUNT(*) into L_do_delete
     from alc_rule_many_to_one
    where alloc_id            = I_alloc_id
      and alt_hier_rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS;

   if L_alt_hier_count > 1 and L_do_delete > 0 then
      --have class remove lower
      delete from alc_calc_need_temp del
       where del.alloc_id   = I_alloc_id
         and del.rule_level NOT IN (ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT,
                                    ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS)
         and (del.dept, del.class, del.eow_date, del.to_loc) IN (select DISTINCT d.dept,
                                                                        d.class,
                                                                        d.eow_date,
                                                                        d.to_loc
                                                                   from alc_calc_need_temp d
                                                                  where d.alloc_id   = I_alloc_id
                                                                    and d.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS
                                                                    and rownum > 0);
   end if;

   select COUNT(*) into L_do_delete
     from alc_rule_many_to_one
    where alloc_id            = I_alloc_id
      and alt_hier_rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS;

   if L_alt_hier_count > 1 and L_do_delete > 0 then

      --have subclass remove lower
      delete from alc_calc_need_temp del
       where del.alloc_id   = I_alloc_id
         and del.rule_level NOT IN (ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT,
                                    ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS,
                                    ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS)
         and (del.dept, del.class, del.subclass, del.eow_date, del.to_loc) IN (select DISTINCT d.dept,
                                                                                      d.class,
                                                                                      d.subclass,
                                                                                      d.eow_date,
                                                                                      d.to_loc
                                                                                 from alc_calc_need_temp d
                                                                                where d.alloc_id   = I_alloc_id
                                                                                  and d.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS
                                                                                  and rownum > 0);
   end if;

   select COUNT(*) into L_do_delete
     from alc_rule_many_to_one
    where alloc_id            = I_alloc_id
      and alt_hier_rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL;

   if L_alt_hier_count > 1 and L_do_delete > 0 then

      --have parent remove lower
      delete from alc_calc_need_temp del
       where del.alloc_id   = I_alloc_id
         and del.rule_level NOT IN (ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT,
                                    ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS,
                                    ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS,
                                    ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL)
         and (del.tran_item, del.eow_date, del.to_loc) IN (select DISTINCT d.tran_item,
                                                                  d.eow_date,
                                                                  d.to_loc
                                                             from alc_calc_need_temp d
                                                            where d.alloc_id   = I_alloc_id
                                                              and d.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL
                                                              and rownum > 0);
   end if;

   select COUNT(*) into L_do_delete
     from alc_rule_many_to_one
    where alloc_id            = I_alloc_id
      and alt_hier_rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE;

   if L_alt_hier_count > 1 and L_do_delete > 0 then

      --have parent/diff remove lower
      delete from alc_calc_need_temp del
       where del.alloc_id   = I_alloc_id
         and del.rule_level NOT IN (ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT,
                                    ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS,
                                    ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS,
                                    ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL,
                                    ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE)
         and (del.tran_item, del.eow_date, del.to_loc) IN (select DISTINCT d.tran_item,
                                                                  d.eow_date,
                                                                  d.to_loc
                                                             from alc_calc_need_temp d
                                                            where d.alloc_id   = I_alloc_id
                                                              and d.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE
                                                              and rownum > 0);

   end if;

   --delete dups at SKU level
   delete from alc_calc_need_temp
    where rowid in(select i.rowid
                     from (select rowid,
                                  row_number() OVER (PARTITION BY tran_item,
                                                                  to_loc, eow_date
                                                         ORDER BY rule_many_to_one_id) dup_count
                             from alc_calc_need_temp
                            where alloc_id  = I_alloc_id
                              and tran_item is NOT NULL) i
                    where i.dup_count > 1);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END REMOVE_ALT_HIER_DUPS;
-------------------------------------------------------------------------------------------------------------
FUNCTION CLEANUP_WORKING_TABLES(O_error_message   IN OUT VARCHAR2,
                                I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.CLEANUP_WORKING_TABLES';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   --Ensure working tables are empty
   delete
     from alc_calc_destination_temp
    where alloc_id = I_alloc_id;

   delete
     from alc_calc_qty_limits_temp
    where alloc_id = I_alloc_id;

   delete
     from alc_merch_hier_rloh_temp
    where alloc_id = I_alloc_id;

   delete
     from alc_calc_pack_level_temp
    where alloc_id = I_alloc_id;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END CLEANUP_WORKING_TABLES;
-------------------------------------------------------------------------------------------------------------
FUNCTION FINALIZE_OUTPUT(O_error_message   IN OUT VARCHAR2,
                         I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                         I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.FINALIZE_OUTPUT';

BEGIN

   LOGGER.LOG_INFORMATION('START: ' || L_program );

   if I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_DEPT then

      merge /*+ INDEX(target, ALC_CALC_DESTINATION_TEMP_I1) LEADING(use_this) */ into alc_calc_destination_temp target
      using (select i.alloc_id,
                    i.dept,
                    i.to_loc,
                    SUM(i.need_value) need_value
              from (select n.alloc_id,
                           n.dept,
                           n.to_loc,
                           n.eow_date,
                           MAX(DECODE(n.sales_hist_need,
                                      -1, 0,
                                      n.sales_hist_need)) +
                           MAX(DECODE(n.forecast_need,
                                      -1, 0,
                                      n.forecast_need)) +
                           MAX(DECODE(n.plan_need,
                                      -1, 0,
                                      n.plan_need))+
                           MAX(DECODE(n.receipt_plan_need,
                                      -1, 0,
                                      n.receipt_plan_need)) +
                           MAX(DECODE(n.corp_rule_need,
                                      -1, 0,
                                      n.corp_rule_need)) +
                           MAX(DECODE(n.plan_reproject_need,
                                      -1, 0,
                                      n.plan_reproject_need)) need_value
                      from alc_calc_need_temp n
                     where n.alloc_id = I_alloc_id
                       and rownum     > 0
                     group by n.alloc_id,
                              n.dept,
                              n.to_loc,
                              n.eow_date) i
              --
              group by i.alloc_id,
                       i.dept,
                       i.to_loc
            ) use_this
      on(    target.dept     = use_this.dept
         and target.to_loc   = use_this.to_loc
         and target.alloc_id = use_this.alloc_id)
      when MATCHED then
         update
            set target.need_value = use_this.need_value;

      if I_alc_rule_row.use_rule_level_on_hand_ind is NOT NULL then

         merge into alc_calc_destination_temp target
         using (select d.dept,
                       d.to_loc,
                       SUM(r.curr_avail) rloh_current_value,
                       SUM(r.future_avail) rloh_future_value
                  from (select DISTINCT alloc_id,
                               dept,
                               to_loc
                          from alc_calc_destination_temp
                         where alloc_id = I_alloc_id) d,
                       alc_calc_rloh_temp r,
                       item_master im
                 where d.alloc_id = r.alloc_id
                   and d.dept     = im.dept
                   and im.item    = r.item
                   and d.to_loc   = r.loc
                 group by d.dept,
                          d.to_loc) use_this
         on(    target.dept     = use_this.dept
            and target.to_loc   = use_this.to_loc
            and target.alloc_id = I_alloc_id)
         when MATCHED then
            update
               set target.rloh_current_value = use_this.rloh_current_value,
                   target.rloh_future_value  = use_this.rloh_future_value;

         merge into alc_calc_destination_temp target
         using (select d.dept,
                       d.to_loc,
                       SUM(r.curr_avail) rloh_current_value,
                       SUM(r.future_avail) rloh_future_value
                  from (select DISTINCT alloc_id,
                               dept,
                               to_loc
                          from alc_calc_destination_temp
                         where alloc_id = I_alloc_id) d,
                       alc_merch_hier_rloh_temp r
                 where d.alloc_id = r.alloc_id
                   and d.dept     = r.dept
                   and d.to_loc   = r.loc
                 group by d.dept,
                          d.to_loc) use_this
         on(    target.dept     = use_this.dept
            and target.to_loc   = use_this.to_loc
            and target.alloc_id = I_alloc_id)
         when MATCHED then
            update
               set target.rloh_current_value = use_this.rloh_current_value,
                   target.rloh_future_value  = use_this.rloh_future_value;

      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_CLASS then

      merge into alc_calc_destination_temp target
      using (
             select i.alloc_id,
                    i.dept,i.class,
                    i.to_loc,
                    SUM(i.need_value) need_value
               from (select n.alloc_id,
                            n.dept,
                            n.class,
                            n.to_loc,
                            n.eow_date,
                            MAX(DECODE(n.sales_hist_need,
                                       -1, 0,
                                       n.sales_hist_need)) +
                            MAX(DECODE(n.forecast_need,
                                       -1, 0,
                                       n.forecast_need)) +
                            MAX(DECODE(n.plan_need,
                                       -1, 0,
                                       n.plan_need)) +
                            MAX(DECODE(n.receipt_plan_need,
                                       -1, 0,
                                       n.receipt_plan_need)) +
                            MAX(DECODE(n.corp_rule_need,
                                       -1, 0,
                                       n.corp_rule_need)) +
                            MAX(DECODE(n.plan_reproject_need,
                                       -1, 0,
                                       n.plan_reproject_need)) need_value
                       from alc_calc_need_temp n
                      where n.alloc_id = I_alloc_id
                      group by n.alloc_id,
                               n.dept,
                               n.class,
                               n.to_loc,
                               n.eow_date) i
              --
              group by i.alloc_id,
                       i.dept,
                       i.class,
                       i.to_loc
            ) use_this
      on(    target.dept     = use_this.dept
         and target.class    = use_this.class
         and target.to_loc   = use_this.to_loc
         and target.alloc_id = use_this.alloc_id)
      when MATCHED then
         update
            set target.need_value = use_this.need_value;

      if I_alc_rule_row.use_rule_level_on_hand_ind is NOT NULL then

         merge into alc_calc_destination_temp target
         using (select d.dept,
                       d.class,
                       d.to_loc,
                       SUM(r.curr_avail) rloh_current_value,
                       SUM(r.future_avail) rloh_future_value
                  from (select DISTINCT alloc_id,
                               dept,
                               class,
                               to_loc
                          from alc_calc_destination_temp
                         where alloc_id = I_alloc_id) d,
                       alc_calc_rloh_temp r,
                       item_master im
                 where d.alloc_id = r.alloc_id
                   and d.dept     = im.dept
                   and d.class    = im.class
                   and im.item    = r.item
                   and d.to_loc   = r.loc
                 group by d.dept,
                          d.class,
                          d.to_loc) use_this
         on(    target.dept     = use_this.dept
            and target.class    = use_this.class
            and target.to_loc   = use_this.to_loc
            and target.alloc_id = I_alloc_id)
         when MATCHED then
            update
               set target.rloh_current_value = use_this.rloh_current_value,
                   target.rloh_future_value  = use_this.rloh_future_value;

         merge into alc_calc_destination_temp target
         using (select d.dept,
                       d.class,
                       d.to_loc,
                       SUM(r.curr_avail) rloh_current_value,
                       SUM(r.future_avail) rloh_future_value
                  from (select DISTINCT alloc_id,
                               dept,
                               class,
                               to_loc
                          from alc_calc_destination_temp
                         where alloc_id = I_alloc_id) d,
                       alc_merch_hier_rloh_temp r
                 where d.alloc_id = r.alloc_id
                   and d.dept     = r.dept
                   and d.class    = r.class
                   and d.to_loc   = r.loc
                 group by d.dept,
                          d.class,
                          d.to_loc) use_this
         on(    target.dept     = use_this.dept
            and target.class    = use_this.class
            and target.to_loc   = use_this.to_loc
            and target.alloc_id = I_alloc_id)
         when MATCHED then
            update
               set target.rloh_current_value = use_this.rloh_current_value,
                   target.rloh_future_value  = use_this.rloh_future_value;

      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SUBCLASS then

      merge into alc_calc_destination_temp target
      using (select i.alloc_id,
                    i.dept,
                    i.class,
                    i.subclass,
                    i.to_loc,
                    SUM(i.need_value) need_value
              from (select n.alloc_id,
                           n.dept,
                           n.class,
                           n.subclass,
                           n.to_loc,
                           n.eow_date,
                           MAX(DECODE(n.sales_hist_need,
                                      -1, 0,
                                      n.sales_hist_need)) +
                           MAX(DECODE(n.forecast_need,
                                      -1, 0,
                                      n.forecast_need)) +
                           MAX(DECODE(n.plan_need,
                                      -1, 0,
                                      n.plan_need)) +
                           MAX(DECODE(n.receipt_plan_need,
                                      -1, 0,
                                      n.receipt_plan_need)) +
                           MAX(DECODE(n.corp_rule_need,
                                      -1, 0,
                                      n.corp_rule_need)) +
                           MAX(DECODE(n.plan_reproject_need,
                                      -1, 0,
                                      n.plan_reproject_need)) need_value
                      from alc_calc_need_temp n
                     where n.alloc_id = I_alloc_id
                     group by n.alloc_id,
                              n.dept,
                              n.class,
                              n.subclass,
                              n.to_loc,
                              n.eow_date) i
              --
              group by i.alloc_id,
                       i.dept,
                       i.class,
                       i.subclass,
                       i.to_loc
          ) use_this
      on(    target.dept     = use_this.dept
         and target.class    = use_this.class
         and target.subclass = use_this.subclass
         and target.to_loc   = use_this.to_loc
         and target.alloc_id = use_this.alloc_id)
      when MATCHED then
         update
            set target.need_value = use_this.need_value;

      if I_alc_rule_row.use_rule_level_on_hand_ind is NOT NULL then

         merge into alc_calc_destination_temp target
         using (select d.dept,
                       d.class,
                       d.subclass,
                       d.to_loc,
                       SUM(r.curr_avail) rloh_current_value,
                       SUM(r.future_avail) rloh_future_value
                  from (select DISTINCT alloc_id,
                               dept,
                               class,
                               subclass,
                               to_loc
                          from alc_calc_destination_temp
                         where alloc_id = I_alloc_id) d,
                       alc_calc_rloh_temp r,
                       item_master im
                 where d.alloc_id = r.alloc_id
                   and d.dept     = im.dept
                   and d.class    = im.class
                   and d.subclass = im.subclass
                   and im.item    = r.item
                   and d.to_loc   = r.loc
                 group by d.dept,
                          d.class,
                          d.subclass,
                          d.to_loc) use_this
         on(    target.dept     = use_this.dept
            and target.class    = use_this.class
            and target.subclass = use_this.subclass
            and target.to_loc   = use_this.to_loc
            and target.alloc_id = I_alloc_id)
         when MATCHED then
            update
               set target.rloh_current_value = use_this.rloh_current_value,
                   target.rloh_future_value  = use_this.rloh_future_value;

         merge into alc_calc_destination_temp target
         using (select d.dept,
                       d.class,
                       d.subclass,
                       d.to_loc,
                       SUM(r.curr_avail) rloh_current_value,
                       SUM(r.future_avail) rloh_future_value
                  from (select DISTINCT alloc_id,
                               dept,
                               class,
                               subclass,
                               to_loc
                          from alc_calc_destination_temp
                         where alloc_id = I_alloc_id) d,
                       alc_merch_hier_rloh_temp r
                 where d.alloc_id = r.alloc_id
                   and d.dept     = r.dept
                   and d.class    = r.class
                   and d.subclass = r.subclass
                   and d.to_loc   = r.loc
                 group by d.dept,
                          d.class,
                          d.subclass,
                          d.to_loc) use_this
         on(    target.dept     = use_this.dept
            and target.class    = use_this.class
            and target.subclass = use_this.subclass
            and target.to_loc   = use_this.to_loc
            and target.alloc_id = I_alloc_id)
         when MATCHED then
            update
               set target.rloh_current_value = use_this.rloh_current_value,
                   target.rloh_future_value  = use_this.rloh_future_value;

      end if;

   elsif ((LP_alloc_type = ALC_CONSTANTS_SQL.FASHION_PACK_GROUP_ALLOCATION and
           (I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL or
            I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE))
          or
          (LP_alloc_type != ALC_CONSTANTS_SQL.FASHION_PACK_ALLOCATION and
           I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLELEVEL)) then

      merge /*+ INDEX(target, ALC_CALC_DESTINATION_TEMP_I1) LEADING(use_this) */ into alc_calc_destination_temp target
      using (select i.alloc_id,
                    i.source_item,
                    i.to_loc,
                    SUM(i.need_value) need_value
               from (
                     --
                     select /*+ INDEX(n, ALC_CALC_NEED_TEMP_I5) */
                            n.alloc_id,
                            n.source_item,
                            n.to_loc,
                            n.eow_date,
                            MAX(DECODE(n.sales_hist_need,
                                       -1, 0,
                                       n.sales_hist_need)) +
                            MAX(DECODE(n.forecast_need,
                                       -1, 0,
                                       n.forecast_need)) +
                            MAX(DECODE(n.plan_need,
                                       -1, 0,
                                       n.plan_need)) +
                            MAX(DECODE(n.receipt_plan_need,
                                       -1, 0,
                                       n.receipt_plan_need)) +
                            MAX(DECODE(n.corp_rule_need,
                                       -1, 0,
                                       n.corp_rule_need)) +
                            MAX(DECODE(n.plan_reproject_need,
                                       -1, 0,
                                       n.plan_reproject_need)) need_value
                       from alc_calc_need_temp n
                      where n.alloc_id    = I_alloc_id
                        and rownum        > 0
                      group by n.alloc_id,
                               n.source_item,
                               n.to_loc,
                               n.eow_date) i
                      --
              group by i.alloc_id, i.source_item, i.to_loc
      ) use_this
      on(    target.source_item = use_this.source_item
         and target.to_loc      = use_this.to_loc
         and target.alloc_id    = use_this.alloc_id)
      when MATCHED then
         update
            set target.need_value = use_this.need_value;

      if I_alc_rule_row.use_rule_level_on_hand_ind is NOT NULL then

         merge into alc_calc_destination_temp target
         using (select d.source_item,
                       d.to_loc,
                       SUM(r.curr_avail) rloh_current_value,
                       SUM(r.future_avail) rloh_future_value
                  from (select DISTINCT alloc_id,
                               source_item,
                               to_loc
                          from alc_calc_destination_temp
                         where alloc_id = I_alloc_id) d,
                       alc_calc_rloh_temp r,
                       item_master im
                 where d.alloc_id    = r.alloc_id
                   and d.source_item = im.item_parent
                   and im.item       = r.item
                   and d.to_loc      = r.loc
                 group by d.source_item,
                          d.to_loc) use_this
         on(    target.source_item  = use_this.source_item
            and target.to_loc       = use_this.to_loc
            and target.alloc_id     = I_alloc_id)
         when MATCHED then
            update
               set target.rloh_current_value = use_this.rloh_current_value,
                   target.rloh_future_value  = use_this.rloh_future_value;

      end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_STYLE then

      merge into alc_calc_destination_temp target
      using (select i.alloc_id,
                    i.source_item,
                    i.source_diff1_id,
                    i.source_diff2_id,
                    i.source_diff3_id,
                    i.source_diff4_id,
                    i.to_loc,
                    SUM(need_value) need_value
               from (select n.alloc_id,
                            n.source_item,
                            n.source_diff1_id,
                            n.source_diff2_id,
                            n.source_diff3_id,
                            n.source_diff4_id,
                            n.to_loc,
                            n.eow_date,
                            MAX(DECODE(n.sales_hist_need,
                                       -1, 0,
                                       n.sales_hist_need)) +
                            MAX(DECODE(n.forecast_need,
                                       -1, 0,
                                       n.forecast_need)) +
                            MAX(DECODE(n.plan_need,
                                       -1, 0,
                                       n.plan_need)) +
                            MAX(DECODE(n.receipt_plan_need,
                                       -1, 0,
                                       n.receipt_plan_need)) +
                            MAX(DECODE(n.corp_rule_need,
                                       -1, 0,
                                       n.corp_rule_need)) +
                            MAX(DECODE(n.plan_reproject_need,
                                       -1, 0,
                                       n.plan_reproject_need)) need_value
                       from alc_calc_need_temp n
                      where n.alloc_id = I_alloc_id
                      group by n.alloc_id,
                               n.source_item,
                               n.source_diff1_id,
                               n.source_diff2_id,
                               n.source_diff3_id,
                               n.source_diff4_id,
                               n.to_loc,
                               n.eow_date) i
             group by i.alloc_id,
                      i.source_item,
                      i.source_diff1_id,
                      i.source_diff2_id,
                      i.source_diff3_id,
                      i.source_diff4_id,
                      i.to_loc
          ) use_this
      on(    target.source_item               = use_this.source_item
         and NVL(target.source_diff1_id,'-1') = NVL(use_this.source_diff1_id,'-1')
         and NVL(target.source_diff2_id,'-1') = NVL(use_this.source_diff2_id,'-1')
         and NVL(target.source_diff3_id,'-1') = NVL(use_this.source_diff3_id,'-1')
         and NVL(target.source_diff4_id,'-1') = NVL(use_this.source_diff4_id,'-1')
         and target.to_loc                    = use_this.to_loc
         and target.alloc_id                  = use_this.alloc_id)
      when MATCHED then
         update
            set target.need_value = use_this.need_value;

      if I_alc_rule_row.use_rule_level_on_hand_ind is NOT NULL then

      merge into alc_calc_destination_temp target
      using (select d.source_item,
                    d.source_diff1_id,
                    d.source_diff2_id,
                    d.source_diff3_id,
                    d.source_diff4_id,
                    d.to_loc,
                    SUM(r.curr_avail) rloh_current_value,
                    SUM(r.future_avail) rloh_future_value
               from (select DISTINCT alloc_id,
                            source_item,
                            source_diff1_id,
                            source_diff2_id,
                            source_diff3_id,
                            source_diff4_id,
                            to_loc
                       from alc_calc_destination_temp
                      where alloc_id = I_alloc_id) d,
                    alc_calc_rloh_temp r,
                    item_master im
              where d.alloc_id    = r.alloc_id
                and d.source_item = im.item_parent
                and im.item       = r.item
                and NVL(im.diff_1,'-1')     = NVL(d.source_diff1_id, NVL(im.diff_1,'-1'))
                and NVL(im.diff_2,'-1')     = NVL(d.source_diff2_id, NVL(im.diff_2,'-1'))
                and NVL(im.diff_3,'-1')     = NVL(d.source_diff3_id, NVL(im.diff_3,'-1'))
                and NVL(im.diff_4,'-1')     = NVL(d.source_diff4_id, NVL(im.diff_4,'-1'))
                and d.to_loc       = r.loc
              group by d.source_item,
                       d.source_diff1_id,
                       d.source_diff2_id,
                       d.source_diff3_id,
                       d.source_diff4_id,
                       d.to_loc) use_this
      on(    target.source_item               = use_this.source_item
         and NVL(target.source_diff1_id,'-1') = NVL(use_this.source_diff1_id,'-1')
         and NVL(target.source_diff2_id,'-1') = NVL(use_this.source_diff2_id,'-1')
         and NVL(target.source_diff3_id,'-1') = NVL(use_this.source_diff3_id,'-1')
         and NVL(target.source_diff4_id,'-1') = NVL(use_this.source_diff4_id,'-1')
         and target.to_loc                    = use_this.to_loc
         and target.alloc_id                  = I_alloc_id)
      when MATCHED then
         update
            set target.rloh_current_value = use_this.rloh_current_value,
                target.rloh_future_value  = use_this.rloh_future_value;

     end if;

   elsif I_alc_rule_row.rule_level = ALC_CONSTANTS_SQL.RULE_LEVEL_SKU then

      merge /*+ INDEX(target, ALC_CALC_DESTINATION_TEMP_I1) LEADING(use_this) */ into alc_calc_destination_temp target
      using (select i.alloc_id,
                    i.tran_item,
                    i.to_loc,
                    SUM(i.need_value) need_value
              from (select /*+ INDEX(n, ALC_CALC_NEED_TEMP_I1) */
                           n.alloc_id,
                           n.tran_item,
                           n.to_loc,
                           n.eow_date,
                           MAX(DECODE(n.sales_hist_need,
                                      -1, 0,
                                      n.sales_hist_need)) +
                           MAX(DECODE(n.forecast_need,
                                      -1, 0,
                                      n.forecast_need)) +
                           MAX(DECODE(n.plan_need,
                                      -1, 0,
                                      n.plan_need)) +
                           MAX(DECODE(n.receipt_plan_need,
                                      -1, 0,
                                      n.receipt_plan_need)) +
                           MAX(DECODE(n.corp_rule_need,
                                      -1, 0,
                                      n.corp_rule_need)) +
                           MAX(DECODE(n.plan_reproject_need,
                                      -1, 0,
                                      n.plan_reproject_need)) need_value
                      from alc_calc_need_temp n
                     where n.alloc_id        = I_alloc_id
                       and rownum            > 0
                     group by n.alloc_id,
                              n.tran_item,
                              n.to_loc,
                              n.eow_date) i
             --
             group by i.alloc_id, i.tran_item, i.to_loc
      ) use_this
      on(    target.tran_item  = use_this.tran_item
         and target.to_loc     = use_this.to_loc
         and target.alloc_id   = I_alloc_id)
      when MATCHED then
         update
            set target.need_value = use_this.need_value;

      if I_alc_rule_row.use_rule_level_on_hand_ind is NOT NULL then

      merge into alc_calc_destination_temp target
      using (select d.tran_item,
                    d.to_loc,
                    SUM(r.curr_avail) rloh_current_value,
                    SUM(r.future_avail) rloh_future_value
               from (select DISTINCT alloc_id,
                            tran_item,
                            to_loc
                       from alc_calc_destination_temp
                      where alloc_id = I_alloc_id) d,
                    alc_calc_rloh_temp r
              where d.alloc_id    = r.alloc_id
                and d.tran_item   = r.item
                and d.to_loc      = r.loc
              group by d.tran_item,
                       d.to_loc) use_this
      on(    target.tran_item = use_this.tran_item
         and target.to_loc    = use_this.to_loc
         and target.alloc_id  = I_alloc_id)
      when MATCHED then
         update
            set target.rloh_current_value = use_this.rloh_current_value,
                target.rloh_future_value  = use_this.rloh_future_value;

      end if;

   elsif I_alc_rule_row.rule_level is NULL then --ALT HIER

      merge /*+ INDEX(target, ALC_CALC_DESTINATION_TEMP_I1) LEADING(use_this) */ into alc_calc_destination_temp target
      using (select i.alloc_id,
                    i.to_loc,
                    SUM(i.need_value) need_value
               from (select n.alloc_id,
                            n.to_loc,
                            n.eow_date,
                            DECODE(n.rule_level,
                                   ALC_CONSTANTS_SQL.RULE_LEVEL_SKU, n.tran_item,
                                   '-1') tran,
                            n.rule_many_to_one_id,
                            MAX(DECODE(n.sales_hist_need,
                                       -1, 0,
                                       n.sales_hist_need)) +
                            MAX(DECODE(n.forecast_need,
                                       -1, 0,
                                       n.forecast_need)) +
                            MAX(DECODE(n.plan_need,
                                       -1, 0,
                                       n.plan_need)) +
                            MAX(DECODE(n.receipt_plan_need,
                                       -1, 0,
                                       n.receipt_plan_need)) +
                            MAX(DECODE(n.corp_rule_need,
                                       -1, 0,
                                       n.corp_rule_need)) +
                            MAX(DECODE(n.plan_reproject_need,
                                       -1, 0,
                                       n.plan_reproject_need)) need_value
                       from alc_calc_need_temp n
                      where n.alloc_id        = I_alloc_id
                        and rownum            > 0
                      group by n.alloc_id,
                               n.to_loc,
                               n.eow_date,
                               DECODE(n.rule_level,
                                      ALC_CONSTANTS_SQL.RULE_LEVEL_SKU, n.tran_item,
                                      '-1'),
                               n.rule_many_to_one_id) i
              --
              group by i.alloc_id, i.to_loc ) use_this
      on(    target.to_loc   = use_this.to_loc
         and target.alloc_id = I_alloc_id)
      when MATCHED then
         update
            set target.need_value = use_this.need_value;


      if I_alc_rule_row.use_rule_level_on_hand_ind is NOT NULL then

         merge into alc_calc_destination_temp target
         using (select d.to_loc,
                       SUM(r.curr_avail) rloh_current_value,
                       SUM(r.future_avail) rloh_future_value
                  from (select DISTINCT alloc_id,
                               to_loc
                          from alc_calc_destination_temp
                         where alloc_id = I_alloc_id) d,
                       alc_calc_rloh_temp r
                 where d.alloc_id   = r.alloc_id
                   and d.to_loc     = r.loc
                 group by d.to_loc) use_this
         on(    target.to_loc    = use_this.to_loc
            and target.alloc_id  = I_alloc_id)
         when MATCHED then
            update
               set target.rloh_current_value = use_this.rloh_current_value,
                   target.rloh_future_value  = use_this.rloh_future_value;

         merge into alc_calc_destination_temp target
         using (select d.to_loc,
                       SUM(r.curr_avail) rloh_current_value,
                       SUM(r.future_avail) rloh_future_value
                  from (select DISTINCT alloc_id,
                               to_loc
                          from alc_calc_destination_temp
                         where alloc_id = I_alloc_id) d,
                       alc_merch_hier_rloh_temp r
                 where d.alloc_id = r.alloc_id
                   and d.to_loc    = r.loc
                 group by d.to_loc) use_this
         on(    target.to_loc = use_this.to_loc
            and target.alloc_id  = I_alloc_id)
         when MATCHED then
            update
               set target.rloh_current_value = use_this.rloh_current_value,
                   target.rloh_future_value  = use_this.rloh_future_value;

      end if;

   end if;

   LOGGER.LOG_INFORMATION('END: ' || L_program );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FINALIZE_OUTPUT;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ALLOC_EOW_DATE(O_error_message   IN OUT VARCHAR2,
                            O_eow_date        IN OUT DATE,
                            I_date            IN     DATE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.GET_ALLOC_EOW_DATE';
   L_curr_day    NUMBER(2);
   L_eow_day     NUMBER(2);
   L_diff_days   NUMBER(2);

BEGIN

   select TO_CHAR(I_date,'D')
     into L_curr_day
     from dual;

   select tp_end_of_week_day
     into L_eow_day
     from alc_system_options;

   L_diff_days := L_eow_day - L_curr_day;

   if L_diff_days < 0 then
      L_diff_days := 7 + L_diff_days;
   end if;

   O_eow_date := TRUNC(I_date) + L_diff_days;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ALLOC_EOW_DATE;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_PACK_LEVEL_TBL(O_error_message   IN OUT VARCHAR2,
                              I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_PACK_LEVEL_TBL';
   L_start_time                 TIMESTAMP    := SYSTIMESTAMP;
   L_fp_pack_range              ALC_SYSTEM_OPTIONS.FP_PACK_RANGING%TYPE;
   L_enforce_wh_store_rel_ind   ALC_ALLOC.ENFORCE_WH_STORE_REL_IND%TYPE;

BEGIN

   select fp_pack_ranging
     into L_fp_pack_range
     from alc_system_options;

   select enforce_wh_store_rel_ind
     into L_enforce_wh_store_rel_ind
     from alc_alloc
    where alloc_id = I_alloc_id;

   insert into alc_calc_pack_level_temp (alloc_id,
                                         item_type,
                                         item,
                                         dept,
                                         class,
                                         subclass,
                                         to_loc,
                                         to_loc_type,
                                         to_loc_name,
                                         sister_store,
                                         release_date,
                                         assign_default_wh,
                                         clear_ind,
                                         item_loc_status,
                                         need_value)
                                  select inner.alloc_id,
                                         inner.item_type,
                                         inner.item_id,
                                         inner.dept,
                                         inner.class,
                                         inner.subclass,
                                         inner.loc,
                                         inner.loc_type,
                                         inner.loc_name,
                                         inner.sister_store,
                                         inner.release_date,
                                         inner.assign_default_wh,
                                         inner.clear_ind,
                                         inner.item_loc_status,
                                         inner.need_value
                                    from (with pack_src_temp as
                                            (select DISTINCT ais.alloc_id,
                                                    ais.item_type,
                                                    ais.item_id,
                                                    locs.loc,
                                                    locs.loc_type,
                                                    locs.loc_name,
                                                    locs.sister_store,
                                                    locs.default_wh,
                                                    MIN(ais.release_date) OVER (PARTITION BY ais.alloc_id,
                                                                                             ais.item_id,
                                                                                             locs.loc) as release_date,
                                                    -1 need_value,
                                                    DECODE(L_fp_pack_range,
                                                           'P', pb.pack_no,
                                                           pb.item) join_item,
                                                    case
                                                       when ais.item_type != ALC_CONSTANTS_SQL.SELLABLEPACK then
                                                          pb.item
                                                    else
                                                       ais.item_id
                                                    end as clear_item
                                               from alc_item_source ais,
                                                    alc_loc_group alg,
                                                    alc_location al,
                                                    packitem_breakout pb,
                                                    (select s.store loc,
                                                            'S' loc_type,
                                                            s.store_name loc_name,
                                                            s.sister_store,
                                                            s.default_wh
                                                       from store s
                                                      union all
                                                     select w.wh loc,
                                                            'W' loc_type,
                                                            w.wh_name loc_name,
                                                            NULL sister_store,
                                                            w.default_wh default_wh
                                                       from wh w) locs
                                              where ais.alloc_id     = I_alloc_id
                                                and ais.alloc_id     = alg.alloc_id
                                                and alg.loc_group_id = al.loc_group_id
                                                and al.location_id   = locs.loc
                                                and ais.item_id      = pb.pack_no)
                                          select DISTINCT pst.alloc_id,
                                                 pst.item_type,
                                                 pst.item_id,
                                                 im.dept,
                                                 im.class,
                                                 im.subclass,
                                                 pst.loc,
                                                 pst.loc_type,
                                                 pst.loc_name,
                                                 pst.sister_store,
                                                 pst.release_date,
                                                 case
                                                    when L_enforce_wh_store_rel_ind = 'Y' then
                                                       NVL(DECODE(il.source_method,
                                                                  'W', il.source_wh),
                                                                  pst.default_wh)
                                                    else
                                                       NVL(DECODE(il.source_method,
                                                                  'W', il.source_wh),
                                                                  NULL)
                                                    end assign_default_wh,
                                                 case
                                                    when pst.item_type IN (ALC_CONSTANTS_SQL.NONSELLFASHIONSIMPLEPACK,
                                                                           ALC_CONSTANTS_SQL.NONSELLSTAPLESIMPLEPACK,
                                                                           ALC_CONSTANTS_SQL.NONSELLSTAPLECOMPLEXPACK,
                                                                           ALC_CONSTANTS_SQL.NONSELLFASHIONMULTICOLORPACK,
                                                                           ALC_CONSTANTS_SQL.NONSELLFASHIONSINGLECOLORPACK,
                                                                           ALC_CONSTANTS_SQL.SELLABLEPACK) then
                                                       NVL(ploc.clear_ind, 'N')
                                                    else
                                                       il.clear_ind
                                                    end as clear_ind,
                                                 il.status item_loc_status,
                                                 pst.need_value
                                            from pack_src_temp pst,
                                                 item_master im,
                                                 item_loc il,
                                                 item_loc ploc
                                           where pst.alloc_id   = I_alloc_id
                                             and pst.item_id    = im.item
                                             and pst.loc        = il.loc (+)
                                             and pst.join_item  = il.item (+)
                                             and pst.clear_item = ploc.item (+)
                                             and pst.loc        = ploc.loc (+)) inner;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_PACK_LEVEL_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if L_enforce_wh_store_rel_ind = 'N' then
      if SETUP_PACK_WH_PRIORITY(O_error_message,
                                I_alloc_id) = FALSE then
         return FALSE;
      end if;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_PACK_LEVEL_TBL;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_PACK_WH_PRIORITY(O_error_message   IN OUT VARCHAR2,
                                I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program                       VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_PACK_WH_PRIORITY';
   L_start_time                    TIMESTAMP    := SYSTIMESTAMP;
   L_intercompany_transfer_basis   INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE;

   cursor C_GET_TRANSFER_BASIS is
      select intercompany_transfer_basis
        from inv_move_unit_options;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   open C_GET_TRANSFER_BASIS;
   fetch C_GET_TRANSFER_BASIS into L_intercompany_transfer_basis;
   close C_GET_TRANSFER_BASIS;

   delete
     from alc_calc_wh_rule_priority
    where alloc_id = I_alloc_id;

   LOGGER.LOG_INFORMATION(L_program||' DELETE ALC_CALC_WH_RULE_PRIORITY - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if SETUP_PACK_FLEXIBLE_SUP_CHAIN(O_error_message,
                                    I_alloc_id,
                                    L_intercompany_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   if SET_PACK_LOC_RULE_PRIORITY(O_error_message,
                                 I_alloc_id) = FALSE then
      return FALSE;
   end if;

   if SET_PACK_SUPPLY_CHAIN_RULE(O_error_message,
                                 I_alloc_id) = FALSE then
      return FALSE;
   end if;

   if SET_PACK_SAME_ORGCHANNEL_RULE(O_error_message,
                                    I_alloc_id,
                                    L_intercompany_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   if SET_PACK_SAMEORG_DIFFCHANNEL(O_error_message,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   if SET_PACK_DIFFORG_SAMECHANNEL(O_error_message,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   if SET_PACK_DIFFORG_DIFFCHANNEL(O_error_message,
                                   I_alloc_id,
                                   L_intercompany_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_PACK_WH_PRIORITY;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_PACK_FLEXIBLE_SUP_CHAIN(O_error_message    IN OUT VARCHAR2,
                                       I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_PACK_WH_PRIORITY';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into alc_calc_flex_supply_chain_gtt(alloc_id,
                                              source_item,
                                              source_diff1_id,
                                              source_diff2_id,
                                              source_diff3_id,
                                              source_diff4_id,
                                              tran_item,
                                              tran_diff1_id,
                                              tran_diff2_id,
                                              tran_diff3_id,
                                              tran_diff4_id,
                                              to_loc,
                                              to_loc_type,
                                              alloc_source_wh_id,
                                              assigned_ranged_src_wh,
                                              to_loc_default_wh,
                                              org_unit_id,
                                              channel_id,
                                              tsf_entity_id,
                                              set_of_books_id,
                                              transfer_basis)
                                       select acpt.alloc_id,
                                              NULL source_item,
                                              NULL source_diff1_id,
                                              NULL source_diff2_id,
                                              NULL source_diff3_id,
                                              NULL source_diff4_id,
                                              acpt.item tran_item,
                                              NULL tran_diff1_id,
                                              NULL tran_diff2_id,
                                              NULL tran_diff3_id,
                                              NULL tran_diff4_id,
                                              acpt.to_loc,
                                              acpt.to_loc_type,
                                              ais.wh_id alloc_source_wh_id,
                                              acpt.assign_default_wh assigned_ranged_src_wh,
                                              v.default_wh to_loc_default_wh,
                                              v.org_unit_id,
                                              v.channel_id,
                                              DECODE(I_transfer_basis,
                                                     ALC_CONSTANTS_SQL.BASED_ON_TRANSFER, v.tsf_entity_id,
                                                     NULL) tsf_entity_id,
                                              DECODE(I_transfer_basis,
                                                     ALC_CONSTANTS_SQL.BASED_ON_SET_OF_BOOKS, v.set_of_books_id,
                                                     NULL) set_of_books_id,
                                              I_transfer_basis
                                         from alc_calc_pack_level_temp acpt,
                                              alc_item_source ais,
                                              v_alloc_supply_chain v
                                        where acpt.alloc_id    = I_alloc_id
                                          and acpt.alloc_id    = ais.alloc_id
                                          and acpt.item        = ais.item_id
                                          and acpt.to_loc      = v.location
                                          and acpt.to_loc_type = v.loc_type;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_CALC_FLEX_SUPPLY_CHAIN_GTT - RECEIPT PLAN PACK - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_PACK_FLEXIBLE_SUP_CHAIN;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_LOC_RULE_PRIORITY(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_PACK_LOC_RULE_PRIORITY';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if SET_ITEM_LOC_RULE_PRIORITY(O_error_message,
                                 I_alloc_id) = FALSE then
      return FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_PACK_LOC_RULE_PRIORITY;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_SUPPLY_CHAIN_RULE(O_error_message   IN OUT VARCHAR2,
                                    I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_PACK_SUPPLY_CHAIN_RULE';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if SET_STORE_WH_RULE_PRIORITY(O_error_message,
                                 I_alloc_id) = FALSE then
      return FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_PACK_SUPPLY_CHAIN_RULE;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_SAME_ORGCHANNEL_RULE(O_error_message    IN OUT VARCHAR2,
                                       I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                       I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_PACK_SAME_ORGCHANNEL_RULE';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if SET_SAMEORG_SAMECHANNEL_RULE(O_error_message,
                                   I_alloc_id,
                                   I_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_PACK_SAME_ORGCHANNEL_RULE;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_SAMEORG_DIFFCHANNEL(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_PACK_SAMEORG_DIFFCHANNEL';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if SET_SAMEORG_DIFFCHANNEL_RULE(O_error_message,
                                   I_alloc_id,
                                   I_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_PACK_SAMEORG_DIFFCHANNEL;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_DIFFORG_SAMECHANNEL(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_PACK_DIFFORG_SAMECHANNEL';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if SET_DIFFORG_SAMECHANNEL_RULE(O_error_message,
                                   I_alloc_id,
                                   I_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_PACK_DIFFORG_SAMECHANNEL;
-------------------------------------------------------------------------------------------------------------
FUNCTION SET_PACK_DIFFORG_DIFFCHANNEL(O_error_message    IN OUT VARCHAR2,
                                      I_alloc_id         IN     ALC_ALLOC.ALLOC_ID%TYPE,
                                      I_transfer_basis   IN     INV_MOVE_UNIT_OPTIONS.INTERCOMPANY_TRANSFER_BASIS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SET_PACK_DIFFORG_DIFFCHANNEL';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if SET_DIFFORG_DIFFCHANNEL_RULE(O_error_message,
                                   I_alloc_id,
                                   I_transfer_basis)  = FALSE then
      return FALSE;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_PACK_DIFFORG_DIFFCHANNEL;
-------------------------------------------------------------------------------------------------------------
FUNCTION PACK_LEVEL_EXCLUSIONS(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.PACK_LEVEL_EXCLUSIONS';
   L_start_time                 TIMESTAMP    := SYSTIMESTAMP;

   L_enforce_wh_store_rel_ind   ALC_ALLOC.ENFORCE_WH_STORE_REL_IND%TYPE := NULL;
   L_range_required             NUMBER(1)   := 0;
   L_invalid_status             OBJ_VARCHAR_ID_TABLE := OBJ_VARCHAR_ID_TABLE();
   L_tp_distribution_level      ALC_SYSTEM_OPTIONS.TP_DISTRIBUTION_LEVEL%TYPE := '1';
   L_process_staple             NUMBER(10)  := 0;
   L_what_if_ind                NUMBER(1)   := 0;

   cursor C_RANGED_REQ is
      select DECODE(invalid_status, 'NULL', 1, 0)
        from ( select REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd,'[^ ]+', 1, level) invalid_status
                 from alc_system_options
              connect by REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd, '[^ ]+', 1, level) is NOT NULL)
       where UPPER(invalid_status) = 'NULL'
         and L_what_if_ind = 0
       union all
      select DECODE(invalid_status, 'NULL', 1, 0)
        from ( select REGEXP_SUBSTR(tp_loc_excp_rsn_what_if,'[^ ]+', 1, level) invalid_status
                from alc_system_options
             connect by REGEXP_SUBSTR(tp_loc_excp_rsn_what_if, '[^ ]+', 1, level) is NOT NULL)
       where UPPER(invalid_status) = 'NULL'
         and L_what_if_ind = 1;

   cursor C_INVALID_STATUS is
      select invalid_status
        from ( select REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd,'[^ ]+', 1, level) invalid_status
                 from alc_system_options
              connect by REGEXP_SUBSTR(tp_loc_excp_rsn_prd_srcd, '[^ ]+', 1, level) is NOT NULL)
        where UPPER(invalid_status) != 'NULL'
          and L_what_if_ind = 0
      union all
      select invalid_status
        from ( select REGEXP_SUBSTR(tp_loc_excp_rsn_what_if,'[^ ]+', 1, level) invalid_status
                 from alc_system_options
              connect by REGEXP_SUBSTR(tp_loc_excp_rsn_what_if, '[^ ]+', 1, level) is NOT NULL)
        where UPPER(invalid_status) != 'NULL'
          and L_what_if_ind = 1;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   delete
     from gtt_num_num_str_str_date_date;

   delete
     from alc_item_loc_exclusion
    where alloc_id = I_alloc_id;

   select DECODE(MIN(source_type),
                     ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF, 1,
                     0)
     into L_what_if_ind
     from alc_item_source
    where alloc_id = I_alloc_id;

   select tp_distribution_level
     into L_tp_distribution_level
     from alc_system_options;

   select enforce_wh_store_rel_ind
     into L_enforce_wh_store_rel_ind
     from alc_alloc
    where alloc_id = I_alloc_id;

   open C_RANGED_REQ;
   fetch C_RANGED_REQ into L_range_required;
   close C_RANGED_REQ;

   open C_INVALID_STATUS;
   fetch C_INVALID_STATUS BULK COLLECT into L_invalid_status;
   close C_INVALID_STATUS;

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2,
                                              varchar2_1)
                                       select DISTINCT src.item_source_id,
                                              i.to_loc,
                                              i.reason
                                         from alc_item_source src,
                                              (--delete non-ranged
                                               select DISTINCT t.alloc_id,
                                                      t.item,
                                                      t.to_loc,
                                                      t.to_loc_name,
                                                      ALC_CONSTANTS_SQL.EXCLUDE_ITEM_LOC_STATUS reason
                                                 from alc_calc_pack_level_temp t
                                                where t.alloc_id                 = I_alloc_id
                                                  and L_range_required           = 1
                                                  and L_enforce_wh_store_rel_ind = 'Y'
                                                  and t.item_loc_status is NULL
                                               union all
                                              --delete non 'A' status on item_loc
                                               select DISTINCT t.alloc_id,
                                                      t.item,
                                                      t.to_loc,
                                                      t.to_loc_name,
                                                      ALC_CONSTANTS_SQL.EXCLUDE_ITEM_LOC_STATUS reason
                                                 from alc_calc_pack_level_temp t,
                                                      table(cast(L_invalid_status as OBJ_VARCHAR_ID_TABLE)) v
                                                where t.alloc_id         = I_alloc_id
                                                  and t.item_loc_status  is NOT NULL
                                                  and t.item_loc_status  = value(v)
                                               union all
                                              --delete store open/close dates
                                               select t.alloc_id,
                                                      t.item,
                                                      t.to_loc,
                                                      t.to_loc_name,
                                                      ALC_CONSTANTS_SQL.EXCLUDE_STORE_CLOSED reason
                                                 from alc_calc_pack_level_temp t,
                                                      alc_item_source src,
                                                      store s
                                                where t.alloc_id       = I_alloc_id
                                                  and t.alloc_id       = src.alloc_id
                                                  and t.item           = src.item_id
                                                  and t.to_loc         = s.store
                                                  and t.to_loc_type    = 'S'
                                                  and (   src.release_date < (s.store_open_date - s.start_order_days)
                                                       or src.release_date >= (s.store_close_date - NVL(s.stop_order_days,0)))
                                              ) i
                                        where src.alloc_id    = i.alloc_id
                                          and src.item_id     = i.item
                                       --
                                       union all
                                       --
                                       (select i2.item_source_id,
                                               i2.to_loc,
                                               ALC_CONSTANTS_SQL.EXCLUDE_MLD_PATH reason_code
                                          from (select a.item_source_id,
                                                       d.item,
                                                       d.to_loc
                                                  from alc_calc_pack_level_temp d,
                                                       alc_item_source a
                                                 where d.alloc_id                 = I_alloc_id
                                                   and L_tp_distribution_level    = '0'
                                                   and L_enforce_wh_store_rel_ind = 'Y'
                                                   and d.alloc_id                 = a.alloc_id
                                                   and d.item                     = a.item_id
                                                   and a.source_type             != ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF
                                                minus
                                                select a.item_source_id,
                                                       d.item,
                                                       d.to_loc
                                                  from alc_calc_pack_level_temp d,
                                                       alc_item_source a
                                                 where d.alloc_id              = I_alloc_id
                                                   and d.alloc_id              = a.alloc_id
                                                   and d.item                  = a.item_id
                                                   and a.wh_id                 = d.assign_default_wh) i2)
                                       --
                                       union all
                                       --
                                       SELECT src.item_source_id,
                                              pack.to_loc,
                                              ALC_CONSTANTS_SQL.EXCLUDE_SUPPLY_CHAIN_PRIORITY reason_code
                                         FROM alc_calc_pack_level_temp pack,
                                              alc_item_source src
                                        where src.alloc_id                = I_alloc_id
                                          and src.alloc_id                = pack.alloc_id
                                          and src.item_id                 = pack.item
                                          and L_enforce_wh_store_rel_ind  = 'N'
                                          and src.source_type            != ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF
                                          and NOT EXISTS (select 'x'
                                                            from alc_calc_wh_rule_priority wrp
                                                           where wrp.alloc_id = I_alloc_id
                                                             and src.alloc_id = wrp.alloc_id
                                                             and src.item_id  = wrp.tran_item
                                                             and src.wh_id    = wrp.source_wh);

   LOGGER.LOG_INFORMATION(L_program||' Insert GTT_NUM_NUM_STR_STR_DATE_DATE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into alc_item_loc_exclusion (item_loc_exclusion_id,
                                       alloc_id,
                                       item_id,
                                       item_desc,
                                       location_id,
                                       location_desc,
                                       reason_code,
                                       diff1_id,
                                       source_location_id,
                                       order_no,
                                       source_type)
                                select alc_item_loc_exclusion_seq.NEXTVAL,
                                       I_alloc_id,
                                       ais.item_id,
                                       im.item_desc,
                                       gtt.number_2,   --to_loc
                                       l.loc_name,
                                       gtt.varchar2_1, --reason
                                       ais.diff1_id,
                                       ais.wh_id,
                                       ais.order_no,
                                       ais.source_type
                                  from (select DISTINCT g.number_1,
                                               g.number_2,
                                               g.varchar2_1
                                          from gtt_num_num_str_str_date_date g) gtt,
                                       alc_item_source ais,
                                       item_master im,
                                       (select store loc,
                                               store_name loc_name
                                          from store
                                         union all
                                        select wh loc,
                                               wh_name loc_name
                                          from wh) l
                                 where gtt.number_1 = ais.item_source_id
                                   and ais.item_id  = im.item
                                   and gtt.number_2 = l.loc;

   LOGGER.LOG_INFORMATION(L_program||' Insert ALC_ITEM_LOC_EXCLUSION - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --delete destination where no source records cover it
   delete from alc_calc_pack_level_temp
    where (alloc_id, item, to_loc) in (select inv.alloc_id,
                                              inv.item,
                                              inv.to_loc
                                         from (select /*+ INDEX(d, ALC_CALC_DESTINATION_TEMP_I4) LEADING(s) */
                                                      d.alloc_id,
                                                      d.item,
                                                      d.to_loc,
                                                      COUNT(DISTINCT s.item_source_id) total_src_cnt
                                                 from alc_item_source s,
                                                      alc_calc_pack_level_temp d
                                                where s.alloc_id  = I_alloc_id
                                                  and s.alloc_id  = d.alloc_id
                                                  and s.item_id   = d.item
                                                group by d.alloc_id,
                                                         d.item,
                                                         d.to_loc) val,
                                              (select /*+ INDEX(d, ALC_CALC_DESTINATION_TEMP_I4) LEADING(gtt) */
                                                      d.alloc_id,
                                                      d.item,
                                                      d.to_loc,
                                                      COUNT(DISTINCT s.item_source_id) invalid_src_cnt
                                                 from alc_item_source s,
                                                      alc_calc_pack_level_temp d,
                                                      gtt_num_num_str_str_date_date gtt
                                                where s.alloc_id       = I_alloc_id
                                                  and s.alloc_id       = d.alloc_id
                                                  and s.item_id        = d.item
                                                  and s.item_source_id = gtt.number_1
                                                  and d.to_loc         = gtt.number_2
                                                group by d.alloc_id,
                                                         d.item,
                                                         d.to_loc) inv
                                        where inv.item           = val.item
                                          and inv.to_loc         = val.to_loc
                                          and val.total_src_cnt <= inv.invalid_src_cnt);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_CALC_PACK_LEVEL_TEMP - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   if L_enforce_wh_store_rel_ind = 'N' then
      delete
        from alc_calc_wh_rule_priority wrp
       where wrp.alloc_id = I_alloc_id
         and wrp.source_wh is NULL;

      LOGGER.LOG_INFORMATION(L_program||' Delete ALC_CALC_WH_RULE_PRIORITY - NULL SOURCE_WH- SQL%ROWCOUNT: ' || SQL%ROWCOUNT);
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PACK_LEVEL_EXCLUSIONS;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_PACK_LEVEL_NEED(O_error_message   IN OUT VARCHAR2,
                               I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                               I_alc_rule_row    IN     ALC_RULE%ROWTYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'ALC_CALC_SETUP_SQL.SETUP_PACK_LEVEL_NEED';
   L_current_eow       DATE         := SYSDATE;
   L_sysdate           DATE         := SYSDATE;

BEGIN

   if GET_RULE_HIST_EOW_DATES(O_error_message,
                              I_alloc_id,
                              I_alc_rule_row,
                              NULL,
                              -1) = FALSE then
      return FALSE;
   end if;

   if GET_ALLOC_EOW_DATE(O_error_message,
                         L_current_eow,
                         L_sysdate) = FALSE then
      return FALSE;
   end if;

   merge into alc_calc_pack_level_temp target
   using (select tmp.alloc_id,
                 tmp.item,
                 tmp.to_loc,
                 SUM(NVL(dsh.qty,-1) * DECODE(d.weight,
                                              NULL, 1.0,
                                              d.weight)) sales
            from alc_receipt_plan dsh,
                 alc_calc_pack_level_temp tmp,
                 alc_calc_need_dates_temp d
           where tmp.alloc_id    = I_alloc_id
             and d.alloc_id      = I_alloc_id
             and dsh.item        = tmp.item
             and dsh.eow_date    = d.eow_date
             and dsh.eow_date   >= L_current_eow
             and dsh.loc         = tmp.to_loc
             and tmp.need_value  = -1
           group by tmp.alloc_id,
                    tmp.item,
                    tmp.to_loc) use_this
   on (    target.alloc_id  = use_this.alloc_id
       and target.to_loc    = use_this.to_loc
       and target.item      = use_this.item)
   when MATCHED then
      update
         set target.need_value = use_this.sales;

   if LP_use_sister_store = 'Y' then

      merge into alc_calc_pack_level_temp target
      using (select tmp.alloc_id,
                    tmp.item,
                    tmp.to_loc,
                    SUM(NVL(dsh.qty,-1) * DECODE(d.weight,
                                                 NULL, 1.0,
                                                 d.weight)) sales
               from alc_receipt_plan dsh,
                    alc_calc_pack_level_temp tmp,
                    alc_calc_need_dates_temp d
              where tmp.alloc_id    = I_alloc_id
                and d.alloc_id      = I_alloc_id
                and dsh.item        = tmp.item
                and dsh.eow_date    = d.eow_date
                and dsh.eow_date   >= L_current_eow
                and dsh.loc         = tmp.sister_store
                and tmp.need_value  = -1
              group by tmp.alloc_id,
                       tmp.item,
                       tmp.to_loc) use_this
      on (    target.alloc_id  = use_this.alloc_id
          and target.to_loc    = use_this.to_loc
          and target.item      = use_this.item)
      when MATCHED then
         update
            set target.need_value = use_this.sales;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_PACK_LEVEL_NEED;
-------------------------------------------------------------------------------------------------------------
END ALC_CALC_SETUP_SQL;
/