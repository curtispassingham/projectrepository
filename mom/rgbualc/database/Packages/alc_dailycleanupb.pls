CREATE OR REPLACE PACKAGE BODY ALC_DAILY_CLEAN_UP_SQL AS
-------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_PS_TXN_PROC(O_error_message   IN OUT VARCHAR2,
                           NO_OF_DAYS        IN     NUMBER   DEFAULT 7)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
PROCEDURE EXEC_STMT(I_statement   IN VARCHAR2);
-------------------------------------------------------------------------------------------------------------
FUNCTION ALTER_CONST(O_error_message   IN OUT VARCHAR2,
                     I_owner           IN     VARCHAR2,
                     I_table           IN     VARCHAR2,
                     I_mode            IN     VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION ALTER_CONST(O_error_message   IN OUT VARCHAR2,
                     I_owner           IN     VARCHAR2,
                     I_table           IN     VARCHAR2,
                     I_mode            IN     VARCHAR2)
RETURN NUMBER  IS

   L_program   VARCHAR2(61) := 'ALC_DAILY_CLEAN_UP_SQL.ALTER_CONST';

   cursor C_CONST is
      select a.constraint_name,
             a.owner||'.'||a.table_name tbl
        from all_constraints a
       where a.owner           = I_owner
         and a.table_name      = I_table
         and a.constraint_type = 'R';

BEGIN

   for rec in C_CONST loop
      EXECUTE IMMEDIATE 'ALTER TABLE '||rec.tbl||' MODIFY CONSTRAINT '||rec.constraint_name||' '||I_mode;
   end loop;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ALTER_CONST;
-------------------------------------------------------------------------------------------------------------
FUNCTION CLEAR_DATA(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_DAILY_CLEAN_UP_SQL.CLEAR_DATA';
   L_owner        VARCHAR2(30);
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   select table_owner
     into L_owner
     from system_options;

   -- delete RAF Facet Session data
   delete
     from raf_facet_session_attribute
    where facet_attribute_cfg_id IN (select facet_attribute_cfg_id
                                       from raf_facet_attribute_cfg
                                      where facet_cfg_id IN (select facet_cfg_id
                                                               from raf_facet_cfg
                                                              where query_name IN ('ALC_WORK_ITEM_SOURCE','ALC_ITEM_LOC')));

   LOGGER.LOG_INFORMATION(L_program||' DELETE RAF_FACET_SESSION_ATTRIBUTE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete
     from raf_facet_session
    where facet_cfg_id IN (select facet_cfg_id
                             from raf_facet_cfg
                            where query_name IN ('ALC_WORK_ITEM_SOURCE','ALC_ITEM_LOC'));

   LOGGER.LOG_INFORMATION(L_program||' DELETE RAF_FACET_SESSION - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- delete allocation session data
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_SESSION_ITEM_LOC';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_SESSION_ITEM_LOC_EXCL';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_SESSION_QUANTITY_LIMITS';

   -- delete worksheet session data
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_WORK_SESSION_ITEM';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_WORK_SESSION_ITEM_ALL';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_WORK_SESSION_ITEM_LOC';

   -- delete calculation temp data
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_CALC_DESTINATION_TEMP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_CALC_ALLITEMLOC_TEMP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_CALC_WH_RULE_PRIORITY';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_CALC_NEED_TEMP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_CALC_RLOH_TEMP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_CALC_QTY_LIMITS_TEMP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_CALC_RLOH_ITEM_TEMP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_MERCH_HIER_RLOH_TEMP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_CALC_SOURCE_TEMP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_CALC_NEED_DATES_TEMP';

   -- delete load data
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_LOAD_TEMP';

   -- delete Allocation approval tables
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_SYNC_HEADER_TEMP';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_SYNC_DETAIL_TEMP';

   -- delete size profile data
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_SESSION_SIZE_PROFILE_RATIO';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_SESSION_GID_PROFILE_LIST';
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_SESSION_SIZE_PROFILE';

   delete from ALC_SESSION_GID_PROFILE;

   LOGGER.LOG_INFORMATION(L_program||' DELETE ALC_SESSION_GID_PROFILE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   -- delete PO Arrivals Dashboard data
   EXECUTE IMMEDIATE 'truncate table '||L_owner||'.ALC_OI_PO_ARRIVAL_DETAILS';
   -------------------------

   if RAF_NOTIFICATION_TASK_PKG.DEL_NOTIF_PAST_RETENTION(O_error_message,
                                                         'ALC') = 0 then
      return 0;
   end if;

   -------------------------

   if PURGE_PS_TXN_PROC(O_error_message,
                        no_of_days => 7) = 0 then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END CLEAR_DATA;
-------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_PS_TXN_PROC(O_error_message   IN OUT VARCHAR2,
                           no_of_days        IN     NUMBER DEFAULT 7)
RETURN NUMBER  IS

   L_program      VARCHAR2(61) := 'ALC_DAILY_CLEAN_UP_SQL.PURGE_PS_TXN_PROC';
   L_sql_stmt     VARCHAR2(2000);
   p_name         VARCHAR2(100);
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;


BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   for c1 in (WITH part_cnt_query AS
                 (select COUNT(1) no_of_part
                    from user_tab_partitions
                   where table_name='PS_TXN')
               ---
               select partition_name p_name
                 from user_tab_partitions a,
                      part_cnt_query b
                where table_name          = 'PS_TXN'
                  and partition_position <= b.no_of_part - no_of_days
                  and interval            = 'YES')
   loop
      L_sql_stmt:='alter table PS_TXN drop partition '|| c1.p_name ||' update global indexes';
      EXECUTE IMMEDIATE L_sql_stmt;
   end loop;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END PURGE_PS_TXN_PROC;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_ALLOC_PURGE(O_error_message     IN OUT VARCHAR2,
                           I_max_thread_size   IN     NUMBER,
                           O_num_threads          OUT NUMBER)
RETURN NUMBER IS

   L_program           VARCHAR2(61) := 'ALC_DAILY_CLEAN_UP_SQL.SETUP_ALLOC_PURGE';
   L_max_thread_size   NUMBER       :=  I_max_thread_size;
   L_start_time        TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   O_num_threads := 0;

   delete
     from alc_alloc_purge_helper;

   insert into alc_alloc_purge_helper(alloc_id,
                                      thread_id)
                               select del.alloc_id,
                                      FLOOR( ((rownum)/L_max_thread_size)+1 ) thread_number
                                 from -- Scheduled allocations with alc_schedule.end_date > retention days
                                      (select aa.alloc_id
                                         from alc_alloc aa,
                                              alc_schedule asch,
                                              alc_system_options aso
                                        where aa.alloc_id = asch.parent_alloc_id
                                          and SYSDATE - asch.end_date > aso.tp_alloc_retention_days
                                          and aa.parent_allocation = 'Y'
                                       union all
                                       -- Closed allocations linked to RMS ALLOC_HEADER but records do not exist in the said table
                                       select axref.alloc_id
                                         from alc_xref axref
                                        where NOT EXISTS (select 'x'
                                                            from alloc_header ahdr
                                                           where ahdr.alloc_no = axref.xref_alloc_no)
                                          and EXISTS (select 'x'
                                                        from alc_alloc aa
                                                       where aa.alloc_id = axref.alloc_id
                                                         and aa.status   = ALC_CONSTANTS_SQL.STATUS_CLOSED)
                                       union all
                                       -- Unclosed allocations linked to RMS ALLOC_HEADER but records do not exist in the said table
                                       -- and alc_alloc.last_update_date > retention days
                                      select axref.alloc_id
                                        from alc_xref axref
                                       where NOT EXISTS (select 'x'
                                                           from alloc_header ahdr
                                                          where ahdr.alloc_no = axref.xref_alloc_no)
                                         and EXISTS (select 'x'
                                                       from alc_alloc aa,
                                                            alc_system_options aso
                                                      where aa.alloc_id = axref.alloc_id
                                                        and aa.status != ALC_CONSTANTS_SQL.STATUS_CLOSED
                                                        and SYSDATE - TRUNC(aa.last_update_date) > aso.tp_alloc_retention_days)
                                       union all
                                       -- allocations not linked to RMS and last_update_date > retention days
                                       select aa.alloc_id
                                         from alc_alloc aa,
                                              alc_system_options aso
                                        where NOT EXISTS (select 'x'
                                                            from alc_xref x
                                                           where x.alloc_id = aa.alloc_id)
                                          and aa.parent_allocation  = 'N'
                                          and SYSDATE - TRUNC(aa.last_update_date) > aso.tp_alloc_retention_days
                                       union
                                       -- allocation in Deleted Status
                                       select aa.alloc_id
                                         from alc_alloc aa
                                        where aa.status = ALC_CONSTANTS_SQL.STATUS_DELETED) del;

   LOGGER.LOG_INFORMATION(L_program||' INSERT ALC_ALLOC_PURGE_HELPER - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select MAX(thread_id)
     into O_num_threads
     from alc_alloc_purge_helper;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END SETUP_ALLOC_PURGE;
-------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_ALLOC(O_error_message   IN OUT VARCHAR2,
                     I_thread_id       IN     NUMBER)
RETURN NUMBER IS

   L_program           VARCHAR2(61)         := 'ALC_DAILY_CLEAN_UP_SQL.PURGE_ALLOC';
   L_alloc_ids         OBJ_NUMERIC_ID_TABLE;
   L_work_header_ids   OBJ_NUMERIC_ID_TABLE;
   L_start_time        TIMESTAMP            := SYSTIMESTAMP;

   cursor C_ALLOC_IDS is
      select alloc_id
        from alc_alloc_purge_helper
       where thread_id = I_thread_id;

   cursor C_WORK_HEADER_ID is
      select work_header_id
        from alc_work_alloc
       where alloc_id IN (select alloc_id
                            from alc_alloc_purge_helper
                           where thread_id = I_thread_id);

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   open C_ALLOC_IDS;
   fetch C_ALLOC_IDS BULK COLLECT into L_alloc_ids;
   close C_ALLOC_IDS;

   open C_WORK_HEADER_ID;
   fetch C_WORK_HEADER_ID BULK COLLECT into L_work_header_ids;
   close C_WORK_HEADER_ID;

   ---

   forall i in L_alloc_ids.first..L_alloc_ids.last
      delete alc_work_item_source_alloc
       where alloc_id = L_alloc_ids(i);

   forall i in L_work_header_ids.first..L_work_header_ids.last
      delete alc_work_item_source
       where work_header_id = L_work_header_ids(i);

   forall i in L_alloc_ids.first..L_alloc_ids.last
      delete alc_work_alloc
       where alloc_id = L_alloc_ids(i);

   forall i in L_work_header_ids.first..L_work_header_ids.last
      delete alc_work_header
       where work_header_id = L_work_header_ids(i);

   forall i in L_work_header_ids.first..L_work_header_ids.last
      delete alc_work_item_source_diff
       where work_header_id = L_work_header_ids(i);

   forall i in L_alloc_ids.first..L_alloc_ids.last
      delete alc_xref
       where alloc_id = L_alloc_ids(i);

	forall i in L_alloc_ids.first..L_alloc_ids.last
      delete alc_task
       where alloc_id = L_alloc_ids(i);

   forall i in L_alloc_ids.first..L_alloc_ids.last
     delete alc_alloc
      where alloc_id = L_alloc_ids(i);

   forall i in L_alloc_ids.first..L_alloc_ids.last
      delete alc_what_if
       where alloc_id = L_alloc_ids(i);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END PURGE_ALLOC;
-------------------------------------------------------------------------------------------------------------
FUNCTION SETUP_WORKSHEET_PURGE(O_error_message     IN OUT VARCHAR2,
                               I_max_thread_size   IN     NUMBER,
                               O_num_threads          OUT NUMBER)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'ALC_DAILY_CLEAN_UP_SQL.SETUP_WORKSHEET_PURGE';
   L_max_thread_size    NUMBER       := I_max_thread_size;
   L_start_time         TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   O_num_threads := 0;

   delete
     from alc_worksheet_purge_helper;

   insert into alc_worksheet_purge_helper(work_header_id,
                                          thread_id)
                                   select del.work_header_id,
                                          FLOOR(((rownum)/L_max_thread_size) + 1) thread_number
                                     from --worksheets to be deleted because alc_work_header.updated_date > retention days
                                          (select awh.work_header_id
                                             from alc_work_header awh,
                                                  alc_system_options aso
                                            where awh.work_type = ALC_CONSTANTS_SQL.WORKSHEET_TYPE_WORK
                                              and SYSDATE - awh.updated_date > aso.tp_worksheet_retention_days) del;

   LOGGER.LOG_INFORMATION(L_program||' INSERT ALC_WORKSHEET_PURGE_HELPER - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select MAX(thread_id)
     into O_num_threads
     from alc_worksheet_purge_helper;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END SETUP_WORKSHEET_PURGE;
-------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_WORKSHEET(O_error_message    IN OUT VARCHAR2,
                         I_thread_id        IN     NUMBER)
RETURN NUMBER IS

   L_program           VARCHAR2(61) := 'ALC_DAILY_CLEAN_UP_SQL.PURGE_WORKSHEET';
   L_work_header_ids   OBJ_NUMERIC_ID_TABLE;
   L_start_time        TIMESTAMP    := SYSTIMESTAMP;

   cursor C_WORK_HEADER_ID is
      select work_header_id
        from alc_work_header
       where work_type = ALC_CONSTANTS_SQL.WORKSHEET_TYPE_WORK
         and work_header_id IN (select work_header_id
                                  from alc_worksheet_purge_helper
                                 where thread_id = I_thread_id);

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   open  C_WORK_HEADER_ID;
   fetch C_WORK_HEADER_ID BULK COLLECT into L_work_header_ids;
   close C_WORK_HEADER_ID;

   forall i in L_work_header_ids.first..L_work_header_ids.last
     delete alc_work_item_source
      where work_header_id = L_work_header_ids(i);

   forall i in L_work_header_ids.first..L_work_header_ids.last
     delete alc_work_alloc
      where work_header_id = L_work_header_ids(i);

   forall i in L_work_header_ids.first..L_work_header_ids.last
     delete alc_work_header
      where work_header_id = L_work_header_ids(i);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END PURGE_WORKSHEET;
-------------------------------------------------------------------------------------------------------------
PROCEDURE EXEC_STMT(I_statement IN     VARCHAR2)
IS

   L_error_message   VARCHAR2(2000);
   L_program         VARCHAR2(61) := 'ALC_DAILY_CLEAN_UP_SQL.EXEC_STMT';

BEGIN

   EXECUTE IMMEDIATE I_statement;

   return;

EXCEPTION
   WHEN OTHERS THEN
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      LOGGER.LOG ('Error when executing the statement:'||I_statement||' Message:'||L_error_message);
   return;
END EXEC_STMT;
-------------------------------------------------------------------------------------------------------------
FUNCTION SHRINK_SESSION_TEMP_TBLS(O_error_message IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program   VARCHAR2(61) := 'ALC_DAILY_CLEAN_UP_SQL.SHRINK_SESSION_TEMP_TBLS';
   L_owner     VARCHAR2(30);

BEGIN

   select table_owner
     into L_owner
     from system_options;
   --
   --
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SESSION_ITEM_LOC enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SESSION_ITEM_LOC_EXCL enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SESSION_QUANTITY_LIMITS enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_WORK_SESSION_ITEM enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_WORK_SESSION_ITEM_ALL enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_WORK_SESSION_ITEM_LOC enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_DESTINATION_TEMP enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_ALLITEMLOC_TEMP enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_NEED_TEMP enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_RLOH_TEMP enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_QTY_LIMITS_TEMP enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_RLOH_ITEM_TEMP enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_MERCH_HIER_RLOH_TEMP enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_SOURCE_TEMP enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_NEED_DATES_TEMP enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SYNC_HEADER_TEMP enable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SYNC_DETAIL_TEMP enable row movement');
   --
   --
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SESSION_ITEM_LOC shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SESSION_ITEM_LOC_EXCL shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SESSION_QUANTITY_LIMITS shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_WORK_SESSION_ITEM shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_WORK_SESSION_ITEM_ALL shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_WORK_SESSION_ITEM_LOC shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_DESTINATION_TEMP shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_ALLITEMLOC_TEMP shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_NEED_TEMP shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_RLOH_TEMP shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_QTY_LIMITS_TEMP shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_RLOH_ITEM_TEMP shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_MERCH_HIER_RLOH_TEMP shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_SOURCE_TEMP shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_NEED_DATES_TEMP shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SYNC_HEADER_TEMP shrink space cascade');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SYNC_DETAIL_TEMP shrink space cascade');
   --
   --
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SESSION_ITEM_LOC disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SESSION_ITEM_LOC_EXCL disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SESSION_QUANTITY_LIMITS disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_WORK_SESSION_ITEM disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_WORK_SESSION_ITEM_ALL disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_WORK_SESSION_ITEM_LOC disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_DESTINATION_TEMP disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_ALLITEMLOC_TEMP disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_NEED_TEMP disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_RLOH_TEMP disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_QTY_LIMITS_TEMP disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_RLOH_ITEM_TEMP disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_MERCH_HIER_RLOH_TEMP disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_SOURCE_TEMP disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_CALC_NEED_DATES_TEMP disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SYNC_HEADER_TEMP disable row movement');
   EXEC_STMT( 'alter table '||L_owner||'.ALC_SYNC_DETAIL_TEMP disable row movement');
   -------------------------

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END SHRINK_SESSION_TEMP_TBLS;
-------------------------------------------------------------------------------------------------------------
END ALC_DAILY_CLEAN_UP_SQL;
/
