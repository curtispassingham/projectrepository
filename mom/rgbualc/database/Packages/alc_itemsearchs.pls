CREATE OR REPLACE PACKAGE ALC_ITEM_SEARCH_SQL AS
------------------------------------------------------------------------------------------------- 
-- This function finds top level items that meet the passed in search criteria.
-- Top level items are: STYLE, STAPLE, SELLPACK, and NONSELLPACK.
-- In order by be returned either the top level item or an item covered by 
--  the top level item must have inventory available to allocate.
-- The number of top level items returned is limited by alc_system_options.tp_item_search_max.
--
-- Used by the Create Standard Allocation screen
-- Used by the Quick Item Add pop-up to find items to add to an existing standard worksheet
-------------------------------------------------------------------------------------------------
FUNCTION SEARCH(O_error_message       IN OUT VARCHAR2,
                O_max_rows_exceeded      OUT NUMBER,
                I_search_criteria     IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------
-- This function creates standard worksheets (type WK) (alc_work_header, alc_work_item_source) for 
--  item/locs covered by the passed search criteria.
-- The search criteria is the selected items along with the criteria that was used
--  to populate the result section initially.
-- The worksheets are populated with their inventory positions.
--
-- Used by the Create Standard Allocation screen when the Create Worksheet button is pressed.
-------------------------------------------------------------------------------------------------
FUNCTION POPULATE_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                            O_work_header_id       OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                            I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                            I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------
-- This function adds rows to existing standard worksheets (WK and WD) (alc_work_item_source) for 
--  item/locs covered by the passed search criteria.
-- The search criteria is the selected items along with the criteria that was used
--  to populate the result section initially.
-- The added worksheet rows are populated with their inventory positions.
--
-- Used by the Quick Item Add pop-up in a standard worksheet when the Add To Worksheet button is pressed.
-------------------------------------------------------------------------------------------------
FUNCTION ADD_TO_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                          I_work_header_id    IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                          I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                          I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------
-- This function finds top level items that meet the passed search criteria.
-- Top level items are: STYLE, STAPLE, SELLPACK, and NONSELLPACK.
-- The number of top level items returned is limited by alc_system_options.tp_item_search_max.
--
-- Used by the Create What If Allocation screen
-------------------------------------------------------------------------------------------------
FUNCTION WHAT_IF_SEARCH(O_error_message       IN OUT VARCHAR2,
                        O_max_rows_exceeded      OUT NUMBER,
                        I_search_criteria     IN     ALC_ITEM_SEARCH_CRITERIA_REC)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------
-- This function creates what-if worksheets (alc_work_header, alc_work_item_source) for 
--  item/locs covered by the passed search criteria.
-- The search criteria is the selected items along with the criteria that was used
--  to populate the result section.
-- The worksheets are populated with -1 as their WH value.
-- The worksheets are NOT populated with their inventory positions.
--
-- Used by the Create What If Allocation screen when the Create Worksheet button is pressed.
-------------------------------------------------------------------------------------------------
FUNCTION WHAT_IF_POPULATE_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                                    O_work_header_id       OUT alc_work_header.work_header_id%TYPE,
                                    I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                                    I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------
-- This function adds rows to existing what-if worksheets (alc_work_item_source) for 
--  item/locs covered by the passed search criteria.
-- The search criteria is the selected items along with the criteria that was used
--  to populate the result section initially.
-- The added worksheet rows are NOT populated with their inventory positions.
--
-- Used by the Quick Item Add pop-up in a what-if worksheet when the Add To Worksheet button is pressed.
-------------------------------------------------------------------------------------------------
FUNCTION WHAT_IF_ADD_TO_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                                  I_work_header_id    IN     ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                                  I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                                  I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------
-- This function creates standard worksheets (type WD) (alc_work_header, alc_work_item_source) for 
--  item/locs covered by the passed search criteria non-what-if soruce type.
-- The search criteria is limited to the fields that exist in the Quick Create Allocation section in 
--  the left side pane of the UI. 
-- Only the items directly entered in the UI (and their PACKCOMPs if any) are populated in the worksheet.
-- The worksheets are populated with their inventory positions.
-- 
-- Used by the Quick Create Allocation section in the left side pane when 
--  Allocation Maintenance is select in the Next drop down. 
-------------------------------------------------------------------------------------------------
FUNCTION QUICK_POP_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                             O_work_header_id       OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                             O_alloc_type           OUT ALC_ALLOC.TYPE%TYPE,
                             I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                             I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------
-- This function creates standard worksheets (type WD) (alc_work_header, alc_work_item_source) for
--  item/locs covered by the passed search criteria for what-if source type.
-- The search criteria is limited to the fields that exist in the Quick Create Allocation section in
--  the left side pane of the UI.
-- Only the items directly entered in the UI (and their PACKCOMPs if any) are populated in the worksheet.
-- The worksheets are populated with their inventory positions.
--
-- Used by the Quick Create Allocation section in the left side pane when
--  Allocation Maintenance is select in the Next drop down.
-------------------------------------------------------------------------------------------------
FUNCTION WHAT_IF_QUICK_POP_WORKSHEET(O_error_message     IN OUT VARCHAR2,
                                     O_work_header_id       OUT ALC_WORK_HEADER.WORK_HEADER_ID%TYPE,
                                     O_alloc_type           OUT ALC_ALLOC.TYPE%TYPE,
                                     I_search_criteria   IN     ALC_ITEM_SEARCH_CRITERIA_REC,
                                     I_user_id           IN     ALC_WORK_HEADER.CREATED_BY%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------
END ALC_ITEM_SEARCH_SQL;
/
