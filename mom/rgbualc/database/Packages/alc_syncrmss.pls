CREATE OR REPLACE PACKAGE ALC_SYNC_RMS_SQL AS
-------------------------------------------------------------------------------------
FUNCTION CREATE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_alc_alloc_ids   IN       ID_TBL,
                      I_status          IN       alloc_header.status%TYPE)

RETURN NUMBER;
-------------------------------------------------------------------------------------
FUNCTION UPDATE_ALLOC_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alc_alloc_ids   IN       ID_TBL,
                             I_status          IN       alloc_header.status%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------
FUNCTION REMOVE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_alc_alloc_ids   IN       ID_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------------
END ALC_SYNC_RMS_SQL;
/
