CREATE OR REPLACE PACKAGE ALC_FREQUENCY_SCHEDULE_SQL AS
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ALLOC_SCHEDULE_INFO(O_error_msg             IN OUT VARCHAR2,
                                 L_alloc_shcedule_info      OUT OBJ_ALLOC_SCHEDULE_INFO_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
END ALC_FREQUENCY_SCHEDULE_SQL;
/