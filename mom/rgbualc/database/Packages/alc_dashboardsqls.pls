CREATE OR REPLACE PACKAGE ALC_DASHBOARD_SQL AS
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SESSION_ID(O_error_message   IN OUT VARCHAR2,
                             O_session_id         OUT OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_PO_ARRIVAL(O_error_message   IN OUT VARCHAR2,
                                   I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_PO_ARRIVAL(O_error_message   IN OUT VARCHAR2,
                        O_result          IN OUT ALC_OI_PO_ARRIVAL_RESULT_REC,
                        I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                        I_week_no         IN     NUMBER,
                        I_depts           IN     OBJ_NUMERIC_ID_TABLE,
                        I_classes         IN     OBJ_NUMERIC_ID_TABLE,
                        I_subclasses      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_MERCH_STOCK_TO_SALES(O_error_message   IN OUT VARCHAR2,
                                  O_result          IN OUT ALC_OI_STOCK_TO_SALES_TBL,
                                  I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                  I_depts           IN     OBJ_NUMERIC_ID_TABLE,
                                  I_classes         IN     OBJ_NUMERIC_ID_TABLE,
                                  I_subclasses      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_STOCK_TO_SALES(O_error_message   IN OUT VARCHAR2,
                                 O_result          IN OUT ALC_OI_STOCK_TO_SALES_TBL,
                                 I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                 I_item            IN     ITEM_MASTER.ITEM%TYPE,
                                 I_diff1_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                                 I_diff2_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                                 I_diff3_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                                 I_diff4_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                                 I_loc             IN     ITEM_LOC.LOC%TYPE,
                                 I_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_TOP_SELLER(O_error_message   IN OUT VARCHAR2,
                        O_result          IN OUT ALC_OI_TOP_BOTTOM_SALES_REC,
                        I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                        I_depts           IN     OBJ_NUMERIC_ID_TABLE,
                        I_classes         IN     OBJ_NUMERIC_ID_TABLE,
                        I_subclasses      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_BOTTOM_SELLER(O_error_message   IN OUT VARCHAR2,
                           O_result          IN OUT ALC_OI_TOP_BOTTOM_SALES_REC,
                           I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                           I_depts           IN     OBJ_NUMERIC_ID_TABLE,
                           I_classes         IN     OBJ_NUMERIC_ID_TABLE,
                           I_subclasses      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_PLAN_FORECAST(O_error_message   IN OUT VARCHAR2,
                           O_result          IN OUT ALC_OI_PLAN_FORECAST_TBL,
                           I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                           I_item            IN     ITEM_MASTER.ITEM%TYPE,
                           I_diff1_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                           I_diff2_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                           I_diff3_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                           I_diff4_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                           I_loc             IN     ITEM_LOC.LOC%TYPE,
                           I_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_OPEN_TO_BUY(O_error_message   IN OUT VARCHAR2,
                         O_result          IN OUT ALC_OI_OTB_REC,
                         I_session_id      IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                         I_alloc_id        IN     ALC_ALLOC.ALLOC_ID%TYPE,
                         I_item            IN     ITEM_MASTER.ITEM%TYPE,
                         I_diff1_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_diff2_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_diff3_id        IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_diff4_id        IN     DIFF_IDS.DIFF_ID%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_STOCK_TO_SALES_VIEW(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_TOP_BOTTOM_SELLER_VIEW(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_MATERIALIZED_VIEWS(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------------------------------------
END ALC_DASHBOARD_SQL;
/
