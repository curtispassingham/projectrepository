CREATE OR REPLACE PACKAGE ALC_CALC_SETUP_SQL AS
--------------------------------------------------------------------
FUNCTION SETUP_CALCULATION(O_error_message      IN OUT VARCHAR2,
                           I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE,
                           I_all_orders         IN     VARCHAR2,
                           I_alc_calc_packs_tbl IN     ALC_CALC_PACKS_TBL)
RETURN NUMBER;
--------------------------------------------------------------------
FUNCTION SETUP_CALC_RULE_LEVEL_PACK(O_error_message      IN OUT VARCHAR2,
                                    I_alloc_id           IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION CLEANUP_WORKING_TABLES(O_error_message IN OUT VARCHAR2,
                                I_alloc_id      IN     ALC_ALLOC.ALLOC_ID%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------
END ALC_CALC_SETUP_SQL;
/
