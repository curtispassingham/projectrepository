create or replace 
package alloc_pkg as
  type allocarray is table of alc_xref.alloc_id%type index by binary_integer;
  alloc_id allocarray;
end;
/