CREATE OR REPLACE PACKAGE BODY ALC_REP_UTIL_SQL AS
FUNCTION        ALC_REP_GET_CURRENT_AVAIL(
      I_item      IN item_loc.item%TYPE
    )
    RETURN item_loc_soh.stock_on_hand%TYPE
  IS
        total_val NUMBER(6);
        result1 boolean; 
        error_msg varchar2(100); 
        soh item_loc_soh.stock_on_hand%TYPE;
BEGIN
 
    result1 := ALC_REP_UTIL_SQL.GET_LOC_CURRENT_AVAIL(error_msg, soh,I_item ,null,'W');
    total_val := soh;
    RETURN total_val;
  
END ALC_REP_GET_CURRENT_AVAIL ;

FUNCTION        ALC_REP_GET_CURRENT_AVAIL(
      I_item      IN item_loc.item%TYPE,
      I_loc       IN item_loc.loc%TYPE
    )
    RETURN item_loc_soh.stock_on_hand%TYPE
IS
        total_val NUMBER(6);
        result1 boolean; 
        error_msg varchar2(100); 
        soh item_loc_soh.stock_on_hand%TYPE;
BEGIN
    result1 := ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(error_msg, soh,I_item ,I_loc,'W');
    total_val := soh;
    RETURN total_val;
    
END ALC_REP_GET_CURRENT_AVAIL;

FUNCTION GET_LOC_CURRENT_AVAIL
        (O_error_message  IN OUT VARCHAR2,
         O_available      IN OUT item_loc_soh.stock_on_hand%TYPE,
         I_item           IN     item_loc.item%TYPE,
         I_loc            IN     item_loc.loc%TYPE,
         I_loc_type       IN     item_loc.loc_type%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL';

   L_soh                    item_loc_soh.stock_on_hand%TYPE := 0;
   L_pack_comp_soh          item_loc_soh.stock_on_hand%TYPE := 0;
   L_in_transit_qty         item_loc_soh.stock_on_hand%TYPE := 0;
   L_pack_comp_intran       item_loc_soh.stock_on_hand%TYPE := 0;
   L_tsf_reserved_qty       item_loc_soh.stock_on_hand%TYPE := 0;
   L_pack_comp_resv         item_loc_soh.stock_on_hand%TYPE := 0;
   L_tsf_expected_qty       item_loc_soh.stock_on_hand%TYPE := 0;
   L_pack_comp_exp          item_loc_soh.stock_on_hand%TYPE := 0;
   L_rtv_qty                item_loc_soh.stock_on_hand%TYPE := 0;
   L_non_sellable_qty       item_loc_soh.stock_on_hand%TYPE := 0;
   L_customer_resv          item_loc_soh.stock_on_hand%TYPE := 0;
   L_customer_backorder     item_loc_soh.stock_on_hand%TYPE := 0;
   L_pack_comp_cust_resv    item_loc_soh.stock_on_hand%TYPE := 0;
   L_pack_comp_cust_back    item_loc_soh.stock_on_hand%TYPE := 0;
   L_dist_qty               alloc_detail.qty_distro%TYPE    := 0;

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_item',
                                           'NULL', 'NOT NULL');
      RETURN FALSE;
   end if;
   if I_loc_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_loc_type',
                                           'NULL', 'NOT NULL');
      RETURN FALSE;
   end if;

   O_available := 0;

   if not ITEMLOC_QUANTITY_SQL.GET_ITEM_LOC_QTYS(O_error_message,
                                                 L_soh,
                                                 L_pack_comp_soh,
                                                 L_in_transit_qty,
                                                 L_pack_comp_intran,
                                                 L_tsf_reserved_qty,
                                                 L_pack_comp_resv,
                                                 L_tsf_expected_qty,
                                                 L_pack_comp_exp,
                                                 L_rtv_qty,
                                                 L_non_sellable_qty,
                                                 L_customer_resv,
                                                 L_customer_backorder,
                                                 L_pack_comp_cust_resv,
                                                 L_pack_comp_cust_back,
                                                 I_item,
                                                 I_loc,
                                                 I_loc_type) then
      return FALSE;
   end if;

   if not ITEMLOC_QUANTITY_SQL.GET_TOTAL_DIST_QTY(O_error_message,
                                                  L_dist_qty,
                                                  I_item,
                                                  I_loc,
                                                  I_loc_type,
                                                  NULL) then
      return FALSE;
   end if;

   O_available := GREATEST(L_soh, 0)
                  - (L_tsf_reserved_qty + L_rtv_qty +
                     GREATEST(L_non_sellable_qty, 0) +
                     L_customer_resv + L_customer_backorder);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_LOC_CURRENT_AVAIL;

END ALC_REP_UTIL_SQL;
/