CREATE OR REPLACE PACKAGE ALC_HIER_LVL_INV_SNAPSHOT_SQL AS
------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_IL_SOH(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_CUSTOMER_ORDER(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_ON_ORDER(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_ALLOC_IN(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_ALLOC_OUT(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION ROLLUP_CROSSLINK_IN(O_error_message   IN OUT VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
END ALC_HIER_LVL_INV_SNAPSHOT_SQL;
/
