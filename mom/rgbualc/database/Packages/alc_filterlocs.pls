CREATE OR REPLACE PACKAGE ALC_FILTER_LOC_SQL AS
--------------------------------------------------------------------
FUNCTION FILTER(O_error_message      IN OUT VARCHAR2,
                I_facet_session_id   IN     ALC_SESSION_ITEM_LOC.FACET_SESSION_ID%TYPE,
                I_filter_type        IN     VARCHAR2,
                I_locations          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION RESET_FILTER(O_error_message      IN OUT VARCHAR2,
                      I_facet_session_id   IN     ALC_SESSION_ITEM_LOC.FACET_SESSION_ID%TYPE,
                      I_filter_type        IN     VARCHAR2,
                      I_filter_value       IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------
END ALC_FILTER_LOC_SQL;
/
