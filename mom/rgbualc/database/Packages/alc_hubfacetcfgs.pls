CREATE OR REPLACE PACKAGE ALC_HUB_FACET_CFG_SQL IS
------------------------------------------------------------------------------------------------------------
-- CONSTANTS

ITEM_NODE_ATTRIBUTE       VARCHAR2(25) := 'ITEM';
ITEM_NODE_TYPE_ATTRIBUTE  VARCHAR2(25) := 'ITEM_TYPE';

ITEM_FACET_PARENT         VARCHAR2(25) := 'PARENT';
ITEM_FACET_PARENT_DIFF    VARCHAR2(25) := 'PARENTDIFF';
ITEM_FACET_PACK           VARCHAR2(25) := 'PACK';
ITEM_FACET_SKU            VARCHAR2(25) := 'SKU';

FACET_ATTRIB_WH           VARCHAR2(25) := 'WH';
FACET_ATTRIB_SOURCE_TYPE  VARCHAR2(25) := 'SOURCE_TYPE';
------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_FACETS(O_error_message        IN OUT VARCHAR2,
                     I_facet_session_id     IN     VARCHAR2,
                     I_facet_source_query   IN     VARCHAR2,
                     I_query_name           IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION LOAD_SOURCE(O_error_message        IN OUT VARCHAR2,
                     I_facet_session_id     IN     VARCHAR2,
                     I_facet_source_query   IN     VARCHAR2,
                     I_query_name           IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION FILTER_SOURCE(O_error_message               IN OUT VARCHAR2,
                       I_facet_session_id            IN     VARCHAR2,
                       I_facet_source_query          IN     VARCHAR2,
                       I_query_name                  IN     VARCHAR2,
                       I_disabled_facet_attributes   IN     DISABLED_FACET_ATTRIBUTES_TBL)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ALLOC_SOURCE(O_error_message        IN OUT VARCHAR2,
                             O_new_work_header_id      OUT NUMBER,
                             O_alloc_type              OUT VARCHAR2,
                             I_facet_session_id     IN     VARCHAR2,
                             I_work_header_id       IN     NUMBER,
                             I_items                IN     ALC_VARCHAR_ID_TABLE,
                             I_what_if_ind          IN     NUMBER DEFAULT 0,
                             I_fpg_alloc_ind        IN     NUMBER DEFAULT 0)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_WORK_ITEM_SOURCE_DIFF(O_error_message    IN OUT VARCHAR2,
                                        I_work_header_id   IN     ALC_WORK_ITEM_SOURCE.WORK_HEADER_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
FUNCTION ADD_ITEM_TO_ALLOC_SOURCE(O_error_message          IN OUT VARCHAR2,
                                  O_work_item_source_ids      OUT OBJ_NUMERIC_ID_TABLE,
                                  I_facet_session_id       IN     VARCHAR2,
                                  I_work_header_id         IN     NUMBER,
                                  I_item_nodes             IN     ALC_VARCHAR_ID_TABLE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION GET_NSF_PACK_STYLE_COUNT(O_error_message        IN OUT VARCHAR2,
                                  O_single_style_count      OUT NUMBER,
                                  O_multi_style_count       OUT NUMBER,
                                  I_items                IN     ALC_VARCHAR_ID_TABLE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_FPG_ITEMS(O_error_message   IN OUT VARCHAR2,
                            O_valid              OUT ALC_VARCHAR_ID_TABLE,
                            I_items           IN     ALC_VARCHAR_ID_TABLE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_SESSION_TABLES(O_error_message      IN OUT VARCHAR2,
                              I_facet_session_id   IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION CLOSE_FACET_SESSION(O_error_message      IN OUT VARCHAR2,
                             I_facet_session_id   IN     ALC_SESSION_ITEM_LOC.FACET_SESSION_ID%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_DISABLE_IND (O_error_message      IN OUT VARCHAR2,
                             I_work_header_id     IN     NUMBER,
                             I_facet_session_id   IN     VARCHAR2,
                             I_items              IN     ALC_VARCHAR_ID_TABLE)
RETURN NUMBER;
------------------------------------------------------------------------------------------------------------
END ALC_HUB_FACET_CFG_SQL;
/
