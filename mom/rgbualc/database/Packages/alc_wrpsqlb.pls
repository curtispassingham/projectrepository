CREATE OR REPLACE PACKAGE BODY ALC_WRAPPER_SQL IS

LP_locations          ALC_LOCATION_TBL;
LP_wh_crosslink_ind   SYSTEM_OPTIONS.WH_CROSS_LINK_IND%TYPE;
LP_clearance_ind      VARCHAR2(1);
----------------------------------------------------------------------
FUNCTION GET_WH_CURRENT_AVAIL(I_item    IN ITEM_MASTER.ITEM%TYPE,
                              I_color   IN ITEM_MASTER.ITEM%TYPE,
                              I_wh      IN ITEM_LOC.LOC%TYPE)

RETURN ITEM_LOC_SOH.STOCK_ON_HAND%TYPE IS

   ABORTING          EXCEPTION;
   L_available       NUMBER(20,4)   := 0;
   L_error_message   VARCHAR2(255)  := NULL;


BEGIN

   if I_wh is NOT NULL and I_item is NOT NULL then
      if NOT ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(L_error_message,
                                                        L_available,
                                                        I_item,
                                                        I_wh,
                                                        'W') then
         L_available := -99999999.9999;
      end if;
   end if;

   return L_available;

EXCEPTION
   when ABORTING then
      return -99999999.9999;
   when OTHERS then
      L_error_message := SQLERRM || ' from ALC_WRAPPER_SQL.GET_WH_CURRENT_AVAIL.'||L_error_message;
      return -99999999.9999;
END GET_WH_CURRENT_AVAIL;
--------------------------------------------------------------------
FUNCTION CONVERT_UOM(O_error_message    IN OUT VARCHAR2,
                     O_to_value         IN OUT NUMBER,
                     I_to_uom           IN     UOM_CONVERSION.TO_UOM%TYPE,
                     I_from_value       IN     NUMBER,
                     I_from_uom         IN     UOM_CONVERSION.FROM_UOM%TYPE,
                     I_item             IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                     I_supplier         IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                     I_origin_country   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
RETURN NUMBER IS

BEGIN

   if UOM_SQL.CONVERT(O_error_message,
                      O_to_value,
                      I_to_uom,
                      I_from_value,
                      I_from_uom,
                      I_item,
                      I_supplier,
                      I_origin_country) = FALSE then
      return -1;
   end if;

   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from ALC_WRAPPER_SQL.CONVERT_UOM. '||O_error_message;
      return -1;
END CONVERT_UOM;
----------------------------------------------------------------------------------
FUNCTION CONVERT_CURRENCY(O_error_message       IN OUT VARCHAR2,
                          I_currency_value      IN     NUMBER,
                          I_currency            IN     CURRENCY_RATES.CURRENCY_CODE%TYPE,
                          I_currency_out        IN     CURRENCY_RATES.CURRENCY_CODE%TYPE,
                          O_currency_value      IN OUT NUMBER,
                          I_cost_retail_ind     IN     VARCHAR2,
                          I_effective_date      IN     CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                          I_exchange_type       IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                          I_in_exchange_rate    IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                          I_out_exchange_rate   IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN NUMBER IS

BEGIN

   if CURRENCY_SQL.CONVERT(O_error_message,
                           I_currency_value,
                           I_currency,
                           I_currency_out,
                           O_currency_value,
                           I_cost_retail_ind,
                           I_effective_date,
                           I_exchange_type,
                           I_in_exchange_rate,
                           I_out_exchange_rate) = FALSE then
      return -1;
   end if;

   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM ||' from ALC_WRAPPER_SQL.CONVERT_CURRENCY '||O_error_message;
      return -1;
END CONVERT_CURRENCY;
--------------------------------------------------------------------
FUNCTION GET_PACK_CURRENT_UNIT_RETAIL(I_location   IN ITEM_LOC.LOC%TYPE,
                                      I_loc_type   IN ITEM_LOC.LOC_TYPE%TYPE,
                                      I_item       IN ITEM_MASTER.ITEM%TYPE)
RETURN ITEM_LOC.UNIT_RETAIL%TYPE IS

   ABORTING          EXCEPTION;
   L_error_message   VARCHAR2(200);
   L_unit_retail     ITEM_LOC.UNIT_RETAIL%TYPE;

BEGIN

   if PRICING_ATTRIB_SQL.BUILD_PACK_RETAIL(L_error_message,
                                           L_unit_retail,
                                           I_item,
                                           I_loc_type,
                                           I_location) = FALSE then
      return -1;
   end if;

   return L_unit_retail;

EXCEPTION
   when ABORTING then
      return -1;
   when OTHERS then
      L_error_message := SQLERRM || ' from ALC_WRAPPER_SQL.GET_PACK_CURRENT_UNIT_RETAIL. '||L_error_message;
      return -2;
END GET_PACK_CURRENT_UNIT_RETAIL;
--------------------------------------------------------------------
/* : REFRESH function is used to refresh available quantity. */
-------------------------------------------------------------------
FUNCTION REFRESH(O_error_message   IN OUT VARCHAR2,
                 I_wk_id           IN     ALC_WORK_ITEM_SOURCE.WORK_HEADER_ID%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(100) := 'ALC_WRAPPER_SQL.REFRESH';
   L_start_time   TIMESTAMP     := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   /* Source type = WH */
   merge into alc_work_item_source target
   using (select GET_WH_CURRENT_AVAIL(awis.item, 0, awis.wh) + NVL((select SUM(ald.qty_allocated)
                                                                      from alloc_header alh,
                                                                           alloc_detail ald,
                                                                           alc_xref alx ,
                                                                           item_loc_soh il
                                                                     where alx.alloc_id = 0
                                                                       and alh.alloc_no = alx.xref_alloc_no
                                                                       and ald.alloc_no = alh.alloc_no
                                                                       and alh.item     = awis.item
                                                                       and alh.wh       = il.loc
                                                                       and alh.order_no IS NULL
                                                                     group by alh.item ),0) avail_qty,
                  awis.item,
                  awis.work_header_id,
                  awis.wh,
                  DECODE(im.status,
                         'A','N',
                         'Y') deleted_ind
             from alc_work_item_source awis,
                  item_master im,
                  item_loc_soh ils
            where im.item             = awis.item
              and awis.work_header_id = I_wk_id
              and im.item             = ils.item
              and ils.loc             = awis.wh
              and ils.loc_type        = 'W'
              and awis.source_type    = 'OH'
              and awis.refresh_ind    = 'Y'
              and awis. item_type    != 'STYLE') source
   on (    target.item           = source.item
       and target.work_header_id = source.work_header_id
       and target.wh             = source.wh
       and target.source_type    = 'OH'
       and target.refresh_ind    = 'Y')
   when MATCHED then
      update
         set target.available_quantity = source.avail_qty,
             target.deleted_ind        = source.deleted_ind;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE OH - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   /* Source type = PO  might need to add code for pack items*/
   merge into alc_work_item_source target
   using (select /*+ INDEX(d, PK_ITEM_MASTER) INDEX(ol, PK_ORDLOC) */
                 (NVL(ol.qty_ordered,0) - NVL(ol.qty_received,0)) -
                  NVL((select NVL(SUM(ald.qty_allocated),0) - NVL(SUM(ald.po_rcvd_qty),0)
                         from alloc_header alh,
                              alloc_detail ald,
                              alc_xref alx
                        where alx.alloc_id != 0
                          and alh.alloc_no  = alx.xref_alloc_no
                          and ald.alloc_no  = alh.alloc_no
                          and alh.item      = d.item
                          and alh.order_no  = o.order_no
                          and alh.wh        = ol.location
                        group by alh.item,
                                 alh.order_no), 0) +
                  NVL((select SUM(ald.po_rcvd_qty)
                         from alloc_header alh,
                              alloc_detail ald,
                              alc_xref alx
                        where alx.alloc_id = 0
                          and alh.alloc_no = alx.xref_alloc_no
                          and ald.alloc_no = alh.alloc_no
                          and alh.item     = d.item
                          and alh.order_no = o.order_no
                          and alh.wh       = ol.location
                        group by alh.item,
                                 alh.order_no), 0) avail_qty,
                  awis.item,
                  awis.work_header_id,
                  awis.wh,
                  awis.doc_no,
                  DECODE(d.status,
                         'A', 'N',
                         'Y') deleted_ind
            from item_master d,
                 ordloc ol,
                 ordhead o,
                 wh,
                 ordsku os,
                 alc_work_item_source awis
           where ol.item             = d.item
             and os.item             = d.item
             and d.item              = awis.item
             and ol.order_no         = o.order_no
             and os.order_no         = o.order_no
             and o.order_type       != 'ARB'
             and o.status            = 'A'
             and ol.loc_type         = 'W'
             and ol.location         = wh.wh
             and wh.wh               = awis.wh
             and wh.finisher_ind     = 'N'
             and o.order_no          = awis.doc_no
             and awis.source_type    = 'PO'
             and awis.work_header_id = I_wk_id) source
       on (    target.item           = source.item
           and target.work_header_id = source.work_header_id
           -- wh and doc no
           and target.wh             = source.wh
           and target.doc_no         = source.doc_no
           and target.source_type    = 'PO'
           and target.refresh_ind    = 'Y')
   when MATCHED then
      update
         set target.available_quantity = source.avail_qty,
             target.deleted_ind        = source.deleted_ind;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE PO - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   /* Source type = ASN */

   merge into alc_work_item_source target
   using (-- ASN Section #1
          select /*+ INDEX(im, PK_ITEM_MASTER) */
                 DISTINCT im1.item,
                 asn_inner.asn order_no,
                 NVL(asn_inner.qty_expected, 0) - NVL(asn_inner.qty_received, 0) -
                 --already allocated through ASN
                 NVL((select SUM(ald.qty_allocated)
                             from alloc_header alh,
                                  alloc_detail ald,
                                  alc_xref alx
                            where alx.alloc_id != 0
                              and alh.alloc_no = alx.xref_alloc_no
                              and ald.alloc_no = alh.alloc_no
                              and alh.item     = im1.item
                              and alh.doc_type = 'ASN'
                              and alh.doc      = asn_inner.asn
                              and alh.wh       = ol.location
                            group by alh.item,
                                     alh.doc),0) -
                  --already allocated through PO
                  NVL((select SUM(ald.qty_allocated)
                         from alloc_header alh,
                              alloc_detail ald,
                              alc_xref alx
                        where alh.alloc_no    = alx.xref_alloc_no
                          and ald.alloc_no    = alh.alloc_no
                          and alh.item        = im1.item
                          and alh.doc_type    = 'PO'
                          and alh.order_no    = ol.order_no
                          and alh.wh          = ol.location
                        group by alh.item,
                                 alh.order_no),0) avail_qty,
                 asn_inner.work_header_id,
                 DECODE(im1.status,
                        'A','N',
                        'Y') deleted_ind,
                 asn_inner.to_loc wh
            from item_master im1,
                 item_master im2,
                 packitem_breakout pb,
                 ordloc ol,
                 (select DISTINCT
                         ss.shipment shipment,
                         s.order_no,
                         s.asn,
                         awis.work_header_id,
                         ss.item item,
                         sl.to_loc,
                         s.to_loc_type,
                         SUM(sl.qty_expected) OVER (PARTITION BY ss.shipment,
                                                                 awis.item,
                                                                 sl.to_loc,
                                                                 s.asn)  qty_expected,
                         SUM(sl.qty_received) OVER (PARTITION BY ss.shipment,
                                                                 awis.item,
                                                                 sl.to_loc,
                                                                 s.asn) qty_received
                    from shipsku ss,
                         shipment s,
                         shipsku_loc sl,
                         (select DISTINCT awis.item_id item,
                                 awis.source_type,
                                 awis.doc_no,
                                 awis.work_header_id,
                                 awis.wh
                            from alc_work_item_source awis
                           where awis.work_header_id = I_wk_id
                             and awis.source_type    = 'ASN'
                             and awis.refresh_ind    = 'Y') awis
                   where ss.shipment      = s.shipment
                     and ss.shipment      = sl.shipment
                     and s.asn            = awis.doc_no
                     and ss.item          = awis.item
                     and ss.item          = sl.item
                     and sl.to_loc        = awis.wh
                     and s.to_loc_type    = 'W'
                     and s.status_code    = 'I') asn_inner
        where im1.item           = asn_inner.item
          and im1.item           = pb.pack_no
          and im2.item           = pb.item
          and asn_inner.to_loc   = ol.location
          and asn_inner.order_no = ol.order_no
          and ((     im2.tran_level = 1
                 and im2.item_level = 1
                 and im2.item_aggregate_ind = 'N')
                or (    im2.tran_level = 2
                    and EXISTS (select 'x'
                                  from item_master i2
                                 where i2.item_aggregate_ind = 'N'
                                   and i2.item               = im2.item_parent))
                or (    im2.tran_level = 3
                    and EXISTS (select 'x'
                                  from item_master i3
                                 where item_aggregate_ind = 'N'
                                   and i3.item            = im2.item_grandparent)))
          and im1.item_aggregate_ind = 'N'
          and im1.pack_ind           = 'Y'
          and im1.sellable_ind       = 'N'
          union all 
          -- ASN Section #2
          select /*+ INDEX(im, PK_ITEM_MASTER) */
                 DISTINCT im.item,
                 asn_inner.asn order_no,
                 NVL(asn_inner.qty_expected, 0) - NVL(asn_inner.qty_received, 0) -
                 --already allocated through ASN
                 NVL((select SUM(ald.qty_allocated)
                             from alloc_header alh,
                                  alloc_detail ald,
                                  alc_xref alx
                            where alx.alloc_id != 0
                              and alh.alloc_no = alx.xref_alloc_no
                              and ald.alloc_no = alh.alloc_no
                              and alh.item     = im.item
                              and alh.doc_type = 'ASN'
                              and alh.doc      = asn_inner.asn
                              and alh.wh       = ol.location
                            group by alh.item,
                                     alh.doc),0) -
                  --already allocated through PO
                  NVL((select SUM(ald.qty_allocated)
                         from alloc_header alh,
                              alloc_detail ald,
                              alc_xref alx
                        where alh.alloc_no    = alx.xref_alloc_no
                          and ald.alloc_no    = alh.alloc_no
                          and alh.item        = im.item
                          and alh.doc_type    = 'PO'
                          and alh.order_no    = ol.order_no
                          and alh.wh          = ol.location
                        group by alh.item,
                                 alh.order_no),0) avail_qty,
                 asn_inner.work_header_id,
                 DECODE(im.status,
                        'A','N',
                        'Y') deleted_ind,
                 asn_inner.to_loc wh
            from item_master im,
                 ordloc ol,
                 (select DISTINCT
                         ss.shipment shipment,
                         s.order_no,
                         s.asn,
                         awis.work_header_id,
                         ss.item item,
                         sl.to_loc,
                         s.to_loc_type,
                         SUM(sl.qty_expected) OVER (PARTITION BY ss.shipment,
                                                                 awis.item,
                                                                 sl.to_loc,
                                                                 s.asn)  qty_expected,
                         SUM(sl.qty_received) OVER (PARTITION BY ss.shipment,
                                                                 awis.item,
                                                                 sl.to_loc,
                                                                 s.asn) qty_received
                    from shipsku ss,
                         shipment s,
                         shipsku_loc sl,
                         (select DISTINCT awis.item_id item,
                                 awis.source_type,
                                 awis.doc_no,
                                 awis.work_header_id, 
                                 awis.wh
                            from alc_work_item_source awis
                           where awis.work_header_id = I_wk_id
                             and awis.source_type    = 'ASN'
                             and awis.refresh_ind    = 'Y') awis
                   where ss.shipment      = s.shipment
                     and ss.shipment      = sl.shipment
                     and s.asn            = awis.doc_no
                     and ss.item          = awis.item
                     and ss.item          = sl.item
                     and sl.to_loc        = awis.wh
                     and s.to_loc_type    = 'W'
                     and s.status_code    = 'I') asn_inner
        where asn_inner.item     = im.item
          and asn_inner.to_loc   = ol.location
          and asn_inner.order_no = ol.order_no
          and im.item_level      = im.tran_level
          and (    im.pack_ind = 'N' 
               or (    im.pack_ind = 'Y'
                   and im.sellable_ind = 'Y'))
          union all 
          -- ASN Section #3
          select /*+ INDEX(im, PK_ITEM_MASTER) */
                 DISTINCT im.item,
                 asn_inner.asn order_no,
                 NVL(asn_inner.qty_expected, 0) - NVL(asn_inner.qty_received, 0) -
                 --already allocated through ASN
                 NVL((select SUM(ald.qty_allocated)
                             from alloc_header alh,
                                  alloc_detail ald,
                                  alc_xref alx
                            where alx.alloc_id != 0
                              and alh.alloc_no = alx.xref_alloc_no
                              and ald.alloc_no = alh.alloc_no
                              and alh.item     = im.item
                              and alh.doc_type = 'ASN'
                              and alh.doc      = asn_inner.asn
                              and alh.wh       = ol.location
                            group by alh.item,
                                     alh.doc),0) -
                  --already allocated through PO
                  NVL((select SUM(ald.qty_allocated)
                         from alloc_header alh,
                              alloc_detail ald,
                              alc_xref alx
                        where alh.alloc_no    = alx.xref_alloc_no
                          and ald.alloc_no    = alh.alloc_no
                          and alh.item        = im.item
                          and alh.doc_type    = 'PO'
                          and alh.order_no    = ol.order_no
                          and alh.wh          = ol.location
                        group by alh.item,
                                 alh.order_no),0) avail_qty,
                 asn_inner.work_header_id,
                 DECODE(im.status,
                        'A','N',
                        'Y') deleted_ind,
                 asn_inner.to_loc wh
            from item_master im,
                 item_master parent,
                 item_master sku,
                 packitem_breakout pb,
                 ordloc ol,
                 (select DISTINCT
                         ss.shipment shipment,
                         s.order_no,
                         s.asn,
                         awis.work_header_id,
                         ss.item item,
                         sl.to_loc,
                         s.to_loc_type,
                         SUM(sl.qty_expected) OVER (PARTITION BY ss.shipment,
                                                                 awis.item,
                                                                 sl.to_loc,
                                                                 s.asn)  qty_expected,
                         SUM(sl.qty_received) OVER (PARTITION BY ss.shipment,
                                                                 awis.item,
                                                                 sl.to_loc,
                                                                 s.asn) qty_received
                    from shipsku ss,
                         shipment s,
                         shipsku_loc sl,
                         (select DISTINCT awis.item_id item,
                                 awis.source_type,
                                 awis.doc_no,
                                 awis.work_header_id,
                                 awis.wh
                            from alc_work_item_source awis
                           where awis.work_header_id = I_wk_id
                             and awis.source_type    = 'ASN'
                             and awis.refresh_ind    = 'Y') awis
                   where ss.shipment      = s.shipment
                     and ss.shipment      = sl.shipment
                     and s.asn            = awis.doc_no
                     and ss.item          = awis.item
                     and ss.item          = sl.item
                     and sl.to_loc        = awis.wh
                     and s.to_loc_type    = 'W'
                     and s.status_code    = 'I') asn_inner
        where asn_inner.item     = im.item
          and asn_inner.item     = pb.pack_no
          and pb.item            = sku.item
          and asn_inner.to_loc   = ol.location
          and asn_inner.order_no = ol.order_no
          and im.sellable_ind    = 'N'
          and im.pack_ind        = 'Y'
          and ((        sku.item_parent = parent.item
                    and parent.item_aggregate_ind = 'Y')
                or (    sku.item_grandparent = parent.item 
                    and parent.item_aggregate_ind = 'Y'))
          ) source
   on (    target.item           = source.item
       and target.work_header_id = source.work_header_id
       and target.wh             = source.wh
       and target.source_type    = 'ASN'
       and target.doc_no         = source.order_no
       and target.refresh_ind    = 'Y')
   when MATCHED then
      update
         set target.available_quantity = source.avail_qty,
             target.deleted_ind        = source.deleted_ind;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE ASN - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   /* Source_type=BOL */

   merge into alc_work_item_source target
   using (select /*+ INDEX(d, PK_ITEM_MASTER) */
                 DISTINCT d.item,
                 s.bol_no order_no,
                 ss.qty_expected - NVL(ss.qty_received,0) - NVL((select SUM(ald.qty_allocated)
                                                                   from alloc_header alh,
                                                                        alloc_detail ald,
                                                                        alc_xref alx
                                                                  where alx.alloc_id != 0
                                                                    and alh.alloc_no  = alx.xref_alloc_no
                                                                    and ald.alloc_no  = alh.alloc_no
                                                                    and ald.alloc_no  = alh.alloc_no
                                                                    and alh.item      = d.item
                                                                    and alh.doc_type  = 'BOL'
                                                                    and alh.doc       = s.bol_no
                                                                    and alh.wh        = th.to_loc
                                                                  group by alh.item,
                                                                           alh.doc),0) avail_qty ,
                  awis.work_header_id ,
                  DECODE(d.status,
                         'A', 'N',
                         'Y') deleted_ind
             from item_master d,
                  item_master im1,
                  packitem_breakout pb,
                  shipment s,
                  shipsku ss,
                  wh,
                  tsfhead th,
                  tsfdetail td ,
                  (select *
                     from alc_work_item_source
                    where source_type    = 'BOL'
                      and refresh_ind    = 'Y'
                      and work_header_id = I_wk_id) awis
            where ss.item = d.item
              and td.item = d.item
              and d.item  = pb.pack_no
              and pb.item = im1.item
              and d.item  = awis.item
              and ((     im1.tran_level = 1
                     and im1.item_level = 1
                     and im1.item_aggregate_ind = 'N')
                 or (    im1.tran_level = 2
                     and im1.item_parent IN (select item
                                               from item_master
                                              where item_aggregate_ind = 'N'))
                 or (    im1.tran_level = 3
                     and im1.item_grandparent IN (select item
                                                    from item_master
                                                   where item_aggregate_ind = 'N')))
              and d.item_aggregate_ind = 'N'
              and d.pack_ind           = 'Y'
              and d.sellable_ind       = 'N'
              and s.to_loc_type        = 'W'
              and s.bol_no             = awis.doc_no
              and th.tsf_no            = ss.distro_no
              and th.tsf_no            = td.tsf_no
              and ss.distro_type       = 'T'
              and s.shipment           = ss.shipment
              and th.to_loc            = wh.wh
              and wh.wh                = awis.wh
              and s.to_loc             = wh.physical_wh
            union all
            -- BOL Section #2
            select /*+ INDEX(d, PK_ITEM_MASTER) */
                   DISTINCT d.item,
                   s.bol_no order_no,
                   ss.qty_expected - NVL(ss.qty_received,0) - NVL((select SUM(ald.qty_allocated)
                                                                     from alloc_header alh,
                                                                          alloc_detail ald,
                                                                          alc_xref alx
                                                                    where alx.alloc_id != 0
                                                                      and alh.alloc_no  = alx.xref_alloc_no
                                                                      and ald.alloc_no  = alh.alloc_no
                                                                      and alh.item      = d.item
                                                                      and alh.doc_type  = 'BOL'
                                                                      and alh.doc       = s.bol_no
                                                                      and alh.wh        = th.to_loc
                                                                    group by alh.item,
                                                                             alh.doc),0) avail_qty,
                   awis.work_header_id,
                   DECODE(d.status,
                          'A', 'N',
                          'Y') deleted_ind
              from item_master d,
                   shipment s,
                   shipsku ss,
                   wh,
                   tsfhead th,
                   tsfdetail td ,
                   (select *
                      from alc_work_item_source
                     where source_type    = 'BOL'
                       and refresh_ind    = 'Y'
                       and work_header_id = I_wk_id) awis
             where ss.item      = d.item
               and td.item      = d.item
               and d.item       = awis.item
               and d.item_level = d.tran_level
               and (d.pack_ind = 'N' or (d.pack_ind = 'Y' and d.sellable_ind = 'Y'))
               and s.to_loc_type  = 'W'
               and s.bol_no       = awis.doc_no
               and th.tsf_no      = ss.distro_no
               and th.tsf_no      = td.tsf_no
               and ss.distro_type = 'T'
               and s.shipment     = ss.shipment
               and th.to_loc      = wh.wh
               and wh.wh          = awis.wh
               and s.to_loc       = wh.physical_wh
            union all
            -- BOL Section #3
            select DISTINCT d.item,
                   s.bol_no order_no,
                   ss.qty_expected - NVL(ss.qty_received,0) - NVL((select SUM(ald.qty_allocated)
                                                                     from alloc_header alh,
                                                                          alloc_detail ald,
                                                                          alc_xref alx
                                                                    where alx.alloc_id != 0
                                                                      and alh.alloc_no = alx.xref_alloc_no
                                                                      and ald.alloc_no = alh.alloc_no
                                                                      and alh.item     = d.item
                                                                      and alh.doc_type = 'BOL'
                                                                      and alh.doc      = s.bol_no
                                                                      and alh.wh       = th.to_loc
                                                                    group by alh.item,
                                                                             alh.doc),0) avail_qty,
                   awis.work_header_id,
                   DECODE(d.status,
                          'A','N',
                          'Y') deleted_ind
              from item_master d, -- pack
                   item_master parent,
                   item_master sku,
                   packitem_breakout pb,
                   shipment s,
                   shipsku ss,
                   wh,
                   tsfhead th,
                   tsfdetail td ,
                   (select *
                      from alc_work_item_source
                     where source_type    = 'BOL'
                       and refresh_ind    = 'Y'
                       and work_header_id = I_wk_id) awis
             where pb.pack_no     = d.item
               and ss.shipment    = s.shipment
               and d.item         = td.item
               and d.item         = awis.item
               and pb.pack_no     = ss.item
               and pb.item        = sku.item
               and d.sellable_ind = 'N'
               and d.pack_ind     = 'Y'
               and pb.pack_no     = ss.item
               and s.to_loc_type  = 'W'
               and (( sku.item_parent = parent.item and parent.item_aggregate_ind = 'Y')
                  or (sku.item_grandparent = parent.item and parent.item_aggregate_ind = 'Y'))
               and s.bol_no       = awis.doc_no
               and th.tsf_no      = ss.distro_no
               and th.tsf_no      = td.tsf_no
               and ss.distro_type = 'T'
               and s.shipment     = ss.shipment
               and th.to_loc      = wh.wh
               and wh.wh          = awis.wh
               and s.to_loc       = wh.physical_wh
             group by d.item,
                   th.to_loc,
                   s.bol_no,
                   ss.qty_expected - NVL(ss.qty_received,0),
                   ss.distro_no,
                   awis.work_header_id,
                   d.status  ) source
   on (    target.item           = source.item
       and target.work_header_id = source.work_header_id
       and target.source_type    = 'BOL'
       and target.doc_no         = source.order_no
       and target.refresh_ind    = 'Y')
   when MATCHED then
      update
         set target.available_quantity = source.avail_qty,
             target.deleted_ind        = source.deleted_ind;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE BOL - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   /* Source_type=TSF */

   merge into alc_work_item_source target
   using (select /*+ INDEX(d, PK_ITEM_MASTER) */
                 DISTINCT d.item,
                 TO_CHAR(t.tsf_no) order_no,
                 (NVL(td.tsf_qty,0) - (NVL(td.received_qty,0) + NVL(td.cancelled_qty,0))) - NVL((select SUM(ald.qty_allocated)
                                                                                                   from alloc_header alh,
                                                                                                        alloc_detail ald,
                                                                                                        alc_xref alx
                                                                                                  where alx.alloc_id != 0
                                                                                                    and alh.alloc_no  = alx.xref_alloc_no
                                                                                                    and ald.alloc_no  = alh.alloc_no
                                                                                                    and alh.item      = d.item
                                                                                                    and alh.order_no  = t.tsf_no
                                                                                                    and alh.wh        = t.to_loc
                                                                                                  group by alh.item,
                                                                                                           alh.order_no),0) avail_qty,
                 awis.work_header_id,
                 DECODE(d.status,
                        'A', 'N',
                        'Y') deleted_ind
            from item_master d,
                 item_master im1,
                 wh wh,
                 packitem_breakout pb,
                 tsfhead t,
                 tsfdetail td,
                 (select *
                    from alc_work_item_source
                   where source_type    = 'TSF'
                     and refresh_ind    = 'Y'
                     and work_header_id = I_wk_id) awis
           where td.item   = d.item
             and td.tsf_no = t.tsf_no
             and d.item    = pb.pack_no
             and d.item    = awis.item
             and wh.wh     = t.to_loc
             and wh.wh     = awis.wh
             and pb.item   = im1.item
             and ((     im1.tran_level = 1
                    and im1.item_level = 1
                    and im1.item_aggregate_ind = 'N')
                or (    im1.tran_level = 2
                    and im1.item_parent IN (select item
                                              from item_master
                                             where item_aggregate_ind = 'N'))
                or (    im1.tran_level = 3
                    and im1.item_grandparent IN (select item
                                                   from item_master
                                                  where item_aggregate_ind = 'N')))
             and d.item_aggregate_ind = 'N'
             and d.pack_ind           = 'Y'
             and d.sellable_ind       = 'N'
             and t.from_loc_type      = 'W'
             and t.to_loc_type        = 'W'
             and t.tsf_no             = awis.doc_no
             and t.tsf_no             = td.tsf_no
             and t.tsf_type           IN ('SR','CF','AD','MR','EG','AIP','IC')
             and t.status             = 'A'
           union all
           -- TSF Section #2
           select /*+ INDEX(d, PK_ITEM_MASTER) */
                  DISTINCT d.item,
                  TO_CHAR(t.tsf_no) order_no,
                  (NVL(td.tsf_qty,0) - (NVL(td.received_qty,0) + NVL(td.cancelled_qty,0))) - NVL((select SUM(ald.qty_allocated)
                                                                                                    from alloc_header alh,
                                                                                                         alloc_detail ald,
                                                                                                         alc_xref alx
                                                                                                   where alx.alloc_id != 0
                                                                                                     and alh.alloc_no  = alx.xref_alloc_no
                                                                                                     and ald.alloc_no  = alh.alloc_no
                                                                                                     and alh.item      = d.item
                                                                                                     and alh.order_no  = t.tsf_no
                                                                                                     and alh.wh        = t.to_loc
                                                                                                   group by alh.item,
                                                                                                            alh.order_no),0) avail_qty ,
                  awis.work_header_id,
                  DECODE(d.status,
                         'A', 'N',
                         'Y') deleted_ind
             from item_master d,
                  wh wh,
                  tsfhead t,
                  tsfdetail td,
                  (select *
                     from alc_work_item_source
                    where source_type    = 'TSF'
                      and refresh_ind    = 'Y'
                      and work_header_id = I_wk_id) awis
            where td.item      = d.item
              and td.tsf_no    = t.tsf_no
              and d.item       = awis.item
              and wh.wh        = t.to_loc
              and wh.wh        = awis.wh
              and d.item_level = d.tran_level
              and (d.pack_ind = 'N' or (d.pack_ind = 'Y' and d.sellable_ind = 'Y'))
              and t.from_loc_type = 'W'
              and t.to_loc_type   = 'W'
              and t.tsf_no        = awis.doc_no
              and t.tsf_no        = td.tsf_no
              and t.from_loc_type = 'W'
              and t.to_loc_type   = 'W'
              and t.tsf_type      IN ('SR','CF','AD','MR','EG','AIP','IC')
              and t.status        = 'A'
           union all
           -- TSF Section #3
           select DISTINCT d.item,
                  TO_CHAR(t.tsf_no) order_no,
                  (NVL(td.tsf_qty,0) - (NVL(td.received_qty,0) + NVL(td.cancelled_qty,0))) - NVL((select SUM(ald.qty_allocated)
                                                                                                    from alloc_header alh,
                                                                                                         alloc_detail ald,
                                                                                                         alc_xref alx
                                                                                                   where alx.alloc_id != 0
                                                                                                     and alh.alloc_no  = alx.xref_alloc_no
                                                                                                     and ald.alloc_no  = alh.alloc_no
                                                                                                     and alh.item      = d.item
                                                                                                     and alh.order_no  = t.tsf_no
                                                                                                     and alh.wh        = t.to_loc
                                                                                                   group by alh.item,
                                                                                                            alh.order_no),0) avail_qty,
                  awis.work_header_id,
                  DECODE(d.status,
                         'A', 'N',
                         'Y') deleted_ind
             from item_master d, -- pack
                  item_master parent,
                  wh wh,
                  item_master sku,
                  packitem_breakout pb,
                  tsfhead t,
                  tsfdetail td,
                  (select *
                     from alc_work_item_source
                    where source_type    = 'TSF'
                      and refresh_ind    = 'Y'
                      and work_header_id = I_wk_id) awis
            where pb.pack_no      = d.item
              and td.tsf_no       = t.tsf_no
              and d.item          = td.item
              and d.item          = awis.item
              and wh.wh           = t.to_loc
              and wh.wh           = awis.wh
              and pb.pack_no      = td.item
              and pb.item         = sku.item
              and d.sellable_ind  = 'N'
              and d.pack_ind      = 'Y'
              and pb.pack_no      = td.item
              and t.from_loc_type = 'W'
              and t.to_loc_type   = 'W'
              and (( sku.item_parent = parent.item and parent.item_aggregate_ind = 'Y')
                 or (sku.item_grandparent = parent.item and parent.item_aggregate_ind = 'Y'))
              and t.tsf_no        = awis.doc_no
              and t.tsf_no        = td.tsf_no
              and t.from_loc_type = 'W'
              and t.to_loc_type   = 'W'
              and t.tsf_type      IN ('SR','CF','AD','MR','EG','AIP','IC')
              and t.status        = 'A') source
   on (    target.item           = source.item
       and target.work_header_id = source.work_header_id
       and target.source_type    = 'TSF'
       and target.doc_no         = source.order_no
       and target.refresh_ind    = 'Y')
   when MATCHED then
      update
         set target.available_quantity = source.avail_qty,
             target.deleted_ind        = source.deleted_ind;

   LOGGER.LOG_INFORMATION(L_program||' Merge ALC_WORK_ITEM_SOURCE TSF - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   /* Delete the records with flag DELETED_IND from alc_work_item_source */
   delete alc_work_item_source
    where work_header_id = I_wk_id
      and (deleted_ind   = 'Y' or (available_quantity < = 0 and item_type != ALC_CONSTANTS_SQL.PACKCOMP));

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_ITEM_SOURCE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from alc_work_item_source wis
    where wis.work_header_id = I_wk_id
      and wis.item_type      = ALC_CONSTANTS_SQL.FASHIONITEM
      and NOT EXISTS (select 'x'
                        from alc_work_item_source wis2
                       where wis2.ancestor_id = wis.work_item_source_id);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_ITEM_SOURCE - FA - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   delete from alc_work_item_source wis
    where wis.work_header_id = I_wk_id
      and wis.item_type      = ALC_CONSTANTS_SQL.STYLE
      and NOT EXISTS (select 'x'
                        from alc_work_item_source wis2
                       where wis2.ancestor_id = wis.work_item_source_id);

   LOGGER.LOG_INFORMATION(L_program||' Delete ALC_WORK_ITEM_SOURCE - STYLE - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQLERRM || ' FROM ' || L_program;
      return 0;

END REFRESH;
--------------------------------------------------------------------
FUNCTION GET_PRICING_INFO(O_error_msg            OUT VARCHAR2,
                          O_prices               OUT ALC_RETAIL_INFO_TBL,
                          I_search_criteria   IN     OBJ_PRICE_INQ_SEARCH_TBL)
RETURN NUMBER IS

   L_program      VARCHAR2(100) := 'ALC_WRAPPER_SQL.GET_PRICING_INFO';
   L_prc_inq_tbl  OBJ_PRC_INQ_TBL;
   L_start_time   TIMESTAMP := SYSTIMESTAMP;

   L_suom_future_retail    ITEM_LOC.SELLING_UNIT_RETAIL%TYPE := NULL;
   L_suom_current_retail   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE := NULL;

   L_retail_rec            ALC_RETAIL_INFO_REC;

   cursor C_RETAIL_LOOP is
      select input.item,
             input.loc,
             I_search_criteria(1).effective_date effective_date,
             --
             prc.selling_retail,
             prc.selling_uom,
             prc.selling_retail_currency,
             --
             il.selling_unit_retail il_retail,
             il.selling_uom il_uom,
             l.currency_code il_currency,
             --
             im.standard_uom,
             il.primary_supp,
             il.primary_cntry
        from (select value(input_items) item,
                     value(input_locs) loc
                from table(cast(I_search_criteria(1).items as OBJ_VARCHAR_ID_TABLE)) input_items,
                     table(cast(I_search_criteria(1).locations as OBJ_NUMERIC_ID_TABLE)) input_locs) input,
             table(cast(L_prc_inq_tbl as OBJ_PRC_INQ_TBL)) prc,
             item_loc il,
             item_master im,
             (select s.store loc,
                     s.currency_code
                from store s
              union all
              select w.wh loc,
                     w.currency_code
                from wh w) l
       where input.item = il.item(+)
         and input.loc  = il.loc(+)
         and input.item = prc.item(+)
         and input.loc  = prc.location(+)
         and input.item = im.item
         and input.loc  = l.loc;


BEGIN

   O_prices := ALC_RETAIL_INFO_TBL();

   -- Call RPM_PRICE_INQUIRY_SQL package to get the retail prices
   if RPM_PRICE_INQUIRY_SQL.GET_PRICE_INQ_VO(O_error_msg,
                                             L_prc_inq_tbl,
                                             I_search_criteria) = 0 then
      return 0;
   end if;

   for rec in C_RETAIL_LOOP loop

      if rec.selling_retail is NOT NULL and rec.selling_uom != rec.standard_uom then
         if UOM_SQL.CONVERT(O_error_msg,
                            L_suom_future_retail,
                            rec.standard_uom,
                            rec.selling_retail,
                            rec.selling_uom,
                            rec.item,
                            rec.primary_supp,
                            rec.primary_cntry) = FALSE then
            return 0;
         end if;
      else
         L_suom_future_retail := rec.selling_retail;
      end if;

      if rec.il_retail is NOT NULL and rec.il_uom != rec.standard_uom then
         if UOM_SQL.CONVERT(O_error_msg,
                            L_suom_current_retail,
                            rec.standard_uom,
                            rec.il_retail,
                            rec.il_uom,
                            rec.item,
                            rec.primary_supp,
                            rec.primary_cntry) = FALSE then
            return 0;
         end if;
      else
         L_suom_current_retail := rec.il_retail;
      end if;

      L_retail_rec := ALC_RETAIL_INFO_REC(rec.item,                     --ITEM
                                          rec.loc,                      --LOCATION
                                          rec.effective_date,           --ACTION_DATE
                                          L_suom_future_retail,         --ACTION_SELLING_RETAIL
                                          rec.standard_uom,             --ACTION_SELLING_UOM
                                          rec.selling_retail_currency,  --ACTION_SELLING_RETAIL_CURRENCY
                                          L_suom_current_retail,        --CURR_SELLING_RETAIL
                                          rec.standard_uom,             --CURR_SELLING_UOM
                                          rec.il_currency);             --CURR_SELLING_RETAIL_CURRENCY
      O_prices.extend;
      O_prices(O_prices.COUNT) := L_retail_rec;

   end loop;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);


   return 1;

EXCEPTION
   when OTHERS then
     O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                       SQLERRM,
                                       L_program,
                                       TO_CHAR(SQLCODE));
   return 0;
END GET_PRICING_INFO;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_FRANCHISE_CREDIT(O_error_message   IN OUT VARCHAR2,
                                   I_alloc_id        IN ALC_ALLOC.ALLOC_ID%TYPE)
RETURN NUMBER IS

   L_program                   VARCHAR2 (50):= 'ALC_WRAPPER_SQL.VALIDATE_FRANCHISE_CREDIT';
   L_no_of_failed_credit_check INT;

BEGIN

   select COUNT(1)
     into L_no_of_failed_credit_check
     from wf_customer wfc,
          (select s.wf_customer_id cust_id,
                  s.store cust_store
             from store s,
                  (select DISTINCT location_id
                     from alc_item_loc
                    where alloc_id = I_alloc_id) alc_loc
            where s.store          = alc_loc.location_id
              and s.store_type       = 'F'
              and s.stockholding_ind = 'Y') cust
    where wfc.wf_customer_id = cust.cust_id
      and wfc.credit_ind     = 'N';

   if L_no_of_failed_credit_check = 0 then
      return 1;
   else
      return 0;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END VALIDATE_FRANCHISE_CREDIT;
-------------------------------------------------------------------------------
FUNCTION CREATE_MOD_LOCPO(O_error_message     IN OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                          O_locpotsfref_rec   OUT NOCOPY  "RIB_LocPOTsfRef_REC",
                          I_locpodesc_rec     IN          "RIB_LocPOTsfDesc_REC",
                          I_action            IN          VARCHAR2)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_WRAPPER_SQL.CREATE_MOD_LOCPO';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   if CORESVC_STOREORDER.CREATE_MOD_LOCPO(O_error_message,
                                          O_locpotsfref_rec,
                                          I_locpodesc_rec,
                                          I_action) = FALSE then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END CREATE_MOD_LOCPO;
-------------------------------------------------------------------------------
END ALC_WRAPPER_SQL;
/
