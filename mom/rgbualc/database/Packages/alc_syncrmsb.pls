CREATE OR REPLACE PACKAGE BODY ALC_SYNC_RMS_SQL AS
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RMS_ALLOC_NOS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_rms_alloc_nos      OUT ID_TBL,
                           I_alc_alloc_ids   IN     ID_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_WORKING_TABLES(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_alc_alloc_ids         IN     ID_TBL,
                            I_status                IN     ALLOC_HEADER.STATUS%TYPE,
                            I_alc_sync_process_id   IN     ALC_SYNC_HEADER_TEMP.ALC_SYNC_PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION ITEM_LOC_RANGE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_alc_sync_process_id   IN     ALC_SYNC_HEADER_TEMP.ALC_SYNC_PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_RESV_EXP(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_allocs           IN     ID_TBL,
                         I_add_delete_ind   IN     VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_TABLES(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alc_sync_process_id   IN     ALC_SYNC_HEADER_TEMP.ALC_SYNC_PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
FUNCTION COMPLETE_CREATE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_resv_exp_ind    IN     VARCHAR2,
                         I_rms_alloc_nos   IN     ID_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ALLOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_alc_alloc_ids   IN     ID_TBL,
                      I_status          IN     ALLOC_HEADER.STATUS%TYPE)
RETURN NUMBER IS

   L_program               VARCHAR2(61) := 'ALC_SYNC_RMS_SQL.CREATE_ALLOC';
   L_alc_sync_process_id   ALC_SYNC_HEADER_TEMP.ALC_SYNC_PROCESS_ID%TYPE := NULL;
   L_rms_alloc_nos         ID_TBL;
   L_start_time            TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if I_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_status',
                                            'NULL',
                                            'NOT NULL');
      return 0;
   end if;

   if I_status not in ('A', 'R') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_status',
                                            I_status,
                                            'A or R');
      return 0;
   end if;

   select alc_sync_process_id_seq.NEXTVAL
     into L_alc_sync_process_id
     from dual;

   if POP_WORKING_TABLES(O_error_message,
                         I_alc_alloc_ids,
                         I_status,
                         L_alc_sync_process_id) = FALSE then
      return 0;
   end if;

   if ITEM_LOC_RANGE(O_error_message,
                     L_alc_sync_process_id) = FALSE then
      return 0;
   end if;

   if INSERT_TABLES(O_error_message,
                    L_alc_sync_process_id) = FALSE then
      return 0;
   end if;

   if GET_RMS_ALLOC_NOS(O_error_message,
                        L_rms_alloc_nos,
                        I_alc_alloc_ids) = FALSE then
      return 0;
   end if;

   if COMPLETE_CREATE(O_error_message,
                      I_resv_exp_ind  => 'Y',
                      I_rms_alloc_nos => L_rms_alloc_nos) = FALSE then
      return 0;
   end if;

   delete
     from alc_sync_detail_temp
    where alc_sync_process_id = L_alc_sync_process_id;

   delete
     from alc_sync_header_temp
    where alc_sync_process_id = L_alc_sync_process_id;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END CREATE_ALLOC;
-------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ALLOC_STATUS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alc_alloc_ids   IN     ID_TBL,
                             I_status          IN     ALLOC_HEADER.STATUS%TYPE)
RETURN NUMBER IS

   L_program      VARCHAR2(61) := 'ALC_SYNC_RMS_SQL.UPDATE_ALLOC_STATUS';
   L_start_time   TIMESTAMP    := SYSTIMESTAMP;

   L_rms_alloc_nos ID_TBL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if I_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_status',
                                            'NULL',
                                            'NOT NULL');
      return 0;
   end if;

   if I_status NOT IN ('A', 'R') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_status',
                                            I_status,
                                            'A or R');
      return 0;
   end if;

   if GET_RMS_ALLOC_NOS(O_error_message,
                        L_rms_alloc_nos,
                        I_alc_alloc_ids) = FALSE then
      return 0;
   end if;

   update alloc_header ah
      set ah.status   = I_status
    where ah.alloc_no IN (select value(input)
                            from table(cast(L_rms_alloc_nos as ID_TBL)) input);

   if I_status = 'A' then

      if COMPLETE_CREATE(O_error_message,
                         I_resv_exp_ind  => 'N',
                         I_rms_alloc_nos => L_rms_alloc_nos) = FALSE then
         return 0;
      end if;

   elsif I_status = 'R' then

      for i in 1..L_rms_alloc_nos.COUNT loop

         if ALLOC_CHARGE_SQL.DELETE_CHRGS(O_error_message,
                                          L_rms_alloc_nos(i),
                                          NULL) = FALSE then
           return 0;
         end if;
         
         if WF_ALLOC_SQL.DELETE_F_ORDER(O_error_message,
                                        L_rms_alloc_nos(i)) = FALSE then
           return 0;
         end if;

      end loop;

   end if;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END UPDATE_ALLOC_STATUS;
-------------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_ALLOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_alc_alloc_ids   IN     ID_TBL)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'ALC_SYNC_RMS_SQL.REMOVE_ALLOC';
   L_start_time         TIMESTAMP    := SYSTIMESTAMP;

   L_rms_alloc_nos      ID_TBL;
   L_f_order_head_tbl   OBJ_F_ORDER_HEAD_TBL := OBJ_F_ORDER_HEAD_TBL();
   L_dummy_order_no     WF_ORDER_HEAD.WF_ORDER_NO%TYPE;

   cursor C_RMS_ALLOCS is
      select h.alloc_no,
             h.status
        from alc_xref x,
             alloc_header h
       where x.alloc_id IN (select value(input)
                              from table(cast(I_alc_alloc_ids as ID_TBL)) input)
         and x.xref_alloc_no = h.alloc_no;

   cursor C_GET_WF_ORDERS is
      select OBJ_F_ORDER_HEAD_REC(wf_order_no,             -- wf_order_no
                                  NULL,                    -- Order_type
                                  NULL,                    -- Currency Code
                                  NULL,                    -- Status
                                  NULL,                    -- Cancel Reason
                                  NULL,                    -- Comments
                                  NULL)                    -- F_ORDER_DETAILS
        from alloc_detail ad
       where ad.alloc_no IN (select value(input)
                               from table(cast(L_rms_alloc_nos as ID_TBL)) input)
         and wf_order_no is NOT NULL
       group by wf_order_no;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   if GET_RMS_ALLOC_NOS(O_error_message,
                        L_rms_alloc_nos,
                        I_alc_alloc_ids) = FALSE then
      return 0;
   end if;

   if UPDATE_RESV_EXP(O_error_message,
                      L_rms_alloc_nos,
                      I_add_delete_ind => 'D') = FALSE then
      return 0;
   end if;


   for rec in C_RMS_ALLOCS loop

     if rec.status = 'A' then

        if ALLOC_CHARGE_SQL.DELETE_CHRGS(O_error_message,
                                         rec.alloc_no,
                                         NULL) = FALSE then
           return 0;
        end if;

     end if;

   end loop;

   open C_GET_WF_ORDERS;
   fetch C_GET_WF_ORDERS BULK COLLECT into L_f_order_head_tbl;
   close C_GET_WF_ORDERS;

   delete
     from alloc_detail
    where alloc_no IN (select value(input)
                         from table(cast(L_rms_alloc_nos as ID_TBL)) input);

   if L_f_order_head_tbl is NOT NULL and L_f_order_head_tbl.COUNT > 0 then
      for i in L_f_order_head_tbl.first..L_f_order_head_tbl.last loop
         if WF_CREATE_ORDER_SQL.PROCESS_F_ORDER(O_error_message,
                                                L_dummy_order_no,
                                                L_f_order_head_tbl(i),
                                                RMS_CONSTANTS.WF_ACTION_DELETE) = FALSE then
            return 0;
         end if;
      end loop;
   end if;

   delete
     from alloc_header
    where alloc_no IN (select value(input)
                         from table(cast(L_rms_alloc_nos as ID_TBL)) input);

   delete
     from alc_xref
    where alloc_id IN (select value(input)
                         from table(cast(I_alc_alloc_ids as ID_TBL)) input);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END REMOVE_ALLOC;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_RMS_ALLOC_NOS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_rms_alloc_nos      OUT ID_TBL,
                           I_alc_alloc_ids   IN     ID_TBL)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_SYNC_RMS_SQL.GET_RMS_ALLOC_NOS';

BEGIN

   select xref_alloc_no BULK COLLECT
     into O_rms_alloc_nos
     from alc_xref
    where alloc_id IN (select value(input)
                         from table(cast(I_alc_alloc_ids as ID_TBL)) input);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RMS_ALLOC_NOS;
-------------------------------------------------------------------------------------------------------------
FUNCTION POP_WORKING_TABLES(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_alc_alloc_ids         IN     ID_TBL,
                            I_status                IN     ALLOC_HEADER.STATUS%TYPE,
                            I_alc_sync_process_id   IN     ALC_SYNC_HEADER_TEMP.ALC_SYNC_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(61) := 'ALC_SYNC_RMS_SQL.POP_WORKING_TABLES';
   L_alloc_no             ALLOC_HEADER.ALLOC_NO%TYPE;
   L_default_order_type   SYSTEM_OPTIONS.DEFAULT_ORDER_TYPE%TYPE;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_DEFAULT_ORDER_TYPE(O_error_message,
                                                L_default_order_type) = FALSE then
      return FALSE;
   end if;

   -- non ALLOC sources
   insert into alc_sync_header_temp (alc_sync_process_id,
                                     rms_alloc_no,
                                     alc_alloc_id,
                                     order_no,
                                     wh,
                                     item,
                                     status,
                                     alloc_desc,
                                     po_type,
                                     alloc_method,
                                     release_date,
                                     order_type,
                                     context_type,
                                     context_value,
                                     comment_desc,
                                     doc,
                                     doc_type,
                                     origin_ind)
                              select I_alc_sync_process_id,
                                     alloc_order_sequence.NEXTVAL,
                                     inner.alc_alloc_id,
                                     inner.order_no,
                                     inner.wh,
                                     inner.item,
                                     inner.status,
                                     inner.alloc_desc,
                                     inner.po_type,
                                     inner.alloc_method,
                                     inner.release_date,
                                     inner.order_type,
                                     inner.context_type,
                                     inner.context_value,
                                     inner.comment_desc,
                                     inner.doc,
                                     inner.doc_type,
                                     inner.origin_ind
                                from (select DISTINCT
                                             l.alloc_id       alc_alloc_id,
                                             DECODE(l.source_type,
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_PO,  l.order_no,
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_TSF, l.order_no,
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ASN, (select MIN(TO_CHAR(order_no))
                                                                                              from shipment
                                                                                             where asn = l.order_no),
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_BOL, (select MIN(TO_CHAR(distro_no))
                                                                                              from shipsku
                                                                                             where shipment IN (select shipment
                                                                                                                  from shipment
                                                                                                                 where bol_no = l.order_no)),
                                                    NULL) order_no,
                                             l.wh_id          wh,
                                             l.item_id        item,
                                             I_status         status,
                                             a.alloc_desc,
                                             NULL             po_type,
                                             'A'              alloc_method,
                                             l.release_date,
                                             DECODE(l.source_type,
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_OH, L_default_order_type,
                                                    'PREDIST') order_type,
                                             a.context context_type,
                                             DECODE(a.context,
                                                    'PROM', a.promotion,
                                                    NULL) context_value,
                                             a.alloc_comment  comment_desc,
                                             DECODE(l.source_type,
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ASN, l.order_no,
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_BOL, l.order_no,
                                                    NULL) doc,
                                             DECODE(l.source_type,
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_PO,  ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_PO,
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ASN, ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ASN,
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_BOL, ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_BOL,
                                                    ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_TSF, ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_TSF,
                                                    NULL) doc_type,
                                             'ALC' origin_ind
                                        from table(cast(I_alc_alloc_ids as ID_TBL)) input,
                                             alc_alloc a,
                                             alc_item_loc l
                                       where a.alloc_id             = value(input)
                                         and a.alloc_id             = l.alloc_id
                                         and l.source_type         != ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ALLOC
                                         and NVL(l.allocated_qty,0) > 0) inner;

   -- ALLOC sources
   insert into alc_sync_header_temp (alc_sync_process_id,
                                     rms_alloc_no,
                                     alc_alloc_id,
                                     order_no,
                                     wh,
                                     item,
                                     status,
                                     alloc_desc,
                                     po_type,
                                     alloc_method,
                                     release_date,
                                     order_type,
                                     context_type,
                                     context_value,
                                     comment_desc,
                                     doc,
                                     doc_type,
                                     origin_ind)
                              select I_alc_sync_process_id,
                                     alloc_order_sequence.NEXTVAL,
                                     inner.alc_alloc_id,
                                     inner.order_no,
                                     inner.wh,
                                     inner.item,
                                     inner.status,
                                     inner.alloc_desc,
                                     inner.po_type,
                                     inner.alloc_method,
                                     inner.release_date,
                                     inner.order_type,
                                     inner.context_type,
                                     inner.context_value,
                                     inner.comment_desc,
                                     inner.doc,
                                     inner.doc_type,
                                     inner.origin_ind
                                from (select DISTINCT
                                             l.alloc_id       alc_alloc_id,
                                             x.xref_alloc_no  order_no,
                                             l.wh_id          wh,
                                             l.item_id        item,
                                             I_status         status,
                                             a.alloc_desc,
                                             NULL             po_type,
                                             'A'              alloc_method,
                                             l.release_date,
                                             'PREDIST'        order_type,
                                             DECODE(a.context,
                                                    'PROM', a.context,
                                                    NULL) context_type,
                                             DECODE(a.context,
                                                    'PROM', a.promotion,
                                                    NULL) context_value,
                                             a.alloc_comment  comment_desc,
                                             NULL             doc,
                                             ALC_CONSTANTS_SQL.CHAR_SOURCE_TYPE_ALLOC doc_type,
                                             'ALC'            origin_ind
                                        from table(cast(I_alc_alloc_ids as ID_TBL)) input,
                                             alc_alloc a,
                                             alc_item_loc l,
                                             alc_xref x
                                       where a.alloc_id             = value(input)
                                         and a.alloc_id             = l.alloc_id
                                         and l.source_type          = ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ALLOC
                                         and NVL(l.allocated_qty,0) > 0
                                         and l.order_no             = x.alloc_id
                                         and l.item_id              = x.item_id
                                         ) inner;

   -- non ALLOC sources
   insert into alc_sync_detail_temp (alc_sync_process_id,
                                     rms_alloc_no,
                                     alc_alloc_id,
                                     to_loc,
                                     to_loc_type,
                                     qty_transferred,
                                     qty_allocated,
                                     qty_prescaled,
                                     non_scale_ind,
                                     in_store_date,
                                     rush_flag)
                              select I_alc_sync_process_id,
                                     h.rms_alloc_no,
                                     h.alc_alloc_id,
                                     l.location_id,
                                     l.loc_type,
                                     NULL,
                                     l.allocated_qty,
                                     l.allocated_qty,
                                     'Y',
                                     l.in_store_date,
                                     'N'
                                from alc_sync_header_temp h,
                                     alc_item_loc l
                               where h.alc_sync_process_id  = I_alc_sync_process_id
                                 and h.alc_alloc_id         = l.alloc_id
                                 and h.item                 = l.item_id
                                 and h.wh                   = l.wh_id
                                 and l.source_type         != ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ALLOC
                                 and NVL(l.allocated_qty,0) > 0
                                 --
                                 and ((        l.source_type IN (ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_PO,
                                                                 ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_TSF)
                                           and h.order_no = l.order_no)
                                       or (    l.source_type IN (ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ASN,
                                                                 ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_BOL)
                                           and h.doc = l.order_no)
                                       or (    l.source_type IN (ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_OH,
                                                                 ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_WHATIF)
                                           and h.doc is NULL 
                                           and h.order_no is NULL));

   -- ALLOC sources
   insert into alc_sync_detail_temp (alc_sync_process_id,
                                     rms_alloc_no,
                                     alc_alloc_id,
                                     to_loc,
                                     to_loc_type,
                                     qty_transferred,
                                     qty_allocated,
                                     qty_prescaled,
                                     non_scale_ind,
                                     in_store_date,
                                     rush_flag)
                              select I_alc_sync_process_id,
                                     i.rms_alloc_no,
                                     i.alc_alloc_id,
                                     i.location_id,
                                     i.loc_type,
                                     NULL qty_transferred,
                                     --
                                     case
                                        when i.rownbr = i.counter then
                                           FLOOR(i.allocated_qty*i.ratio) + SUM(MOD(i.allocated_qty*i.ratio,1)) OVER (PARTITION BY i.order_no,
                                                                                                                                   i.item_id,
                                                                                                                                   i.wh_id,
                                                                                                                                   i.location_id)
                                        else
                                           FLOOR(i.allocated_qty*i.ratio)
                                     end qty_allocated,
                                     --
                                     case
                                        when i.rownbr = i.counter then
                                           FLOOR(i.allocated_qty*i.ratio) + SUM(MOD(i.allocated_qty*i.ratio,1)) OVER (PARTITION BY i.order_no,
                                                                                                                                   i.item_id,
                                                                                                                                   i.wh_id,
                                                                                                                                   i.location_id)
                                        else
                                           FLOOR(i.allocated_qty*i.ratio)
                                     end qty_prescaled,
                                     --
                                     'Y',
                                     i.in_store_date,
                                     'N'
                                from (select h.rms_alloc_no,
                                             h.alc_alloc_id,
                                             l.order_no,
                                             l.location_id,
                                             l.loc_type,
                                             l.allocated_qty,
                                             l.in_store_date,
                                             l.item_id,
                                             l.wh_id,
                                             --
                                             row_number() OVER (PARTITION BY l.order_no,
                                                                             l.item_id,
                                                                             l.wh_id,
                                                                             l.location_id
                                                                    ORDER BY ah.alloc_no) rownbr,
                                             COUNT(*) OVER (PARTITION BY l.order_no,
                                                                         l.item_id,
                                                                         l.wh_id,
                                                                         l.location_id) counter,
                                             --
                                             (SUM(ad.qty_allocated - NVL(ad.qty_received, 0)) OVER (PARTITION BY l.order_no,
                                                                                                                 ah.alloc_no,
                                                                                                                 ah.item,
                                                                                                                 ad.to_loc)) /
                                             (SUM(ad.qty_allocated - NVL(ad.qty_received, 0)) OVER (PARTITION BY l.order_no,
                                                                                                                 ah.item,
                                                                                                                 ad.to_loc)) ratio
                                             --
                                        from alc_sync_header_temp h,
                                             alc_item_loc l,
                                             alc_xref x,
                                             alloc_header ah,
                                             alloc_detail ad
                                       where h.alc_sync_process_id  = I_alc_sync_process_id
                                         and h.alc_alloc_id         = l.alloc_id
                                         and h.item                 = l.item_id
                                         and h.wh                   = l.wh_id
                                         and l.source_type          = ALC_CONSTANTS_SQL.NUM_SOURCE_TYPE_ALLOC
                                         and NVL(l.allocated_qty,0) > 0
                                         --
                                         and l.order_no             = x.alloc_id
                                         and l.item_id              = x.item_id
                                         and h.order_no             = x.xref_alloc_no
                                         --
                                         and ah.alloc_no            = xref_alloc_no
                                         and ah.alloc_no            = ad.alloc_no
                                         and ad.to_loc              = l.wh_id) i;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POP_WORKING_TABLES;
-------------------------------------------------------------------------------------------------------------
FUNCTION ITEM_LOC_RANGE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_alc_sync_process_id   IN     ALC_SYNC_HEADER_TEMP.ALC_SYNC_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(61) := 'ALC_SYNC_RMS_SQL.ITEM_LOC_RANGE';
   L_input_tbl   NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;

   cursor C_NON_RANGED is
      select i2.item,         -- item
             NULL,            -- item_parent
             NULL,            -- item_grandparent
             NULL,            -- item_short_desc
             NULL,            -- dept
             NULL,            -- item_class
             NULL,            -- subclass
             NULL,            -- item_level
             NULL,            -- tran_level
             NULL,            -- item_status
             NULL,            -- waste_type
             NULL,            -- sellable_ind
             NULL,            -- orderable_ind
             NULL,            -- pack_ind
             NULL,            -- pack_type
             NULL,            -- item_desc
             NULL,            -- diff_1
             NULL,            -- diff_2
             NULL,            -- diff_3
             NULL,            -- diff_4
             i2.to_loc,       -- loc
             i2.to_loc_type,  -- loc_type
             NULL,            -- daily_waste_pct
             NULL,            -- unit_cost_loc
             NULL,            -- unit_retail_loc
             NULL,            -- selling_retail_loc
             NULL,            -- selling_uom
             NULL,            -- multi_units
             NULL,            -- multi_unit_retail
             NULL,            -- multi_selling_uom
             NULL,            -- item_loc_status
             NULL,            -- taxable_ind
             NULL,            -- ti
             NULL,            -- hi
             NULL,            -- store_ord_mult
             NULL,            -- meas_of_each
             NULL,            -- meas_of_price
             NULL,            -- uom_of_price
             NULL,            -- primary_variant
             NULL,            -- primary_supp
             NULL,            -- primary_cntry
             NULL,            -- local_item_desc
             NULL,            -- local_short_desc
             NULL,            -- primary_cost_pack
             NULL,            -- receive_as_type
             NULL,            -- store_price_ind
             NULL,            -- uin_type
             NULL,            -- uin_label
             NULL,            -- capture_time
             NULL,            -- ext_uin_ind
             NULL,            -- source_method
             NULL,            -- source_wh
             NULL,            -- inbound_handling_days
             NULL,            -- currency_code
             NULL,            -- like_store
             NULL,            -- default_to_children_ind
             NULL,            -- class_vat_ind
             NULL,            -- hier_level
             NULL,            -- hier_num_value
             NULL,            -- hier_char_value
             NULL,            -- costing_loc
             NULL,            -- costing_loc_type
             NULL,            -- ranged_ind
             NULL,            -- default_wh
             NULL             -- item_loc_ind
        from (select DISTINCT i.item,
                     i.to_loc,
                     i.to_loc_type,
                     il.status
                from item_loc il,
                     (select h.item,
                             d.to_loc,
                             d.to_loc_type
                        from alc_sync_header_temp h,
                             alc_sync_detail_temp d
                       where h.alc_sync_process_id = I_alc_sync_process_id
                         and h.alc_sync_process_id = d.alc_sync_process_id
                         and h.rms_alloc_no = d.rms_alloc_no) i
               where i.item   = il.item(+)
                 and i.to_loc = il.loc(+)) i2
       where i2.status is NULL;

BEGIN

   open C_NON_RANGED;
   fetch C_NON_RANGED BULK COLLECT into L_input_tbl;
   close C_NON_RANGED;

   if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message,
                                    L_input_tbl,
                                    'N') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_LOC_RANGE;
-------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_RESV_EXP(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_allocs           IN     ID_TBL,
                         I_add_delete_ind   IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program       VARCHAR2(61) := 'ALC_SYNC_RMS_SQL.UPDATE_RESV_EXP';

   cursor C_NON_XDOCK is
      select h.alloc_no
        from alloc_header h
       where h.alloc_no IN (select value(input)
                              from table(cast(I_allocs as ID_TBL)) input)
         and h.order_no is NULL;

BEGIN

   for rec in C_NON_XDOCK loop

     if ALLOC_ATTRIB_SQL.UPD_ALLOC_RESV_EXP(O_error_message,
                                            rec.alloc_no,
                                            I_add_delete_ind) = FALSE then
       return FALSE;
     end if;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_RESV_EXP;
-------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_TABLES(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_alc_sync_process_id   IN     ALC_SYNC_HEADER_TEMP.ALC_SYNC_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'ALC_SYNC_RMS_SQL.INSERT_TABLES';

BEGIN

   insert into alloc_header (alloc_no,
                             order_no,
                             wh,
                             item,
                             status,
                             alloc_desc,
                             po_type,
                             alloc_method,
                             release_date,
                             order_type,
                             context_type,
                             context_value,
                             comment_desc,
                             doc,
                             doc_type,
                             alloc_parent,
                             origin_ind,
                             close_date)
                      select h.rms_alloc_no,
                             h.order_no,
                             h.wh,
                             h.item,
                             h.status,
                             h.alloc_desc,
                             h.po_type,
                             h.alloc_method,
                             h.release_date,
                             h.order_type,
                             h.context_type,
                             h.context_value,
                             h.comment_desc,
                             h.doc,
                             h.doc_type,
                             NULL,  --alloc_parent
                             h.origin_ind,
                             NULL   --close_date
                        from alc_sync_header_temp h
                       where h.alc_sync_process_id = I_alc_sync_process_id;

   insert into alloc_detail (alloc_no,
                             to_loc,
                             to_loc_type,
                             qty_transferred,
                             qty_allocated,
                             qty_prescaled,
                             qty_distro,
                             qty_selected,
                             qty_cancelled,
                             qty_received,
                             qty_reconciled,
                             po_rcvd_qty,
                             non_scale_ind,
                             in_store_date,
                             rush_flag,
                             wf_order_no,
                             processed_ind)
                      select rms_alloc_no,
                             to_loc,
                             to_loc_type,
                             qty_transferred,
                             qty_allocated,
                             qty_prescaled,
                             NULL qty_distro,
                             NULL qty_selected,
                             NULL qty_cancelled,
                             NULL qty_received,
                             NULL qty_reconciled,
                             NULL po_rcvd_qty,
                             'N',
                             NULL,
                             NULL,
                             NULL,
                             'N'
                        from alc_sync_detail_temp d
                       where d.alc_sync_process_id = I_alc_sync_process_id;

   insert into alc_xref (xref_id,
                         alloc_id,
                         item_id,
                         wh_id,
                         release_date,
                         order_no,
                         allocated_qty,
                         xref_alloc_no,
                         close_ind)
                  select alc_xref_seq.NEXTVAL,
                         h.alc_alloc_id,
                         h.item,
                         h.wh,
                         h.release_date,
                         NVL(h.doc, h.order_no),
                         d.qty_allocated,
						 h.rms_alloc_no,
						 null
					from alc_sync_header_temp h,
						 (select 
						  sum(qty_allocated) qty_allocated, rms_alloc_no
						  from alc_sync_detail_temp a
						  where a.alc_sync_process_id = I_alc_sync_process_id
						  group by rms_alloc_no) d
					where h.alc_sync_process_id = I_alc_sync_process_id
					and h.rms_alloc_no = d.rms_alloc_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_TABLES;
-------------------------------------------------------------------------------------------------------------
FUNCTION COMPLETE_CREATE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_resv_exp_ind    IN     VARCHAR2,
                         I_rms_alloc_nos   IN     ID_TBL)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(61)               := 'ALC_SYNC_RMS_SQL.COMPLETE_CREATE';

   L_wf_order_nos              OBJ_ALLOC_LINKED_F_ORD_TBL := OBJ_ALLOC_LINKED_F_ORD_TBL();
   L_input_alloc_status_tbl    OBJ_ALLOC_STATUS_TBL       := OBJ_ALLOC_STATUS_TBL();
   L_output_alloc_status_tbl   OBJ_ALLOC_STATUS_TBL       := OBJ_ALLOC_STATUS_TBL();

   cursor C_ITEM_LOCS is
      select h.alloc_no,
             h.wh,
             d.to_loc,
             d.to_loc_type,
             h.item
        from table(cast(I_rms_alloc_nos as ID_TBL)) input,
             alloc_header h,
             alloc_detail d
       where h.alloc_no = value(input)
         and h.alloc_no = d.alloc_no
         and h.status   = 'A';

   cursor C_RMS_ALLOC_STATUS is
      select OBJ_ALLOC_STATUS_REC(h.alloc_no,
                                  h.status)
        from table(cast(I_rms_alloc_nos as ID_TBL)) input,
             alloc_header h
       where h.alloc_no = value(input)
         and h.status = 'A';

BEGIN

   for rec in C_ITEM_LOCS loop
      if ALLOC_CHARGE_SQL.DEFAULT_CHRGS(O_error_message,
                                        rec.alloc_no,
                                        rec.wh,
                                        rec.to_loc,
                                        rec.to_loc_type,
                                        rec.item) = FALSE then
         return FALSE;
      end if;
   end loop;

   ---

   if I_resv_exp_ind = 'Y' then

      if UPDATE_RESV_EXP(O_error_message,
                         I_rms_alloc_nos,
                         I_add_delete_ind => 'A') = FALSE then
         return FALSE;
      end if;

   end if;

   ---

   open C_RMS_ALLOC_STATUS;
   fetch C_RMS_ALLOC_STATUS BULK COLLECT into L_input_alloc_status_tbl;
   close C_RMS_ALLOC_STATUS;

   if L_input_alloc_status_tbl is NOT NULL and L_input_alloc_status_tbl.COUNT > 0 then

      if WF_ALLOC_SQL.BULK_SYNC_F_ORDER(O_error_message,
                                        L_wf_order_nos,
                                        L_output_alloc_status_tbl,
                                        L_input_alloc_status_tbl,
                                        RMS_CONSTANTS.WF_ACTION_CREATE) = FALSE then
         return FALSE;
      end if;
   end if;

   --check return status for W which equals credit check failure
   if L_output_alloc_status_tbl is NOT NULL and L_output_alloc_status_tbl.COUNT > 0 then
      for i in 1..L_output_alloc_status_tbl.COUNT loop
         if L_output_alloc_status_tbl(i).status = 'W' then
            O_error_message := ALC_CONSTANTS_SQL.ERRMSG_CREDIT_CHECK;
            return FALSE;
         end if;
      end loop;
   end if;

   --update alloc_detail with wh_order_no
   merge into alloc_detail ad
   using table(cast(L_wf_order_nos AS OBJ_ALLOC_LINKED_F_ORD_TBL)) wfo
   on (    ad.alloc_no    = wfo.alloc_no
       and ad.to_loc      = wfo.to_loc
       and ad.to_loc_type = wfo.to_loc_type)
   when MATCHED then
      update
         set ad.wf_order_no = wfo.wf_order_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END COMPLETE_CREATE;
-------------------------------------------------------------------------------------------------------------
END ALC_SYNC_RMS_SQL;
/
